/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: sd1lv2lw.c,v 1.12 2017/12/28 10:01:47 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
#include "lldinc.h"
#include "lldcli.h"
/* LOW LEVEL Routines for Table : LldpV2Xdot1ConfigPortVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1ConfigPortVlanTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot1ConfigPortVlanTable (INT4
                                                        i4LldpV2PortConfigIfIndex,
                                                        UINT4
                                                        u4LldpV2PortConfigDestAddressIndex)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: u4LldpV2PortConfigDestAddressIndex is Wrong\r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlValidateIndexInstanceLldpXdot1ConfigPortVlanTable
        (i4LldpV2PortConfigIfIndex,
         pTempDestTbl->Lldpv2DestMacAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot1ConfigPortVlanTable failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1ConfigPortVlanTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1ConfigPortVlanTable (INT4
                                                *pi4LldpV2PortConfigIfIndex,
                                                UINT4
                                                *pu4LldpV2PortConfigDestAddressIndex)
{
    return (nmhGetNextIndexLldpV2Xdot1ConfigPortVlanTable (LLDP_ZERO,
                                                           pi4LldpV2PortConfigIfIndex,
                                                           LLDP_ZERO,
                                                           pu4LldpV2PortConfigDestAddressIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1ConfigPortVlanTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                nextLldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
                nextLldpV2PortConfigDestAddressIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot1ConfigPortVlanTable (INT4 i4LldpV2PortConfigIfIndex,
                                               INT4
                                               *pi4NextLldpV2PortConfigIfIndex,
                                               UINT4
                                               u4LldpV2PortConfigDestAddressIndex,
                                               UINT4
                                               *pu4NextLldpV2PortConfigDestAddressIndex)
{
    tLldpv2AgentToLocPort Lldpv2AgentToLoc;
    tLldpv2AgentToLocPort *pLldpv2AgentToLocPort = NULL;

    MEMSET (&Lldpv2AgentToLoc, 0, sizeof (tLldpv2AgentToLocPort));

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    Lldpv2AgentToLoc.i4IfIndex = i4LldpV2PortConfigIfIndex;
    Lldpv2AgentToLoc.u4DstMacAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pLldpv2AgentToLocPort =
        (tLldpv2AgentToLocPort *) RBTreeGetNext (gLldpGlobalInfo.
                                                 Lldpv2AgentMapTblRBTree,
                                                 &Lldpv2AgentToLoc,
                                                 LldpAgentToLocPortIndexUtlRBCmpInfo);

    if (pLldpv2AgentToLocPort != NULL)
    {
        *pi4NextLldpV2PortConfigIfIndex = pLldpv2AgentToLocPort->i4IfIndex;
        *pu4NextLldpV2PortConfigDestAddressIndex =
            pLldpv2AgentToLocPort->u4DstMacAddrTblIndex;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1ConfigPortVlanTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                retValLldpV2Xdot1ConfigPortVlanTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1ConfigPortVlanTxEnable (INT4 i4LldpV2PortConfigIfIndex,
                                         UINT4
                                         u4LldpV2PortConfigDestAddressIndex,
                                         INT4
                                         *pi4RetValLldpV2Xdot1ConfigPortVlanTxEnable)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: u4LldpV2PortConfigDestAddressIndex is Wrong\r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlGetLldpXdot1ConfigPortVlanTxEnable
        (i4LldpV2PortConfigIfIndex, pTempDestTbl->Lldpv2DestMacAddress,
         pi4RetValLldpV2Xdot1ConfigPortVlanTxEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot1ConfigPortVlanTxEnable failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpV2Xdot1ConfigPortVlanTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                setValLldpV2Xdot1ConfigPortVlanTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpV2Xdot1ConfigPortVlanTxEnable (INT4 i4LldpV2PortConfigIfIndex,
                                         UINT4
                                         u4LldpV2PortConfigDestAddressIndex,
                                         INT4
                                         i4SetValLldpV2Xdot1ConfigPortVlanTxEnable)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: u4LldpV2PortConfigDestAddressIndex is Wrong\r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlSetLldpXdot1ConfigPortVlanTxEnable
        (i4LldpV2PortConfigIfIndex, pTempDestTbl->Lldpv2DestMacAddress,
         i4SetValLldpV2Xdot1ConfigPortVlanTxEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlSetLldpXdot1ConfigPortVlanTxEnable failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpV2Xdot1ConfigPortVlanTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                testValLldpV2Xdot1ConfigPortVlanTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpV2Xdot1ConfigPortVlanTxEnable (UINT4 *pu4ErrorCode,
                                            INT4 i4LldpV2PortConfigIfIndex,
                                            UINT4
                                            u4LldpV2PortConfigDestAddressIndex,
                                            INT4
                                            i4TestValLldpV2Xdot1ConfigPortVlanTxEnable)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: u4LldpV2PortConfigDestAddressIndex is Wrong\r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlTestLldpXdot1ConfigPortVlanTxEnable
        (pu4ErrorCode, i4LldpV2PortConfigIfIndex,
         i4TestValLldpV2Xdot1ConfigPortVlanTxEnable,
         (UINT1 *) pTempDestTbl->Lldpv2DestMacAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlTestLldpXdot1ConfigPortVlanTxEnable failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpV2Xdot1ConfigPortVlanTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpV2Xdot1ConfigPortVlanTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot1ConfigVlanNameTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1ConfigVlanNameTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot1ConfigVlanNameTable (INT4
                                                        i4LldpV2LocPortIfIndex,
                                                        INT4
                                                        i4LldpV2Xdot1LocVlanId)
{
    if (LldpUtlValidateIndexInstanceLldpXdot1ConfigVlanNameTable
        (i4LldpV2LocPortIfIndex, i4LldpV2Xdot1LocVlanId) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot1ConfigVlanNameTable"
                  " failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1ConfigVlanNameTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1ConfigVlanNameTable (INT4 *pi4LldpV2LocPortIfIndex,
                                                INT4 *pi4LldpV2Xdot1LocVlanId)
{
    if (LldpUtlGetFirstIndexLldpXdot1ConfigVlanNameTable
        (pi4LldpV2LocPortIfIndex, pi4LldpV2Xdot1LocVlanId) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot1ConfigVlanNameTable"
                  " failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1ConfigVlanNameTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
                LldpV2Xdot1LocVlanId
                nextLldpV2Xdot1LocVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot1ConfigVlanNameTable (INT4 i4LldpV2LocPortIfIndex,
                                               INT4
                                               *pi4NextLldpV2LocPortIfIndex,
                                               INT4 i4LldpV2Xdot1LocVlanId,
                                               INT4
                                               *pi4NextLldpV2Xdot1LocVlanId)
{
    if (LldpUtlGetNextIndexLldpXdot1ConfigVlanNameTable
        (i4LldpV2LocPortIfIndex, pi4NextLldpV2LocPortIfIndex,
         i4LldpV2Xdot1LocVlanId, pi4NextLldpV2Xdot1LocVlanId) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot1ConfigVlanNameTable"
                  " failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1ConfigVlanNameTxEnable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocVlanId

                The Object 
                retValLldpV2Xdot1ConfigVlanNameTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1ConfigVlanNameTxEnable (INT4 i4LldpV2LocPortIfIndex,
                                         INT4 i4LldpV2Xdot1LocVlanId,
                                         INT4
                                         *pi4RetValLldpV2Xdot1ConfigVlanNameTxEnable)
{
    if (LldpUtlGetLldpXdot1ConfigVlanNameTxEnable
        (i4LldpV2LocPortIfIndex, i4LldpV2Xdot1LocVlanId,
         pi4RetValLldpV2Xdot1ConfigVlanNameTxEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot1ConfigVlanNameTxEnable"
                  " failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpV2Xdot1ConfigVlanNameTxEnable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocVlanId

                The Object 
                setValLldpV2Xdot1ConfigVlanNameTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpV2Xdot1ConfigVlanNameTxEnable (INT4 i4LldpV2LocPortIfIndex,
                                         INT4 i4LldpV2Xdot1LocVlanId,
                                         INT4
                                         i4SetValLldpV2Xdot1ConfigVlanNameTxEnable)
{
    if (LldpUtlSetLldpXdot1ConfigVlanNameTxEnable
        (i4LldpV2LocPortIfIndex, i4LldpV2Xdot1LocVlanId,
         i4SetValLldpV2Xdot1ConfigVlanNameTxEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlSetLldpXdot1ConfigVlanNameTxEnable"
                  " failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpV2Xdot1ConfigVlanNameTxEnable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocVlanId

                The Object 
                testValLldpV2Xdot1ConfigVlanNameTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpV2Xdot1ConfigVlanNameTxEnable (UINT4 *pu4ErrorCode,
                                            INT4 i4LldpV2LocPortIfIndex,
                                            INT4 i4LldpV2Xdot1LocVlanId,
                                            INT4
                                            i4TestValLldpV2Xdot1ConfigVlanNameTxEnable)
{
    if (LldpUtlTestLldpXdot1ConfigVlanNameTxEnable
        (pu4ErrorCode, i4LldpV2LocPortIfIndex, i4LldpV2Xdot1LocVlanId,
         i4TestValLldpV2Xdot1ConfigVlanNameTxEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlTestLldpXdot1ConfigVlanNameTxEnable failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpV2Xdot1ConfigVlanNameTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocVlanId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpV2Xdot1ConfigVlanNameTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot1ConfigProtoVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1ConfigProtoVlanTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocProtoVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot1ConfigProtoVlanTable (INT4
                                                         i4LldpV2LocPortIfIndex,
                                                         INT4
                                                         i4LldpV2Xdot1LocProtoVlanId)
{
    if (LldpUtlValidateIndexInstanceLldpXdot1ConfigProtoVlanTable
        (i4LldpV2LocPortIfIndex, i4LldpV2Xdot1LocProtoVlanId) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot1ConfigProtoVlanTable"
                  " failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1ConfigProtoVlanTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocProtoVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1ConfigProtoVlanTable (INT4 *pi4LldpV2LocPortIfIndex,
                                                 INT4
                                                 *pi4LldpV2Xdot1LocProtoVlanId)
{
    if (LldpUtlGetFirstIndexLldpXdot1ConfigProtoVlanTable
        (pi4LldpV2LocPortIfIndex, pi4LldpV2Xdot1LocProtoVlanId) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpXdot1ConfigProtoVlanTable"
                  " failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1ConfigProtoVlanTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
                LldpV2Xdot1LocProtoVlanId
                nextLldpV2Xdot1LocProtoVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot1ConfigProtoVlanTable (INT4 i4LldpV2LocPortIfIndex,
                                                INT4
                                                *pi4NextLldpV2LocPortIfIndex,
                                                INT4
                                                i4LldpV2Xdot1LocProtoVlanId,
                                                INT4
                                                *pi4NextLldpV2Xdot1LocProtoVlanId)
{
    if (LldpUtlGetNextIndexLldpXdot1ConfigProtoVlanTable
        (i4LldpV2LocPortIfIndex, pi4NextLldpV2LocPortIfIndex,
         i4LldpV2Xdot1LocProtoVlanId,
         pi4NextLldpV2Xdot1LocProtoVlanId) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpXdot1ConfigProtoVlanTable"
                  " failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1ConfigProtoVlanTxEnable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocProtoVlanId

                The Object 
                retValLldpV2Xdot1ConfigProtoVlanTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1ConfigProtoVlanTxEnable (INT4 i4LldpV2LocPortIfIndex,
                                          INT4 i4LldpV2Xdot1LocProtoVlanId,
                                          INT4
                                          *pi4RetValLldpV2Xdot1ConfigProtoVlanTxEnable)
{
    if (LldpUtlGetLldpXdot1ConfigProtoVlanTxEnable
        (i4LldpV2LocPortIfIndex, i4LldpV2Xdot1LocProtoVlanId,
         pi4RetValLldpV2Xdot1ConfigProtoVlanTxEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot1ConfigProtoVlanTxEnable failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpV2Xdot1ConfigProtoVlanTxEnable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocProtoVlanId

                The Object 
                setValLldpV2Xdot1ConfigProtoVlanTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpV2Xdot1ConfigProtoVlanTxEnable (INT4 i4LldpV2LocPortIfIndex,
                                          INT4 i4LldpV2Xdot1LocProtoVlanId,
                                          INT4
                                          i4SetValLldpV2Xdot1ConfigProtoVlanTxEnable)
{
    if (LldpUtlSetLldpXdot1ConfigProtoVlanTxEnable
        (i4LldpV2LocPortIfIndex, i4LldpV2Xdot1LocProtoVlanId,
         i4SetValLldpV2Xdot1ConfigProtoVlanTxEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlSetLldpXdot1ConfigProtoVlanTxEnable failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpV2Xdot1ConfigProtoVlanTxEnable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocProtoVlanId

                The Object 
                testValLldpV2Xdot1ConfigProtoVlanTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpV2Xdot1ConfigProtoVlanTxEnable (UINT4 *pu4ErrorCode,
                                             INT4 i4LldpV2LocPortIfIndex,
                                             INT4 i4LldpV2Xdot1LocProtoVlanId,
                                             INT4
                                             i4TestValLldpV2Xdot1ConfigProtoVlanTxEnable)
{
    if (LldpUtlTestv2LldpXdot1ConfigProtoVlanTxEnable
        (pu4ErrorCode, i4LldpV2LocPortIfIndex, i4LldpV2Xdot1LocProtoVlanId,
         i4TestValLldpV2Xdot1ConfigProtoVlanTxEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlTestv2LldpXdot1ConfigProtoVlanTxEnable failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpV2Xdot1ConfigProtoVlanTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocProtoVlanId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpV2Xdot1ConfigProtoVlanTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot1ConfigProtocolTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1ConfigProtocolTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocProtocolIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot1ConfigProtocolTable (INT4
                                                        i4LldpV2LocPortIfIndex,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pLldpV2Xdot1LocProtocolIndex)
{
    /*This Feature is not supported */
    UNUSED_PARAM (i4LldpV2LocPortIfIndex);
    UNUSED_PARAM (pLldpV2Xdot1LocProtocolIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1ConfigProtocolTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocProtocolIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1ConfigProtocolTable (INT4 *pi4LldpV2LocPortIfIndex,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pLldpV2Xdot1LocProtocolIndex)
{
    UNUSED_PARAM (pi4LldpV2LocPortIfIndex);
    UNUSED_PARAM (pLldpV2Xdot1LocProtocolIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1ConfigProtocolTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
                LldpV2Xdot1LocProtocolIndex
                nextLldpV2Xdot1LocProtocolIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot1ConfigProtocolTable (INT4 i4LldpV2LocPortIfIndex,
                                               INT4
                                               *pi4NextLldpV2LocPortIfIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pLldpV2Xdot1LocProtocolIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pNextLldpV2Xdot1LocProtocolIndex)
{
    UNUSED_PARAM (i4LldpV2LocPortIfIndex);
    UNUSED_PARAM (pi4NextLldpV2LocPortIfIndex);
    UNUSED_PARAM (pLldpV2Xdot1LocProtocolIndex);
    UNUSED_PARAM (pNextLldpV2Xdot1LocProtocolIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1ConfigProtocolTxEnable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocProtocolIndex

                The Object 
                retValLldpV2Xdot1ConfigProtocolTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1ConfigProtocolTxEnable (INT4 i4LldpV2LocPortIfIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pLldpV2Xdot1LocProtocolIndex,
                                         INT4
                                         *pi4RetValLldpV2Xdot1ConfigProtocolTxEnable)
{
    UNUSED_PARAM (i4LldpV2LocPortIfIndex);
    UNUSED_PARAM (pLldpV2Xdot1LocProtocolIndex);
    UNUSED_PARAM (pi4RetValLldpV2Xdot1ConfigProtocolTxEnable);
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpV2Xdot1ConfigProtocolTxEnable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocProtocolIndex

                The Object 
                setValLldpV2Xdot1ConfigProtocolTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpV2Xdot1ConfigProtocolTxEnable (INT4 i4LldpV2LocPortIfIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pLldpV2Xdot1LocProtocolIndex,
                                         INT4
                                         i4SetValLldpV2Xdot1ConfigProtocolTxEnable)
{
    UNUSED_PARAM (i4LldpV2LocPortIfIndex);
    UNUSED_PARAM (pLldpV2Xdot1LocProtocolIndex);
    UNUSED_PARAM (i4SetValLldpV2Xdot1ConfigProtocolTxEnable);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpV2Xdot1ConfigProtocolTxEnable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocProtocolIndex

                The Object 
                testValLldpV2Xdot1ConfigProtocolTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpV2Xdot1ConfigProtocolTxEnable (UINT4 *pu4ErrorCode,
                                            INT4 i4LldpV2LocPortIfIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pLldpV2Xdot1LocProtocolIndex,
                                            INT4
                                            i4TestValLldpV2Xdot1ConfigProtocolTxEnable)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4LldpV2LocPortIfIndex);
    UNUSED_PARAM (pLldpV2Xdot1LocProtocolIndex);
    UNUSED_PARAM (i4TestValLldpV2Xdot1ConfigProtocolTxEnable);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpV2Xdot1ConfigProtocolTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocProtocolIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpV2Xdot1ConfigProtocolTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot1ConfigVidUsageDigestTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1ConfigVidUsageDigestTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot1ConfigVidUsageDigestTable (INT4
                                                              i4LldpV2LocPortIfIndex)
{
    tLldpLocPortTable  *pLldpPortTable = NULL;
    tLldpLocPortTable   LldpPortTable;

    MEMSET (&LldpPortTable, 0, sizeof (tLldpLocPortTable));

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if ((i4LldpV2LocPortIfIndex < LLDP_MIN_PORTS) ||
        ((UINT4) i4LldpV2LocPortIfIndex > LLDP_MAX_PORTS))
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Index Validation Failed!!\n");
        return SNMP_FAILURE;
    }

    LldpPortTable.i4IfIndex = i4LldpV2LocPortIfIndex;
    pLldpPortTable = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpPortTable);
    if ((pLldpPortTable != NULL)
        && (pLldpPortTable->u1VidUsageDigestTlvTxEnabled == LLDP_TRUE))
    {
        return SNMP_SUCCESS;
    }
    LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
              " SNMPSTD: Table Does not exsist!!\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1ConfigVidUsageDigestTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1ConfigVidUsageDigestTable (INT4
                                                      *pi4LldpV2LocPortIfIndex)
{
    return (nmhGetNextIndexLldpV2Xdot1ConfigVidUsageDigestTable (LLDP_ZERO,
                                                                 pi4LldpV2LocPortIfIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1ConfigVidUsageDigestTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot1ConfigVidUsageDigestTable (INT4
                                                     i4LldpV2LocPortIfIndex,
                                                     INT4
                                                     *pi4NextLldpV2LocPortIfIndex)
{
    tLldpLocPortTable  *pLldpPortTable = NULL;
    tLldpLocPortTable   LldpPortTable;

    MEMSET (&LldpPortTable, 0, sizeof (tLldpLocPortTable));
    LldpPortTable.i4IfIndex = i4LldpV2LocPortIfIndex;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pLldpPortTable = (tLldpLocPortTable *)
        RBTreeGetNext (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpPortTable,
                       LldpPortInfoUtlRBCmpInfo);
    while (pLldpPortTable != NULL)
    {
        if (pLldpPortTable->u1VidUsageDigestTlvTxEnabled == LLDP_TRUE)
        {
            *pi4NextLldpV2LocPortIfIndex = pLldpPortTable->i4IfIndex;
            return SNMP_SUCCESS;

        }
        LldpPortTable.i4IfIndex = pLldpPortTable->i4IfIndex;
        pLldpPortTable = (tLldpLocPortTable *)
            RBTreeGetNext (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpPortTable,
                           LldpPortInfoUtlRBCmpInfo);
    }
    LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
              " SNMPSTD: Table Does not exsist!!\n");
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1ConfigVidUsageDigestTxEnable
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2Xdot1ConfigVidUsageDigestTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1ConfigVidUsageDigestTxEnable (INT4 i4LldpV2LocPortIfIndex,
                                               INT4
                                               *pi4RetValLldpV2Xdot1ConfigVidUsageDigestTxEnable)
{
    tLldpLocPortTable  *pLldpPortTable = NULL;
    tLldpLocPortTable   LldpPortTable;

    MEMSET (&LldpPortTable, 0, sizeof (tLldpLocPortTable));

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    LldpPortTable.i4IfIndex = i4LldpV2LocPortIfIndex;
    pLldpPortTable = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpPortTable);
    if (pLldpPortTable == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Table Does not exsist!!\n");
        return SNMP_FAILURE;
    }
    *pi4RetValLldpV2Xdot1ConfigVidUsageDigestTxEnable =
        (INT4) pLldpPortTable->u1VidUsageDigestTlvTxEnabled;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpV2Xdot1ConfigVidUsageDigestTxEnable
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                setValLldpV2Xdot1ConfigVidUsageDigestTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpV2Xdot1ConfigVidUsageDigestTxEnable (INT4 i4LldpV2LocPortIfIndex,
                                               INT4
                                               i4SetValLldpV2Xdot1ConfigVidUsageDigestTxEnable)
{
    tLldpLocPortTable  *pLldpPortTable = NULL;
    tLldpLocPortTable   LldpPortTable;
    tLldpv2AgentToLocPort AgentToLocPort;
    tLldpv2AgentToLocPort *pAgentToLocPort = NULL;
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;

    MEMSET (&AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
    MEMSET (&LldpPortTable, 0, sizeof (tLldpLocPortTable));

    LldpPortTable.i4IfIndex = i4LldpV2LocPortIfIndex;
    pLldpPortTable = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpPortTable);
    if (pLldpPortTable == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Table Does not exsist!!\n");
        return SNMP_FAILURE;
    }

    pLldpPortTable->u1VidUsageDigestTlvTxEnabled =
        (UINT1) i4SetValLldpV2Xdot1ConfigVidUsageDigestTxEnable;
    AgentToLocPort.i4IfIndex = i4LldpV2LocPortIfIndex;
    pAgentToLocPort = (tLldpv2AgentToLocPort *)
        RBTreeGetNext (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                       &AgentToLocPort, LldpAgentToLocPortUtlRBCmpInfo);
    while ((pAgentToLocPort != NULL) &&
           (pAgentToLocPort->i4IfIndex == i4LldpV2LocPortIfIndex))
    {
        pLldpLocPortInfo =
            LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);
        if (pLldpLocPortInfo != NULL)
        {
            if (LldpTxUtlHandleLocPortInfoChg (pLldpLocPortInfo)
                != OSIX_SUCCESS)
            {
                LLDP_TRC (ALL_FAILURE_TRC, "SNMPSTD: "
                          "LldpTxUtlHandleLocPortInfoChg failed\r\n");
            }
        }

        AgentToLocPort.i4IfIndex = pAgentToLocPort->i4IfIndex;
        MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                pAgentToLocPort->Lldpv2DestMacAddress, MAC_ADDR_LEN);
        pAgentToLocPort =
            (tLldpv2AgentToLocPort *) RBTreeGetNext (gLldpGlobalInfo.
                                                     Lldpv2AgentToLocPortMapTblRBTree,
                                                     &AgentToLocPort,
                                                     LldpAgentToLocPortUtlRBCmpInfo);
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpV2Xdot1ConfigVidUsageDigestTxEnable
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                testValLldpV2Xdot1ConfigVidUsageDigestTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpV2Xdot1ConfigVidUsageDigestTxEnable (UINT4 *pu4ErrorCode,
                                                  INT4 i4LldpV2LocPortIfIndex,
                                                  INT4
                                                  i4TestValLldpV2Xdot1ConfigVidUsageDigestTxEnable)
{
    tLldpLocPortTable  *pLldpPortTable = NULL;
    tLldpLocPortTable   LldpPortTable;

    MEMSET (&LldpPortTable, 0, sizeof (tLldpLocPortTable));

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    LldpPortTable.i4IfIndex = i4LldpV2LocPortIfIndex;
    pLldpPortTable = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpPortTable);
    if (pLldpPortTable == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Table Does not exsist!!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValLldpV2Xdot1ConfigVidUsageDigestTxEnable != LLDP_TRUE) &&
        (i4TestValLldpV2Xdot1ConfigVidUsageDigestTxEnable != LLDP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        LLDP_TRC ((MGMT_TRC | ALL_FAILURE_TRC),
                  "SNMPSTD: Wrong value for i4TestValLldpV2Xdot1ConfigVidUsageDigestTxEnable!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpV2Xdot1ConfigVidUsageDigestTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpV2Xdot1ConfigVidUsageDigestTable (UINT4 *pu4ErrorCode,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot1ConfigManVidTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1ConfigManVidTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot1ConfigManVidTable (INT4
                                                      i4LldpV2LocPortIfIndex)
{
    tLldpLocPortTable  *pLldpPortTable = NULL;
    tLldpLocPortTable   LldpPortTable;

    MEMSET (&LldpPortTable, 0, sizeof (tLldpLocPortTable));

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if ((i4LldpV2LocPortIfIndex < LLDP_MIN_PORTS) ||
        ((UINT4) i4LldpV2LocPortIfIndex > LLDP_MAX_PORTS))
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Index Validation Failed!!\n");
        return SNMP_FAILURE;
    }

    LldpPortTable.i4IfIndex = i4LldpV2LocPortIfIndex;
    pLldpPortTable = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpPortTable);
    if ((pLldpPortTable != NULL)
        && (pLldpPortTable->u1ManVidTlvTxEnabled == LLDP_TRUE))
    {
        return SNMP_SUCCESS;
    }
    LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
              " SNMPSTD: Table Does not exsist!!\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1ConfigManVidTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1ConfigManVidTable (INT4 *pi4LldpV2LocPortIfIndex)
{
    return (nmhGetNextIndexLldpV2Xdot1ConfigManVidTable (LLDP_ZERO,
                                                         pi4LldpV2LocPortIfIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1ConfigManVidTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot1ConfigManVidTable (INT4 i4LldpV2LocPortIfIndex,
                                             INT4 *pi4NextLldpV2LocPortIfIndex)
{
    tLldpLocPortTable  *pLldpPortTable = NULL;
    tLldpLocPortTable   LldpPortTable;

    MEMSET (&LldpPortTable, 0, sizeof (tLldpLocPortTable));
    LldpPortTable.i4IfIndex = i4LldpV2LocPortIfIndex;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pLldpPortTable = (tLldpLocPortTable *)
        RBTreeGetNext (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpPortTable,
                       LldpPortInfoUtlRBCmpInfo);
    while (pLldpPortTable != NULL)
    {
        if (pLldpPortTable->u1ManVidTlvTxEnabled == LLDP_TRUE)
        {
            *pi4NextLldpV2LocPortIfIndex = pLldpPortTable->i4IfIndex;
            return SNMP_SUCCESS;

        }
        LldpPortTable.i4IfIndex = pLldpPortTable->i4IfIndex;
        pLldpPortTable = (tLldpLocPortTable *)
            RBTreeGetNext (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpPortTable,
                           LldpPortInfoUtlRBCmpInfo);
    }
    LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
              " SNMPSTD: Table Does not exsist!!\n");
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1ConfigManVidTxEnable
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2Xdot1ConfigManVidTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1ConfigManVidTxEnable (INT4 i4LldpV2LocPortIfIndex,
                                       INT4
                                       *pi4RetValLldpV2Xdot1ConfigManVidTxEnable)
{
    tLldpLocPortTable  *pLldpPortTable = NULL;
    tLldpLocPortTable   LldpPortTable;

    MEMSET (&LldpPortTable, 0, sizeof (tLldpLocPortTable));

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    LldpPortTable.i4IfIndex = i4LldpV2LocPortIfIndex;
    pLldpPortTable = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpPortTable);
    if (pLldpPortTable == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Table Does not exsist!!\n");
        return SNMP_FAILURE;
    }
    *pi4RetValLldpV2Xdot1ConfigManVidTxEnable =
        (INT4) pLldpPortTable->u1ManVidTlvTxEnabled;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpV2Xdot1ConfigManVidTxEnable
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                setValLldpV2Xdot1ConfigManVidTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpV2Xdot1ConfigManVidTxEnable (INT4 i4LldpV2LocPortIfIndex,
                                       INT4
                                       i4SetValLldpV2Xdot1ConfigManVidTxEnable)
{
    tLldpLocPortTable  *pLldpPortTable = NULL;
    tLldpLocPortTable   LldpPortTable;
    tLldpv2AgentToLocPort AgentToLocPort;
    tLldpv2AgentToLocPort *pAgentToLocPort = NULL;
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;

    MEMSET (&AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
    MEMSET (&LldpPortTable, 0, sizeof (tLldpLocPortTable));

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    LldpPortTable.i4IfIndex = i4LldpV2LocPortIfIndex;
    pLldpPortTable = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpPortTable);
    if (pLldpPortTable == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Table Does not exsist!!\n");
        return SNMP_FAILURE;
    }

    pLldpPortTable->u1ManVidTlvTxEnabled =
        (UINT1) i4SetValLldpV2Xdot1ConfigManVidTxEnable;
    AgentToLocPort.i4IfIndex = i4LldpV2LocPortIfIndex;
    pAgentToLocPort = (tLldpv2AgentToLocPort *)
        RBTreeGetNext (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                       &AgentToLocPort, LldpAgentToLocPortUtlRBCmpInfo);
    while ((pAgentToLocPort != NULL) &&
           (pAgentToLocPort->i4IfIndex == i4LldpV2LocPortIfIndex))
    {
        pLldpLocPortInfo =
            LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);
        if (pLldpLocPortInfo != NULL)
        {
            if (LldpTxUtlHandleLocPortInfoChg (pLldpLocPortInfo)
                != OSIX_SUCCESS)
            {
                LLDP_TRC (ALL_FAILURE_TRC, "SNMPSTD: "
                          "LldpTxUtlHandleLocPortInfoChg failed\r\n");
            }
        }

        AgentToLocPort.i4IfIndex = pAgentToLocPort->i4IfIndex;
        MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                pAgentToLocPort->Lldpv2DestMacAddress, MAC_ADDR_LEN);
        pAgentToLocPort =
            (tLldpv2AgentToLocPort *) RBTreeGetNext (gLldpGlobalInfo.
                                                     Lldpv2AgentToLocPortMapTblRBTree,
                                                     &AgentToLocPort,
                                                     LldpAgentToLocPortUtlRBCmpInfo);
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpV2Xdot1ConfigManVidTxEnable
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                testValLldpV2Xdot1ConfigManVidTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpV2Xdot1ConfigManVidTxEnable (UINT4 *pu4ErrorCode,
                                          INT4 i4LldpV2LocPortIfIndex,
                                          INT4
                                          i4TestValLldpV2Xdot1ConfigManVidTxEnable)
{
    tLldpLocPortTable  *pLldpPortTable = NULL;
    tLldpLocPortTable   LldpPortTable;

    MEMSET (&LldpPortTable, 0, sizeof (tLldpLocPortTable));

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    LldpPortTable.i4IfIndex = i4LldpV2LocPortIfIndex;
    pLldpPortTable = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpPortTable);
    if (pLldpPortTable == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Table Does not exsist!!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValLldpV2Xdot1ConfigManVidTxEnable != LLDP_TRUE) &&
        (i4TestValLldpV2Xdot1ConfigManVidTxEnable != LLDP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        LLDP_TRC ((MGMT_TRC | ALL_FAILURE_TRC),
                  "SNMPSTD: Wrong value for i4TestValLldpV2Xdot1ConfigManVidTxEnable!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpV2Xdot1ConfigManVidTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpV2Xdot1ConfigManVidTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot1LocTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1LocTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot1LocTable (INT4 i4LldpV2LocPortIfIndex)
{
    if (LldpUtlValidateIndexInstanceLldpXdot1LocTable (i4LldpV2LocPortIfIndex)
        == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot1LocTable Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1LocTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1LocTable (INT4 *pi4LldpV2LocPortIfIndex)
{
    if (LldpUtlGetFirstIndexLldpXdot1LocTable (pi4LldpV2LocPortIfIndex) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpXdot1LocTable Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1LocTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot1LocTable (INT4 i4LldpV2LocPortIfIndex,
                                    INT4 *pi4NextLldpV2LocPortIfIndex)
{
    if (LldpUtlGetNextIndexLldpXdot1LocTable
        (i4LldpV2LocPortIfIndex, pi4NextLldpV2LocPortIfIndex) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpXdot1LocTable Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1LocPortVlanId
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2Xdot1LocPortVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1LocPortVlanId (INT4 i4LldpV2LocPortIfIndex,
                                UINT4 *pu4RetValLldpV2Xdot1LocPortVlanId)
{
    if (LldpUtlGetLldpXdot1LocPortVlanId
        (i4LldpV2LocPortIfIndex,
         (INT4 *) pu4RetValLldpV2Xdot1LocPortVlanId) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot1LocPortVlanId Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot1LocProtoVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1LocProtoVlanTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocProtoVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot1LocProtoVlanTable (INT4
                                                      i4LldpV2LocPortIfIndex,
                                                      UINT4
                                                      u4LldpV2Xdot1LocProtoVlanId)
{
    if (LldpUtlValidateIndexInstanceLldpXdot1LocProtoVlanTable
        (i4LldpV2LocPortIfIndex, u4LldpV2Xdot1LocProtoVlanId) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot1LocProtoVlanTable"
                  " Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1LocProtoVlanTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocProtoVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1LocProtoVlanTable (INT4 *pi4LldpV2LocPortIfIndex,
                                              UINT4
                                              *pu4LldpV2Xdot1LocProtoVlanId)
{
    if (LldpUtlGetFirstIndexLldpXdot1LocProtoVlanTable (pi4LldpV2LocPortIfIndex,
                                                        (INT4 *)
                                                        pu4LldpV2Xdot1LocProtoVlanId)
        == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpXdot1LocProtoVlanTable"
                  " Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1LocProtoVlanTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
                LldpV2Xdot1LocProtoVlanId
                nextLldpV2Xdot1LocProtoVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot1LocProtoVlanTable (INT4 i4LldpV2LocPortIfIndex,
                                             INT4 *pi4NextLldpV2LocPortIfIndex,
                                             UINT4 u4LldpV2Xdot1LocProtoVlanId,
                                             UINT4
                                             *pu4NextLldpV2Xdot1LocProtoVlanId)
{
    if (LldpUtlGetNextIndexLldpXdot1LocProtoVlanTable
        (i4LldpV2LocPortIfIndex, pi4NextLldpV2LocPortIfIndex,
         (INT4) u4LldpV2Xdot1LocProtoVlanId,
         (INT4 *) pu4NextLldpV2Xdot1LocProtoVlanId) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpXdot1LocProtoVlanTable"
                  " Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1LocProtoVlanSupported
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocProtoVlanId

                The Object 
                retValLldpV2Xdot1LocProtoVlanSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1LocProtoVlanSupported (INT4 i4LldpV2LocPortIfIndex,
                                        UINT4 u4LldpV2Xdot1LocProtoVlanId,
                                        INT4
                                        *pi4RetValLldpV2Xdot1LocProtoVlanSupported)
{
    if (LldpUtlGetLldpXdot1LocProtoVlanSupported
        (i4LldpV2LocPortIfIndex, u4LldpV2Xdot1LocProtoVlanId,
         pi4RetValLldpV2Xdot1LocProtoVlanSupported) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot1LocProtoVlanSupported Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1LocProtoVlanEnabled
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocProtoVlanId

                The Object 
                retValLldpV2Xdot1LocProtoVlanEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1LocProtoVlanEnabled (INT4 i4LldpV2LocPortIfIndex,
                                      UINT4 u4LldpV2Xdot1LocProtoVlanId,
                                      INT4
                                      *pi4RetValLldpV2Xdot1LocProtoVlanEnabled)
{
    if (LldpUtlGetLldpXdot1LocProtoVlanEnabled
        (i4LldpV2LocPortIfIndex, u4LldpV2Xdot1LocProtoVlanId,
         pi4RetValLldpV2Xdot1LocProtoVlanEnabled) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot1LocProtoVlanEnabled Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot1LocVlanNameTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1LocVlanNameTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot1LocVlanNameTable (INT4
                                                     i4LldpV2LocPortIfIndex,
                                                     INT4
                                                     i4LldpV2Xdot1LocVlanId)
{
    if (LldpUtlValidateIndexInstanceLldpXdot1LocVlanNameTable
        (i4LldpV2LocPortIfIndex, i4LldpV2Xdot1LocVlanId) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot1LocVlanNameTable Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1LocVlanNameTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1LocVlanNameTable (INT4 *pi4LldpV2LocPortIfIndex,
                                             INT4 *pi4LldpV2Xdot1LocVlanId)
{
    if (LldpUtlGetFirstIndexLldpXdot1LocVlanNameTable
        (pi4LldpV2LocPortIfIndex, pi4LldpV2Xdot1LocVlanId) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpXdot1LocVlanNameTable Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1LocVlanNameTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
                LldpV2Xdot1LocVlanId
                nextLldpV2Xdot1LocVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot1LocVlanNameTable (INT4 i4LldpV2LocPortIfIndex,
                                            INT4 *pi4NextLldpV2LocPortIfIndex,
                                            INT4 i4LldpV2Xdot1LocVlanId,
                                            INT4 *pi4NextLldpV2Xdot1LocVlanId)
{
    if (LldpUtlGetNextIndexLldpXdot1LocVlanNameTable
        (i4LldpV2LocPortIfIndex, pi4NextLldpV2LocPortIfIndex,
         i4LldpV2Xdot1LocVlanId, pi4NextLldpV2Xdot1LocVlanId) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpXdot1LocVlanNameTable Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1LocVlanName
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocVlanId

                The Object 
                retValLldpV2Xdot1LocVlanName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1LocVlanName (INT4 i4LldpV2LocPortIfIndex,
                              INT4 i4LldpV2Xdot1LocVlanId,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValLldpV2Xdot1LocVlanName)
{
    if (LldpUtlGetLldpXdot1LocVlanName
        (i4LldpV2LocPortIfIndex, i4LldpV2Xdot1LocVlanId,
         pRetValLldpV2Xdot1LocVlanName) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot1LocVlanName Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot1LocProtocolTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1LocProtocolTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocProtocolIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot1LocProtocolTable (INT4
                                                     i4LldpV2LocPortIfIndex,
                                                     UINT4
                                                     u4LldpV2Xdot1LocProtocolIndex)
{
    UNUSED_PARAM (i4LldpV2LocPortIfIndex);
    UNUSED_PARAM (u4LldpV2Xdot1LocProtocolIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1LocProtocolTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocProtocolIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1LocProtocolTable (INT4 *pi4LldpV2LocPortIfIndex,
                                             UINT4
                                             *pu4LldpV2Xdot1LocProtocolIndex)
{
    UNUSED_PARAM (pi4LldpV2LocPortIfIndex);
    UNUSED_PARAM (pu4LldpV2Xdot1LocProtocolIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1LocProtocolTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
                LldpV2Xdot1LocProtocolIndex
                nextLldpV2Xdot1LocProtocolIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot1LocProtocolTable (INT4 i4LldpV2LocPortIfIndex,
                                            INT4 *pi4NextLldpV2LocPortIfIndex,
                                            UINT4 u4LldpV2Xdot1LocProtocolIndex,
                                            UINT4
                                            *pu4NextLldpV2Xdot1LocProtocolIndex)
{
    UNUSED_PARAM (i4LldpV2LocPortIfIndex);
    UNUSED_PARAM (pi4NextLldpV2LocPortIfIndex);
    UNUSED_PARAM (u4LldpV2Xdot1LocProtocolIndex);
    UNUSED_PARAM (pu4NextLldpV2Xdot1LocProtocolIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1LocProtocolId
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpV2Xdot1LocProtocolIndex

                The Object 
                retValLldpV2Xdot1LocProtocolId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1LocProtocolId (INT4 i4LldpV2LocPortIfIndex,
                                UINT4 u4LldpV2Xdot1LocProtocolIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValLldpV2Xdot1LocProtocolId)
{
    UNUSED_PARAM (i4LldpV2LocPortIfIndex);
    UNUSED_PARAM (u4LldpV2Xdot1LocProtocolIndex);
    UNUSED_PARAM (pRetValLldpV2Xdot1LocProtocolId);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot1LocVidUsageDigestTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1LocVidUsageDigestTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot1LocVidUsageDigestTable (INT4
                                                           i4LldpV2LocPortIfIndex)
{
    tLldpLocPortTable  *pLldpPortTable = NULL;
    tLldpLocPortTable   LldpPortTable;

    MEMSET (&LldpPortTable, 0, sizeof (tLldpLocPortTable));
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if ((i4LldpV2LocPortIfIndex < LLDP_MIN_PORTS) ||
        ((UINT4) i4LldpV2LocPortIfIndex > LLDP_MAX_PORTS))
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Index Validation Failed!!\n");
        return SNMP_FAILURE;
    }

    LldpPortTable.i4IfIndex = i4LldpV2LocPortIfIndex;
    pLldpPortTable = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpPortTable);
    if (pLldpPortTable == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Table Does not exsist!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1LocVidUsageDigestTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1LocVidUsageDigestTable (INT4
                                                   *pi4LldpV2LocPortIfIndex)
{
    return (nmhGetNextIndexLldpV2Xdot1LocVidUsageDigestTable (LLDP_ZERO,
                                                              pi4LldpV2LocPortIfIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1LocVidUsageDigestTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot1LocVidUsageDigestTable (INT4 i4LldpV2LocPortIfIndex,
                                                  INT4
                                                  *pi4NextLldpV2LocPortIfIndex)
{
    tLldpLocPortTable  *pLldpPortTable = NULL;
    tLldpLocPortTable   LldpPortTable;

    MEMSET (&LldpPortTable, 0, sizeof (tLldpLocPortTable));
    LldpPortTable.i4IfIndex = i4LldpV2LocPortIfIndex;

    pLldpPortTable = (tLldpLocPortTable *)
        RBTreeGetNext (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpPortTable,
                       LldpPortInfoUtlRBCmpInfo);
    if (pLldpPortTable == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Table Does not exsist!!\n");
        return SNMP_FAILURE;
    }
    *pi4NextLldpV2LocPortIfIndex = pLldpPortTable->i4IfIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1LocVidUsageDigest
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2Xdot1LocVidUsageDigest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1LocVidUsageDigest (INT4 i4LldpV2LocPortIfIndex,
                                    UINT4
                                    *pu4RetValLldpV2Xdot1LocVidUsageDigest)
{
    tLldpv2AgentToLocPort *pLldpAgentToLocPort = NULL;
    tLldpv2AgentToLocPort LldpAgentToLocPort;
    tLldpLocPortInfo   *pLldpPortInfo = NULL;
    tLldpLocPortInfo    LldpPortInfo;

    MEMSET (&LldpAgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
    MEMSET (&LldpPortInfo, 0, sizeof (tLldpLocPortInfo));

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }

    LldpAgentToLocPort.i4IfIndex = i4LldpV2LocPortIfIndex;
    MEMCPY (LldpAgentToLocPort.Lldpv2DestMacAddress, gau1LldpMcastAddr,
            MAC_ADDR_LEN);

    pLldpAgentToLocPort = (tLldpv2AgentToLocPort *)
        RBTreeGet (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                   &LldpAgentToLocPort);

    if (pLldpAgentToLocPort == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Table Does not exsist!!\n");
        return SNMP_FAILURE;
    }

    LldpPortInfo.u4LocPortNum = pLldpAgentToLocPort->u4LldpLocPort;

    pLldpPortInfo = (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                                    LldpLocPortAgentInfoRBTree,
                                                    &LldpPortInfo);

    if (pLldpPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Table Does not exsist!!\n");
        return SNMP_FAILURE;
    }

    *pu4RetValLldpV2Xdot1LocVidUsageDigest = pLldpPortInfo->u4LocVidUsageDigest;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot1LocManVidTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1LocManVidTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot1LocManVidTable (INT4 i4LldpV2LocPortIfIndex)
{
    tLldpLocPortTable  *pLldpPortTable = NULL;
    tLldpLocPortTable   LldpPortTable;

    MEMSET (&LldpPortTable, 0, sizeof (tLldpLocPortTable));

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if ((i4LldpV2LocPortIfIndex < LLDP_MIN_PORTS) ||
        ((UINT4) i4LldpV2LocPortIfIndex > LLDP_MAX_PORTS))
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Index Validation Failed!!\n");
        return SNMP_FAILURE;
    }

    LldpPortTable.i4IfIndex = i4LldpV2LocPortIfIndex;
    pLldpPortTable = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpPortTable);
    if (pLldpPortTable == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Table Does not exsist!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1LocManVidTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1LocManVidTable (INT4 *pi4LldpV2LocPortIfIndex)
{
    return (nmhGetNextIndexLldpV2Xdot1LocManVidTable (LLDP_ZERO,
                                                      pi4LldpV2LocPortIfIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1LocManVidTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot1LocManVidTable (INT4 i4LldpV2LocPortIfIndex,
                                          INT4 *pi4NextLldpV2LocPortIfIndex)
{
    tLldpLocPortTable  *pLldpPortTable = NULL;
    tLldpLocPortTable   LldpPortTable;

    MEMSET (&LldpPortTable, 0, sizeof (tLldpLocPortTable));
    LldpPortTable.i4IfIndex = i4LldpV2LocPortIfIndex;
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }

    pLldpPortTable = (tLldpLocPortTable *)
        RBTreeGetNext (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpPortTable,
                       LldpPortInfoUtlRBCmpInfo);
    if (pLldpPortTable == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Table Does not exsist!!\n");
        return SNMP_FAILURE;
    }
    *pi4NextLldpV2LocPortIfIndex = pLldpPortTable->i4IfIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1LocManVid
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2Xdot1LocManVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1LocManVid (INT4 i4LldpV2LocPortIfIndex,
                            UINT4 *pu4RetValLldpV2Xdot1LocManVid)
{
    tLldpLocPortTable  *pLldpPortTable = NULL;
    tLldpLocPortTable   LldpPortTable;

    MEMSET (&LldpPortTable, 0, sizeof (tLldpLocPortTable));
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }

    LldpPortTable.i4IfIndex = i4LldpV2LocPortIfIndex;
    pLldpPortTable = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpPortTable);
    if (pLldpPortTable == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Table Does not exsist!!\n");
        return SNMP_FAILURE;
    }
    *pu4RetValLldpV2Xdot1LocManVid = (INT4) pLldpPortTable->u4LocMgmtVid;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot1LocLinkAggTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1LocLinkAggTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot1LocLinkAggTable (INT4 i4LldpV2LocPortIfIndex)
{
    if (LldpUtlValidateIndexInstanceLldpXdot3LocLinkAggTable
        (i4LldpV2LocPortIfIndex) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot3LocLinkAggTable Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1LocLinkAggTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1LocLinkAggTable (INT4 *pi4LldpV2LocPortIfIndex)
{
    if (LldpUtlGetFirstIndexLldpXdot3LocLinkAggTable (pi4LldpV2LocPortIfIndex)
        == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpXdot3LocLinkAggTable Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1LocLinkAggTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot1LocLinkAggTable (INT4 i4LldpV2LocPortIfIndex,
                                           INT4 *pi4NextLldpV2LocPortIfIndex)
{
    if (LldpUtlGetNextIndexLldpXdot3LocLinkAggTable
        (i4LldpV2LocPortIfIndex, pi4NextLldpV2LocPortIfIndex) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpXdot3LocLinkAggTable Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1LocLinkAggStatus
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2Xdot1LocLinkAggStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1LocLinkAggStatus (INT4 i4LldpV2LocPortIfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValLldpV2Xdot1LocLinkAggStatus)
{
    if (LldpUtlGetLldpXdot3LocLinkAggStatus
        (i4LldpV2LocPortIfIndex,
         pRetValLldpV2Xdot1LocLinkAggStatus) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot3LocLinkAggStatus Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1LocLinkAggPortId
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2Xdot1LocLinkAggPortId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1LocLinkAggPortId (INT4 i4LldpV2LocPortIfIndex,
                                   UINT4 *pu4RetValLldpV2Xdot1LocLinkAggPortId)
{
    if (LldpUtlGetLldpXdot3LocLinkAggPortId
        (i4LldpV2LocPortIfIndex,
         (INT4 *) pu4RetValLldpV2Xdot1LocLinkAggPortId) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot3LocLinkAggPortId Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot1RemTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1RemTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot1RemTable (UINT4 u4LldpV2RemTimeMark,
                                             INT4 i4LldpV2RemLocalIfIndex,
                                             UINT4
                                             u4LldpV2RemLocalDestMACAddress,
                                             INT4 i4LldpV2RemIndex)
{
    if (LldpUtlValidateIndexInstanceLldpXdot1RemTable
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, i4LldpV2RemIndex,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot1RemTable Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1RemTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1RemTable (UINT4 *pu4LldpV2RemTimeMark,
                                     INT4 *pi4LldpV2RemLocalIfIndex,
                                     UINT4 *pu4LldpV2RemLocalDestMACAddress,
                                     INT4 *pi4LldpV2RemIndex)
{
    return (nmhGetNextIndexLldpV2Xdot1RemTable (LLDP_ZERO,
                                                pu4LldpV2RemTimeMark,
                                                LLDP_ZERO,
                                                pi4LldpV2RemLocalIfIndex,
                                                LLDP_ZERO,
                                                pu4LldpV2RemLocalDestMACAddress,
                                                LLDP_ZERO, pi4LldpV2RemIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1RemTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot1RemTable (UINT4 u4LldpV2RemTimeMark,
                                    UINT4 *pu4NextLldpV2RemTimeMark,
                                    INT4 i4LldpV2RemLocalIfIndex,
                                    INT4 *pi4NextLldpV2RemLocalIfIndex,
                                    UINT4 u4LldpV2RemLocalDestMACAddress,
                                    UINT4 *pu4NextLldpV2RemLocalDestMACAddress,
                                    INT4 i4LldpV2RemIndex,
                                    INT4 *pi4NextLldpV2RemIndex)
{
    tLldpRemoteNode    *pTmpRemoteNode = NULL;
    tLldpRemoteNode    *pLldpRemoteNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }

    /* Allocate Memory for Remote Node */
    if ((pTmpRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_MANDATORY_TLV_TRC,
                  "SNMPSTD: Failed to Allocate Memory " "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return SNMP_FAILURE;
    }
    MEMSET (pTmpRemoteNode, 0, sizeof (tLldpRemoteNode));
    pTmpRemoteNode->u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    pTmpRemoteNode->i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    pTmpRemoteNode->u4DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    pTmpRemoteNode->i4RemIndex = i4LldpV2RemIndex;

    pLldpRemoteNode =
        (tLldpRemoteNode *) RBTreeGetNext (gLldpGlobalInfo.RemSysDataRBTree,
                                           (tRBElem *) pTmpRemoteNode, NULL);

    if (pLldpRemoteNode != NULL)
    {
        *pu4NextLldpV2RemTimeMark = pLldpRemoteNode->u4RemLastUpdateTime;
        *pi4NextLldpV2RemLocalIfIndex = pLldpRemoteNode->i4RemLocalPortNum;
        *pi4NextLldpV2RemIndex = pLldpRemoteNode->i4RemIndex;
        *pu4NextLldpV2RemLocalDestMACAddress =
            pLldpRemoteNode->u4DestAddrTblIndex;
        i1RetVal = SNMP_SUCCESS;
    }
    /* Release the memory allocated for Remote Node */
    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                        (UINT1 *) pTmpRemoteNode);
    return i1RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1RemPortVlanId
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2Xdot1RemPortVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1RemPortVlanId (UINT4 u4LldpV2RemTimeMark,
                                INT4 i4LldpV2RemLocalIfIndex,
                                UINT4 u4LldpV2RemLocalDestMACAddress,
                                INT4 i4LldpV2RemIndex,
                                UINT4 *pu4RetValLldpV2Xdot1RemPortVlanId)
{
    if (LldpUtlGetLldpXdot1RemPortVlanId
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, i4LldpV2RemIndex,
         (INT4 *) pu4RetValLldpV2Xdot1RemPortVlanId,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot1RemPortVlanId Failed\r\n");
        *pu4RetValLldpV2Xdot1RemPortVlanId = 0;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot1RemProtoVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1RemProtoVlanTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpV2Xdot1RemProtoVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot1RemProtoVlanTable (UINT4 u4LldpV2RemTimeMark,
                                                      INT4
                                                      i4LldpV2RemLocalIfIndex,
                                                      UINT4
                                                      u4LldpV2RemLocalDestMACAddress,
                                                      INT4 i4LldpV2RemIndex,
                                                      UINT4
                                                      u4LldpV2Xdot1RemProtoVlanId)
{
    if (LldpUtlValidateIndexInstanceLldpXdot1RemProtoVlanTable
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, i4LldpV2RemIndex,
         u4LldpV2Xdot1RemProtoVlanId,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot1RemPortVlanId Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1RemProtoVlanTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpV2Xdot1RemProtoVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1RemProtoVlanTable (UINT4 *pu4LldpV2RemTimeMark,
                                              INT4 *pi4LldpV2RemLocalIfIndex,
                                              UINT4
                                              *pu4LldpV2RemLocalDestMACAddress,
                                              INT4 *pi4LldpV2RemIndex,
                                              UINT4
                                              *pu4LldpV2Xdot1RemProtoVlanId)
{
    return (nmhGetNextIndexLldpV2Xdot1RemProtoVlanTable (LLDP_ZERO,
                                                         pu4LldpV2RemTimeMark,
                                                         LLDP_ZERO,
                                                         pi4LldpV2RemLocalIfIndex,
                                                         LLDP_ZERO,
                                                         pu4LldpV2RemLocalDestMACAddress,
                                                         LLDP_ZERO,
                                                         pi4LldpV2RemIndex,
                                                         LLDP_ZERO,
                                                         pu4LldpV2Xdot1RemProtoVlanId));

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1RemProtoVlanTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
                LldpV2Xdot1RemProtoVlanId
                nextLldpV2Xdot1RemProtoVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot1RemProtoVlanTable (UINT4 u4LldpV2RemTimeMark,
                                             UINT4 *pu4NextLldpV2RemTimeMark,
                                             INT4 i4LldpV2RemLocalIfIndex,
                                             INT4 *pi4NextLldpV2RemLocalIfIndex,
                                             UINT4
                                             u4LldpV2RemLocalDestMACAddress,
                                             UINT4
                                             *pu4NextLldpV2RemLocalDestMACAddress,
                                             INT4 i4LldpV2RemIndex,
                                             INT4 *pi4NextLldpV2RemIndex,
                                             UINT4 u4LldpV2Xdot1RemProtoVlanId,
                                             UINT4
                                             *pu4NextLldpV2Xdot1RemProtoVlanId)
{
    tLldpxdot1RemProtoVlanInfo LldpRemoteNode;
    tLldpxdot1RemProtoVlanInfo *pLldpRemoteNode = NULL;
    MEMSET (&LldpRemoteNode, 0, sizeof (tLldpxdot1RemProtoVlanInfo));

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    LldpRemoteNode.u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    LldpRemoteNode.i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    LldpRemoteNode.u4DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    LldpRemoteNode.i4RemIndex = i4LldpV2RemIndex;
    LldpRemoteNode.u2ProtoVlanId = (UINT2) u4LldpV2Xdot1RemProtoVlanId;

    pLldpRemoteNode =
        (tLldpxdot1RemProtoVlanInfo *) RBTreeGetNext (gLldpGlobalInfo.
                                                      RemProtoVlanRBTree,
                                                      (tRBElem *) &
                                                      LldpRemoteNode, NULL);

    if (pLldpRemoteNode == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4NextLldpV2RemTimeMark = pLldpRemoteNode->u4RemLastUpdateTime;
    *pi4NextLldpV2RemLocalIfIndex = pLldpRemoteNode->i4RemLocalPortNum;
    *pi4NextLldpV2RemIndex = pLldpRemoteNode->i4RemIndex;
    *pu4NextLldpV2RemLocalDestMACAddress = pLldpRemoteNode->u4DestAddrTblIndex;
    *pu4NextLldpV2Xdot1RemProtoVlanId = (UINT4) pLldpRemoteNode->u2ProtoVlanId;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1RemProtoVlanSupported
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpV2Xdot1RemProtoVlanId

                The Object 
                retValLldpV2Xdot1RemProtoVlanSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1RemProtoVlanSupported (UINT4 u4LldpV2RemTimeMark,
                                        INT4 i4LldpV2RemLocalIfIndex,
                                        UINT4 u4LldpV2RemLocalDestMACAddress,
                                        INT4 i4LldpV2RemIndex,
                                        UINT4 u4LldpV2Xdot1RemProtoVlanId,
                                        INT4
                                        *pi4RetValLldpV2Xdot1RemProtoVlanSupported)
{
    if (LldpUtlGetLldpXdot1RemProtoVlanSupported
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, i4LldpV2RemIndex,
         u4LldpV2Xdot1RemProtoVlanId, pi4RetValLldpV2Xdot1RemProtoVlanSupported,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot1RemProtoVlanSupported  Failed\r\n");
        /* Remote Proto VLAN is not supported */
        *pi4RetValLldpV2Xdot1RemProtoVlanSupported = OSIX_FALSE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1RemProtoVlanEnabled
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpV2Xdot1RemProtoVlanId

                The Object 
                retValLldpV2Xdot1RemProtoVlanEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1RemProtoVlanEnabled (UINT4 u4LldpV2RemTimeMark,
                                      INT4 i4LldpV2RemLocalIfIndex,
                                      UINT4 u4LldpV2RemLocalDestMACAddress,
                                      INT4 i4LldpV2RemIndex,
                                      UINT4 u4LldpV2Xdot1RemProtoVlanId,
                                      INT4
                                      *pi4RetValLldpV2Xdot1RemProtoVlanEnabled)
{
    if (LldpUtlGetLldpXdot1RemProtoVlanEnabled
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, i4LldpV2RemIndex,
         u4LldpV2Xdot1RemProtoVlanId, pi4RetValLldpV2Xdot1RemProtoVlanEnabled,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot1RemProtoVlanSupported  Failed\r\n");
        /* Remote Proto VLAN is disabled */
        *pi4RetValLldpV2Xdot1RemProtoVlanEnabled = OSIX_FALSE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot1RemVlanNameTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1RemVlanNameTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpV2Xdot1RemVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot1RemVlanNameTable (UINT4 u4LldpV2RemTimeMark,
                                                     INT4
                                                     i4LldpV2RemLocalIfIndex,
                                                     UINT4
                                                     u4LldpV2RemLocalDestMACAddress,
                                                     INT4 i4LldpV2RemIndex,
                                                     INT4
                                                     i4LldpV2Xdot1RemVlanId)
{
    if (LldpUtlValidateIndexInstanceLldpXdot1RemVlanNameTable
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, i4LldpV2RemIndex,
         i4LldpV2Xdot1RemVlanId,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot1RemVlanNameTable Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1RemVlanNameTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpV2Xdot1RemVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1RemVlanNameTable (UINT4 *pu4LldpV2RemTimeMark,
                                             INT4 *pi4LldpV2RemLocalIfIndex,
                                             UINT4
                                             *pu4LldpV2RemLocalDestMACAddress,
                                             INT4 *pi4LldpV2RemIndex,
                                             INT4 *pi4LldpV2Xdot1RemVlanId)
{
    return (nmhGetNextIndexLldpV2Xdot1RemVlanNameTable (LLDP_ZERO,
                                                        pu4LldpV2RemTimeMark,
                                                        LLDP_ZERO,
                                                        pi4LldpV2RemLocalIfIndex,
                                                        LLDP_ZERO,
                                                        pu4LldpV2RemLocalDestMACAddress,
                                                        LLDP_ZERO,
                                                        pi4LldpV2RemIndex,
                                                        LLDP_ZERO,
                                                        pi4LldpV2Xdot1RemVlanId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1RemVlanNameTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
                LldpV2Xdot1RemVlanId
                nextLldpV2Xdot1RemVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot1RemVlanNameTable (UINT4 u4LldpV2RemTimeMark,
                                            UINT4 *pu4NextLldpV2RemTimeMark,
                                            INT4 i4LldpV2RemLocalIfIndex,
                                            INT4 *pi4NextLldpV2RemLocalIfIndex,
                                            UINT4
                                            u4LldpV2RemLocalDestMACAddress,
                                            UINT4
                                            *pu4NextLldpV2RemLocalDestMACAddress,
                                            INT4 i4LldpV2RemIndex,
                                            INT4 *pi4NextLldpV2RemIndex,
                                            INT4 i4LldpV2Xdot1RemVlanId,
                                            INT4 *pi4NextLldpV2Xdot1RemVlanId)
{
    tLldpxdot1RemVlanNameInfo LldpRemoteNode;
    tLldpxdot1RemVlanNameInfo *pLldpRemoteNode = NULL;
    MEMSET (&LldpRemoteNode, 0, sizeof (tLldpxdot1RemVlanNameInfo));

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    LldpRemoteNode.u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    LldpRemoteNode.i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    LldpRemoteNode.u4DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    LldpRemoteNode.i4RemIndex = i4LldpV2RemIndex;
    LldpRemoteNode.u2VlanId = (UINT2) i4LldpV2Xdot1RemVlanId;

    pLldpRemoteNode =
        (tLldpxdot1RemVlanNameInfo *) RBTreeGetNext (gLldpGlobalInfo.
                                                     RemVlanNameInfoRBTree,
                                                     (tRBElem *) &
                                                     LldpRemoteNode, NULL);

    if (pLldpRemoteNode == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4NextLldpV2RemTimeMark = pLldpRemoteNode->u4RemLastUpdateTime;
    *pi4NextLldpV2RemLocalIfIndex = pLldpRemoteNode->i4RemLocalPortNum;
    *pi4NextLldpV2RemIndex = pLldpRemoteNode->i4RemIndex;
    *pi4NextLldpV2Xdot1RemVlanId = (UINT4) pLldpRemoteNode->u2VlanId;
    *pu4NextLldpV2RemLocalDestMACAddress = pLldpRemoteNode->u4DestAddrTblIndex;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1RemVlanName
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpV2Xdot1RemVlanId

                The Object 
                retValLldpV2Xdot1RemVlanName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1RemVlanName (UINT4 u4LldpV2RemTimeMark,
                              INT4 i4LldpV2RemLocalIfIndex,
                              UINT4 u4LldpV2RemLocalDestMACAddress,
                              INT4 i4LldpV2RemIndex,
                              INT4 i4LldpV2Xdot1RemVlanId,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValLldpV2Xdot1RemVlanName)
{
    if (LldpUtlGetLldpXdot1RemVlanName
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, i4LldpV2RemIndex,
         i4LldpV2Xdot1RemVlanId, pRetValLldpV2Xdot1RemVlanName,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD:nmhGetLldpV2Xdot1RemVlanName Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot1RemProtocolTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1RemProtocolTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpV2Xdot1RemProtocolIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot1RemProtocolTable (UINT4 u4LldpV2RemTimeMark,
                                                     INT4
                                                     i4LldpV2RemLocalIfIndex,
                                                     UINT4
                                                     u4LldpV2RemLocalDestMACAddress,
                                                     INT4 i4LldpV2RemIndex,
                                                     UINT4
                                                     u4LldpV2Xdot1RemProtocolIndex)
{
    if (LldpUtlValidateIndexInstanceLldpXdot1RemProtocolTable
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, i4LldpV2RemIndex,
         u4LldpV2Xdot1RemProtocolIndex,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlValidateIndexInstanceLldpXdot1RemProtocolTable Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1RemProtocolTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpV2Xdot1RemProtocolIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1RemProtocolTable (UINT4 *pu4LldpV2RemTimeMark,
                                             INT4 *pi4LldpV2RemLocalIfIndex,
                                             UINT4
                                             *pu4LldpV2RemLocalDestMACAddress,
                                             INT4 *pi4LldpV2RemIndex,
                                             UINT4
                                             *pu4LldpV2Xdot1RemProtocolIndex)
{
    return (nmhGetNextIndexLldpV2Xdot1RemProtocolTable (LLDP_ZERO,
                                                        pu4LldpV2RemTimeMark,
                                                        LLDP_ZERO,
                                                        pi4LldpV2RemLocalIfIndex,
                                                        LLDP_ZERO,
                                                        pu4LldpV2RemLocalDestMACAddress,
                                                        LLDP_ZERO,
                                                        pi4LldpV2RemIndex,
                                                        LLDP_ZERO,
                                                        pu4LldpV2Xdot1RemProtocolIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1RemProtocolTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
                LldpV2Xdot1RemProtocolIndex
                nextLldpV2Xdot1RemProtocolIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot1RemProtocolTable (UINT4 u4LldpV2RemTimeMark,
                                            UINT4 *pu4NextLldpV2RemTimeMark,
                                            INT4 i4LldpV2RemLocalIfIndex,
                                            INT4 *pi4NextLldpV2RemLocalIfIndex,
                                            UINT4
                                            u4LldpV2RemLocalDestMACAddress,
                                            UINT4
                                            *pu4NextLldpV2RemLocalDestMACAddress,
                                            INT4 i4LldpV2RemIndex,
                                            INT4 *pi4NextLldpV2RemIndex,
                                            UINT4 u4LldpV2Xdot1RemProtocolIndex,
                                            UINT4
                                            *pu4NextLldpV2Xdot1RemProtocolIndex)
{
    tLldpxdot1RemProtoIdInfo LldpRemoteNode;
    tLldpxdot1RemProtoIdInfo *pLldpRemoteNode = NULL;
    MEMSET (&LldpRemoteNode, 0, sizeof (tLldpxdot1RemProtoIdInfo));

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    LldpRemoteNode.u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    LldpRemoteNode.i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    LldpRemoteNode.u4DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    LldpRemoteNode.i4RemIndex = i4LldpV2RemIndex;
    LldpRemoteNode.u4ProtocolIndex = u4LldpV2Xdot1RemProtocolIndex;

    pLldpRemoteNode =
        (tLldpxdot1RemProtoIdInfo *) RBTreeGetNext (gLldpGlobalInfo.
                                                    RemProtoIdRBTree,
                                                    (tRBElem *) &
                                                    LldpRemoteNode, NULL);

    if (pLldpRemoteNode == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4NextLldpV2RemTimeMark = pLldpRemoteNode->u4RemLastUpdateTime;
    *pi4NextLldpV2RemLocalIfIndex = pLldpRemoteNode->i4RemLocalPortNum;
    *pi4NextLldpV2RemIndex = pLldpRemoteNode->i4RemIndex;
    *pu4NextLldpV2RemLocalDestMACAddress = pLldpRemoteNode->u4DestAddrTblIndex;
    *pu4NextLldpV2Xdot1RemProtocolIndex = pLldpRemoteNode->u4ProtocolIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1RemProtocolId
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpV2Xdot1RemProtocolIndex

                The Object 
                retValLldpV2Xdot1RemProtocolId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1RemProtocolId (UINT4 u4LldpV2RemTimeMark,
                                INT4 i4LldpV2RemLocalIfIndex,
                                UINT4 u4LldpV2RemLocalDestMACAddress,
                                INT4 i4LldpV2RemIndex,
                                UINT4 u4LldpV2Xdot1RemProtocolIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValLldpV2Xdot1RemProtocolId)
{
    if (LldpUtlGetLldpXdot1RemProtocolId
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, i4LldpV2RemIndex,
         u4LldpV2Xdot1RemProtocolIndex, pRetValLldpV2Xdot1RemProtocolId,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlGetLldpXdot1RemProtocolId Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot1RemVidUsageDigestTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1RemVidUsageDigestTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot1RemVidUsageDigestTable (UINT4
                                                           u4LldpV2RemTimeMark,
                                                           INT4
                                                           i4LldpV2RemLocalIfIndex,
                                                           UINT4
                                                           u4LldpV2RemLocalDestMACAddress)
{
    tLldpRemoteNode    *pTempRemNode = NULL;
    tLldpRemoteNode    *pRemNode = NULL;
    /* DEFAULT INDEX is 1 LLDP_MIN_REM_INDEX */
    INT4                i4LldpV2RemIndex = 1;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: nmhValidateIndexInstanceLldpV2Xdot1RemVidUsageDigestTable:"
                  " LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    /* Allocate Memory for Temp Remote Node */
    if ((pTempRemNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "nmhValidateIndexInstanceLldpV2Xdot1RemVidUsageDigestTable:"
                  " Failed to Allocate Memory " "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return SNMP_FAILURE;
    }
    MEMSET (pTempRemNode, 0, sizeof (tLldpRemoteNode));

    if (LLDP_IS_REM_INDICES_VALID
        ((UINT4) i4LldpV2RemLocalIfIndex, u4LldpV2RemLocalDestMACAddress,
         i4LldpV2RemIndex) == OSIX_TRUE)
    {
        /* Get the pointer to the Remote Node with the given indices */
        pTempRemNode->u4RemLastUpdateTime = u4LldpV2RemTimeMark;
        pTempRemNode->i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
        pTempRemNode->u4DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
        /* RemIndex is not used in this table . Hence skipping it
         * for validation. This is achieved through LldpRxUtlRBCmpV2SysData */
        pTempRemNode->i4RemIndex = 0;

        pRemNode =
            (tLldpRemoteNode *) RBTreeGetNext (gLldpGlobalInfo.RemSysDataRBTree,
                                               (tRBElem *) pTempRemNode,
                                               LldpRxUtlRBCmpV2ValidateSysData);
    }
    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId, (UINT1 *) pTempRemNode);

    if (pRemNode == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1RemVidUsageDigestTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1RemVidUsageDigestTable (UINT4 *pu4LldpV2RemTimeMark,
                                                   INT4
                                                   *pi4LldpV2RemLocalIfIndex,
                                                   UINT4
                                                   *pu4LldpV2RemLocalDestMACAddress)
{
    return (nmhGetNextIndexLldpV2Xdot1RemVidUsageDigestTable (LLDP_ZERO,
                                                              pu4LldpV2RemTimeMark,
                                                              LLDP_ZERO,
                                                              pi4LldpV2RemLocalIfIndex,
                                                              LLDP_ZERO,
                                                              pu4LldpV2RemLocalDestMACAddress));
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1RemVidUsageDigestTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot1RemVidUsageDigestTable (UINT4 u4LldpV2RemTimeMark,
                                                  UINT4
                                                  *pu4NextLldpV2RemTimeMark,
                                                  INT4 i4LldpV2RemLocalIfIndex,
                                                  INT4
                                                  *pi4NextLldpV2RemLocalIfIndex,
                                                  UINT4
                                                  u4LldpV2RemLocalDestMACAddress,
                                                  UINT4
                                                  *pu4NextLldpV2RemLocalDestMACAddress)
{
    tLldpRemoteNode     LldpRemoteNode;
    tLldpRemoteNode    *pLldpRemoteNode = NULL;
    MEMSET (&LldpRemoteNode, 0, sizeof (tLldpRemoteNode));
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }

    LldpRemoteNode.u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    LldpRemoteNode.i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    LldpRemoteNode.u4DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;

    pLldpRemoteNode =
        (tLldpRemoteNode *) RBTreeGetNext (gLldpGlobalInfo.RemSysDataRBTree,
                                           (tRBElem *) & LldpRemoteNode,
                                           LldpRxUtlRBCmpV2SysData);

    if (pLldpRemoteNode == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4NextLldpV2RemTimeMark = pLldpRemoteNode->u4RemLastUpdateTime;
    *pi4NextLldpV2RemLocalIfIndex = pLldpRemoteNode->i4RemLocalPortNum;
    *pu4NextLldpV2RemLocalDestMACAddress = pLldpRemoteNode->u4DestAddrTblIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1RemVidUsageDigest
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress

                The Object 
                retValLldpV2Xdot1RemVidUsageDigest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1RemVidUsageDigest (UINT4 u4LldpV2RemTimeMark,
                                    INT4 i4LldpV2RemLocalIfIndex,
                                    UINT4 u4LldpV2RemLocalDestMACAddress,
                                    UINT4
                                    *pu4RetValLldpV2Xdot1RemVidUsageDigest)
{
    tLldpRemoteNode    *pTmpRemoteNode = NULL;
    tLldpRemoteNode    *pLldpRemoteNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: nmhGetLldpV2Xdot1RemVidUsageDigest: "
                  " LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    /* Allocate Memory for Remote Node */
    if ((pTmpRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_MANDATORY_TLV_TRC,
                  "SNMPSTD: nmhGetLldpV2Xdot1RemVidUsageDigest: "
                  "Failed to Allocate Memory " "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return SNMP_FAILURE;
    }
    MEMSET (pTmpRemoteNode, 0, sizeof (tLldpRemoteNode));
    pTmpRemoteNode->u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    pTmpRemoteNode->i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    pTmpRemoteNode->u4DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    pTmpRemoteNode->i4RemIndex = 0;

    pLldpRemoteNode =
        (tLldpRemoteNode *) RBTreeGetNext (gLldpGlobalInfo.RemSysDataRBTree,
                                           pTmpRemoteNode, NULL);

    if (pLldpRemoteNode != NULL)
    {
        *pu4RetValLldpV2Xdot1RemVidUsageDigest =
            pLldpRemoteNode->u4RemVidUsageDigest;
        i1RetVal = SNMP_SUCCESS;
    }
    /* Release the memory allocated for Remote Node */
    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                        (UINT1 *) pTmpRemoteNode);
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot1RemManVidTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1RemManVidTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot1RemManVidTable (UINT4 u4LldpV2RemTimeMark,
                                                   INT4 i4LldpV2RemLocalIfIndex,
                                                   UINT4
                                                   u4LldpV2RemLocalDestMACAddress)
{
    INT4                i4LldpV2RemIndex = 1;
    tLldpRemoteNode    *pTempRemNode = NULL;
    tLldpRemoteNode    *pRemNode = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }

    /* Allocate Memory for Temp Remote Node */
    if ((pTempRemNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpRxUtlGetRemoteNode: Failed to Allocate Memory "
                  "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return SNMP_FAILURE;
    }

    MEMSET (pTempRemNode, 0, sizeof (tLldpRemoteNode));

    if (LLDP_IS_REM_INDICES_VALID
        ((UINT4) i4LldpV2RemLocalIfIndex, u4LldpV2RemLocalDestMACAddress,
         i4LldpV2RemIndex) == OSIX_TRUE)
    {
        /* Get the pointer to the Remote Node with the given indices */
        pTempRemNode->u4RemLastUpdateTime = u4LldpV2RemTimeMark;
        pTempRemNode->i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
        pTempRemNode->u4DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
        /* RemIndex is not used in this table . Hence skipping it
         * for validation. This is achieved through LldpRxUtlRBCmpV2SysData */
        pTempRemNode->i4RemIndex = 0;

        pRemNode =
            (tLldpRemoteNode *) RBTreeGetNext (gLldpGlobalInfo.RemSysDataRBTree,
                                               (tRBElem *) pTempRemNode,
                                               LldpRxUtlRBCmpV2ValidateSysData);
    }
    /* Release the memory allocated for Temp Remote Node */
    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId, (UINT1 *) pTempRemNode);

    if (pRemNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot1RemTable Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1RemManVidTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1RemManVidTable (UINT4 *pu4LldpV2RemTimeMark,
                                           INT4 *pi4LldpV2RemLocalIfIndex,
                                           UINT4
                                           *pu4LldpV2RemLocalDestMACAddress)
{
    return (nmhGetNextIndexLldpV2Xdot1RemManVidTable (LLDP_ZERO,
                                                      pu4LldpV2RemTimeMark,
                                                      LLDP_ZERO,
                                                      pi4LldpV2RemLocalIfIndex,
                                                      LLDP_ZERO,
                                                      pu4LldpV2RemLocalDestMACAddress));
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1RemManVidTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot1RemManVidTable (UINT4 u4LldpV2RemTimeMark,
                                          UINT4 *pu4NextLldpV2RemTimeMark,
                                          INT4 i4LldpV2RemLocalIfIndex,
                                          INT4 *pi4NextLldpV2RemLocalIfIndex,
                                          UINT4 u4LldpV2RemLocalDestMACAddress,
                                          UINT4
                                          *pu4NextLldpV2RemLocalDestMACAddress)
{
    tLldpRemoteNode    *pTmpRemoteNode = NULL;
    tLldpRemoteNode    *pLldpRemoteNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }

    /* Allocate Memory for Remote Node */
    if ((pTmpRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_MANDATORY_TLV_TRC,
                  "SNMPSTD: Failed to Allocate Memory " "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return SNMP_FAILURE;
    }
    MEMSET (pTmpRemoteNode, 0, sizeof (tLldpRemoteNode));
    pTmpRemoteNode->u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    pTmpRemoteNode->i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    pTmpRemoteNode->u4DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;

    pLldpRemoteNode =
        (tLldpRemoteNode *) RBTreeGetNext (gLldpGlobalInfo.RemSysDataRBTree,
                                           (tRBElem *) pTmpRemoteNode,
                                           LldpRxUtlRBCmpV2SysData);

    if (pLldpRemoteNode != NULL)
    {
        *pu4NextLldpV2RemTimeMark = pLldpRemoteNode->u4RemLastUpdateTime;
        *pi4NextLldpV2RemLocalIfIndex = pLldpRemoteNode->i4RemLocalPortNum;
        *pu4NextLldpV2RemLocalDestMACAddress =
            pLldpRemoteNode->u4DestAddrTblIndex;
        i1RetVal = SNMP_SUCCESS;

    }

    /* Release the memory allocated for Remote Node */
    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                        (UINT1 *) pTmpRemoteNode);
    return i1RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1RemManVid
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress

                The Object 
                retValLldpV2Xdot1RemManVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1RemManVid (UINT4 u4LldpV2RemTimeMark,
                            INT4 i4LldpV2RemLocalIfIndex,
                            UINT4 u4LldpV2RemLocalDestMACAddress,
                            UINT4 *pu4RetValLldpV2Xdot1RemManVid)
{
    tLldpRemoteNode    *pTmpRemoteNode = NULL;
    tLldpRemoteNode    *pLldpRemoteNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    /* Allocate Memory for Remote Node */
    if ((pTmpRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_MANDATORY_TLV_TRC,
                  "SNMPSTD: Failed to Allocate Memory " "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return SNMP_FAILURE;
    }
    MEMSET (pTmpRemoteNode, 0, sizeof (tLldpRemoteNode));
    pTmpRemoteNode->u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    pTmpRemoteNode->i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    pTmpRemoteNode->u4DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    pTmpRemoteNode->i4RemIndex = 0;

    pLldpRemoteNode =
        (tLldpRemoteNode *) RBTreeGetNext (gLldpGlobalInfo.RemSysDataRBTree,
                                           pTmpRemoteNode, NULL);

    if (pLldpRemoteNode != NULL)
    {
        *pu4RetValLldpV2Xdot1RemManVid = pLldpRemoteNode->u2RemMgmtVid;
        i1RetVal = SNMP_SUCCESS;
    }
    /* Release the memory allocated for Remote Node */
    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                        (UINT1 *) pTmpRemoteNode);
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot1RemLinkAggTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot1RemLinkAggTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot1RemLinkAggTable (UINT4 u4LldpV2RemTimeMark,
                                                    INT4
                                                    i4LldpV2RemLocalIfIndex,
                                                    UINT4
                                                    u4LldpV2RemLocalDestMACAddress,
                                                    INT4 i4LldpV2RemIndex)
{
    if (LldpUtlValidateIndexInstanceLldpXdot3RemLinkAggTable
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, i4LldpV2RemIndex,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlGetLldpXdot1RemProtocolId Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot1RemLinkAggTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot1RemLinkAggTable (UINT4 *pu4LldpV2RemTimeMark,
                                            INT4 *pi4LldpV2RemLocalIfIndex,
                                            UINT4
                                            *pu4LldpV2RemLocalDestMACAddress,
                                            INT4 *pi4LldpV2RemIndex)
{
    return (nmhGetNextIndexLldpV2Xdot1RemLinkAggTable (LLDP_ZERO,
                                                       pu4LldpV2RemTimeMark,
                                                       LLDP_ZERO,
                                                       pi4LldpV2RemLocalIfIndex,
                                                       LLDP_ZERO,
                                                       pu4LldpV2RemLocalDestMACAddress,
                                                       LLDP_ZERO,
                                                       pi4LldpV2RemIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot1RemLinkAggTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot1RemLinkAggTable (UINT4 u4LldpV2RemTimeMark,
                                           UINT4 *pu4NextLldpV2RemTimeMark,
                                           INT4 i4LldpV2RemLocalIfIndex,
                                           INT4 *pi4NextLldpV2RemLocalIfIndex,
                                           UINT4 u4LldpV2RemLocalDestMACAddress,
                                           UINT4
                                           *pu4NextLldpV2RemLocalDestMACAddress,
                                           INT4 i4LldpV2RemIndex,
                                           INT4 *pi4NextLldpV2RemIndex)
{
    tLldpRemoteNode    *pTmpRemoteNode = NULL;
    tLldpRemoteNode    *pLldpRemoteNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    /* Allocate Memory for Remote Node */
    if ((pTmpRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_MANDATORY_TLV_TRC,
                  "SNMPSTD: Failed to Allocate Memory " "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return SNMP_FAILURE;
    }
    MEMSET (pTmpRemoteNode, 0, sizeof (tLldpRemoteNode));
    pTmpRemoteNode->u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    pTmpRemoteNode->i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    pTmpRemoteNode->u4DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    pTmpRemoteNode->i4RemIndex = i4LldpV2RemIndex;

    pLldpRemoteNode =
        (tLldpRemoteNode *) RBTreeGetNext (gLldpGlobalInfo.RemSysDataRBTree,
                                           (tRBElem *) pTmpRemoteNode, NULL);

    if (pLldpRemoteNode != NULL)
    {
        *pu4NextLldpV2RemTimeMark = pLldpRemoteNode->u4RemLastUpdateTime;
        *pi4NextLldpV2RemLocalIfIndex = pLldpRemoteNode->i4RemLocalPortNum;
        *pi4NextLldpV2RemIndex = pLldpRemoteNode->i4RemIndex;
        *pu4NextLldpV2RemLocalDestMACAddress =
            pLldpRemoteNode->u4DestAddrTblIndex;
        i1RetVal = SNMP_SUCCESS;
    }

    /* Release the memory allocated for Remote Node */
    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                        (UINT1 *) pTmpRemoteNode);
    return i1RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1RemLinkAggStatus
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2Xdot1RemLinkAggStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1RemLinkAggStatus (UINT4 u4LldpV2RemTimeMark,
                                   INT4 i4LldpV2RemLocalIfIndex,
                                   UINT4 u4LldpV2RemLocalDestMACAddress,
                                   INT4 i4LldpV2RemIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValLldpV2Xdot1RemLinkAggStatus)
{
    if (LldpUtlGetLldpXdot3RemLinkAggStatus
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, i4LldpV2RemIndex,
         pRetValLldpV2Xdot1RemLinkAggStatus,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlGetLldpXdot3RemLinkAggStatus Failed\r\n");
        /* the port is not in link aggregation state and/or it
         * does not support link aggregation, */
        pRetValLldpV2Xdot1RemLinkAggStatus->pu1_OctetList[0] = 0;
        pRetValLldpV2Xdot1RemLinkAggStatus->i4_Length = sizeof (UINT1);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot1RemLinkAggPortId
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2Xdot1RemLinkAggPortId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot1RemLinkAggPortId (UINT4 u4LldpV2RemTimeMark,
                                   INT4 i4LldpV2RemLocalIfIndex,
                                   UINT4 u4LldpV2RemLocalDestMACAddress,
                                   INT4 i4LldpV2RemIndex,
                                   UINT4 *pu4RetValLldpV2Xdot1RemLinkAggPortId)
{
    if (LldpUtlGetLldpXdot3RemLinkAggPortId
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, i4LldpV2RemIndex,
         (INT4 *) pu4RetValLldpV2Xdot1RemLinkAggPortId,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlGetLldpXdot3RemLinkAggPortId Failed\r\n");
        /* the port is not in link aggregation state and/or it
         * does not support link aggregation, */
        *pu4RetValLldpV2Xdot1RemLinkAggPortId = (UINT4) -1;
    }
    return SNMP_SUCCESS;

}
