/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldrxutl.c,v 1.43 2017/10/11 13:42:02 siva Exp $
 *
 * Description: The file contails the utility functions related to Rx Module 
 *****************************************************************************/
#ifndef _LLDRXUTL_C_
#define _LLDRXUTL_C_

#include "lldinc.h"

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlDrainRemSysTables
 *
 *    DESCRIPTION      : This function Deletes all the RBTrees uesd to store 
 *                       the Neighbor information. But it doesnot delete the
 *                       RBTree. By this function call the 
 *                       following RBTrees are Drained -
 *                       1. RemManAddrRBTree
 *                       2. RemOrgDefInfoRBTree
 *                       3. RemUnknownTLVRBTree
 *                       4. RemVlanNameInfoRBTree
 *                       5. RemProtoVlanRBTree
 *                       6. RemProtoIdRBTree
 *                       7. RemSysDataRBTree
 *                       8. RemMSAPRBTree
 *                       9. LldpMedRemLocationInfoRBTree
 *                       10. LldpMedRemNwPolicyInfoRBTree
 *                       
 *                       This function Release the memory for the RBNodes also.
 *
 *                       NOTE : During Removal of RBNodes from the RBTrees in
 *                       turn the RBNodeFree Functions are call which is
 *                       responsible for releaseing the memory of used by the 
 *                       RBNodes.
 *                       
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
VOID
LldpRxUtlDrainRemSysTables (VOID)
{
    tLldpLocPortInfo   *pPortInfo = NULL;
    tLldpLocPortInfo    PortInfo;

    MEMSET (&PortInfo, 0, sizeof (tLldpLocPortInfo));

    /* 1. Drain RemManAddrRBTree */
    RBTreeDrain (gLldpGlobalInfo.RemManAddrRBTree, LldpRxUtlRBFreeManAddr, 0);

    /* 2. Drain RemOrgDefInfoRBTree */
    RBTreeDrain (gLldpGlobalInfo.RemOrgDefInfoRBTree,
                 LldpRxUtlRBFreeOrgDefInfo, 0);

    /* 3. Drain RemUnknownTLVRBTree */
    RBTreeDrain (gLldpGlobalInfo.RemUnknownTLVRBTree,
                 LldpRxUtlRBFreeUnknownTLV, 0);

    /* 4. Drain RemVlanNameInfoRBTree */
    RBTreeDrain (gLldpGlobalInfo.RemVlanNameInfoRBTree,
                 LldpRxUtlRBFreeVlanNameInfo, 0);

    /* 5. Drain RemProtoVlanRBTree */
    RBTreeDrain (gLldpGlobalInfo.RemProtoVlanRBTree,
                 LldpRxUtlRBFreeProtoVlanInfo, 0);

    /* 6. Drain RemProtoIdRBTree */
    RBTreeDrain (gLldpGlobalInfo.RemProtoIdRBTree,
                 LldpRxUtlRBFreeProtoIdInfo, 0);

    /* 7. Drain RemSysDataRBTree */
    RBTreeDrain (gLldpGlobalInfo.RemSysDataRBTree, LldpRxUtlRBFreeSysData, 0);

    /* 8. Drain RemMSAPRBTree */
    RBTreeDrain (gLldpGlobalInfo.RemMSAPRBTree, LldpRxUtlRBFreeMSAP, 0);

    /* 9. Drain LldpMedRemLocationInfoRBTree */
    RBTreeDrain (gLldpGlobalInfo.LldpMedRemLocationInfoRBTree,
                 LldpRxUtlRBFreeLocationInfo, 0);

    /* 10. Drain LldpMedRemNwPolicyInfoRBTree */
    RBTreeDrain (gLldpGlobalInfo.LldpMedRemNwPolicyInfoRBTree,
                 LldpRxUtlRBFreeNwPolicyInfo, 0);

    pPortInfo = (tLldpLocPortInfo *)
        RBTreeGetFirst (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree);
    while (pPortInfo != NULL)
    {
        /* Check whether LLDP-MED is enabled for particular agent. 
         * If it is enabled, reset the flag and construct the preformed buffer.*/
        if (pPortInfo->bMedCapable == LLDP_MED_TRUE)
        {
            pPortInfo->bMedCapable = LLDP_MED_FALSE;
            if (LldpTxUtlHandleLocPortInfoChg (pPortInfo) != OSIX_SUCCESS)
            {
                LLDP_TRC (ALL_FAILURE_TRC, "LldpRxUtlDrainRemSysTables: "
                          "LldpTxUtlHandleLocPortInfoChg failed\r\n");
            }
        }
        LldpUtlSendAppAgeout (pPortInfo);
        pPortInfo->u2NoOfNeighbors = 0;
        PortInfo.u4LocPortNum = pPortInfo->u4LocPortNum;
        pPortInfo = (tLldpLocPortInfo *)
            RBTreeGetNext (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
                           &PortInfo, LldpAgentInfoUtlRBCmpInfo);
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlDelRemInfoForPort
 *
 *    DESCRIPTION      : This function Deletes all the remote information 
 *                       learnt on the given port. 
 *
 *    INPUT            : pLocPortInfo - Pointer to the port structure, All the 
 *                       remote infomation learnt on this port are removed here
 *
 *    OUTPUT           : NONE
 *
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
VOID
LldpRxUtlDelRemInfoForPort (tLldpLocPortInfo * pLocPortInfo)
{
    tLldpRemoteNode    *pFirstRemoteNode = NULL;
    tLldpRemoteNode    *pNextRemoteNode = NULL;
    tLldpRemoteNode    *pTempRemoteNode = NULL;

    /* Walk the MSAP RBTree to Get the first Remote Node Learnt on this port */
    RBTreeWalk (gLldpGlobalInfo.RemMSAPRBTree,
                (tRBWalkFn) LldpRxUtlWalkFnRemNodeForPort,
                &pLocPortInfo->i4IfIndex, &pFirstRemoteNode);

    if (pFirstRemoteNode == NULL)
    {
        /* No Neighbor is learnt is learnt on this port */
        return;
    }

    pTempRemoteNode = pFirstRemoteNode;
    /* Get the next Remote Node Learnt on this port */
    while (LldpRxUtlGetNextMSAPRemoteNode (pFirstRemoteNode, &pNextRemoteNode)
           == OSIX_SUCCESS)
    {
        if ((pNextRemoteNode->i4RemLocalPortNum
             == (INT4) pLocPortInfo->i4IfIndex) &&
            (pNextRemoteNode->u4DestAddrTblIndex ==
             pLocPortInfo->u4DstMacAddrTblIndex))

        {
            LldpRxUtlDelRemoteNode (pNextRemoteNode);
            LLDP_REMOTE_STAT_TABLES_DELETES++;
        }
        else
        {
            pFirstRemoteNode = pNextRemoteNode;
            pNextRemoteNode = NULL;
        }
    }

    /* Delete the First Node now */
    if ((pTempRemoteNode->i4RemLocalPortNum
         == (INT4) pLocPortInfo->i4IfIndex) &&
        (pTempRemoteNode->u4DestAddrTblIndex ==
         pLocPortInfo->u4DstMacAddrTblIndex))
    {
        LldpRxUtlDelRemoteNode (pTempRemoteNode);
        LLDP_REMOTE_STAT_TABLES_DELETES++;
    }
    if (pLocPortInfo->bMedCapable == LLDP_MED_TRUE)
    {
        pLocPortInfo->bMedCapable = LLDP_MED_FALSE;
        if (LldpTxUtlConstructPreformedBuf (pLocPortInfo, LLDP_INFO_FRAME)
            != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC | LLDP_MED_CAPAB_TRC,
                      "LldpRxUtlDelRemInfoForPort: "
                      "Construction of preformed buffer Failed.\r\n");
        }

    }
    LLDP_REMOTE_STAT_LAST_CHNG_TIME = LLDP_SYS_UP_TIME ();
    /* Since all the remote information are flushed here,
     * age out the applications on the port */
#ifdef L2RED_WANTED
    if (LLDP_RED_NODE_STATUS () == RM_ACTIVE)
#endif
    {
        LldpUtlSendAppAgeout (pLocPortInfo);
    }

    LLDP_TRC_ARG1 (OS_RESOURCE_TRC, "LldpRxUtlDelRemInfoForPort: "
                   "All the remote nodes learnt on port: %d are deleted.\r\n",
                   pLocPortInfo->u4LocPortNum);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlDelRemVlanNameInfo
 *
 *    DESCRIPTION      : This function deletes all the Vlan Name Nodes
 *                       from the RemVlanNameInfoRBTree associated with 
 *                       the given neighbour.
 *                       During the removal of RBNodes from the RBTree,
 *                       RBNodeFree Function is called which is
 *                       responsible for releasing the memory allocated to 
 *                       that node. 
 *
 *    INPUT            : pRemoteNode - Pointer to the Remote Node structure
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
LldpRxUtlDelRemVlanNameInfo (tLldpRemoteNode * pRemoteNode)
{
    tLldpxdot1RemVlanNameInfo *pFirstVlanNameNode = NULL;
    tLldpxdot1RemVlanNameInfo *pNextVlanNameNode = NULL;

    /* Get the first Vlan Name Node for this neighbhor */
    pFirstVlanNameNode = pRemoteNode->pDot1RemVlanNameInfo;

    if (pFirstVlanNameNode == NULL)
    {
        /* No vlan name node has been added for this neighbor. Hence 
         * return SUCCESS */
        return OSIX_SUCCESS;
    }

    while (LldpRxUtlGetNextVlanName (pFirstVlanNameNode, &pNextVlanNameNode)
           == OSIX_SUCCESS)
    {
        if (LLDP_IS_NODE_FOR_NEIGHBOR (pFirstVlanNameNode, pNextVlanNameNode)
            == OSIX_TRUE)
        {
            RBTreeRem (gLldpGlobalInfo.RemVlanNameInfoRBTree,
                       (tRBElem *) pNextVlanNameNode);
            LldpRxUtlRBFreeVlanNameInfo ((tRBElem *) pNextVlanNameNode, 0);
        }
        else
        {
            break;
        }
    }

    /* Delete the first node */
    RBTreeRem (gLldpGlobalInfo.RemVlanNameInfoRBTree,
               (tRBElem *) pFirstVlanNameNode);
    LldpRxUtlRBFreeVlanNameInfo ((tRBElem *) pFirstVlanNameNode, 0);

    /* Reset the Link in Remote Node */
    pRemoteNode->pDot1RemVlanNameInfo = NULL;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlDelRemProtoVlan
 *
 *    DESCRIPTION      : This function deletes all the Protocol Vlan Nodes
 *                       from the RemProtoVlanRBTree associated with 
 *                       the given neighbour.
 *                       During the removal of RBNodes from the RBTree,
 *                       RBNodeFree Function is called which is
 *                       responsible for releasing the memory allocated to 
 *                       that node. 
 *
 *    INPUT            : pRemoteNode - Pointer to the remote node
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
LldpRxUtlDelRemProtoVlan (tLldpRemoteNode * pRemoteNode)
{
    tLldpxdot1RemProtoVlanInfo *pFirstProtoVlanNode;
    tLldpxdot1RemProtoVlanInfo *pNextProtoVlanNode = NULL;

    /* Get the first Protocol Vlan Node for this neighbhor */
    pFirstProtoVlanNode = pRemoteNode->pDot1RemProtoVlanInfo;

    if (pFirstProtoVlanNode == NULL)
    {
        /* No protocol vlan has been added to this neighbor. Hence 
         * return SUCCESS */
        return OSIX_SUCCESS;
    }

    while (LldpRxUtlGetNextProtoVlan (pFirstProtoVlanNode, &pNextProtoVlanNode)
           == OSIX_SUCCESS)
    {
        if (LLDP_IS_NODE_FOR_NEIGHBOR (pFirstProtoVlanNode, pNextProtoVlanNode)
            == OSIX_TRUE)
        {
            RBTreeRem (gLldpGlobalInfo.RemProtoVlanRBTree,
                       (tRBElem *) pNextProtoVlanNode);
            LldpRxUtlRBFreeProtoVlanInfo ((tRBElem *) pNextProtoVlanNode, 0);
        }
        else
        {
            break;
        }
    }

    /* Delete the first node */
    RBTreeRem (gLldpGlobalInfo.RemProtoVlanRBTree,
               (tRBElem *) pFirstProtoVlanNode);
    LldpRxUtlRBFreeProtoVlanInfo ((tRBElem *) pFirstProtoVlanNode, 0);

    /* Reset the link in Remote Node */
    pRemoteNode->pDot1RemProtoVlanInfo = NULL;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlDelRemProtoId
 *
 *    DESCRIPTION      : This function deletes all the Protocol Id Nodes
 *                       from the RemProtoIdRBTree associated with 
 *                       the given neighbour.
 *                       During the removal of RBNodes from the RBTree,
 *                       RBNodeFree Function is called which is
 *                       responsible for releasing the memory allocated to 
 *                       that node. 
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
LldpRxUtlDelRemProtoId (tLldpRemoteNode * pRemoteNode)
{

    tLldpxdot1RemProtoIdInfo *pFirstProtoIdNode;
    tLldpxdot1RemProtoIdInfo *pNextProtoIdNode = NULL;

    pFirstProtoIdNode = pRemoteNode->pDot1RemProtocolInfo;

    if (pFirstProtoIdNode == NULL)
    {
        /* No protocol id node has been added to this neighbor. Hence 
         * return SUCCESS */
        return OSIX_SUCCESS;
    }

    while (LldpRxUtlGetNextProtoId (pFirstProtoIdNode, &pNextProtoIdNode)
           == OSIX_SUCCESS)
    {
        if (LLDP_IS_NODE_FOR_NEIGHBOR (pFirstProtoIdNode, pNextProtoIdNode)
            == OSIX_TRUE)
        {
            RBTreeRem (gLldpGlobalInfo.RemProtoIdRBTree,
                       (tRBElem *) pNextProtoIdNode);
            LldpRxUtlRBFreeProtoIdInfo ((tRBElem *) pNextProtoIdNode, 0);
        }
        else
        {
            break;
        }
    }

    /* Delete the first node */
    RBTreeRem (gLldpGlobalInfo.RemProtoIdRBTree, (tRBElem *) pFirstProtoIdNode);
    LldpRxUtlRBFreeProtoIdInfo ((tRBElem *) pFirstProtoIdNode, 0);

    /* Reset the link in Remote Node */
    pRemoteNode->pDot1RemProtocolInfo = NULL;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedRxUtlDelRemPolicyInfo
 *
 *    DESCRIPTION      : This function deletes all the LLDP-MED 
 *                       Media policy Nodes from the LldpMedRemNwPolicyInfoRBTree 
 *                       associated with the given neighbour.
 *                       During the removal of RBNodes from the RBTree,
 *                       RBNodeFree Function is called which is
 *                       responsible for releasing the memory allocated to 
 *                       that node. 
 *
 *    INPUT            : pRemoteNode - Pointer to the remote node
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PUBLIC VOID
LldpMedRxUtlDelRemPolicyInfo (tLldpRemoteNode * pRemoteNode)
{
    tLldpMedRemNwPolicyInfo PolicyNode;
    tLldpMedRemNwPolicyInfo *pPolicyNode = NULL;
    tLldpMedRemNwPolicyInfo *pNextPolicyNode = NULL;

    MEMSET (&PolicyNode, 0, sizeof (tLldpMedRemNwPolicyInfo));

    PolicyNode.u4RemLastUpdateTime = pRemoteNode->u4RemLastUpdateTime;
    PolicyNode.i4RemLocalPortNum = pRemoteNode->i4RemLocalPortNum;
    PolicyNode.i4RemIndex = pRemoteNode->i4RemIndex;
    PolicyNode.u4RemDestAddrTblIndex = pRemoteNode->u4DestAddrTblIndex;

    pPolicyNode = RBTreeGetNext (gLldpGlobalInfo.LldpMedRemNwPolicyInfoRBTree,
                                 (tRBElem *) & PolicyNode, NULL);
    while ((pPolicyNode != NULL) &&
           ((pRemoteNode->u4RemLastUpdateTime ==
             pPolicyNode->u4RemLastUpdateTime)
            && (pRemoteNode->i4RemLocalPortNum ==
                pPolicyNode->i4RemLocalPortNum)
            && (pRemoteNode->i4RemIndex == pPolicyNode->i4RemIndex)
            && (pRemoteNode->u4DestAddrTblIndex ==
                pPolicyNode->u4RemDestAddrTblIndex)))
    {
        /* Get the next node pointer */
        pNextPolicyNode =
            RBTreeGetNext (gLldpGlobalInfo.LldpMedRemNwPolicyInfoRBTree,
                           (tRBElem *) pPolicyNode, NULL);

        /* Release the memory to the current node */
        RBTreeRem (gLldpGlobalInfo.LldpMedRemNwPolicyInfoRBTree,
                   (tRBElem *) pPolicyNode);
        MemReleaseMemBlock (gLldpGlobalInfo.LldpMedRemNwPolicyPoolId,
                            (UINT1 *) pPolicyNode);

        /* Assign the next node to current pointer */
        pPolicyNode = pNextPolicyNode;
    }
    return;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedRxUtlDelRemLocationInfo 
 *
 *    DESCRIPTION      : This function deletes all the LLDP-MED 
 *                       Location Information Nodes from the LldpMedRemLocationInfoRBTree 
 *                       associated with the given neighbour.
 *                       During the removal of RBNodes from the RBTree,
 *                       RBNodeFree Function is called which is
 *                       responsible for releasing the memory allocated to 
 *                       that node. 
 *
 *    INPUT            : pRemoteNode - Pointer to the remote node
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PUBLIC VOID
LldpMedRxUtlDelRemLocationInfo (tLldpRemoteNode * pRemoteNode)
{
    tLldpMedRemLocationInfo LocationNode;
    tLldpMedRemLocationInfo *pLocationNode = NULL;
    tLldpMedRemLocationInfo *pNextLocationNode = NULL;

    MEMSET (&LocationNode, 0, sizeof (tLldpMedRemLocationInfo));

    LocationNode.u4RemLastUpdateTime = pRemoteNode->u4RemLastUpdateTime;
    LocationNode.i4RemLocalPortNum = pRemoteNode->i4RemLocalPortNum;
    LocationNode.i4RemIndex = pRemoteNode->i4RemIndex;
    LocationNode.u4RemDestAddrTblIndex = pRemoteNode->u4DestAddrTblIndex;

    pLocationNode = RBTreeGetNext (gLldpGlobalInfo.LldpMedRemLocationInfoRBTree,
                                   (tRBElem *) & LocationNode, NULL);
    while ((pLocationNode != NULL) &&
           ((pRemoteNode->u4RemLastUpdateTime ==
             pLocationNode->u4RemLastUpdateTime)
            && (pRemoteNode->i4RemLocalPortNum ==
                pLocationNode->i4RemLocalPortNum)
            && (pRemoteNode->i4RemIndex == pLocationNode->i4RemIndex)
            && (pRemoteNode->u4DestAddrTblIndex ==
                pLocationNode->u4RemDestAddrTblIndex)))
    {
        /* Get the next node pointer */
        pNextLocationNode =
            RBTreeGetNext (gLldpGlobalInfo.LldpMedRemLocationInfoRBTree,
                           (tRBElem *) pLocationNode, NULL);

        /* Release the memory to the current node */
        RBTreeRem (gLldpGlobalInfo.LldpMedRemLocationInfoRBTree,
                   (tRBElem *) pLocationNode);
        MemReleaseMemBlock (gLldpGlobalInfo.LldpMedRemLocationPoolId,
                            (UINT1 *) pLocationNode);

        /* Assign the next node to current pointer */
        pLocationNode = pNextLocationNode;
    }
    return;
}

/*--------------------------------------------------------------------------*/
/*                       RB Tree Compare Routines                           */
/*--------------------------------------------------------------------------*/
/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlRBCmpMSAP
 *
 *    DESCRIPTION      : RBTree compare function for RemMSAPRBTree. This RB
 *                       Tree is usefull for search, updation and deletion
 *                       of a particular remote node.
 *                       Index of this table -
 *                       i4RemLocalPortNum
 *                       i4RemChassisIdSubtype
 *                       au1RemChassisId
 *                       i4RemPortIdSubtype
 *                       au1RemPortId
 *
 *    INPUT            : pRBElemi1 - Pointer to the RemoteNode nod pRBElem1
 *                       pRBElem2 - Pointer to the RemoteNode node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : LLDP_RB_EQUAL   - if all the keys matched for both 
 *                                         the nodes.
 *                       LLDP_RB_LESS    - if node pRBElem1's key is less than
 *                                         node pRBElem2's key.
 *                       LLDP_RB_GREATER - if node pRBElem1's key is greater 
 *                                         than node pRBElem2's key.
 *
 ****************************************************************************/
INT4
LldpRxUtlRBCmpMSAP (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tLldpRemoteNode    *pRemEntry1 = (tLldpRemoteNode *) pRBElem1;
    tLldpRemoteNode    *pRemEntry2 = (tLldpRemoteNode *) pRBElem2;
    INT4                i4MemcmpRetVal = LLDP_INVALID_VALUE;
    UINT2               u2ChassIdLen1 = 0;
    UINT2               u2ChassIdLen2 = 0;
    UINT2               u2PortIdLen1 = 0;
    UINT2               u2PortIdLen2 = 0;

    /* Compare the First Index */
    if (pRemEntry1->i4RemLocalPortNum > pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemLocalPortNum < pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_LESS;
    }

    /*check Destination address table index */
    if (pRemEntry1->u4DestAddrTblIndex > pRemEntry2->u4DestAddrTblIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4DestAddrTblIndex < pRemEntry2->u4DestAddrTblIndex)
    {
        return LLDP_RB_LESS;
    }

    /* First Indexes are equal so Compare the Second Index */
    if (pRemEntry1->i4RemChassisIdSubtype > pRemEntry2->i4RemChassisIdSubtype)
    {
        return LLDP_RB_GREATER;
    }
    if (pRemEntry1->i4RemChassisIdSubtype < pRemEntry2->i4RemChassisIdSubtype)
    {
        return LLDP_RB_LESS;
    }

    /* First  and Second Indexes are equal so Compare the Third Index */
    if (LldpUtilGetChassisIdLen (pRemEntry1->i4RemChassisIdSubtype,
                                 pRemEntry1->au1RemChassisId,
                                 &u2ChassIdLen1) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpRxUtlRBCmpMSAP: "
                  "LldpUtilGetChassisIdLen return FAILURE\r\n");

    }
    if (LldpUtilGetChassisIdLen (pRemEntry2->i4RemChassisIdSubtype,
                                 pRemEntry2->au1RemChassisId,
                                 &u2ChassIdLen2) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpRxUtlRBCmpMSAP: "
                  "LldpUtilGetChassisIdLen return FAILURE\r\n");
    }

    if ((u2ChassIdLen1 <= LLDP_MAX_LEN_CHASSISID) &&
        (u2ChassIdLen2 <= LLDP_MAX_LEN_CHASSISID))
    {
        i4MemcmpRetVal = MEMCMP (pRemEntry1->au1RemChassisId,
                                 pRemEntry2->au1RemChassisId,
                                 LLDP_MAX_MEM_CMP_LEN (u2ChassIdLen1,
                                                       u2ChassIdLen2));
    }

    if (i4MemcmpRetVal > 0)
    {
        return LLDP_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return LLDP_RB_LESS;
    }

    /* 2nd and 3rd Indexes are equal so Compare the 4th Index */
    if (pRemEntry1->i4RemPortIdSubtype > pRemEntry2->i4RemPortIdSubtype)
    {
        return LLDP_RB_GREATER;
    }
    if (pRemEntry1->i4RemPortIdSubtype < pRemEntry2->i4RemPortIdSubtype)
    {
        return LLDP_RB_LESS;
    }

    /* 3rd and 4th Indexes are equal so Compare the 5th Index */
    if (LldpUtilGetPortIdLen (pRemEntry1->i4RemPortIdSubtype,
                              pRemEntry1->au1RemPortId,
                              &u2PortIdLen1) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpUtilGetPortIdLen: LldpUtilGetPortIdLen"
                  "returns FAILURE\r\n");
    }
    if (LldpUtilGetPortIdLen (pRemEntry2->i4RemPortIdSubtype,
                              pRemEntry2->au1RemPortId,
                              &u2PortIdLen2) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpUtilGetPortIdLen: LldpUtilGetPortIdLen"
                  "returns FAILURE\r\n");
    }

    if ((u2PortIdLen1 <= LLDP_MAX_LEN_PORTID) &&
        (u2PortIdLen2 <= LLDP_MAX_LEN_PORTID))
    {
        i4MemcmpRetVal = MEMCMP (pRemEntry1->au1RemPortId,
                                 pRemEntry2->au1RemPortId,
                                 LLDP_MAX_MEM_CMP_LEN (u2PortIdLen1,
                                                       u2PortIdLen2));
    }

    if (i4MemcmpRetVal > 0)
    {
        return LLDP_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return LLDP_RB_LESS;
    }
    else
    {
        return LLDP_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlRBCmpSysData
 *
 *    DESCRIPTION      : RBTree compare function for RemSysDataRBTree. This 
 *                       RB Tree is usefull for SNMP operations on Remote
 *                       System Data.
 *                       Index of this table -
 *                       u4RemLastUpdateTime
 *                       i4RemLocalPortNum
 *                       i4RemIndex
 *
 *    INPUT            : e1 - Pointer to the RemoteNode node1
 *                       e2 - Pointer to the RemoteNode node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : LLDP_RB_EQUAL   - if all the keys matched for both 
 *                                         the nodes.
 *                       LLDP_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       LLDP_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
INT4
LldpRxUtlRBCmpSysData (tRBElem * e1, tRBElem * e2)
{
    tLldpRemoteNode    *pRemEntry1 = (tLldpRemoteNode *) e1;
    tLldpRemoteNode    *pRemEntry2 = (tLldpRemoteNode *) e2;

    /* Compare the First Key */
    if (pRemEntry1->u4RemLastUpdateTime > pRemEntry2->u4RemLastUpdateTime)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4RemLastUpdateTime < pRemEntry2->u4RemLastUpdateTime)
    {
        return LLDP_RB_LESS;
    }
    /* First key is equal so compare the 2nd key */
    if (pRemEntry1->i4RemLocalPortNum > pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemLocalPortNum < pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_LESS;
    }
    /* First key is equal so compare the 2nd key */
    if (pRemEntry1->u4DestAddrTblIndex > pRemEntry2->u4DestAddrTblIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4DestAddrTblIndex < pRemEntry2->u4DestAddrTblIndex)
    {
        return LLDP_RB_LESS;
    }
    /* 2nd key is equal so compare the 3rd key */
    if (pRemEntry1->i4RemIndex > pRemEntry2->i4RemIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemIndex < pRemEntry2->i4RemIndex)
    {
        return LLDP_RB_LESS;
    }
    else
    {
        return LLDP_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlRBCmpManAddr
 *
 *    DESCRIPTION      : RBTree compare function for RemManAddrRBTree. This 
 *                       RB Tree is usefull for SNMP operations on Remote
 *                       Management Addr Table.
 *                       Index of this table -
 *                       1. u4RemLastUpdateTime
 *                       2. i4RemLocalPortNum
 *                       3. i4RemIndex
 *                       4. i4RemManAddrSubtype
 *                       5. au1RemManAddr
 *
 *    INPUT            : e1 - Pointer to the RemoteNode node1
 *                       e2 - Pointer to the RemoteNode node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : LLDP_RB_EQUAL   - if all the keys matched for both 
 *                                         the nodes.
 *                       LLDP_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       LLDP_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
INT4
LldpRxUtlRBCmpManAddr (tRBElem * pElem1, tRBElem * pElem2)
{
    tLldpRemManAddressTable *pRemEntry1 = (tLldpRemManAddressTable *) pElem1;
    tLldpRemManAddressTable *pRemEntry2 = (tLldpRemManAddressTable *) pElem2;
    INT4                i4MemcmpRetVal = LLDP_INVALID_VALUE;
    UINT2               u2ManAddrLen1 = 0;
    UINT2               u2ManAddrLen2 = 0;

    /* Compare the First Key */
    if (pRemEntry1->u4RemLastUpdateTime > pRemEntry2->u4RemLastUpdateTime)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4RemLastUpdateTime < pRemEntry2->u4RemLastUpdateTime)
    {
        return LLDP_RB_LESS;
    }
    /* First key is equal so compare the 2nd key */
    if (pRemEntry1->i4RemLocalPortNum > pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemLocalPortNum < pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_LESS;
    }
    /* First key is equal so compare the 2nd key */
    if (pRemEntry1->u4DestAddrTblIndex > pRemEntry2->u4DestAddrTblIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4DestAddrTblIndex < pRemEntry2->u4DestAddrTblIndex)
    {
        return LLDP_RB_LESS;
    }
    /* 2nd key is equal so compare the 3rd key */
    if (pRemEntry1->i4RemIndex > pRemEntry2->i4RemIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemIndex < pRemEntry2->i4RemIndex)
    {
        return LLDP_RB_LESS;
    }
    /* 3rd key is equal so compare the 4th key */
    if (pRemEntry1->i4RemManAddrSubtype > pRemEntry2->i4RemManAddrSubtype)
    {
        return LLDP_RB_GREATER;
    }
    if (pRemEntry1->i4RemManAddrSubtype < pRemEntry2->i4RemManAddrSubtype)
    {
        return LLDP_RB_LESS;
    }
    u2ManAddrLen1 = LldpUtilGetManAddrLen (pRemEntry1->i4RemManAddrSubtype);
    u2ManAddrLen2 = LldpUtilGetManAddrLen (pRemEntry2->i4RemManAddrSubtype);

    i4MemcmpRetVal = MEMCMP (pRemEntry1->au1RemManAddr,
                             pRemEntry2->au1RemManAddr,
                             LLDP_MAX_MEM_CMP_LEN (u2ManAddrLen1,
                                                   u2ManAddrLen2));

    if (i4MemcmpRetVal > 0)
    {
        return LLDP_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return LLDP_RB_LESS;
    }
    else
    {
        return LLDP_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlRBCmpOrgDefInfo
 *
 *    DESCRIPTION      : RBTree compare function for RemOrgDefInfoRBTree. This 
 *                       RB Tree is usefull for SNMP operations on Remote
 *                       Organizationally Defined Info Table.
 *                       Index of this table -
 *                       1. u4RemLastUpdateTime
 *                       2. i4RemLocalPortNum
 *                       3. i4RemIndex
 *                       4. i4RemOrgDefInfoSubtype
 *                       5. i4RemOrgDefInfoIndex
 *
 *    INPUT            : e1 - Pointer to the RemoteNode node1
 *                       e2 - Pointer to the RemoteNode node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : LLDP_RB_EQUAL   - if all the keys matched for both 
 *                                         the nodes.
 *                       LLDP_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       LLDP_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
INT4
LldpRxUtlRBCmpOrgDefInfo (tRBElem * pElem1, tRBElem * pElem2)
{
    INT4                i4MemcmpRetVal = 0;

    tLldpRemOrgDefInfoTable *pRemEntry1 = (tLldpRemOrgDefInfoTable *) pElem1;
    tLldpRemOrgDefInfoTable *pRemEntry2 = (tLldpRemOrgDefInfoTable *) pElem2;

    /* Compare the First Key */
    if (pRemEntry1->u4RemLastUpdateTime > pRemEntry2->u4RemLastUpdateTime)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4RemLastUpdateTime < pRemEntry2->u4RemLastUpdateTime)
    {
        return LLDP_RB_LESS;
    }
    /* First key is equal so compare the 2nd key */
    if (pRemEntry1->i4RemLocalPortNum > pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemLocalPortNum < pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_LESS;
    }
    /* First key is equal so compare the 2nd key */
    if (pRemEntry1->u4DestAddrTblIndex > pRemEntry2->u4DestAddrTblIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4DestAddrTblIndex < pRemEntry2->u4DestAddrTblIndex)
    {
        return LLDP_RB_LESS;
    }
    /* 2nd key is equal so compare the 3rd key */
    if (pRemEntry1->i4RemIndex > pRemEntry2->i4RemIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemIndex < pRemEntry2->i4RemIndex)
    {
        return LLDP_RB_LESS;
    }
    /* 3rd key is equal so compare the 4th key */
    i4MemcmpRetVal = MEMCMP (pRemEntry1->au1RemOrgDefInfoOUI,
                             pRemEntry2->au1RemOrgDefInfoOUI, LLDP_MAX_LEN_OUI);
    if (i4MemcmpRetVal > 0)
    {
        return LLDP_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return LLDP_RB_LESS;
    }
    /* 4th key is equal so compare the 5th key */
    if (pRemEntry1->i4RemOrgDefInfoSubtype > pRemEntry2->i4RemOrgDefInfoSubtype)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemOrgDefInfoSubtype <
             pRemEntry2->i4RemOrgDefInfoSubtype)
    {
        return LLDP_RB_LESS;
    }
    /* 5th key is equal so compare the 6th key */
    if (pRemEntry1->i4RemOrgDefInfoIndex > pRemEntry2->i4RemOrgDefInfoIndex)

    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemOrgDefInfoIndex <
             pRemEntry2->i4RemOrgDefInfoIndex)
    {
        return LLDP_RB_LESS;
    }
    else
    {
        return LLDP_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlRBCmpUnknownTLV
 *
 *    DESCRIPTION      : RBTree compare function for RemUnknownTLVRBTree. This 
 *                       RB Tree is usefull for SNMP operations on Remote
 *                       UnknownTLV Info Table.
 *                       Index of this table -
 *                       1. u4RemLastUpdateTime
 *                       2. i4RemLocalPortNum
 *                       3. i4RemIndex
 *                       4. i4RemUnknownTLVType
 *
 *    INPUT            : e1 - Pointer to the RemoteNode node1
 *                       e2 - Pointer to the RemoteNode node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : LLDP_RB_EQUAL   - if all the keys matched for both 
 *                                         the nodes.
 *                       LLDP_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       LLDP_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
INT4
LldpRxUtlRBCmpUnknownTLV (tRBElem * pElem1, tRBElem * pElem2)
{
    tLldpRemUnknownTLVTable *pRemEntry1 = (tLldpRemUnknownTLVTable *) pElem1;
    tLldpRemUnknownTLVTable *pRemEntry2 = (tLldpRemUnknownTLVTable *) pElem2;

    /* Compare the First Key */
    if (pRemEntry1->u4RemLastUpdateTime > pRemEntry2->u4RemLastUpdateTime)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4RemLastUpdateTime < pRemEntry2->u4RemLastUpdateTime)
    {
        return LLDP_RB_LESS;
    }
    /* First key is equal so compare the 2nd key */
    if (pRemEntry1->i4RemLocalPortNum > pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemLocalPortNum < pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_LESS;
    }
    /* First key is equal so compare the 2nd key */
    if (pRemEntry1->u4DestAddrTblIndex > pRemEntry2->u4DestAddrTblIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4DestAddrTblIndex < pRemEntry2->u4DestAddrTblIndex)
    {
        return LLDP_RB_LESS;
    }
    /* 2nd key is equal so compare the 3rd key */
    if (pRemEntry1->i4RemIndex > pRemEntry2->i4RemIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemIndex < pRemEntry2->i4RemIndex)
    {
        return LLDP_RB_LESS;
    }
    /* 3rd key is equal so compare the 4th key */
    if (pRemEntry1->i4RemUnknownTLVType > pRemEntry2->i4RemUnknownTLVType)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemUnknownTLVType < pRemEntry2->i4RemUnknownTLVType)
    {
        return LLDP_RB_LESS;
    }
    else
    {
        return LLDP_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlRBCmpVlanNameInfo
 *
 *    DESCRIPTION      : RBTree compare function for RemVlanNameInfoRBTree.
 *                       This RB Tree is usefull for SNMP operations on Remote 
 *                       Vlan Name Info Table.
 *                       Index of this table -
 *                       1. u4RemLastUpdateTime
 *                       2. i4RemLocalPortNum
 *                       3. i4RemIndex
 *                       4. u2VlanId
 *
 *    INPUT            : e1 - Pointer to the RemoteNode node1
 *                       e2 - Pointer to the RemoteNode node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : LLDP_RB_EQUAL   - if all the keys matched for both 
 *                                         the nodes.
 *                       LLDP_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       LLDP_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
INT4
LldpRxUtlRBCmpVlanNameInfo (tRBElem * pElem1, tRBElem * pElem2)
{
    tLldpxdot1RemVlanNameInfo *pRemEntry1;
    tLldpxdot1RemVlanNameInfo *pRemEntry2;

    pRemEntry1 = (tLldpxdot1RemVlanNameInfo *) pElem1;
    pRemEntry2 = (tLldpxdot1RemVlanNameInfo *) pElem2;

    /* Compare the First Key */
    if (pRemEntry1->u4RemLastUpdateTime > pRemEntry2->u4RemLastUpdateTime)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4RemLastUpdateTime < pRemEntry2->u4RemLastUpdateTime)
    {
        return LLDP_RB_LESS;
    }

    /* First key is equal so compare the 2nd key */
    if (pRemEntry1->i4RemLocalPortNum > pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemLocalPortNum < pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_LESS;
    }

    /* First key is equal so compare the 2nd key */
    if (pRemEntry1->u4DestAddrTblIndex > pRemEntry2->u4DestAddrTblIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4DestAddrTblIndex < pRemEntry2->u4DestAddrTblIndex)
    {
        return LLDP_RB_LESS;
    }
    /* 2nd key is equal so compare the 3rd key */
    if (pRemEntry1->i4RemIndex > pRemEntry2->i4RemIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemIndex < pRemEntry2->i4RemIndex)
    {
        return LLDP_RB_LESS;
    }

    /* 3rd key is equal so compare the 4th key */
    if (pRemEntry1->u2VlanId > pRemEntry2->u2VlanId)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u2VlanId < pRemEntry2->u2VlanId)
    {
        return LLDP_RB_LESS;
    }
    else
    {
        return LLDP_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlRBCmpProtoVlanInfo
 *
 *    DESCRIPTION      : RBTree compare function for RemProtoVlanRBTree. This 
 *                       RB Tree is usefull for SNMP operations on Remote
 *                       Protocol Vlan Info Table.
 *                       Index of this table -
 *                       1. u4RemLastUpdateTime
 *                       2. i4RemLocalPortNum
 *                       3. i4RemIndex
 *                       4. u2ProtoVlanId
 *
 *    INPUT            : e1 - Pointer to the RemoteNode node1
 *                       e2 - Pointer to the RemoteNode node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : LLDP_RB_EQUAL   - if all the keys matched for both 
 *                                         the nodes.
 *                       LLDP_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       LLDP_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
INT4
LldpRxUtlRBCmpProtoVlanInfo (tRBElem * pElem1, tRBElem * pElem2)
{
    tLldpxdot1RemProtoVlanInfo *pRemEntry1;
    tLldpxdot1RemProtoVlanInfo *pRemEntry2;

    pRemEntry1 = (tLldpxdot1RemProtoVlanInfo *) pElem1;
    pRemEntry2 = (tLldpxdot1RemProtoVlanInfo *) pElem2;

    /* Compare the First Key */
    if (pRemEntry1->u4RemLastUpdateTime > pRemEntry2->u4RemLastUpdateTime)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4RemLastUpdateTime < pRemEntry2->u4RemLastUpdateTime)
    {
        return LLDP_RB_LESS;
    }

    /* First key is equal so compare the 2nd key */
    if (pRemEntry1->i4RemLocalPortNum > pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemLocalPortNum < pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_LESS;
    }

    /* First key is equal so compare the 2nd key */
    if (pRemEntry1->u4DestAddrTblIndex > pRemEntry2->u4DestAddrTblIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4DestAddrTblIndex < pRemEntry2->u4DestAddrTblIndex)
    {
        return LLDP_RB_LESS;
    }
    /* 2nd key is equal so compare the 3rd key */
    if (pRemEntry1->i4RemIndex > pRemEntry2->i4RemIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemIndex < pRemEntry2->i4RemIndex)
    {
        return LLDP_RB_LESS;
    }

    /* 3rd key is equal so compare the 4th key */
    if (pRemEntry1->u2ProtoVlanId > pRemEntry2->u2ProtoVlanId)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u2ProtoVlanId < pRemEntry2->u2ProtoVlanId)
    {
        return LLDP_RB_LESS;
    }
    else
    {
        return LLDP_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlRBCmpProtoIdInfo
 *
 *    DESCRIPTION      : RBTree compare function for RemProtoIdRBTree. This 
 *                       RB Tree is usefull for SNMP operations on Remote
 *                       Protocol Identity Info Table.
 *                       Index of this table -
 *                       1. u4RemLastUpdateTime
 *                       2. i4RemLocalPortNum
 *                       3. i4RemIndex
 *                       4. u4ProtocolIndex
 *
 *    INPUT            : e1 - Pointer to the RemoteNode node1
 *                       e2 - Pointer to the RemoteNode node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : LLDP_RB_EQUAL   - if all the keys matched for both 
 *                                         the nodes.
 *                       LLDP_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       LLDP_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
INT4
LldpRxUtlRBCmpProtoIdInfo (tRBElem * pElem1, tRBElem * pElem2)
{
    tLldpxdot1RemProtoIdInfo *pRemEntry1;
    tLldpxdot1RemProtoIdInfo *pRemEntry2;

    pRemEntry1 = (tLldpxdot1RemProtoIdInfo *) pElem1;
    pRemEntry2 = (tLldpxdot1RemProtoIdInfo *) pElem2;

    /* Compare the First Key */
    if (pRemEntry1->u4RemLastUpdateTime > pRemEntry2->u4RemLastUpdateTime)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4RemLastUpdateTime < pRemEntry2->u4RemLastUpdateTime)
    {
        return LLDP_RB_LESS;
    }

    /* First key is equal so compare the 2nd key */
    if (pRemEntry1->i4RemLocalPortNum > pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemLocalPortNum < pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_LESS;
    }
    /* First key is equal so compare the 2nd key */
    if (pRemEntry1->u4DestAddrTblIndex > pRemEntry2->u4DestAddrTblIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4DestAddrTblIndex < pRemEntry2->u4DestAddrTblIndex)
    {
        return LLDP_RB_LESS;
    }

    /* 2nd key is equal so compare the 3rd key */
    if (pRemEntry1->i4RemIndex > pRemEntry2->i4RemIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemIndex < pRemEntry2->i4RemIndex)
    {
        return LLDP_RB_LESS;
    }

    /* 3rd key is equal so compare the 4th key */
    if (pRemEntry1->u4ProtocolIndex > pRemEntry2->u4ProtocolIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4ProtocolIndex < pRemEntry2->u4ProtocolIndex)
    {
        return LLDP_RB_LESS;
    }
    else
    {
        return LLDP_RB_EQUAL;
    }
}

/*--------------------------------------------------------------------------*/
/*                       RB Tree free routines                              */
/*--------------------------------------------------------------------------*/

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlRBFreeSysData
 *
 *    DESCRIPTION      : Whenevr A Remote Node needs to be removed from the 
 *                       RemSysDataRBTree, this callback funcution is 
 *                       registered. This function takes care of freeing the
 *                       memory used by the Remote Node after removing that node
 *                       from the RBTree.
 *                       This function can be registered with -
 *                        o RBTreeDestroy()
 *                        o RBTreeDrain()
 *
 *                        NOTE: During Reinsertion of Remote Node this function 
 *                        is invoked by the RBTree. So memory should not be 
 *                        released here.
 *
 *    INPUT            : pElem - Pointer to the Remote Node whoes 
 *                               Memory needs to be freed.
 *                       u4Arg - Argument Registed while registering this 
 *                               callback function with RemSysDataRBTree.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpRxUtlRBFreeSysData (tRBElem * pElem, UINT4 u4Arg)
{
    /* This Free Fn is intentionally left blank */
    /* As this Remote Node is maintained in both the RBTrees -
     * RemSysDataRBTree and RemMSAPRBTree, so memory for this Remote Node 
     * cannot be removed here. Procedure to Release a Remote Node Memory is as
     * follows -
     * STEP 1: Detach the Remote Node from RemSysDataRBTree.
     * STEP 2: Detach the Remote Node from RemMSAPRBTree.
     * STEP 3: Release the memory of Remote Node.
     */
    UNUSED_PARAM (pElem);
    UNUSED_PARAM (u4Arg);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlRBFreeMSAP
 *
 *    DESCRIPTION      : Whenevr A Remote Node needs to be removed form the 
 *                       RemMSAPRBTree, this callback funcution is 
 *                       registered. This function takes care of freeing the
 *                       memory used by the Remote Node after removing that node
 *                       from the RBTree. If the Info ageout timer is running
 *                       for this remote node, then that timer is stoped before
 *                       free the memory of Remote Node.
 *                       This function can be registered with -
 *                        o RBTreeDestroy()
 *                        o RBTreeDrain()
 *
 *    INPUT            : pRBElem - Pointer to the Remote Node whoes Memory 
 *                                 needs to be freed.
 *                       u4Arg - Argument Registed while registering this 
 *                               callback function with RemMSAPRBTree.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpRxUtlRBFreeMSAP (tRBElem * pRBElem, UINT4 u4Arg)
{
    tLldpRemoteNode    *pRemoteNode = NULL;

    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        pRemoteNode = (tLldpRemoteNode *) pRBElem;

        LldpRxStopInfoAgeTmr (pRemoteNode);

        /* Release memory for associated tLldpxdot3RemPortInfo structure */
        if (pRemoteNode->pDot3RemPortInfo != NULL)
        {
            MemReleaseMemBlock (gLldpGlobalInfo.RemDot3PortInfoPoolId,
                                (UINT1 *) pRemoteNode->pDot3RemPortInfo);
            pRemoteNode->pDot3RemPortInfo = NULL;
        }

        /* Release the memory for Remote Node */
        MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId, (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlRBFreeManAddr
 *
 *    DESCRIPTION      :
 *
 *
 *
 *    INPUT            :
 *
 *    OUTPUT           :
 *
 *    RETURNS          :
 *
 ****************************************************************************/
INT4
LldpRxUtlRBFreeManAddr (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.RemManAddrPoolId,
                            (UINT1 *) pRBElem);
        pRBElem = NULL;

    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlRBFreeOrgDefInfo
 *
 *    DESCRIPTION      :
 *
 *
 *
 *    INPUT            :
 *
 *    OUTPUT           :
 *
 *    RETURNS          :
 *
 ****************************************************************************/
INT4
LldpRxUtlRBFreeOrgDefInfo (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.RemOrgDefInfoPoolId,
                            (UINT1 *) pRBElem);
        pRBElem = NULL;

    }
    return OSIX_SUCCESS;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlRBFreeUnknownTLV
 *
 *    DESCRIPTION      :
 *
 *
 *
 *    INPUT            :
 *
 *    OUTPUT           :
 *
 *    RETURNS          :
 *
 ****************************************************************************/
INT4
LldpRxUtlRBFreeUnknownTLV (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.RemUnknownTLVPoolId,
                            (UINT1 *) pRBElem);
        pRBElem = NULL;

    }
    return OSIX_SUCCESS;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlRBFreeVlanNameInfo
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the RemVlanNameInfoRBTree.
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpRxUtlRBFreeVlanNameInfo (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.RemVlanNameInfoPoolId,
                            (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlRBFreeProtoVlanInfo
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the RemProtoVlanRBTree
 *
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpRxUtlRBFreeProtoVlanInfo (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.RemProtoVlanPoolId,
                            (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlRBFreeProtoIdInfo
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the RemProtoIdRBTree 
 *
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpRxUtlRBFreeProtoIdInfo (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.RemProtocolPoolId,
                            (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlRBFreeLocationInfo 
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the LldpMedRemLocationInfoRBTree 
 *
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpRxUtlRBFreeLocationInfo (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.LldpMedRemLocationPoolId,
                            (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlRBFreeNwPolicyInfo 
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the LldpMedLocNwPolicyInfoRBTree 
 *
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpRxUtlRBFreeNwPolicyInfo (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.LldpMedRemNwPolicyPoolId,
                            (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/*--------------------------------------------------------------------------*/
/*                          Remote Node Related Utilities                   */
/*--------------------------------------------------------------------------*/

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlAddRemoteNode
 *
 *    DESCRIPTION      : This function Add the given Remote Node to the 
 *                       Remote System Data Table(i.e. RemSysDataRBTree) and
 *                       also to RemMSAPRBTree.
 *
 *    INPUT            : pRemoteNode - Pointer to the Remote Node that 
 *                                     needs to be Added
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
LldpRxUtlAddRemoteNode (tLldpRemoteNode * pRemoteNode)
{
    tLldpLocPortInfo   *pPortInfo = NULL;
    tLldpv2AgentToLocPort Lldpv2AgentToLocPort;
    tLldpv2AgentToLocPort *pLldpv2AgentToLocPort = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    /* Add the Remote Node to the RemSysDataRBTree */
    if (RBTreeAdd (gLldpGlobalInfo.RemSysDataRBTree, (tRBElem *) pRemoteNode)
        == RB_SUCCESS)
    {
        /* RBTreeAdd function call will fail only if an entry with same index
         * already exists in the RBTree. But the following RBTreeAdd will 
         * never fail, because if the entry with same index exists it would
         * have faild while adding(RBTreeAdd) to RemSysDataRBTree */
        if (RBTreeAdd (gLldpGlobalInfo.RemMSAPRBTree, (tRBElem *) pRemoteNode)
            == RB_FAILURE)
        {
            LLDP_TRC (ALL_FAILURE_TRC, "LldpRxUtlAddRemoteNode:"
                      "RBTreeAdd\r\n");
            RBTreeRem (gLldpGlobalInfo.RemSysDataRBTree,
                       (tRBElem *) pRemoteNode);
            return OSIX_FAILURE;
        }
        i4RetVal = OSIX_SUCCESS;
    }

    /* increment the no of neighbours count only for LLDP packet */
    MEMSET (&Lldpv2AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));

    Lldpv2AgentToLocPort.i4IfIndex = pRemoteNode->i4RemLocalPortNum;
    Lldpv2AgentToLocPort.u4DstMacAddrTblIndex = pRemoteNode->u4DestAddrTblIndex;
    pLldpv2AgentToLocPort =
        (tLldpv2AgentToLocPort *) RBTreeGet (gLldpGlobalInfo.
                                             Lldpv2AgentMapTblRBTree,
                                             &Lldpv2AgentToLocPort);

    if ((pLldpv2AgentToLocPort != NULL) &&
        (MEMCMP (pLldpv2AgentToLocPort->Lldpv2DestMacAddress,
                 gau1LldpMcastAddr, MAC_ADDR_LEN)) == 0)
    {
        /* Update NoOfNeighbours count */
        pPortInfo =
            LLDP_GET_LOC_PORT_INFO (pLldpv2AgentToLocPort->u4LldpLocPort);
        if (pPortInfo != NULL)
        {
            pPortInfo->u2NoOfNeighbors++;
        }
    }

    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlDelRemoteNode
 *
 *    DESCRIPTION      : This function removes all the information associated 
 *                       with a particular neighbor. The steps are
 *                       followeed in the same sequence mention below -
 *                       1. Remove the associated nodes from RemManAddrRBTree
 *                       2. Remove the associated nodes from RemOrgDefInfoRBTree
 *                       3. Remove the associated nodes from RemUnknownTLVRBTree
 *                       4. Remove the associated nodes from 
 *                          RemVlanNameInfoRBTree
 *                       5. Remove the associated nodes from RemProtoVlanRBTree
 *                       6. Remove the associated nodes from RemProtoIdRBTree
 *                       7. Remove the LLDP-MED Network Policy Info
 *                       8. Release memory for associated tLldpxdot3RemPortInfo 
 *                          structure
 *                       9. Remove the Remote Node from RemSysDataRBTree.
 *                       10. Remove the Remote Node from RemMSAPRBTree. 
 *
 *    INPUT            : pRemoteNode - Pointer to the Remote Node that 
 *                                     needs to be deleted
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
VOID
LldpRxUtlDelRemoteNode (tLldpRemoteNode * pRemoteNode)
{
    tLldpLocPortInfo   *pPortInfo = NULL;
    tLldpv2AgentToLocPort Lldpv2AgentToLocPort;
    tLldpv2AgentToLocPort *pLldpv2AgentToLocPort = NULL;

    /* 1. Remove the associated nodes from RemManAddrRBTree */
    LldpRxUtlDelRemManAddr (pRemoteNode);
    /* 2. Remove the associated nodes from RemOrgDefInfoRBTree */
    LldpRxUtlDelRemOrgDefInfo (pRemoteNode);
    /* 3. Remove the associated nodes from RemUnknownTLVRBTree */
    LldpRxUtlDelRemUnknownTLV (pRemoteNode);
    /* 4. Remove the associated nodes from RemVlanNameInfoRBTree */
    LldpRxUtlDelRemVlanNameInfo (pRemoteNode);
    /* 5. Remove the associated nodes from RemProtoVlanRBTree */
    LldpRxUtlDelRemProtoVlan (pRemoteNode);
    /* 6. Remove the associated nodes from RemProtoIdRBTree */
    LldpRxUtlDelRemProtoId (pRemoteNode);
    /* 7. Remove the LLDP-MED Network Policy Info */
    LldpMedRxUtlDelRemPolicyInfo (pRemoteNode);
    /* 8. Remove the LLDP-MED Location Info */
    LldpMedRxUtlDelRemLocationInfo (pRemoteNode);
    /* 9. Release memory for associated tLldpxdot3RemPortInfo structure */
    if (pRemoteNode->pDot3RemPortInfo != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.RemDot3PortInfoPoolId,
                            (UINT1 *) pRemoteNode->pDot3RemPortInfo);
        pRemoteNode->pDot3RemPortInfo = NULL;
    }

    /* 10. Remove the Remote Node from RemSysDataRBTree. */
    RBTreeRem (gLldpGlobalInfo.RemSysDataRBTree, (tRBElem *) pRemoteNode);
    /* 11. Remove the Remote Node from RemMSAPRBTree. */
    RBTreeRem (gLldpGlobalInfo.RemMSAPRBTree, (tRBElem *) pRemoteNode);

    /* Stop the info age timer and Release the Memory for Remote Node */
    LldpRxUtlRBFreeMSAP ((tRBElem *) pRemoteNode, 0);

    /* Decrement the no of Neighbor count */
    MEMSET (&Lldpv2AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));

    Lldpv2AgentToLocPort.i4IfIndex = pRemoteNode->i4RemLocalPortNum;
    Lldpv2AgentToLocPort.u4DstMacAddrTblIndex = pRemoteNode->u4DestAddrTblIndex;
    pLldpv2AgentToLocPort =
        (tLldpv2AgentToLocPort *) RBTreeGet (gLldpGlobalInfo.
                                             Lldpv2AgentMapTblRBTree,
                                             &Lldpv2AgentToLocPort);

    if ((pLldpv2AgentToLocPort != NULL) &&
        (MEMCMP (pLldpv2AgentToLocPort->Lldpv2DestMacAddress,
                 gau1LldpMcastAddr, MAC_ADDR_LEN)) == 0)
    {
        /* Update NoOfNeighbours count */
        pPortInfo =
            LLDP_GET_LOC_PORT_INFO (pLldpv2AgentToLocPort->u4LldpLocPort);
        if ((pPortInfo != NULL) && (pPortInfo->u2NoOfNeighbors != 0))
        {
            pPortInfo->u2NoOfNeighbors--;
        }
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlDelRemManAddr
 *
 *    DESCRIPTION      : This function deletes all the Management Addr Nodes
 *                       from the RemManAddrRBTree associated with 
 *                       the given neighbour.
 *                       During the removal of RBNodes from the RBTree,
 *                       RBNodeFree Function is called which is
 *                       responsible for releasing the memory allocated to 
 *                       that node. 
 *
 *    INPUT            : pRemoteNode - pointer to the Remote Node for this 
 *                                     neighbor.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS/FAILURE
 *
 ****************************************************************************/
INT4
LldpRxUtlDelRemManAddr (tLldpRemoteNode * pRemoteNode)
{
    tLldpRemManAddressTable *pFirstManAddrNode = NULL;
    tLldpRemManAddressTable *pNextManAddrNode = NULL;

    /* Get the First Man Addr Node for this Neighbor */
    pFirstManAddrNode = pRemoteNode->pRemManAddr;
    if (pFirstManAddrNode == NULL)
    {
        /* No Entry Present for this Neighbor */
        return OSIX_SUCCESS;
    }

    while (LldpRxUtlGetNextManAddr (pFirstManAddrNode, &pNextManAddrNode)
           == OSIX_SUCCESS)
    {
        if (LLDP_IS_NODE_FOR_NEIGHBOR (pFirstManAddrNode, pNextManAddrNode)
            == OSIX_TRUE)
        {
            RBTreeRem (gLldpGlobalInfo.RemManAddrRBTree,
                       (tRBElem *) pNextManAddrNode);
            LldpRxUtlRBFreeManAddr ((tRBElem *) pNextManAddrNode, 0);
        }
        else
        {
            break;
        }
    }

    /* Release the First Node */
    RBTreeRem (gLldpGlobalInfo.RemManAddrRBTree, (tRBElem *) pFirstManAddrNode);
    LldpRxUtlRBFreeManAddr ((tRBElem *) pFirstManAddrNode, 0);

    /* Reset the Link in Remote Node */
    pRemoteNode->pRemManAddr = NULL;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlDelRemOrgDefInfo
 *
 *    DESCRIPTION      : This function deletes all the Organizationally Define
 *                       Info Nodes from the RemOrgDefInfoRBTree associated 
 *                       with the given neighbour.
 *                       During the removal of RBNodes from the RBTree,
 *                       RBNodeFree Function is called which is
 *                       responsible for releasing the memory allocated to 
 *                       that node. 
 *
 *    INPUT            : pRemoteNode - pointer to the Remote Node for this 
 *                                     given neighbor.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS/FAILURE
 *
 ****************************************************************************/
INT4
LldpRxUtlDelRemOrgDefInfo (tLldpRemoteNode * pRemoteNode)
{
    tLldpRemOrgDefInfoTable *pFirstOrgDefInfoNode = NULL;
    tLldpRemOrgDefInfoTable *pNextOrgDefInfoNode = NULL;

    /* Get the First Man Addr Node for this Neighbor */
    pFirstOrgDefInfoNode = pRemoteNode->pRemOrgDefInfo;
    if (pFirstOrgDefInfoNode == NULL)
    {
        /* No Entry Present for this Neighbor */
        return OSIX_SUCCESS;
    }

    while (LldpRxUtlGetNextOrgDefInfo (pFirstOrgDefInfoNode,
                                       &pNextOrgDefInfoNode) == OSIX_SUCCESS)
    {
        if (LLDP_IS_NODE_FOR_NEIGHBOR (pFirstOrgDefInfoNode,
                                       pNextOrgDefInfoNode) == OSIX_TRUE)
        {
            RBTreeRem (gLldpGlobalInfo.RemOrgDefInfoRBTree,
                       (tRBElem *) pNextOrgDefInfoNode);
            LldpRxUtlRBFreeOrgDefInfo ((tRBElem *) pNextOrgDefInfoNode, 0);
        }
        else
        {
            break;
        }
    }

    /* Release the First Node */
    RBTreeRem (gLldpGlobalInfo.RemOrgDefInfoRBTree,
               (tRBElem *) pFirstOrgDefInfoNode);
    LldpRxUtlRBFreeOrgDefInfo ((tRBElem *) pFirstOrgDefInfoNode, 0);

    /* Reset the Link in Remote Node */
    pRemoteNode->pRemOrgDefInfo = NULL;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlDelRemUnknownTLV
 *
 *    DESCRIPTION      : This function deletes all the Unknown TLV
 *                       Info Nodes from the RemUnknownTLVRBTree associated 
 *                       with the given neighbour.
 *                       During the removal of RBNodes from the RBTree,
 *                       RBNodeFree Function is called which is
 *                       responsible for releasing the memory allocated to 
 *                       that node. 
 *
 *    INPUT            : pRemoteNode - pointer to the Remote Node for this 
 *                                     given neighbor.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS/FAILURE
 *
 ****************************************************************************/
INT4
LldpRxUtlDelRemUnknownTLV (tLldpRemoteNode * pRemoteNode)
{
    tLldpRemUnknownTLVTable *pFirstUnknownTlvNode = NULL;
    tLldpRemUnknownTLVTable *pNextUnknownTlvNode = NULL;

    /* Get the First Man Addr Node for this Neighbor */
    pFirstUnknownTlvNode = pRemoteNode->pRemUnknownTLV;
    if (pFirstUnknownTlvNode == NULL)
    {
        /* No Entry Present for this Neighbor */
        return OSIX_SUCCESS;
    }

    while (LldpRxUtlGetNextUnknownTLV (pFirstUnknownTlvNode,
                                       &pNextUnknownTlvNode) == OSIX_SUCCESS)
    {
        if (LLDP_IS_NODE_FOR_NEIGHBOR (pFirstUnknownTlvNode,
                                       pNextUnknownTlvNode) == OSIX_TRUE)
        {
            RBTreeRem (gLldpGlobalInfo.RemUnknownTLVRBTree,
                       (tRBElem *) pNextUnknownTlvNode);
            LldpRxUtlRBFreeUnknownTLV ((tRBElem *) pNextUnknownTlvNode, 0);
        }
        else
        {
            break;
        }
    }

    /* Release the First Node */
    RBTreeRem (gLldpGlobalInfo.RemUnknownTLVRBTree,
               (tRBElem *) pFirstUnknownTlvNode);
    LldpRxUtlRBFreeUnknownTLV ((tRBElem *) pFirstUnknownTlvNode, 0);

    /* Reset the Link in Remote Node */
    pRemoteNode->pRemUnknownTLV = NULL;

    return OSIX_SUCCESS;
}

/*--------------------------------------------------------------------------*/
/*                       Get Next Routines for the Remote Tables            */
/*--------------------------------------------------------------------------*/
/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlGetNextRemoteNode
 *
 *    DESCRIPTION      : Get the Next Remote Node.
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : Pointer to the Next Remote Node, or NULL
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
LldpRxUtlGetNextRemoteNode (tLldpRemoteNode * pCurrRemoteNode,
                            tLldpRemoteNode ** ppNextRemoteNode)
{
    *ppNextRemoteNode = (tLldpRemoteNode *) RBTreeGetNext
        (gLldpGlobalInfo.RemSysDataRBTree, (tRBElem *) pCurrRemoteNode, NULL);

    if (*ppNextRemoteNode != NULL)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlGetNextMSAPRemoteNode
 *
 *    DESCRIPTION      : Get the Next Remote Node of RemMSAPRBTree.
 *
 *    INPUT            : pCurrRemoteNode   - Current remote node of the
 *                                           RemMSAPRBTree.
 *                       ppNextRemoteNode  - Next remote node of the 
 *                                           given current node.
 *
 *    OUTPUT           : Pointer to the Next Remote Node, or NULL
 *
 *    RETURNS          : if the next remote node is not null
 *                       returns : OSIX_SUCCESS
 *                       else 
 *                       returns : OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
LldpRxUtlGetNextMSAPRemoteNode (tLldpRemoteNode * pCurrRemoteNode,
                                tLldpRemoteNode ** ppNextRemoteNode)
{
    *ppNextRemoteNode = (tLldpRemoteNode *) RBTreeGetNext
        (gLldpGlobalInfo.RemMSAPRBTree, (tRBElem *) pCurrRemoteNode, NULL);

    if (*ppNextRemoteNode != NULL)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlGetNextProtoId
 *
 *    DESCRIPTION      : Gets the Next Protocol Id Entry for this
 *                       particular Neighbor from RemProtoIdRBTree.
 *
 *
 *    INPUT            : pCurProtoIdNode  - Current Remote Protocol Id Node,
 *                                          whose next Node needs to be
 *                                          returned.
 *
 *    OUTPUT           : ppNextProtoIdNode - Pointer To the Next Protocol Id 
 *                                          Node, or NULL
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpRxUtlGetNextProtoId (tLldpxdot1RemProtoIdInfo * pCurProtoIdNode,
                         tLldpxdot1RemProtoIdInfo ** ppNextProtoIdNode)
{
    INT4                i4RetValue = OSIX_FAILURE;

    /* Get the Next Node from RemProtoIdRBTree. */
    *ppNextProtoIdNode = RBTreeGetNext (gLldpGlobalInfo.RemVlanNameInfoRBTree,
                                        (tRBElem *) pCurProtoIdNode, NULL);

    /* Verify this Protocol Id Node is for the current Neighbor */
    if (*ppNextProtoIdNode != NULL)
    {
        i4RetValue = OSIX_SUCCESS;
    }
    return i4RetValue;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlGetNextOrgDefInfo
 *
 *    DESCRIPTION      : Get the Next Org Define Info Entry for this 
 *                       particular Neighbor from RemOrgDefInfoRBTree.
 *
 *
 *    INPUT            : pRemOrgDefInfoNode - Current Rem  Org Define Info
 *                                            Node, whoes Next Node Need 
 *                                            to be returned.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : Pointer To the Next Org Define Info Node, or NULL
 *
 ****************************************************************************/
INT4
LldpRxUtlGetNextOrgDefInfo (tLldpRemOrgDefInfoTable * pRemOrgDefInfoNode,
                            tLldpRemOrgDefInfoTable ** ppNextOrgDefInfoNode)
{
    /* RBTree GetNext */
    *ppNextOrgDefInfoNode = RBTreeGetNext (gLldpGlobalInfo.RemOrgDefInfoRBTree,
                                           (tRBElem *) pRemOrgDefInfoNode,
                                           NULL);

    if (*ppNextOrgDefInfoNode != NULL)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlGetNextVlanName
 *
 *    DESCRIPTION      : Gets the Next Vlan Name Entry for this
 *                       particular Neighbor from RemVlanNameInfoRBTree.
 *
 *
 *    INPUT            : pCurVlanNameNode  -  Current Rem Vlan Name
 *                                            Node, whose next Node needs
 *                                            to be returned.
 *
 *    OUTPUT           : ppNextVlanNameNode - Pointer To the Next Vlan 
 *                                           Name Node, or NULL
 *
 *    RETURNS          :OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpRxUtlGetNextVlanName (tLldpxdot1RemVlanNameInfo * pCurVlanNameNode,
                          tLldpxdot1RemVlanNameInfo ** ppNextVlanNameNode)
{
    INT4                i4RetValue = OSIX_FAILURE;

    /* Get the Next Node from RemVlanNameInfoRBTree. */
    *ppNextVlanNameNode = RBTreeGetNext (gLldpGlobalInfo.RemVlanNameInfoRBTree,
                                         (tRBElem *) pCurVlanNameNode, NULL);

    if (*ppNextVlanNameNode != NULL)
    {
        i4RetValue = OSIX_SUCCESS;
    }
    return i4RetValue;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlGetNextProtoVlan
 *
 *    DESCRIPTION      : Gets the Next Protocol Vlan Entry for this
 *                       particular Neighbor from RemProtoVlanRBTree.
 *
 *
 *    INPUT            : pCurProtoVlanNode -  Current Rem  Protocol Vlan
 *                                            Node, whoes next Node needs
 *                                            to be returned.
 *
 *    OUTPUT           : ppNextProtoVlanNode -  Pointer To the Next Protocol
 *                                             Vlan Info Node, or NULL
 *
 *    RETURNS          :OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpRxUtlGetNextProtoVlan (tLldpxdot1RemProtoVlanInfo * pCurProtoVlanNode,
                           tLldpxdot1RemProtoVlanInfo ** ppNextProtoVlanNode)
{
    INT4                i4RetValue = OSIX_FAILURE;

    /* Get the Next Node from RemProtoVlanRBTree. */
    *ppNextProtoVlanNode = RBTreeGetNext (gLldpGlobalInfo.RemProtoVlanRBTree,
                                          (tRBElem *) pCurProtoVlanNode, NULL);

    if (*ppNextProtoVlanNode != NULL)
    {
        i4RetValue = OSIX_SUCCESS;
    }
    return i4RetValue;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlGetNextUnknownTLV
 *
 *    DESCRIPTION      : Get the Next Unknown TLV Entry for this 
 *                       particular Neighbor from RemUnknownTLVRBTree.
 *
 *
 *    INPUT            : pRemUnknownTLVNode - Current Rem UnknownTLV Node, 
 *                                         whoes Next Node Need to be returned
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : Pointer To the Next UnknownTLV Addr Node, or NULL
 *
 ****************************************************************************/
INT4
LldpRxUtlGetNextUnknownTLV (tLldpRemUnknownTLVTable * pRemUnknownTLVNode,
                            tLldpRemUnknownTLVTable ** ppNextRemUnknownTLVNode)
{
    /* RBTree GetNext */
    *ppNextRemUnknownTLVNode =
        RBTreeGetNext (gLldpGlobalInfo.RemUnknownTLVRBTree,
                       (tRBElem *) pRemUnknownTLVNode, NULL);

    if (*ppNextRemUnknownTLVNode != NULL)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlGetNextManAddr
 *
 *    DESCRIPTION      : Get the Next Management Address Entry for this 
 *                       particular Neighbor from RemManAddrRBTree.
 *
 *
 *    INPUT            : pCurrManAddrNode - Current ManAddr Node, whoes Next
 *                                         Node Need to be returned
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : Pointer To the Next Mnagement Addr Node, or NULL if 
 *                       End of ManAddr for that neighbor.
 *
 ****************************************************************************/
INT4
LldpRxUtlGetNextManAddr (tLldpRemManAddressTable * pCurrManAddrNode,
                         tLldpRemManAddressTable ** ppNextManAddrNode)
{
    /* Get the Next Node from RemManAddrRBTree */
    *ppNextManAddrNode = RBTreeGetNext (gLldpGlobalInfo.RemManAddrRBTree,
                                        (tRBElem *) pCurrManAddrNode, NULL);

    if (*ppNextManAddrNode != NULL)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/*--------------------------------------------------------------------------*/
/*            Get First and Walk  Routines for the Remote Tables            */
/*--------------------------------------------------------------------------*/
/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlWalkFnRemNodeForPort
 *
 *    DESCRIPTION      : Walk Action routine for the RemMSAPRBTree. This 
 *                       function is Walks the RemMSAPRBTree and return the
 *                       First  Remote Node Entry learnt through a particular
 *                       port.
 *
 *    INPUT            : pRBElem -  pointer to the current Node
 *                       visit   - indicates the order of the tree traversal.
 *                       u4Level - indicates the present level of the tree
 *                       pArg - Argument passed by the user function
 *                       pOut - Output that needs to be indicated to the user
 *                              function.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : Pointer to the Management Addr Node, or
 *                       NULL
 *
 ****************************************************************************/
INT4
LldpRxUtlWalkFnRemNodeForPort (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                               void *pArg, void *pOut)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT4               u4RecvdPortNum = 0;

    UNUSED_PARAM (u4Level);
    u4RecvdPortNum = *(UINT4 *) pArg;

    if (visit == postorder || visit == leaf)
    {
        if (pRBElem != NULL)
        {
            pRemoteNode = (tLldpRemoteNode *) pRBElem;

            if (pRemoteNode->i4RemLocalPortNum == (INT4) u4RecvdPortNum)
            {
                /* This is the First Node learnt on the port u4RecvdPortNum */
                *(tLldpRemoteNode **) pOut = pRemoteNode;
                return RB_WALK_BREAK;
            }
        }
    }
    return RB_WALK_CONT;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlWalkFnManAddr
 *
 *    DESCRIPTION      : Walk Action routine for the RemManAddrRBTree walk 
 *                       Function. This function is registered with the 
 *                       RemManAddrRBTree to Get the First Management Address 
 *                       Entry for a particular Neighbor.
 *
 *    INPUT            : pRBElem -  pointer to the current Node
 *                       visit   - indicates the order of the tree traversal.
 *                       u4Level - indicates the present level of the tree
 *                       pArg - Argument passed by the user function
 *                       pOut - Output that needs to be indicated to the user
 *                              function.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : Pointer to the Management Addr Node, or
 *                       NULL
 *
 ****************************************************************************/
INT4
LldpRxUtlWalkFnManAddr (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                        void *pArg, void *pOut)
{
    tLldpRemManAddressTable *pRemManAddrNode = NULL;
    tLldpRemoteNode    *pRemoteNode = NULL;

    UNUSED_PARAM (u4Level);

    /* Pre-Order is usefull for get the root of a particular branch */
    if (visit == postorder || visit == leaf)
    {
        if (pRBElem != NULL)
        {
            pRemManAddrNode = (tLldpRemManAddressTable *) pRBElem;
            pRemoteNode = (tLldpRemoteNode *) pArg;

            if (LLDP_IS_NODE_FOR_NEIGHBOR (pRemoteNode, pRemManAddrNode)
                == OSIX_TRUE)
            {
                *(tLldpRemManAddressTable **) pOut = pRemManAddrNode;
                return RB_WALK_BREAK;
            }
        }
    }
    return RB_WALK_CONT;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlGetFirstManAddr
 *
 *    DESCRIPTION      : Get the FIrst Management Address Entry for a 
 *                       particular Neighbor from RemManAddrRBTree.
 *
 *
 *    INPUT            : pRemoteNode - Remote Node Pointer
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : Pointer to the Management Addr Node, or
 *                       NULL
 *
 ****************************************************************************/
tLldpRemManAddressTable *
LldpRxUtlGetFirstManAddr (tLldpRemoteNode * pRemoteNode)
{
    tLldpRemManAddressTable *pManAddrNode = NULL;

    /* Walk the RemManAddrRBTree */
    RBTreeWalk (gLldpGlobalInfo.RemManAddrRBTree,
                (tRBWalkFn) LldpRxUtlWalkFnManAddr, pRemoteNode, &pManAddrNode);

    /* NOTE: In case entry not present, NULL will be returned */
    return pManAddrNode;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlWalkFnUnknownTlv
 *
 *    DESCRIPTION      : Walk Action routine for the RemUnknownTLVRBTree walk 
 *                       Function. This function is registered with the 
 *                       RemUnknownTLVRBTree to Get the FIrst Unknown TLV Info 
 *                       Entry for a particular Neighbor.
 *
 *    INPUT            : pRBElem -  pointer to the current Node
 *                       visit   - indicates the order of the tree traversal.
 *                       u4Level - indicates the present level of the tree
 *                       pArg - Argument passed by the user function
 *                       pOut - Output that needs to be indicated to the user
 *                              function.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpRxUtlWalkFnUnknownTlv (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                           void *pArg, void *pOut)
{
    tLldpRemUnknownTLVTable *pRemUnknownTlvNode = NULL;
    tLldpRemoteNode    *pRemoteNode = NULL;

    UNUSED_PARAM (u4Level);

    if (visit == postorder || visit == leaf)
    {
        if (pRBElem != NULL)
        {
            pRemUnknownTlvNode = (tLldpRemUnknownTLVTable *) pRBElem;
            pRemoteNode = (tLldpRemoteNode *) pArg;

            if (LLDP_IS_NODE_FOR_NEIGHBOR (pRemoteNode, pRemUnknownTlvNode)
                == OSIX_TRUE)
            {
                *(tLldpRemUnknownTLVTable **) pOut = pRemUnknownTlvNode;
                return RB_WALK_BREAK;
            }
        }
    }
    return RB_WALK_CONT;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlGetFirstUnknownTLV
 *
 *    DESCRIPTION      : Get the FIrst Unknown TLV Entry for a 
 *                       particular Neighbor from RemUnknownTLVRBTree.
 *
 *
 *    INPUT            : pRemoteNode - Remote Node Pointer
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : Pointer to the UnknownTLV Node, or
 *                       NULL
 *
 ****************************************************************************/
tLldpRemUnknownTLVTable *
LldpRxUtlGetFirstUnknownTLV (tLldpRemoteNode * pRemoteNode)
{
    tLldpRemUnknownTLVTable *pUnknownTlvNode = NULL;

    /* Walk the RemUnknownTLVRBTree */
    RBTreeWalk (gLldpGlobalInfo.RemUnknownTLVRBTree,
                (tRBWalkFn) LldpRxUtlWalkFnUnknownTlv, pRemoteNode,
                &pUnknownTlvNode);

    /* NOTE: In case entry not present, NULL will be returned */
    return (tLldpRemUnknownTLVTable *) pUnknownTlvNode;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlWalkFnOrgDefInfo
 *
 *    DESCRIPTION      : Walk Action routine for the RemOrgDefInfoRBTree walk 
 *                       Function. This function is registered with the 
 *                       RemOrgDefInfoRBTree to Get the First Organisationally
 *                       Defined TLV Info entry for a particular Neighbor.
 *
 *    INPUT            : pRBElem -  pointer to the current Node
 *                       visit   - indicates the order of the tree traversal.
 *                       u4Level - indicates the present level of the tree
 *                       pArg - Argument passed by the user function
 *                       pOut - Output that needs to be indicated to the user
 *                              function.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpRxUtlWalkFnOrgDefInfo (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                           void *pArg, void *pOut)
{
    tLldpRemOrgDefInfoTable *pRemOrgDefInfoNode = NULL;
    tLldpRemoteNode    *pRemoteNode = NULL;

    UNUSED_PARAM (u4Level);

    if (visit == postorder || visit == leaf)
    {
        if (pRBElem != NULL)
        {
            pRemOrgDefInfoNode = (tLldpRemOrgDefInfoTable *) pRBElem;
            pRemoteNode = (tLldpRemoteNode *) pArg;

            if (LLDP_IS_NODE_FOR_NEIGHBOR (pRemoteNode, pRemOrgDefInfoNode)
                == OSIX_TRUE)
            {
                *(tLldpRemOrgDefInfoTable **) pOut = pRemOrgDefInfoNode;
                return RB_WALK_BREAK;
            }
        }
    }
    return RB_WALK_CONT;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlGetFirstOrgDefInfo
 *
 *    DESCRIPTION      : Get the FIrst Org Define Info Entry for a 
 *                       particular Neighbor from RemOrgDefInfoRBTree.
 *
 *
 *    INPUT            : pRemoteNode - Remote Node Pointer
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : Pointer to the Org Define Info Node, or
 *                       NULL
 *
 ****************************************************************************/
tLldpRemOrgDefInfoTable *
LldpRxUtlGetFirstOrgDefInfo (tLldpRemoteNode * pRemoteNode)
{
    tLldpRemOrgDefInfoTable *pRemOrgDefInfoNode = NULL;
    /* Walk the RemOrgDefInfoRBTree */
    RBTreeWalk (gLldpGlobalInfo.RemOrgDefInfoRBTree,
                (tRBWalkFn) LldpRxUtlWalkFnOrgDefInfo, pRemoteNode,
                &pRemOrgDefInfoNode);

    /* NOTE: In case entry not present, NULL will be returned */
    return pRemOrgDefInfoNode;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlWalkFnProtoVlan
 *
 *    DESCRIPTION      : Walk Action routine for the RemProtoVlanRBTree walk
 *                       Function. This function is registered with the
 *                       RemProtoVlanRBTree to get the First Protocol Vlan
 *                       Entry for a particular Neighbor.
 *
 *    INPUT            : pRBElem -  pointer to the current Node
 *                       visit   - indicates the order of the tree traversal.
 *                       u4Level - indicates the present level of the tree
 *                       pArg    - Argument passed by the user function
 *
 *
 *
 *    OUTPUT           : pOut    - Output that needs to be indicated to the
 *                                 user function (Pointer to the Protocol Vlan
 *                                 Node, or NULL)
 *
 *    RETURNS          : RB_WALK_BREAK / RB_WALK_CONT
 *
 *
 ****************************************************************************/
INT4
LldpRxUtlWalkFnProtoVlan (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                          void *pArg, void *pOut)
{
    tLldpxdot1RemProtoVlanInfo *pRemProtoVlanNode = NULL;
    tLldpRemoteNode    *pRemoteNode = NULL;

    UNUSED_PARAM (u4Level);

    if (visit == postorder || visit == leaf)
    {
        if (pRBElem != NULL)
        {
            pRemProtoVlanNode = (tLldpxdot1RemProtoVlanInfo *) pRBElem;
            pRemoteNode = (tLldpRemoteNode *) pArg;

            if (LLDP_IS_NODE_FOR_NEIGHBOR (pRemoteNode, pRemProtoVlanNode)
                == OSIX_TRUE)
            {
                *(tLldpxdot1RemProtoVlanInfo **) pOut = pRemProtoVlanNode;
                return RB_WALK_BREAK;
            }
        }
    }
    return RB_WALK_CONT;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlGetFirstProtoVlan
 *
 *    DESCRIPTION      : Gets the First Protocol Vlan entry for a
 *                       particular Neighbor from RemProtoVlanRBTree.
 *
 *
 *    INPUT            : pRemoteNode - Remote Node Pointer
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : Pointer to the Protocol Vlan Info Node, or
 *                       NULL
 *
 ****************************************************************************/
tLldpxdot1RemProtoVlanInfo *
LldpRxUtlGetFirstProtoVlan (tLldpRemoteNode * pRemoteNode)
{
    tLldpxdot1RemProtoVlanInfo *pProtoVlanNode = NULL;

    /* Walk the RemProtoVlanRBTree */
    RBTreeWalk (gLldpGlobalInfo.RemProtoVlanRBTree,
                (tRBWalkFn) LldpRxUtlWalkFnProtoVlan, pRemoteNode,
                &pProtoVlanNode);

    /* In case entry not present, NULL will be returned */
    return pProtoVlanNode;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlWalkFnVlanName
 *
 *    DESCRIPTION      : Walk Action routine for the RemVlanNameInfoRBTree walk
 *                       Function. This function is registered with the
 *                       RemVlanNameInfoRBTree to get the First Vlan Name
 *                       Entry for a particular Neighbor.
 *
 *    INPUT            : pRBElem -  pointer to the current Node
 *                       visit   - indicates the order of the tree traversal.
 *                       u4Level - indicates the present level of the tree
 *                       pArg    - Argument passed by the user function
 *
 *    OUTPUT           : pOut    - Output that needs to be indicated to the
 *                                 user function (Pointer to the Vlan Name Node
 *                                 or NULL)
 *
 *    RETURNS          : RB_WALK_BREAK / RB_WALK_CONT
 *
 ****************************************************************************/
INT4
LldpRxUtlWalkFnVlanName (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                         void *pArg, void *pOut)
{
    tLldpxdot1RemVlanNameInfo *pRemVlanNameNode = NULL;
    tLldpRemoteNode    *pRemoteNode = NULL;

    UNUSED_PARAM (u4Level);

    if (visit == postorder || visit == leaf)
    {
        if (pRBElem != NULL)
        {
            pRemVlanNameNode = (tLldpxdot1RemVlanNameInfo *) pRBElem;
            pRemoteNode = (tLldpRemoteNode *) pArg;

            if (LLDP_IS_NODE_FOR_NEIGHBOR (pRemoteNode, pRemVlanNameNode)
                == OSIX_TRUE)
            {
                *(tLldpxdot1RemVlanNameInfo **) pOut = pRemVlanNameNode;
                return RB_WALK_BREAK;
            }
        }
    }
    return RB_WALK_CONT;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlGetFirstVlanName
 *
 *    DESCRIPTION      : Gets the First Vlan Name entry for a
 *                       particular Neighbor from RemVlanNameInfoRBTree.
 *
 *
 *    INPUT            : pRemoteNode - Remote Node Pointer
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : Pointer to the Vlan Name Node, or
 *                       NULL
 *
 ****************************************************************************/
tLldpxdot1RemVlanNameInfo *
LldpRxUtlGetFirstVlanName (tLldpRemoteNode * pRemoteNode)
{
    tLldpxdot1RemVlanNameInfo *pVlanNameNode = NULL;

    /* Walk the RemVlanNameInfoRBTree */
    RBTreeWalk (gLldpGlobalInfo.RemVlanNameInfoRBTree,
                (tRBWalkFn) LldpRxUtlWalkFnVlanName, pRemoteNode,
                &pVlanNameNode);

    /* In case entry not present, NULL will be returned */
    return pVlanNameNode;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlWalkFnProtocolId
 *
 *    DESCRIPTION      : Walk Action routine for the RemProtoIdRBTree walk
 *                       Function. This function is registered with the
 *                       RemProtoIdRBTree to get the First Protocol Id
 *                       Entry for a particular Neighbor.
 *
 *    INPUT            : pRBElem -  pointer to the current Node
 *                       visit   - indicates the order of the tree traversal.
 *                       u4Level - indicates the present level of the tree
 *                       pArg    - Argument passed by the user function
 *
 *    OUTPUT           : pOut    - Output that needs to be indicated to the
 *                                 user function (Pointer to the Protocol Id
 *                                 Node, or NULL)
 *
 *    RETURNS          : RB_WALK_BREAK / RB_WALK_CONT
 *
 ****************************************************************************/
INT4
LldpRxUtlWalkFnProtocolId (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                           void *pArg, void *pOut)
{
    tLldpxdot1RemProtoIdInfo *pRemProtoIdNode = NULL;
    tLldpRemoteNode    *pRemoteNode = NULL;

    UNUSED_PARAM (u4Level);

    if (visit == postorder || visit == leaf)
    {
        if (pRBElem != NULL)
        {
            pRemProtoIdNode = (tLldpxdot1RemProtoIdInfo *) pRBElem;
            pRemoteNode = (tLldpRemoteNode *) pArg;

            if (LLDP_IS_NODE_FOR_NEIGHBOR (pRemoteNode, pRemProtoIdNode)
                == OSIX_TRUE)
            {
                *(tLldpxdot1RemProtoIdInfo **) pOut = pRemProtoIdNode;
                return RB_WALK_BREAK;
            }
        }
    }
    return RB_WALK_CONT;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlGetFirstProtoId
 *
 *    DESCRIPTION      : Gets the First Protocol Id entry for a
 *                       particular Neighbor from RemProtoIdRBTree.
 *
 *
 *    INPUT            : pRemoteNode - Remote Node Pointer
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : Pointer to the Protocol Id Node, or
 *                       NULL
 *
 ****************************************************************************/
tLldpxdot1RemProtoIdInfo *
LldpRxUtlGetFirstProtoId (tLldpRemoteNode * pRemoteNode)
{
    tLldpxdot1RemProtoIdInfo *pProtoIdNode = NULL;

    /* Walk the RemProtoVlanRBTree */
    RBTreeWalk (gLldpGlobalInfo.RemProtoIdRBTree,
                (tRBWalkFn) LldpRxUtlWalkFnProtocolId, pRemoteNode,
                &pProtoIdNode);

    /* In case entry not present, NULL will be returned */
    return pProtoIdNode;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlWalkFnRemTable
 *
 *    DESCRIPTION      : Walk Action routine  to get the First Neighbor Node 
 *                       with Dot3 Information.
 *
 *    INPUT            : pRBElem -  pointer to the current Node
 *                       visit   - indicates the order of the tree traversal.
 *                       u4Level - indicates the present level of the tree
 *                       pArg    - Argument passed by the user function
 *
 *    OUTPUT           : pOut    - Output that needs to be indicated to the
 *                                 user function (Pointer to the RemoteNode
 *                                 or NULL)
 *
 *    RETURNS          : RB_WALK_BREAK / RB_WALK_CONT
 *
 ****************************************************************************/
INT4
LldpRxUtlWalkFnRemTable (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                         void *pArg, void *pOut)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    tLldpRemNodeInfo   *pRemDot3TlvInfo = NULL;
    BOOL1               bTlvRcvd = OSIX_FALSE;

    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (pArg);

    pRemDot3TlvInfo = (tLldpRemNodeInfo *) pOut;
    pRemDot3TlvInfo->pRemNode = NULL;

    if (visit == postorder || visit == leaf)
    {
        if (pRBElem != NULL)
        {
            pRemoteNode = (tLldpRemoteNode *) pRBElem;

            if (pRemoteNode->pDot3RemPortInfo != NULL)
            {
                switch (pRemDot3TlvInfo->u1Dot3TlvType)
                {
                    case LLDP_MAC_PHY_CONFIG_STATUS_TLV:
                        bTlvRcvd = pRemoteNode->bRcvdMacPhyTlv;
                        break;

                    case LLDP_LINK_AGG_TLV:
                        bTlvRcvd = pRemoteNode->bRcvdLinkAggTlv;
                        break;

                    case LLDP_MAX_FRAME_SIZE_TLV:
                        bTlvRcvd = pRemoteNode->bRcvdMaxFrameTlv;
                        break;

                    default:
                        break;
                }
                if (bTlvRcvd == OSIX_TRUE)
                {
                    /* Return the First Remote Node */
                    pRemDot3TlvInfo->pRemNode = pRemoteNode;
                    return RB_WALK_BREAK;
                }
            }
        }
    }
    return RB_WALK_CONT;
}

/*--------------------------------------------------------------------------*/
/*                       Get Routines for the Remote Tables                 */
/*--------------------------------------------------------------------------*/
/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlGetRemoteNodeByMSAP
 *
 *    DESCRIPTION      : Find the Remote Node from RemMSAPRBTree. 
 *
 *    INPUT            : i4RecvdPortNum - Local Port where the LLDPDU is 
 *                                        received.
 *                       pu1ChassisId   - Chassis Id of the Remote
 *                       pu1PortId      - Port Id Remote
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : Pointer To the Remote Node. If the Remote node is 
 *                       not found in the RBTree return NULL. Caller should 
 *                       check the return value.
 *
 ****************************************************************************/
tLldpRemoteNode    *
LldpRxUtlGetRemoteNodeByMSAP (INT4 i4RecvdPortNum, INT4 i4ChassisIdSubtype,
                              UINT1 *pu1ChassisId, INT4 i4PortIdSubtye,
                              UINT1 *pu1PortId)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    tLldpRemoteNode    *pTempRemoteNode = NULL;
    tLldpLocPortInfo   *pLocPortInfo = NULL;
    UINT2               u2ChassIdLen = 0;
    UINT2               u2PortIdLen = 0;

    pLocPortInfo = LLDP_GET_LOC_PORT_INFO ((UINT4) i4RecvdPortNum);

    if (pLocPortInfo == NULL)
    {
        return NULL;
    }

    if (LldpUtilGetChassisIdLen (i4ChassisIdSubtype, pu1ChassisId,
                                 &u2ChassIdLen) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpRxUtlGetRemoteNodeByMSAP: "
                  "LldpUtilGetChassisIdLen return FAILURE\r\n");
        return pRemoteNode;
    }
    /* Allocate Memory for Temp Remote Node */
    if ((pTempRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpRxUtlGetRemoteNodeByMSAP: Failed to Allocate Memory "
                  "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return pRemoteNode;
    }

    MEMSET (pTempRemoteNode, 0, sizeof (tLldpRemoteNode));

    pTempRemoteNode->i4RemLocalPortNum = pLocPortInfo->i4IfIndex;
    pTempRemoteNode->u4DestAddrTblIndex = pLocPortInfo->u4DstMacAddrTblIndex;
    pTempRemoteNode->i4RemChassisIdSubtype = i4ChassisIdSubtype;

    MEMCPY (pTempRemoteNode->au1RemChassisId, pu1ChassisId,
            MEM_MAX_BYTES (u2ChassIdLen, LLDP_MAX_LEN_CHASSISID));
    pTempRemoteNode->i4RemPortIdSubtype = i4PortIdSubtye;
    if (LldpUtilGetPortIdLen (i4PortIdSubtye, pu1PortId, &u2PortIdLen) !=
        OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpRxUtlGetRemoteNodeByMSAP: "
                  "LldpUtilGetPortIdLen returns FAILURE\r\n");
        /* Release the memory allocated for Temp Remote Node */
        MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                            (UINT1 *) pTempRemoteNode);
        return pRemoteNode;
    }

    MEMCPY (pTempRemoteNode->au1RemPortId, pu1PortId,
            MEM_MAX_BYTES (u2PortIdLen, LLDP_MAX_LEN_PORTID));

    pRemoteNode = (tLldpRemoteNode *) RBTreeGet (gLldpGlobalInfo.RemMSAPRBTree,
                                                 (tRBElem *) pTempRemoteNode);

    /* Release the memory allocated for Temp Remote Node */
    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                        (UINT1 *) pTempRemoteNode);
    return pRemoteNode;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlGetRemoteNode
 *
 *    DESCRIPTION      : This function returns the Remote Node for the given
 *                       time mark, local port number and remote index.
 *
 *    INPUT            : u4TimeMark     - Time at which the informtaion for the
 *                       neighbhor is stored.
 *                       i4LocalPortNum - Local Port where the LLDPDU is
 *                       received.
 *                       i4RemIndex - Identifies the remote system.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : Pointer to the Remote Node, or NULL
 *
 ****************************************************************************/
tLldpRemoteNode    *
LldpRxUtlGetRemoteNode (UINT4 u4TimeMark, INT4 i4LocalPortNum,
                        UINT4 u4DestIndex, INT4 i4RemIndex)
{
    tLldpRemoteNode    *pTempRemNode = NULL;
    tLldpRemoteNode    *pRemNode = NULL;

    /* Allocate Memory for Temp Remote Node */
    if ((pTempRemNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpRxUtlGetRemoteNode: Failed to Allocate Memory "
                  "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return pRemNode;
    }

    MEMSET (pTempRemNode, 0, sizeof (tLldpRemoteNode));

    if (LLDP_IS_REM_INDICES_VALID
        ((UINT4) i4LocalPortNum, u4DestIndex, i4RemIndex) == OSIX_TRUE)
    {
        /* Get the pointer to the Remote Node with the given indices */
        pTempRemNode->u4RemLastUpdateTime = u4TimeMark;
        pTempRemNode->i4RemLocalPortNum = i4LocalPortNum;
        pTempRemNode->i4RemIndex = i4RemIndex;
        pTempRemNode->u4DestAddrTblIndex = u4DestIndex;

        pRemNode =
            (tLldpRemoteNode *) RBTreeGet (gLldpGlobalInfo.RemSysDataRBTree,
                                           (tRBElem *) pTempRemNode);
    }

    /* Release the memory allocated for Temp Remote Node */
    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId, (UINT1 *) pTempRemNode);
    /* NULL will be returned if the node is not present */
    return pRemNode;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlGetRemManAddrNode
 *
 *    DESCRIPTION      : This fiunction fetch the Remote Management Address
 *                       information node from RemManAddrRBTree 
 *                       and return the pointer of the Node. This function 
 *                       is useful for SNMP low level interface operations.
 *
 *    INPUT            :
 *
 *    OUTPUT           :
 *
 *    RETURNS          :
 *
 ****************************************************************************/
tLldpRemManAddressTable *
LldpRxUtlGetRemManAddrNode (UINT4 u4LldpRemTimeMark,
                            INT4 i4LldpRemLocalPortNum, UINT4 u4DestIndex,
                            INT4 i4LldpRemIndex,
                            INT4 i4LldpRemManAddrSubtype, UINT1 *pu1RemManAddr)
{
    tLldpRemManAddressTable *pManAddrNode = NULL;
    tLldpRemManAddressTable TmpRemManNode;
    INT4                i4AddrLen = LLDP_INVALID_VALUE;

    MEMSET (&TmpRemManNode, 0, sizeof (tLldpRemManAddressTable));

    i4AddrLen = (UINT4) LldpUtilGetManAddrLen (i4LldpRemManAddrSubtype);
    /* Validate the Index */
    if (LldpRxUtlValidateManAddrTblIndices (u4LldpRemTimeMark,
                                            i4LldpRemLocalPortNum,
                                            u4DestIndex,
                                            i4LldpRemIndex,
                                            i4LldpRemManAddrSubtype,
                                            i4AddrLen) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "(LldpRxUtlGetRemManAddrNode) Index Validation Failed.\r\n");
        return NULL;
    }

    TmpRemManNode.u4RemLastUpdateTime = u4LldpRemTimeMark;
    TmpRemManNode.i4RemLocalPortNum = i4LldpRemLocalPortNum;
    TmpRemManNode.u4DestAddrTblIndex = u4DestIndex;
    TmpRemManNode.i4RemIndex = i4LldpRemIndex;
    TmpRemManNode.i4RemManAddrSubtype = i4LldpRemManAddrSubtype;
    MEMCPY (TmpRemManNode.au1RemManAddr, pu1RemManAddr, i4AddrLen);
    /* Fetch the Node from the RBTree */
    pManAddrNode = (tLldpRemManAddressTable *) RBTreeGet
        (gLldpGlobalInfo.RemManAddrRBTree, (tRBElem *) & TmpRemManNode);

    return pManAddrNode;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlGetRemUnknownTlvInfo
 *
 *    DESCRIPTION      : This fiunction fetch the Remote UnKnown TLV info
 *                       node from RemUnknownTLVRBTree and return the pointer
 *                       of the Node. This function is useful for SNMP 
 *                       low level interface operations.
 *
 *    INPUT            :
 *
 *    OUTPUT           :
 *
 *    RETURNS          :
 *
 ****************************************************************************/
tLldpRemUnknownTLVTable *
LldpRxUtlGetRemUnknownTlvInfo (UINT4 u4LldpRemTimeMark,
                               INT4 i4LldpRemLocalPortNum, UINT4 u4DestIndex,
                               INT4 i4LldpRemIndex,
                               INT4 i4LldpRemUnknownTLVType)
{
    tLldpRemUnknownTLVTable *pUnknowTlv = NULL;
    tLldpRemUnknownTLVTable TempUnknownTlv;

    MEMSET (&TempUnknownTlv, 0, sizeof (tLldpRemUnknownTLVTable));

    /* Validate the Unknown Tlv Info table indices */
    if (LldpRxUtlValidateUnknownTlvInfoTblIndices (u4LldpRemTimeMark,
                                                   i4LldpRemLocalPortNum,
                                                   u4DestIndex, i4LldpRemIndex,
                                                   i4LldpRemUnknownTLVType)
        != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "(LldpRxUtlGetRemUnknownTlvInfo) "
                  "Index Validation Failed.\r\n");
        return NULL;
    }

    /* Fetch the Node from the RemUnknownTLVRBTree */
    TempUnknownTlv.u4RemLastUpdateTime = u4LldpRemTimeMark;
    TempUnknownTlv.i4RemLocalPortNum = i4LldpRemLocalPortNum;
    TempUnknownTlv.u4DestAddrTblIndex = u4DestIndex;
    TempUnknownTlv.i4RemIndex = i4LldpRemIndex;
    TempUnknownTlv.i4RemUnknownTLVType = i4LldpRemUnknownTLVType;
    pUnknowTlv = (tLldpRemUnknownTLVTable *) RBTreeGet
        (gLldpGlobalInfo.RemUnknownTLVRBTree, (tRBElem *) & TempUnknownTlv);

    return pUnknowTlv;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlGetRemOrgDefInfo
 *
 *    DESCRIPTION      : This fiunction fetch the Remote Organizationaly Defined
 *                       information node from RemOrgDefInfoRBTree
 *                       and return the pointer of the Node. This function
 *                       is useful for SNMP low level interface operations.
 *
 *    INPUT            :
 *
 *    OUTPUT           :
 *
 *    RETURNS          :
 *
 ****************************************************************************/
tLldpRemOrgDefInfoTable *
LldpRxUtlGetRemOrgDefInfo (UINT4 u4LldpRemTimeMark,
                           INT4 i4LldpRemLocalPortNum,
                           UINT4 u4DestIndex,
                           INT4 i4LldpRemIndex,
                           UINT1 *pu1RemOrgDefInfoOUI,
                           INT4 i4LldpRemOrgDefInfoSubtype,
                           INT4 i4LldpRemOrgDefInfoIndex)
{
    tLldpRemOrgDefInfoTable *pOrgDefInfoNode = NULL;
    tLldpRemOrgDefInfoTable TempOrgDefInfo;

    MEMSET (&TempOrgDefInfo, 0, sizeof (tLldpRemOrgDefInfoTable));

    /* Validate the Remote Organizationally Defined information Table Indoces */
    if (LldpRxUtlValidateOrgDefInfoTblIndices (u4LldpRemTimeMark,
                                               i4LldpRemLocalPortNum,
                                               u4DestIndex,
                                               i4LldpRemIndex,
                                               i4LldpRemOrgDefInfoSubtype,
                                               i4LldpRemOrgDefInfoIndex)
        != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "(LldpRxUtlGetRemOrgDefInfo) Index Validation Failed.\r\n");
        return NULL;
    }
    /* Fetch the Node From RemOrgDefInfoRBTree */
    TempOrgDefInfo.u4RemLastUpdateTime = u4LldpRemTimeMark;
    TempOrgDefInfo.u4DestAddrTblIndex = u4DestIndex;
    TempOrgDefInfo.i4RemLocalPortNum = i4LldpRemLocalPortNum;
    TempOrgDefInfo.i4RemIndex = i4LldpRemIndex;
    MEMCPY (TempOrgDefInfo.au1RemOrgDefInfoOUI, pu1RemOrgDefInfoOUI,
            LLDP_MAX_LEN_OUI);
    TempOrgDefInfo.i4RemOrgDefInfoSubtype = i4LldpRemOrgDefInfoSubtype;
    TempOrgDefInfo.i4RemOrgDefInfoIndex = i4LldpRemOrgDefInfoIndex;
    pOrgDefInfoNode = (tLldpRemOrgDefInfoTable *) RBTreeGet
        (gLldpGlobalInfo.RemOrgDefInfoRBTree, (tRBElem *) & TempOrgDefInfo);

    return pOrgDefInfoNode;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlGetRemProtoVlanNode
 *
 *    DESCRIPTION      : This function returns the Remote Protocol Vlan Node
 *                       for the given time mark, local port number, remote
 *                       index and protocol vlan id.
 *
 *    INPUT            : u4TimeMark     - Time at which the informtaion about
 *                                        the neighbhor is stored.
 *                       i4LocalPortNum - Local Port where the LLDPDU is
 *                       received.
 *                       i4RemIndex - Identifies the remote system.
 *                       u2ProtoVlanId - Protocol vlan id.
 *
 *    OUTPUT           : ppProtoVlanNode - Address of pointer to the Remote 
 *                                         Protocol Vlan Node, or NULL 
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
LldpRxUtlGetRemProtoVlanNode (UINT4 u4TimeMark, INT4 i4LocalPortNum,
                              UINT4 u4DestIndex, INT4 i4RemIndex,
                              UINT2 u2ProtoVlanId,
                              tLldpxdot1RemProtoVlanInfo ** ppProtoVlanNode)
{
    tLldpxdot1RemProtoVlanInfo ProtoVlanNode;
    INT4                i4RetVal = OSIX_FAILURE;

    MEMSET (&ProtoVlanNode, 0, sizeof (tLldpxdot1RemProtoVlanInfo));

    if ((LLDP_IS_PORT_ID_VALID ((UINT4) i4LocalPortNum) == OSIX_TRUE) &&
        (LLDP_IS_REM_INDEX_VALID (i4RemIndex) == OSIX_TRUE) &&
        (LLDP_IS_VLAN_ID_VALID (u2ProtoVlanId) == OSIX_TRUE))
    {
        ProtoVlanNode.u4RemLastUpdateTime = u4TimeMark;
        ProtoVlanNode.i4RemLocalPortNum = i4LocalPortNum;
        ProtoVlanNode.i4RemIndex = i4RemIndex;
        ProtoVlanNode.u2ProtoVlanId = u2ProtoVlanId;
        ProtoVlanNode.u4DestAddrTblIndex = u4DestIndex;
        *ppProtoVlanNode = (tLldpxdot1RemProtoVlanInfo *) RBTreeGet
            (gLldpGlobalInfo.RemProtoVlanRBTree, (tRBElem *) & ProtoVlanNode);

        if (*ppProtoVlanNode != NULL)
        {
            i4RetVal = OSIX_SUCCESS;
        }
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlGetRemVlanNameNode
 *
 *    DESCRIPTION      : This function returns the Remote Vlan Name Node
 *                       for the given time mark, local port number, remote
 *                       index and vlan id.
 *
 *    INPUT            : u4TimeMark     - Time at which the informtaion for the
 *                       neighbhor is stored.
 *                       i4LocalPortNum - Local Port where the LLDPDU is
 *                       received.
 *                       i4RemIndex     - Identifies the remote system.
 *                       u2VlanId       - Vlan id.
 *
 *    OUTPUT           : ppVlanNode      - Address of pointer to the Remote Vlan
 *                                         Name Node, or NULL 
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
LldpRxUtlGetRemVlanNameNode (UINT4 u4TimeMark, INT4 i4LocalPortNum,
                             UINT4 u4DestIndex, INT4 i4RemIndex, UINT2 u2VlanId,
                             tLldpxdot1RemVlanNameInfo ** ppVlanNode)
{
    tLldpxdot1RemVlanNameInfo VlanNode;
    INT4                i4RetVal = OSIX_FAILURE;

    MEMSET (&VlanNode, 0, sizeof (tLldpxdot1RemVlanNameInfo));

    if ((LLDP_IS_PORT_ID_VALID ((UINT4) i4LocalPortNum) == OSIX_TRUE) &&
        (LLDP_IS_REM_INDEX_VALID (i4RemIndex) == OSIX_TRUE) &&
        (VLAN_IS_VLAN_ID_VALID (u2VlanId) == OSIX_TRUE))
    {
        VlanNode.u4RemLastUpdateTime = u4TimeMark;
        VlanNode.i4RemLocalPortNum = i4LocalPortNum;
        VlanNode.i4RemIndex = i4RemIndex;
        VlanNode.u2VlanId = u2VlanId;
        VlanNode.u4DestAddrTblIndex = u4DestIndex;
        *ppVlanNode = (tLldpxdot1RemVlanNameInfo *) RBTreeGet
            (gLldpGlobalInfo.RemVlanNameInfoRBTree, (tRBElem *) & VlanNode);
        if (*ppVlanNode != NULL)
        {
            i4RetVal = OSIX_SUCCESS;
        }
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlGetRemProtoIdNode
 *
 *    DESCRIPTION      : This function returns the Remote Protocol ID Node
 *                       for the given time mark, local port number, remote
 *                       index and protocol index.
 *
 *    INPUT            : u4TimeMark       - Time at which the informtaion for 
 *                                          the neighbhor is stored.
 *                       i4LocalPortNum - Local Port where the LLDPDU is
 *                       received.
 *                       i4RemIndex     - Identifies the remote system.
 *                       u4ProtocolIndex  - Protocol Index 
 *
 *    OUTPUT           : ppProtoIdNode     - Address of pointer to the Remote
 *                                           Protocol ID Node, or NULL
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpRxUtlGetRemProtoIdNode (UINT4 u4TimeMark, INT4 i4LocalPortNum,
                            UINT4 u4DestIndex,
                            INT4 i4RemIndex, UINT4 u4ProtocolIndex,
                            tLldpxdot1RemProtoIdInfo ** ppProtoIdNode)
{
    tLldpxdot1RemProtoIdInfo ProtoIdNode;
    INT1                i1RetVal = OSIX_FAILURE;

    MEMSET (&ProtoIdNode, 0, sizeof (tLldpxdot1RemProtoIdInfo));

    if ((LLDP_IS_PORT_ID_VALID ((UINT4) i4LocalPortNum) == OSIX_TRUE) &&
        (LLDP_IS_REM_INDEX_VALID (i4RemIndex) == OSIX_TRUE) &&
        (LLDP_IS_PROTO_ID_VALID (u4ProtocolIndex) == OSIX_TRUE))
    {
        ProtoIdNode.u4RemLastUpdateTime = u4TimeMark;
        ProtoIdNode.i4RemLocalPortNum = i4LocalPortNum;
        ProtoIdNode.i4RemIndex = i4RemIndex;
        ProtoIdNode.u4ProtocolIndex = u4ProtocolIndex;
        ProtoIdNode.u4DestAddrTblIndex = u4DestIndex;
        *ppProtoIdNode = (tLldpxdot1RemProtoIdInfo *) RBTreeGet
            (gLldpGlobalInfo.RemProtoIdRBTree, (tRBElem *) & ProtoIdNode);
        if (*ppProtoIdNode != NULL)
        {
            i1RetVal = OSIX_SUCCESS;
        }
    }
    return i1RetVal;
}

/*--------------------------------------------------------------------------*/
/*                       Table Index Validation Routines                   */
/*--------------------------------------------------------------------------*/
/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlValidateManAddrTblIndices
 *
 *    DESCRIPTION      : This routine validate the Indices of the Remote
 *                       Management Address Table.
 *
 *    INPUT            :
 *
 *    OUTPUT           :
 *
 *    RETURNS          :
 *
 ****************************************************************************/
INT4
LldpRxUtlValidateManAddrTblIndices (UINT4 u4LldpRemTimeMark,
                                    INT4 i4LldpRemLocalPortNum,
                                    UINT4 u4DestIndex,
                                    INT4 i4LldpRemIndex,
                                    INT4 i4LldpRemManAddrSubtype,
                                    INT4 i4ManAddrLen)
{
    /* Validate the common Indices */
    if (LLDP_IS_REM_INDICES_VALID ((UINT4) i4LldpRemLocalPortNum, u4DestIndex,
                                   i4LldpRemIndex) != OSIX_TRUE)
    {
        return OSIX_FAILURE;
    }
    /* Validate the Man Addr Specific Indices */
    if ((i4LldpRemManAddrSubtype != IPVX_ADDR_FMLY_IPV4) &&
        (i4LldpRemManAddrSubtype != IPVX_ADDR_FMLY_IPV6) &&
        (i4LldpRemManAddrSubtype != LLDP_MAN_ADDR_SUB_TYPE_OTHER))
    {
        return OSIX_FAILURE;
    }
    if ((i4ManAddrLen > LLDP_MAX_LEN_MAN_ADDR) ||
        (i4ManAddrLen < LLDP_MIN_LEN_MAN_ADDR))
    {
        return OSIX_FAILURE;
    }

    UNUSED_PARAM (u4LldpRemTimeMark);
    /* All indices are validated */
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlValidateUnknownTlvInfoTblIndices
 *
 *    DESCRIPTION      :
 *
 *
 *
 *    INPUT            :
 *
 *    OUTPUT           :
 *
 *    RETURNS          :
 *
 ****************************************************************************/
INT4
LldpRxUtlValidateUnknownTlvInfoTblIndices (UINT4 u4LldpRemTimeMark,
                                           INT4 i4LldpRemLocalPortNum,
                                           UINT4 u4DestIndex,
                                           INT4 i4LldpRemIndex,
                                           INT4 i4LldpRemUnknownTLVType)
{
    /* Validate the common Indices */
    if (LLDP_IS_REM_INDICES_VALID ((UINT4) i4LldpRemLocalPortNum, u4DestIndex,
                                   i4LldpRemIndex) != OSIX_TRUE)
    {
        return OSIX_FAILURE;
    }

    /* Validate the Unknown TLV Info Specific Indices */
    if ((i4LldpRemUnknownTLVType < LLDP_MIN_UNKNOWN_TLV_TYPE) ||
        (i4LldpRemUnknownTLVType > LLDP_MAX_UNKNOWN_TLV_TYPE))
    {
        return OSIX_FAILURE;
    }

    UNUSED_PARAM (u4LldpRemTimeMark);
    /* All indices are validated */
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlValidateOrgDefInfoTblIndices
 *
 *    DESCRIPTION      :
 *
 *
 *
 *    INPUT            :
 *
 *    OUTPUT           :
 *
 *    RETURNS          :
 *
 ****************************************************************************/
INT4
LldpRxUtlValidateOrgDefInfoTblIndices (UINT4 u4LldpRemTimeMark,
                                       INT4 i4LldpRemLocalPortNum,
                                       UINT4 u4DestIndex,
                                       INT4 i4LldpRemIndex,
                                       INT4 i4LldpRemOrgDefInfoSubtype,
                                       INT4 i4LldpRemOrgDefInfoIndex)
{
    /* Validate the common indices */
    if (LLDP_IS_REM_INDICES_VALID ((UINT4) i4LldpRemLocalPortNum, u4DestIndex,
                                   i4LldpRemIndex) != OSIX_TRUE)
    {
        return OSIX_FAILURE;
    }
    if ((i4LldpRemOrgDefInfoSubtype < LLDP_MIN_LEN_ORG_DEF_INFO_SUBTYPE) ||
        (i4LldpRemOrgDefInfoSubtype > LLDP_MAX_LEN_ORG_DEF_INFO_SUBTYPE))
    {
        return OSIX_FAILURE;
    }
    if (i4LldpRemOrgDefInfoIndex < LLDP_MIN_LEN_ORG_DEF_INFO_INDEX)
    {
        return OSIX_FAILURE;
    }

    UNUSED_PARAM (u4LldpRemTimeMark);
    /* All indices are validated */
    return OSIX_SUCCESS;
}

/****************************************************************************
 * 
 *    FUNCTION NAME    : LldpRxUtlValidateRemTableIndices
 *
 *    DESCRIPTION      : This function validates the indices of the remote
 *                       table. The indices are time mark, local port number
 *                       and remote index.
 *
 *    INPUT            : u4TimeMark     - Time at which the informtaion for the
 *                       neighbhor is stored.
 *                       i4LocalPortNum - Local Port where the LLDPDU is
 *                       received.
 *                       i4RemIndex - Identifies the remote system. 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
LldpRxUtlValidateRemTableIndices (UINT4 u4TimeMark, INT4 i4LocalPortNum,
                                  UINT4 u4DestIndex, INT4 i4RemIndex)
{
    if (LldpRxUtlGetRemoteNode
        (u4TimeMark, i4LocalPortNum, u4DestIndex, i4RemIndex) != NULL)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 * 
 *    FUNCTION NAME    : LldpRxUtlValidateDot3RemTableInd
 *
 *    DESCRIPTION      : This function validates the indices of the Dot3 remote
 *                       table. The indices are time mark, local port number
 *                       and remote index.
 *
 *    INPUT            : u4TimeMark     - Time at which the informtaion for the
 *                                        neighbhor is stored.
 *                       i4LocalPortNum - Local Port where the LLDPDU is
 *                                        received.
 *                       i4RemIndex     - Identifies the remote system. 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
LldpRxUtlValidateDot3RemTableInd (UINT4 u4TimeMark, INT4 i4LocalPortNum,
                                  UINT4 u4DestIndex, INT4 i4RemIndex)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    tLldpRemoteNode    *pTmpRemoteNode = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    /* Allocate Memory for Remote Node */
    if ((pTmpRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_MANDATORY_TLV_TRC,
                  "SNMPSTD: Failed to Allocate Memory " "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }
    MEMSET (pTmpRemoteNode, 0, sizeof (tLldpRemoteNode));
    pTmpRemoteNode->u4RemLastUpdateTime = u4TimeMark;
    pTmpRemoteNode->i4RemLocalPortNum = i4LocalPortNum;
    pTmpRemoteNode->i4RemIndex = i4RemIndex;
    pTmpRemoteNode->u4DestAddrTblIndex = u4DestIndex;
    pRemoteNode =
        (tLldpRemoteNode *) RBTreeGet (gLldpGlobalInfo.RemSysDataRBTree,
                                       (tRBElem *) pTmpRemoteNode);
    if ((pRemoteNode != NULL))
    {
        i4RetVal = OSIX_SUCCESS;
    }

    /* Release the memory allocated for Remote Node */
    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                        (UINT1 *) pTmpRemoteNode);
    return i4RetVal;
}

/****************************************************************************
 * 
 *    FUNCTION NAME    : LldpRxUtlGetFirstDot3RemTableInd
 *
 *    DESCRIPTION      : This function returns the indices of the First Remote
 *                       Node with Dot3 Information. The indices are time mark,
 *                       local port number and remote index.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : *pu4TimeMark     - Time at which the informtaion for 
 *                                          the neighbhor is stored.
 *                       *pi4LocalPortNum - Local Port where the LLDPDU is
 *                                          received.
 *                       *pi4RemIndex     - Identifies the remote system. 
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
LldpRxUtlGetFirstDot3RemTableInd (UINT4 *pu4TimeMark, INT4 *pi4LocalPortNum,
                                  INT4 *pi4RemIndex, UINT1 u1TlvType)
{
    tLldpRemNodeInfo    RemDot3TlvInfo;
    INT4                i4RetVal = OSIX_FAILURE;

    MEMSET (&RemDot3TlvInfo, 0, sizeof (tLldpRemNodeInfo));
    RemDot3TlvInfo.u1Dot3TlvType = u1TlvType;

    RBTreeWalk (gLldpGlobalInfo.RemSysDataRBTree,
                (tRBWalkFn) LldpRxUtlWalkFnRemTable, NULL, &RemDot3TlvInfo);

    if (RemDot3TlvInfo.pRemNode != NULL)
    {
        *pu4TimeMark = RemDot3TlvInfo.pRemNode->u4RemLastUpdateTime;
        *pi4LocalPortNum = RemDot3TlvInfo.pRemNode->i4RemLocalPortNum;
        *pi4RemIndex = RemDot3TlvInfo.pRemNode->i4RemIndex;
        i4RetVal = OSIX_SUCCESS;
    }
    return i4RetVal;
}

/****************************************************************************
 * 
 *    FUNCTION NAME    : LldpRxUtlGetNextDot3RemTableInd
 *
 *    DESCRIPTION      : This function returns the indices of the Next Remote
 *                       Node with Dot3 Information. The indices are time mark,
 *                       local port number and remote index.
 *
 *    INPUT            : u4TimeMark    - Time at which the informtaion for the 
 *                                       neighbhor is stored
 *                       LocalPortNum  - Local Port whose next port number is
 *                                      required
 *                       i4RemIndex    - Index for the remote system
 *
 *    OUTPUT           : *pu4NextTimeMark  - Ponter to the time at which the 
 *                                           informtaion about the next 
 *                                           neighbhor is stored.
 *                       *pi4NextLocalPortNum - Ponter to the next Local Port
 *                       *pi4NextRemIndex     - Ponter to the index which 
 *                                              identifies the next remote 
 *                                              system. 
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
LldpRxUtlGetNextDot3RemTableInd (UINT4 u4TimeMark, UINT4 *pu4NextTimeMark,
                                 INT4 i4LocalPortNum, INT4 *pi4NextLocalPortNum,
                                 INT4 i4RemIndex, INT4 *pi4NextRemIndex,
                                 UINT1 u1TlvType)
{
    tLldpRemoteNode    *pTmpRemoteNode = NULL;
    tLldpRemoteNode    *pRemNode = NULL;
    tLldpRemoteNode    *pNextRemNode = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    BOOL1               bTlvRcvd = OSIX_FALSE;

    /* Allocate Memory for Remote Node */
    if ((pTmpRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_MANDATORY_TLV_TRC,
                  "SNMPSTD: Failed to Allocate Memory " "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }
    MEMSET (pTmpRemoteNode, 0, sizeof (tLldpRemoteNode));
    pTmpRemoteNode->u4RemLastUpdateTime = u4TimeMark;
    pTmpRemoteNode->i4RemLocalPortNum = i4LocalPortNum;
    pTmpRemoteNode->i4RemIndex = i4RemIndex;
    pTmpRemoteNode->u4DestAddrTblIndex = 1;

    pRemNode = (tLldpRemoteNode *) RBTreeGet
        (gLldpGlobalInfo.RemSysDataRBTree, pTmpRemoteNode);

    if (pRemNode != NULL)
    {
        while ((pNextRemNode = (tLldpRemoteNode *)
                RBTreeGetNext (gLldpGlobalInfo.RemSysDataRBTree,
                               (tRBElem *) pRemNode, NULL)) != NULL)
        {
            if (pNextRemNode->pDot3RemPortInfo != NULL)
            {
                switch (u1TlvType)
                {
                    case LLDP_MAC_PHY_CONFIG_STATUS_TLV:
                        bTlvRcvd = pNextRemNode->bRcvdMacPhyTlv;
                        break;

                    case LLDP_LINK_AGG_TLV:
                        bTlvRcvd = pNextRemNode->bRcvdLinkAggTlv;
                        break;

                    case LLDP_MAX_FRAME_SIZE_TLV:
                        bTlvRcvd = pNextRemNode->bRcvdMaxFrameTlv;
                        break;

                    default:
                        break;
                }
                if (bTlvRcvd == OSIX_TRUE)
                {

                    *pu4NextTimeMark = pNextRemNode->u4RemLastUpdateTime;
                    *pi4NextLocalPortNum = pNextRemNode->i4RemLocalPortNum;
                    *pi4NextRemIndex = pNextRemNode->i4RemIndex;
                    i4RetVal = OSIX_SUCCESS;
                    break;
                }
            }
            pRemNode = pNextRemNode;
        }
    }
    /* Release the memory allocated for Remote Node */
    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                        (UINT1 *) pTmpRemoteNode);
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlValidateProtoIdIndices
 *
 *    DESCRIPTION      : This function validates the indices of the remote 
 *                       protocol ID table. The indices are time mark, 
 *                       local port number, remote index and ProtocolIndex.
 *
 *    INPUT            : u4TimeMark     - Time at which the informtaion for the
 *                                        neighbhor is stored.
 *                       i4LocalPortNum - Local Port where the LLDPDU is
 *                                        received.
 *                       i4RemIndex     - Identifies the remote system. 
 *                       u4ProtocolIndex- ProtocolIndex 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
LldpRxUtlValidateProtoIdIndices (UINT4 u4TimeMark, INT4 i4LocalPortNum,
                                 UINT4 u4DestIndex, INT4 i4RemIndex,
                                 UINT4 u4ProtocolIndex)
{
    tLldpxdot1RemProtoIdInfo *pProtoIdNode = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    if (LldpRxUtlGetRemProtoIdNode
        (u4TimeMark, i4LocalPortNum, u4DestIndex, i4RemIndex, u4ProtocolIndex,
         &pProtoIdNode) == OSIX_SUCCESS)
    {
        i4RetVal = OSIX_SUCCESS;
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlValidateProtVlanIndices
 *
 *    DESCRIPTION      : This function validates the indices of the remote 
 *                       protocol vlan table. The indices are time mark, 
 *                       local port number, remote index and protocol vlan id.
 *
 *    INPUT            : u4TimeMark     - Time at which the informtaion about
 *                                        the neighbhor is stored
 *                       i4LocalPortNum - Local Port where the LLDPDU is
 *                                        received
 *                       i4RemIndex     - Identifies the remote system 
 *                       u2ProtoVlanId  - Protocol vlan id
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
LldpRxUtlValidateProtVlanIndices (UINT4 u4TimeMark, INT4 i4LocalPortNum,
                                  UINT4 u4DestIndex, INT4 i4RemIndex,
                                  UINT2 u2ProtoVlanId)
{
    tLldpxdot1RemProtoVlanInfo *pProtoVlanNode = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    if (LldpRxUtlGetRemProtoVlanNode (u4TimeMark, i4LocalPortNum, u4DestIndex,
                                      i4RemIndex, u2ProtoVlanId,
                                      &pProtoVlanNode) == OSIX_SUCCESS)
    {
        i4RetVal = OSIX_SUCCESS;
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUtlValidateVlanNameIndices
 *
 *    DESCRIPTION      : This function validates the indices of the remote 
 *                       vlan name table. The indices are time mark, 
 *                       local port number, remote index and vlan id.
 *
 *    INPUT            : u4TimeMark     - Time at which the informtaion about
 *                                        the neighbhor is stored.
 *                       i4LocalPortNum - Local Port where the LLDPDU is
 *                                        received.
 *                       i4RemIndex     - Identifies the remote system. 
 *                       u2VlanId       - Vlan id.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
LldpRxUtlValidateVlanNameIndices (UINT4 u4TimeMark, INT4 i4LocalPortNum,
                                  UINT4 u4DestIndex, INT4 i4RemIndex,
                                  UINT2 u2VlanId)
{
    tLldpxdot1RemVlanNameInfo *pVlanNode = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    if (LldpRxUtlGetRemVlanNameNode (u4TimeMark, i4LocalPortNum, u4DestIndex,
                                     i4RemIndex, u2VlanId, &pVlanNode)
        == OSIX_SUCCESS)
    {
        i4RetVal = OSIX_SUCCESS;
    }
    return i4RetVal;
}

INT4
LldpRxUtlRBCmpV2ValidateSysData (tRBElem * e1, tRBElem * e2)
{
    tLldpRemoteNode    *pRemEntry1 = (tLldpRemoteNode *) e1;
    tLldpRemoteNode    *pRemEntry2 = (tLldpRemoteNode *) e2;

    /* Compare the First Key */
    if (pRemEntry1->u4RemLastUpdateTime > pRemEntry2->u4RemLastUpdateTime)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4RemLastUpdateTime < pRemEntry2->u4RemLastUpdateTime)
    {
        return LLDP_RB_LESS;
    }
    /* First key is equal so compare the 2nd key */
    if (pRemEntry1->i4RemLocalPortNum > pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemLocalPortNum < pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_LESS;
    }
    /* First key is equal so compare the 2nd key */
    if (pRemEntry1->u4DestAddrTblIndex > pRemEntry2->u4DestAddrTblIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4DestAddrTblIndex < pRemEntry2->u4DestAddrTblIndex)
    {
        return LLDP_RB_LESS;
    }
    if (pRemEntry1->i4RemIndex > pRemEntry2->i4RemIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemIndex < pRemEntry2->i4RemIndex)
    {
        return LLDP_RB_LESS;
    }
    else
    {
        return LLDP_RB_EQUAL;
    }
}

INT4
LldpRxUtlRBCmpV2SysData (tRBElem * e1, tRBElem * e2)
{
    tLldpRemoteNode    *pRemEntry1 = (tLldpRemoteNode *) e1;
    tLldpRemoteNode    *pRemEntry2 = (tLldpRemoteNode *) e2;

    /* Compare the First Key */
    if (pRemEntry1->u4RemLastUpdateTime > pRemEntry2->u4RemLastUpdateTime)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4RemLastUpdateTime < pRemEntry2->u4RemLastUpdateTime)
    {
        return LLDP_RB_LESS;
    }
    /* First key is equal so compare the 2nd key */
    if (pRemEntry1->i4RemLocalPortNum > pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemLocalPortNum < pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_LESS;
    }
    /* First key is equal so compare the 2nd key */
    if (pRemEntry1->u4DestAddrTblIndex > pRemEntry2->u4DestAddrTblIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4DestAddrTblIndex < pRemEntry2->u4DestAddrTblIndex)
    {
        return LLDP_RB_LESS;
    }
    else
    {
        return LLDP_RB_EQUAL;
    }
}

#endif /* _LLDRXUTL_C_ */
/*--------------------------------------------------------------------------*/
/*                       End of the file  <lldrxutl.c>                      */
/*--------------------------------------------------------------------------*/
