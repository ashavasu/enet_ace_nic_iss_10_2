/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fslldlw.c,v 1.67 2017/12/14 10:30:25 siva Exp $
*
* Description: Contains LLDP properitory MIB Low Level Routines
*********************************************************************/
# include "lldinc.h"
# include "lldcli.h"

extern UINT4        fslldp[8];
extern UINT4        stdlld[6];
extern UINT4        sd3lv2[8];
extern UINT4        slldv2[8];
extern UINT4        sd1lv2[8];
extern UINT4        FsLldpSystemControl[10];
PRIVATE VOID        LldpNotifyProtocolShutdownStatus (VOID);
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLldpSystemControl
 Input       :  The Indices

                The Object 
                retValFsLldpSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLldpSystemControl (INT4 *pi4RetValFsLldpSystemControl)
{
    *pi4RetValFsLldpSystemControl = gLldpGlobalInfo.u1LldpSystemControl;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLldpModuleStatus
 Input       :  The Indices

                The Object 
                retValFsLldpModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLldpModuleStatus (INT4 *pi4RetValFsLldpModuleStatus)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        *pi4RetValFsLldpModuleStatus = (INT4) LLDP_DISABLED;
        return SNMP_SUCCESS;
    }
    *pi4RetValFsLldpModuleStatus = gLldpGlobalInfo.u1ModuleStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLldpTraceInput
 Input       :  The Indices

                The Object 
                retValFsLldpTraceInput
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLldpTraceInput (tSNMP_OCTET_STRING_TYPE * pRetValFsLldpTraceInput)
{
    UINT4               u4TraceOption = 0;

    if (LLDP_IS_SHUTDOWN ())
    {
        /* get the actaul trace option and add ciritical trace option, then
         * get the corresponding trace input and return the trace input */
        u4TraceOption = gLldpGlobalInfo.u4TraceOption;
        /* default trace option - critical trace */
        u4TraceOption |= (UINT4) LLDP_CRITICAL_TRC;
        /* get trace input string corresponding to the given
         * trace option */
        pRetValFsLldpTraceInput->i4_Length = LldpUtilGetTraceInputValue
            (pRetValFsLldpTraceInput->pu1_OctetList, u4TraceOption);
        return SNMP_SUCCESS;
    }

    pRetValFsLldpTraceInput->i4_Length = LldpUtilGetTraceInputValue
        (pRetValFsLldpTraceInput->pu1_OctetList, gLldpGlobalInfo.u4TraceOption);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLldpTraceOption
 Input       :  The Indices

                The Object 
                retValFsLldpTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLldpTraceOption (INT4 *pi4RetValFsLldpTraceOption)
{
    INT4                i4TraceOption = 0;

    if (LLDP_IS_SHUTDOWN ())
    {
        /* get the actaul trace option and add critical trace 
         * option(default) */
        i4TraceOption = (INT4) gLldpGlobalInfo.u4TraceOption;
        i4TraceOption |= LLDP_CRITICAL_TRC;
        *pi4RetValFsLldpTraceOption = i4TraceOption;
        return SNMP_SUCCESS;
    }

    *pi4RetValFsLldpTraceOption = gLldpGlobalInfo.u4TraceOption;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLldpSystemControl
 Input       :  The Indices

                The Object 
                setValFsLldpSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLldpSystemControl (INT4 i4SetValFsLldpSystemControl)
{
    /*
     * If the value is same no need to set it again.
     * */
    if (gLldpGlobalInfo.u1LldpSystemControl !=
        (UINT1) i4SetValFsLldpSystemControl)
    {
        if (i4SetValFsLldpSystemControl == LLDP_START)
        {
            if (LldpModuleStart () != OSIX_SUCCESS)
            {
                LLDP_TRC (ALL_FAILURE_TRC, " SNMPFS: LldpModuleStart"
                          " returns FAILURE \r\n");
                return SNMP_FAILURE;
            }
        }
        else
        {
            LldpModuleShutDown ();
            /* Notify MSR with the LLDP oids */
            LldpNotifyProtocolShutdownStatus ();
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLldpModuleStatus
 Input       :  The Indices

                The Object 
                setValFsLldpModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLldpModuleStatus (INT4 i4SetValFsLldpModuleStatus)
{
    if (gLldpGlobalInfo.u1ModuleStatus != (UINT1) i4SetValFsLldpModuleStatus)
    {
        gLldpGlobalInfo.u1ModuleStatus = (UINT1) i4SetValFsLldpModuleStatus;
        if (gLldpGlobalInfo.u1ModuleStatus == LLDP_ENABLED)
        {

            LldpModuleEnable ();
        }
        else
        {
            LldpModuleDisable ();
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLldpTraceInput
 Input       :  The Indices

                The Object 
                setValFsLldpTraceInput
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLldpTraceInput (tSNMP_OCTET_STRING_TYPE * pSetValFsLldpTraceInput)
{
    UINT4               u4TrcOption = 0;
    UINT4               u4TrcLen = 0;

    u4TrcOption = LldpUtilGetTraceOptionValue
        (pSetValFsLldpTraceInput->pu1_OctetList,
         pSetValFsLldpTraceInput->i4_Length);

    if (!STRNCMP (pSetValFsLldpTraceInput->pu1_OctetList, "enable",
                  STRLEN ("enable")))
    {
        gLldpGlobalInfo.u4TraceOption |= u4TrcOption;
    }
    else
    {
        gLldpGlobalInfo.u4TraceOption &= ~u4TrcOption;
    }

    /* set trace input */
    MEMSET (gLldpGlobalInfo.au1TraceInput, 0, LLDP_TRC_MAX_SIZE);
    u4TrcLen = LldpUtilGetTraceInputValue (gLldpGlobalInfo.au1TraceInput,
                                           gLldpGlobalInfo.u4TraceOption);

    UNUSED_PARAM (u4TrcLen);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLldpSystemControl
 Input       :  The Indices

                The Object 
                testValFsLldpSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLldpSystemControl (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsLldpSystemControl)
{
    INT4                i4RetVal = 0;
    if ((i4TestValFsLldpSystemControl == LLDP_SHUTDOWN))
    {
        if (LLDP_IS_SHUTDOWN ())
        {
            LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                      "nmhTestv2FsLldpSystemControl: "
                      "LLDP System is ShutDown\n");

            return SNMP_SUCCESS;
        }
    }
    if ((i4TestValFsLldpSystemControl == LLDP_SHUTDOWN_INPROGRESS) ||
        (LLDP_SYSTEM_CONTROL () == LLDP_SHUTDOWN_INPROGRESS))

    {
        /* System control cannot be set to shutdown in progress and
         * cannot be set if shutdown process is being carried out */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (LLDP_CLI_ERR_SHUTDOWN_IN_PROGRESS);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsLldpSystemControl != LLDP_START) &&
        (i4TestValFsLldpSystemControl != LLDP_SHUTDOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VlanGetBaseBridgeMode () == DOT_1D_BRIDGE_MODE)
    {
        CLI_SET_ERR (LLDP_VLAN_BASE_BRIDGE_LLDP_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsLldpSystemControl == LLDP_SHUTDOWN)
    {
        i4RetVal = LldpUtlTestLldpCdcpTxStatus ();

        if (i4RetVal == SNMP_FAILURE)
        {
            CLI_SET_ERR (LLDP_VLAN_EVB_CDCP_ENABLED);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLldpModuleStatus
 Input       :  The Indices

                The Object 
                testValFsLldpModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLldpModuleStatus (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsLldpModuleStatus)
{
    INT4                i4RetVal = 0;
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC, "nmhTestv2FsLldpModuleStatus: "
                  "LLDP System is ShutDown\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsLldpModuleStatus != LLDP_ENABLED) &&
        (i4TestValFsLldpModuleStatus != LLDP_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsLldpModuleStatus == LLDP_DISABLED)
    {
        i4RetVal = LldpUtlTestLldpCdcpTxStatus ();

        if (i4RetVal == SNMP_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (LLDP_VLAN_EVB_CDCP_ENABLED);
            return SNMP_FAILURE;
        }
    }

    if (LLDP_GLOB_SHUT_WHILE_TMR_STATUS () == LLDP_TMR_RUNNING)
    {
        /* Global Timer will be started when LLDP module is disabled.
         * After starting the timer, the flag will be set. This flag
         * will be reset after processing the corresponding timer expiry
         * functions. Since the flag is set, disable process is not
         * completed. Hence the operation should return failure. */
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC, "nmhTestv2FsLldpModuleStatus: "
                  "LLDP System Disable is in progress."
                  "Global ShutWhile Timer is running\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (LLDP_CLI_ERR_GLOBAL_SHUTWHILE_EXP);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLldpTraceInput
 Input       :  The Indices

                The Object 
                testValFsLldpTraceInput
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLldpTraceInput (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pTestValFsLldpTraceInput)
{
    UINT4               u4TrcOption = 0;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC, "LLDP System ShutDown\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pTestValFsLldpTraceInput->i4_Length > LLDP_TRC_MAX_SIZE) ||
        (pTestValFsLldpTraceInput->i4_Length < LLDP_TRC_MIN_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    u4TrcOption = LldpUtilGetTraceOptionValue
        (pTestValFsLldpTraceInput->pu1_OctetList,
         pTestValFsLldpTraceInput->i4_Length);
    if (u4TrcOption == LLDP_INVALID_TRC)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLldpSystemControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLldpSystemControl (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLldpModuleStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLldpModuleStatus (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLldpTraceInput
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLldpTraceInput (UINT4 *pu4ErrorCode,
                          tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsLldpLocChassisIdSubtype
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLldpLocChassisIdSubtype (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLldpLocChassisId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLldpLocChassisId (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsLldpLocPortTable
 Input       :  The Indices
                LldpLocPortNum
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLldpLocPortTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLldpLocChassisIdSubtype
 Input       :  The Indices

                The Object 
                retValFsLldpLocChassisIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLldpLocChassisIdSubtype (INT4 *pi4RetValFsLldpLocChassisIdSubtype)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        *pi4RetValFsLldpLocChassisIdSubtype = (INT4) LLDP_CHASS_ID_SUB_MAC_ADDR;
        return SNMP_SUCCESS;
    }
    *pi4RetValFsLldpLocChassisIdSubtype =
        gLldpGlobalInfo.LocSysInfo.i4LocChassisIdSubtype;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLldpLocChassisId
 Input       :  The Indices

                The Object 
                retValFsLldpLocChassisId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLldpLocChassisId (tSNMP_OCTET_STRING_TYPE * pRetValFsLldpLocChassisId)
{
    UINT2               u2ChassisIdLen = 0;
    tMacAddr            SwitchMacAddr;

    MEMSET (SwitchMacAddr, 0, sizeof (tMacAddr));

    if (LLDP_IS_SHUTDOWN ())
    {
        /* return default chasssis id - switch mac address */
        LldpPortGetSysMacAddr (SwitchMacAddr);
        /* default chassis id is mac address, so the mac addr length is
         * returned as default chassis id length */
        u2ChassisIdLen = (UINT2) MAC_ADDR_LEN;
        MEMCPY (pRetValFsLldpLocChassisId->pu1_OctetList, SwitchMacAddr,
                u2ChassisIdLen);
        pRetValFsLldpLocChassisId->i4_Length = u2ChassisIdLen;
        return SNMP_SUCCESS;
    }

    if (LldpUtilGetChassisIdLen (gLldpGlobalInfo.LocSysInfo.
                                 i4LocChassisIdSubtype,
                                 gLldpGlobalInfo.LocSysInfo.au1LocChassisId,
                                 &u2ChassisIdLen) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD:LldpUtilGetChassisIdLen returns FAILURE !!\r\n");
        return SNMP_FAILURE;
    }

    MEMCPY (pRetValFsLldpLocChassisId->pu1_OctetList,
            gLldpGlobalInfo.LocSysInfo.au1LocChassisId,
            MEM_MAX_BYTES (u2ChassisIdLen, LLDP_MAX_LEN_CHASSISID));

    pRetValFsLldpLocChassisId->i4_Length = u2ChassisIdLen;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLldpLocChassisIdSubtype
 Input       :  The Indices

                The Object 
                setValFsLldpLocChassisIdSubtype
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLldpLocChassisIdSubtype (INT4 i4SetValFsLldpLocChassisIdSubtype)
{
    INT4                i4CurrChassisIdSubtype = LLDP_INVALID_VALUE;

    i4CurrChassisIdSubtype = gLldpGlobalInfo.LocSysInfo.i4LocChassisIdSubtype;
    if (i4CurrChassisIdSubtype == i4SetValFsLldpLocChassisIdSubtype)
    {
        return SNMP_SUCCESS;
    }
    gLldpGlobalInfo.LocSysInfo.i4LocChassisIdSubtype =
        i4SetValFsLldpLocChassisIdSubtype;
    /* if chassis id subtype is if-alias/mac-addr/mw-addr/if-name
     * then call LldpTxUtlSetChassisId which will set the chassis id with
     * system specific values 
     * else the user has to configure chassis id which inturn will call
     * nmhSetFsLldpLocChassisId and set the chassis id */
    if ((i4SetValFsLldpLocChassisIdSubtype == LLDP_CHASS_ID_SUB_IF_ALIAS) ||
        (i4SetValFsLldpLocChassisIdSubtype == LLDP_CHASS_ID_SUB_MAC_ADDR) ||
        (i4SetValFsLldpLocChassisIdSubtype == LLDP_CHASS_ID_SUB_NW_ADDR) ||
        (i4SetValFsLldpLocChassisIdSubtype == LLDP_CHASS_ID_SUB_IF_NAME))
    {
        if (LldpTxUtlSetChassisId (i4SetValFsLldpLocChassisIdSubtype)
            != OSIX_SUCCESS)
        {
            gLldpGlobalInfo.LocSysInfo.i4LocChassisIdSubtype =
                i4CurrChassisIdSubtype;
            LLDP_TRC (ALL_FAILURE_TRC, " SNMPFS: LldpTxUtlSetChassisId"
                      " returns FAILURE \r\n");
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLldpLocChassisId
 Input       :  The Indices

                The Object 
                setValFsLldpLocChassisId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLldpLocChassisId (tSNMP_OCTET_STRING_TYPE * pSetValFsLldpLocChassisId)
{
    INT4                i4ChassisIdSubtype = 0;

    nmhGetFsLldpLocChassisIdSubtype (&i4ChassisIdSubtype);
    if ((i4ChassisIdSubtype == LLDP_CHASS_ID_SUB_CHASSIS_COMP) ||
        (i4ChassisIdSubtype == LLDP_CHASS_ID_SUB_PORT_COMP) ||
        (i4ChassisIdSubtype == LLDP_CHASS_ID_SUB_LOCAL))
    {

        MEMSET (gLldpGlobalInfo.LocSysInfo.au1LocChassisId, 0,
                LLDP_MAX_LEN_CHASSISID);

        MEMCPY (gLldpGlobalInfo.LocSysInfo.au1LocChassisId,
                pSetValFsLldpLocChassisId->pu1_OctetList,
                pSetValFsLldpLocChassisId->i4_Length);
        /* change in local sys info */
        LldpTxUtlHandleLocSysInfoChg ();
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLldpLocChassisIdSubtype
 Input       :  The Indices

                The Object 
                testValFsLldpLocChassisIdSubtype
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLldpLocChassisIdSubtype (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFsLldpLocChassisIdSubtype)
{
    UINT4               u4DefIfIndex = 0;
    UINT4               u4Ipv4Addr = 0;
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "LLDP should be started before accessing its "
                  "MIB objects !!\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (i4TestValFsLldpLocChassisIdSubtype == LLDP_CHASS_ID_SUB_NW_ADDR)
    {
        LldpPortGetDefaultRouterIfIndex (&u4DefIfIndex);
        if (LldpPortValidateIfIndex (u4DefIfIndex) != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC, "SNMPFS: LldpPortValidateIfIndex "
                      "returns FAILURE.\r\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (LLDP_CLI_ERR_DEFAULT_VLAN_INTERFACE);
            return SNMP_FAILURE;
        }
        if (LldpPortGetIpv4Addr (u4DefIfIndex, &u4Ipv4Addr) != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC, "SNMPFS: LldpPortGetIpv4Addr "
                      "returns FAILURE.\r\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (LLDP_CLI_ERR_DEFAULT_VLAN_INTERFACE);
            return SNMP_FAILURE;
        }
    }
    if ((i4TestValFsLldpLocChassisIdSubtype < LLDP_CHASS_ID_SUB_CHASSIS_COMP)
        || (i4TestValFsLldpLocChassisIdSubtype > LLDP_CHASS_ID_SUB_LOCAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLldpLocChassisId
 Input       :  The Indices

                The Object 
                testValFsLldpLocChassisId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLldpLocChassisId (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValFsLldpLocChassisId)
{
    INT4                i4ChassisIdSubtype = LLDP_INVALID_VALUE;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "LLDP should be started before accessing its "
                  "MIB objects !!\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pTestValFsLldpLocChassisId->i4_Length < LLDP_MIN_LEN_CHASSISID) ||
        (pTestValFsLldpLocChassisId->i4_Length > LLDP_MAX_LEN_CHASSISID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    /* before calling nmhTestv2FsLldpLocChassisId,  
     * nmhSetFsLldpLocChassisIdSubtype should have been called so that the 
     * newly subtype can be used here for subtype validation */
    /* get the current chassis id subtype */
    i4ChassisIdSubtype = gLldpGlobalInfo.LocSysInfo.i4LocChassisIdSubtype;
    /* if chassis id subtype is ifalias/ifname/mwaddr/macaddr 
     * return failure */
    if ((i4ChassisIdSubtype == LLDP_CHASS_ID_SUB_IF_ALIAS) ||
        (i4ChassisIdSubtype == LLDP_CHASS_ID_SUB_IF_NAME) ||
        (i4ChassisIdSubtype == LLDP_CHASS_ID_SUB_NW_ADDR) ||
        (i4ChassisIdSubtype == LLDP_CHASS_ID_SUB_MAC_ADDR))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsLldpLocPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLldpLocPortTable
 Input       :  The Indices
                LldpLocPortNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsLldpLocPortTable (INT4 i4LldpLocPortNum)
{
    tLldpLocPortTable  *pPortEntry = NULL;
    tLldpLocPortTable  *pTmpPortEntry = NULL;
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "LLDP should be started before accessing its "
                  "MIB objects !!\n");

        return SNMP_FAILURE;
    }
    if ((pTmpPortEntry = (tLldpLocPortTable *)
         MemAllocMemBlk (gLldpGlobalInfo.LocPortTablePoolId)) == NULL)
    {
        LLDP_TRC ((LLDP_CRITICAL_TRC | ALL_FAILURE_TRC),
                  "LldpTxUtlSetPortId: Failed to Allocate Memory "
                  "for LldpLocPortTable node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return SNMP_FAILURE;
    }
    MEMSET (pTmpPortEntry, 0, sizeof (tLldpLocPortTable));

    pTmpPortEntry->i4IfIndex = i4LldpLocPortNum;
    pPortEntry = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree,
                   (tRBElem *) pTmpPortEntry);
    if (pPortEntry == NULL)        /*For Klocwork */
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                (UINT1 *) pTmpPortEntry) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "LldpTxUtlSetPortId: "
                      "Node info Memory release failed!!. \r\n");
        }
        return SNMP_FAILURE;
    }
    if ((i4LldpLocPortNum < LLDP_MIN_PORTS)
        || ((UINT4) i4LldpLocPortNum > LLDP_MAX_PORTS))
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                (UINT1 *) pTmpPortEntry) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "LldpTxUtlSetPortId: "
                      "Node info Memory release failed!!. \r\n");
        }
        return SNMP_FAILURE;
    }
    if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                            (UINT1 *) pTmpPortEntry) == MEM_FAILURE)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpTxUtlSetPortId: "
                  "Node info Memory release failed!!. \r\n");
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsLldpLocPortTable
 Input       :  The Indices
                LldpLocPortNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsLldpLocPortTable (INT4 *pi4LldpLocPortNum)
{

    return (nmhGetNextIndexFsLldpLocPortTable (LLDP_ZERO, pi4LldpLocPortNum));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsLldpLocPortTable
 Input       :  The Indices
                LldpLocPortNum
                nextLldpLocPortNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsLldpLocPortTable (INT4 i4LldpLocPortNum,
                                   INT4 *pi4NextLldpLocPortNum)
{
    tLldpLocPortTable  *pPortEntry = NULL;
    tLldpLocPortTable   LldpPortTable;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "LLDP should be started before accessing its "
                  "MIB objects !!\n");

        return SNMP_FAILURE;
    }

    MEMSET (&LldpPortTable, 0, sizeof (tLldpLocPortTable));
    LldpPortTable.i4IfIndex = i4LldpLocPortNum;
    pPortEntry = (tLldpLocPortTable *)
        RBTreeGetNext (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpPortTable,
                       LldpPortInfoUtlRBCmpInfo);
    if (pPortEntry != NULL)
    {
        *pi4NextLldpLocPortNum = pPortEntry->i4IfIndex;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLldpLocPortIdSubtype
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValFsLldpLocPortIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLldpLocPortIdSubtype (INT4 i4LldpLocPortNum,
                              INT4 *pi4RetValFsLldpLocPortIdSubtype)
{
    tLldpLocPortTable  *pPortEntry = NULL;
    tLldpLocPortTable   PortEntry;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }

    MEMSET (&PortEntry, 0, sizeof (tLldpLocPortTable));
    PortEntry.i4IfIndex = i4LldpLocPortNum;
    pPortEntry = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, (tRBElem *) & PortEntry);

    if (pPortEntry == NULL)        /*For Klocwork */
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsLldpLocPortIdSubtype = pPortEntry->i4LocPortIdSubtype;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLldpLocPortId
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValFsLldpLocPortId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLldpLocPortId (INT4 i4LldpLocPortNum,
                       tSNMP_OCTET_STRING_TYPE * pRetValFsLldpLocPortId)
{
    tLldpLocPortTable  *pPortEntry = NULL;
    tLldpLocPortTable   PortEntry;
    UINT2               u2PortIdLen = 0;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }

    MEMSET (&PortEntry, 0, sizeof (tLldpLocPortTable));
    PortEntry.i4IfIndex = i4LldpLocPortNum;
    pPortEntry = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, (tRBElem *) & PortEntry);

    if (pPortEntry == NULL)        /*For Klocwork */
    {
        return SNMP_FAILURE;
    }

    if (LldpUtilGetPortIdLen (pPortEntry->i4LocPortIdSubtype,
                              pPortEntry->au1LocPortId,
                              &u2PortIdLen) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtilGetPortIdLen is failed \r\n");
        return SNMP_FAILURE;
    }

    pRetValFsLldpLocPortId->i4_Length = u2PortIdLen;
    MEMCPY (pRetValFsLldpLocPortId->pu1_OctetList,
            pPortEntry->au1LocPortId,
            MEM_MAX_BYTES (u2PortIdLen, LLDP_MAX_LEN_PORTID));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLldpPortConfigNotificationType
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValFsLldpPortConfigNotificationType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLldpPortConfigNotificationType (INT4 i4LldpLocPortNum,
                                        INT4
                                        *pi4RetValFsLldpPortConfigNotificationType)
{
    tLldpLocPortInfo   *pLocPortInfo = NULL;
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }

    pLocPortInfo = LLDP_GET_LOC_PORT_INFO (i4LldpLocPortNum);
    if (pLocPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsLldpPortConfigNotificationType =
        pLocPortInfo->PortConfigTable.u1FsConfigNotificationType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLldpLocPortDstMac
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValFsLldpLocPortDstMac
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLldpLocPortDstMac (INT4 i4LldpLocPortNum,
                           tMacAddr * pRetValFsLldpLocPortDstMac)
{
    tLldpLocPortTable  *pLocPortTable = NULL;
    tLldpLocPortTable   LocPortTable;
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }

    MEMSET (&LocPortTable, 0, sizeof (tLldpLocPortTable));

    LocPortTable.i4IfIndex = i4LldpLocPortNum;
    pLocPortTable = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LocPortTable);
    if (pLocPortTable != NULL)
    {
        MEMCPY (pRetValFsLldpLocPortDstMac, pLocPortTable->au1DstMac,
                MAC_ADDR_LEN);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsLldpMedAdminStatus
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValFsLldpMedAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLldpMedAdminStatus (INT4 i4LldpLocPortNum,
                            INT4 *pi4RetValFsLldpMedAdminStatus)
{

    if (LLDP_IS_SHUTDOWN ())
    {
        *pi4RetValFsLldpMedAdminStatus = (INT4) LLDP_DISABLED;
        return SNMP_SUCCESS;
    }
    tLldpLocPortTable  *pLocPortTable = NULL;
    tLldpLocPortTable   LocPortTable;

    LocPortTable.i4IfIndex = i4LldpLocPortNum;
    pLocPortTable = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LocPortTable);
    if (pLocPortTable == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsLldpMedAdminStatus = pLocPortTable->i4MedAdminStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLldpTraceLevel
 Input       :  The Indices

                The Object 
                retValFsLldpTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLldpTraceLevel (INT4 *pi4RetValFsLldpTraceLevel)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        /* get the actaul trace option and add ciritical trace option, then
         * get the corresponding trace input and return the trace input */
        *pi4RetValFsLldpTraceLevel = (INT4) gLldpGlobalInfo.u4TraceOption;
        /* default trace option - critical trace */
        *pi4RetValFsLldpTraceLevel |= (INT4) LLDP_CRITICAL_TRC;
        /* get trace input string corresponding to the given
         * trace option */
        return SNMP_SUCCESS;
    }

    *pi4RetValFsLldpTraceLevel = (INT4) gLldpGlobalInfo.u4TraceOption;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLldpTagStatus
 Input       :  The Indices

                The Object 
                retValFsLldpTagStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLldpTagStatus (INT4 *pi4RetValFsLldpTagStatus)
{
    *pi4RetValFsLldpTagStatus = gLldpGlobalInfo.i4TagStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLldpConfiguredMgmtIpv4Address
 Input       :  The Indices

                The Object 
                retValFsLldpConfiguredMgmtIpv4Address
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLldpConfiguredMgmtIpv4Address (tSNMP_OCTET_STRING_TYPE *
                                       pRetValFsLldpConfiguredMgmtIpv4Address)
{
    MEMCPY (pRetValFsLldpConfiguredMgmtIpv4Address->pu1_OctetList,
            (gLldpGlobalInfo.MgmtAddr.au1Addr),
            gLldpGlobalInfo.MgmtAddr.u1AddrLen);
    pRetValFsLldpConfiguredMgmtIpv4Address->i4_Length =
        gLldpGlobalInfo.MgmtAddr.u1AddrLen;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLldpConfiguredMgmtIpv6Address
 Input       :  The Indices

                The Object 
                retValFsLldpConfiguredMgmtIpv6Address
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLldpConfiguredMgmtIpv6Address (tSNMP_OCTET_STRING_TYPE
                                       * pRetValFsLldpConfiguredMgmtIpv6Address)
{
    MEMCPY (pRetValFsLldpConfiguredMgmtIpv6Address->pu1_OctetList,
            (gLldpGlobalInfo.MgmtAddr6.au1Addr),
            gLldpGlobalInfo.MgmtAddr6.u1AddrLen);
    pRetValFsLldpConfiguredMgmtIpv6Address->i4_Length =
        gLldpGlobalInfo.MgmtAddr6.u1AddrLen;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLldpLocPortIdSubtype
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                setValFsLldpLocPortIdSubtype
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLldpLocPortIdSubtype (INT4 i4LldpLocPortNum,
                              INT4 i4SetValFsLldpLocPortIdSubtype)
{
    tLldpLocPortTable  *pPortEntry = NULL;
    tLldpLocPortTable   PortEntry;
    INT4                i4CurrPortIdSubtype = LLDP_INVALID_VALUE;

    MEMSET (&PortEntry, 0, sizeof (tLldpLocPortTable));
    PortEntry.i4IfIndex = i4LldpLocPortNum;
    pPortEntry = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, (tRBElem *) & PortEntry);

    if (pPortEntry == NULL)        /*For Klocwork */
    {
        return SNMP_FAILURE;
    }
    i4CurrPortIdSubtype = pPortEntry->i4LocPortIdSubtype;
    if (i4CurrPortIdSubtype == i4SetValFsLldpLocPortIdSubtype)
    {
        return SNMP_SUCCESS;
    }
    pPortEntry->i4LocPortIdSubtype = i4SetValFsLldpLocPortIdSubtype;
    /* if port id subtype is if-alias/mac-addr/mw-addr/if-name/agentcircuitid
     * then call LldpTxUtlSetPortId which will set the port id with
     * system specific values 
     * else the user has to configure port id which inturn will call
     * nmhSetFsLldpLocChassisId and set the port id */
    if ((i4SetValFsLldpLocPortIdSubtype == LLDP_PORT_ID_SUB_IF_ALIAS) ||
        (i4SetValFsLldpLocPortIdSubtype == LLDP_PORT_ID_SUB_MAC_ADDR) ||
        (i4SetValFsLldpLocPortIdSubtype == LLDP_PORT_ID_SUB_NW_ADDR) ||
        (i4SetValFsLldpLocPortIdSubtype == LLDP_PORT_ID_SUB_IF_NAME) ||
        (i4SetValFsLldpLocPortIdSubtype == LLDP_PORT_ID_SUB_AGENT_CKT_ID))
    {
        if (LldpTxUtlSetPortId (i4LldpLocPortNum,
                                i4SetValFsLldpLocPortIdSubtype) != OSIX_SUCCESS)
        {
            pPortEntry->i4LocPortIdSubtype = i4CurrPortIdSubtype;
            LLDP_TRC (ALL_FAILURE_TRC, " SNMPFS: LldpTxUtlSetPortId"
                      " returns FAILURE \r\n");
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLldpLocPortId
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                setValFsLldpLocPortId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLldpLocPortId (INT4 i4LldpLocPortNum,
                       tSNMP_OCTET_STRING_TYPE * pSetValFsLldpLocPortId)
{
    tLldpLocPortTable  *pPortEntry = NULL;
    tLldpLocPortInfo   *pLocPortInfo = NULL;
    tLldpLocPortTable   PortEntry;
    tLldpv2AgentToLocPort AgentToLocPort;
    tLldpv2AgentToLocPort *pAgentToLocPort = NULL;
    INT4                i4DataPortType = 0;

    MEMSET (&PortEntry, 0, sizeof (tLldpLocPortTable));
    PortEntry.i4IfIndex = i4LldpLocPortNum;
    pPortEntry = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, (tRBElem *) & PortEntry);

    if (pPortEntry == NULL)        /*For Klocwork */
    {
        return SNMP_FAILURE;
    }

    MEMSET (&AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
    AgentToLocPort.i4IfIndex = i4LldpLocPortNum;
    pAgentToLocPort = (tLldpv2AgentToLocPort *)
        RBTreeGetNext (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                       &AgentToLocPort, LldpAgentToLocPortUtlRBCmpInfo);
    if (pAgentToLocPort == NULL)
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "SNMPFS: nmhSetFsLldpLocPortId "
                       " pAgentToLocPort for port %d not found \r\n",
                       i4LldpLocPortNum);
        return SNMP_FAILURE;
    }
    pLocPortInfo = LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);

    if (pLocPortInfo == NULL)
    {
        LLDP_TRC_ARG1 (MGMT_TRC | ALL_FAILURE_TRC,
                       "nmhSetFsLldpLocPortIdSubtype: SNMPFS: No local port "
                       "info available for %d\r\n", i4LldpLocPortNum);
        return SNMP_FAILURE;
    }
    nmhGetFsLldpLocPortIdSubtype (i4LldpLocPortNum, &i4DataPortType);
    if ((i4DataPortType == LLDP_PORT_ID_SUB_PORT_COMP) ||
        (i4DataPortType == LLDP_PORT_ID_SUB_LOCAL))
    {
        MEMSET (pPortEntry->au1LocPortId, 0, LLDP_MAX_LEN_PORTID);
        MEMCPY (pPortEntry->au1LocPortId,
                pSetValFsLldpLocPortId->pu1_OctetList,
                pSetValFsLldpLocPortId->i4_Length);

        while ((pAgentToLocPort != NULL) &&
               (pAgentToLocPort->i4IfIndex == i4LldpLocPortNum))
        {
            pLocPortInfo =
                LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);
            if (pLocPortInfo != NULL)
            {
                if (LldpTxUtlHandleLocPortInfoChg (pLocPortInfo) !=
                    OSIX_SUCCESS)
                {
                    LLDP_TRC (ALL_FAILURE_TRC, "SNMPFS: nmhSetFsLldpLocPortId "
                              "returns FAILURE\r\n");
                    return SNMP_FAILURE;
                }
                MEMSET (&AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
                AgentToLocPort.i4IfIndex = pAgentToLocPort->i4IfIndex;
                MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                        pAgentToLocPort->Lldpv2DestMacAddress, MAC_ADDR_LEN);
                pAgentToLocPort = (tLldpv2AgentToLocPort *)
                    RBTreeGetNext (gLldpGlobalInfo.
                                   Lldpv2AgentToLocPortMapTblRBTree,
                                   &AgentToLocPort,
                                   LldpAgentToLocPortUtlRBCmpInfo);
            }
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLldpPortConfigNotificationType
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                setValFsLldpPortConfigNotificationType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLldpPortConfigNotificationType (INT4 i4LldpLocPortNum,
                                        INT4
                                        i4SetValFsLldpPortConfigNotificationType)
{
    tLldpLocPortInfo   *pLocPortInfo = NULL;
    pLocPortInfo = LLDP_GET_LOC_PORT_INFO (i4LldpLocPortNum);
    if (pLocPortInfo == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpTxUtlGetLocPortEntry is failed \n");
        return SNMP_FAILURE;
    }
    pLocPortInfo->PortConfigTable.u1FsConfigNotificationType =
        (UINT1) i4SetValFsLldpPortConfigNotificationType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLldpLocPortDstMac
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                setValFsLldpLocPortDstMac
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLldpLocPortDstMac (INT4 i4LldpLocPortNum,
                           tMacAddr SetValFsLldpLocPortDstMac)
{
    tLldpLocPortTable  *pLocPortTable = NULL;
    tLldpLocPortTable   LocPortTable;
    tLldpLocPortInfo   *pLocPortInfo = NULL;
    tLldpRemoteNode    *pRemoteNode = NULL;
    tLldpv2AgentToLocPort *pAgentToLocPort = NULL;
    tLldpv2AgentToLocPort AgentToLocPort;
    tLldpv2DestAddrTbl  DestAddrTbl;
    tLldpv2DestAddrTbl *pDestAddrTbl = NULL;
    tMacAddr            MacAddr;
    UINT4               u4DstMacAddrTblIndex = 0;
    UINT4               u4LldpLocPortNum = 0;
    BOOL1               b1Flag = OSIX_FALSE;

    MEMSET (&LocPortTable, 0, sizeof (tLldpLocPortTable));
    MEMSET (&AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
    MEMSET (&DestAddrTbl, 0, sizeof (tLldpv2DestAddrTbl));

    LocPortTable.i4IfIndex = i4LldpLocPortNum;
    pLocPortTable = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LocPortTable);
    if (pLocPortTable == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pLocPortTable->au1DstMac, SetValFsLldpLocPortDstMac, MAC_ADDR_LEN);

    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
    {
        MEMSET (&MacAddr, 0, sizeof (tMacAddr));
        MEMCPY (&MacAddr, SetValFsLldpLocPortDstMac, MAC_ADDR_LEN);
        if (LldpGetDestAddrTblIndexFromMacAddr (&MacAddr,
                                                &u4DstMacAddrTblIndex) !=
            OSIX_SUCCESS)
        {
            pDestAddrTbl =
                (tLldpv2DestAddrTbl
                 *) (MemAllocMemBlk (gLldpGlobalInfo.LldpDestMacAddrPoolId));
            if (pDestAddrTbl == NULL)
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "Memory allocation failed for Destination Mac address table\r\n");
                return SNMP_FAILURE;
            }

            MEMSET (pDestAddrTbl, 0, sizeof (tLldpv2DestAddrTbl));
            if (gu4LldpDestMacAddrTblIndex < LLDP_MAX_DEST_ADDR_TBL_INDEX)
            {
                pDestAddrTbl->u4LlldpV2DestAddrTblIndex =
                    gu4LldpDestMacAddrTblIndex;
                gu4LldpDestMacAddrTblIndex++;
            }
            else
            {
                MemReleaseMemBlock (gLldpGlobalInfo.LldpDestMacAddrPoolId,
                                    (UINT1 *) pDestAddrTbl);
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "Max agents created already\r\n");
                return SNMP_FAILURE;
            }
            pDestAddrTbl->u1RowStatus = ACTIVE;
            MEMCPY (pDestAddrTbl->Lldpv2DestMacAddress,
                    SetValFsLldpLocPortDstMac, MAC_ADDR_LEN);
            if (RBTreeAdd
                (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree,
                 (tRBElem *) & (pDestAddrTbl->DestMacAddrTblNode)) !=
                RB_SUCCESS)
            {
                gu4LldpDestMacAddrTblIndex--;
                MemReleaseMemBlock (gLldpGlobalInfo.LldpDestMacAddrPoolId,
                                    (UINT1 *) pDestAddrTbl);
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "Max agents created already\r\n");
                return SNMP_FAILURE;

            }
            u4DstMacAddrTblIndex = pDestAddrTbl->u4LlldpV2DestAddrTblIndex;
            b1Flag = OSIX_TRUE;
        }
        u4LldpLocPortNum = LldpV1LocPortNum (i4LldpLocPortNum);
        pLocPortInfo = LLDP_GET_LOC_PORT_INFO (u4LldpLocPortNum);
        if (pLocPortInfo == NULL)
        {
            if (b1Flag == OSIX_TRUE)
            {
                gu4LldpDestMacAddrTblIndex--;
                if (RBTreeRemove (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree,
                                  pDestAddrTbl) != RB_SUCCESS)
                {
                    LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                              "MemRelease for pDestAddrTbl failed\r\n");

                }
                MemReleaseMemBlock (gLldpGlobalInfo.LldpDestMacAddrPoolId,
                                    (UINT1 *) pDestAddrTbl);
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "Local port entry not found\r\n");
            }
            return SNMP_FAILURE;
        }
        else                    /*if (pLldpLocPortInfo != NULL) */
        {
            pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;
            if (pRemoteNode != NULL)
            {
                if (pRemoteNode->RxInfoAgeTmrNode.u1Status == LLDP_TMR_RUNNING)
                {
                    if (TmrStopTimer (gLldpGlobalInfo.TmrListId,
                                      (tTmrAppTimer *) & (pRemoteNode->
                                                          RxInfoAgeTmrNode)) !=
                        TMR_SUCCESS)
                    {
                        LLDP_TRC_ARG1 (OS_RESOURCE_TRC | ALL_FAILURE_TRC
                                       | CONTROL_PLANE_TRC,
                                       "TX-SEM[%d]: In LldpTxSmStateShutFrame "
                                       "TmrStopTimer(TtrTmr) returns FAILURE!!!\r\n",
                                       pLocPortInfo->u4LocPortNum);
                    }
                    /* set the transmit interval timer status as NOT_RUNNING */
                    pRemoteNode->RxInfoAgeTmrNode.u1Status =
                        LLDP_TMR_NOT_RUNNING;
                }
                LldpRxUtlDelRemoteNode (pRemoteNode);
                pLocPortInfo->pBackPtrRemoteNode = NULL;

            }
        }
        AgentToLocPort.i4IfIndex = i4LldpLocPortNum;
        MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                pLocPortInfo->PortConfigTable.u1DstMac, MAC_ADDR_LEN);
        pAgentToLocPort = (tLldpv2AgentToLocPort *)
            RBTreeGet (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                       &AgentToLocPort);
        if (pAgentToLocPort == NULL)
        {
            if (b1Flag == OSIX_TRUE)
            {
                gu4LldpDestMacAddrTblIndex--;
                if (RBTreeRemove (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree,
                                  pDestAddrTbl) != RB_SUCCESS)
                {
                    LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                              "MemRelease for pDestAddrTbl failed\r\n");

                }
                MemReleaseMemBlock (gLldpGlobalInfo.LldpDestMacAddrPoolId,
                                    (UINT1 *) pDestAddrTbl);
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "AgentToLocPort entry not found\r\n");
            }
            return SNMP_FAILURE;
        }
        AgentToLocPort.u4LldpLocPort = pAgentToLocPort->u4LldpLocPort;
        MEMCPY (AgentToLocPort.Lldpv2DestMacAddress, SetValFsLldpLocPortDstMac,
                MAC_ADDR_LEN);
        AgentToLocPort.u4DstMacAddrTblIndex = u4DstMacAddrTblIndex;
        RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                      (tRBElem *) pAgentToLocPort);
        RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentMapTblRBTree,
                      (tRBElem *) pAgentToLocPort);

        MemReleaseMemBlock (gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId,
                            (UINT1 *) pAgentToLocPort);

        pLocPortInfo->u4DstMacAddrTblIndex = u4DstMacAddrTblIndex;
        pAgentToLocPort =
            (tLldpv2AgentToLocPort *) MemAllocMemBlk (gLldpGlobalInfo.
                                                      LldpAgentToLocPortMappingPoolId);
        if (pAgentToLocPort == NULL)
        {
            if (b1Flag == OSIX_TRUE)
            {
                gu4LldpDestMacAddrTblIndex--;
                if (RBTreeRemove (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree,
                                  pDestAddrTbl) != RB_SUCCESS)
                {
                    LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                              "MemRelease for pDestAddrTbl failed\r\n");

                }
                MemReleaseMemBlock (gLldpGlobalInfo.LldpDestMacAddrPoolId,
                                    (UINT1 *) pDestAddrTbl);
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "AgentToLocPort entry memory allocation failed\r\n");
            }
            return SNMP_FAILURE;
        }
        pAgentToLocPort->i4IfIndex = AgentToLocPort.i4IfIndex;
        pAgentToLocPort->u4LldpLocPort = AgentToLocPort.u4LldpLocPort;
        MEMCPY (pAgentToLocPort->Lldpv2DestMacAddress,
                AgentToLocPort.Lldpv2DestMacAddress, sizeof (tMacAddr));
        pAgentToLocPort->u4DstMacAddrTblIndex =
            AgentToLocPort.u4DstMacAddrTblIndex;

        if (RBTreeAdd (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                       (tRBElem *) pAgentToLocPort) == RB_FAILURE)
        {
            if (b1Flag == OSIX_TRUE)
            {
                gu4LldpDestMacAddrTblIndex--;
                if (RBTreeRemove (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree,
                                  pDestAddrTbl) != RB_SUCCESS)
                {
                    LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                              "MemRelease for pDestAddrTbl failed\r\n");

                }
                MemReleaseMemBlock (gLldpGlobalInfo.LldpDestMacAddrPoolId,
                                    (UINT1 *) pDestAddrTbl);
            }
            MemReleaseMemBlock (gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId,
                                (UINT1 *) pAgentToLocPort);
            return SNMP_FAILURE;
        }
        if (RBTreeAdd (gLldpGlobalInfo.Lldpv2AgentMapTblRBTree,
                       (tRBElem *) pAgentToLocPort) == RB_FAILURE)
        {
            if (b1Flag == OSIX_TRUE)
            {
                gu4LldpDestMacAddrTblIndex--;
                if (RBTreeRemove (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree,
                                  pDestAddrTbl) != RB_SUCCESS)
                {
                    LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                              "MemRelease for pDestAddrTbl failed\r\n");

                }
                MemReleaseMemBlock (gLldpGlobalInfo.LldpDestMacAddrPoolId,
                                    (UINT1 *) pDestAddrTbl);
            }
            RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                          (tRBElem *) pAgentToLocPort);
            MemReleaseMemBlock (gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId,
                                (UINT1 *) pAgentToLocPort);
            return SNMP_FAILURE;
        }
        MEMCPY (pLocPortInfo->PortConfigTable.u1DstMac,
                SetValFsLldpLocPortDstMac, MAC_ADDR_LEN);

        if (LldpTxUtlHandleLocPortInfoChg (pLocPortInfo) != OSIX_SUCCESS)
        {
            LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC, "nmhSetFsLldpLocPortDstMac: "
                      "LldpTxUtlHandleLocPortInfoChg returns FAILURE\r\n");

        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLldpMedAdminStatus
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                setValFsLldpMedAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLldpMedAdminStatus (INT4 i4LldpLocPortNum,
                            INT4 i4SetValFsLldpMedAdminStatus)
{
    tLldpLocPortTable  *pLocPortTable = NULL;
    tLldpLocPortTable   LocPortTable;

    LocPortTable.i4IfIndex = i4LldpLocPortNum;
    pLocPortTable = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LocPortTable);
    if (pLocPortTable == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pLocPortTable->i4MedAdminStatus != i4SetValFsLldpMedAdminStatus)
    {
        pLocPortTable->i4MedAdminStatus = i4SetValFsLldpMedAdminStatus;
        LldpMedHandleAdminStatusChng (i4LldpLocPortNum,
                                      i4SetValFsLldpMedAdminStatus);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLldpTraceLevel
 Input       :  The Indices

                The Object 
                setValFsLldpTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLldpTraceLevel (INT4 i4SetValFsLldpTraceLevel)
{
    gLldpGlobalInfo.u4TraceOption = (UINT4) i4SetValFsLldpTraceLevel;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsLldpTagStatus
 Input       :  The Indices

                The Object 
                setValFsLldpTagStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLldpTagStatus (INT4 i4SetValFsLldpTagStatus)
{
    gLldpGlobalInfo.i4TagStatus = i4SetValFsLldpTagStatus;
    /* LLDP Local system has changed, so construct the packet again
     * in case if there is any valid change in packet info to be sent */
    LldpTxUtlHandleLocSysInfoChg ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLldpConfiguredMgmtIpv4Address
 Input       :  The Indices

                The Object 
                setValFsLldpConfiguredMgmtIpv4Address
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLldpConfiguredMgmtIpv4Address (tSNMP_OCTET_STRING_TYPE
                                       * pSetValFsLldpConfiguredMgmtIpv4Address)
{

    MEMCPY ((gLldpGlobalInfo.MgmtAddr.au1Addr),
            pSetValFsLldpConfiguredMgmtIpv4Address->pu1_OctetList,
            pSetValFsLldpConfiguredMgmtIpv4Address->i4_Length);
    gLldpGlobalInfo.MgmtAddr.u1AddrLen =
        (UINT1) pSetValFsLldpConfiguredMgmtIpv4Address->i4_Length;

    /* Management Ip has been configured so change the IP address in TLV */
    LldpTxUtlHandleLocSysInfoChg ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLldpConfiguredMgmtIpv6Address
 Input       :  The Indices

                The Object 
                setValFsLldpConfiguredMgmtIpv6Address
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLldpConfiguredMgmtIpv6Address (tSNMP_OCTET_STRING_TYPE
                                       * pSetValFsLldpConfiguredMgmtIpv6Address)
{
    MEMCPY ((gLldpGlobalInfo.MgmtAddr6.au1Addr),
            pSetValFsLldpConfiguredMgmtIpv6Address->pu1_OctetList,
            (pSetValFsLldpConfiguredMgmtIpv6Address->i4_Length));
    gLldpGlobalInfo.MgmtAddr6.u1AddrLen =
        (UINT1) pSetValFsLldpConfiguredMgmtIpv6Address->i4_Length;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLldpLocPortIdSubtype
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                testValFsLldpLocPortIdSubtype
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLldpLocPortIdSubtype (UINT4 *pu4ErrorCode, INT4 i4LldpLocPortNum,
                                 INT4 i4TestValFsLldpLocPortIdSubtype)
{
    tLldpLocPortTable  *pPortEntry = NULL;
    tLldpLocPortTable  *pTmpPortEntry = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "LLDP should be started before accessing its "
                  "MIB objects !!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4LldpLocPortNum < LLDP_MIN_PORTS)
        || ((UINT4) i4LldpLocPortNum > LLDP_MAX_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((pTmpPortEntry = (tLldpLocPortTable *)
         MemAllocMemBlk (gLldpGlobalInfo.LocPortTablePoolId)) == NULL)
    {
        LLDP_TRC ((LLDP_CRITICAL_TRC | ALL_FAILURE_TRC),
                  "LldpTxUtlSetPortId: Failed to Allocate Memory "
                  "for LldpLocPortTable node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }
    MEMSET (pTmpPortEntry, 0, sizeof (tLldpLocPortTable));

    pTmpPortEntry->i4IfIndex = i4LldpLocPortNum;
    pPortEntry = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree,
                   (tRBElem *) pTmpPortEntry);
    if (pPortEntry == NULL)        /*For Klocwork */
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                (UINT1 *) pTmpPortEntry) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "LldpTxUtlSetPortId: "
                      "Node info Memory release failed!!. \r\n");
        }
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (LldpPortValidatePortIdSubType (i4TestValFsLldpLocPortIdSubtype) !=
        OSIX_SUCCESS)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                (UINT1 *) pTmpPortEntry) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "LldpTxUtlSetPortId: "
                      "Node info Memory release failed!!. \r\n");
        }
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                            (UINT1 *) pTmpPortEntry) == MEM_FAILURE)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpTxUtlSetPortId: "
                  "Node info Memory release failed!!. \r\n");
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLldpLocPortId
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                testValFsLldpLocPortId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLldpLocPortId (UINT4 *pu4ErrorCode, INT4 i4LldpLocPortNum,
                          tSNMP_OCTET_STRING_TYPE * pTestValFsLldpLocPortId)
{
    tLldpLocPortTable  *pPortEntry = NULL;
    tLldpLocPortTable   PortEntry;
    INT4                i4PortIdSubType = LLDP_INVALID_VALUE;

    MEMSET (&PortEntry, 0, sizeof (tLldpLocPortTable));

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " LLDP should be started before accessing its "
                  "MIB objects !!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4LldpLocPortNum < LLDP_MIN_PORTS)
        || ((UINT4) i4LldpLocPortNum > LLDP_MAX_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* get local port entry */
    PortEntry.i4IfIndex = i4LldpLocPortNum;
    pPortEntry = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, (tRBElem *) & PortEntry);

    if (pPortEntry == NULL)        /*For Klocwork */
    {
        return SNMP_FAILURE;
    }

    if ((pTestValFsLldpLocPortId->i4_Length < LLDP_MIN_LEN_PORTID)
        || (pTestValFsLldpLocPortId->i4_Length > LLDP_MAX_LEN_PORTID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /* before calling nmhTestv2FsLldpLocPortId,  nmhSetFsLldpLocPortIdSubtype
     * should have been called so that the newly subtype can be used here
     * for subtype validation */
    /* get current port id subtype */
    i4PortIdSubType = pPortEntry->i4LocPortIdSubtype;
    /* if chassis id subtype is ifalias/ifname/mwaddr/macaddr/agentcircuitid 
     * return failure */
    if ((i4PortIdSubType == LLDP_PORT_ID_SUB_IF_ALIAS) ||
        (i4PortIdSubType == LLDP_PORT_ID_SUB_IF_NAME) ||
        (i4PortIdSubType == LLDP_PORT_ID_SUB_MAC_ADDR) ||
        (i4PortIdSubType == LLDP_PORT_ID_SUB_NW_ADDR) ||
        (i4PortIdSubType == LLDP_PORT_ID_SUB_AGENT_CKT_ID))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLldpPortConfigNotificationType
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                testValFsLldpPortConfigNotificationType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLldpPortConfigNotificationType (UINT4 *pu4ErrorCode,
                                           INT4 i4LldpLocPortNum,
                                           INT4
                                           i4TestValFsLldpPortConfigNotificationType)
{
    tLldpLocPortInfo   *pPortEntry = NULL;
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "LLDP should be started before accessing its "
                  "MIB objects !!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsLldpPortConfigNotificationType !=
         LLDP_REMOTE_CHG_NOTIFICATION) &&
        (i4TestValFsLldpPortConfigNotificationType !=
         LLDP_MIS_CFG_NOTIFICATION) &&
        (i4TestValFsLldpPortConfigNotificationType !=
         LLDP_REMOTE_CHG_MIS_CFG_NOTIFICATION))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    pPortEntry = LLDP_GET_LOC_PORT_INFO (i4LldpLocPortNum);
    if (pPortEntry == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpTxUtlGetLocPortEntry is failed \n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLldpLocPortDstMac
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                testValFsLldpLocPortDstMac
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLldpLocPortDstMac (UINT4 *pu4ErrorCode, INT4 i4LldpLocPortNum,
                              tMacAddr TestValFsLldpLocPortDstMac)
{
    tLldpLocPortTable  *pPortEntry = NULL;
    tLldpLocPortTable   PortEntry;
    tMacAddr            zeroAddr;

    MEMSET (&PortEntry, 0, sizeof (tLldpLocPortTable));
    MEMSET (zeroAddr, 0, MAC_ADDR_LEN);

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "LLDP should be started before accessing its "
                  "MIB objects !!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
    {
        return SNMP_FAILURE;
    }

    /* Check for Destination mac address with all zeros */
    if ((MEMCMP (TestValFsLldpLocPortDstMac, zeroAddr, MAC_ADDR_LEN) == 0))
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  "\r\nDestination Mac address cannot be given with all zeros\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    PortEntry.i4IfIndex = i4LldpLocPortNum;
    pPortEntry = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &PortEntry);
    if (pPortEntry == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpTxUtlGetLocPortEntry is failed \n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLldpMedAdminStatus
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                testValFsLldpMedAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLldpMedAdminStatus (UINT4 *pu4ErrorCode, INT4 i4LldpLocPortNum,
                               INT4 i4TestValFsLldpMedAdminStatus)
{
    tLldpLocPortTable  *pLocPortTable = NULL;
    tLldpLocPortTable   LocPortTable;
    tLldpLocPortInfo   *pLocPortInfo = NULL;
    INT4                i4AdminStatus = 0;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC, "LLDP System ShutDown\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsLldpMedAdminStatus != LLDP_ENABLED) &&
        (i4TestValFsLldpMedAdminStatus != LLDP_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    LocPortTable.i4IfIndex = i4LldpLocPortNum;
    pLocPortTable = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LocPortTable);
    if (pLocPortTable == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC, "pLocPortTable is NULL\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    pLocPortInfo = LLDP_GET_LOC_PORT_INFO ((UINT4) i4LldpLocPortNum);
    if (pLocPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Get AdminStatus for checking TX and RX */
    if (nmhGetLldpV2PortConfigAdminStatus (i4LldpLocPortNum,
                                           pLocPortInfo->u4DstMacAddrTblIndex,
                                           &i4AdminStatus) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (i4TestValFsLldpMedAdminStatus == LLDP_ENABLED)
    {
        if (i4AdminStatus != LLDP_ADM_STAT_TX_AND_RX)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLldpMemAllocFailure
 Input       :  The Indices

                The Object 
                retValFsLldpMemAllocFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLldpMemAllocFailure (INT4 *pi4RetValFsLldpMemAllocFailure)
{
    *pi4RetValFsLldpMemAllocFailure = LLDP_ERR_CNTR_MEM_ALLOC_FAIL;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLldpInputQOverFlows
 Input       :  The Indices

                The Object 
                retValFsLldpInputQOverFlows
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLldpInputQOverFlows (INT4 *pi4RetValFsLldpInputQOverFlows)
{
    *pi4RetValFsLldpInputQOverFlows = LLDP_ERR_CNTR_INPUT_Q_OVERFLOW;
    return SNMP_SUCCESS;
}

/****************************************************************************
  Function    :  nmhGetFsLldpStatsRemTablesUpdates
 Input       :  The Indices

                The Object 
                retValFsLldpStatsRemTablesUpdates
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLldpStatsRemTablesUpdates (UINT4 *pu4RetValFsLldpStatsRemTablesUpdates)
{
    *pu4RetValFsLldpStatsRemTablesUpdates =
        (UINT4) LLDP_REMOTE_STAT_TABLES_UPDATES;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLldpClearStats
 Input       :  The Indices

                The Object
                retValFsLldpClearStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLldpClearStats (INT4 *pi4RetValFsLldpClearStats)
{
    *pi4RetValFsLldpClearStats = gLldpGlobalInfo.bClearStats;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLldpClearStats
 Input       :  The Indices

                The Object
                setValFsLldpClearStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLldpClearStats (INT4 i4SetValFsLldpClearStats)
{
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    tLldpLocPortInfo    LldpLocPortInfo;

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));

    if (i4SetValFsLldpClearStats == LLDP_SET_CLEAR_COUNTER)
    {
        gLldpGlobalInfo.bClearStats = LLDP_SET_CLEAR_COUNTER;

        pLldpLocPortInfo = (tLldpLocPortInfo *)
            RBTreeGetFirst (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree);

        /* Clears LLDP statistics info for every ports */
        while (pLldpLocPortInfo != NULL)
        {
            LLDP_RESET_ALL_COUNTERS (pLldpLocPortInfo);
            LLDP_RESET_CNTR_TX_FRAMES (pLldpLocPortInfo);
            LldpLocPortInfo.u4LocPortNum = pLldpLocPortInfo->u4LocPortNum;
            pLldpLocPortInfo = (tLldpLocPortInfo *)
                RBTreeGetNext (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
                               &LldpLocPortInfo, LldpAgentInfoUtlRBCmpInfo);
        }
        gLldpGlobalInfo.bClearStats = LLDP_NO_SET_CLEAR_COUNTER;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLldpClearStats
 Input       :  The Indices

                The Object
                testValFsLldpClearStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLldpClearStats (UINT4 *pu4ErrorCode, INT4 i4TestValFsLldpClearStats)
{
    if ((i4TestValFsLldpClearStats != LLDP_SET_CLEAR_COUNTER) &&
        (i4TestValFsLldpClearStats != LLDP_NO_SET_CLEAR_COUNTER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsLldpClearStats
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLldpClearStats (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  LldpNotifyProtocolShutdownStatus
 Input       :  None
 Output      :  None
 Returns     :  None
****************************************************************************/

VOID
LldpNotifyProtocolShutdownStatus (VOID)
{
#ifdef ISS_WANTED
    UINT1               au1ObjectOid[3][SNMP_MAX_OID_LENGTH];

    /* The oid list contains the list of Oids that has been registered 
     * for the protocol + the oid of the SystemControl object. */
    SNMPGetOidString (fslldp, (sizeof (fslldp) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    SNMPGetOidString (stdlld, (sizeof (stdlld) / sizeof (UINT4)),
                      au1ObjectOid[1]);
    SNMPGetOidString (slldv2, (sizeof (slldv2) / sizeof (UINT4)),
                      au1ObjectOid[2]);
    MSR_NOTIFY_PROTOCOL_SHUT (au1ObjectOid, 3, MSR_INVALID_CNTXT);

    MEMSET (&au1ObjectOid, 0, sizeof (au1ObjectOid));
    SNMPGetOidString (sd1lv2, (sizeof (sd1lv2) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    SNMPGetOidString (sd3lv2, (sizeof (sd3lv2) / sizeof (UINT4)),
                      au1ObjectOid[1]);
    /* As the stdot1 and stdot3 are child objects of stdlldp mib, 
     * these objects are not sent to the MSR */

    SNMPGetOidString (FsLldpSystemControl,
                      (sizeof (FsLldpSystemControl) / sizeof (UINT4)),
                      au1ObjectOid[2]);

    /* Send a notification to MSR to process the lldp shutdown, with 
     * lldp oids and its system control object 
     * As the protocol does not have MI support, invalid context id is sent */
    MSR_NOTIFY_PROTOCOL_SHUT (au1ObjectOid, 3, MSR_INVALID_CNTXT);
#endif
    return;
}

/* LOW LEVEL Routines for Table : FsLldpManAddrConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLldpManAddrConfigTable
 Input       :  The Indices
                LldpLocManAddrSubtype
                LldpLocManAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsLldpManAddrConfigTable (INT4 i4LldpLocManAddrSubtype,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pLldpLocManAddr)
{
    return nmhValidateIndexInstanceLldpLocManAddrTable
        (i4LldpLocManAddrSubtype, pLldpLocManAddr);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsLldpManAddrConfigTable
 Input       :  The Indices
                LldpLocManAddrSubtype
                LldpLocManAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsLldpManAddrConfigTable (INT4 *pi4LldpLocManAddrSubtype,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pLldpLocManAddr)
{
    return nmhGetFirstIndexLldpLocManAddrTable
        (pi4LldpLocManAddrSubtype, pLldpLocManAddr);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsLldpManAddrConfigTable
 Input       :  The Indices
                LldpLocManAddrSubtype
                nextLldpLocManAddrSubtype
                LldpLocManAddr
                nextLldpLocManAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsLldpManAddrConfigTable (INT4 i4LldpLocManAddrSubtype,
                                         INT4 *pi4NextLldpLocManAddrSubtype,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pLldpLocManAddr,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNextLldpLocManAddr)
{
    return nmhGetNextIndexLldpLocManAddrTable
        (i4LldpLocManAddrSubtype, pi4NextLldpLocManAddrSubtype,
         pLldpLocManAddr, pNextLldpLocManAddr);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLldpManAddrConfigOperStatus
 Input       :  The Indices
                LldpLocManAddrSubtype
                LldpLocManAddr

                The Object 
                retValFsLldpManAddrConfigOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLldpManAddrConfigOperStatus (INT4 i4LldpLocManAddrSubtype,
                                     tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr,
                                     INT4
                                     *pi4RetValFsLldpManAddrConfigOperStatus)
{
    tLldpLocManAddrTable *pLocManAddrNode = NULL;
    UINT1               au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR] = { 0 };

    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    MEMSET (&au1LocManAddr[0], 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMCPY (&au1LocManAddr[0], pLldpLocManAddr->pu1_OctetList,
            pLldpLocManAddr->i4_Length);

    pLocManAddrNode = (tLldpLocManAddrTable *)
        LldpTxUtlGetLocManAddrNode (i4LldpLocManAddrSubtype, &au1LocManAddr[0]);

    if (pLocManAddrNode == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsLldpManAddrConfigOperStatus = pLocManAddrNode->u1OperStatus;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFslldpv2Version
 Input       :  The Indices

                The Object
                retValFslldpv2Version
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFslldpv2Version (INT4 *pi4RetValFslldpv2Version)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        *pi4RetValFslldpv2Version = LLDP_VERSION_05;
        return SNMP_SUCCESS;
    }
    *pi4RetValFslldpv2Version = gLldpGlobalInfo.i4Version;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFslldpv2Version
 Input       :  The Indices

                The Object
                setValFslldpv2Version
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFslldpv2Version (INT4 i4SetValFslldpv2Version)
{
    tMacAddr            DstMac;
    tLldpLocPortInfo    PortInfo;
    tLldpLocPortInfo   *pPortEntry = NULL;
    tLldpv2DestAddrTbl *pDestAddrTbl = NULL;
    tLldpv2DestAddrTbl  DestAddrTbl;
    UINT1               au1LldpMcastAddr[MAC_ADDR_LEN] =
        { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x0E };
    UINT4               u4DestIndex = 0;
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;
    tLldpLocPortTable   LocPortTable;
    tLldpLocPortTable  *pLocPortTable = NULL;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    UINT1               u1FatalError = OSIX_FALSE;
    MEMSET (&PortInfo, 0, sizeof (tLldpLocPortInfo));
    MEMSET (&LocPortTable, 0, sizeof (tLldpLocPortTable));
    MEMSET (&DstMac, 0, sizeof (tMacAddr));
    MEMSET (&DestAddrTbl, 0, sizeof (tLldpv2DestAddrTbl));

    if (gLldpGlobalInfo.i4Version != i4SetValFslldpv2Version)
    {
        if (i4SetValFslldpv2Version == LLDP_VERSION_05)
        {
            gLldpGlobalInfo.SysConfigInfo.u4TxCreditMax =
                LLDP_DEFAULT_CREDIT_MAX;
            gLldpGlobalInfo.SysConfigInfo.u4MessageFastTx =
                LLDP_DEFAULT_FAST_TX;
            gLldpGlobalInfo.SysConfigInfo.u4TxFastInit =
                LLDP_DEFAULT_TX_FAST_INIT;
            gLldpGlobalInfo.SysConfigInfo.i4NotificationInterval =
                LLDP_NOTIFICATION_INTERVAL;
            pLocPortTable =
                (tLldpLocPortTable *) RBTreeGetFirst (gLldpGlobalInfo.
                                                      LldpPortInfoRBTree);

            while (pLocPortTable != NULL)
            {
                pDestAddrTbl = (tLldpv2DestAddrTbl *)
                    RBTreeGetFirst (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree);
                while (pDestAddrTbl != NULL)
                {
                    if (MEMCMP (pDestAddrTbl->Lldpv2DestMacAddress,
                                au1LldpMcastAddr, MAC_ADDR_LEN) != 0)
                    {
                        u4DestIndex = pDestAddrTbl->u4LlldpV2DestAddrTblIndex;
                        pLldpAgentPortInfo =
                            LldpGetPortMapTableNode ((UINT4) pLocPortTable->
                                                     i4IfIndex,
                                                     &pDestAddrTbl->
                                                     Lldpv2DestMacAddress);
                        if ((pLldpAgentPortInfo != NULL) && (u4DestIndex != 0))
                        {
                            LldpUtilSetDstMac (pLocPortTable->i4IfIndex,
                                               (UINT1 *) &(pDestAddrTbl->
                                                           Lldpv2DestMacAddress),
                                               LLDP_DESTROY, &u1FatalError);
                        }
                    }
                    DestAddrTbl.u4LlldpV2DestAddrTblIndex =
                        pDestAddrTbl->u4LlldpV2DestAddrTblIndex;
                    pDestAddrTbl = (tLldpv2DestAddrTbl *)
                        RBTreeGetNext (gLldpGlobalInfo.
                                       Lldpv2DestMacAddrTblRBTree, &DestAddrTbl,
                                       LldpDstMacAddrUtlRBCmpInfo);
                }
                LocPortTable.i4IfIndex = pLocPortTable->i4IfIndex;
                pLocPortTable = (tLldpLocPortTable *)
                    RBTreeGetNext (gLldpGlobalInfo.LldpPortInfoRBTree,
                                   &LocPortTable, LldpPortInfoUtlRBCmpInfo);
            }

        }
        else
        {
            gLldpGlobalInfo.SysConfigInfo.u4TxCreditMax =
                LLDPV2_DEFAULT_CREDIT_MAX;
            gLldpGlobalInfo.SysConfigInfo.u4MessageFastTx =
                LLDPV2_DEFAULT_MESSAGE_FAST_TX;
            gLldpGlobalInfo.SysConfigInfo.u4TxFastInit =
                LLDPV2_DEFAULT_TX_FAST_INIT;

            gLldpGlobalInfo.SysConfigInfo.u4MedFastStart =
                LLDPV2_DEFAULT_MED_FAST_REPEAT_COUNT;

            gLldpGlobalInfo.SysConfigInfo.i4NotificationInterval =
                LLDPV2_NOTIFICATION_INTERVAL;
            pLldpLocalPortInfo =
                (tLldpLocPortInfo *) RBTreeGetFirst (gLldpGlobalInfo.
                                                     LldpLocPortAgentInfoRBTree);
            while (pLldpLocalPortInfo != NULL)
            {
                pLldpLocalPortInfo->u1Credit = LLDPV2_DEFAULT_CREDIT_MAX;
                pLldpLocalPortInfo = (tLldpLocPortInfo *) RBTreeGetNext
                    (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
                     (tRBElem *) pLldpLocalPortInfo, LldpAgentInfoUtlRBCmpInfo);
            }
        }
        pPortEntry = (tLldpLocPortInfo *)
            RBTreeGetFirst (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree);
        while (pPortEntry != NULL)
        {
            LldpRxUtlDelRemInfoForPort (pPortEntry);
            if ((gLldpGlobalInfo.u1ModuleStatus == LLDP_ENABLED) &&
                ((pPortEntry->PortConfigTable.i4AdminStatus ==
                  LLDP_ADM_STAT_TX_ONLY) ||
                 (pPortEntry->PortConfigTable.i4AdminStatus ==
                  LLDP_ADM_STAT_TX_AND_RX)))
            {
                /* Ongoing LLDP Tx should happen with new
                 * LLDP packet construction based on the version.
                 */
                pPortEntry->bSomethingChangedLocal = OSIX_TRUE;
            }

            PortInfo.u4LocPortNum = pPortEntry->u4LocPortNum;
            pPortEntry = (tLldpLocPortInfo *)
                RBTreeGetNext (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
                               &PortInfo, LldpAgentInfoUtlRBCmpInfo);
        }
    }

    gLldpGlobalInfo.i4Version = i4SetValFslldpv2Version;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fslldpv2Version
 Input       :  The Indices

                The Object
                testValFslldpv2Version
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fslldpv2Version (UINT4 *pu4ErrorCode, INT4 i4TestValFslldpv2Version)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC, "LLDP System ShutDown\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFslldpv2Version == LLDP_VERSION_05) ||
        (i4TestValFslldpv2Version == LLDP_VERSION_09))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsLldpTraceLevel
 Input       :  The Indices

                The Object 
                testValFsLldpTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLldpTraceLevel (UINT4 *pu4ErrorCode, INT4 i4TestValFsLldpTraceLevel)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC, "LLDP System ShutDown\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (i4TestValFsLldpTraceLevel < LLDP_INVALID_TRC)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLldpTagStatus
 Input       :  The Indices

                The Object 
                testValFsLldpTagStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLldpTagStatus (UINT4 *pu4ErrorCode, INT4 i4TestValFsLldpTagStatus)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC, "LLDP System ShutDown\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsLldpTagStatus != LLDP_TAG_ENABLE) &&
        (i4TestValFsLldpTagStatus != LLDP_TAG_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsLldpConfiguredMgmtIpv4Address
 Input       :  The Indices

                The Object 
                testValFsLldpConfiguredMgmtIpv4Address
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLldpConfiguredMgmtIpv4Address (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pTestValFsLldpConfiguredMgmtIpv4Address)
{
    UINT4               u4IpAddr = 0;
    /* Only for IPv4 Address */
    if (pTestValFsLldpConfiguredMgmtIpv4Address->i4_Length ==
        IPVX_IPV4_ADDR_LEN)
    {
        MEMCPY (&u4IpAddr,
                pTestValFsLldpConfiguredMgmtIpv4Address->pu1_OctetList,
                pTestValFsLldpConfiguredMgmtIpv4Address->i4_Length);

        u4IpAddr = OSIX_HTONL (u4IpAddr);

        if ((IP_IS_ADDR_MCAST (u4IpAddr) == IP_SUCCESS) ||
            (IP_IS_ADDR_RESV_MCAST (u4IpAddr) == IP_SUCCESS) ||
            (u4IpAddr == IP_GEN_BCAST_ADDR))
        {
            CLI_SET_ERR (LLDP_CLI_INVALID_IP_ADDRESS);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    CLI_SET_ERR (LLDP_CLI_INVALID_IP_ADDRESS);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsLldpConfiguredMgmtIpv6Address
 Input       :  The Indices

                The Object 
                testValFsLldpConfiguredMgmtIpv6Address
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLldpConfiguredMgmtIpv6Address (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pTestValFsLldpConfiguredMgmtIpv6Address)
{
    tIp6Addr            ip6addr;

    /* MEMSET for ip6addr */
    MEMSET (&ip6addr, 0, sizeof (tIp6Addr));

    if (pTestValFsLldpConfiguredMgmtIpv6Address->i4_Length ==
        IPVX_IPV6_ADDR_LEN)
    {
        Ip6AddrCopy (&ip6addr,
                     (tIp6Addr *) (VOID *)
                     pTestValFsLldpConfiguredMgmtIpv6Address->pu1_OctetList);

        if (IS_ADDR_MULTI (ip6addr) || IS_ADDR_LOOPBACK (ip6addr))
        {
            /* Invalid IPv6 Address. */
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (LLDP_CLI_INVALID_IP6_ADDRESS);
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    CLI_SET_ERR (LLDP_CLI_INVALID_IP6_ADDRESS);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fslldpv2Version
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fslldpv2Version (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLldpTraceLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLldpTraceLevel (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLldpTagStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLldpTagStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLldpConfiguredMgmtIpv4Address
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLldpConfiguredMgmtIpv4Address (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLldpConfiguredMgmtIpv6Address
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLldpConfiguredMgmtIpv6Address (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fslldpv2ConfigPortMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFslldpv2ConfigPortMapTable
 Input       :  The Indices
                Fslldpv2ConfigPortMapIfIndex
                Fslldpv2ConfigPortMapDestMacAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFslldpv2ConfigPortMapTable (INT4
                                                    i4Fslldpv2ConfigPortMapIfIndex,
                                                    tMacAddr
                                                    Fslldpv2ConfigPortMapDestMacAddress)
{
    tLldpv2AgentToLocPort LldpAgentPortInfo;
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return SNMP_FAILURE;
    }

    LldpAgentPortInfo.i4IfIndex = i4Fslldpv2ConfigPortMapIfIndex;
    MEMCPY (LldpAgentPortInfo.Lldpv2DestMacAddress,
            Fslldpv2ConfigPortMapDestMacAddress, MAC_ADDR_LEN);

    pLldpAgentPortInfo = (tLldpv2AgentToLocPort *) RBTreeGet
        (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
         (tRBElem *) & LldpAgentPortInfo);

    if (pLldpAgentPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFslldpv2ConfigPortMapTable
 Input       :  The Indices
                Fslldpv2ConfigPortMapIfIndex
                Fslldpv2ConfigPortMapDestMacAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFslldpv2ConfigPortMapTable (INT4
                                            *pi4Fslldpv2ConfigPortMapIfIndex,
                                            tMacAddr *
                                            pFslldpv2ConfigPortMapDestMacAddress)
{
    tMacAddr            MacAddr;

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    return (nmhGetNextIndexFslldpv2ConfigPortMapTable (LLDP_ZERO,
                                                       pi4Fslldpv2ConfigPortMapIfIndex,
                                                       MacAddr,
                                                       pFslldpv2ConfigPortMapDestMacAddress));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFslldpv2ConfigPortMapTable
 Input       :  The Indices
                Fslldpv2ConfigPortMapIfIndex
                nextFslldpv2ConfigPortMapIfIndex
                Fslldpv2ConfigPortMapDestMacAddress
                nextFslldpv2ConfigPortMapDestMacAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFslldpv2ConfigPortMapTable (INT4 i4Fslldpv2ConfigPortMapIfIndex,
                                           INT4
                                           *pi4NextFslldpv2ConfigPortMapIfIndex,
                                           tMacAddr
                                           Fslldpv2ConfigPortMapDestMacAddress,
                                           tMacAddr *
                                           pNextFslldpv2ConfigPortMapDestMacAddress)
{
    tLldpv2AgentToLocPort LldpAgentPortInfo;
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return SNMP_FAILURE;
    }

    LldpAgentPortInfo.i4IfIndex = i4Fslldpv2ConfigPortMapIfIndex;
    MEMCPY (LldpAgentPortInfo.Lldpv2DestMacAddress,
            Fslldpv2ConfigPortMapDestMacAddress, MAC_ADDR_LEN);

    pLldpAgentPortInfo = (tLldpv2AgentToLocPort *) RBTreeGetNext
        (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
         (tRBElem *) & LldpAgentPortInfo, LldpAgentToLocPortUtlRBCmpInfo);

    if (pLldpAgentPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFslldpv2ConfigPortMapIfIndex = pLldpAgentPortInfo->i4IfIndex;
    MEMCPY (pNextFslldpv2ConfigPortMapDestMacAddress,
            pLldpAgentPortInfo->Lldpv2DestMacAddress, MAC_ADDR_LEN);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFslldpv2ConfigPortMapNum
 Input       :  The Indices
                Fslldpv2ConfigPortMapIfIndex
                Fslldpv2ConfigPortMapDestMacAddress

                The Object
                retValFslldpv2ConfigPortMapNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFslldpv2ConfigPortMapNum (INT4 i4Fslldpv2ConfigPortMapIfIndex,
                                tMacAddr Fslldpv2ConfigPortMapDestMacAddress,
                                INT4 *pi4RetValFslldpv2ConfigPortMapNum)
{
    tLldpv2AgentToLocPort LldpAgentPortInfo;
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;

    MEMSET (&LldpAgentPortInfo, 0, sizeof (tLldpv2AgentToLocPort));
    LldpAgentPortInfo.i4IfIndex = i4Fslldpv2ConfigPortMapIfIndex;
    MEMCPY (LldpAgentPortInfo.Lldpv2DestMacAddress,
            Fslldpv2ConfigPortMapDestMacAddress, MAC_ADDR_LEN);

    pLldpAgentPortInfo = (tLldpv2AgentToLocPort *) RBTreeGet
        (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
         (tRBElem *) & LldpAgentPortInfo);

    if (pLldpAgentPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFslldpv2ConfigPortMapNum =
        (INT4) pLldpAgentPortInfo->u4LldpLocPort;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFslldpv2ConfigPortRowStatus
 Input       :  The Indices
                Fslldpv2ConfigPortMapIfIndex
                Fslldpv2ConfigPortMapDestMacAddress

                The Object
                retValFslldpv2ConfigPortRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFslldpv2ConfigPortRowStatus (INT4 i4Fslldpv2ConfigPortMapIfIndex,
                                   tMacAddr Fslldpv2ConfigPortMapDestMacAddress,
                                   INT4 *pi4RetValFslldpv2ConfigPortRowStatus)
{
    tLldpv2AgentToLocPort LldpAgentPortInfo;
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;

    MEMSET (&LldpAgentPortInfo, 0, sizeof (tLldpv2AgentToLocPort));
    LldpAgentPortInfo.i4IfIndex = i4Fslldpv2ConfigPortMapIfIndex;
    MEMCPY (LldpAgentPortInfo.Lldpv2DestMacAddress,
            Fslldpv2ConfigPortMapDestMacAddress, MAC_ADDR_LEN);

    pLldpAgentPortInfo = (tLldpv2AgentToLocPort *) RBTreeGet
        (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
         (tRBElem *) & LldpAgentPortInfo);

    if (pLldpAgentPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFslldpv2ConfigPortRowStatus =
        (INT4) pLldpAgentPortInfo->u1RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFslldpv2ConfigPortRowStatus
 Input       :  The Indices
                Fslldpv2ConfigPortMapIfIndex
                Fslldpv2ConfigPortMapDestMacAddress

                The Object
                setValFslldpv2ConfigPortRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFslldpv2ConfigPortRowStatus (INT4 i4Fslldpv2ConfigPortMapIfIndex,
                                   tMacAddr Fslldpv2ConfigPortMapDestMacAddress,
                                   INT4 i4SetValFslldpv2ConfigPortRowStatus)
{
    UINT4               u4RetStatus;
    tMacAddr            MacAddress;
    tLldpv2AgentToLocPort LldpAgentPortInfo;
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;

    switch (i4SetValFslldpv2ConfigPortRowStatus)
    {
        case LLDP_CREATE_AND_GO:
        case LLDP_CREATE_AND_WAIT:
            u4RetStatus =
                LldpCreateDestMacAddressEnty (i4Fslldpv2ConfigPortMapIfIndex,
                                              Fslldpv2ConfigPortMapDestMacAddress);
            if (u4RetStatus == OSIX_FAILURE)
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "Entry Already Created \r\n");
                return SNMP_FAILURE;
            }
            MEMSET (&MacAddress, 0, sizeof (tMacAddr));
            MEMCPY (&MacAddress, Fslldpv2ConfigPortMapDestMacAddress,
                    MAC_ADDR_LEN);
            pLldpAgentPortInfo =
                LldpGetPortMapTableNode ((UINT4) i4Fslldpv2ConfigPortMapIfIndex,
                                         &MacAddress);
            if (pLldpAgentPortInfo == NULL)
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC, "Entry not Created \r\n");
                return SNMP_FAILURE;
            }
            if (i4SetValFslldpv2ConfigPortRowStatus == LLDP_CREATE_AND_GO)
            {
                pLldpAgentPortInfo->u1RowStatus = LLDP_ACTIVE;
            }
            else
            {
                pLldpAgentPortInfo->u1RowStatus = LLDP_NOT_READY;
            }

            return SNMP_SUCCESS;

        case LLDP_ACTIVE:
        case LLDP_NOT_IN_SERVICE:
            MEMSET (&LldpAgentPortInfo, 0, sizeof (tLldpv2AgentToLocPort));
            LldpAgentPortInfo.i4IfIndex = i4Fslldpv2ConfigPortMapIfIndex;
            MEMCPY (LldpAgentPortInfo.Lldpv2DestMacAddress,
                    Fslldpv2ConfigPortMapDestMacAddress, MAC_ADDR_LEN);
            pLldpAgentPortInfo =
                (tLldpv2AgentToLocPort *) RBTreeGet (gLldpGlobalInfo.
                                                     Lldpv2AgentToLocPortMapTblRBTree,
                                                     (tRBElem *) &
                                                     LldpAgentPortInfo);
            if (pLldpAgentPortInfo == NULL)
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC, "Entry not Created \r\n");
                return SNMP_FAILURE;
            }
            if (pLldpAgentPortInfo->u1RowStatus ==
                (UINT1) i4SetValFslldpv2ConfigPortRowStatus)
            {
                return SNMP_SUCCESS;
            }
            pLldpAgentPortInfo->u1RowStatus =
                (UINT1) i4SetValFslldpv2ConfigPortRowStatus;
            return SNMP_SUCCESS;

        case LLDP_DESTROY:
            MEMSET (&LldpAgentPortInfo, 0, sizeof (tLldpv2AgentToLocPort));
            LldpAgentPortInfo.i4IfIndex = i4Fslldpv2ConfigPortMapIfIndex;
            MEMCPY (LldpAgentPortInfo.Lldpv2DestMacAddress,
                    Fslldpv2ConfigPortMapDestMacAddress, MAC_ADDR_LEN);
            pLldpAgentPortInfo =
                (tLldpv2AgentToLocPort *) RBTreeGet (gLldpGlobalInfo.
                                                     Lldpv2AgentToLocPortMapTblRBTree,
                                                     (tRBElem *) &
                                                     LldpAgentPortInfo);
            if (pLldpAgentPortInfo == NULL)
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC, "Entry not Created \r\n");
                return SNMP_FAILURE;
            }
            u4RetStatus =
                (UINT4)
                LldpDeleteDestMacAddrEntry (i4Fslldpv2ConfigPortMapIfIndex,
                                            Fslldpv2ConfigPortMapDestMacAddress);
            if (u4RetStatus == OSIX_FAILURE)
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC, "Unable to Delete \r\n");
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fslldpv2ConfigPortRowStatus
 Input       :  The Indices
                Fslldpv2ConfigPortMapIfIndex
                Fslldpv2ConfigPortMapDestMacAddress

                The Object
                testValFslldpv2ConfigPortRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fslldpv2ConfigPortRowStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4Fslldpv2ConfigPortMapIfIndex,
                                      tMacAddr
                                      Fslldpv2ConfigPortMapDestMacAddress,
                                      INT4 i4TestValFslldpv2ConfigPortRowStatus)
{
    tLldpv2AgentToLocPort LldpAgentPortInfo;
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;

    MEMSET (&LldpAgentPortInfo, 0, sizeof (tLldpv2AgentToLocPort));

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }

    LldpAgentPortInfo.i4IfIndex = i4Fslldpv2ConfigPortMapIfIndex;
    MEMCPY (LldpAgentPortInfo.Lldpv2DestMacAddress,
            Fslldpv2ConfigPortMapDestMacAddress, MAC_ADDR_LEN);
    pLldpAgentPortInfo =
        (tLldpv2AgentToLocPort *) RBTreeGet (gLldpGlobalInfo.
                                             Lldpv2AgentToLocPortMapTblRBTree,
                                             (tRBElem *) & LldpAgentPortInfo);

    if ((i4TestValFslldpv2ConfigPortRowStatus != LLDP_ACTIVE) &&
        (i4TestValFslldpv2ConfigPortRowStatus != LLDP_NOT_IN_SERVICE) &&
        (i4TestValFslldpv2ConfigPortRowStatus != LLDP_CREATE_AND_GO) &&
        (i4TestValFslldpv2ConfigPortRowStatus != LLDP_CREATE_AND_WAIT) &&
        (i4TestValFslldpv2ConfigPortRowStatus != LLDP_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValFslldpv2ConfigPortRowStatus)
    {
        case LLDP_ACTIVE:
        case LLDP_NOT_IN_SERVICE:
        case LLDP_DESTROY:
            if (pLldpAgentPortInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC, "Entry not Created \r\n");
                return SNMP_FAILURE;
            }
            break;

        case LLDP_CREATE_AND_WAIT:
        default:                /* LLDP_CREATE_AND_GO Case */
            if (LldpIsDcbxApplicationRegistered ((UINT4)
                                                 i4Fslldpv2ConfigPortMapIfIndex)
                != OSIX_FALSE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (LLDP_CLI_DCBX_REGISTERED);
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "DCBX application is registered on this port"
                          "Hence, multiple agent is not allowed \r\n");
                return SNMP_FAILURE;
            }

            if (pLldpAgentPortInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "Entry Already Created \r\n");
                return SNMP_FAILURE;
            }
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fslldpv2ConfigPortMapTable
 Input       :  The Indices
                Fslldpv2ConfigPortMapIfIndex
                Fslldpv2ConfigPortMapDestMacAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fslldpv2ConfigPortMapTable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FslldpV2DestAddressTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFslldpV2DestAddressTable
 Input       :  The Indices
                FslldpV2AddressTableIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFslldpV2DestAddressTable (UINT4
                                                  u4FslldpV2AddressTableIndex)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4FslldpV2AddressTableIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFslldpV2DestAddressTable
 Input       :  The Indices
                FslldpV2AddressTableIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFslldpV2DestAddressTable (UINT4 *pu4FslldpV2AddressTableIndex)
{
    return (nmhGetNextIndexFslldpV2DestAddressTable (LLDP_ZERO,
                                                     pu4FslldpV2AddressTableIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFslldpV2DestAddressTable
 Input       :  The Indices
                FslldpV2AddressTableIndex
                nextFslldpV2AddressTableIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFslldpV2DestAddressTable (UINT4 u4FslldpV2AddressTableIndex,
                                         UINT4
                                         *pu4NextFslldpV2AddressTableIndex)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4FslldpV2AddressTableIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGetNext (gLldpGlobalInfo.
                                              Lldpv2DestMacAddrTblRBTree,
                                              (tRBElem *) & DestTbl,
                                              LldpDstMacAddrUtlRBCmpInfo);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }
    *pu4NextFslldpV2AddressTableIndex = pTempDestTbl->u4LlldpV2DestAddrTblIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFslldpV2DestMacAddress
 Input       :  The Indices
                FslldpV2AddressTableIndex

                The Object
                retValFslldpV2DestMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFslldpV2DestMacAddress (UINT4 u4FslldpV2AddressTableIndex,
                              tMacAddr * pRetValFslldpV2DestMacAddress)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4FslldpV2AddressTableIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }
    MEMCPY (pRetValFslldpV2DestMacAddress, pTempDestTbl->Lldpv2DestMacAddress,
            MAC_ADDR_LEN);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFslldpv2DestRowStatus
 Input       :  The Indices
                FslldpV2AddressTableIndex

                The Object
                retValFslldpv2DestRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFslldpv2DestRowStatus (UINT4 u4FslldpV2AddressTableIndex,
                             INT4 *pi4RetValFslldpv2DestRowStatus)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4FslldpV2AddressTableIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFslldpv2DestRowStatus = (INT4) pTempDestTbl->u1RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFslldpV2DestMacAddress
 Input       :  The Indices
                FslldpV2AddressTableIndex

                The Object
                setValFslldpV2DestMacAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFslldpV2DestMacAddress (UINT4 u4FslldpV2AddressTableIndex,
                              tMacAddr SetValFslldpV2DestMacAddress)
{
    tLldpv2DestAddrTbl *pDestMacAddrTbl = NULL;
    tLldpv2DestAddrTbl  DestMacAddrTbl;

    MEMSET (&DestMacAddrTbl, 0, sizeof (tLldpv2DestAddrTbl));

    DestMacAddrTbl.u4LlldpV2DestAddrTblIndex = u4FslldpV2AddressTableIndex;
    pDestMacAddrTbl = (tLldpv2DestAddrTbl *)
        RBTreeGet (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree, &DestMacAddrTbl);
    if (pDestMacAddrTbl == NULL)
    {
        LLDP_TRC ((MGMT_TRC | ALL_FAILURE_TRC),
                  "DestMacAddrTablEntry not found\r\n");
        return SNMP_FAILURE;
    }
    MEMCPY (pDestMacAddrTbl->Lldpv2DestMacAddress, SetValFslldpV2DestMacAddress,
            MAC_ADDR_LEN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFslldpv2DestRowStatus
 Input       :  The Indices
                FslldpV2AddressTableIndex

                The Object
                setValFslldpv2DestRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFslldpv2DestRowStatus (UINT4 u4FslldpV2AddressTableIndex,
                             INT4 i4SetValFslldpv2DestRowStatus)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    switch (i4SetValFslldpv2DestRowStatus)
    {
        case LLDP_CREATE_AND_GO:
        case LLDP_CREATE_AND_WAIT:
            /*u4RetStatus = LldpUtlCreateDestMacAddrtbl (u4FslldpV2AddressTableIndex); */
            DestTbl.u4LlldpV2DestAddrTblIndex = u4FslldpV2AddressTableIndex;
            pTempDestTbl = (tLldpv2DestAddrTbl *)
                RBTreeGet (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree,
                           &DestTbl);
            if (pTempDestTbl != NULL)
            {
                return SNMP_SUCCESS;
            }
            if (gLldpGlobalInfo.LldpDestMacAddrPoolId == 0)
            {
                return SNMP_FAILURE;
            }
            pTempDestTbl =
                (tLldpv2DestAddrTbl
                 *) (MemAllocMemBlk (gLldpGlobalInfo.LldpDestMacAddrPoolId));
            if (pTempDestTbl == NULL)
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "Memory allocation failed for Destination Mac address table\r\n");
                return SNMP_FAILURE;
            }

            MEMSET (pTempDestTbl, 0, sizeof (tLldpv2DestAddrTbl));
            if (gu4LldpDestMacAddrTblIndex < LLDP_MAX_DEST_ADDR_TBL_INDEX)
            {
                pTempDestTbl->u4LlldpV2DestAddrTblIndex =
                    u4FslldpV2AddressTableIndex;
                gu4LldpDestMacAddrTblIndex++;
            }
            else
            {
                MemReleaseMemBlock (gLldpGlobalInfo.LldpDestMacAddrPoolId,
                                    (UINT1 *) pTempDestTbl);
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "Max agents created already\r\n");
                return SNMP_FAILURE;
            }
            pTempDestTbl->u1RowStatus = NOT_IN_SERVICE;
            if (RBTreeAdd (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree,
                           (tRBElem *) & (pTempDestTbl->DestMacAddrTblNode)) !=
                RB_SUCCESS)
            {
                gu4LldpDestMacAddrTblIndex--;
                MemReleaseMemBlock (gLldpGlobalInfo.LldpDestMacAddrPoolId,
                                    (UINT1 *) pTempDestTbl);
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC, "RBTreeAdd failed\r\n");
                return SNMP_FAILURE;
            }
            if (i4SetValFslldpv2DestRowStatus == LLDP_CREATE_AND_GO)
            {
                pTempDestTbl->u1RowStatus = LLDP_ACTIVE;
            }
            else
            {
                pTempDestTbl->u1RowStatus = LLDP_NOT_READY;
            }
            return SNMP_SUCCESS;

        case LLDP_ACTIVE:
            MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
            DestTbl.u4LlldpV2DestAddrTblIndex = u4FslldpV2AddressTableIndex;
            pTempDestTbl =
                (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                                  Lldpv2DestMacAddrTblRBTree,
                                                  (tRBElem *) & DestTbl);
            if (pTempDestTbl == NULL)
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC, "Entry not Created \r\n");
                return SNMP_FAILURE;
            }
            if (pTempDestTbl->u1RowStatus == i4SetValFslldpv2DestRowStatus)
            {
                return SNMP_SUCCESS;
            }
            pTempDestTbl->u1RowStatus = i4SetValFslldpv2DestRowStatus;
            return SNMP_SUCCESS;

        case LLDP_NOT_IN_SERVICE:
            MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
            DestTbl.u4LlldpV2DestAddrTblIndex = u4FslldpV2AddressTableIndex;
            pTempDestTbl =
                (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                                  Lldpv2DestMacAddrTblRBTree,
                                                  (tRBElem *) & DestTbl);
            if (pTempDestTbl == NULL)
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC, "Entry not Created \r\n");
                return SNMP_FAILURE;
            }
            if (pTempDestTbl->u1RowStatus == i4SetValFslldpv2DestRowStatus)
            {
                return SNMP_SUCCESS;
            }
            pTempDestTbl->u1RowStatus = i4SetValFslldpv2DestRowStatus;
            return SNMP_SUCCESS;

        case LLDP_DESTROY:        /*case LLDP_DESTROY */
            MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
            DestTbl.u4LlldpV2DestAddrTblIndex = u4FslldpV2AddressTableIndex;
            pTempDestTbl = (tLldpv2DestAddrTbl *)
                RBTreeGet (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree,
                           (tRBElem *) & DestTbl);
            if (pTempDestTbl == NULL)
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC, "Entry not Created \r\n");
                return SNMP_FAILURE;
            }
            if (LldpUtlCheckDestMacAddrIndexUsage (u4FslldpV2AddressTableIndex)
                == OSIX_TRUE)
            {
                return SNMP_FAILURE;
            }
            RBTreeRemove (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree,
                          pTempDestTbl);
            MemReleaseMemBlock (gLldpGlobalInfo.LldpDestMacAddrPoolId,
                                (UINT1 *) pTempDestTbl);
            return SNMP_SUCCESS;
        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FslldpV2DestMacAddress
 Input       :  The Indices
                FslldpV2AddressTableIndex

                The Object
                testValFslldpV2DestMacAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FslldpV2DestMacAddress (UINT4 *pu4ErrorCode,
                                 UINT4 u4FslldpV2AddressTableIndex,
                                 tMacAddr TestValFslldpV2DestMacAddress)
{

    tLldpv2DestAddrTbl *pDestMacAddrTbl = NULL;
    tLldpv2DestAddrTbl  DestMacAddrTbl;
    tMacAddr            zeroAddr;

    UNUSED_PARAM (TestValFslldpV2DestMacAddress);
    MEMSET (&DestMacAddrTbl, 0, sizeof (tLldpv2DestAddrTbl));
    MEMSET (zeroAddr, 0, MAC_ADDR_LEN);

    DestMacAddrTbl.u4LlldpV2DestAddrTblIndex = u4FslldpV2AddressTableIndex;
    pDestMacAddrTbl = (tLldpv2DestAddrTbl *)
        RBTreeGet (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree, &DestMacAddrTbl);

    if (pDestMacAddrTbl == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Check for zero destination mac address */
    if ((MEMCMP (TestValFslldpV2DestMacAddress, zeroAddr, MAC_ADDR_LEN) == 0))
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " Destination Mac address cannot be given with all zeros");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4FslldpV2AddressTableIndex > LLDP_MAX_DEST_ADDR_TBL_INDEX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (pDestMacAddrTbl->u1RowStatus == LLDP_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fslldpv2DestRowStatus
 Input       :  The Indices
                FslldpV2AddressTableIndex

                The Object
                testValFslldpv2DestRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fslldpv2DestRowStatus (UINT4 *pu4ErrorCode,
                                UINT4 u4FslldpV2AddressTableIndex,
                                INT4 i4TestValFslldpv2DestRowStatus)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }
    if ((MemGetFreeUnits (gLldpGlobalInfo.LldpDestMacAddrPoolId) == 0) &&
        ((i4TestValFslldpv2DestRowStatus == CREATE_AND_WAIT) ||
         (i4TestValFslldpv2DestRowStatus == CREATE_AND_GO)))
    {
        CLI_SET_ERR (LLDP_CLI_MAX_DEST_MAC_REACHED);
        return SNMP_FAILURE;
    }

    DestTbl.u4LlldpV2DestAddrTblIndex = u4FslldpV2AddressTableIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if ((i4TestValFslldpv2DestRowStatus != LLDP_ACTIVE) &&
        (i4TestValFslldpv2DestRowStatus != LLDP_NOT_IN_SERVICE) &&
        (i4TestValFslldpv2DestRowStatus != LLDP_CREATE_AND_GO) &&
        (i4TestValFslldpv2DestRowStatus != LLDP_CREATE_AND_WAIT) &&
        (i4TestValFslldpv2DestRowStatus != LLDP_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValFslldpv2DestRowStatus)
    {
        case LLDP_ACTIVE:
        case LLDP_NOT_IN_SERVICE:
        case LLDP_DESTROY:
            if (pTempDestTbl == NULL)
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC, "Entry not Created \r\n");
                return SNMP_FAILURE;
            }
            break;

        case LLDP_CREATE_AND_WAIT:
        default:                /* LLDP_CREATE_AND_GO Case */
            if (pTempDestTbl != NULL)
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "Entry Already Created \r\n");
                return SNMP_FAILURE;
            }
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FslldpV2DestAddressTable
 Input       :  The Indices
                FslldpV2AddressTableIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FslldpV2DestAddressTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsLldpStatsTaggedTxPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLldpStatsTaggedTxPortTable
 Input       :  The Indices
                LldpV2StatsTxIfIndex
                LldpV2StatsTxDestMACAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsLldpStatsTaggedTxPortTable
    (INT4 i4LldpV2StatsTxIfIndex, UINT4 u4LldpV2StatsTxDestMACAddress)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    tLldpLocPortInfo    LldpLocPortInfo;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    UINT4               u4LocPort = 0;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));

    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2StatsTxDestMACAddress;
    pTempDestTbl = (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                                     Lldpv2DestMacAddrTblRBTree,
                                                     (tRBElem *) & DestTbl);
    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2StatsTxDestMACAddress \r\n");
        return SNMP_FAILURE;
    }
    if (LldpUtlGetLocalPort
        (i4LldpV2StatsTxIfIndex, pTempDestTbl->Lldpv2DestMacAddress,
         &u4LocPort) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return SNMP_FAILURE;
    }

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    LldpLocPortInfo.u4LocPortNum = u4LocPort;
    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD pLldpLocPortInfo is NULL LldpLocPortInfo is Invalid \r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsLldpStatsTaggedTxPortTable
 Input       :  The Indices
                LldpV2StatsTxIfIndex
                LldpV2StatsTxDestMACAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsLldpStatsTaggedTxPortTable (INT4 *pi4LldpV2StatsTxIfIndex,
                                              UINT4
                                              *pu4LldpV2StatsTxDestMACAddress)
{

    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGetFirst (gLldpGlobalInfo.
                                             LldpLocPortAgentInfoRBTree);
    if (pLldpLocalPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC, " SNMPSTD pLldpLocPortInfo is NULL \r\n");
        return SNMP_FAILURE;
    }
    *pi4LldpV2StatsTxIfIndex = pLldpLocalPortInfo->i4IfIndex;
    *pu4LldpV2StatsTxDestMACAddress = pLldpLocalPortInfo->u4DstMacAddrTblIndex;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsLldpStatsTaggedTxPortTable
 Input       :  The Indices
                LldpV2StatsTxIfIndex
                nextLldpV2StatsTxIfIndex
                LldpV2StatsTxDestMACAddress
                nextLldpV2StatsTxDestMACAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsLldpStatsTaggedTxPortTable (INT4 i4LldpV2StatsTxIfIndex,
                                             INT4 *pi4NextLldpV2StatsTxIfIndex,
                                             UINT4
                                             u4LldpV2StatsTxDestMACAddress,
                                             UINT4
                                             *pu4NextLldpV2StatsTxDestMACAddress)
{

    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    tLldpLocPortInfo    LldpLocPortInfo;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    UINT4               u4LocPort = 0;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));

    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2StatsTxDestMACAddress;
    pTempDestTbl = (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                                     Lldpv2DestMacAddrTblRBTree,
                                                     (tRBElem *) & DestTbl);
    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (MGMT_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2StatsTxDestMACAddress \r\n");
        return SNMP_FAILURE;
    }
    if (LldpUtlGetLocalPort
        (i4LldpV2StatsTxIfIndex, pTempDestTbl->Lldpv2DestMacAddress,
         &u4LocPort) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return SNMP_FAILURE;
    }

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        RBTreeGetNext (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
                       &LldpLocPortInfo, LldpAgentInfoUtlRBCmpInfo);

    if (pLldpLocalPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC, " SNMPSTD pLldpLocPortInfo is NULL \r\n");
        return SNMP_FAILURE;
    }

    *pi4NextLldpV2StatsTxIfIndex = pLldpLocalPortInfo->i4IfIndex;
    *pu4NextLldpV2StatsTxDestMACAddress =
        pLldpLocalPortInfo->u4DstMacAddrTblIndex;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLldpStatsTaggedTxPortFramesTotal
 Input       :  The Indices
                LldpV2StatsTxIfIndex
                LldpV2StatsTxDestMACAddress

                The Object 
                retValFsLldpStatsTaggedTxPortFramesTotal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLldpStatsTaggedTxPortFramesTotal (INT4 i4LldpV2StatsTxIfIndex,
                                          UINT4 u4LldpV2StatsTxDestMACAddress,
                                          UINT4
                                          *pu4RetValFsLldpStatsTaggedTxPortFramesTotal)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2StatsTxDestMACAddress;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);
    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (MGMT_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2StatsTxDestMACAddress \r\n");
        return SNMP_FAILURE;
    }
    if (LldpUtlGetStatsTxTaggedPortFramesTotal (i4LldpV2StatsTxIfIndex,
                                                pu4RetValFsLldpStatsTaggedTxPortFramesTotal,
                                                pTempDestTbl->
                                                Lldpv2DestMacAddress) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC,
                  " SNMPSTD:LldpUtlGetStatsTxTaggedPortFramesTotal Failed!! \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
