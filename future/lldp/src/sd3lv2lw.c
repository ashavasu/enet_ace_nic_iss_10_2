/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: sd3lv2lw.c,v 1.8 2017/10/11 13:42:02 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
#include "lldinc.h"
#include "lldcli.h"
/* LOW LEVEL Routines for Table : LldpV2Xdot3PortConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot3PortConfigTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot3PortConfigTable (INT4
                                                    i4LldpV2PortConfigIfIndex,
                                                    UINT4
                                                    u4LldpV2PortConfigDestAddressIndex)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: u4LldpV2PortConfigDestAddressIndex is Wrong\r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlValidateIndexInstanceLldpPortConfigTable
        (i4LldpV2PortConfigIfIndex,
         pTempDestTbl->Lldpv2DestMacAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpPortConfigTable failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot3PortConfigTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot3PortConfigTable (INT4 *pi4LldpV2PortConfigIfIndex,
                                            UINT4
                                            *pu4LldpV2PortConfigDestAddressIndex)
{

    return (nmhGetNextIndexLldpV2Xdot3PortConfigTable (LLDP_ZERO,
                                                       pi4LldpV2PortConfigIfIndex,
                                                       LLDP_ZERO,
                                                       pu4LldpV2PortConfigDestAddressIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot3PortConfigTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                nextLldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
                nextLldpV2PortConfigDestAddressIndex
 Output      :  The Get Next Function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot3PortConfigTable (INT4 i4LldpV2PortConfigIfIndex,
                                           INT4 *pi4NextLldpV2PortConfigIfIndex,
                                           UINT4
                                           u4LldpV2PortConfigDestAddressIndex,
                                           UINT4
                                           *pu4NextLldpV2PortConfigDestAddressIndex)
{
    tLldpv2AgentToLocPort Lldpv2AgentToLocPort;
    tLldpv2AgentToLocPort *pLldpv2AgentToLocPort = NULL;

    MEMSET (&Lldpv2AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    Lldpv2AgentToLocPort.i4IfIndex = i4LldpV2PortConfigIfIndex;
    Lldpv2AgentToLocPort.u4DstMacAddrTblIndex =
        u4LldpV2PortConfigDestAddressIndex;
    pLldpv2AgentToLocPort =
        (tLldpv2AgentToLocPort *) RBTreeGetNext (gLldpGlobalInfo.
                                                 Lldpv2AgentMapTblRBTree,
                                                 &Lldpv2AgentToLocPort,
                                                 LldpAgentToLocPortIndexUtlRBCmpInfo);

    if (pLldpv2AgentToLocPort == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4NextLldpV2PortConfigIfIndex = pLldpv2AgentToLocPort->i4IfIndex;
    *pu4NextLldpV2PortConfigDestAddressIndex =
        pLldpv2AgentToLocPort->u4DstMacAddrTblIndex;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3PortConfigTLVsTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                retValLldpV2Xdot3PortConfigTLVsTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3PortConfigTLVsTxEnable (INT4 i4LldpV2PortConfigIfIndex,
                                         UINT4
                                         u4LldpV2PortConfigDestAddressIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValLldpV2Xdot3PortConfigTLVsTxEnable)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: u4LldpV2PortConfigDestAddressIndex is Wrong\r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlGetLldpXdot3PortConfigTLVsTxEnable (i4LldpV2PortConfigIfIndex,
                                                   pRetValLldpV2Xdot3PortConfigTLVsTxEnable,
                                                   pTempDestTbl->
                                                   Lldpv2DestMacAddress) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot3PortConfigTLVsTxEnable Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpV2Xdot3PortConfigTLVsTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                setValLldpV2Xdot3PortConfigTLVsTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpV2Xdot3PortConfigTLVsTxEnable (INT4 i4LldpV2PortConfigIfIndex,
                                         UINT4
                                         u4LldpV2PortConfigDestAddressIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pSetValLldpV2Xdot3PortConfigTLVsTxEnable)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: u4LldpV2PortConfigDestAddressIndex is Wrong\r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlSetLldpXdot3PortConfigTLVsTxEnable (i4LldpV2PortConfigIfIndex,
                                                   pSetValLldpV2Xdot3PortConfigTLVsTxEnable,
                                                   pTempDestTbl->
                                                   Lldpv2DestMacAddress) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlSetLldpXdot3PortConfigTLVsTxEnable Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpV2Xdot3PortConfigTLVsTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                testValLldpV2Xdot3PortConfigTLVsTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpV2Xdot3PortConfigTLVsTxEnable (UINT4 *pu4ErrorCode,
                                            INT4 i4LldpV2PortConfigIfIndex,
                                            UINT4
                                            u4LldpV2PortConfigDestAddressIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pTestValLldpV2Xdot3PortConfigTLVsTxEnable)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: u4LldpV2PortConfigDestAddressIndex is Wrong\r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlTestLldpXdot3PortConfigTLVsTxEnable
        (pu4ErrorCode, i4LldpV2PortConfigIfIndex,
         pTestValLldpV2Xdot3PortConfigTLVsTxEnable,
         pTempDestTbl->Lldpv2DestMacAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlTestLldpXdot3PortConfigTLVsTxEnable Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpV2Xdot3PortConfigTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpV2Xdot3PortConfigTable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot3LocPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot3LocPortTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot3LocPortTable (INT4 i4LldpV2LocPortIfIndex)
{
    if (LldpUtlValidateIndexInstanceLldpXdot3LocPortTable
        (i4LldpV2LocPortIfIndex) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlValidateIndexInstanceLldpXdot3LocPortTable Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot3LocPortTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot3LocPortTable (INT4 *pi4LldpV2LocPortIfIndex)
{
    if (LldpUtlGetFirstIndexLldpXdot3LocPortTable (pi4LldpV2LocPortIfIndex) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlGetFirstIndexLldpXdot3LocPortTable  Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot3LocPortTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
 Output      :  The Get Next Function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot3LocPortTable (INT4 i4LldpV2LocPortIfIndex,
                                        INT4 *pi4NextLldpV2LocPortIfIndex)
{
    if (LldpUtlGetNextIndexLldpXdot3LocPortTable
        (i4LldpV2LocPortIfIndex, pi4NextLldpV2LocPortIfIndex) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlGetNextIndexLldpXdot3LocPortTable  Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3LocPortAutoNegSupported
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2Xdot3LocPortAutoNegSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3LocPortAutoNegSupported (INT4 i4LldpV2LocPortIfIndex,
                                          INT4
                                          *pi4RetValLldpV2Xdot3LocPortAutoNegSupported)
{
    UINT4               u4DestMacIndex = 0;
    UINT4               u4LocPortNum = 0;

    u4DestMacIndex = LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpV2LocPortIfIndex);
    u4LocPortNum =
        LldpGetLocalPortFromIfIndexDestMacIfIndex ((UINT4)
                                                   i4LldpV2LocPortIfIndex,
                                                   u4DestMacIndex);

    if (LldpUtlGetLldpXdot3LocPortAutoNegSupported ((INT4) u4LocPortNum,
                                                    pi4RetValLldpV2Xdot3LocPortAutoNegSupported)
        == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlGetLldpXdot3LocPortAutoNegSupported  Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3LocPortAutoNegEnabled
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2Xdot3LocPortAutoNegEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3LocPortAutoNegEnabled (INT4 i4LldpV2LocPortIfIndex,
                                        INT4
                                        *pi4RetValLldpV2Xdot3LocPortAutoNegEnabled)
{
    UINT4               u4DestMacIndex = 0;
    UINT4               u4LocPortNum = 0;

    u4DestMacIndex = LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpV2LocPortIfIndex);
    u4LocPortNum =
        LldpGetLocalPortFromIfIndexDestMacIfIndex ((UINT4)
                                                   i4LldpV2LocPortIfIndex,
                                                   u4DestMacIndex);
    if (LldpUtlGetLldpXdot3LocPortAutoNegEnabled
        ((INT4) u4LocPortNum,
         pi4RetValLldpV2Xdot3LocPortAutoNegEnabled) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlGetLldpXdot3LocPortAutoNegEnabled  Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3LocPortAutoNegAdvertisedCap
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2Xdot3LocPortAutoNegAdvertisedCap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3LocPortAutoNegAdvertisedCap (INT4 i4LldpV2LocPortIfIndex,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pRetValLldpV2Xdot3LocPortAutoNegAdvertisedCap)
{
    UINT4               u4DestMacIndex = 0;
    UINT4               u4LocPortNum = 0;

    u4DestMacIndex = LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpV2LocPortIfIndex);
    u4LocPortNum =
        LldpGetLocalPortFromIfIndexDestMacIfIndex ((UINT4)
                                                   i4LldpV2LocPortIfIndex,
                                                   u4DestMacIndex);
    if (LldpUtlGetLldpXdot3LocPortAutoNegAdvertisedCap
        ((INT4) u4LocPortNum,
         pRetValLldpV2Xdot3LocPortAutoNegAdvertisedCap) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlGetLldpXdot3LocPortAutoNegAdvertisedCap Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3LocPortOperMauType
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2Xdot3LocPortOperMauType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3LocPortOperMauType (INT4 i4LldpV2LocPortIfIndex,
                                     UINT4
                                     *pu4RetValLldpV2Xdot3LocPortOperMauType)
{
    UINT4               u4DestMacIndex = 0;
    UINT4               u4LocPortNum = 0;

    u4DestMacIndex = LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpV2LocPortIfIndex);
    u4LocPortNum =
        LldpGetLocalPortFromIfIndexDestMacIfIndex ((UINT4)
                                                   i4LldpV2LocPortIfIndex,
                                                   u4DestMacIndex);
    if (LldpUtlGetLldpXdot3LocPortOperMauType
        ((INT4) u4LocPortNum,
         (INT4 *) pu4RetValLldpV2Xdot3LocPortOperMauType) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlGetLldpXdot3LocPortOperMauType Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot3LocPowerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot3LocPowerTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot3LocPowerTable (INT4 i4LldpV2LocPortIfIndex)
{
    UNUSED_PARAM (i4LldpV2LocPortIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot3LocPowerTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot3LocPowerTable (INT4 *pi4LldpV2LocPortIfIndex)
{
    UNUSED_PARAM (pi4LldpV2LocPortIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot3LocPowerTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
 Output      :  The Get Next Function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot3LocPowerTable (INT4 i4LldpV2LocPortIfIndex,
                                         INT4 *pi4NextLldpV2LocPortIfIndex)
{
    UNUSED_PARAM (i4LldpV2LocPortIfIndex);
    UNUSED_PARAM (pi4NextLldpV2LocPortIfIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3LocPowerPortClass
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2Xdot3LocPowerPortClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3LocPowerPortClass (INT4 i4LldpV2LocPortIfIndex,
                                    INT4 *pi4RetValLldpV2Xdot3LocPowerPortClass)
{
    UNUSED_PARAM (i4LldpV2LocPortIfIndex);
    UNUSED_PARAM (pi4RetValLldpV2Xdot3LocPowerPortClass);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3LocPowerMDISupported
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2Xdot3LocPowerMDISupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3LocPowerMDISupported (INT4 i4LldpV2LocPortIfIndex,
                                       INT4
                                       *pi4RetValLldpV2Xdot3LocPowerMDISupported)
{
    UNUSED_PARAM (i4LldpV2LocPortIfIndex);
    UNUSED_PARAM (pi4RetValLldpV2Xdot3LocPowerMDISupported);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3LocPowerMDIEnabled
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2Xdot3LocPowerMDIEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3LocPowerMDIEnabled (INT4 i4LldpV2LocPortIfIndex,
                                     INT4
                                     *pi4RetValLldpV2Xdot3LocPowerMDIEnabled)
{
    UNUSED_PARAM (i4LldpV2LocPortIfIndex);
    UNUSED_PARAM (pi4RetValLldpV2Xdot3LocPowerMDIEnabled);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3LocPowerPairControlable
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2Xdot3LocPowerPairControlable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3LocPowerPairControlable (INT4 i4LldpV2LocPortIfIndex,
                                          INT4
                                          *pi4RetValLldpV2Xdot3LocPowerPairControlable)
{
    UNUSED_PARAM (i4LldpV2LocPortIfIndex);
    UNUSED_PARAM (pi4RetValLldpV2Xdot3LocPowerPairControlable);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3LocPowerPairs
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2Xdot3LocPowerPairs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3LocPowerPairs (INT4 i4LldpV2LocPortIfIndex,
                                UINT4 *pu4RetValLldpV2Xdot3LocPowerPairs)
{
    UNUSED_PARAM (i4LldpV2LocPortIfIndex);
    UNUSED_PARAM (pu4RetValLldpV2Xdot3LocPowerPairs);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3LocPowerClass
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2Xdot3LocPowerClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3LocPowerClass (INT4 i4LldpV2LocPortIfIndex,
                                UINT4 *pu4RetValLldpV2Xdot3LocPowerClass)
{
    UNUSED_PARAM (i4LldpV2LocPortIfIndex);
    UNUSED_PARAM (pu4RetValLldpV2Xdot3LocPowerClass);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot3LocMaxFrameSizeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot3LocMaxFrameSizeTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot3LocMaxFrameSizeTable (INT4
                                                         i4LldpV2LocPortIfIndex)
{
    if (LldpUtlValidateIndexInstanceLldpXdot3LocMaxFrameSizeTable
        (i4LldpV2LocPortIfIndex) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlValidateIndexInstanceLldpXdot3LocMaxFrameSizeTable Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot3LocMaxFrameSizeTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot3LocMaxFrameSizeTable (INT4 *pi4LldpV2LocPortIfIndex)
{
    if (LldpUtlGetFirstIndexLldpXdot3LocMaxFrameSizeTable
        (pi4LldpV2LocPortIfIndex) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlGetFirstIndexLldpXdot3LocMaxFrameSizeTable Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot3LocMaxFrameSizeTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
 Output      :  The Get Next Function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot3LocMaxFrameSizeTable (INT4 i4LldpV2LocPortIfIndex,
                                                INT4
                                                *pi4NextLldpV2LocPortIfIndex)
{
    if (LldpUtlGetNextIndexLldpXdot3LocMaxFrameSizeTable
        (i4LldpV2LocPortIfIndex, pi4NextLldpV2LocPortIfIndex) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlGetNextIndexLldpXdot3LocMaxFrameSizeTable Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3LocMaxFrameSize
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2Xdot3LocMaxFrameSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3LocMaxFrameSize (INT4 i4LldpV2LocPortIfIndex,
                                  UINT4 *pu4RetValLldpV2Xdot3LocMaxFrameSize)
{
    if (LldpUtlGetLldpXdot3LocMaxFrameSize
        (i4LldpV2LocPortIfIndex,
         (INT4 *) pu4RetValLldpV2Xdot3LocMaxFrameSize) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlGetLldpXdot3LocMaxFrameSize Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot3RemPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot3RemPortTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot3RemPortTable (UINT4 u4LldpV2RemTimeMark,
                                                 INT4 i4LldpV2RemLocalIfIndex,
                                                 UINT4
                                                 u4LldpV2RemLocalDestMACAddress,
                                                 INT4 i4LldpV2RemIndex)
{
    if (LldpUtlValidateIndexInstanceLldpXdot3RemPortTable
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, i4LldpV2RemIndex,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlValidateIndexInstanceLldpXdot3RemPortTable Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot3RemPortTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot3RemPortTable (UINT4 *pu4LldpV2RemTimeMark,
                                         INT4 *pi4LldpV2RemLocalIfIndex,
                                         UINT4 *pu4LldpV2RemLocalDestMACAddress,
                                         INT4 *pi4LldpV2RemIndex)
{
    return (nmhGetNextIndexLldpV2Xdot3RemPortTable (LLDP_ZERO,
                                                    pu4LldpV2RemTimeMark,
                                                    LLDP_ZERO,
                                                    pi4LldpV2RemLocalIfIndex,
                                                    LLDP_ZERO,
                                                    pu4LldpV2RemLocalDestMACAddress,
                                                    LLDP_ZERO,
                                                    pi4LldpV2RemIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot3RemPortTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
 Output      :  The Get Next Function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot3RemPortTable (UINT4 u4LldpV2RemTimeMark,
                                        UINT4 *pu4NextLldpV2RemTimeMark,
                                        INT4 i4LldpV2RemLocalIfIndex,
                                        INT4 *pi4NextLldpV2RemLocalIfIndex,
                                        UINT4 u4LldpV2RemLocalDestMACAddress,
                                        UINT4
                                        *pu4NextLldpV2RemLocalDestMACAddress,
                                        INT4 i4LldpV2RemIndex,
                                        INT4 *pi4NextLldpV2RemIndex)
{
    tLldpRemoteNode    *pTmpRemoteNode = NULL;
    tLldpRemoteNode    *pLldpRemoteNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    /* Allocate Memory for Remote Node */
    if ((pTmpRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_MANDATORY_TLV_TRC,
                  "SNMPSTD: Failed to Allocate Memory " "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return SNMP_FAILURE;
    }
    MEMSET (pTmpRemoteNode, 0, sizeof (tLldpRemoteNode));
    pTmpRemoteNode->u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    pTmpRemoteNode->i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    pTmpRemoteNode->u4DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    pTmpRemoteNode->i4RemIndex = i4LldpV2RemIndex;

    pLldpRemoteNode =
        (tLldpRemoteNode *) RBTreeGetNext (gLldpGlobalInfo.RemSysDataRBTree,
                                           (tRBElem *) pTmpRemoteNode, NULL);

    if (pLldpRemoteNode != NULL)
    {
        *pu4NextLldpV2RemTimeMark = pLldpRemoteNode->u4RemLastUpdateTime;
        *pi4NextLldpV2RemLocalIfIndex = pLldpRemoteNode->i4RemLocalPortNum;
        *pi4NextLldpV2RemIndex = pLldpRemoteNode->i4RemIndex;
        *pu4NextLldpV2RemLocalDestMACAddress =
            pLldpRemoteNode->u4DestAddrTblIndex;
        i1RetVal = SNMP_SUCCESS;
    }

    /* Release the memory allocated for Remote Node */
    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                        (UINT1 *) pTmpRemoteNode);

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3RemPortAutoNegSupported
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2Xdot3RemPortAutoNegSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3RemPortAutoNegSupported (UINT4 u4LldpV2RemTimeMark,
                                          INT4 i4LldpV2RemLocalIfIndex,
                                          UINT4 u4LldpV2RemLocalDestMACAddress,
                                          INT4 i4LldpV2RemIndex,
                                          INT4
                                          *pi4RetValLldpV2Xdot3RemPortAutoNegSupported)
{
    if (LldpUtlGetLldpXdot3RemPortAutoNegSupported
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, i4LldpV2RemIndex,
         pi4RetValLldpV2Xdot3RemPortAutoNegSupported,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlGetLldpXdot3RemPortAutoNegSupported Failed\r\n");
        *pi4RetValLldpV2Xdot3RemPortAutoNegSupported = OSIX_FALSE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3RemPortAutoNegEnabled
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2Xdot3RemPortAutoNegEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3RemPortAutoNegEnabled (UINT4 u4LldpV2RemTimeMark,
                                        INT4 i4LldpV2RemLocalIfIndex,
                                        UINT4 u4LldpV2RemLocalDestMACAddress,
                                        INT4 i4LldpV2RemIndex,
                                        INT4
                                        *pi4RetValLldpV2Xdot3RemPortAutoNegEnabled)
{
    if (LldpUtlGetLldpXdot3RemPortAutoNegEnabled
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, i4LldpV2RemIndex,
         pi4RetValLldpV2Xdot3RemPortAutoNegEnabled,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlGetLldpXdot3RemPortAutoNegEnabled Failed\r\n");
        *pi4RetValLldpV2Xdot3RemPortAutoNegEnabled = OSIX_FALSE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3RemPortAutoNegAdvertisedCap
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2Xdot3RemPortAutoNegAdvertisedCap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3RemPortAutoNegAdvertisedCap (UINT4 u4LldpV2RemTimeMark,
                                              INT4 i4LldpV2RemLocalIfIndex,
                                              UINT4
                                              u4LldpV2RemLocalDestMACAddress,
                                              INT4 i4LldpV2RemIndex,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pRetValLldpV2Xdot3RemPortAutoNegAdvertisedCap)
{
    if (LldpUtlGetLldpXdot3RemPortAutoNegAdvertisedCap
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, i4LldpV2RemIndex,
         pRetValLldpV2Xdot3RemPortAutoNegAdvertisedCap,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlGetLldpXdot3RemPortAutoNegAdvertisedCap Failed\r\n");
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3RemPortOperMauType
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2Xdot3RemPortOperMauType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3RemPortOperMauType (UINT4 u4LldpV2RemTimeMark,
                                     INT4 i4LldpV2RemLocalIfIndex,
                                     UINT4 u4LldpV2RemLocalDestMACAddress,
                                     INT4 i4LldpV2RemIndex,
                                     UINT4
                                     *pu4RetValLldpV2Xdot3RemPortOperMauType)
{
    if (LldpUtlGetLldpXdot3RemPortOperMauType
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, i4LldpV2RemIndex,
         (INT4 *) pu4RetValLldpV2Xdot3RemPortOperMauType,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlGetLldpXdot3RemPortOperMauType Failed\r\n");
        *pu4RetValLldpV2Xdot3RemPortOperMauType = 0;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot3RemPowerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot3RemPowerTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot3RemPowerTable (UINT4 u4LldpV2RemTimeMark,
                                                  INT4 i4LldpV2RemLocalIfIndex,
                                                  UINT4
                                                  u4LldpV2RemLocalDestMACAddress,
                                                  INT4 i4LldpV2RemIndex)
{
    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (i4LldpV2RemLocalIfIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);
    UNUSED_PARAM (i4LldpV2RemIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot3RemPowerTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot3RemPowerTable (UINT4 *pu4LldpV2RemTimeMark,
                                          INT4 *pi4LldpV2RemLocalIfIndex,
                                          UINT4
                                          *pu4LldpV2RemLocalDestMACAddress,
                                          INT4 *pi4LldpV2RemIndex)
{
    UNUSED_PARAM (pu4LldpV2RemTimeMark);
    UNUSED_PARAM (pi4LldpV2RemLocalIfIndex);
    UNUSED_PARAM (pu4LldpV2RemLocalDestMACAddress);
    UNUSED_PARAM (pi4LldpV2RemIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot3RemPowerTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
 Output      :  The Get Next Function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot3RemPowerTable (UINT4 u4LldpV2RemTimeMark,
                                         UINT4 *pu4NextLldpV2RemTimeMark,
                                         INT4 i4LldpV2RemLocalIfIndex,
                                         INT4 *pi4NextLldpV2RemLocalIfIndex,
                                         UINT4 u4LldpV2RemLocalDestMACAddress,
                                         UINT4
                                         *pu4NextLldpV2RemLocalDestMACAddress,
                                         INT4 i4LldpV2RemIndex,
                                         INT4 *pi4NextLldpV2RemIndex)
{
    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (i4LldpV2RemLocalIfIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);
    UNUSED_PARAM (i4LldpV2RemIndex);
    UNUSED_PARAM (pu4NextLldpV2RemTimeMark);
    UNUSED_PARAM (pi4NextLldpV2RemLocalIfIndex);
    UNUSED_PARAM (pu4NextLldpV2RemLocalDestMACAddress);
    UNUSED_PARAM (pi4NextLldpV2RemIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3RemPowerPortClass
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2Xdot3RemPowerPortClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3RemPowerPortClass (UINT4 u4LldpV2RemTimeMark,
                                    INT4 i4LldpV2RemLocalIfIndex,
                                    UINT4 u4LldpV2RemLocalDestMACAddress,
                                    INT4 i4LldpV2RemIndex,
                                    INT4 *pi4RetValLldpV2Xdot3RemPowerPortClass)
{
    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (i4LldpV2RemLocalIfIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);
    UNUSED_PARAM (i4LldpV2RemIndex);
    UNUSED_PARAM (pi4RetValLldpV2Xdot3RemPowerPortClass);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3RemPowerMDISupported
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2Xdot3RemPowerMDISupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3RemPowerMDISupported (UINT4 u4LldpV2RemTimeMark,
                                       INT4 i4LldpV2RemLocalIfIndex,
                                       UINT4 u4LldpV2RemLocalDestMACAddress,
                                       INT4 i4LldpV2RemIndex,
                                       INT4
                                       *pi4RetValLldpV2Xdot3RemPowerMDISupported)
{
    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (i4LldpV2RemLocalIfIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);
    UNUSED_PARAM (i4LldpV2RemIndex);
    UNUSED_PARAM (pi4RetValLldpV2Xdot3RemPowerMDISupported);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3RemPowerMDIEnabled
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2Xdot3RemPowerMDIEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3RemPowerMDIEnabled (UINT4 u4LldpV2RemTimeMark,
                                     INT4 i4LldpV2RemLocalIfIndex,
                                     UINT4 u4LldpV2RemLocalDestMACAddress,
                                     INT4 i4LldpV2RemIndex,
                                     INT4
                                     *pi4RetValLldpV2Xdot3RemPowerMDIEnabled)
{
    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (i4LldpV2RemLocalIfIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);
    UNUSED_PARAM (i4LldpV2RemIndex);
    UNUSED_PARAM (pi4RetValLldpV2Xdot3RemPowerMDIEnabled);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3RemPowerPairControlable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2Xdot3RemPowerPairControlable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3RemPowerPairControlable (UINT4 u4LldpV2RemTimeMark,
                                          INT4 i4LldpV2RemLocalIfIndex,
                                          UINT4 u4LldpV2RemLocalDestMACAddress,
                                          INT4 i4LldpV2RemIndex,
                                          INT4
                                          *pi4RetValLldpV2Xdot3RemPowerPairControlable)
{
    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (i4LldpV2RemLocalIfIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);
    UNUSED_PARAM (i4LldpV2RemIndex);
    UNUSED_PARAM (pi4RetValLldpV2Xdot3RemPowerPairControlable);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3RemPowerPairs
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2Xdot3RemPowerPairs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3RemPowerPairs (UINT4 u4LldpV2RemTimeMark,
                                INT4 i4LldpV2RemLocalIfIndex,
                                UINT4 u4LldpV2RemLocalDestMACAddress,
                                INT4 i4LldpV2RemIndex,
                                UINT4 *pu4RetValLldpV2Xdot3RemPowerPairs)
{
    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (i4LldpV2RemLocalIfIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);
    UNUSED_PARAM (i4LldpV2RemIndex);
    UNUSED_PARAM (pu4RetValLldpV2Xdot3RemPowerPairs);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3RemPowerClass
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2Xdot3RemPowerClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3RemPowerClass (UINT4 u4LldpV2RemTimeMark,
                                INT4 i4LldpV2RemLocalIfIndex,
                                UINT4 u4LldpV2RemLocalDestMACAddress,
                                INT4 i4LldpV2RemIndex,
                                UINT4 *pu4RetValLldpV2Xdot3RemPowerClass)
{
    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (i4LldpV2RemLocalIfIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);
    UNUSED_PARAM (i4LldpV2RemIndex);
    UNUSED_PARAM (pu4RetValLldpV2Xdot3RemPowerClass);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : LldpV2Xdot3RemMaxFrameSizeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2Xdot3RemMaxFrameSizeTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2Xdot3RemMaxFrameSizeTable (UINT4
                                                         u4LldpV2RemTimeMark,
                                                         INT4
                                                         i4LldpV2RemLocalIfIndex,
                                                         UINT4
                                                         u4LldpV2RemLocalDestMACAddress,
                                                         INT4 i4LldpV2RemIndex)
{
    if (LldpUtlValidateIndexInstanceLldpXdot3RemMaxFrameSizeTable
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, i4LldpV2RemIndex,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlValidateIndexInstanceLldpXdot3RemMaxFrameSizeTable Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2Xdot3RemMaxFrameSizeTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2Xdot3RemMaxFrameSizeTable (UINT4 *pu4LldpV2RemTimeMark,
                                                 INT4 *pi4LldpV2RemLocalIfIndex,
                                                 UINT4
                                                 *pu4LldpV2RemLocalDestMACAddress,
                                                 INT4 *pi4LldpV2RemIndex)
{
    return (nmhGetNextIndexLldpV2Xdot3RemMaxFrameSizeTable (LLDP_ZERO,
                                                            pu4LldpV2RemTimeMark,
                                                            LLDP_ZERO,
                                                            pi4LldpV2RemLocalIfIndex,
                                                            LLDP_ZERO,
                                                            pu4LldpV2RemLocalDestMACAddress,
                                                            LLDP_ZERO,
                                                            pi4LldpV2RemIndex));

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2Xdot3RemMaxFrameSizeTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
 Output      :  The Get Next Function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2Xdot3RemMaxFrameSizeTable (UINT4 u4LldpV2RemTimeMark,
                                                UINT4 *pu4NextLldpV2RemTimeMark,
                                                INT4 i4LldpV2RemLocalIfIndex,
                                                INT4
                                                *pi4NextLldpV2RemLocalIfIndex,
                                                UINT4
                                                u4LldpV2RemLocalDestMACAddress,
                                                UINT4
                                                *pu4NextLldpV2RemLocalDestMACAddress,
                                                INT4 i4LldpV2RemIndex,
                                                INT4 *pi4NextLldpV2RemIndex)
{
    tLldpRemoteNode     LldpRemoteNode;
    tLldpRemoteNode    *pLldpRemoteNode = NULL;
    MEMSET (&LldpRemoteNode, 0, sizeof (tLldpRemoteNode));

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    LldpRemoteNode.u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    LldpRemoteNode.i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    LldpRemoteNode.u4DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    LldpRemoteNode.i4RemIndex = i4LldpV2RemIndex;

    pLldpRemoteNode =
        (tLldpRemoteNode *) RBTreeGetNext (gLldpGlobalInfo.RemSysDataRBTree,
                                           (tRBElem *) & LldpRemoteNode, NULL);

    if (pLldpRemoteNode == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4NextLldpV2RemTimeMark = pLldpRemoteNode->u4RemLastUpdateTime;
    *pi4NextLldpV2RemLocalIfIndex = pLldpRemoteNode->i4RemLocalPortNum;
    *pi4NextLldpV2RemIndex = pLldpRemoteNode->i4RemIndex;
    *pu4NextLldpV2RemLocalDestMACAddress = pLldpRemoteNode->u4DestAddrTblIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2Xdot3RemMaxFrameSize
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2Xdot3RemMaxFrameSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2Xdot3RemMaxFrameSize (UINT4 u4LldpV2RemTimeMark,
                                  INT4 i4LldpV2RemLocalIfIndex,
                                  UINT4 u4LldpV2RemLocalDestMACAddress,
                                  INT4 i4LldpV2RemIndex,
                                  UINT4 *pu4RetValLldpV2Xdot3RemMaxFrameSize)
{
    if (LldpUtlGetLldpXdot3RemMaxFrameSize
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, i4LldpV2RemIndex,
         (INT4 *) pu4RetValLldpV2Xdot3RemMaxFrameSize,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " LldpUtlGetLldpXdot3RemMaxFrameSize Failed\r\n");
        *pu4RetValLldpV2Xdot3RemMaxFrameSize = 0;
    }
    return SNMP_SUCCESS;
}
