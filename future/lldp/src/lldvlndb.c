/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldvlndb.c,v 1.12 2015/06/04 14:50:34 siva Exp $
 *
 * Description: This file contains the routines to access the PortVlan RBTree
 *              maintained in L2IWF. This data structure is part of LLDP and so
 *              the routines written in this file need not be ported.
 ******************************************************************************/
#ifndef _LLDVLNDB_C_
#define _LLDVLNDB__

#include "lldinc.h"

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpVlndbValidateVlanNameTable
 *
 *    DESCRIPTION      : This function validates the port index and the vlan id
 *
 *    INPUT            : u4IfIndex - PortIndex
 *                       VlanId    - Vlan id
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpVlndbValidateVlanNameTable (UINT4 u4IfIndex, UINT2 u2VlanId)
{
    INT4                i4RetVal = OSIX_FAILURE;

    if ((LLDP_IS_PORT_ID_VALID (u4IfIndex) == OSIX_TRUE) &&
        (VLAN_IS_VLAN_ID_VALID (u2VlanId) == OSIX_TRUE))
    {
        if (L2IwfValidatePortVlanTableIndex (u4IfIndex, u2VlanId)
            != L2IWF_FAILURE)
        {
            i4RetVal = OSIX_SUCCESS;
        }
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpVlndbGetFirstVlanNameTblInd
 *
 *    DESCRIPTION      : This function gets the first valid indices of the Vlan 
 *                       Name Table. Data structure to store the Vlan Name 
 *                       Table Information is maintained in L2IWF
 *
 *    INPUT            : None
 *
 *    OUTPUT           : *pi4IfIndex - PortIndex
 *                       *pi4VlanId  - Vlan id
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpVlndbGetFirstVlanNameTblInd (INT4 *pi4IfIndex, INT4 *pi4VlanId)
{
    UINT4               u4IfIndex = 0;
    UINT2               u2VlanId = 0;
    INT4                i4RetVal = OSIX_FAILURE;

    if (L2IwfGetFirstPortVlanTableIndex (&u4IfIndex, &u2VlanId) ==
        L2IWF_SUCCESS)
    {
        if ((LLDP_IS_PORT_ID_VALID (u4IfIndex) == OSIX_TRUE) &&
            (VLAN_IS_VLAN_ID_VALID (u2VlanId) == OSIX_TRUE))
        {
            *pi4IfIndex = (INT4) u4IfIndex;
            *pi4VlanId = (INT4) u2VlanId;
            i4RetVal = OSIX_SUCCESS;
        }
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpVlndbGetNextVlanNameTblInd
 *
 *    DESCRIPTION      : This function gets next port index and the vlan id for
 *                       the given port index and the vlan id.
 *
 *    INPUT            : i4LldpLocPortNum - Port Index whose next index is
 *                                          required
 *                       i4ldpXdot1LocVlanId - Vlan id whose next index is 
 *                                              required
 *   OUTPUT            : pi4NextLldpLocPortNum - Pointer to the next Port Index
 *                       pi4NextLldpXdot1LocVlanId - Pointer to the next Vlan
 *                                                   id                        
 *
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpVlndbGetNextVlanNameTblInd (INT4 i4LldpLocPortNum,
                                INT4 *pi4NextLldpLocPortNum,
                                INT4 i4LldpXdot1LocVlanId,
                                INT4 *pi4NextLldpXdot1LocVlanId)
{
    INT4                i4RetVal = OSIX_FAILURE;
    UINT4               u4NextLldpLocPortNum = 0;
    UINT2               u2NextLldpXdot1LocVlanId = 0;

    if (i4LldpLocPortNum >= LLDP_MIN_PORTS)
    {
        if (L2IwfGetNextPortVlanTableIndex ((UINT4) i4LldpLocPortNum,
                                            &u4NextLldpLocPortNum,
                                            (UINT2) i4LldpXdot1LocVlanId,
                                            &u2NextLldpXdot1LocVlanId)
            == L2IWF_SUCCESS)
        {
            if ((LLDP_IS_PORT_ID_VALID (u4NextLldpLocPortNum) == OSIX_TRUE) &&
                (VLAN_IS_VLAN_ID_VALID (u2NextLldpXdot1LocVlanId) == OSIX_TRUE))
            {
                *pi4NextLldpLocPortNum = (INT4) u4NextLldpLocPortNum;
                *pi4NextLldpXdot1LocVlanId = (INT4) u2NextLldpXdot1LocVlanId;
                i4RetVal = OSIX_SUCCESS;
            }
        }
    }
    else
    {
        LldpVlndbGetFirstVlanNameTblInd (pi4NextLldpLocPortNum,
                                         pi4NextLldpXdot1LocVlanId);
        i4RetVal = OSIX_SUCCESS;
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpVlndbGetVlanName
 *
 *    DESCRIPTION      : This function gets the vlan name for the given port 
 *                       index and the vlan id. Vlan Name Table related
 *                       information is maintained as an RBTree (PortVlanTable)
 *                       in L2IWF. 
 *
 *    INPUT            : u4LldpLocPortNum - Port Index
 *                       u2LldpXdot1LocVlanId - Vlan id
 *
 *    OUTPUT           : pu1VlanName - Pointer to Vlan name
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpVlndbGetVlanName (UINT4 u4LldpLocPortNum, UINT2 u2LldpXdot1LocVlanId,
                      UINT1 *pu1VlanName)
{
    INT4                i4RetVal = OSIX_FAILURE;

    if ((LLDP_IS_PORT_ID_VALID (u4LldpLocPortNum) == OSIX_TRUE) &&
        (VLAN_IS_VLAN_ID_VALID (u2LldpXdot1LocVlanId) == OSIX_TRUE))
    {
        if (L2IwfGetVlanName (u4LldpLocPortNum, u2LldpXdot1LocVlanId,
                              pu1VlanName) == L2IWF_SUCCESS)
        {
            i4RetVal = OSIX_SUCCESS;
        }
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpVlndbGetVlanNameTxStatus
 *
 *    DESCRIPTION      : This function gets the vlan name transmission status 
 *                       for the given port index and the vlan id.
 *
 *    INPUT            : u4LldpLocPortNum - Port Index
 *                       u2LldpXdot1LocVlanId - Vlan id
 *
 *    OUTPUT           : pu1VlanNameTxStatus - Pointer to Vlan name transmission
 *                                            status
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/

PUBLIC INT4
LldpVlndbGetVlanNameTxStatus (UINT4 u4LldpLocPortNum,
                              UINT2 u2LldpXdot1LocVlanId,
                              UINT1 *pu1VlanNameTxStatus)
{
    INT4                i4RetVal = OSIX_FAILURE;

    if ((LLDP_IS_PORT_ID_VALID (u4LldpLocPortNum) == OSIX_TRUE) &&
        (VLAN_IS_VLAN_ID_VALID (u2LldpXdot1LocVlanId) == OSIX_TRUE))
    {
        if (L2IwfGetVlanNameTxStatus (u4LldpLocPortNum, u2LldpXdot1LocVlanId,
                                      pu1VlanNameTxStatus) == L2IWF_SUCCESS)
        {
            i4RetVal = OSIX_SUCCESS;
        }
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpVlndbSetVlanNameTxStatus
 *
 *    DESCRIPTION      : This function sets the vlan name transmission status
 *                       for the given port index and the vlan id.
 *
 *    INPUT            : u4LldpLocPortNum - Port Index
 *                       u2LldpXdot1LocVlanId - Vlan id
 *                       u1VlanNameTxStatus - Vlan name transmission status
 *
 *    OUTPUT           : pbConfTxEnable - Set to TRUE if transmission status
 *                                        is configured with the value to be 
 *                                        set.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/

PUBLIC INT4
LldpVlndbSetVlanNameTxStatus (UINT4 u4LldpLocPortNum,
                              UINT2 u2LldpXdot1LocVlanId,
                              UINT1 u1VlanNameTxStatus, UINT1 *pu1ConfTxEnable)
{
    INT4                i4RetVal = OSIX_FAILURE;

    if ((LLDP_IS_PORT_ID_VALID (u4LldpLocPortNum) == OSIX_TRUE) &&
        (VLAN_IS_VLAN_ID_VALID (u2LldpXdot1LocVlanId) == OSIX_TRUE))
    {
        if (L2IwfSetVlanNameTxStatus (u4LldpLocPortNum, u2LldpXdot1LocVlanId,
                                      u1VlanNameTxStatus, pu1ConfTxEnable)
            == L2IWF_SUCCESS)
        {
            i4RetVal = OSIX_SUCCESS;
        }
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpVlndbValidateVlanNameEntry
 *
 *    DESCRIPTION      : This function validates the vlan name entry before 
 *                       setting the transmission status for the given port
 *                       index and the vlan id. The transmission status can 
 *                       be set only if a valid name is configured for the
 *                       vlan.
 *
 *    INPUT            : u4LldpLocPortNum - Port Index
 *                       u2LldpXdot1LocVlanId - Vlan id
 *
 *    OUTPUT           : None  
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/

PUBLIC INT4
LldpVlndbValidateVlanNameEntry (UINT4 u4LldpLocPortNum,
                                UINT2 u2LldpXdot1LocVlanId)
{
    UINT1               au1VlanName[VLAN_STATIC_MAX_NAME_LEN];
    INT4                i4RetVal = OSIX_FAILURE;

    MEMSET (&au1VlanName[0], 0, VLAN_STATIC_MAX_NAME_LEN);

    if ((LLDP_IS_PORT_ID_VALID (u4LldpLocPortNum) == OSIX_TRUE) &&
        (VLAN_IS_VLAN_ID_VALID (u2LldpXdot1LocVlanId) == OSIX_TRUE))
    {
        if (L2IwfGetVlanName
            (u4LldpLocPortNum, u2LldpXdot1LocVlanId,
             au1VlanName) != L2IWF_FAILURE)
        {
                i4RetVal = OSIX_SUCCESS;
        }
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpVlndbGetAllVlanName 
 *
 *    DESCRIPTION      : This routine gets all the vlan name from l2iwf 
 *    
 *    INPUT            : u4PortNum - Port number
 *                       pVlanNameInfo - pointer to vlan namne info structure
 *                       pu2VlanNameCount - pointer to vlan name count
 *
 *    OUTPUT           : pVlanNameInfo - pointer to vlan namne info structure
 *                       pu2VlanNameCount - pointer to vlan name count
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpVlndbGetAllVlanName (UINT4 u4PortNum, tVlanNameInfo * pVlanNameInfo,
                         UINT2 *pu2VlanNameCount)
{

    L2IwfGetAllVlanNameIfTxEnabled (u4PortNum, pVlanNameInfo, pu2VlanNameCount);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpVlndbNotifyVlnInfoChgForPort 
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message 
 *                       queue.
 *                       This function will be called only when a port is
 *                       removed from the vlan membership and when the 
 *                       transmission of vlan name tlv for that vlan is enabled 
 *                       on this port.
 *
 *    INPUT            : u4IfIndex   - Port number
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/

PUBLIC INT4
LldpVlndbNotifyVlnInfoChgForPort (UINT4 u4IfIndex)
{
    tLldpQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "LLDP is not started\r\n");
        return OSIX_FAILURE;
    }

    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                   "LldpVlndbNotifyVlnInfoChgForPort:Sending Vlan name "
                   "Notification for port %d\r\n", u4IfIndex);

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tLldpQMsg *) MemAllocMemBlk (gLldpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpVlndbNotifyVlnInfoChgForPort Intf Msg "
                  " ALLOC_MEM_BLOCK FAILED\r\n");
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpVlndbNotifyVlnInfoChgForPort Intf Msg"
                      "ALLOC_MEM_BLOCK FAILED"));
#endif
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tLldpQMsg));

    pMsg->u4MsgType = LLDP_VLAN_INFO_CHG_FOR_PORT;
    pMsg->u4Port = u4IfIndex;
    i4RetVal = LldpQueEnqAppMsg (pMsg);

    return (i4RetVal);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpVlndbNotifyVlanInfoChg 
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message 
 *                       queue.
 *                       This function will be called in the following cases 
 *                       only when the transmission of vlan name tlv is enabled
 *                       on the ports in the given portlist.
 *                       1. Whenever the vlan name is updated.
 *                       2. When vlan is deleted.
 *                       3. When vlan member ports changes.
 *
 *    INPUT            : VlanPortList - List of ports mapped to the vlan
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/

PUBLIC INT4
LldpVlndbNotifyVlanInfoChg (tPortList VlanPortList)
{
    tLldpQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "LLDP is not started\r\n");
        return OSIX_FAILURE;
    }

    LLDP_TRC (CONTROL_PLANE_TRC,
              "LldpVlndbNotifyVlanInfoChg:Sending Vlan name Notification\r\n");

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tLldpQMsg *) MemAllocMemBlk (gLldpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpVlndbNotifyVlanInfoChg Intf Msg"
                  " ALLOC_MEM_BLOCK FAILED\r\n");
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpVlndbNotifyVlanInfoChg Intf Msg"
                      "ALLOC_MEM_BLOCK FAILED"));
#endif
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tLldpQMsg));

    pMsg->u4MsgType = LLDP_VLAN_INFO_CHG_MSG;
    MEMCPY (pMsg->PortList, VlanPortList, sizeof (tPortList));
    i4RetVal = LldpQueEnqAppMsg (pMsg);

    return (i4RetVal);
}

#endif /* _LLDVLNDB_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  lldvlndb.c                     */
/*-----------------------------------------------------------------------*/
