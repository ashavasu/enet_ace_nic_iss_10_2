/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lldrxsm.c,v 1.38 2016/07/29 09:49:40 siva Exp $
 *
 * Description: This file contains the LLDP Rx module related state event 
 *              machine implimentation routines.
 *****************************************************************************/
#ifndef _LLDRXSM_H_
#define _LLDRXSM_H_

#include "lldinc.h"
#include "lldrxsm.h"

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpRxSemRun 
 *                                                                          
 *    DESCRIPTION      : Entry point to LLDP Receive State Event Machine. 
 *                       This function call the appropriate Rx state routine
 *                       based on the current state and event received.
 *                       Rx State-Event Machine is running on per port 
 *                       basis. So current state of a port is maintained in
 *                       the Local Port Info structure.
 *                       
 *    INPUT            : pLocPortInfo - Local Port Structure pointer
 *                                                                          
 *    OUTPUT           : NONE
 *                                                                          
 *    RETURNS          : NONE
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
LldpRxSemRun (tLldpLocPortInfo * pLocPortInfo, INT4 i4Event)
{
    INT4                i4CurrentState = LLDP_INVALID_VALUE;
    INT4                i4ActionProcId = LLDP_INVALID_VALUE;

    /* pLocPortInfo should not be NULL */
    FSAP_ASSERT (pLocPortInfo != NULL);
    /* Validate the Event value */
    FSAP_ASSERT (i4Event < LLDP_RX_MAX_EVENTS);
    FSAP_ASSERT (i4Event >= LLDP_RX_EV_PORT_OPER_DOWN);

    /* Get the Current State */
    i4CurrentState = pLocPortInfo->i4RxSemCurrentState;
    /* Validate the sate value */
    FSAP_ASSERT (i4CurrentState < LLDP_RX_MAX_STATES);
    FSAP_ASSERT (i4CurrentState >= LLDP_RX_WAIT_PORT_OPERATIONAL);

    LLDP_TRC_ARG3 (CONTROL_PLANE_TRC,
                   "RX-SEM: PORT: %d, STATE: %s, EVENT: %s\r\n",
                   pLocPortInfo->u4LocPortNum,
                   gau1LldpRxStateStr[i4CurrentState],
                   gau1LldpRxEvntStr[i4Event]);
    /* Get the Index of the Action Procedure */
    i4ActionProcId = gau1LldpRxSem[i4Event][i4CurrentState];

    /* Call the correcponding Action Procedure */
    if (i4ActionProcId < LLDP_MAX_RXSEM_FN_PTRS)
    {
        (*gaLldpRxActionProc[i4ActionProcId]) (pLocPortInfo);
    }

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpRxSemEventIgnore 
 *                                                                          
 *    DESCRIPTION      : This function will handle the invalid events in SEM
 *                       
 *    INPUT            : pLocPortInfo - Local Port Structure pointer
 *                                                                          
 *    OUTPUT           : NONE
 *                                                                          
 *    RETURNS          : NONE
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
LldpRxSemEventIgnore (tLldpLocPortInfo * pLocPortInfo)
{
    UNUSED_PARAM (pLocPortInfo);
    LLDP_TRC_ARG1 (INIT_SHUT_TRC | CONTROL_PLANE_TRC,
                   "RX-SEM[%d]: Event Impossible.\r\n",
                   pLocPortInfo->u4LocPortNum);

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpRxSmStateWaitPortOperational
 *                                                                          
 *    DESCRIPTION      : This procedure is invoked when enters to the state 
 *                       
 *                       LLDP_RX_WAIT_PORT_OPERATIONAL. 
 *                       
 *
 *    INPUT            : pLocPortInfo - Local Port Structure pointer
 *                                                                          
 *    OUTPUT           : NONE
 *                                                                          
 *    RETURNS          : NONE
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
LldpRxSmStateWaitPortOperational (tLldpLocPortInfo * pLocPortInfo)
{
    /* Set the current state */
    LLDP_RXSEM_SET_CURRENT_STATE (pLocPortInfo, LLDP_RX_WAIT_PORT_OPERATIONAL);
    LLDP_TRC_ARG1 (INIT_SHUT_TRC | CONTROL_PLANE_TRC,
                   "RX-SEM[%d]: State = LLDP_RX_WAIT_PORT_OPERATIONAL\r\n",
                   pLocPortInfo->u4LocPortNum);

    pLocPortInfo->pBackPtrRemoteNode = NULL;

    /* Do Nothing */
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpRxSmStateDelAgedInfo
 *                                                                          
 *    DESCRIPTION      : This procedure is invoked when Rx State Event Machine
 *                       enters to the state
 *                       LLDP_RX_DELETE_AGED_INFO.  
 *
 *    INPUT            : pLocPortInfo - Local Port Structure pointer
 *                                                                          
 *    OUTPUT           : NONE
 *                                                                          
 *    RETURNS          : NONE
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
LldpRxSmStateDelAgedInfo (tLldpLocPortInfo * pLocPortInfo)
{
    tLldpRemoteNode    *pRemoteNode = NULL;

#ifdef L2RED_WANTED
    tLldpRedMsapRBIndex MsapRBIndex;

    MEMSET (&MsapRBIndex, 0, sizeof (tLldpRedMsapRBIndex));
#endif

    /* Set the current state */
    LLDP_RXSEM_SET_CURRENT_STATE (pLocPortInfo, LLDP_RX_DELETE_AGED_INFO);
    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                   "RX-SEM[%d]: State = LLDP_RX_DELETE_AGED_INFO\r\n",
                   pLocPortInfo->u4LocPortNum);

    /* Get the Remote Node Pointer */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;

#ifdef L2RED_WANTED
    LldpRedUpdNxtRemNodeBlkUpdPtr (pRemoteNode);
    /* Get the MSAP RBTree Index of the remote node
     * which is being deleted */
    LldpRedFillMsapRBIndex (&MsapRBIndex, pRemoteNode);
#endif
    /* Application Ageout Indication should be given for the 
     * port */
    LldpUtlSendAppAgeout (pLocPortInfo);
    /* Delete the RemoteNode */
    LldpRxUtlDelRemoteNode (pRemoteNode);

    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC, "RX-SEM[%d]: "
                   "Remote Node is deleted successfully.\r\n",
                   pLocPortInfo->u4LocPortNum);
    /* Update Counters and statistics */
    LLDP_INCR_CNTR_RX_AGEOUTS (pLocPortInfo);
    LLDP_REMOTE_STAT_TABLES_AGEOUTS++;
    LLDP_REMOTE_STAT_TABLES_DELETES++;
    LLDP_REMOTE_STAT_LAST_CHNG_TIME = LLDP_SYS_UP_TIME ();

#ifdef L2RED_WANTED
    /* Send remote table change(delete/ageout) information */
    LldpRedSendSyncUpMsg ((UINT1) LLDP_RED_REM_TAB_AGEOUT_MSG,
                          (VOID *) &MsapRBIndex);
#endif
    /* Send Remtoe table change notification */
    if ((pLocPortInfo->PortConfigTable.u1NotificationEnable == LLDP_TRUE) &&
            ((pLocPortInfo->PortConfigTable.u1FsConfigNotificationType ==
              LLDP_REMOTE_CHG_NOTIFICATION) ||
             (pLocPortInfo->PortConfigTable.u1FsConfigNotificationType ==
              LLDP_REMOTE_CHG_MIS_CFG_NOTIFICATION)))
    {
        gLldpGlobalInfo.bSendRemTblNotif = OSIX_TRUE;

        /* Check Whether Topology change Notification is enabled */
        if (pLocPortInfo->bMedCapable == LLDP_MED_TRUE)
        {
            /* call Notification when remote node is aged out */
            LldpMedSendNotification (pRemoteNode);
        }
        else
        {
            LldpSendNotification (NULL, LLDP_REM_TABLE_CHG);
        }
    }
    if (pLocPortInfo->u2NoOfNeighbors == 1) 
    { 
        /* After deleting a neighbor there is only one neighbor 
         * present which needs to be informed to dcbx. 
         */ 
        pLocPortInfo->u1RefreshNotify = LLDP_TRUE; 
    } 

    /* Reset the bMedCapable flag if it is enabled 
     * since the neighbor is aged out*/
    pLocPortInfo->bMedCapable = LLDP_MED_FALSE;
    /* Reconstruct LLDP PDU since we are changing Local port info*/
    if (LldpTxUtlHandleLocPortInfoChg (pLocPortInfo) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpRxSmStateDelAgedInfo: "
                  "LldpTxUtlHandleLocPortInfoChg failed\r\n");
    }
    /* UCT */
    LldpRxSmStateWaitPortOperational (pLocPortInfo);

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpRxSmStateRxLldpInit
 *                                                                          
 *    DESCRIPTION      : This procedure is invoked when enters to the state
 *                       LLDP_RX_LLDP_INITIALIZE.
 *                       
 *                       
 *
 *    INPUT            : pLocPortInfo - Local Port Structure pointer
 *                                                                          
 *    OUTPUT           : NONE
 *                                                                          
 *    RETURNS          : NONE
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
LldpRxSmStateRxLldpInit (tLldpLocPortInfo * pLocPortInfo)
{
    /* Set the current state */
    LLDP_RXSEM_SET_CURRENT_STATE (pLocPortInfo, LLDP_RX_LLDP_INITIALIZE);
    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                   "RX-SEM[%d]: State = LLDP_RX_LLDP_INITIALIZE\r\n",
                   pLocPortInfo->u4LocPortNum);
    /* Set the SEM variables */
    pLocPortInfo->pBackPtrRemoteNode = NULL;
    pLocPortInfo->pu1RxLldpdu = NULL;
    pLocPortInfo->u2RxPduLen = 0;
    pLocPortInfo->u1RxFrameType = 0;
    pLocPortInfo->i4RxTtl = LLDP_INVALID_VALUE;
    pLocPortInfo->u2TlvRcvdBmp = 0;
    /*LLDP-MED variables*/
    pLocPortInfo->u2MedTlvRcvdBmp = 0;
    pLocPortInfo->u2PolicyBmp = 0;
    pLocPortInfo->bMedCapable = LLDP_MED_FALSE;
    pLocPortInfo->u2NoOfNeighbors = 0;

    /* Delete all the Remote Node associated with current port */
    LldpRxUtlDelRemInfoForPort (pLocPortInfo);

    /* Check admin status */
    if (pLocPortInfo->PortConfigTable.i4AdminStatus == LLDP_ADM_STAT_TX_AND_RX
        || pLocPortInfo->PortConfigTable.i4AdminStatus == LLDP_ADM_STAT_RX_ONLY)
    {
        if (LLDP_MODULE_STATUS () == LLDP_ENABLED)
        {
            /* module admin status is already enabled. and module status is also
             * enabled. So move to the next state */
            LldpRxSmStateRxWaitForFrame (pLocPortInfo);
            return;
        }
        else
        {
            LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                           "RX-SEM[%d]: LLDP Module status is desabled. "
                           "So wait in LLDP_RX_LLDP_INITIALIZE state\r\n",
                           pLocPortInfo->u4LocPortNum);
            return;
        }
    }
    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC, "RX-SEM[%d]: "
                   "LLDP Admin status is in Disable/TxOnly state. So, wait in "
                   "LLDP_RX_LLDP_INITIALIZE state\r\n",
                   pLocPortInfo->u4LocPortNum);
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpRxSmStateRxWaitForFrame
 *                                                                          
 *    DESCRIPTION      : This procedure is invoked when enters to the state
 *                       LLDP_RX_WAIT_FOR_FRAME.
 *
 *    INPUT            : pLocPortInfo - Local Port Structure pointer
 *                                                                          
 *    OUTPUT           : NONE
 *                                                                          
 *    RETURNS          : NONE
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
LldpRxSmStateRxWaitForFrame (tLldpLocPortInfo * pLocPortInfo)
{
    /* Set the current state */
    LLDP_RXSEM_SET_CURRENT_STATE (pLocPortInfo, LLDP_RX_WAIT_FOR_FRAME);
    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                   "RX-SEM[%d]: State = LLDP_RX_WAIT_FOR_FRAME\r\n",
                   pLocPortInfo->u4LocPortNum);
    /* Update the SEM Vriables */
    pLocPortInfo->pu1RxLldpdu = NULL;
    pLocPortInfo->u2RxPduLen = 0;
    pLocPortInfo->u1RxFrameType = 0;
    pLocPortInfo->i4RxTtl = LLDP_INVALID_VALUE;
    pLocPortInfo->u2TlvRcvdBmp = 0;
    pLocPortInfo->u2MedTlvRcvdBmp = 0;
    pLocPortInfo->u2PolicyBmp = 0;

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpRxSmStateRxFrame
 *                                                                          
 *    DESCRIPTION      : This procedure is invoked when enters to the state
 *                       LLDP_RX_FRAME_RECEIVED.
 *
 *    INPUT            : pLocPortInfo - Local Port Structure pointer
 *                                                                          
 *    OUTPUT           : NONE
 *                                                                          
 *    RETURNS          : NONE
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
LldpRxSmStateRxFrame (tLldpLocPortInfo * pLocPortInfo)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT4               u4RemNodeCount = 0;
    UINT4               i4RemoteIndex = 0;
    INT4                i4ChassisIdSubtype = 0;
    INT4                i4PortIdSubtye = 0;
    UINT1              *pu1Lldpdu = NULL;
    UINT1               au1ChassisId[LLDP_MAX_LEN_CHASSISID];
    UINT1               au1PortId[LLDP_MAX_LEN_PORTID];
    UINT1               au1PduMsgDigest[16];
    BOOL1               bIsProcessingDone = OSIX_FALSE;

    /* Set the current state */
    LLDP_RXSEM_SET_CURRENT_STATE (pLocPortInfo, LLDP_RX_FRAME_RECEIVED);
    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                   "RX-SEM[%d]: State = LLDP_RX_FRAME_RECEIVED\r\n",
                   pLocPortInfo->u4LocPortNum);


    /* Get the LLDPDU - Liner buf (Network Byte Order) */
    pu1Lldpdu = pLocPortInfo->pu1RxLldpdu;

    MEMSET (&au1ChassisId[0], 0, LLDP_MAX_LEN_CHASSISID);
    MEMSET (&au1PortId[0], 0, LLDP_MAX_LEN_PORTID);

    if (LldpRxGetMSAPIdFromPdu (pu1Lldpdu, &i4ChassisIdSubtype,
                                &au1ChassisId[0], &i4PortIdSubtye,
                                &au1PortId[0]) != OSIX_SUCCESS)
    {
        LLDP_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                       "RX-SEM[%d]: Get MSAPId From LLDPDU is Failed. "
                       "-- Bad Frame --\r\n", pLocPortInfo->u4LocPortNum);
        /* Bad Frame */
        LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
        LLDP_INCR_CNTR_RX_FRAMES_DISCARDED (pLocPortInfo);
        LLDP_INCR_CNTR_RX_FRAMES (pLocPortInfo);
        LldpRxSmStateRxWaitForFrame (pLocPortInfo);
        return;
    }

    /*LLDP PDU with unknown ChassisID Subtype/PortID subtype is */
    /*received, Discard the LLDP PDU */
    /*Refer:Table 8-2.For value of Chassis ID subtype enumeration */
    /*in IEEE P802.1AB-REV/D6.0 standard */
    if ((i4ChassisIdSubtype > LLDP_SEVEN || i4ChassisIdSubtype == LLDP_ZERO) ||
        i4PortIdSubtye > LLDP_SEVEN || i4PortIdSubtye == LLDP_ZERO)
    {
        LLDP_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                       "RX-SEM[%d]:Dicarding the LLDP PDU"
                       "-- Bad Frame --\r\n", pLocPortInfo->u4LocPortNum);
        /* Bad Frame */
        LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
        LLDP_INCR_CNTR_RX_FRAMES_DISCARDED (pLocPortInfo);
        LLDP_INCR_CNTR_RX_FRAMES (pLocPortInfo);
        LldpRxSmStateRxWaitForFrame (pLocPortInfo);
        return;
    }

    /* Find the Remote Node by MSAP ID in RemMSAPRBTree */
    pRemoteNode = LldpRxUtlGetRemoteNodeByMSAP (pLocPortInfo->u4LocPortNum,
                                                i4ChassisIdSubtype,
                                                &au1ChassisId[0],
                                                i4PortIdSubtye, &au1PortId[0]);

    /* Calculate Message Digest of the received LLDPDU */
    LldpPortCalculatePduMsgDigest (pu1Lldpdu, pLocPortInfo->u2RxPduLen,
                                   &au1PduMsgDigest[0]);

    /* Identify the Frame Type : NewFrame/UpdateFrame/RefreshFrame */
    /* ----------------------------------------------------------- */
    if (pRemoteNode == NULL)    /* New Neighbor Info */
    {
        pLocPortInfo->u1RxFrameType = LLDP_RX_FRAME_NEW;
    }
    else                        /* Existing Neighbor Info */
    {
        /* Compare Checksum */
        if (LldpRxComparePduMsgDigest (&au1PduMsgDigest[0],
                                       pRemoteNode->au1PduMsgDigest)
            == OSIX_SUCCESS)
        {
            pLocPortInfo->u1RxFrameType = LLDP_RX_FRAME_REFRESH;
            LldpRxFrameNotificationToApplns (pLocPortInfo);
            pLocPortInfo->pBackPtrRemoteNode = pRemoteNode;
            LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                           "RX-SEM[%d]: Checksum Matched. REFRESH FRAME "
                           "Received.\r\n", pLocPortInfo->u4LocPortNum);
            /* Process Refresh Frame */
            if (LldpRxProcessRefreshFrame (pLocPortInfo) != OSIX_SUCCESS)
            {
                LLDP_TRC_ARG1 (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                               "RX-SEM[%d]: Failed to process the refresh "
                               "frame\r\n", pLocPortInfo->u4LocPortNum);
                if (OSIX_FALSE == pLocPortInfo->bstatsFramesInerrorsTotal)
                {
                    LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
                    pLocPortInfo->bstatsFramesInerrorsTotal = OSIX_TRUE;
                }
                LLDP_INCR_CNTR_RX_FRAMES_DISCARDED (pLocPortInfo);
            }


            LLDP_TRC_ARG1 (CONTROL_PLANE_TRC, "RX-SEM[%d]: "
                           "Successfully Restarted the Info Age Timer\r\n",
                           pLocPortInfo->u4LocPortNum);
            /*This check is added to handle the scenario where LLDP-MED is disabled for 
             * the first time when LLDP-MED packet is received and then enable the 
             * LLDP-MED status. This time the next packet from MED device will be 
             * treated as refresh frame and data base will not be updated with the 
             * recevied values MED TLVs. */

            if ((pLocPortInfo->bMedCapable == LLDP_MED_TRUE) && 
                    (pRemoteNode->RemMedCapableInfo.u2MedRemCapEnable == 0))
            {
                /* Since this is the first LLDP-MED packet received after Enabling 
                 * LLDP-MED Admin Status, Consider this as Update Frame.*/
                pLocPortInfo->u1RxFrameType = LLDP_RX_FRAME_UPDATE;
                bIsProcessingDone = OSIX_TRUE;
                LLDP_TRC_ARG1 (CONTROL_PLANE_TRC, "RX-SEM[%d]: "
                        "Received LLDP-MED packet\r\n", pLocPortInfo->u4LocPortNum);
            }
            else
            {
	            if (pLocPortInfo->u1RefreshNotify == LLDP_TRUE) 
    	        { 
        	        /* u1RefreshNotify flag is set means multiple neghbors 
            	     * scenario is cleared and current neighbor count is 1 
                	 * so process the received packet and indicate to dcbx. 
	                 */ 
    	            pLocPortInfo->u1RxFrameType = LLDP_RX_FRAME_UPDATE; 
	                if (LldpRxProcessFrame (pLocPortInfo) != OSIX_SUCCESS) 
	                { 
	                    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC, 
	                            "RX-SEM[%d]: LLDPDU Validation Failed. " 
	                            "-- Bad Frame --\r\n", pLocPortInfo->u4LocPortNum); 
    	            } 
	       
	                LldpRxPostTlvsToApplications (pLocPortInfo); 
	                pLocPortInfo->u1RefreshNotify = LLDP_FALSE; 
	                pLocPortInfo->u1RxFrameType = LLDP_RX_FRAME_REFRESH; 
	             }

                /* For frame type == LLDP_RX_FRAME_REFRESH and there are applications that
                 * need to be intimated then continue to parse and send TLVs to
                 * applications that are waiting and change their status so that future
                 * unmodified TLVs will not be given to them */
                if (pLocPortInfo->bIsAnyPendingApp == OSIX_FALSE)
                {
                    /* Update Received Frame Statistics */
                    /* If the received packet contains MED TLVs, Increment LLDP-MED
                     * receive counter else increment LLDP PDU received counter */
                    if (pLocPortInfo->bMedCapable == LLDP_MED_TRUE)
                    {
                        LLDP_MED_INCR_CNTR_RX_FRAMES(pLocPortInfo);
                    }
                    else
                    {
                        LLDP_INCR_CNTR_RX_FRAMES (pLocPortInfo);
                    }
                    /* Go to the wait for frame state */
                    LldpRxSmStateRxWaitForFrame (pLocPortInfo);
                    return;
                }
            }
        }
        else                    /* Updation Frame */
        {
            pLocPortInfo->u1RxFrameType = LLDP_RX_FRAME_UPDATE;
            LldpRxFrameNotificationToApplns (pLocPortInfo);
            LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                           "RX-SEM[%d]: Checksum Not Matched\r\n",
                           pLocPortInfo->u4LocPortNum);
        }
    }

    /* Process Received PDU : Parsing and Validation */
    /* --------------------------------------------- */

    /* if bIsProcessingDone is set to TRUE, The processing of the TLVs 
     * is already done while processing Refresh frame. 
     * So no need to do it again */
    if (bIsProcessingDone != OSIX_TRUE)
    {
        if (LldpRxProcessFrame (pLocPortInfo) != OSIX_SUCCESS)
        {
            LLDP_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "RX-SEM[%d]: LLDPDU Validation Failed. "
                    "-- Bad Frame --\r\n", pLocPortInfo->u4LocPortNum);
            /* Validation failed. Bad Frame */
            if (OSIX_FALSE == pLocPortInfo->bstatsFramesInerrorsTotal)
            {
                LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
                pLocPortInfo->bstatsFramesInerrorsTotal = OSIX_TRUE;
            }
            if (pLocPortInfo->bMedFrameDiscarded == LLDP_MED_TRUE)
            {
                LLDP_MED_INCR_CNTR_RX_FRAMES_DISCARDED(pLocPortInfo);
            }
            else
            {
                LLDP_INCR_CNTR_RX_FRAMES_DISCARDED (pLocPortInfo);
            }
            if (pLocPortInfo->bMedCapable == LLDP_MED_TRUE)
            {
                LLDP_MED_INCR_CNTR_RX_FRAMES(pLocPortInfo);
            }
            else
            {
                LLDP_INCR_CNTR_RX_FRAMES (pLocPortInfo);
            }
            LldpRxSmStateRxWaitForFrame (pLocPortInfo);
            return;
        }
    }

    /* LLDPDU is Validated, Take decission based on processing result and Frame
     * Type */
    /* ---------------------------------------------------------------------- */
    if (pLocPortInfo->i4RxTtl == 0)    /* If shutdown Frame */
    {
        pLocPortInfo->u1RxFrameType = LLDP_RX_FRAME_SHUTDOWN;
        /* Store the RemoteNode pointer for future reference */
        pLocPortInfo->pBackPtrRemoteNode = pRemoteNode;
        /* Update Received Frame Statistics */
        /* If the received packet contains MED TLVs, Increment LLDP-MED receive
         * counter else increment LLDP PDU received counter */
        if (pLocPortInfo->bMedCapable == LLDP_MED_TRUE)
        {
            LLDP_MED_INCR_CNTR_RX_FRAMES(pLocPortInfo);
        }
        else
        {
            LLDP_INCR_CNTR_RX_FRAMES (pLocPortInfo);
        }

        LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                       "RX-SEM[%d]: ShutDown Frame Received\r\n",
                       pLocPortInfo->u4LocPortNum);
        /* Go to Delete Info state */
        LldpRxSmStateDelInfo (pLocPortInfo);

        return;
    }
    /* For New/Update Frames */
    else if (pLocPortInfo->u1RxFrameType != LLDP_RX_FRAME_REFRESH)
    {
        if (pLocPortInfo->u1RxFrameType == LLDP_RX_FRAME_UPDATE)
        {
            LLDP_TRC_ARG3 (CONTROL_PLANE_TRC,
                           "RX-SEM[%d]: Update Frame Received from Neighbor -"
                           " ChassisId: %s, PortId: %s\r\n",
                           pLocPortInfo->u4LocPortNum,
                           pRemoteNode->au1RemChassisId,
                           pRemoteNode->au1RemPortId);

            /* Maintain the old remote index for the update frame */
            i4RemoteIndex = pRemoteNode->i4RemIndex;
#ifdef L2RED_WANTED
            LldpRedUpdNxtRemNodeBlkUpdPtr (pRemoteNode);
#endif

            /* No need to age out the application info here as
             * we are not sure the exact TLVs that are agedout. 
             * So do the same in LldpRxProcessFrame function to
             * identify the aged out TLVs */

            /* Delete the Existing Remote Entries */
            LldpRxUtlDelRemoteNode (pRemoteNode);
            /* Reset MedEnable flag only when the updated frame is received 
             * without Med Capability TLV .*/
            if ((LLDP_MED_IS_TLV_RCVD_BMP_SET (pLocPortInfo, LLDP_MED_RECV_MED_CAP_TLV) 
                 != OSIX_TRUE) && (pLocPortInfo->bMedCapable == LLDP_MED_TRUE))
            {
                pLocPortInfo->bMedCapable = LLDP_MED_FALSE;
                if (LldpTxUtlHandleLocPortInfoChg (pLocPortInfo) != OSIX_SUCCESS)
                {
                    LLDP_TRC (CONTROL_PLANE_TRC, "LldpRxSmStateRxFrame: "
                              "LldpTxUtlHandleLocPortInfoChg failed\r\n");
                }

            }          
        }
        else if (pLocPortInfo->u1RxFrameType == LLDP_RX_FRAME_NEW)
        {
            LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                           "RX-SEM[%d]: New Neighbor Information "
                           "received\r\n", pLocPortInfo->u4LocPortNum);
            /* Check the too many neighbor condition */
            RBTreeCount (gLldpGlobalInfo.RemMSAPRBTree, &u4RemNodeCount);
            if (u4RemNodeCount == LLDP_MAX_NEIGHBOR_COUNT)
            {                    /* Too Many Neighbor */
                if (pLocPortInfo->pBackPtrRemoteNode != NULL)
                {
                    pLocPortInfo->pBackPtrRemoteNode->bTooManyNeighbors =
                        OSIX_TRUE;
                }
                LLDP_TRC_ARG1 (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                               "RX-SEM[%d]: -- Too Many Neighbors --\r\n",
                               pLocPortInfo->u4LocPortNum);
                LLDP_REMOTE_STAT_TABLES_DROPS++;
                /* Go to the wait for frame state */
                LldpRxSmStateRxWaitForFrame (pLocPortInfo);
                return;
            }
            LldpTxTmrSmMachine (pLocPortInfo, LLDP_TX_TMR_EV_NEIGHBOR_DETECTED);
            /* Get a new remote index for the new frame */
            i4RemoteIndex = LLDP_GET_NEXT_FREE_REM_INDEX ();
        }

        /* Create Memory for a New/updated Remote Node */
        if ((pRemoteNode = (tLldpRemoteNode *)
             (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
        {
            LLDP_TRC_ARG1 (LLDP_CRITICAL_TRC,
                           "RX-SEM[%d]: Failed to Allocate Memory "
                           "for Remote Node\r\n", pLocPortInfo->u4LocPortNum);
            LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
            /* Go to the wait for frame state */
            LldpRxSmStateRxWaitForFrame (pLocPortInfo);
            return;
        }

        MEMSET (pRemoteNode, 0, sizeof (tLldpRemoteNode));
        /* Update the Remote Indices */
        pRemoteNode->u4RemLastUpdateTime = LLDP_SYS_UP_TIME ();
        pRemoteNode->i4RemLocalPortNum = (INT4) pLocPortInfo->i4IfIndex;
        pRemoteNode->u4DestAddrTblIndex = pLocPortInfo->u4DstMacAddrTblIndex;
        pRemoteNode->i4RemIndex = i4RemoteIndex;

        /* Store the new message digest */
        MEMCPY (&pRemoteNode->au1PduMsgDigest[0], &au1PduMsgDigest[0], 16);

        /*Update the Remote changes flag */
        if (pLocPortInfo->u1RxFrameType == LLDP_RX_FRAME_UPDATE)
        {
            pRemoteNode->bRemoteChanges = OSIX_TRUE;
        }

        /* Store the RemoteNode pointer for future reference */
        pLocPortInfo->pBackPtrRemoteNode = pRemoteNode;
        /* Update Info Frame or New Frame */

        LldpRxSmStateUpdateInfo (pLocPortInfo);
        LldpRxPostTlvsToApplications (pLocPortInfo);
    }
    else
    {
        /* If the received packet contains MED TLVs, Increment LLDP-MED receive
         * counter else increment LLDP PDU received counter */
        if (pLocPortInfo->bMedCapable == LLDP_MED_TRUE)
        {
            LLDP_MED_INCR_CNTR_RX_FRAMES(pLocPortInfo);
        }
        else
        {
            LLDP_INCR_CNTR_RX_FRAMES (pLocPortInfo);
        }
        /* Now all the Rcvd TLVs are updated in the Application table
         * so scan all the applications on the port (pLocPortInfo->u4LocPortNum) and 
         * send events and reset the TX status. 
         * If any of the application has not received any TLV
         * then send AGED OUT indication */
        LldpRxPostTlvsToApplications (pLocPortInfo);

        /* Go to the wait for frame state */
        LldpRxSmStateRxWaitForFrame (pLocPortInfo);
    }

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpRxSmStateDelInfo 
 *                                                                          
 *    DESCRIPTION      : This procedure is invoked when enters to the state
 *                       LLDP_RX_DELETE_INFO.
 *
 *    INPUT            : pLocPortInfo - Local Port Structure pointer
 *                                                                          
 *    OUTPUT           : NONE
 *                                                                          
 *    RETURNS          : NONE
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
LldpRxSmStateDelInfo (tLldpLocPortInfo * pLocPortInfo)
{
    tLldpRemoteNode    *pRemoteNode = NULL;

#ifdef L2RED_WANTED
    tLldpRedMsapRBIndex MsapRBIndex;
    UINT1               u1MsgType = (UINT1) LLDP_RED_REM_TAB_DELETE_MSG;

    MEMSET (&MsapRBIndex, 0, sizeof (tLldpRedMsapRBIndex));
#endif

    /* Set the current state */
    LLDP_RXSEM_SET_CURRENT_STATE (pLocPortInfo, LLDP_RX_DELETE_INFO);
    LLDP_TRC_ARG1 (INIT_SHUT_TRC | CONTROL_PLANE_TRC,
                   "RX-SEM[%d]: State = LLDP_RX_DELETE_INFO\r\n",
                   pLocPortInfo->u4LocPortNum);
    /* Get the Remote Node pointer */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;
    /* Delete the the RemoteNode */
    if (pRemoteNode != NULL)
    {
#ifdef L2RED_WANTED
        LldpRedUpdNxtRemNodeBlkUpdPtr (pRemoteNode);
        /* Get the MSAP RBTree Index of the remote node
         * which is being deleted */
        LldpRedFillMsapRBIndex (&MsapRBIndex, pRemoteNode);
#endif
        /* Application Ageout Indication should be given for the 
         * port */
        LldpUtlSendAppAgeout (pLocPortInfo);
        /* Update the counters */
        LLDP_REMOTE_STAT_TABLES_DELETES++;
        LLDP_REMOTE_STAT_LAST_CHNG_TIME = LLDP_SYS_UP_TIME ();
        if (pLocPortInfo->u1RxFrameType != LLDP_RX_FRAME_SHUTDOWN)
        {
            /* If the frame type is not shut down, then the
             * remote node has been aged out.
             * Ageout counter need to be incremented */
            LLDP_INCR_CNTR_RX_AGEOUTS (pLocPortInfo);
            LLDP_REMOTE_STAT_TABLES_AGEOUTS++;
        }
#ifdef L2RED_WANTED
        /* Fill remote table change(delete/ageout) information */
        if (pLocPortInfo->u1RxFrameType != LLDP_RX_FRAME_SHUTDOWN)
        {
            u1MsgType = (UINT1) LLDP_RED_REM_TAB_AGEOUT_MSG;
        }
        /* Send remote table change(delete/ageout) information */
        LldpRedSendSyncUpMsg (u1MsgType, (VOID *) &MsapRBIndex);
#endif

        /* Send Remote table change notification */
        if ((pLocPortInfo->PortConfigTable.u1NotificationEnable == LLDP_TRUE) &&
                ((pLocPortInfo->PortConfigTable.u1FsConfigNotificationType ==
                  LLDP_REMOTE_CHG_NOTIFICATION) ||
                 (pLocPortInfo->PortConfigTable.u1FsConfigNotificationType ==
                  LLDP_REMOTE_CHG_MIS_CFG_NOTIFICATION)))
        {
            gLldpGlobalInfo.bSendRemTblNotif = OSIX_TRUE;

            /* Check Whether Topology change Notification is enabled */
            if (pLocPortInfo->bMedCapable == LLDP_MED_TRUE)
            {
                /* call Notification when remote node is aged out */
                LldpMedSendNotification (pRemoteNode);
            }
            else
            {
                LldpSendNotification (NULL, LLDP_REM_TABLE_CHG);
            }
        }

        /* Deleting the remote node*/
        LldpRxUtlDelRemoteNode (pRemoteNode);
        if (pLocPortInfo->u2NoOfNeighbors == 1) 
        { 
            /* After deleting a neighbor there is only one neighbor 
             * present which needs to be informed to dcbx. 
             */ 
            pLocPortInfo->u1RefreshNotify = LLDP_TRUE; 
        } 
       
        /* If LLDP-MED is enabled on the port, check whether any other LLDP-MED neighbors are
         * learned by the port. If no other LLDP-MED neighbor is present, Set the Flag as
         * Flase. */
        if (pLocPortInfo->bMedCapable == LLDP_MED_TRUE)
        {
            LldpMedResetMedCapableFlag(pLocPortInfo);
            /* Reconstruct LLDP PDU only when there is a change in the
               bMedCapable FLAG */
            if ((pLocPortInfo->bMedCapable == LLDP_MED_FALSE) &&
                    (LldpTxUtlHandleLocPortInfoChg (pLocPortInfo) !=
                     OSIX_SUCCESS))
            {
                LLDP_TRC (ALL_FAILURE_TRC, "LldpRxSmStateDelInfo: "
                        "LldpTxUtlHandleLocPortInfoChg failed\r\n");
            }
            else if (pLocPortInfo->bMedCapable == LLDP_MED_TRUE)
            {
                LldpMedTriggerLocTlvChng(pLocPortInfo);
            }
        }
    }

    /* UCT */
    LldpRxSmStateRxWaitForFrame (pLocPortInfo);
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpRxSmStateUpdateInfo
 *                                                                          
 *    DESCRIPTION      : This procedure is invoked when enters to the state
 *                       LLDP_RX_UPDATE_INFO.  
 *
 *    INPUT            : pLocPortInfo - Local Port Structure pointer
 *                                                                          
 *    OUTPUT           : NONE
 *                                                                          
 *    RETURNS          : NONE
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
LldpRxSmStateUpdateInfo (tLldpLocPortInfo * pLocPortInfo)
{
    tLldpRemoteNode    *pRemoteNode = NULL;

#ifdef L2RED_WANTED
    tLldpRedRemTabChgInfo RemTabChgInfo;
    UINT1               u1MsgType = (UINT1) LLDP_RED_REM_TAB_INSERT_MSG;
    MEMSET (&RemTabChgInfo, 0, sizeof (tLldpRedRemTabChgInfo));
#endif

    /* Set the current state */
    LLDP_RXSEM_SET_CURRENT_STATE (pLocPortInfo, LLDP_RX_UPDATE_INFO);
    LLDP_TRC_ARG1 (INIT_SHUT_TRC | CONTROL_PLANE_TRC,
                   "RX-SEM[%d]: State = LLDP_RX_UPDATE_INFO\r\n",
                   pLocPortInfo->u4LocPortNum);
    if (pLocPortInfo->u1RxFrameType == LLDP_RX_FRAME_NEW ||
        pLocPortInfo->u1RxFrameType == LLDP_RX_FRAME_UPDATE)
    {
        pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;
    }
    else
    {
        /* UCT */
        LldpRxSmStateRxWaitForFrame (pLocPortInfo);
        /* Update Received Frame Statistics */
        /* If the received packet contains MED TLVs, Increment LLDP-MED receive
         * counter else increment LLDP PDU received counter */
        if (pLocPortInfo->bMedCapable == LLDP_MED_TRUE)
        {
            LLDP_MED_INCR_CNTR_RX_FRAMES(pLocPortInfo);
        }
        else
        {
            LLDP_INCR_CNTR_RX_FRAMES (pLocPortInfo);
        }
        return;
    }
    /* Update the Information */
    if (pRemoteNode == NULL)
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                       "RX-SEM[%d]: No Remote Node Found in port structure\r\n",
                       pLocPortInfo->u4LocPortNum);

    }
    else
    {
        pRemoteNode->bRcvdPVidTlv = OSIX_FALSE;
        pRemoteNode->bRcvdMacPhyTlv = OSIX_FALSE;
        pRemoteNode->bRcvdLinkAggTlv = OSIX_FALSE;
        pRemoteNode->bRcvdMaxFrameTlv = OSIX_FALSE;
        /* Update the Remote System Information Tables */
        if (LldpRxUpdateRemDataBase (pLocPortInfo) == OSIX_SUCCESS)
        {
            if (LldpRxUtlAddRemoteNode (pRemoteNode) == OSIX_FAILURE)
            {
                /* Release the memory allocated for Remote Node */
                MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                        (UINT1 *) pRemoteNode);
                pRemoteNode = NULL;
                LLDP_TRC (LLDP_CRITICAL_TRC,
                        "LldpRxSmStateUpdateInfo : Failed to add remote node to "
                        "RBTree\r\n");
                /* Update Received Frame Statistics */
                /* If the received packet contains MED TLVs, Increment LLDP-MED receive
                 * counter else increment LLDP PDU received counter */
                if (pLocPortInfo->bMedCapable == LLDP_MED_TRUE)
                {
                    LLDP_MED_INCR_CNTR_RX_FRAMES(pLocPortInfo);
                }
                else
                {
                    LLDP_INCR_CNTR_RX_FRAMES (pLocPortInfo);
                }
                return;
            }

            if ((pLocPortInfo->bMedCapable == LLDP_MED_TRUE) && 
                    (pRemoteNode->RemMedCapableInfo.u1MedRemDeviceClass == 
                     LLDP_MED_CLASS_3_DEVICE))
            {
                if (LldpTxUtlConstructPreformedBuf (pLocPortInfo, LLDP_INFO_FRAME)
                        != OSIX_SUCCESS)
                {
                    LLDP_TRC (ALL_FAILURE_TRC | LLDP_MED_CAPAB_TRC, "LldpRxSmStateUpdateInfo: "
                            "Construction of preformed buffer Failed.\r\n");
                }

            }

            LLDP_REMOTE_STAT_LAST_CHNG_TIME = LLDP_SYS_UP_TIME ();

            if (pLocPortInfo->u1RxFrameType == LLDP_RX_FRAME_NEW)
            {
                /* Update the New free remote index */
                LLDP_UPDT_NEXT_FREE_REM_INDEX ();
                /* Update counters */
                LLDP_REMOTE_STAT_TABLES_INSERTS++;
            }
            else if (pLocPortInfo->u1RxFrameType == LLDP_RX_FRAME_UPDATE)
            {
                /* Update counters */
                LLDP_REMOTE_STAT_TABLES_UPDATES++;
            }

            /* Start the Info Age Timer */
            if (LldpRxStartInfoAgeTmr (pRemoteNode) != OSIX_SUCCESS)
            {
                LLDP_TRC_ARG1 (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                               "RX-SEM[%d]: Failed to start the Info Age "
                               "Timer\r\n", pLocPortInfo->u4LocPortNum);
            }

#ifdef L2RED_WANTED
            /* Fill remote table change(insert/update) information */
            if (pLocPortInfo->u1RxFrameType == LLDP_RX_FRAME_UPDATE)
            {
                u1MsgType = (UINT1) LLDP_RED_REM_TAB_UPDATE_MSG;
            }
            RemTabChgInfo.pPortInfo = pLocPortInfo;
            RemTabChgInfo.i4RemIndex = pRemoteNode->i4RemIndex;
            /* Send remote table change(insert/update) information */
            LldpRedSendSyncUpMsg (u1MsgType, (VOID *) &RemTabChgInfo);
#endif
        }
        else                    /* Updation Process Failed */
        {
            LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                           "RX-SEM[%d]: Failed to update the Remote "
                           "Database\r\n", pLocPortInfo->u4LocPortNum);
            /* Delete the Remote Node */
            LldpRxUtlDelRemoteNode (pRemoteNode);
            LLDP_REMOTE_STAT_TABLES_DROPS++;
        }

        /* Send Remtoe table change notification */
        if ((pLocPortInfo->PortConfigTable.u1NotificationEnable == LLDP_TRUE) &&
                ((pLocPortInfo->PortConfigTable.u1FsConfigNotificationType ==
                  LLDP_REMOTE_CHG_NOTIFICATION) ||
                 (pLocPortInfo->PortConfigTable.u1FsConfigNotificationType ==
                  LLDP_REMOTE_CHG_MIS_CFG_NOTIFICATION)))
        {
            gLldpGlobalInfo.bSendRemTblNotif = OSIX_TRUE;

            /* Check Whether Topology change Notification is enabled */
            if (pLocPortInfo->bMedCapable == LLDP_MED_TRUE)
            {
                /* call Notification when remote node is aged out */
                LldpMedSendNotification (pRemoteNode);
            }
            else
            {
                LldpSendNotification (NULL, LLDP_REM_TABLE_CHG);
            }
        }
    }

    /* Update Received Frame Statistics */
    /* If the received packet contains MED TLVs, Increment LLDP-MED receive
     * counter else increment LLDP PDU received counter */
    if (pLocPortInfo->bMedCapable == LLDP_MED_TRUE)
    {
        LLDP_MED_INCR_CNTR_RX_FRAMES(pLocPortInfo);
    }
    else
    {
        LLDP_INCR_CNTR_RX_FRAMES (pLocPortInfo);
    }
    /* UCT */
    LldpRxSmStateRxWaitForFrame (pLocPortInfo);
    return;
}
#endif /* _LLDRXSM_H_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  <filename.c>                   */
/*-----------------------------------------------------------------------*/
