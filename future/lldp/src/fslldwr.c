/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: fslldwr.c,v 1.12 2016/07/12 12:40:29 siva Exp $
*
* Description: Contains LLDP properitory wrapper routines
*********************************************************************/
# include  "lldinc.h"
# include  "fsllddb.h"

VOID
RegisterFSLLDP ()
{
    SNMPRegisterMibWithLock (&fslldpOID, &fslldpEntry, LldpLock, LldpUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fslldpOID, (const UINT1 *) "fslldp");
}

INT4
FsLldpSystemControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsLldpSystemControl (&(pMultiData->i4_SLongValue)));
}

INT4
FsLldpModuleStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsLldpModuleStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsLldpTraceInputGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsLldpTraceInput (pMultiData->pOctetStrValue));
}

INT4
FsLldpTraceOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsLldpTraceOption (&(pMultiData->i4_SLongValue)));
}

INT4
FsLldpTraceLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsLldpTraceLevel (&(pMultiData->i4_SLongValue)));
}
INT4 FsLldpTagStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsLldpTagStatus(&(pMultiData->i4_SLongValue)));
}
INT4 FsLldpConfiguredMgmtIpv4AddressGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsLldpConfiguredMgmtIpv4Address(pMultiData->pOctetStrValue));
}
INT4 FsLldpConfiguredMgmtIpv6AddressGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsLldpConfiguredMgmtIpv6Address(pMultiData->pOctetStrValue));
}

INT4
FsLldpSystemControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsLldpSystemControl (pMultiData->i4_SLongValue));
}

INT4
FsLldpModuleStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsLldpModuleStatus (pMultiData->i4_SLongValue));
}

INT4
FsLldpTraceInputSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsLldpTraceInput (pMultiData->pOctetStrValue));
}

INT4
FsLldpTraceLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsLldpTraceLevel (pMultiData->i4_SLongValue));
}
INT4 FsLldpTagStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsLldpTagStatus(pMultiData->i4_SLongValue));
}

INT4 FsLldpConfiguredMgmtIpv4AddressSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsLldpConfiguredMgmtIpv4Address(pMultiData->pOctetStrValue));
}


INT4 FsLldpConfiguredMgmtIpv6AddressSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsLldpConfiguredMgmtIpv6Address(pMultiData->pOctetStrValue));
}

INT4
FsLldpSystemControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsLldpSystemControl (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsLldpModuleStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsLldpModuleStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsLldpTraceInputTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsLldpTraceInput (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsLldpTraceLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsLldpTraceLevel (pu4Error, pMultiData->i4_SLongValue));
}
INT4 FsLldpTagStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsLldpTagStatus(pu4Error, pMultiData->i4_SLongValue));
}
INT4 FsLldpConfiguredMgmtIpv4AddressTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsLldpConfiguredMgmtIpv4Address(pu4Error, pMultiData->pOctetStrValue));
}


INT4 FsLldpConfiguredMgmtIpv6AddressTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsLldpConfiguredMgmtIpv6Address(pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsLldpSystemControlDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsLldpSystemControl (pu4Error,
                                         pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsLldpModuleStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsLldpModuleStatus (pu4Error, pSnmpIndexList,
                                        pSnmpvarbinds));
}

INT4
FsLldpTraceInputDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsLldpTraceInput (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsLldpTraceLevelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsLldpTraceLevel (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
INT4 FsLldpTagStatusDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsLldpTagStatus(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
INT4 FsLldpConfiguredMgmtIpv4AddressDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, 
                                        tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsLldpConfiguredMgmtIpv4Address(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsLldpConfiguredMgmtIpv6AddressDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList,
                                        tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsLldpConfiguredMgmtIpv6Address(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsLldpLocChassisIdSubtypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsLldpLocChassisIdSubtype (&(pMultiData->i4_SLongValue)));
}

INT4
FsLldpLocChassisIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsLldpLocChassisId (pMultiData->pOctetStrValue));
}

INT4
FsLldpLocChassisIdSubtypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsLldpLocChassisIdSubtype (pMultiData->i4_SLongValue));
}

INT4
FsLldpLocChassisIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsLldpLocChassisId (pMultiData->pOctetStrValue));
}

INT4
FsLldpLocChassisIdSubtypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsLldpLocChassisIdSubtype
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsLldpLocChassisIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsLldpLocChassisId (pu4Error, pMultiData->pOctetStrValue));
}

INT4
GetNextIndexFsLldpLocPortTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsLldpLocPortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsLldpLocPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsLldpLocPortIdSubtypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsLldpLocPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsLldpLocPortIdSubtype (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsLldpLocPortIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsLldpLocPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsLldpLocPortId (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiData->pOctetStrValue));

}

INT4
FsLldpPortConfigNotificationTypeGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsLldpLocPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsLldpPortConfigNotificationType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsLldpLocPortDstMacGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsLldpLocPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetFsLldpLocPortDstMac (pMultiIndex->pIndex[0].i4_SLongValue,
                                       (tMacAddr *) pMultiData->pOctetStrValue->
                                       pu1_OctetList));

}

INT4 
FsLldpMedAdminStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpLocPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedAdminStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4
FsLldpLocPortIdSubtypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsLldpLocPortIdSubtype (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsLldpLocPortIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsLldpLocPortId (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiData->pOctetStrValue));

}

INT4
FsLldpPortConfigNotificationTypeSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsLldpPortConfigNotificationType
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsLldpLocPortDstMacSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsLldpLocPortDstMac (pMultiIndex->pIndex[0].i4_SLongValue,
                                       (*(tMacAddr *) pMultiData->
                                        pOctetStrValue->pu1_OctetList)));

}
INT4 
FsLldpMedAdminStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsLldpMedAdminStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4
FsLldpLocPortIdSubtypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsLldpLocPortIdSubtype (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsLldpLocPortIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsLldpLocPortId (pu4Error,
                                      pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsLldpPortConfigNotificationTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsLldpPortConfigNotificationType (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
FsLldpLocPortDstMacTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return (nmhTestv2FsLldpLocPortDstMac (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          (*(tMacAddr *) pMultiData->
                                           pOctetStrValue->pu1_OctetList)));

}
INT4 
FsLldpMedAdminStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsLldpMedAdminStatus(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4
GetNextIndexFsLldpManAddrConfigTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsLldpManAddrConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsLldpManAddrConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

INT4
FsLldpManAddrConfigOperStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsLldpManAddrConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsLldpManAddrConfigOperStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));
}

INT4
FsLldpMemAllocFailureGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsLldpMemAllocFailure (&(pMultiData->i4_SLongValue)));
}

INT4
FsLldpInputQOverFlowsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsLldpInputQOverFlows (&(pMultiData->i4_SLongValue)));
}

INT4
FsLldpStatsRemTablesUpdatesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsLldpStatsRemTablesUpdates (&(pMultiData->u4_ULongValue)));
}

INT4 FsLldpClearStatsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFsLldpClearStats(&(pMultiData->i4_SLongValue)));
}
INT4 FsLldpClearStatsSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhSetFsLldpClearStats(pMultiData->i4_SLongValue));
}


INT4 FsLldpClearStatsTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhTestv2FsLldpClearStats(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsLldpClearStatsDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2FsLldpClearStats(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fslldpv2VersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFslldpv2Version (&(pMultiData->i4_SLongValue)));
}

INT4
Fslldpv2VersionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFslldpv2Version (pMultiData->i4_SLongValue));
}

INT4
Fslldpv2VersionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fslldpv2Version (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fslldpv2VersionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fslldpv2Version (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsLldpLocPortTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsLldpLocPortTable (pu4Error, pSnmpIndexList,
                                        pSnmpvarbinds));
}

INT4
FsLldpLocChassisIdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsLldpLocChassisId
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsLldpLocChassisIdSubtypeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsLldpLocChassisIdSubtype (pu4Error, pSnmpIndexList,
                                               pSnmpvarbinds));
}

INT4
GetNextIndexFslldpv2ConfigPortMapTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFslldpv2ConfigPortMapTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFslldpv2ConfigPortMapTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
Fslldpv2ConfigPortMapNumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFslldpv2ConfigPortMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFslldpv2ConfigPortMapNum
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
              pu1_OctetList), &(pMultiData->i4_SLongValue)));

}

INT4
Fslldpv2ConfigPortRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFslldpv2ConfigPortMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFslldpv2ConfigPortRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
              pu1_OctetList), &(pMultiData->i4_SLongValue)));

}

INT4
Fslldpv2ConfigPortRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFslldpv2ConfigPortRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
              pu1_OctetList), pMultiData->i4_SLongValue));

}

INT4
Fslldpv2ConfigPortRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Fslldpv2ConfigPortRowStatus (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  (*(tMacAddr *) pMultiIndex->
                                                   pIndex[1].pOctetStrValue->
                                                   pu1_OctetList),
                                                  pMultiData->i4_SLongValue));

}

INT4
Fslldpv2ConfigPortMapTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fslldpv2ConfigPortMapTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFslldpV2DestAddressTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFslldpV2DestAddressTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFslldpV2DestAddressTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FslldpV2DestMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFslldpV2DestAddressTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetFslldpV2DestMacAddress (pMultiIndex->pIndex[0].u4_ULongValue,
                                          (tMacAddr *) pMultiData->
                                          pOctetStrValue->pu1_OctetList));

}

INT4
Fslldpv2DestRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFslldpV2DestAddressTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFslldpv2DestRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FslldpV2DestMacAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFslldpV2DestMacAddress (pMultiIndex->pIndex[0].u4_ULongValue,
                                          (*(tMacAddr *) pMultiData->
                                           pOctetStrValue->pu1_OctetList)));

}

INT4
Fslldpv2DestRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFslldpv2DestRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FslldpV2DestMacAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return (nmhTestv2FslldpV2DestMacAddress (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             (*(tMacAddr *) pMultiData->
                                              pOctetStrValue->pu1_OctetList)));

}

INT4
Fslldpv2DestRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Fslldpv2DestRowStatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FslldpV2DestAddressTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FslldpV2DestAddressTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
INT4 GetNextIndexFsLldpStatsTaggedTxPortTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsLldpStatsTaggedTxPortTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsLldpStatsTaggedTxPortTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pFirstMultiIndex->pIndex[1].u4_ULongValue,
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsLldpStatsTaggedTxPortFramesTotalGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpStatsTaggedTxPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpStatsTaggedTxPortFramesTotal(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
