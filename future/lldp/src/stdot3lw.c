/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: stdot3lw.c,v 1.16 2015/05/16 12:02:01 siva Exp $
*
* Description: LLDP dot3 Protocol Low Level Routines
*********************************************************************/
#include "lldinc.h"
#include "sd3lv2cli.h"

/* LOW LEVEL Routines for Table : LldpXdot3PortConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot3PortConfigTable
 Input       :  The Indices
                LldpPortConfigPortNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot3PortConfigTable (INT4 i4LldpPortConfigPortNum)
{
    if (LldpUtlValidateIndexInstanceLldpXdot3PortConfigTable
        (i4LldpPortConfigPortNum, gau1LldpMcastAddr) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot3PortConfigTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot3PortConfigTable
 Input       :  The Indices
                LldpPortConfigPortNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot3PortConfigTable (INT4 *pi4LldpPortConfigPortNum)
{
    if (LldpUtlGetFirstIndexLldpXdot3PortConfigTable (pi4LldpPortConfigPortNum)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpXdot3PortConfigTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot3PortConfigTable
 Input       :  The Indices
                LldpPortConfigPortNum
                nextLldpPortConfigPortNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot3PortConfigTable (INT4 i4LldpPortConfigPortNum,
                                         INT4 *pi4NextLldpPortConfigPortNum)
{
    if (LldpUtlGetNextIndexLldpXdot3PortConfigTable (i4LldpPortConfigPortNum,
                                                     pi4NextLldpPortConfigPortNum)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpXdot3PortConfigTable failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot3PortConfigTLVsTxEnable
 Input       :  The Indices
                LldpPortConfigPortNum

                The Object 
                retValLldpXdot3PortConfigTLVsTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3PortConfigTLVsTxEnable (INT4 i4LldpPortConfigPortNum,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pRetValLldpXdot3PortConfigTLVsTxEnable)
{
    if (LldpUtlGetLldpXdot3PortConfigTLVsTxEnable (i4LldpPortConfigPortNum,
                                                   pRetValLldpXdot3PortConfigTLVsTxEnable,
                                                   gau1LldpMcastAddr) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot3PortConfigTLVsTxEnable failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXdot3PortConfigTLVsTxEnable
 Input       :  The Indices
                LldpPortConfigPortNum

                The Object 
                setValLldpXdot3PortConfigTLVsTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpXdot3PortConfigTLVsTxEnable (INT4 i4LldpPortConfigPortNum,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pSetValLldpXdot3PortConfigTLVsTxEnable)
{
    if (LldpUtlSetLldpXdot3PortConfigTLVsTxEnable (i4LldpPortConfigPortNum,
                                                   pSetValLldpXdot3PortConfigTLVsTxEnable,
                                                   gau1LldpMcastAddr) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlSetLldpXdot3PortConfigTLVsTxEnable failed!!\n");
        return SNMP_FAILURE;
    }

    /*Sending Trigger for MSR */
    LLDP_MSR_NOTIFY_DOT3_PORT_CONFIG_TBL (LldpV2Xdot3PortConfigTLVsTxEnable,
                                          (sizeof
                                           (LldpV2Xdot3PortConfigTLVsTxEnable) /
                                           sizeof (UINT4)),
                                          i4LldpPortConfigPortNum,
                                          pSetValLldpXdot3PortConfigTLVsTxEnable);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXdot3PortConfigTLVsTxEnable
 Input       :  The Indices
                LldpPortConfigPortNum

                The Object 
                testValLldpXdot3PortConfigTLVsTxEnable
 Output      :  The Test Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpXdot3PortConfigTLVsTxEnable (UINT4 *pu4ErrorCode,
                                          INT4 i4LldpPortConfigPortNum,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pTestValLldpXdot3PortConfigTLVsTxEnable)
{
    if (LldpUtlTestLldpXdot3PortConfigTLVsTxEnable
        (pu4ErrorCode, i4LldpPortConfigPortNum,
         pTestValLldpXdot3PortConfigTLVsTxEnable,
         gau1LldpMcastAddr) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD LldpUtlTestLldpXdot3PortConfigTLVsTxEnable failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot3LocPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot3LocPortTable
 Input       :  The Indices
                LldpLocPortNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot3LocPortTable (INT4 i4LldpLocPortNum)
{
    if (LldpUtlValidateIndexInstanceLldpXdot3LocPortTable (i4LldpLocPortNum) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot3LocPortTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot3LocPortTable
 Input       :  The Indices
                LldpLocPortNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot3LocPortTable (INT4 *pi4LldpLocPortNum)
{
    if (LldpUtlGetFirstIndexLldpXdot3LocPortTable (pi4LldpLocPortNum) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpXdot3LocPortTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot3LocPortTable
 Input       :  The Indices
                LldpLocPortNum
                nextLldpLocPortNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot3LocPortTable (INT4 i4LldpLocPortNum,
                                      INT4 *pi4NextLldpLocPortNum)
{
    if (LldpUtlGetNextIndexLldpXdot3LocPortTable
        (i4LldpLocPortNum, pi4NextLldpLocPortNum) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpXdot3LocPortTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot3LocPortAutoNegSupported
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValLldpXdot3LocPortAutoNegSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3LocPortAutoNegSupported (INT4 i4LldpLocPortNum,
                                        INT4
                                        *pi4RetValLldpXdot3LocPortAutoNegSupported)
{
    if (LldpUtlGetLldpXdot3LocPortAutoNegSupported (i4LldpLocPortNum,
                                                    pi4RetValLldpXdot3LocPortAutoNegSupported)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot3LocPortAutoNegSupported Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXdot3LocPortAutoNegEnabled
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValLldpXdot3LocPortAutoNegEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3LocPortAutoNegEnabled (INT4 i4LldpLocPortNum,
                                      INT4
                                      *pi4RetValLldpXdot3LocPortAutoNegEnabled)
{
    if (LldpUtlGetLldpXdot3LocPortAutoNegEnabled (i4LldpLocPortNum,
                                                  pi4RetValLldpXdot3LocPortAutoNegEnabled)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot3LocPortAutoNegSupported Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXdot3LocPortAutoNegAdvertisedCap
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValLldpXdot3LocPortAutoNegAdvertisedCap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3LocPortAutoNegAdvertisedCap (INT4 i4LldpLocPortNum,
                                            tSNMP_OCTET_STRING_TYPE
                                            *
                                            pRetValLldpXdot3LocPortAutoNegAdvertisedCap)
{
    if (LldpUtlGetLldpXdot3LocPortAutoNegAdvertisedCap (i4LldpLocPortNum,
                                                        pRetValLldpXdot3LocPortAutoNegAdvertisedCap)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot3LocPortAutoNegAdvertisedCap Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXdot3LocPortOperMauType
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValLldpXdot3LocPortOperMauType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3LocPortOperMauType (INT4 i4LldpLocPortNum,
                                   INT4 *pi4RetValLldpXdot3LocPortOperMauType)
{
    if (LldpUtlGetLldpXdot3LocPortOperMauType (i4LldpLocPortNum,
                                               pi4RetValLldpXdot3LocPortOperMauType)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot3LocPortOperMauType Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot3LocPowerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot3LocPowerTable
 Input       :  The Indices
                LldpLocPortNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot3LocPowerTable (INT4 i4LldpLocPortNum)
{
    UNUSED_PARAM (i4LldpLocPortNum);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot3LocPowerTable
 Input       :  The Indices
                LldpLocPortNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot3LocPowerTable (INT4 *pi4LldpLocPortNum)
{
    UNUSED_PARAM (pi4LldpLocPortNum);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot3LocPowerTable
 Input       :  The Indices
                LldpLocPortNum
                nextLldpLocPortNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot3LocPowerTable (INT4 i4LldpLocPortNum,
                                       INT4 *pi4NextLldpLocPortNum)
{
    UNUSED_PARAM (i4LldpLocPortNum);
    UNUSED_PARAM (pi4NextLldpLocPortNum);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot3LocPowerPortClass
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValLldpXdot3LocPowerPortClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3LocPowerPortClass (INT4 i4LldpLocPortNum,
                                  INT4 *pi4RetValLldpXdot3LocPowerPortClass)
{
    UNUSED_PARAM (i4LldpLocPortNum);
    UNUSED_PARAM (pi4RetValLldpXdot3LocPowerPortClass);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetLldpXdot3LocPowerMDISupported
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValLldpXdot3LocPowerMDISupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetLldpXdot3LocPowerMDISupported
    (INT4 i4LldpLocPortNum, INT4 *pi4RetValLldpXdot3LocPowerMDISupported)
{
    UNUSED_PARAM (i4LldpLocPortNum);
    UNUSED_PARAM (pi4RetValLldpXdot3LocPowerMDISupported);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetLldpXdot3LocPowerMDIEnabled
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValLldpXdot3LocPowerMDIEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3LocPowerMDIEnabled (INT4 i4LldpLocPortNum,
                                   INT4 *pi4RetValLldpXdot3LocPowerMDIEnabled)
{
    UNUSED_PARAM (i4LldpLocPortNum);
    UNUSED_PARAM (pi4RetValLldpXdot3LocPowerMDIEnabled);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetLldpXdot3LocPowerPairControlable
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValLldpXdot3LocPowerPairControlable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetLldpXdot3LocPowerPairControlable
    (INT4 i4LldpLocPortNum, INT4 *pi4RetValLldpXdot3LocPowerPairControlable)
{
    UNUSED_PARAM (i4LldpLocPortNum);
    UNUSED_PARAM (pi4RetValLldpXdot3LocPowerPairControlable);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetLldpXdot3LocPowerPairs
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValLldpXdot3LocPowerPairs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3LocPowerPairs (INT4 i4LldpLocPortNum,
                              INT4 *pi4RetValLldpXdot3LocPowerPairs)
{
    UNUSED_PARAM (i4LldpLocPortNum);
    UNUSED_PARAM (pi4RetValLldpXdot3LocPowerPairs);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetLldpXdot3LocPowerClass
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValLldpXdot3LocPowerClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3LocPowerClass (INT4 i4LldpLocPortNum,
                              INT4 *pi4RetValLldpXdot3LocPowerClass)
{
    UNUSED_PARAM (i4LldpLocPortNum);
    UNUSED_PARAM (pi4RetValLldpXdot3LocPowerClass);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : LldpXdot3LocLinkAggTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot3LocLinkAggTable
 Input       :  The Indices
                LldpLocPortNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot3LocLinkAggTable (INT4 i4LldpLocPortNum)
{
    if (LldpUtlValidateIndexInstanceLldpXdot3LocLinkAggTable (i4LldpLocPortNum)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot3LocLinkAggTable failed !!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot3LocLinkAggTable
 Input       :  The Indices
                LldpLocPortNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot3LocLinkAggTable (INT4 *pi4LldpLocPortNum)
{
    if (LldpUtlGetFirstIndexLldpXdot3LocLinkAggTable (pi4LldpLocPortNum) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpXdot3LocLinkAggTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot3LocLinkAggTable
 Input       :  The Indices
                LldpLocPortNum
                nextLldpLocPortNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot3LocLinkAggTable (INT4 i4LldpLocPortNum,
                                         INT4 *pi4NextLldpLocPortNum)
{
    if (LldpUtlGetNextIndexLldpXdot3LocLinkAggTable (i4LldpLocPortNum,
                                                     pi4NextLldpLocPortNum) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpXdot3LocLinkAggTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot3LocLinkAggStatus
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValLldpXdot3LocLinkAggStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3LocLinkAggStatus (INT4 i4LldpLocPortNum,
                                 tSNMP_OCTET_STRING_TYPE
                                 * pRetValLldpXdot3LocLinkAggStatus)
{
    if (LldpUtlGetLldpXdot3LocLinkAggStatus
        (i4LldpLocPortNum, pRetValLldpXdot3LocLinkAggStatus) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot3LocLinkAggStatus Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXdot3LocLinkAggPortId
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValLldpXdot3LocLinkAggPortId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3LocLinkAggPortId (INT4 i4LldpLocPortNum,
                                 INT4 *pi4RetValLldpXdot3LocLinkAggPortId)
{
    if (LldpUtlGetLldpXdot3LocLinkAggPortId (i4LldpLocPortNum,
                                             pi4RetValLldpXdot3LocLinkAggPortId)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot3LocLinkAggPortId Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot3LocMaxFrameSizeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot3LocMaxFrameSizeTable
 Input       :  The Indices
                LldpLocPortNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot3LocMaxFrameSizeTable (INT4 i4LldpLocPortNum)
{
    if (LldpUtlValidateIndexInstanceLldpXdot3LocMaxFrameSizeTable
        (i4LldpLocPortNum) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot3LocMaxFrameSizeTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot3LocMaxFrameSizeTable
 Input       :  The Indices
                LldpLocPortNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot3LocMaxFrameSizeTable (INT4 *pi4LldpLocPortNum)
{
    if (LldpUtlGetFirstIndexLldpXdot3LocMaxFrameSizeTable (pi4LldpLocPortNum) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpXdot3LocMaxFrameSizeTable Failed !!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot3LocMaxFrameSizeTable
 Input       :  The Indices
                LldpLocPortNum
                nextLldpLocPortNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot3LocMaxFrameSizeTable (INT4 i4LldpLocPortNum,
                                              INT4 *pi4NextLldpLocPortNum)
{
    if (LldpUtlGetNextIndexLldpXdot3LocMaxFrameSizeTable
        (i4LldpLocPortNum, pi4NextLldpLocPortNum) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpXdot3LocMaxFrameSizeTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot3LocMaxFrameSize
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValLldpXdot3LocMaxFrameSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3LocMaxFrameSize (INT4 i4LldpLocPortNum,
                                INT4 *pi4RetValLldpXdot3LocMaxFrameSize)
{
    if (LldpUtlGetLldpXdot3LocMaxFrameSize (i4LldpLocPortNum,
                                            pi4RetValLldpXdot3LocMaxFrameSize)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot3LocMaxFrameSize Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot3RemPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot3RemPortTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot3RemPortTable (UINT4 u4LldpRemTimeMark,
                                               INT4 i4LldpRemLocalPortNum,
                                               INT4 i4LldpRemIndex)
{
    if (LldpUtlValidateIndexInstanceLldpXdot3RemPortTable
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot3RemPortTable Failed !!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot3RemPortTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot3RemPortTable (UINT4 *pu4LldpRemTimeMark,
                                       INT4 *pi4LldpRemLocalPortNum,
                                       INT4 *pi4LldpRemIndex)
{

    if (LldpUtlGetFirstIndexLldpXdot3RemPortTable
        (pu4LldpRemTimeMark, pi4LldpRemLocalPortNum,
         pi4LldpRemIndex) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpXdot3RemPortTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot3RemPortTable
 Input       :  The Indices
                LldpRemTimeMark
                nextLldpRemTimeMark
                LldpRemLocalPortNum
                nextLldpRemLocalPortNum
                LldpRemIndex
                nextLldpRemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot3RemPortTable (UINT4 u4LldpRemTimeMark,
                                      UINT4 *pu4NextLldpRemTimeMark,
                                      INT4 i4LldpRemLocalPortNum,
                                      INT4 *pi4NextLldpRemLocalPortNum,
                                      INT4 i4LldpRemIndex,
                                      INT4 *pi4NextLldpRemIndex)
{
    if (LldpUtlGetNextIndexLldpXdot3RemPortTable
        (u4LldpRemTimeMark, pu4NextLldpRemTimeMark, i4LldpRemLocalPortNum,
         pi4NextLldpRemLocalPortNum, i4LldpRemIndex, pi4NextLldpRemIndex,
         gau1LldpMcastAddr) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpXdot3RemPortTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot3RemPortAutoNegSupported
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXdot3RemPortAutoNegSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3RemPortAutoNegSupported (UINT4 u4LldpRemTimeMark,
                                        INT4 i4LldpRemLocalPortNum,
                                        INT4 i4LldpRemIndex,
                                        INT4
                                        *pi4RetValLldpXdot3RemPortAutoNegSupported)
{

    if (LldpUtlGetLldpXdot3RemPortAutoNegSupported
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         pi4RetValLldpXdot3RemPortAutoNegSupported,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot3RemPortAutoNegSupported Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXdot3RemPortAutoNegEnabled
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXdot3RemPortAutoNegEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3RemPortAutoNegEnabled (UINT4 u4LldpRemTimeMark,
                                      INT4 i4LldpRemLocalPortNum,
                                      INT4 i4LldpRemIndex,
                                      INT4
                                      *pi4RetValLldpXdot3RemPortAutoNegEnabled)
{
    if (LldpUtlGetLldpXdot3RemPortAutoNegEnabled
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         pi4RetValLldpXdot3RemPortAutoNegEnabled,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot3RemPortAutoNegEnabled Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXdot3RemPortAutoNegAdvertisedCap
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXdot3RemPortAutoNegAdvertisedCap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3RemPortAutoNegAdvertisedCap (UINT4 u4LldpRemTimeMark,
                                            INT4 i4LldpRemLocalPortNum,
                                            INT4 i4LldpRemIndex,
                                            tSNMP_OCTET_STRING_TYPE
                                            *
                                            pRetValLldpXdot3RemPortAutoNegAdvertisedCap)
{
    if (LldpUtlGetLldpXdot3RemPortAutoNegAdvertisedCap
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         pRetValLldpXdot3RemPortAutoNegAdvertisedCap,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)

    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot3RemPortAutoNegAdvertisedCap Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXdot3RemPortOperMauType
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXdot3RemPortOperMauType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3RemPortOperMauType (UINT4 u4LldpRemTimeMark,
                                   INT4 i4LldpRemLocalPortNum,
                                   INT4 i4LldpRemIndex,
                                   INT4 *pi4RetValLldpXdot3RemPortOperMauType)
{
    if (LldpUtlGetLldpXdot3RemPortOperMauType
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         pi4RetValLldpXdot3RemPortOperMauType,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot3RemPortOperMauType Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot3RemPowerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot3RemPowerTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot3RemPowerTable (UINT4 u4LldpRemTimeMark,
                                                INT4 i4LldpRemLocalPortNum,
                                                INT4 i4LldpRemIndex)
{
    if (LldpUtlValidateIndexInstanceLldpXdot3RemPowerTable
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: nmhValidateIndexInstanceLldpXdot3RemPowerTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot3RemPowerTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot3RemPowerTable (UINT4 *pu4LldpRemTimeMark,
                                        INT4 *pi4LldpRemLocalPortNum,
                                        INT4 *pi4LldpRemIndex)
{
    UNUSED_PARAM (pu4LldpRemTimeMark);
    UNUSED_PARAM (pi4LldpRemLocalPortNum);
    UNUSED_PARAM (pi4LldpRemIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot3RemPowerTable
 Input       :  The Indices
                LldpRemTimeMark
                nextLldpRemTimeMark
                LldpRemLocalPortNum
                nextLldpRemLocalPortNum
                LldpRemIndex
                nextLldpRemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot3RemPowerTable (UINT4 u4LldpRemTimeMark,
                                       UINT4 *pu4NextLldpRemTimeMark,
                                       INT4 i4LldpRemLocalPortNum,
                                       INT4 *pi4NextLldpRemLocalPortNum,
                                       INT4 i4LldpRemIndex,
                                       INT4 *pi4NextLldpRemIndex)
{

    UNUSED_PARAM (u4LldpRemTimeMark);
    UNUSED_PARAM (pu4NextLldpRemTimeMark);
    UNUSED_PARAM (i4LldpRemLocalPortNum);
    UNUSED_PARAM (pi4NextLldpRemLocalPortNum);
    UNUSED_PARAM (i4LldpRemIndex);
    UNUSED_PARAM (pi4NextLldpRemIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot3RemPowerPortClass
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXdot3RemPowerPortClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3RemPowerPortClass (UINT4 u4LldpRemTimeMark,
                                  INT4 i4LldpRemLocalPortNum,
                                  INT4 i4LldpRemIndex,
                                  INT4 *pi4RetValLldpXdot3RemPowerPortClass)
{
    UNUSED_PARAM (u4LldpRemTimeMark);
    UNUSED_PARAM (i4LldpRemLocalPortNum);
    UNUSED_PARAM (i4LldpRemIndex);
    UNUSED_PARAM (pi4RetValLldpXdot3RemPowerPortClass);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetLldpXdot3RemPowerMDISupported
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXdot3RemPowerMDISupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetLldpXdot3RemPowerMDISupported
    (UINT4 u4LldpRemTimeMark,
     INT4 i4LldpRemLocalPortNum,
     INT4 i4LldpRemIndex, INT4 *pi4RetValLldpXdot3RemPowerMDISupported)
{
    UNUSED_PARAM (u4LldpRemTimeMark);
    UNUSED_PARAM (i4LldpRemLocalPortNum);
    UNUSED_PARAM (i4LldpRemIndex);
    UNUSED_PARAM (pi4RetValLldpXdot3RemPowerMDISupported);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetLldpXdot3RemPowerMDIEnabled
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXdot3RemPowerMDIEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3RemPowerMDIEnabled (UINT4 u4LldpRemTimeMark,
                                   INT4 i4LldpRemLocalPortNum,
                                   INT4 i4LldpRemIndex,
                                   INT4 *pi4RetValLldpXdot3RemPowerMDIEnabled)
{
    UNUSED_PARAM (u4LldpRemTimeMark);
    UNUSED_PARAM (i4LldpRemLocalPortNum);
    UNUSED_PARAM (i4LldpRemIndex);
    UNUSED_PARAM (pi4RetValLldpXdot3RemPowerMDIEnabled);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetLldpXdot3RemPowerPairControlable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXdot3RemPowerPairControlable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetLldpXdot3RemPowerPairControlable
    (UINT4 u4LldpRemTimeMark,
     INT4 i4LldpRemLocalPortNum,
     INT4 i4LldpRemIndex, INT4 *pi4RetValLldpXdot3RemPowerPairControlable)
{
    UNUSED_PARAM (u4LldpRemTimeMark);
    UNUSED_PARAM (i4LldpRemLocalPortNum);
    UNUSED_PARAM (i4LldpRemIndex);
    UNUSED_PARAM (pi4RetValLldpXdot3RemPowerPairControlable);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetLldpXdot3RemPowerPairs
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXdot3RemPowerPairs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3RemPowerPairs (UINT4 u4LldpRemTimeMark,
                              INT4 i4LldpRemLocalPortNum,
                              INT4 i4LldpRemIndex,
                              INT4 *pi4RetValLldpXdot3RemPowerPairs)
{
    UNUSED_PARAM (u4LldpRemTimeMark);
    UNUSED_PARAM (i4LldpRemLocalPortNum);
    UNUSED_PARAM (i4LldpRemIndex);
    UNUSED_PARAM (pi4RetValLldpXdot3RemPowerPairs);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetLldpXdot3RemPowerClass
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXdot3RemPowerClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3RemPowerClass (UINT4 u4LldpRemTimeMark,
                              INT4 i4LldpRemLocalPortNum,
                              INT4 i4LldpRemIndex,
                              INT4 *pi4RetValLldpXdot3RemPowerClass)
{
    UNUSED_PARAM (u4LldpRemTimeMark);
    UNUSED_PARAM (i4LldpRemLocalPortNum);
    UNUSED_PARAM (i4LldpRemIndex);
    UNUSED_PARAM (pi4RetValLldpXdot3RemPowerClass);
    return SNMP_FAILURE;

}

/* LOW LEVEL Routines for Table : LldpXdot3RemLinkAggTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot3RemLinkAggTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot3RemLinkAggTable (UINT4 u4LldpRemTimeMark,
                                                  INT4 i4LldpRemLocalPortNum,
                                                  INT4 i4LldpRemIndex)
{
    if (LldpUtlValidateIndexInstanceLldpXdot3RemLinkAggTable
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot3RemLinkAggTable failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot3RemLinkAggTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot3RemLinkAggTable (UINT4 *pu4LldpRemTimeMark,
                                          INT4 *pi4LldpRemLocalPortNum,
                                          INT4 *pi4LldpRemIndex)
{
    if (LldpUtlGetFirstIndexLldpXdot3RemLinkAggTable
        (pu4LldpRemTimeMark, pi4LldpRemLocalPortNum,
         pi4LldpRemIndex) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpXdot3RemLinkAggTable Failed !!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot3RemLinkAggTable
 Input       :  The Indices
                LldpRemTimeMark
                nextLldpRemTimeMark
                LldpRemLocalPortNum
                nextLldpRemLocalPortNum
                LldpRemIndex
                nextLldpRemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot3RemLinkAggTable (UINT4 u4LldpRemTimeMark,
                                         UINT4 *pu4NextLldpRemTimeMark,
                                         INT4 i4LldpRemLocalPortNum,
                                         INT4 *pi4NextLldpRemLocalPortNum,
                                         INT4 i4LldpRemIndex,
                                         INT4 *pi4NextLldpRemIndex)
{
    if (LldpUtlGetNextIndexLldpXdot3RemLinkAggTable (u4LldpRemTimeMark,
                                                     pu4NextLldpRemTimeMark,
                                                     i4LldpRemLocalPortNum,
                                                     pi4NextLldpRemLocalPortNum,
                                                     i4LldpRemIndex,
                                                     pi4NextLldpRemIndex,
                                                     gau1LldpMcastAddr) !=
        OSIX_SUCCESS)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpXdot3RemLinkAggTable failed !!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot3RemLinkAggStatus
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXdot3RemLinkAggStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3RemLinkAggStatus (UINT4 u4LldpRemTimeMark,
                                 INT4 i4LldpRemLocalPortNum,
                                 INT4 i4LldpRemIndex,
                                 tSNMP_OCTET_STRING_TYPE
                                 * pRetValLldpXdot3RemLinkAggStatus)
{
    if (LldpUtlGetLldpXdot3RemLinkAggStatus
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         pRetValLldpXdot3RemLinkAggStatus,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD:  LldpUtlGetLldpXdot3RemLinkAggStatus failed !!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetLldpXdot3RemLinkAggPortId
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXdot3RemLinkAggPortId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3RemLinkAggPortId (UINT4 u4LldpRemTimeMark,
                                 INT4 i4LldpRemLocalPortNum,
                                 INT4 i4LldpRemIndex,
                                 INT4 *pi4RetValLldpXdot3RemLinkAggPortId)
{
    if (LldpUtlGetLldpXdot3RemLinkAggPortId
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         pi4RetValLldpXdot3RemLinkAggPortId,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot3RemLinkAggPortId failed !!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot3RemMaxFrameSizeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot3RemMaxFrameSizeTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot3RemMaxFrameSizeTable (UINT4 u4LldpRemTimeMark,
                                                       INT4
                                                       i4LldpRemLocalPortNum,
                                                       INT4 i4LldpRemIndex)
{
    if (LldpUtlValidateIndexInstanceLldpXdot3RemMaxFrameSizeTable
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot3RemMaxFrameSizeTable failed !!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot3RemMaxFrameSizeTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot3RemMaxFrameSizeTable (UINT4 *pu4LldpRemTimeMark,
                                               INT4 *pi4LldpRemLocalPortNum,
                                               INT4 *pi4LldpRemIndex)
{
    if (LldpUtlGetFirstIndexLldpXdot3RemMaxFrameSizeTable
        (pu4LldpRemTimeMark, pi4LldpRemLocalPortNum,
         pi4LldpRemIndex) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpXdot3RemMaxFrameSizeTable failed !!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot3RemMaxFrameSizeTable
 Input       :  The Indices
                LldpRemTimeMark
                nextLldpRemTimeMark
                LldpRemLocalPortNum
                nextLldpRemLocalPortNum
                LldpRemIndex
                nextLldpRemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot3RemMaxFrameSizeTable (UINT4 u4LldpRemTimeMark,
                                              UINT4 *pu4NextLldpRemTimeMark,
                                              INT4 i4LldpRemLocalPortNum,
                                              INT4 *pi4NextLldpRemLocalPortNum,
                                              INT4 i4LldpRemIndex,
                                              INT4 *pi4NextLldpRemIndex)
{
    if (LldpUtlGetNextIndexLldpXdot3RemMaxFrameSizeTable
        (u4LldpRemTimeMark, pu4NextLldpRemTimeMark, i4LldpRemLocalPortNum,
         pi4NextLldpRemLocalPortNum, i4LldpRemIndex, pi4NextLldpRemIndex,
         gau1LldpMcastAddr) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpXdot3RemMaxFrameSizeTable failed !!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot3RemMaxFrameSize
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXdot3RemMaxFrameSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot3RemMaxFrameSize (UINT4 u4LldpRemTimeMark,
                                INT4 i4LldpRemLocalPortNum,
                                INT4 i4LldpRemIndex,
                                INT4 *pi4RetValLldpXdot3RemMaxFrameSize)
{
    if (LldpUtlGetLldpXdot3RemMaxFrameSize
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         pi4RetValLldpXdot3RemMaxFrameSize,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot3RemMaxFrameSize failed !!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpXdot3PortConfigTable
 Input       :  The Indices
                LldpPortConfigPortNum
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXdot3PortConfigTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
