/*****************************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: lldmed.c,v 1.12 2017/10/11 13:42:02 siva Exp $
 *
 * Description: This File contains all the LLDP MED TLV related Parsing,
 *              Validation and remote MIB updation routines.
 *             
 *****************************************************************************/
#ifndef _LLDP_MEDTLV_C_
#define _LLDP_MEDTLV_C_
#include "lldinc.h"

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedProcOrgSpecTlv
 *
 *    DESCRIPTION      : This function processes the LLDP-MED  
 *                       Oganizationally Specific TLV in the received frame.
 *                       Decode the Information from the TLV Information 
 *                       field  and then Validate the information.
 *
 *    INPUT            : pLocPortInfo    - Pointer to the port info structure
 *                       pu1TlvInfo      - Pointer to the TLV SubType Specific
 *                                         Information
 *                       u1TlvSubType    - TLV SubType
 *                       u2TlvLength     - Length of the Information Field
 *                       pu2MedSupTlvs   - Contains the list of TLVs supported by
 *                                         the remote agent
 *
 *    OUTPUT           : pbTlvDiscardFlag - Determines whether to discard the 
 *                                          TLV or not
 *
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpMedProcOrgSpecTlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1TlvInfo,
                       UINT1 u1TlvSubType, UINT2 u2TlvLength,
                       BOOL1 * pbTlvDiscardFlag, UINT2 *pu2MedSupTlvs)
{
    INT4                i4RetVal = OSIX_FAILURE;
    UINT1               u1DeviceClass = 0;

    if ((u1TlvSubType < LLDP_MED_MIN_SUB_TYPE) ||
        (u1TlvSubType > LLDP_MED_MAX_SUB_TYPE))
    {
        if ((u2TlvLength - (LLDP_MAX_LEN_OUI + LLDP_TLV_SUBTYPE_LEN)) >=
            LLDP_MAX_LEN_ORGDEF_INFO)
        {
            LLDP_TRC (ALL_FAILURE_TRC,
                      "\tUnrecognised Org spec. TLV Length is Too Big\r\n");
            return OSIX_FAILURE;
        }

        /* Unrecognized subtype, statsTLVsUnrecognizedTotal counter shall
         * be incremented and the TLV is assumed to be validated */
        LLDP_INCR_CNTR_RX_TLVS_UNRECOGNIZED (pLocPortInfo);
        LLDP_TRC (CONTROL_PLANE_TRC, "Received LLDP-MED Unrecognized TLV!!!\n");
        return OSIX_SUCCESS;
    }

    if (((LLDP_MED_IS_TLV_RCVD_BMP_SET (pLocPortInfo, LLDP_MED_RECV_MED_CAP_TLV)
          != OSIX_TRUE) && (u1TlvSubType != LLDP_MED_CAPABILITY_SUBTYPE)) ||
        ((LLDP_MED_IS_TLV_RCVD_BMP_SET (pLocPortInfo, LLDP_MED_RECV_MED_CAP_TLV)
          == OSIX_TRUE) && (u1TlvSubType == LLDP_MED_CAPABILITY_SUBTYPE)))
    {
        /* PDU should not contain more than one MED Capability TLV and MED
         * Capability TLV should be the first TLV in LLDPDU. Hence
         * discard the TLV */
        LLDP_TRC (ALL_FAILURE_TRC, "Received"
                  " an Invalid LLDP-MED TLV, Discarding the TLV!!\n");
        *pbTlvDiscardFlag = OSIX_TRUE;
        return OSIX_SUCCESS;

    }
    if (u1TlvSubType >= LLDP_MED_HW_REVISION_SUBTYPE &&
        u1TlvSubType <= LLDP_MED_ASSET_ID_SUBTYPE)
    {
        /* check whether the Inventory TLV is supported by the remote device */
        if (!(*pu2MedSupTlvs & LLDP_MED_INVENTORY_MGMT_TLV))
        {
            LLDP_TRC (LLDP_MED_INV_MGMT_TRC, "LldpMedProcOrgSpecTlv:"
                      "Inventory TLV is not enabled in the Remote Agent supported"
                      "TLVs bitmap, So discarding the TLV\n");
            *pbTlvDiscardFlag = OSIX_TRUE;
            return OSIX_SUCCESS;
        }
    }

    switch (u1TlvSubType)
    {
        case LLDP_MED_CAPABILITY_SUBTYPE:

            /* Since Med Capability TLV is a fixed length TLV, check whether
             * the length is valid or not */
            if (u2TlvLength != LLDP_MED_CAPABILITY_TLV_INFO_LEN)
            {
                LLDP_TRC (LLDP_MED_CAPAB_TRC, "Discarding:"
                          " LLDPDU as obtained Med Capability TLV length is less than"
                          " actual length\n");
                /* Return failure as the location of the next TLV is 
                 * indeterminate */
                pLocPortInfo->bMedFrameDiscarded = LLDP_MED_TRUE;
                return OSIX_FAILURE;
            }

            /* Decoding the TLV and validating the fields in TLV */
            LLDP_LBUF_GET_2_BYTE (pu1TlvInfo, 0, *pu2MedSupTlvs);
            if ((*pu2MedSupTlvs | LLDP_MED_MAX_TLV_SUPPORTED) !=
                LLDP_MED_MAX_TLV_SUPPORTED)
            {
                LLDP_TRC (LLDP_MED_CAPAB_TRC,
                          "LldpMedProcOrgSpecTlv:"
                          "Reserved bits in Capabilities field are having non zero values."
                          "So discarding the Tlv\n");
                *pbTlvDiscardFlag = OSIX_TRUE;
                return OSIX_SUCCESS;
            }
            if (*pu2MedSupTlvs & LLDP_MED_CAPABILITY_TLV)
            {
                LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, 0, u1DeviceClass);

                if ((u1DeviceClass != LLDP_MED_CLASS_1_DEVICE) &&
                    (u1DeviceClass != LLDP_MED_CLASS_2_DEVICE) &&
                    (u1DeviceClass != LLDP_MED_CLASS_3_DEVICE))
                {
                    LLDP_TRC (LLDP_MED_CAPAB_TRC,
                              "LldpMedProcOrgSpecTlv:"
                              "Received packet from Unknown Device, So discarding the TLV\n");
                    *pbTlvDiscardFlag = OSIX_TRUE;
                    return OSIX_SUCCESS;

                }
                /* Set the bit map with the received TLV */
                LLDP_MED_SET_TLV_RCVD_BMP (pLocPortInfo,
                                           LLDP_MED_RECV_MED_CAP_TLV);
            }
            else
            {
                LLDP_TRC (LLDP_MED_CAPAB_TRC, "LldpMedProcOrgSpecTlv:"
                          "TLV is not enabled in the Remote Agent supported"
                          "TLVs bitmap, So discarding the TLV\n");
                *pbTlvDiscardFlag = OSIX_TRUE;
                return OSIX_SUCCESS;
            }
            i4RetVal = OSIX_SUCCESS;
            break;

        case LLDP_MED_NW_POLICY_SUBTYPE:

            /* Since Network Policy TLV is a fixed length TLV, check whether
             * the length is valid or not */
            if (u2TlvLength != LLDP_MED_NW_POLICY_INFO_LEN)
            {
                LLDP_TRC (LLDP_MED_NW_POL_TRC, "Discarding:"
                          "Length of Network Policy information"
                          "is less than actual length \n");
                /* Return failure as the location of the next TLV is 
                 * indeterminate */
                pLocPortInfo->bMedFrameDiscarded = LLDP_MED_TRUE;
                return OSIX_FAILURE;
            }
            /* check whether the TLV is supported by the remote device */
            if (*pu2MedSupTlvs & LLDP_MED_NETWORK_POLICY_TLV)
            {
                /* Decode the TLV and process all attributes in the TLV */
                i4RetVal = LldpMedTlvProcNwPolicyTlv (pLocPortInfo, pu1TlvInfo);
                if (i4RetVal != OSIX_SUCCESS)
                {
                    *pbTlvDiscardFlag = OSIX_TRUE;
                    return OSIX_SUCCESS;
                }
            }
            else
            {
                LLDP_TRC (LLDP_MED_NW_POL_TRC, "LldpMedProcOrgSpecTlv:"
                          "TLV is not enabled in the Remote Agent supported"
                          "TLVs bitmap, So discarding the TLV\n");
                *pbTlvDiscardFlag = OSIX_TRUE;
                return OSIX_SUCCESS;
            }

            break;

        case LLDP_MED_LOCATION_ID_SUBTYPE:

            /* check whether the TLV is supported by the remote device */
            if (*pu2MedSupTlvs & LLDP_MED_LOCATION_ID_TLV)
            {
                /* Decode the TLV and process all attributes in the TLV */
                i4RetVal = LldpMedTlvProcLocIdTlv (pLocPortInfo, pu1TlvInfo,
                                                   u2TlvLength);
                if (i4RetVal != OSIX_SUCCESS)
                {
                    if (pLocPortInfo->bMedFrameDiscarded == LLDP_MED_TRUE)
                    {
                        LLDP_TRC (LLDP_MED_LOC_ID_TRC, "Discarding:"
                                  "Length of Location Identification information"
                                  "is more than actual length \n");
                        /* Return failure as the location of the next TLV is
                         * indeterminate */
                        return OSIX_FAILURE;
                    }
                    *pbTlvDiscardFlag = OSIX_TRUE;
                    return OSIX_SUCCESS;
                }
            }
            else
            {
                LLDP_TRC (LLDP_MED_LOC_ID_TRC, "LldpMedProcOrgSpecTlv:"
                          "TLV is not enabled in the Remote Agent supported"
                          "TLVs bitmap, So discarding the TLV\n");
                *pbTlvDiscardFlag = OSIX_TRUE;
                return OSIX_SUCCESS;
            }
            break;

        case LLDP_MED_EXT_PW_MDI_SUBTYPE:

            /* Since Power-via-MDI TLV is a fixed length TLV, check whether
             * the length is valid or not */
            if (u2TlvLength != LLDP_MED_POWER_TLV_INFO_LEN)
            {
                LLDP_TRC (LLDP_MED_EX_POW_TRC, "Discarding:"
                          "Length of Extended Power-via-MDI"
                          "is not equal to actual length \n");
                /* Return failure as the location of the next TLV is
                 * indeterminate */
                pLocPortInfo->bMedFrameDiscarded = LLDP_MED_TRUE;
                return OSIX_FAILURE;
            }
            /* check whether the TLV is supported by the remote device */
            if (*pu2MedSupTlvs & LLDP_MED_PW_MDI_PD_TLV)
            {
                /* Decode the TLV and process all attributes in the TLV */
                i4RetVal = LldpMedTlvProcPowMDITlv (pLocPortInfo, pu1TlvInfo);
                if (i4RetVal != OSIX_SUCCESS)
                {
                    *pbTlvDiscardFlag = OSIX_TRUE;
                    return OSIX_SUCCESS;
                }
            }
            else
            {
                LLDP_TRC (LLDP_MED_EX_POW_TRC, "LldpMedProcOrgSpecTlv:"
                          "TLV is not enabled in the Remote Agent supported"
                          "TLVs bitmap, So discarding the TLV\n");
                *pbTlvDiscardFlag = OSIX_TRUE;
                return OSIX_SUCCESS;
            }
            /* Set the bit map with the received TLV */
            LLDP_MED_SET_TLV_RCVD_BMP (pLocPortInfo,
                                       LLDP_MED_RECV_EXT_PW_PDI_TLV);
            break;
        case LLDP_MED_HW_REVISION_SUBTYPE:

            /* TLV string information length validation */
            if (u2TlvLength > (UINT2) LLDP_MED_INVENTORY_TLV_LEN)
            {
                LLDP_TRC (LLDP_MED_INV_MGMT_TRC,
                          "Discarding Hardware Revision is too big \n");
                /* Return failure as the location of the next TLV is 
                 * indeterminate */
                pLocPortInfo->bMedFrameDiscarded = LLDP_MED_TRUE;
                return OSIX_FAILURE;
            }
            if (LLDP_MED_IS_TLV_RCVD_BMP_SET (pLocPortInfo,
                                              LLDP_MED_RECV_HW_REV_TLV) ==
                OSIX_TRUE)
            {
                /* PDU should not contain more than one Hardware TLV. Hence
                 * discard the TLV */
                LLDP_TRC (LLDP_MED_INV_MGMT_TRC, "Received"
                          " more than one Hardware Revision TLV!!\n");
                *pbTlvDiscardFlag = OSIX_TRUE;
                return OSIX_SUCCESS;

            }
            /* Set the bit map with the received TLV */
            LLDP_MED_SET_TLV_RCVD_BMP (pLocPortInfo, LLDP_MED_RECV_HW_REV_TLV);
            i4RetVal = OSIX_SUCCESS;
            break;
        case LLDP_MED_FW_REVISION_SUBTYPE:

            /* TLV information length validation */

            if (u2TlvLength > (UINT2) LLDP_MED_INVENTORY_TLV_LEN)
            {
                LLDP_TRC (LLDP_MED_INV_MGMT_TRC,
                          "Discarding"
                          "length of Firmware Revision is too big \n");
                /* Return failure as the location of the next TLV is 
                 * indeterminate */
                pLocPortInfo->bMedFrameDiscarded = LLDP_MED_TRUE;
                return OSIX_FAILURE;
            }

            if (LLDP_MED_IS_TLV_RCVD_BMP_SET (pLocPortInfo,
                                              LLDP_MED_RECV_FW_REV_TLV) ==
                OSIX_TRUE)
            {
                /* PDU should not contain more than one Firmware Revision TLV. 
                 * Hence discard the TLV */
                LLDP_TRC (LLDP_MED_INV_MGMT_TRC,
                          "Received"
                          " more than one Firmware Revision TLV!!\n");
                *pbTlvDiscardFlag = OSIX_TRUE;
                return OSIX_SUCCESS;

            }
            /* Set the bit map with the received TLV */
            LLDP_MED_SET_TLV_RCVD_BMP (pLocPortInfo, LLDP_MED_RECV_FW_REV_TLV);

            i4RetVal = OSIX_SUCCESS;
            break;
        case LLDP_MED_SW_REVISION_SUBTYPE:

            /* TLV information length validation */

            if (u2TlvLength > (UINT2) LLDP_MED_INVENTORY_TLV_LEN)
            {
                LLDP_TRC (LLDP_MED_INV_MGMT_TRC,
                          "Discarding"
                          "length of Software Revision is too big \n");
                /* Return failure as the location of the next TLV is 
                 * indeterminate */
                pLocPortInfo->bMedFrameDiscarded = LLDP_MED_TRUE;
                return OSIX_FAILURE;
            }
            if (LLDP_MED_IS_TLV_RCVD_BMP_SET (pLocPortInfo,
                                              LLDP_MED_RECV_SW_REV_TLV) ==
                OSIX_TRUE)
            {
                /* PDU should not contain more than one Software Revision TLV.
                 * Hence discard the TLV */
                LLDP_TRC (LLDP_MED_INV_MGMT_TRC, "Received"
                          " more than one Software Revision TLV!!\n");
                *pbTlvDiscardFlag = OSIX_TRUE;
                return OSIX_SUCCESS;

            }
            /* Set the bit map with the received TLV */
            LLDP_MED_SET_TLV_RCVD_BMP (pLocPortInfo, LLDP_MED_RECV_SW_REV_TLV);

            i4RetVal = OSIX_SUCCESS;
            break;
        case LLDP_MED_SERIAL_NUM_SUBTYPE:

            /* TLV information length validation */

            if (u2TlvLength > (UINT2) LLDP_MED_INVENTORY_TLV_LEN)
            {
                LLDP_TRC (LLDP_MED_INV_MGMT_TRC,
                          "Discarding:"
                          "length of Serial number Revision is too big \n");
                /* Return failure as the location of the next TLV is 
                 * indeterminate */
                pLocPortInfo->bMedFrameDiscarded = LLDP_MED_TRUE;
                return OSIX_FAILURE;
            }
            if (LLDP_MED_IS_TLV_RCVD_BMP_SET (pLocPortInfo,
                                              LLDP_MED_RECV_SER_NUM_TLV) ==
                OSIX_TRUE)
            {
                /* PDU should not contain more than one Serial Number TLV. Hence
                 * discard the TLV */
                LLDP_TRC (LLDP_MED_INV_MGMT_TRC, "Received"
                          " more than one Serial Number TLV!!\n");
                *pbTlvDiscardFlag = OSIX_TRUE;
                return OSIX_SUCCESS;

            }
            /* Set the bit map with the received TLV */
            LLDP_MED_SET_TLV_RCVD_BMP (pLocPortInfo, LLDP_MED_RECV_SER_NUM_TLV);
            i4RetVal = OSIX_SUCCESS;
            break;
        case LLDP_MED_MFG_NAME_SUBTYPE:

            /* TLV information length validation */
            if (u2TlvLength > (UINT2) LLDP_MED_INVENTORY_TLV_LEN)
            {
                LLDP_TRC (LLDP_MED_INV_MGMT_TRC,
                          "Discarding:"
                          "length of Manufacturer name is too big \n");
                /* Return failure as the location of the next TLV is 
                 * indeterminate */
                pLocPortInfo->bMedFrameDiscarded = LLDP_MED_TRUE;
                return OSIX_FAILURE;
            }

            if (LLDP_MED_IS_TLV_RCVD_BMP_SET (pLocPortInfo,
                                              LLDP_MED_RECV_MFG_NAME_TLV) ==
                OSIX_TRUE)
            {
                /* PDU should not contain more than one Manufacturer Name TLV.
                 * Hence discard the TLV */
                LLDP_TRC (LLDP_MED_INV_MGMT_TRC, "Received"
                          " more than one Manufacturer Name TLV!!\n");
                *pbTlvDiscardFlag = OSIX_TRUE;
                return OSIX_SUCCESS;

            }
            /* Set the bit map with the received TLV */
            LLDP_MED_SET_TLV_RCVD_BMP (pLocPortInfo,
                                       LLDP_MED_RECV_MFG_NAME_TLV);

            i4RetVal = OSIX_SUCCESS;
            break;
        case LLDP_MED_MODEL_NAME_SUBTYPE:

            /* TLV information length validation */
            if (u2TlvLength > (UINT2) LLDP_MED_INVENTORY_TLV_LEN)
            {
                LLDP_TRC (LLDP_MED_INV_MGMT_TRC,
                          "Discarding:" "length of Model name is too big \n");
                /* Return failure as the location of the next TLV is 
                 * indeterminate */
                pLocPortInfo->bMedFrameDiscarded = LLDP_MED_TRUE;
                return OSIX_FAILURE;
            }

            if (LLDP_MED_IS_TLV_RCVD_BMP_SET (pLocPortInfo,
                                              LLDP_MED_RECV_MODEL_NAME_TLV) ==
                OSIX_TRUE)
            {
                /* PDU should not contain more than one Model Name TLV. Hence
                 * discard the TLV */
                LLDP_TRC (LLDP_MED_INV_MGMT_TRC, "Received"
                          " more than one Model Name TLV!!\n");
                *pbTlvDiscardFlag = OSIX_TRUE;
                return OSIX_SUCCESS;
            }
            /* Set the bit map with the received TLV */
            LLDP_MED_SET_TLV_RCVD_BMP (pLocPortInfo,
                                       LLDP_MED_RECV_MODEL_NAME_TLV);
            i4RetVal = OSIX_SUCCESS;
            break;
        case LLDP_MED_ASSET_ID_SUBTYPE:

            /* TLV information length validation */
            if (u2TlvLength > (UINT2) LLDP_MED_INVENTORY_TLV_LEN)
            {
                LLDP_TRC (LLDP_MED_INV_MGMT_TRC,
                          "Discarding:" "length of Asset ID is too big \n");
                /* Return failure as the location of the next TLV is 
                 * indeterminate */
                pLocPortInfo->bMedFrameDiscarded = LLDP_MED_TRUE;
                return OSIX_FAILURE;
            }
            if (LLDP_MED_IS_TLV_RCVD_BMP_SET (pLocPortInfo,
                                              LLDP_MED_RECV_ASSET_ID_TLV) ==
                OSIX_TRUE)
            {
                /* PDU should not contain more than one Asset ID TLV. Hence
                 * discard the TLV */
                LLDP_TRC (LLDP_MED_INV_MGMT_TRC, "Received"
                          " more than one Asset ID TLV!!\n");
                *pbTlvDiscardFlag = OSIX_TRUE;
                return OSIX_SUCCESS;

            }
            /* Set the bit map with the received TLV */
            LLDP_MED_SET_TLV_RCVD_BMP (pLocPortInfo,
                                       LLDP_MED_RECV_ASSET_ID_TLV);
            i4RetVal = OSIX_SUCCESS;
            break;
            /* if the tlvsubtype doesn't match any of the above cases
             * then the TLV is considered as unrecognized TLV and the same
             * is handled above(LLDP_IS_UNRECOG_ORGDEF_TLV). Hence default 
             * case is not required */
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedTlvProcNwPolicyTlv
 *
 *    DESCRIPTION      : This function processes the Nw policy Tlv in the
 *                       received frame.
 *                       Decode the Information from the TLV Information
 *                       field  and then Validate the information.
 *
 *    INPUT            : pu1TlvInfo - Pointer to the TLV SubType Specific
 *                                    Information
 *                       pLocPortInfo - Pointer to the port info
 *                                    structure
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS /
 *                       OSIX_FAILURE - if the PDU is to be discarded
 *
 ****************************************************************************/
INT4
LldpMedTlvProcNwPolicyTlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1TlvInfo)
{

    tLldpMedRemNwPolicyInfo RemPolicy;    /* Structure variable is used to get and 
                                           validate all the fields */
    UINT4               u4PolicyInfo = 0;

    MEMSET (&RemPolicy, 0, sizeof (tLldpMedRemNwPolicyInfo));

    /* Decode the attributes from Network Policy Information field
       ------------------------------------------------------
       | Application | U | T | X | Vlan   | L2      | DSCP  |
       |   Type      |   |   |   | ID     | Priority| VALUE |
       ------------------------------------------------------
       1 Octet        3 Bits      12-bits  3-bits    6-bits  
       <------------------  4 Octets -----------------------> */

    /* Get the 4-byte value to local variable */
    LLDP_LBUF_GET_4_BYTE (pu1TlvInfo, 0, u4PolicyInfo);

    /* Get first octet for Application type value */
    RemPolicy.u1RemPolicyAppType |= (u4PolicyInfo & LLDP_MED_APP_TYPE_BIT_MASK)
        >> LLDP_MED_APP_TYPE_SHIFT_BIT;

    /* Get next 1 bit value for Unknown flag */
    RemPolicy.bRemPolicyUnKnown = LLDP_MED_GET_POLICY_BIT (u4PolicyInfo,
                                                           LLDP_MED_UNKNOWN_SHIFT_BIT);
    /* Get next 1 bit value for Tagged flag */
    RemPolicy.bRemPolicyTagged = LLDP_MED_GET_POLICY_BIT (u4PolicyInfo,
                                                          LLDP_MED_TAGGED_SHIFT_BIT);
    /* Get next 12-bits value for Vlan ID */
    RemPolicy.u2RemPolicyVlanId |= (u4PolicyInfo & LLDP_MED_VLAN_ID_BIT_MASK)
        >> LLDP_MED_VLAN_ID_SHIFT_BIT;
    /* Get next 3-bits value for Policy Priority */
    RemPolicy.u1RemPolicyPriority |= (u4PolicyInfo & LLDP_MED_PRIORITY_BIT_MASK)
        >> LLDP_MED_PRIORITY_SHIFT_BIT;
    /* Get the last 6-bits value for Dscp */
    RemPolicy.u1RemPolicyDscp |= (u4PolicyInfo & LLDP_MED_DSCP_BIT_MASK);

    if ((RemPolicy.u1RemPolicyAppType < LLDP_MED_MIN_APP_TYPE) ||
        (RemPolicy.u1RemPolicyAppType > LLDP_MED_MAX_APP_TYPE))
    {
        LLDP_TRC (LLDP_MED_NW_POL_TRC,
                  "LldpMedTlvProcNwPolicyTlv:"
                  "Policy App-Type is not in range so discarding the TLV\n");
        return OSIX_FAILURE;
    }

    switch (RemPolicy.u1RemPolicyAppType)
    {

        case LLDP_MED_VOICE_APP:

            if (LLDP_MED_IS_APP_TYPE_BMP_SET (pLocPortInfo,
                                              LLDP_MED_NW_POL_VOICE) ==
                OSIX_TRUE)
            {
                LLDP_TRC (LLDP_MED_NW_POL_TRC,
                          "LldpMedTlvProcNwPolicyTlv:"
                          "Duplicate TLV is received with application "
                          "type as voice, so discarding the TLV \n");
                return OSIX_FAILURE;
            }
            LLDP_MED_SET_APP_TYPE_BMP (pLocPortInfo, LLDP_MED_NW_POL_VOICE);
            break;

        case LLDP_MED_VOICE_SIGNALING_APP:

            if (LLDP_MED_IS_APP_TYPE_BMP_SET (pLocPortInfo,
                                              LLDP_MED_NW_POL_VOICE_SIG) ==
                OSIX_TRUE)
            {
                LLDP_TRC (LLDP_MED_NW_POL_TRC,
                          "LldpMedTlvProcNwPolicyTlv:"
                          "Duplicate TLV is received with application "
                          "type as VoiceSignaling, so discarding the TLV \n");
                return OSIX_FAILURE;
            }
            LLDP_MED_SET_APP_TYPE_BMP (pLocPortInfo, LLDP_MED_NW_POL_VOICE_SIG);
            break;

        case LLDP_MED_GUEST_VOICE_APP:

            if (LLDP_MED_IS_APP_TYPE_BMP_SET (pLocPortInfo,
                                              LLDP_MED_NW_POL_GUEST_VOICE) ==
                OSIX_TRUE)
            {
                LLDP_TRC (LLDP_MED_NW_POL_TRC,
                          "LldpMedTlvProcNwPolicyTlv:"
                          "Duplicate TLV is received with application "
                          "type as GuestVoice, so discarding the TLV \n");
                return OSIX_FAILURE;
            }
            LLDP_MED_SET_APP_TYPE_BMP (pLocPortInfo,
                                       LLDP_MED_NW_POL_GUEST_VOICE);
            break;

        case LLDP_MED_GUEST_VOICE_SIGNALING_APP:

            if (LLDP_MED_IS_APP_TYPE_BMP_SET (pLocPortInfo,
                                              LLDP_MED_NW_POL_GUEST_VOICE_SIG)
                == OSIX_TRUE)
            {
                LLDP_TRC (LLDP_MED_NW_POL_TRC,
                          "LldpMedTlvProcNwPolicyTlv:"
                          "Duplicate TLV is received with application type"
                          "as GuestVoiceSignaling, so discarding the TLV\n");
                return OSIX_FAILURE;
            }
            LLDP_MED_SET_APP_TYPE_BMP (pLocPortInfo,
                                       LLDP_MED_NW_POL_GUEST_VOICE_SIG);
            break;

        case LLDP_MED_SOFT_PHONE_VOICE_APP:

            if (LLDP_MED_IS_APP_TYPE_BMP_SET (pLocPortInfo,
                                              LLDP_MED_NW_POL_SOFT_PHN_VOICE) ==
                OSIX_TRUE)
            {
                LLDP_TRC (LLDP_MED_NW_POL_TRC,
                          "LldpMedTlvProcNwPolicyTlv:"
                          "Duplicate TLV is received with application type"
                          "as SoftPhoneVoice, so discarding the TLV\n");
                return OSIX_FAILURE;
            }
            LLDP_MED_SET_APP_TYPE_BMP (pLocPortInfo,
                                       LLDP_MED_NW_POL_SOFT_PHN_VOICE);
            break;

        case LLDP_MED_VIDEO_CONFERENCING_APP:

            if (LLDP_MED_IS_APP_TYPE_BMP_SET (pLocPortInfo,
                                              LLDP_MED_NW_POL_VIDEO_CONF) ==
                OSIX_TRUE)
            {
                LLDP_TRC (LLDP_MED_NW_POL_TRC,
                          "LldpMedTlvProcNwPolicyTlv:"
                          "Duplicate TLV is received with application type"
                          "as VideoConferencing, so discarding the TLV\n");
                return OSIX_FAILURE;
            }
            LLDP_MED_SET_APP_TYPE_BMP (pLocPortInfo,
                                       LLDP_MED_NW_POL_VIDEO_CONF);
            break;

        case LLDP_MED_STREAMING_VIDEO_APP:

            if (LLDP_MED_IS_APP_TYPE_BMP_SET (pLocPortInfo,
                                              LLDP_MED_NW_POL_STREAM_VIDEO) ==
                OSIX_TRUE)
            {
                LLDP_TRC (LLDP_MED_NW_POL_TRC,
                          "LldpMedTlvProcNwPolicyTlv:"
                          "Duplicate TLV is received with application type"
                          "as StreamingVideo, so discarding the TLV\n");
                return OSIX_FAILURE;
            }
            LLDP_MED_SET_APP_TYPE_BMP (pLocPortInfo,
                                       LLDP_MED_NW_POL_STREAM_VIDEO);
            break;

        case LLDP_MED_VIDEO_SIGNALING_APP:

            if (LLDP_MED_IS_APP_TYPE_BMP_SET (pLocPortInfo,
                                              LLDP_MED_NW_POL_VIDEO_SIG) ==
                OSIX_TRUE)
            {
                LLDP_TRC (LLDP_MED_NW_POL_TRC,
                          "LldpMedTlvProcNwPolicyTlv:"
                          "Duplicate TLV is received with application type"
                          "as VideoSignaling, so discarding the TLV\n");
                return OSIX_FAILURE;
            }
            LLDP_MED_SET_APP_TYPE_BMP (pLocPortInfo, LLDP_MED_NW_POL_VIDEO_SIG);
            break;

            /* if the Appliation type  doesn't match any of the above cases
             * then the TLV will be discarded and the same is handled above.
             * Hence default case is not required */
    }

    /* Check the range of all attributes in case of Unknown Flag is not set 
     * for the application type. If Unknown flag set ignore all the fields */

    if (RemPolicy.bRemPolicyUnKnown != LLDP_MED_UNKNOWN_FLAG)
    {
        if (((RemPolicy.bRemPolicyTagged != LLDP_MED_VLAN_UNTAGGED) &&
             ((RemPolicy.u2RemPolicyVlanId < LLDP_MIN_VLAN_ID ||
               RemPolicy.u2RemPolicyVlanId > LLDP_MAX_VLAN_ID))))
        {
            LLDP_TRC (LLDP_MED_NW_POL_TRC,
                      "LldpMedTlvProcNwPolicyTlv:"
                      "TLV received with Invalid policy attributes \n");
            return OSIX_FAILURE;

        }
    }

    return OSIX_SUCCESS;
}

/*--------------------------------------------------------------------------*/
/*                       TLV Update Routines                                */
/*--------------------------------------------------------------------------*/

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedTlvUpdtOrgSpecTlv
 *
 *    DESCRIPTION      : This function updates the LLDP-MED  
 *                       Oganizationally Specific TLV in the received frame.
 *
 *    INPUT            : pLocPortInfo  - Port Info Structure
 *                       pu1TlvInfo    - Pointer to the TLV SubType Specific 
 *                       Information 
 *                       u2TlvLength   - Length of the Information Field
 *                       u1TlvSubType  - TLV SubType
 *                        
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpMedTlvUpdtOrgSpecTlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1TlvInfo,
                          UINT2 u2TlvLength, UINT1 u1TlvSubType)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;
    if (pRemoteNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  "\tNo Remote Node found to update the LLDP MED "
                  " information.\r\n");
        return OSIX_FAILURE;
    }

    if ((u1TlvSubType < LLDP_MED_MIN_SUB_TYPE) ||
        (u1TlvSubType > LLDP_MED_MAX_SUB_TYPE))
    {
        i4RetVal = LldpTlvUpdtUnrecogOrgDefTlv (pLocPortInfo, pu1TlvInfo,
                                                u2TlvLength,
                                                gau1LldpMedOUI, u1TlvSubType);
        LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "LldpMedTlvUpdtOrgSpecTlv: Received unknown Subtype\r\n");
        return i4RetVal;
    }

#ifdef L2RED_WANTED
    if (LldpPortGetRmNodeState () == RM_ACTIVE)
#endif
    {
        /* As per LLDP-MED standard, MAC/PHY TLV is madatory to be included in 
         * LLDP-MED LLDPDU's. Check whether MAC/PHY TLV is present 
         * before processing LLDP-MED TLV in LLDPDU. This check is not added in
         * processing TLV since MAC/PHY TLV can be present before or 
         * after LLDP-MED TLVs in PDU. Hence decession can not be taken in 
         * TLV processing.*/
        if (LLDP_IS_SET_TLV_RCVD_BMP (pLocPortInfo, LLDP_TLV_RCVD_MAC_PHY_BMP)
            == OSIX_FALSE)
        {
            LLDP_TRC (LLDP_MED_CAPAB_TRC | ALL_FAILURE_TRC, "Discarding:"
                      " LLDP-MED LLDPDU is not containing MAC/PHY TLV. Hence"
                      " Discarding Med Capability TLV !!\n");
            /* Buffer pu1TlvInfo pointing to the TLV value field. Hence move the buffer pointer 
             * to starting address of OUI and MEMSET the value */
            MEMSET (pu1TlvInfo - (LLDP_MAX_LEN_OUI + 1), 0, u2TlvLength);
            LLDP_MED_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo, u1TlvSubType);
            if (OSIX_FALSE == pLocPortInfo->bstatsFramesInerrorsTotal)
            {
                LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
                pLocPortInfo->bstatsFramesInerrorsTotal = OSIX_TRUE;
            }
            return OSIX_SUCCESS;
        }

        /* If one of the Inventory TLV is received but Inventory Flag is not set 
         * for the remote agent, it means few of the Inventory set TLVs are
         * missed in PDU so discard the TLV. */
        if (((u1TlvSubType >= LLDP_MED_HW_REVISION_SUBTYPE) &&
             (u1TlvSubType <= LLDP_MED_ASSET_ID_SUBTYPE)) &&
            (LLDP_MED_IS_TLV_RCVD_BMP_SET (pLocPortInfo,
                                           LLDP_MED_RECV_INVENTORY_TLV)) !=
            OSIX_TRUE)
        {
            LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "LldpMedTlvUpdtOrgSpecTlv: Discarding Inventory TLV since"
                      "few of the TLVs in Inventory set are missed PDU\r\n");
            MEMSET (pu1TlvInfo - (LLDP_MAX_LEN_OUI + 1), 0, u2TlvLength);
            LLDP_MED_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo, u1TlvSubType);
            if (OSIX_FALSE == pLocPortInfo->bstatsFramesInerrorsTotal)
            {
                LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
                pLocPortInfo->bstatsFramesInerrorsTotal = OSIX_TRUE;
            }
            return OSIX_SUCCESS;
        }
    }

    switch (u1TlvSubType)
    {
        case LLDP_MED_CAPABILITY_SUBTYPE:
            i4RetVal = LldpMedDecodeCapTlv (pRemoteNode, pu1TlvInfo);
            if (i4RetVal == OSIX_SUCCESS)
            {
                pLocPortInfo->bMedCapable = LLDP_MED_TRUE;
            }
            break;

        case LLDP_MED_NW_POLICY_SUBTYPE:
            i4RetVal = LldpMedDecodeNwPolicyTlv (pLocPortInfo, pu1TlvInfo);
            break;

        case LLDP_MED_LOCATION_ID_SUBTYPE:
            i4RetVal = LldpMedDecodeLocIdTlv (pLocPortInfo, pu1TlvInfo,
                                              u2TlvLength);
            break;

        case LLDP_MED_EXT_PW_MDI_SUBTYPE:
            i4RetVal = LldpMedDecodePowMDITlv (pRemoteNode, pu1TlvInfo);
            break;

        case LLDP_MED_HW_REVISION_SUBTYPE:
            i4RetVal = LldpMedDecodeHwRevTlv (pRemoteNode, pu1TlvInfo,
                                              u2TlvLength);
            break;

        case LLDP_MED_FW_REVISION_SUBTYPE:
            i4RetVal = LldpMedDecodeFwRevTlv (pRemoteNode, pu1TlvInfo,
                                              u2TlvLength);
            break;

        case LLDP_MED_SW_REVISION_SUBTYPE:
            i4RetVal = LldpMedDecodeSwRevTlv (pRemoteNode, pu1TlvInfo,
                                              u2TlvLength);
            break;

        case LLDP_MED_SERIAL_NUM_SUBTYPE:
            i4RetVal = LldpMedDecodeSerialNumTlv (pRemoteNode, pu1TlvInfo,
                                                  u2TlvLength);
            break;

        case LLDP_MED_MFG_NAME_SUBTYPE:
            i4RetVal = LldpMedDecodeMfgNameTlv (pRemoteNode, pu1TlvInfo,
                                                u2TlvLength);
            break;

        case LLDP_MED_MODEL_NAME_SUBTYPE:
            i4RetVal = LldpMedDecodeModelNameTlv (pRemoteNode, pu1TlvInfo,
                                                  u2TlvLength);
            break;

        case LLDP_MED_ASSET_ID_SUBTYPE:
            i4RetVal = LldpMedDecodeAssetIdTlv (pRemoteNode, pu1TlvInfo,
                                                u2TlvLength);
            break;

            /* if the tlvsubtype doesn't match any of the above cases
             * then the TLV is considered as unrecognized TLV and the same
             * is handled above(LLDP_IS_UNRECOG_ORGDEF_TLV). Hence default 
             * case is not required */
    }

    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedDecodeCapTlv
 *
 *    DESCRIPTION      : This routine decodes the LLDP-MED capability
 *                       TLV information and update the Remote Node. 
 *                        
 *    INPUT            : pRemoteNode - The Node to be updated with Remote
 *                                     Med Capability Values. 
 *                       pu1TlvInfo  - Points to the TLV Subtype information
 *                                    field
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS 
 *
 ****************************************************************************/
PUBLIC INT4
LldpMedDecodeCapTlv (tLldpRemoteNode * pRemoteNode, UINT1 *pu1TlvInfo)
{
    UINT2               u2CapSupported = 0;

    LLDP_LBUF_GET_2_BYTE (pu1TlvInfo, 0, u2CapSupported);
    LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, 0, pRemoteNode->RemMedCapableInfo.
                          u1MedRemDeviceClass);

    pRemoteNode->RemMedCapableInfo.u2MedRemCapSupported =
        (u2CapSupported & LLDP_MED_MAX_TLV_SUPPORTED);
    pRemoteNode->RemMedCapableInfo.u2MedRemCapEnable = LLDP_MED_CAPABILITY_TLV;

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME    : LldpMedDecodeNwPolicyTlv
 *
 * DESCRIPTION      : This routine updates the LLDP-MED network
 *                    policy TLV information in the Remote Node. 
 *                     
 * INPUT            : tLldpLocPortInfo - The Node to be updated with Remote
 *                    Med Capability Values. 
 *                    pu1TlvInfo - Points to the TLV Subtype information field
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
LldpMedDecodeNwPolicyTlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1TlvInfo)
{

    tLldpMedLocNwPolicyInfo LldpMedLocPolicy;
    tLldpMedLocNwPolicyInfo *pLldpMedLocPolicy = NULL;
    tLldpMedRemNwPolicyInfo *pPolicyInfo = NULL;
    tLldpRemoteNode    *pRemoteNode = NULL;
    tLldMisConfigTrap   MisConfTrap;
    UINT4               u4PolicyInfo = 0;
    UINT2               u2PortIdLen = 0;
    UINT2               u2ChassisIdLen = 0;

    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;

    MEMSET (&MisConfTrap, 0, sizeof (tLldMisConfigTrap));
    MEMSET (&LldpMedLocPolicy, 0, sizeof (tLldpMedLocNwPolicyInfo));

    if ((pPolicyInfo =
         (tLldpMedRemNwPolicyInfo *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpMedRemNwPolicyPoolId))) == NULL)
    {

        LLDP_TRC (ALL_FAILURE_TRC | LLDP_CRITICAL_TRC | LLDP_MED_NW_POL_TRC,
                  "LldpMedDecodeNwPolicyTlv: "
                  "Mempool Allocation Failure for New Network Policy Node\r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }

    MEMSET (pPolicyInfo, 0, sizeof (tLldpMedRemNwPolicyInfo));
    /* Update the common Remote Indices */
    pPolicyInfo->u4RemLastUpdateTime = pRemoteNode->u4RemLastUpdateTime;
    pPolicyInfo->i4RemLocalPortNum = pRemoteNode->i4RemLocalPortNum;
    pPolicyInfo->i4RemIndex = pRemoteNode->i4RemIndex;
    pPolicyInfo->u4RemDestAddrTblIndex = pRemoteNode->u4DestAddrTblIndex;

    /* Decode the attributes from Network Policy Information field
       ------------------------------------------------------
       | Application | U | T | X | Vlan   | L2      | DSCP  |
       |   Type      |   |   |   | ID     | Priority| VALUE |
       ------------------------------------------------------
       1 Octet        3 Bits      12-bits  3-bits    6-bits  
       <------------------  4 Octets -----------------------> */

    /* Get the 4-byte value to local variable */
    LLDP_LBUF_GET_4_BYTE (pu1TlvInfo, 0, u4PolicyInfo);

    /* Get first octet for Application type value */
    pPolicyInfo->u1RemPolicyAppType |=
        (u4PolicyInfo & LLDP_MED_APP_TYPE_BIT_MASK) >>
        LLDP_MED_APP_TYPE_SHIFT_BIT;
    /* Get next 1 bit value for Unknown flag */
    pPolicyInfo->bRemPolicyUnKnown = LLDP_MED_GET_POLICY_BIT (u4PolicyInfo,
                                                              LLDP_MED_UNKNOWN_SHIFT_BIT);

    if (pPolicyInfo->bRemPolicyUnKnown != LLDP_MED_UNKNOWN_FLAG)
    {
        /* Get next 1 bit value for Tagged flag */
        pPolicyInfo->bRemPolicyTagged = LLDP_MED_GET_POLICY_BIT (u4PolicyInfo,
                                                                 LLDP_MED_TAGGED_SHIFT_BIT);
        if (pPolicyInfo->bRemPolicyTagged != LLDP_MED_VLAN_UNTAGGED)
        {
            /* Get next 12-bits value for Vlan ID */
            pPolicyInfo->u2RemPolicyVlanId |=
                (u4PolicyInfo & LLDP_MED_VLAN_ID_BIT_MASK) >>
                LLDP_MED_VLAN_ID_SHIFT_BIT;
            /* Get next 3-bits value for Policy Priority */
            pPolicyInfo->u1RemPolicyPriority |=
                (u4PolicyInfo & LLDP_MED_PRIORITY_BIT_MASK) >>
                LLDP_MED_PRIORITY_SHIFT_BIT;
        }
        /* Get the last 6-bits value for Dscp */
        pPolicyInfo->u1RemPolicyDscp |= (u4PolicyInfo & LLDP_MED_DSCP_BIT_MASK);
    }

    /* Insert the Node in the LldpMedRemNwPolicyInfoRBTree */
    if (RBTreeAdd (gLldpGlobalInfo.LldpMedRemNwPolicyInfoRBTree,
                   (tRBElem *) pPolicyInfo) != RB_SUCCESS)
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC | LLDP_MED_NW_POL_TRC,
                       "LLDPDU received on port %d "
                       "contains multiple network policy Tlvs with the same"
                       " application-type and port number combination\n",
                       pRemoteNode->i4RemLocalPortNum);
        MemReleaseMemBlock (gLldpGlobalInfo.LldpMedRemNwPolicyPoolId,
                            (UINT1 *) pPolicyInfo);
        return OSIX_SUCCESS;
    }

    /* Since the Network Policy TLV is included in PDU by the agent,
     * enable the TLV in Remote database */
    pRemoteNode->RemMedCapableInfo.u2MedRemCapEnable |=
        LLDP_MED_NETWORK_POLICY_TLV;

    /* Check remote device policy information is same as local 
     * policy information for the given application type.
     * If any mismatch is observed send the Policy MisMatch SNMP 
     * Notification */
    LldpMedLocPolicy.u4IfIndex = (UINT4) pLocPortInfo->i4IfIndex;
    LldpMedLocPolicy.u1LocPolicyAppType = pPolicyInfo->u1RemPolicyAppType;
    pLldpMedLocPolicy =
        (tLldpMedLocNwPolicyInfo *) RBTreeGet (gLldpGlobalInfo.
                                               LldpMedLocNwPolicyInfoRBTree,
                                               (tRBElem *) & LldpMedLocPolicy);

    /* Check for Policy Mismatch */
    if (pPolicyInfo->bRemPolicyUnKnown != LLDP_MED_UNKNOWN_FLAG)
    {
        if ((pLldpMedLocPolicy == NULL) ||
            (pLldpMedLocPolicy->bLocPolicyTagged !=
             pPolicyInfo->bRemPolicyTagged) ||
            (pLldpMedLocPolicy->u2LocPolicyVlanId !=
             pPolicyInfo->u2RemPolicyVlanId) ||
            (pLldpMedLocPolicy->u1LocPolicyPriority !=
             pPolicyInfo->u1RemPolicyPriority) ||
            (pLldpMedLocPolicy->u1LocPolicyDscp !=
             pPolicyInfo->u1RemPolicyDscp))
        {
            if ((pLocPortInfo->PortConfigTable.u1NotificationEnable ==
                 LLDP_TRUE)
                &&
                ((pLocPortInfo->PortConfigTable.u1FsConfigNotificationType ==
                  LLDP_MIS_CFG_NOTIFICATION)
                 || (pLocPortInfo->PortConfigTable.u1FsConfigNotificationType ==
                     LLDP_REMOTE_CHG_MIS_CFG_NOTIFICATION)))
            {
                /* Get the length of Chassis Id */
                LldpUtilGetChassisIdLen (pRemoteNode->i4RemChassisIdSubtype,
                                         pRemoteNode->au1RemChassisId,
                                         &u2ChassisIdLen);

                /* Get the length of port Id */
                LldpUtilGetPortIdLen (pRemoteNode->i4RemPortIdSubtype,
                                      pRemoteNode->au1RemPortId, &u2PortIdLen);

                MisConfTrap.u4RemTimeMark = pRemoteNode->u4RemLastUpdateTime;
                MisConfTrap.i4RemIndex = pRemoteNode->i4RemIndex;
                MisConfTrap.i4RemLocalPortNum = pRemoteNode->i4RemLocalPortNum;
                MisConfTrap.i4RemChassisIdSubtype =
                    pRemoteNode->i4RemChassisIdSubtype;
                MisConfTrap.i4RemPortIdSubtype =
                    pRemoteNode->i4RemPortIdSubtype;
                MEMCPY (&MisConfTrap.au1RemChassisId[0],
                        pRemoteNode->au1RemChassisId,
                        MEM_MAX_BYTES (u2ChassisIdLen, LLDP_MAX_LEN_CHASSISID));
                MEMCPY (&MisConfTrap.au1PortId[0], pRemoteNode->au1RemPortId,
                        MEM_MAX_BYTES (u2PortIdLen, LLDP_MAX_LEN_PORTID));
                MisConfTrap.MisAppType = pPolicyInfo->u1RemPolicyAppType;

                /* Call Notification Function */
                LldpSendNotification ((VOID *) &MisConfTrap,
                                      LLDP_MED_POLICY_MISMATCH);
            }
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedDecodeHwRevTlv
 *
 *    DESCRIPTION      : This routine updates the LLDP-MED hardware
 *                       Revision TLV information in the Remote Node. 
 *                        
 *    INPUT            : pRemoteNode - The Node to be updated with Remote
 *                                     Hardware Revision Value. 
 *                       pu1TlvInfo  - Points to the TLV specific information
 *                       u2TlvLength - Length of the Information Field
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpMedDecodeHwRevTlv (tLldpRemoteNode * pRemoteNode,
                       UINT1 *pu1TlvInfo, UINT2 u2TlvLength)
{

    LLDP_LBUF_GET_STRING (pu1TlvInfo,
                          &(pRemoteNode->RemInventoryInfo.
                            au1MedRemHardwareRev[0]), 0,
                          MEM_MAX_BYTES ((u2TlvLength - LLDP_TLV_SUBTYPE_LEN -
                                          LLDP_MAX_LEN_OUI),
                                         LLDP_MED_HW_REV_LEN));

    pRemoteNode->RemMedCapableInfo.u2MedRemCapEnable |=
        LLDP_MED_INVENTORY_MGMT_TLV;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedDecodeSwRevTlv
 *
 *    DESCRIPTION      : This routine updates the LLDP-MED software
 *                       Revision TLV information in the Remote Node. 
 *                        
 *    INPUT            : pRemoteNode - The Node to be updated with Remote
 *                                     Software Revision Value. 
 *                       pu1TlvInfo  - Points to the TLV specific information
 *                       u2TlvLength - Length of the Information Field
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpMedDecodeSwRevTlv (tLldpRemoteNode * pRemoteNode,
                       UINT1 *pu1TlvInfo, UINT2 u2TlvLength)
{

    LLDP_LBUF_GET_STRING (pu1TlvInfo,
                          &(pRemoteNode->RemInventoryInfo.
                            au1MedRemSoftwareRev[0]), 0,
                          MEM_MAX_BYTES ((u2TlvLength - LLDP_TLV_SUBTYPE_LEN -
                                          LLDP_MAX_LEN_OUI),
                                         LLDP_MED_SW_REV_LEN));

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedDecodeFwRevTlv
 *
 *    DESCRIPTION      : This routine updates the LLDP-MED Firmware
 *                       Revision TLV information in the Remote Node. 
 *                        
 *    INPUT            : pRemoteNode - The Node to be updated with Remote
 *                                     Firmware Revision Value. 
 *                       pu1TlvInfo  - Points to the TLV specific information
 *                       u2TlvLength - Length of the Information Field
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpMedDecodeFwRevTlv (tLldpRemoteNode * pRemoteNode,
                       UINT1 *pu1TlvInfo, UINT2 u2TlvLength)
{

    LLDP_LBUF_GET_STRING (pu1TlvInfo,
                          &(pRemoteNode->RemInventoryInfo.
                            au1MedRemFirmwareRev[0]), 0,
                          MEM_MAX_BYTES ((u2TlvLength - LLDP_TLV_SUBTYPE_LEN -
                                          LLDP_MAX_LEN_OUI),
                                         LLDP_MED_FW_REV_LEN));

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedDecodeSerialNumTlv
 *
 *    DESCRIPTION      : This routine updates the LLDP-MED Serial Number
 *                       TLV information in the Remote Node. 
 *                        
 *    INPUT            : pRemoteNode - The Node to be updated with Remote
 *                                     Serial Number Value. 
 *                       pu1TlvInfo  - Points to the TLV specific information
 *                       u2TlvLength - Length of the Information Field
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpMedDecodeSerialNumTlv (tLldpRemoteNode * pRemoteNode,
                           UINT1 *pu1TlvInfo, UINT2 u2TlvLength)
{

    LLDP_LBUF_GET_STRING (pu1TlvInfo,
                          &(pRemoteNode->RemInventoryInfo.
                            au1MedRemSerialNum[0]), 0,
                          MEM_MAX_BYTES ((u2TlvLength - LLDP_TLV_SUBTYPE_LEN -
                                          LLDP_MAX_LEN_OUI),
                                         LLDP_MED_SER_NUM_LEN));

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedDecodeMfgNameTlv
 *
 *    DESCRIPTION      : This routine updates the LLDP-MED Manufacturer Name
 *                       TLV information in the Remote Node. 
 *                        
 *    INPUT            : pRemoteNode - The Node to be updated with Remote
 *                                     Manufacturer Name Value. 
 *                       pu1TlvInfo  - Points to the TLV specific information
 *                       u2TlvLength - Length of the Information Field
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpMedDecodeMfgNameTlv (tLldpRemoteNode * pRemoteNode,
                         UINT1 *pu1TlvInfo, UINT2 u2TlvLength)
{

    LLDP_LBUF_GET_STRING (pu1TlvInfo,
                          &(pRemoteNode->RemInventoryInfo.au1MedRemMfgName[0]),
                          0,
                          MEM_MAX_BYTES ((u2TlvLength - LLDP_TLV_SUBTYPE_LEN -
                                          LLDP_MAX_LEN_OUI),
                                         LLDP_MED_MFG_NAME_LEN));

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedDecodeModelNameTlv
 *
 *    DESCRIPTION      : This routine updates the LLDP-MED Model Name
 *                       TLV information in the Remote Node. 
 *                        
 *    INPUT            : pRemoteNode - The Node to be updated with Remote
 *                                     Model Name Value. 
 *                       pu1TlvInfo  - Points to the TLV specific information
 *                       u2TlvLength - Length of the Information Field
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpMedDecodeModelNameTlv (tLldpRemoteNode * pRemoteNode,
                           UINT1 *pu1TlvInfo, UINT2 u2TlvLength)
{

    LLDP_LBUF_GET_STRING (pu1TlvInfo,
                          &(pRemoteNode->RemInventoryInfo.
                            au1MedRemModelName[0]), 0,
                          MEM_MAX_BYTES ((u2TlvLength - LLDP_TLV_SUBTYPE_LEN -
                                          LLDP_MAX_LEN_OUI),
                                         LLDP_MED_MODEL_NAME_LEN));

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedDecodeAssetIdTlv
 *
 *    DESCRIPTION      : This routine updates the LLDP-MED Asset ID
 *                       TLV information in the Remote Node. 
 *                        
 *    INPUT            : pRemoteNode - The Node to be updated with Remote
 *                       Asset ID Value. 
 *                       pu1TlvInfo  - Points to the TLV specific information
 *                       u2TlvLength - Length of the Information Field
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpMedDecodeAssetIdTlv (tLldpRemoteNode * pRemoteNode,
                         UINT1 *pu1TlvInfo, UINT2 u2TlvLength)
{

    LLDP_LBUF_GET_STRING (pu1TlvInfo,
                          &(pRemoteNode->RemInventoryInfo.au1MedRemAssetId[0]),
                          0,
                          MEM_MAX_BYTES ((u2TlvLength - LLDP_TLV_SUBTYPE_LEN -
                                          LLDP_MAX_LEN_OUI),
                                         LLDP_MED_ASSET_ID_LEN));

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedSendNotification
 *
 *    DESCRIPTION      : This function calls the LLDP notifications
 *
 *    INPUT            : pRemoteNode  - pointer to remote node 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/

PUBLIC VOID
LldpMedSendNotification (tLldpRemoteNode * pRemoteNode)
{
    tLldMisConfigTrap   MisConfTrap;
    UINT2               u2ChassisIdLen = 0;

    MEMSET (&MisConfTrap, 0, sizeof (tLldMisConfigTrap));

    gLldpGlobalInfo.bSendTopChgNotif = OSIX_TRUE;
    /* Get the length of Chassis Id */
    LldpUtilGetChassisIdLen (pRemoteNode->i4RemChassisIdSubtype,
                             pRemoteNode->au1RemChassisId, &u2ChassisIdLen);

    MisConfTrap.u4RemTimeMark = pRemoteNode->u4RemLastUpdateTime;
    MisConfTrap.i4RemIndex = pRemoteNode->i4RemIndex;
    MisConfTrap.i4RemLocalPortNum = pRemoteNode->i4RemLocalPortNum;
    MisConfTrap.i4RemChassisIdSubtype = pRemoteNode->i4RemChassisIdSubtype;
    MEMCPY (&MisConfTrap.au1RemChassisId[0],
            pRemoteNode->au1RemChassisId,
            MEM_MAX_BYTES (u2ChassisIdLen, LLDP_MAX_LEN_CHASSISID));
    MisConfTrap.MisDeviceClass =
        pRemoteNode->RemMedCapableInfo.u1MedRemDeviceClass;

    /* Call Notification Function */
    LldpSendNotification ((VOID *) &MisConfTrap, LLDP_REM_TABLE_CHG);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedResetMedCapableFlag
 *
 *    DESCRIPTION      : This routine will reset the Med Capable flag for
 *                       particular port or destination mac
 *
 *    INPUT            : pLocPortInfo - Local port info of the particular
 *                       destination mac and port combination
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PUBLIC VOID
LldpMedResetMedCapableFlag (tLldpLocPortInfo * pLocPortInfo)
{
    tLldpRemoteNode    *pCurrRemoteNode = NULL;
    tLldpRemoteNode    *pNextRemoteNode = NULL;
    BOOL1               bMedFound = LLDP_MED_FALSE;

    /* LLDP-MED packets should be trasmitted till last LLDP-MED neighbor is
     * expired on the port.
     * If the Mac address is multicast address check for all neighbors received
     * on this port number before resetting LLDP-MED capability flag.*/
    if ((pCurrRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_CRITICAL_TRC,
                  "\tMemory Allocation Failure for New pCurrRemoteNode \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return;
    }
    MEMSET (pCurrRemoteNode, 0, sizeof (tLldpRemoteNode));

    pCurrRemoteNode->i4RemLocalPortNum = pLocPortInfo->i4IfIndex;
    pNextRemoteNode = (tLldpRemoteNode *) RBTreeGetNext
        (gLldpGlobalInfo.RemSysDataRBTree, (tRBElem *) pCurrRemoteNode, NULL);

    if (pNextRemoteNode == NULL)
    {
        pLocPortInfo->bMedCapable = LLDP_MED_FALSE;
    }
    else
    {
        while ((pNextRemoteNode != NULL) &&
               (pNextRemoteNode->i4RemLocalPortNum == pLocPortInfo->i4IfIndex))
        {
            if (pNextRemoteNode->RemMedCapableInfo.u1MedRemDeviceClass != 0)
            {
                bMedFound = LLDP_MED_TRUE;
                break;
            }
            pCurrRemoteNode->u4RemLastUpdateTime =
                pNextRemoteNode->u4RemLastUpdateTime;
            pCurrRemoteNode->i4RemLocalPortNum =
                pNextRemoteNode->i4RemLocalPortNum;
            pCurrRemoteNode->i4RemIndex = pNextRemoteNode->i4RemIndex;
            pCurrRemoteNode->u4DestAddrTblIndex =
                pNextRemoteNode->u4DestAddrTblIndex;

            pNextRemoteNode = (tLldpRemoteNode *) RBTreeGetNext
                (gLldpGlobalInfo.RemSysDataRBTree, (tRBElem *) pCurrRemoteNode,
                 NULL);
        }
        /* If no LLDP-MED neighbor found, reset the MedCapable flag to stop 
         * transmitting LLDP-MED PDUs */
        if (bMedFound == LLDP_MED_FALSE)
        {
            pLocPortInfo->bMedCapable = LLDP_MED_FALSE;
        }
    }
    if (pCurrRemoteNode != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                            (UINT1 *) pCurrRemoteNode);
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedTriggerLocTlvChng
 *
 *    DESCRIPTION      : This routine will check for Class III remote device in
 *                       neighbors on port and reconstruct the Buffer to remove 
 *                       the LocationIdentification TLV for transmission if there
 *                       is no Class III Device.
 *                       
 *    INPUT            : pLocPortInfo - Local port info of the particular
 *                       destination mac and port combination
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PUBLIC VOID
LldpMedTriggerLocTlvChng (tLldpLocPortInfo * pLocPortInfo)
{
    tLldpRemoteNode    *pCurrRemoteNode = NULL;
    tLldpRemoteNode    *pNextRemoteNode = NULL;
    BOOL1               bLocIdTlv = LLDP_MED_FALSE;

    if ((pCurrRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_CRITICAL_TRC |
                  LLDP_MED_LOC_ID_TRC,
                  "\tMemory Allocation Failure for New pCurrRemoteNode \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return;
    }
    MEMSET (pCurrRemoteNode, 0, sizeof (tLldpRemoteNode));

    pCurrRemoteNode->i4RemLocalPortNum = pLocPortInfo->i4IfIndex;
    pNextRemoteNode = (tLldpRemoteNode *) RBTreeGetNext
        (gLldpGlobalInfo.RemSysDataRBTree, (tRBElem *) pCurrRemoteNode, NULL);

    while ((pNextRemoteNode != NULL) &&
           (pNextRemoteNode->i4RemLocalPortNum == pLocPortInfo->i4IfIndex))
    {
        /* Check for any remote device is Class III device
           If so there is no need to reconstruct the buffer */
        if (pNextRemoteNode->RemMedCapableInfo.u1MedRemDeviceClass
            == LLDP_MED_CLASS_3_DEVICE)
        {
            bLocIdTlv = LLDP_MED_TRUE;
            break;
        }
        pCurrRemoteNode->u4RemLastUpdateTime =
            pNextRemoteNode->u4RemLastUpdateTime;
        pCurrRemoteNode->i4RemLocalPortNum = pNextRemoteNode->i4RemLocalPortNum;
        pCurrRemoteNode->i4RemIndex = pNextRemoteNode->i4RemIndex;
        pCurrRemoteNode->u4DestAddrTblIndex =
            pNextRemoteNode->u4DestAddrTblIndex;

        pNextRemoteNode = (tLldpRemoteNode *) RBTreeGetNext
            (gLldpGlobalInfo.RemSysDataRBTree, (tRBElem *) pCurrRemoteNode,
             NULL);
    }
    if (pCurrRemoteNode != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                            (UINT1 *) pCurrRemoteNode);
    }

    if (bLocIdTlv == LLDP_MED_FALSE)
    {
        if (LldpTxUtlHandleLocPortInfoChg (pLocPortInfo) != OSIX_SUCCESS)
        {
            LLDP_TRC (CONTROL_PLANE_TRC | LLDP_MED_LOC_ID_TRC,
                      "LldpMedTriggerLocTlvChng: LldpTxUtlHandleLocPortInfoChg"
                      "failed\r\n");
        }
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedTlvProcLocIdTlv
 *
 *    DESCRIPTION      : This function processes the Location Identification 
 *                       Tlv in the received frame.
 *                       Decode the Information from the TLV Information
 *                       field  and then Validate the information.
 *
 *    INPUT            : pu1TlvInfo   - Pointer to the TLV SubType Specific
 *                                      Information
 *                       pLocPortInfo - Pointer to the port info
 *                                      structure
 *                       u2TlvLength  - Length of the Information Field
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS 
 *                       OSIX_FAILURE - if the PDU is to be discarded
 *
 ****************************************************************************/
INT4
LldpMedTlvProcLocIdTlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1TlvInfo,
                        UINT2 u2TlvLength)
{

    tLldpMedRemLocationInfo RemLocIdInfo;    /* Structure variable is used to get and
                                               validate all the fields */
    UINT1               u1LocData = 0;
    UINT1               au1LocInfo[5];

    MEMSET (&au1LocInfo, 0, 5);

    MEMSET (&RemLocIdInfo, 0, sizeof (tLldpMedRemLocationInfo));

    /* Decode the attributes from Location Identification Information field
       --------------------------------------
       | Location Data Format | Location Id |    
       --------------------------------------
       1 Octet                 Variable size
     */
    /* Get 1st Octet for Location Data format */
    LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, 0, u1LocData);
    RemLocIdInfo.u1RemLocationSubType = u1LocData;

    switch (RemLocIdInfo.u1RemLocationSubType)
    {
        case LLDP_MED_COORDINATE:

            /* Check for Duplicate Tlv */
            if (LLDP_MED_IS_TLV_RCVD_BMP_SET (pLocPortInfo,
                                              LLDP_MED_RECV_COORDINATE_LOC_TLV)
                == OSIX_TRUE)
            {
                LLDP_TRC (LLDP_MED_LOC_ID_TRC,
                          "LldpMedTlvProcLocIdTlv: Duplicate TLV is received with same"
                          "Location Subtype\r\n");
                return OSIX_FAILURE;
            }

            /* Check the length of TLV */
            if (u2TlvLength != LLDP_MED_COORDINATE_TLV_LENGTH)
            {
                LLDP_TRC (LLDP_MED_LOC_ID_TRC,
                          "LldpMedTlvProcLocIdTlv: Coordinate Location TLV length is "
                          "not in range");
                pLocPortInfo->bMedFrameDiscarded = LLDP_MED_TRUE;
                return OSIX_FAILURE;
            }

            /* Get Location Identification Value */
            LLDP_LBUF_GET_STRING (pu1TlvInfo,
                                  &(RemLocIdInfo.au1RemLocationID[0]), 0,
                                  u2TlvLength - (LLDP_TLV_SUBTYPE_LEN +
                                                 LLDP_MAX_LEN_OUI +
                                                 LLDP_MED_LOC_DATA_FORMAT_LEN));

            /* Check for Co-ordinate Location Identification Length */
            if (STRLEN (RemLocIdInfo.au1RemLocationID) !=
                LLDP_MED_MAX_COORDINATE_LOC_LENGTH)
            {
                LLDP_TRC (LLDP_MED_LOC_ID_TRC,
                          "LldpMedTlvProcLocIdTlv: Co-ordinate Location Id"
                          "is more than the maximum length\r\n");
                pLocPortInfo->bMedFrameDiscarded = LLDP_MED_TRUE;
                return OSIX_FAILURE;
            }

            LLDP_MED_SET_TLV_RCVD_BMP (pLocPortInfo,
                                       LLDP_MED_RECV_COORDINATE_LOC_TLV);

            break;

        case LLDP_MED_CIVIC:

            /* Check for Duplicate Tlv */
            if (LLDP_MED_IS_TLV_RCVD_BMP_SET (pLocPortInfo,
                                              LLDP_MED_RECV_CIVIC_LOC_TLV) ==
                OSIX_TRUE)
            {
                LLDP_TRC (LLDP_MED_LOC_ID_TRC,
                          "LldpMedTlvProcLocIdTlv: Duplicate TLV is received with same"
                          "Location Subtype\r\n");
                return OSIX_FAILURE;
            }

            /* Check the length of TLV */
            if ((u2TlvLength < LLDP_MED_MIN_CIVIC_TLV_LENGTH) ||
                (u2TlvLength > LLDP_MED_LOCATION_ID_TLV_LEN))
            {
                LLDP_TRC (LLDP_MED_LOC_ID_TRC,
                          "LldpMedTlvProcLocIdTlv: Civic Location TLV length is "
                          "not in range");
                pLocPortInfo->bMedFrameDiscarded = LLDP_MED_TRUE;
                return OSIX_FAILURE;
            }

            /* Get Location Identification Value */
            LLDP_LBUF_GET_STRING (pu1TlvInfo,
                                  &(RemLocIdInfo.au1RemLocationID[0]), 0,
                                  u2TlvLength - (LLDP_TLV_SUBTYPE_LEN +
                                                 LLDP_MAX_LEN_OUI +
                                                 LLDP_MED_LOC_DATA_FORMAT_LEN));

            /* Check for Civic Location Identification Length */
            if (STRLEN (RemLocIdInfo.au1RemLocationID) >
                LLDP_MED_MAX_CIVIC_LOC_LENGTH)
            {
                LLDP_TRC (LLDP_MED_LOC_ID_TRC,
                          "LldpMedTlvProcLocIdTlv: Civic Location Id"
                          "is more than the maximum length\r\n");
                pLocPortInfo->bMedFrameDiscarded = LLDP_MED_TRUE;
                return OSIX_FAILURE;
            }

            LLDP_MED_SET_TLV_RCVD_BMP (pLocPortInfo,
                                       LLDP_MED_RECV_CIVIC_LOC_TLV);
            break;

        case LLDP_MED_ELIN:

            /* Check for Duplicate Tlv */
            if (LLDP_MED_IS_TLV_RCVD_BMP_SET (pLocPortInfo,
                                              LLDP_MED_RECV_ELIN_LOC_TLV) ==
                OSIX_TRUE)
            {
                LLDP_TRC (LLDP_MED_LOC_ID_TRC,
                          "LldpMedTlvProcLocIdTlv: Duplicate TLV is received with same"
                          "Location Subtype\r\n");
                return OSIX_FAILURE;
            }

            if ((u2TlvLength < LLDP_MED_MIN_ELIN_TLV_LENGTH) ||
                (u2TlvLength > LLDP_MED_MAX_ELIN_TLV_LENGTH))
            {
                LLDP_TRC (LLDP_MED_LOC_ID_TRC,
                          "LldpMedTlvProcLocIdTlv: ELIN Location TLV length is "
                          "not in range");
                pLocPortInfo->bMedFrameDiscarded = LLDP_MED_TRUE;
                return OSIX_FAILURE;
            }

            /* Get Location Identification Value */
            LLDP_LBUF_GET_STRING (pu1TlvInfo,
                                  &(RemLocIdInfo.au1RemLocationID[0]), 0,
                                  u2TlvLength - (LLDP_TLV_SUBTYPE_LEN +
                                                 LLDP_MAX_LEN_OUI +
                                                 LLDP_MED_LOC_DATA_FORMAT_LEN));

            /* Check for ELIN Id range */
            if ((STRLEN (RemLocIdInfo.au1RemLocationID) <
                 LLDP_MED_MIN_ELIN_LOC_LENGTH) ||
                (STRLEN (RemLocIdInfo.au1RemLocationID) >
                 LLDP_MED_MAX_ELIN_LOC_LENGTH))
            {
                LLDP_TRC (LLDP_MED_LOC_ID_TRC,
                          "LldpMedTlvProcLocIdTlv: Elin Location Id"
                          "length is not in range\r\n");
                pLocPortInfo->bMedFrameDiscarded = LLDP_MED_TRUE;
                return OSIX_FAILURE;
            }

            LLDP_MED_SET_TLV_RCVD_BMP (pLocPortInfo,
                                       LLDP_MED_RECV_ELIN_LOC_TLV);
            break;

        default:
            return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedTlvProcPowMDITlv
 *
 *    DESCRIPTION      : This function processes the Extended Power-via-MDI
 *                       Tlv in the received frame.
 *                       Decode the Information from the TLV Information
 *                       field  and then Validate the information.
 *
 *    INPUT            : pu1TlvInfo - Pointer to the TLV SubType Specific
 *                                    Information
 *                       pLocPortInfo - Pointer to the port info
 *                                    structure
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS 
 *                       OSIX_FAILURE - if the PDU is to be discarded
 *
 ****************************************************************************/
INT4
LldpMedTlvProcPowMDITlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1TlvInfo)
{
    tLldpMedRemPowerMDIInfo RemPowInfo;    /* Structure variable is used to get and
                                           validate all the fields */
    UINT1               u1RemPowInfo = 0;

    MEMSET (&RemPowInfo, 0, sizeof (tLldpMedRemPowerMDIInfo));

    /* Decode the attributes from Extended Power-via-MDI field 
       -------------------------------------
       | Power | Power  | Power    | Power |
       | Type  | Source | Priority | Value |
       -------------------------------------
       2 bits   2 bits   4 bits    2 Octets
       <------------3 Octets---------------> */

    if (LLDP_MED_IS_TLV_RCVD_BMP_SET
        (pLocPortInfo, LLDP_MED_RECV_EXT_PW_PDI_TLV) == OSIX_TRUE)
    {
        LLDP_TRC (LLDP_MED_EX_POW_TRC,
                  "LldpMedTlvProcPowMDITlv: Duplicate Tlv is received with same Extended"
                  "Power-Via-MDI Tlv\r\n");
        return OSIX_FAILURE;
    }

    /* Get first octet for Power Info */
    LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, 0, u1RemPowInfo);

    /* Get 2 bits for Power type */
    RemPowInfo.u1PoEDeviceType |= (u1RemPowInfo & LLDP_MED_POWER_TYPE_BIT_MASK)
        >> LLDP_MED_POWER_TYPE_SHIFT_BIT;

    /* Get 2 bits for Power Source */
    RemPowInfo.u1PowerSource |= (u1RemPowInfo & LLDP_MED_POWER_SRC_BIT_MASK)
        >> LLDP_MED_POWER_SRC_SHIFT_BIT;

    /* Get 4 bits for Power priority */
    RemPowInfo.u1PowerPriority |= (u1RemPowInfo & LLDP_MED_POWER_PRI_BIT_MASK);

    /* Get 2 Octets for Power Value */
    LLDP_LBUF_GET_2_BYTE (pu1TlvInfo, 0, RemPowInfo.u2PowerValue);

    /* Check for range of Power type */
    if (RemPowInfo.u1PoEDeviceType != LLDP_MED_PD_DEVICE)
    {
        LLDP_TRC (LLDP_MED_EX_POW_TRC,
                  "LldpMedTlvProcPowMDITlv: Received value of Power Device type"
                  "is not valid\r\n");
        return OSIX_FAILURE;
    }

    /* Check for range of Power source */
    if ((RemPowInfo.u1PowerSource != LLDP_MED_PD_UNKNOWN) &&
        (RemPowInfo.u1PowerSource != LLDP_MED_PD_PSE) &&
        (RemPowInfo.u1PowerSource != LLDP_MED_PD_LOCAL) &&
        (RemPowInfo.u1PowerSource != LLDP_MED_PD_PSE_LOCAL))
    {
        LLDP_TRC (LLDP_MED_EX_POW_TRC,
                  "LldpMedTlvProcPowMDITlv: Received value of Power Source"
                  "is not valid\r\n");
        return OSIX_FAILURE;
    }

    /* Check for range of Power Priority */
    if (RemPowInfo.u1PowerPriority > (UINT1) LLDP_MED_MAX_POW_PRI)
    {
        LLDP_TRC (LLDP_MED_EX_POW_TRC,
                  "LldpMedTlvProcPowMDITlv: Received Power priority value"
                  "is not in a valid range\r\n");
        return OSIX_FAILURE;
    }

    /* Check for range of Power Value */
    if (RemPowInfo.u2PowerValue > LLDP_MED_MAX_POWER_VAL)
    {
        LLDP_TRC (LLDP_MED_EX_POW_TRC,
                  "LldpMedTlvProcPowMDITlv: Received Power Value is"
                  "not in range\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/****************************************************************************
 * FUNCTION NAME    : LldpMedDecodeLocIdTlv
 *
 * DESCRIPTION      : This routine updates the LLDP-MED Location
 *                    Identification TLV information in the Remote Node.
 *
 * INPUT            : tLldpLocPortInfo - The Node to be updated with Remote
 *                                       Med Capability Values.
 *                    pu1TlvInfo  - Points to the TLV Subtype information field
 *                    u2TlvLength - Length of the Information Field
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
LldpMedDecodeLocIdTlv (tLldpLocPortInfo * pLocPortInfo,
                       UINT1 *pu1TlvInfo, UINT2 u2TlvLength)
{
    tLldpMedRemLocationInfo *pRemLocIdInfo = NULL;
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT1               u1LocData = 0;

    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;

    /* Check whether the Remote Node is Class III Device.
     * If not, discard the TLV */
    if (pRemoteNode->RemMedCapableInfo.u1MedRemDeviceClass !=
        LLDP_MED_CLASS_3_DEVICE)
    {
        LLDP_TRC (LLDP_MED_LOC_ID_TRC | ALL_FAILURE_TRC,
                  "LldpMedDecodeLocIdTlv: "
                  "Discarding the Location Identification TLV for"
                  "End points other than Class III \r\n");
        LLDP_MED_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo,
                                              LLDP_MED_LOCATION_ID_SUBTYPE);
        return OSIX_SUCCESS;
    }

    if ((pRemLocIdInfo = (tLldpMedRemLocationInfo *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpMedRemLocationPoolId))) == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_CRITICAL_TRC | LLDP_MED_LOC_ID_TRC,
                  "LldpMedDecodeLocIdTlv: Mempool Allocation Failure for New"
                  "Location Identification Node\r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }

    MEMSET (pRemLocIdInfo, 0, sizeof (tLldpMedRemLocationInfo));

    /* Update the common Remote Indices */
    pRemLocIdInfo->u4RemLastUpdateTime = pRemoteNode->u4RemLastUpdateTime;
    pRemLocIdInfo->i4RemLocalPortNum = pRemoteNode->i4RemLocalPortNum;
    pRemLocIdInfo->i4RemIndex = pRemoteNode->i4RemIndex;
    pRemLocIdInfo->u4RemDestAddrTblIndex = pRemoteNode->u4DestAddrTblIndex;

    /* Decode the attributes from Location Identification Information field
       --------------------------------------
       | Location Data Format | Location Id |
       --------------------------------------
       1 Octet                 Variable size
     */

    /* Get 1st Octet for Location Data format */
    LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, 0, u1LocData);

    /* There is some deviation observed between standard MIB and LLDP-MED 
     * draft version LLDP-MED Draft 07-R1.
     *
     * As per standard MIB:
     * LocationSubtype ::= TEXTUAL-CONVENTION
     *     SYNTAX      INTEGER  {
     *                    unknown(1),
     *               coordinateBased(2),
     *               civicAddress(3),
     *               elin(4)
     *
     * As per standard document:
     *   Location data 
     *   format type         value 
     *   --------------------------
     *   0                   Invalid 
     *   1                   Coordinate-based LCI  
     *   2                   Civic Address LCI  
     *   3                   ECS ELIN  
     *   4 - 255             Reserved for future expansion
     *
     * To support both MIB and standard, the below covertion takes place.
     * */

    if (u1LocData == LLDP_MED_COORDINATE)
    {
        pRemLocIdInfo->u1RemLocationSubType = LLDP_MED_COORDINATE_LOC;
    }
    else if (u1LocData == LLDP_MED_CIVIC)
    {
        pRemLocIdInfo->u1RemLocationSubType = LLDP_MED_CIVIC_LOC;
    }
    else if (u1LocData == LLDP_MED_ELIN)
    {
        pRemLocIdInfo->u1RemLocationSubType = LLDP_MED_ELIN_LOC;
    }
    else
    {
        pRemLocIdInfo->u1RemLocationSubType = u1LocData;
    }
    /* Get Location Identification Value */
    LLDP_LBUF_GET_STRING (pu1TlvInfo, &(pRemLocIdInfo->au1RemLocationID[0]),
                          0,
                          u2TlvLength - (LLDP_TLV_SUBTYPE_LEN +
                                         LLDP_MAX_LEN_OUI +
                                         LLDP_MED_LOC_DATA_FORMAT_LEN));

    /* Insert the Node in the LldpMedRemLocationInfoRBTree */
    if (RBTreeAdd (gLldpGlobalInfo.LldpMedRemLocationInfoRBTree,
                   (tRBElem *) pRemLocIdInfo) != RB_SUCCESS)
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC | LLDP_MED_LOC_ID_TRC,
                       "LLDPDU received on port %d "
                       "contains multiple Location Identification Tlvs with the same"
                       "Location subtype and port number combination\n",
                       pRemoteNode->i4RemLocalPortNum);
        MemReleaseMemBlock (gLldpGlobalInfo.LldpMedRemLocationPoolId,
                            (UINT1 *) pRemLocIdInfo);
        return OSIX_FAILURE;
    }

    /* Since the Location TLV is included in PDU by the agent,
     * enable the TLV in Remote database */
    pRemoteNode->RemMedCapableInfo.u2MedRemCapEnable |=
        LLDP_MED_LOCATION_ID_TLV;
    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME    : LldpMedDecodePowMDITlv
 *
 * DESCRIPTION      : This routine updates the LLDP-MED Power-Via-MDI
 *                    TLV information in the Remote Node.
 *
 * INPUT            : pRemoteNode - The Node to be updated with Remote
 *                                  Med Capability Values.
 *                    pu1TlvInfo  - Points to the TLV Subtype information field
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
LldpMedDecodePowMDITlv (tLldpRemoteNode * pRemoteNode, UINT1 *pu1TlvInfo)
{
    UINT1               u1RemPowInfo = 0;

    /* Decode the attributes from Extended Power-via-MDI field
       -------------------------------------
       | Power | Power  | Power    | Power |
       | Type  | Source | Priority | Value |
       -------------------------------------
       2 bits   2 bits   4 bits    2 Octets
       <------------3 Octets---------------> */

    /* Get first octet for Power Info */
    LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, 0, u1RemPowInfo);

    /* Get 2 bits for Power type */
    pRemoteNode->RemMedPowerMDIInfo.u1PoEDeviceType |=
        (u1RemPowInfo & LLDP_MED_POWER_TYPE_BIT_MASK)
        >> LLDP_MED_POWER_TYPE_SHIFT_BIT;

    /* Get 2 bits for Power Source */
    pRemoteNode->RemMedPowerMDIInfo.u1PowerSource |=
        (u1RemPowInfo & LLDP_MED_POWER_SRC_BIT_MASK)
        >> LLDP_MED_POWER_SRC_SHIFT_BIT;

    /* Get 4 bits for Power priority */
    pRemoteNode->RemMedPowerMDIInfo.u1PowerPriority |=
        (u1RemPowInfo & LLDP_MED_POWER_PRI_BIT_MASK);

    /* Get 2 Octets for Power Value */
    LLDP_LBUF_GET_2_BYTE (pu1TlvInfo, 0, pRemoteNode->RemMedPowerMDIInfo.
                          u2PowerValue);
    /* Since the Extended Power TLV is included in PDU by the agent,
     * enable the TLV in Remote database */
    pRemoteNode->RemMedCapableInfo.u2MedRemCapEnable |= LLDP_MED_PW_MDI_PD_TLV;
    return OSIX_SUCCESS;

}

#endif /* _LLDP_MEDTLV_C_ */
/*--------------------------------------------------------------------------*/
/*                       End of the file  lldmed.c                      */
/*--------------------------------------------------------------------------*/
