/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldapi.c,v 1.51 2017/12/26 11:05:46 siva Exp $
 *
 * Description: This file contains the APIs exported by LLDP module.
 ******************************************************************************/
#ifndef _LLDPAPI_C_
#define _LLDPAPI_C_
#include "lldinc.h"

extern INT4         MbsmIsConnectingPort (UINT4 u4IfIndex);

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiIsLldpCtrlFrame
 *
 *    DESCRIPTION      : This function checks whether the given frame is
 *                       an LLDP control frame. It returns OSIX_TRUE if it
 *                       is an LLDP control frame.
 *
 *    INPUT            : pBuf     - Frame to be verified
 *                       u4IfIndex - Index of the port on which the frame was 
 *                                   received
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_TRUE / OSIX_FALSE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpApiIsLldpCtrlFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex)
{
    tMacAddr            SrcAddr;
    tMacAddr            DstAddr;
    UINT2               u2ProtType = 0;
    UINT4               u4Offset = 0;
    UINT1               au1LldpMcastAddr[MAC_ADDR_LEN] =
        { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x0E };
    UINT1               au1LldpCustBrgMcastAddr[MAC_ADDR_LEN] =
        { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x00 };
    UINT1               au1LldpNonTPMRBrgiMcastAddr[MAC_ADDR_LEN] =
        { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x03 };
    UINT1               au1DstMac[MAC_ADDR_LEN] = { 0 };
    UINT1               u1Flag = OSIX_TRUE;
    tLldpLocPortInfo   *pPortEntry = NULL;
    tCfaIfInfo          CfaIfInfo;
    tLldpv2AgentToLocPort AgentToLocPort;
    tLldpv2AgentToLocPort *pAgentToLocPort = NULL;
    tLldpLocPortTable  *pTmpLocPortTable = NULL;
    tLldpLocPortTable  *pLocPortTable = NULL;

    MEMSET (&SrcAddr, 0, sizeof (tMacAddr));
    MEMSET (&DstAddr, 0, sizeof (tMacAddr));
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    MEMSET (&AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));

    if (CRU_BUF_Copy_FromBufChain (pBuf, SrcAddr, LLDP_SRCADDR_OFFSET,
                                   MAC_ADDR_LEN) == CRU_FAILURE)
    {
        return OSIX_FALSE;
    }

    if (CRU_BUF_Copy_FromBufChain (pBuf, DstAddr, LLDP_DESTADDR_OFFSET,
                                   MAC_ADDR_LEN) == CRU_FAILURE)
    {
        return OSIX_FALSE;
    }

    if (gLldpGlobalInfo.u1LldpSystemControl == LLDP_SHUTDOWN)
    {
        return OSIX_FALSE;
    }

    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_09)
    {
        if ((pTmpLocPortTable = (tLldpLocPortTable *)
             MemAllocMemBlk (gLldpGlobalInfo.LocPortTablePoolId)) == NULL)
        {
            LLDP_TRC ((LLDP_CRITICAL_TRC | ALL_FAILURE_TRC),
                      "LldpApiIsLldpCtrlFrame: Failed to Allocate Memory "
                      "for LldpLocPortTable node \r\n");
            LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
            return OSIX_FALSE;
        }
        MEMSET (pTmpLocPortTable, 0, sizeof (tLldpLocPortTable));
        pTmpLocPortTable->i4IfIndex = (INT4) u4IfIndex;
        pLocPortTable = (tLldpLocPortTable *)
            RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, pTmpLocPortTable);

        /* pPortEntry is not NULL, port with same index already exists */
        if (pLocPortTable == NULL)
        {
            LLDP_TRC_ARG1 (INIT_SHUT_TRC, "LldpApiIsLldpCtrlFrame: "
                           "Port entry not found for index %d\r\n", u4IfIndex);
            MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                (UINT1 *) pTmpLocPortTable);
            return OSIX_FALSE;
        }

        AgentToLocPort.i4IfIndex = (INT4) u4IfIndex;
        if ((u4IfIndex > LLDP_MAX_PORTS) ||
            ((MEMCMP (DstAddr, au1LldpMcastAddr, MAC_ADDR_LEN)) == 0))
        {
            MEMCPY (AgentToLocPort.Lldpv2DestMacAddress, au1LldpMcastAddr,
                    MAC_ADDR_LEN);
        }
        else if ((MEMCMP (DstAddr, au1LldpCustBrgMcastAddr, MAC_ADDR_LEN) == 0)
                 || (MEMCMP (DstAddr, au1LldpNonTPMRBrgiMcastAddr, MAC_ADDR_LEN)
                     == 0))
        {
            MEMCPY (AgentToLocPort.Lldpv2DestMacAddress, DstAddr, MAC_ADDR_LEN);
        }
        else if ((FS_UTIL_IS_MCAST_MAC (DstAddr) == OSIX_TRUE))
        {
            MEMCPY (AgentToLocPort.Lldpv2DestMacAddress, DstAddr, MAC_ADDR_LEN);
        }
        else
        {
            MEMCPY (AgentToLocPort.Lldpv2DestMacAddress, SrcAddr, MAC_ADDR_LEN);
        }
        pAgentToLocPort = (tLldpv2AgentToLocPort *)
            RBTreeGet (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                       &AgentToLocPort);
        if (pAgentToLocPort == NULL)
        {
            MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                (UINT1 *) pTmpLocPortTable);
            return OSIX_FALSE;
        }
        u1Flag = OSIX_TRUE;
        MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                            (UINT1 *) pTmpLocPortTable);
    }
    else
    {

        AgentToLocPort.i4IfIndex = (INT4) u4IfIndex;
        if ((u4IfIndex > LLDP_MAX_PORTS) ||
            ((MEMCMP (DstAddr, au1LldpMcastAddr, MAC_ADDR_LEN)) == 0))
        {
            MEMCPY (AgentToLocPort.Lldpv2DestMacAddress, au1LldpMcastAddr,
                    MAC_ADDR_LEN);
        }
        else
        {
            MEMCPY (AgentToLocPort.Lldpv2DestMacAddress, SrcAddr, MAC_ADDR_LEN);
        }
        pAgentToLocPort = (tLldpv2AgentToLocPort *)
            RBTreeGet (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                       &AgentToLocPort);
        if (pAgentToLocPort == NULL)
        {
            return OSIX_FALSE;
        }
        pPortEntry = LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);
        if (pPortEntry == NULL)
        {
            LLDP_TRC_ARG1 (INIT_SHUT_TRC, "LldpApiIsLldpCtrlFrame: "
                           "local port info not found for port %d\r\n",
                           u4IfIndex);
            return OSIX_FALSE;
        }
        if (u4IfIndex > LLDP_MAX_PORTS)
        {
            MEMCPY (au1DstMac, au1LldpMcastAddr, MAC_ADDR_LEN);
        }
        else
        {
            MEMCPY (au1DstMac, pPortEntry->PortConfigTable.u1DstMac,
                    MAC_ADDR_LEN);
        }
        if ((MEMCMP (DstAddr, au1LldpMcastAddr, MAC_ADDR_LEN)) != 0)
        {
            if ((MEMCMP (SrcAddr, au1DstMac, MAC_ADDR_LEN)) == 0)
            {
                u1Flag = OSIX_TRUE;
            }
            else
            {
                u1Flag = OSIX_FALSE;
            }
        }
        else
        {
            u1Flag = OSIX_TRUE;
        }

    }
    if (u1Flag == OSIX_TRUE)
    {
        if (LldpPortGetIfInfo (u4IfIndex, &CfaIfInfo) != OSIX_SUCCESS)
        {
            LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpApiIsLldpCtrlFrame: "
                           "Failed to get CfaIfIndex for ifindex %d\r\n",
                           u4IfIndex);
            return OSIX_FALSE;
        }
        if (CfaIfInfo.u1IfType == CFA_ENET)
        {
            u4Offset = LLDP_PROTOCOL_TYPE_OFFSET;
        }
        else if ((CfaIfInfo.u1IfType == CFA_TOKENRING) ||
                 (CfaIfInfo.u1IfType == CFA_FDDI))
        {
            u4Offset = LLDP_SNAP_ENCODED_PROTO_TYPE_OFFSET;
        }
        else
        {
            return OSIX_FALSE;
        }
        if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2ProtType, u4Offset,
                                       CFA_ENET_TYPE_OR_LEN) == CRU_FAILURE)
        {
            return OSIX_FALSE;
        }
        u2ProtType = OSIX_NTOHS (u2ProtType);

        if (u2ProtType == LLDP_ENET_TYPE)
        {
            return OSIX_TRUE;
        }
    }

    return OSIX_FALSE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyIfCreate
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message
 *                       queue to indicate the creation of physical port
 *
 *    INPUT            : u4IfIndex - Interface index
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpApiNotifyIfCreate (UINT4 u4IfIndex)
{
    tLldpQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC,
                  "LldpApiNotifyIfCreate: LLDP is not started\r\n");
        return OSIX_FAILURE;
    }

    if ((u4IfIndex < LLDP_MIN_PORTS) || (u4IfIndex > LLDP_MAX_PORTS))
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpApiNotifyIfCreate "
                       "called for non-physical interface %d\r\n", u4IfIndex);
        return OSIX_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tLldpQMsg *) MemAllocMemBlk (gLldpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        LLDP_TRC_ARG1 (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                       "LldpApiNotifyIfCreate: Port %d :"
                       " Intf creation Msg ALLOC_MEM_BLOCK FAILED\r\n",
                       u4IfIndex);
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpApiNotifyIfCreate: Port %d : Intf creation"
                      "Msg ALLOC_MEM_BLOCK FAILED", u4IfIndex));
#endif

        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        LLDP_ERR_CNTR_INPUT_Q_OVERFLOW++;
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tLldpQMsg));

    pMsg->u4Port = u4IfIndex;
    pMsg->u4MsgType = LLDP_CREATE_PORT_MSG;
    i4RetVal = LldpQueEnqAppMsg (pMsg);

    return (i4RetVal);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyIfMap
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message
 *                       queue to indicate the mapping of physical port
 *
 *    INPUT            : u4IfIndex - Interface index
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/

PUBLIC INT4
LldpApiNotifyIfMap (UINT4 u4IfIndex)
{
    tLldpQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "LldpApiNotifyIfMap: LLDP is not started\r\n");
        return OSIX_FAILURE;
    }

    if ((u4IfIndex < LLDP_MIN_PORTS) || (u4IfIndex > LLDP_MAX_PORTS))
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpApiNotifyIfMap "
                  "called for non-physical interface\r\n");
        return OSIX_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tLldpQMsg *) MemAllocMemBlk (gLldpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        LLDP_TRC_ARG1 (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                       "LldpApiNotifyIfMap: Port %d :"
                       " Intf mapping Msg ALLOC_MEM_BLOCK FAILED\r\n",
                       u4IfIndex);
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpApiNotifyIfMap: Port %d : Intf mapping Msg"
                      "ALLOC_MEM_BLOCK FAILED", u4IfIndex));
#endif

        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        LLDP_ERR_CNTR_INPUT_Q_OVERFLOW++;
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tLldpQMsg));

    pMsg->u4Port = u4IfIndex;
    pMsg->u4MsgType = LLDP_MAP_PORT_MSG;
    i4RetVal = LldpQueEnqAppMsg (pMsg);
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyIfDelete
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message
 *                       queue to indicate the deletion of physical port
 *
 *    INPUT            : u4IfIndex - Interface index
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpApiNotifyIfDelete (UINT4 u4IfIndex)
{
    tLldpQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC,
                  "LldpApiNotifyIfDelete: LLDP is not started\r\n");
        return OSIX_FAILURE;
    }

    if ((u4IfIndex < LLDP_MIN_PORTS) || (u4IfIndex > LLDP_MAX_PORTS))
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpApiNotifyIfDelete "
                       "called for non-physical interface %d\r\n", u4IfIndex);
        return OSIX_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tLldpQMsg *) MemAllocMemBlk (gLldpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        LLDP_TRC_ARG1 (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                       "LldpApiNotifyIfDelete: Port %d :"
                       " Intf Deletion Msg ALLOC_MEM_BLOCK FAILED\r\n",
                       u4IfIndex);
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpApiNotifyIfDelete: Port %d : Intf Deletion Msg"
                      "ALLOC_MEM_BLOCK FAILED", u4IfIndex));
#endif

        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        LLDP_ERR_CNTR_INPUT_Q_OVERFLOW++;
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tLldpQMsg));

    pMsg->u4Port = u4IfIndex;
    pMsg->u4MsgType = LLDP_DELETE_PORT_MSG;
    i4RetVal = LldpQueEnqAppMsg (pMsg);

    return (i4RetVal);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyIfUnMap
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message
 *                       queue to indicate the unmapping of physical port
 *
 *    INPUT            : u4IfIndex - Interface index
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/

PUBLIC INT4
LldpApiNotifyIfUnMap (UINT4 u4IfIndex)
{
    tLldpQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC,
                  "LldpApiNotifyIfUnMap: LLDP is not started\r\n");
        return OSIX_FAILURE;
    }

    if ((u4IfIndex < LLDP_MIN_PORTS) || (u4IfIndex > LLDP_MAX_PORTS))
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpApiNotifyIfUnMap "
                       "called for non-physical interface %d\r\n", u4IfIndex);
        return OSIX_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tLldpQMsg *) MemAllocMemBlk (gLldpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        LLDP_TRC_ARG1 (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                       "LldpApiNotifyIfUnMap: Port %d :"
                       " Intf Unmapping Msg ALLOC_MEM_BLOCK FAILED\r\n",
                       u4IfIndex);
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpApiNotifyIfUnMap: Port %d : Intf Unmapping Msg"
                      "ALLOC_MEM_BLOCK FAILED", u4IfIndex));
#endif

        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        LLDP_ERR_CNTR_INPUT_Q_OVERFLOW++;
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tLldpQMsg));

    pMsg->u4Port = u4IfIndex;
    pMsg->u4MsgType = LLDP_UNMAP_PORT_MSG;
    i4RetVal = LldpQueEnqAppMsg (pMsg);
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyIfAlias
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message 
 *                       queue to notify  the change in Interface Alias
 *
 *    INPUT            : u4IfIndex - Interface index 
 *                       pu1IfAlias - Pointer to INterface Alias
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpApiNotifyIfAlias (UINT4 u4IfIndex, UINT1 *pu1IfAlias)
{
    tLldpQMsg          *pMsg = NULL;
    UINT4               u4DefIfIndex = 0;
    INT4                i4RetVal = OSIX_SUCCESS;
    tCfaIfInfo          CfaIfInfo;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC,
                  "LldpApiNotifyIfAlias: LLDP is not started\r\n");
        return OSIX_FAILURE;
    }

    if (LldpPortGetIfInfo (u4IfIndex, &CfaIfInfo) != OSIX_SUCCESS)
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpApiNotifyIfAlias "
                       "Failed to get CfaIfInfo for port %d\r\n", u4IfIndex);
        return OSIX_FAILURE;
    }
    /* if the interface type is not CFA_ENET(port id subtype) and if the 
     * ifIndex is not default router ifindex no need to process the 
     * notification */
    LldpPortGetDefaultRouterIfIndex (&u4DefIfIndex);
    if ((CfaIfInfo.u1IfType != CFA_ENET) && (u4IfIndex != u4DefIfIndex))
    {
        LLDP_TRC_ARG2 (CONTROL_PLANE_TRC, "LldpApiNotifyIfAlias"
                       " iftype %d is neither CFA_ENET nor CFA_L2VLAN for port %d\r\n",
                       CfaIfInfo.u1IfType, u4IfIndex);
        return OSIX_FAILURE;
    }

    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                   "LldpApiNotifyIfAlias:posting Interface Alias"
                   "change event for interface index %d\r\n", u4IfIndex);
    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tLldpQMsg *) MemAllocMemBlk (gLldpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        LLDP_TRC_ARG1 (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                       "LldpApiNotifyIfAlias: Port %d : "
                       "Intf Msg ALLOC_MEM_BLOCK FAILED\r\n", u4IfIndex);
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpApiNotifyIfAlias: Port %d : Intf Msg"
                      "ALLOC_MEM_BLOCK FAILED", u4IfIndex));
#endif

        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        LLDP_ERR_CNTR_INPUT_Q_OVERFLOW++;
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tLldpQMsg));

    pMsg->u4Port = u4IfIndex;
    STRNCPY (pMsg->IfAlias, pu1IfAlias,
             MEM_MAX_BYTES (STRLEN (pu1IfAlias), CFA_MAX_PORT_NAME_LENGTH));
    pMsg->u4MsgType = LLDP_PORT_IFALIAS_MSG;
    i4RetVal = LldpQueEnqAppMsg (pMsg);

    return (i4RetVal);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyAgentCircuitId 
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message 
 *                       queue to notify the change in Agent Circuit Id
 *
 *    INPUT            : u4IfIndex - Interface index 
 *                       u4AgentCircuitId - Agent CircuitID
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpApiNotifyAgentCircuitId (UINT4 u4IfIndex, UINT4 u4AgentCircuitId)
{
    tLldpQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC,
                  "LldpApiNotifyAgentCircuitId: LLDP is not started\r\n");
        return OSIX_FAILURE;
    }

    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                   "LldpApiNotifyAgentCircuitId:posting agent circuit ID"
                   "change event for port %d\r\n", u4IfIndex);

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tLldpQMsg *) MemAllocMemBlk (gLldpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        LLDP_TRC_ARG1 (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                       "LldpApiNotifyAgentCircuitId: Port %d :"
                       " Intf Msg ALLOC_MEM_BLOCK FAILED\r\n", u4IfIndex);
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpApiNotifyAgentCircuitId: Port %d : Intf Msg"
                      "ALLOC_MEM_BLOCK FAILED", u4IfIndex));
#endif

        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        LLDP_ERR_CNTR_INPUT_Q_OVERFLOW++;
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tLldpQMsg));

    pMsg->u4Port = u4IfIndex;
    pMsg->AgentCircuitId = u4AgentCircuitId;
    pMsg->u4MsgType = LLDP_PORT_AGENT_CKTID_MSG;
    i4RetVal = LldpQueEnqAppMsg (pMsg);

    return (i4RetVal);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyIfOperStatusChg
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message
 *                       queue to indicate the oper status change of physical 
 *                       port
 *
 *    INPUT            : u4IfIndex    - Interface index
 *                       u1OperStatus - Operational link status of the port
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpApiNotifyIfOperStatusChg (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tLldpQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC,
                  "LldpApiNotifyIfOperStatusChg: LLDP is not started\r\n");
        return OSIX_FAILURE;
    }

    if ((u4IfIndex < LLDP_MIN_PORTS) || (u4IfIndex > LLDP_MAX_PORTS))
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpApiNotifyIfOperStatusChg"
                       "called for non-physical interface %d\r\n", u4IfIndex);
        return OSIX_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tLldpQMsg *) MemAllocMemBlk (gLldpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        LLDP_TRC_ARG1 (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                       "LldpApiNotifyIfOperStatusChg: Port %d :"
                       " Intf Msg ALLOC_MEM_BLOCK FAILED\r\n", u4IfIndex);
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpApiNotifyIfOperStatusChg: Port %d : Intf Msg"
                      "ALLOC_MEM_BLOCK FAILED", u4IfIndex));
#endif

        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        LLDP_ERR_CNTR_INPUT_Q_OVERFLOW++;
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tLldpQMsg));

    pMsg->u4Port = u4IfIndex;
    pMsg->u4MsgType = LLDP_OPER_STATUS_CHG_MSG;
    pMsg->OperStatus = u1OperStatus;
    i4RetVal = LldpQueEnqAppMsg (pMsg);

    return (i4RetVal);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiEnqIncomingFrame
 *
 *    DESCRIPTION      : This function receives the incoming LLDP frame from
 *                       the CFA Module and enqueues it to LLDP Control task.
 *
 *    INPUT            : pBuf      - Pointer to the received buffer
 *                       u4IfIndex - Interface index
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpApiEnqIncomingFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex)
{
    tLldpRxPduQMsg     *pMsg = NULL;

    if (LLDP_MODULE_STATUS () != LLDP_ENABLED)
    {
        LLDP_TRC (CONTROL_PLANE_TRC,
                  "LldpApiEnqIncomingFrame: " "Module not enabled \r\n");
        return OSIX_FAILURE;
    }

    if ((u4IfIndex < LLDP_MIN_PORTS) || (u4IfIndex > LLDP_MAX_PORTS))
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpApiEnqIncomingFrame : "
                       "called for non-physical interface %d\r\n", u4IfIndex);
        return OSIX_FAILURE;
    }

    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC, "LldpApiEnqIncomingFrame : "
                   "LLDPDU is Received on port %d\r\n", u4IfIndex);

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tLldpRxPduQMsg *) MemAllocMemBlk
         (gLldpGlobalInfo.RxPduQPoolId)) == NULL)
    {
        LLDP_TRC_ARG1 (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                       "LldpApiEnqIncomingFrame: Port %d :"
                       " MemAlloc Failed for RxPduQPoolId.\r\n", u4IfIndex);
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpApiEnqIncomingFrame: Port %d :"
                      "MemAlloc Failed for RxPduQPoolId.", u4IfIndex));
#endif

        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        LLDP_ERR_CNTR_INPUT_Q_OVERFLOW++;
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tLldpRxPduQMsg));

    pMsg->u4Port = u4IfIndex;
    pMsg->pLldpPdu = pBuf;

    if (LldpQueEnqRxPduMsg (pMsg) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.RxPduQPoolId, (UINT1 *) pMsg);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyPortVlanId 
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message 
 *                       queue to notify  the change in Port Vlan Id
 *
 *    INPUT            : u4IfIndex - Interface index 
 *                       u2VlanId  - Port VLAN Id
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/

PUBLIC INT4
LldpApiNotifyPortVlanId (UINT4 u4IfIndex, UINT2 u2VlanId)
{
    tLldpQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC,
                  "LldpApiNotifyPortVlanId: LLDP is not started\r\n");
        return OSIX_FAILURE;
    }

    if ((u4IfIndex < LLDP_MIN_PORTS) || (u4IfIndex > LLDP_MAX_PORTS))
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LLDPNotifyPortVlanId "
                       "called for non-physical interface %d\r\n", u4IfIndex);
        return OSIX_FAILURE;
    }

    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                   "LLDPNotifyPortVlanId:Updating port vlan id of port %d\r\n",
                   u4IfIndex);

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tLldpQMsg *) MemAllocMemBlk (gLldpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        LLDP_TRC_ARG1 (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                       "LLDPNotifyPortVlanId: Port %d :"
                       " Intf Msg ALLOC_MEM_BLOCK FAILED\r\n", u4IfIndex);
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LLDPNotifyPortVlanId: Port %d : Intf Msg"
                      "ALLOC_MEM_BLOCK FAILED", u4IfIndex));
#endif

        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        LLDP_ERR_CNTR_INPUT_Q_OVERFLOW++;
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tLldpQMsg));

    pMsg->u4Port = u4IfIndex;
    pMsg->u4MsgType = LLDP_PORT_VLAN_ID_MSG;
    pMsg->NewVlanId = u2VlanId;
    i4RetVal = LldpQueEnqAppMsg (pMsg);

    return (i4RetVal);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyProtoVlanStatus 
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message 
 *                       queue to notify  the change in the status of Protocol
 *                       based Vlan Classification on the port. If the status 
 *                       is changed globally then this function will be called
 *                       with u4IfIndex as zero.
 *
 *    INPUT            : u4IfIndex - Interface index 
 *                       u1ProtVlanStatus  - Status of protocol based vlan 
 *                       classification (Enabled / Disabled) 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpApiNotifyProtoVlanStatus (UINT4 u4IfIndex, UINT1 u1ProtVlanStatus)
{
    tLldpQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC,
                  "LldpApiNotifyProtoVlanStatus: LLDP is not started\r\n");
        return OSIX_FAILURE;
    }

    /*u4IfIndex can be zero. So the minimum condition is not checked */
    if (u4IfIndex > LLDP_MAX_PORTS)
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpApiNotifyProtoVlanStatus "
                       "called for non-physical interface %d\r\n", u4IfIndex);
        return OSIX_FAILURE;
    }

    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                   "LldpApiNotifyProtoVlanStatus:Updating status of "
                   "protocol based vlan Classification on the port %d\r\n",
                   u4IfIndex);

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tLldpQMsg *) MemAllocMemBlk (gLldpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        LLDP_TRC_ARG1 (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                       "LldpApiNotifyProtoVlanStatus: Port %d :"
                       " Intf Msg ALLOC_MEM_BLOCK FAILED\r\n", u4IfIndex);
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpApiNotifyProtoVlanStatus: Port %d : Intf Msg"
                      " ALLOC_MEM_BLOCK FAILED", u4IfIndex));
#endif

        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        LLDP_ERR_CNTR_INPUT_Q_OVERFLOW++;
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tLldpQMsg));

    pMsg->u4Port = u4IfIndex;
    pMsg->u4MsgType = LLDP_PROT_VLAN_STATUS_MSG;
    pMsg->ProtVlanStatus = u1ProtVlanStatus;

    i4RetVal = LldpQueEnqAppMsg (pMsg);

    return (i4RetVal);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyProtoVlanId 
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message 
 *                       queue to notify  the change in the Protocol Vlan Id
 *                       mapped to the port.
 *
 *    INPUT            : u4IfIndex    - Interface index 
 *                       u2VlanId     - Protocol Vlan Id which need to be added
 *                                      or deleted to/from LLDP data structure.
 *                       u1ActionFlag - Operation to be performed (Add / Delete 
 *                                      / Updation)
 *                       u2OldVlanId  - Non-zero only if u1ActionFlag = Updation
 *                                      i.e if for the same protocol
 *                                      group the protocol vlan id is replaced 
 *                                      with another protocol vlan id. In this 
 *                                      case the node containing the u2OldVlanId 
 *                                      will be deleted and a new node with 
 *                                      u2VlanId will be added.  
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/

PUBLIC INT4
LldpApiNotifyProtoVlanId (UINT4 u4IfIndex, UINT2 u2VlanId, UINT1 u1ActionFlag,
                          UINT2 u2OldVlanId)
{
    tLldpQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC,
                  "LldpApiNotifyProtoVlanId: LLDP is not started\r\n");
        return OSIX_FAILURE;
    }

    if ((u4IfIndex < LLDP_MIN_PORTS) || (u4IfIndex > LLDP_MAX_PORTS))
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpApiNotifyProtoVlanId "
                       "called for non-physical interface %d\r\n", u4IfIndex);
        return OSIX_FAILURE;
    }

    LLDP_TRC_ARG3 (CONTROL_PLANE_TRC, "LldpApiNotifyProtoVlanId:Updating"
                   " Portocol vlan id for port %d with vlan id %d and old vlan"
                   " id %d\r\n", u4IfIndex, u2VlanId, u2OldVlanId);

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tLldpQMsg *) MemAllocMemBlk (gLldpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        LLDP_TRC_ARG1 (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                       "LldpApiNotifyProtoVlanId: Port %d :"
                       " Intf Msg ALLOC_MEM_BLOCK FAILED\r\n", u4IfIndex);
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpApiNotifyProtoVlanId: Port %d : Intf Msg"
                      "ALLOC_MEM_BLOCK FAILED ", u4IfIndex));
#endif

        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        LLDP_ERR_CNTR_INPUT_Q_OVERFLOW++;
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tLldpQMsg));

    pMsg->u4Port = u4IfIndex;
    pMsg->u4MsgType = LLDP_PROT_VLAN_ID_MSG;
    pMsg->u4MsgSubType = (UINT4) u1ActionFlag;
    pMsg->NewVlanId = u2VlanId;
    pMsg->OldVlanId = u2OldVlanId;
    i4RetVal = LldpQueEnqAppMsg (pMsg);

    return (i4RetVal);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyPortAggCapability 
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message 
 *                       queue to notify  the change in the Port Aggregation 
 *                       Capability.
 *
 *    INPUT            : u4IfIndex    - Interface index 
 *                       u1AggCap     - Aggregation Capability 
 *                                     (TRUE indicates capable of being 
 *                                     aggregated and FALSE indicates  not
 *                                     capable of being aggregated) 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/

PUBLIC INT4
LldpApiNotifyPortAggCapability (UINT4 u4IfIndex, UINT1 u1AggCap)
{
    tLldpQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC,
                  "LldpApiNotifyPortAggCapability: LLDP is not started\r\n");
        return OSIX_FAILURE;
    }

    if ((u4IfIndex < LLDP_MIN_PORTS) || (u4IfIndex > LLDP_MAX_PORTS))
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpApiNotifyPortAggCapability "
                       "called for non-physical interface %d\r\n", u4IfIndex);
        return OSIX_FAILURE;
    }

    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC, "LldpApiNotifyPortAggCapability:Updating"
                   " Port Aggregation Capability for port %d\r\n", u4IfIndex);

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tLldpQMsg *) MemAllocMemBlk (gLldpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        LLDP_TRC_ARG1 (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                       "LldpApiNotifyPortAggCapability: Port %d :"
                       " Intf Msg ALLOC_MEM_BLOCK FAILED\r\n", u4IfIndex);
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpApiNotifyPortAggCapability: Port %d : Intf Msg"
                      "ALLOC_MEM_BLOCK FAILED", u4IfIndex));
#endif

        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        LLDP_ERR_CNTR_INPUT_Q_OVERFLOW++;
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tLldpQMsg));

    pMsg->u4Port = u4IfIndex;
    pMsg->u4MsgType = LLDP_PORT_AGG_CAP_MSG;
    pMsg->AggCapability = u1AggCap;

    i4RetVal = LldpQueEnqAppMsg (pMsg);

    return (i4RetVal);
}

/********************************************************************************
 *                                                                              *
 *    FUNCTION NAME    : LldpApiResetAgentVariables                             *
 *                                                                              *
 *    DESCRIPTION      : This function resets the  global variables             *
 *                       gu4LldpAgentNumber, gLldpAgentBitList and              *
 *                       gu4LldpDestMacAddrTblIndex and adds RB tree entry for  *
 *                       initialized gu4LldpDestMacAddrTblIndex.                *
 *                                                                              *
 *                                                                              *
 *    INPUT            : NONE                                                   *
 *                                                                              *
 *    OUTPUT           : NONE                                                   *
 *                                                                              *
 *    RETURNS          : NONE                                                   *
 *                                                                              *
 *******************************************************************************/

PUBLIC VOID
LldpApiResetAgentVariables (VOID)
{
    tLldpv2DestAddrTbl *pDestMacAddrTbl = NULL;
    pDestMacAddrTbl =
        (tLldpv2DestAddrTbl
         *) (MemAllocMemBlk (gLldpGlobalInfo.LldpDestMacAddrPoolId));

    if (pDestMacAddrTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD Memory allocation failed for Destination Mac address table  \r\n");
        return;
    }
/* Initializing all Agent related and destination mac address global variables */
    gu4LldpAgentNumber = SYS_DEF_MAX_PHYSICAL_INTERFACES + 1;
    gu4LldpDestMacAddrTblIndex = 1;

    MEMSET (pDestMacAddrTbl, 0, sizeof (tLldpv2DestAddrTbl));
    MEMCPY (pDestMacAddrTbl->Lldpv2DestMacAddress, gau1LldpMcastAddr,
            MAC_ADDR_LEN);
    pDestMacAddrTbl->u4LlldpV2DestAddrTblIndex = gu4LldpDestMacAddrTblIndex;

    if (RBTreeAdd (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree,
                   (tRBElem *) & (pDestMacAddrTbl->DestMacAddrTblNode)) !=
        RB_SUCCESS)
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                       " RBTree Add failed for Destination Mac address table. Dest Mac Index %d\r\n",
                       gu4LldpDestMacAddrTblIndex);
    }
    return;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyResetAggCapability 
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message 
 *                       queue to notify  the change in the Port Aggregation 
 *                       Capability when the port-channel is deleted.
 *
 *    INPUT            : AggConfPorts - List of ports which are not capable of 
 *                                      aggregation
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/

PUBLIC INT4
LldpApiNotifyResetAggCapability (tPortList AggConfPorts)
{
    tLldpQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC,
                  "LldpApiNotifyResetAggCapability: LLDP is not started\r\n");
        return OSIX_FAILURE;
    }

    LLDP_TRC (CONTROL_PLANE_TRC,
              "LldpApiNotifyResetAggCapability:Reseting Port Aggregation \r\n");

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tLldpQMsg *) MemAllocMemBlk (gLldpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpApiNotifyResetAggCapability Intf Msg"
                  " ALLOC_MEM_BLOCK FAILED\r\n");
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpApiNotifyResetAggCapability Intf Msg"
                      "ALLOC_MEM_BLOCK FAILED"));
#endif

        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        LLDP_ERR_CNTR_INPUT_Q_OVERFLOW++;
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tLldpQMsg));

    pMsg->u4MsgType = LLDP_RESET_AGG_CAP_MSG;
    MEMCPY (pMsg->PortList, AggConfPorts, sizeof (tPortList));
    i4RetVal = LldpQueEnqAppMsg (pMsg);

    return (i4RetVal);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyAggStatus 
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's queue 
 *                       whenever there is a change in the active port-channel
 *                       membership of a port.
 *
 *    INPUT            : u4IfIndex   - Index of the port which has become an
 *                                     active member of a port-channel or
 *                                     removed from the active member list.
 *                       u1AggStatus - Aggregation Status 
 *                                     (LLDP_AGG_ACTIVE_PORT /
 *                                     LLDP_AGG_NOT_ACTIVE_PORT)
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpApiNotifyAggStatus (UINT4 u4IfIndex, UINT1 u1AggStatus)
{
    tLldpQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC,
                  "LldpApiNotifyAggStatus: LLDP is not started\r\n");
        return OSIX_FAILURE;
    }

    if ((u4IfIndex < LLDP_MIN_PORTS) || (u4IfIndex > LLDP_MAX_PORTS))
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpApiNotifyAggStatus "
                       "called for non-physical interface %d\r\n", u4IfIndex);
        return OSIX_FAILURE;
    }

    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC, "LldpApiNotifyAggStatus:Updating Port"
                   " Aggregation Status for port %d\r\n", u4IfIndex);

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tLldpQMsg *) MemAllocMemBlk (gLldpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        LLDP_TRC_ARG1 (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                       "LldpApiNotifyAggStatus: Port %d :"
                       " Intf Msg ALLOC_MEM_BLOCK FAILED\r\n", u4IfIndex);
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpApiNotifyAggStatus: Port %d : Intf Msg"
                      "ALLOC_MEM_BLOCK FAILED", u4IfIndex));
#endif

        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        LLDP_ERR_CNTR_INPUT_Q_OVERFLOW++;
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tLldpQMsg));

    pMsg->u4Port = u4IfIndex;
    pMsg->u4MsgType = LLDP_PORT_AGG_STATUS_MSG;
    pMsg->AggStatus = u1AggStatus;

    i4RetVal = LldpQueEnqAppMsg (pMsg);

    return (i4RetVal);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifySysName
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's queue
 *                       to notify the change in the System Name. 
 *
 *    INPUT            : pu1SysName  - System Name string is passed through 
 *                                     this pointer.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 *    CALLED BY        : nmhSetSysName
 *
 *    CALLS            : LldpQueEnqAppMsg
 *
 ****************************************************************************/
INT4
LldpApiNotifySysName (UINT1 *pu1SysName)
{
    tLldpQMsg          *pMsg = NULL;

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC,
                  "LldpApiNotifySysName: LLDP is not started\r\n");
        return OSIX_FAILURE;
    }

    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC, "LldpApiNotifySysName : Recvd SysName "
                   "Chang Notification for System Name =%s\r\n", pu1SysName);

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tLldpQMsg *) MemAllocMemBlk (gLldpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpApiNotifySysName : Mem Allocation Failed for QMsgPoolId"
                  " while Notifying System Name change\r\n");
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpApiNotifySysName : Mem Allocation Failed for QMsgPoolId"
                      "while Notifying System Name change"));
#endif

        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        LLDP_ERR_CNTR_INPUT_Q_OVERFLOW++;
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tLldpQMsg));

    pMsg->u4MsgType = LLDP_SYS_NAME_CHG_MSG;
    STRNCPY (pMsg->SystemName, pu1SysName, LLDP_MAX_LEN_SYSNAME);

    if (LldpQueEnqAppMsg (pMsg) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.QMsgPoolId, (UINT1 *) pMsg);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
*
 *    FUNCTION NAME    : LldpApiNotifySysDesc
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's queue
 *                       to notify the change in the System Desc.
 *
 *    INPUT            : pu1SysDesc  - System Desc string is passed through
 *                                     this pointer.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 *    CALLED BY        : nmhGetSysDesc
 *
 *    CALLS            : LldpQueEnqAppMsg
 *
 ****************************************************************************/
INT4
LldpApiNotifySysDesc (UINT1 *pu1SysDesc)
{
    tLldpQMsg          *pMsg = NULL;

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC,
                  "LldpApiNotifySysDesc: LLDP is not started\r\n");
        return OSIX_FAILURE;
    }

    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC, "LldpApiNotifySysDesc : Recvd SysDesc "
                   "Chang Notification for System Desc =%s\r\n", pu1SysDesc);

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tLldpQMsg *) MemAllocMemBlk (gLldpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpApiNotifySysDesc : Mem Allocation Failed for QMsgPoolId"
                  " while Notifying System Desc change\r\n");
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpApiNotifySysDesc : Mem Allocation Failed for QMsgPoolId"
                      "while Notifying System Desc change"));
#endif

        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        LLDP_ERR_CNTR_INPUT_Q_OVERFLOW++;
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tLldpQMsg));

    pMsg->u4MsgType = LLDP_SYS_DESC_CHG_MSG;
    STRNCPY (pMsg->SystemDesc, pu1SysDesc, LLDP_MAX_LEN_SYSDESC);

    if (LldpQueEnqAppMsg (pMsg) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.QMsgPoolId, (UINT1 *) pMsg);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyIpv4IfStatusChange
 *
 *    DESCRIPTION      : This Callback function is registered with ipv4 
 *                       module(using NetIpv4RegisterHigherLayerProtocol() API)
 *                       to get the notification about the If status change.
 *                       Whenever status of any ip interface changed this
 *                       callback routine is invoked. 
 *                       After getting this 
 *                       notification LLDP module poll the if tables in in 
 *                       Ipv4 module to get the IP addresses of all active 
 *                       interfaces, and update its local Management Address 
 *                       database. These Ip addresses are used by LLDP module 
 *                       to construct the Management Address TLV's.
 *
 *    INPUT            : pNetIpIfInfo - If Config Record Structure
 *                       u4Mask - Unused Param
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 *    CALLED BY        : IPv4 Module
 *
 *    CALLS            : LldpQueEnqAppMsg
 *
 ****************************************************************************/
PUBLIC VOID
LldpApiNotifyIpv4IfStatusChange (tNetIpv4IfInfo * pNetIpIfInfo, UINT4 u4Mask)
{
    tLldpQMsg          *pMsg = NULL;

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC,
                  "LldpApiNotifyIpv4IfStatusChange: LLDP is not started\r\n");
        return;
    }

    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                   "LldpApiNotifyIpv4IfStatusChange : Rcvd Event mask = %d\r\n",
                   u4Mask);

    if ((u4Mask & OPER_STATE) || (u4Mask & IFACE_DELETED) ||
        (u4Mask & IP_ADDR_BIT_MASK))
        /* Process Oper Status Change/Ip Interface delete Notification */
    {
        /* Check whether the received Ip Address is stacking port IP when MBSM is enabled.
         * Since Stacking port should not visible to the user so not adding the stacking port ip
         * to management IP database */
#ifdef MBSM_WANTED
        if (MbsmIsConnectingPort (pNetIpIfInfo->u4CfaIfIndex) == MBSM_TRUE)
        {
            return;
        }
#endif
        /* Allocate Interface message buffer from the pool */
        if ((pMsg = (tLldpQMsg *) MemAllocMemBlk (gLldpGlobalInfo.QMsgPoolId))
            == NULL)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC,
                      "LldpApiNotifyIpv4IfStatusChange : Failed to allocate "
                      "memory for the QMsgPoolId\r\n");
            LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
            LLDP_ERR_CNTR_INPUT_Q_OVERFLOW++;
            return;
        }

        MEMSET (pMsg, 0, sizeof (tLldpQMsg));

        pMsg->u4MsgType = LLDP_IP4_MAN_ADDR_CHG_MSG;

        MEMCPY (&(pMsg->Ip4ManAddr.NetIpIfInfo), pNetIpIfInfo,
                sizeof (tNetIpv4IfInfo));
        pMsg->Ip4ManAddr.u4BitMap = u4Mask;

        if (LldpQueEnqAppMsg (pMsg) != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC,
                      "LldpApiNotifyIpv4IfStatusChange : Failed to Enqueue the"
                      " App Msg.\r\n");
            MemReleaseMemBlock (gLldpGlobalInfo.QMsgPoolId, (UINT1 *) pMsg);
            return;
        }
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyIpv6IfStatusChange
 *
 *    DESCRIPTION      : This Callback function is registered with ipv6 
 *                       module(using NetIpv6RegisterHigherLayerProtocol() API)
 *                       to get the notification about the If status change.
 *                       Whenever status of any ip interface changed this
 *                       callback routine is invoked. 
 *                       After getting this notification.
 *                       notification LLDP module poll the if tables in 
 *                       Ipv6 module to get the IP addresses of all active 
 *                       interfaces, and update its local Management Address 
 *                       database. These Ip addresses are used by LLDP module 
 *                       to construct the Management Address TLV's.
 *                       Registered with NETIPV6_ADDRESS_CHANGE.
 *
 *    INPUT            : tNetIpv6HliParams - If Config Record Structure
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 *    CALLED BY        : IPv6 Module
 *
 *    CALLS            : LldpQueEnqAppMsg
 *
 ****************************************************************************/
PUBLIC VOID
LldpApiNotifyIpv6IfStatusChange (tNetIpv6HliParams * pNetIpv6HlParams)
{
    tLldpQMsg          *pMsg = NULL;
    UINT4               u4Mask = 0;
    UINT4               u4Index = 0;
    UINT4               u4Type = 0;

    u4Mask = pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.u4Mask;
    u4Index = pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.u4Index;
    u4Type = pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.u4Type;

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC,
                  "LldpApiNotifyIpv6IfStatusChange: LLDP is not started\r\n");
        return;
    }

    if (pNetIpv6HlParams->u4Command != NETIPV6_ADDRESS_CHANGE)
    {
        return;
    }
    if ((u4Mask != NETIPV6_ADDRESS_ADD) && (u4Mask != NETIPV6_ADDRESS_DELETE))
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  "LldpApiNotifyIpv6IfStatusChange : Process only the IP "
                  "address Addition and Deletion Event. Ignoring all other "
                  "events\r\n");
        /* Process only the IP address Addition And Deletion Notification */
        return;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tLldpQMsg *) MemAllocMemBlk
         (gLldpGlobalInfo.QMsgPoolId)) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC,
                  "LldpApiNotifyIpv6IfStatusChange : Failed to allocate memory "
                  "for the QMsgPoolId");
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpApiNotifyIpv6IfStatusChange : Failed to allocate"
                      "memory for the QMsgPoolId"));
#endif

        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        LLDP_ERR_CNTR_INPUT_Q_OVERFLOW++;
        return;
    }

    MEMSET (pMsg, 0, sizeof (tLldpQMsg));

    pMsg->u4MsgType = LLDP_IP6_MAN_ADDR_CHG_MSG;

    MEMCPY (&(pMsg->Ip6ManAddr.Ipv6AddrInfo.Ip6Addr),
            &(pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.
              Ip6Addr), sizeof (tIp6Addr));
    pMsg->Ip6ManAddr.u4Mask = u4Mask;
    pMsg->Ip6ManAddr.u4Index = u4Index;
    pMsg->Ip6ManAddr.Ipv6AddrInfo.u4Type = u4Type;
    if (LldpQueEnqAppMsg (pMsg) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  "LldpApiNotifyIpv6IfStatusChange : Failed to Enqueue the "
                  "App Msg.\r\n");
        MemReleaseMemBlock (gLldpGlobalInfo.QMsgPoolId, (UINT1 *) pMsg);
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiModuleStart
 *
 *    DESCRIPTION      : Other modules invoke this API to start
 *                       LLDP module. In current implementation, Redundancy
 *                       Manager invokes this API start LLDP module in
 *                       cable pull-out scenario.
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpApiModuleStart (VOID)
{
    if (LLDP_SYSTEM_CONTROL () == LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC, "LLDP is already started\r\n");
        return OSIX_SUCCESS;
    }

    LldpLock ();
    if (LldpModuleStart () != OSIX_SUCCESS)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC, "LLDP module start failed\r\n");
        LldpUnLock ();
        return OSIX_FAILURE;
    }
    LldpUnLock ();

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiModuleShutDown 
 *
 *    DESCRIPTION      : Other modules invoke this API to shutdown
 *                       LLDP module. In current implementation, Redundancy
 *                       Manager invokes this API shutdown LLDP module in
 *                       cable pull-out scenario.
 *                       NOTE: This API will provide the fecility to
 *                       forcefully shutdown the LLDP module by stopping the
 *                       GlobalShutWhileTmr timer.
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
PUBLIC VOID
LldpApiModuleShutDown (VOID)
{
    tLldpLocPortInfo   *pPortEntry = NULL;
    tLldpLocPortInfo    PortEntry;
    UINT1               u1TimerId = 0;

    /* For LLDP Module shutdown is not an immediate process. once shutdown is
     * initiated. it will start a Global Shut-while timer (GlobalShutWhileTmr). 
     * Default interval of this timer is 2 Sec. Within this interval LLDP 
     * module remain in LLDP_SHUTDOWN_INPROGRESS state. It will not shutdown 
     * completely. and restrting the module will also be restricted by this 
     * timer.
     * But This API will provide the fecility to forcefully shutdown the LLDP
     * module  be stopping the timer forcefully */
    if (LLDP_SYSTEM_CONTROL () == LLDP_SHUTDOWN)
    {
        LLDP_TRC (CONTROL_PLANE_TRC, "LLDP is already shutdown\r\n");
        return;
    }

    LldpLock ();

    if (LLDP_SYSTEM_CONTROL () == LLDP_START)
        /* if LLDP_SYSTEM_CONTROL () is LLDP_SHUTDOWN_INPROGRESS then Module 
         * shutdown is already initiated */
    {
        LldpModuleShutDown ();
    }

    if (LLDP_GLOB_SHUT_WHILE_TMR_STATUS () == LLDP_TMR_RUNNING)
    {
        /* Forcefully shutdown the module now by stopping the
         * GlobalShutWhileTmr timer */
        if (TmrStopTimer (gLldpGlobalInfo.TmrListId,
                          &(gLldpGlobalInfo.GlobalShutWhileTmr.TimerNode))
            == TMR_SUCCESS)
        {
            /* Expiry Handler must be called to handle the pending shutdown 
             * routines */
            u1TimerId = (UINT1) LLDP_TX_TMR_GLOBAL_SHUT_WHILE;
            (*(gLldpGlobalInfo.aTmrDesc[u1TimerId].TmrExpFn)) (NULL);
        }
        else
        {
            LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "Failed to stop the Global ShutWhile Timer\r\n");
        }

        /* This particular case occurs when LLDP module is shut down and
         * all the ports are in initialise state */

        pPortEntry = (tLldpLocPortInfo *)
            RBTreeGetFirst (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree);
        while (pPortEntry != NULL)
        {
            MEMSET (&PortEntry, 0, sizeof (tLldpLocPortInfo));
            PortEntry.u4LocPortNum = pPortEntry->u4LocPortNum;
            /* Remove the port specific info from port table. */
            if (LldpTxUtlClearPortInfo (pPortEntry) != OSIX_SUCCESS)
            {
                LLDP_TRC (INIT_SHUT_TRC, "LldpIfCreate: "
                          "LldpTxUtlClearPortInfo returns FAILURE!!!\r\n");
            }
            /* Release Memory allocated for holding PDU Info */
            if (pPortEntry->pPreFormedLldpdu != NULL)
            {
                if (MemReleaseMemBlock (gLldpGlobalInfo.LldpPduInfoPoolId,
                                        (UINT1 *) (pPortEntry->
                                                   pPreFormedLldpdu)) ==
                    MEM_FAILURE)
                {
                    LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                              "LldpModuleShutDown: "
                              "PDU info Memory release failed!!.. "
                              "LLDP shutdown is unsuccessful\r\n");
                }
            }

            /* release the memory allocated for port info structure */
            RBTreeRem (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree, pPortEntry);
            if (MemReleaseMemBlock
                (gLldpGlobalInfo.LocPortInfoPoolId,
                 (UINT1 *) pPortEntry) == MEM_FAILURE)
            {
                LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "LldpModuleShutDown: "
                          "port info Memory release failed!!.. "
                          "LLDP shutdown is unsuccessful\r\n");
                continue;

            }
            /* reset the pointer corresponding to this port in the global
             * information strcture to NULL */
            pPortEntry = (tLldpLocPortInfo *)
                RBTreeGetNext (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
                               &PortEntry, LldpAgentInfoUtlRBCmpInfo);
        }
        pPortEntry = NULL;
        if (gLldpGlobalInfo.LldpPduInfoPoolId != 0)
        {
            if (MemDeleteMemPool (gLldpGlobalInfo.LldpPduInfoPoolId) ==
                MEM_FAILURE)
            {
                LLDP_TRC (OS_RESOURCE_TRC, "LldpModuleShutDown: "
                          "PDU Info MemPool Delete FAILED\r\n");
                LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "LldpModuleShutDown: "
                          "LLDP shutdown is unsuccessful\r\n");
            }
            gLldpGlobalInfo.LldpPduInfoPoolId = 0;
        }
        if (gLldpGlobalInfo.LocPortInfoPoolId != 0)
        {
            if (MemDeleteMemPool (gLldpGlobalInfo.LocPortInfoPoolId) ==
                MEM_FAILURE)
            {
                LLDP_TRC (OS_RESOURCE_TRC, "LldpModuleShutDown: "
                          "Local Port Info MemPool Delete FAILED\r\n");
                LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "LldpModuleShutDown: "
                          "LLDP shutdown is unsuccessful\r\n");
            }
            gLldpGlobalInfo.LocPortInfoPoolId = 0;
        }

        /* DeInit Timer */
        if (gLldpGlobalInfo.TmrListId != 0)
        {
            if (LldpTmrDeInit () == OSIX_FAILURE)
            {
                LLDP_TRC (OS_RESOURCE_TRC, "LldpModuleShutDown: "
                          "Timer Deinit FAILED\r\n");
                LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "LldpModuleShutDown"
                          "LLDP shutdown is unsuccessful\r\n");
            }
        }

        if (LldpPortDeRegisterWithRM () != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC,
                      "LldpMainTaskInit: DeRegistration with RM "
                      "FAILED!!!\r\n");
        }

        LLDP_SYSTEM_CONTROL () = LLDP_SHUTDOWN;
    }

    LldpUnLock ();

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpIsLldpStarted 
 *
 *    DESCRIPTION      : This is the portable routine to get the Shutdown 
 *                       Status of the module
 *
 *    INPUT            : None
 *
 *    OUTPUT           : gLldpGlobalInfo.u1LldpSystemControl  
 *
 *    RETURNS          : LLDP_START / LLDP_SHUTDOWN 
 *
 ****************************************************************************/
INT4
LldpIsLldpStarted (VOID)
{
    LldpLock ();

    if (LLDP_IS_SHUTDOWN ())
    {
        LldpUnLock ();
        return LLDP_FALSE;
    }

    LldpUnLock ();
    return LLDP_TRUE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiHandleApplPortRequest
 *
 *    DESCRIPTION      : Function to post application register data to LLDP
 *                       module
 *
 *    INPUT            : u4IfIndex - Port on which the application is to 
 *                                   be registered
 *                       pLldpAppPortMsg - Message to be stored in LLDP
 *                       u1ApplPortRequest
 *                            L2IWF_LLDP_APPL_PORT_REGISTER - To register
 *                            L2IWF_LLDP_APPL_PORT_UPDATE - To update 
 *                            L2IWF_LLDP_APPL_PORT_DEREGISTER - To de-register
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpApiHandleApplPortRequest (UINT4 u4IfIndex,
                              tLldpAppPortMsg * pLldpAppPortMsg,
                              UINT1 u1ApplPortRequest)
{
    tLldpQMsg          *pMsg = NULL;

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC,
                  "LldpApiApplPortReg: LLDP is not started\r\n");
        return OSIX_FAILURE;
    }

    if ((u4IfIndex < LLDP_MIN_PORTS) ||
        (u4IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpApiApplPortReg "
                  "called for non-physical/LAG interface\r\n");
        return OSIX_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tLldpQMsg *) MemAllocMemBlk (gLldpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        LLDP_TRC_ARG1 (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                       "LldpApiApplPortReg: Port %d :"
                       " Appln Reg. Msg ALLOC_MEM_BLOCK FAILED\r\n", u4IfIndex);
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpApiApplPortReg: Port %d : Appln Reg."
                      "Msg ALLOC_MEM_BLOCK FAILED", u4IfIndex));
#endif

        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        LLDP_ERR_CNTR_INPUT_Q_OVERFLOW++;
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tLldpQMsg));

    switch (u1ApplPortRequest)
    {
        case L2IWF_LLDP_APPL_PORT_REGISTER:
        case L2IWF_LLDP_APPL_PORT_UPDATE:

            /* Ensure a valid TLV is present in the message */
            if (pLldpAppPortMsg->u2TxAppTlvLen != 0)
            {
                if ((pMsg->AppPortMsg.pu1TxAppTlv = (UINT1 *)
                     MemAllocMemBlk (gLldpGlobalInfo.AppTlvPoolId)) == NULL)
                {
                    MemReleaseMemBlock (gLldpGlobalInfo.QMsgPoolId,
                                        (UINT1 *) pMsg);
                    LLDP_TRC_ARG1 (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                                   "LldpApiApplPortReg: Port %d :"
                                   "Appln TLV Msg ALLOC_MEM_BLOCK FAILED\r\n",
                                   u4IfIndex);
#ifndef FM_WANTED
                    SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL,
                                  gLldpGlobalInfo.u4SysLogId,
                                  "LldpApiApplPortReg: Port %d : Appln TLV "
                                  "Msg ALLOC_MEM_BLOCK FAILED", u4IfIndex));
#endif

                    LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
                    LLDP_ERR_CNTR_INPUT_Q_OVERFLOW++;
                    return OSIX_FAILURE;
                }

                MEMSET (pMsg->AppPortMsg.pu1TxAppTlv, 0, LLDP_MAX_APP_TLV_LEN);

                pMsg->AppPortMsg.u2TxAppTlvLen = pLldpAppPortMsg->u2TxAppTlvLen;
                MEMCPY (pMsg->AppPortMsg.pu1TxAppTlv,
                        pLldpAppPortMsg->pu1TxAppTlv,
                        pMsg->AppPortMsg.u2TxAppTlvLen);
            }
            else
            {
                pMsg->AppPortMsg.pu1TxAppTlv = NULL;
            }

            pMsg->u4MsgType =
                (u1ApplPortRequest == L2IWF_LLDP_APPL_PORT_REGISTER) ?
                LLDP_APPL_PORT_REGISTER : LLDP_APPL_PORT_UPDATES;

            break;

        case L2IWF_LLDP_APPL_PORT_DEREGISTER:

            pMsg->u4MsgType = LLDP_APPL_PORT_DEREGISTER;

            break;
        default:
            MemReleaseMemBlock (gLldpGlobalInfo.QMsgPoolId, (UINT1 *) pMsg);

            LLDP_TRC_ARG1 (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                           "LldpApiHandleApplPortRequest: Port %d :"
                           "Invalid application port request received \r\n",
                           u4IfIndex);
            return OSIX_FAILURE;
    }

    pMsg->u4Port = u4IfIndex;

    MEMCPY (&pMsg->AppPortMsg.LldpAppId, &pLldpAppPortMsg->LldpAppId,
            sizeof (tLldpAppId));

    pMsg->AppPortMsg.pAppCallBackFn = pLldpAppPortMsg->pAppCallBackFn;
    if (pLldpAppPortMsg->u4LldpInstSelector == 0)
    {
        pMsg->AppPortMsg.u4LldpInstSelector = DEFAULT_DEST_MAC_ADDR_INDEX;
    }
    else
    {
        pMsg->AppPortMsg.u4LldpInstSelector =
            pLldpAppPortMsg->u4LldpInstSelector;
    }
    MEMCPY (pMsg->AppPortMsg.au1LldpApplSpecMac,
            pLldpAppPortMsg->au1LldpApplSpecMac, MAC_ADDR_LEN);
    pMsg->AppPortMsg.bAppTlvTxStatus = pLldpAppPortMsg->bAppTlvTxStatus;
    pMsg->AppPortMsg.u1AppTlvRxStatus = pLldpAppPortMsg->u1AppTlvRxStatus;
    pMsg->AppPortMsg.u1AppPDURecv = pLldpAppPortMsg->u1AppPDURecv;
    pMsg->AppPortMsg.u1AppNeighborExistenceNotify =
        pLldpAppPortMsg->u1AppNeighborExistenceNotify;
    pMsg->AppPortMsg.u1AppRefreshFrameNotify =
        pLldpAppPortMsg->u1AppRefreshFrameNotify;
    pMsg->AppPortMsg.u1TakeTlvFrmApp = pLldpAppPortMsg->u1TakeTlvFrmApp;
    pMsg->AppPortMsg.pAppCallBackTakeTlvFrmAppFn =
        pLldpAppPortMsg->pAppCallBackTakeTlvFrmAppFn;

    return (LldpQueEnqAppMsg (pMsg));
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiUpdateDigestInput
 *
 *    DESCRIPTION      : Function to update the VID digest in lldp
 *
 *    INPUT            : u4IfIndex - Port on which the application is to 
 *                                   be registered to update the VID digest
 *    OUTPUT           : pu1VidDigestInput - Digest to be updated
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpApiUpdateDigestInput (INT4 i4IfIndex, tMacAddr * pAgentMacAddr,
                          UINT4 u4CRC32, UINT1 *pu1VidDigestInput)
{
    tLldpv2AgentToLocPort LldpAgentPortInfo;
    tLldpLocPortInfo    LldpLocPortInfo;
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;
    UINT4               u4Len = LLDP_MAX_VID_DIGEST_LEN /* 128 */ ;

    MEMSET (&LldpAgentPortInfo, 0, sizeof (tLldpv2AgentToLocPort));
    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));

    if ((pu1VidDigestInput == NULL) && (u4CRC32 == 0))
    {
        return OSIX_FAILURE;
    }

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }

    if (u4CRC32 == 0)
    {
        /* If CRC is zero, then calculate application is interested in calculation */
        u4CRC32 = UtilCalculateCRC32 (pu1VidDigestInput, u4Len);
    }

    if (MEMCMP (pAgentMacAddr, gau1LldpMcastAddr, sizeof (tMacAddr)) == 0)
    {
        MEMCPY (pAgentMacAddr, gau1LldpMcastAddr, sizeof (tMacAddr));
    }

    /* With IfIndex and Destination MAC address, get the LLDP Agent  */
    MEMSET (&LldpAgentPortInfo, 0, sizeof (tLldpv2AgentToLocPort));
    LldpAgentPortInfo.i4IfIndex = i4IfIndex;
    MEMCPY (LldpAgentPortInfo.Lldpv2DestMacAddress,
            pAgentMacAddr, sizeof (tMacAddr));

    pLldpAgentPortInfo = (tLldpv2AgentToLocPort *) RBTreeGetNext
        (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
         (tRBElem *) & LldpAgentPortInfo, LldpAgentToLocPortUtlRBCmpInfo);

    if (pLldpAgentPortInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    /* With LLDP Agent number, get the Agent node */
    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));

    LldpLocPortInfo.u4LocPortNum = pLldpAgentPortInfo->u4LldpLocPort;

    pLldpLocPortInfo = (tLldpLocPortInfo *)
        RBTreeGet (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
                   &LldpLocPortInfo);

    if (pLldpLocPortInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    pLldpLocPortInfo->u4LocVidUsageDigest = u4CRC32;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiGetInstanceId
 *
 *    DESCRIPTION      : Function that returns the Agent Id given a destination
 *                       Mac address
 *
 *    INPUT            : pu1Mac - Destination mac address index to be used in
 *                       LLDP PDU that transmits the appilication TLV.
 *    OUTPUT           : pu4InstanceId - Destination mac address index 
 *                       corresponding to the mac address.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpApiGetInstanceId (UINT1 *pu1Mac, UINT4 *pu4InstanceId)
{
    tLldpv2DestAddrTbl *pDestAddrTbl = NULL;
    tLldpv2DestAddrTbl  DestAddrTbl;

    MEMSET (&DestAddrTbl, 0, sizeof (tLldpv2DestAddrTbl));

    if (pu1Mac != NULL)
    {
        pDestAddrTbl = (tLldpv2DestAddrTbl *)
            RBTreeGetFirst (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree);
        while (pDestAddrTbl != NULL)
        {
            if (MEMCMP
                (pDestAddrTbl->Lldpv2DestMacAddress, pu1Mac, MAC_ADDR_LEN) == 0)
            {
                *pu4InstanceId = pDestAddrTbl->u4LlldpV2DestAddrTblIndex;
                return OSIX_SUCCESS;
            }
            DestAddrTbl.u4LlldpV2DestAddrTblIndex =
                pDestAddrTbl->u4LlldpV2DestAddrTblIndex;
            pDestAddrTbl =
                (tLldpv2DestAddrTbl *) RBTreeGetNext (gLldpGlobalInfo.
                                                      Lldpv2DestMacAddrTblRBTree,
                                                      &DestAddrTbl,
                                                      LldpDstMacAddrUtlRBCmpInfo);
        }
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiSetDstMac
 *
 *    DESCRIPTION      : API to set the destination mac address
 *
 *    INPUT            : pu1MacAddr - Destination mac address index to be 
 *                       set for the LLDP agent
 *                       i4IfIndex - Interface Index on which the LLDP agent
 *                       should be set
 *                       u1RowStatus - specifies if the agent is created/deleted
 *
 *    OUTPUT           : sets the LLDP agent with i4IfIndex and pu1MacAddr
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpApiSetDstMac (INT4 i4IfIndex, UINT1 *pu1MacAddr, UINT1 u1RowStatus)
{
    tLldpQMsg          *pMsg = NULL;

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC,
                  "LldpApiSetDstMac: LLDP is not started\r\n");
        return OSIX_FAILURE;
    }

    if ((i4IfIndex < LLDP_MIN_PORTS) ||
        (i4IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpApiSetDstMac "
                  "called for non-physical/LAG interface\r\n");
        return OSIX_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tLldpQMsg *) MemAllocMemBlk (gLldpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        LLDP_TRC_ARG1 (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                       "LldpApiSetDstMac: Port %d :"
                       " Appln Reg. Msg ALLOC_MEM_BLOCK FAILED\r\n", i4IfIndex);
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpApiSetDstMac: Port %d : Appln Reg."
                      "Msg ALLOC_MEM_BLOCK FAILED", i4IfIndex));
#endif

        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        LLDP_ERR_CNTR_INPUT_Q_OVERFLOW++;
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tLldpQMsg));

    pMsg->u4Port = (UINT4) i4IfIndex;
    pMsg->u4MsgType = LLDP_SET_DST_MAC;
    pMsg->Agent.i4IfIndex = i4IfIndex;
    MEMCPY (pMsg->Agent.MacAddr, pu1MacAddr, MAC_ADDR_LEN);
    pMsg->Agent.u1RowStatus = u1RowStatus;
    return (LldpQueEnqAppMsg (pMsg));

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LLdpApiUpdateLLdpStatusOnIccl
 *
 *    DESCRIPTION      : API to enable or disable LLDP on ICCL ports
 *
 *    INPUT            : u2IfIndex - Interface index of port on which LLDP   
 *                                   has to be disabled or enabled            
 *                       u1AggCap  - Aggregation Capability                   
 *                                   (TRUE indicates capable of being         
 *                                   aggregated and FALSE indicates  not      
 *                                   capable of being aggregated)             
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LLdpApiUpdateLLdpStatusOnIccl (UINT4 u4IfIndex, UINT1 u1AggCap)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    UINT4               u4DestMacAddrIndex = 0;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));

    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
    {
        u4DestMacAddrIndex = LLDP_V1_DEST_MAC_ADDR_INDEX ((INT4) u4IfIndex);
    }
    else
    {
        u4DestMacAddrIndex = DEFAULT_DEST_MAC_ADDR_INDEX;
    }

    DestTbl.u4LlldpV2DestAddrTblIndex = u4DestMacAddrIndex;

    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);
    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return OSIX_FAILURE;
    }

    if (u1AggCap == LA_AGG_CAPABLE)
    {
        LldpUtlSetLldpPortConfigAdminStatus ((INT4) u4IfIndex,
                                             pTempDestTbl->Lldpv2DestMacAddress,
                                             LLDP_ADM_STAT_DISABLED);
    }
    else if (u1AggCap == LA_AGG_NOT_CAPABLE)
    {
        LldpUtlSetLldpPortConfigAdminStatus ((INT4) u4IfIndex,
                                             pTempDestTbl->Lldpv2DestMacAddress,
                                             LLDP_ADM_STAT_TX_AND_RX);
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LldpUpdateMtu                                        */
/*                                                                           */
/* Description        : When the MTU of a port or port channel is changed,   */
/*                      notification is given to LLDP and MTU is updated in  */
/*                      LLDP database                                        */
/*                                                                           */
/* Input(s)           : u4IfIndex - Index of the physical port               */
/*                      u4Mtu     -  The MTU of the port                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
LldpUpdateMtu (UINT4 u4IfIndex, UINT4 u4Mtu)
{
    tLldpLocPortInfo   *pLocPortEntry = NULL;
    tCfaIfInfo          CfaIfInfo;
    tLldpv2AgentToLocPort Lldpv2AgentToLocPort;
    tLldpv2AgentToLocPort *pLldpv2AgentToLocPort = NULL;
    UINT2               au2ConfPorts[LA_MAX_PORTS_PER_AGG];
    UINT2               u2NumPorts = 0;
    UINT2               u2Index = 0;
    UINT2               u2IfIndex = 0;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    MEMSET (au2ConfPorts, 0, (LA_MAX_PORTS_PER_AGG * sizeof (UINT2)));

    MEMSET (&Lldpv2AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));

    LldpLock ();

    if (LldpPortGetIfInfo (u4IfIndex, &CfaIfInfo) == OSIX_SUCCESS)
    {
        if (CfaIfInfo.u1IfType == CFA_LAGG)
        {
            u2IfIndex = (UINT2) u4IfIndex;
            if (L2IwfGetConfiguredPortsForPortChannel (u2IfIndex,
                                                       au2ConfPorts,
                                                       &u2NumPorts) !=
                L2IWF_SUCCESS)
            {
                LLDP_TRC (ALL_FAILURE_TRC,
                          "Port channel has no member ports\r\n");
                LldpUnLock ();
                return;
            }

            for (u2Index = 0; u2Index < u2NumPorts; u2Index++)
            {
                Lldpv2AgentToLocPort.i4IfIndex = (INT4) au2ConfPorts[u2Index];

                pLldpv2AgentToLocPort = (tLldpv2AgentToLocPort *)
                    RBTreeGetNext (gLldpGlobalInfo.
                                   Lldpv2AgentToLocPortMapTblRBTree,
                                   (tRBElem *) & Lldpv2AgentToLocPort, NULL);

                while ((pLldpv2AgentToLocPort != NULL) &&
                       (pLldpv2AgentToLocPort->i4IfIndex ==
                        (INT4) au2ConfPorts[u2Index]))
                {
                    pLocPortEntry =
                        LLDP_GET_LOC_PORT_INFO (pLldpv2AgentToLocPort->
                                                u4LldpLocPort);

                    if (pLocPortEntry == NULL)
                    {
                        pLldpv2AgentToLocPort = (tLldpv2AgentToLocPort *)
                            RBTreeGetNext (gLldpGlobalInfo.
                                           Lldpv2AgentToLocPortMapTblRBTree,
                                           (tRBElem *) pLldpv2AgentToLocPort,
                                           NULL);
                        continue;
                    }

                    /* Update the MTU if there is a change in MTU */
                    if (pLocPortEntry->Dot3LocPortInfo.u2MaxFrameSize != u4Mtu)
                    {
                        pLocPortEntry->Dot3LocPortInfo.u2MaxFrameSize =
                            (UINT2) u4Mtu;
                        pLocPortEntry->bSomethingChangedLocal = OSIX_TRUE;
                    }
                    pLocPortEntry = NULL;
                    pLldpv2AgentToLocPort = (tLldpv2AgentToLocPort *)
                        RBTreeGetNext (gLldpGlobalInfo.
                                       Lldpv2AgentToLocPortMapTblRBTree,
                                       (tRBElem *) pLldpv2AgentToLocPort, NULL);
                }
            }
        }
        else if (CfaIfInfo.u1IfType == CFA_ENET)
        {
            Lldpv2AgentToLocPort.i4IfIndex = (INT4) u4IfIndex;

            pLldpv2AgentToLocPort = (tLldpv2AgentToLocPort *)
                RBTreeGetNext (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                               (tRBElem *) & Lldpv2AgentToLocPort, NULL);

            while ((pLldpv2AgentToLocPort != NULL) &&
                   (pLldpv2AgentToLocPort->i4IfIndex == (INT4) u4IfIndex))
            {
                pLocPortEntry =
                    LLDP_GET_LOC_PORT_INFO (pLldpv2AgentToLocPort->
                                            u4LldpLocPort);

                if (pLocPortEntry == NULL)
                {
                    LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                                   "Port entry is NULL for port : %d. "
                                   "So, MTU is not updated in LLDP database\r\n",
                                   u4IfIndex);
                    pLocPortEntry = NULL;
                    pLldpv2AgentToLocPort = (tLldpv2AgentToLocPort *)
                        RBTreeGetNext (gLldpGlobalInfo.
                                       Lldpv2AgentToLocPortMapTblRBTree,
                                       (tRBElem *) pLldpv2AgentToLocPort, NULL);
                    continue;
                }

                /* Update the MTU if there is a change in MTU */
                if (pLocPortEntry->Dot3LocPortInfo.u2MaxFrameSize != u4Mtu)
                {
                    pLocPortEntry->Dot3LocPortInfo.u2MaxFrameSize =
                        (UINT2) u4Mtu;
                    pLocPortEntry->bSomethingChangedLocal = OSIX_TRUE;
                }
                pLocPortEntry = NULL;
                pLldpv2AgentToLocPort = (tLldpv2AgentToLocPort *)
                    RBTreeGetNext (gLldpGlobalInfo.
                                   Lldpv2AgentToLocPortMapTblRBTree,
                                   (tRBElem *) pLldpv2AgentToLocPort, NULL);
            }
        }
    }
    LldpUnLock ();
    return;
}

/*****************************************************************************/
/* Function Name       : LldpUpdateNegStatus                                 */
/*                                                                           */
/* Description        : When the Negotiation status of Port is  changed,     */
/*                      notification is given to LLDP and Status is updated  */
/*                      in LLDP database                                     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Index of the physical port               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
LldpUpdateNegStatus (UINT4 u4IfIndex)
{
    tLldpLocPortInfo   *pLocPortEntry = NULL;
    tCfaIfInfo          CfaIfInfo;
    tLldpv2AgentToLocPort Lldpv2AgentToLocPort;
    tLldpv2AgentToLocPort *pLldpv2AgentToLocPort = NULL;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    MEMSET (&Lldpv2AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));

    LldpLock ();

    if (LldpPortGetIfInfo (u4IfIndex, &CfaIfInfo) == OSIX_SUCCESS)
    {
        Lldpv2AgentToLocPort.i4IfIndex = (INT4) u4IfIndex;

        pLldpv2AgentToLocPort = (tLldpv2AgentToLocPort *)
            RBTreeGetNext (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                           (tRBElem *) & Lldpv2AgentToLocPort, NULL);

        while ((pLldpv2AgentToLocPort != NULL) &&
               (pLldpv2AgentToLocPort->i4IfIndex == (INT4) u4IfIndex))
        {
            pLocPortEntry =
                LLDP_GET_LOC_PORT_INFO (pLldpv2AgentToLocPort->u4LldpLocPort);

            if (pLocPortEntry == NULL)
            {
                pLldpv2AgentToLocPort = (tLldpv2AgentToLocPort *)
                    RBTreeGetNext (gLldpGlobalInfo.
                                   Lldpv2AgentToLocPortMapTblRBTree,
                                   (tRBElem *) pLldpv2AgentToLocPort, NULL);
                continue;
            }

            /* Update the MTU if there is a change in MTU */
            if (pLocPortEntry->Dot3LocPortInfo.Dot3AutoNegInfo.
                u1AutoNegEnabled != CfaIfInfo.u1AutoNegStatus)
            {
                pLocPortEntry->Dot3LocPortInfo.Dot3AutoNegInfo.
                    u1AutoNegEnabled = CfaIfInfo.u1AutoNegStatus;
                pLocPortEntry->bSomethingChangedLocal = OSIX_TRUE;
            }
            pLocPortEntry = NULL;
            pLldpv2AgentToLocPort = (tLldpv2AgentToLocPort *)
                RBTreeGetNext (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                               (tRBElem *) pLldpv2AgentToLocPort, NULL);
        }
    }
    LldpUnLock ();
    return;

}

/****************************************************************************
 * Function Name : LldpApiNotifyPortDesc
 *
 * Description : This function posts a message to LLDP task's queue
 *          to notify the change in the Port Description
 *
 * Input : pu1PortDesc - Port Description passed through this pointer
 *         
 *         u4IfIndex - Interface Index for which port Description to be
 *                    configured
 *
 * Output : None
 *
 * Returns : OSIX_SUCCESS/OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
LldpApiNotifyPortDesc (UINT1 *pu1PortDesc, UINT4 u4IfIndex)
{
    tLldpQMsg          *pMsg = NULL;

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "LldpApiNotifyPortDesc: LLDP is not started\r\n");
        return OSIX_FAILURE;
    }
    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC, "LldpApiNotifyPortDesc : Recvd PortDesc "
                   "Chang Notification for Port Desc=%s\r\n", pu1PortDesc);

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tLldpQMsg *) MemAllocMemBlk (gLldpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpApiNotifyPortDesc: Mem Allocation Failed for QMsgPoolId"
                  " while Notifying Port Desc change\r\n");
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpApiNotifyPortDesc : Mem Allocation Failed for QMsgPoolId"
                      "while Notifying Port Desc change"));
#endif

        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        LLDP_ERR_CNTR_INPUT_Q_OVERFLOW++;
        return OSIX_FAILURE;
    }
    MEMSET (pMsg, 0, sizeof (tLldpQMsg));

    pMsg->u4MsgType = LLDP_PORT_DESC_CHG_MSG;
    STRNCPY (pMsg->PortDescConfig, pu1PortDesc, LLDP_MAX_LEN_PORTDESC);
    pMsg->u4IfIndex = u4IfIndex;
    if (LldpQueEnqAppMsg (pMsg) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.QMsgPoolId, (UINT1 *) pMsg);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LldpApiUpdateSvidOnUap                               */
/*                                                                           */
/* Description        : This function updates/deletes the SVID in the SVID   */
/*                      array mainted for each port in the LocPortTable      */
/*                      based on the type of incoming request                */
/*                                                                           */
/* Input(s)           : u4IfIndex - Index of the physical port               */
/*                      u4Svid   - SVID created/deleted on the physical port */
/*                      u4Request - Updation/Deletion Request to the SVID    */
/*                                  Array mainted in the LocPortTable        */
/*                                                                           */
/* Output(s)          : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
INT4
LldpApiUpdateSvidOnUap (UINT4 u4IfIndex, UINT4 u4Svid, UINT4 u4Request)
{

    tLldpQMsg          *pMsg = NULL;

    if ((pMsg = (tLldpQMsg *) MemAllocMemBlk (gLldpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpApiUpdateSvidOnUap: Mem Allocation Failed for"
                  "LldpUpdateSvidOnUapPoolId while Notifying System Desc"
                  "change\r\n");
        return OSIX_FAILURE;
    }
    pMsg->UpdSvidOnUap.u4IfIndex = u4IfIndex;
    pMsg->UpdSvidOnUap.u4Svid = u4Svid;

    if (u4Request == VLAN_EVB_SVID_UPDATE)
    {
        pMsg->u4MsgType = LLDP_SVID_ADD;
    }
    else
    {
        pMsg->u4MsgType = LLDP_SVID_DELETE;
    }

    if (OsixQueSend (gLldpGlobalInfo.MsgQId, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.QMsgPoolId, (UINT1 *) pMsg);
        LLDP_TRC (OS_RESOURCE_TRC, "LldpApiUpdateSvidOnUap"
                  "Osix Queue send Failed!!!\r\n");
        return OSIX_FAILURE;
    }
    KW_FALSEPOSITIVE_FIX (pMsg);

    /* post the event to the LLDP module */
    OsixEvtSend (gLldpGlobalInfo.TaskId, LLDP_QMSG_EVENT);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpApiIsMultipleAgentsOnInterface
 *
 *    DESCRIPTION      : This function is to check if multiple agents are
 *                       configured on a particular interface.
 *
 *    INPUT            : u4IfIndex - Interface index
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_TRUE/OSIX_FALSE
 ****************************************************************************/

PUBLIC              BOOL1
LldpApiIsMultipleAgentsOnInterface (INT4 i4IfIndex)
{
    UINT4               u4DestMacIndex = 0;
    UINT4               u4Count = 0;
    INT4                i4Index = i4IfIndex;
    INT4                i4PrevIfIndex = 0;
    UINT4               u4PrevDestMacIndex = 0;
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
    {
        u4DestMacIndex = LLDP_V1_DEST_MAC_ADDR_INDEX (i4IfIndex);
    }
    else
    {
        u4DestMacIndex = DEFAULT_DEST_MAC_ADDR_INDEX;
    }

    if (nmhValidateIndexInstanceLldpV2PortConfigTable
        (i4IfIndex, u4DestMacIndex) == SNMP_FAILURE)
    {
        u4DestMacIndex = LLDP_V1_DEST_MAC_ADDR_INDEX (i4IfIndex);
    }
    do
    {
        if (i4IfIndex == i4Index)
        {
            u4Count++;
        }
        else
        {
            break;
        }
        i4PrevIfIndex = i4IfIndex;
        u4PrevDestMacIndex = u4DestMacIndex;
    }
    while (nmhGetNextIndexLldpV2PortConfigTable
           (i4PrevIfIndex, &i4IfIndex, u4PrevDestMacIndex,
            &u4DestMacIndex) == SNMP_SUCCESS);
    if (u4Count > 1)
    {
        return OSIX_TRUE;
    }
    else
    {
        return OSIX_FALSE;
    }
}

#endif /* _LLDPAPI_C_ */
/*---------------------------------------------------------------------------*/
/*                        End of the file  <lldapi.c>                        */
/*---------------------------------------------------------------------------*/
