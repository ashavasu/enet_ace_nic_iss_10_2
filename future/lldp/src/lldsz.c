/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lldsz.c,v 1.3 2013/11/29 11:04:14 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _LLDPSZ_C
#include "lldinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
LldpSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < LLDP_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsLLDPSizingParams[i4SizingId].u4StructSize,
                              FsLLDPSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(LLDPMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            LldpSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
LldpSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsLLDPSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, LLDPMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
LldpSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < LLDP_MAX_SIZING_ID; i4SizingId++)
    {
        if (LLDPMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (LLDPMemPoolIds[i4SizingId]);
            LLDPMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
