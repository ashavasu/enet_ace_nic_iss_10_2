/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fslldmwr.c,v 1.2 2016/01/28 11:09:43 siva Exp $
*
* Description: LLDP-MED Wrapper Functions
*********************************************************************/

# include  "lldinc.h"
# include  "fslldmdb.h"

INT4 GetNextIndexFsLldpMedPortConfigTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsLldpMedPortConfigTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsLldpMedPortConfigTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pFirstMultiIndex->pIndex[1].u4_ULongValue,
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}


VOID RegisterFSLLDM ()
{
	SNMPRegisterMibWithLock (&fslldmOID, &fslldmEntry, LldpLock, LldpUnLock, SNMP_MSR_TGR_TRUE);
	SNMPAddSysorEntry (&fslldmOID, (const UINT1 *) "fslldpmed");
}



VOID UnRegisterFSLLDM ()
{
	SNMPUnRegisterMib (&fslldmOID, &fslldmEntry);
	SNMPDelSysorEntry (&fslldmOID, (const UINT1 *) "fslldpmed");
}

INT4 FsLldpMedPortCapSupportedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedPortConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedPortCapSupported(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsLldpMedPortConfigTLVsTxEnableGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedPortConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedPortConfigTLVsTxEnable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsLldpMedPortConfigTLVsTxEnableSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsLldpMedPortConfigTLVsTxEnable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->pOctetStrValue));

}

INT4 FsLldpMedPortConfigTLVsTxEnableTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsLldpMedPortConfigTLVsTxEnable(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->pOctetStrValue));

}

INT4 FsLldpMedPortConfigTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsLldpMedPortConfigTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsLldpMedLocMediaPolicyTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsLldpMedLocMediaPolicyTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsLldpMedLocMediaPolicyTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pFirstMultiIndex->pIndex[1].pOctetStrValue,
			pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsLldpMedLocMediaPolicyVlanIDGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedLocMediaPolicyTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedLocMediaPolicyVlanID(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsLldpMedLocMediaPolicyPriorityGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedLocMediaPolicyTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedLocMediaPolicyPriority(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsLldpMedLocMediaPolicyDscpGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedLocMediaPolicyTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedLocMediaPolicyDscp(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsLldpMedLocMediaPolicyUnknownGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedLocMediaPolicyTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedLocMediaPolicyUnknown(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsLldpMedLocMediaPolicyTaggedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedLocMediaPolicyTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedLocMediaPolicyTagged(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsLldpMedLocMediaPolicyRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedLocMediaPolicyTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedLocMediaPolicyRowStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsLldpMedLocMediaPolicyVlanIDSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsLldpMedLocMediaPolicyVlanID(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsLldpMedLocMediaPolicyPrioritySet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsLldpMedLocMediaPolicyPriority(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsLldpMedLocMediaPolicyDscpSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsLldpMedLocMediaPolicyDscp(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsLldpMedLocMediaPolicyUnknownSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsLldpMedLocMediaPolicyUnknown(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsLldpMedLocMediaPolicyTaggedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsLldpMedLocMediaPolicyTagged(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsLldpMedLocMediaPolicyRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsLldpMedLocMediaPolicyRowStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsLldpMedLocMediaPolicyVlanIDTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsLldpMedLocMediaPolicyVlanID(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsLldpMedLocMediaPolicyPriorityTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsLldpMedLocMediaPolicyPriority(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsLldpMedLocMediaPolicyDscpTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsLldpMedLocMediaPolicyDscp(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsLldpMedLocMediaPolicyUnknownTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsLldpMedLocMediaPolicyUnknown(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsLldpMedLocMediaPolicyTaggedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsLldpMedLocMediaPolicyTagged(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsLldpMedLocMediaPolicyRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsLldpMedLocMediaPolicyRowStatus(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsLldpMedLocMediaPolicyTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsLldpMedLocMediaPolicyTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsLldpMedLocLocationTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsLldpMedLocLocationTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsLldpMedLocLocationTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsLldpMedLocLocationRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedLocLocationTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedLocLocationRowStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsLldpMedLocLocationRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsLldpMedLocLocationRowStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsLldpMedLocLocationRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsLldpMedLocLocationRowStatus(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsLldpMedLocLocationTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsLldpMedLocLocationTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsLldpXMedRemCapabilitiesTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsLldpXMedRemCapabilitiesTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].u4_ULongValue),
			&(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsLldpXMedRemCapabilitiesTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].u4_ULongValue,
			&(pNextMultiIndex->pIndex[2].u4_ULongValue),
			pFirstMultiIndex->pIndex[3].i4_SLongValue,
			&(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsLldpXMedRemCapSupportedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpXMedRemCapabilitiesTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpXMedRemCapSupported(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsLldpXMedRemCapCurrentGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpXMedRemCapabilitiesTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpXMedRemCapCurrent(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsLldpXMedRemDeviceClassGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpXMedRemCapabilitiesTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpXMedRemDeviceClass(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}

INT4 GetNextIndexFsLldpXMedRemMediaPolicyTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsLldpXMedRemMediaPolicyTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].u4_ULongValue),
			&(pNextMultiIndex->pIndex[3].i4_SLongValue),
			pNextMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsLldpXMedRemMediaPolicyTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].u4_ULongValue,
			&(pNextMultiIndex->pIndex[2].u4_ULongValue),
			pFirstMultiIndex->pIndex[3].i4_SLongValue,
			&(pNextMultiIndex->pIndex[3].i4_SLongValue),
			pFirstMultiIndex->pIndex[4].pOctetStrValue,
			pNextMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsLldpXMedRemMediaPolicyVlanIDGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpXMedRemMediaPolicyTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpXMedRemMediaPolicyVlanID(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsLldpXMedRemMediaPolicyPriorityGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpXMedRemMediaPolicyTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpXMedRemMediaPolicyPriority(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsLldpXMedRemMediaPolicyDscpGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpXMedRemMediaPolicyTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpXMedRemMediaPolicyDscp(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsLldpXMedRemMediaPolicyUnknownGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpXMedRemMediaPolicyTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpXMedRemMediaPolicyUnknown(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsLldpXMedRemMediaPolicyTaggedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpXMedRemMediaPolicyTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpXMedRemMediaPolicyTagged(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}

INT4 GetNextIndexFsLldpXMedRemInventoryTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsLldpXMedRemInventoryTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].u4_ULongValue),
			&(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsLldpXMedRemInventoryTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].u4_ULongValue,
			&(pNextMultiIndex->pIndex[2].u4_ULongValue),
			pFirstMultiIndex->pIndex[3].i4_SLongValue,
			&(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsLldpXMedRemHardwareRevGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpXMedRemInventoryTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpXMedRemHardwareRev(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsLldpXMedRemFirmwareRevGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpXMedRemInventoryTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpXMedRemFirmwareRev(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsLldpXMedRemSoftwareRevGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpXMedRemInventoryTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpXMedRemSoftwareRev(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsLldpXMedRemSerialNumGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpXMedRemInventoryTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpXMedRemSerialNum(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsLldpXMedRemMfgNameGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpXMedRemInventoryTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpXMedRemMfgName(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsLldpXMedRemModelNameGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpXMedRemInventoryTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpXMedRemModelName(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsLldpXMedRemAssetIDGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpXMedRemInventoryTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpXMedRemAssetID(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 GetNextIndexFsLldpMedStatsTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsLldpMedStatsTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsLldpMedStatsTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pFirstMultiIndex->pIndex[1].u4_ULongValue,
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsLldpMedStatsTxFramesTotalGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedStatsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedStatsTxFramesTotal(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsLldpMedStatsRxFramesTotalGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedStatsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedStatsRxFramesTotal(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsLldpMedStatsRxFramesDiscardedTotalGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedStatsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedStatsRxFramesDiscardedTotal(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsLldpMedStatsRxTLVsDiscardedTotalGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedStatsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedStatsRxTLVsDiscardedTotal(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsLldpMedStatsRxCapTLVsDiscardedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedStatsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedStatsRxCapTLVsDiscarded(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsLldpMedStatsRxPolicyTLVsDiscardedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedStatsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedStatsRxPolicyTLVsDiscarded(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsLldpMedStatsRxInventoryTLVsDiscardedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedStatsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedStatsRxInventoryTLVsDiscarded(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsLldpMedStatsRxLocationTLVsDiscardedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedStatsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedStatsRxLocationTLVsDiscarded(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsLldpMedStatsRxExPowerMDITLVsDiscardedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedStatsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedStatsRxExPowerMDITLVsDiscarded(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsLldpMedStatsRxCapTLVsDiscardedReasonGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedStatsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedStatsRxCapTLVsDiscardedReason(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsLldpMedStatsRxPolicyDiscardedReasonGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedStatsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedStatsRxPolicyDiscardedReason(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsLldpMedStatsRxInventoryDiscardedReasonGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedStatsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedStatsRxInventoryDiscardedReason(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsLldpMedStatsRxLocationDiscardedReasonGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedStatsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedStatsRxLocationDiscardedReason(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsLldpMedStatsRxExPowerDiscardedReasonGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpMedStatsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpMedStatsRxExPowerDiscardedReason(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsLldpMedClearStatsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsLldpMedClearStats(&(pMultiData->i4_SLongValue)));
}
INT4 FsLldpMedClearStatsSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsLldpMedClearStats(pMultiData->i4_SLongValue));
}


INT4 FsLldpMedClearStatsTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsLldpMedClearStats(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsLldpMedClearStatsDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsLldpMedClearStats(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsLldpXMedRemLocationTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsLldpXMedRemLocationTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].u4_ULongValue),
			&(pNextMultiIndex->pIndex[3].i4_SLongValue),
			&(pNextMultiIndex->pIndex[4].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsLldpXMedRemLocationTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].u4_ULongValue,
			&(pNextMultiIndex->pIndex[2].u4_ULongValue),
			pFirstMultiIndex->pIndex[3].i4_SLongValue,
			&(pNextMultiIndex->pIndex[3].i4_SLongValue),
			pFirstMultiIndex->pIndex[4].i4_SLongValue,
			&(pNextMultiIndex->pIndex[4].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsLldpXMedRemLocationInfoGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpXMedRemLocationTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpXMedRemLocationInfo(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 GetNextIndexFsLldpXMedRemXPoEPDTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsLldpXMedRemXPoEPDTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].u4_ULongValue),
			&(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsLldpXMedRemXPoEPDTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].u4_ULongValue,
			&(pNextMultiIndex->pIndex[2].u4_ULongValue),
			pFirstMultiIndex->pIndex[3].i4_SLongValue,
			&(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsLldpXMedRemXPoEDeviceTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpXMedRemXPoEPDTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpXMedRemXPoEDeviceType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsLldpXMedRemXPoEPDPowerReqGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpXMedRemXPoEPDTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpXMedRemXPoEPDPowerReq(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsLldpXMedRemXPoEPDPowerSourceGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpXMedRemXPoEPDTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpXMedRemXPoEPDPowerSource(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsLldpXMedRemXPoEPDPowerPriorityGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsLldpXMedRemXPoEPDTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsLldpXMedRemXPoEPDPowerPriority(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
