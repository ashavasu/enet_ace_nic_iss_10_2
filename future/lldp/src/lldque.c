/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldque.c,v 1.68 2017/12/29 09:32:27 siva Exp $
 *
 * Description: This file contains procedures related to 
 *              - rocessing QMsgs
 *              - Enqing QMsg from external modules to LLDP task 
 *********************************************************************/
#ifndef _LLDPQUE_C_
#define _LLDPQUE_C_
#include "lldinc.h"

/* Macros used only in this file */
#define LLDP_APPL_PORT_REG_REQ      1
#define LLDP_APPL_PORT_UPDATE_REQ   2
#define LLDP_APPL_PORT_DEREG_REQ    3

UINT1               gau1LldPduQue[LLDP_MAX_PDU_SIZE] = { 0 };

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpQueRxPduMsgHandler 
 *                                                                          
 *    DESCRIPTION      : This function processes the RxPDU queue messages 
 *                       received by LLDP task. 
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
LldpQueRxPduMsgHandler (VOID)
{
    tLldpRxPduQMsg     *pQMsg = NULL;
    tLldpLocPortInfo   *pLocPortEntry = NULL;

    tLldpv2AgentToLocPort AgentMapping;
    tLldpv2AgentToLocPort *pAgentMapping = NULL;
    tMacAddr            DstAddr;
    tMacAddr            SrcAddr;
    UINT1               au1LldpMcastAddr[MAC_ADDR_LEN] =
        { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x0E };
    UINT1               au1LldpCustBrgMcastAddr[MAC_ADDR_LEN] =
        { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x00 };
    UINT1               au1LldpNonTPMRBrgiMcastAddr[MAC_ADDR_LEN] =
        { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x03 };

    MEMSET (&DstAddr, 0, sizeof (tMacAddr));
    MEMSET (&SrcAddr, 0, sizeof (tMacAddr));
    MEMSET (&AgentMapping, 0, sizeof (tLldpv2AgentToLocPort));
    /* Event received, dequeue messages for processing */
    while (OsixQueRecv (gLldpGlobalInfo.RxPduQId, (UINT1 *) &pQMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        /* when ISSU Miantenace Mode is in progress
         * drop all the received PDUs */
        if (IssuGetMaintModeOperation () == OSIX_TRUE)
        {
            LldpUtilPktFree (pQMsg->pLldpPdu);
            /* Release the RxPduQMsg buffer to pool */
            if (MemReleaseMemBlock
                (gLldpGlobalInfo.RxPduQPoolId, (UINT1 *) pQMsg) != MEM_SUCCESS)
            {
                LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "ISSU MM: LldpQueRxPduMsgHandler"
                          "Free MemBlock RxPdu FAILED!!! \r\n");
            }
            return;
        }

        if (CRU_BUF_Copy_FromBufChain
            (pQMsg->pLldpPdu, SrcAddr, LLDP_SRCADDR_OFFSET,
             MAC_ADDR_LEN) == CRU_FAILURE)
        {
            LLDP_TRC ((CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                      "LldpApiEnqIncomingFrame: "
                      "Fetching Source mac from incoming frame failed \r\n");
            /* Free the LLDPDU Buffer */
            LldpUtilPktFree (pQMsg->pLldpPdu);
            /* Release the RxPduQMsg buffer to pool */
            if (MemReleaseMemBlock
                (gLldpGlobalInfo.RxPduQPoolId, (UINT1 *) pQMsg) != MEM_SUCCESS)
            {
                LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "LldpQueRxPduMsgHandler: Free MemBlock RxPdu "
                          "FAILED!!!\r\n");
            }
            return;
        }
        if (CRU_BUF_Copy_FromBufChain
            (pQMsg->pLldpPdu, DstAddr, LLDP_DESTADDR_OFFSET,
             MAC_ADDR_LEN) == CRU_FAILURE)
        {
            LLDP_TRC ((CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                      "LldpApiEnqIncomingFrame: "
                      "Fetching destination mac from incoming frame failed \r\n");
            /* Free the LLDPDU Buffer */
            LldpUtilPktFree (pQMsg->pLldpPdu);
            /* Release the RxPduQMsg buffer to pool */
            if (MemReleaseMemBlock
                (gLldpGlobalInfo.RxPduQPoolId, (UINT1 *) pQMsg) != MEM_SUCCESS)
            {
                LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "LldpQueRxPduMsgHandler: Free MemBlock RxPdu "
                          "FAILED!!!\r\n");
            }
            return;
        }

        AgentMapping.i4IfIndex = pQMsg->u4Port;
        if ((MEMCMP (DstAddr, au1LldpMcastAddr, MAC_ADDR_LEN)) == 0)
        {
            MEMCPY (AgentMapping.Lldpv2DestMacAddress, au1LldpMcastAddr,
                    MAC_ADDR_LEN);
        }
        else if ((MEMCMP (DstAddr, au1LldpCustBrgMcastAddr, MAC_ADDR_LEN) == 0)
                 || (MEMCMP (DstAddr, au1LldpNonTPMRBrgiMcastAddr, MAC_ADDR_LEN)
                     == 0))
        {
            MEMCPY (AgentMapping.Lldpv2DestMacAddress, DstAddr, MAC_ADDR_LEN);
        }
        else if ((FS_UTIL_IS_MCAST_MAC (DstAddr) == OSIX_TRUE))
        {
            MEMCPY (AgentMapping.Lldpv2DestMacAddress, DstAddr, MAC_ADDR_LEN);
        }
        else
        {
            MEMCPY (AgentMapping.Lldpv2DestMacAddress, SrcAddr, MAC_ADDR_LEN);
        }
        pAgentMapping = (tLldpv2AgentToLocPort *)
            RBTreeGet (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                       &AgentMapping);
        if (pAgentMapping == NULL)
        {
            LLDP_TRC_ARG7 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                           "LldpQueRxPduMsgHandler: Agent to Local port mapping is "
                           "not present for IfIndex: %d and Destination MAC: %02x:%02x:%02x:%02x:%02x:%02x \r\n",
                           AgentMapping.i4IfIndex,
                           AgentMapping.Lldpv2DestMacAddress[0],
                           AgentMapping.Lldpv2DestMacAddress[1],
                           AgentMapping.Lldpv2DestMacAddress[2],
                           AgentMapping.Lldpv2DestMacAddress[3],
                           AgentMapping.Lldpv2DestMacAddress[4],
                           AgentMapping.Lldpv2DestMacAddress[5]);

            /* Free the LLDPDU Buffer */
            LldpUtilPktFree (pQMsg->pLldpPdu);
            /* Release the RxPduQMsg buffer to pool */
            if (MemReleaseMemBlock
                (gLldpGlobalInfo.RxPduQPoolId, (UINT1 *) pQMsg) != MEM_SUCCESS)
            {
                LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "LldpQueRxPduMsgHandler: Free MemBlock RxPdu "
                          "FAILED!!!\r\n");
            }
            return;
        }

        pLocPortEntry = LLDP_GET_LOC_PORT_INFO (pAgentMapping->u4LldpLocPort);
        if (pLocPortEntry == NULL)
        {
            LLDP_TRC_ARG7 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                           "LldpQueRxPduMsgHandler: Local port info is not present "
                           "for IfIndex: %d and Destination MAC: %02x:%02x:%02x:%02x:%02x:%02x \r\n",
                           AgentMapping.i4IfIndex,
                           AgentMapping.Lldpv2DestMacAddress[0],
                           AgentMapping.Lldpv2DestMacAddress[1],
                           AgentMapping.Lldpv2DestMacAddress[2],
                           AgentMapping.Lldpv2DestMacAddress[3],
                           AgentMapping.Lldpv2DestMacAddress[4],
                           AgentMapping.Lldpv2DestMacAddress[5]);

            /* Free the LLDPDU Buffer */
            LldpUtilPktFree (pQMsg->pLldpPdu);
        }
        else
        {
            LldpQueHandleIncomingFrame (pLocPortEntry, pQMsg->pLldpPdu);
            /* LLDPDU Buffer is freed inside the function
             * LldpQueHandleIncomingFrame */
        }

        /* Release the RxPduQMsg buffer to pool */
        if (MemReleaseMemBlock (gLldpGlobalInfo.RxPduQPoolId, (UINT1 *) pQMsg)
            != MEM_SUCCESS)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "LldpQueRxPduMsgHandler: Free MemBlock RxPdu "
                      "FAILED!!!\r\n");
            return;
        }
    }
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpQueAppMsgHandler 
 *                                                                          
 *    DESCRIPTION      : This function process the queue messages received 
 *                       by LLDP task. 
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
LldpQueAppMsgHandler (VOID)
{
    tLldpQMsg          *pQMsg = NULL;
    tLldpLocPortInfo   *pLocPortEntry = NULL;
    tLldpv2AgentToLocPort AgentToLocPort;
    tLldpv2AgentToLocPort *pAgentToLocPort = NULL;
    tLldpLocPortTable   LocPortTableEntry;
    tLldpLocPortTable  *pLocPortTableEntry = NULL;
    UINT4               u4SkipMemRelease = OSIX_FALSE;

    MEMSET (&AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));

    /* Event received, dequeue messages for processing */
    while (OsixQueRecv (gLldpGlobalInfo.MsgQId, (UINT1 *) &pQMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        switch (pQMsg->u4MsgType)
        {
            case LLDP_CREATE_PORT_MSG:
                /* In case of single Instance, msp port msg will not be
                 * received */
                LldpIfCreate (pQMsg->u4Port);
                L2_SYNC_GIVE_SEM ();
                break;

            case LLDP_DELETE_PORT_MSG:
                MEMSET (&LocPortTableEntry, 0, sizeof (tLldpLocPortTable));
                LocPortTableEntry.i4IfIndex = pQMsg->u4Port;
                pLocPortTableEntry = (tLldpLocPortTable *)
                    RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree,
                               &LocPortTableEntry);
                if (pLocPortTableEntry == NULL)
                {
                    break;
                }
                AgentToLocPort.i4IfIndex = pQMsg->u4Port;
                MEMSET (AgentToLocPort.Lldpv2DestMacAddress, 0,
                        sizeof (tMacAddr));
                pAgentToLocPort =
                    (tLldpv2AgentToLocPort *) RBTreeGetNext (gLldpGlobalInfo.
                                                             Lldpv2AgentToLocPortMapTblRBTree,
                                                             &AgentToLocPort,
                                                             LldpAgentToLocPortUtlRBCmpInfo);
                while ((pAgentToLocPort != NULL)
                       && (pAgentToLocPort->i4IfIndex ==
                           ((INT4) pQMsg->u4Port)))
                {
                    AgentToLocPort.i4IfIndex = pAgentToLocPort->i4IfIndex;
                    MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                            pAgentToLocPort->Lldpv2DestMacAddress,
                            MAC_ADDR_LEN);
                    pLocPortEntry =
                        LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);
                    if (pLocPortEntry != NULL)
                    {
                        LldpIfDelete (pLocPortEntry);
                    }
                    pAgentToLocPort = (tLldpv2AgentToLocPort *)
                        RBTreeGetNext (gLldpGlobalInfo.
                                       Lldpv2AgentToLocPortMapTblRBTree,
                                       &AgentToLocPort,
                                       LldpAgentToLocPortUtlRBCmpInfo);
                }
                RBTreeRem (gLldpGlobalInfo.LldpPortInfoRBTree,
                           pLocPortTableEntry);
                MemReleaseMemBlock
                    (gLldpGlobalInfo.LocPortTablePoolId,
                     (UINT1 *) pLocPortTableEntry);
                pLocPortTableEntry = NULL;
                break;

            case LLDP_MAP_PORT_MSG:
                /* In case of Multiple Instance, create port msg will not be 
                 * rcvd. */
                LldpIfCreate (pQMsg->u4Port);
                L2MI_SYNC_GIVE_SEM ();
                break;

            case LLDP_UNMAP_PORT_MSG:
                MEMSET (&LocPortTableEntry, 0, sizeof (tLldpLocPortTable));
                LocPortTableEntry.i4IfIndex = pQMsg->u4Port;
                pLocPortTableEntry = (tLldpLocPortTable *)
                    RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree,
                               &LocPortTableEntry);
                if (pLocPortTableEntry == NULL)    /*For Klocwork */
                {
                    L2MI_SYNC_GIVE_SEM ();
                    break;        /*Port entry not present */
                }
                /*remove the agent and mapping entries */
                AgentToLocPort.i4IfIndex = pQMsg->u4Port;
                MEMSET (AgentToLocPort.Lldpv2DestMacAddress, 0,
                        sizeof (tMacAddr));
                pAgentToLocPort =
                    (tLldpv2AgentToLocPort *) RBTreeGetNext (gLldpGlobalInfo.
                                                             Lldpv2AgentToLocPortMapTblRBTree,
                                                             &AgentToLocPort,
                                                             LldpAgentToLocPortUtlRBCmpInfo);
                while ((pAgentToLocPort != NULL)
                       && (pAgentToLocPort->i4IfIndex ==
                           ((INT4) pQMsg->u4Port)))
                {
                    AgentToLocPort.i4IfIndex = pAgentToLocPort->i4IfIndex;
                    MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                            pAgentToLocPort->Lldpv2DestMacAddress,
                            MAC_ADDR_LEN);
                    pLocPortEntry =
                        LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);
                    if (pLocPortEntry != NULL)
                    {
                        LldpIfDelete (pLocPortEntry);
                    }
                    pAgentToLocPort = (tLldpv2AgentToLocPort *)
                        RBTreeGetNext (gLldpGlobalInfo.
                                       Lldpv2AgentToLocPortMapTblRBTree,
                                       &AgentToLocPort,
                                       LldpAgentToLocPortUtlRBCmpInfo);
                }
                /* remove the port table entry */
                RBTreeRem (gLldpGlobalInfo.LldpPortInfoRBTree,
                           pLocPortTableEntry);
                MemReleaseMemBlock
                    (gLldpGlobalInfo.LocPortTablePoolId,
                     (UINT1 *) pLocPortTableEntry);
                pLocPortTableEntry = NULL;
                L2MI_SYNC_GIVE_SEM ();
                break;

            case LLDP_OPER_STATUS_CHG_MSG:
                AgentToLocPort.i4IfIndex = pQMsg->u4Port;
                MEMSET (AgentToLocPort.Lldpv2DestMacAddress, 0,
                        sizeof (tMacAddr));
                pAgentToLocPort =
                    (tLldpv2AgentToLocPort *) RBTreeGetNext (gLldpGlobalInfo.
                                                             Lldpv2AgentToLocPortMapTblRBTree,
                                                             &AgentToLocPort,
                                                             LldpAgentToLocPortUtlRBCmpInfo);
                while ((pAgentToLocPort != NULL)
                       && (pAgentToLocPort->i4IfIndex ==
                           ((INT4) pQMsg->u4Port)))
                {
                    AgentToLocPort.i4IfIndex = pAgentToLocPort->i4IfIndex;
                    MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                            pAgentToLocPort->Lldpv2DestMacAddress,
                            MAC_ADDR_LEN);
                    pLocPortEntry =
                        LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);
                    if (pLocPortEntry != NULL)
                    {
                        LldpIfOperChg (pLocPortEntry, pQMsg->OperStatus);
                    }
                    pAgentToLocPort = (tLldpv2AgentToLocPort *)
                        RBTreeGetNext (gLldpGlobalInfo.
                                       Lldpv2AgentToLocPortMapTblRBTree,
                                       &AgentToLocPort,
                                       LldpAgentToLocPortUtlRBCmpInfo);
                }
                break;

            case LLDP_PORT_IFALIAS_MSG:
                AgentToLocPort.i4IfIndex = pQMsg->u4Port;
                MEMSET (AgentToLocPort.Lldpv2DestMacAddress, 0,
                        sizeof (tMacAddr));
                pAgentToLocPort =
                    (tLldpv2AgentToLocPort *) RBTreeGetNext (gLldpGlobalInfo.
                                                             Lldpv2AgentToLocPortMapTblRBTree,
                                                             &AgentToLocPort,
                                                             LldpAgentToLocPortUtlRBCmpInfo);
                while ((pAgentToLocPort != NULL)
                       && (pAgentToLocPort->i4IfIndex ==
                           ((INT4) pQMsg->u4Port)))
                {
                    AgentToLocPort.i4IfIndex = pAgentToLocPort->i4IfIndex;
                    MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                            pAgentToLocPort->Lldpv2DestMacAddress,
                            MAC_ADDR_LEN);
                    pLocPortEntry =
                        LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);
                    if (pLocPortEntry != NULL)
                    {
                        LldpQueHandleIfAlias (pLocPortEntry, pQMsg->u4Port,
                                              pQMsg->IfAlias);
                    }
                    pAgentToLocPort = (tLldpv2AgentToLocPort *)
                        RBTreeGetNext (gLldpGlobalInfo.
                                       Lldpv2AgentToLocPortMapTblRBTree,
                                       &AgentToLocPort,
                                       LldpAgentToLocPortUtlRBCmpInfo);
                }
                break;

            case LLDP_PORT_AGENT_CKTID_MSG:
                AgentToLocPort.i4IfIndex = pQMsg->u4Port;
                MEMSET (AgentToLocPort.Lldpv2DestMacAddress, 0,
                        sizeof (tMacAddr));
                pAgentToLocPort =
                    (tLldpv2AgentToLocPort *) RBTreeGetNext (gLldpGlobalInfo.
                                                             Lldpv2AgentToLocPortMapTblRBTree,
                                                             &AgentToLocPort,
                                                             LldpAgentToLocPortUtlRBCmpInfo);
                while ((pAgentToLocPort != NULL)
                       && (pAgentToLocPort->i4IfIndex ==
                           ((INT4) pQMsg->u4Port)))
                {
                    AgentToLocPort.i4IfIndex = pAgentToLocPort->i4IfIndex;
                    MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                            pAgentToLocPort->Lldpv2DestMacAddress,
                            MAC_ADDR_LEN);
                    pLocPortEntry =
                        LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);
                    if (pLocPortEntry != NULL)
                    {
                        LldpQueHandleAgentCktId (pLocPortEntry,
                                                 pQMsg->AgentCircuitId);
                    }
                    pAgentToLocPort = (tLldpv2AgentToLocPort *)
                        RBTreeGetNext (gLldpGlobalInfo.
                                       Lldpv2AgentToLocPortMapTblRBTree,
                                       &AgentToLocPort,
                                       LldpAgentToLocPortUtlRBCmpInfo);
                }
                break;

            case LLDP_SYS_NAME_CHG_MSG:
                LldpQueHandleSysName (pQMsg->SystemName);
                break;

            case LLDP_SYS_DESC_CHG_MSG:
                LldpQueHandleSysDesc (pQMsg->SystemDesc);
                break;

            case LLDP_IP4_MAN_ADDR_CHG_MSG:
                LldpQueHandleIp4ManAddr (&(pQMsg->Ip4ManAddr));
                break;

            case LLDP_IP6_MAN_ADDR_CHG_MSG:
                LldpQueHandleIp6ManAddr (&(pQMsg->Ip6ManAddr));
                break;

            case LLDP_PORT_VLAN_ID_MSG:
                AgentToLocPort.i4IfIndex = pQMsg->u4Port;
                MEMSET (AgentToLocPort.Lldpv2DestMacAddress, 0,
                        sizeof (tMacAddr));
                pAgentToLocPort =
                    (tLldpv2AgentToLocPort *) RBTreeGetNext (gLldpGlobalInfo.
                                                             Lldpv2AgentToLocPortMapTblRBTree,
                                                             &AgentToLocPort,
                                                             LldpAgentToLocPortUtlRBCmpInfo);
                while ((pAgentToLocPort != NULL)
                       && (pAgentToLocPort->i4IfIndex ==
                           ((INT4) pQMsg->u4Port)))
                {
                    AgentToLocPort.i4IfIndex = pAgentToLocPort->i4IfIndex;
                    MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                            pAgentToLocPort->Lldpv2DestMacAddress,
                            MAC_ADDR_LEN);
                    pLocPortEntry =
                        LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);
                    if (pLocPortEntry != NULL)
                    {
                        LldpQueHandlePVid (pLocPortEntry, pQMsg->NewVlanId);
                    }
                    pAgentToLocPort = (tLldpv2AgentToLocPort *)
                        RBTreeGetNext (gLldpGlobalInfo.
                                       Lldpv2AgentToLocPortMapTblRBTree,
                                       &AgentToLocPort,
                                       LldpAgentToLocPortUtlRBCmpInfo);
                }
                break;

            case LLDP_PROT_VLAN_STATUS_MSG:
                LldpQueHandleProtoVlanStatus (pQMsg->u4Port,
                                              pQMsg->ProtVlanStatus);
                break;

            case LLDP_PROT_VLAN_ID_MSG:
                LldpQueHandleProtoVlanId (pQMsg->u4Port, pQMsg->NewVlanId,
                                          pQMsg->OldVlanId,
                                          pQMsg->u4MsgSubType);
                break;

            case LLDP_VLAN_INFO_CHG_FOR_PORT:
                AgentToLocPort.i4IfIndex = pQMsg->u4Port;
                MEMSET (AgentToLocPort.Lldpv2DestMacAddress, 0,
                        sizeof (tMacAddr));
                pAgentToLocPort =
                    (tLldpv2AgentToLocPort *) RBTreeGetNext (gLldpGlobalInfo.
                                                             Lldpv2AgentToLocPortMapTblRBTree,
                                                             &AgentToLocPort,
                                                             LldpAgentToLocPortUtlRBCmpInfo);
                while ((pAgentToLocPort != NULL)
                       && (pAgentToLocPort->i4IfIndex ==
                           ((INT4) pQMsg->u4Port)))
                {
                    AgentToLocPort.i4IfIndex = pAgentToLocPort->i4IfIndex;
                    MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                            pAgentToLocPort->Lldpv2DestMacAddress,
                            MAC_ADDR_LEN);
                    pLocPortEntry =
                        LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);
                    if (pLocPortEntry != NULL)
                    {
                        LldpQueHandleVlanInfoChgForPort (pLocPortEntry);
                    }
                    pAgentToLocPort = (tLldpv2AgentToLocPort *)
                        RBTreeGetNext (gLldpGlobalInfo.
                                       Lldpv2AgentToLocPortMapTblRBTree,
                                       &AgentToLocPort,
                                       LldpAgentToLocPortUtlRBCmpInfo);
                }
                break;

            case LLDP_VLAN_INFO_CHG_MSG:
                LldpQueHandleVlanInfoChg (pQMsg->PortList);
                break;

            case LLDP_PROT_IDENT_MSG:
                /*TODO: not supported in the current release */
                break;

            case LLDP_POW_MDI_STATUS:
                /*TODO: not supported in the current release */
                break;

            case LLDP_POW_PAIRS:
                /*TODO: not supported in the current release */
                break;

            case LLDP_PORT_AGG_CAP_MSG:
                AgentToLocPort.i4IfIndex = pQMsg->u4Port;
                MEMSET (AgentToLocPort.Lldpv2DestMacAddress, 0,
                        sizeof (tMacAddr));
                pAgentToLocPort =
                    (tLldpv2AgentToLocPort *) RBTreeGetNext (gLldpGlobalInfo.
                                                             Lldpv2AgentToLocPortMapTblRBTree,
                                                             &AgentToLocPort,
                                                             LldpAgentToLocPortUtlRBCmpInfo);
                while ((pAgentToLocPort != NULL)
                       && (pAgentToLocPort->i4IfIndex ==
                           ((INT4) pQMsg->u4Port)))
                {
                    AgentToLocPort.i4IfIndex = pAgentToLocPort->i4IfIndex;
                    MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                            pAgentToLocPort->Lldpv2DestMacAddress,
                            MAC_ADDR_LEN);
                    pLocPortEntry =
                        LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);
                    if (pLocPortEntry != NULL)
                    {
                        LldpQueHandlePortAggCapability (pLocPortEntry,
                                                        pQMsg->AggCapability);
                    }
                    pAgentToLocPort = (tLldpv2AgentToLocPort *)
                        RBTreeGetNext (gLldpGlobalInfo.
                                       Lldpv2AgentToLocPortMapTblRBTree,
                                       &AgentToLocPort,
                                       LldpAgentToLocPortUtlRBCmpInfo);
                }
                break;

            case LLDP_RESET_AGG_CAP_MSG:
                LldpQueHandleResetAggCapOnPorts (pQMsg->PortList);
                break;

            case LLDP_PORT_AGG_STATUS_MSG:
                AgentToLocPort.i4IfIndex = pQMsg->u4Port;
                MEMSET (AgentToLocPort.Lldpv2DestMacAddress, 0,
                        sizeof (tMacAddr));
                pAgentToLocPort =
                    (tLldpv2AgentToLocPort *) RBTreeGetNext (gLldpGlobalInfo.
                                                             Lldpv2AgentToLocPortMapTblRBTree,
                                                             &AgentToLocPort,
                                                             LldpAgentToLocPortUtlRBCmpInfo);
                while ((pAgentToLocPort != NULL)
                       && (pAgentToLocPort->i4IfIndex ==
                           ((INT4) pQMsg->u4Port)))
                {
                    AgentToLocPort.i4IfIndex = pAgentToLocPort->i4IfIndex;
                    MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                            pAgentToLocPort->Lldpv2DestMacAddress,
                            MAC_ADDR_LEN);
                    pLocPortEntry =
                        LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);
                    if (pLocPortEntry != NULL)
                    {
                        LldpQueHandlePortAggStatus (pLocPortEntry,
                                                    pQMsg->AggStatus);
                    }
                    pAgentToLocPort = (tLldpv2AgentToLocPort *)
                        RBTreeGetNext (gLldpGlobalInfo.
                                       Lldpv2AgentToLocPortMapTblRBTree,
                                       &AgentToLocPort,
                                       LldpAgentToLocPortUtlRBCmpInfo);
                }
                break;

            case LLDP_APPL_PORT_REGISTER:
                LldpQueHandleApplPortRequest (pQMsg->u4Port,
                                              &pQMsg->AppPortMsg,
                                              LLDP_APPL_PORT_REG_REQ);
                /* Release the TLV buffer */
                if (pQMsg->AppPortMsg.u2TxAppTlvLen != 0)
                {
                    MemReleaseMemBlock
                        (gLldpGlobalInfo.AppTlvPoolId,
                         (UINT1 *) pQMsg->AppPortMsg.pu1TxAppTlv);
                }
                break;

            case LLDP_APPL_PORT_DEREGISTER:
                LldpQueHandleApplPortRequest (pQMsg->u4Port,
                                              &pQMsg->AppPortMsg,
                                              LLDP_APPL_PORT_DEREG_REQ);
                break;

            case LLDP_APPL_PORT_UPDATES:
                LldpQueHandleApplPortRequest (pQMsg->u4Port, &pQMsg->AppPortMsg,
                                              LLDP_APPL_PORT_UPDATE_REQ);
                /* Release the TLV buffer */
                if (pQMsg->AppPortMsg.u2TxAppTlvLen != 0)
                {
                    MemReleaseMemBlock
                        (gLldpGlobalInfo.AppTlvPoolId,
                         (UINT1 *) pQMsg->AppPortMsg.pu1TxAppTlv);
                }
                break;
#ifdef L2RED_WANTED
            case LLDP_RM_MSG:
                LldpRedProcessRmMsg (&(pQMsg->LldpRmMsg));
                if (gLldpRedGlobalInfo.bGlobShutWhileRcvdTimeProcessed
                    == OSIX_TRUE)
                {
                    /* The Changes were introduced to avoid duplicate memory release
                     * during LLDP_RED_TMR_EXP_MSG event handling, which is already 
                     * released by LLPD - Shut in Active - Standby Scenario */

                    u4SkipMemRelease = OSIX_TRUE;
                }
                break;
#endif
            case LLDP_SET_DST_MAC:
                LldpQueHandleDstMac (&(pQMsg->Agent));
                break;
            case LLDP_SVID_ADD:
                LldpUtlSvidUpdateOnUap (pQMsg->UpdSvidOnUap.u4IfIndex,
                                        pQMsg->UpdSvidOnUap.u4Svid);
                break;
            case LLDP_SVID_DELETE:
                LldpUtlSvidDeleteOnUap (pQMsg->UpdSvidOnUap.u4IfIndex,
                                        pQMsg->UpdSvidOnUap.u4Svid);
                break;

            case LLDP_PORT_DESC_CHG_MSG:
                LldpQueHandlePortDesc (pQMsg->PortDescConfig, pQMsg->u4IfIndex);
                break;

            default:
                LLDP_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                               "LldpQueAppMsgHandler:Unknown message type %d "
                               "received\r\n", pQMsg->u4MsgType);
                break;
        }
        /* Release the buffer to pool */
        if (u4SkipMemRelease == OSIX_FALSE)
        {
            if (MemReleaseMemBlock (gLldpGlobalInfo.QMsgPoolId, (UINT1 *) pQMsg)
                != MEM_SUCCESS)
            {
                LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "LldpQueAppMsgHandler"
                          "Free MemBlock AppMsg FAILED!!!\r\n");
                return;
            }
        }

    }
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpQueEnqAppMsg 
 *                                                                          
 *    DESCRIPTION      : Function to post application message to LLDP task. 
 *
 *    INPUT            : pMsg - Pointer to msg to be posted 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
LldpQueEnqAppMsg (tLldpQMsg * pMsg)
{
    UINT4               u4Event = pMsg->u4MsgType;

    if (OsixQueSend (gLldpGlobalInfo.MsgQId, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {

        LLDP_TRC_ARG1 (OS_RESOURCE_TRC | LLDP_CRITICAL_TRC, "LldpQueEnqAppMsg"
                       "Osix Queue send Failed for message type: %d!!!\r\n",
                       pMsg->u4MsgType);
        if ((u4Event == LLDP_APPL_PORT_REGISTER) ||
            (u4Event == LLDP_APPL_PORT_UPDATES))
        {
            /* Release the application TLV buffer also */
            if (pMsg->AppPortMsg.pu1TxAppTlv != NULL)
            {
                MemReleaseMemBlock (gLldpGlobalInfo.AppTlvPoolId,
                                    (UINT1 *) pMsg->AppPortMsg.pu1TxAppTlv);
            }
        }

        MemReleaseMemBlock (gLldpGlobalInfo.QMsgPoolId, (UINT1 *) pMsg);
        return OSIX_FAILURE;
    }

    OsixEvtSend (gLldpGlobalInfo.TaskId, LLDP_QMSG_EVENT);

    if (u4Event == LLDP_CREATE_PORT_MSG)
    {
        L2_SYNC_TAKE_SEM ();
    }
    else if ((u4Event == LLDP_MAP_PORT_MSG) || (u4Event == LLDP_UNMAP_PORT_MSG))
    {
        L2MI_SYNC_TAKE_SEM ();
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpQueEnqRxPduMsg 
 *                                                                          
 *    DESCRIPTION      : Function to post Received RxPdu message to LLDP task. 
 *
 *    INPUT            : pMsg - Pointer to msg to be posted 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
LldpQueEnqRxPduMsg (tLldpRxPduQMsg * pMsg)
{
    if (OsixQueSend (gLldpGlobalInfo.RxPduQId, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        LLDP_TRC_ARG1 (LLDP_CRITICAL_TRC, "LldpQueEnqRxPduMsg"
                       "Osix Queue send Failed for port :%d!!!\r\n",
                       pMsg->u4Port);
        return OSIX_FAILURE;
    }

    OsixEvtSend (gLldpGlobalInfo.TaskId, LLDP_RXPDU_EVENT);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpQueHandleIncomingFrame 
 *                                                                          
 *    DESCRIPTION      : Function receives the incoming frame from lower layer.
 *                        
 *
 *    INPUT            : pLocPortEntry - Pointer to local port info structure
 *                       pBuf          - pointer to CRU Buffer
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
LldpQueHandleIncomingFrame (tLldpLocPortInfo * pLocPortEntry,
                            tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tCRU_BUF_CHAIN_HEADER *pLldpdu = NULL;
    UINT4               u4FrameSize = 0;
    UINT4               u4PduLen = 0;
    UINT4               u4Offset = 0;
    tCfaIfInfo          CfaIfInfo;

    /* Abort if pointer is NULL */
    FSAP_ASSERT (pBuf != NULL);
    FSAP_ASSERT (pLocPortEntry != NULL);

    /* pLldpdu is used for further processing and pBuf is used to free the Buf
     * in case of failure */
    pLldpdu = pBuf;

    MEMSET (gau1LldPduQue, 0, LLDP_MAX_PDU_SIZE);
    MEMSET (pLocPortEntry->au1LstRcvdSrcMacAddr, 0, MAC_ADDR_LEN);
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    if ((pLocPortEntry->PortConfigTable.i4AdminStatus
         != LLDP_ADM_STAT_RX_ONLY) &&
        (pLocPortEntry->PortConfigTable.i4AdminStatus
         != LLDP_ADM_STAT_TX_AND_RX))
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpQueHandleIncomingFrame: "
                       "LLDP Admin status is not enabled for RX on port: %d \r\n",
                       pLocPortEntry->i4IfIndex);
        LLDP_INCR_CNTR_RX_FRAMES (pLocPortEntry);
        LLDP_INCR_CNTR_RX_FRAMES_DISCARDED (pLocPortEntry);
        LldpUtilPktFree (pBuf);
        return;
    }

    u4FrameSize = CRU_BUF_Get_ChainValidByteCount (pLldpdu);

    if ((u4FrameSize < LLDP_MIN_FRAME_SIZE) ||
        (u4FrameSize > LLDP_MAX_FRAME_SIZE))
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                       "LldpqueHandleIncomingFrame: Port %d :"
                       "Invalid length of Ethernet Frame; "
                       "Discarding frame.\r\n", pLocPortEntry->i4IfIndex);

        LLDP_INCR_CNTR_RX_FRAMES (pLocPortEntry);
        LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortEntry);
        LLDP_INCR_CNTR_RX_FRAMES_DISCARDED (pLocPortEntry);
        LldpUtilPktFree (pBuf);
        return;
    }
    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC | DUMP_TRC,
                   "LldpQueHandleIncomingFrame: Dumping LLDPDU "
                   "received from lower layer on Port:%u\r\n",
                   pLocPortEntry->i4IfIndex);

    LLDP_PKT_DUMP (DUMP_TRC, pLldpdu, u4FrameSize, "");

    if (LldpPortGetIfInfo (pLocPortEntry->i4IfIndex, &CfaIfInfo) !=
        OSIX_SUCCESS)
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpqueHandleIncomingFrame:"
                       "for Port %d : LldpPortGetIfInfo failed.\r\n",
                       pLocPortEntry->i4IfIndex);
        LLDP_INCR_CNTR_RX_FRAMES (pLocPortEntry);
        LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortEntry);
        LLDP_INCR_CNTR_RX_FRAMES_DISCARDED (pLocPortEntry);
        LldpUtilPktFree (pBuf);
        return;
    }
    CRU_BUF_Copy_FromBufChain (pLldpdu, pLocPortEntry->au1LstRcvdSrcMacAddr,
                               MAC_ADDR_LEN, MAC_ADDR_LEN);
    /* Frame can be direct encoded or snap enacoded */
    if (CfaIfInfo.u1IfType == CFA_ENET)
    {
        u4Offset = CFA_ENET_V2_HEADER_SIZE;
    }
    else if ((CfaIfInfo.u1IfType == CFA_TOKENRING) ||
             (CfaIfInfo.u1IfType == CFA_FDDI))
    {
        u4Offset = LLDP_SNAP_ENCODED_PDU_OFFSET;
    }

    /* Strip off the ethernet header */
    CRU_BUF_Move_ValidOffset (pLldpdu, u4Offset);
    u4PduLen = u4FrameSize - u4Offset;
    u4Offset = 0;

    if ((u4PduLen < LLDP_MIN_PDU_SIZE) || (u4PduLen > LLDP_MAX_PDU_SIZE))
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpqueHandleIncomingFrame:"
                       "for Port %d : Invalid length of LLDPDU;"
                       " Discarding the frame.\r\n", pLocPortEntry->i4IfIndex);
        LLDP_INCR_CNTR_RX_FRAMES (pLocPortEntry);
        LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortEntry);
        LLDP_INCR_CNTR_RX_FRAMES_DISCARDED (pLocPortEntry);
        LldpUtilPktFree (pBuf);
        return;
    }
    if (CRU_BUF_Copy_FromBufChain (pLldpdu, gau1LldPduQue, u4Offset, u4PduLen)
        == CRU_FAILURE)
    {
        LLDP_TRC_ARG1 (BUFFER_TRC | ALL_FAILURE_TRC, "MSG: Received lldpdu Copy"
                       " from CRU Buffer FAILED for Port : %d!\r\n",
                       pLocPortEntry->i4IfIndex);
        LLDP_INCR_CNTR_RX_FRAMES (pLocPortEntry);
        LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortEntry);
        LLDP_INCR_CNTR_RX_FRAMES_DISCARDED (pLocPortEntry);
        LldpUtilPktFree (pBuf);
        return;
    }

    LldpUtilPktFree (pBuf);
    pLocPortEntry->pu1RxLldpdu = gau1LldPduQue;
    pLocPortEntry->u2RxPduLen = (UINT2) u4PduLen;
    pLocPortEntry->bstatsFramesInerrorsTotal = OSIX_FALSE;
    /* Reset all the bits of the flag for every LLDPDU */
    MEMSET (pLocPortEntry->au1DupTlvChkFlag, 0,
            sizeof (pLocPortEntry->au1DupTlvChkFlag));

    /* Call Rx State Event Machine */
    LldpRxSemRun (pLocPortEntry, LLDP_RX_EV_FRAME_RECEIVED);
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpQueHandleSysName 
 *                                                                          
 *    DESCRIPTION      : This function is called when the System Name 
 *                       is Changed. 
 *
 *    INPUT            : pu1SysName - Changed System Name String
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
INT4
LldpQueHandleSysName (UINT1 *pu1SysName)
{
    if (pu1SysName == NULL)
    {
        return OSIX_FAILURE;
    }

    if (STRNCMP (&gLldpGlobalInfo.LocSysInfo.au1LocSysName,
                 pu1SysName, LLDP_MAX_LEN_SYSNAME) != 0)
    {
        MEMSET (gLldpGlobalInfo.LocSysInfo.au1LocSysName, 0,
                LLDP_MAX_LEN_SYSNAME);
        STRNCPY (gLldpGlobalInfo.LocSysInfo.au1LocSysName, pu1SysName,
                 LLDP_MAX_LEN_SYSNAME);
        LldpTxUtlHandleLocSysInfoChg ();
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpQueHandleDstMac
 *
 *    DESCRIPTION      : This function is called to create/delete an LLDP agent
 *
 *
 *    INPUT            :
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpQueHandleDstMac (tLldpAgent * pLldpAgent)
{
    UINT1               u1FatalError = OSIX_FALSE;

    return (LldpUtilSetDstMac
            (pLldpAgent->i4IfIndex, (UINT1 *) &(pLldpAgent->MacAddr),
             pLldpAgent->u1RowStatus, &u1FatalError));
}

/****************************************************************************
  *
 *    FUNCTION NAME    : LldpQueHandleSysDesc
 *
 *    DESCRIPTION      : This function is called when the System Description
 *                       is Changed.
 *
 *    INPUT            : pu1SysDesc - Changed System Name String
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpQueHandleSysDesc (UINT1 *pu1SysDesc)
{
    if (pu1SysDesc == NULL)
    {
        return OSIX_FAILURE;
    }

    if (STRNCMP (&gLldpGlobalInfo.LocSysInfo.au1LocSysDesc,
                 pu1SysDesc, LLDP_MAX_LEN_SYSDESC) != 0)
    {
        MEMSET (gLldpGlobalInfo.LocSysInfo.au1LocSysDesc, 0,
                LLDP_MAX_LEN_SYSDESC);
        STRNCPY (gLldpGlobalInfo.LocSysInfo.au1LocSysDesc, pu1SysDesc,
                 LLDP_MAX_LEN_SYSDESC);
        LldpTxUtlHandleLocSysInfoChg ();
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpQueHandleIp4ManAddr
 *
 *    DESCRIPTION      : This function is called whever IP interface is
 *                       Created Deleted or Changed.
 *
 *    INPUT            : pIp4ManAddrQMsg - Q message structure containing the
 *                                         information.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpQueHandleIp4ManAddr (tLldpQIp4ManAddress * pIp4ManAddrQMsg)
{
    tLldpLocPortTable   LocPortTableEntry;
    tLldpv2AgentToLocPort AgentToLocPort;
    tLldpv2AgentToLocPort *pAgentToLocPort = NULL;
    tLldpLocPortInfo   *pLocPortEntry = NULL;
    tLldpLocPortTable  *pLocPortTableEntry = NULL;
    tLldpLocManAddrTable *pCurManAddrNode = NULL;
    tLldpLocManAddrTable *pNextManAddrNode = NULL;
    UINT4               u4ManAddr = 0;
    UINT4               u4DelManAddr = 0;
    UINT4               u4BitMap = 0;
    UINT4               u4DefIfIndex = 0;
    UINT4               u4CfaIfIndex = 0;
    UINT4               u4L3IfIndex = 0;
    INT4                i4ManAddrSubtype = LLDP_INVALID_VALUE;
    UINT1               au1ManAddr[LLDP_MAX_LEN_MAN_ADDR] = { 0 };
    UINT1              *pu1TmpChassisId = NULL;
    UINT1              *pu1TmpPortId = NULL;
#ifdef IP_WANTED
    tLldpLocPortInfo   *pPortInfo = NULL;
    tLldpLocPortInfo   *pLocPort = NULL;
#endif
    MEMSET (&AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));

    MEMSET (&LocPortTableEntry, 0, sizeof (tLldpLocPortTable));
    if (pIp4ManAddrQMsg == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (&au1ManAddr[0], 0, LLDP_MAX_LEN_MAN_ADDR);

    u4L3IfIndex = pIp4ManAddrQMsg->NetIpIfInfo.u4IfIndex;
    u4ManAddr = OSIX_HTONL (pIp4ManAddrQMsg->NetIpIfInfo.u4Addr);
    pIp4ManAddrQMsg->NetIpIfInfo.u4Addr = u4ManAddr;
    u4BitMap = pIp4ManAddrQMsg->u4BitMap;
    i4ManAddrSubtype = IPVX_ADDR_FMLY_IPV4;

    if ((u4BitMap & IFACE_DELETED) || (u4BitMap & IP_ADDR_BIT_MASK))
    {
        pNextManAddrNode = (tLldpLocManAddrTable *) RBTreeGetFirst
            (gLldpGlobalInfo.LocManAddrRBTree);
        if (pNextManAddrNode == NULL)
        {
            LLDP_TRC (CONTROL_PLANE_TRC | OS_RESOURCE_TRC | LLDP_MAN_ADDR_TRC,
                      "LldpQueHandleIp4ManAddr : No entry present in the  "
                      "Local Mangement DB. So ignoring the Delete indication\r\n");
            return OSIX_FAILURE;
        }

        do
        {
            pCurManAddrNode = pNextManAddrNode;
            /* If the L3IfIndex matched delete the node */
            if (pCurManAddrNode->u4L3IfIndex == u4L3IfIndex)
            {
                /* u4CfaIfIndex is used for updating chassis id, if the 
                 * chassis id subtype is configured as network address */
                u4CfaIfIndex = (UINT4) pCurManAddrNode->i4LocManAddrIfId;
                /* u4DelManAddr will be used for trace msg */

                MEMCPY (&u4DelManAddr, pCurManAddrNode->au1LocManAddr,
                        sizeof (UINT4));
                u4DelManAddr = OSIX_HTONL (u4DelManAddr);
                RBTreeRem (gLldpGlobalInfo.LocManAddrRBTree,
                           (tRBElem *) pCurManAddrNode);
                LLDP_TRC_ARG1 (CONTROL_PLANE_TRC | OS_RESOURCE_TRC |
                               LLDP_MAN_ADDR_TRC,
                               "LldpQueHandleIp4ManAddr : IpAddr(%x) is "
                               "deleted from the LocalDB\r\n", u4DelManAddr);
                /* If deletion indication is received, release the node.
                 * Else change the address and add the node to the RB
                 * Tree. During address change, IP does not indicate
                 * the old and the new address separately. Only the new
                 * address is received. This function should be modified
                 * once the notification is changed
                 */
                if (u4BitMap & IFACE_DELETED)
                {
                    u4ManAddr = 0;
                    if (MemReleaseMemBlock (gLldpGlobalInfo.LocManAddrPoolId,
                                            (UINT1 *) pCurManAddrNode)
                        != MEM_SUCCESS)
                    {
                        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC |
                                  LLDP_MAN_ADDR_TRC,
                                  "LldpQueHandleIp4ManAddr: "
                                  "MemBlock Release failed for local ManAddr "
                                  "entry\r\n");
                        return OSIX_FAILURE;
                    }
                }
#ifdef IP_WANTED
                else
                {
                    MEMSET (pCurManAddrNode->au1LocManAddr, 0,
                            LLDP_MAX_LEN_MAN_ADDR);
                    MEMCPY (pCurManAddrNode->au1LocManAddr, &u4ManAddr,
                            sizeof (UINT4));

                    if ((pLocPort = (tLldpLocPortInfo *) MemAllocMemBlk
                         (gLldpGlobalInfo.LocPortInfoPoolId)) == NULL)
                    {
                        LLDP_TRC (LLDP_CRITICAL_TRC | MGMT_TRC |
                                  ALL_FAILURE_TRC,
                                  "Memory allocation failed for agent\r\n");
                        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
                        return OSIX_FAILURE;
                    }

                    MEMSET (pLocPort, 0, sizeof (tLldpLocPortInfo));
                    pPortInfo = (tLldpLocPortInfo *)
                        RBTreeGetFirst (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree);
                    while (pPortInfo != NULL)

                    {

                        if (pPortInfo->u1ManAddrTlvTxEnabled == LLDP_TRUE)
                        {

                            OSIX_BITLIST_SET_BIT (pCurManAddrNode->
                                                  au1LocManAddrPortsTxEnable,
                                                  (UINT2) pPortInfo->
                                                  u4LocPortNum,
                                                  LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
                        }
                        else
                        {
                            OSIX_BITLIST_RESET_BIT (pCurManAddrNode->
                                                    au1LocManAddrPortsTxEnable,
                                                    (UINT2) pPortInfo->
                                                    u4LocPortNum,
                                                    LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
                        }

                        pLocPort->u4LocPortNum = pPortInfo->u4LocPortNum;
                        pPortInfo = (tLldpLocPortInfo *)
                            RBTreeGetNext (gLldpGlobalInfo.
                                           LldpLocPortAgentInfoRBTree,
                                           pLocPort, LldpAgentInfoUtlRBCmpInfo);
                    }

                    if (RBTreeAdd (gLldpGlobalInfo.LocManAddrRBTree,
                                   (tRBElem *) pCurManAddrNode) != RB_SUCCESS)
                    {
                        LLDP_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC |
                                  LLDP_MAN_ADDR_TRC,
                                  "LldpQueHandleIp4ManAddr : Failed to Add the Node to "
                                  "LocManAddrRBTree\r\n");
                        MemReleaseMemBlock (gLldpGlobalInfo.LocManAddrPoolId,
                                            (UINT1 *) pCurManAddrNode);
                        MemReleaseMemBlock (gLldpGlobalInfo.LocPortInfoPoolId,
                                            (UINT1 *) pLocPort);
                        return OSIX_FAILURE;
                    }

                    MemReleaseMemBlock (gLldpGlobalInfo.LocPortInfoPoolId,
                                        (UINT1 *) pLocPort);
                }
#endif
                LldpTxUtlHandleLocSysInfoChg ();
                break;
            }
        }
        while (LldpTxUtlGetNextManAddr (pCurManAddrNode, &pNextManAddrNode)
               != OSIX_FAILURE);
    }

    else if (u4BitMap & OPER_STATE)
    {
        if (LldpPortGetIfIndexFromPort (u4L3IfIndex, &u4CfaIfIndex)
            != OSIX_SUCCESS)
        {
            LLDP_TRC_ARG1 (ALL_FAILURE_TRC | LLDP_MAN_ADDR_TRC,
                           "LldpQueHandleIp4ManAddr: "
                           "Failed to get CfaIfIndex from l3IfIndex: %d\r\n",
                           u4L3IfIndex);
            return OSIX_FAILURE;
        }

        if (pIp4ManAddrQMsg->NetIpIfInfo.u4Oper == IPIF_OPER_ENABLE)
        {
            if (LldpTxUtlAddIpv4Addr (&pIp4ManAddrQMsg->NetIpIfInfo)
                != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }

            LLDP_TRC_ARG1 (CONTROL_PLANE_TRC | OS_RESOURCE_TRC,
                           "LldpQueHandleIp4ManAddr : IpAddr(%x) is "
                           "added to the LocalDB\r\nn", u4ManAddr);
        }
        else if (pIp4ManAddrQMsg->NetIpIfInfo.u4Oper == IPIF_OPER_DISABLE)
        {
            MEMCPY (au1ManAddr, &u4ManAddr, sizeof (UINT4));
            /* Oper down received for the management interface
             * Set the oper status as down and handle local sys change
             */
            pCurManAddrNode = LldpTxUtlGetLocManAddrNode
                (i4ManAddrSubtype, au1ManAddr);

            if (pCurManAddrNode == NULL)
            {
                return OSIX_FAILURE;
            }

            pCurManAddrNode->u1OperStatus = IPIF_OPER_DISABLE;
        }
        LldpTxUtlHandleLocSysInfoChg ();
    }

    if (gLldpGlobalInfo.LocSysInfo.i4LocChassisIdSubtype
        == LLDP_CHASS_ID_SUB_NW_ADDR)
    {
        /* update chassis id(if chassis id subtype is network address) */
        LldpPortGetDefaultRouterIfIndex (&u4DefIfIndex);
        if (u4CfaIfIndex == u4DefIfIndex)
        {
            MEMSET (gLldpGlobalInfo.LocSysInfo.au1LocChassisId, 0,
                    LLDP_MAX_LEN_CHASSISID);

            pu1TmpChassisId = &gLldpGlobalInfo.LocSysInfo.au1LocChassisId[0];
            LLDP_PUT_1BYTE (pu1TmpChassisId, (UINT1) IPVX_ADDR_FMLY_IPV4);
            u4ManAddr = OSIX_HTONL (u4ManAddr);
            PTR_ASSIGN4 (&gLldpGlobalInfo.LocSysInfo.au1LocChassisId[1],
                         u4ManAddr);
            /* chassis id is changed */
            LldpTxUtlHandleLocSysInfoChg ();
        }
    }
    /* update port id(if port id subtype is network address) */
    /* get the port entry associated with the interface index */
    LocPortTableEntry.i4IfIndex = (INT4) u4CfaIfIndex;
    pLocPortTableEntry = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LocPortTableEntry);
    if (pLocPortTableEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if (pLocPortTableEntry->i4LocPortIdSubtype == LLDP_PORT_ID_SUB_NW_ADDR)
    {
        MEMSET (pLocPortTableEntry->au1LocPortId, 0, LLDP_MAX_LEN_PORTID);

        pu1TmpPortId = &(pLocPortTableEntry->au1LocPortId[0]);
        LLDP_PUT_1BYTE (pu1TmpPortId, (UINT1) IPVX_ADDR_FMLY_IPV4);
        PTR_ASSIGN4 (&(pLocPortTableEntry->au1LocPortId[1]), u4ManAddr);
        /* port id is changed */
        AgentToLocPort.i4IfIndex = u4CfaIfIndex;
        MEMSET (AgentToLocPort.Lldpv2DestMacAddress, 0, sizeof (tMacAddr));
        pAgentToLocPort = (tLldpv2AgentToLocPort *)
            RBTreeGetNext (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                           &AgentToLocPort, LldpAgentToLocPortUtlRBCmpInfo);
        while ((pAgentToLocPort != NULL)
               && (pAgentToLocPort->i4IfIndex == (INT4) u4CfaIfIndex))
        {
            AgentToLocPort.i4IfIndex = pAgentToLocPort->i4IfIndex;
            MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                    pAgentToLocPort->Lldpv2DestMacAddress, MAC_ADDR_LEN);
            pLocPortEntry =
                LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);
            if (pLocPortEntry != NULL)
            {
                if (LldpTxUtlHandleLocPortInfoChg (pLocPortEntry) !=
                    OSIX_SUCCESS)
                {
                    LLDP_TRC_ARG1 (ALL_FAILURE_TRC | LLDP_MAN_ADDR_TRC,
                                   "LldpQueHandleIp4ManAddr: "
                                   "LldpTxUtlHandleLocPortInfoChg failed for port : %d\r\n",
                                   AgentToLocPort.i4IfIndex);
                }
            }
            pAgentToLocPort = (tLldpv2AgentToLocPort *)
                RBTreeGetNext (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                               &AgentToLocPort, LldpAgentToLocPortUtlRBCmpInfo);
        }
    }
    /* Memory of the QMsg will be released at the caller function */
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpQueHandleIp6ManAddr
 *
 *    DESCRIPTION      : This function is called whever IP interface is
 *                       Created Deleted or Changed.
 *
 *    INPUT            : pIp6ManAddrQMsg - Q message structure containing the
 *                                         information.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpQueHandleIp6ManAddr (tNetIpv6AddrChange * pIp6ManAddrQMsg)
{
    tIp6Addr            Ipv6Addr;
    UINT4               u4BitMap = 0;
    INT4                i4ManAddrSubtype = LLDP_INVALID_VALUE;
    UINT4               u4Type = 0;

    if (pIp6ManAddrQMsg == NULL)
    {
        return OSIX_FAILURE;
    }

    Ipv6Addr = pIp6ManAddrQMsg->Ipv6AddrInfo.Ip6Addr;
    u4BitMap = pIp6ManAddrQMsg->u4Mask;
    u4Type = pIp6ManAddrQMsg->Ipv6AddrInfo.u4Type;
    i4ManAddrSubtype = IPVX_ADDR_FMLY_IPV6;

    if ((u4Type == ADDR6_UNICAST) || (u4Type == ADDR6_LLOCAL))
    {
        if (u4BitMap == NETIPV6_ADDRESS_DELETE)
        {
            if (LldpTxUtlDelManAddrNode (i4ManAddrSubtype,
                                         (UINT1 *) &Ipv6Addr.u1_addr)
                != OSIX_SUCCESS)
            {
                LLDP_TRC (ALL_FAILURE_TRC | LLDP_MAN_ADDR_TRC,
                          "LldpQueHandleIp6ManAddr: "
                          "Deletion of Local Management Address failed\r\n");
                return OSIX_FAILURE;
            }
            LldpTxUtlHandleLocSysInfoChg ();
        }
        else if (u4BitMap == NETIPV6_ADDRESS_ADD)
        {
            if (LldpTxUtlAddIpv6Addr (pIp6ManAddrQMsg) != OSIX_SUCCESS)
            {
                LLDP_TRC (ALL_FAILURE_TRC | LLDP_MAN_ADDR_TRC,
                          "LldpQueHandleIp6ManAddr: "
                          "Failed to add local Management Address Node\r\n");
                return OSIX_FAILURE;
            }

            LldpTxUtlHandleLocSysInfoChg ();
        }
    }
    /* Memory of the QMsg will be released at the caller function */
    return OSIX_SUCCESS;

}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpQueHandleAgentCktId 
 *                                                                          
 *    DESCRIPTION      : This function is called when the agent circuit id 
 *                       changes. 
 *
 *    INPUT            : pLocPortEntry - Pointer to local port info structure
 *                       u4NewAgentCktId   - agent circuit id 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
INT4
LldpQueHandleAgentCktId (tLldpLocPortInfo * pLocPortEntry,
                         UINT4 u4NewAgentCktId)
{
    tLldpLocPortTable  *pLocPortTableEntry = NULL;
    tLldpLocPortTable   LocPortTableEntry;
    UINT1              *pCurrentCktId = NULL;
    UINT1               au1AgentCktId[LLDP_MAX_LEN_PORTID];
    MEMSET (au1AgentCktId, 0, LLDP_MAX_LEN_PORTID);

    SNPRINTF ((char *) au1AgentCktId, LLDP_MAX_LEN_PORTID, "%u",
              u4NewAgentCktId);

    /*For Klocwork */
    if (pLocPortEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&LocPortTableEntry, 0, sizeof (tLldpLocPortTable));
    LocPortTableEntry.i4IfIndex = pLocPortEntry->i4IfIndex;
    pLocPortTableEntry = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LocPortTableEntry);
    if (pLocPortTableEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if (pLocPortEntry == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpQueHandleAgentCktId: pLocPortEntry NULL"
                  " pointer received\r\n");
        return OSIX_FAILURE;
    }

    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                   "LldpQueHandleAgentCktId:Updating interface alias of "
                   "port %d\r\n", pLocPortEntry->u4LocPortNum);

    if (pLocPortTableEntry->i4LocPortIdSubtype != LLDP_PORT_ID_SUB_AGENT_CKT_ID)
    {
        LLDP_TRC (CONTROL_PLANE_TRC, "LldpQueHandleAgentCktId: "
                  "Port id subtype id not agent circuit id, so no need to send"
                  " agent circuit id change notification to LLDP\r\n");
        return OSIX_SUCCESS;
    }

    pCurrentCktId = pLocPortTableEntry->au1LocPortId;

    /* compare existing value and new value */
    if (STRNCMP (au1AgentCktId, pCurrentCktId, LLDP_MAX_LEN_PORTID) != 0)
    {
        MEMSET (pLocPortTableEntry->au1LocPortId, 0, LLDP_MAX_LEN_PORTID);
        MEMCPY (pLocPortTableEntry->au1LocPortId, au1AgentCktId,
                LLDP_MAX_LEN_PORTID);
        /* handle port id change */
        if (LldpTxUtlHandleLocPortInfoChg (pLocPortEntry) != OSIX_SUCCESS)
        {
            LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpQueHandleAgentCktId: "
                           "Preformed Buffer construction failed for port: %d\r\n",
                           pLocPortEntry->i4IfIndex);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpQueHandleIfAlias 
 *                                                                          
 *    DESCRIPTION      : This function is called when the interface alias 
 *                       changes. 
 *
 *    INPUT            : u4IfIndex     - ifindex for which ifalias is changed
 *                       pLocPortEntry - Pointer to local port info structure
 *                       pu1IfAlias    - pointer to interface alias 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
INT4
LldpQueHandleIfAlias (tLldpLocPortInfo * pLocPortEntry, UINT4 u4IfIndex,
                      UINT1 *pu1IfAlias)
{
    UINT4               u4DefIfIndex = 0;
    tLldpLocPortTable  *pLocPortTableEntry = NULL;
    tLldpLocPortTable   LocPortTableEntry;

    UNUSED_PARAM (pLocPortEntry);

    MEMSET (&LocPortTableEntry, 0, sizeof (tLldpLocPortTable));
    LocPortTableEntry.i4IfIndex = (INT4) u4IfIndex;
    pLocPortTableEntry = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LocPortTableEntry);
    if (pLocPortTableEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                   "LldpQueHandleIfAlias:Updating interface alias for "
                   "interface %d\r\n", u4IfIndex);

    /* if chassis id subtype is if-alias update chassis id */
    if (gLldpGlobalInfo.LocSysInfo.i4LocChassisIdSubtype ==
        LLDP_CHASS_ID_SUB_IF_ALIAS)
    {
        /* get default router ifindex */
        LldpPortGetDefaultRouterIfIndex (&u4DefIfIndex);
        if (u4IfIndex == u4DefIfIndex)
        {
            /* update chassis id */
            LldpQueUpdtIfAlias (gLldpGlobalInfo.LocSysInfo.au1LocChassisId,
                                pu1IfAlias, LLDP_MAX_LEN_CHASSISID);
            /* handle chassis id change */
            LldpTxUtlHandleLocSysInfoChg ();
        }
    }

    /*If Port ID subtype is if-alias updated port id and reconstruct the buffer */
    if (pLocPortTableEntry->i4LocPortIdSubtype == LLDP_PORT_ID_SUB_IF_ALIAS)
    {
        /* update port id */
        LldpQueUpdtIfAlias (pLocPortTableEntry->au1LocPortId,
                            pu1IfAlias, LLDP_MAX_LEN_PORTID);
        /* handle port id change */
        LldpTxUtlHandleLocSysInfoChg ();
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpQueHandlePVid 
 *                                                                          
 *    DESCRIPTION      : This function handles the port vlan id changes. 
 *
 *    INPUT            : pLocPortEntry - Pointer to local port info structure
 *                       u2VlanId      - Vlan id
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
INT4
LldpQueHandlePVid (tLldpLocPortInfo * pLocPortEntry, UINT2 u2PVId)
{
    if (pLocPortEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                   "LldpQueHandlePVid:Updating port vlan id for port %d\r\n",
                   pLocPortEntry->u4LocPortNum);

    if (pLocPortEntry->u2PVid != u2PVId)
    {
        pLocPortEntry->u2PVid = u2PVId;
        if (LldpTxUtlHandleLocPortInfoChg (pLocPortEntry) != OSIX_SUCCESS)
        {
            LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpQueHandlePVid:"
                           "LldpTxUtlHandleLocPortInfoChg failed for "
                           "port %d\r\n", pLocPortEntry->u4LocPortNum);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpQueHandleProtoVlanStatus
 *                                                                          
 *    DESCRIPTION      : This function updates the status of Protocol 
 *                       based Vlan Classification.  
 *
 *    INPUT            : u4IfIndex - Interface Index  
 *                       u1ProtVlanStatus - Status of protocol based vlan
 *                       classification (enabled /disabled)
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
INT4
LldpQueHandleProtoVlanStatus (UINT4 u4IfIndex, UINT1 u1ProtVlanStatus)
{
#define LLDP_GLOBAL_PROT_VLAN_STAT_CHG 0

    tLldpLocPortInfo   *pLocPortInfo = NULL;
    tLldpLocPortInfo    LocPortInfo;
    tLldpv2AgentToLocPort AgentToLocPort;
    tLldpv2AgentToLocPort *pAgentToLocPort = NULL;

    MEMSET (&LocPortInfo, 0, sizeof (tLldpLocPortInfo));
    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                   "LldpQueHandleProtoVlanStatus:Updating status of Protocol based"
                   " Vlan Classification on the port %d\r\n", u4IfIndex);

    /* Protocol based Vlan Classification has been enabled or disabled globally.
     * Hence set the new status for all the configured ports. */
    if (u4IfIndex == LLDP_GLOBAL_PROT_VLAN_STAT_CHG)
    {
        pLocPortInfo = (tLldpLocPortInfo *)
            RBTreeGetFirst (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree);
        while (pLocPortInfo != NULL)
        {
            if (pLocPortInfo->u1ProtoVlanEnabled != u1ProtVlanStatus)
            {
                pLocPortInfo->u1ProtoVlanEnabled = u1ProtVlanStatus;
                if (LldpTxUtlHandleLocPortInfoChg (pLocPortInfo)
                    != OSIX_SUCCESS)
                {
                    LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                                   "LldpQueHandleProtoVlanStatus:"
                                   "LldpTxUtlHandleLocPortInfoChg failed for"
                                   "port %d\r\n", pLocPortInfo->u4LocPortNum);
                }
            }
            LocPortInfo.u4LocPortNum = pLocPortInfo->u4LocPortNum;
            pLocPortInfo = (tLldpLocPortInfo *)
                RBTreeGetNext (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
                               &LocPortInfo, LldpAgentInfoUtlRBCmpInfo);
        }
    }
    else
    {
        /* Protocol based Vlan Classification has been enabled or disabled on 
         * a port. Hence set the new status for that particular port */
        AgentToLocPort.i4IfIndex = u4IfIndex;
        MEMSET (AgentToLocPort.Lldpv2DestMacAddress, 0, sizeof (tMacAddr));
        pAgentToLocPort = (tLldpv2AgentToLocPort *)
            RBTreeGetNext (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                           &AgentToLocPort, LldpAgentToLocPortUtlRBCmpInfo);
        while ((pAgentToLocPort != NULL)
               && (pAgentToLocPort->i4IfIndex == (INT4) u4IfIndex))
        {
            AgentToLocPort.i4IfIndex = pAgentToLocPort->i4IfIndex;
            MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                    pAgentToLocPort->Lldpv2DestMacAddress, MAC_ADDR_LEN);
            pLocPortInfo =
                LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);
            if ((pLocPortInfo != NULL)
                && (pLocPortInfo->u1ProtoVlanEnabled != u1ProtVlanStatus))
            {
                if (LldpTxUtlHandleLocPortInfoChg (pLocPortInfo) !=
                    OSIX_SUCCESS)
                {
                    LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                                   "LldpQueHandleProtoVlanStatus:"
                                   "LldpTxUtlHandleLocPortInfoChg failed for"
                                   "port %d\r\n", pLocPortInfo->u4LocPortNum);
                }
            }
            pAgentToLocPort = (tLldpv2AgentToLocPort *)
                RBTreeGetNext (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                               &AgentToLocPort, LldpAgentToLocPortUtlRBCmpInfo);
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpQueHandleProtoVlanId
 *                                                                          
 *    DESCRIPTION      : This function handles the change in the Protocol
 *                       Vlan Id mapped to the port.
 *
 *    INPUT            : u4IfIndex   - Interface Index  
 *                       u2VlanId    - Protocol Vlan Id
 *                       u2OldVlanId - Non-zero only if for the same protocol 
 *                                     group the protocol vlan id is replaced 
 *                                     with another protocol vlan id. The node
 *                                     containing the u2OldVlanId will be
 *                                     deleted and a new node with u2VlanId 
 *                                     will be added.   
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
INT4
LldpQueHandleProtoVlanId (UINT4 u4IfIndex, UINT2 u2VlanId, UINT2 u2OldVlanId,
                          UINT4 u4ActionFlag)
{
    tLldpLocPortInfo   *pLocPortInfo = NULL;
    tLldpv2AgentToLocPort *pAgentToLocPort = NULL;
    tLldpv2AgentToLocPort AgentToLocPort;
    INT4                i4RetVal = OSIX_FAILURE;

    MEMSET (&AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
    AgentToLocPort.i4IfIndex = (INT4) u4IfIndex;
    pAgentToLocPort = (tLldpv2AgentToLocPort *)
        RBTreeGetNext (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                       &AgentToLocPort, LldpAgentToLocPortUtlRBCmpInfo);
    while ((pAgentToLocPort != NULL)
           && (pAgentToLocPort->i4IfIndex == (INT4) u4IfIndex))
    {
        AgentToLocPort.i4IfIndex = pAgentToLocPort->i4IfIndex;
        MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                pAgentToLocPort->Lldpv2DestMacAddress, MAC_ADDR_LEN);
        pLocPortInfo = LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);
        if (pLocPortInfo != NULL)
        {
            LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                           "LldpQueHandleProtoVlanId:Updating"
                           " Portocol vlan id for port %d\r\n", u4IfIndex);

            if (u4ActionFlag == LLDP_ADD)
            {
                if (LldpTxUtlAddProtoVlanEntry (pLocPortInfo, u2VlanId)
                    == OSIX_SUCCESS)
                {
                    i4RetVal = OSIX_SUCCESS;
                }
            }
            else if ((u4ActionFlag == LLDP_DELETE)
                     || (u4ActionFlag == LLDP_UPDATE))
            {
                if (LldpTxUtlUpdateProtoVlanEntry (pLocPortInfo, u2VlanId,
                                                   u2OldVlanId, u4ActionFlag)
                    == OSIX_SUCCESS)
                {
                    i4RetVal = OSIX_SUCCESS;
                }
            }

            if (i4RetVal == OSIX_SUCCESS)
            {
                if (LldpTxUtlHandleLocPortInfoChg (pLocPortInfo) !=
                    OSIX_SUCCESS)
                {
                    LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                                   "LldpQueHandleProtoVlanId:"
                                   "LldpTxUtlHandleLocPortInfoChg failed for"
                                   "port %d\r\n", pLocPortInfo->u4LocPortNum);
                    i4RetVal = OSIX_FAILURE;
                }
            }
        }
        pAgentToLocPort = (tLldpv2AgentToLocPort *)
            RBTreeGetNext (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                           &AgentToLocPort, LldpAgentToLocPortUtlRBCmpInfo);
    }
    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpQueHandleVlanInfoChgForPort 
 *                                                                          
 *    DESCRIPTION      : This function handles vlan information change
 *                       (local port information change) for the given port.
 *
 *    INPUT            : pLocPortEntry - Pointer to local port info structure
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/

INT4
LldpQueHandleVlanInfoChgForPort (tLldpLocPortInfo * pLocPortEntry)
{
    if (pLocPortEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                   "LldpQueHandleVlanInfoChgForPort :Handling Vlan name"
                   " change notifictaion for port %d\r\n",
                   pLocPortEntry->u4LocPortNum);
    if (LldpTxUtlHandleLocPortInfoChg (pLocPortEntry) != OSIX_SUCCESS)
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                       "LldpQueHandleVlanInfoChgForPort:"
                       "LldpTxUtlHandleLocPortInfoChg failed for"
                       "port %d\r\n", pLocPortEntry->u4LocPortNum);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpQueHandleVlanInfoChg 
 *                                                                          
 *    DESCRIPTION      : This function handles vlan information change(local 
 *                       port information change) for the ports in the 
 *                       portlist.
 *
 *    INPUT            : EggressPortList - List of ports for which the 
 *                                         configured vlan name changed
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/

INT4
LldpQueHandleVlanInfoChg (tPortList EggressPortList)
{
    tLldpLocPortInfo   *pLocPortEntry = NULL;
    tLldpv2AgentToLocPort AgentToLocPort;
    tLldpv2AgentToLocPort *pAgentToLocPort = NULL;
    UINT4               u4IfIndex;
    UINT2               u2ByteIndex;
    UINT2               u2BitIndex;
    UINT1               u1PortFlag;

    MEMSET (&AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));

    for (u2ByteIndex = 0; u2ByteIndex < BRG_PORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = EggressPortList[u2ByteIndex];
        for (u2BitIndex = 0; ((u2BitIndex < LLDP_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & LLDP_BIT8) != 0)
            {
                u4IfIndex = (UINT4) ((u2ByteIndex * LLDP_PORTS_PER_BYTE) +
                                     u2BitIndex + 1);
                AgentToLocPort.i4IfIndex = (INT4) u4IfIndex;
                pAgentToLocPort = (tLldpv2AgentToLocPort *)
                    RBTreeGetNext (gLldpGlobalInfo.
                                   Lldpv2AgentToLocPortMapTblRBTree,
                                   &AgentToLocPort,
                                   LldpAgentToLocPortUtlRBCmpInfo);
                while ((pAgentToLocPort != NULL)
                       && (pAgentToLocPort->i4IfIndex == (INT4) u4IfIndex))
                {
                    AgentToLocPort.i4IfIndex = pAgentToLocPort->i4IfIndex;
                    MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                            pAgentToLocPort->Lldpv2DestMacAddress,
                            MAC_ADDR_LEN);
                    pLocPortEntry =
                        LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);
                    if (pLocPortEntry != NULL)
                    {
                        LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                                       "LldpQueHandleVlanInfoChg:Handling Vlan name"
                                       " change notifictaion for port %d\r\n",
                                       u4IfIndex);
                        if (LldpTxUtlHandleLocPortInfoChg (pLocPortEntry)
                            != OSIX_SUCCESS)
                        {
                            LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                                           "LldpQueHandleVlanInfoChg:"
                                           "LldpTxUtlHandleLocPortInfoChg failed"
                                           "for port %d\r\n",
                                           pLocPortEntry->u4LocPortNum);
                        }
                    }
                    pAgentToLocPort = (tLldpv2AgentToLocPort *)
                        RBTreeGetNext (gLldpGlobalInfo.
                                       Lldpv2AgentToLocPortMapTblRBTree,
                                       &AgentToLocPort,
                                       LldpAgentToLocPortUtlRBCmpInfo);
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpQueHandlePortAggCapability
 *                                                                          
 *    DESCRIPTION      : This function updates the change in the aggregation 
 *                       capability of the port.
 *
 *    INPUT            : pLocPortEntry - Pointer to local port info structure 
 *                       u1AggCap - Aggregation Capability  
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
INT4
LldpQueHandlePortAggCapability (tLldpLocPortInfo * pLocPortEntry,
                                UINT1 u1AggCap)
{
    UINT1              *pu1AggStatus = 0;

    if (pLocPortEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC, "LldpQueHandlePortAggCapability:Updating"
                   " Port Aggregation Capability for port %d\r\n",
                   pLocPortEntry->u4LocPortNum);

    /* bit 0 of pu1AggStatus represents Aggregation capability and bit 1 
     * represents aggregation status.
     */
    pu1AggStatus = &(LLDP_LOC_PORT_AGG_INFO (pLocPortEntry));

    if (u1AggCap == LLDP_AGG_CAPABLE)
    {
        *pu1AggStatus = *pu1AggStatus | LLDP_AGG_CAP_BMAP;
    }
    else
    {
        /* Port is not capable of aggregation. Hence set both the capability
         * and status bit to zero */
        *pu1AggStatus = *pu1AggStatus & LLDP_RESET_AGG_CAP;
    }
    if (LldpTxUtlHandleLocPortInfoChg (pLocPortEntry) != OSIX_SUCCESS)
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                       "LldpQueHandlePortAggCapability:"
                       "LldpTxUtlHandleLocPortInfoChg failed"
                       "for port %d\r\n", pLocPortEntry->u4LocPortNum);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpQueHandleResetAggCapOnPorts 
 *                                                                          
 *    DESCRIPTION      : This function resets the aggregation capability for
 *                       the ports in the list. This case occurs when the port-
 *                       channel itself is removed.
 *
 *    INPUT            : AggConfPorts - List of ports configured to the port-
 *                                      channel
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/

INT4
LldpQueHandleResetAggCapOnPorts (tPortList AggConfPorts)
{
    tLldpLocPortInfo   *pLocPortEntry = NULL;
    tLldpv2AgentToLocPort AgentToLocPort;
    tLldpv2AgentToLocPort *pAgentToLocPort = NULL;
    UINT4               u4IfIndex = 0;
    UINT1              *pu1AggStatus = 0;
    UINT2               u2ByteIndex;
    UINT2               u2BitIndex;
    UINT1               u1PortFlag;

    LLDP_TRC (CONTROL_PLANE_TRC,
              "LldpQueHandleResetAggCapOnPorts:Reseting Port "
              "Aggregation \r\n");

    MEMSET (&AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
    for (u2ByteIndex = 0; u2ByteIndex < BRG_PORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = AggConfPorts[u2ByteIndex];
        for (u2BitIndex = 0; ((u2BitIndex < LLDP_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & LLDP_BIT8) != 0)
            {
                u4IfIndex = (UINT4) ((u2ByteIndex * LLDP_PORTS_PER_BYTE) +
                                     u2BitIndex + 1);
                MEMSET (&AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
                AgentToLocPort.i4IfIndex = (INT4) u4IfIndex;

                pAgentToLocPort = (tLldpv2AgentToLocPort *)
                    RBTreeGetNext (gLldpGlobalInfo.
                                   Lldpv2AgentToLocPortMapTblRBTree,
                                   &AgentToLocPort,
                                   LldpAgentToLocPortUtlRBCmpInfo);
                while ((pAgentToLocPort != NULL)
                       && (pAgentToLocPort->i4IfIndex == (INT4) u4IfIndex))
                {
                    AgentToLocPort.i4IfIndex = pAgentToLocPort->i4IfIndex;
                    MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                            pAgentToLocPort->Lldpv2DestMacAddress,
                            MAC_ADDR_LEN);
                    pLocPortEntry =
                        LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);
                    if (pLocPortEntry != NULL)
                    {
                        pu1AggStatus =
                            &(LLDP_LOC_PORT_AGG_INFO (pLocPortEntry));
                        *pu1AggStatus = *pu1AggStatus & LLDP_RESET_AGG_CAP;
                        if (LldpTxUtlHandleLocPortInfoChg (pLocPortEntry)
                            != OSIX_SUCCESS)
                        {
                            LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                                           "LldpQueHandleResetAggCapOnPorts:"
                                           "LldpTxUtlHandleLocPortInfoChg failed"
                                           "for port %d\r\n",
                                           pLocPortEntry->u4LocPortNum);
                        }
                    }
                    pAgentToLocPort = (tLldpv2AgentToLocPort *)
                        RBTreeGetNext (gLldpGlobalInfo.
                                       Lldpv2AgentToLocPortMapTblRBTree,
                                       &AgentToLocPort,
                                       LldpAgentToLocPortUtlRBCmpInfo);
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpQueHandlePortAggStatus
 *                                                                          
 *    DESCRIPTION      : This function updates the change in the aggregation 
 *                       status of the port.
 *
 *    INPUT            : pLocPortEntry - Pointer to local port info structure 
 *                       u1AggStatus   - Aggregation Status 
 *                                       (LLDP_AGG_ACTIVE_PORT /
 *                                        LLDP_AGG_NOT_ACTIVE_PORT)
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
INT4
LldpQueHandlePortAggStatus (tLldpLocPortInfo * pLocPortInfo, UINT1 u1AggStatus)
{
    UINT1              *pu1AggStatus = 0;
    UINT2               u2AggId = 0;

    if (pLocPortInfo == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpQueHandlePortAggStatus:"
                  " pLocPortEntry NULL pointer received\r\n");
        return OSIX_FAILURE;
    }

    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC, "LldpQueHandlePortAggStatus:Updating Port"
                   " Aggregation Status for port %d\r\n",
                   pLocPortInfo->u4LocPortNum);

    /* bit 0 of pu1AggStatus represents Aggregation capability and bit 1 
     * represents aggregation status.
     */
    pu1AggStatus = &(LLDP_LOC_PORT_AGG_INFO (pLocPortInfo));

    /*port is not in port channel set the AggPortId value to zero */
    pLocPortInfo->Dot3LocPortInfo.u4AggPortId = u2AggId;

    if (u1AggStatus == LLDP_AGG_ACTIVE_PORT)
    {
        *pu1AggStatus = *pu1AggStatus | LLDP_AGG_STATUS_BMAP;

        /* port is in port channel get the AggId and set it */
        if (L2IwfGetPortChannelForPort (pLocPortInfo->i4IfIndex, &u2AggId) ==
            L2IWF_SUCCESS)
        {
            pLocPortInfo->Dot3LocPortInfo.u4AggPortId = u2AggId;
        }

    }
    else
    {
        /* Port is not active in aggregation. Hence set the status bit to zero */
        *pu1AggStatus = *pu1AggStatus & LLDP_RESET_AGG_STATUS_BMAP;
    }
    if (LldpTxUtlHandleLocPortInfoChg (pLocPortInfo) != OSIX_SUCCESS)
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                       "LldpQueHandlePortAggStatus:"
                       "LldpTxUtlHandleLocPortInfoChg failed"
                       "for port %d\r\n", pLocPortInfo->u4LocPortNum);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpQueHandleApplPortRequest
 *                                                                          
 *    DESCRIPTION      : This function handles the ports specific requests 
 *                       from applications. This function either adds, updates,
 *                       deletes the applications from a port in LLDP
 *
 *    INPUT            : u4PortId - Port number
 *                       pLocPortEntry - Pointer to local port info structure 
 *                       pLldpAppPortMsg - Port specific application message 
 *                       u1Request -
 *                       LLDP_APPL_PORT_REG_REQ - Register application to a 
 *                                                    port
 *                       LLDP_APPL_PORT_UPDATE_REQ - Update application on a
 *                                                   port
 *                       LLDP_APPL_PORT_DEREG_REQ - To de-register application
 *                                                  from a port
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
INT4
LldpQueHandleApplPortRequest (UINT4 u4PortId, tLldpAppPortMsg * pLldpAppPortMsg,
                              UINT1 u1Request)
{
    tLldpLocPortInfo   *pLocPortInfo = NULL;
    tLldpAppInfo        TmpLldpAppNode;
    tLldpAppInfo       *pLldpAppNode = NULL;
    tLldpv2AgentToLocPort AgentToLocPort;
    tLldpv2AgentToLocPort *pAgentToLocPort = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    tLldpv2DestAddrTbl *pDestTbl = NULL;
    UINT2               au2ConfPorts[L2IWF_MAX_PORTS_PER_CONTEXT] = { 0 };
    UINT2               u2NumPorts = 0;
    UINT2               u2Count = 0;
    BOOL1               bTlvTxStatus = OSIX_TRUE;
    BOOL1               bPrevAppTlvTxStatus = OSIX_TRUE;
    UINT1               u1FatalError = 0;
    UINT4               u4InstanceId = 0;
    UINT1               au1NullMacAddr[MAC_ADDR_LEN] = { 0 };

    MEMSET (&AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    /* u4PortId can be physical port or LAG.
     * so if u4PortId == LAG then iterate its member 
     * ports and register the application */

    /* Iterate the member ports if the registered port is a port channel */
    if (LLDP_IS_PORT_CHANNEL (u4PortId) == OSIX_TRUE)
    {
        LldpPortGetPortsForPortChannel ((UINT2) u4PortId,
                                        au2ConfPorts, &u2NumPorts);

        if (u2NumPorts == 0)
        {
            LLDP_TRC_ARG1 (CONTROL_PLANE_TRC, "LldpQueHandleApplPortRequest:"
                           " Application request on port-channel port %d with "
                           "no members, so returning\r\n", u4PortId);
            /* Port channel just got created, so return */
            return OSIX_SUCCESS;
        }
    }
    else
    {
        au2ConfPorts[0] = (UINT2) u4PortId;
        u2NumPorts = 1;
    }

    LLDP_TRC_ARG2 (CONTROL_PLANE_TRC,
                   "LldpQueHandleApplPortRequest:"
                   "Application request %d received on port %d\r\n",
                   u1Request, u4PortId);

    do
    {
        DestTbl.u4LlldpV2DestAddrTblIndex = pLldpAppPortMsg->u4LldpInstSelector;
        pDestTbl =
            (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                              Lldpv2DestMacAddrTblRBTree,
                                              &DestTbl);
        if (pDestTbl == NULL)
        {
            return OSIX_FAILURE;
        }
        AgentToLocPort.i4IfIndex = (INT4) au2ConfPorts[u2Count];
        MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                pDestTbl->Lldpv2DestMacAddress, MAC_ADDR_LEN);
        pAgentToLocPort =
            (tLldpv2AgentToLocPort *) RBTreeGet (gLldpGlobalInfo.
                                                 Lldpv2AgentToLocPortMapTblRBTree,
                                                 &AgentToLocPort);
        if (pAgentToLocPort != NULL)
        {
            pLocPortInfo =
                LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);
            if (pLocPortInfo == NULL)
            {
                LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpQueHandleApplPortRequest:"
                               " Local port info is not present for port: "
                               "%d\r\n", AgentToLocPort.i4IfIndex);
                return OSIX_FAILURE;
            }

            MEMSET (&TmpLldpAppNode, 0, sizeof (tLldpAppInfo));

            /* Ensure the application is not registered already */

            TmpLldpAppNode.u4PortId = au2ConfPorts[u2Count];
            MEMCPY (&TmpLldpAppNode.LldpAppId, &pLldpAppPortMsg->LldpAppId,
                    sizeof (tLldpAppId));

            pLldpAppNode =
                (tLldpAppInfo *) RBTreeGet (gLldpGlobalInfo.AppRBTree,
                                            (tRBElem *) & TmpLldpAppNode);

            switch (u1Request)
            {
                case LLDP_APPL_PORT_REG_REQ:
                    if (pLldpAppNode == NULL)
                    {
                        /* Application is not present, add it */

                        if ((pLldpAppNode = (tLldpAppInfo *)
                             MemAllocMemBlk (gLldpGlobalInfo.AppTblPoolId)) ==
                            NULL)
                        {
                            LLDP_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                           "LldpQueHandleApplPortRequest:"
                                           "Memory allocation for application table"
                                           "failed on port %d\r\n",
                                           au2ConfPorts[u2Count]);
                            return OSIX_FAILURE;
                        }

                        MEMSET (pLldpAppNode, 0, sizeof (tLldpAppInfo));
                        /* Get the buffer for application TLV */

                        if ((pLldpAppNode->pu1TxAppTlv = (UINT1 *)
                             MemAllocMemBlk (gLldpGlobalInfo.AppTlvPoolId)) ==
                            NULL)
                        {
                            LLDP_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                           "LldpQueHandleApplPortRequest:"
                                           "Memory allocation for application TLV "
                                           "failed on port %d\r\n",
                                           au2ConfPorts[u2Count]);
                            /* Release App. Tbl Node */
                            MemReleaseMemBlock (gLldpGlobalInfo.AppTblPoolId,
                                                (UINT1 *) pLldpAppNode);
                            return OSIX_FAILURE;
                        }

                        MEMSET (pLldpAppNode->pu1TxAppTlv, 0,
                                sizeof (LLDP_MAX_APP_TLV_LEN));

                        MEMCPY (&pLldpAppNode->LldpAppId,
                                &pLldpAppPortMsg->LldpAppId,
                                sizeof (tLldpAppId));

                        pLldpAppNode->u4PortId = (UINT4) au2ConfPorts[u2Count];

                        pLldpAppNode->pAppCallBackFn =
                            pLldpAppPortMsg->pAppCallBackFn;

                        MEMCPY (pLldpAppNode->pu1TxAppTlv,
                                pLldpAppPortMsg->pu1TxAppTlv,
                                pLldpAppPortMsg->u2TxAppTlvLen);

                        pLldpAppNode->u2TxAppTlvLen =
                            pLldpAppPortMsg->u2TxAppTlvLen;

                        if (MEMCMP
                            (pLldpAppPortMsg->au1LldpApplSpecMac,
                             au1NullMacAddr, MAC_ADDR_LEN) != 0)
                        {
                            /* MAC address to be registered with LLDP from
                             * application */
                            LldpUtilSetDstMac ((INT4) u4PortId,
                                               pLldpAppPortMsg->
                                               au1LldpApplSpecMac,
                                               CREATE_AND_GO, &u1FatalError);
                            /* Instance id is set for this MAC address which
                             * will be used when the packet needs to be posted to
                             * the application */
                            LldpApiGetInstanceId
                                (pLldpAppPortMsg->au1LldpApplSpecMac,
                                 &u4InstanceId);
                            pLldpAppPortMsg->u4LldpInstSelector = u4InstanceId;
                            AgentToLocPort.i4IfIndex =
                                (INT4) au2ConfPorts[u2Count];
                            MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                                    pLldpAppPortMsg->au1LldpApplSpecMac,
                                    MAC_ADDR_LEN);
                            pAgentToLocPort = (tLldpv2AgentToLocPort *)
                                RBTreeGet (gLldpGlobalInfo.
                                           Lldpv2AgentToLocPortMapTblRBTree,
                                           &AgentToLocPort);
                            if (pAgentToLocPort != NULL)
                            {
                                /* Newly created pLocPortInfo is passed
                                 * in so that LldpTxUtlHandleLocPortInfoChg
                                 * TLV is updated with APP info only
                                 * for this pLocPortInfo node. */
                                pLocPortInfo = LLDP_GET_LOC_PORT_INFO
                                    (pAgentToLocPort->u4LldpLocPort);
                                if (pLocPortInfo == NULL)
                                {
                                    LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                                                   "LldpQueHandleApplPortRequest:"
                                                   " Local Port info is not present "
                                                   "for port %d\r\n", u4PortId);
                                }
                            }

                        }
                        pLldpAppNode->u4LldpInstSelector =
                            pLldpAppPortMsg->u4LldpInstSelector;

                        pLldpAppNode->bAppTlvTxStatus =
                            pLldpAppPortMsg->bAppTlvTxStatus;

                        pLldpAppNode->u1AppTlvRxStatus =
                            pLldpAppPortMsg->u1AppTlvRxStatus;

                        pLldpAppNode->u1AppPDURecv =
                            pLldpAppPortMsg->u1AppPDURecv;
                        pLldpAppNode->u1AppNeighborExistenceNotify =
                            pLldpAppPortMsg->u1AppNeighborExistenceNotify;

                        pLldpAppNode->u1AppRefreshFrameNotify =
                            pLldpAppPortMsg->u1AppRefreshFrameNotify;
                        pLldpAppNode->u1TakeTlvFrmApp =
                            pLldpAppPortMsg->u1TakeTlvFrmApp;

                        pLldpAppNode->pAppCallBackTakeTlvFrmAppFn =
                            pLldpAppPortMsg->pAppCallBackTakeTlvFrmAppFn;

                        /* For this embedded Tree it is ensured that RBTreeGET has 
                         * returned NULL, so call to RBTreeAdd will not fail. */

                        if (RBTreeAdd (gLldpGlobalInfo.AppRBTree,
                                       (tRBElem *) pLldpAppNode) != RB_SUCCESS)
                        {
                            LLDP_TRC (ALL_FAILURE_TRC,
                                      "LldpQueHandleApplPortRequest:"
                                      "Failed to add application node into RB Tree\r\n");
                        }

                        /* If application TLV needs to be sent then 
                         * LldpTxUtlHandleLocPortInfoChg is invoked to include the 
                         * application TLV in the LLDP PDUs */
                        if (pLldpAppNode->bAppTlvTxStatus == OSIX_TRUE)
                        {
                            LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                                           "LldpQueHandleApplPortRequest:"
                                           "LldpTxUtlHandleLocPortInfoChg called "
                                           "on port %d\r\n",
                                           au2ConfPorts[u2Count]);
                            if (pLocPortInfo != NULL)
                            {
                                if (LldpTxUtlHandleLocPortInfoChg (pLocPortInfo)
                                    != OSIX_SUCCESS)
                                {
                                    LLDP_TRC_ARG3 (ALL_FAILURE_TRC,
                                                   "LldpQueHandleApplPortRequest:"
                                                   "Failed to reconstruct the PDU for port: %d"
                                                   "Mac index: %d, request type: %d\r\n",
                                                   pLocPortInfo->i4IfIndex,
                                                   pLocPortInfo->
                                                   u4DstMacAddrTblIndex,
                                                   u1Request);
                                }
                            }
                        }

                        /* Unconditionally set the pending application status 
                         * to be TRUE so that application will get the callback 
                         * for the reception of next LLDP PDU */
                        if (pLocPortInfo != NULL)
                        {
                            pLocPortInfo->bIsAnyPendingApp = OSIX_TRUE;
                        }
                    }
                    else
                    {
                        LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                                       "LldpQueHandleApplPortRequest: "
                                       "Application add failed on port %d:"
                                       "Node already found\r\n",
                                       TmpLldpAppNode.u4PortId);

                    }
                    /* Add the application if it is not registered and dont update
                     * if it is already registered */
                    break;

                case LLDP_APPL_PORT_UPDATE_REQ:
                    pLocPortInfo->bIsAnyPendingApp = OSIX_TRUE;
                    /* Update if it is already registered */
                    if (pLldpAppNode != NULL)
                    {
                        /* Update the node with the received message */
                        bPrevAppTlvTxStatus = pLldpAppNode->bAppTlvTxStatus;
                        pLldpAppNode->pAppCallBackTakeTlvFrmAppFn =
                            pLldpAppPortMsg->pAppCallBackTakeTlvFrmAppFn;

                        pLldpAppNode->u1AppTlvRxStatus =
                            pLldpAppPortMsg->u1AppTlvRxStatus;

                        pLldpAppNode->u1AppPDURecv =
                            pLldpAppPortMsg->u1AppPDURecv;

                        pLldpAppNode->u1AppNeighborExistenceNotify =
                            pLldpAppPortMsg->u1AppNeighborExistenceNotify;
                        pLldpAppNode->u1AppRefreshFrameNotify =
                            pLldpAppPortMsg->u1AppRefreshFrameNotify;

                        pLldpAppNode->pAppCallBackFn =
                            pLldpAppPortMsg->pAppCallBackFn;
                        MEMCPY (pLldpAppNode->pu1TxAppTlv,
                                pLldpAppPortMsg->pu1TxAppTlv,
                                pLldpAppPortMsg->u2TxAppTlvLen);

                        pLldpAppNode->u2TxAppTlvLen =
                            pLldpAppPortMsg->u2TxAppTlvLen;

                        pLldpAppNode->u4LldpInstSelector =
                            pLldpAppPortMsg->u4LldpInstSelector;

                        pLldpAppNode->bAppTlvTxStatus =
                            pLldpAppPortMsg->bAppTlvTxStatus;

                        pLldpAppNode->u1TakeTlvFrmApp =
                            pLldpAppPortMsg->u1TakeTlvFrmApp;

                        /* If application TLV needs to be sent then 
                         * LldpTxUtlHandleLocPortInfoChg is invoked to include the 
                         * application TLV in the LLDP PDUs */

                        /* TxStat(I/P)|PrevTxStat(I/P)|LLDP PDU Updt Required(O/P)
                         * ------------------------------------------------------
                         * OSIX_TRUE  |   OSIX_TRUE   |    YES
                         * OSIX_TRUE  |   OSIX_FALSE  |    YES
                         * OSIX_FALSE |   OSIX_TRUE   |    YES
                         * OSIX_FALSE |   OSIX_FALSE  |    NO
                         *
                         * Above table follows binary OR algorithm */
                        if ((bPrevAppTlvTxStatus | pLldpAppNode->
                             bAppTlvTxStatus) == OSIX_TRUE)
                        {
                            LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                                           "LldpQueHandleApplPortRequest:"
                                           "LldpTxUtlHandleLocPortInfoChg called"
                                           "for port %d\r\n",
                                           au2ConfPorts[u2Count]);
                            if (LldpTxUtlHandleLocPortInfoChg (pLocPortInfo)
                                != OSIX_SUCCESS)
                            {
                                LLDP_TRC_ARG3 (ALL_FAILURE_TRC,
                                               "LldpQueHandleApplPortRequest:"
                                               "Failed to reconstruct the PDU for port: %d "
                                               "Mac index: %d, request type: %d\r\n",
                                               pLocPortInfo->i4IfIndex,
                                               pLocPortInfo->
                                               u4DstMacAddrTblIndex, u1Request);

                            }
                        }
                    }
                    else
                    {
                        LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                                       "LldpQueHandleApplPortRequest: "
                                       "Application update failed on port %d:"
                                       "Node not found\r\n",
                                       TmpLldpAppNode.u4PortId);
                    }
                    break;

                case LLDP_APPL_PORT_DEREG_REQ:
                    if (pLldpAppNode != NULL)
                    {
                        /* If this node was part of LLDP PDU so invoke 
                         * LldpTxUtlHandleLocPortInfoChg to form the new 
                         * LLDP PDU which will remove this node's TLV 
                         * from the PDUs */

                        bTlvTxStatus = pLldpAppNode->bAppTlvTxStatus;

                        pLldpAppNode->u1AppTlvRxStatus =
                            pLldpAppPortMsg->u1AppTlvRxStatus;

                        pLldpAppNode->u1AppPDURecv =
                            pLldpAppPortMsg->u1AppPDURecv;
                        pLldpAppNode->u1AppNeighborExistenceNotify =
                            pLldpAppPortMsg->u1AppNeighborExistenceNotify;

                        pLldpAppNode->u1AppRefreshFrameNotify =
                            pLldpAppPortMsg->u1AppRefreshFrameNotify;
                        pLldpAppNode->u1TakeTlvFrmApp =
                            pLldpAppPortMsg->u1TakeTlvFrmApp;

                        pLldpAppNode->pAppCallBackTakeTlvFrmAppFn =
                            pLldpAppPortMsg->pAppCallBackTakeTlvFrmAppFn;
                        if (MEMCMP
                            (pLldpAppPortMsg->au1LldpApplSpecMac,
                             au1NullMacAddr, MAC_ADDR_LEN) == 0)
                        {
                            RBTreeRemove (gLldpGlobalInfo.AppRBTree,
                                          (UINT1 *) pLldpAppNode);

                            MemReleaseMemBlock (gLldpGlobalInfo.AppTlvPoolId,
                                                (UINT1 *) pLldpAppNode->
                                                pu1TxAppTlv);
                            MemReleaseMemBlock (gLldpGlobalInfo.AppTblPoolId,
                                                (UINT1 *) pLldpAppNode);
                        }

                        if (bTlvTxStatus == OSIX_TRUE)
                        {
                            LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                                           "LldpQueHandleApplPortRequest:"
                                           "LldpTxUtlHandleLocPortInfoChg called "
                                           "for port %d\r\n",
                                           au2ConfPorts[u2Count]);
                            /* Ensure this application TLV is removed from 
                             * the LLDP PDUs */
                            if (LldpTxUtlHandleLocPortInfoChg (pLocPortInfo)
                                != OSIX_SUCCESS)
                            {
                                LLDP_TRC_ARG3 (ALL_FAILURE_TRC,
                                               "LldpQueHandleApplPortRequest:"
                                               "Failed to reconstruct the PDU for port: %d"
                                               "Mac index: %d, request type: %d\r\n",
                                               pLocPortInfo->i4IfIndex,
                                               pLocPortInfo->
                                               u4DstMacAddrTblIndex, u1Request);
                            }
                            /* Dont return proceed to next port to de-register */
                        }
                        if (MEMCMP
                            (pLldpAppPortMsg->au1LldpApplSpecMac,
                             au1NullMacAddr, MAC_ADDR_LEN) != 0)
                        {

                            /* App in fo is removed inside this function and also 
                             * local port info created for particular dest mac-addr */
                            LldpUtilDeletePortInfo (pLocPortInfo);
                        }

                        LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                                       "LldpQueHandleApplPortRequest:"
                                       "De-Register success on port %d\r\n",
                                       TmpLldpAppNode.u4PortId);
                    }
                    else
                    {
                        LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                                       "LldpQueHandleApplPortRequest:"
                                       "Node not found, De-Register failed for "
                                       "port %d\r\n", TmpLldpAppNode.u4PortId);
                    }
                    break;

                default:
                    LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                                   "LldpQueHandleApplPortRequest:"
                                   " Invalid application port request received "
                                   "for port %d\r\n", TmpLldpAppNode.u4PortId);
                    return OSIX_FAILURE;
            }                    /* End of switch-case */
        }
        u2Count++;
    }
    while (u2Count < u2NumPorts);    /* Scan till the last member port */

    LLDP_TRC_ARG2 (CONTROL_PLANE_TRC,
                   "LldpQueHandleApplPortRequest:"
                   "Application request %d complete on port %d\r\n",
                   u1Request, u4PortId);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpQueUpdtIfAlias 
 *                                                                          
 *    DESCRIPTION      : This function is to update the port id or chassis id
 *                       whenever if-alias change notificationis received. 
 *
 *    INPUT            : pu1Src - pointer to source information(if-alias) to be
 *                                updated 
 *                       i4MaxLen - max allowed lengh of chassis id/port id
 *                                                                          
 *    OUTPUT           : pu1Dest - pointer to updated value(chassis id/port id)
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
VOID
LldpQueUpdtIfAlias (UINT1 *pu1Dest, UINT1 *pu1Src, INT4 i4MaxLen)
{
    MEMSET (pu1Dest, 0, i4MaxLen);
    STRNCPY (pu1Dest, pu1Src, i4MaxLen);
    return;
}

/*************************************************************************
 * Function Name : LldpQueHandlePortDesc
 * 
 * Description : This function is called when Port Description is changed
 *
 * Inputs : pu1PortDesc - Port Description to be changed
 *         u4IfIndex - Interface Index
 *
 * Output : None
 * 
 * Returns : OSIX_SUCCESS/OSIX_FAILURE 
 * **********************************************************************/
INT4
LldpQueHandlePortDesc (UINT1 *pu1PortDesc, UINT4 u4IfIndex)
{
    tLldpLocPortTable  *pLldpPortTable = NULL;
    tLldpLocPortTable   LldpPortTable;

    if (pu1PortDesc == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpQueHandlePortDesc : NULL pointer "
                  "received\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&LldpPortTable, 0, sizeof (tLldpLocPortTable));

    LldpPortTable.i4IfIndex = (INT4) u4IfIndex;
    pLldpPortTable = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpPortTable);
    if (pLldpPortTable == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "LldpQueHandlePortDesc: Table Does not exsist!!\n");
        return OSIX_FAILURE;
    }
    if (STRNCMP
        (pLldpPortTable->au1LocPortDesc, pu1PortDesc,
         LLDP_MAX_LEN_PORTDESC) != 0)
    {
        MEMSET (pLldpPortTable->au1LocPortDesc, 0, LLDP_MAX_LEN_PORTDESC);
        STRNCPY (pLldpPortTable->au1LocPortDesc, pu1PortDesc,
                 LLDP_MAX_LEN_PORTDESC);
        LldpTxUtlHandleLocSysInfoChg ();
    }
    return OSIX_SUCCESS;
}

#endif /* _LLDPQUE_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  lldque.c                       */
/*-----------------------------------------------------------------------*/
