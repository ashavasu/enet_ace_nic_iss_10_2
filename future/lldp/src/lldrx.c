/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldrx.c,v 1.44 2017/11/22 12:33:38 siva Exp $
 *
 * Description: This function contains all the LLDP Receive module related 
 *              Procedures.
 *
 *****************************************************************************/
#ifndef _LLDP_RX_C_
#define _LLDP_RX_C_

#include "lldinc.h"

extern INT4         DcbxApiVerifyTlvisDcbx (UINT2, UINT1, UINT1 *);
#ifdef DCBX_WANTED
extern INT4         DcbxApiVerifyTlvIsCee (UINT1 *, UINT4);
extern INT4         DcbxApiVerifyTlvIsIeee (UINT1 *, UINT4);
UINT1               gu1DcbxTlvBitMap = 0;
#endif
/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxComparePduMsgDigest
 *
 *    DESCRIPTION      : This function compares the Message Digest of currently
 *                       Received LLDPDU with existing Digest value  present in 
 *                       the Remote Node structure.
 *
 *
 *    INPUT            : pu1RecvDigest - Digest of the received lldpdu.
 *                       pu1ExDigest   - Existing Message Digest value 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS - if current chksum matches with existing
 *                                      chksum stored in Remote Node.
 *                      otherwise return  OSIX_FAILURE.
 *
 ****************************************************************************/
INT4
LldpRxComparePduMsgDigest (UINT1 *pu1RecvDigest, UINT1 *pu1ExDigest)
{
    INT4                i4RetVal = LLDP_INVALID_VALUE;

    if (pu1ExDigest != NULL)
    {
        i4RetVal = MEMCMP (pu1RecvDigest, pu1ExDigest, 16);
        if (i4RetVal == 0)
        {
            /* Received digest matches the calculated digest */
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUpdateRemoteLink
 *
 *    DESCRIPTION      : This Function Update all the pointer present 
 *                       in the Remote Node.
 *                       These pointers points to the First Node
 *                       of a associated branch in other Remote Tables. Through
 *                       these pointers all the Remote Information regarding a 
 *                       particular neighbor can be accessed.
 *
 *    INPUT            : pRemoteNode -  Remote Node Pointer
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpRxUpdateRemoteLink (tLldpRemoteNode * pRemoteNode)
{
    if (pRemoteNode != NULL)
    {
        pRemoteNode->pRemManAddr = LldpRxUtlGetFirstManAddr (pRemoteNode);
        pRemoteNode->pRemUnknownTLV = LldpRxUtlGetFirstUnknownTLV (pRemoteNode);
        pRemoteNode->pRemOrgDefInfo = LldpRxUtlGetFirstOrgDefInfo (pRemoteNode);
        pRemoteNode->pDot1RemVlanNameInfo =
            LldpRxUtlGetFirstVlanName (pRemoteNode);
        pRemoteNode->pDot1RemProtoVlanInfo =
            LldpRxUtlGetFirstProtoVlan (pRemoteNode);
        pRemoteNode->pDot1RemProtocolInfo =
            LldpRxUtlGetFirstProtoId (pRemoteNode);
        /* pRemoteNode->pDot3RemPortInfo will updated during 
         * TLV processing time*/
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxGetMSAPIdFromPdu
 *
 *    DESCRIPTION      : This function decode the chassis id and port id tlv
 *                       from the LLDPDU and return the Chassis Id and Port Id.
 *
 *    INPUT            : pu1Lldpdu - pointer to the LLDPDU Buffer
 *
 *    OUTPUT           : pu1ChassisId - Return the Chassis ID
 *                       au1PortId - Return the Port ID
 *
 *    RETURNS          :
 *
 ****************************************************************************/
PUBLIC INT4
LldpRxGetMSAPIdFromPdu (UINT1 *pu1Lldpdu, INT4 *pi4ChassisIdSubtype,
                        UINT1 *pu1ChassisId, INT4 *pi4PortIdSubtype,
                        UINT1 *pu1PortId)
{
    UINT1              *pu1TlvInfo = NULL;
    UINT2               u2TlvType = 0;
    UINT2               u2TlvInfoLen = 0;
    UINT2               u2ChassisIdLen = 0;
    UINT2               u2PortIdLen = 0;

    /* Process the ChssisId TLV first to get the ChssisId */
    pu1TlvInfo = pu1Lldpdu;

    /* Get the TLV Header values */
    LLDP_GET_TLV_TYPE_LENGTH (pu1TlvInfo, &u2TlvType, &u2TlvInfoLen);
    pu1TlvInfo = pu1TlvInfo + LLDP_TLV_HEADER_LEN;

    if (u2TlvType != LLDP_CHASSIS_ID_TLV)
    {
        /* Bad Frame. Discard LLDPDU */
        return OSIX_FAILURE;
    }

    /* TLV Validation */
    /* 1. TLV information string length validation - [std IEEE 802.1AB-2005,
     *    clause 10.3.2 - (a) 2 ] */
    /* u2TlvInfoLength = ChassisId Subtype + Chassis Id string info */
    /* 2 < u2TlvInfoLength < 256 */
    if ((u2TlvInfoLen < (LLDP_MIN_LEN_CHASSISID + LLDP_TLV_SUBTYPE_LEN)) ||
        (u2TlvInfoLen > LLDP_MAX_LEN_CHASSISID + LLDP_TLV_SUBTYPE_LEN))
    {
        /* Discard LLDPDU */
        return OSIX_FAILURE;
    }

    /* Get the Chassis Id Subtype */
    LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, 0, *pi4ChassisIdSubtype);
    u2ChassisIdLen = (UINT2) (u2TlvInfoLen - LLDP_TLV_SUBTYPE_LEN);

    /* Get the Chassis Id */
    LLDP_LBUF_GET_STRING (pu1TlvInfo, pu1ChassisId, 0, u2ChassisIdLen);

    /* PortId TLV */
    u2TlvType = 0;                /* reset the type variable */
    u2TlvInfoLen = 0;            /* reset the info length  variable */
    LLDP_GET_TLV_TYPE_LENGTH (pu1TlvInfo, &u2TlvType, &u2TlvInfoLen);
    pu1TlvInfo = pu1TlvInfo + LLDP_TLV_HEADER_LEN;

    if (u2TlvType != LLDP_PORT_ID_TLV)
    {
        /* Bad Frame. Discard LLDPDU */
        return OSIX_FAILURE;
    }

    /* TLV Validation */
    /* 1. TLV information string length validation - [std IEEE 802.1AB-2005,
     *    clause 10.3.2 - (b) 2 ] */
    /* u2TlvInfoLength = PortId Subtype + Port Id string info */
    /* 2 < u2TlvInfoLength < 256 */
    if ((u2TlvInfoLen < LLDP_MIN_LEN_PORTID + LLDP_TLV_SUBTYPE_LEN) ||
        (u2TlvInfoLen > LLDP_MAX_LEN_PORTID + LLDP_TLV_SUBTYPE_LEN))
    {
        /* Discard LLDPDU */
        return OSIX_FAILURE;
    }

    /* Get the PortId Subtype */
    LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, 0, *pi4PortIdSubtype);
    u2PortIdLen = (UINT2) (u2TlvInfoLen - LLDP_TLV_SUBTYPE_LEN);

    /* Get the Port Id */
    LLDP_LBUF_GET_STRING (pu1TlvInfo, pu1PortId, 0, u2PortIdLen);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxProcessTlv
 *
 *    DESCRIPTION      : This routine calls the tlv specific processing 
 *                       routines. This is used during TLV Validation.
 *
 *    INPUT            : pLocPortInfo - Local Port Info Structure where the TLV
 *                                      is received.
 *                       u2TlvType - TLV Type
 *                       u2TlvLength - TLV Length
 *                       pu1Tlv -   Buffer points to the TLV
 *                       pu2MedSupTlvs - Contains the list of TLVs supported by
 *                                       the remote agent
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpRxProcessTlv (tLldpLocPortInfo * pLocPortInfo, UINT2 u2TlvType,
                  UINT2 u2TlvLength, UINT1 *pu1Tlv, UINT2 *pu2MedSupTlvs)
{
    tLldpAppInfo        TmpAppInfo;
    tLldpAppInfo       *pLldpAppNode = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    INT4                i4DupMgmtAddrCount = 0;
    UINT1               au1RxOUI[LLDP_MAX_LEN_OUI] = { 0 };
    UINT1               u1TlvSubType = 0;
    BOOL1               bResult = OSIX_FALSE;

    if (LLDP_IS_UNKNOWN_TLV (u2TlvType) == OSIX_TRUE)
    {
        /* Fall under Reserved Tlv Range */
        i4RetVal = LldpTlvProcUnknownTlv (pLocPortInfo, pu1Tlv, u2TlvLength);
        return i4RetVal;
    }

    MEMSET (&TmpAppInfo, 0, sizeof (tLldpAppInfo));

    /* General Validation of TLVs */
    switch (u2TlvType)
    {
        case LLDP_END_OF_LLDPDU_TLV:

            i4RetVal = LldpTlvProcEndOfPduTlv (u2TlvLength);

            break;

        case LLDP_CHASSIS_ID_TLV:
            /* Malformed PDU. Drop the LLDPDU As per */
            /* General Validation Rule Std IEEE 802.1AB-2005, clause 10.3.2.1 */
            i4RetVal = OSIX_FAILURE;
            break;

        case LLDP_PORT_ID_TLV:
            /* Malformed PDU. Drop the LLDPDU As per */
            /* General Validation Rule Std IEEE 802.1AB-2005, clause 10.3.2.1 */
            i4RetVal = OSIX_FAILURE;
            break;

        case LLDP_TIME_TO_LIVE_TLV:
            /* Malformed PDU. Drop the LLDPDU As per */
            /* General Validation Rule Std IEEE 802.1AB-2005, clause 10.3.2.1 */
            i4RetVal = OSIX_FAILURE;
            break;

        case LLDP_PORT_DESC_TLV:
            i4RetVal = LldpTlvProcPortDescTlv (pLocPortInfo, pu1Tlv,
                                               u2TlvLength);
            break;

        case LLDP_SYS_NAME_TLV:
            i4RetVal = LldpTlvProcSysNameTlv (pLocPortInfo, pu1Tlv,
                                              u2TlvLength);
            break;

        case LLDP_SYS_DESC_TLV:
            i4RetVal = LldpTlvProcSysDescTlv (pLocPortInfo, pu1Tlv,
                                              u2TlvLength);
            break;

        case LLDP_SYS_CAPAB_TLV:
            i4RetVal = LldpTlvProcSysCapaTlv (pLocPortInfo, pu1Tlv,
                                              u2TlvLength);
            break;

        case LLDP_MAN_ADDR_TLV:
            i4RetVal = LldpTlvProcManAddrTlv (pLocPortInfo, pu1Tlv,
                                              u2TlvLength);
            if (OSIX_SUCCESS == i4RetVal)
            {
                OSIX_BITLIST_IS_BIT_SET (pLocPortInfo->au1DupTlvChkFlag,
                                         (UINT2) LLDP_DUP_MGMT_ADDR_TLV,
                                         (INT4) LLDP_MAX_DUP_TLV_ARRAY_SIZE,
                                         bResult);
                if ((LLDP_RX_FRAME_REFRESH == pLocPortInfo->u1RxFrameType) &&
                    (OSIX_FALSE == bResult))
                {
                    if (OSIX_SUCCESS == (LldpUtlGetDupMgmtAddrCount
                                         (pLocPortInfo, &i4DupMgmtAddrCount)))
                    {
                        while (i4DupMgmtAddrCount > 0)
                        {
                            LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
                            if (OSIX_FALSE ==
                                pLocPortInfo->bstatsFramesInerrorsTotal)
                            {
                                LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
                                pLocPortInfo->bstatsFramesInerrorsTotal =
                                    OSIX_TRUE;
                            }
                            i4DupMgmtAddrCount--;
                        }
                        OSIX_BITLIST_SET_BIT (pLocPortInfo->au1DupTlvChkFlag,
                                              (UINT2) LLDP_DUP_MGMT_ADDR_TLV,
                                              (INT4)
                                              LLDP_MAX_DUP_TLV_ARRAY_SIZE);
                    }
                }
            }
            break;

        case LLDP_ORG_SPEC_TLV:
            i4RetVal = LldpTlvProcOrgSpecificTlv (pLocPortInfo, pu1Tlv,
                                                  u2TlvLength, au1RxOUI,
                                                  &u1TlvSubType, pu2MedSupTlvs);
            break;
        default:
            break;

            /* In each TLV, Tlvtype is 7bits long. So the max value
             * Tlvtype can have is 127(ie 0111 111). If the Tlvtype
             * is 0 to 8(basic TLV), 9 to 126(unknown TLV),
             * 127 (unknown TLV)is handled above. Hence default
             * case is not required */
    }

    /* Dont send events to applications for,
     * 1. End of TLVs
     * 2. Other TLVs whose processing has failed
     */
    if ((i4RetVal == OSIX_SUCCESS) && (u2TlvType != LLDP_END_OF_LLDPDU_TLV))
    {
        /* Update the Application table with the received TLV in 
         * the packet and set the TX status for recvd TLV as TRUE */
        TmpAppInfo.LldpAppId.u2TlvType = u2TlvType;
        MEMCPY (TmpAppInfo.LldpAppId.au1OUI, &au1RxOUI, LLDP_MAX_LEN_OUI);
        TmpAppInfo.LldpAppId.u1SubType = u1TlvSubType;
        TmpAppInfo.u4PortId = (UINT4) pLocPortInfo->i4IfIndex;

        pLldpAppNode = (tLldpAppInfo *) RBTreeGet (gLldpGlobalInfo.AppRBTree,
                                                   (tRBElem *) & TmpAppInfo);

        if (pLldpAppNode != NULL)
        {
            pLldpAppNode->pu1RxAppTlv = pu1Tlv;
            /* u2TlvLength specifies the length of the TLV excluding the 
             * lengths of TLV type & length. So add the LLDP TLV header
             * length to the this. LLDP sends the total length of the 
             * TLV to its applications */
            pLldpAppNode->u2RxAppTlvLen =
                (UINT2) (u2TlvLength + LLDP_TLV_HEADER_LEN);
            /* Set the flag, so that TLV will be sent to application */
            pLldpAppNode->bIsTlvRecvd = OSIX_TRUE;
#ifdef DCBX_WANTED
            if (OSIX_FALSE !=
                DcbxApiVerifyTlvIsCee (au1RxOUI,
                                       (UINT4) pLocPortInfo->i4IfIndex))
            {
                gu1DcbxTlvBitMap = gu1DcbxTlvBitMap | (1 << CEE_TLV_PRESENT);
            }
            if (OSIX_FALSE !=
                DcbxApiVerifyTlvIsIeee (au1RxOUI,
                                        (UINT4) pLocPortInfo->i4IfIndex))
            {
                gu1DcbxTlvBitMap = gu1DcbxTlvBitMap | (1 << IEEE_TLV_PRESENT);
            }
#endif
        }
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxProcessFrame
 *
 *    DESCRIPTION      : This function is usefull for parsing the LLDPDU,
 *                       Validating the LLDPDU and TLVs. 
 *    
 *    INPUT            : pLocPortInfo - Local Port Information Structure Pointer
 *                                      for the port where the LLDPDU 
 *                                      is received
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS - If LLDPDU validation passed
 *                       OSIX_FAILURE - If LLDPDU Validatioin failed
 *
 ****************************************************************************/
INT4
LldpRxProcessFrame (tLldpLocPortInfo * pLocPortInfo)
{
    UINT1              *pu1Tlv = NULL;
    UINT1              *pu1TlvInfo = NULL;
    UINT2               u2TlvType = 0;
    UINT2               u2TlvCount = 0;
    UINT2               u2TlvLength = 0;
    UINT2               u2RxTtl = 0;
    UINT4               u4ProcPduLen = 0;
    /* This variable is used to store the list of TLVs supported by LLDP-MED
     * remote agent. When ever LLDP-MED TLV is processed, need to ensure that
     * the TLV is present in supported TLV list of the remote agent. 
     * It will be updated only when MED capability TLV is processed.*/
    UINT2               u2MedSupTlvs = 0;

    /* Get the LLDPDU */
    pu1Tlv = pLocPortInfo->pu1RxLldpdu;

    if (pu1Tlv == NULL)
    {
        /* No LLDP Pdu found for processing */
        LLDP_TRC (ALL_FAILURE_TRC, "LldpRxProcessFrame:  No LLDP Pdu found for"
                  "processing\r\n");
        return OSIX_FAILURE;
    }
    LldpRxProcessWholePdu (pLocPortInfo);

    u2TlvCount = 1;
    /* Process Tlvs One by one */
    do
    {
        /* Get the New TLV Type from the TLV */
        LLDP_GET_TLV_TYPE_LENGTH (pu1Tlv, &u2TlvType, &u2TlvLength);

        /* Process the first 3 TLV */
        if (u2TlvCount == 1)    /* Chassis ID TLV */
        {
            /* The TLV type (for Chassis ID TLV) is verified while
             * getting MSAP id from PDU (LldpRxGetMSAPIdFromPdu).
             * Hence no need to verify here. */
            if (LldpTlvProcChassisIdTlv (pu1Tlv, u2TlvLength) != OSIX_SUCCESS)
            {
                /* Bad Frame */
                LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
                return OSIX_FAILURE;
            }
        }
        else if (u2TlvCount == 2)    /* Port ID TLV */
        {
            /* The TLV type (for Port ID TLV) is verified while
             * getting MSAP id from PDU (LldpRxGetMSAPIdFromPdu).
             * Hence no need to verify here. */
            if (LldpTlvProcPortIdTlv (pu1Tlv, u2TlvLength) != OSIX_SUCCESS)
            {
                /* Bad Frame */
                LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
                return OSIX_FAILURE;
            }
        }
        else if (u2TlvCount == 3)    /* TTL TLV */
        {
            if (u2TlvType != LLDP_TIME_TO_LIVE_TLV)
            {
                /* Bad Frame */
                return OSIX_FAILURE;
            }
            if (LldpTlvProcTtlTlv (pu1Tlv, u2TlvLength) != OSIX_SUCCESS)
            {
                /* Bad Frame */
                LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
                return OSIX_FAILURE;
            }
            /* Get the RxTTL value and store it in the port info structure, so 
             * that it can be used by the RX-SEM */
            pu1TlvInfo = pu1Tlv + LLDP_TLV_HEADER_LEN;
            LLDP_LBUF_GET_2_BYTE (pu1TlvInfo, 0, u2RxTtl);
            pLocPortInfo->i4RxTtl = u2RxTtl;
            if (pLocPortInfo->i4RxTtl == 0)
            {
                /* If shutdown frame is received, No need to process 
                 * the remaining TLVs if any present in the PDU */
                break;
            }
        }
        else                    /* Process other TLVs  */
        {
            u4ProcPduLen = (pu1Tlv - pLocPortInfo->pu1RxLldpdu) +
                LLDP_TLV_HEADER_LEN + u2TlvLength;

            /* Check the LLDPDU Physical Boundary */
            if (u4ProcPduLen > LLDP_MAX_PDU_SIZE)
            {
                /* Discard FRAME and stop processing */
                return OSIX_FAILURE;
            }

            /* Check if the processed PDU length is more than the rcvd PDU
             * length. */
            if (u4ProcPduLen > pLocPortInfo->u2RxPduLen)
            {
                /* Discard FRAME and stop processing */
                LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
                return OSIX_FAILURE;
            }

            if (LldpRxProcessTlv (pLocPortInfo, u2TlvType, u2TlvLength,
                                  pu1Tlv, &u2MedSupTlvs) != OSIX_SUCCESS)
            {
                /* Bad Frame */
                return OSIX_FAILURE;
            }
        }

        /* Increment the TLV count */
        u2TlvCount++;
        /* Update the Next Pointer */
        pu1Tlv = pu1Tlv + LLDP_TLV_HEADER_LEN + u2TlvLength;

    }
    while (u2TlvType != LLDP_END_OF_LLDPDU_TLV);

    /* Once All TLV processing is completed, check whether MAC/PHY TLV and MED capability TLV
     * is present in the PDU. If both the TLVs are present and MedCapable flag us FALSE, enable
     * Med Capable flag and reconstruct the buffer to start transmitting LLDP-MED TLVs */

    if ((LLDP_IS_SET_TLV_RCVD_BMP (pLocPortInfo, LLDP_TLV_RCVD_MAC_PHY_BMP) ==
         OSIX_TRUE)
        &&
        (LLDP_MED_IS_TLV_RCVD_BMP_SET (pLocPortInfo, LLDP_MED_RECV_MED_CAP_TLV)
         == OSIX_TRUE) && (pLocPortInfo->bMedCapable != LLDP_MED_TRUE))
    {
        pLocPortInfo->bMedCapable = LLDP_MED_TRUE;
        /* Since LLDP-MED packet is received on the port, reconstruct the
         * LLDPDU with LLDP MED enabled TLVs */
        if (pLocPortInfo->u1RxFrameType == LLDP_RX_FRAME_NEW)
        {
            if (LldpTxUtlConstructPreformedBuf (pLocPortInfo, LLDP_INFO_FRAME)
                != OSIX_SUCCESS)
            {
                LLDP_TRC (ALL_FAILURE_TRC | LLDP_MED_CAPAB_TRC,
                          "LldpRxProcessFrame: "
                          "Construction of preformed buffer Failed.\r\n");
            }
        }
        else
        {
            if (LldpTxUtlHandleLocPortInfoChg (pLocPortInfo) != OSIX_SUCCESS)
            {
                LLDP_TRC (CONTROL_PLANE_TRC | LLDP_MED_CAPAB_TRC,
                          "LldpRxProcessFrame: "
                          "LldpTxUtlHandleLocPortInfoChg failed\r\n");
            }
        }
    }

    /* As per LLDP-MED standard, if one of the TLV in Inventory TLV set is
     * present in the PDU, it is mandatory to have all other TLVs from the 
     * Inventory set to be included in the PDU. The below validation is added
     * for the same. If all TLVs are present, enable Inventory TLV in bit map.*/
    if ((pLocPortInfo->u2MedTlvRcvdBmp & LLDP_MED_INVENTORY_SET_BMP) ==
        LLDP_MED_INVENTORY_SET_BMP)
    {
        LLDP_MED_SET_TLV_RCVD_BMP (pLocPortInfo, LLDP_MED_RECV_INVENTORY_TLV);
    }

    return OSIX_SUCCESS;
}

/*--------------------------------------------------------------------------*/
/*               Remote Database Update routines                            */
/*--------------------------------------------------------------------------*/
/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUpdateRemDataBase
 *
 *    DESCRIPTION      : This function updates the Remote System Database with
 *                       the information present in the LLDPDU. All Existing
 *                       information for a particular neighbor is deleted first
 *                       and then New information are added.
 *        
 *    INPUT            : pLocPortInfo - Local Port Information Structure Pointer
 *                                      where the LLDPDU is received.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpRxUpdateRemDataBase (tLldpLocPortInfo * pLocPortInfo)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT1              *pu1Tlv = NULL;
    UINT1              *pu1TmpTlv = NULL;
    UINT2               u2TlvType = 0;
    UINT2               u2TlvLength = 0;
    BOOL1               bResult = OSIX_TRUE;

    /* Get the LLDPDU */
    pu1Tlv = pLocPortInfo->pu1RxLldpdu;

    if (pu1Tlv == NULL)
    {
        /* No LLDP Pdu found for processing */
        LLDP_TRC (ALL_FAILURE_TRC, "(LldpRxUpdateRemDataBase) "
                  "No LLDP Pdu found for processing\r\n");
        return OSIX_FAILURE;
    }

    /* Fetch Data From TLV One by one and update the Remote Database */
    do
    {
        pu1TmpTlv = pu1Tlv;

        /* Get the New TLV Type from the TLV */
        LLDP_GET_TLV_TYPE_LENGTH (pu1TmpTlv, &u2TlvType, &u2TlvLength);

        if ((u2TlvType != LLDP_CHASSIS_ID_TLV) &&
            (u2TlvType != LLDP_PORT_ID_TLV) &&
            (u2TlvType != LLDP_TIME_TO_LIVE_TLV) &&
            (u2TlvType != LLDP_END_OF_LLDPDU_TLV) &&
            (LLDP_IS_UNKNOWN_TLV (u2TlvType) != OSIX_TRUE))
            /* For mandatory TLVs no need to check for Tlv Discard
             * as in that case PDU itself will be discarded.*/
        {
            LLDP_RX_IS_SET_DISCARD_TLV (pu1Tlv, u2TlvLength, bResult);
        }
        else
        {
            bResult = OSIX_FALSE;
        }

        if (bResult == OSIX_FALSE)
        {
            if (LldpRxUpdtTlvInfo (pLocPortInfo, u2TlvType, u2TlvLength,
                                   pu1Tlv) != OSIX_SUCCESS)
            {
                LLDP_TRC (CONTROL_PLANE_TRC, "LldpRxUpdateRemDataBase: "
                          "LldpRxUpdtTlvInfo returns FAILURE\r\n");
                return OSIX_FAILURE;
            }
        }
        else
        {
            LLDP_TRC (CONTROL_PLANE_TRC, "(LldpRxUpdateRemDataBase) - "
                      "Skiping the invalid TLV\r\n");
        }
        /* Update the Next Pointer */
        pu1Tlv = pu1Tlv + LLDP_TLV_HEADER_LEN + u2TlvLength;
    }
    while (u2TlvType != LLDP_END_OF_LLDPDU_TLV);

    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;

    if (pLocPortInfo->bMedCapable == LLDP_MED_TRUE)
    {
        if (pLocPortInfo->u1RxFrameType == LLDP_RX_FRAME_NEW)
        {
            /* Enable Topology change Notification if MED Device is 
             * connected or disconnected */
            gLldpGlobalInfo.bSendTopChgNotif = OSIX_TRUE;
        }
    }
    /* All the Tlv Data are update in the Remote Tables, Update the Links in
     * the Remote Node Now*/
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;
    if (LldpRxUpdateRemoteLink (pRemoteNode) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "(LldpRxUpdateRemDataBase) - "
                  "Failed to update the Remote Links in Remote Node\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxUpdtTlvInfo
 *
 *    DESCRIPTION      : This function calls the TLV specific update routine,
 *                       to update the Remote Database with the information 
 *                       present in the TLV info field.
 *                       NODE: Skip the invalid TLVs
 *        
 *    INPUT            : pLocPortInfo  - Local Port Information Structure Pointer
 *                                       where the LLDPDU is received.
 *                       u2TlvType     - type of the TLV (Basic TLV or 
 *                                       Org Spec TLV)
 *                       u2TlvLength   - length of the TLV
 *                       pu1Tlv        - Buffer points to the TLV
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpRxUpdtTlvInfo (tLldpLocPortInfo * pLocPortInfo, UINT2 u2TlvType,
                   UINT2 u2TlvLength, UINT1 *pu1Tlv)
{
    INT4                i4RetVal = OSIX_FAILURE;

    if (LLDP_IS_UNKNOWN_TLV (u2TlvType) == OSIX_TRUE)
    {
        /* Fall under Reserved Tlv Range */
        i4RetVal = LldpTlvUpdtRemUnknownTlvInfo (pLocPortInfo, pu1Tlv,
                                                 u2TlvLength, u2TlvType);
        return i4RetVal;
    }

    switch (u2TlvType)
    {
        case LLDP_END_OF_LLDPDU_TLV:
            /* Nothing to do */
            i4RetVal = OSIX_SUCCESS;
            break;
        case LLDP_CHASSIS_ID_TLV:
            i4RetVal = LldpTlvUpdtRemChassisIdInfo (pLocPortInfo, pu1Tlv,
                                                    u2TlvLength);
            break;
        case LLDP_PORT_ID_TLV:
            i4RetVal = LldpTlvUpdtRemPortIdInfo (pLocPortInfo, pu1Tlv,
                                                 u2TlvLength);
            /* Chassis Id mis-config can be notified only after updating
             * the port id information. So checking for the ChassisId 
             * mis-config here. */
            LldpRxChkMisConfigChassId (pLocPortInfo->pBackPtrRemoteNode);
            break;
        case LLDP_TIME_TO_LIVE_TLV:
            i4RetVal = LldpTlvUpdtRemTtlInfo (pLocPortInfo, pu1Tlv,
                                              u2TlvLength);
            break;
        case LLDP_PORT_DESC_TLV:
            i4RetVal = LldpTlvUpdtRemPortDescInfo (pLocPortInfo, pu1Tlv,
                                                   u2TlvLength);
            break;
        case LLDP_SYS_NAME_TLV:
            i4RetVal = LldpTlvUpdtRemSysNameInfo (pLocPortInfo, pu1Tlv,
                                                  u2TlvLength);
            break;
        case LLDP_SYS_DESC_TLV:
            i4RetVal = LldpTlvUpdtRemSysDescInfo (pLocPortInfo, pu1Tlv,
                                                  u2TlvLength);
            break;
        case LLDP_SYS_CAPAB_TLV:
            i4RetVal = LldpTlvUpdtRemSysCapaInfo (pLocPortInfo, pu1Tlv,
                                                  u2TlvLength);
            break;
        case LLDP_MAN_ADDR_TLV:
            i4RetVal = LldpTlvUpdtRemManAddrInfo (pLocPortInfo, pu1Tlv,
                                                  u2TlvLength);
            break;
        case LLDP_ORG_SPEC_TLV:
            i4RetVal = LldpTlvUpdtRemOrgSpecificInfo (pLocPortInfo, pu1Tlv,
                                                      u2TlvLength);
            break;
        default:
            break;

            /* In each TLV, Tlvtype is 7bits long. So the max value
             * Tlvtype can have is 127(ie 0111 111). If the Tlvtype
             * is 0 to 8(basic TLV), 9 to 126(unknown TLV),
             * 127 (unknown TLV)is handled above. Hence default
             * case is not required */
    }
    return i4RetVal;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxProcessRefreshFrame
 *
 *    DESCRIPTION      : This function handles the Refresh Frame received for
 *                       a particular neighbor. Its Restart the Info Ageout 
 *                       timer for a particular neighbor.
 *
 *    INPUT            : pLocPortInfo - pointer to the Local Port Information
 *                                      Structure.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpRxProcessRefreshFrame (tLldpLocPortInfo * pLocPortInfo)
{
    tLldpRemoteNode    *pRemoteNode = NULL;

#ifdef L2RED_WANTED
    tLldpRedTmrSyncUpInfo TmrSyncUpInfo;
    tLldpRedMsapRBIndex MsapRBIndex;

    MEMSET (&TmrSyncUpInfo, 0, sizeof (tLldpRedTmrSyncUpInfo));
    MEMSET (&MsapRBIndex, 0, sizeof (tLldpRedMsapRBIndex));
#endif

    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;
    /* Restart the Info AgeOut Timer */
    if (LldpRxReStartInfoAgeTmr (pRemoteNode) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "(LldpRxProcessRefreshFrame) - Fail to Re-Start "
                  "the Info Ageout Timer.\r\n");
        return OSIX_FAILURE;
    }

#ifdef L2RED_WANTED
    /* Fill rx info age out timer restart sync up information */
    TmrSyncUpInfo.u1TmrType = LLDP_RX_TMR_RX_INFO_TTL;
    TmrSyncUpInfo.u4TmrInterval = pRemoteNode->u4RxTTL;
    LldpRedFillMsapRBIndex (&MsapRBIndex, pRemoteNode);
    TmrSyncUpInfo.pMsapRBIndex = &MsapRBIndex;
    /* Send rx info age out timer restart sync up inforamtion */
    LldpRedSendSyncUpMsg ((UINT1) LLDP_RED_TMR_START_MSG,
                          (VOID *) &TmrSyncUpInfo);
#endif
    if (OSIX_FALSE == pLocPortInfo->bIsAnyPendingApp)
    {
        LldpRxProcessFrame (pLocPortInfo);
        LldpRxPostTlvsToApplications (pLocPortInfo);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxChkMisConfigProtoVlanId
 *
 *    DESCRIPTION      : This function sends a misconfiguration trap if the
 *                       received protocol vlan id does not match with the
 *                       protocol vlan id configured for the port.
 *
 *    INPUT            : pLocPortInfo - Pointer to the Local  Port Structure 
 *                       pProtoVlanNode - Pointer to the received Protocol
 *                                        Vlan info structure
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
VOID
LldpRxChkMisConfigProtoVlanId (tLldpLocPortInfo * pLocPortInfo,
                               tLldpxdot1RemProtoVlanInfo * pProtoVlanNode)
{
    tTMO_DLL           *pDll = NULL;
    tLldpxdot1LocProtoVlanInfo *pCurProtoVlanNode = NULL;
    tLldpRemoteNode    *pRemoteNode = NULL;
    tLldMisConfigTrap   MisConfigInfo;
    UINT2               u2ChassisIdLen = 0;
    UINT2               u2PortIdLen = 0;

    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;

    pDll = &(pLocPortInfo->LocProtoVlanDllList);

    /* Get the length of Chassis Id */
    LldpUtilGetChassisIdLen (pRemoteNode->i4RemChassisIdSubtype,
                             pRemoteNode->au1RemChassisId, &u2ChassisIdLen);

    /* Get the length of port Id */
    LldpUtilGetPortIdLen (pRemoteNode->i4RemPortIdSubtype,
                          pRemoteNode->au1RemPortId, &u2PortIdLen);

    MEMSET (&MisConfigInfo, 0, sizeof (tLldMisConfigTrap));
    MisConfigInfo.i4RemPortIdSubtype = pRemoteNode->i4RemPortIdSubtype;
    MisConfigInfo.i4RemChassisIdSubtype = pRemoteNode->i4RemChassisIdSubtype;
    MisConfigInfo.u4RemTimeMark = pRemoteNode->u4RemLastUpdateTime;
    MisConfigInfo.i4RemLocalPortNum = pRemoteNode->i4RemLocalPortNum;
    MisConfigInfo.i4RemIndex = pRemoteNode->i4RemIndex;
    MEMCPY (&MisConfigInfo.au1RemChassisId[0], pRemoteNode->au1RemChassisId,
            MEM_MAX_BYTES (u2ChassisIdLen, LLDP_MAX_LEN_CHASSISID));
    MEMCPY (&MisConfigInfo.au1PortId[0], pRemoteNode->au1RemPortId,
            MEM_MAX_BYTES (u2PortIdLen, LLDP_MAX_LEN_PORTID));
    MisConfigInfo.MisPpvid = pProtoVlanNode->u2ProtoVlanId;
    MisConfigInfo.MisVlanSupport = pProtoVlanNode->u1ProtoVlanSupported;

    /* The List is empty */
    if (TMO_DLL_Count (pDll) == 0)
    {
        /* Send Misconfiguration Notification */
        LldpSendNotification ((VOID *) &MisConfigInfo, LLDP_MIS_CONFIG_PPVID);
        return;
    }

    TMO_DLL_Scan (pDll, pCurProtoVlanNode, tLldpxdot1LocProtoVlanInfo *)
    {
        if (pCurProtoVlanNode->u2ProtoVlanId == pProtoVlanNode->u2ProtoVlanId)
        {
            /* No mis-configuration. Hence return */
            return;
        }
    }
    /* Send Misconfiguration Notification as the same PPVID is not configured 
     * for this port
     */
    LldpSendNotification ((VOID *) &MisConfigInfo, LLDP_MIS_CONFIG_PPVID);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxChkMisConfigVlanName
 *
 *    DESCRIPTION      : This function sends a misconfiguration trap if the 
 *                       received vlan name for the vlan id does not match with
 *                       the vlan name locally configured for that vlan.
 *
 *                       NOTE: This function can be called only when chassis id
 *                       and port id both are avilable with the remote node 
 *                       structure.
 *
 *    INPUT            : pLocPortInfo - Pointer to the Local  Port Structure 
 *                       pVlanNameNode - Pointer to the received Vlan Name
 *                                       info structure
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
VOID
LldpRxChkMisConfigVlanName (tLldpLocPortInfo * pLocPortInfo,
                            tLldpxdot1RemVlanNameInfo * pVlanNameNode)
{
    UINT1               au1VlanName[VLAN_STATIC_MAX_NAME_LEN];
    tLldpRemoteNode    *pRemoteNode = NULL;
    tLldMisConfigTrap   MisConfigInfo;
    UINT2               u2ChassisIdLen = 0;
    UINT2               u2PortIdLen = 0;

    MEMSET (&au1VlanName[0], 0, VLAN_STATIC_MAX_NAME_LEN);
    MEMSET (&MisConfigInfo, 0, sizeof (tLldMisConfigTrap));
    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;

    /* Get the vlan name configured in the local database */
    if (LldpVlndbGetVlanName ((UINT4) pLocPortInfo->i4IfIndex,
                              pVlanNameNode->u2VlanId, &au1VlanName[0])
        == OSIX_SUCCESS)
    {
        if (STRNCMP (au1VlanName, pVlanNameNode->au1VlanName,
                     VLAN_STATIC_MAX_NAME_LEN) != 0)
        {
            /* Get the length of Chassis Id */
            LldpUtilGetChassisIdLen (pRemoteNode->i4RemChassisIdSubtype,
                                     pRemoteNode->au1RemChassisId,
                                     &u2ChassisIdLen);

            /* Get the length of port Id */
            LldpUtilGetPortIdLen (pRemoteNode->i4RemPortIdSubtype,
                                  pRemoteNode->au1RemPortId, &u2PortIdLen);

            MisConfigInfo.i4RemPortIdSubtype = pRemoteNode->i4RemPortIdSubtype;
            MisConfigInfo.i4RemChassisIdSubtype =
                pRemoteNode->i4RemChassisIdSubtype;
            MisConfigInfo.u4RemTimeMark = pRemoteNode->u4RemLastUpdateTime;
            MisConfigInfo.i4RemLocalPortNum = pRemoteNode->i4RemLocalPortNum;
            MisConfigInfo.i4RemIndex = pRemoteNode->i4RemIndex;
            MEMCPY (&MisConfigInfo.au1RemChassisId[0],
                    pRemoteNode->au1RemChassisId,
                    MEM_MAX_BYTES (u2ChassisIdLen, LLDP_MAX_LEN_CHASSISID));
            MEMCPY (&MisConfigInfo.au1PortId[0], pRemoteNode->au1RemPortId,
                    MEM_MAX_BYTES (u2PortIdLen, LLDP_MAX_LEN_PORTID));
            MisConfigInfo.MisVlanId = pVlanNameNode->u2VlanId;
            /* Coverity Fix */
            STRNCPY (MisConfigInfo.MisVlanName, pVlanNameNode->au1VlanName,
                     MEM_MAX_BYTES (STRLEN (pVlanNameNode->au1VlanName),
                                    VLAN_STATIC_MAX_NAME_LEN));
            LldpSendNotification ((VOID *) &MisConfigInfo,
                                  LLDP_MIS_CONFIG_VLAN_NAME);
        }
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxChkMisConfigChassId
 *
 *    DESCRIPTION      : This function sends a misconfiguration trap if the 
 *                       received chassis id matched with local chassis id.
 *
 *    INPUT            : pRemoteNode - Pointer to the remote node 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
VOID
LldpRxChkMisConfigChassId (tLldpRemoteNode * pRemoteNode)
{
    tLldMisConfigTrap   MisConfigInfo;
    UINT2               u2LocChassisIdLen = 0;
    UINT2               u2RemChassisIdLen = 0;
    UINT2               u2PortIdLen = 0;

    MEMSET (&MisConfigInfo, 0, sizeof (tLldMisConfigTrap));
    if (gLldpGlobalInfo.LocSysInfo.i4LocChassisIdSubtype ==
        pRemoteNode->i4RemChassisIdSubtype)
    {
        LldpUtilGetChassisIdLen (gLldpGlobalInfo.LocSysInfo.
                                 i4LocChassisIdSubtype,
                                 gLldpGlobalInfo.LocSysInfo.au1LocChassisId,
                                 &u2LocChassisIdLen);

        LldpUtilGetChassisIdLen (pRemoteNode->i4RemChassisIdSubtype,
                                 pRemoteNode->au1RemChassisId,
                                 &u2RemChassisIdLen);

        if (u2LocChassisIdLen == u2RemChassisIdLen)
        {
            /* Get the length of port Id */
            LldpUtilGetPortIdLen (pRemoteNode->i4RemPortIdSubtype,
                                  pRemoteNode->au1RemPortId, &u2PortIdLen);

            if (MEMCMP (pRemoteNode->au1RemChassisId,
                        gLldpGlobalInfo.LocSysInfo.au1LocChassisId,
                        MEM_MAX_BYTES (u2LocChassisIdLen,
                                       LLDP_MAX_LEN_CHASSISID)) == 0)
            {
                /* Send Fault Notification */
                MisConfigInfo.i4RemChassisIdSubtype =
                    pRemoteNode->i4RemChassisIdSubtype;
                MisConfigInfo.i4RemPortIdSubtype =
                    pRemoteNode->i4RemPortIdSubtype;
                MisConfigInfo.u4RemTimeMark = pRemoteNode->u4RemLastUpdateTime;
                MisConfigInfo.i4RemIndex = pRemoteNode->i4RemIndex;
                MisConfigInfo.i4RemLocalPortNum =
                    pRemoteNode->i4RemLocalPortNum;
                MEMCPY (&MisConfigInfo.au1RemChassisId,
                        pRemoteNode->au1RemChassisId,
                        MEM_MAX_BYTES (u2LocChassisIdLen,
                                       LLDP_MAX_LEN_CHASSISID));
                MEMCPY (&MisConfigInfo.au1PortId, pRemoteNode->au1RemPortId,
                        MEM_MAX_BYTES (u2PortIdLen, LLDP_MAX_LEN_PORTID));
                LldpSendNotification ((VOID *) &MisConfigInfo,
                                      LLDP_DUP_CHASIS_ID);
            }
        }
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxPostTlvsToApplications
 *
 *    DESCRIPTION      : This function sends the received TLVs to the
 *                       corresponding applications
 *
 *    INPUT            : pLocPortInfo - Pointer to the local port structure
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
PUBLIC VOID
LldpRxPostTlvsToApplications (tLldpLocPortInfo * pLocPortInfo)
{
    tLldpAppInfo        TmpLldpAppNode;
    tLldpAppInfo       *pLldpAppNode = NULL;
    tLldpAppTlv         LldpAppTlv;
    UINT1               au1LldpMcastAddr[MAC_ADDR_LEN] =
        LLDP_DEST_MCAST_MAC_ADDR;
    UINT1               bPrevIsAnyPendingApp = OSIX_FALSE;

    /*Store the previous value of bIsAnyPendingApp for this port */
    bPrevIsAnyPendingApp = pLocPortInfo->bIsAnyPendingApp;

    /* There are no pending applications for the TLVs */
    pLocPortInfo->bIsAnyPendingApp = OSIX_FALSE;

    MEMSET (&TmpLldpAppNode, 0, sizeof (tLldpAppInfo));
    /* Get the first application register on this port by specifying
     * only port number as part of the index of application RB Tree */
    TmpLldpAppNode.u4PortId = (UINT4) pLocPortInfo->i4IfIndex;
    pLldpAppNode = (tLldpAppInfo *) RBTreeGetNext (gLldpGlobalInfo.AppRBTree,
                                                   (tRBElem *) & TmpLldpAppNode,
                                                   NULL);

    if (pLldpAppNode == NULL)
    {
        /* No application is registered, so return */
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                       "\r(LldpRxPostTlvsToApplications) No application TLVs "
                       "to be added to preformed buffer for port"
                       "%d\r\n", pLocPortInfo->i4IfIndex);
        return;
    }

    do
    {

        /* Ensure only applications registered on the current local port
         * only is processed. If the port number changes it implies all
         * the registered applications are processed.
         * Read the comment in LldpAppUtlRBCmpInfo to get more details 
         * on the comparison logic */

        if ((pLldpAppNode->u4PortId != (UINT4) pLocPortInfo->i4IfIndex))
        {
            LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                           "\r(LldpRxPostTlvsToApplications) All application TLV "
                           " nodes processed for port"
                           "%d\r\n", pLocPortInfo->u4LocPortNum);
            break;
        }
        if ((pLldpAppNode->u4LldpInstSelector ==
             pLocPortInfo->u4DstMacAddrTblIndex))
        {
            if (pLldpAppNode->u1AppTlvRxStatus == OSIX_TRUE)
            {
                if ((LLDP_RX_FRAME_REFRESH != pLocPortInfo->u1RxFrameType) ||
                    ((LLDP_RX_FRAME_REFRESH == pLocPortInfo->u1RxFrameType) &&
                     ((OSIX_TRUE == pLldpAppNode->u1AppRefreshFrameNotify) ||
                      (OSIX_TRUE == bPrevIsAnyPendingApp))))
                {
                    if (pLldpAppNode->u1AppPDURecv == OSIX_FALSE)
                    {

                        MEMSET (&LldpAppTlv, 0, sizeof (tLldpAppTlv));

                        LldpAppTlv.u4RxAppTlvPortId =
                            (UINT4) pLocPortInfo->i4IfIndex;

                        if (pLldpAppNode->bIsTlvRecvd == OSIX_TRUE)
                        {
#ifdef DCBX_WANTED
                            /* When both IEEE & CEE TLVs are in the same PDU then drop CEE TLV.
                             * Checking count to 3 because 0th bit set means cEE and 1st means IEEE so 
                             * 3 means both are present */
                            if ((gu1DcbxTlvBitMap == CEE_IEEE_TLV_BOTH_PRESENT)
                                && (OSIX_FALSE !=
                                    DcbxApiVerifyTlvIsCee (pLldpAppNode->
                                                           LldpAppId.au1OUI,
                                                           (UINT4)
                                                           pLocPortInfo->
                                                           i4IfIndex)))
                            {
                                LLDP_TRC (ALL_FAILURE_TRC,
                                          "Both IEEE and CEE are in the same PDU. Drop CEE Pkt!!\r\n");
                                pLldpAppNode =
                                    (tLldpAppInfo *)
                                    RBTreeGetNext (gLldpGlobalInfo.AppRBTree,
                                                   (tRBElem *) pLldpAppNode,
                                                   NULL);
                                continue;
                            }
                            /* When there are more than one neighbors are detected
                             * then DCBX should not update any remote entry but
                             * it is acceptable for other registered protocols.
                             */
                            if ((pLocPortInfo->u2NoOfNeighbors == 1) ||
                                (DcbxApiVerifyTlvisDcbx
                                 (pLldpAppNode->LldpAppId.u2TlvType,
                                  pLldpAppNode->LldpAppId.u1SubType,
                                  pLldpAppNode->LldpAppId.au1OUI) ==
                                 OSIX_FALSE))
                            {
#endif
                                pLldpAppNode->bIsTlvRecvd = OSIX_FALSE;
                                /* Valid TLV is received in the last LLDP PDU so it needs
                                 * to be sent to the application, so frame the message */
                                LldpAppTlv.u4MsgType = L2IWF_LLDP_APPL_TLV_RECV;

                                /* Remote node will be valid here so no need to check 
                                 * for NULL */

                                LldpAppTlv.i4RemIndex =
                                    pLocPortInfo->pBackPtrRemoteNode->
                                    i4RemIndex;
                                LldpAppTlv.u4RxAppTlvTimeStamp =
                                    pLocPortInfo->pBackPtrRemoteNode->
                                    u4RemLastUpdateTime;

                                /* Since LLDPv2 is not implemented, destination MAC has
                                 * to be hardcoded. */
                                MEMCPY (&LldpAppTlv.RemLocalDestMacAddr,
                                        au1LldpMcastAddr, MAC_ADDR_LEN);
                                LldpAppTlv.pu1RxAppTlv =
                                    pLldpAppNode->pu1RxAppTlv;
                                LldpAppTlv.u2RxAppTlvLen =
                                    pLldpAppNode->u2RxAppTlvLen;
                                MEMCPY (&LldpAppTlv.RemNodeMacAddr,
                                        pLocPortInfo->au1LstRcvdSrcMacAddr,
                                        MAC_ADDR_LEN);
                                LldpAppTlv.u4RemLocalDestMACIndex =
                                    pLocPortInfo->u4DstMacAddrTblIndex;

                            }
                            else
                            {
#ifdef DCBX_WANTED
                                LldpAppTlv.u4MsgType =
                                    L2IWF_LLDP_MULTIPLE_PEER_NOTIFY;
                            }
                        }
                        else
                        {
#endif
                            /* Valid TLV is not received in the last LLDP PDU so 
                             * age out indication needs to be sent to the 
                             * application */
                            LldpAppTlv.u4MsgType = L2IWF_LLDP_APPL_TLV_AGED;
                        }

                        /* Fill the application Id */
                        MEMCPY (&LldpAppTlv.LldpAppId, &pLldpAppNode->LldpAppId,
                                sizeof (tLldpAppId));

                        /* If the pPortInfo->u4LocPortNum is part of any LAG then
                         * send this event to be on the port channel instead
                         * of physical ports */
#ifdef DCBX_WANTED
                        if (DcbxApiVerifyTlvisDcbx
                            (pLldpAppNode->LldpAppId.u2TlvType,
                             pLldpAppNode->LldpAppId.u1SubType,
                             pLldpAppNode->LldpAppId.au1OUI) == OSIX_FALSE)
#endif

                        {
                            /* Even if the DCBX enabled port is part of LAG port, 
                             * send this event to DCBX on physical port */
                            if (LldpPortIsPortInPortChannel
                                (LldpAppTlv.u4RxAppTlvPortId) == OSIX_SUCCESS)
                            {
                                LldpPortGetPortChannelForPort
                                    (pLocPortInfo,
                                     &LldpAppTlv.u4RxAppTlvPortId);

                                LLDP_TRC_ARG2 (CONTROL_PLANE_TRC,
                                               "\r(LldpRxPostTlvsToApplications) TLV received "
                                               " on port %d which is a member of port channel "
                                               "%d\r\n",
                                               pLocPortInfo->u4LocPortNum,
                                               LldpAppTlv.u4RxAppTlvPortId);

                            }
                        }
                        /* Invoke the application call back function */
                        pLldpAppNode->pAppCallBackFn (&LldpAppTlv);
                    }
                    else
                    {
                        pLldpAppNode->bIsTlvRecvd = OSIX_FALSE;
                    }
                }
                else
                {
                    pLldpAppNode->bIsTlvRecvd = OSIX_FALSE;
                }
            }
            else
            {
                pLldpAppNode->bIsTlvRecvd = OSIX_FALSE;
            }
        }

        /* Get the next application in the port */
        pLldpAppNode =
            (tLldpAppInfo *) RBTreeGetNext (gLldpGlobalInfo.AppRBTree,
                                            (tRBElem *) pLldpAppNode, NULL);
    }
    while (pLldpAppNode != NULL);
#ifdef DCBX_WANTED
    gu1DcbxTlvBitMap = 0;
#endif
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxFrameNotificationToApplns
 *
 *    DESCRIPTION      : This function is to notify the applications upon
 *                       receiving a update/refresh frame
 *
 *    INPUT            : pLocPortInfo - Pointer to the local port structure
 *
 *    OUTPUT           : Notification is given to the modules registered
 *
 *    RETURNS          : NONE
 *
 *****************************************************************************/

VOID
LldpRxFrameNotificationToApplns (tLldpLocPortInfo * pLocPortInfo)
{
    tLldpAppInfo        TmpLldpAppNode;
    tLldpAppInfo       *pLldpAppNode = NULL;
    tLldpAppTlv         LldpAppTlv;

    MEMSET (&LldpAppTlv, 0, sizeof (tLldpAppTlv));

    MEMSET (&TmpLldpAppNode, 0, sizeof (tLldpAppInfo));
    /* Get the first application register on this port by specifying
     * only port number as part of the index of application RB Tree */
    TmpLldpAppNode.u4PortId = (UINT4) pLocPortInfo->i4IfIndex;
    pLldpAppNode = (tLldpAppInfo *) RBTreeGetNext (gLldpGlobalInfo.AppRBTree,
                                                   (tRBElem *) & TmpLldpAppNode,
                                                   NULL);
    if (NULL == pLldpAppNode)
    {
        /* No application is registered, so return */
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                       "\r(LldpRxFrameNotify) No application  "
                       "registered for this port"
                       "%d\r\n", pLocPortInfo->u4LocPortNum);
        return;
    }

    do
    {
        /* Ensure only applications registered on the current local port
         * only is processed. If the port number changes it implies all
         * the registered applications are processed.
         * Read the comment in LldpAppUtlRBCmpInfo to get more details
         * on the comparison logic */

        if ((pLldpAppNode->u4PortId != (UINT4) pLocPortInfo->i4IfIndex) ||
            (pLldpAppNode->u4LldpInstSelector !=
             pLocPortInfo->u4DstMacAddrTblIndex))
        {
            LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                           "\r(LldpRxFrameNotify) All application "
                           " nodes processed for port"
                           "%d\r\n", pLocPortInfo->u4LocPortNum);
            break;
        }

        if (OSIX_TRUE == pLldpAppNode->u1AppNeighborExistenceNotify)
        {
            /* Assigning the values to the LldpAppTlv variable and
             * calling the callback function */
            LldpAppTlv.u4RxAppTlvPortId = (UINT4) pLocPortInfo->i4IfIndex;
            LldpAppTlv.u4MsgType = L2IWF_LLDP_APPL_NEIGH_EXIST_IND;
            LldpAppTlv.u1RxFrameType = pLocPortInfo->u1RxFrameType;
            pLldpAppNode->pAppCallBackFn (&LldpAppTlv);
        }
        pLldpAppNode =
            (tLldpAppInfo *) RBTreeGetNext (gLldpGlobalInfo.AppRBTree,
                                            (tRBElem *) pLldpAppNode, NULL);

    }
    while (NULL != pLldpAppNode);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRxProcessWholePdu 
 *
 *    DESCRIPTION      : This function is to send the whole PDU to the 
 *                       applications that is registered for whole PDu
 *
 *    INPUT            : pLocPortInfo - Pointer to the local port structure
 *
 *    OUTPUT           : PDU has been sent to the modules registered
 *
 *    RETURNS          : NONE
 *
 *****************************************************************************/

PUBLIC VOID
LldpRxProcessWholePdu (tLldpLocPortInfo * pLocPortInfo)
{
    tLldpAppInfo        TmpLldpAppNode;
    tLldpAppInfo       *pLldpAppNode = NULL;
    tLldpAppTlv         LldpAppTlv;

    MEMSET (&LldpAppTlv, 0, sizeof (tLldpAppTlv));
    MEMSET (&TmpLldpAppNode, 0, sizeof (tLldpAppInfo));
    /* Get the first application register on this port by specifying
     *only port number as part of the index of application RB Tree */
    TmpLldpAppNode.u4PortId = (UINT4) pLocPortInfo->i4IfIndex;
    pLldpAppNode = (tLldpAppInfo *) RBTreeGetNext (gLldpGlobalInfo.AppRBTree,
                                                   (tRBElem *) & TmpLldpAppNode,
                                                   NULL);

    if (pLldpAppNode == NULL)
    {
        /* No application is registered, so return */
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                       "\r(LldpRxProcessWholePdu) No applications"
                       "registered for this port"
                       "%d\r\n", pLocPortInfo->u4LocPortNum);
        return;
    }
    do
    {
        /* Ensure only applications registered on the current local port
         *only is processed. If the port number changes it implies all
         *the registered applications are processed.
         *Read the comment in LldpAppUtlRBCmpInfo to get more details
         *on the comparison logic */

        if (pLldpAppNode->u4PortId != (UINT4) pLocPortInfo->i4IfIndex)
        {
            LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                           "\r(LldpRxProcessWholePdu) All application TLV "
                           " nodes processed for port"
                           "%d\r\n", pLocPortInfo->u4LocPortNum);
            break;
        }
        if (pLldpAppNode->u1AppTlvRxStatus == OSIX_TRUE)
        {
            if (pLldpAppNode->u1AppPDURecv == OSIX_TRUE)
            {
                LldpAppTlv.pu1RxAppPdu = pLocPortInfo->pu1RxLldpdu;
                LldpAppTlv.u2RxAppPduLen = pLocPortInfo->u2RxPduLen;
                LldpAppTlv.u4RxAppTlvPortId = (UINT4) pLocPortInfo->i4IfIndex;
                LldpAppTlv.u1RxFrameType = pLocPortInfo->u1RxFrameType;
                LldpAppTlv.u4MsgType = L2IWF_LLDP_APPL_PDU_RECV;
                /* Invoke the application call back function */
                pLldpAppNode->pAppCallBackFn (&LldpAppTlv);
            }
        }

        /* Get the next application in the port */
        pLldpAppNode =
            (tLldpAppInfo *) RBTreeGetNext (gLldpGlobalInfo.AppRBTree,
                                            (tRBElem *) pLldpAppNode, NULL);

    }
    while (NULL != pLldpAppNode);

    return;
}
#endif /* _LLDP_RX_C_ */
/*--------------------------------------------------------------------------*/
/*                       End of the file  lldrproc.c                        */
/*--------------------------------------------------------------------------*/
