/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldmod.c,v 1.48 2017/12/14 10:30:26 siva Exp $
 *
 * Description: This file contains procedures to 
 *              - start/shutdown LLDP module.
 *              - enable/disable LLDP module,
 *********************************************************************/
#include "lldinc.h"
#include "mux.h"

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpModuleStart 
 *                                                                          
 *    DESCRIPTION      : This function allocates memory pools for all tables 
 *                       in LLDP module. It also initalizes the global 
 *                       structure.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
LldpModuleStart (VOID)
{
    static tMacAddr     LldpMac = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x0E };

    tProtocolInfo       LldpProtocolInfo;
    tLldpv2DestAddrTbl *pDestMacAddrTbl = NULL;

    UINT1               u1PrevLldpSysCtrl = LLDP_SYSTEM_CONTROL ();

    gu4LldpAgentNumber = SYS_DEF_MAX_PHYSICAL_INTERFACES + 1;

    gu4LldpDestMacAddrTblIndex = 1;
    gLldpGlobalInfo.i4Version = LLDP_VERSION_05;
    /* initialize global memory */
    if (LldpSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        LLDP_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                  "LldpModuleStart: Memory Initialization FAILED !!!\r\n");
        LldpMainMemClear ();
        return (OSIX_FAILURE);
    }
    /*Assigning respective mempool Id's */
    LldpMainAssignMempoolIds ();
    /* LLDP RBTree creation */
    LldpUtilCreateRBTree ();
    /* Timer Initialization */
    if (LldpTmrInit () == OSIX_FAILURE)
    {
        LLDP_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                  "LldpModuleStart: Timer Initialization FAILED !!!\r\n");
        LldpUtilDeleteRBTree ();
        LldpMainMemClear ();
        return (OSIX_FAILURE);
    }
    /* Initialize globale information */
    LldpMainInitGlobalInfo ();

    /* LLDP has been successfully initialized. Make the System Control START */
    LLDP_SYSTEM_CONTROL () = LLDP_START;

    /* registration with other modules */
    if (LldpPortRegisterWithIp () != OSIX_SUCCESS)
    {
        LLDP_TRC ((LLDP_CRITICAL_TRC | ALL_FAILURE_TRC),
                  "LldpModuleStart: Registration with IP " "FAILED!!!\r\n");
        return OSIX_FAILURE;
    }

#ifdef L2RED_WANTED
    /* Initialize redundancy related global information structure */
    LldpRedInitRedGlobalInfo ();
#endif

    /* Register with Redundancy manager */
    if (LldpPortRegisterWithRM () != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpModuleStart: Registration with RM "
                  "FAILED!!!\r\n");
        LldpTmrDeInit ();
        LldpUtilDeleteRBTree ();
        LldpMainMemClear ();
        LLDP_SYSTEM_CONTROL () = u1PrevLldpSysCtrl;
        return OSIX_FAILURE;
    }
    LLDP_TRC (INIT_SHUT_TRC, "LldpModuleStart: System Control status set"
              " to START\r\n");

    /* Create all valid ports */
    LldpIfCreateAllPorts ();
    /* Intimate L2IWF to notify the LLDP applications */

    LldpPortSendReRegToApp ();
    /* Register with packet handler for control packets */
    MEMSET (&LldpProtocolInfo, 0, sizeof (tProtocolInfo));
    MEMCPY (&LldpProtocolInfo.ProtRegTuple.MacInfo.MacAddr, &LldpMac,
            sizeof (tMacAddr));
    MEMSET (LldpProtocolInfo.ProtRegTuple.MacInfo.MacMask, 0xFF,
            sizeof (tMacAddr));
    LldpProtocolInfo.ProtRegTuple.EthInfo.u2Protocol = LLDP_ENET_TYPE;
    LldpProtocolInfo.ProtRegTuple.EthInfo.u2Mask = 0xFFFF;
    LldpProtocolInfo.i4Command = MUX_PROT_REG_HDLR;
    LldpProtocolInfo.u1SendToFlag = MUX_REG_SEND_TO_ISS;
    LldpUtilCreateDestMacAddrTable ();
    pDestMacAddrTbl = (tLldpv2DestAddrTbl *)
        RBTreeGetFirst (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree);
    if (pDestMacAddrTbl == NULL)
    {
        LldpPortDeRegisterWithRM ();
        LldpTmrDeInit ();
        LldpUtilDeleteRBTree ();
        LldpMainMemClear ();
        return OSIX_FAILURE;
    }
    pDestMacAddrTbl->u1RowStatus = LLDP_ACTIVE;
    if (CfaMuxRegisterProtocolCallBack
        (PACKET_HDLR_PROTO_LLDP, &LldpProtocolInfo) != CFA_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  "LldpModuleStart: Registration with Packet Handler "
                  "FAILED!!!\r\n");
        LldpPortDeRegisterWithRM ();
        LldpTmrDeInit ();
        LldpUtilDeleteRBTree ();
        LldpMainMemClear ();
        return OSIX_FAILURE;
    }

    return (OSIX_SUCCESS);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpModuleShutDown 
 *                                                                          
 *    DESCRIPTION      : This is function is called during system shutdown or
 *                       when the manager wants to shut the LLDP module.
 *
 *    INPUT            : None.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
LldpModuleShutDown (VOID)
{
    tLldpLocPortInfo   *pPortEntry = NULL;
    tLldpLocPortInfo    PortEntry;
    UINT1               u1GlobShutWhileTmrStatus = 0;

    /* Shut down the system control status */
    LLDP_SYSTEM_CONTROL () = LLDP_SHUTDOWN_INPROGRESS;

    /* Disable LLDP module */
    /* NOTE: stopping all timers except notification interval timer is
     * taken care by the SEM
     * And stopping notification interval timer taken  care in 
     * LldpModuleDisable */
    LldpModuleDisable ();

    /* update global shutdown while timer status */
    u1GlobShutWhileTmrStatus = LLDP_GLOB_SHUT_WHILE_TMR_STATUS ();

    /* Delete RBTree */
    LldpUtilDeleteRBTree ();

#ifdef L2RED_WANTED
    if (LLDP_RED_NODE_STATUS () == RM_STANDBY)
    {
        if (gLldpRedGlobalInfo.bGlobShutWhileTmrSyncUpRvcd == OSIX_TRUE)
        {
            u1GlobShutWhileTmrStatus = LLDP_TMR_RUNNING;
        }
        else
        {
            u1GlobShutWhileTmrStatus = LLDP_TMR_NOT_RUNNING;
        }
    }
#endif

    /* If the global timer is started, then releasing of port info mem blocks
     * and pool id will be taken care during timer expiry. If the timer is not
     * started, it has to be taken care here */
    if (u1GlobShutWhileTmrStatus == LLDP_TMR_NOT_RUNNING)
    {
        pPortEntry = (tLldpLocPortInfo *)
            RBTreeGetFirst (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree);
        while (pPortEntry != NULL)
        {
            MEMSET (&PortEntry, 0, sizeof (tLldpLocPortInfo));
            PortEntry.u4LocPortNum = pPortEntry->u4LocPortNum;
            /* Release the Memory allocated for pPreFormedLldpdu Linear Buffer */
            if (pPortEntry->pPreFormedLldpdu != NULL)
            {
                MemReleaseMemBlock (gLldpGlobalInfo.LldpPduInfoPoolId,
                                    (UINT1 *) (pPortEntry->pPreFormedLldpdu));
                pPortEntry->pPreFormedLldpdu = NULL;
                pPortEntry->u4PreFormedLldpduLen = 0;
            }
            pPortEntry = (tLldpLocPortInfo *)
                RBTreeGetNext (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
                               &PortEntry, LldpAgentInfoUtlRBCmpInfo);

        }

        /* This particular case occurs when LLDP module is shut down and
         * all the ports are in initialise state */

        if (gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId != 0)
        {
            RBTreeDestroy (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                           LldpUtlRBFreeAgentToLocPortTable, 0);
            gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree = NULL;
            RBTreeDestroy (gLldpGlobalInfo.Lldpv2AgentMapTblRBTree,
                           LldpUtlRBFreeAgentMapTable, 0);
            gLldpGlobalInfo.Lldpv2AgentMapTblRBTree = NULL;
            gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId = 0;
        }
        if (gLldpGlobalInfo.LocPortInfoPoolId != 0)
        {
            RBTreeDestroy (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
                           LldpUtlRBFreeAgentTable, 0);
            gLldpGlobalInfo.LldpLocPortAgentInfoRBTree = NULL;
            gLldpGlobalInfo.LocPortInfoPoolId = 0;
        }

        if (gLldpGlobalInfo.LldpDestMacAddrPoolId != 0)
        {
            RBTreeDestroy (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree,
                           LldpUtlRBFreeDestAddrTable, 0);
            gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree = NULL;
            gLldpGlobalInfo.LldpDestMacAddrPoolId = 0;
        }

        if (gLldpGlobalInfo.LocPortTablePoolId != 0)
        {
            RBTreeDestroy (gLldpGlobalInfo.LldpPortInfoRBTree,
                           LldpUtlRBFreePortTable, 0);
            gLldpGlobalInfo.LldpPortInfoRBTree = NULL;
            gLldpGlobalInfo.LocPortTablePoolId = 0;
        }

        /* DeInit Timer */
        if (gLldpGlobalInfo.TmrListId != 0)
        {
            if (LldpTmrDeInit () == OSIX_FAILURE)
            {
                LLDP_TRC (OS_RESOURCE_TRC, "LldpModuleShutDown: "
                          "Timer Deinit FAILED\r\n");
                LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "LldpModuleShutDown"
                          "LLDP shutdown is unsuccessful\r\n");
            }
        }

        if (LldpPortDeRegisterWithRM () != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC,
                      "LldpMainTaskInit: DeRegistration with RM "
                      "FAILED!!!\r\n");
        }
        /* Delete all the messages in queue */
        LldpTxUtlDeleteQMsg (gLldpGlobalInfo.MsgQId,
                             gLldpGlobalInfo.QMsgPoolId);
        LldpTxUtlDeleteQMsg (gLldpGlobalInfo.RxPduQId,
                             gLldpGlobalInfo.RxPduQPoolId);
        LldpMainMemClear ();
        LLDP_SYSTEM_CONTROL () = LLDP_SHUTDOWN;
    }
    /* De-registration with other modules */
    if (LldpPortDeRegisterWithIp () != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpMainTaskInit: DeRegistration with IP "
                  "FAILED!!!\r\n");
    }

    /* Unregister with packet handler */
    CfaMuxUnRegisterProtocolCallBack (PACKET_HDLR_PROTO_LLDP);
    gu4LldpAgentNumber = 0;
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpModuleEnable 
 *                                                                          
 *    DESCRIPTION      : This function is called to enable LLDP module
 *                       If the port is operationally down SEM is not called
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
LldpModuleEnable (VOID)
{
    UINT1               u1Event = 0;
#ifndef FM_WANTED
    INT4                i4SysLogId = 0;
#endif

    tLldpLocPortInfo   *pPortInfo = NULL;
    tLldpLocPortInfo    PortInfo;

    MEMSET (&PortInfo, 0, sizeof (tLldpLocPortInfo));
    LLDP_TRC (INIT_SHUT_TRC, "LldpModuleEnable: Enabling LLDP module\r\n");

    LLDP_MODULE_STATUS () = LLDP_ENABLED;

#ifdef L2RED_WANTED
    /* Configurations are restored before GO_ACTIVE/GO_STANDBY event is
     * received by protocol. Hence if LLDP enable is called before GO_ACTIVE/
     * GO_STANDBY event is received then store that in LLDP_MODULE_STATUS. In
     * LldpRedMakeNodeActiveFromIdle based on the LLDP_MODULE_STATUS 
     * enable/disable LLDP.
     */
    if (LLDP_RED_NODE_STATUS () == RM_INIT)
    {
        return;
    }
#endif

#ifndef FM_WANTED
    /* Register the module with Syslog server */
    i4SysLogId =
        SYS_LOG_REGISTER ((CONST UINT1 *) "[LLDP]", SYSLOG_ALERT_LEVEL);

    if (i4SysLogId < 0)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC, "LldpModuleEnable : "
                  "Failed to register with Syslog\r\n");
        return;
    }
    gLldpGlobalInfo.u4SysLogId = (UINT4) i4SysLogId;
#endif

    /* start state event machine on all ports */
    pPortInfo = (tLldpLocPortInfo *)
        RBTreeGetFirst (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree);
    while (pPortInfo != NULL)
    {
        /* This flag indicates that the msg interval timer is started 
         * with jitterd msg interval (i.e. for the first time when starting 
         * meg interval timer we should use jittered msg interval) */
        pPortInfo->bMsgIntvalJittered = OSIX_FALSE;

        if (pPortInfo->u1OperStatus != CFA_IF_DOWN)
        {
            if (LLdpLaIsPortInIcclIf ((UINT4) pPortInfo->i4IfIndex) ==
                OSIX_SUCCESS)
            {
                PortInfo.u4LocPortNum = pPortInfo->u4LocPortNum;
                pPortInfo = (tLldpLocPortInfo *)
                    RBTreeGetNext (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
                                   &PortInfo, LldpAgentInfoUtlRBCmpInfo);
                continue;
            }
            switch (pPortInfo->PortConfigTable.i4AdminStatus)
            {
                case LLDP_ADM_STAT_TX_ONLY:
                    u1Event = LLDP_TX_EV_TXMOD_ADMIN_UP;
                    LldpTxSmMachine (pPortInfo, u1Event);
                    LldpTxTmrSmMachine (pPortInfo, u1Event);
                    u1Event = LLDP_RX_EV_RXMOD_ADMIN_DOWN;
                    LldpRxSemRun (pPortInfo, u1Event);
                    break;
                case LLDP_ADM_STAT_RX_ONLY:
                    u1Event = LLDP_RX_EV_RXMOD_ADMIN_UP;
                    LldpRxSemRun (pPortInfo, u1Event);
                    u1Event = LLDP_TX_EV_TXMOD_ADMIN_DOWN;
                    LldpTxSmMachine (pPortInfo, u1Event);
                    LldpTxTmrSmMachine (pPortInfo, u1Event);
                    break;
                case LLDP_ADM_STAT_TX_AND_RX:
                    u1Event = LLDP_TX_EV_TXMOD_ADMIN_UP;

                    /* clear tx counters */

                    LLDP_RESET_CNTR_TX_FRAMES (pPortInfo);
                    LldpTxSmMachine (pPortInfo, u1Event);
                    LldpTxTmrSmMachine (pPortInfo, u1Event);
                    u1Event = LLDP_RX_EV_RXMOD_ADMIN_UP;

                    /* Clear Rx counters */
                    LLDP_RESET_ALL_COUNTERS (pPortInfo);

                    LldpRxSemRun (pPortInfo, u1Event);
                    break;
                case LLDP_ADM_STAT_DISABLED:
                    u1Event = LLDP_TX_EV_TXMOD_ADMIN_DOWN;
                    LldpTxSmMachine (pPortInfo, u1Event);
                    LldpTxTmrSmMachine (pPortInfo, u1Event);
                    u1Event = LLDP_RX_EV_RXMOD_ADMIN_DOWN;
                    LldpRxSemRun (pPortInfo, u1Event);
                    LLDP_TRC (ALL_FAILURE_TRC,
                              " SNMPSTD: Lldp Admin Status is disabled" "\r\n");
                    break;
                default:
                    break;
            }
        }
        PortInfo.u4LocPortNum = pPortInfo->u4LocPortNum;
        pPortInfo = (tLldpLocPortInfo *)
            RBTreeGetNext (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
                           &PortInfo, LldpAgentInfoUtlRBCmpInfo);
    }

    LLDP_TRC (CONTROL_PLANE_TRC, "Enabled LLDP Module\r\n");

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpModuleDisable 
 *                                                                          
 *    DESCRIPTION      : This function is called to disable LLDP module. 
 *                       Stops global timers and sends admin down event to
 *                       TX SEM and RX SEM
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
LldpModuleDisable (VOID)
{
    tLldpLocPortInfo   *pPortInfo = NULL;
    tLldpLocPortInfo    PortInfo;
    UINT1               u1Event = 0;

    MEMSET (&PortInfo, 0, sizeof (tLldpLocPortInfo));
    LLDP_TRC (INIT_SHUT_TRC, "LldpModuleDisable: Disabling LLDP module\r\n");
    LLDP_MODULE_STATUS () = LLDP_DISABLED;

#ifndef FM_WANTED
    SYS_LOG_DEREGISTER (gLldpGlobalInfo.u4SysLogId);
    gLldpGlobalInfo.u4SysLogId = 0;
#endif

    /* stop all timer if running */
    /* but transmit module timers(message interval timer, txdelay timer,
     * reinit delay timer) are stopped in the TX SEM, if admin down
     * event is recived, so no need to stop them here */
    /* receive module has only one timer RxInfoAge timer and stopping 
     * this timer it is take care by RX SEM*/

    /* stop notification timer if running */
    if (LLDP_NOTIF_INTVAL_TMR_STATUS () == LLDP_TMR_RUNNING)
    {
        if (TmrStopTimer (gLldpGlobalInfo.TmrListId,
                          &(gLldpGlobalInfo.NotifIntervalTmr.TimerNode))
            != TMR_SUCCESS)
        {
            LLDP_TRC (INIT_SHUT_TRC, "LldpModuleDisable: Stop notification"
                      " timer FAILED!!!\r\n");
        }
        LLDP_NOTIF_INTVAL_TMR_STATUS () = LLDP_TMR_NOT_RUNNING;
    }
    pPortInfo = (tLldpLocPortInfo *)
        RBTreeGetFirst (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree);
    while (pPortInfo != NULL)
    {
        /* Reset LLDP-MED flag when Lldp module status is disbled */
        if (pPortInfo->bMedCapable == LLDP_MED_TRUE)
        {
            pPortInfo->bMedCapable = LLDP_MED_FALSE;
            if (LldpTxUtlConstructPreformedBuf (pPortInfo, LLDP_INFO_FRAME)
                != OSIX_SUCCESS)
            {
                LLDP_TRC (INIT_SHUT_TRC, "LldpModuleDisable: "
                          "Construction of preformed buffer Failed.\r\n");
            }
        }
        /* send admin down event to tx and rx SEM */
        u1Event = LLDP_TX_EV_TXMOD_ADMIN_DOWN;
        LldpTxSmMachine (pPortInfo, u1Event);
        u1Event = LLDP_TX_TMR_EV_TXMOD_ADMIN_DOWN;
        LldpTxTmrSmMachine (pPortInfo, u1Event);
        /* Clearing TX counters */
        LLDP_RESET_CNTR_TX_FRAMES (pPortInfo);
        u1Event = LLDP_RX_EV_RXMOD_ADMIN_DOWN;
        LldpRxSemRun (pPortInfo, u1Event);
        /* Clearing RX counters */
        LLDP_RESET_ALL_COUNTERS (pPortInfo);
        /* Setting the number of neighbors to 0 during 
           the LLDP disable */
        pPortInfo->u2NoOfNeighbors = 0;
        PortInfo.u4LocPortNum = pPortInfo->u4LocPortNum;
        pPortInfo = (tLldpLocPortInfo *)
            RBTreeGetNext (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
                           &PortInfo, LldpAgentInfoUtlRBCmpInfo);
    }

    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  lldmod.c                       */
/*-----------------------------------------------------------------------*/
