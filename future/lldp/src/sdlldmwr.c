/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: sdlldmwr.c,v 1.1 2015/09/09 13:29:13 siva Exp $
*
* Description: LLDP-MED Standard Wrapper Functions
*********************************************************************/

# include  "lr.h" 
# include  "fssnmp.h" 
# include "lldinc.h"
# include  "sdlldmdb.h"


VOID RegisterSTDLLDPM ()
{
	SNMPRegisterMibWithLock (&stdlldpmOID, &stdlldpmEntry, LldpLock, LldpUnLock, SNMP_MSR_TGR_TRUE);
	SNMPAddSysorEntry (&stdlldpmOID, (const UINT1 *) "stdlldpmedx");
}



VOID UnRegisterSTDLLDPM ()
{
	SNMPUnRegisterMib (&stdlldpmOID, &stdlldpmEntry);
	SNMPDelSysorEntry (&stdlldpmOID, (const UINT1 *) "stdlldpmedx");
}

INT4 LldpXMedLocDeviceClassGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLldpXMedLocDeviceClass(&(pMultiData->i4_SLongValue)));
}

INT4 GetNextIndexLldpXMedPortConfigTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexLldpXMedPortConfigTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexLldpXMedPortConfigTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 LldpXMedPortCapSupportedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedPortConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedPortCapSupported(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 LldpXMedPortConfigTLVsTxEnableGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedPortConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedPortConfigTLVsTxEnable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 LldpXMedPortConfigNotifEnableGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedPortConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedPortConfigNotifEnable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LldpXMedPortConfigTLVsTxEnableSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetLldpXMedPortConfigTLVsTxEnable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 LldpXMedPortConfigNotifEnableSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetLldpXMedPortConfigNotifEnable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 LldpXMedPortConfigTLVsTxEnableTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2LldpXMedPortConfigTLVsTxEnable(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 LldpXMedPortConfigNotifEnableTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2LldpXMedPortConfigNotifEnable(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 LldpXMedPortConfigTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2LldpXMedPortConfigTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 LldpXMedFastStartRepeatCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLldpXMedFastStartRepeatCount(&(pMultiData->u4_ULongValue)));
}
INT4 LldpXMedFastStartRepeatCountSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetLldpXMedFastStartRepeatCount(pMultiData->u4_ULongValue));
}


INT4 LldpXMedFastStartRepeatCountTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2LldpXMedFastStartRepeatCount(pu4Error, pMultiData->u4_ULongValue));
}


INT4 LldpXMedFastStartRepeatCountDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2LldpXMedFastStartRepeatCount(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexLldpXMedLocMediaPolicyTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexLldpXMedLocMediaPolicyTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexLldpXMedLocMediaPolicyTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pFirstMultiIndex->pIndex[1].pOctetStrValue,
			pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 LldpXMedLocMediaPolicyVlanIDGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedLocMediaPolicyTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedLocMediaPolicyVlanID(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LldpXMedLocMediaPolicyPriorityGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedLocMediaPolicyTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedLocMediaPolicyPriority(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LldpXMedLocMediaPolicyDscpGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedLocMediaPolicyTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedLocMediaPolicyDscp(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LldpXMedLocMediaPolicyUnknownGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedLocMediaPolicyTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedLocMediaPolicyUnknown(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LldpXMedLocMediaPolicyTaggedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedLocMediaPolicyTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedLocMediaPolicyTagged(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LldpXMedLocHardwareRevGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLldpXMedLocHardwareRev(pMultiData->pOctetStrValue));
}
INT4 LldpXMedLocFirmwareRevGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLldpXMedLocFirmwareRev(pMultiData->pOctetStrValue));
}
INT4 LldpXMedLocSoftwareRevGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLldpXMedLocSoftwareRev(pMultiData->pOctetStrValue));
}
INT4 LldpXMedLocSerialNumGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLldpXMedLocSerialNum(pMultiData->pOctetStrValue));
}
INT4 LldpXMedLocMfgNameGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLldpXMedLocMfgName(pMultiData->pOctetStrValue));
}
INT4 LldpXMedLocModelNameGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLldpXMedLocModelName(pMultiData->pOctetStrValue));
}
INT4 LldpXMedLocAssetIDGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLldpXMedLocAssetID(pMultiData->pOctetStrValue));
}

INT4 GetNextIndexLldpXMedLocLocationTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexLldpXMedLocLocationTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexLldpXMedLocLocationTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 LldpXMedLocLocationInfoGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedLocLocationTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedLocLocationInfo(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 LldpXMedLocLocationInfoSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetLldpXMedLocLocationInfo(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 LldpXMedLocLocationInfoTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2LldpXMedLocLocationInfo(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 LldpXMedLocLocationTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2LldpXMedLocLocationTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 LldpXMedLocXPoEDeviceTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLldpXMedLocXPoEDeviceType(&(pMultiData->i4_SLongValue)));
}

INT4 GetNextIndexLldpXMedLocXPoEPSEPortTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexLldpXMedLocXPoEPSEPortTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexLldpXMedLocXPoEPSEPortTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 LldpXMedLocXPoEPSEPortPowerAvGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedLocXPoEPSEPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedLocXPoEPSEPortPowerAv(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 LldpXMedLocXPoEPSEPortPDPriorityGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedLocXPoEPSEPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedLocXPoEPSEPortPDPriority(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LldpXMedLocXPoEPSEPowerSourceGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLldpXMedLocXPoEPSEPowerSource(&(pMultiData->i4_SLongValue)));
}
INT4 LldpXMedLocXPoEPDPowerReqGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLldpXMedLocXPoEPDPowerReq(&(pMultiData->u4_ULongValue)));
}
INT4 LldpXMedLocXPoEPDPowerSourceGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLldpXMedLocXPoEPDPowerSource(&(pMultiData->i4_SLongValue)));
}
INT4 LldpXMedLocXPoEPDPowerPriorityGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLldpXMedLocXPoEPDPowerPriority(&(pMultiData->i4_SLongValue)));
}

INT4 GetNextIndexLldpXMedRemCapabilitiesTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexLldpXMedRemCapabilitiesTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexLldpXMedRemCapabilitiesTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].i4_SLongValue,
			&(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 LldpXMedRemCapSupportedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemCapabilitiesTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemCapSupported(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 LldpXMedRemCapCurrentGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemCapabilitiesTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemCapCurrent(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 LldpXMedRemDeviceClassGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemCapabilitiesTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemDeviceClass(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}

INT4 GetNextIndexLldpXMedRemMediaPolicyTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexLldpXMedRemMediaPolicyTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].i4_SLongValue),
			pNextMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexLldpXMedRemMediaPolicyTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].i4_SLongValue,
			&(pNextMultiIndex->pIndex[2].i4_SLongValue),
			pFirstMultiIndex->pIndex[3].pOctetStrValue,
			pNextMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 LldpXMedRemMediaPolicyVlanIDGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemMediaPolicyTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemMediaPolicyVlanID(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LldpXMedRemMediaPolicyPriorityGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemMediaPolicyTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemMediaPolicyPriority(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LldpXMedRemMediaPolicyDscpGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemMediaPolicyTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemMediaPolicyDscp(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LldpXMedRemMediaPolicyUnknownGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemMediaPolicyTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemMediaPolicyUnknown(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LldpXMedRemMediaPolicyTaggedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemMediaPolicyTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemMediaPolicyTagged(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}

INT4 GetNextIndexLldpXMedRemInventoryTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexLldpXMedRemInventoryTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexLldpXMedRemInventoryTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].i4_SLongValue,
			&(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 LldpXMedRemHardwareRevGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemInventoryTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemHardwareRev(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 LldpXMedRemFirmwareRevGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemInventoryTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemFirmwareRev(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 LldpXMedRemSoftwareRevGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemInventoryTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemSoftwareRev(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 LldpXMedRemSerialNumGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemInventoryTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemSerialNum(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 LldpXMedRemMfgNameGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemInventoryTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemMfgName(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 LldpXMedRemModelNameGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemInventoryTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemModelName(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 LldpXMedRemAssetIDGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemInventoryTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemAssetID(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 GetNextIndexLldpXMedRemLocationTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexLldpXMedRemLocationTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].i4_SLongValue),
			&(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexLldpXMedRemLocationTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].i4_SLongValue,
			&(pNextMultiIndex->pIndex[2].i4_SLongValue),
			pFirstMultiIndex->pIndex[3].i4_SLongValue,
			&(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 LldpXMedRemLocationInfoGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemLocationTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemLocationInfo(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 GetNextIndexLldpXMedRemXPoETable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexLldpXMedRemXPoETable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexLldpXMedRemXPoETable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].i4_SLongValue,
			&(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 LldpXMedRemXPoEDeviceTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemXPoETable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemXPoEDeviceType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}

INT4 GetNextIndexLldpXMedRemXPoEPSETable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexLldpXMedRemXPoEPSETable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexLldpXMedRemXPoEPSETable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].i4_SLongValue,
			&(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 LldpXMedRemXPoEPSEPowerAvGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemXPoEPSETable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemXPoEPSEPowerAv(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 LldpXMedRemXPoEPSEPowerSourceGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemXPoEPSETable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemXPoEPSEPowerSource(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LldpXMedRemXPoEPSEPowerPriorityGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemXPoEPSETable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemXPoEPSEPowerPriority(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}

INT4 GetNextIndexLldpXMedRemXPoEPDTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexLldpXMedRemXPoEPDTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexLldpXMedRemXPoEPDTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].i4_SLongValue,
			&(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 LldpXMedRemXPoEPDPowerReqGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemXPoEPDTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemXPoEPDPowerReq(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 LldpXMedRemXPoEPDPowerSourceGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemXPoEPDTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemXPoEPDPowerSource(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LldpXMedRemXPoEPDPowerPriorityGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLldpXMedRemXPoEPDTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLldpXMedRemXPoEPDPowerPriority(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
