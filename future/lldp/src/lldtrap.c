/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldtrap.c,v 1.30 2016/02/03 10:50:07 siva Exp $
 *
 * Description: This file contains the functions to send the trap message.
 ******************************************************************************/

#include "fssnmp.h"
#include "lldinc.h"
#include "snmputil.h"

#ifdef SNMP_3_WANTED
#include "fslldp.h"
#include "stdlldp.h"
#include "stdlldt3.h"
#include "stdlldt1.h"
#include "fslldpmed.h"
#include "stdlldpmedx.h"
#endif /* SNMP_3_WANTED */

CHR1               *gac1SysLogMsg[] = {
    NULL,
    "Remote Table Changed",
    "Exceeds Maximum frame Size",
    "Duplicate Chasis ID",
    "Duplicate System Name",
    "Duplicate Management Address",
    "Mis Configured Port Vlan ID",
    "Mis Configured Port and Protocol VlanID",
    "Mis Configured Vlan Name",
    "Mis Configured Protocol Identity",
    "Mis Configured Link Agg Info",
    "Mis Configured Power via MDI",
    "Mis configured Maximum Frame Size",
    "Mis Confugred Operational MauType",
    "LLDP-MED Network Policy Mismatch"
};
UINT1               gau1Buf[LLDP_OBJECT_NAME_LEN];

#ifdef SNMP_3_WANTED
/****************************************************************************
 *
 *    FUNCTION NAME    : LldpGetObjIndexOIDs 
 *
 *    DESCRIPTION      : This function forms the list in OID, corresponding value 
 *                       format. 
 *
 *    INPUT            : pMisConfTrap - Pointer to the tLldMisConfigTrap 
 *                                      datastructure.
 *                       
 *    OUTPUT           : Gives formatted List value and Next Value.
 *
 *    RETURNS          : OSIX_FAILURE / OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpGetObjIndexOIDs (tLldMisConfigTrap * pMisConfTrap,
                     tSNMP_VAR_BIND ** ppStartVb, tSNMP_VAR_BIND ** ppVbList)
{
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT1               au1Buf[LLDP_OBJECT_NAME_LEN];
    tSNMP_OCTET_STRING_TYPE *pValueStr = NULL;
    UINT2               u2ChassisIdLen = 0;
    UINT2               u2PortIdLen = 0;

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    SPRINTF ((char *) au1Buf, "lldpRemChassisId");
    pOid =
        LldpMakeObjIdFrmString ((INT1 *) au1Buf,
                                (UINT1 *) std_lldp_orig_mib_oid_table);
    if (pOid == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpGetObjIndexOIDs: OID Not Found\r\n");
        return OSIX_FAILURE;
    }
    pOid->pu4_OidList[pOid->u4_Length] = pMisConfTrap->u4RemTimeMark;
    pOid->u4_Length++;
    pOid->pu4_OidList[pOid->u4_Length] = (UINT4) 
        pMisConfTrap->i4RemLocalPortNum;
    pOid->u4_Length++;
    pOid->pu4_OidList[pOid->u4_Length] = (UINT4) pMisConfTrap->i4RemIndex;
    pOid->u4_Length++;

    /* Get the length of Chassis Id */
    LldpUtilGetChassisIdLen (pMisConfTrap->i4RemChassisIdSubtype,
                             pMisConfTrap->au1RemChassisId, &u2ChassisIdLen);
    /* Get the length of port Id */
    LldpUtilGetPortIdLen (pMisConfTrap->i4RemPortIdSubtype,
                          pMisConfTrap->au1PortId, &u2PortIdLen);

    /* Form the octet string */
    if ((pValueStr =
         SNMP_AGT_FormOctetString
         (pMisConfTrap->au1RemChassisId, u2ChassisIdLen)) == NULL)
    {
        SNMP_FreeOid (pOid);
        LLDP_TRC (ALL_FAILURE_TRC, "LldpGetObjIndexOIDs: "
                  "Form Octet String Failed\r\n");
        return OSIX_FAILURE;
    }

    (*ppVbList)->pNextVarBind =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0, pValueStr,
                              NULL, SnmpCnt64Type);
    if ((*ppVbList)->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeOctetString (pValueStr);
        return OSIX_FAILURE;
    }

    *ppVbList = (*ppVbList)->pNextVarBind;

    SPRINTF ((char *) au1Buf, "lldpRemPortId");
    pOid =
        LldpMakeObjIdFrmString ((INT1 *) au1Buf,
                                (UINT1 *) std_lldp_orig_mib_oid_table);
    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (*ppStartVb);
        LLDP_TRC (ALL_FAILURE_TRC, "LldpGetObjIndexOIDs: OID Not Found\r\n");
        return OSIX_FAILURE;
    }
    pOid->pu4_OidList[pOid->u4_Length] = pMisConfTrap->u4RemTimeMark;
    pOid->u4_Length++;
    pOid->pu4_OidList[pOid->u4_Length] = pMisConfTrap->i4RemLocalPortNum;
    pOid->u4_Length++;
    pOid->pu4_OidList[pOid->u4_Length] = pMisConfTrap->i4RemIndex;
    pOid->u4_Length++;

    /* Form the octet string */
    if ((pValueStr =
         SNMP_AGT_FormOctetString
         (pMisConfTrap->au1PortId, u2PortIdLen)) == NULL)
    {
        SNMP_FreeOid (pOid);
        LLDP_TRC (ALL_FAILURE_TRC, "LldpGetObjIndexOIDs: "
                  "Form Octet String Failed\r\n");
        return OSIX_FAILURE;
    }

    (*ppVbList)->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                      SNMP_DATA_TYPE_OCTET_PRIM,
                                                      0, 0, pValueStr, NULL,
                                                      SnmpCnt64Type);
    if ((*ppVbList)->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeOctetString (pValueStr);
        return OSIX_FAILURE;
    }

    *ppVbList = (*ppVbList)->pNextVarBind;
    (*ppVbList)->pNextVarBind = NULL;
    return OSIX_SUCCESS;

}

/******************************************************************************
* Function :   LldpMakeObjIdFrmString
*
* Description: This Function retuns the OID  of the given string for the 
*              proprietary MIB.
*
* Input    :   pi1TextStr - pointer to the string.
*              pTableName - TableName has to be fetched.
*
* Output   :   None.
*
* Returns  :   pOidPtr or NULL
*******************************************************************************/

tSNMP_OID_TYPE     *
LldpMakeObjIdFrmString (INT1 *pi1TextStr, UINT1 *pu1TableName)
{
#ifdef SNMP_2_WANTED
    tSNMP_OID_TYPE     *pOidPtr;
    INT1               *pi1DotPtr;
    UINT2               u2Index;
    UINT2               u2DotCount;
    INT1                ai1TempBuffer[LLDP_OBJECT_NAME_LEN + 1];
    UINT1              *pu1TempPtr = NULL;
    struct MIB_OID     *pTableName = NULL;
    pTableName = (struct MIB_OID *) (VOID *) pu1TableName;

    MEMSET (ai1TempBuffer, 0, LLDP_OBJECT_NAME_LEN);

    /* see if there is an alpha descriptor at begining */
    if (isalpha (*pi1TextStr))
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pu1TempPtr = (UINT1 *) pi1TextStr;

        for (u2Index = 0;
             ((pu1TempPtr < (UINT1 *) pi1DotPtr) && (u2Index < 256)); u2Index++)
        {
            ai1TempBuffer[u2Index] = *pu1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';
        for (u2Index = 0; pTableName[u2Index].pName != NULL; u2Index++)
        {
            if ((STRCMP
                 (pTableName[u2Index].pName,
                  (INT1 *) ai1TempBuffer) == 0)
                && (STRLEN ((INT1 *) ai1TempBuffer) ==
                    STRLEN (pTableName[u2Index].pName)))
            {
                if (LLDP_OBJECT_NAME_LEN >=
                    (STRLEN (pTableName[u2Index].pNumber)))
                {
                    STRNCPY ((INT1 *) ai1TempBuffer,
                            pTableName[u2Index].pNumber, STRLEN(pTableName[u2Index].pNumber));
		    ai1TempBuffer[STRLEN(pTableName[u2Index].pNumber)] = '\0';
                }
                break;
            }
        }

        if (pTableName[u2Index].pName == NULL)
        {
            return (NULL);
        }
        if (LLDP_OBJECT_NAME_LEN >= (STRLEN ((INT1 *) ai1TempBuffer)) +
            (STRLEN ((INT1 *) pi1DotPtr)))
        {
            /* now concatenate the non-alpha part to the begining */
            STRCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr);
        }
    }
    else
    {                            /* is not alpha, so just copy into ai1TempBuffer */
        STRNCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr, STRLEN((INT1 *) pi1TextStr));
	ai1TempBuffer[STRLEN((INT1 *) pi1TextStr)] = '\0';
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = 0;
    for (u2Index = 0; ((u2Index < (LLDP_OBJECT_NAME_LEN + 1))
                       && (ai1TempBuffer[u2Index] != '\0')); u2Index++)
    {
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }
    pOidPtr = alloc_oid (SNMP_MAX_OID_LENGTH);
    if (pOidPtr == NULL)
    {
        return (NULL);
    }

    pOidPtr->u4_Length = u2DotCount + 1;

    /* now we convert number.number.... strings */
    pu1TempPtr = (UINT1 *) ai1TempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {
        if (LldpParseSubIdNew
            (&pu1TempPtr, &(pOidPtr->pu4_OidList[u2Index])) == OSIX_FAILURE)
        {
            MEM_FREE (pOidPtr->pu4_OidList);
            MEM_FREE (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if (*pu1TempPtr == '.')
        {
            pu1TempPtr++;        /* to skip over dot */
        }
        else if (*pu1TempPtr != '\0')
        {
            MEM_FREE (pOidPtr->pu4_OidList);
            MEM_FREE (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
#else
    UNUSED_PARAM (pi1TextStr);
    UNUSED_PARAM (pu1TableName);
    return (NULL);
#endif
}

/******************************************************************************
* Function :   LldpParseSubIdNew
* 
* Description : Parse the string format in number.number..format.
*
* Input       : ppu1TempPtr - pointer to the string.
*               pu4Value    - Pointer the OID List value.
*               
* Output      : value of ppu1TempPtr
*
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*******************************************************************************/

INT4
LldpParseSubIdNew (UINT1 **ppu1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    UINT1              *pu1Tmp = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    for (pu1Tmp = *ppu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                                 ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                                 ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pu1Tmp & 0xf);
    }

    if (*ppu1TempPtr == pu1Tmp)
    {
        i4RetVal = OSIX_FAILURE;
    }
    *ppu1TempPtr = pu1Tmp;
    *pu4Value = u4Value;
    return (i4RetVal);
}
#endif /* SNMP_3_WANTED */

/******************************************************************************
 * Function Name      : LldpSendNotification 
 *
 * Description        : VOID * - Pointer to the tLldMisConfigTrap 
 *                                       structure
 *                      u1Flag - To select the which OID.
 *
 * Input(s)           : pBuf - pointer to CRU Buffer
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
LldpSendNotification (VOID *pNotifyInfo, UINT1 u1TrapId)
{
#ifdef SNMP_3_WANTED
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_OCTET_STRING_TYPE *pValueStr;
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };
    tLldMisConfigTrap  *pMisConfTrap = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tLldpExceedFrameSize *pExceedFrameSize = NULL;
    tSNMP_OID_TYPE      LldpPrefixTrapOid;
    tSNMP_OID_TYPE      LldpSuffixTrapOid;
    tSNMP_OID_TYPE      LldpTrapOid;
    UINT4               au4LldpPrefixTrapOid[] = { 1, 3, 6, 1, 4, 1 };
    UINT4               au4LldpSuffixTrapOid[] = { 158, 4, 0 };
    tSNMP_OID_TYPE     *pLldpTrapOid = NULL;
    UINT4               au4SnmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    tLldpLocPortInfo   *pPortEntry = NULL;
    UINT2               u2PortIdLen = 0;
    UINT2               u2ChassisIdLen = 0;
    UINT1               u1Count = 0;
    UINT1               u1ManAddrLen = 0;

#ifdef L2RED_WANTED
    tLldpRedTmrSyncUpInfo TmrSyncUpInfo;

    MEMSET (&TmrSyncUpInfo, 0, sizeof (tLldpRedTmrSyncUpInfo));

    /* Standby node can't send notification */
    if (LLDP_RED_NODE_STATUS () == RM_STANDBY)
    {
        return;
    }
#endif

    LldpPrefixTrapOid.pu4_OidList = au4LldpPrefixTrapOid;
    LldpPrefixTrapOid.u4_Length =
        sizeof (au4LldpPrefixTrapOid) / sizeof (UINT4);

    LldpSuffixTrapOid.pu4_OidList = au4LldpSuffixTrapOid;
    LldpSuffixTrapOid.u4_Length =
        sizeof (au4LldpSuffixTrapOid) / sizeof (UINT4);

    pLldpTrapOid = alloc_oid (SNMP_MAX_OID_LENGTH);
    if (pLldpTrapOid == NULL)
    {
        return;
    }
    MEMSET (pLldpTrapOid->pu4_OidList, 0, SNMP_MAX_OID_LENGTH);

    LldpTrapOid.pu4_OidList = pLldpTrapOid->pu4_OidList;
    LldpTrapOid.u4_Length = 0;

    if (SNMPAddEnterpriseOid (&LldpPrefixTrapOid, &LldpSuffixTrapOid,
                              &LldpTrapOid) == OSIX_FAILURE)
    {
        SNMP_FreeOid (pLldpTrapOid);
        LLDP_TRC (ALL_FAILURE_TRC,
                  "LldpSendNotification: Adding EnterpriseOid failed \r\n");
        return;
    }

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    pEnterpriseOid = alloc_oid (SNMP_MAX_OID_LENGTH);
    if (pEnterpriseOid == NULL)
    {
        SNMP_FreeOid (pLldpTrapOid);
        LLDP_TRC (ALL_FAILURE_TRC,
                  "LldpSendNotification: OID Memory Allocation Failed\r\n");
        return;
    }
    MEMCPY (pEnterpriseOid->pu4_OidList, LldpTrapOid.pu4_OidList,
            LldpTrapOid.u4_Length * sizeof (UINT4));
    pEnterpriseOid->u4_Length = LldpTrapOid.u4_Length;

    SNMP_FreeOid (pLldpTrapOid);
    pLldpTrapOid = NULL;

    pSnmpTrapOid = alloc_oid (LLDP_SNMPV2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        LLDP_TRC (ALL_FAILURE_TRC,
                  "LldpSendNotification: OID Memory Allocation Failed\r\n");
        return;
    }
    MEMCPY (pSnmpTrapOid->pu4_OidList, au4SnmpTrapOid,
            LLDP_SNMPV2_TRAP_OID_LEN * sizeof (UINT4));

    pEnterpriseOid->u4_Length = pEnterpriseOid->u4_Length - 1;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u1TrapId;

    /*Assigning the TrapOID value */
    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0, 0, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        LLDP_TRC (ALL_FAILURE_TRC,
                  "LldpSendNotification: Variable Binding Failed\r\n");
        return;
    }

    pStartVb = pVbList;

    switch (u1TrapId)
    {
        case LLDP_REM_TABLE_CHG:
            /*If timer is running Do nothing */
            if (LLDP_NOTIF_INTVAL_TMR_STATUS () == LLDP_TMR_RUNNING)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            /* Check whether the Toplogy Change Notification is enabled 
             * or not. It will be enabled when MED device is connected or
             * disconnected */
            if (gLldpGlobalInfo.bSendTopChgNotif != OSIX_TRUE)
            {
                SPRINTF ((char *) gau1Buf, "lldpStatsRemTablesInserts");
                pOid =
                    LldpMakeObjIdFrmString ((INT1 *) gau1Buf,
                            (UINT1 *) std_lldp_orig_mib_oid_table);
                if (pOid == NULL)
                {
                    SNMP_free_snmp_vb_list (pStartVb);
                    LLDP_TRC (ALL_FAILURE_TRC,
                            "LldpSendNotification: OID Not Found\r\n");
                    return;
                }
                pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                        SNMP_DATA_TYPE_GAUGE32,
                        gLldpGlobalInfo.
                        StatsRemTabInfo.
                        u4Inserts, 0, NULL,
                        NULL, SnmpCnt64Type);
                if (pVbList->pNextVarBind == NULL)
                {
                    SNMP_free_snmp_vb_list (pStartVb);
                    SNMP_FreeOid (pOid);
                    LLDP_TRC (ALL_FAILURE_TRC,
                            "LldpSendNotification: Variable Binding Failed\r\n");
                    return;
                }

                pVbList = pVbList->pNextVarBind;
                SPRINTF ((char *) gau1Buf, "lldpStatsRemTablesDeletes");
                pOid =
                    LldpMakeObjIdFrmString ((INT1 *) gau1Buf,
                            (UINT1 *) std_lldp_orig_mib_oid_table);
                if (pOid == NULL)
                {
                    SNMP_free_snmp_vb_list (pStartVb);
                    LLDP_TRC (ALL_FAILURE_TRC,
                            "LldpSendNotification: OID Not Found\r\n");
                    return;
                }

                pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                        SNMP_DATA_TYPE_GAUGE32,
                        gLldpGlobalInfo.
                        StatsRemTabInfo.
                        u4Deletes, 0, NULL,
                        NULL, SnmpCnt64Type);
                if (pVbList->pNextVarBind == NULL)
                {
                    SNMP_FreeOid (pOid);
                    SNMP_free_snmp_vb_list (pStartVb);
                    LLDP_TRC (ALL_FAILURE_TRC,
                            "LldpSendNotification: Variable Binding Failed\r\n");
                    return;
                }
                pVbList = pVbList->pNextVarBind;
                SPRINTF ((char *) gau1Buf, "lldpStatsRemTablesDrops");
                pOid =
                    LldpMakeObjIdFrmString ((INT1 *) gau1Buf,
                            (UINT1 *) std_lldp_orig_mib_oid_table);
                if (pOid == NULL)
                {
                    SNMP_free_snmp_vb_list (pStartVb);
                    LLDP_TRC (ALL_FAILURE_TRC,
                            "LldpSendNotification: OID Not Found\r\n");
                    return;
                }

                pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                        SNMP_DATA_TYPE_GAUGE32,
                        gLldpGlobalInfo.
                        StatsRemTabInfo.
                        u4Drops, 0, NULL,
                        NULL, SnmpCnt64Type);
                if (pVbList->pNextVarBind == NULL)
                {
                    SNMP_FreeOid (pOid);
                    SNMP_free_snmp_vb_list (pStartVb);
                    LLDP_TRC (ALL_FAILURE_TRC,
                            "LldpSendNotification: Variable Binding Failed\r\n");
                    return;
                }
                pVbList = pVbList->pNextVarBind;
                SPRINTF ((char *) gau1Buf, "lldpStatsRemTablesAgeouts");
                pOid =
                    LldpMakeObjIdFrmString ((INT1 *) gau1Buf,
                            (UINT1 *) std_lldp_orig_mib_oid_table);
                if (pOid == NULL)
                {
                    SNMP_free_snmp_vb_list (pStartVb);
                    LLDP_TRC (ALL_FAILURE_TRC,
                            "LldpSendNotification: OID Not Found\r\n");
                    return;
                }

                pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                        SNMP_DATA_TYPE_GAUGE32,
                        gLldpGlobalInfo.
                        StatsRemTabInfo.
                        u4AgeOuts, 0, NULL,
                        NULL, SnmpCnt64Type);
                if (pVbList->pNextVarBind == NULL)
                {
                    SNMP_FreeOid (pOid);
                    SNMP_free_snmp_vb_list (pStartVb);
                    LLDP_TRC (ALL_FAILURE_TRC,
                            "LldpSendNotification: Variable Binding Failed\r\n");
                    return;
                }

                pVbList = pVbList->pNextVarBind;
                SPRINTF ((char *) gau1Buf, "fsLldpStatsRemTablesUpdates");
                pOid =
                    LldpMakeObjIdFrmString ((INT1 *) gau1Buf,
                            (UINT1 *) fs_lldp_orig_mib_oid_table);
                if (pOid == NULL)
                {
                    SNMP_free_snmp_vb_list (pStartVb);
                    LLDP_TRC (ALL_FAILURE_TRC,
                            "LldpSendNotification: OID Not Found\r\n");
                    return;
                }

                pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                        SNMP_DATA_TYPE_GAUGE32,
                        gLldpGlobalInfo.
                        StatsRemTabInfo.
                        u4Updates, 0, NULL,
                        NULL, SnmpCnt64Type);
                if (pVbList->pNextVarBind == NULL)
                {
                    SNMP_FreeOid (pOid);
                    SNMP_free_snmp_vb_list (pStartVb);
                    LLDP_TRC (ALL_FAILURE_TRC,
                            "LldpSendNotification: Variable Binding Failed\r\n");
                    return;
                }
            }
            /* If Toplogy Change Notification is enabled */
            else
            {
                /* If Timer expires, the remote table change trap will be
                 * called even when Topology change variable is enabled.
                 * So, check whether pNotifyInfo is NULL or not */
                if (pNotifyInfo == NULL)
                {
                    LLDP_TRC ( CONTROL_PLANE_TRC |ALL_FAILURE_TRC, 
                            "LldpSendNotification: "
                            "Timer expired and Toplogy change is called with"
                            "no information \r\n");
                    return;
                }

                pMisConfTrap = (tLldMisConfigTrap *) pNotifyInfo;

                SPRINTF ((char *) gau1Buf, "lldpRemChassisIdSubtype");
                pOid =
                    LldpMakeObjIdFrmString ((INT1 *) gau1Buf,
                            (UINT1 *) std_lldp_orig_mib_oid_table);
                if (pOid == NULL)
                {
                    SNMP_free_snmp_vb_list (pStartVb);
                    LLDP_TRC (ALL_FAILURE_TRC,
                            "LldpSendNotification: OID Not Found\r\n");
                    return;
                }

                pOid->pu4_OidList[pOid->u4_Length] =
                    pMisConfTrap->u4RemTimeMark;
                pOid->u4_Length++;
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pMisConfTrap->i4RemLocalPortNum;
                pOid->u4_Length++;
                pOid->pu4_OidList[pOid->u4_Length] = (UINT4) pMisConfTrap->i4RemIndex;
                pOid->u4_Length++;

                pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                        SNMP_DATA_TYPE_INTEGER,
                        0, pMisConfTrap->i4RemChassisIdSubtype, NULL,
                        NULL, SnmpCnt64Type);
                if (pVbList->pNextVarBind == NULL)
                {
                    SNMP_FreeOid (pOid);
                    SNMP_free_snmp_vb_list (pStartVb);
                    LLDP_TRC (ALL_FAILURE_TRC,
                            "LldpSendNotification: Variable Binding"
                            "Failed\r\n");
                    return;
                }
                pVbList = pVbList->pNextVarBind;

                SPRINTF ((char *) gau1Buf, "lldpRemChassisId");
                pOid =
                    LldpMakeObjIdFrmString ((INT1 *) gau1Buf,
                            (UINT1 *) std_lldp_orig_mib_oid_table);

                if (pOid == NULL)
                {
                    SNMP_free_snmp_vb_list (pStartVb);
                    LLDP_TRC (ALL_FAILURE_TRC,
                            "LldpSendNotification: OID Not Found\r\n");
                    return;
                }

                pOid->pu4_OidList[pOid->u4_Length] =
                    pMisConfTrap->u4RemTimeMark;
                pOid->u4_Length++;
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pMisConfTrap->i4RemLocalPortNum;
                pOid->u4_Length++;
                pOid->pu4_OidList[pOid->u4_Length] = (UINT4) 
                    pMisConfTrap->i4RemIndex;
                pOid->u4_Length++;

                /* Get the length of Chassis Id */
                LldpUtilGetChassisIdLen (pMisConfTrap->i4RemChassisIdSubtype,
                        pMisConfTrap->au1RemChassisId, &u2ChassisIdLen);

                /* Form the octet string */
                if ((pValueStr =
                            SNMP_AGT_FormOctetString(pMisConfTrap->
                                au1RemChassisId, u2ChassisIdLen)) == NULL)
                {
                    SNMP_FreeOid (pOid);
                    LLDP_TRC (ALL_FAILURE_TRC, "LldpSendNotification: "
                            "Form Octet String Failed\r\n");
                    return;
                }

                pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                        SNMP_DATA_TYPE_OCTET_PRIM,
                        0, 0, pValueStr,
                        NULL, SnmpCnt64Type);
                if (pVbList->pNextVarBind == NULL)
                {
                    SNMP_FreeOid (pOid);
                    SNMP_free_snmp_vb_list (pStartVb);
                    LLDP_TRC (ALL_FAILURE_TRC,
                            "LldpSendNotification: Variable Binding"
                            "Failed\r\n");
                    return;
                }
                pVbList = pVbList->pNextVarBind;

                SPRINTF ((char *) gau1Buf, "lldpXMedRemDeviceClass");

                pOid =
                    LldpMakeObjIdFrmString ((INT1 *) gau1Buf,
                            (UINT1 *) std_lldp_med_orig_mib_oid_table);
                if (pOid == NULL)
                {
                    SNMP_free_snmp_vb_list (pStartVb);
                    LLDP_TRC (ALL_FAILURE_TRC,
                            "LldpSendNotification: OID Not Found\r\n");
                    return;
                }

                pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                        SNMP_DATA_TYPE_INTEGER32,
                        0, pMisConfTrap->MisDeviceClass, 
                        NULL, NULL, SnmpCnt64Type);
                if (pVbList->pNextVarBind == NULL)
                {
                    SNMP_FreeOid (pOid);
                    SNMP_free_snmp_vb_list (pStartVb);
                    LLDP_TRC (ALL_FAILURE_TRC,
                            "LldpSendNotification: Variable Binding"
                            "Failed\r\n");
                    return;
                }
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                    "Remote Table Changed\n");
            LldpPortNotifyFaults (pStartVb, (UINT1 *) gac1SysLogMsg[u1TrapId],
                    FM_NOTIFY_MOD_ID_LLDP);
            gLldpGlobalInfo.bSendRemTblNotif = OSIX_FALSE;
            gLldpGlobalInfo.bSendTopChgNotif = OSIX_FALSE;
            /* Start the notification timer to handle the trap generation 
             * for every notfication interval configured. */
            if (TmrStart (gLldpGlobalInfo.TmrListId,
                        &(gLldpGlobalInfo.NotifIntervalTmr),
                        LLDP_NOTIF_INT_TIMER,
                        (UINT4) LLDP_NOTIF_INTERVAL (), 0) == TMR_FAILURE)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                        "LldpTmrNotifInterval: LLDP StartTimer for "
                        "Notification Interval failed\r\n");
                return;
            }
            LLDP_NOTIF_INTVAL_TMR_STATUS () = LLDP_TMR_RUNNING;

#ifdef L2RED_WANTED
            /* store the time interval */
            gLldpGlobalInfo.u4NotifTmrStartVal = 
                (UINT4) LLDP_NOTIF_INTERVAL ();
            /* Fill notification interval timer start sync up information */
            TmrSyncUpInfo.u1TmrType = (UINT1) LLDP_NOTIF_INT_TIMER;
            TmrSyncUpInfo.u4TmrInterval = (UINT4) LLDP_NOTIF_INTERVAL ();
            TmrSyncUpInfo.pMsapRBIndex = NULL;
            /* Send notification interva timer start sync up information */
            LldpRedSendSyncUpMsg ((UINT1) LLDP_RED_TMR_START_MSG,
                    (VOID *) &TmrSyncUpInfo);
#endif
            break;

        case LLDP_EXCEED_FRAME_SIZE:
            SPRINTF ((char *) gau1Buf, "lldpLocPortId");
            pOid =
                LldpMakeObjIdFrmString ((INT1 *) gau1Buf,
                        (UINT1 *) std_lldp_orig_mib_oid_table);
            if (pOid == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: OID Not Found\r\n");
                return;
            }
            pExceedFrameSize = (tLldpExceedFrameSize *) pNotifyInfo;

            /* Get the local port info from the port num and check the
               notification status. Trap should not be sent if the status
               is disabled */
            pPortEntry =
                LLDP_GET_LOC_PORT_INFO (pExceedFrameSize->u4LocPortNum);
            if (((pPortEntry) == NULL)
                    || (pPortEntry->PortConfigTable.u1NotificationEnable ==
                        LLDP_FALSE))
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpSendNotification: "
                        "Port Info not available or "
                        "notification status is disabled on Port %d\r\n",
                        pExceedFrameSize->u4LocPortNum);
                return;
            }

            pOid->pu4_OidList[pOid->u4_Length] = pExceedFrameSize->u4LocPortNum;
            pOid->u4_Length++;

            LldpUtilGetPortIdLen (pExceedFrameSize->i4LocPortIdSubtype,
                    pExceedFrameSize->au1LocPortId, &u2PortIdLen);

            /* Form the octet string */
            if ((pValueStr =
                        SNMP_AGT_FormOctetString
                        (pExceedFrameSize->au1LocPortId, u2PortIdLen)) == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC, "LldpSendNotification: "
                        "Form Octet String Failed\r\n");
                return;
            }
            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM,
                        0, 0, pValueStr, NULL, SnmpCnt64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                SNMP_AGT_FreeOctetString (pValueStr);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: Variable Binding Failed\r\n");
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                    "Exceeds Maximum frame Size\n");
            LldpPortNotifyFaults (pStartVb, (UINT1 *) gac1SysLogMsg[u1TrapId],
                    FM_NOTIFY_MOD_ID_LLDP);
            break;
        case LLDP_DUP_CHASIS_ID:
            pMisConfTrap = (tLldMisConfigTrap *) pNotifyInfo;

            /* Get the local port info from the port num and check the
               notification status. Trap should not be sent if the status
               is disabled */
            pPortEntry =
                LLDP_GET_LOC_PORT_INFO ((UINT4) pMisConfTrap->
                        i4RemLocalPortNum);
            if ((pPortEntry == NULL)
                    || (pPortEntry->PortConfigTable.u1NotificationEnable ==
                        LLDP_FALSE)
                    || (pPortEntry->PortConfigTable.u1FsConfigNotificationType ==
                        LLDP_REMOTE_CHG_NOTIFICATION))
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC, "LldpSendNotification: "
                        "Port Info not available or "
                        "notification status is disabled\r\n");
                return;
            }

            if (LldpGetObjIndexOIDs (pMisConfTrap,
                        &pStartVb, &pVbList) == OSIX_FAILURE)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                    "Duplicate Chasis ID\n");
            LldpPortNotifyFaults (pStartVb, (UINT1 *) gac1SysLogMsg[u1TrapId],
                    FM_NOTIFY_MOD_ID_LLDP);

            break;
        case LLDP_DUP_SYSTEM_NAME:
            pMisConfTrap = (tLldMisConfigTrap *) pNotifyInfo;

            /* Get the local port info from the port num and check the
               notification status. Trap should not be sent if the status
               is ,disabled */
            pPortEntry =
                LLDP_GET_LOC_PORT_INFO ((UINT4) pMisConfTrap->
                        i4RemLocalPortNum);
            if ((pPortEntry == NULL)
                    || (pPortEntry->PortConfigTable.u1NotificationEnable ==
                        LLDP_FALSE)
                    || (pPortEntry->PortConfigTable.u1FsConfigNotificationType ==
                        LLDP_REMOTE_CHG_NOTIFICATION))
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC, "LldpSendNotification: "
                        "Port Info not available or "
                        "notification status is disabled\r\n");
                return;
            }

            if (LldpGetObjIndexOIDs (pMisConfTrap,
                        &pStartVb, &pVbList) == OSIX_FAILURE)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }
            SPRINTF ((char *) gau1Buf, "lldpRemSysName");
            pOid =
                LldpMakeObjIdFrmString ((INT1 *) gau1Buf,
                        (UINT1 *) std_lldp_orig_mib_oid_table);
            if (pOid == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: OID Not Found\r\n");
                return;
            }
            pOid->pu4_OidList[pOid->u4_Length] = pMisConfTrap->u4RemTimeMark;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] =
                (UINT4) pMisConfTrap->i4RemLocalPortNum;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] = (UINT4) pMisConfTrap->i4RemIndex;
            pOid->u4_Length++;

            /* Form the octet string */
            if ((pValueStr =
                        SNMP_AGT_FormOctetString
                        (pMisConfTrap->DupSysName,
                         (INT4) LLDP_STRLEN (pMisConfTrap->DupSysName,
                             LLDP_MAX_LEN_SYSNAME))) == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC, "LldpSendNotification: "
                        "Form Octet String Failed\r\n");
                return;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM,
                        0, 0, pValueStr, NULL, SnmpCnt64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                SNMP_AGT_FreeOctetString (pValueStr);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: Variable Binding Failed\r\n");
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                    "Duplicate System Name\n");
            LldpPortNotifyFaults (pStartVb, (UINT1 *) gac1SysLogMsg[u1TrapId],
                    FM_NOTIFY_MOD_ID_LLDP);
            break;
        case LLDP_DUP_MAN_ADDR:
            pMisConfTrap = (tLldMisConfigTrap *) pNotifyInfo;

            /* Get the local port info from the port num and check the
               notification status. Trap should not be sent if the status
               is disabled */
            pPortEntry =
                LLDP_GET_LOC_PORT_INFO ((UINT4) pMisConfTrap->
                        i4RemLocalPortNum);
            if ((pPortEntry == NULL)
                    || (pPortEntry->PortConfigTable.u1NotificationEnable ==
                        LLDP_FALSE)
                    || (pPortEntry->PortConfigTable.u1FsConfigNotificationType ==
                        LLDP_REMOTE_CHG_NOTIFICATION))
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC, "LldpSendNotification: "
                        "Port Info not available or "
                        "notification status is disabled\r\n");
                return;
            }

            if (LldpGetObjIndexOIDs (pMisConfTrap,
                        &pStartVb, &pVbList) == OSIX_FAILURE)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }
            SPRINTF ((char *) gau1Buf, "lldpRemManAddr");
            pOid =
                LldpMakeObjIdFrmString ((INT1 *) gau1Buf,
                        (UINT1 *) std_lldp_orig_mib_oid_table);
            if (pOid == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: OID Not Found\r\n");
                return;
            }
            pOid->pu4_OidList[pOid->u4_Length] = pMisConfTrap->u4RemTimeMark;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] =
                (UINT4) pMisConfTrap->i4RemLocalPortNum;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] = (UINT4) pMisConfTrap->i4RemIndex;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] = pMisConfTrap->DupAddrSubType;
            pOid->u4_Length++;

            /* get the management address length based on the sub type */
            if (pMisConfTrap->DupAddrSubType == IPVX_ADDR_FMLY_IPV4)
            {
                u1ManAddrLen = IPVX_IPV4_ADDR_LEN;
            }
            else if (pMisConfTrap->DupAddrSubType == IPVX_ADDR_FMLY_IPV6)
            {
                u1ManAddrLen = IPVX_IPV6_ADDR_LEN;
            }

            /* copy management address in to OID string */
            for (u1Count = 0; u1Count < u1ManAddrLen; u1Count++)
            {
                pOid->pu4_OidList[pOid->u4_Length] =
                    pMisConfTrap->DupManAddress[u1Count];
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32,
                        0, pMisConfTrap->DupManAddrIfId, 0,
                        NULL, SnmpCnt64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: Variable Binding Failed\r\n");
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                    "Duplicate Management Address\n");
            LldpPortNotifyFaults (pStartVb, (UINT1 *) gac1SysLogMsg[u1TrapId],
                    FM_NOTIFY_MOD_ID_LLDP);
            break;
        case LLDP_MIS_CONFIG_PORT_VID:
            pMisConfTrap = (tLldMisConfigTrap *) pNotifyInfo;

            /* Get the local port info from the port num and check the
               notification status. Trap should not be sent if the status
               is disabled */
            pPortEntry =
                LLDP_GET_LOC_PORT_INFO ((UINT4) pMisConfTrap->
                        i4RemLocalPortNum);
            if ((pPortEntry == NULL)
                    || (pPortEntry->PortConfigTable.u1NotificationEnable ==
                        LLDP_FALSE)
                    || (pPortEntry->PortConfigTable.u1FsConfigNotificationType ==
                        LLDP_REMOTE_CHG_NOTIFICATION))
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC, "LldpSendNotification: "
                        "Port Info not available or "
                        "notification status is disabled\r\n");
                return;
            }

            if (LldpGetObjIndexOIDs (pMisConfTrap,
                        &pStartVb, &pVbList) == OSIX_FAILURE)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }
            SPRINTF ((char *) gau1Buf, "lldpXdot1RemPortVlanId");
            pOid = LldpMakeObjIdFrmString ((INT1 *) gau1Buf,
                    (UINT1 *)
                    std_lldp_dot1_orig_mib_oid_table);
            if (pOid == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: OID Not Found\r\n");
                return;
            }
            pOid->pu4_OidList[pOid->u4_Length] = pMisConfTrap->u4RemTimeMark;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] =
                (UINT4) pMisConfTrap->i4RemLocalPortNum;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] = (UINT4) pMisConfTrap->i4RemIndex;
            pOid->u4_Length++;

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid,
                        SNMP_DATA_TYPE_INTEGER32, 0,
                        pMisConfTrap->MisPortVlanId,
                        NULL, NULL, SnmpCnt64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: Variable Binding Failed\r\n");
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                    "Mis Configured Port Vlan ID\n");
            LldpPortNotifyFaults (pStartVb, (UINT1 *) gac1SysLogMsg[u1TrapId],
                    FM_NOTIFY_MOD_ID_LLDP);
            break;

        case LLDP_MIS_CONFIG_PPVID:
            pMisConfTrap = (tLldMisConfigTrap *) pNotifyInfo;

            /* Get the local port info from the port num and check the
               notification status. Trap should not be sent if the status
               is disabled */
            pPortEntry =
                LLDP_GET_LOC_PORT_INFO ((UINT4) pMisConfTrap->
                        i4RemLocalPortNum);
            if ((pPortEntry == NULL)
                    || (pPortEntry->PortConfigTable.u1NotificationEnable ==
                        LLDP_FALSE)
                    || (pPortEntry->PortConfigTable.u1FsConfigNotificationType ==
                        LLDP_REMOTE_CHG_NOTIFICATION))
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC, "LldpSendNotification: "
                        "Port Info not available or "
                        "notification status is disabled\r\n");
                return;
            }

            if (LldpGetObjIndexOIDs (pMisConfTrap,
                        &pStartVb, &pVbList) == OSIX_FAILURE)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }
            SPRINTF ((char *) gau1Buf, "lldpXdot1RemProtoVlanSupported");
            pOid = LldpMakeObjIdFrmString ((INT1 *) gau1Buf,
                    (UINT1 *)
                    std_lldp_dot1_orig_mib_oid_table);
            if (pOid == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: OID Not Found\r\n");
                return;
            }
            pOid->pu4_OidList[pOid->u4_Length] = pMisConfTrap->u4RemTimeMark;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] =
                (UINT4) pMisConfTrap->i4RemLocalPortNum;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] = (UINT4) pMisConfTrap->i4RemIndex;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] = pMisConfTrap->MisPpvid;
            pOid->u4_Length++;

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid,
                        SNMP_DATA_TYPE_INTEGER32, 0,
                        pMisConfTrap->MisVlanSupport,
                        NULL, NULL, SnmpCnt64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: Variable Binding Failed\r\n");
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                    "Mis Configured Port and Protocol VlanID\n");
            LldpPortNotifyFaults (pStartVb, (UINT1 *) gac1SysLogMsg[u1TrapId],
                    FM_NOTIFY_MOD_ID_LLDP);
            break;

        case LLDP_MIS_CONFIG_VLAN_NAME:
            pMisConfTrap = (tLldMisConfigTrap *) pNotifyInfo;

            /* Get the local port info from the port num and check the
               notification status. Trap should not be sent if the status
               is disabled */
            pPortEntry =
                LLDP_GET_LOC_PORT_INFO ((UINT4) pMisConfTrap->
                        i4RemLocalPortNum);
            if ((pPortEntry == NULL)
                    || (pPortEntry->PortConfigTable.u1NotificationEnable ==
                        LLDP_FALSE)
                    || (pPortEntry->PortConfigTable.u1FsConfigNotificationType ==
                        LLDP_REMOTE_CHG_NOTIFICATION))
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC, "LldpSendNotification: "
                        "Port Info not available or "
                        "notification status is disabled\r\n");
                return;
            }

            if (LldpGetObjIndexOIDs (pMisConfTrap,
                        &pStartVb, &pVbList) == OSIX_FAILURE)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }
            SPRINTF ((char *) gau1Buf, "lldpXdot1RemVlanName");
            pOid = LldpMakeObjIdFrmString ((INT1 *) gau1Buf,
                    (UINT1 *)
                    std_lldp_dot1_orig_mib_oid_table);
            if (pOid == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: OID Not Found\r\n");
                return;
            }
            pOid->pu4_OidList[pOid->u4_Length] = pMisConfTrap->u4RemTimeMark;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] =
                (UINT4)pMisConfTrap->i4RemLocalPortNum;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] = (UINT4) pMisConfTrap->i4RemIndex;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] = pMisConfTrap->MisVlanId;
            pOid->u4_Length++;

            /* Form the octet string */
            if ((pValueStr =
                        SNMP_AGT_FormOctetString
                        (pMisConfTrap->MisVlanName,
                         (INT4) LLDP_STRLEN (pMisConfTrap->MisVlanName,
                             VLAN_STATIC_MAX_NAME_LEN))) == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC, "LldpSendNotification: "
                        "Form Octet String Failed\r\n");
                return;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM,
                        0, 0, pValueStr, NULL, SnmpCnt64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                SNMP_AGT_FreeOctetString (pValueStr);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: Variable Binding Failed\r\n");
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                    "Mis Configured Vlan Name\n");
            LldpPortNotifyFaults (pStartVb, (UINT1 *) gac1SysLogMsg[u1TrapId],
                    FM_NOTIFY_MOD_ID_LLDP);
            break;
        case LLDP_MIS_CONFIG_PROTO_IDENTITY_CONFIG:
            pMisConfTrap = (tLldMisConfigTrap *) pNotifyInfo;

            /* Get the local port info from the port num and check the
               notification status. Trap should not be sent if the status
               is disabled */
            pPortEntry =
                LLDP_GET_LOC_PORT_INFO ((UINT4) pMisConfTrap->
                        i4RemLocalPortNum);
            if ((pPortEntry == NULL)
                    || (pPortEntry->PortConfigTable.u1NotificationEnable ==
                        LLDP_FALSE)
                    || (pPortEntry->PortConfigTable.u1FsConfigNotificationType ==
                        LLDP_REMOTE_CHG_NOTIFICATION))
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC, "LldpSendNotification: "
                        "Port Info not available or "
                        "notification status is disabled\r\n");
                return;
            }

            if (LldpGetObjIndexOIDs (pMisConfTrap,
                        &pStartVb, &pVbList) == OSIX_FAILURE)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }
            SPRINTF ((char *) gau1Buf, "lldpXdot1RemProtocolId");
            pOid = LldpMakeObjIdFrmString ((INT1 *) gau1Buf,
                    (UINT1 *)
                    std_lldp_dot1_orig_mib_oid_table);
            if (pOid == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: OID Not Found\r\n");
                return;
            }
            pOid->pu4_OidList[pOid->u4_Length] = pMisConfTrap->u4RemTimeMark;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] =
                (UINT4) pMisConfTrap->i4RemLocalPortNum;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] = (UINT4) pMisConfTrap->i4RemIndex;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] = pMisConfTrap->MisProtoIndex;
            pOid->u4_Length++;

            /* Form the octet string */
            if ((pValueStr =
                        SNMP_AGT_FormOctetString
                        (pMisConfTrap->MisProtoID,
                         (INT4) LLDP_STRLEN (pMisConfTrap->MisProtoID,
                             LLDP_MAX_LEN_PROTOID))) == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC, "LldpSendNotification: "
                        "Form Octet String Failed\r\n");
                return;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM,
                        0, 0, pValueStr, NULL, SnmpCnt64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                SNMP_AGT_FreeOctetString (pValueStr);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: Variable Binding Failed\r\n");
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                    "Mis Configured Protocol Identity\n");
            LldpPortNotifyFaults (pStartVb, (UINT1 *) gac1SysLogMsg[u1TrapId],
                    FM_NOTIFY_MOD_ID_LLDP);
            break;
        case LLDP_MIS_CONFIG_LINK_AGG_INFO:
            pMisConfTrap = (tLldMisConfigTrap *) pNotifyInfo;

            /* Get the local port info from the port num and check the
               notification status. Trap should not be sent if the status
               is disabled */
            pPortEntry =
                LLDP_GET_LOC_PORT_INFO ((UINT4) pMisConfTrap->
                        i4RemLocalPortNum);
            if ((pPortEntry == NULL)
                    || (pPortEntry->PortConfigTable.u1NotificationEnable ==
                        LLDP_FALSE)
                    || (pPortEntry->PortConfigTable.u1FsConfigNotificationType ==
                        LLDP_REMOTE_CHG_NOTIFICATION))
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC, "LldpSendNotification: "
                        "Port Info not available or "
                        "notification status is disabled\r\n");
                return;
            }

            if (LldpGetObjIndexOIDs (pMisConfTrap,
                        &pStartVb, &pVbList) == OSIX_FAILURE)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }
            SPRINTF ((char *) gau1Buf, "lldpXdot3RemLinkAggStatus");
            pOid = LldpMakeObjIdFrmString ((INT1 *) gau1Buf,
                    (UINT1 *)
                    std_lldp_dot3_orig_mib_oid_table);
            if (pOid == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: OID Not Found\r\n");
                return;
            }
            pOid->pu4_OidList[pOid->u4_Length] = pMisConfTrap->u4RemTimeMark;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] =
                (UINT4) pMisConfTrap->i4RemLocalPortNum;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] = (UINT4) pMisConfTrap->i4RemIndex;
            pOid->u4_Length++;

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid,
                        SNMP_DATA_TYPE_INTEGER32, 0,
                        pMisConfTrap->MisLinkAggInfo,
                        NULL, NULL, SnmpCnt64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: Variable Binding Failed\r\n");
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                    "Mis Configured Link Agg Info\n");
            LldpPortNotifyFaults (pStartVb, (UINT1 *) gac1SysLogMsg[u1TrapId],
                    FM_NOTIFY_MOD_ID_LLDP);
            break;
        case LLDP_MIS_CONFIG_POWER_VIA_MDI:
            pMisConfTrap = (tLldMisConfigTrap *) pNotifyInfo;

            /* Get the local port info from the port num and check the
               notification status. Trap should not be sent if the status
               is disabled */
            pPortEntry =
                LLDP_GET_LOC_PORT_INFO ((UINT4) pMisConfTrap->
                        i4RemLocalPortNum);
            if ((pPortEntry == NULL)
                    || (pPortEntry->PortConfigTable.u1NotificationEnable ==
                        LLDP_FALSE)
                    || (pPortEntry->PortConfigTable.u1FsConfigNotificationType ==
                        LLDP_REMOTE_CHG_NOTIFICATION))
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC, "LldpSendNotification: "
                        "Port Info not available or "
                        "notification status is disabled\r\n");
                return;
            }

            if (LldpGetObjIndexOIDs (pMisConfTrap,
                        &pStartVb, &pVbList) == OSIX_FAILURE)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }
            SPRINTF ((char *) gau1Buf, "lldpXdot3RemPowerClass");
            pOid = LldpMakeObjIdFrmString ((INT1 *) gau1Buf,
                    (UINT1 *)
                    std_lldp_dot3_orig_mib_oid_table);
            if (pOid == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: OID Not Found\r\n");
                return;
            }
            pOid->pu4_OidList[pOid->u4_Length] = pMisConfTrap->u4RemTimeMark;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] =
                (UINT4) pMisConfTrap->i4RemLocalPortNum;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] = (UINT4) pMisConfTrap->i4RemIndex;
            pOid->u4_Length++;

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid,
                        SNMP_DATA_TYPE_INTEGER32, 0,
                        pMisConfTrap->MisPowViaMDI,
                        NULL, NULL, SnmpCnt64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: Variable Binding Failed\r\n");
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                    "Mis Configured Power via MDI\n");
            LldpPortNotifyFaults (pStartVb, (UINT1 *) gac1SysLogMsg[u1TrapId],
                    FM_NOTIFY_MOD_ID_LLDP);
            break;
        case LLDP_MIS_CONFIG_MAX_FRAME_SIZE:
            pMisConfTrap = (tLldMisConfigTrap *) pNotifyInfo;

            /* Get the local port info from the port num and check the
               notification status. Trap should not be sent if the status
               is disabled */
            pPortEntry =
                LLDP_GET_LOC_PORT_INFO ((UINT4) pMisConfTrap->
                        i4RemLocalPortNum);
            if ((pPortEntry == NULL)
                    || (pPortEntry->PortConfigTable.u1NotificationEnable ==
                        LLDP_FALSE)
                    || (pPortEntry->PortConfigTable.u1FsConfigNotificationType ==
                        LLDP_REMOTE_CHG_NOTIFICATION))
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC, "LldpSendNotification: "
                        "Port Info not available or "
                        "notification status is disabled\r\n");
                return;
            }

            if (LldpGetObjIndexOIDs (pMisConfTrap,
                        &pStartVb, &pVbList) == OSIX_FAILURE)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }
            SPRINTF ((char *) gau1Buf, "lldpXdot3RemMaxFrameSize");
            pOid = LldpMakeObjIdFrmString ((INT1 *) gau1Buf,
                    (UINT1 *)
                    std_lldp_dot3_orig_mib_oid_table);
            if (pOid == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: OID Not Found\r\n");
                return;
            }
            pOid->pu4_OidList[pOid->u4_Length] = pMisConfTrap->u4RemTimeMark;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] =
                (UINT4) pMisConfTrap->i4RemLocalPortNum;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] = (UINT4) pMisConfTrap->i4RemIndex;
            pOid->u4_Length++;

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid,
                        SNMP_DATA_TYPE_INTEGER32, 0,
                        pMisConfTrap->MisMaxFrmSize,
                        NULL, NULL, SnmpCnt64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: Variable Binding Failed\r\n");
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC, 
                    "Mis configured Maximum Frame Size\n"); 
            LldpPortNotifyFaults (pStartVb, (UINT1 *) gac1SysLogMsg[u1TrapId],
                    FM_NOTIFY_MOD_ID_LLDP);
            break;
        case LLDP_MIS_CONFIG_OPER_MAU_TYPE:
            pMisConfTrap = (tLldMisConfigTrap *) pNotifyInfo;

            /* Get the local port info from the port num and check the
               notification status. Trap should not be sent if the status
               is disabled */
            pPortEntry =
                LLDP_GET_LOC_PORT_INFO ((UINT4) pMisConfTrap->
                        i4RemLocalPortNum);
            if ((pPortEntry == NULL)
                    || (pPortEntry->PortConfigTable.u1NotificationEnable ==
                        LLDP_FALSE)
                    || (pPortEntry->PortConfigTable.u1FsConfigNotificationType ==
                        LLDP_REMOTE_CHG_NOTIFICATION))
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC, "LldpSendNotification: "
                        "Port Info not available or "
                        "notification status is disabled\r\n");
                return;
            }

            if (LldpGetObjIndexOIDs (pMisConfTrap,
                        &pStartVb, &pVbList) == OSIX_FAILURE)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }
            SPRINTF ((char *) gau1Buf, "lldpXdot3RemPortOperMauType");
            pOid = LldpMakeObjIdFrmString ((INT1 *) gau1Buf,
                    (UINT1 *)
                    std_lldp_dot3_orig_mib_oid_table);
            if (pOid == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: OID Not Found\r\n");
                return;
            }
            pOid->pu4_OidList[pOid->u4_Length] = pMisConfTrap->u4RemTimeMark;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] =
                (UINT4) pMisConfTrap->i4RemLocalPortNum;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] = (UINT4) pMisConfTrap->i4RemIndex;
            pOid->u4_Length++;

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid,
                        SNMP_DATA_TYPE_INTEGER32, 0,
                        pMisConfTrap->MisOperMauType,
                        NULL, NULL, SnmpCnt64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: Variable Binding Failed\r\n");
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                    "Mis Confugred Operational MauType\n");
            LldpPortNotifyFaults (pStartVb, (UINT1 *) gac1SysLogMsg[u1TrapId],
                    FM_NOTIFY_MOD_ID_LLDP);
            break;

        case LLDP_MED_POLICY_MISMATCH:

            pMisConfTrap = (tLldMisConfigTrap *) pNotifyInfo;

            /* Get the local port info from the port num and check the
               notification status. Trap should not be sent if the status
               is ,disabled */
            pPortEntry =
                LLDP_GET_LOC_PORT_INFO ((UINT4) pMisConfTrap->
                        i4RemLocalPortNum);
            if ((pPortEntry == NULL)
                    || (pPortEntry->PortConfigTable.u1NotificationEnable ==
                        LLDP_FALSE)
                    || (pPortEntry->PortConfigTable.u1FsConfigNotificationType ==
                        LLDP_REMOTE_CHG_NOTIFICATION))
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC, "LldpSendNotification: "
                        "Port Info not available or "
                        "notification status is disabled\r\n");
                return;
            }

            if (LldpGetObjIndexOIDs (pMisConfTrap,
                        &pStartVb, &pVbList) == OSIX_FAILURE)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            /* Copy Oid of AppType */
            SPRINTF ((char *) gau1Buf, "fsLldpXMedMediaPolicyAppType");
            pOid =
                LldpMakeObjIdFrmString ((INT1 *) gau1Buf,
                        (UINT1 *) fs_lldp_med_orig_mib_oid_table);

            if (pOid == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: OID Not Found\r\n");
                return;
            }

            pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                    SNMP_DATA_TYPE_INTEGER32,
                    0, pMisConfTrap->MisAppType,
                    NULL, NULL, SnmpCnt64Type);

            /* Check if Varbind is Null */
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                LLDP_TRC (ALL_FAILURE_TRC,
                        "LldpSendNotification: Variable Binding Failed\r\n");
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                    "LLDP-MED Network Policy Mismatch\n");
            LldpPortNotifyFaults (pStartVb, (UINT1 *) gac1SysLogMsg[u1TrapId],
                    FM_NOTIFY_MOD_ID_LLDP);
            break;

        default:
            SNMP_free_snmp_vb_list (pStartVb);
            return;

    }
#else
    UNUSED_PARAM (u1TrapId);
    UNUSED_PARAM (pNotifyInfo);
#endif /* SNMP_3_WANTED */
}
