/***********************************************************************
 *  Copyright (C) 2007 Aricent Inc. All Rights Reserved.
 *
 *  $Id: lldcli.c,v 1.142 2017/12/28 10:01:47 siva Exp $
 *
 *  Description : This file contains CLI SET/GET/TEST and GETNEXT
 *                routines for the MIB objects specified in LLDP mibs. 
 ************************************************************************/

#ifndef _LLDCLI_C
#define _LLDCLI_C

#include "lldinc.h"
#include "lldcli.h"
#include "fslldwr.h"
#include "fslldpcli.h"
#include "stdlldcli.h"
#include "stdot1cli.h"
#include "stdot3cli.h"
#include "sd1lv2cli.h"
#include "utilcli.h"
#include "slldv2cli.h"
#include "sd3lv2cli.h"
#include "fslldmcli.h"
#include "stdlldpmcli.h"

const CHR1         *gapc1Support[] = { " ", "Supported", "Not Supported" };
const CHR1         *gapc1Enable[] = { " ", "Enabled", "Disabled" };
const CHR1          gac1SysCapab[] = { 'O', 'P', 'B', 'W', 'R', 'T', 'C', 'S' };

const CHR1         *gapc1PhyMediaCap[] =
    { "Other", "10base-T(HD)", "10base-T(FD)",
    "100base-T4", "100base-TX(HD)",
    "100base-TX(FD)", "100base-T2(HD)",
    "100base-T2(FD)", "FdxPause(FD)",
    "Asym PAUSE(FD)", "Symm PAUSE(FD)",
    "Asym and Symm PAUSE(FD)",
    "1000base-X, -LX, -SX, -CX(HD)",
    "1000base-X, -LX, -SX, -CX(FD)",
    "1000base-T(HD)", "1000base-T(FD)"
};

PRIVATE INT4
    LldpCliOctetToIfName PROTO ((tCliHandle CliHandle, CHR1 * pu1FirstLine,
                                 tSNMP_OCTET_STRING_TYPE * pOctetStr,
                                 UINT4 u4MaxPorts, UINT4 u4MaxLen,
                                 UINT1 u1Column, UINT4 *pu4PagingStatus,
                                 UINT1 u1MaxPortsPerLine));
/* Private Function Prototypes */
PRIVATE VOID LldpCliShowChassisIdSubType PROTO ((tCliHandle CliHandle,
                                                 INT4 i4ChassisIdSubType));

PRIVATE VOID LldpCliShowPortIdSubType PROTO ((tCliHandle CliHandle,
                                              INT4 i4PortIdSubType));

PRIVATE VOID LldpCliShowSysSupportCapab PROTO ((tCliHandle CliHandle,
                                                tSNMP_OCTET_STRING_TYPE
                                                SysCapSupported));

PRIVATE VOID LldpCliShowSysEnabledCapab PROTO ((tCliHandle CliHandle,
                                                tSNMP_OCTET_STRING_TYPE
                                                SysCapEnabled));

PRIVATE VOID LldpCliShowAutoNegAdvCap PROTO ((tCliHandle CliHandle,
                                              tSNMP_OCTET_STRING_TYPE
                                              * pPhyMediaCap));

PRIVATE INT4
    LldpCliShowIntfNeighborInfo PROTO ((UINT4 u4RemTimeMark,
                                        INT4 i4RemLocalPortNum,
                                        INT4 i4RemIndex,
                                        tLldpCliNeighborInfo * pCliNeighborInfo,
                                        UINT4 u4RemDestMacAddr,
                                        UINT1 u1MacSetFlag));

PRIVATE INT4
    LldpCliShowNeighborMSAPInfo PROTO ((UINT4 u4RemTimeMark,
                                        INT4 i4RemLocalPortNum,
                                        INT4 i4RemIndex,
                                        tLldpCliNeighborInfo * pCliNeighborInfo,
                                        UINT4 u4RemDestMacAddr));

PRIVATE INT4
    LldpCliShowNeighborMSAPIntfInfo PROTO ((UINT4 u4RemTimeMark,
                                            INT4 i4RemLocalPortNum,
                                            INT4 i4RemIndex,
                                            tLldpCliNeighborInfo *
                                            pCliNeighborInfo,
                                            UINT4 u4RemDestMacAddr));

PRIVATE INT4 LldpCliShowAllNeighborInfo PROTO ((UINT4 u4RemTimeMark,
                                                INT4 i4RemLocalPortNum,
                                                INT4 i4RemIndex,
                                                tLldpCliNeighborInfo
                                                * pCliNeighborInfo,
                                                UINT4 u4RemDestMacAddr));

PRIVATE INT4 LldpCliShowNeighborBriefInfo PROTO ((tCliHandle CliHandle,
                                                  UINT4 u4RemTimeMark,
                                                  INT4 i4RemLocalPortNum,
                                                  INT4 i4RemIndex,
                                                  UINT4 u4RemDestMacAddr));

PRIVATE INT4 LldpCliShowNeighborDetailInfo PROTO ((tCliHandle CliHandle,
                                                   UINT4 u4RemTimeMark,
                                                   INT4 i4RemLocalPortNum,
                                                   INT4 i4RemIndex,
                                                   UINT4 u4RemDestMacAddr));

PRIVATE VOID LldpCliShowLocDot3LinkAggStatus PROTO ((tCliHandle CliHandle,
                                                     tSNMP_OCTET_STRING_TYPE
                                                     * pDot3LAStatus));

PRIVATE VOID LldpCliShowLocEnabledTlvTx PROTO ((tCliHandle CliHandle,
                                                INT4 i4IfIndex,
                                                UINT4 u4DestMacIndex));

PRIVATE VOID LldpCliShowRemManAddr PROTO ((tCliHandle CliHandle,
                                           UINT4 u4RemTimeMark,
                                           INT4 i4RemLocalPortNum,
                                           INT4 i4RemIndex,
                                           UINT4 u4RemDestMacAddr));

PRIVATE INT4 LldpCliSetMgmtAddrTLV PROTO ((tCliHandle CliHandle,
                                           INT4 i4IfIndex,
                                           INT4 i4MgmtAddrSubType,
                                           UINT1 *pu1ManAddr, INT4 i4Val));

PRIVATE VOID LldpCliShowLocProtoVlanInfo PROTO ((tCliHandle CliHandle,
                                                 INT4 i4IfIndex));

PRIVATE VOID LldpCliShowLocVlanNameInfo PROTO ((tCliHandle CliHandle,
                                                INT4 i4IfIndex));

PRIVATE VOID LldpCliShowRemProtoVlanInfo PROTO ((tCliHandle CliHandle,
                                                 UINT4 u4RemProtoTimeMark,
                                                 INT4 i4IfIndex,
                                                 INT4 i4RemProtoIndex));

PRIVATE VOID LldpCliShowRemVlanNameInfo PROTO ((tCliHandle CliHandle,
                                                UINT4 u4RemVlanTimeMark,
                                                INT4 i4IfIndex,
                                                INT4 i4RemVlanIndex));

PRIVATE UINT1       LldpCliIsManAddrTlvEnabled
PROTO ((INT4 i4IfIndex, UINT4 u4DestMacIndex));

PRIVATE INT4        LldpMemRelease
PROTO ((tMemPoolId PoolIdInfo, INT4 u4RelCount, ...));

/*************************************************************************
 *
 *  FUNCTION NAME   : cli_process_lldp_cmd
 *
 *  DESCRIPTION     : CLI message handler function
 *
 *  INPUT           : CliHandle - CliContext ID
 *                    u4Command - Command identifier
 *                    ...       - Variable command argument list
 *
 *  OUTPUT          : None
 *
 *  RETURNS         : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PUBLIC INT4
cli_process_lldp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    tIp6Addr            Ip6Addr;
    UINT4              *apu4args[LLDP_CLI_MAX_ARGS];
    UINT4               u4DestMacAddrIndex = 0;
    UINT4               u4VlanId = 0;
    UINT4               u4Dscp = 0;
    UINT4               u4Priority = 0;
    UINT4               u4CaType = 0;
    UINT4               u4AltType = 0;
    UINT1               u1Dot3Tlv = 0;
    UINT1               u1BasicTlv = 0;
    INT1                i1argno = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4TlvType = 0;
    UINT4               u4AppType = 0;
    INT4                i4IfaceId = 0;
    INT4                i4IfIndex = 0;
    INT4                i4Status = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    INT4                i4VidDigest = OSIX_FALSE;
    INT4                i4MgmtVidDigest = OSIX_FALSE;
    INT4                i4LinkAggTLV = OSIX_FALSE;
    INT4                i4LocType = 0;
    UINT1               au1Loc[LLDP_MED_MAX_LOC_LENGTH];
    UINT1               au1MacAddr[MAC_ADDR_LEN];
    UINT1              *pau1MacAddr = NULL;
    UINT1               u1ShowOption = 0;
    UINT2               u2optTLV = 0;
    UINT1               u1InterfaceFlag = OSIX_FALSE;
    UINT1               au1LldpMcastAddr[MAC_ADDR_LEN] =
        { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x0E };
    BOOL1               bVlanType = LLDP_MED_VLAN_UNTAGGED;
    BOOL1               bPolicyUnKnown = 0;

    ;

    CliRegisterLock (CliHandle, LldpLock, LldpUnLock);
    LldpLock ();

    MEMSET (au1MacAddr, 0, MAC_ADDR_LEN);
    MEMSET (au1Loc, 0, LLDP_MED_MAX_LOC_LENGTH);

    nmhGetFsLldpSystemControl (&i4Status);

    if ((u4Command != LLDP_CLI_SYS_CTRL) && (i4Status != LLDP_START))
    {
        if (i4Status == LLDP_SHUTDOWN)
        {
            CliPrintf (CliHandle, "\r%% Lldp Module is shutdown\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "\r%% LLDP shutdown is in progress\r\n");
        }
        LldpUnLock ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* third argument is always interface name/index */
    i4IfaceId = va_arg (ap, INT4);
    MEMSET (apu4args, 0, LLDP_CLI_MAX_ARGS);
    while (1)
    {
        apu4args[i1argno++] = va_arg (ap, UINT4 *);

        if (i1argno == LLDP_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    switch (u4Command)
    {
        case LLDP_CLI_SYS_CTRL:

            /* apu4args[0] contains Start/Shutdown status */
            i4RetStatus = LldpCliSetSystemCtrl (CliHandle,
                                                CLI_PTR_TO_I4 ((apu4args[0])));
            break;

        case LLDP_CLI_MOD_STAT:

            /* apu4args[0] contains Enable/Disable status */
            i4RetStatus = LldpCliSetModuleStatus (CliHandle,
                                                  CLI_PTR_TO_I4
                                                  ((apu4args[0])));
            break;

        case LLDP_CLI_TX_INTRVL:

            i4RetStatus = LldpCliSetTransmitInterval (CliHandle,
                                                      *(INT4 *) (apu4args[0]));
            break;
        case LLDP_CLI_NO_TX_INTRVL:

            i4RetStatus = LldpCliSetTransmitInterval (CliHandle,
                                                      LLDP_MSG_TX_INTERVAL);
            break;

        case LLDP_CLI_HOLD_MULPR:

            i4RetStatus = LldpCliSetHoldtimeMuliplier (CliHandle,
                                                       *(INT4 *) (apu4args[0]));
            break;

        case LLDP_CLI_NO_HOLD_MULPR:

            i4RetStatus = LldpCliSetHoldtimeMuliplier
                (CliHandle, LLDP_MSG_TX_HOLD_MULTIPLIER);
            break;

        case LLDP_CLI_REINIT_DELAY:

            i4RetStatus = LldpCliSetReinitDelay (CliHandle,
                                                 *(INT4 *) apu4args[0]);
            break;

        case LLDP_CLI_NO_REINIT_DELAY:

            i4RetStatus = LldpCliSetReinitDelay (CliHandle, LLDP_REINIT_DELAY);
            break;

        case LLDP_CLI_TX_DELAY:

            i4RetStatus = LldpCliSetTransmitDelay (CliHandle,
                                                   *(INT4 *) apu4args[0]);
            break;

        case LLDP_CLI_NO_TX_DELAY:

            i4RetStatus = LldpCliSetTransmitDelay (CliHandle, LLDP_TX_DELAY);
            break;

        case LLDP_CLI_NOTIFY_INTRVL:

            i4RetStatus = LldpCliSetNotifyInterval (CliHandle,
                                                    *(INT4 *) apu4args[0]);
            break;

        case LLDP_CLI_NO_NOTIFY_INTRVL:

            i4RetStatus = LldpCliSetNotifyInterval (CliHandle,
                                                    LLDP_NOTIFICATION_INTERVAL);
            break;

        case LLDP_CLI_CHASSIS_SUBTYPE:

            /* apu4args[0] contains Chassis Id SubType */
            /* apu4args[1] contains Chassis Id */
            i4RetStatus = LldpCliSetChassisSubtype (CliHandle,
                                                    CLI_PTR_TO_I4 (apu4args[0]),
                                                    (UINT1 *) apu4args[1]);
            break;
        case LLDP_CLI_CLEAR_CNTRS:

            i4RetStatus = LldpCliClearCounters (CliHandle);
            break;

        case LLDP_CLI_CLEAR_TABLE:

            i4RetStatus = LldpCliClearTable (CliHandle);
            break;

        case LLDP_CLI_DEBUG_SHOW:

            i4RetStatus = LldpCliShowDebugging (CliHandle);
            break;

        case LLDP_CLI_DEBUG:

            i4RetStatus = LldpCliUpdTraceInput (CliHandle,
                                                (UINT1 *) (apu4args[0]));
            break;

        case LLDP_CLI_SHOW_GLOBAL:

            i4RetStatus = LldpCliShowGlobal (CliHandle);
            break;

        case LLDP_CLI_SHOW_INTF:
            /*apu4args[0] Contains the dest Mac address */
            if (apu4args[0] != NULL)
            {
                StrToMac ((UINT1 *) apu4args[0], au1MacAddr);
                pau1MacAddr = au1MacAddr;
            }

            i4RetStatus =
                LldpCliShowOpt (CliHandle, i4IfaceId, pau1MacAddr,
                                &u1ShowOption);
            if (i4RetStatus == CLI_FAILURE)
            {
                break;
            }

            i4RetStatus =
                LldpCliShowInterface (CliHandle, i4IfaceId, pau1MacAddr,
                                      u1ShowOption);
            break;

        case LLDP_CLI_SHOW_NEIGBR:

            /* apu4args[0] contains Option value to show Neighbors Info */
            /* apu4args[1] contains Chassis Id */
            /* apu4args[2] contains Port Id */

            i4RetStatus = LldpCliShowNeighbor (CliHandle,
                                               CLI_PTR_TO_I4 (apu4args[0]),
                                               i4IfaceId,
                                               (UINT1 *) (apu4args[1]),
                                               (UINT1 *) (apu4args[2]),
                                               pau1MacAddr);
            break;

        case LLDP_CLI_SHOW_PEER:

            /* apu4args[0] contains Option value to show Neighbors Info */
            /* apu4args[1] contains Chassis Id */
            /* apu4args[2] contains Port Id */
            /* apu4args[3] contains Dest Mac Address */
            if (apu4args[3] != NULL)
            {
                StrToMac ((UINT1 *) apu4args[3], au1MacAddr);
                pau1MacAddr = au1MacAddr;
            }

            i4RetStatus =
                LldpCliShowOpt (CliHandle, i4IfaceId, pau1MacAddr,
                                &u1ShowOption);
            if (i4RetStatus == CLI_FAILURE)
            {
                break;
            }

            i4RetStatus = LldpCliShowNeighbor (CliHandle,
                                               CLI_PTR_TO_I4 (apu4args[0]),
                                               i4IfaceId,
                                               (UINT1 *) (apu4args[1]),
                                               (UINT1 *) (apu4args[2]),
                                               pau1MacAddr);
            break;

        case LLDP_CLI_SHOW_TRAFFIC:
            /* apu4args[0] contains Dest Mac Address */
            if (apu4args[0] != NULL)
            {
                StrToMac ((UINT1 *) apu4args[0], au1MacAddr);
                pau1MacAddr = au1MacAddr;
            }

            i4RetStatus =
                LldpCliShowOpt (CliHandle, i4IfaceId, pau1MacAddr,
                                &u1ShowOption);
            if (i4RetStatus == CLI_FAILURE)
            {
                break;
            }

            i4RetStatus =
                LldpCliShowTraffic (CliHandle, i4IfaceId, pau1MacAddr,
                                    u1ShowOption);
            break;

        case LLDP_CLI_SHOW_LOCAL:
            /* apu4args[0] contains Management address Option */
            /* apu4args[1] contains Dest Mac Address */
            if (apu4args[1] != NULL)
            {
                StrToMac ((UINT1 *) apu4args[1], au1MacAddr);
                pau1MacAddr = au1MacAddr;
            }

            i4RetStatus =
                LldpCliShowOpt (CliHandle, i4IfaceId, pau1MacAddr,
                                &u1ShowOption);
            if (i4RetStatus == CLI_FAILURE)
            {
                break;
            }

            i4RetStatus =
                LldpCliGetMacAddrIndex (CliHandle, i4IfaceId, pau1MacAddr,
                                        &u4DestMacAddrIndex, &u1InterfaceFlag);
            if (i4RetStatus == CLI_FAILURE)
            {
                break;
            }

            i4RetStatus = LldpCliShowLocal (CliHandle, i4IfaceId,
                                            CLI_PTR_TO_I4 (apu4args[0]),
                                            u1ShowOption, u4DestMacAddrIndex);
            break;

        case LLDP_CLI_SHOW_ERRORS:

            i4RetStatus = LldpCliShowErrors (CliHandle);
            break;

        case LLDP_CLI_SHOW_STATS:

            i4RetStatus = LldpCliShowStatistics (CliHandle);
            break;

        case LLDP_CLI_TRANS_RECV:

            /* apu4args[0] contains Admin Status */
            /* apu4args[1] contains the Dest Mac Addr */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();

            if (apu4args[1] != NULL)
            {
                StrToMac ((UINT1 *) apu4args[1], au1MacAddr);
                pau1MacAddr = au1MacAddr;
            }

            i4RetStatus =
                LldpCliGetMacAddrIndex (CliHandle, i4IfIndex, pau1MacAddr,
                                        &u4DestMacAddrIndex, &u1InterfaceFlag);
            if (i4RetStatus == CLI_FAILURE)
            {
                break;
            }

            i4RetStatus = LldpCliSetAdminStatus (CliHandle, i4IfIndex,
                                                 CLI_PTR_TO_I4 (apu4args[0]),
                                                 u4DestMacAddrIndex,
                                                 u1InterfaceFlag);
            break;

        case LLDP_CLI_SET_NOTIFY:

            /* apu4args[0] contains Notification Enable/Disable Status */
            /* apu4args[1] contains Notification Type */
            /* apu4args[2] contains the Dest Mac Addr */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();

            if (apu4args[2] != NULL)
            {
                StrToMac ((UINT1 *) apu4args[2], au1MacAddr);
                pau1MacAddr = au1MacAddr;
            }

            i4RetStatus =
                LldpCliGetMacAddrIndex (CliHandle, i4IfIndex, pau1MacAddr,
                                        &u4DestMacAddrIndex, &u1InterfaceFlag);
            if (i4RetStatus == CLI_FAILURE)
            {
                break;
            }

            i4RetStatus = LldpCliSetNotification (CliHandle, i4IfIndex,
                                                  CLI_PTR_TO_I4 (apu4args[0]),
                                                  CLI_PTR_TO_I4 (apu4args[1]),
                                                  u4DestMacAddrIndex,
                                                  u1InterfaceFlag);
            break;

        case LLDP_CLI_TLV_SEL:

            /* apu4args[0] contains TLV sets to Set */
            /* apu4args[1] contains Management Address SubType */
            /* apu4args[2] contains Management Address */
            /* apu4args[3] contains dest Mac Addr */
            if ((apu4args[0] == NULL) && (apu4args[1] == NULL)
                && (apu4args[2] == NULL))
            {
                CliPrintf (CliHandle, "%% Incomplete command.\r\n");
                break;
            }
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            u4TlvType = CLI_PTR_TO_U4 (apu4args[0]);
            u1BasicTlv = (UINT1) u4TlvType;
            if (apu4args[2] != NULL)
            {
                if (CLI_PTR_TO_I4 (apu4args[1]) == IPVX_ADDR_FMLY_IPV4)
                {
                    u4IpAddr = *(apu4args[2]);
                    u4IpAddr = OSIX_HTONL (u4IpAddr);
                    MEMCPY (apu4args[2], &u4IpAddr, IPVX_IPV4_ADDR_LEN);
                }
            }

            if (apu4args[3] != NULL)
            {
                StrToMac ((UINT1 *) apu4args[3], au1MacAddr);
                pau1MacAddr = au1MacAddr;
            }

            i4RetStatus =
                LldpCliGetMacAddrIndex (CliHandle, i4IfIndex, pau1MacAddr,
                                        &u4DestMacAddrIndex, &u1InterfaceFlag);
            if (i4RetStatus == CLI_FAILURE)
            {
                break;
            }

            i4RetStatus = LldpCliSetTLV (CliHandle, i4IfIndex,
                                         &u1BasicTlv,
                                         CLI_PTR_TO_I4 (apu4args[1]),
                                         (UINT1 *) apu4args[2],
                                         u4DestMacAddrIndex, u1InterfaceFlag);
            break;

        case LLDP_CLI_NO_TLV_SEL:

            /* apu4args[0] contains TLV sets to Reset */
            /* apu4args[1] contains Management Address SubType */
            /* apu4args[2] contains Management Address */
            /* apu4args[3] contains Dest Mac Addr */
            if ((apu4args[0] == NULL) && (apu4args[1] == NULL)
                && (apu4args[2] == NULL))
            {
                CliPrintf (CliHandle, "%% Incomplete command.\r\n");
                break;
            }
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            u4TlvType = CLI_PTR_TO_U4 (apu4args[0]);
            u1BasicTlv = (UINT1) u4TlvType;
            if (apu4args[2] != NULL)
            {
                if (CLI_PTR_TO_I4 (apu4args[1]) == IPVX_ADDR_FMLY_IPV4)
                {

                    u4IpAddr = *(apu4args[2]);
                    u4IpAddr = OSIX_HTONL (u4IpAddr);
                    MEMCPY (apu4args[2], &u4IpAddr, IPVX_IPV4_ADDR_LEN);
                }
            }

            if (apu4args[3] != NULL)
            {
                StrToMac ((UINT1 *) apu4args[3], au1MacAddr);
                pau1MacAddr = au1MacAddr;
            }

            i4RetStatus =
                LldpCliGetMacAddrIndex (CliHandle, i4IfIndex, pau1MacAddr,
                                        &u4DestMacAddrIndex, &u1InterfaceFlag);
            if (i4RetStatus == CLI_FAILURE)
            {
                break;
            }

            i4RetStatus = LldpCliResetTLV (CliHandle, i4IfIndex,
                                           &u1BasicTlv,
                                           CLI_PTR_TO_I4 (apu4args[1]),
                                           (UINT1 *) apu4args[2],
                                           u4DestMacAddrIndex, u1InterfaceFlag);
            break;

        case LLDP_CLI_PORT_SUBTYPE:

            /* apu4args[0] contains Port Id SubType */
            /* apu4args[1] contains Port Id */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = LldpCliSetPortSubtype (CliHandle, i4IfIndex,
                                                 CLI_PTR_TO_I4 (apu4args[0]),
                                                 (UINT1 *) (apu4args[1]));
            break;

        case LLDP_CLI_DOT1TLV_SEL:

            /* apu4args[0] contains PortVlanId value */
            /* apu4args[1] contains ProtoVlanId value */
            /* apu4args[2] contains VlanName value */
            /* apu4args[3] contains All ProtoVlanId set value */
            /* apu4args[4] contains All VlanName set value */
            /* apu4args[5] contains VID Digest TLV  */
            /* apu4args[6] contains Management VID TLV */
            /* apu4args[7] contains Link Aggregation TLV */
            /* apu4args[8] contains Dest Mac Address */

            if ((apu4args[0] == NULL) && (apu4args[1] == NULL)
                && (apu4args[2] == NULL) && (apu4args[3] == NULL)
                && (apu4args[4] == NULL) && (apu4args[5] == NULL)
                && (apu4args[6] == NULL) && (apu4args[7] == NULL))
            {
                CliPrintf (CliHandle, "%% Incomplete command.\r\n");
                break;
            }
            if (apu4args[7] != NULL)
            {
                i4LinkAggTLV = (INT4) CLI_PTR_TO_I4 ((apu4args[7]));
            }
            if (apu4args[6] != NULL)
            {
                i4MgmtVidDigest = (INT4) CLI_PTR_TO_I4 (apu4args[6]);
            }
            if (apu4args[5] != NULL)
            {
                i4VidDigest = (INT4) CLI_PTR_TO_I4 (apu4args[5]);
            }

            if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
            {
                if ((i4VidDigest == LLDP_VID_USAGE_DIGEST_TLV)
                    || (i4MgmtVidDigest == LLDP_MGMT_VID_TLV)
                    || (i4LinkAggTLV == LLDP_V2_LINK_AGG_TLV))
                {
                    CliPrintf (CliHandle,
                               "%% Wrong Command these Options are supported in LLDPv2 Version \r\n");
                    break;
                }
            }

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();

            if (apu4args[8] != NULL)
            {
                StrToMac ((UINT1 *) apu4args[8], au1MacAddr);
                pau1MacAddr = au1MacAddr;
            }

            i4RetStatus =
                LldpCliGetMacAddrIndex (CliHandle, i4IfIndex, pau1MacAddr,
                                        &u4DestMacAddrIndex, &u1InterfaceFlag);

            if (i4RetStatus == CLI_FAILURE)
            {
                break;
            }

            i4RetStatus = LldpCliSetDot1TLV (CliHandle,
                                             i4IfIndex,
                                             CLI_PTR_TO_I4 (apu4args[0]),
                                             (UINT2 *) (apu4args[1]),
                                             (UINT1 *) (apu4args[2]),
                                             CLI_PTR_TO_I4 (apu4args[3]),
                                             CLI_PTR_TO_I4 (apu4args[4]),
                                             LLDP_TRUE, i4VidDigest,
                                             i4MgmtVidDigest, i4LinkAggTLV,
                                             u4DestMacAddrIndex,
                                             u1InterfaceFlag);
            break;

        case LLDP_CLI_NO_DOT1TLV_SEL:

            /* apu4args[0] contains PortVlanId value */
            /* apu4args[1] contains ProtoVlanId value */
            /* apu4args[2] contains VlanName value */
            /* apu4args[3] contains All ProtoVlanId set value */
            /* apu4args[4] contains All VlanName set value */
            /* apu4args[5] contains VID Digest TLV  */
            /* apu4args[6] contains Management VID TLV */
            /* apu4args[7] contains Link Aggregation TLV */
            /* apu4args[8] contains Dest Mac Address */

            if ((apu4args[0] == NULL) && (apu4args[1] == NULL)
                && (apu4args[2] == NULL) && (apu4args[3] == NULL)
                && (apu4args[4] == NULL) && (apu4args[5] == NULL)
                && (apu4args[6] == NULL) && (apu4args[7] == NULL))
            {
                CliPrintf (CliHandle, "%% Incomplete command.\r\n");
                break;
            }

            if (apu4args[7] != NULL)
            {
                i4LinkAggTLV = (INT4) CLI_PTR_TO_I4 ((apu4args[7]));
            }
            if (apu4args[6] != NULL)
            {
                i4MgmtVidDigest = (INT4) CLI_PTR_TO_I4 (apu4args[6]);
            }
            if (apu4args[5] != NULL)
            {
                i4VidDigest = (INT4) CLI_PTR_TO_I4 (apu4args[5]);
            }

            if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
            {
                if ((i4VidDigest == LLDP_VID_USAGE_DIGEST_TLV)
                    || (i4MgmtVidDigest == LLDP_MGMT_VID_TLV)
                    || (i4LinkAggTLV == LLDP_V2_LINK_AGG_TLV))
                {
                    CliPrintf (CliHandle,
                               "%% Wrong Command these Options are supported in LLDPv2 Version\r\n");
                    break;
                }
            }

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();

            if (apu4args[8] != NULL)
            {
                StrToMac ((UINT1 *) apu4args[8], au1MacAddr);
                pau1MacAddr = au1MacAddr;
            }

            i4RetStatus =
                LldpCliGetMacAddrIndex (CliHandle, i4IfIndex, pau1MacAddr,
                                        &u4DestMacAddrIndex, &u1InterfaceFlag);

            if (i4RetStatus == CLI_FAILURE)
            {
                break;
            }
            i4RetStatus = LldpCliSetDot1TLV (CliHandle, i4IfIndex,
                                             CLI_PTR_TO_I4 (apu4args[0]),
                                             (UINT2 *) (apu4args[1]),
                                             (UINT1 *) (apu4args[2]),
                                             CLI_PTR_TO_I4 (apu4args[3]),
                                             CLI_PTR_TO_I4 (apu4args[4]),
                                             LLDP_FALSE, i4VidDigest,
                                             i4MgmtVidDigest, i4LinkAggTLV,
                                             u4DestMacAddrIndex,
                                             u1InterfaceFlag);
            break;

        case LLDP_CLI_DOT3TLV_SEL:

            /* apu4args[0] contains Dot3 TLV sets to Set */
            if (apu4args[0] == NULL)
            {
                CliPrintf (CliHandle, "%% Incomplete command.\r\n");
                break;
            }
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            u4TlvType = CLI_PTR_TO_U4 (apu4args[0]);
            u1Dot3Tlv = (UINT1) u4TlvType;
            if ((u1Dot3Tlv & LLDP_LINK_AGG_TLV_ENABLED)
                && (gLldpGlobalInfo.i4Version == LLDP_VERSION_09))
            {
                CliPrintf (CliHandle,
                           "%% Link Aggregation in Dot3Tlv is supported in LLDPv1\r\n");
            }
            else
            {
                i4RetStatus = LldpCliSetDot3TLV (CliHandle, i4IfIndex,
                                                 u1Dot3Tlv, OSIX_TRUE);
            }

            break;

        case LLDP_CLI_NO_DOT3TLV_SEL:

            /* apu4args[0] contains Dot3 TLV sets to Set */
            if (apu4args[0] == NULL)
            {
                CliPrintf (CliHandle, "%% Incomplete command.\r\n");
                break;
            }
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            u4TlvType = CLI_PTR_TO_U4 (apu4args[0]);
            u1Dot3Tlv = (UINT1) u4TlvType;
            i4RetStatus = LldpCliSetDot3TLV (CliHandle, i4IfIndex,
                                             u1Dot3Tlv, OSIX_FALSE);
            break;

        case LLDP_CLI_SET_DST_MAC:

            /* args[0] - destination MAC address */
            StrToMac ((UINT1 *) apu4args[0], au1MacAddr);

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus =
                LldpCliSetDstMac (CliHandle, i4IfIndex, au1MacAddr,
                                  LLDP_CREATE_AND_GO);
            break;

        case LLDP_CLI_RESET_DST_MAC:
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            if (apu4args[0] != NULL)
            {
                StrToMac ((UINT1 *) apu4args[0], au1MacAddr);
            }
            else
            {
                MEMCPY (au1MacAddr, au1LldpMcastAddr, MAC_ADDR_LEN);
            }
            i4RetStatus =
                LldpCliSetDstMac (CliHandle, i4IfIndex, au1MacAddr,
                                  LLDP_DESTROY);
            break;

        case LLDP_CLI_MOD_VERSION:

            /* apu4args[0] contains LLDP_VERSION_05/LLDP_VERSION_09  status */
            i4RetStatus = LldpCliSetModuleVersion (CliHandle,
                                                   CLI_PTR_TO_I4
                                                   ((apu4args[0])));
            break;
        case LLDP_TX_CREDIT_MAX:

            i4RetStatus = LldpCliSetTxCreditMax (CliHandle, *(apu4args[0]));
            break;

        case LLDP_MESSAGE_FAST_TX:

            i4RetStatus = LldpCliSetMessageFastTx (CliHandle, *(apu4args[0]));
            break;

        case LLDP_TX_FAST_INIT:

            i4RetStatus = LldpCliSetTxFastInit (CliHandle, *(apu4args[0]));
            break;
        case LLDP_MED_FAST_REPEAT_COUNT:

            i4RetStatus =
                LldpCliSetMedFastRepeatCount (CliHandle, *(apu4args[0]));
            break;
        case LLDP_MED_CLI_TLV_SEL:

            /* LLDP-MED */
            /* Get IfIndex from CliGetModeInfo function 
             * through CLI_GET_IFINDEX */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            u4TlvType = CLI_PTR_TO_U4 (apu4args[0]);
            u2optTLV = (UINT2) u4TlvType;

            if (apu4args[1] != NULL)
            {
                StrToMac ((UINT1 *) apu4args[1], au1MacAddr);
                pau1MacAddr = au1MacAddr;
            }

            /* Get Destination MacAddressindex and interface flag by
             * passing mac address and IfIndex to LldpCliGetMacAddrIndex */
            i4RetStatus =
                LldpCliGetMacAddrIndex (CliHandle, i4IfIndex, pau1MacAddr,
                                        &u4DestMacAddrIndex, &u1InterfaceFlag);

            if (i4RetStatus != CLI_SUCCESS)
            {
                break;
            }

            i4RetStatus = LldpMedCliSetTlv (CliHandle, i4IfIndex,
                                            u2optTLV,
                                            u4DestMacAddrIndex,
                                            u1InterfaceFlag, LLDP_MED_TLV_ADD);

            break;

        case LLDP_MED_CLI_NO_TLV_SEL:

            /* Get IfIndex from CliGetModeInfo function 
             * through CLI_GET_IFINDEX */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            u4TlvType = CLI_PTR_TO_U4 (apu4args[0]);
            u2optTLV = (UINT2) u4TlvType;

            if (apu4args[1] != NULL)
            {
                StrToMac ((UINT1 *) apu4args[1], au1MacAddr);
                pau1MacAddr = au1MacAddr;
            }

            /* Get Destination MacAddressindex and interface flag by
             * passing mac address and IfIndex to LldpCliGetMacAddrIndex */
            i4RetStatus =
                LldpCliGetMacAddrIndex (CliHandle, i4IfIndex, pau1MacAddr,
                                        &u4DestMacAddrIndex, &u1InterfaceFlag);

            if (i4RetStatus != CLI_SUCCESS)
            {
                break;
            }

            i4RetStatus = LldpMedCliSetTlv (CliHandle, i4IfIndex,
                                            u2optTLV,
                                            u4DestMacAddrIndex,
                                            u1InterfaceFlag, LLDP_MED_TLV_DEL);

            break;

        case LLDP_MED_CLI_NW_POLICY_CONFIG:

            /* Get IfIndex from CliGetModeInfo function 
             * through CLI_GET_IFINDEX */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            u4AppType = CLI_PTR_TO_U4 (apu4args[0]);

            /* Check for Network Policy Unknown Flag */
            if (apu4args[3] == NULL)
            {
                bPolicyUnKnown = LLDP_MED_UNKNOWN_FLAG;
            }
            else
            {
                /* Get Dscp value. Dscp, VlanId, Priority values will be ignored 
                 * if Unknown flag is set */
                u4Dscp = *(apu4args[3]);
                if ((apu4args[1] != NULL) && (apu4args[2] != NULL))
                {
                    u4VlanId = *(apu4args[1]);
                    bVlanType = LLDP_MED_VLAN_TAGGED;
                    u4Priority = *(apu4args[2]);
                }
            }

            i4RetStatus = LldpMedCliSetNwPolicyConfig (CliHandle,
                                                       i4IfIndex,
                                                       (UINT1) u4AppType,
                                                       bPolicyUnKnown,
                                                       bVlanType,
                                                       u4VlanId,
                                                       u4Priority, u4Dscp);
            break;

        case LLDP_MED_CLI_NO_NW_POLICY_CONFIG:

            /* Get IfIndex from CliGetModeInfo function 
             * through CLI_GET_IFINDEX */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            u4AppType = CLI_PTR_TO_U4 (apu4args[0]);

            i4RetStatus = LldpMedCliDelNwPolicyConfig (CliHandle,
                                                       i4IfIndex,
                                                       (UINT1) u4AppType);
            break;
        case LLDP_CLI_TRANSMIT_TAG:
            i4RetStatus = LldpCliSetTagStatus (CliHandle,
                                               CLI_PTR_TO_I4 ((apu4args[0])));
            break;
        case LLDP_CLI_SET_MGMT_ADDR:
            if (apu4args[1] != NULL)
            {
                if (CLI_PTR_TO_I4 (apu4args[0]) == IPVX_ADDR_FMLY_IPV4)
                {
                    u4IpAddr = *(apu4args[1]);
                    u4IpAddr = OSIX_HTONL (u4IpAddr);
                    MEMCPY (apu4args[1], &u4IpAddr, IPVX_IPV4_ADDR_LEN);
                    i4RetStatus = LldpCliSetManagementIpAddress (CliHandle,
                                                                 CLI_PTR_TO_I4
                                                                 (apu4args[0]),
                                                                 (UINT1 *)
                                                                 apu4args[1]);
                }
                /* IPv6 Address */
                else if (CLI_PTR_TO_I4 (apu4args[0]) == IPVX_ADDR_FMLY_IPV6)
                {
                    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
                    if (INET_ATON6 (apu4args[1], &Ip6Addr) == 0)
                    {
                        i4RetStatus = CLI_FAILURE;
                        CLI_SET_ERR (LLDP_CLI_INVALID_IP6_ADDRESS);
                        break;
                    }
                    i4RetStatus =
                        LldpCliSetManagementIpv6Address (CliHandle,
                                                         IPVX_ADDR_FMLY_IPV6,
                                                         Ip6Addr);
                }
            }
            break;

        case LLDP_MED_CLI_ELIN_CONFIG:

            /* Get IfIndex from CliGetModeInfo function
             * through CLI_GET_IFINDEX */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = LldpMedCliSetElinConfig (CliHandle,
                                                   i4IfIndex,
                                                   (UINT1 *) apu4args[0]);
            break;

        case LLDP_MED_CLI_LOC_CONFIG:

            /* Parsing Octet string to validate */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4LocType = CLI_PTR_TO_I4 (apu4args[0]);
            i4RetStatus = LldpMedCliParseLocOctetStr (CliHandle,
                                                      (UINT1 *) apu4args[1]);
            if (i4RetStatus != CLI_SUCCESS)
            {
                break;
            }
            /* Converting octet string to Location info string */
            i4RetStatus = LldpCliDotStrToLocInfo (CliHandle,
                                                  (UINT1 *) apu4args[1],
                                                  i4LocType, au1Loc);
            if (i4RetStatus != CLI_SUCCESS)
            {
                break;
            }
            /* Setting Location Info */
            i4RetStatus = LldpMedCliSetLocConfig (CliHandle,
                                                  i4IfIndex, i4LocType, au1Loc);
            break;

        case LLDP_MED_CLI_COORDINATE_CONFIG:

            /* Get IfIndex from CliGetModeInfo function
             * through CLI_GET_IFINDEX */

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            u4AltType = CLI_PTR_TO_U4 (apu4args[0]);
            i4RetStatus = LldpMedCliSetCoordinateLocConfig (CliHandle,
                                                            i4IfIndex,
                                                            *(UINT1
                                                              *) (apu4args[0]),
                                                            (UINT1
                                                             *) (apu4args[1]),
                                                            *(UINT1
                                                              *) (apu4args[2]),
                                                            (UINT1
                                                             *) (apu4args[3]),
                                                            (UINT1) u4AltType,
                                                            (UINT1
                                                             *) (apu4args[5]),
                                                            *(UINT1
                                                              *) (apu4args[6]),
                                                            *(UINT1
                                                              *) (apu4args[7]));

            break;

        case LLDP_MED_CLI_CIVIC_COUNTRY:

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = LldpMedCliSetCountryCodeConfig (CliHandle,
                                                          i4IfIndex,
                                                          (UINT1
                                                           *) (apu4args[0]));
            break;

        case LLDP_MED_CLI_CIVIC_LOC:

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            u4CaType = CLI_PTR_TO_U4 (apu4args[0]);
            i4RetStatus = LldpMedCliSetCaTypeConfig (CliHandle,
                                                     i4IfIndex,
                                                     (UINT1) u4CaType,
                                                     (UINT1 *) (apu4args[1]));

            break;

        case LLDP_MED_CLI_CIVIC_ACTIVE:

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = LldpMedCliSetCivicActive (CliHandle, i4IfIndex);

            break;

        case LLDP_MED_CLI_NO_LOC_CONFIG:

            /* Get IfIndex from CliGetModeInfo function
             * through CLI_GET_IFINDEX */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4LocType = CLI_PTR_TO_I4 (apu4args[0]);
            i4RetStatus = LldpMedCliDelLocConfig (CliHandle,
                                                  i4IfIndex, i4LocType);

            break;
        case LLDP_MED_CLI_MOD_STAT:
            /* Get IfIndex from CliGetModeInfo function
             * through CLI_GET_IFINDEX */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            /* apu4args[0] contains Enable/Disable status */
            i4RetStatus = LldpMedCliSetModuleStatus (CliHandle,
                                                     i4IfIndex,
                                                     CLI_PTR_TO_I4
                                                     ((apu4args[0])));
            break;

        default:

            CliPrintf (CliHandle, "\r%% Unknown command \r\n");
            LldpUnLock ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {

        if ((u4ErrCode > 0) && (u4ErrCode < LLDP_CLI_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", gapc1LldpCliErrString[u4ErrCode]);
        }

        CLI_SET_ERR (0);
    }

    LldpUnLock ();
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetSystemCtrl
 *
 *     DESCRIPTION      : This function will start/shut Lldp module
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Status   - Lldp System Control Status
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliSetSystemCtrl (tCliHandle CliHandle, INT4 i4Status)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsLldpSystemControl (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsLldpSystemControl (i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetModuleStatus
 *
 *     DESCRIPTION      : This function will enable/disable Lldp module
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Status   - Lldp Module status
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliSetModuleStatus (tCliHandle CliHandle, INT4 i4Status)
{

    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsLldpModuleStatus (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsLldpModuleStatus (i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetTransmitInterval
 *
 *     DESCRIPTION      : This module sets the interval (in seconds) at
 *                        which LLDP frames are transmitted.
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Interval  - Lldp Transmission Interval
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliSetTransmitInterval (tCliHandle CliHandle, INT4 i4Interval)
{

    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2LldpV2MessageTxInterval (&u4ErrorCode, (UINT4) i4Interval)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhSetLldpV2MessageTxInterval (i4Interval);
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetHoldtimeMuliplier
 *
 *     DESCRIPTION      : This module changes the multiplier (An LLDP switch
 *                        uses the multiplier to calculate the Time-to-Live
 *                        for the LLDP advertisements) on LLDP.
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Val      - Lldp Holdtime Multiplier
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliSetHoldtimeMuliplier (tCliHandle CliHandle, INT4 i4Val)
{

    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2LldpV2MessageTxHoldMultiplier (&u4ErrorCode, (UINT4) i4Val) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhSetLldpV2MessageTxHoldMultiplier (i4Val);
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetReinitDelay
 *
 *     DESCRIPTION      : Sets the delay time (in seconds) for LLDP
 *                        to re-initialize on any interface
 *
 *     INPUT            : CliHandle     - CliContext ID
 *                        i4ReinitDelay - Lldp Reinit Delay
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliSetReinitDelay (tCliHandle CliHandle, INT4 i4ReinitDelay)
{

    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2LldpV2ReinitDelay (&u4ErrorCode, (UINT4) i4ReinitDelay) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetLldpV2ReinitDelay (i4ReinitDelay);
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetTransmitDelay
 *
 *     DESCRIPTION      : Sets the delay (in seconds) between successive
 *                        LLDP frame transmission
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4TxDelay  - Lldp Transmit Delay
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliSetTransmitDelay (tCliHandle CliHandle, INT4 i4TxDelay)
{

    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2LldpTxDelay (&u4ErrorCode, i4TxDelay) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhSetLldpTxDelay (i4TxDelay);
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetNotifyInterval
 *
 *     DESCRIPTION      : Sets the interval (in seconds) between successive
 *                        traps generated by the switch
 *
 *     INPUT            : CliHandle        - CliContext ID
 *                        i4NotifyInterval - LLDP Notification Interval
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliSetNotifyInterval (tCliHandle CliHandle, INT4 i4NotifyInterval)
{

    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2LldpV2NotificationInterval
        (&u4ErrorCode, (UINT4) i4NotifyInterval) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhSetLldpV2NotificationInterval (i4NotifyInterval);
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetChassisSubtype
 *
 *     DESCRIPTION      : Configures lldp chassis id subtype and chassis
 *                        id value
 *
 *     INPUT            : CliHandle        - CliContext ID
 *                        i4ChassisSubtype - Chassis id subtype
 *                        pu1ChassisId     - Chassis id
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliSetChassisSubtype (tCliHandle CliHandle, INT4 i4ChassisIdSubtype,
                          UINT1 *pu1ChassisId)
{
    tSNMP_OCTET_STRING_TYPE ChassisId;
    UINT4               u4ErrorCode = 0;
    UINT2               u2ChassisIdLen = 0;

    MEMSET (&ChassisId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    if (nmhTestv2FsLldpLocChassisIdSubtype (&u4ErrorCode, i4ChassisIdSubtype)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsLldpLocChassisIdSubtype (i4ChassisIdSubtype) == SNMP_FAILURE)
    {
        if (LLDP_CHASS_ID_SUB_IF_ALIAS == i4ChassisIdSubtype)
        {
            CliPrintf (CliHandle,
                       "\r\n%% Default L3 VLAN should be configured to set the "
                       "chassis-id Subtype to if-alias\r\n");
        }
        else if (LLDP_CHASS_ID_SUB_NW_ADDR == i4ChassisIdSubtype)
        {
            CliPrintf (CliHandle,
                       "\r\n%% Default L3 VLAN should be configured to set the "
                       "chassis-id Subtype to nw-addr\r\n");
        }
        else if (LLDP_CHASS_ID_SUB_IF_NAME == i4ChassisIdSubtype)
        {
            CliPrintf (CliHandle,
                       "\r\n%% Default L3 VLAN should be configured to set the "
                       "chassis-id Subtype to if-name\r\n");
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
        return CLI_FAILURE;
    }
    if ((i4ChassisIdSubtype == LLDP_CHASS_ID_SUB_CHASSIS_COMP) ||
        (i4ChassisIdSubtype == LLDP_CHASS_ID_SUB_PORT_COMP) ||
        (i4ChassisIdSubtype == LLDP_CHASS_ID_SUB_LOCAL))

    {
        LldpUtilGetChassisIdLen (i4ChassisIdSubtype, pu1ChassisId,
                                 &u2ChassisIdLen);
        ChassisId.pu1_OctetList = pu1ChassisId;
        ChassisId.i4_Length = (INT4) u2ChassisIdLen;

        if (nmhTestv2FsLldpLocChassisId (&u4ErrorCode, &ChassisId)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        nmhSetFsLldpLocChassisId (&ChassisId);
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *     FUNCTION NAME    : LldpMedCliSetTlv
 *
 *     DESCRIPTION      : Enables or Disables the particular LLDP-MED TLV
 *                        transmission on given switch port
 *
 *     INPUT            : CliHandle          - CliContext ID
 *                        i4IfIndex          - Interface Index
 *                        u2optTLV           - Enabled or Disabled TLV set
 *                        u4DestMacAddrIndex - Destination Mac address Index of
 *                        the agent
 *                        u1InterfaceFlag    - Interface Flag
 *                        i4SetVal           - Add Status or Delete Status of TLV
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/
PUBLIC INT4
LldpMedCliSetTlv (tCliHandle CliHandle,
                  INT4 i4IfIndex,
                  UINT2 u2optTLV,
                  UINT4 u4DestMacAddrIndex,
                  UINT1 u1InterfaceFlag, INT4 i4SetVal)
{
    tSNMP_OCTET_STRING_TYPE LldpMedPortTLVsEnabled;
    tSNMP_OCTET_STRING_TYPE LldpMedGetPortTLVsEnabled;
    UINT4               u4PrevDestMacIndex = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4PrevIfIndex = 0;
    INT4                i4Index = 0;
    UINT2               u2TlvsEnabled = 0;
    UINT2               u2TlvsSelected = 0;

    MEMSET (&LldpMedPortTLVsEnabled, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&LldpMedGetPortTLVsEnabled, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    /* Assign LLDP MED enabled TLVs to LldpMedPortTLVsEnabled structure */
    LldpMedPortTLVsEnabled.pu1_OctetList = (UINT1 *) &u2optTLV;
    LldpMedPortTLVsEnabled.i4_Length = sizeof (UINT2);
    /* Intialize another octet list which is used to get the TLVs enabled
     * previously and update it */
    LldpMedGetPortTLVsEnabled.pu1_OctetList = (UINT1 *) &u2TlvsEnabled;
    LldpMedGetPortTLVsEnabled.i4_Length = sizeof (UINT2);
    u2TlvsSelected = u2optTLV;
    i4Index = i4IfIndex;

    do
    {
        /* Check if the interface flag is set and agent is of same port */
        if ((u1InterfaceFlag == OSIX_TRUE) && (i4IfIndex != i4Index))
        {
            i4PrevIfIndex = i4IfIndex;
            u4PrevDestMacIndex = u4DestMacAddrIndex;
        }
        else if ((u1InterfaceFlag != OSIX_TRUE) ||
                 ((u1InterfaceFlag == OSIX_TRUE) && (i4IfIndex == i4Index)))
        {
            if (nmhTestv2FsLldpMedPortConfigTLVsTxEnable (&u4ErrorCode,
                                                          i4Index,
                                                          u4DestMacAddrIndex,
                                                          &LldpMedPortTLVsEnabled)
                != SNMP_SUCCESS)
            {
                if (u4ErrorCode == SNMP_ERR_WRONG_VALUE)
                {
                    CliPrintf (CliHandle,
                               "\r\n%%TX_RX id disabled,"
                               "So Unable to set Selected Tlvs\r\n");
                    return CLI_FAILURE;
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r\n%%Unable to set Selected Tlvs\r\n");
                }

                return CLI_FAILURE;
            }
            if (nmhGetFsLldpMedPortConfigTLVsTxEnable (i4Index,
                                                       u4DestMacAddrIndex,
                                                       &LldpMedGetPortTLVsEnabled)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            /* Check whether TLV select is to set or 
             * delete the configuration */
            if (i4SetVal == LLDP_MED_TLV_ADD)
            {
                u2optTLV |= u2TlvsEnabled;
            }
            else
            {
                u2optTLV = ~u2optTLV;
                u2optTLV &= u2TlvsEnabled;
            }

            if (nmhSetFsLldpMedPortConfigTLVsTxEnable (i4Index,
                                                       u4DestMacAddrIndex,
                                                       &LldpMedPortTLVsEnabled)
                != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            /* If interface flag is not set break the loop for next IfIndex */
            if (u1InterfaceFlag != OSIX_TRUE)
            {
                break;
            }
            i4PrevIfIndex = i4IfIndex;
            u4PrevDestMacIndex = u4DestMacAddrIndex;
            u2TlvsEnabled = 0;

            MEMSET (&LldpMedGetPortTLVsEnabled, 0,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
            LldpMedGetPortTLVsEnabled.pu1_OctetList = (UINT1 *) &u2TlvsEnabled;
            LldpMedGetPortTLVsEnabled.i4_Length = sizeof (UINT2);
            u2optTLV = u2TlvsSelected;
        }
    }
    /* Get next IfIndex and Destination MacAddress Index */
    while (nmhGetNextIndexFsLldpMedPortConfigTable (i4PrevIfIndex,
                                                    &i4IfIndex,
                                                    u4PrevDestMacIndex,
                                                    &u4DestMacAddrIndex)
           == SNMP_SUCCESS);
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpMedCliSetNwPolicyConfig
 *
 *     DESCRIPTION      : Configures the properties of Network-policy TLV
 *
 *     INPUT            : CliHandle      - CliContext ID
 *                        i4IfIndex      - Interface Index
 *                        u1AppType      - Type of Application
 *                        bPolicyUnKnown - Unknown Flag
 *                        bVlanType      - Tagged or Untagged value
 *                        u4VlanId       - VlanId number
 *                        u4Priority     - Priority of VLAN
 *                        u4Dscp         - Diffserv node behaviour of Application
 *                                         Type
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
LldpMedCliSetNwPolicyConfig (tCliHandle CliHandle,
                             INT4 i4IfIndex,
                             UINT1 u1AppType,
                             BOOL1 bPolicyUnKnown,
                             BOOL1 bVlanType,
                             UINT4 u4VlanId, UINT4 u4Priority, UINT4 u4Dscp)
{
    tSNMP_OCTET_STRING_TYPE LldpMedLocMediaPolicyAppType;
    UINT4               u4ErrorCode = 0;
    INT4                i4PolicyRowStatus = 0;

    MEMSET (&LldpMedLocMediaPolicyAppType, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    LldpMedLocMediaPolicyAppType.pu1_OctetList = &u1AppType;
    LldpMedLocMediaPolicyAppType.i4_Length = LLDP_MED_MAX_LEN_APP_TYPE;

    /* If there is no entry in Row Status, we will create it */
    if (nmhGetFsLldpMedLocMediaPolicyRowStatus (i4IfIndex,
                                                &LldpMedLocMediaPolicyAppType,
                                                &i4PolicyRowStatus) !=
        SNMP_SUCCESS)
    {
        if (nmhTestv2FsLldpMedLocMediaPolicyRowStatus (&u4ErrorCode,
                                                       i4IfIndex,
                                                       &LldpMedLocMediaPolicyAppType,
                                                       CREATE_AND_WAIT) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r\n%% Unable to set Network Policy Configuration\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsLldpMedLocMediaPolicyRowStatus (i4IfIndex,
                                                    &LldpMedLocMediaPolicyAppType,
                                                    CREATE_AND_WAIT) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsLldpMedLocMediaPolicyRowStatus (&u4ErrorCode,
                                                   i4IfIndex,
                                                   &LldpMedLocMediaPolicyAppType,
                                                   NOT_IN_SERVICE) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Unable to set Network Policy Configuration\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsLldpMedLocMediaPolicyRowStatus (i4IfIndex,
                                                &LldpMedLocMediaPolicyAppType,
                                                NOT_IN_SERVICE) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    /* Check whether Policy Unknown Flag is enabled or disabled */
    if (bPolicyUnKnown == LLDP_MED_UNKNOWN_FLAG)
    {
        if (nmhTestv2FsLldpMedLocMediaPolicyUnknown (&u4ErrorCode, i4IfIndex,
                                                     &LldpMedLocMediaPolicyAppType,
                                                     (INT4) bPolicyUnKnown) !=
            SNMP_SUCCESS)
        {
            CLI_SET_ERR (LLDP_MED_CLI_ERR_UNKNOWN_POL);
            return CLI_FAILURE;
        }

        if (nmhSetFsLldpMedLocMediaPolicyUnknown (i4IfIndex,
                                                  &LldpMedLocMediaPolicyAppType,
                                                  (INT4) bPolicyUnKnown) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    /* If Unknown Flag is disabled */
    else
    {
        /* Check whether VlanType is Untagged */
        if (bVlanType == LLDP_MED_VLAN_UNTAGGED)
        {
            /* Test the Policy VlanType, VlanId, Priority */
            if (nmhTestv2FsLldpMedLocMediaPolicyTagged (&u4ErrorCode,
                                                        i4IfIndex,
                                                        &LldpMedLocMediaPolicyAppType,
                                                        (INT4) bVlanType) !=
                SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to set VlanType\r\n");
                return CLI_FAILURE;
            }
            /* Set the Value for VlanType */
            if (nmhSetFsLldpMedLocMediaPolicyTagged (i4IfIndex,
                                                     &LldpMedLocMediaPolicyAppType,
                                                     (INT4) bVlanType) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        /* If VlanType is Tagged, Set VlanId, Priority */
        else
        {
            if (nmhTestv2FsLldpMedLocMediaPolicyVlanID (&u4ErrorCode,
                                                        i4IfIndex,
                                                        &LldpMedLocMediaPolicyAppType,
                                                        (INT4) u4VlanId) !=
                SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Invalid Vlan ID %d\r\n", u4VlanId);
                return CLI_FAILURE;
            }
            if (nmhTestv2FsLldpMedLocMediaPolicyPriority (&u4ErrorCode,
                                                          i4IfIndex,
                                                          &LldpMedLocMediaPolicyAppType,
                                                          (INT4) u4Priority) !=
                SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Invalid Priority %d\r\n",
                           u4Priority);
                return CLI_FAILURE;
            }

            /* Set the Value for VlanId */
            if (nmhSetFsLldpMedLocMediaPolicyVlanID (i4IfIndex,
                                                     &LldpMedLocMediaPolicyAppType,
                                                     (INT4) u4VlanId) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            /* Set the value for Priority */
            if (nmhSetFsLldpMedLocMediaPolicyPriority (i4IfIndex,
                                                       &LldpMedLocMediaPolicyAppType,
                                                       (INT4) u4Priority) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        /* Dscp is common for both Tagged and Untagged */
        if (nmhTestv2FsLldpMedLocMediaPolicyDscp (&u4ErrorCode, i4IfIndex,
                                                  &LldpMedLocMediaPolicyAppType,
                                                  (INT4) u4Dscp) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Invalid Dscp %d\r\n", u4Dscp);
            return CLI_FAILURE;
        }
        /* Set the value of Dscp */
        if (nmhSetFsLldpMedLocMediaPolicyDscp (i4IfIndex,
                                               &LldpMedLocMediaPolicyAppType,
                                               (INT4) u4Dscp) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    /* After Setting the values, Row status should be set Active */
    if (nmhTestv2FsLldpMedLocMediaPolicyRowStatus (&u4ErrorCode,
                                                   i4IfIndex,
                                                   &LldpMedLocMediaPolicyAppType,
                                                   ACTIVE) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to set Network Policy Configuration\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsLldpMedLocMediaPolicyRowStatus (i4IfIndex,
                                                &LldpMedLocMediaPolicyAppType,
                                                ACTIVE) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpMedCliDelNwPolicyConfig
 *
 *     DESCRIPTION      : Deletes the Configuration of Network-policy TLV
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex  - Interface Index
 *                        u1AppType  - Type of Application
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpMedCliDelNwPolicyConfig (tCliHandle CliHandle,
                             INT4 i4IfIndex, UINT1 u1AppType)
{
    tSNMP_OCTET_STRING_TYPE LldpMedLocMediaPolicyAppType;
    UINT4               u4ErrorCode = 0;

    MEMSET (&LldpMedLocMediaPolicyAppType, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    LldpMedLocMediaPolicyAppType.pu1_OctetList = &u1AppType;
    LldpMedLocMediaPolicyAppType.i4_Length = LLDP_MED_MAX_LEN_APP_TYPE;

    if (nmhTestv2FsLldpMedLocMediaPolicyRowStatus
        (&u4ErrorCode,
         i4IfIndex, &LldpMedLocMediaPolicyAppType, DESTROY) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to set Network Policy Configuration\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsLldpMedLocMediaPolicyRowStatus
        (i4IfIndex, &LldpMedLocMediaPolicyAppType, DESTROY) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *     FUNCTION NAME    : LldpMedCliSetLocConfig
 *
 *     DESCRIPTION      : Configures the Location Information for a
                          switch port
 *                      
 *
 *     INPUT            : CliHandle          - CliContext ID
 *                        i4IfIndex          - Interface Index
                          i4LocType          - Locatiuon subtype
 *                        pu1LocInfo         - Location Inforamtion
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/
PUBLIC INT4
LldpMedCliSetLocConfig (tCliHandle CliHandle,
                        INT4 i4IfIndex, INT4 i4LocType, UINT1 *pu1LocInfo)
{
    tSNMP_OCTET_STRING_TYPE LldpMedLocLocationInfo;
    UINT4               u4ErrorCode = 0;
    INT4                i4LocationRowStatus = 0;

    /* If there is no entry in Row Status, we will create it */
    if (nmhGetFsLldpMedLocLocationRowStatus (i4IfIndex,
                                             i4LocType,
                                             &i4LocationRowStatus) ==
        SNMP_SUCCESS)
    {
        if (i4LocType == LLDP_MED_COORDINATE_LOC)
        {

            CliPrintf (CliHandle,
                       "\r\n%% Coordinate Location info is already configured\r\n");
        }
        else if (i4LocType == LLDP_MED_CIVIC_LOC)
        {

            CliPrintf (CliHandle,
                       "\r\n%% Civic Location info is already configured\r\n");
        }
        return CLI_FAILURE;
    }

    if (nmhTestv2FsLldpMedLocLocationRowStatus (&u4ErrorCode,
                                                i4IfIndex,
                                                i4LocType,
                                                CREATE_AND_WAIT) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Unable to set Location Configuration\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsLldpMedLocLocationRowStatus (i4IfIndex,
                                             i4LocType,
                                             CREATE_AND_WAIT) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsLldpMedLocLocationRowStatus (&u4ErrorCode,
                                                i4IfIndex,
                                                i4LocType,
                                                NOT_IN_SERVICE) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Unable to set Location Configuration\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsLldpMedLocLocationRowStatus (i4IfIndex,
                                             i4LocType,
                                             NOT_IN_SERVICE) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    MEMSET (&LldpMedLocLocationInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    LldpMedLocLocationInfo.pu1_OctetList = pu1LocInfo;
    if (i4LocType == LLDP_MED_COORDINATE_LOC)
    {
        LldpMedLocLocationInfo.i4_Length =
            (INT4) LLDP_MED_MAX_COORDINATE_LOC_LENGTH;
    }
    else if (i4LocType == LLDP_MED_CIVIC_LOC)
    {
        /* Setting civic id length = LCI length + 1 */
        LldpMedLocLocationInfo.i4_Length = (*pu1LocInfo + 1);
    }
    else
    {

        LldpMedLocLocationInfo.i4_Length = (INT4) STRLEN (pu1LocInfo);
    }
    if (nmhTestv2LldpXMedLocLocationInfo (&u4ErrorCode,
                                          i4IfIndex,
                                          i4LocType,
                                          &LldpMedLocLocationInfo) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%% Unable to set Location Information\r\n");
        if (nmhTestv2FsLldpMedLocLocationRowStatus
            (&u4ErrorCode, i4IfIndex, i4LocType, DESTROY) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsLldpMedLocLocationRowStatus
            (i4IfIndex, i4LocType, DESTROY) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        return CLI_FAILURE;
    }

    if (nmhSetLldpXMedLocLocationInfo (i4IfIndex,
                                       i4LocType,
                                       &LldpMedLocLocationInfo) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    /* After Setting the values, Row status should be set Active */
    if (nmhTestv2FsLldpMedLocLocationRowStatus (&u4ErrorCode,
                                                i4IfIndex,
                                                i4LocType,
                                                ACTIVE) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set Location Configuration\r\n");
        if (nmhTestv2FsLldpMedLocLocationRowStatus
            (&u4ErrorCode, i4IfIndex, i4LocType, DESTROY) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set Location Configuration\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsLldpMedLocLocationRowStatus
            (i4IfIndex, i4LocType, DESTROY) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        return CLI_FAILURE;
    }
    if (nmhSetFsLldpMedLocLocationRowStatus (i4IfIndex,
                                             i4LocType, ACTIVE) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 *     FUNCTION NAME    : LldpMedCliSetElinConfig
 *
 *     DESCRIPTION      : Configures the Elin  Location Information for a
 *                        switch port
 *
 *
 *     INPUT            : CliHandle          - CliContext ID
 *                        i4IfIndex          - Interface Index
 *                        pu1LocInfo         - Location Inforamtion
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/
PUBLIC INT4
LldpMedCliSetElinConfig (tCliHandle CliHandle,
                         INT4 i4IfIndex, UINT1 *pu1LocInfo)
{
    tSNMP_OCTET_STRING_TYPE LldpMedLocLocationInfo;
    UINT4               u4ErrorCode = 0;
    INT4                i4LocationRowStatus = 0;
    INT4                i4LocType = 0;

    i4LocType = LLDP_MED_ELIN_LOC;
    /* Location info length check for Elin */
    if ((STRLEN (pu1LocInfo) < LLDP_MED_MIN_ELIN_LOC_LENGTH) ||
        (STRLEN (pu1LocInfo) > LLDP_MED_MAX_ELIN_LOC_LENGTH))
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "LldpMedCliSetLocConfig : Elin location "
                  "length not within supported range!!\r\n");
        CLI_SET_ERR (LLDP_MED_CLI_INVALID_ELIN_LEN);
        return CLI_FAILURE;
    }
    /* If there is no entry in Row Status, we will create it */
    if (nmhGetFsLldpMedLocLocationRowStatus (i4IfIndex,
                                             i4LocType,
                                             &i4LocationRowStatus) ==
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Elin Location info is already configured\r\n");
        return CLI_FAILURE;
    }
    if (nmhTestv2FsLldpMedLocLocationRowStatus (&u4ErrorCode,
                                                i4IfIndex,
                                                i4LocType,
                                                CREATE_AND_WAIT) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Unable to set Elin location configuration\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsLldpMedLocLocationRowStatus (i4IfIndex,
                                             i4LocType,
                                             CREATE_AND_WAIT) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsLldpMedLocLocationRowStatus (&u4ErrorCode,
                                                i4IfIndex,
                                                i4LocType,
                                                NOT_IN_SERVICE) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Unable to set Elin location configuration\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsLldpMedLocLocationRowStatus (i4IfIndex,
                                             i4LocType,
                                             NOT_IN_SERVICE) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    MEMSET (&LldpMedLocLocationInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    LldpMedLocLocationInfo.pu1_OctetList = pu1LocInfo;
    LldpMedLocLocationInfo.i4_Length = (INT4) STRLEN (pu1LocInfo);
    if (nmhTestv2LldpXMedLocLocationInfo (&u4ErrorCode,
                                          i4IfIndex,
                                          i4LocType,
                                          &LldpMedLocLocationInfo) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Unable to set Elin  location information\r\n");
        if (nmhTestv2FsLldpMedLocLocationRowStatus
            (&u4ErrorCode, i4IfIndex, i4LocType, DESTROY) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsLldpMedLocLocationRowStatus
            (i4IfIndex, i4LocType, DESTROY) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        return CLI_FAILURE;
    }

    if (nmhSetLldpXMedLocLocationInfo (i4IfIndex,
                                       i4LocType,
                                       &LldpMedLocLocationInfo) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    /* After Setting the values, Row status should be set Active */
    if (nmhTestv2FsLldpMedLocLocationRowStatus (&u4ErrorCode,
                                                i4IfIndex,
                                                i4LocType,
                                                ACTIVE) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to set Elin location configuration\r\n");
        if (nmhTestv2FsLldpMedLocLocationRowStatus
            (&u4ErrorCode, i4IfIndex, i4LocType, DESTROY) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set Elin location configuration\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsLldpMedLocLocationRowStatus
            (i4IfIndex, i4LocType, DESTROY) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        return CLI_FAILURE;
    }
    if (nmhSetFsLldpMedLocLocationRowStatus (i4IfIndex,
                                             i4LocType, ACTIVE) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 *     FUNCTION NAME    : LldpMedCliSetCoordinateLocConfig
 *
 *     DESCRIPTION      : Configures the Coordinate Location Information for a
 *                        switch port
 *
 *
 *     INPUT            : CliHandle       - CliContext ID
 *                        i4IfIndex       - Interface Index
 *                        u1latitude      - Latitude
 *                        u1latres        - Latitude-resolution
 *                        u1longitude     - Longitude
 *                        u1longres       - Longitude-resolution
 *                        u1alttype       - Altitude-type
 *                        u1altitude      - Altitude
 *                        u1altres        - Altitude-resolution
 *                        u1datum         - Datum
 *
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/

PUBLIC INT4
LldpMedCliSetCoordinateLocConfig (tCliHandle CliHandle,
                                  INT4 i4IfIndex,
                                  UINT1 u1latres,
                                  UINT1 *u1latitude,
                                  UINT1 u1longres,
                                  UINT1 *u1longitude,
                                  UINT1 u1alttype,
                                  UINT1 *u1altitude,
                                  UINT1 u1altres, UINT1 u1datum)
{
    tSNMP_OCTET_STRING_TYPE LldpMedLocLocationInfo;
    UINT1              *pu1TempLat = NULL;
    UINT1              *pu1TempLong = NULL;
    UINT1              *pu1TempAlt = NULL;
    UINT1               au1lat[LLDP_MED_MAX_COORDINATE_LOC_LENGTH];
    UINT4               u4ErrorCode = 0;
    INT4                i4LocationRowStatus = 0;
    INT4                i4LocType = 0;

    MEMSET (&au1lat, 0, LLDP_MED_MAX_COORDINATE_LOC_LENGTH);
    i4LocType = LLDP_MED_COORDINATE_LOC;
    pu1TempLat = u1latitude;
    pu1TempLong = u1longitude;
    pu1TempAlt = u1altitude;

    if (*pu1TempLat == '-')
    {
        pu1TempLat++;
    }
    if (*pu1TempLong == '-')
    {
        pu1TempLong++;
    }
    if (*pu1TempAlt == '-')
    {
        pu1TempAlt++;
    }

    while (*pu1TempLat)
    {
        if (*pu1TempLat == '.')
        {
            pu1TempLat++;
            continue;
        }
        if (!ISDIGIT (*pu1TempLat++))
        {
            CliPrintf (CliHandle,
                       "\r\n%% Please enter numeric digits for latitude\r\n");
            return CLI_FAILURE;
        }
    }
    while (*pu1TempLong)
    {
        if (*pu1TempLong == '.')
        {
            pu1TempLong++;
            continue;
        }
        if (!ISDIGIT (*pu1TempLong++))
        {
            CliPrintf (CliHandle,
                       "\r\n%% Please enter numeric digits for longitude\r\n");
            return CLI_FAILURE;
        }
    }
    while (*pu1TempAlt)
    {
        if (*pu1TempAlt == '.')
        {
            pu1TempAlt++;
            continue;
        }
        if (!ISDIGIT (*pu1TempAlt++))
        {
            CliPrintf (CliHandle,
                       "\r\n%% Please enter numeric digits for altitude\r\n");
            return CLI_FAILURE;
        }
    }
    if ((atoi ((CONST CHR1 *) u1latitude) >
         LLDP_MED_MAX_LATITUDE) | (atoi ((CONST CHR1 *) u1latitude) <
                                   LLDP_MED_MIN_LATITUDE))
    {
        CliPrintf (CliHandle, "\r\n%% Latitude is not with in range\r\n");
        return CLI_FAILURE;
    }
    if ((atoi ((CONST CHR1 *) u1longitude) >
         LLDP_MED_MAX_LONGITUDE) | (atoi ((CONST CHR1 *) u1longitude) <
                                    LLDP_MED_MIN_LONGITUDE))
    {
        CliPrintf (CliHandle, "\r\n%% Longitude is not with in range\r\n");
        return CLI_FAILURE;
    }
    if ((atoi ((CONST CHR1 *) u1altitude) >
         LLDP_MED_MAX_ALTITUDE) | (atoi ((CONST CHR1 *) u1altitude) <
                                   LLDP_MED_MIN_ALTITUDE))
    {
        CliPrintf (CliHandle, "\r\n%% Altitude is not with in range\r\n");
        return CLI_FAILURE;
    }

    /* If there is no entry in Row Status, we will create it */
    if (nmhGetFsLldpMedLocLocationRowStatus (i4IfIndex,
                                             i4LocType,
                                             &i4LocationRowStatus) ==
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Coordinate Location info is already configured\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsLldpMedLocLocationRowStatus (&u4ErrorCode,
                                                i4IfIndex,
                                                i4LocType,
                                                CREATE_AND_WAIT) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Unable to set Location Configuration\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsLldpMedLocLocationRowStatus (i4IfIndex,
                                             i4LocType,
                                             CREATE_AND_WAIT) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsLldpMedLocLocationRowStatus (&u4ErrorCode,
                                                i4IfIndex,
                                                i4LocType,
                                                NOT_IN_SERVICE) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Unable to set Location Configuration\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsLldpMedLocLocationRowStatus (i4IfIndex,
                                             i4LocType,
                                             NOT_IN_SERVICE) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    /*Construct Coordinate Location data format value */
    LldpMedUtlCoordinateLocEncode (u1latitude, u1latres, u1longitude,
                                   u1longres, u1alttype, u1altitude,
                                   u1altres, u1datum, au1lat);

    MEMSET (&LldpMedLocLocationInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    LldpMedLocLocationInfo.pu1_OctetList = &au1lat[0];
    LldpMedLocLocationInfo.i4_Length =
        (INT4) LLDP_MED_MAX_COORDINATE_LOC_LENGTH;
    if (nmhTestv2LldpXMedLocLocationInfo
        (&u4ErrorCode, i4IfIndex, i4LocType,
         &LldpMedLocLocationInfo) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%% Unable to set Location Information\r\n");
        if (nmhTestv2FsLldpMedLocLocationRowStatus
            (&u4ErrorCode, i4IfIndex, i4LocType, DESTROY) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsLldpMedLocLocationRowStatus
            (i4IfIndex, i4LocType, DESTROY) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        return CLI_FAILURE;
    }

    if (nmhSetLldpXMedLocLocationInfo (i4IfIndex,
                                       i4LocType,
                                       &LldpMedLocLocationInfo) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    /* After Setting the values, Row status should be set Active */
    if (nmhTestv2FsLldpMedLocLocationRowStatus (&u4ErrorCode,
                                                i4IfIndex,
                                                i4LocType,
                                                ACTIVE) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set Location Configuration\r\n");
        if (nmhTestv2FsLldpMedLocLocationRowStatus
            (&u4ErrorCode, i4IfIndex, i4LocType, DESTROY) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set Location Configuration\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsLldpMedLocLocationRowStatus
            (i4IfIndex, i4LocType, DESTROY) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        return CLI_FAILURE;
    }
    if (nmhSetFsLldpMedLocLocationRowStatus (i4IfIndex,
                                             i4LocType, ACTIVE) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 *     FUNCTION NAME    : LldpMedCliSetCountryCodeConfig
 *
 *     DESCRIPTION      : Configures the Civic country code Information for a
 *                        Switch port and changes the prompt to config-civic-loc
 *
 *
 *     INPUT            : CliHandle          - CliContext ID
 *                        i4IfIndex          - Interface Index
 *                        pu1ContryCode      - Country code
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/

PUBLIC INT4
LldpMedCliSetCountryCodeConfig (tCliHandle CliHandle,
                                INT4 i4IfIndex, UINT1 *pu1ContryCode)
{
    tSNMP_OCTET_STRING_TYPE LocationInfo;
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    INT4                i4LocType = 0;
    UINT4               u4ErrorCode = 0;
    UINT1               au1LocationId[LLDP_MED_MAX_LOC_LENGTH];

    MEMSET (&au1LocationId, 0, LLDP_MED_MAX_LOC_LENGTH);
    MEMSET (&LocationInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    i4LocType = LLDP_MED_CIVIC_LOC;
    LocationInfo.pu1_OctetList = au1LocationId;

    if (nmhGetLldpXMedLocLocationInfo (i4IfIndex,
                                       i4LocType,
                                       &LocationInfo) == SNMP_SUCCESS)
    {
        LocationInfo.pu1_OctetList += LLDP_MED_COUNTRY_CODE_LENGTH;
        if (MEMCMP (LocationInfo.pu1_OctetList,
                    pu1ContryCode, LLDP_MED_COUNTRY_CODE_LENGTH) != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n%% Civic Location info is already configured\r\n");
            return CLI_SUCCESS;
        }
    }

    else
    {
        if (nmhTestv2FsLldpMedLocLocationRowStatus (&u4ErrorCode,
                                                    i4IfIndex,
                                                    i4LocType,
                                                    CREATE_AND_WAIT) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r\n%% Unable to set Location Configuration\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsLldpMedLocLocationRowStatus (i4IfIndex,
                                                 i4LocType,
                                                 CREATE_AND_WAIT) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhTestv2FsLldpMedLocLocationRowStatus (&u4ErrorCode,
                                                    i4IfIndex,
                                                    i4LocType,
                                                    NOT_IN_SERVICE) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r\n%% Unable to set Location Configuration\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsLldpMedLocLocationRowStatus (i4IfIndex,
                                                 i4LocType,
                                                 NOT_IN_SERVICE) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        /* Setting LCI length field */
        au1LocationId[0] = (UINT1) ((LLDP_MED_COUNTRY_CODE_LENGTH +
                                     LLDP_MED_CIVIC_WHAT_LENGTH));
        /* Setting What field */
        au1LocationId[1] = (UINT1) LLDP_MED_CIVIC_CLIENT;
        MEMCPY (&au1LocationId[2], pu1ContryCode, LLDP_MED_COUNTRY_CODE_LENGTH);
        LocationInfo.i4_Length = (INT4) STRLEN (au1LocationId);
        if (nmhSetLldpXMedLocLocationInfo (i4IfIndex,
                                           i4LocType,
                                           &LocationInfo) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    SPRINTF ((CHR1 *) au1Cmd, "%s", LLDP_CIVIC_MODE);
    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "%% Unable to enter into civic-loc "
                   "configuration mode\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *     FUNCTION NAME    : LldpMedCliSetCaTypeConfig
 *
 *     DESCRIPTION      : Configures the Civic CA type Information for a
 *                        Switch port
 *
 *
 *     INPUT            : CliHandle          - CliContext ID
 *                        i4IfIndex          - Interface Index
 *                        u1CaType           - CA type
 *                        pu1CaValue         - CA value
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/

PUBLIC INT4
LldpMedCliSetCaTypeConfig (tCliHandle CliHandle,
                           INT4 i4IfIndex, UINT1 u1CaType, UINT1 *pu1CaValue)
{
    tSNMP_OCTET_STRING_TYPE LocationInfo;
    UINT1               au1LocationId[LLDP_MED_MAX_LOC_LENGTH];
    INT4                i4LocationRowStatus = 0;
    INT4                i4LocType = 0;
    UINT1               u1LocOffset = 0;
    UINT1               u1Index = 0;

    MEMSET (&au1LocationId, 0, LLDP_MED_MAX_LOC_LENGTH);
    MEMSET (&LocationInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    i4LocType = LLDP_MED_CIVIC_LOC;
    LocationInfo.pu1_OctetList = au1LocationId;
    if (pu1CaValue != NULL)
    {
        /* check CA Value rnage */
        if (STRLEN (pu1CaValue) > LLDP_MED_MAX_CA_TYPE_LENGTH)
        {
            CliPrintf (CliHandle, "%% CA value exceeding the Range "
                       "Please Enter with in Range\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhGetFsLldpMedLocLocationRowStatus (i4IfIndex,
                                             i4LocType,
                                             &i4LocationRowStatus) ==
        SNMP_SUCCESS)
    {
        if (i4LocationRowStatus == ACTIVE)
        {
            CliPrintf (CliHandle, "%% Civic Location information is active, "
                       "you are not allowed to configure.\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhGetLldpXMedLocLocationInfo (i4IfIndex,
                                       i4LocType,
                                       &LocationInfo) == SNMP_SUCCESS)
    {
        u1LocOffset = (UINT1) LocationInfo.i4_Length;
        /* check for duplicate CA types */
        for (u1Index = 4; u1Index < au1LocationId[0];)
        {
            if (au1LocationId[u1Index++] == u1CaType)
            {
                CliPrintf (CliHandle, "%% CA Type is already configured.\r\n");
                return CLI_FAILURE;
            }
            /* Incrementing index to next CA type */
            u1Index = (UINT1) (u1Index + au1LocationId[u1Index] + 1);
        }
        if (pu1CaValue != NULL)
        {
            /* Check Civic Location Info range */
            if ((u1LocOffset + STRLEN (pu1CaValue) + 2) >
                LLDP_MED_MAX_LOC_LENGTH)
            {
                CliPrintf (CliHandle,
                           "%% Civic Location Information exceeding the Range "
                           "\r\n");
                return CLI_FAILURE;
            }
            au1LocationId[u1LocOffset++] = u1CaType;
            au1LocationId[u1LocOffset++] = (UINT1) STRLEN (pu1CaValue);
            MEMCPY (&au1LocationId[u1LocOffset], pu1CaValue,
                    STRLEN (pu1CaValue));
            /* Setting LCI length field */
            au1LocationId[0] =
                (UINT1) (LocationInfo.i4_Length + STRLEN (pu1CaValue) + 1);
            LocationInfo.i4_Length =
                (INT4) (LocationInfo.i4_Length + STRLEN (pu1CaValue) + 2);
        }
        else
        {
            /* Check Civic Location Info range */
            if ((u1LocOffset + 2) > LLDP_MED_MAX_LOC_LENGTH)
            {
                CliPrintf (CliHandle,
                           "%% Civic Location Information exceeding the Range "
                           "\r\n");
                return CLI_FAILURE;
            }
            au1LocationId[u1LocOffset++] = u1CaType;
            au1LocationId[u1LocOffset] = 0;
            /* Setting LCI length field */
            au1LocationId[0] = (UINT1) (LocationInfo.i4_Length + 1);
            LocationInfo.i4_Length = (INT4) (LocationInfo.i4_Length + 2);

        }

        if (nmhSetLldpXMedLocLocationInfo (i4IfIndex,
                                           i4LocType,
                                           &LocationInfo) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *     FUNCTION NAME    : LldpMedCliSetCivicActive
 *
 *     DESCRIPTION      : To set Civic Location Information row status active
 *                        for a switch port
 *
 *
 *     INPUT            : CliHandle          - CliContext ID
 *                        i4IfIndex          - Interface Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/

PUBLIC INT4
LldpMedCliSetCivicActive (tCliHandle CliHandle, INT4 i4IfIndex)
{
    tSNMP_OCTET_STRING_TYPE LocationInfo;
    UINT1               au1LocationId[LLDP_MED_MAX_LOC_LENGTH];
    UINT4               u4ErrorCode = 0;
    INT4                i4LocType = 0;
    INT4                i4LocationRowStatus = 0;

    MEMSET (&LocationInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    i4LocType = LLDP_MED_CIVIC_LOC;
    LocationInfo.pu1_OctetList = au1LocationId;

    if (nmhGetLldpXMedLocLocationInfo (i4IfIndex,
                                       i4LocType,
                                       &LocationInfo) == SNMP_SUCCESS)
    {
        if (au1LocationId[0] < 5)
        {
            CliPrintf (CliHandle,
                       "%%Unable to set Civic Location Information active, "
                       "Please Configure atleast one CA Type.\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhGetFsLldpMedLocLocationRowStatus (i4IfIndex,
                                             i4LocType,
                                             &i4LocationRowStatus) ==
        SNMP_SUCCESS)
    {
        if (i4LocationRowStatus == ACTIVE)
        {
            CliPrintf (CliHandle,
                       "%% Civic Location information is already active."
                       "\r\n");
            return CLI_SUCCESS;
        }
    }

    if (nmhTestv2FsLldpMedLocLocationRowStatus (&u4ErrorCode,
                                                i4IfIndex,
                                                i4LocType,
                                                ACTIVE) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set Location Configuration\r\n");
        if (nmhTestv2FsLldpMedLocLocationRowStatus
            (&u4ErrorCode, i4IfIndex, i4LocType, DESTROY) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set Location Configuration\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsLldpMedLocLocationRowStatus
            (i4IfIndex, i4LocType, DESTROY) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        return CLI_FAILURE;
    }
    if (nmhSetFsLldpMedLocLocationRowStatus (i4IfIndex, i4LocType,
                                             ACTIVE) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/****************************************************************************
 *     FUNCTION NAME    : LldpMedCliDelLocConfig
 *
 *     DESCRIPTION      : Deletes the Location Information 
 *                       
 *
 *
 *     INPUT            : CliHandle          - CliContext ID
 *                        i4IfIndex          - Interface Index
 *                        pu1LocType         - Location Subtype 
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/
PUBLIC INT4
LldpMedCliDelLocConfig (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4LocType)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsLldpMedLocLocationRowStatus
        (&u4ErrorCode, i4IfIndex, i4LocType, DESTROY) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Location Information is not configured\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsLldpMedLocLocationRowStatus
        (i4IfIndex, i4LocType, DESTROY) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliClearCounters
 *
 *     DESCRIPTION      : Clears LLDP transmit and receive statistics
 *
 *     INPUT            : CliHandle  - CliContext ID
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliClearCounters (tCliHandle CliHandle)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsLldpClearStats (&u4ErrorCode,
                                   LLDP_SET_CLEAR_COUNTER) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set clear counters\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsLldpClearStats (LLDP_SET_CLEAR_COUNTER) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

 /****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliClearTable
 *
 *     DESCRIPTION      : Clears LLDP neighbors information
 *
 *     INPUT            : CliHandle  - CliContext ID
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliClearTable (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);

    /* Clears LLDP neighbors info table */
    LldpRxUtlDrainRemSysTables ();
    return CLI_SUCCESS;
}

 /*****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowDebugging
 *
 *     DESCRIPTION      : Shows LLDP debug level
 *
 *     INPUT            : CliHandle  - CliContext ID
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DbgLevel = 0;
    INT4                i4Status = 0;

    nmhGetFsLldpSystemControl (&i4Status);

    if (i4Status != LLDP_START)
    {
        return CLI_FAILURE;
    }
    nmhGetFsLldpTraceOption (&i4DbgLevel);

    if (i4DbgLevel == 0)
    {
        return CLI_SUCCESS;
    }
    CliPrintf (CliHandle, "\r\nLLDP :\n");
    if (i4DbgLevel == LLDP_ALL_TRC)
    {
        CliPrintf (CliHandle, "  LLDP all debugging is on\r\n");
        return CLI_SUCCESS;
    }
    if ((i4DbgLevel & INIT_SHUT_TRC) != 0)
    {
        CliPrintf (CliHandle, "  LLDP init and shutdown debugging is on\r\n");
    }
    if ((i4DbgLevel & MGMT_TRC) != 0)
    {
        CliPrintf (CliHandle, "  LLDP management debugging is on\r\n");
    }
    if ((i4DbgLevel & DATA_PATH_TRC) != 0)
    {
        CliPrintf (CliHandle, "  LLDP Data path debugging is on\r\n");
    }
    if ((i4DbgLevel & CONTROL_PLANE_TRC) != 0)
    {
        CliPrintf (CliHandle, "  LLDP control plane debugging is on\r\n");
    }
    if ((i4DbgLevel & DUMP_TRC) != 0)
    {
        CliPrintf (CliHandle, "  LLDP packet dump debugging is on\r\n");
    }
    if ((i4DbgLevel & OS_RESOURCE_TRC) != 0)
    {
        CliPrintf (CliHandle, "  LLDP resources debugging is on\r\n");
    }
    if ((i4DbgLevel & ALL_FAILURE_TRC) != 0)
    {
        CliPrintf (CliHandle, "  LLDP error debugging is on\r\n");
    }
    if ((i4DbgLevel & BUFFER_TRC) != 0)
    {
        CliPrintf (CliHandle, "  LLDP buffer debugging is on\r\n");
    }
    if ((i4DbgLevel & LLDP_CRITICAL_TRC) != 0)
    {
        CliPrintf (CliHandle, "  LLDP critical debugging is on\r\n");
    }
    if ((i4DbgLevel & LLDP_ALL_TLV_TRC) == LLDP_ALL_TLV_TRC)
    {
        CliPrintf (CliHandle, "  LLDP All Tlv debugging is on\r\n");
    }
    else
    {
        if ((i4DbgLevel & LLDP_MANDATORY_TLV_TRC) != 0)
        {
            CliPrintf (CliHandle, "  LLDP Mandatory TLVs debugging is on\r\n");
        }
        if ((i4DbgLevel & LLDP_PORT_DESC_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "  LLDP Port Description debugging is on\r\n");
        }
        if ((i4DbgLevel & LLDP_SYS_NAME_TRC) != 0)
        {
            CliPrintf (CliHandle, "  LLDP System Name Tlv debugging is on\r\n");
        }
        if ((i4DbgLevel & LLDP_SYS_DESC_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "  LLDP system description Tlv debugging is on\r\n");
        }
        if ((i4DbgLevel & LLDP_SYS_CAPAB_TRC) != 0)
        {
            CliPrintf (CliHandle, "  LLDP System Capability Tlv debugging "
                       "is on\r\n");
        }
        if ((i4DbgLevel & LLDP_MAN_ADDR_TRC) != 0)
        {
            CliPrintf (CliHandle, "  LLDP Management Address debugging "
                       "is on\r\n");
        }
        if ((i4DbgLevel & LLDP_PORT_VLAN_TRC) != 0)
        {
            CliPrintf (CliHandle, "  LLDP Port Vlan debugging is on\r\n");
        }
        if ((i4DbgLevel & LLDP_PPVLAN_TRC) != 0)
        {
            CliPrintf (CliHandle, "  LLDP ppvlan debugging is on\r\n");
        }
        if ((i4DbgLevel & LLDP_VLAN_NAME_TRC) != 0)
        {
            CliPrintf (CliHandle, "  LLDP Vlan Name tlv debugging is on\r\n");
        }
        if ((i4DbgLevel & LLDP_PROTO_ID_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "  LLDP Proto Id Tlv is currently Not Supported\r\n");
        }
        if ((i4DbgLevel & LLDP_MAC_PHY_TRC) != 0)
        {
            CliPrintf (CliHandle, "  LLDP MAC phy tlv debugging is on\r\n");
        }
        if ((i4DbgLevel & LLDP_PWR_MDI_TRC) != 0)
        {
            CliPrintf (CliHandle, "  LLDP Power MDI Trc debugging is on\r\n");
        }
        if ((i4DbgLevel & LLDP_LAGG_TRC) != 0)
        {
            CliPrintf (CliHandle, "  LLDP LAGG Trc debugging is on\r\n");
        }
        if ((i4DbgLevel & LLDP_MAX_FRAME_TRC) != 0)
        {
            CliPrintf (CliHandle, "  LLDP Max Frame Trc debugging is on\r\n");
        }
        if ((i4DbgLevel & LLDP_VID_DIGEST_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "  LLDP VID Digest TLV TRC debugging is on\r\n");
        }
        if ((i4DbgLevel & LLDP_MGMT_VID_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "  LLDP Management VID TLV TRC debugging is on\r\n");
        }
        if ((i4DbgLevel & LLDP_MED_CAPAB_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "  LLDP MED Capability TLV TRC debugging is on\r\n");
        }
        if ((i4DbgLevel & LLDP_MED_NW_POL_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "  LLDP MED Network Policy TLV TRC debugging is on\r\n");
        }
        if ((i4DbgLevel & LLDP_MED_INV_MGMT_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "  LLDP MED Inventory TLV TRC debugging is on\r\n");
        }
        if ((i4DbgLevel & LLDP_MED_LOC_ID_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "  LLDP MED Location ID TLV TRC debugging is on\r\n");
        }
        if ((i4DbgLevel & LLDP_MED_EX_POW_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "  LLDP MED Extended Power-Via-MDI TLV TRC debugging is on\r\n");
        }

    }
    if ((i4DbgLevel & LLDP_RED_TRC) != 0)
    {
        CliPrintf (CliHandle, "  LLDP redundancy debugging is on\r\n");
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliUpdTraceInput
 *
 *     DESCRIPTION      : This function updates(sets/resets) the trace input
 *
 *     INPUT            : CliHandle       - CliContext ID
 *                        pu1TraceInput   - Pointer to trace input string
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/

PUBLIC INT4
LldpCliUpdTraceInput (tCliHandle CliHandle, UINT1 *pu1TraceInput)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE TraceInput;

    MEMSET (&TraceInput, 0, sizeof (TraceInput));
    TraceInput.pu1_OctetList = pu1TraceInput;
    TraceInput.i4_Length = (INT4) STRLEN (pu1TraceInput);

    if (nmhTestv2FsLldpTraceInput (&u4ErrorCode, &TraceInput) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsLldpTraceInput (&TraceInput) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowGlobal
 *
 *     DESCRIPTION      : This function will display the LLDP global
 *                        information
 *
 *     INPUT            : CliHandle - CliContext ID
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/

PUBLIC INT4
LldpCliShowGlobal (tCliHandle CliHandle)
{
    UINT1               au1ChassisId[LLDP_MAX_LEN_CHASSISID + 1];
    UINT1               au1MgmtAddr[IPVX_IPV6_ADDR_LEN] = { 0 };
    UINT1               au1MgmtAddr6[IPVX_IPV6_ADDR_LEN] = { 0 };
    UINT1               au1DispChassisId[LLDP_MAX_LEN_CHASSISID + 1];
    CHR1               *pc1DispChassIdNwAddr;
    CHR1               *pu1Ipv4MgmtString = NULL;
    UINT4               u4ChassIdNwAddr = 0;
    INT4                i4TxIntrvl = 0;
    INT4                i4HoldMulpr = 0;
    INT4                i4ReinitDelay = 0;
    INT4                i4TxDelay = 0;
    INT4                i4NotifyIntrvl = 0;
    INT4                i4ModStat = 0;
    INT4                i4ChassisIdSubType = 0;
    INT4                i4Version = 0;
    UINT4               u4CreditMax = 0;
    UINT4               u4MessageFastTx = 0;
    UINT4               u4TxFastInit = 0;
    UINT4               u4MgmtIpAddr = 0;
    UINT4               u4MedFastStart = 0;
    tSNMP_OCTET_STRING_TYPE ChassisId;
    tSNMP_OCTET_STRING_TYPE Ipv4MgmtAddr;
    tSNMP_OCTET_STRING_TYPE Ipv6MgmtAddr;

    MEMSET (&ChassisId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&Ipv4MgmtAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&Ipv6MgmtAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ChassisId, 0, LLDP_MAX_LEN_CHASSISID + 1);

    ChassisId.pu1_OctetList = (UINT1 *) &au1ChassisId[0];
    Ipv4MgmtAddr.pu1_OctetList = (UINT1 *) &au1MgmtAddr[0];
    Ipv6MgmtAddr.pu1_OctetList = (UINT1 *) &au1MgmtAddr6[0];

    nmhGetFsLldpModuleStatus (&i4ModStat);

    if (i4ModStat == LLDP_DISABLED)
    {
        CliPrintf (CliHandle, "\r\nLLDP is disabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nLLDP is enabled\r\n");
    }
    nmhGetFslldpv2Version (&i4Version);
    if (i4Version == LLDP_VERSION_09)
    {
        CliPrintf (CliHandle, "LLDP Version                        : v2 \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "LLDP Version                        : v1 \r\n");
    }

    nmhGetLldpV2MessageTxInterval ((UINT4 *) &i4TxIntrvl);
    nmhGetLldpV2MessageTxHoldMultiplier ((UINT4 *) &i4HoldMulpr);
    nmhGetLldpV2ReinitDelay ((UINT4 *) &i4ReinitDelay);
    nmhGetLldpTxDelay (&i4TxDelay);
    nmhGetLldpV2NotificationInterval ((UINT4 *) &i4NotifyIntrvl);

    CliPrintf (CliHandle, "Transmit Interval                   : %ld\r\n",
               i4TxIntrvl);
    CliPrintf (CliHandle, "Holdtime Multiplier                 : %ld\r\n",
               i4HoldMulpr);
    CliPrintf (CliHandle, "Reinitialization Delay              : %ld\r\n",
               i4ReinitDelay);
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
    {
        nmhGetLldpTxDelay (&i4TxDelay);
        CliPrintf (CliHandle, "Tx Delay                            : %ld\r\n",
                   i4TxDelay);
    }
    CliPrintf (CliHandle, "Notification Interval               : %ld\r\n",
               i4NotifyIntrvl);

    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_09)
    {
        nmhGetLldpV2TxCreditMax (&u4CreditMax);
        nmhGetLldpV2MessageFastTx (&u4MessageFastTx);
        nmhGetLldpV2TxFastInit (&u4TxFastInit);
        nmhGetLldpXMedFastStartRepeatCount (&u4MedFastStart);
        CliPrintf (CliHandle, "TxCreditMax                         : %ld\r\n",
                   u4CreditMax);
        CliPrintf (CliHandle, "MessageFastTx                       : %ld\r\n",
                   u4MessageFastTx);
        CliPrintf (CliHandle, "TxFastInit                          : %ld\r\n",
                   u4TxFastInit);
        CliPrintf (CliHandle, "MedFastStartRepeatCount             : %ld\r\n",
                   u4MedFastStart);
    }

    nmhGetFsLldpLocChassisIdSubtype (&i4ChassisIdSubType);
    CliPrintf (CliHandle, "Chassis Id SubType                  : ");
    LldpCliShowChassisIdSubType (CliHandle, i4ChassisIdSubType);

    nmhGetLldpV2LocChassisId (&ChassisId);
    if (i4ChassisIdSubType == LLDP_CHASS_ID_SUB_MAC_ADDR)
    {
        MEMSET (&au1DispChassisId, 0, sizeof (au1DispChassisId));

        /* Convert octet values into strings of octets seperated by colon */
        CLI_CONVERT_MAC_TO_DOT_STR (ChassisId.pu1_OctetList,
                                    &au1DispChassisId[0]);
        CliPrintf (CliHandle, "Chassis Id                          : %s\r\n",
                   au1DispChassisId);
    }
    else if (i4ChassisIdSubType == LLDP_CHASS_ID_SUB_NW_ADDR)
    {
        MEMSET (au1DispChassisId, 0, sizeof (au1DispChassisId));
        /* To skip the IANA Address Family Numbers */
        ChassisId.pu1_OctetList++;
        PTR_FETCH4 (u4ChassIdNwAddr, ChassisId.pu1_OctetList);
        pc1DispChassIdNwAddr = (CHR1 *) & au1DispChassisId[0];
        /* Convert octet values into strings of octets seperated by dot */
        CLI_CONVERT_IPADDR_TO_STR (pc1DispChassIdNwAddr, u4ChassIdNwAddr);
        CliPrintf (CliHandle, "Chassis Id                          : %s\r\n",
                   pc1DispChassIdNwAddr);
    }
    else
    {
        CliPrintf (CliHandle, "Chassis Id                          : %s\r\n",
                   ChassisId.pu1_OctetList);
    }
    nmhGetFsLldpTagStatus (&i4ModStat);
    if (i4ModStat == LLDP_TAG_DISABLE)
    {
        CliPrintf (CliHandle, "LLDP Tag Status                     :"
                   " disabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "LLDP Tag Status                     :"
                   " enabled\r\n");
    }
    nmhGetFsLldpConfiguredMgmtIpv4Address (&Ipv4MgmtAddr);
    MEMCPY (&u4MgmtIpAddr, Ipv4MgmtAddr.pu1_OctetList, Ipv4MgmtAddr.i4_Length);
    u4MgmtIpAddr = OSIX_HTONL (u4MgmtIpAddr);
    CLI_CONVERT_IPADDR_TO_STR (pu1Ipv4MgmtString, u4MgmtIpAddr);
    CliPrintf (CliHandle, "Configured Management Ipv4 Address  : %s\r\n",
               pu1Ipv4MgmtString);
    nmhGetFsLldpConfiguredMgmtIpv6Address (&Ipv6MgmtAddr);

    CliPrintf (CliHandle, "Configured Management Ipv6 Address  : %s\r\n",
               Ip6PrintNtop ((tIp6Addr *) (VOID *) Ipv6MgmtAddr.pu1_OctetList));
    return CLI_SUCCESS;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowChassisIdSubType
 *
 *     DESCRIPTION      : This function will display the LLDP Chassis Id
 *                        Subtype
 *
 *     INPUT            : CliHandle          - CliContext ID
 *                        i4ChassisIdSubType - Chassis Id SubType
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 ***************************************************************************/

PRIVATE VOID
LldpCliShowChassisIdSubType (tCliHandle CliHandle, INT4 i4ChassisIdSubType)
{
    switch (i4ChassisIdSubType)
    {
        case LLDP_CHASS_ID_SUB_CHASSIS_COMP:
            CliPrintf (CliHandle, "Chassis Component \r\n");
            break;
        case LLDP_CHASS_ID_SUB_IF_ALIAS:
            CliPrintf (CliHandle, "Interface Alias \r\n");
            break;
        case LLDP_CHASS_ID_SUB_PORT_COMP:
            CliPrintf (CliHandle, "Port Component \r\n");
            break;
        case LLDP_CHASS_ID_SUB_MAC_ADDR:
            CliPrintf (CliHandle, "Mac Address \r\n");
            break;
        case LLDP_CHASS_ID_SUB_NW_ADDR:
            CliPrintf (CliHandle, "Network Address \r\n");
            break;
        case LLDP_CHASS_ID_SUB_IF_NAME:
            CliPrintf (CliHandle, "Interface Name \r\n");
            break;
        case LLDP_CHASS_ID_SUB_LOCAL:
            CliPrintf (CliHandle, "Local \r\n");
            break;
        default:
            LLDP_TRC (ALL_FAILURE_TRC, " LldpCliShowChassisIdSubType: "
                      "Received invalid subtype \r\n");
            break;
    }
    return;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowPortIdSubType
 *
 *     DESCRIPTION      : This function will display the LLDP Port Id
 *                        Subtype
 *
 *     INPUT            : CliHandle       - CliContext ID
 *                        i4PortIdSubType - Port Id SubType
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 ***************************************************************************/

PRIVATE VOID
LldpCliShowPortIdSubType (tCliHandle CliHandle, INT4 i4PortIdSubType)
{
    switch (i4PortIdSubType)
    {
        case LLDP_PORT_ID_SUB_IF_ALIAS:
            CliPrintf (CliHandle, "Interface Alias \r\n");
            break;
        case LLDP_PORT_ID_SUB_PORT_COMP:
            CliPrintf (CliHandle, "Port Component \r\n");
            break;
        case LLDP_PORT_ID_SUB_MAC_ADDR:
            CliPrintf (CliHandle, "Mac Address \r\n");
            break;
        case LLDP_PORT_ID_SUB_NW_ADDR:
            CliPrintf (CliHandle, "Network Address \r\n");
            break;
        case LLDP_PORT_ID_SUB_IF_NAME:
            CliPrintf (CliHandle, "Interface Name \r\n");
            break;
        case LLDP_PORT_ID_SUB_AGENT_CKT_ID:
            CliPrintf (CliHandle, "Agent Circuit ID\r\n");
            break;
        case LLDP_PORT_ID_SUB_LOCAL:
            CliPrintf (CliHandle, "Local \r\n");
            break;
        default:
            LLDP_TRC (ALL_FAILURE_TRC, " LldpCliShowPortIdSubType: "
                      "Received invalid subtype \r\n");
            break;
    }
    return;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowSysSupportCapab
 *
 *     DESCRIPTION      : This function will display the Supported
 *                        LLDP System Capabilities
 *
 *     INPUT            : CliHandle        - CliContext ID
 *                        SysCapSupported  - System Capabilities Supported
 *                                           Octet String
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 ***************************************************************************/

PRIVATE VOID
LldpCliShowSysSupportCapab (tCliHandle CliHandle,
                            tSNMP_OCTET_STRING_TYPE SysCapSupported)
{

    UINT1               au1SysCapabSupported[ISS_SYS_CAPABILITIES_LEN];
    UINT1               au1DispSysCapabSupported
        [LLDP_CLI_SYS_CAPAB_MAX_BITS * 2];
    UINT1               u1Count = 0;
    UINT1               u1TempCount = 0;
    CHR1                ac1Comma[] = { ',' };
    CHR1                ac1Space[] = { ' ' };
    BOOL1               bResult = OSIX_FALSE;

    MEMSET (au1SysCapabSupported, 0, sizeof (au1SysCapabSupported));
    MEMSET (au1DispSysCapabSupported, 0, sizeof (au1DispSysCapabSupported));

    MEMCPY (&au1SysCapabSupported, SysCapSupported.pu1_OctetList,
            SysCapSupported.i4_Length);

    for (u1Count = 1; u1Count <= LLDP_CLI_SYS_CAPAB_MAX_BITS; u1Count++)
    {
        /* Reset flag */
        bResult = OSIX_FALSE;
        OSIX_BITLIST_IS_BIT_SET (au1SysCapabSupported, u1Count,
                                 ISS_SYS_CAPABILITIES_LEN, bResult);
        if (bResult == OSIX_TRUE)
        {
            au1DispSysCapabSupported[u1TempCount] = gac1SysCapab[u1Count - 1];
            /* Put comma(,) after every system capability display */
            au1DispSysCapabSupported[u1TempCount + 1] = ac1Comma[0];
            u1TempCount += 2;
        }
    }
    if (u1TempCount != 0)
    {
        /* Replace the last comma(,) with space */
        au1DispSysCapabSupported[u1TempCount - 1] = (UINT1) ac1Space[0];
    }
    CliPrintf (CliHandle, "%-16s", au1DispSysCapabSupported);
    return;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowSysEnabled
 *
 *     DESCRIPTION      : This function will display the Enabled
 *                        LLDP System Capabilities
 *
 *     INPUT            : CliHandle        - CliContext ID
 *                        SysCapEnabled    - System Capabilities Enabled
 *                                           Octet String
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 ***************************************************************************/

PRIVATE VOID
LldpCliShowSysEnabledCapab (tCliHandle CliHandle,
                            tSNMP_OCTET_STRING_TYPE SysCapEnabled)
{
    UINT1               au1SysCapabEnabled[ISS_SYS_CAPABILITIES_LEN];
    UINT1               u1Count = 0;
    BOOL1               bResult = OSIX_FALSE;
    BOOL1               bFirst = OSIX_FALSE;

    MEMSET (au1SysCapabEnabled, 0, sizeof (au1SysCapabEnabled));

    MEMCPY (&au1SysCapabEnabled, SysCapEnabled.pu1_OctetList,
            SysCapEnabled.i4_Length);

    for (u1Count = 1; u1Count <= LLDP_CLI_SYS_CAPAB_MAX_BITS; u1Count++)
    {
        /* Reset flag */
        bResult = OSIX_FALSE;
        OSIX_BITLIST_IS_BIT_SET (au1SysCapabEnabled, u1Count,
                                 ISS_SYS_CAPABILITIES_LEN, bResult);
        if (bResult == OSIX_TRUE)
        {
            if (bFirst == OSIX_FALSE)
            {
                bFirst = OSIX_TRUE;
            }
            else
            {
                CliPrintf (CliHandle, ",");
            }
            CliPrintf (CliHandle, "%c", gac1SysCapab[u1Count - 1]);
        }
    }
    return;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowAutoNegAdvCap
 *
 *     DESCRIPTION      : This function will display the Enabled
 *                        LLDP Auto Negotiation Advertised Capabilities
 *
 *     INPUT            : CliHandle        - CliContext ID
 *                        pPhyMediaCap     - Auto Negotiation Advertised
 *                                           Capabilities octet string
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 ***************************************************************************/
PRIVATE VOID
LldpCliShowAutoNegAdvCap (tCliHandle CliHandle,
                          tSNMP_OCTET_STRING_TYPE * pPhyMediaCap)
{
    UINT1               u1Count = 0;
    BOOL1               bResult = OSIX_FALSE;

    for (u1Count = 1; u1Count <= LLDP_CLI_PHY_MEDIA_CAP_MAX_BITS; u1Count++)
    {
        /* Reset flag */
        bResult = OSIX_FALSE;
        OSIX_BITLIST_IS_BIT_SET (pPhyMediaCap->pu1_OctetList, u1Count,
                                 LLDP_MAX_LEN_PHY_MEDIA_CAP, bResult);

        if (bResult == OSIX_TRUE)
        {
            CliPrintf (CliHandle, "%s\n", gapc1PhyMediaCap[u1Count - 1]);
        }
    }
    return;
}

/**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowInterface
 *
 *     DESCRIPTION      : Displays LLDP configuration details on a particular
 *                        interface or all interfaces
 *
 *     INPUT            : CliHandle - CliContext ID
 *                        i4IfIndex - Interface Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/

PUBLIC INT4
LldpCliShowInterface (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 *pDestMac,
                      UINT1 u1ShowOpt)
{
    INT1               *pi1IfName = NULL;
    INT1                ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1String[LLDP_CLI_MAX_MAC_STRING_SIZE];
    tMacAddr            DstMac;
    INT4                i4Status = 0;
    INT4                i4PrevIfIndex = 0;
    INT4                i4TxState = 0;
    INT4                i4RxState = 0;
    INT4                i4NotifyStatus = 0;
    INT4                i4NotifyType = 0;
    UINT4               u4DestMacIndex = 0;
    UINT4               u4PrevDestMacIndex = 0;
    UINT4               u4LocPortNum = 0;
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    INT4                i4Index = i4IfIndex;

    MEMSET (&ai1IfName[0], 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (DstMac, 0, sizeof (tMacAddr));
    MEMSET (au1String, 0, sizeof (LLDP_CLI_MAX_MAC_STRING_SIZE));

    pi1IfName = ai1IfName;

    if (u1ShowOpt == LLDP_CLI_SHOW_ALL)
    {
        /*Show for all the ports */
        if (nmhGetFirstIndexLldpV2PortConfigTable (&i4IfIndex, &u4DestMacIndex)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        /*Show for a specific port */
        if (u1ShowOpt == LLDP_CLI_SHOW_INTERFACE)
        {
            i4PrevIfIndex = i4IfIndex;

            if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
            {
                u4DestMacIndex = LLDP_V1_DEST_MAC_ADDR_INDEX (i4IfIndex);
            }
            else
            {
                u4DestMacIndex = DEFAULT_DEST_MAC_ADDR_INDEX;
            }

            if (nmhValidateIndexInstanceLldpV2PortConfigTable
                (i4IfIndex, u4DestMacIndex) == SNMP_FAILURE)
            {
                u4DestMacIndex = LLDP_V1_DEST_MAC_ADDR_INDEX (i4IfIndex);
            }
        }

        else                    /* u1ShowOpt == LLDP_CLI_SHOW_MAC */
        {
            if (pDestMac != NULL)
            {
                if (nmhGetFslldpv2ConfigPortMapNum
                    (i4IfIndex, pDestMac,
                     (INT4 *) &u4LocPortNum) == SNMP_FAILURE)
                {
                    CLI_SET_ERR (LLDP_CLI_INVALID_DESTINATION_MAC);
                    return CLI_FAILURE;
                }
                if (LldpUtilGetDestMacAddrTblIndex
                    (u4LocPortNum, &u4DestMacIndex) == OSIX_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if (nmhValidateIndexInstanceLldpV2PortConfigTable
                    (i4IfIndex, u4DestMacIndex) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
        }
    }

    do
    {
        if (u1ShowOpt == LLDP_CLI_SHOW_INTERFACE)
        {
            if (i4IfIndex != i4Index)
            {
                do
                {
                    if (i4IfIndex == i4Index)
                    {
                        break;
                    }
                    i4PrevIfIndex = i4IfIndex;
                    u4PrevDestMacIndex = u4DestMacIndex;
                }
                while (nmhGetNextIndexLldpV2PortConfigTable
                       (i4PrevIfIndex, &i4IfIndex, u4PrevDestMacIndex,
                        &u4DestMacIndex) == SNMP_SUCCESS);

                if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
                {
                    break;
                }
            }
        }

        if ((i4Index != i4IfIndex) && (u1ShowOpt == LLDP_CLI_SHOW_INTERFACE))
        {
            break;
        }
        nmhGetLldpV2PortConfigAdminStatus (i4IfIndex, u4DestMacIndex,
                                           &i4Status);
        if (LldpTxUtlGetLocPortEntry ((UINT4) i4IfIndex, &pLldpLocPortInfo)
            == OSIX_FAILURE)
        {
            LLDP_TRC (ALL_FAILURE_TRC, "LldpCliShowInterface"
                      "LldpTxUtlGetLocPortEntry");
        }
        CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName);
        CliPrintf (CliHandle, "%s:\r\n", pi1IfName);

        /* LLDP Admin Status */
        switch (i4Status)
        {
            case LLDP_ADM_STAT_TX_AND_RX:
                CliPrintf (CliHandle, "Tx State            : Enabled\r\n");
                CliPrintf (CliHandle, "Rx State            : Enabled\r\n");
                break;
            case LLDP_ADM_STAT_TX_ONLY:
                CliPrintf (CliHandle, "Tx State            : Enabled\r\n");
                CliPrintf (CliHandle, "Rx State            : Disabled\r\n");
                break;
            case LLDP_ADM_STAT_RX_ONLY:
                CliPrintf (CliHandle, "Tx State            : Disabled\r\n");
                CliPrintf (CliHandle, "Rx State            : Enabled\r\n");
                break;
            case LLDP_ADM_STAT_DISABLED:
                CliPrintf (CliHandle, "Tx State            : Disabled\r\n");
                CliPrintf (CliHandle, "Rx State            : Disabled\r\n");
                break;
            default:
                LLDP_TRC (ALL_FAILURE_TRC, "LldpCliShowInterface: "
                          "Invalid LLDP admin status \r\n");
                break;
        }

        nmhGetLldpV2DestMacAddress (u4DestMacIndex, &DstMac);
        if (LldpUtlGetLocalPort (i4IfIndex, DstMac, &u4LocPortNum) ==
            OSIX_FAILURE)
        {
            LLDP_TRC (ALL_FAILURE_TRC, "LldpCliShowInterface"
                      "LldpTxUtlGetLocPortEntry");
        }
        pLldpLocPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPortNum);
        LLDP_CHK_NULL_PTR_RET (pLldpLocPortInfo, CLI_FAILURE);

        i4TxState = pLldpLocPortInfo->i4TxSemCurrentState;

        /* Transmit SEM state */
        switch (i4TxState)
        {
            case LLDP_TX_INIT:
                CliPrintf (CliHandle, "Tx SEM State        : INITIALIZE\r\n");
                break;
            case LLDP_TX_IDLE:
                CliPrintf (CliHandle, "Tx SEM State        : IDLE\r\n");
                break;
            case LLDP_TX_SHUT_FRAME:
                CliPrintf (CliHandle, "Tx SEM State        : "
                           "SHUTDOWN FRAME\r\n");
                break;
            case LLDP_TX_INFO_FRAME:
                CliPrintf (CliHandle, "Tx SEM State        : INFO FRAME\r\n");
                break;
            default:
                LLDP_TRC (ALL_FAILURE_TRC, "LldpCliShowInterface: "
                          "Invalid TX-SEM state\r\n");
                break;
        }

        i4RxState = pLldpLocPortInfo->i4RxSemCurrentState;

        /* Receive SEM state */
        switch (i4RxState)
        {
            case LLDP_RX_WAIT_PORT_OPERATIONAL:
                CliPrintf (CliHandle, "Rx SEM State        : "
                           "WAIT PORT OPERATIONAL\r\n");
                break;
            case LLDP_RX_DELETE_AGED_INFO:
                CliPrintf (CliHandle, "Rx SEM State        : "
                           "DELETE AGED INFO\r\n");
                break;
            case LLDP_RX_LLDP_INITIALIZE:
                CliPrintf (CliHandle, "Rx SEM State        : INITIALIZE\r\n");
                break;
            case LLDP_RX_WAIT_FOR_FRAME:
                CliPrintf (CliHandle, "Rx SEM State        : "
                           "WAIT FOR FRAME\r\n");
                break;
            case LLDP_RX_FRAME_RECEIVED:
                CliPrintf (CliHandle, "Rx SEM State        : "
                           "FRAME RECEIVED\r\n");
                break;
            case LLDP_RX_DELETE_INFO:
                CliPrintf (CliHandle, "Rx SEM State        : DELETE INFO\r\n");
                break;
            case LLDP_RX_UPDATE_INFO:
                CliPrintf (CliHandle, "Rx SEM State        : UPDATE INFO\r\n");
                break;
            default:
                LLDP_TRC (ALL_FAILURE_TRC, "LldpCliShowInterface: "
                          "Invalid RX-SEM state\r\n");
                break;
        }

        nmhGetLldpV2PortConfigNotificationEnable (i4IfIndex, u4DestMacIndex,
                                                  &i4NotifyStatus);

        /* LLDP Notification Status */
        if (i4NotifyStatus == LLDP_TRUE)
        {
            CliPrintf (CliHandle, "Notification Status : Enabled\r\n");
        }
        else if (i4NotifyStatus == LLDP_FALSE)
        {
            CliPrintf (CliHandle, "Notification Status : Disabled\r\n");
        }

        nmhGetFsLldpPortConfigNotificationType (i4IfIndex, &i4NotifyType);

        /* LLDP Notification Type  */
        switch (i4NotifyType)
        {
            case LLDP_REMOTE_CHG_NOTIFICATION:
                CliPrintf (CliHandle,
                           "Notification Type   : Remote Table Change\r\n");
                break;
            case LLDP_MIS_CFG_NOTIFICATION:
                CliPrintf (CliHandle,
                           "Notification Type   : Mis-configuration\r\n");
                break;
            case LLDP_REMOTE_CHG_MIS_CFG_NOTIFICATION:
                CliPrintf (CliHandle, "Notification Type   : RemoteTableChg "
                           "and MisConfiguration\r\n");
                break;
            default:
                LLDP_TRC (ALL_FAILURE_TRC, "LldpCliShowInterface: "
                          "Invalid Notification type\r\n");
                break;
        }

        /* Destination Mac Address */
        nmhGetLldpV2DestMacAddress (u4DestMacIndex, &DstMac);
        PrintMacAddress (DstMac, au1String);
        CliPrintf (CliHandle, "DestinationMacAddr  : ");
        CliPrintf (CliHandle, "%s\r\n", au1String);

        if (u1ShowOpt == LLDP_CLI_SHOW_MAC)
        {
            break;                /*Displayed the particular entry */
        }

        i4PrevIfIndex = i4IfIndex;
        u4PrevDestMacIndex = u4DestMacIndex;
    }
    while (nmhGetNextIndexLldpV2PortConfigTable
           (i4PrevIfIndex, &i4IfIndex, u4PrevDestMacIndex,
            &u4DestMacIndex) == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowNeighbor
 *
 *     DESCRIPTION      : Displays information about neighbors learnt on an
 *                        interface or all interfaces
 *
 *     INPUT            : CliHandle    - CliContext ID
 *                        i4Option     - Show Neighbor Option
 *                        i4IfIndex    - Interface Index
 *                        pu1ChassisId - Chassis Id
 *                        pu1PortId    - Port Id
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/

PUBLIC INT4
LldpCliShowNeighbor (tCliHandle CliHandle, INT4 i4Option, INT4 i4IfIndex,
                     UINT1 *pu1ChassisId, UINT1 *pu1PortId,
                     UINT1 *pau1DestMacAddr)
{
    UINT1               u1MacSetFlag = LLDP_FALSE;
    UINT2               u2ChassisIdLen;
    UINT2               u2PortIdLen;
    UINT4               u4RemNodeCount = 0;
    UINT4               u4RemTimeMark = 0;
    UINT4               u4RemDestMacAddr = 0;
    UINT4               u4LocPortNum = 0;
    INT4                i4RemLocalPortNum = 0;
    INT4                i4RemIndex = 0;
    UINT4               u4PrevRemTimeMark = 0;
    UINT4               u4PrevRemLocalPortNum = 0;
    UINT4               u4PrevRemDestMacAddr = 0;
    UINT4               u4PrevRemIndex = 0;

    tLldCliSizingNeighInfo *pCliNeighInfo = NULL;

    if ((pCliNeighInfo = (tLldCliSizingNeighInfo *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpCliSizingNeighPoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowNeighbor: Failed to Allocate Memory "
                  "CliNeighInfo \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }

    MEMSET (pCliNeighInfo, 0, sizeof (tLldCliSizingNeighInfo));
    pCliNeighInfo->CliHandle = CliHandle;

    if (i4Option == LLDP_CLI_SHOW_ALL_NEIGBR ||
        i4Option == LLDP_CLI_SHOW_NEIGBR_INTF ||
        i4Option == LLDP_CLI_SHOW_NEIGBR_MSAP_INTF ||
        i4Option == LLDP_CLI_SHOW_NEIGBR_MSAP)
    {

        /* LLDP Standard System Capability codes */
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "Capability Codes   : \r\n");
        CliPrintf (CliHandle, "(R) Router, (B) Bridge, (T) Telephone, "
                   "(C) DOCSIS Cable Device, \r\n"
                   "(W) WLAN Access Point, "
                   "(P) Repeater, (S) Station, (O) Other \r\n\r\n");

        CliPrintf (CliHandle,
                   "%-20s %-13s %-11s %-15s %s\r\n",
                   "Chassis ID", "Local Intf", "Hold-time", "Capability",
                   "Port Id");

        CliPrintf (CliHandle,
                   "%-20s %-13s %-11s %-15s %s\r\n",
                   "----------", "----------", "---------", "----------",
                   "-------");

    }
    RBTreeCount (gLldpGlobalInfo.RemMSAPRBTree, &u4RemNodeCount);
    if (i4Option == LLDP_CLI_SHOW_ALL_NEIGBR_DETL ||
        i4Option == LLDP_CLI_SHOW_NEIGBR_INTF_DETL ||
        i4Option == LLDP_CLI_SHOW_NEIGBR_MSAP_INTF_DETL ||
        i4Option == LLDP_CLI_SHOW_NEIGBR_MSAP_DETL)
    {
        if (u4RemNodeCount != 0)
        {
            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle, "Capability Codes   : \r\n");
            CliPrintf (CliHandle, "(R) Router, (B) Bridge, (T) Telephone, "
                       "(C) DOCSIS Cable Device, \r\n"
                       "(W) WLAN Access Point, "
                       "(P) Repeater, (S) Station, (O) Other\r\n" "\r\n");
        }
    }
    if (nmhGetFirstIndexLldpV2RemTable (&u4RemTimeMark,
                                        &i4RemLocalPortNum,
                                        &u4RemDestMacAddr,
                                        (UINT4 *) &i4RemIndex) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "Total Entries Displayed : "
                   "%d\r\n", u4RemNodeCount);
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpCliSizingNeighPoolId,
                                (UINT1 *) pCliNeighInfo) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "LldpCliShowNeighbor: Failed to Release Memory "
                      "for LldpCliSizingNeighInfo");
        }
        return CLI_SUCCESS;
    }

    if ((i4Option == LLDP_CLI_SHOW_NEIGBR_MSAP_INTF_DETL) ||
        (i4Option == LLDP_CLI_SHOW_NEIGBR_MSAP_INTF) ||
        (i4Option == LLDP_CLI_SHOW_NEIGBR_MSAP_DETL) ||
        (i4Option == LLDP_CLI_SHOW_NEIGBR_MSAP))
    {
        u2ChassisIdLen = (UINT2) (STRLEN (pu1ChassisId));
        u2PortIdLen = (UINT2) (STRLEN (pu1PortId));
        MEMCPY (pCliNeighInfo->au1ChassisId, pu1ChassisId,
                MEM_MAX_BYTES (u2ChassisIdLen, LLDP_MAX_LEN_CHASSISID));
        MEMCPY (pCliNeighInfo->au1PortId, pu1PortId,
                MEM_MAX_BYTES (u2PortIdLen, LLDP_MAX_LEN_PORTID));
    }
    pCliNeighInfo->i4IfIndex = i4IfIndex;
    pCliNeighInfo->i4Option = i4Option;

    if (pau1DestMacAddr != NULL)
    {
        u1MacSetFlag = LLDP_TRUE;
        if (nmhValidateIndexInstanceFslldpv2ConfigPortMapTable
            (pCliNeighInfo->i4IfIndex, pau1DestMacAddr) == SNMP_FAILURE)
        {
            MemReleaseMemBlock (gLldpGlobalInfo.LldpCliSizingNeighPoolId,
                                (UINT1 *) pCliNeighInfo);
            return CLI_FAILURE;
        }
        nmhGetFslldpv2ConfigPortMapNum (pCliNeighInfo->i4IfIndex,
                                        pau1DestMacAddr,
                                        (INT4 *) &u4LocPortNum);
        LldpGetDestAddrTblIndexFromMacAddr ((tMacAddr *) pau1DestMacAddr,
                                            &u4RemDestMacAddr);

        u4PrevRemTimeMark = 0;
        u4PrevRemLocalPortNum = (UINT4) pCliNeighInfo->i4IfIndex;
        u4PrevRemDestMacAddr = u4RemDestMacAddr;
        u4PrevRemIndex = 0;

        if (nmhGetNextIndexLldpV2RemTable (u4PrevRemTimeMark, &u4RemTimeMark,
                                           (INT4) u4PrevRemLocalPortNum,
                                           &i4RemLocalPortNum,
                                           u4PrevRemDestMacAddr,
                                           &u4RemDestMacAddr, u4PrevRemIndex,
                                           (UINT4 *) &i4RemIndex) ==
            SNMP_FAILURE)
        {
            MemReleaseMemBlock (gLldpGlobalInfo.LldpCliSizingNeighPoolId,
                                (UINT1 *) pCliNeighInfo);
            return CLI_FAILURE;
        }

    }

    /* Based on given option, displays Neighbor Info */
    switch (i4Option)
    {
        case LLDP_CLI_SHOW_NEIGBR_INTF:
        case LLDP_CLI_SHOW_NEIGBR_INTF_DETL:

            /* Shows Brief or Detail Info for a particular
             * neighbor based on the interface index
             * given by the user */
            LldpCliShowIntfNeighborInfo (u4RemTimeMark,
                                         i4RemLocalPortNum,
                                         i4RemIndex, pCliNeighInfo,
                                         u4RemDestMacAddr, u1MacSetFlag);
            break;

        case LLDP_CLI_SHOW_NEIGBR_MSAP:
        case LLDP_CLI_SHOW_NEIGBR_MSAP_DETL:

            /* Shows Brief or Detail Info for a particular
             * neighbor based on the indices chassisid and portid (MSAP) */
            LldpCliShowNeighborMSAPInfo (u4RemTimeMark,
                                         i4RemLocalPortNum,
                                         i4RemIndex, pCliNeighInfo,
                                         u4RemDestMacAddr);
            break;

        case LLDP_CLI_SHOW_NEIGBR_MSAP_INTF_DETL:
        case LLDP_CLI_SHOW_NEIGBR_MSAP_INTF:

            /* Shows Brief or Detail Info for a particular
             * neighbor based on the indices chassisid, portid (MSAP)
             * and interface index  */
            LldpCliShowNeighborMSAPIntfInfo (u4RemTimeMark,
                                             i4RemLocalPortNum,
                                             i4RemIndex, pCliNeighInfo,
                                             u4RemDestMacAddr);
            break;

        case LLDP_CLI_SHOW_ALL_NEIGBR:
        case LLDP_CLI_SHOW_ALL_NEIGBR_DETL:

            /* Shows Brief or Detail Info for a all the
             * neighbors */
            LldpCliShowAllNeighborInfo (u4RemTimeMark,
                                        i4RemLocalPortNum,
                                        i4RemIndex, pCliNeighInfo,
                                        u4RemDestMacAddr);

            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle, "Total Entries Displayed : "
                       "%d\r\n", u4RemNodeCount);
            break;

        default:
            break;
    }

    if (MemReleaseMemBlock (gLldpGlobalInfo.LldpCliSizingNeighPoolId,
                            (UINT1 *) pCliNeighInfo) == MEM_FAILURE)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowNeighbor: Failed to Release Memory "
                  "for LldpCliSizingNeighInfo");
    }
    return CLI_SUCCESS;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowIntfNeighborInfo
 *
 *     DESCRIPTION      : Displays brief or detailed information about
 *                        neighbors learnt on given interface
 *
 *     INPUT            : pCliNeighborInfo  - LldpCliNeighborInfo structure
 *                        u4RemTimeMark     - Remote Node Time Mark
 *                        i4RemLocalPortNum - Local port where remote
 *                                            information is received
 *                        i4RemIndex        - Remote Node Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS
 *
 ***************************************************************************/

PRIVATE INT4
LldpCliShowIntfNeighborInfo (UINT4 u4RemTimeMark,
                             INT4 i4RemLocalPortNum,
                             INT4 i4RemIndex,
                             tLldpCliNeighborInfo * pCliNeighborInfo,
                             UINT4 u4RemDestMacAddr, UINT1 u1MacSetFlag)
{
    UINT4               u4PrevRemTimeMark = 0;
    UINT4               u4PrevRemDestMacAddr = 0;
    INT4                i4PrevRemLocalPortNum = 0;
    INT4                i4PrevRemIndex = 0;
    INT4                i4TotNeighCount = 0;

    do
    {
        if ((i4RemLocalPortNum == pCliNeighborInfo->i4IfIndex) != 0)
        {
            if (pCliNeighborInfo->i4Option == LLDP_CLI_SHOW_NEIGBR_INTF)
            {
                i4TotNeighCount++;

                /* Shows Brief Info */
                LldpCliShowNeighborBriefInfo (pCliNeighborInfo->
                                              CliHandle,
                                              u4RemTimeMark,
                                              i4RemLocalPortNum, i4RemIndex,
                                              u4RemDestMacAddr);
            }
            else if (pCliNeighborInfo->i4Option
                     == LLDP_CLI_SHOW_NEIGBR_INTF_DETL)
            {
                i4TotNeighCount++;

                /* Shows Detail Info */
                LldpCliShowNeighborDetailInfo (pCliNeighborInfo->
                                               CliHandle,
                                               u4RemTimeMark,
                                               i4RemLocalPortNum, i4RemIndex,
                                               u4RemDestMacAddr);
            }
        }

        if (u1MacSetFlag == LLDP_TRUE)
        {
            break;
        }

        u4PrevRemTimeMark = u4RemTimeMark;
        i4PrevRemLocalPortNum = i4RemLocalPortNum;
        i4PrevRemIndex = i4RemIndex;
        u4PrevRemDestMacAddr = u4RemDestMacAddr;
    }
    while (nmhGetNextIndexLldpV2RemTable (u4PrevRemTimeMark,
                                          &u4RemTimeMark,
                                          i4PrevRemLocalPortNum,
                                          &i4RemLocalPortNum,
                                          u4PrevRemDestMacAddr,
                                          &u4RemDestMacAddr,
                                          (UINT4) i4PrevRemIndex,
                                          (UINT4 *) &i4RemIndex) ==
           SNMP_SUCCESS);

    CliPrintf (pCliNeighborInfo->CliHandle, "\r\n");
    CliPrintf (pCliNeighborInfo->CliHandle, "Total Entries Displayed : "
               "%d\r\n", i4TotNeighCount);
    return CLI_SUCCESS;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowNeighborMSAPInfo
 *
 *     DESCRIPTION      : Displays information about Neighbor which has
 *                        the given Chassis and Port (MSAP)
 *
 *     INPUT            : pCliNeighborInfo  - Neighbor Info Structure
 *                        u4RemTimeMark     - Remote Node Time Mark
 *                        i4RemLocalPortNum - Local port where remote
 *                                            information is received
 *                        i4RemIndex        - Remote Node Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS
 *
 ***************************************************************************/

PRIVATE INT4
LldpCliShowNeighborMSAPInfo (UINT4 u4RemTimeMark,
                             INT4 i4RemLocalPortNum,
                             INT4 i4RemIndex,
                             tLldpCliNeighborInfo * pCliNeighborInfo,
                             UINT4 u4RemDestMacAddr)
{
    UINT1              *pu1ChassisId = NULL;
    UINT1               au1PortId[LLDP_MAX_LEN_PORTID + 1];
    UINT1               au1ChassisIdStr[LLDP_MAX_LEN_CHASSISID + 1];
    UINT1               au1PortIdStr[LLDP_MAX_LEN_PORTID + 1];
    CHR1               *pc1ChassIdNwAddr;
    UINT4               u4ChassIdNwAddr = 0;
    UINT4               u4PrevRemTimeMark = 0;
    UINT4               u4PrevRemDestMacAddr = 0;
    INT4                i4TotNeighCount = 0;
    INT4                i4PrevRemLocalPortNum = 0;
    INT4                i4PrevRemIndex = 0;
    INT4                i4ChassisIdSubType = 0;
    INT4                i4PortIdSubType = 0;
    INT4                i4ArrayAllocCnt = 0;
    tSNMP_OCTET_STRING_TYPE ChassisId;
    tSNMP_OCTET_STRING_TYPE PortId;

    if ((pu1ChassisId = (UINT1 *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpArrayInfoPoolId))) == NULL)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowNeighborMSAPInfo: Failed to Allocate Memory"
                  "for Chassis \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }
    i4ArrayAllocCnt++;

    do
    {
        MEMSET (&ChassisId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (&PortId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (pu1ChassisId, 0, LLDP_MAX_LEN_CHASSISID + 1);
        MEMSET (au1PortId, 0, LLDP_MAX_LEN_PORTID + 1);

        ChassisId.pu1_OctetList = pu1ChassisId;
        PortId.pu1_OctetList = &au1PortId[0];
        nmhGetLldpV2RemChassisId (u4RemTimeMark, i4RemLocalPortNum,
                                  u4RemDestMacAddr, (UINT4) i4RemIndex,
                                  &ChassisId);
        nmhGetLldpV2RemChassisIdSubtype (u4RemTimeMark, i4RemLocalPortNum,
                                         u4RemDestMacAddr, (UINT4) i4RemIndex,
                                         &i4ChassisIdSubType);
        nmhGetLldpV2RemPortId (u4RemTimeMark, i4RemLocalPortNum,
                               u4RemDestMacAddr, (UINT4) i4RemIndex, &PortId);
        nmhGetLldpV2RemPortIdSubtype (u4RemTimeMark, i4RemLocalPortNum,
                                      u4RemDestMacAddr, (UINT4) i4RemIndex,
                                      &i4PortIdSubType);

        if (i4ChassisIdSubType == LLDP_CHASS_ID_SUB_MAC_ADDR)
        {
            MEMSET (&au1ChassisIdStr, 0, sizeof (au1ChassisIdStr));

            /* Convert octet values into strings of octets seperated by colon */
            CLI_CONVERT_MAC_TO_DOT_STR (ChassisId.pu1_OctetList,
                                        &au1ChassisIdStr[0]);
            ChassisId.pu1_OctetList = &au1ChassisIdStr[0];
        }
        else if (i4ChassisIdSubType == LLDP_CHASS_ID_SUB_NW_ADDR)
        {
            MEMSET (au1ChassisIdStr, 0, sizeof (au1ChassisIdStr));
            PTR_FETCH4 (u4ChassIdNwAddr, (ChassisId.pu1_OctetList + 1));
            pc1ChassIdNwAddr = (CHR1 *) & au1ChassisIdStr[0];
            /* Convert octet values into strings of octets seperated by dot */
            CLI_CONVERT_IPADDR_TO_STR (pc1ChassIdNwAddr, u4ChassIdNwAddr);
            ChassisId.pu1_OctetList = (UINT1 *) pc1ChassIdNwAddr;
        }
        if (i4PortIdSubType == LLDP_PORT_ID_SUB_MAC_ADDR)
        {
            MEMSET (&au1PortIdStr, 0, sizeof (au1PortIdStr));

            /* Convert octet values into strings of octets
             * seperated by colon */
            CLI_CONVERT_MAC_TO_DOT_STR (PortId.pu1_OctetList, &au1PortIdStr[0]);
            PortId.pu1_OctetList = &au1PortIdStr[0];
        }

        if ((STRNCMP (pCliNeighborInfo->au1ChassisId, ChassisId.pu1_OctetList,
                      LLDP_MAX_LEN_CHASSISID)
             == 0) &&
            (STRNCMP (pCliNeighborInfo->au1PortId, PortId.pu1_OctetList,
                      LLDP_MAX_LEN_PORTID) == 0))
        {
            i4TotNeighCount++;
            if (pCliNeighborInfo->i4Option == LLDP_CLI_SHOW_NEIGBR_MSAP)
            {
                /* Shows Brief Info */
                LldpCliShowNeighborBriefInfo (pCliNeighborInfo->
                                              CliHandle,
                                              u4RemTimeMark,
                                              i4RemLocalPortNum, i4RemIndex,
                                              u4RemDestMacAddr);
            }
            else
            {
                /* Shows Detail Info */
                LldpCliShowNeighborDetailInfo (pCliNeighborInfo->
                                               CliHandle,
                                               u4RemTimeMark,
                                               i4RemLocalPortNum, i4RemIndex,
                                               u4RemDestMacAddr);
            }
        }
        u4PrevRemTimeMark = u4RemTimeMark;
        i4PrevRemLocalPortNum = i4RemLocalPortNum;
        i4PrevRemIndex = i4RemIndex;
        u4PrevRemDestMacAddr = u4RemDestMacAddr;
    }
    while (nmhGetNextIndexLldpV2RemTable (u4PrevRemTimeMark,
                                          &u4RemTimeMark,
                                          i4PrevRemLocalPortNum,
                                          &i4RemLocalPortNum,
                                          u4PrevRemDestMacAddr,
                                          &u4RemDestMacAddr,
                                          (UINT4) i4PrevRemIndex,
                                          (UINT4 *) &i4RemIndex) ==
           SNMP_SUCCESS);

    CliPrintf (pCliNeighborInfo->CliHandle, "\r\n");
    CliPrintf (pCliNeighborInfo->CliHandle, "Total Entries Displayed : "
               "%d\r\n", i4TotNeighCount);

    if (LldpMemRelease (gLldpGlobalInfo.LldpArrayInfoPoolId,
                        i4ArrayAllocCnt, pu1ChassisId) == LLDP_FALSE)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC | ALL_FAILURE_TRC,
                  "LldpArrayInfoPoolId Memory release failed!!.\r\n");
    }
    return CLI_SUCCESS;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowNeighborMSAPIntfInfo
 *
 *     DESCRIPTION      : Displays information about Neighbor which has
 *                        the given chassis id, port id (MSAP) & interface
 *                        index
 *
 *     INPUT            : pCliNeighborInfo  - Neighbor Info Structure
 *                        u4RemTimeMark     - Remote Node Time Mark
 *                        i4RemLocalPortNum - Local port where remote
 *                                                information is received
 *                        i4RemIndex        - Remote Node Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS
 *
 ***************************************************************************/

PRIVATE INT4
LldpCliShowNeighborMSAPIntfInfo (UINT4 u4RemTimeMark,
                                 INT4 i4RemLocalPortNum,
                                 INT4 i4RemIndex,
                                 tLldpCliNeighborInfo * pCliNeighborInfo,
                                 UINT4 u4RemDestMacAddr)
{
    UINT1              *pu1ChassisId = NULL;
    UINT1               au1PortId[LLDP_MAX_LEN_PORTID + 1];
    UINT1               au1ChassisIdStr[LLDP_MAX_LEN_CHASSISID + 1];
    UINT1               au1PortIdStr[LLDP_MAX_LEN_PORTID + 1];
    CHR1               *pc1ChassIdNwAddr;
    UINT4               u4ChassIdNwAddr = 0;
    UINT4               u4PrevRemTimeMark = 0;
    UINT4               u4PrevRemDestMacAddr = 0;
    INT4                i4TotNeighCount = 0;
    INT4                i4PrevRemLocalPortNum = 0;
    INT4                i4PrevRemIndex = 0;
    INT4                i4ChassisIdSubType = 0;
    INT4                i4PortIdSubType = 0;
    INT4                i4ArrayAllocCnt = 0;
    tSNMP_OCTET_STRING_TYPE ChassisId;
    tSNMP_OCTET_STRING_TYPE PortId;

    if ((pu1ChassisId = (UINT1 *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpArrayInfoPoolId))) == NULL)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowNeighborMSAPInfo: Failed to Allocate Memory"
                  "for Chassis \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }
    i4ArrayAllocCnt++;
    do
    {
        MEMSET (&ChassisId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (&PortId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (pu1ChassisId, 0, LLDP_MAX_LEN_CHASSISID + 1);
        MEMSET (au1PortId, 0, LLDP_MAX_LEN_PORTID + 1);

        ChassisId.pu1_OctetList = pu1ChassisId;
        PortId.pu1_OctetList = &au1PortId[0];

        nmhGetLldpV2RemChassisId (u4RemTimeMark, i4RemLocalPortNum,
                                  u4RemDestMacAddr, (UINT4) i4RemIndex,
                                  &ChassisId);
        nmhGetLldpV2RemChassisIdSubtype (u4RemTimeMark, i4RemLocalPortNum,
                                         u4RemDestMacAddr, (UINT4) i4RemIndex,
                                         &i4ChassisIdSubType);
        nmhGetLldpV2RemPortId (u4RemTimeMark, i4RemLocalPortNum,
                               u4RemDestMacAddr, (UINT4) i4RemIndex, &PortId);
        nmhGetLldpV2RemPortIdSubtype (u4RemTimeMark, i4RemLocalPortNum,
                                      u4RemDestMacAddr, (UINT4) i4RemIndex,
                                      &i4PortIdSubType);

        if (i4ChassisIdSubType == LLDP_CHASS_ID_SUB_MAC_ADDR)
        {
            MEMSET (&au1ChassisIdStr, 0, sizeof (au1ChassisIdStr));

            /* Convert octet values into strings of octets seperated by colon */
            CLI_CONVERT_MAC_TO_DOT_STR (ChassisId.pu1_OctetList,
                                        &au1ChassisIdStr[0]);
            ChassisId.pu1_OctetList = &au1ChassisIdStr[0];
        }
        else if (i4ChassisIdSubType == LLDP_CHASS_ID_SUB_NW_ADDR)
        {
            MEMSET (au1ChassisIdStr, 0, sizeof (au1ChassisIdStr));
            PTR_FETCH4 (u4ChassIdNwAddr, (ChassisId.pu1_OctetList + 1));
            pc1ChassIdNwAddr = (CHR1 *) & au1ChassisIdStr[0];
            /* Convert octet values into strings of octets seperated by dot */
            CLI_CONVERT_IPADDR_TO_STR (pc1ChassIdNwAddr, u4ChassIdNwAddr);
            ChassisId.pu1_OctetList = (UINT1 *) pc1ChassIdNwAddr;
        }
        if (i4PortIdSubType == LLDP_PORT_ID_SUB_MAC_ADDR)
        {
            MEMSET (&au1PortIdStr, 0, sizeof (au1PortIdStr));

            /* Convert octet values into strings of octets
             * seperated by colon */
            CLI_CONVERT_MAC_TO_DOT_STR (PortId.pu1_OctetList, &au1PortIdStr[0]);
            PortId.pu1_OctetList = &au1PortIdStr[0];
        }

        if ((i4RemLocalPortNum == pCliNeighborInfo->i4IfIndex) &&
            (STRNCMP (pCliNeighborInfo->au1ChassisId, ChassisId.pu1_OctetList,
                      LLDP_MAX_LEN_CHASSISID)
             == 0) &&
            (STRNCMP (pCliNeighborInfo->au1PortId, PortId.pu1_OctetList,
                      LLDP_MAX_LEN_PORTID) == 0))
        {
            i4TotNeighCount++;
            if (pCliNeighborInfo->i4Option == LLDP_CLI_SHOW_NEIGBR_MSAP_INTF)
            {
                /* Shows Brief Info */
                LldpCliShowNeighborBriefInfo (pCliNeighborInfo->
                                              CliHandle,
                                              u4RemTimeMark,
                                              i4RemLocalPortNum, i4RemIndex,
                                              u4RemDestMacAddr);
            }
            else
            {
                /* Shows Detail Info */
                LldpCliShowNeighborDetailInfo (pCliNeighborInfo->
                                               CliHandle,
                                               u4RemTimeMark,
                                               i4RemLocalPortNum, i4RemIndex,
                                               u4RemDestMacAddr);
            }
        }
        u4PrevRemTimeMark = u4RemTimeMark;
        i4PrevRemLocalPortNum = i4RemLocalPortNum;
        i4PrevRemIndex = i4RemIndex;
        u4PrevRemDestMacAddr = u4RemDestMacAddr;
    }
    while (nmhGetNextIndexLldpV2RemTable (u4PrevRemTimeMark,
                                          &u4RemTimeMark,
                                          i4PrevRemLocalPortNum,
                                          &i4RemLocalPortNum,
                                          u4PrevRemDestMacAddr,
                                          &u4RemDestMacAddr,
                                          (UINT4) i4PrevRemIndex,
                                          (UINT4 *) &i4RemIndex) ==
           SNMP_SUCCESS);

    CliPrintf (pCliNeighborInfo->CliHandle, "\r\n");
    CliPrintf (pCliNeighborInfo->CliHandle, "Total Entries Displayed : "
               "%d\r\n", i4TotNeighCount);

    if (LldpMemRelease (gLldpGlobalInfo.LldpArrayInfoPoolId,
                        i4ArrayAllocCnt, pu1ChassisId) == LLDP_FALSE)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC | ALL_FAILURE_TRC,
                  "LldpArrayInfoPoolId Memory release failed!!.\r\n");
    }
    return CLI_SUCCESS;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowAllNeighborInfo
 *
 *     DESCRIPTION      : Displays information about all the Neighbors
 *                        learnt on the LLDP system.
 *
 *     INPUT            : pCliNeighborInfo  - Neighbor Info Structure
 *                        u4RemTimeMark     - Remote Node Time Mark
 *                        i4RemLocalPortNum - Local port where remote
 *                                                information is received
 *                        i4RemIndex        - Remote Node Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS
 *
 ***************************************************************************/

PRIVATE INT4
LldpCliShowAllNeighborInfo (UINT4 u4RemTimeMark,
                            INT4 i4RemLocalPortNum,
                            INT4 i4RemIndex,
                            tLldpCliNeighborInfo * pCliNeighborInfo,
                            UINT4 u4RemDestMacAddr)
{
    UINT4               u4PrevRemTimeMark = 0;
    UINT4               u4PrevRemDestMacAddr = 0;
    INT4                i4PrevRemLocalPortNum = 0;
    INT4                i4PrevRemIndex = 0;

    do
    {

        if (pCliNeighborInfo->i4Option == LLDP_CLI_SHOW_ALL_NEIGBR)
        {
            /* Shows Brief Info */
            LldpCliShowNeighborBriefInfo (pCliNeighborInfo->CliHandle,
                                          u4RemTimeMark,
                                          i4RemLocalPortNum, i4RemIndex,
                                          u4RemDestMacAddr);
        }
        else
        {
            /* Shows Detail Info */
            LldpCliShowNeighborDetailInfo (pCliNeighborInfo->CliHandle,
                                           u4RemTimeMark,
                                           i4RemLocalPortNum, i4RemIndex,
                                           u4RemDestMacAddr);
        }
        u4PrevRemTimeMark = u4RemTimeMark;
        i4PrevRemLocalPortNum = i4RemLocalPortNum;
        i4PrevRemIndex = i4RemIndex;
        u4PrevRemDestMacAddr = u4RemDestMacAddr;
    }
    while (nmhGetNextIndexLldpV2RemTable (u4PrevRemTimeMark,
                                          &u4RemTimeMark,
                                          i4PrevRemLocalPortNum,
                                          &i4RemLocalPortNum,
                                          u4PrevRemDestMacAddr,
                                          &u4RemDestMacAddr,
                                          (UINT4) i4PrevRemIndex,
                                          (UINT4 *) &i4RemIndex) ==
           SNMP_SUCCESS);
    return CLI_SUCCESS;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowNeighborBriefInfo
 *
 *     DESCRIPTION      : Displays Brief information about neighbors
 *                        learnt on an interface or all interfaces
 *
 *     INPUT            : CliHandle         - CliContext ID
 *                        u4RemTimeMark     - Remote Node Time Mark
 *                        i4RemLocalPortNum - Local port where remote
 *                                            information is received
 *                        i4RemIndex        - Remote Node Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/

PRIVATE INT4
LldpCliShowNeighborBriefInfo (tCliHandle CliHandle,
                              UINT4 u4RemTimeMark,
                              INT4 i4RemLocalPortNum, INT4 i4RemIndex,
                              UINT4 u4RemDestMacAddr)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1              *pu1ChassisId = NULL;
    UINT1              *pu1PortId = NULL;
    UINT1              *pu1DispChassisId = NULL;
    UINT1              *pu1DispPortId = NULL;
    CHR1               *pc1DispPortIdNwAddr = NULL;
    CHR1               *pc1DispChassIdNwAddr = NULL;
    INT1               *pi1IfName = NULL;
    UINT2               u2SysCapSup;
    INT4                i4ChassisIdSubtype = 0;
    INT4                i4PortIdSubtype = 0;
    UINT4               u4RxTTL = 0;
    UINT4               u4ChassIdNwAddr = 0;
    UINT4               u4PortIdNwAddr = 0;
    INT4                i4RemNodeAllocCnt = 0;
    INT4                i4ArrayAllocCnt = 0;
    tLldpRemoteNode    *pTempRemNode = NULL;
    tLldpRemoteNode    *pRemNode = NULL;
    tSNMP_OCTET_STRING_TYPE PortId;
    tSNMP_OCTET_STRING_TYPE ChassisId;
    tSNMP_OCTET_STRING_TYPE SysCapSupported;

    /* Allocate Memory for Chassis */
    if ((pu1ChassisId = (UINT1 *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpArrayInfoPoolId))) == NULL)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowNeighborBriefInfo: Failed to Allocate Memory"
                  "for Chassis \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }
    i4ArrayAllocCnt++;

    /* Allocate Memory for DispChassisId */
    if ((pu1DispChassisId = (UINT1 *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpArrayInfoPoolId))) == NULL)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1ChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowNeighborBriefInfo: Failed to Allocate Memory "
                  "for DispChassis \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }
    i4ArrayAllocCnt++;

    /* Allocate Memory for PortId */
    if ((pu1PortId = (UINT1 *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpArrayInfoPoolId))) == NULL)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1ChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowNeighborBriefInfo: Failed to Allocate Memory "
                  "for Port \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }
    i4ArrayAllocCnt++;

    /* Allocate Memory for DispPortId */
    if ((pu1DispPortId = (UINT1 *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpArrayInfoPoolId))) == NULL)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1ChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }

        LLDP_TRC (LLDP_MANDATORY_TLV_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowNeighborBriefInfo: Failed to Allocate Memory "
                  "for DispPort \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }
    i4ArrayAllocCnt++;

    MEMSET (&ChassisId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PortId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&SysCapSupported, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (pu1ChassisId, 0, LLDP_MAX_LEN_CHASSISID + 1);
    MEMSET (pu1PortId, 0, LLDP_MAX_LEN_PORTID + 1);
    MEMSET (pu1DispChassisId, 0, LLDP_MAX_LEN_CHASSISID + 1);
    MEMSET (pu1DispPortId, 0, LLDP_MAX_LEN_PORTID + 1);

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    pi1IfName = (INT1 *) au1IfName;
    ChassisId.pu1_OctetList = pu1ChassisId;
    PortId.pu1_OctetList = pu1PortId;
    SysCapSupported.pu1_OctetList = (UINT1 *) &u2SysCapSup;
    SysCapSupported.i4_Length = sizeof (u2SysCapSup);

    nmhGetLldpV2RemChassisIdSubtype (u4RemTimeMark,
                                     i4RemLocalPortNum,
                                     u4RemDestMacAddr,
                                     (UINT4) i4RemIndex, &i4ChassisIdSubtype);
    if (nmhGetLldpV2RemChassisId (u4RemTimeMark,
                                  i4RemLocalPortNum,
                                  u4RemDestMacAddr,
                                  (UINT4) i4RemIndex,
                                  &ChassisId) != SNMP_SUCCESS)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1ChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispPortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }

        return CLI_SUCCESS;
    }

    if (i4ChassisIdSubtype == LLDP_CHASS_ID_SUB_MAC_ADDR)
    {
        MEMSET (pu1DispChassisId, 0, LLDP_MAX_LEN_CHASSISID + 1);

        /* Convert octet values into strings of octets seperated by colon */
        CLI_CONVERT_MAC_TO_DOT_STR (ChassisId.pu1_OctetList, pu1DispChassisId);
        CliPrintf (CliHandle, "%-21s", pu1DispChassisId);
    }
    else if (i4ChassisIdSubtype == LLDP_CHASS_ID_SUB_NW_ADDR)
    {
        MEMSET (pu1DispChassisId, 0, LLDP_MAX_LEN_CHASSISID + 1);
        /* To skip the IANA Address Family Numbers */
        ChassisId.pu1_OctetList++;
        PTR_FETCH4 (u4ChassIdNwAddr, ChassisId.pu1_OctetList);
        pc1DispChassIdNwAddr = (CHR1 *) pu1DispChassisId;
        /* Convert octet values into strings of octets seperated by dot */
        CLI_CONVERT_IPADDR_TO_STR (pc1DispChassIdNwAddr, u4ChassIdNwAddr);
        CliPrintf (CliHandle, "%-21s", pc1DispChassIdNwAddr);
    }
    else
    {
        if (STRLEN (ChassisId.pu1_OctetList) >= LLDP_CLI_CHASSIS_DISP_LEN)
        {
            MEMSET (pu1DispChassisId, 0, LLDP_MAX_LEN_CHASSISID + 1);
            STRNCPY (pu1DispChassisId, ChassisId.pu1_OctetList,
                     LLDP_CLI_CHASSIS_DISP_LEN);
            STRCAT (pu1DispChassisId, "...");
            CliPrintf (CliHandle, "%-21s", pu1DispChassisId);
        }
        else
        {
            CliPrintf (CliHandle, "%-21s", ChassisId.pu1_OctetList);
        }
    }
    /* Allocate Memory for Temp Remote Node */
    if ((pTempRemNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1ChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispPortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }

        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowNeighborBriefInfo: Failed to Allocate Memory "
                  "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }
    i4RemNodeAllocCnt++;

    MEMSET (pTempRemNode, 0, sizeof (tLldpRemoteNode));

    /* Get the pointer to the Remote Node with the given indices */
    pTempRemNode->u4RemLastUpdateTime = u4RemTimeMark;
    pTempRemNode->i4RemLocalPortNum = i4RemLocalPortNum;
    pTempRemNode->i4RemIndex = i4RemIndex;
    pTempRemNode->u4DestAddrTblIndex = u4RemDestMacAddr;

    /* No MIB object is present for LLDP Holdtime (u4RxTTL).
     * Hence RBTreeGet routine is used here. */
    pRemNode =
        (tLldpRemoteNode *) RBTreeGet (gLldpGlobalInfo.RemSysDataRBTree,
                                       (tRBElem *) pTempRemNode);

    if (LldpMemRelease (gLldpGlobalInfo.RemNodePoolId, i4RemNodeAllocCnt,
                        pTempRemNode) == LLDP_FALSE)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "RemNodePoolId Memory release failed!!. \r\n");
    }

    if (pRemNode == NULL)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1ChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispPortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }

        return CLI_FAILURE;
    }

    u4RxTTL = pRemNode->u4RxTTL;

    CfaCliGetIfName ((UINT4) i4RemLocalPortNum, pi1IfName);
    CliPrintf (CliHandle, "%-14s", pi1IfName);
    CliPrintf (CliHandle, "%-12d", u4RxTTL);

    nmhGetLldpV2RemSysCapSupported (u4RemTimeMark,
                                    i4RemLocalPortNum,
                                    u4RemDestMacAddr,
                                    (UINT4) i4RemIndex, &SysCapSupported);

    if (SysCapSupported.pu1_OctetList[0] == 0)
    {
        /* Space alignment for system capabilities TLV */
        CliPrintf (CliHandle, "%-16s", "");
    }
    else
    {
        /* Shows Supported System Capabilities */
        LldpCliShowSysSupportCapab (CliHandle, SysCapSupported);
    }
    nmhGetLldpV2RemPortIdSubtype (u4RemTimeMark,
                                  i4RemLocalPortNum,
                                  u4RemDestMacAddr,
                                  (UINT4) i4RemIndex, &i4PortIdSubtype);
    if (nmhGetLldpV2RemPortId (u4RemTimeMark,
                               i4RemLocalPortNum,
                               u4RemDestMacAddr,
                               (UINT4) i4RemIndex, &PortId) != SNMP_SUCCESS)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1ChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispPortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }

        return CLI_SUCCESS;
    }
    if (i4PortIdSubtype == LLDP_PORT_ID_SUB_MAC_ADDR)
    {
        MEMSET (pu1DispPortId, 0, LLDP_MAX_LEN_PORTID + 1);

        /* Convert octet values into strings of octets
         * seperated by colon */
        CLI_CONVERT_MAC_TO_DOT_STR (PortId.pu1_OctetList, pu1DispPortId);
        CliPrintf (CliHandle, "%s", pu1DispPortId);
    }
    else if (i4PortIdSubtype == LLDP_PORT_ID_SUB_NW_ADDR)
    {
        MEMSET (pu1DispPortId, 0, LLDP_MAX_LEN_PORTID + 1);
        /* To skip the IANA Address Family Numbers */
        PortId.pu1_OctetList++;
        PTR_FETCH4 (u4PortIdNwAddr, PortId.pu1_OctetList);
        pc1DispPortIdNwAddr = (CHR1 *) pu1DispPortId;
        /* Convert octet values into strings of octets seperated by dot */
        CLI_CONVERT_IPADDR_TO_STR (pc1DispPortIdNwAddr, u4PortIdNwAddr);
        CliPrintf (CliHandle, "%s", pc1DispPortIdNwAddr);
    }
    else if (STRLEN (PortId.pu1_OctetList) >= LLDP_CLI_PORT_DISP_LEN)
    {
        MEMSET (pu1DispPortId, 0, LLDP_MAX_LEN_PORTID + 1);
        STRNCPY (pu1DispPortId, PortId.pu1_OctetList, LLDP_CLI_PORT_DISP_LEN);
        CliPrintf (CliHandle, "%s...", pu1DispPortId);
    }
    else
    {
        CliPrintf (CliHandle, "%s", PortId.pu1_OctetList);
    }
    CliPrintf (CliHandle, "\r\n");

    if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                            pu1ChassisId) == LLDP_FALSE)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                  ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                  "failed!!. \r\n");
    }
    if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                            pu1DispChassisId) == LLDP_FALSE)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                  ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                  "failed!!. \r\n");
    }
    if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                            pu1PortId) == LLDP_FALSE)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                  ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                  "failed!!. \r\n");
    }
    if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                            pu1DispPortId) == LLDP_FALSE)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                  ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                  "failed!!. \r\n");
    }

    return CLI_SUCCESS;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowNeighborDetailInfo
 *
 *     DESCRIPTION      : Displays Detailed information about neighbors learnt
 *                        on an interface or all interfaces
 *
 *     INPUT            : CliHandle         - CliContext ID
 *                        u4RemTimeMark     - Remote Node Time Mark
 *                        i4RemLocalPortNum - Local port where remote
 *                                            information is received
 *                        i4RemIndex        - Remote Node Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/

PRIVATE INT4
LldpCliShowNeighborDetailInfo (tCliHandle CliHandle,
                               UINT4 u4RemTimeMark,
                               INT4 i4RemLocalPortNum, INT4 i4RemIndex,
                               UINT4 u4RemDestMacAddr)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1              *pu1ChassisId = NULL;
    UINT1              *pu1PortId = NULL;
    UINT1              *pu1PortDesc = NULL;
    UINT1              *pu1SysName = NULL;
    UINT1              *pu1SysDesc = NULL;
    UINT1              *pu1DispChassisId = NULL;
    UINT1               au1RemAdvtCap[LLDP_MAX_LEN_ADVT_CAPAB + 1];
    CHR1               *pc1DispChassIdNwAddr = NULL;
    CHR1               *pc1DispPortIdNwAddr = NULL;
    UINT1              *pu1DispPortId = NULL;
    UINT1               u1LAStatus;
    UINT2               u2SysCapSupported;
    UINT2               u2SysCapEnabled;
    UINT2               u2PhyMediaCap;
    UINT4               u4ChassIdNwAddr = 0;
    UINT4               u4PortIdNwAddr = 0;
    UINT4               u4RemainingTime = 0;
    INT1               *pi1IfName = NULL;
    INT4                i4PortIdSubtype = 0;
    UINT4               u4VidTxStatus = 0;
    UINT4               u4ManVidTxStatus = 0;
    INT4                i4AutoNegEnabled = 0;
    INT4                i4AutoNegSupported = 0;
    UINT4               u4OperMauType = 0;
    UINT4               u4PVId = 0;
    UINT4               u4MaxFrameSize = 0;
    UINT4               u4AggPortId = 0;
    INT4                i4ChassisIdSubtype = 0;
    INT4                i4ArrayAllocCnt = 0;
    INT4                i4RemNodeAllocCnt = 0;
    tLldpRemoteNode    *pTempRemNode = NULL;
    tLldpRemoteNode    *pRemNode = NULL;
    tSNMP_OCTET_STRING_TYPE PortId;
    tSNMP_OCTET_STRING_TYPE PortDesc;
    tSNMP_OCTET_STRING_TYPE ChassisId;
    tSNMP_OCTET_STRING_TYPE SysName;
    tSNMP_OCTET_STRING_TYPE SysDesc;
    tSNMP_OCTET_STRING_TYPE SysCapSupported;
    tSNMP_OCTET_STRING_TYPE SysCapEnabled;
    tSNMP_OCTET_STRING_TYPE PhyMediaCap;
    tSNMP_OCTET_STRING_TYPE LAStatus;
    tMacAddr            MacAddr;

    /* Allocate Memory for Chassis */
    if ((pu1ChassisId = (UINT1 *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpArrayInfoPoolId))) == NULL)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowNeighborDetailInfo: Failed to Allocate Memory"
                  "for Chassis \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }
    i4ArrayAllocCnt++;

    /* Allocate Memory for DispChassisId */
    if ((pu1DispChassisId = (UINT1 *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpArrayInfoPoolId))) == NULL)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1ChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }

        LLDP_TRC (LLDP_MANDATORY_TLV_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowNeighborDetailInfo: Failed to Allocate Memory"
                  "for DispChassis \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }
    i4ArrayAllocCnt++;

    /* Allocate Memory for PortId */
    if ((pu1PortId = (UINT1 *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpArrayInfoPoolId))) == NULL)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1ChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }

        LLDP_TRC (LLDP_MANDATORY_TLV_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowNeighborDetailInfo: Failed to Allocate Memory"
                  "for Port \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }
    i4ArrayAllocCnt++;

    /* Allocate Memory for DispPortId */
    if ((pu1DispPortId = (UINT1 *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpArrayInfoPoolId))) == NULL)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1ChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }

        LLDP_TRC (LLDP_MANDATORY_TLV_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowNeighborDetailInfo: Failed to Allocate Memory"
                  "for DispPort \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }
    i4ArrayAllocCnt++;

    /* Allocate Memory for PortDesc */
    if ((pu1PortDesc = (UINT1 *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpArrayInfoPoolId))) == NULL)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1ChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispPortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }

        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowNeighborDetailInfo: Failed to Allocate Memory "
                  "for PortDesc \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }
    i4ArrayAllocCnt++;

    /* Allocate Memory for SysName */
    if ((pu1SysName = (UINT1 *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpArrayInfoPoolId))) == NULL)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1ChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispPortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortDesc) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }

        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowNeighborDetailInfo: Failed to Allocate Memory"
                  "for SysName \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }
    i4ArrayAllocCnt++;

    /* Allocate Memory for SysDesc */
    if ((pu1SysDesc = (UINT1 *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpArrayInfoPoolId))) == NULL)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1ChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispPortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortDesc) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1SysName) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }

        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowNeighborDetailInfo: Failed to Allocate Memory "
                  "for SysDesc \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }
    i4ArrayAllocCnt++;

    MEMSET (&ChassisId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PortId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PortDesc, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&SysName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&SysDesc, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&SysCapSupported, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&SysCapEnabled, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PhyMediaCap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&LAStatus, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&MacAddr, 0, sizeof (tMacAddr));

    MEMSET (pu1ChassisId, 0, LLDP_MAX_LEN_CHASSISID + 1);
    MEMSET (pu1PortId, 0, LLDP_MAX_LEN_PORTID + 1);
    MEMSET (pu1PortDesc, 0, LLDP_MAX_LEN_PORTDESC + 1);
    MEMSET (pu1SysName, 0, LLDP_MAX_LEN_SYSNAME + 1);
    MEMSET (pu1SysDesc, 0, LLDP_MAX_LEN_SYSDESC + 1);
    MEMSET (au1RemAdvtCap, 0, LLDP_MAX_LEN_ADVT_CAPAB + 1);

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = (INT1 *) au1IfName;

    ChassisId.pu1_OctetList = pu1ChassisId;
    PortId.pu1_OctetList = pu1PortId;
    PortDesc.pu1_OctetList = pu1PortDesc;
    SysName.pu1_OctetList = pu1SysName;
    SysDesc.pu1_OctetList = pu1SysDesc;
    SysCapSupported.pu1_OctetList = (UINT1 *) &u2SysCapSupported;
    SysCapSupported.i4_Length = sizeof (u2SysCapSupported);
    SysCapEnabled.pu1_OctetList = (UINT1 *) &u2SysCapEnabled;
    SysCapEnabled.i4_Length = sizeof (u2SysCapEnabled);
    PhyMediaCap.pu1_OctetList = (UINT1 *) &u2PhyMediaCap;
    PhyMediaCap.i4_Length = sizeof (u2PhyMediaCap);
    LAStatus.pu1_OctetList = &u1LAStatus;
    LAStatus.i4_Length = sizeof (u1LAStatus);

    nmhGetLldpV2RemChassisIdSubtype (u4RemTimeMark,
                                     i4RemLocalPortNum,
                                     u4RemDestMacAddr,
                                     (UINT4) i4RemIndex, &i4ChassisIdSubtype);
    CliPrintf (CliHandle, "Chassis Id SubType            : ");
    LldpCliShowChassisIdSubType (CliHandle, i4ChassisIdSubtype);
    if (nmhGetLldpV2RemChassisId (u4RemTimeMark,
                                  i4RemLocalPortNum,
                                  u4RemDestMacAddr,
                                  i4RemIndex, &ChassisId) != SNMP_SUCCESS)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1ChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispPortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortDesc) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1SysName) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1SysDesc) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }

        return CLI_SUCCESS;
    }
    if (i4ChassisIdSubtype == LLDP_CHASS_ID_SUB_MAC_ADDR)
    {
        MEMSET (pu1DispChassisId, 0, LLDP_MAX_LEN_CHASSISID + 1);

        /* Convert octet values into strings of octets seperated by colon */
        CLI_CONVERT_MAC_TO_DOT_STR (pu1ChassisId, pu1DispChassisId);
        CliPrintf (CliHandle, "Chassis Id                    : %s\r\n",
                   pu1DispChassisId);
    }
    else if (i4ChassisIdSubtype == LLDP_CHASS_ID_SUB_NW_ADDR)
    {
        MEMSET (pu1DispChassisId, 0, LLDP_MAX_LEN_CHASSISID + 1);
        /* To skip the IANA Address Family Numbers */
        ChassisId.pu1_OctetList++;
        PTR_FETCH4 (u4ChassIdNwAddr, ChassisId.pu1_OctetList);
        pc1DispChassIdNwAddr = (CHR1 *) pu1DispChassisId;
        /* Convert octet values into strings of octets seperated by dot */
        CLI_CONVERT_IPADDR_TO_STR (pc1DispChassIdNwAddr, u4ChassIdNwAddr);
        CliPrintf (CliHandle, "Chassis Id                    : %s\r\n",
                   pc1DispChassIdNwAddr);
    }
    else
    {
        CliPrintf (CliHandle, "Chassis Id                    : %s\r\n",
                   pu1ChassisId);
    }

    nmhGetLldpV2RemPortIdSubtype (u4RemTimeMark,
                                  i4RemLocalPortNum,
                                  u4RemDestMacAddr,
                                  i4RemIndex, &i4PortIdSubtype);
    CliPrintf (CliHandle, "Port Id SubType               : ");
    LldpCliShowPortIdSubType (CliHandle, i4PortIdSubtype);
    if (nmhGetLldpV2RemPortId (u4RemTimeMark,
                               i4RemLocalPortNum,
                               u4RemDestMacAddr,
                               (UINT4) i4RemIndex, &PortId) != SNMP_SUCCESS)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1ChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispPortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortDesc) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1SysName) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1SysDesc) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        return CLI_SUCCESS;
    }
    if (i4PortIdSubtype == LLDP_PORT_ID_SUB_MAC_ADDR)
    {

        MEMSET (pu1DispPortId, 0, LLDP_MAX_LEN_PORTID + 1);

        /* Convert octet values into strings of octets seperated by colon */
        CLI_CONVERT_MAC_TO_DOT_STR (pu1PortId, pu1DispPortId);
        CliPrintf (CliHandle, "Port Id                       : %s\r\n",
                   pu1DispPortId);
    }
    else if (i4PortIdSubtype == LLDP_PORT_ID_SUB_NW_ADDR)
    {
        MEMSET (pu1DispPortId, 0, LLDP_MAX_LEN_PORTID + 1);
        /* To skip the IANA Address Family Numbers */
        PortId.pu1_OctetList++;
        PTR_FETCH4 (u4PortIdNwAddr, PortId.pu1_OctetList);
        pc1DispPortIdNwAddr = (CHR1 *) pu1DispPortId;
        /* Convert octet values into strings of octets seperated by dot */
        CLI_CONVERT_IPADDR_TO_STR (pc1DispPortIdNwAddr, u4PortIdNwAddr);
        CliPrintf (CliHandle, "Port Id                       : %s\r\n",
                   pc1DispPortIdNwAddr);
    }
    else
    {
        CliPrintf (CliHandle, "Port Id                       : %s\r\n",
                   pu1PortId);
    }
    nmhGetLldpV2RemPortDesc (u4RemTimeMark,
                             i4RemLocalPortNum, u4RemDestMacAddr, i4RemIndex,
                             &PortDesc);

    if (STRCMP (PortDesc.pu1_OctetList, "") == 0)
    {

        CliPrintf (CliHandle, "Port Description              : "
                   "Not Advertised\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Port Description              : %s\r\n",
                   PortDesc.pu1_OctetList);
    }

    nmhGetLldpV2RemSysName (u4RemTimeMark,
                            i4RemLocalPortNum, u4RemDestMacAddr, i4RemIndex,
                            &SysName);

    if (STRCMP (SysName.pu1_OctetList, "") == 0)
    {
        CliPrintf (CliHandle, "System Name                   : "
                   "Not Advertised\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "System Name                   : %s\r\n",
                   SysName.pu1_OctetList);
    }
    nmhGetLldpV2RemSysDesc (u4RemTimeMark,
                            i4RemLocalPortNum, u4RemDestMacAddr, i4RemIndex,
                            &SysDesc);
    if (STRCMP (SysDesc.pu1_OctetList, "") == 0)
    {
        CliPrintf (CliHandle, "System Desc                   : "
                   "Not Advertised\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "System Desc                   : %s\r\n",
                   SysDesc.pu1_OctetList);
    }
    CfaCliGetIfName ((UINT4) i4RemLocalPortNum, pi1IfName);
    CliPrintf (CliHandle, "Local Intf                    : %s\r\n", pi1IfName);

    if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                            pu1ChassisId) == LLDP_FALSE)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                  ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                  "failed!!. \r\n");
    }
    if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                            pu1DispChassisId) == LLDP_FALSE)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                  ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                  "failed!!. \r\n");
    }
    if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                            pu1PortId) == LLDP_FALSE)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                  ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                  "failed!!. \r\n");
    }
    if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                            pu1DispPortId) == LLDP_FALSE)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                  ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                  "failed!!. \r\n");
    }
    if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                            pu1PortDesc) == LLDP_FALSE)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                  ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                  "failed!!. \r\n");
    }
    if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                            pu1SysName) == LLDP_FALSE)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                  ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                  "failed!!. \r\n");
    }
    if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                            pu1SysDesc) == LLDP_FALSE)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                  ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                  "failed!!. \r\n");
    }

    /* Allocate Memory for Temp Remote Node */
    if ((pTempRemNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowNeighborDetailInfo: Failed to Allocate Memory"
                  "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }
    i4RemNodeAllocCnt++;
    MEMSET (pTempRemNode, 0, sizeof (tLldpRemoteNode));

    /* Get the pointer to the Remote Node with the given indices */
    pTempRemNode->u4RemLastUpdateTime = u4RemTimeMark;
    pTempRemNode->i4RemLocalPortNum = i4RemLocalPortNum;
    pTempRemNode->i4RemIndex = i4RemIndex;
    pTempRemNode->u4DestAddrTblIndex = u4RemDestMacAddr;

    /* No MIB object is present for LLDP Ageout Time Info.
     * Hence, RBTreeGet routine is used here. */
    pRemNode =
        (tLldpRemoteNode *) RBTreeGet (gLldpGlobalInfo.RemSysDataRBTree,
                                       (tRBElem *) pTempRemNode);

    if (LldpMemRelease (gLldpGlobalInfo.RemNodePoolId, i4RemNodeAllocCnt,
                        pTempRemNode) == LLDP_FALSE)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "RemNodePoolId Memory release failed!!. \r\n");
    }

    LLDP_CHK_NULL_PTR_RET (pRemNode, CLI_FAILURE);

    if (pRemNode->RxInfoAgeTmrNode.u1Status == LLDP_TMR_RUNNING)
    {
        if (TmrGetRemainingTime (gLldpGlobalInfo.TmrListId,
                                 &(pRemNode->RxInfoAgeTmr.TimerNode),
                                 &u4RemainingTime) != TMR_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        u4RemainingTime = (u4RemainingTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
    }
#ifdef L2RED_WANTED
    else
    {
        if (LLDP_RED_NODE_STATUS () == RM_STANDBY)
        {
            u4RemainingTime = (pRemNode->u4RxInfoAgeTmrExpTime -
                               UtlGetTimeSinceEpoch ());
        }
    }
#endif
    CliPrintf (CliHandle, "Time Remaining                : %d Seconds \r\n",
               u4RemainingTime);
    nmhGetLldpV2RemSysCapSupported (u4RemTimeMark,
                                    i4RemLocalPortNum,
                                    u4RemDestMacAddr,
                                    i4RemIndex, &SysCapSupported);

    if (STRCMP (SysCapSupported.pu1_OctetList, "") == 0)
    {
        CliPrintf (CliHandle, "System Capabilities Tlv       : "
                   "Not Advertised\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "System Capabilities Supported : ");
        /* Shows Supported System Capabilities */
        LldpCliShowSysSupportCapab (CliHandle, SysCapSupported);
        CliPrintf (CliHandle, "\r\n");
        nmhGetLldpV2RemSysCapEnabled (u4RemTimeMark,
                                      i4RemLocalPortNum,
                                      u4RemDestMacAddr,
                                      i4RemIndex, &SysCapEnabled);
        CliPrintf (CliHandle, "System Capabilities Enabled   : ");
        /* Shows Enabled System Capabilities */
        LldpCliShowSysEnabledCapab (CliHandle, SysCapEnabled);
        CliPrintf (CliHandle, "\r\n");
    }
    /* Shows Remote Management Address */
    LldpCliShowRemManAddr (CliHandle, u4RemTimeMark, i4RemLocalPortNum,
                           i4RemIndex, u4RemDestMacAddr);
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, "Extended 802.3 TLV Info \r\n");
    CliPrintf (CliHandle, "-MAC PHY Configuration & Status\r\n");

    nmhGetLldpV2DestMacAddress (u4RemDestMacAddr, &MacAddr);

    nmhGetLldpV2Xdot3RemPortAutoNegSupported
        (u4RemTimeMark, i4RemLocalPortNum, u4RemDestMacAddr,
         i4RemIndex, &i4AutoNegSupported);
    if (i4AutoNegSupported == OSIX_FALSE)
    {
        CliPrintf (CliHandle, "Auto Negotiation Tlv          : "
                   "Not Advertised\r\n");
    }
    else
    {
        nmhGetLldpV2Xdot3RemPortAutoNegEnabled (u4RemTimeMark,
                                                i4RemLocalPortNum,
                                                u4RemDestMacAddr, i4RemIndex,
                                                &i4AutoNegEnabled);

        nmhGetLldpV2Xdot3RemPortAutoNegAdvertisedCap (u4RemTimeMark,
                                                      i4RemLocalPortNum,
                                                      u4RemDestMacAddr,
                                                      i4RemIndex, &PhyMediaCap);
        /* Shows Auto Negotiation Support & Status */
        if (i4AutoNegEnabled != 0)
        {
            CliPrintf (CliHandle, "Auto-Neg Support & Status     : %s, %s\r\n",
                       gapc1Support[i4AutoNegSupported],
                       gapc1Enable[i4AutoNegEnabled]);
            CliOctetToHexStr (PhyMediaCap.pu1_OctetList, PhyMediaCap.i4_Length,
                              au1RemAdvtCap);
            /* Shows Auto Negotiation Advertised Capabilities */
            CliPrintf (CliHandle, "Advertised Capability Bits    : %s\r\n",
                       au1RemAdvtCap);

        }

        /* Shows Auto Negotiation Advertised Capabilities */
        LldpCliShowAutoNegAdvCap (CliHandle, &PhyMediaCap);
        nmhGetLldpV2Xdot3RemPortOperMauType (u4RemTimeMark, i4RemLocalPortNum,
                                             u4RemDestMacAddr, i4RemIndex,
                                             &u4OperMauType);
        if (u4OperMauType != 0)
        {
            CliPrintf (CliHandle, "Operational MAU Type          : %d\r\n",
                       u4OperMauType);
        }
    }
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
    {
        CliPrintf (CliHandle, "-Link Aggregation\r\n");
        nmhGetLldpV2Xdot1RemLinkAggPortId
            (u4RemTimeMark, i4RemLocalPortNum, u4RemDestMacAddr, i4RemIndex,
             &u4AggPortId);
        if (u4AggPortId == (UINT4) -1)
        {
            CliPrintf (CliHandle, "Link Aggregation Tlv          : "
                       "Not Advertised\r\n");
        }
        else
        {
            if (nmhGetLldpV2Xdot1RemLinkAggStatus
                (u4RemTimeMark, i4RemLocalPortNum, u4RemDestMacAddr, i4RemIndex,
                 &LAStatus) != SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }

            CliPrintf (CliHandle, "Capability & Status           : ");
            if (u1LAStatus & LLDP_AGG_CAP_BMAP)
            {
                CliPrintf (CliHandle, "Capable, ");
            }
            else
            {
                CliPrintf (CliHandle, "Not Capable, ");
            }
            if (u1LAStatus & LLDP_AGG_STATUS_BMAP)
            {
                CliPrintf (CliHandle, "In Aggregation\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "Not In Aggregation\r\n");
            }
            CliPrintf (CliHandle, "Aggregated Port Id            : %d\r\n",
                       u4AggPortId);
        }
    }
    if (nmhGetLldpV2Xdot3RemMaxFrameSize
        (u4RemTimeMark, i4RemLocalPortNum, u4RemDestMacAddr, i4RemIndex,
         &u4MaxFrameSize) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "-Maximum Frame Size Tlv       : "
                   "Not Advertised\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "-Maximum Frame Size           : %d\r\n",
                   u4MaxFrameSize);
    }
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, "Extended 802.1 TLV Info \r\n");
    if (nmhGetLldpV2Xdot1RemPortVlanId
        (u4RemTimeMark, i4RemLocalPortNum, u4RemDestMacAddr, i4RemIndex,
         &u4PVId) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "-Port VLAN Tlv                : "
                   "Not Advertised\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "-Port VLAN Id                 : %d\r\n", u4PVId);
    }
    CliPrintf (CliHandle, "-Port & Protocol VLAN Id \r\n");
    LldpCliShowRemProtoVlanInfo (CliHandle, u4RemTimeMark, i4RemLocalPortNum,
                                 i4RemIndex);
    CliPrintf (CliHandle, "-Vlan Name\r\n");
    LldpCliShowRemVlanNameInfo (CliHandle, u4RemTimeMark, i4RemLocalPortNum,
                                i4RemIndex);
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_09)
    {
        CliPrintf (CliHandle, "-Link Aggregation\r\n");
        if (nmhGetLldpV2Xdot1RemLinkAggPortId
            (u4RemTimeMark, i4RemLocalPortNum, u4RemDestMacAddr, i4RemIndex,
             &u4AggPortId) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Link Aggregation Tlv          : "
                       "Not Advertised\r\n");
        }
        else
        {
            if (nmhGetLldpV2Xdot1RemLinkAggStatus
                (u4RemTimeMark, i4RemLocalPortNum, u4RemDestMacAddr, i4RemIndex,
                 &LAStatus) != SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }

            CliPrintf (CliHandle, "Capability & Status           : ");
            if (u1LAStatus & LLDP_AGG_CAP_BMAP)
            {
                CliPrintf (CliHandle, "Capable, ");
            }
            else
            {
                CliPrintf (CliHandle, "Not Capable, ");
            }
            if (u1LAStatus & LLDP_AGG_STATUS_BMAP)
            {
                CliPrintf (CliHandle, "In Aggregation\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "Not In Aggregation\r\n");
            }
            CliPrintf (CliHandle, "Aggregated Port Id            : %d\r\n",
                       u4AggPortId);
        }
        if (nmhGetLldpV2Xdot1RemVidUsageDigest
            (u4RemTimeMark, i4RemLocalPortNum, u4RemDestMacAddr,
             &u4VidTxStatus) == SNMP_SUCCESS)
        {
            if (u4VidTxStatus != 0)
            {
                CliPrintf (CliHandle, "-VID TLV                      : %x\r\n",
                           u4VidTxStatus);
            }
            else
            {

                CliPrintf (CliHandle, "-VID TLV                      : "
                           "Not Advertised\r\n");
            }
        }
        if (nmhGetLldpV2Xdot1RemManVid
            (u4RemTimeMark, i4RemLocalPortNum, u4RemDestMacAddr,
             &u4ManVidTxStatus) == SNMP_SUCCESS)
        {
            if (u4ManVidTxStatus != 0)
            {
                CliPrintf (CliHandle, "-Management Vid TLV           : %d\r\n",
                           u4ManVidTxStatus);
            }
            else
            {
                CliPrintf (CliHandle, "-Management Vid TLV           : "
                           "Not Advertised\r\n");
            }
        }
    }

    /* Lldp Med */
    LldpMedCliShowRemCapabilitiesTable (CliHandle,
                                        u4RemTimeMark,
                                        i4RemLocalPortNum,
                                        u4RemDestMacAddr, i4RemIndex);

    LldpMedCliShowRemInventoryTable (CliHandle,
                                     u4RemTimeMark,
                                     i4RemLocalPortNum,
                                     u4RemDestMacAddr, i4RemIndex);

    LldpMedCliShowRemPolicyTable (CliHandle,
                                  u4RemTimeMark,
                                  i4RemLocalPortNum,
                                  u4RemDestMacAddr, i4RemIndex);

    LldpMedCliShowRemLocationInfo (CliHandle,
                                   u4RemTimeMark,
                                   i4RemLocalPortNum,
                                   u4RemDestMacAddr, i4RemIndex);

    LldpMedCliShowRemPowerInfo (CliHandle,
                                u4RemTimeMark,
                                i4RemLocalPortNum,
                                u4RemDestMacAddr, i4RemIndex);

    CliPrintf (CliHandle, "--------------------------------------"
               "----------------------\r\n");
    return CLI_SUCCESS;
}

 /**************************************************************************
 *     FUNCTION NAME    : LldpMedCliShowRemCapabilitiesTable
 *
 *     DESCRIPTION      : Displays the Capabilities of Remote Table
 *
 *     INPUT            : CliHandle          - CliContext ID
 *                        u4RemTimeMark      - Time Mark
 *                        i4RemLocalPortNum  - Interface Index
 *                        u4RemDestMacAddr   - Destination Mac Address Index of
 *                        the agent
 *                        i4RemIndex         - Remote Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 ***************************************************************************/
PUBLIC VOID
LldpMedCliShowRemCapabilitiesTable (tCliHandle CliHandle,
                                    UINT4 u4RemTimeMark,
                                    INT4 i4RemLocalPortNum,
                                    UINT4 u4RemDestMacAddr, INT4 i4RemIndex)
{

    tSNMP_OCTET_STRING_TYPE RemCapSupported;
    tLldpOctetStringType DispRemCapSupported;
    tLldpOctetStringType RemCapSupportedTemp;
    tSNMP_OCTET_STRING_TYPE RemCapCurrent;
    tLldpOctetStringType DispRemCapCurrent;
    tLldpOctetStringType RemCapCurrentTemp;
    UINT1               au1RemCapSupported[LLDP_MED_MAX_LEN_TLV_TXENABLE];
    UINT1               au1DispRemCapSupported[LLDP_MED_MAX_LEN_TLV_TXENABLE];
    UINT1               au1RemCapSupportedTemp[LLDP_MED_MAX_LEN_TLV_TXENABLE];
    UINT1               au1RemCapCurrent[LLDP_MED_MAX_LEN_TLV_TXENABLE];
    UINT1               au1DispRemCapCurrent[LLDP_MED_MAX_LEN_TLV_TXENABLE];
    UINT1               au1RemCapCurrentTemp[LLDP_MED_MAX_LEN_TLV_TXENABLE];
    INT4                i4DeviceClass = 0;
    BOOL1               bCapEnable = LLDP_FALSE;

    MEMSET (&RemCapSupported, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RemCapSupportedTemp, 0, sizeof (tLldpOctetStringType));
    MEMSET (&DispRemCapSupported, 0, sizeof (tLldpOctetStringType));
    MEMSET (&RemCapCurrent, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RemCapCurrentTemp, 0, sizeof (tLldpOctetStringType));
    MEMSET (&DispRemCapCurrent, 0, sizeof (tLldpOctetStringType));

    MEMSET (au1RemCapSupported, 0, LLDP_MED_MAX_LEN_TLV_TXENABLE);
    MEMSET (au1RemCapSupportedTemp, 0, LLDP_MED_MAX_LEN_TLV_TXENABLE);
    MEMSET (au1DispRemCapSupported, 0, LLDP_MED_MAX_LEN_TLV_TXENABLE);
    MEMSET (au1RemCapCurrent, 0, LLDP_MED_MAX_LEN_TLV_TXENABLE);
    MEMSET (au1RemCapCurrentTemp, 0, LLDP_MED_MAX_LEN_TLV_TXENABLE);
    MEMSET (au1DispRemCapCurrent, 0, LLDP_MED_MAX_LEN_TLV_TXENABLE);

    RemCapSupported.pu1_OctetList = au1RemCapSupported;
    RemCapSupported.i4_Length = LLDP_MED_MAX_LEN_TLV_TXENABLE;
    RemCapCurrent.pu1_OctetList = au1RemCapCurrent;
    RemCapCurrent.i4_Length = LLDP_MED_MAX_LEN_TLV_TXENABLE;

    DispRemCapSupported.pu1_OctetList = au1DispRemCapSupported;
    DispRemCapSupported.i4_Length = LLDP_MED_MAX_LEN_TLV_TXENABLE;
    DispRemCapCurrent.pu1_OctetList = au1DispRemCapCurrent;
    DispRemCapCurrent.i4_Length = LLDP_MED_MAX_LEN_TLV_TXENABLE;

    RemCapSupportedTemp.pu1_OctetList = au1RemCapSupportedTemp;
    RemCapSupportedTemp.i4_Length = LLDP_MED_MAX_LEN_TLV_TXENABLE;
    RemCapCurrentTemp.pu1_OctetList = au1RemCapCurrentTemp;
    RemCapCurrentTemp.i4_Length = LLDP_MED_MAX_LEN_TLV_TXENABLE;

    if (nmhGetFsLldpXMedRemCapSupported (u4RemTimeMark,
                                         i4RemLocalPortNum,
                                         u4RemDestMacAddr,
                                         i4RemIndex,
                                         &RemCapSupported) != SNMP_SUCCESS)
    {
        return;
    }
    MEMCPY (DispRemCapSupported.pu1_OctetList,
            RemCapSupported.pu1_OctetList, LLDP_MED_MAX_LEN_TLV_TXENABLE);
    LldpReverseOctetString (&DispRemCapSupported, &RemCapSupportedTemp);
    CliPrintf (CliHandle, "\r\n");

    /* If the Remote Capbilities Table Entries are not present */
    if (*RemCapSupported.pu1_OctetList == 0)
    {
        CliPrintf (CliHandle,
                   "-LLDP MED Capability TLV      : Not Advertised \r\n");
        CliPrintf (CliHandle, "Capabilities Supported        : 0 \r\n");
        CliPrintf (CliHandle, "Capabilities Enabled          : 0 \r\n");
        CliPrintf (CliHandle, "Device Class                  : 0 \r\n");
        return;
    }

    CliPrintf (CliHandle, "-LLDP MED Capability TLV       \r\n");
    CliPrintf (CliHandle, "Capabilities Supported        : ");
    /* check if any supported Capability is there before */
    bCapEnable = LLDP_FALSE;
    /* Check whether LLDP_MED_CAPABILITY_TLV is supported or not */
    if (au1RemCapSupportedTemp[1] & LLDP_MED_CAPABILITY_TLV)
    {
        bCapEnable = LLDP_TRUE;
        CliPrintf (CliHandle, "MedCapability");
    }
    /* Check whether LLDP_MED_NETWORK_POLICY_TLV is supported or not */
    if (au1RemCapSupportedTemp[1] & LLDP_MED_NETWORK_POLICY_TLV)
    {
        /* If any other capability is printed before bCapEnable will 
         * be LLDP_TRUE, then print comma */
        if (bCapEnable == LLDP_TRUE)
        {
            CliPrintf (CliHandle, ", ");
        }
        else
        {
            bCapEnable = LLDP_TRUE;
        }
        CliPrintf (CliHandle, "NetworkPolicy");
    }
    /* Check whether LLDP_MED_LOCATION_ID_TLV is supported or not */
    if (au1RemCapSupportedTemp[1] & LLDP_MED_LOCATION_ID_TLV)
    {
        if (bCapEnable == LLDP_TRUE)
        {
            CliPrintf (CliHandle, ", ");
        }
        else
        {
            bCapEnable = LLDP_TRUE;
        }
        CliPrintf (CliHandle, "LocationIdentity");
    }
    /* Check whether LLDP MED Power via MDI PSE is supported or not */
    if (au1RemCapSupportedTemp[1] & LLDP_MED_PW_MDI_PSE_TLV)
    {
        if (bCapEnable == LLDP_TRUE)
        {
            CliPrintf (CliHandle, ", ");
        }
        else
        {
            bCapEnable = LLDP_TRUE;
        }
        CliPrintf (CliHandle, "Ex-PowerViaMDI-PSE");
    }
    /* Check whether LLDP MED Power via MDI_PD is supported or not */
    if (au1RemCapSupportedTemp[1] & LLDP_MED_PW_MDI_PD_TLV)
    {
        if (bCapEnable == LLDP_TRUE)
        {
            CliPrintf (CliHandle, ", ");
        }
        else
        {
            bCapEnable = LLDP_TRUE;
        }
        CliPrintf (CliHandle, "Ex-PowerViaMDI-PD");
    }
    /* Check whether LLDP-MED INVENTORY MANAGEMENT TLV is supported or not */
    if (au1RemCapSupportedTemp[1] & LLDP_MED_INVENTORY_MGMT_TLV)
    {
        if (bCapEnable == LLDP_TRUE)
        {
            CliPrintf (CliHandle, ", ");
        }
        CliPrintf (CliHandle, "Inventory");
    }
    CliPrintf (CliHandle, "\r\n");

    if (nmhGetFsLldpXMedRemCapCurrent (u4RemTimeMark,
                                       i4RemLocalPortNum,
                                       u4RemDestMacAddr,
                                       i4RemIndex,
                                       &RemCapCurrent) != SNMP_SUCCESS)
    {
        return;
    }
    MEMCPY (DispRemCapCurrent.pu1_OctetList,
            RemCapCurrent.pu1_OctetList, LLDP_MED_MAX_LEN_TLV_TXENABLE);
    LldpReverseOctetString (&DispRemCapCurrent, &RemCapCurrentTemp);

    CliPrintf (CliHandle, "Capabilities Enabled          : ");
    /* Check if any supported Capability is there before */
    bCapEnable = LLDP_FALSE;
    /* Check whether LLDP_MED_CAPABILITY_TLV is enabled or not */
    if (au1RemCapCurrentTemp[1] & LLDP_MED_CAPABILITY_TLV)
    {
        bCapEnable = LLDP_TRUE;
        CliPrintf (CliHandle, "MedCapability");
    }
    /* Check whether LLDP_MED_NETWORK_POLICY_TLV is enabled or not */
    if (au1RemCapCurrentTemp[1] & LLDP_MED_NETWORK_POLICY_TLV)
    {
        /* If any other capability is printed before bCapEnable 
         * will be LLDP_TRUE, then print comma */
        if (bCapEnable == LLDP_TRUE)
        {
            CliPrintf (CliHandle, ", ");
        }
        else
        {
            bCapEnable = LLDP_TRUE;
        }
        CliPrintf (CliHandle, "NetworkPolicy");
    }
    /* Check whether LLDP_MED_LOCATION_ID_TLV is enabled or not */
    if (au1RemCapCurrentTemp[1] & LLDP_MED_LOCATION_ID_TLV)
    {
        if (bCapEnable == LLDP_TRUE)
        {
            CliPrintf (CliHandle, ", ");
        }
        else
        {
            bCapEnable = LLDP_TRUE;
        }
        CliPrintf (CliHandle, "LocationIdentity");
    }
    /* Check whether LLDP MED Power via MDI PSE is enabled or not */
    if (au1RemCapCurrentTemp[1] & LLDP_MED_PW_MDI_PSE_TLV)
    {
        if (bCapEnable == LLDP_TRUE)
        {
            CliPrintf (CliHandle, ", ");
        }
        else
        {
            bCapEnable = LLDP_TRUE;
        }
        CliPrintf (CliHandle, "Ex-PowerViaMDI-PSE");
    }
    /* Check whether LLDP MED Power via MDI_PD is enabled or not */
    if (au1RemCapCurrentTemp[1] & LLDP_MED_PW_MDI_PD_TLV)
    {
        if (bCapEnable == LLDP_TRUE)
        {
            CliPrintf (CliHandle, ", ");
        }
        else
        {
            bCapEnable = LLDP_TRUE;
        }
        CliPrintf (CliHandle, "Ex-PowerViaMDI-PD");
    }
    /* Check whether LLDP-MED INVENTORY MANAGEMENT TLV is enabled or not */
    if (au1RemCapCurrentTemp[1] & LLDP_MED_INVENTORY_MGMT_TLV)
    {
        if (bCapEnable == LLDP_TRUE)
        {
            CliPrintf (CliHandle, ", ");
        }
        CliPrintf (CliHandle, "Inventory");
    }
    CliPrintf (CliHandle, "\r\n");

    if (nmhGetFsLldpXMedRemDeviceClass (u4RemTimeMark,
                                        i4RemLocalPortNum,
                                        u4RemDestMacAddr,
                                        i4RemIndex,
                                        &i4DeviceClass) != SNMP_SUCCESS)
    {
        return;
    }
    /* Check if Device Class is Device class is not defined */
    if (i4DeviceClass == LLDP_MED_NOT_DEF_DEVICE)
    {
        CliPrintf (CliHandle, "Device Class                  : "
                   "Type Not Defined \r\n");
    }
    /* Check if Device Class is End point Class-1 Device */
    else if (i4DeviceClass == LLDP_MED_CLASS_1_DEVICE)
    {
        CliPrintf (CliHandle, "Device Class                  : "
                   "Endpoint Class I \r\n");
    }
    /* Check if Device Class is End point Class-2 Device */
    else if (i4DeviceClass == LLDP_MED_CLASS_2_DEVICE)
    {
        CliPrintf (CliHandle, "Device Class                  : "
                   "Endpoint Class II \r\n");
    }
    /* Check if Device Class is End point Class-3 Device */
    else if (i4DeviceClass == LLDP_MED_CLASS_3_DEVICE)
    {
        CliPrintf (CliHandle, "Device Class                  : "
                   "Endpoint Class III \r\n");
    }
    /* Check if Device Class is Network Connectivity Device */
    else if (i4DeviceClass == LLDP_MED_NW_CONNECTIVITY_DEVICE)
    {
        CliPrintf (CliHandle, "Device Class                  : "
                   "Network Connectivity \r\n");
    }
    return;
}

 /**************************************************************************
 *     FUNCTION NAME    : LldpMedCliShowRemPolicyTable
 *
 *     DESCRIPTION      : Displays the Media Policy Information of Remote Table
 *
 *     INPUT            : CliHandle          - CliContext ID
 *                        u4RemTimeMark      - Time Mark
 *                        i4RemLocalPortNum  - Interface Index
 *                        u4RemDestMacAddr   - Destination Mac 
 *                                             Address Index of the agent
 *                        i4RemIndex         - Remote Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 ***************************************************************************/
PUBLIC VOID
LldpMedCliShowRemPolicyTable (tCliHandle CliHandle,
                              UINT4 u4RemTimeMark,
                              INT4 i4RemLocalPortNum,
                              UINT4 u4RemDestMacAddr, INT4 i4RemIndex)
{
    tSNMP_OCTET_STRING_TYPE RemMediaPolicyAppType;
    tSNMP_OCTET_STRING_TYPE RemNextMediaPolicyAppType;
    UINT4               u4TimeMark = 0;
    UINT4               u4NextRemTimeMark = 0;
    UINT4               u4NextRemDestMacAddr = 0;
    UINT4               u4RemoteMacAddr = 0;
    INT4                i4index = 0;
    INT4                i4NextRemLocalPortNum = 0;
    INT4                i4NextRemIndex = 0;
    INT4                i4RemoteIndex = 0;
    INT4                i4RemUnknownPolicy = 0;
    INT4                i4RemPolicyTagged = 0;
    INT4                i4RemPolicyVlanID = 0;
    INT4                i4RemPolicyPriority = 0;
    INT4                i4RemPolicyDscp = 0;
    INT4                i4Counter = 1;
    UINT1               u1PolicyAppType = 0;
    UINT1               u1NextPolicyAppType = 0;

    MEMSET (&RemMediaPolicyAppType, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RemNextMediaPolicyAppType, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    RemMediaPolicyAppType.pu1_OctetList = &u1PolicyAppType;
    RemMediaPolicyAppType.i4_Length = LLDP_MED_MAX_LEN_APP_TYPE;
    RemNextMediaPolicyAppType.pu1_OctetList = &u1NextPolicyAppType;
    RemNextMediaPolicyAppType.i4_Length = LLDP_MED_MAX_LEN_APP_TYPE;

    CliPrintf (CliHandle, "\r\n");
    if (nmhGetNextIndexFsLldpXMedRemMediaPolicyTable (u4RemTimeMark,
                                                      &u4NextRemTimeMark,
                                                      i4RemLocalPortNum,
                                                      &i4NextRemLocalPortNum,
                                                      u4RemDestMacAddr,
                                                      &u4NextRemDestMacAddr,
                                                      i4RemIndex,
                                                      &i4NextRemIndex,
                                                      &RemMediaPolicyAppType,
                                                      &RemNextMediaPolicyAppType)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "-LLDP-MED Nw Policy TLV       : "
                   "Not Advertised \r\n");

        return;
    }
    CliPrintf (CliHandle, "-LLDP-MED Nw Policy TLV  \r\n");
    do
    {
        /* Assign the Next indices to current to check whether 
         * they are same */
        u4TimeMark = u4NextRemTimeMark;
        i4index = i4NextRemLocalPortNum;
        i4RemoteIndex = i4NextRemIndex;
        u4RemoteMacAddr = u4NextRemDestMacAddr;
        u1PolicyAppType = 0;

        MEMSET (&RemMediaPolicyAppType, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

        RemMediaPolicyAppType.pu1_OctetList = &u1PolicyAppType;
        RemMediaPolicyAppType.i4_Length = LLDP_MED_MAX_LEN_APP_TYPE;

        /* Copy the Next AppType to current */
        MEMCPY (RemMediaPolicyAppType.pu1_OctetList,
                RemNextMediaPolicyAppType.pu1_OctetList, sizeof (UINT1));
        RemMediaPolicyAppType.i4_Length = RemNextMediaPolicyAppType.i4_Length;
        u1NextPolicyAppType = 0;
        /* Intialize the varaible for getting next AppType */
        MEMSET (&RemNextMediaPolicyAppType, 0,
                sizeof (tSNMP_OCTET_STRING_TYPE));
        RemNextMediaPolicyAppType.pu1_OctetList = &u1NextPolicyAppType;
        RemNextMediaPolicyAppType.i4_Length = LLDP_MED_MAX_LEN_APP_TYPE;

        /* Check whether it is of same port */
        if ((i4index == i4RemLocalPortNum) &&
            (u4TimeMark == u4RemTimeMark) &&
            (u4RemDestMacAddr == u4RemoteMacAddr) &&
            (i4RemoteIndex == i4RemIndex))
        {
            CliPrintf (CliHandle, "Network Policy %d\n", i4Counter);
            /* Get the value of Unknown Policy Flag */
            if (nmhGetFsLldpXMedRemMediaPolicyUnknown (u4RemTimeMark,
                                                       i4RemLocalPortNum,
                                                       u4RemoteMacAddr,
                                                       i4RemIndex,
                                                       &RemMediaPolicyAppType,
                                                       &i4RemUnknownPolicy)
                != SNMP_SUCCESS)
            {
                return;
            }
            /* Get the VlanType */
            if (nmhGetFsLldpXMedRemMediaPolicyTagged (u4RemTimeMark,
                                                      i4RemLocalPortNum,
                                                      u4RemoteMacAddr,
                                                      i4RemIndex,
                                                      &RemMediaPolicyAppType,
                                                      &i4RemPolicyTagged)
                != SNMP_SUCCESS)
            {
                return;
            }

            CliPrintf (CliHandle, "Application Type              : ");

            switch (u1PolicyAppType)
            {
                case LLDP_MED_VOICE_APP:

                    CliPrintf (CliHandle, "Voice \r\n");
                    break;

                case LLDP_MED_VOICE_SIGNALING_APP:

                    CliPrintf (CliHandle, "Voice Signaling \r\n");
                    break;

                case LLDP_MED_GUEST_VOICE_APP:

                    CliPrintf (CliHandle, "Guest Voice \r\n");
                    break;

                case LLDP_MED_GUEST_VOICE_SIGNALING_APP:

                    CliPrintf (CliHandle, "Guest Voice Signaling \r\n");
                    break;

                case LLDP_MED_SOFT_PHONE_VOICE_APP:

                    CliPrintf (CliHandle, "Soft Phone Voice \r\n");
                    break;

                case LLDP_MED_VIDEO_CONFERENCING_APP:

                    CliPrintf (CliHandle, "Video Conferencing \r\n");
                    break;

                case LLDP_MED_STREAMING_VIDEO_APP:

                    CliPrintf (CliHandle, "Streaming Video \r\n");
                    break;

                case LLDP_MED_VIDEO_SIGNALING_APP:

                    CliPrintf (CliHandle, "Video Signaling \r\n");
                    break;

                default:
                    break;
            }

            /* Check whether Unknown Policy Flag is enabled or not */
            if ((BOOL1) i4RemUnknownPolicy == LLDP_MED_UNKNOWN_FLAG)
            {
                CliPrintf (CliHandle, "Unknown Policy Flag           : Enabled "
                           "\r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           "Unknown Policy Flag           : Disabled " "\r\n");
            }
            /* Check whether Vlan is Tagged or Untagged */
            if ((BOOL1) i4RemPolicyTagged == LLDP_MED_VLAN_TAGGED)
            {
                CliPrintf (CliHandle, "VlanType                      : Tagged "
                           "\r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           "VlanType                      : UnTagged " "\r\n");
            }

            /* Get the value of VlanId */
            if (nmhGetFsLldpXMedRemMediaPolicyVlanID (u4RemTimeMark,
                                                      i4RemLocalPortNum,
                                                      u4RemoteMacAddr,
                                                      i4RemIndex,
                                                      &RemMediaPolicyAppType,
                                                      &i4RemPolicyVlanID)
                != SNMP_SUCCESS)
            {
                return;
            }
            CliPrintf (CliHandle, "VlanID                        : %d\r\n",
                       i4RemPolicyVlanID);

            /* Get the value of Priority */
            if (nmhGetFsLldpXMedRemMediaPolicyPriority (u4RemTimeMark,
                                                        i4RemLocalPortNum,
                                                        u4RemoteMacAddr,
                                                        i4RemIndex,
                                                        &RemMediaPolicyAppType,
                                                        &i4RemPolicyPriority)
                != SNMP_SUCCESS)
            {
                return;
            }
            CliPrintf (CliHandle, "Priority                      : %d\r\n",
                       i4RemPolicyPriority);

            /* Get the Value of Dscp */
            if (nmhGetFsLldpXMedRemMediaPolicyDscp (u4RemTimeMark,
                                                    i4RemLocalPortNum,
                                                    u4RemoteMacAddr,
                                                    i4RemIndex,
                                                    &RemMediaPolicyAppType,
                                                    &i4RemPolicyDscp)
                != SNMP_SUCCESS)
            {
                return;
            }
            CliPrintf (CliHandle, "Dscp                          : %d\r\n",
                       i4RemPolicyDscp);

            i4Counter++;
        }
    }
    while (nmhGetNextIndexFsLldpXMedRemMediaPolicyTable (u4TimeMark,
                                                         &u4NextRemTimeMark,
                                                         i4index,
                                                         &i4NextRemLocalPortNum,
                                                         u4RemoteMacAddr,
                                                         &u4NextRemDestMacAddr,
                                                         i4RemoteIndex,
                                                         &i4NextRemIndex,
                                                         &RemMediaPolicyAppType,
                                                         &RemNextMediaPolicyAppType)
           == SNMP_SUCCESS);
    return;
}

 /**************************************************************************
 *     FUNCTION NAME    : LldpMedCliShowRemLocationInfo
 *
 *     DESCRIPTION      : Displays the Location Information of Remote Table
 *
 *     INPUT            : CliHandle          - CliContext ID
 *                        u4RemTimeMark      - Time Mark
 *                        i4RemLocalPortNum  - Interface Index
 *                        u4RemDestMacAddr   - Destination Mac
 *                                             Address Index of the agent
 *                        i4RemIndex         - Remote Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 ***************************************************************************/
PUBLIC VOID
LldpMedCliShowRemLocationInfo (tCliHandle CliHandle,
                               UINT4 u4RemTimeMark,
                               INT4 i4RemLocalPortNum,
                               UINT4 u4RemDestMacAddr, INT4 i4RemIndex)
{
    tSNMP_OCTET_STRING_TYPE RemLocationInfo;
    UINT1               au1LocationId[LLDP_MED_MAX_LOC_LENGTH];
    UINT1               au1CaValue[LLDP_MED_MAX_CA_TYPE_LENGTH];
    UINT1               au1CountryCode[3];
    CONST CHR1         *pu1CaType = NULL;
    CONST CHR1         *pu1What = NULL;
    DBL8                d8latitude = 0;
    DBL8                d8longitude = 0;
    DBL8                d8altitude = 0;
    UINT4               u4TimeMark = 0;
    UINT4               u4NextRemTimeMark = 0;
    UINT4               u4NextRemDestMacAddr = 0;
    UINT4               u4RemoteMacAddr = 0;
    INT4                i4index = 0;
    INT4                i4NextRemLocalPortNum = 0;
    INT4                i4NextRemIndex = 0;
    INT4                i4RemoteIndex = 0;
    INT4                i4RemLocSubtype = 0;
    INT4                i4NextRemLocSubtype = 0;
    UINT1               u1latres = 0;
    UINT1               u1longres = 0;
    UINT1               u1altres = 0;
    UINT1               u1alttype = 0;
    UINT1               u1datum = 0;
    UINT1               u1CaLength = 0;
    UINT1               u1LocOffset = 0;

    MEMSET (&au1CaValue, 0, LLDP_MED_MAX_CA_TYPE_LENGTH);
    MEMSET (&au1CountryCode, 0, 3);
    CliPrintf (CliHandle, "\r\n");
    if (nmhGetNextIndexFsLldpXMedRemLocationTable (u4RemTimeMark,
                                                   &u4NextRemTimeMark,
                                                   i4RemLocalPortNum,
                                                   &i4NextRemLocalPortNum,
                                                   u4RemDestMacAddr,
                                                   &u4NextRemDestMacAddr,
                                                   i4RemIndex, &i4NextRemIndex,
                                                   i4RemLocSubtype,
                                                   &i4NextRemLocSubtype) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "-LLDP-MED Location Info TLV   : "
                   "Not Advertised \r\n");

        return;
    }

    CliPrintf (CliHandle, "-LLDP-MED Location Info TLV  \r\n");
    do
    {
        /* Assign the Next indices to current to check whether
         * they are same */
        u4TimeMark = u4NextRemTimeMark;
        i4index = i4NextRemLocalPortNum;
        i4RemoteIndex = i4NextRemIndex;
        u4RemoteMacAddr = u4NextRemDestMacAddr;
        i4RemLocSubtype = i4NextRemLocSubtype;

        MEMSET (&au1LocationId, 0, LLDP_MED_MAX_LOC_LENGTH);
        MEMSET (&RemLocationInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        RemLocationInfo.pu1_OctetList = au1LocationId;
        RemLocationInfo.i4_Length = LLDP_MED_MAX_LOC_LENGTH;

        /* Check whether it is of same port */
        if ((i4index == i4RemLocalPortNum) &&
            (u4TimeMark == u4RemTimeMark) &&
            (u4RemDestMacAddr == u4RemoteMacAddr) &&
            (i4RemoteIndex == i4RemIndex))
        {
            if (nmhGetFsLldpXMedRemLocationInfo (u4RemTimeMark,
                                                 i4RemLocalPortNum,
                                                 u4RemoteMacAddr, i4RemIndex,
                                                 i4RemLocSubtype,
                                                 &RemLocationInfo) !=
                SNMP_SUCCESS)
            {
                return;
            }

            switch (i4RemLocSubtype)
            {
                case LLDP_MED_COORDINATE_LOC:

                    CliPrintf (CliHandle, "Location Subtype              : "
                               "Coordinate Location \r\n");
                    LldpMedUtlCoordinateLocDecode (&u1latres, &d8latitude,
                                                   &u1longres, &d8longitude,
                                                   &u1alttype, &u1altres,
                                                   &d8altitude, &u1datum,
                                                   au1LocationId);
                    CliPrintf (CliHandle,
                               "Latitude Resolution           : " "%d \r\n",
                               u1latres);
                    CliPrintf (CliHandle,
                               "Latitude                      : " "%lf \r\n",
                               d8latitude);

                    CliPrintf (CliHandle, "Longitude Resolution          : "
                               "%d \r\n", u1longres);
                    CliPrintf (CliHandle, "Longitude                     : "
                               "%lf \r\n", d8longitude);
                    if (u1alttype == LLDP_MED_ALT_METER)
                    {
                        CliPrintf (CliHandle, "Altitude Type                 : "
                                   "Meters\r\n");
                    }
                    else if (u1alttype == LLDP_MED_ALT_FLOOR)
                    {
                        CliPrintf (CliHandle, "Altitude Type                 : "
                                   "Floors\r\n");
                    }
                    CliPrintf (CliHandle, "Altitude Resolution           : "
                               "%d \r\n", u1altres);
                    CliPrintf (CliHandle, "Altitude                      : "
                               "%lf \r\n", d8altitude);
                    CliPrintf (CliHandle, "Datum                         : "
                               "%d \r\n", u1datum);

                    break;

                case LLDP_MED_CIVIC_LOC:

                    CliPrintf (CliHandle, "Location Subtype              : "
                               "Civic Location \r\n");
                    CliPrintf (CliHandle, "LCI Length                    : "
                               "%d \r\n", au1LocationId[0]);
                    pu1What = LldpMedUtlCivicWhatToStr (au1LocationId[1]);
                    if (pu1What != NULL)
                    {
                        CliPrintf (CliHandle, "What                          : "
                                   "%s \r\n", pu1What);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "What                          : "
                                   "\r\n");
                    }
                    MEMCPY (&au1CountryCode, &au1LocationId[2], 2);
                    CliPrintf (CliHandle, "Country Code                  : "
                               "%s \r\n", au1CountryCode);
                    MEMSET (&au1CountryCode, 0, 2);
                    u1LocOffset = 4;
                    while (u1LocOffset <= au1LocationId[0])
                    {
                        pu1CaType =
                            LldpMedUtlCaTypeToStr (au1LocationId[u1LocOffset]);
                        if (pu1CaType != NULL)
                        {
                            CliPrintf (CliHandle,
                                       "CA Type                       : "
                                       "%s \r\n", pu1CaType);
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "CA Type                       : "
                                       "\r\n");
                        }
                        u1LocOffset++;
                        u1CaLength = au1LocationId[u1LocOffset];
                        if (u1CaLength != 0)
                        {
                            CliPrintf (CliHandle,
                                       "CA Length                     : "
                                       "%d \r\n", au1LocationId[u1LocOffset++]);

                            MEMCPY (&au1CaValue, &au1LocationId[u1LocOffset],
                                    u1CaLength);
                            CliPrintf (CliHandle,
                                       "CA Value                      : "
                                       "%s \r\n", au1CaValue);
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "CA Length                     : "
                                       "%d \r\n", au1LocationId[u1LocOffset++]);

                            CliPrintf (CliHandle,
                                       "CA Value                      : "
                                       "\r\n");
                        }

                        MEMSET (&au1CaValue, 0, LLDP_MED_MAX_CA_TYPE_LENGTH);
                        u1LocOffset = (UINT1) (u1LocOffset + u1CaLength);
                    }

                    break;

                case LLDP_MED_ELIN_LOC:

                    CliPrintf (CliHandle, "Location Subtype              : "
                               "Elin Location \r\n");

                    CliPrintf (CliHandle, "Elin Location Id              : "
                               "%s \r\n", au1LocationId);
                    break;

                default:
                    break;

            }
        }
    }
    while (nmhGetNextIndexFsLldpXMedRemLocationTable (u4TimeMark,
                                                      &u4NextRemTimeMark,
                                                      i4index,
                                                      &i4NextRemLocalPortNum,
                                                      u4RemoteMacAddr,
                                                      &u4NextRemDestMacAddr,
                                                      i4RemoteIndex,
                                                      &i4NextRemIndex,
                                                      i4RemLocSubtype,
                                                      &i4NextRemLocSubtype) ==
           SNMP_SUCCESS);
    return;
}

 /**************************************************************************
 *     FUNCTION NAME    : LldpMedCliShowRemPowerInfo
 *
 *     DESCRIPTION      : Displays the Power MDI information of Remote Table
 *
 *     INPUT            : CliHandle          - CliContext ID
 *                        u4RemTimeMark      - Time Mark
 *                        i4RemLocalPortNum  - Interface Index
 *                        u4RemDestMacAddr   - Destination Mac Address Index of
 *                        the agent
 *                        i4RemIndex         - Remote Time Mark
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 ***************************************************************************/
PUBLIC VOID
LldpMedCliShowRemPowerInfo (tCliHandle CliHandle,
                            UINT4 u4RemTimeMark,
                            INT4 i4RemLocalPortNum,
                            UINT4 u4RemDestMacAddr, INT4 i4RemIndex)
{
    tSNMP_OCTET_STRING_TYPE RemCapCurrent;
    tLldpOctetStringType RemCapCurrentTemp;
    tLldpOctetStringType DispRemCapCurrent;
    UINT4               u4PowerValue = 0;
    INT4                i4PoEDeviceType = 0;
    INT4                i4PowerPriority = 0;
    INT4                i4PowerSource = 0;
    UINT1               au1RemCapCurrent[LLDP_MED_MAX_LEN_TLV_TXENABLE];
    UINT1               au1DispRemCapCurrent[LLDP_MED_MAX_LEN_TLV_TXENABLE];
    UINT1               au1RemCapCurrentTemp[LLDP_MED_MAX_LEN_TLV_TXENABLE];

    MEMSET (&RemCapCurrent, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&DispRemCapCurrent, 0, sizeof (tLldpOctetStringType));
    MEMSET (&RemCapCurrentTemp, 0, sizeof (tLldpOctetStringType));
    MEMSET (au1RemCapCurrent, 0, LLDP_MED_MAX_LEN_TLV_TXENABLE);
    MEMSET (au1RemCapCurrentTemp, 0, LLDP_MED_MAX_LEN_TLV_TXENABLE);
    RemCapCurrent.pu1_OctetList = au1RemCapCurrent;
    RemCapCurrent.i4_Length = LLDP_MED_MAX_LEN_TLV_TXENABLE;
    DispRemCapCurrent.pu1_OctetList = au1DispRemCapCurrent;
    DispRemCapCurrent.i4_Length = LLDP_MED_MAX_LEN_TLV_TXENABLE;
    RemCapCurrentTemp.pu1_OctetList = au1RemCapCurrentTemp;
    RemCapCurrentTemp.i4_Length = LLDP_MED_MAX_LEN_TLV_TXENABLE;

    if (nmhGetFsLldpXMedRemCapCurrent (u4RemTimeMark,
                                       i4RemLocalPortNum,
                                       u4RemDestMacAddr,
                                       i4RemIndex,
                                       &RemCapCurrent) != SNMP_SUCCESS)
    {
        return;
    }
    MEMCPY (DispRemCapCurrent.pu1_OctetList,
            RemCapCurrent.pu1_OctetList, LLDP_MED_MAX_LEN_TLV_TXENABLE);
    LldpReverseOctetString (&DispRemCapCurrent, &RemCapCurrentTemp);

    CliPrintf (CliHandle, "\r\n");
    if (!(au1RemCapCurrentTemp[1] & LLDP_MED_PW_MDI_PD_TLV))
    {
        CliPrintf (CliHandle, "-LLDP-MED Ex-PowerViaMDI TLV  : "
                   "Not Advertised \r\n");
        return;
    }
    CliPrintf (CliHandle, "-LLDP-MED Ex-PowerViaMDI TLV \r\n");
    /* Get Power Device Type */
    if (nmhGetFsLldpXMedRemXPoEDeviceType (u4RemTimeMark,
                                           i4RemLocalPortNum, u4RemDestMacAddr,
                                           i4RemIndex,
                                           &i4PoEDeviceType) != SNMP_SUCCESS)
    {
        return;
    }
    /* Get Power Source */
    if (nmhGetFsLldpXMedRemXPoEPDPowerSource (u4RemTimeMark,
                                              i4RemLocalPortNum,
                                              u4RemDestMacAddr, i4RemIndex,
                                              &i4PowerSource) != SNMP_SUCCESS)
    {
        return;
    }
    /* Get Power Priority */
    if (nmhGetFsLldpXMedRemXPoEPDPowerPriority (u4RemTimeMark,
                                                i4RemLocalPortNum,
                                                u4RemDestMacAddr, i4RemIndex,
                                                &i4PowerPriority) !=
        SNMP_SUCCESS)
    {
        return;
    }
    /* Get Power Value */
    if (nmhGetFsLldpXMedRemXPoEPDPowerReq (u4RemTimeMark,
                                           i4RemLocalPortNum, u4RemDestMacAddr,
                                           i4RemIndex,
                                           &u4PowerValue) != SNMP_SUCCESS)
    {
        return;
    }

    /* print Power Device Type */
    /* Check for PSE Device */
    if (i4PoEDeviceType == LLDP_MED_PSE)
    {
        CliPrintf (CliHandle, "Power Device Type             : "
                   "PSE Device \r\n");
    }
    /* Check for PD Device */
    else if (i4PoEDeviceType == LLDP_MED_PD)
    {
        CliPrintf (CliHandle, "Power Device Type             : "
                   "PD Device \r\n");
    }

    /* Print Power Source */
    /* Check for Unknown Source */
    if (i4PowerSource == LLDP_MED_PD_UNKNOWN_MIB)
    {
        CliPrintf (CliHandle, "Power Source                  : "
                   "Unknown \r\n");
    }
    /* Check for PSE */
    else if (i4PowerSource == LLDP_MED_PD_PSE_MIB)
    {
        CliPrintf (CliHandle, "Power Source                  : " "PSE \r\n");
    }
    /* Check for Local */
    else if (i4PowerSource == LLDP_MED_PD_LOCAL_MIB)
    {
        CliPrintf (CliHandle, "Power Source                  : " "Local \r\n");
    }
    /* Check for PSE and Local */
    else if (i4PowerSource == LLDP_MED_PD_PSE_LOCAL_MIB)
    {
        CliPrintf (CliHandle, "Power Source                  : "
                   "PSE and Local \r\n");
    }

    /* Print power Priority type */
    if (i4PowerPriority == LLDP_MED_PRI_CRITICAL_MIB)
    {
        CliPrintf (CliHandle, "Power Priority                : "
                   "Critical \r\n");
    }
    else if (i4PowerPriority == LLDP_MED_PRI_HIGH_MIB)
    {
        CliPrintf (CliHandle, "Power Priority                : " "High \r\n");
    }
    else if (i4PowerPriority == LLDP_MED_PRI_LOW_MIB)
    {
        CliPrintf (CliHandle, "Power Priority                : " "Low \r\n");
    }

    /* Print Power Value */
    CliPrintf (CliHandle, "Power Value                   : "
               "%d \r\n", u4PowerValue);
    return;
}

 /**************************************************************************
 *     FUNCTION NAME    : LldpMedCliShowRemInventoryTable
 *
 *     DESCRIPTION      : Displays the Inventory information of Remote Table
 *
 *     INPUT            : CliHandle          - CliContext ID
 *                        u4RemTimeMark      - Time Mark
 *                        i4RemLocalPortNum  - Interface Index
 *                        u4RemDestMacAddr   - Destination Mac Address Index of
 *                        the agent
 *                        i4RemIndex         - Remote Time Mark
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 ***************************************************************************/
PUBLIC VOID
LldpMedCliShowRemInventoryTable (tCliHandle CliHandle,
                                 UINT4 u4RemTimeMark,
                                 INT4 i4RemLocalPortNum,
                                 UINT4 u4RemDestMacAddr, INT4 i4RemIndex)
{
    tSNMP_OCTET_STRING_TYPE InventoryInfo;
    UINT1               au1InventoryInfo[LLDP_MED_HW_REV_LEN];

    MEMSET (&au1InventoryInfo, 0, LLDP_MED_HW_REV_LEN);
    MEMSET (&InventoryInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InventoryInfo.pu1_OctetList = au1InventoryInfo;
    InventoryInfo.i4_Length = 0;
    /* Get the Hardware Revision Details of Remote Table */
    if (nmhGetFsLldpXMedRemHardwareRev (u4RemTimeMark,
                                        i4RemLocalPortNum,
                                        u4RemDestMacAddr,
                                        i4RemIndex,
                                        &InventoryInfo) != SNMP_SUCCESS)
    {
        return;
    }
    CliPrintf (CliHandle, "\r\n");

    /* Check whether the Inventory Info entry is present or not */
    if (*InventoryInfo.pu1_OctetList == 0)
    {
        CliPrintf (CliHandle, "-LLDP MED Inventory TLV       : Not Advertised"
                   "\r\n");
        return;
    }

    CliPrintf (CliHandle, "-LLDP MED Inventory TLV       : \r\n");
    CliPrintf (CliHandle, "Hardware Revision             : %s\r\n",
               au1InventoryInfo);

    MEMSET (&au1InventoryInfo, 0, LLDP_MED_HW_REV_LEN);
    MEMSET (&InventoryInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InventoryInfo.pu1_OctetList = au1InventoryInfo;
    InventoryInfo.i4_Length = 0;

    /* Get the Firmware Revision Details */
    if (nmhGetFsLldpXMedRemFirmwareRev (u4RemTimeMark,
                                        i4RemLocalPortNum,
                                        u4RemDestMacAddr,
                                        i4RemIndex,
                                        &InventoryInfo) != SNMP_SUCCESS)
    {
        return;
    }
    CliPrintf (CliHandle, "Firmware Revision             : %s\r\n",
               au1InventoryInfo);

    MEMSET (&au1InventoryInfo, 0, LLDP_MED_HW_REV_LEN);
    MEMSET (&InventoryInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InventoryInfo.pu1_OctetList = au1InventoryInfo;
    InventoryInfo.i4_Length = 0;
    /* Get the Software Revision Details */
    if (nmhGetFsLldpXMedRemSoftwareRev (u4RemTimeMark,
                                        i4RemLocalPortNum,
                                        u4RemDestMacAddr,
                                        i4RemIndex,
                                        &InventoryInfo) != SNMP_SUCCESS)
    {
        return;
    }
    CliPrintf (CliHandle, "Software Revision             : %s\r\n",
               au1InventoryInfo);

    MEMSET (&au1InventoryInfo, 0, LLDP_MED_HW_REV_LEN);
    MEMSET (&InventoryInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InventoryInfo.pu1_OctetList = au1InventoryInfo;
    InventoryInfo.i4_Length = 0;

    /* Get the Serial Number Details */
    if (nmhGetFsLldpXMedRemSerialNum (u4RemTimeMark,
                                      i4RemLocalPortNum,
                                      u4RemDestMacAddr,
                                      i4RemIndex,
                                      &InventoryInfo) != SNMP_SUCCESS)
    {
        return;
    }
    CliPrintf (CliHandle, "Serial Number                 : %s\r\n",
               au1InventoryInfo);

    MEMSET (&au1InventoryInfo, 0, LLDP_MED_HW_REV_LEN);
    MEMSET (&InventoryInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InventoryInfo.pu1_OctetList = au1InventoryInfo;
    InventoryInfo.i4_Length = 0;

    /* Get the Manufacturer Name details */
    if (nmhGetFsLldpXMedRemMfgName (u4RemTimeMark,
                                    i4RemLocalPortNum,
                                    u4RemDestMacAddr,
                                    i4RemIndex, &InventoryInfo) != SNMP_SUCCESS)
    {
        return;
    }
    CliPrintf (CliHandle, "Manufacturer Name             : %s\r\n",
               au1InventoryInfo);

    MEMSET (&au1InventoryInfo, 0, LLDP_MED_HW_REV_LEN);
    MEMSET (&InventoryInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InventoryInfo.pu1_OctetList = au1InventoryInfo;
    InventoryInfo.i4_Length = 0;

    /* Get the Model Name */
    if (nmhGetFsLldpXMedRemModelName (u4RemTimeMark,
                                      i4RemLocalPortNum,
                                      u4RemDestMacAddr,
                                      i4RemIndex,
                                      &InventoryInfo) != SNMP_SUCCESS)
    {
        return;
    }
    CliPrintf (CliHandle, "Model Name                    : %s\r\n",
               au1InventoryInfo);

    MEMSET (&au1InventoryInfo, 0, LLDP_MED_HW_REV_LEN);
    MEMSET (&InventoryInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InventoryInfo.pu1_OctetList = au1InventoryInfo;
    InventoryInfo.i4_Length = 0;

    /* Get the AssetId */
    if (nmhGetFsLldpXMedRemAssetID (u4RemTimeMark,
                                    i4RemLocalPortNum,
                                    u4RemDestMacAddr,
                                    i4RemIndex, &InventoryInfo) != SNMP_SUCCESS)
    {
        return;
    }
    CliPrintf (CliHandle, "AssetID                       : %s\r\n",
               au1InventoryInfo);

    return;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowRemManAddr
 *
 *     DESCRIPTION      : Displays the remote management address
 *
 *     INPUT            : CliHandle         - CliContext ID
 *                        u4RemTimeMark     - Remote Node Time Mark
 *                        i4RemLocalPortNum - Local port where remote
 *                                            information is received
 *                        i4RemIndex        - Remote Node Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 ***************************************************************************/

PRIVATE VOID
LldpCliShowRemManAddr (tCliHandle CliHandle, UINT4 u4RemTimeMark,
                       INT4 i4RemLocalPortNum, INT4 i4RemIndex,
                       UINT4 u4RemDestMacAddr)
{
    UINT1               au1TempManAddrVal[LLDP_MAX_LEN_MAN_ADDR + 1];
    UINT1               au1PrevTempManAddrVal[LLDP_MAX_LEN_MAN_ADDR];
    UINT1               u1FlagEntryFound = OSIX_FALSE;
    UINT1               au1RemManSubtypeStr[6];
    UINT1               au1DispRemManMacAddr[LLDP_MAX_LEN_MAN_ADDR];
    UINT1               au1DispManAddrOther[LLDP_MAX_LEN_MAN_ADDR + 1];
    CHR1                ac1DispManAddr[LLDP_MAX_LEN_MAN_ADDR];
    CHR1               *pc1DispManAddr = NULL;
    CHR1               *pc1DispManAddrOID = NULL;
    UINT4               u4ManAddr = 0;
    UINT4               u4RemManTimeMark = 0;
    UINT4               u4PrevRemManTimeMark = 0;
    UINT4               u4RemManDestMacAddr = 0;
    UINT4               u4PrevRemManDestMacAddr = 0;
    UINT4               au4TempManAddrOID[LLDP_MAX_LEN_MAN_OID];
    INT4                i4RemManAddrSubtype = 0;
    INT4                i4PrevRemManAddrSubtype = 0;
    INT4                i4RemManLocalPortNum = 0;
    INT4                i4PrevRemManLocalPortNum = 0;
    INT4                i4RemManIndex = 0;
    INT4                i4PrevRemManIndex = 0;
    INT4                i4RemManIfId = 0;
    tSNMP_OID_TYPE      RemManAddrOID;
    tSNMP_OCTET_STRING_TYPE RemManAddr;
    tSNMP_OCTET_STRING_TYPE PrevRemManAddr;

    MEMSET (&au1RemManSubtypeStr[0], 0, 6);
    MEMSET (&RemManAddrOID, 0, sizeof (tSNMP_OID_TYPE));
    MEMSET (&RemManAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PrevRemManAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (au4TempManAddrOID, 0, LLDP_MAX_LEN_MAN_OID * sizeof (UINT4));
    MEMSET (au1TempManAddrVal, 0, LLDP_MAX_LEN_MAN_ADDR + 1);
    MEMSET (au1PrevTempManAddrVal, 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMSET (au1DispManAddrOther, 0, LLDP_MAX_LEN_MAN_ADDR + 1);

    MEMSET (&ac1DispManAddr, 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMSET (&au1DispRemManMacAddr, 0, LLDP_MAX_LEN_MAN_ADDR);

    RemManAddrOID.pu4_OidList = au4TempManAddrOID;
    RemManAddr.pu1_OctetList = au1TempManAddrVal;
    PrevRemManAddr.pu1_OctetList = au1PrevTempManAddrVal;

    if (nmhGetFirstIndexLldpV2RemManAddrTable (&u4RemManTimeMark,
                                               &i4RemManLocalPortNum,
                                               &u4RemManDestMacAddr,
                                               (UINT4 *) &i4RemManIndex,
                                               &i4RemManAddrSubtype,
                                               &RemManAddr) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Management Address            : "
                   "Not Advertised\r\n");
        return;
    }
    CliPrintf (CliHandle, "Management Addresses          : ");
    do
    {
        if (u4RemTimeMark == u4RemManTimeMark &&
            i4RemLocalPortNum == i4RemManLocalPortNum &&
            i4RemIndex == i4RemManIndex &&
            u4RemDestMacAddr == u4RemManDestMacAddr)
        {
            if (u1FlagEntryFound == OSIX_FALSE)
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "%-4s %-7s %-31s %-34s\r\n", "IfId",
                           "SubType", "Address", "OID");
                CliPrintf (CliHandle, "%-4s %-7s %-31s %-34s\r\n", "----",
                           "-------", "-------", "---");
                u1FlagEntryFound = OSIX_TRUE;
            }

            /* Get the RemManIfId */
            nmhGetLldpV2RemManAddrIfId (u4RemManTimeMark, i4RemManLocalPortNum,
                                        u4RemManDestMacAddr,
                                        (UINT4) i4RemManIndex,
                                        i4RemManAddrSubtype, &RemManAddr,
                                        (UINT4 *) &i4RemManIfId);
            nmhGetLldpV2RemManAddrOID (u4RemManTimeMark, i4RemManLocalPortNum,
                                       u4RemManDestMacAddr,
                                       (UINT4) i4RemManIndex,
                                       i4RemManAddrSubtype, &RemManAddr,
                                       &RemManAddrOID);
            /* Allocate Memory for ManAddrOID */
            if ((pc1DispManAddrOID = (CHR1 *)
                 (MemAllocMemBlk (gLldpGlobalInfo.LldpManAddrOidPoolId)))
                == NULL)
            {
                LLDP_TRC (LLDP_CRITICAL_TRC |
                          ALL_FAILURE_TRC, "LldpCliShowRemManAddr: Failed"
                          "to Allocate Memory for ManAddrOID \r\n");
                LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
                return;
            }
            MEMSET (pc1DispManAddrOID, 0, LLDP_MAX_LEN_MAN_OID * 2);
            pc1DispManAddr = &ac1DispManAddr[0];

            /* Converts octet valuse to stings of octets separated by a
             * blank spaces */
            CLI_CONVERT_OID_TO_STR (pc1DispManAddrOID, RemManAddrOID);

            switch (i4RemManAddrSubtype)
            {
                case IPVX_ADDR_FMLY_IPV4:

                    PTR_FETCH4 (u4ManAddr, RemManAddr.pu1_OctetList);
                    SPRINTF ((CHR1 *) au1RemManSubtypeStr, "%s", "IPv4");

                    /* Converts octet values into strings of octets seperated by
                     * dot */
                    CLI_CONVERT_IPADDR_TO_STR (pc1DispManAddr, u4ManAddr);

                    CliPrintf (CliHandle, "%-4d %-7s %-31s %-34s\r\n",
                               i4RemManIfId, au1RemManSubtypeStr,
                               pc1DispManAddr, pc1DispManAddrOID);
                    break;
                case IPVX_ADDR_FMLY_IPV6:

                    SPRINTF ((CHR1 *) au1RemManSubtypeStr, "%s", "IPv6");

                    CliPrintf (CliHandle, "%-4d %-7s %-31s %-34s\r\n",
                               i4RemManIfId, au1RemManSubtypeStr,
                               Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                             au1TempManAddrVal),
                               pc1DispManAddrOID);
                    break;
                case LLDP_CLI_MAN_ADDR_TYPE_MAC:

                    SPRINTF ((CHR1 *) au1RemManSubtypeStr, "%s", "MAC");

                    /* Convert octet values into strings of octets
                     * seperated by colon */
                    CLI_CONVERT_MAC_TO_DOT_STR (RemManAddr.pu1_OctetList,
                                                &au1DispRemManMacAddr[0]);

                    CliPrintf (CliHandle, "%-4d %-7s %-31s %-34s\r\n",
                               i4RemManIfId, au1RemManSubtypeStr,
                               au1DispRemManMacAddr, pc1DispManAddrOID);
                    break;
                default:

                    SPRINTF ((CHR1 *) au1RemManSubtypeStr, "%s", "Other");
                    STRNCPY (au1DispManAddrOther, au1TempManAddrVal,
                             LLDP_MAX_LEN_MAN_ADDR);
                    CliPrintf (CliHandle, "%-4d %-7s %-31s %-34s\r\n",
                               i4RemManIfId, au1RemManSubtypeStr,
                               au1DispManAddrOther, pc1DispManAddrOID);
                    break;
            }
            if (MemReleaseMemBlock (gLldpGlobalInfo.LldpManAddrOidPoolId,
                                    (UINT1 *) pc1DispManAddrOID) == MEM_FAILURE)
            {
                LLDP_TRC (LLDP_CRITICAL_TRC |
                          ALL_FAILURE_TRC, "LldpCliShowRemManAddr: Failed"
                          "to Release Memory for ManAddrOID \r\n");
            }
        }
        i4PrevRemManAddrSubtype = i4RemManAddrSubtype;
        MEMCPY (PrevRemManAddr.pu1_OctetList, RemManAddr.pu1_OctetList,
                RemManAddr.i4_Length);
        PrevRemManAddr.i4_Length = RemManAddr.i4_Length;

        u4PrevRemManTimeMark = u4RemManTimeMark;
        i4PrevRemManLocalPortNum = i4RemManLocalPortNum;
        i4PrevRemManIndex = i4RemManIndex;
        u4PrevRemManDestMacAddr = u4RemManDestMacAddr;
    }
    while (nmhGetNextIndexLldpV2RemManAddrTable (u4PrevRemManTimeMark,
                                                 &u4RemManTimeMark,
                                                 i4PrevRemManLocalPortNum,
                                                 &i4RemManLocalPortNum,
                                                 u4PrevRemManDestMacAddr,
                                                 &u4RemManDestMacAddr,
                                                 (UINT4) i4PrevRemManIndex,
                                                 (UINT4 *) &i4RemManIndex,
                                                 i4PrevRemManAddrSubtype,
                                                 &i4RemManAddrSubtype,
                                                 &PrevRemManAddr,
                                                 &RemManAddr) == SNMP_SUCCESS);

    if (u1FlagEntryFound == OSIX_FALSE)
    {
        CliPrintf (CliHandle, "Not Advertised\r\n");
    }
    return;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowTraffic
 *
 *     DESCRIPTION      : Displays LLDP counters, including the no. of frames
 *                        sent, received, discarded, etc
 *
 *     INPUT            : CliHandle - CliContext ID
 *                        i4IfIndex - Interface Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS
 *
 ***************************************************************************/

PUBLIC INT4
LldpCliShowTraffic (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 *pDestMac,
                    UINT1 u1ShowOpt)
{
    UINT4               u4FramesOut = 0;
    UINT4               u4TotalFramesOut = 0;
    UINT4               u4TaggedFramesOut = 0;
    UINT4               u4PortFramesOut = 0;
    UINT4               u4PortTaggedFramesOut = 0;
    UINT4               u4FramesRecvInErr = 0;
    UINT4               u4PortFramesErr = 0;
    UINT4               u4EntriesAged = 0;
    UINT4               u4PortEntAged = 0;
    UINT4               u4PortFramesTot = 0;
    UINT4               u4FramesIn = 0;
    UINT4               u4PortTlvsDiscardTot = 0;
    UINT4               u4TlvsDiscardTot = 0;
    UINT4               u4PortTlvsUnrecogTot = 0;
    UINT4               u4TlvsUnrecogTot = 0;
    UINT4               u4PortFramesDiscardTot = 0;
    UINT4               u4FramesDiscardTot = 0;
    INT4                i4PrevIfIndex = 0;
    UINT4               u4LocPortNum = 0;
    UINT4               u4DestMacIndex = 0;
    UINT4               u4PrevDestMacIndex = 0;
    INT4                i4Index = i4IfIndex;
    UINT4               u4PduLengthError = 0;
    UINT4               u4PduLengthErrorTot = 0;
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    UINT4               u4MedFrames = 0;
    UINT4               u4MedTxFramesTotal = 0;
    UINT4               u4MedRxFramesTotal = 0;
    UINT4               u4MedRxFramesDisc = 0;
    UINT4               u4MedRxTlvsDisc = 0;
    UINT4               u4MedCapTlvsDisc = 0;
    UINT4               u4MedNwPolTlvsDisc = 0;
    UINT4               u4MedInvenTlvsDisc = 0;
    UINT4               u4MedLocTlvsDisc = 0;
    UINT4               u4MedPowTlvsDisc = 0;

    if (u1ShowOpt == LLDP_CLI_SHOW_ALL)
    {
        /*Show for all the ports */
        if (nmhGetFirstIndexLldpV2PortConfigTable (&i4IfIndex, &u4DestMacIndex)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        /*Show for a specific port */
        if (u1ShowOpt == LLDP_CLI_SHOW_INTERFACE)
        {
            i4PrevIfIndex = i4IfIndex;
            u4DestMacIndex = DEFAULT_DEST_MAC_ADDR_INDEX;
            if (nmhValidateIndexInstanceLldpV2PortConfigTable
                (i4IfIndex, u4DestMacIndex) == SNMP_FAILURE)
            {
                u4DestMacIndex = LLDP_V1_DEST_MAC_ADDR_INDEX (i4IfIndex);
            }
        }

        else                    /* u1ShowOpt == LLDP_CLI_SHOW_MAC */
        {
            if (pDestMac != NULL)
            {
                if (nmhGetFslldpv2ConfigPortMapNum
                    (i4IfIndex, pDestMac,
                     (INT4 *) &u4LocPortNum) == SNMP_FAILURE)
                {
                    CLI_SET_ERR (LLDP_CLI_INVALID_DESTINATION_MAC);
                    return CLI_FAILURE;
                }
                if (LldpUtilGetDestMacAddrTblIndex
                    (u4LocPortNum, &u4DestMacIndex) == OSIX_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if (nmhValidateIndexInstanceLldpV2PortConfigTable
                    (i4IfIndex, u4DestMacIndex) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
        }
    }

    do
    {
        if (u1ShowOpt == LLDP_CLI_SHOW_INTERFACE)
        {
            if (i4IfIndex != i4Index)
            {
                break;
            }

        }
        if (LldpTxUtlGetLocPortEntry ((UINT4) i4IfIndex, &pLldpLocPortInfo) ==
            OSIX_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (pLldpLocPortInfo != NULL)
        {

            u4PortFramesOut = 0;
            /* LLDP Transmit(Tx) Traffic */
            if (nmhGetLldpV2StatsTxPortFramesTotal
                (i4IfIndex, u4DestMacIndex, &u4PortFramesOut) != SNMP_FAILURE)
            {
                u4FramesOut = u4FramesOut + u4PortFramesOut;
            }
            /* LLDP Transmit [Tagged Tx Traffic] */
            if (nmhGetFsLldpStatsTaggedTxPortFramesTotal (i4IfIndex,
                                                          u4DestMacIndex,
                                                          &u4PortTaggedFramesOut)
                != SNMP_FAILURE)
            {
                u4TaggedFramesOut = u4TaggedFramesOut + u4PortTaggedFramesOut;
            }

            u4PortFramesErr = 0;
            u4TotalFramesOut = u4FramesOut + u4TaggedFramesOut;
            /* LLDP Receive(Rx) Traffic */
            if (nmhGetLldpV2StatsRxPortFramesErrors
                (i4IfIndex, u4DestMacIndex, &u4PortFramesErr) != SNMP_FAILURE)
            {
                u4FramesRecvInErr = u4FramesRecvInErr + u4PortFramesErr;
            }

            u4PortEntAged = 0;
            if (nmhGetLldpV2StatsRxPortAgeoutsTotal
                (i4IfIndex, u4DestMacIndex, &u4PortEntAged) != SNMP_FAILURE)
            {
                u4EntriesAged = u4EntriesAged + u4PortEntAged;
            }

            u4PortFramesTot = 0;
            if (nmhGetLldpV2StatsRxPortFramesTotal
                (i4IfIndex, u4DestMacIndex, &u4PortFramesTot) != SNMP_FAILURE)
            {
                u4FramesIn = u4FramesIn + u4PortFramesTot;
            }

            u4PortTlvsDiscardTot = 0;
            if (nmhGetLldpV2StatsRxPortTLVsDiscardedTotal
                (i4IfIndex, u4DestMacIndex,
                 &u4PortTlvsDiscardTot) != SNMP_FAILURE)
            {
                u4TlvsDiscardTot = u4TlvsDiscardTot + u4PortTlvsDiscardTot;
            }

            u4PortTlvsUnrecogTot = 0;
            if (nmhGetLldpV2StatsRxPortTLVsUnrecognizedTotal
                (i4IfIndex, u4DestMacIndex,
                 &u4PortTlvsUnrecogTot) != SNMP_FAILURE)
            {
                u4TlvsUnrecogTot = u4TlvsUnrecogTot + u4PortTlvsUnrecogTot;
            }

            u4PortFramesDiscardTot = 0;
            if (nmhGetLldpV2StatsRxPortFramesDiscardedTotal
                (i4IfIndex, u4DestMacIndex,
                 &u4PortFramesDiscardTot) != SNMP_FAILURE)
            {
                u4FramesDiscardTot =
                    u4FramesDiscardTot + u4PortFramesDiscardTot;
            }
            u4PduLengthError = 0;
            if (nmhGetLldpV2StatsTxLLDPDULengthErrors
                (i4IfIndex, u4DestMacIndex, &u4PduLengthError) != SNMP_FAILURE)
            {
                u4PduLengthErrorTot = u4PduLengthErrorTot + u4PduLengthError;
            }
            /*LLDP-MED related counters */
            u4MedFrames = 0;
            if (nmhGetFsLldpMedStatsTxFramesTotal (i4IfIndex, u4DestMacIndex,
                                                   &u4MedFrames) !=
                SNMP_FAILURE)
            {
                u4MedTxFramesTotal = u4MedTxFramesTotal + u4MedFrames;
            }
            u4MedFrames = 0;
            if (nmhGetFsLldpMedStatsRxFramesTotal (i4IfIndex, u4DestMacIndex,
                                                   &u4MedFrames) !=
                SNMP_FAILURE)
            {
                u4MedRxFramesTotal = u4MedRxFramesTotal + u4MedFrames;
            }
            u4MedFrames = 0;
            if (nmhGetFsLldpMedStatsRxFramesDiscardedTotal (i4IfIndex,
                                                            u4DestMacIndex,
                                                            &u4MedFrames) !=
                SNMP_FAILURE)
            {
                u4MedRxFramesDisc = u4MedRxFramesDisc + u4MedFrames;
            }
            u4MedFrames = 0;
            if (nmhGetFsLldpMedStatsRxTLVsDiscardedTotal (i4IfIndex,
                                                          u4DestMacIndex,
                                                          &u4MedFrames) !=
                SNMP_FAILURE)
            {
                u4MedRxTlvsDisc = u4MedRxTlvsDisc + u4MedFrames;
            }
            u4MedFrames = 0;
            if (nmhGetFsLldpMedStatsRxCapTLVsDiscarded (i4IfIndex,
                                                        u4DestMacIndex,
                                                        &u4MedFrames) !=
                SNMP_FAILURE)
            {
                u4MedCapTlvsDisc = u4MedCapTlvsDisc + u4MedFrames;
            }
            u4MedFrames = 0;
            if (nmhGetFsLldpMedStatsRxPolicyTLVsDiscarded (i4IfIndex,
                                                           u4DestMacIndex,
                                                           &u4MedFrames) !=
                SNMP_FAILURE)
            {
                u4MedNwPolTlvsDisc = u4MedNwPolTlvsDisc + u4MedFrames;
            }
            u4MedFrames = 0;
            if (nmhGetFsLldpMedStatsRxInventoryTLVsDiscarded (i4IfIndex,
                                                              u4DestMacIndex,
                                                              &u4MedFrames) !=
                SNMP_FAILURE)
            {
                u4MedInvenTlvsDisc = u4MedInvenTlvsDisc + u4MedFrames;
            }
            u4MedFrames = 0;
            if (nmhGetFsLldpMedStatsRxLocationTLVsDiscarded (i4IfIndex,
                                                             u4DestMacIndex,
                                                             &u4MedFrames) !=
                SNMP_FAILURE)
            {
                u4MedLocTlvsDisc = u4MedLocTlvsDisc + u4MedFrames;
            }
            u4MedFrames = 0;
            if (nmhGetFsLldpMedStatsRxExPowerMDITLVsDiscarded (i4IfIndex,
                                                               u4DestMacIndex,
                                                               &u4MedFrames) !=
                SNMP_FAILURE)
            {
                u4MedPowTlvsDisc = u4MedPowTlvsDisc + u4MedFrames;
            }
            if (u1ShowOpt == LLDP_CLI_SHOW_MAC)
            {
                break;            /*Displayed the particular entry */
            }
        }

        i4PrevIfIndex = i4IfIndex;
        u4PrevDestMacIndex = u4DestMacIndex;
    }
    while (nmhGetNextIndexLldpV2PortConfigTable
           (i4PrevIfIndex, &i4IfIndex, u4PrevDestMacIndex,
            &u4DestMacIndex) == SNMP_SUCCESS);

    CliPrintf (CliHandle, "Total Frames Out                       : %ld\r\n",
               u4TotalFramesOut);
    CliPrintf (CliHandle, "Total Tagged Frames Out                : %ld\r\n",
               u4TaggedFramesOut);
    CliPrintf (CliHandle, "Total Entries Aged                     : %u\r\n",
               u4EntriesAged);
    CliPrintf (CliHandle, "Total Frames In                        : %u\r\n",
               u4FramesIn);
    CliPrintf (CliHandle, "Total Frames Received In Error         : %u\r\n",
               u4FramesRecvInErr);
    CliPrintf (CliHandle, "Total Frames Discarded                 : %ld\r\n",
               u4FramesDiscardTot);
    CliPrintf (CliHandle, "Total TLVS Unrecognized                : %u\r\n",
               u4TlvsUnrecogTot);
    CliPrintf (CliHandle, "Total TLVs Discarded                   : %u\r\n",
               u4TlvsDiscardTot);
    CliPrintf (CliHandle, "Total PDU length error Drops           : %d\r\n",
               u4PduLengthErrorTot);
    CliPrintf (CliHandle, "Total LLDP-MED Frames Out              : %ld\r\n",
               u4MedTxFramesTotal);
    CliPrintf (CliHandle, "Total LLDP-MED Frames In               : %u\r\n",
               u4MedRxFramesTotal);
    CliPrintf (CliHandle, "Total LLDP-MED Frames Discarded        : %ld\r\n",
               u4MedRxFramesDisc);
    CliPrintf (CliHandle, "Total LLDP-MED TLVs Discarded          : %u\r\n",
               u4MedRxTlvsDisc);
    CliPrintf (CliHandle, "Total Media Capability TLVs Discarded  : %u\r\n",
               u4MedCapTlvsDisc);
    CliPrintf (CliHandle, "Total Network Policy TLVs Discarded    : %u\r\n",
               u4MedNwPolTlvsDisc);
    CliPrintf (CliHandle, "Total Inventory TLVs Discarded         : %u\r\n",
               u4MedInvenTlvsDisc);
    CliPrintf (CliHandle, "Total Location TLVs Discarded          : %u\r\n",
               u4MedLocTlvsDisc);
    CliPrintf (CliHandle, "Total Ex-PowerViaMDI TLVs Discarded    : %u\r\n",
               u4MedPowTlvsDisc);
    CliPrintf (CliHandle,
               "Med-Capability TLV Discard Reason      : Not Applicable\r\n");
    CliPrintf (CliHandle,
               "Nw-Policy TLV Discard Reason           : Not Applicable\r\n");
    CliPrintf (CliHandle,
               "Inventory TLV Discard Reason           : Not Applicable\r\n");
    CliPrintf (CliHandle,
               "Location-ID TLV Discard Reason         : Not Applicable\r\n");
    CliPrintf (CliHandle,
               "Ex-PowerViaMDI TLV Discard Reason      : Not Applicable\r\n");

    return CLI_SUCCESS;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowLocal
 *
 *     DESCRIPTION      : Displays the current switch information that will be
 *                        used to populate outbound LLDP advertisements for a
 *                        specific interface or all interfaces.
 *
 *     INPUT            : CliHandle - CliContext ID
 *                        i4IfIndex - Interface Index
 *                        i4Option  - Option for showing the
 *                                    Local Management Address
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/

PUBLIC INT4
LldpCliShowLocal (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Option,
                  UINT1 u1ShowOption, UINT4 u4DestMacIndex)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1              *pu1ChassisId = NULL;
    UINT1              *pu1PortId = NULL;
    UINT1              *pu1PortDesc = NULL;
    UINT1              *pu1SysName = NULL;
    UINT1              *pu1SysDesc = NULL;
    UINT1              *pu1DispChassisId = NULL;
    CHR1               *pc1DispChassIdNwAddr;
    UINT1              *pu1DispPortId = NULL;
    CHR1               *pc1DispPortIdNwAddr = NULL;
    UINT1               au1LocAdvtCap[LLDP_MAX_LEN_ADVT_CAPAB + 1];
    UINT1               u1LAStatus;
    UINT2               u2SysCapSupported;
    UINT2               u2SysCapEnabled;
    UINT2               u2PhyMediaCap;
    UINT4               u4ChassIdNwAddr = 0;
    UINT4               u4PortIdNwAddr = 0;
    INT1               *pi1IfName = NULL;
    INT4                i4PortIdSubType = 0;
    INT4                i4PrevIfIndex = 0;
    INT4                i4AutoNegEnabled = 0;
    INT4                i4AutoNegSupport = 0;
    INT4                i4DeviceClass = 0;
    UINT4               u4MauType = 0;
    UINT4               u4PVId = 0;
    UINT4               u4Dot3MaxFrameSize = 0;
    UINT4               u4Dot3LAPorId = 0;
    UINT4               u4PrevDestMacIndex = 0;
    INT4                i4ChassisIdSubType;
    INT4                i4ArrayAllocCnt = 0;
    INT4                i4Index = i4IfIndex;
    INT4                i4VidTxStatus = 0;
    INT4                i4ManVidTxStatus = 0;
    INT4                i4PoEDeviceType = 0;
    INT4                i4PowerSource = 0;
    INT4                i4MedAdminStatus = 0;
    UINT4               u4LocVid = 0;
    UINT4               u4LocManVid = 0;
    tSNMP_OCTET_STRING_TYPE PortId;
    tSNMP_OCTET_STRING_TYPE PortDesc;
    tSNMP_OCTET_STRING_TYPE ChassisId;
    tSNMP_OCTET_STRING_TYPE SysName;
    tSNMP_OCTET_STRING_TYPE SysDesc;
    tSNMP_OCTET_STRING_TYPE SysCapSupported;
    tSNMP_OCTET_STRING_TYPE SysCapEnabled;
    tSNMP_OCTET_STRING_TYPE Dot3PhyMediaCap;
    tSNMP_OCTET_STRING_TYPE Dot3LAStatus;

    /* Allocate Memory for Chassis */
    if ((pu1ChassisId = (UINT1 *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpArrayInfoPoolId))) == NULL)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowLocal: Failed to Allocate Memory "
                  "for Chassis \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }
    i4ArrayAllocCnt++;

    /* Allocate Memory for DispChassisId */
    if ((pu1DispChassisId = (UINT1 *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpArrayInfoPoolId))) == NULL)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1ChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowLocal: Failed to Allocate Memory "
                  "for DispChassis \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }
    i4ArrayAllocCnt++;

    /* Allocate Memory for PortId */
    if ((pu1PortId = (UINT1 *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpArrayInfoPoolId))) == NULL)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1ChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        LLDP_TRC (LLDP_PROTO_ID_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowLocal: Failed to Allocate Memory "
                  "for Port \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }
    i4ArrayAllocCnt++;

    /* Allocate Memory for DispPortId */
    if ((pu1DispPortId = (UINT1 *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpArrayInfoPoolId))) == NULL)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1ChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        LLDP_TRC (LLDP_PROTO_ID_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowLocal: Failed to Allocate Memory "
                  "for DispPort \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }
    i4ArrayAllocCnt++;

    /* Allocate Memory for PortDesc */
    if ((pu1PortDesc = (UINT1 *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpArrayInfoPoolId))) == NULL)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1ChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispPortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        LLDP_TRC (LLDP_PORT_DESC_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowLocal: Failed to Allocate Memory "
                  "for PortDesc \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }
    i4ArrayAllocCnt++;

    /* Allocate Memory for SysName */
    if ((pu1SysName = (UINT1 *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpArrayInfoPoolId))) == NULL)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1ChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispPortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortDesc) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        LLDP_TRC (LLDP_SYS_NAME_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowLocal: Failed to Allocate Memory "
                  "for SysName \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }
    i4ArrayAllocCnt++;

    /* Allocate Memory for SysDesc */
    if ((pu1SysDesc = (UINT1 *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpArrayInfoPoolId))) == NULL)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1ChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispPortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortDesc) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1SysName) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        LLDP_TRC (LLDP_SYS_DESC_TRC | ALL_FAILURE_TRC,
                  "LldpCliShowLocal: Failed to Allocate Memory "
                  "for SysDesc \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return CLI_FAILURE;
    }
    i4ArrayAllocCnt++;

    MEMSET (&ChassisId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PortId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PortDesc, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&SysName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&SysDesc, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&SysCapSupported, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&SysCapEnabled, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&Dot3PhyMediaCap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&Dot3LAStatus, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (pu1ChassisId, 0, LLDP_MAX_LEN_CHASSISID + 1);
    MEMSET (pu1PortId, 0, LLDP_MAX_LEN_PORTID + 1);
    MEMSET (pu1PortDesc, 0, LLDP_MAX_LEN_PORTDESC + 1);
    MEMSET (pu1SysName, 0, LLDP_MAX_LEN_SYSNAME + 1);
    MEMSET (pu1SysDesc, 0, LLDP_MAX_LEN_SYSDESC + 1);
    MEMSET (au1LocAdvtCap, 0, LLDP_MAX_LEN_ADVT_CAPAB + 1);

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    pi1IfName = (INT1 *) au1IfName;
    ChassisId.pu1_OctetList = pu1ChassisId;
    PortId.pu1_OctetList = pu1PortId;
    PortDesc.pu1_OctetList = pu1PortDesc;
    SysName.pu1_OctetList = pu1SysName;
    SysDesc.pu1_OctetList = pu1SysDesc;
    SysCapSupported.pu1_OctetList = (UINT1 *) &u2SysCapSupported;
    SysCapSupported.i4_Length = sizeof (u2SysCapSupported);
    SysCapEnabled.pu1_OctetList = (UINT1 *) &u2SysCapEnabled;
    SysCapEnabled.i4_Length = sizeof (u2SysCapEnabled);
    Dot3PhyMediaCap.pu1_OctetList = (UINT1 *) &u2PhyMediaCap;
    Dot3PhyMediaCap.i4_Length = sizeof (u2PhyMediaCap);
    Dot3LAStatus.pu1_OctetList = &u1LAStatus;
    Dot3LAStatus.i4_Length = sizeof (u1LAStatus);

    if (i4Option == LLDP_CLI_SHOW_LOCAL_MGMT_ADDR)
    {
        LldpCliShowLocalManAddr (CliHandle);
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1ChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispChassisId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1DispPortId) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1PortDesc) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1SysName) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                pu1SysDesc) == LLDP_FALSE)
        {
            LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                      ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                      "failed!!. \r\n");
        }
        return CLI_SUCCESS;
    }
    if (u1ShowOption == LLDP_CLI_SHOW_ALL)
    {
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "Capability Codes   : \r\n");
        CliPrintf (CliHandle, "(R) Router, (B) Bridge, (T) Telephone, "
                   "(C) DOCSIS Cable Device, \r\n"
                   "(W) WLAN Access Point, "
                   "(P) Repeater, (S) Station, (O) Other\r\n\r\n");
        nmhGetFsLldpLocChassisIdSubtype (&i4ChassisIdSubType);
        CliPrintf (CliHandle, "Chassis Id SubType            : ");
        LldpCliShowChassisIdSubType (CliHandle, i4ChassisIdSubType);

        nmhGetLldpV2LocChassisId (&ChassisId);
        if (i4ChassisIdSubType == LLDP_CHASS_ID_SUB_MAC_ADDR)
        {
            MEMSET (pu1DispChassisId, 0, LLDP_MAX_LEN_CHASSISID + 1);

            /* Convert octet values into strings of octets seperated
               by colon */
            CLI_CONVERT_MAC_TO_DOT_STR (ChassisId.pu1_OctetList,
                                        pu1DispChassisId);
            CliPrintf (CliHandle, "Chassis Id                    : %s\r\n",
                       pu1DispChassisId);
        }
        else if (i4ChassisIdSubType == LLDP_CHASS_ID_SUB_NW_ADDR)
        {
            MEMSET (pu1DispChassisId, 0, LLDP_MAX_LEN_CHASSISID + 1);
            /* To skip the IANA Address Family Numbers */
            ChassisId.pu1_OctetList++;
            PTR_FETCH4 (u4ChassIdNwAddr, ChassisId.pu1_OctetList);
            pc1DispChassIdNwAddr = (CHR1 *) pu1DispChassisId;
            /* Convert octet values into strings of octets seperated by dot */
            CLI_CONVERT_IPADDR_TO_STR (pc1DispChassIdNwAddr, u4ChassIdNwAddr);
            CliPrintf (CliHandle, "Chassis Id                    : %s\r\n",
                       pc1DispChassIdNwAddr);
        }
        else
        {
            CliPrintf (CliHandle, "Chassis Id                    : %s\r\n",
                       ChassisId.pu1_OctetList);
        }

        nmhGetLldpV2LocSysName (&SysName);
        CliPrintf (CliHandle, "System Name                   : %s\r\n",
                   SysName.pu1_OctetList);
        nmhGetLldpV2LocSysDesc (&SysDesc);
        CliPrintf (CliHandle, "System Description            : %s\r\n",
                   SysDesc.pu1_OctetList);
        nmhGetLldpV2LocSysCapSupported (&SysCapSupported);
        nmhGetLldpV2LocSysCapEnabled (&SysCapEnabled);
        CliPrintf (CliHandle, "System Capabilities Supported : ");
        /* Shows Supported System Capabilities */
        LldpCliShowSysSupportCapab (CliHandle, SysCapSupported);
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "System Capabilities Enabled   : ");
        /* Shows Enabled System Capabilities */
        LldpCliShowSysEnabledCapab (CliHandle, SysCapEnabled);
        CliPrintf (CliHandle, "\r\n");

        /* LLDP-MED */
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "-LLDP-MED Info                \r\n");

        /* Get Device Class Information */
        if (nmhGetLldpXMedLocDeviceClass (&i4DeviceClass) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        /* Check if Device Class is not defined */
        if (i4DeviceClass == LLDP_MED_NOT_DEF_DEVICE)
        {
            CliPrintf (CliHandle, "Device Class                  : "
                       "Type Not Defined \r\n");
        }
        /* Check if Device Class is End point Class-1 Device */
        else if (i4DeviceClass == LLDP_MED_CLASS_1_DEVICE)
        {
            CliPrintf (CliHandle, "Device Class                  : "
                       "Endpoint Class I \r\n");
        }
        /* Check if Device Class is End point Class-2 Device */
        else if (i4DeviceClass == LLDP_MED_CLASS_2_DEVICE)
        {
            CliPrintf (CliHandle, "Device Class                  : "
                       "Endpoint Class II \r\n");
        }
        /* Check if Device Class is End point Class-3 Device */
        else if (i4DeviceClass == LLDP_MED_CLASS_3_DEVICE)
        {
            CliPrintf (CliHandle, "Device Class                  : "
                       "Endpoint Class III \r\n");
        }
        /* Check if Device Class is Network Connectivity Device */
        else if (i4DeviceClass == LLDP_MED_NW_CONNECTIVITY_DEVICE)
        {
            CliPrintf (CliHandle, "Device Class                  : "
                       "Network Connectivity \r\n");
        }

        /* Display Inventory Information */
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "LLDP-MED Inventory Info        \r\n");
        LldpMedCliShowInventoryInfo (CliHandle);

        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "LLDP-MED PoE Info              \r\n");
        /* Get Power Device Type */
        if (nmhGetLldpXMedLocXPoEDeviceType (&i4PoEDeviceType) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (i4PoEDeviceType == LLDP_MED_PSE)
        {
            CliPrintf (CliHandle, "Power Device Type             : "
                       "PSE Device \r\n");
        }
        else if (i4PoEDeviceType == LLDP_MED_PD)
        {
            CliPrintf (CliHandle, "Power Device Type             : "
                       "PD Device \r\n");
        }

        /* Get Power Source */
        if (nmhGetLldpXMedLocXPoEPSEPowerSource (&i4PowerSource) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (i4PowerSource == LLDP_MED_PSE_UNKNOWN_MIB)
        {
            CliPrintf (CliHandle, "Power Source                  : "
                       "Unknown \r\n");
        }
        else if (i4PowerSource == LLDP_MED_PSE_PRIMARY_MIB)
        {
            CliPrintf (CliHandle, "Power Source                  : "
                       "Primary \r\n");
        }
        else if (i4PowerSource == LLDP_MED_PSE_BACK_UP_MIB)
        {
            CliPrintf (CliHandle, "Power Source                  : "
                       "Backup \r\n");
        }

        /* Show for all the ports */
        if (nmhGetFirstIndexLldpV2LocPortTable (&i4IfIndex) == SNMP_FAILURE)
        {
            if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                    pu1ChassisId) == LLDP_FALSE)
            {
                LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                          ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                          "failed!!. \r\n");
            }
            if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                    pu1DispChassisId) == LLDP_FALSE)
            {
                LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                          ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                          "failed!!. \r\n");
            }
            if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                    pu1PortId) == LLDP_FALSE)
            {
                LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                          ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                          "failed!!. \r\n");
            }
            if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                    pu1DispPortId) == LLDP_FALSE)
            {
                LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                          ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                          "failed!!. \r\n");
            }
            if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                    pu1PortDesc) == LLDP_FALSE)
            {
                LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                          ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                          "failed!!. \r\n");
            }
            if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                    pu1SysName) == LLDP_FALSE)
            {
                LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                          ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                          "failed!!. \r\n");
            }
            if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                    pu1SysDesc) == LLDP_FALSE)
            {
                LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                          ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                          "failed!!. \r\n");
            }
            return CLI_FAILURE;
        }
        /* Coverity Fix */
        if (nmhGetFirstIndexLldpV2PortConfigTable (&i4IfIndex, &u4DestMacIndex)
            == SNMP_FAILURE)
        {
        }
    }
    else
    {
        if (nmhValidateIndexInstanceLldpV2LocPortTable (i4IfIndex) ==
            SNMP_FAILURE)
        {
            if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                    pu1ChassisId) == LLDP_FALSE)
            {
                LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                          ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                          "failed!!. \r\n");
            }
            if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                    pu1DispChassisId) == LLDP_FALSE)
            {
                LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                          ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                          "failed!!. \r\n");
            }
            if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                    pu1PortId) == LLDP_FALSE)
            {
                LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                          ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                          "failed!!. \r\n");
            }
            if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                    pu1DispPortId) == LLDP_FALSE)
            {
                LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                          ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                          "failed!!. \r\n");
            }
            if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                    pu1PortDesc) == LLDP_FALSE)
            {
                LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                          ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                          "failed!!. \r\n");
            }
            if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                    pu1SysName) == LLDP_FALSE)
            {
                LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                          ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                          "failed!!. \r\n");
            }
            if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                                    pu1SysDesc) == LLDP_FALSE)
            {
                LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                          ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                          "failed!!. \r\n");
            }
            return CLI_FAILURE;
        }
        /*    nmhGetFslldpv2ConfigPortMapNum (i4IfIndex, pDestMac,(INT4 *) &u4LocPortNum);
           LldpUtilGetDestMacAddrTblIndex(u4LocPortNum, &u4DestMacIndex); */
    }

    do
    {
        if (u1ShowOption == LLDP_CLI_SHOW_INTERFACE)
        {
            if (i4IfIndex != i4Index)
            {
                i4PrevIfIndex = i4IfIndex;
                u4PrevDestMacIndex = u4DestMacIndex;
                continue;
            }
        }

        MEMSET (pu1PortId, 0, LLDP_MAX_LEN_PORTID + 1);
        MEMSET (pu1PortDesc, 0, LLDP_MAX_LEN_PORTDESC + 1);
        MEMSET (au1LocAdvtCap, 0, LLDP_MAX_LEN_ADVT_CAPAB + 1);
        MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

        if (u1ShowOption == LLDP_CLI_SHOW_ALL)
        {
            CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName);
            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle, "%s :\r\n", pi1IfName);
        }
        nmhGetLldpV2LocPortIdSubtype (i4IfIndex, &i4PortIdSubType);
        CliPrintf (CliHandle, "Port Id SubType               : ");
        LldpCliShowPortIdSubType (CliHandle, i4PortIdSubType);
        nmhGetLldpV2LocPortId (i4IfIndex, &PortId);

        if (i4PortIdSubType == LLDP_PORT_ID_SUB_MAC_ADDR)
        {
            MEMSET (pu1DispPortId, 0, LLDP_MAX_LEN_PORTID + 1);

            /* Convert octet values into strings of octets
             * seperated by colon */
            CLI_CONVERT_MAC_TO_DOT_STR (PortId.pu1_OctetList, pu1DispPortId);
            CliPrintf (CliHandle, "Port Id                       : %s\r\n",
                       pu1DispPortId);
        }
        else if (i4PortIdSubType == LLDP_PORT_ID_SUB_NW_ADDR)
        {
            MEMSET (pu1DispPortId, 0, LLDP_MAX_LEN_PORTID + 1);
            /* To skip the IANA Address Family Numbers */
            PortId.pu1_OctetList++;
            PTR_FETCH4 (u4PortIdNwAddr, PortId.pu1_OctetList);
            pc1DispPortIdNwAddr = (CHR1 *) pu1DispPortId;
            /* Convert octet values into strings of octets seperated by dot */
            CLI_CONVERT_IPADDR_TO_STR (pc1DispPortIdNwAddr, u4PortIdNwAddr);
            CliPrintf (CliHandle, "Port Id                       : %s\r\n",
                       pc1DispPortIdNwAddr);
        }

        else
        {
            CliPrintf (CliHandle, "Port Id                       : %s\r\n",
                       PortId.pu1_OctetList);
        }
        nmhGetLldpV2LocPortDesc (i4IfIndex, &PortDesc);
        CliPrintf (CliHandle, "Port Description              : %s\r\n",
                   PortDesc.pu1_OctetList);

        /* Shows Enabled Basic, Dot1, Dot3 Tlvs of Local LLDP system */
        LldpCliShowLocEnabledTlvTx (CliHandle, i4IfIndex, u4DestMacIndex);

        /* Shows 802.3 TLV Information */
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "Extended 802.3 TLV Info \r\n");
        CliPrintf (CliHandle, "-MAC PHY Configuration & Status\r\n");
        nmhGetLldpV2Xdot3LocPortAutoNegSupported (i4IfIndex, &i4AutoNegSupport);
        nmhGetLldpV2Xdot3LocPortAutoNegEnabled (i4IfIndex, &i4AutoNegEnabled);
        CliPrintf (CliHandle, "Auto-Neg Support & Status     : %s, %s\r\n",
                   gapc1Support[i4AutoNegSupport],
                   gapc1Enable[i4AutoNegEnabled]);
        nmhGetLldpV2Xdot3LocPortAutoNegAdvertisedCap (i4IfIndex,
                                                      &Dot3PhyMediaCap);
        CliOctetToHexStr (Dot3PhyMediaCap.pu1_OctetList,
                          Dot3PhyMediaCap.i4_Length, au1LocAdvtCap);
        /* Shows Auto Negotiation Advertised Capabilities */
        CliPrintf (CliHandle, "Advertised Capability Bits    : %s\r\n",
                   au1LocAdvtCap);
        LldpCliShowAutoNegAdvCap (CliHandle, &Dot3PhyMediaCap);
        nmhGetLldpV2Xdot3LocPortOperMauType (i4IfIndex, &u4MauType);
        CliPrintf (CliHandle, "Operational MAU Type          : %d\r\n",
                   u4MauType);
        if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
        {
            CliPrintf (CliHandle, "-Link Aggregation\r\n");
            nmhGetLldpV2Xdot1LocLinkAggStatus (i4IfIndex, &Dot3LAStatus);
            LldpCliShowLocDot3LinkAggStatus (CliHandle, &Dot3LAStatus);
            nmhGetLldpV2Xdot1LocLinkAggPortId (i4IfIndex, &u4Dot3LAPorId);
            CliPrintf (CliHandle, "Aggregated Port Id            : %d\r\n",
                       u4Dot3LAPorId);
        }
        nmhGetLldpV2Xdot3LocMaxFrameSize (i4IfIndex, &u4Dot3MaxFrameSize);
        CliPrintf (CliHandle, "-Maximum Frame Size           : %d\r\n",
                   u4Dot3MaxFrameSize);

        /* Shows 802.1 TLV Information */
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "Extended 802.1 TLV Info \r\n");
        nmhGetLldpV2Xdot1LocPortVlanId (i4IfIndex, &u4PVId);
        CliPrintf (CliHandle, "-Port VLAN Id                 : %d\r\n", u4PVId);
        CliPrintf (CliHandle, "-Port & Protocol VLAN Id \r\n");
        LldpCliShowLocProtoVlanInfo (CliHandle, i4IfIndex);
        CliPrintf (CliHandle, "-Vlan Name\r\n");
        LldpCliShowLocVlanNameInfo (CliHandle, i4IfIndex);
        if (gLldpGlobalInfo.i4Version == LLDP_VERSION_09)
        {
            CliPrintf (CliHandle, "-Link Aggregation\r\n");
            nmhGetLldpV2Xdot1LocLinkAggStatus (i4IfIndex, &Dot3LAStatus);
            LldpCliShowLocDot3LinkAggStatus (CliHandle, &Dot3LAStatus);
            nmhGetLldpV2Xdot1LocLinkAggPortId (i4IfIndex, &u4Dot3LAPorId);
            CliPrintf (CliHandle, "Aggregated Port Id            : %d\r\n",
                       u4Dot3LAPorId);

            CliPrintf (CliHandle, "-VID TLV: \r\n");
            CliPrintf (CliHandle, "%-14s %-34s \r\n", "VID ", "TxStatus");
            CliPrintf (CliHandle, "%-14s %-34s \r\n", "-------", "---------");

            nmhGetLldpV2Xdot1LocVidUsageDigest (i4IfIndex, &u4LocVid);
            nmhGetLldpV2Xdot1ConfigVidUsageDigestTxEnable (i4IfIndex,
                                                           &i4VidTxStatus);
            if (i4VidTxStatus == LLDP_TRUE)
            {
                CliPrintf (CliHandle, "%-14x %-34s\r\n", u4LocVid, "Enabled");
            }
            else
            {
                CliPrintf (CliHandle, "%-14x %-34s\r\n", u4LocVid, "Disabled");
            }

            CliPrintf (CliHandle, "-Management Vid TLV: \r\n");
            CliPrintf (CliHandle, "%-14s %-34s \r\n", "Vlan Id", "TxStatus");
            CliPrintf (CliHandle, "%-14s %-34s \r\n", "-------", "---------");

            nmhGetLldpV2Xdot1LocManVid (i4IfIndex, &u4LocManVid);
            nmhGetLldpV2Xdot1ConfigManVidTxEnable (i4IfIndex,
                                                   &i4ManVidTxStatus);
            if (i4ManVidTxStatus == LLDP_TRUE)
            {
                CliPrintf (CliHandle, "%-14d %-34s\r\n", u4LocManVid,
                           "Enabled");
            }
            else
            {
                CliPrintf (CliHandle, "%-14d %-34s\r\n", u4LocManVid,
                           "Disabled");
            }
        }
        /* LLDP-MED */
        CliPrintf (CliHandle, "\r\n");
        nmhGetFsLldpMedAdminStatus (i4IfIndex, &i4MedAdminStatus);
        if (i4MedAdminStatus == LLDP_DISABLED)
        {
            CliPrintf (CliHandle,
                       "LLDP-MED Admin Status         : Disabled\r\n");
        }
        else if (i4MedAdminStatus == LLDP_ENABLED)
        {
            CliPrintf (CliHandle,
                       "LLDP-MED Admin Status         : Enabled\r\n");
        }
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "-LLDP-MED Capability TLV       \r\n");
        /* Display Port Config table Infromation */
        LldpMedCliShowPortConfigInfo (CliHandle, i4IfIndex, u4DestMacIndex);
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "-LLDP-MED Network Policy TLV    \r\n");
        LldpMedCliShowPolicyInfo (CliHandle, i4IfIndex);
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "-LLDP-MED Location TLV Info     \r\n");
        LldpMedCliShowLocationInfo (CliHandle, i4IfIndex);
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "-LLDP-MED Ex-PowerViaMDI TLV Info  \r\n");
        LldpMedCliShowPowerInfo (CliHandle, i4IfIndex);

        CliPrintf (CliHandle, "--------------------------------------"
                   "----------------------\r\n\r\n");

        i4PrevIfIndex = i4IfIndex;
        u4PrevDestMacIndex = u4DestMacIndex;

        if (u1ShowOption == LLDP_CLI_SHOW_MAC)
        {
            break;                /*Displayed the particular entry */
        }

    }
    while (nmhGetNextIndexLldpV2PortConfigTable (i4PrevIfIndex, &i4IfIndex,
                                                 u4PrevDestMacIndex,
                                                 &u4DestMacIndex)
           == SNMP_SUCCESS);

    if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                            pu1ChassisId) == LLDP_FALSE)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                  ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                  "failed!!. \r\n");
    }
    if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                            pu1DispChassisId) == LLDP_FALSE)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                  ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                  "failed!!. \r\n");
    }
    if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                            pu1PortId) == LLDP_FALSE)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                  ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                  "failed!!. \r\n");
    }
    if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                            pu1DispPortId) == LLDP_FALSE)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                  ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                  "failed!!. \r\n");
    }
    if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                            pu1PortDesc) == LLDP_FALSE)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                  ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                  "failed!!. \r\n");
    }
    if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                            pu1SysName) == LLDP_FALSE)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                  ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                  "failed!!. \r\n");
    }
    if (MemReleaseMemBlock (gLldpGlobalInfo.LldpArrayInfoPoolId,
                            pu1SysDesc) == LLDP_FALSE)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC |
                  ALL_FAILURE_TRC, "LldpArrayInfoPoolId Memory release"
                  "failed!!. \r\n");
    }

    return CLI_SUCCESS;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpMedCliShowPortConfigInfo
 *
 *     DESCRIPTION      : Displays the Port Config Information
 *
 *     INPUT            : CliHandle      - CliContext ID
 *                        i4IfIndex      - Interface Index
 *                        u4DestMacIndex - Destination Mac Address 
 *                                         Index of the agent
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 ***************************************************************************/

PUBLIC VOID
LldpMedCliShowPortConfigInfo (tCliHandle CliHandle,
                              INT4 i4IfIndex, UINT4 u4DestMacIndex)
{

    UINT2               u2CapSupported = 0;
    UINT2               u2TlvsEnabled = 0;
    tSNMP_OCTET_STRING_TYPE CapSupported;
    tSNMP_OCTET_STRING_TYPE TlvsEnabled;
    BOOL1               bCapEnable = LLDP_FALSE;

    MEMSET (&CapSupported, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TlvsEnabled, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    CapSupported.pu1_OctetList = (UINT1 *) &u2CapSupported;
    CapSupported.i4_Length = LLDP_MED_MAX_LEN_TLV_TXENABLE;
    TlvsEnabled.pu1_OctetList = (UINT1 *) &u2TlvsEnabled;
    TlvsEnabled.i4_Length = LLDP_MED_MAX_LEN_TLV_TXENABLE;

    /* Get Capabilities Supported Information */
    if (nmhGetFsLldpMedPortCapSupported (i4IfIndex,
                                         u4DestMacIndex,
                                         &CapSupported) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "LLDP-MED Tx Supported         : ");

        /* Boolean Variable for checking if any supported 
         * Capability is there before */
        bCapEnable = LLDP_FALSE;
        /* Check whether LLDP_MED_CAPABILITY_TLV is supported or not */
        if (u2CapSupported & LLDP_MED_CAPABILITY_TLV)
        {
            bCapEnable = LLDP_TRUE;
            CliPrintf (CliHandle, "MedCapability");
        }
        /* Check whether LLDP_MED_NETWORK_POLICY_TLV is supported or not */
        if (u2CapSupported & LLDP_MED_NETWORK_POLICY_TLV)
        {
            /* If any other capability is printed before bCapEnable 
             * will be LLDP_TRUE, then print comma */
            if (bCapEnable == LLDP_TRUE)
            {
                CliPrintf (CliHandle, ", ");
            }
            else
            {
                bCapEnable = LLDP_TRUE;
            }
            CliPrintf (CliHandle, "NetworkPolicy");
        }
        /* Check whether LLDP_MED_LOCATION_ID_TLV is supported or not */
        if (u2CapSupported & LLDP_MED_LOCATION_ID_TLV)
        {
            if (bCapEnable == LLDP_TRUE)
            {
                CliPrintf (CliHandle, ", ");
            }
            else
            {
                bCapEnable = LLDP_TRUE;
            }
            CliPrintf (CliHandle, "LocationIdentity");
        }
        /* Check whether LLDP MED Power via MDI PSE is supported or not */
        if (u2CapSupported & LLDP_MED_PW_MDI_PSE_TLV)
        {
            if (bCapEnable == LLDP_TRUE)
            {
                CliPrintf (CliHandle, ", ");
            }
            else
            {
                bCapEnable = LLDP_TRUE;
            }
            CliPrintf (CliHandle, "Ex-PowerViaMDI-PSE");
        }
        /* Check whether LLDP MED Power via MDI_PD is supported or not */
        if (u2CapSupported & LLDP_MED_PW_MDI_PD_TLV)
        {
            if (bCapEnable == LLDP_TRUE)
            {
                CliPrintf (CliHandle, ", ");
            }
            else
            {
                bCapEnable = LLDP_TRUE;
            }
            CliPrintf (CliHandle, "Ex-PowerViaMDI-PD");
        }
        /* Check whether LLDP-MED INVENTORY MANAGEMENT TLV is supported or not */
        if (u2CapSupported & LLDP_MED_INVENTORY_MGMT_TLV)
        {
            if (bCapEnable == LLDP_TRUE)
            {
                CliPrintf (CliHandle, ", ");
            }

            CliPrintf (CliHandle, "Inventory");
        }

        CliPrintf (CliHandle, "\r\n");
    }

    /*Get PortConfigTable Information */
    if (nmhGetFsLldpMedPortConfigTLVsTxEnable (i4IfIndex,
                                               u4DestMacIndex,
                                               &TlvsEnabled) == SNMP_SUCCESS)
    {

        CliPrintf (CliHandle, "LLDP-MED Tx Enabled           : ");
        bCapEnable = LLDP_FALSE;

        /* Check whether LLDP_MED_CAPABILITY_TLV is enabled */
        if (u2TlvsEnabled & LLDP_MED_CAPABILITY_TLV)
        {
            bCapEnable = LLDP_TRUE;
            CliPrintf (CliHandle, "MedCapability");
        }
        /* Check whether LLDP_MED_NETWORK_POLICY_TLV is enabled */
        if (u2TlvsEnabled & LLDP_MED_NETWORK_POLICY_TLV)
        {
            if (bCapEnable == LLDP_TRUE)
            {
                CliPrintf (CliHandle, ", ");
            }
            else
            {
                bCapEnable = LLDP_TRUE;
            }
            CliPrintf (CliHandle, "NetworkPolicy");
        }
        /* Check whether LLDP_MED_LOCATION_ID_TLV is enabled */
        if (u2TlvsEnabled & LLDP_MED_LOCATION_ID_TLV)
        {
            if (bCapEnable == LLDP_TRUE)
            {
                CliPrintf (CliHandle, ", ");
            }
            else
            {
                bCapEnable = LLDP_TRUE;
            }
            CliPrintf (CliHandle, "LocationIdentity");
        }
        /* Check whether LLDP MED Power via MDI PSE is enabled */
        if (u2TlvsEnabled & LLDP_MED_PW_MDI_PSE_TLV)
        {
            if (bCapEnable == LLDP_TRUE)
            {
                CliPrintf (CliHandle, ", ");
            }
            else
            {
                bCapEnable = LLDP_TRUE;
            }
            CliPrintf (CliHandle, "Ex-PowerViaMDI-PSE");
        }
        /* Check whether LLDP_MED_INVENTORY_MGMT_TLV is enabled */
        if (u2TlvsEnabled & LLDP_MED_INVENTORY_MGMT_TLV)
        {
            if (bCapEnable == LLDP_TRUE)
            {
                CliPrintf (CliHandle, ", ");
            }
            CliPrintf (CliHandle, "Inventory");
        }
        CliPrintf (CliHandle, "\r\n");
    }
    return;
}

 /**************************************************************************
 *     FUNCTION NAME    : LldpMedCliShowPolicyInfo
 *
 *     DESCRIPTION      : Displays the Local Media Policy Information
 *
 *     INPUT            : CliHandle      - CliContext ID
 *                        i4IfIndex      - Interface Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 ***************************************************************************/
PUBLIC VOID
LldpMedCliShowPolicyInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    INT4                i4PolicyUnKnown = 0;
    INT4                i4VlanType = 0;
    INT4                i4VlanId = 0;
    INT4                i4Priority = 0;
    INT4                i4Dscp = 0;
    INT4                i4Index = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4Counter = 1;
    INT4                i4RowStatus = 0;
    UINT1               u1AppType = 0;
    UINT1               u1NextAppType = 0;
    tSNMP_OCTET_STRING_TYPE LldpMedPolicyAppType;
    tSNMP_OCTET_STRING_TYPE NextLldpMedPolicyAppType;

    MEMSET (&LldpMedPolicyAppType, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextLldpMedPolicyAppType, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    LldpMedPolicyAppType.pu1_OctetList = &u1AppType;
    LldpMedPolicyAppType.i4_Length = LLDP_MED_MAX_LEN_APP_TYPE;

    NextLldpMedPolicyAppType.pu1_OctetList = &u1NextAppType;
    NextLldpMedPolicyAppType.i4_Length = LLDP_MED_MAX_LEN_APP_TYPE;

    /* Assign the input value of Interface Index for displaying the
     * values of only that port */
    i4Index = i4IfIndex;

    if (nmhGetNextIndexFsLldpMedLocMediaPolicyTable (i4IfIndex,
                                                     &i4NextIfIndex,
                                                     &LldpMedPolicyAppType,
                                                     &NextLldpMedPolicyAppType)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Application Type              : \r\n");
        CliPrintf (CliHandle, "Unknown Policy Flag           : \r\n");
        CliPrintf (CliHandle, "VlanType                      : \r\n");
        CliPrintf (CliHandle, "VlanID                        : \r\n");
        CliPrintf (CliHandle, "Priority                      : \r\n");
        CliPrintf (CliHandle, "Dscp                          : \r\n");
        return;
    }

    do
    {
        /* Assign Next Indices to current for checking whether they are of
         * same port */
        i4IfIndex = i4NextIfIndex;
        u1AppType = 0;
        MEMSET (&LldpMedPolicyAppType, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        LldpMedPolicyAppType.pu1_OctetList = &u1AppType;
        LldpMedPolicyAppType.i4_Length = LLDP_MED_MAX_LEN_APP_TYPE;

        MEMCPY (LldpMedPolicyAppType.pu1_OctetList,
                NextLldpMedPolicyAppType.pu1_OctetList, sizeof (UINT1));
        LldpMedPolicyAppType.i4_Length = NextLldpMedPolicyAppType.i4_Length;

        u1NextAppType = 0;
        MEMSET (&NextLldpMedPolicyAppType, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        NextLldpMedPolicyAppType.pu1_OctetList = &u1NextAppType;
        NextLldpMedPolicyAppType.i4_Length = LLDP_MED_MAX_LEN_APP_TYPE;

        if (nmhGetFsLldpMedLocMediaPolicyRowStatus (i4IfIndex,
                                                    &LldpMedPolicyAppType,
                                                    &i4RowStatus) !=
            SNMP_SUCCESS)
        {
            return;
        }

        /* Check whether it is of same port given in input */
        if ((i4IfIndex == i4Index) && (i4RowStatus == ACTIVE))
        {
            CliPrintf (CliHandle, "Network Policy %d \r\n", i4Counter);

            /* Get the value of Unknown Flag */
            if (nmhGetFsLldpMedLocMediaPolicyUnknown (i4IfIndex,
                                                      &LldpMedPolicyAppType,
                                                      &i4PolicyUnKnown) !=
                SNMP_SUCCESS)
            {
                return;
            }
            /* Get the value of VlanType */
            if (nmhGetFsLldpMedLocMediaPolicyTagged (i4IfIndex,
                                                     &LldpMedPolicyAppType,
                                                     &i4VlanType) !=
                SNMP_SUCCESS)
            {
                return;
            }

            /* Get the Value of VlanId */
            if (nmhGetFsLldpMedLocMediaPolicyVlanID (i4IfIndex,
                                                     &LldpMedPolicyAppType,
                                                     &i4VlanId) != SNMP_SUCCESS)
            {
                return;
            }

            /* Get the Value of Priority */
            if (nmhGetFsLldpMedLocMediaPolicyPriority (i4IfIndex,
                                                       &LldpMedPolicyAppType,
                                                       &i4Priority) !=
                SNMP_SUCCESS)
            {
                return;
            }

            /* Get the value of Dscp */
            if (nmhGetFsLldpMedLocMediaPolicyDscp (i4IfIndex,
                                                   &LldpMedPolicyAppType,
                                                   &i4Dscp) != SNMP_SUCCESS)
            {
                return;
            }

            CliPrintf (CliHandle, "Application Type              : ");

            switch (u1AppType)
            {
                case LLDP_MED_VOICE_APP:

                    CliPrintf (CliHandle, "Voice \r\n");
                    break;

                case LLDP_MED_VOICE_SIGNALING_APP:

                    CliPrintf (CliHandle, "Voice Signaling \r\n");
                    break;

                case LLDP_MED_GUEST_VOICE_APP:

                    CliPrintf (CliHandle, "Guest Voice \r\n");
                    break;

                case LLDP_MED_GUEST_VOICE_SIGNALING_APP:

                    CliPrintf (CliHandle, "Guest Voice Signaling \r\n");
                    break;

                case LLDP_MED_SOFT_PHONE_VOICE_APP:

                    CliPrintf (CliHandle, "Soft Phone Voice \r\n");
                    break;

                case LLDP_MED_VIDEO_CONFERENCING_APP:

                    CliPrintf (CliHandle, "Video Conferencing \r\n");
                    break;

                case LLDP_MED_STREAMING_VIDEO_APP:

                    CliPrintf (CliHandle, "Streaming Video \r\n");
                    break;

                case LLDP_MED_VIDEO_SIGNALING_APP:

                    CliPrintf (CliHandle, "Video Signaling \r\n");
                    break;

                default:
                    break;
            }

            /* Check whether Unknown policy flag is enabled or not */
            if ((BOOL1) i4PolicyUnKnown == LLDP_MED_UNKNOWN_FLAG)
            {
                CliPrintf (CliHandle, "Unknown Policy Flag           : "
                           "Enabled \r\n");
            }
            else
            {
                CliPrintf (CliHandle, "Unknown Policy Flag           : "
                           "Disabled \r\n");
            }
            /* Check whether Vlan is Tagged or Untagged */
            if (i4VlanType == LLDP_MED_VLAN_TAGGED)
            {
                CliPrintf (CliHandle, "Vlan Type                     : Tagged"
                           "\r\n");

            }
            else
            {
                CliPrintf (CliHandle, "Vlan Type                     : Untagged"
                           "\r\n");
            }

            CliPrintf (CliHandle, "VlanId                        : %d\r\n",
                       i4VlanId);
            CliPrintf (CliHandle, "Priority                      : %d\r\n",
                       i4Priority);
            CliPrintf (CliHandle, "Dscp                          : %d\r\n",
                       i4Dscp);

            i4Counter++;

        }

    }
    while (nmhGetNextIndexFsLldpMedLocMediaPolicyTable (i4IfIndex,
                                                        &i4NextIfIndex,
                                                        &LldpMedPolicyAppType,
                                                        &NextLldpMedPolicyAppType)
           == SNMP_SUCCESS);

    return;
}

 /**************************************************************************
 *     FUNCTION NAME    : LldpMedCliShowLocationInfo
 *
 *     DESCRIPTION      : Displays the Local Location Identification Information
 *
 *     INPUT            : CliHandle      - CliContext ID
 *                        i4IfIndex      - Interface Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 ***************************************************************************/
PUBLIC VOID
LldpMedCliShowLocationInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    tSNMP_OCTET_STRING_TYPE LocationInfo;
    UINT1               au1LocationId[LLDP_MED_MAX_LOC_LENGTH];
    UINT1               au1CaValue[LLDP_MED_MAX_CA_TYPE_LENGTH];
    UINT1               au1CountryCode[3];
    CONST CHR1         *pu1CaType = NULL;
    CONST CHR1         *pu1RowStatus = NULL;
    CONST CHR1         *pu1What = NULL;
    DBL8                d8latitude = 0;
    DBL8                d8longitude = 0;
    DBL8                d8altitude = 0;
    INT4                i4LocationSubtype = 0;
    INT4                i4Index = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4NextLocationSubtype = 0;
    INT4                i4RowStatus = 0;
    UINT1               u1latres = 0;
    UINT1               u1longres = 0;
    UINT1               u1altres = 0;
    UINT1               u1alttype = 0;
    UINT1               u1datum = 0;
    UINT1               u1CaLength = 0;
    UINT1               u1LocOffset = 0;

    /* Assign the input value of Interface Index for displaying the
     * values of only that port */
    i4Index = i4IfIndex;
    MEMSET (&au1LocationId, 0, LLDP_MED_MAX_LOC_LENGTH);
    MEMSET (&au1CaValue, 0, LLDP_MED_MAX_CA_TYPE_LENGTH);
    MEMSET (&au1CountryCode, 0, 3);

    if (nmhGetNextIndexLldpXMedLocLocationTable (i4IfIndex,
                                                 &i4NextIfIndex,
                                                 i4LocationSubtype,
                                                 &i4NextLocationSubtype) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Location Subtype              : \r\n");
        CliPrintf (CliHandle, "Location Info                 : \r\n");
        return;
    }

    do
    {
        /* Assign Next Indices to current for checking whether they are of
         * same port */
        i4IfIndex = i4NextIfIndex;
        i4LocationSubtype = i4NextLocationSubtype;

        /* Reset Location Id for different index on same port */
        MEMSET (&LocationInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (&au1LocationId, 0, LLDP_MED_MAX_LOC_LENGTH);
        LocationInfo.pu1_OctetList = au1LocationId;
        LocationInfo.i4_Length = 0;

        if (nmhGetFsLldpMedLocLocationRowStatus (i4IfIndex,
                                                 i4LocationSubtype,
                                                 &i4RowStatus) != SNMP_SUCCESS)
        {
            return;
        }

        if ((i4IfIndex == i4Index))
        {

            if (nmhGetLldpXMedLocLocationInfo (i4IfIndex,
                                               i4LocationSubtype,
                                               &LocationInfo) != SNMP_SUCCESS)
            {
                return;
            }

            switch (i4LocationSubtype)
            {
                case LLDP_MED_COORDINATE_LOC:

                    CliPrintf (CliHandle, "Location Subtype              : "
                               "Coordinate Location \r\n");
                    LldpMedUtlCoordinateLocDecode (&u1latres, &d8latitude,
                                                   &u1longres, &d8longitude,
                                                   &u1alttype, &u1altres,
                                                   &d8altitude, &u1datum,
                                                   au1LocationId);
                    CliPrintf (CliHandle,
                               "Latitude Resolution           : " "%d \r\n",
                               u1latres);
                    CliPrintf (CliHandle,
                               "Latitude                      : " "%lf \r\n",
                               d8latitude);

                    CliPrintf (CliHandle, "Longitude Resolution          : "
                               "%d \r\n", u1longres);
                    CliPrintf (CliHandle, "Longitude                     : "
                               "%lf \r\n", d8longitude);
                    if (u1alttype == LLDP_MED_ALT_METER)
                    {
                        CliPrintf (CliHandle, "Altitude Type                 : "
                                   "Meters\r\n");
                    }
                    else if (u1alttype == LLDP_MED_ALT_FLOOR)
                    {
                        CliPrintf (CliHandle, "Altitude Type                 : "
                                   "Floors\r\n");
                    }
                    CliPrintf (CliHandle, "Altitude Resolution           : "
                               "%d \r\n", u1altres);
                    CliPrintf (CliHandle, "Altitude                      : "
                               "%lf \r\n", d8altitude);
                    CliPrintf (CliHandle, "Datum                         : "
                               "%d \r\n", u1datum);
                    break;

                case LLDP_MED_CIVIC_LOC:

                    CliPrintf (CliHandle, "Location Subtype              : "
                               "Civic Location \r\n");

                    pu1RowStatus = LldpMedUtlRowStatusToStr (i4RowStatus);
                    if (pu1RowStatus != NULL)
                    {
                        CliPrintf (CliHandle, "Civic Location Status         : "
                                   "%s \r\n", pu1RowStatus);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "Civic Location Status         : "
                                   "\r\n");
                    }
                    CliPrintf (CliHandle, "LCI Length                    : "
                               "%d \r\n", au1LocationId[0]);

                    pu1What = LldpMedUtlCivicWhatToStr (au1LocationId[1]);

                    if (pu1What != NULL)
                    {
                        CliPrintf (CliHandle, "What                          : "
                                   "%s \r\n", pu1What);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "What                          : "
                                   "\r\n");
                    }

                    MEMCPY (&au1CountryCode, &au1LocationId[2], 2);
                    CliPrintf (CliHandle, "Country Code                  : "
                               "%s \r\n", au1CountryCode);
                    MEMSET (&au1CountryCode, 0, 2);
                    /* Moving the CA Type offset to first CA Type */
                    u1LocOffset = 4;
                    while (u1LocOffset <= au1LocationId[0])
                    {
                        pu1CaType =
                            LldpMedUtlCaTypeToStr (au1LocationId[u1LocOffset]);
                        if (pu1CaType != NULL)
                        {
                            CliPrintf (CliHandle,
                                       "CA Type                       : "
                                       "%s \r\n", pu1CaType);
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "CA Type                       : "
                                       "\r\n");
                        }

                        u1LocOffset++;
                        u1CaLength = au1LocationId[u1LocOffset];
                        if (u1CaLength != 0)
                        {
                            CliPrintf (CliHandle,
                                       "CA Length                     : "
                                       "%d \r\n", au1LocationId[u1LocOffset++]);

                            MEMCPY (&au1CaValue, &au1LocationId[u1LocOffset],
                                    u1CaLength);
                            CliPrintf (CliHandle,
                                       "CA Value                      : "
                                       "%s \r\n", au1CaValue);
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "CA Length                     : "
                                       "%d \r\n", au1LocationId[u1LocOffset++]);

                            CliPrintf (CliHandle,
                                       "CA Value                      : "
                                       "\r\n");
                        }
                        MEMSET (&au1CaValue, 0, LLDP_MED_MAX_CA_TYPE_LENGTH);
                        u1LocOffset = (UINT1) (u1LocOffset + u1CaLength);

                    }
                    break;

                case LLDP_MED_ELIN_LOC:

                    CliPrintf (CliHandle, "Location Subtype              : "
                               "Elin Location \r\n");

                    CliPrintf (CliHandle, "Elin Id                       : "
                               "%s \r\n", au1LocationId);
                    break;

                default:
                    break;

            }
        }

    }
    while (nmhGetNextIndexLldpXMedLocLocationTable (i4IfIndex,
                                                    &i4NextIfIndex,
                                                    i4LocationSubtype,
                                                    &i4NextLocationSubtype) ==
           SNMP_SUCCESS);

    return;
}

/**************************************************************************
 *     FUNCTION NAME    : LldpMedCliShowPowerInfo
 *
 *     DESCRIPTION      : Displays the Local Power-via-MDI Information
 *
 *     INPUT            : CliHandle      - CliContext ID
 *                        i4IfIndex      - Interface Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 ***************************************************************************/
PUBLIC VOID
LldpMedCliShowPowerInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT4               u4PowerValue = 0;
    INT4                i4PowerPriority = 0;

    /* Get Power Priority */
    if (nmhGetLldpXMedLocXPoEPSEPortPDPriority (i4IfIndex, &i4PowerPriority)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Power Priority                :  \r\n");
    }
    /* Get Power Value */
    if (nmhGetLldpXMedLocXPoEPSEPortPowerAv (i4IfIndex, &u4PowerValue)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Power Value                   :  \r\n");
        return;
    }

    /* Print power Priority type */
    if (i4PowerPriority == LLDP_MED_PRI_CRITICAL_MIB)
    {
        CliPrintf (CliHandle, "Power Priority                : "
                   "Critical \r\n");
    }
    else if (i4PowerPriority == LLDP_MED_PRI_HIGH_MIB)
    {
        CliPrintf (CliHandle, "Power Priority                : " "High \r\n");
    }
    else if (i4PowerPriority == LLDP_MED_PRI_LOW_MIB)
    {
        CliPrintf (CliHandle, "Power Priority                : " "Low \r\n");
    }

    /* Print Power Value */
    CliPrintf (CliHandle, "Power Value                   : "
               "%d \r\n", u4PowerValue);
    return;

}

 /**************************************************************************
 *     FUNCTION NAME    : LldpMedCliShowInventoryInfo
 *
 *     DESCRIPTION      : Displays the Inventory Information
 *
 *     INPUT            : CliHandle      - CliContext ID
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 ***************************************************************************/
PUBLIC VOID
LldpMedCliShowInventoryInfo (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE InventoryInfo;
    UINT1               au1InventoryInfo[LLDP_MED_HW_REV_LEN];

    MEMSET (&au1InventoryInfo, 0, LLDP_MED_HW_REV_LEN);
    MEMSET (&InventoryInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InventoryInfo.pu1_OctetList = au1InventoryInfo;
    InventoryInfo.i4_Length = 0;

    /*Get Hardware Revision Information */
    if (nmhGetLldpXMedLocHardwareRev (&InventoryInfo) != SNMP_SUCCESS)
    {
        return;
    }
    CliPrintf (CliHandle, "Hardware Revision             : %s\r\n",
               au1InventoryInfo);

    MEMSET (&au1InventoryInfo, 0, LLDP_MED_HW_REV_LEN);
    MEMSET (&InventoryInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InventoryInfo.pu1_OctetList = au1InventoryInfo;
    InventoryInfo.i4_Length = 0;

    /*Get Firmware Revision Information */
    if (nmhGetLldpXMedLocFirmwareRev (&InventoryInfo) != SNMP_SUCCESS)
    {
        return;
    }
    CliPrintf (CliHandle, "Firmware Revision             : %s\r\n",
               au1InventoryInfo);

    MEMSET (&au1InventoryInfo, 0, LLDP_MED_HW_REV_LEN);
    MEMSET (&InventoryInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InventoryInfo.pu1_OctetList = au1InventoryInfo;
    InventoryInfo.i4_Length = 0;

    /*Get Software Revision Information */
    if (nmhGetLldpXMedLocSoftwareRev (&InventoryInfo) != SNMP_SUCCESS)
    {
        return;
    }
    CliPrintf (CliHandle, "Software Revision             : %s\r\n",
               au1InventoryInfo);

    MEMSET (&au1InventoryInfo, 0, LLDP_MED_HW_REV_LEN);
    MEMSET (&InventoryInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InventoryInfo.pu1_OctetList = au1InventoryInfo;
    InventoryInfo.i4_Length = 0;

    /*Get Serial Number */
    if (nmhGetLldpXMedLocSerialNum (&InventoryInfo) != SNMP_SUCCESS)
    {
        return;
    }
    CliPrintf (CliHandle, "Serial Number                 : %s\r\n",
               au1InventoryInfo);

    MEMSET (&au1InventoryInfo, 0, LLDP_MED_HW_REV_LEN);
    MEMSET (&InventoryInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InventoryInfo.pu1_OctetList = au1InventoryInfo;
    InventoryInfo.i4_Length = 0;

    /* Get Manufacturer Name */
    if (nmhGetLldpXMedLocMfgName (&InventoryInfo) != SNMP_SUCCESS)
    {
        return;
    }
    CliPrintf (CliHandle, "Manufacturer Name             : %s\r\n",
               au1InventoryInfo);

    MEMSET (&au1InventoryInfo, 0, LLDP_MED_HW_REV_LEN);
    MEMSET (&InventoryInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InventoryInfo.pu1_OctetList = au1InventoryInfo;
    InventoryInfo.i4_Length = 0;

    /* Get Model Name */
    if (nmhGetLldpXMedLocModelName (&InventoryInfo) != SNMP_SUCCESS)
    {
        return;
    }
    CliPrintf (CliHandle, "Model Name                    : %s\r\n",
               au1InventoryInfo);

    MEMSET (&au1InventoryInfo, 0, LLDP_MED_HW_REV_LEN);
    MEMSET (&InventoryInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InventoryInfo.pu1_OctetList = au1InventoryInfo;
    InventoryInfo.i4_Length = 0;

    /* Get Asset Id */
    if (nmhGetLldpXMedLocAssetID (&InventoryInfo) != SNMP_SUCCESS)
    {
        return;
    }
    CliPrintf (CliHandle, "Asset Id                      : %s\r\n",
               au1InventoryInfo);

    return;

}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowLocDot3LinkAggStatus
 *
 *     DESCRIPTION      : Displays the Link Aggregation status of Local
 *                        LLDP system.
 *
 *     INPUT            : CliHandle      - CliContext ID
 *                        pDot3LAStatus  - Link Aggregation Status
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 ***************************************************************************/

PRIVATE VOID
LldpCliShowLocDot3LinkAggStatus (tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE *
                                 pDot3LAStatus)
{
    CliPrintf (CliHandle, "Capability & Status           : ");
    if (pDot3LAStatus->pu1_OctetList[0] & LLDP_AGG_CAP_BMAP)
    {
        CliPrintf (CliHandle, "Capable, ");
    }
    else
    {
        CliPrintf (CliHandle, "Not Capable, ");
    }
    if (pDot3LAStatus->pu1_OctetList[0] & LLDP_AGG_STATUS_BMAP)
    {
        CliPrintf (CliHandle, "In Aggregation\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Not In Aggregation\r\n");
    }
    return;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowLocEnabledTlvTx
 *
 *     DESCRIPTION      : Displays the Enabled Basic, Dot1, Dot3
 *                        Tlvs on a given port of the Local LLDP System
 *
 *     INPUT            : CliHandle      - CliContext ID
 *                        i4IfIndex      - Interface Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 ***************************************************************************/

PRIVATE VOID
LldpCliShowLocEnabledTlvTx (tCliHandle CliHandle, INT4 i4IfIndex,
                            UINT4 u4DestMacIndex)
{
    UINT1               u1BasicTlvTxEnable = 0;
    UINT1               u1Dot3TlvTxEnable = 0;
    UINT1               u1BasicString = 0;
    UINT1               u1Dot3String = 0;
    UINT1               u1FirstLineLen = 0;
    BOOL1               bFirst = OSIX_FALSE;
    INT4                i4Dot1PortVlanTlvTxEnable = 0;
    INT4                i4BasicTlvEnLen = 0;
    tSNMP_OCTET_STRING_TYPE BasicTlvsTxEnable;
    tSNMP_OCTET_STRING_TYPE Dot3TlvTxEnable;

    MEMSET (&BasicTlvsTxEnable, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&Dot3TlvTxEnable, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&BasicTlvsTxEnable, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&Dot3TlvTxEnable, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    BasicTlvsTxEnable.pu1_OctetList = &u1BasicString;
    BasicTlvsTxEnable.i4_Length = sizeof (u1BasicString);
    Dot3TlvTxEnable.pu1_OctetList = &u1Dot3String;
    Dot3TlvTxEnable.i4_Length = sizeof (u1Dot3String);

    nmhGetLldpV2PortConfigTLVsTxEnable (i4IfIndex, u4DestMacIndex,
                                        &BasicTlvsTxEnable);
    nmhGetLldpV2Xdot3PortConfigTLVsTxEnable (i4IfIndex, u4DestMacIndex,
                                             &Dot3TlvTxEnable);

    MEMCPY (&u1BasicTlvTxEnable, BasicTlvsTxEnable.pu1_OctetList,
            BasicTlvsTxEnable.i4_Length);
    MEMCPY (&u1Dot3TlvTxEnable, Dot3TlvTxEnable.pu1_OctetList,
            Dot3TlvTxEnable.i4_Length);
    CliPrintf (CliHandle, "Enabled Tx Tlvs               : ");
    u1FirstLineLen = (UINT1) (STRLEN ("Enabled Tx Tlvs                : "));
    i4BasicTlvEnLen = u1FirstLineLen;

    if (u1BasicTlvTxEnable != 0)
    {
        if (u1BasicTlvTxEnable & LLDP_PORT_DESC_TLV_ENABLED)
        {
            i4BasicTlvEnLen += STRLEN ("Port Description");
            CliPrintf (CliHandle, "Port Description");
            bFirst = OSIX_TRUE;
        }
        if (u1BasicTlvTxEnable & LLDP_SYS_NAME_TLV_ENABLED)
        {
            i4BasicTlvEnLen += STRLEN (", System Name");
            if (bFirst == OSIX_FALSE)
            {
                CliPrintf (CliHandle, "System Name");
                bFirst = OSIX_TRUE;
            }
            else
            {
                CliPrintf (CliHandle, ", System Name");
            }
        }
        if (u1BasicTlvTxEnable & LLDP_SYS_DESC_TLV_ENABLED)
        {
            i4BasicTlvEnLen += STRLEN (", System Description");
            if (bFirst == OSIX_FALSE)
            {
                CliPrintf (CliHandle, "System Description");
                bFirst = OSIX_TRUE;
            }
            else if (i4BasicTlvEnLen >= CLI_MAX_COLS)
            {
                CliPrintf (CliHandle, ", \r\n");
                CliPrintf (CliHandle, "%-32s", " ");
                CliPrintf (CliHandle, "System Description");
                i4BasicTlvEnLen = (INT4) (u1FirstLineLen +
                                          STRLEN ("System Description"));
            }
            else
            {
                CliPrintf (CliHandle, ", System Description");
            }
        }
        if (u1BasicTlvTxEnable & LLDP_SYS_CAPAB_TLV_ENABLED)
        {
            i4BasicTlvEnLen += STRLEN (", System Capability");
            if (bFirst == OSIX_FALSE)
            {
                CliPrintf (CliHandle, "System Capability");
                bFirst = OSIX_TRUE;
            }
            else if (i4BasicTlvEnLen >= CLI_MAX_COLS)
            {
                CliPrintf (CliHandle, ", \r\n");
                CliPrintf (CliHandle, "%-32s", " ");
                CliPrintf (CliHandle, "System Capability");
                i4BasicTlvEnLen = u1FirstLineLen + STRLEN ("System Capability");
            }
            else
            {
                CliPrintf (CliHandle, ", System Capability");
            }
        }
    }

    if (LldpCliIsManAddrTlvEnabled (i4IfIndex, u4DestMacIndex) == LLDP_TRUE)
    {
        i4BasicTlvEnLen += STRLEN (", Management Address");
        if (bFirst == OSIX_FALSE)
        {
            CliPrintf (CliHandle, "Management Address");
            bFirst = OSIX_TRUE;
        }
        else if (i4BasicTlvEnLen >= CLI_MAX_COLS)
        {
            CliPrintf (CliHandle, ", \r\n");
            CliPrintf (CliHandle, "%-32s", " ");
            CliPrintf (CliHandle, "Management Address");
            i4BasicTlvEnLen = u1FirstLineLen + STRLEN ("Management Address");
        }
        else
        {
            CliPrintf (CliHandle, ", Management Address");
        }
    }

    /* Enabled Local Dot1 Tlvs */
    nmhGetLldpV2Xdot1ConfigPortVlanTxEnable (i4IfIndex, u4DestMacIndex,
                                             &i4Dot1PortVlanTlvTxEnable);
    if (i4Dot1PortVlanTlvTxEnable == LLDP_TRUE)
    {
        i4BasicTlvEnLen += STRLEN (", Port Vlan");
        if (bFirst == OSIX_FALSE)
        {
            CliPrintf (CliHandle, "Port Vlan");
            bFirst = OSIX_TRUE;
        }
        else if (i4BasicTlvEnLen >= CLI_MAX_COLS)
        {
            CliPrintf (CliHandle, ", \r\n");
            CliPrintf (CliHandle, "%-32s", " ");
            CliPrintf (CliHandle, "Port Vlan");
            i4BasicTlvEnLen = u1FirstLineLen + STRLEN ("Port Vlan");
        }
        else
        {
            CliPrintf (CliHandle, ", Port Vlan");
        }
    }
    /* Enabled Local Dot3 Tlvs */
    if (u1Dot3TlvTxEnable != 0)
    {
        if (u1Dot3TlvTxEnable & LLDP_MAC_PHY_TLV_ENABLED)
        {
            i4BasicTlvEnLen += STRLEN (", Mac Phy");
            if (bFirst == OSIX_FALSE)
            {
                CliPrintf (CliHandle, "Mac Phy");
                bFirst = OSIX_TRUE;
            }
            else if (i4BasicTlvEnLen >= CLI_MAX_COLS)
            {
                CliPrintf (CliHandle, ", \r\n");
                CliPrintf (CliHandle, "%-32s", " ");
                CliPrintf (CliHandle, "Mac Phy");
                i4BasicTlvEnLen = u1FirstLineLen + STRLEN ("Mac Phy");
            }
            else
            {
                CliPrintf (CliHandle, ", Mac Phy");
            }
        }
        if (u1Dot3TlvTxEnable & LLDP_LINK_AGG_TLV_ENABLED)
        {
            i4BasicTlvEnLen += STRLEN (", Link Aggregation");
            if (bFirst == OSIX_FALSE)
            {
                CliPrintf (CliHandle, "Link Aggregation");
                bFirst = OSIX_TRUE;
            }
            else if (i4BasicTlvEnLen >= CLI_MAX_COLS)
            {
                CliPrintf (CliHandle, ", \r\n");
                CliPrintf (CliHandle, "%-32s", " ");
                CliPrintf (CliHandle, "Link Aggregation");
                i4BasicTlvEnLen = u1FirstLineLen + STRLEN ("Link Aggregation");
            }
            else
            {
                CliPrintf (CliHandle, ", Link Aggregation");
            }
        }
        if (u1Dot3TlvTxEnable & LLDP_MAX_FRAME_SIZE_TLV_ENABLED)
        {
            i4BasicTlvEnLen += STRLEN (", Max Frame Size");
            if (bFirst == OSIX_FALSE)
            {
                CliPrintf (CliHandle, "Max Frame Size");
                bFirst = OSIX_TRUE;
            }
            else if (i4BasicTlvEnLen >= CLI_MAX_COLS)
            {
                CliPrintf (CliHandle, ", \r\n");
                CliPrintf (CliHandle, "%-32s", " ");
                CliPrintf (CliHandle, "Max Frame Size");
                i4BasicTlvEnLen = u1FirstLineLen + STRLEN ("Max Frame Size");
            }
            else
            {
                CliPrintf (CliHandle, ", Max Frame Size");
            }
        }
    }
    CliPrintf (CliHandle, "\r\n");
    return;
}

/**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliIsManAddrTlvEnabled
 *
 *     DESCRIPTION      : This functionn verifies that whether management
 *                        address TLV is enabled for transmission or not on the
 *                        given port. It returns LLDP_TRUE, If transmission of
 *                        management address TLV is enabled, Else returns
 *                        LLDP_FALSE.
 *
 *     INPUT            : i4IfIndex - Interface Index
 *                        u4DestMacIndex - Destination Mac Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : LLDP_TRUE/LLDP_FALSE
 *
 ***************************************************************************/
PRIVATE UINT1
LldpCliIsManAddrTlvEnabled (INT4 i4IfIndex, UINT4 u4DestMacIndex)
{
    INT4                i4ManAddrSubtype = 0;
    INT4                i4PrevManAddrSubtype = 0;
    INT4                i4Result = 0;
    UINT1               au1ManAddr[LLDP_MAX_LEN_MAN_ADDR];
    UINT1               au1PrevManAddr[LLDP_MAX_LEN_MAN_ADDR];
    UINT1               au1ManAddrTlvTxEnable[LLDP_MAN_ADDR_TX_EN_MAX_BYTES];
    tSNMP_OCTET_STRING_TYPE ManAddr;
    tSNMP_OCTET_STRING_TYPE PrevManAddr;
    tSNMP_OCTET_STRING_TYPE ManAddrTlvTxEnable;

    MEMSET (au1ManAddr, 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMSET (au1PrevManAddr, 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMSET (au1ManAddrTlvTxEnable, 0, LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
    MEMSET (&ManAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PrevManAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ManAddrTlvTxEnable, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    ManAddr.pu1_OctetList = au1ManAddr;
    PrevManAddr.pu1_OctetList = au1PrevManAddr;
    ManAddrTlvTxEnable.pu1_OctetList = au1ManAddrTlvTxEnable;

    if (nmhGetFirstIndexLldpV2LocManAddrTable (&i4ManAddrSubtype,
                                               &ManAddr) == SNMP_FAILURE)
    {
        return LLDP_FALSE;
    }

    do
    {
        MEMSET (&ManAddrTlvTxEnable, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        ManAddrTlvTxEnable.pu1_OctetList = au1ManAddrTlvTxEnable;

        nmhGetLldpV2ManAddrConfigTxEnable (i4IfIndex, u4DestMacIndex,
                                           i4ManAddrSubtype,
                                           &ManAddr, &i4Result);

        if (i4Result == OSIX_TRUE)
        {
            return LLDP_TRUE;
        }
        i4PrevManAddrSubtype = i4ManAddrSubtype;
        PrevManAddr.i4_Length = ManAddr.i4_Length;
        MEMCPY (PrevManAddr.pu1_OctetList, ManAddr.pu1_OctetList,
                ManAddr.i4_Length);
    }
    while (nmhGetNextIndexLldpV2LocManAddrTable (i4PrevManAddrSubtype,
                                                 &i4ManAddrSubtype,
                                                 &PrevManAddr,
                                                 &ManAddr) == SNMP_SUCCESS);
    return LLDP_FALSE;
}

/**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowLocProtoVlanInfo
 *
 *     DESCRIPTION      : Displays information about Proto Vlan Tlv of
 *                        LLDP system
 *
 *     INPUT            : CliHandle - CliContext ID
 *                        i4IfIndex - Interface Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 *****************************************************************************/

PRIVATE VOID
LldpCliShowLocProtoVlanInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    INT4                i4LocPortNum = 0;
    INT4                i4LocProtoVlanId = 0;
    INT4                i4PrevLocPortNum = 0;
    INT4                i4PrevLocProtoVlanId = 0;
    INT4                i4LocProtoVlanSupported = 0;
    INT4                i4LocProtoVlanEnabled = 0;
    INT4                i4LocProtoVlanTxEnabled = 0;

    if (nmhGetFirstIndexLldpV2Xdot1LocProtoVlanTable (&i4LocPortNum,
                                                      (UINT4 *)
                                                      &i4LocProtoVlanId) !=
        SNMP_SUCCESS)
    {
        return;
    }

    CliPrintf (CliHandle, "%-20s %-10s %-24s %-14s\r\n", "Protocol VLAN Id",
               "Support", "Protocol VLAN Status", "TxStatus");
    CliPrintf (CliHandle, "%-20s %-10s %-24s %-14s\r\n", "----------------",
               "-------", "--------------------", "--------");
    do
    {
        nmhGetLldpV2Xdot1LocProtoVlanSupported (i4LocPortNum, i4LocProtoVlanId,
                                                &i4LocProtoVlanSupported);
        nmhGetLldpV2Xdot1LocProtoVlanEnabled (i4LocPortNum, i4LocProtoVlanId,
                                              &i4LocProtoVlanEnabled);
        nmhGetLldpV2Xdot1ConfigProtoVlanTxEnable (i4LocPortNum,
                                                  i4LocProtoVlanId,
                                                  &i4LocProtoVlanTxEnabled);

        if (i4IfIndex == i4LocPortNum)
        {
            CliPrintf (CliHandle, "%-20d %-10s %-24s %-14s",
                       i4LocProtoVlanId,
                       gapc1Support[i4LocProtoVlanSupported],
                       gapc1Enable[i4LocProtoVlanEnabled],
                       gapc1Enable[i4LocProtoVlanTxEnabled]);
            CliPrintf (CliHandle, "\r\n");
        }
        i4PrevLocPortNum = i4LocPortNum;
        i4PrevLocProtoVlanId = i4LocProtoVlanId;

    }
    while (nmhGetNextIndexLldpV2Xdot1LocProtoVlanTable (i4PrevLocPortNum,
                                                        &i4LocPortNum,
                                                        i4PrevLocProtoVlanId,
                                                        (UINT4 *)
                                                        &i4LocProtoVlanId) ==
           SNMP_SUCCESS);

    return;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowLocVlanNameInfo
 *
 *     DESCRIPTION      : Displays information about Vlan Name Tlv of
 *                        LLDP system
 *
 *     INPUT            : CliHandle - CliContext ID
 *                        i4IfIndex - Interface Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 *****************************************************************************/

PRIVATE VOID
LldpCliShowLocVlanNameInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT1               au1VlanName[VLAN_STATIC_MAX_NAME_LEN + 1];
    INT4                i4LocPortNum = 0;
    INT4                i4LocVlanId = 0;
    INT4                i4PrevLocPortNum = 0;
    INT4                i4PrevLocVlanId = 0;
    INT4                i4LocVlanNameEnabled = 0;
    tSNMP_OCTET_STRING_TYPE LocVlanName;

    MEMSET (&LocVlanName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1VlanName, 0, VLAN_STATIC_MAX_NAME_LEN + 1);

    LocVlanName.pu1_OctetList = au1VlanName;
    LocVlanName.i4_Length = VLAN_STATIC_MAX_NAME_LEN + 1;

    if (nmhGetFirstIndexLldpV2Xdot1ConfigVlanNameTable (&i4LocPortNum,
                                                        &i4LocVlanId)
        != SNMP_SUCCESS)
    {
        return;
    }

    CliPrintf (CliHandle, "%-14s %-34s %-10s\r\n", "Vlan Id",
               "Vlan Name", "TxStatus");
    CliPrintf (CliHandle, "%-14s %-34s %-10s\r\n", "-------",
               "---------", "--------");
    do
    {
        nmhGetLldpV2Xdot1LocVlanName (i4LocPortNum, i4LocVlanId, &LocVlanName);
        nmhGetLldpV2Xdot1ConfigVlanNameTxEnable (i4LocPortNum, i4LocVlanId,
                                                 &i4LocVlanNameEnabled);

        if (i4IfIndex == i4LocPortNum)
        {
            /* Displays VlanId, VlanName & VlanTxEnable Status */
            CliPrintf (CliHandle, "%-14d %-34s %-10s\r\n",
                       i4LocVlanId,
                       LocVlanName.pu1_OctetList,
                       gapc1Enable[i4LocVlanNameEnabled]);
        }
        i4PrevLocPortNum = i4LocPortNum;
        i4PrevLocVlanId = i4LocVlanId;

    }
    while ((nmhGetNextIndexLldpV2Xdot1ConfigVlanNameTable (i4PrevLocPortNum,
                                                           &i4LocPortNum,
                                                           i4PrevLocVlanId,
                                                           &i4LocVlanId))
           == SNMP_SUCCESS);

    return;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowLocalManAddr
 *
 *     DESCRIPTION      : Displays information about local Management address
 *                        of LLDP system
 *
 *     INPUT            : CliHandle - CliContext ID
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliShowLocalManAddr (tCliHandle CliHandle)
{
    tLldpLocManAddrTable *pManAddrNode = NULL;
    UINT1               au1TempManAddrVal[LLDP_MAX_LEN_MAN_ADDR];
    UINT1               au1PrevTempManAddrVal[LLDP_MAX_LEN_MAN_ADDR];
    UINT1               au1LocManAddrTxEn[LLDP_MAN_ADDR_TX_EN_MAX_BYTES];
    CHR1                ac1DispManAddr[LLDP_MAX_LEN_MAN_ADDR];
    CHR1               *pc1DispManAddr = NULL;
    UINT4               u4ManAddr = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4LocManAddrSubtype = 0;
    INT4                i4PrevLocManAddrSubtype = 0;
    tSNMP_OCTET_STRING_TYPE LocManAddr;
    tSNMP_OCTET_STRING_TYPE PrevLocManAddr;
    tSNMP_OCTET_STRING_TYPE ManAddrTlvTxEnable;
    UINT1               u1ManAddrType = 0;
    INT4                i4OperStatus = 0;

    MEMSET (&LocManAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PrevLocManAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ManAddrTlvTxEnable, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (au1TempManAddrVal, 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMSET (au1PrevTempManAddrVal, 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMSET (au1LocManAddrTxEn, 0, LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
    MEMSET (&ac1DispManAddr, 0, LLDP_MAX_LEN_MAN_ADDR);

    LocManAddr.pu1_OctetList = au1TempManAddrVal;
    PrevLocManAddr.pu1_OctetList = au1PrevTempManAddrVal;
    ManAddrTlvTxEnable.pu1_OctetList = au1LocManAddrTxEn;

    if (nmhGetFirstIndexLldpV2LocManAddrTable (&i4LocManAddrSubtype,
                                               &LocManAddr) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "%-31s %-47s\r\n", "Management Address",
               "TxEnabledPorts");
    CliPrintf (CliHandle, "%-31s %-47s\r\n", "------------------",
               "--------------");
    do
    {
        pc1DispManAddr = &ac1DispManAddr[0];
        switch (i4LocManAddrSubtype)
        {
            case IPVX_ADDR_FMLY_IPV4:
                PTR_FETCH4 (u4ManAddr, LocManAddr.pu1_OctetList);
                /* Convert octet values into strings of octets
                 * seperated by dot */
                CLI_CONVERT_IPADDR_TO_STR (pc1DispManAddr, u4ManAddr);
                break;
            case IPVX_ADDR_FMLY_IPV6:
                STRNCPY (pc1DispManAddr,
                         Ip6PrintNtop ((tIp6Addr *) (VOID *) LocManAddr.
                                       pu1_OctetList),
                         STRLEN (Ip6PrintNtop
                                 ((tIp6Addr *) (VOID *) LocManAddr.
                                  pu1_OctetList)));
                pc1DispManAddr[STRLEN
                               (Ip6PrintNtop
                                ((tIp6Addr *) (VOID *) LocManAddr.
                                 pu1_OctetList))] = '\0';
                break;
            default:
                break;
        }

        /* If the oper status of management address node is not enable,
         * get the next node
         */
        pManAddrNode = (tLldpLocManAddrTable *)
            LldpTxUtlGetLocManAddrNode (u1ManAddrType,
                                        LocManAddr.pu1_OctetList);
        nmhGetFsLldpManAddrConfigOperStatus (i4LocManAddrSubtype,
                                             &LocManAddr, &i4OperStatus);

        if (i4OperStatus != IPIF_OPER_ENABLE)
        {
            i4PrevLocManAddrSubtype = i4LocManAddrSubtype;
            PrevLocManAddr.i4_Length = LocManAddr.i4_Length;
            MEMCPY (PrevLocManAddr.pu1_OctetList, LocManAddr.pu1_OctetList,
                    LocManAddr.i4_Length);
            continue;
        }
        /* Local Management Address */
        CliPrintf (CliHandle, "%-31s", pc1DispManAddr);
        i4PrevLocManAddrSubtype = i4LocManAddrSubtype;

        PrevLocManAddr.i4_Length = LocManAddr.i4_Length;
        MEMCPY (PrevLocManAddr.pu1_OctetList, LocManAddr.pu1_OctetList,
                LocManAddr.i4_Length);

        /* Clear management address tlv tx enable */
        MEMSET (ManAddrTlvTxEnable.pu1_OctetList, 0,
                LLDP_MAN_ADDR_TX_EN_MAX_BYTES);

        nmhGetLldpConfigManAddrPortsTxEnable (i4LocManAddrSubtype,
                                              &LocManAddr, &ManAddrTlvTxEnable);
        LldpCliOctetToIfName (CliHandle, NULL, &ManAddrTlvTxEnable,
                              LLDP_MAX_DEST_INDEX,
                              LLDP_MAN_ADDR_TX_EN_MAX_BYTES, 31,
                              &u4PagingStatus, 4);
    }
    while (nmhGetNextIndexLldpV2LocManAddrTable (i4PrevLocManAddrSubtype,
                                                 &i4LocManAddrSubtype,
                                                 &PrevLocManAddr,
                                                 &LocManAddr) == SNMP_SUCCESS);

    UNUSED_PARAM (pManAddrNode);

    return CLI_SUCCESS;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowErrors
 *
 *     DESCRIPTION      : Displays the information about the errors like
 *                        memory allocation failures, queue overflows, table
 *                        overflows, etc
 *
 *     INPUT            : CliHandle - CliContext ID
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/

PUBLIC INT4
LldpCliShowErrors (tCliHandle CliHandle)
{
    INT4                i4MemAllocFailure = 0;
    INT4                i4InputQOverFlows = 0;
    UINT4               u4RemTableOverFlows = 0;

    nmhGetFsLldpMemAllocFailure (&i4MemAllocFailure);
    nmhGetFsLldpInputQOverFlows (&i4InputQOverFlows);

    nmhGetLldpV2StatsRemTablesDrops (&u4RemTableOverFlows);

    CliPrintf (CliHandle, "Total Memory Allocation Failures  : %d\n",
               i4MemAllocFailure);
    CliPrintf (CliHandle, "Total Input Queue Overflows       : %d\n",
               i4InputQOverFlows);
    CliPrintf (CliHandle, "Total Table Overflows             : %d\n",
               u4RemTableOverFlows);

    return CLI_SUCCESS;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowStatistics
 *
 *     DESCRIPTION      : Displays the LLDP Statistics information
 *
 *     INPUT            : CliHandle - CliContext ID
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS
 *
 ***************************************************************************/

PUBLIC INT4
LldpCliShowStatistics (tCliHandle CliHandle)
{
    UINT4               u4RemTabsLastChgTime = 0;
    UINT4               u4RemTabsInserts = 0;
    UINT4               u4RemTablesDeletes = 0;
    UINT4               u4RemTablesDrops = 0;
    UINT4               u4RemTablesAgeouts = 0;
    UINT4               u4RemTablesUpdates = 0;

    nmhGetLldpV2StatsRemTablesLastChangeTime (&u4RemTabsLastChgTime);
    nmhGetLldpV2StatsRemTablesInserts (&u4RemTabsInserts);
    nmhGetLldpV2StatsRemTablesDeletes (&u4RemTablesDeletes);
    nmhGetLldpV2StatsRemTablesDrops (&u4RemTablesDrops);
    nmhGetLldpV2StatsRemTablesAgeouts (&u4RemTablesAgeouts);
    nmhGetFsLldpStatsRemTablesUpdates (&u4RemTablesUpdates);

    CliPrintf (CliHandle, "Remote Table Last Change Time : %d\r\n",
               u4RemTabsLastChgTime);
    CliPrintf (CliHandle, "Remote Table Inserts          : %d\r\n",
               u4RemTabsInserts);
    CliPrintf (CliHandle, "Remote Table Deletes          : %d\r\n",
               u4RemTablesDeletes);
    CliPrintf (CliHandle, "Remote Table Drops            : %d\r\n",
               u4RemTablesDrops);
    CliPrintf (CliHandle, "Remote Table Ageouts          : %d\r\n",
               u4RemTablesAgeouts);
    CliPrintf (CliHandle, "Remote Table Updates          : %d\r\n",
               u4RemTablesUpdates);

    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetAdminStatus
 *
 *     DESCRIPTION      : Enables/Disable transmission/reception or both
 *                        of LLDP frames on an interface
 *
 *     INPUT            : CliHandle      -   CliContext ID
 *                        i4IfIndex      -   Interface Index
 *                        i4AdminStatus  -   LLDP Admin Status
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliSetAdminStatus (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4AdminStatus,
                       UINT4 u4DestMacAddrIndex, UINT1 u1InterfaceFlag)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4CurrentAdminStatus = 0;
    INT4                i4Index = i4IfIndex;
    INT4                i4PrevIfIndex = 0;
    UINT4               u4PrevDestMacAddInd = 0;

    if (u4DestMacAddrIndex == 0)
    {
        u4DestMacAddrIndex = LLDP_V1_DEST_MAC_ADDR_INDEX (i4IfIndex);
    }
    do
    {
        if (u1InterfaceFlag == OSIX_TRUE)
        {
            if (i4IfIndex != i4Index)
            {
                return CLI_SUCCESS;
            }
        }

        if (nmhGetLldpV2PortConfigAdminStatus (i4IfIndex, u4DestMacAddrIndex,
                                               &i4CurrentAdminStatus) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* Depends upon current admin status, sets admin status */
        switch (i4AdminStatus)
        {
            case LLDP_CLI_TRANSMIT:

                if (i4CurrentAdminStatus == LLDP_ADM_STAT_RX_ONLY)
                {
                    if (nmhTestv2LldpV2PortConfigAdminStatus
                        (&u4ErrorCode, i4IfIndex, u4DestMacAddrIndex,
                         LLDP_ADM_STAT_TX_AND_RX) == SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n%%Unable to set Admin Status\r\n");
                        return CLI_FAILURE;
                    }
                    if (nmhSetLldpV2PortConfigAdminStatus
                        (i4IfIndex, u4DestMacAddrIndex,
                         LLDP_ADM_STAT_TX_AND_RX) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return CLI_FAILURE;
                    }
                }
                else if (i4CurrentAdminStatus == LLDP_ADM_STAT_DISABLED)
                {
                    if (nmhTestv2LldpV2PortConfigAdminStatus
                        (&u4ErrorCode, i4IfIndex, u4DestMacAddrIndex,
                         LLDP_ADM_STAT_TX_ONLY) == SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n%%Unable to set Admin Status\r\n");
                        return CLI_FAILURE;
                    }
                    if (nmhSetLldpV2PortConfigAdminStatus
                        (i4IfIndex, u4DestMacAddrIndex,
                         LLDP_ADM_STAT_TX_ONLY) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return CLI_FAILURE;
                    }
                }
                break;

            case LLDP_CLI_RECEIVE:

                if (i4CurrentAdminStatus == LLDP_ADM_STAT_TX_ONLY)
                {
                    if (nmhTestv2LldpV2PortConfigAdminStatus
                        (&u4ErrorCode, i4IfIndex, u4DestMacAddrIndex,
                         LLDP_ADM_STAT_TX_AND_RX) == SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n%%Unable to set Admin Status\r\n");
                        return CLI_FAILURE;
                    }
                    if (nmhSetLldpV2PortConfigAdminStatus
                        (i4IfIndex, u4DestMacAddrIndex,
                         LLDP_ADM_STAT_TX_AND_RX) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return CLI_FAILURE;
                    }
                }
                else if (i4CurrentAdminStatus == LLDP_ADM_STAT_DISABLED)
                {
                    if (nmhTestv2LldpV2PortConfigAdminStatus (&u4ErrorCode,
                                                              i4IfIndex,
                                                              u4DestMacAddrIndex,
                                                              LLDP_ADM_STAT_RX_ONLY)
                        == SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n%%Unable to set Admin Status\r\n");
                        return CLI_FAILURE;
                    }
                    if (nmhSetLldpV2PortConfigAdminStatus
                        (i4IfIndex, u4DestMacAddrIndex,
                         LLDP_ADM_STAT_RX_ONLY) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return CLI_FAILURE;
                    }
                }
                break;

            case LLDP_CLI_NO_TRANSMIT:

                if (i4CurrentAdminStatus == LLDP_ADM_STAT_TX_ONLY)
                {
                    if (nmhTestv2LldpV2PortConfigAdminStatus
                        (&u4ErrorCode, i4IfIndex, u4DestMacAddrIndex,
                         LLDP_ADM_STAT_DISABLED) == SNMP_FAILURE)
                    {
                        if (u4ErrorCode == SNMP_ERR_WRONG_VALUE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n%%LLDP MED is Enabled, So Unable to set LLDP Admin Status\r\n");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\n%%Unable to set Admin Status\r\n");
                        }

                        return CLI_FAILURE;
                    }

                    if (nmhSetLldpV2PortConfigAdminStatus
                        (i4IfIndex, u4DestMacAddrIndex,
                         LLDP_ADM_STAT_DISABLED) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return CLI_FAILURE;
                    }
                }
                else if (i4CurrentAdminStatus == LLDP_ADM_STAT_TX_AND_RX)
                {
                    if (nmhTestv2LldpV2PortConfigAdminStatus
                        (&u4ErrorCode, i4IfIndex, u4DestMacAddrIndex,
                         LLDP_ADM_STAT_RX_ONLY) == SNMP_FAILURE)
                    {
                        if (u4ErrorCode == SNMP_ERR_WRONG_VALUE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n%%LLDP MED is Enabled, So Unable to set LLDP Admin Status\r\n");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\n%%Unable to set Admin Status\r\n");
                        }

                        return CLI_FAILURE;
                    }

                    if (nmhSetLldpV2PortConfigAdminStatus
                        (i4IfIndex, u4DestMacAddrIndex,
                         LLDP_ADM_STAT_RX_ONLY) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return CLI_FAILURE;
                    }
                }
                break;

            case LLDP_CLI_NO_RECEIVE:

                if (i4CurrentAdminStatus == LLDP_ADM_STAT_RX_ONLY)
                {
                    if (nmhTestv2LldpV2PortConfigAdminStatus
                        (&u4ErrorCode, i4IfIndex, u4DestMacAddrIndex,
                         LLDP_ADM_STAT_DISABLED) == SNMP_FAILURE)
                    {
                        if (u4ErrorCode == SNMP_ERR_WRONG_VALUE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n%%LLDP MED is Enabled, So Unable to set LLDP Admin Status\r\n");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\n%%Unable to set Admin Status\r\n");
                        }

                        return CLI_FAILURE;
                    }

                    if (nmhSetLldpV2PortConfigAdminStatus
                        (i4IfIndex, u4DestMacAddrIndex,
                         LLDP_ADM_STAT_DISABLED) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return CLI_FAILURE;
                    }
                }
                else if (i4CurrentAdminStatus == LLDP_ADM_STAT_TX_AND_RX)
                {
                    if (nmhTestv2LldpV2PortConfigAdminStatus
                        (&u4ErrorCode, i4IfIndex, u4DestMacAddrIndex,
                         LLDP_ADM_STAT_TX_ONLY) == SNMP_FAILURE)
                    {
                        if (u4ErrorCode == SNMP_ERR_WRONG_VALUE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n%%LLDP MED is Enabled, So Unable to set LLDP Admin Status\r\n");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\n%%Unable to set Admin Status\r\n");
                        }

                        return CLI_FAILURE;
                    }

                    if (nmhSetLldpV2PortConfigAdminStatus
                        (i4IfIndex, u4DestMacAddrIndex,
                         LLDP_ADM_STAT_TX_ONLY) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return CLI_FAILURE;
                    }
                }
                break;
            default:
                LLDP_TRC (ALL_FAILURE_TRC, "LldpCliSetAdminStatus: "
                          "Received invalid option \r\n");
                break;
        }

        if (u1InterfaceFlag == OSIX_TRUE)
        {
            i4PrevIfIndex = i4IfIndex;
            u4PrevDestMacAddInd = u4DestMacAddrIndex;
        }
        else
        {
            break;
        }

    }
    while (nmhGetNextIndexLldpV2PortConfigTable (i4PrevIfIndex, &i4IfIndex,
                                                 u4PrevDestMacAddInd,
                                                 &u4DestMacAddrIndex) ==
           SNMP_SUCCESS);

    return CLI_SUCCESS;
}

 /****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetNotification
 *
 *     DESCRIPTION      : Enables/Disable LLDP trap notification on an
 *                        interface
 *
 *     INPUT            : CliHandle    -   CliContext ID
 *                        i4IfIndex    -   Interface Index
 *                        i4Status     -   Port Admin Status
 *                        i4NotifyType -   Notification Type
 *                                         (Remote tabel change,
 *                                         Misconfiguration)
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliSetNotification (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Status,
                        INT4 i4NotifyType, UINT4 u4DestMacAddrIndex,
                        UINT1 u1InterfaceFlag)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4Index = i4IfIndex;
    INT4                i4PrevIfIndex = 0;
    UINT4               u4PrevDestMacAddInd = 0;
    UINT4               u4LocPort = 0;

    do
    {
        if (u1InterfaceFlag == OSIX_TRUE)
        {
            if (i4IfIndex != i4Index)
            {
                return CLI_SUCCESS;
            }
        }

        /*  Notification Enable/Disable Status */
        if (nmhTestv2LldpV2PortConfigNotificationEnable
            (&u4ErrorCode, i4IfIndex, u4DestMacAddrIndex,
             i4Status) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetLldpV2PortConfigNotificationEnable
            (i4IfIndex, u4DestMacAddrIndex, i4Status) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        u4LocPort =
            LldpGetLocalPortFromIfIndexDestMacIfIndex ((UINT4) i4IfIndex,
                                                       u4DestMacAddrIndex);
        /* Notification Type */
        if ((i4NotifyType == LLDP_REMOTE_CHG_MIS_CFG_NOTIFICATION) ||
            (i4NotifyType == LLDP_REMOTE_CHG_NOTIFICATION) ||
            (i4NotifyType == LLDP_MIS_CFG_NOTIFICATION))
        {
            if (nmhTestv2FsLldpPortConfigNotificationType
                (&u4ErrorCode, (INT4) u4LocPort, i4NotifyType) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsLldpPortConfigNotificationType
                ((INT4) u4LocPort, i4NotifyType) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }

        if (u1InterfaceFlag == OSIX_TRUE)
        {
            i4PrevIfIndex = i4IfIndex;
            u4PrevDestMacAddInd = u4DestMacAddrIndex;
        }
        else
        {
            break;
        }

    }
    while (nmhGetNextIndexLldpV2PortConfigTable (i4PrevIfIndex, &i4IfIndex,
                                                 u4PrevDestMacAddInd,
                                                 &u4DestMacAddrIndex) ==
           SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetTLV
 *
 *     DESCRIPTION      : Configures basic TLV types transmitted on a
 *                        given port
 *
 *     INPUT            : CliHandle         -  CliContext ID
 *                        i4IfIndex         -  Interface Index
 *                        pu1Tlv            -  Enabled Basic TLV set
 *                        i4MgmtAddrSubType -  Management Address sub type
 *                        u4ManAddr         -  Management Address
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliSetTLV (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 *pu1Tlv,
               INT4 i4MgmtAddrSubType, UINT1 *pu1ManAddr,
               UINT4 u4DestMacAddrIndex, UINT1 u1InterfaceFlag)
{
    UINT1               au1TempManAddrVal[LLDP_MAX_LEN_MAN_ADDR];
    UINT1               au1PrevTempManAddrVal[LLDP_MAX_LEN_MAN_ADDR];
    UINT1               au1ManAddrTlvTxEnable[LLDP_MAN_ADDR_TX_EN_MAX_BYTES];
    UINT4               u4ErrorCode = 0;
    INT4                i4ManAddrAll = LLDP_FALSE;
    INT4                i4LocManAddrSubtype = 0;
    INT4                i4PrevLocManAddrSubtype = 0;
    tSNMP_OCTET_STRING_TYPE LldpTlvEnabled;
    tSNMP_OCTET_STRING_TYPE LocManAddr;
    tSNMP_OCTET_STRING_TYPE PrevLocManAddr;
    tSNMP_OCTET_STRING_TYPE ManAddrTlvTxEnable;
    tLldpLocPortInfo   *pPortInfo = NULL;
    tMacAddr            DestMacAddress;
    UINT4               u4LocPortNum = 0;

    MEMSET (&LocManAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PrevLocManAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ManAddrTlvTxEnable, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (au1TempManAddrVal, 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMSET (au1PrevTempManAddrVal, 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMSET (au1ManAddrTlvTxEnable, 0, LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
    MEMSET (&DestMacAddress, 0, MAC_ADDR_LEN);

    UNUSED_PARAM (u1InterfaceFlag);
    LocManAddr.pu1_OctetList = au1TempManAddrVal;
    PrevLocManAddr.pu1_OctetList = au1PrevTempManAddrVal;

    ManAddrTlvTxEnable.pu1_OctetList = au1ManAddrTlvTxEnable;
    ManAddrTlvTxEnable.i4_Length = LLDP_MAN_ADDR_TX_EN_MAX_BYTES;

    /* get local port information structure */
    nmhGetFslldpV2DestMacAddress (u4DestMacAddrIndex, &DestMacAddress);
    nmhGetFslldpv2ConfigPortMapNum (i4IfIndex, DestMacAddress,
                                    (INT4 *) &u4LocPortNum);
    pPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPortNum);
    if (pPortInfo == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LocPortInfo Entry not found \r\n");
        return CLI_FAILURE;
    }
    if ((*pu1Tlv) & LLDP_MAN_ADDR_TLV_ENABLED)
    {
        pPortInfo->u1ManAddrTlvTxEnabled = LLDP_TRUE;
        if (i4MgmtAddrSubType == 0)
        {
            i4ManAddrAll = LLDP_TRUE;
        }
        *pu1Tlv = ((*pu1Tlv) & (UINT1) (~LLDP_MAN_ADDR_TLV_ENABLED));
    }
    /* mask new value with current value */
    *pu1Tlv = (pPortInfo->PortConfigTable.u1TLVsTxEnable) | (*pu1Tlv);
    LldpTlvEnabled.pu1_OctetList = pu1Tlv;
    LldpTlvEnabled.i4_Length = sizeof (UINT1);
    if (nmhTestv2LldpV2PortConfigTLVsTxEnable
        (&u4ErrorCode, i4IfIndex, u4DestMacAddrIndex,
         &LldpTlvEnabled) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set PortConfigTLVsTxEnable\r\n");
        return CLI_FAILURE;
    }

    nmhGetLldpV2DestMacAddress (u4DestMacAddrIndex, &DestMacAddress);
    nmhGetFslldpv2ConfigPortMapNum (i4IfIndex, DestMacAddress,
                                    (INT4 *) &u4LocPortNum);
    /* Set Management Address */
    if (i4ManAddrAll == LLDP_TRUE)
    {
        if (nmhGetFirstIndexLldpV2LocManAddrTable (&i4LocManAddrSubtype,
                                                   &LocManAddr) == SNMP_FAILURE)
        {
            /* As per standard IEEE802.1AB - LLDP, if no management address
             * is present in RBTree, hence construct preformed LLDPDU with
             * system mac address */
            pPortInfo->u1ManAddrTlvTxEnabled = LLDP_FALSE;
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set LldpConfigManAddrPortsTxEnable; No ip address configured\r\n");
            return CLI_FAILURE;
        }
        do
        {
            /* Clear management address tlv tx enable */
            MEMSET (ManAddrTlvTxEnable.pu1_OctetList, 0,
                    LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
            /* Coverity Fix */
            if (nmhGetLldpConfigManAddrPortsTxEnable (i4LocManAddrSubtype,
                                                      &LocManAddr,
                                                      &ManAddrTlvTxEnable)
                != SNMP_SUCCESS)
            {
            }
            /* Enables Local Management Address to Transmit */
            OSIX_BITLIST_SET_BIT (ManAddrTlvTxEnable.pu1_OctetList,
                                  (INT4) u4LocPortNum,
                                  LLDP_MAN_ADDR_TX_EN_MAX_BYTES);

            if (nmhSetLldpConfigManAddrPortsTxEnable (i4LocManAddrSubtype,
                                                      &LocManAddr,
                                                      &ManAddrTlvTxEnable)
                != SNMP_SUCCESS)
            {
            }
            i4PrevLocManAddrSubtype = i4LocManAddrSubtype;
            MEMCPY (PrevLocManAddr.pu1_OctetList,
                    LocManAddr.pu1_OctetList, LocManAddr.i4_Length);
            PrevLocManAddr.i4_Length = LocManAddr.i4_Length;
        }
        while (nmhGetNextIndexLldpLocManAddrTable
               (i4PrevLocManAddrSubtype, &i4LocManAddrSubtype,
                &PrevLocManAddr, &LocManAddr) == SNMP_SUCCESS);
    }
    if (i4MgmtAddrSubType != 0)
    {
        /* Enables the propagation of the given ipv4, ipv6 address in the
         * Management address TLV for a particular port */
        if (LldpCliSetMgmtAddrTLV
            (CliHandle, (INT4) u4LocPortNum, i4MgmtAddrSubType,
             pu1ManAddr, LLDP_TRUE) != CLI_SUCCESS)
        {
            pPortInfo->u1ManAddrTlvTxEnabled = LLDP_FALSE;
            return CLI_FAILURE;
        }
    }

    if (nmhSetLldpV2PortConfigTLVsTxEnable
        (i4IfIndex, u4DestMacAddrIndex, &LldpTlvEnabled) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set PortConfigTLVsTxEnable\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliResetTLV
 *
 *     DESCRIPTION      : Disables basic TLV types transmitted on a
 *                        given port
 *
 *     INPUT            : CliHandle         -  CliContext ID
 *                        i4IfIndex         -  Interface Index
 *                        pu1Tlv            -  Enabled Basic TLV set
 *                        i4MgmtAddrSubType -  Management Address sub type
 *                        u4ManAddr         -  Management Address
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliResetTLV (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 *pu1Tlv,
                 INT4 i4MgmtAddrSubType, UINT1 *pu1ManAddr,
                 UINT4 u4DestMacAddrIndex, UINT1 u1InterfaceFlag)
{
    UINT1               au1TempManAddrVal[LLDP_MAX_LEN_MAN_ADDR];
    UINT1               au1PrevTempManAddrVal[LLDP_MAX_LEN_MAN_ADDR];
    UINT1               au1ManAddrTlvTxEnable[LLDP_MAN_ADDR_TX_EN_MAX_BYTES];
    UINT4               u4ErrorCode = 0;
    INT4                i4LocManAddrSubtype = 0;
    INT4                i4PrevLocManAddrSubtype = 0;
    tSNMP_OCTET_STRING_TYPE LldpTlvEnabled;
    tSNMP_OCTET_STRING_TYPE LocManAddr;
    tSNMP_OCTET_STRING_TYPE PrevLocManAddr;
    tSNMP_OCTET_STRING_TYPE ManAddrTlvTxEnable;
    tMacAddr            DestMacAddress;
    tLldpLocPortInfo   *pPortInfo = NULL;
    INT4                i4Index = i4IfIndex;
    INT4                i4PrevIfIndex = 0;
    UINT4               u4PrevDestMacAddInd = 0;
    UINT4               u4LocPortNum = 0;
    UINT1               u1Tlv = *pu1Tlv;

    MEMSET (&LocManAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PrevLocManAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ManAddrTlvTxEnable, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&DestMacAddress, 0, MAC_ADDR_LEN);

    MEMSET (au1TempManAddrVal, 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMSET (au1PrevTempManAddrVal, 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMSET (au1ManAddrTlvTxEnable, 0, LLDP_MAN_ADDR_TX_EN_MAX_BYTES);

    LocManAddr.pu1_OctetList = au1TempManAddrVal;
    PrevLocManAddr.pu1_OctetList = au1PrevTempManAddrVal;

    ManAddrTlvTxEnable.pu1_OctetList = au1ManAddrTlvTxEnable;
    ManAddrTlvTxEnable.i4_Length = LLDP_MAN_ADDR_TX_EN_MAX_BYTES;

    /* get local port information structure */
    nmhGetFslldpV2DestMacAddress (u4DestMacAddrIndex, &DestMacAddress);
    nmhGetFslldpv2ConfigPortMapNum (i4IfIndex, DestMacAddress,
                                    (INT4 *) &u4LocPortNum);
    pPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPortNum);
    if (pPortInfo == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LocPortInfo Entry not found \r\n");
        return CLI_FAILURE;
    }
    do
    {
        *pu1Tlv = u1Tlv;
        if ((u1InterfaceFlag == OSIX_TRUE) && (i4IfIndex != i4Index))
        {
            i4PrevIfIndex = i4IfIndex;
            u4PrevDestMacAddInd = u4DestMacAddrIndex;
        }
        else if ((u1InterfaceFlag != OSIX_TRUE) ||
                 ((u1InterfaceFlag == OSIX_TRUE) && (i4IfIndex == i4Index)))
        {
            nmhGetLldpV2DestMacAddress (u4DestMacAddrIndex, &DestMacAddress);
            if (LldpUtlGetLocalPort (i4IfIndex, DestMacAddress, &u4LocPortNum)
                == OSIX_FAILURE)
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                          "i4IfIndex or DestMacAddr is Invalid  \r\n");
            }

            if ((*pu1Tlv) & LLDP_MAN_ADDR_TLV_ENABLED)
            {
                pPortInfo->u1ManAddrTlvTxEnabled = LLDP_FALSE;
                *pu1Tlv = ((*pu1Tlv) & (UINT1) (~LLDP_MAN_ADDR_TLV_ENABLED));
                /* Reset management address enable bit for all management
                 * addresses enabled on this port */
                if (nmhGetFirstIndexLldpLocManAddrTable (&i4LocManAddrSubtype,
                                                         &LocManAddr) ==
                    SNMP_FAILURE)
                {
                    pPortInfo->u1ManAddrTlvTxEnabled = LLDP_FALSE;
                    CliPrintf (CliHandle,
                               "\r\n%%Unable to set LldpConfigManAddrPortsTxEnable; No ip address configured\r\n");
                    return CLI_FAILURE;
                }
                do
                {
                    /* Clear management address tlv tx enable */
                    MEMSET (ManAddrTlvTxEnable.pu1_OctetList, 0,
                            LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
                    /* Coverity Fix */
                    if (nmhGetLldpConfigManAddrPortsTxEnable
                        (i4LocManAddrSubtype, &LocManAddr,
                         &ManAddrTlvTxEnable) != SNMP_SUCCESS)
                    {
                    }
                    /* Enables Local Management Address to Transmit */
                    OSIX_BITLIST_RESET_BIT (ManAddrTlvTxEnable.pu1_OctetList,
                                            i4IfIndex,
                                            LLDP_MAN_ADDR_TX_EN_MAX_BYTES);

                    if (nmhSetLldpConfigManAddrPortsTxEnable
                        (i4LocManAddrSubtype, &LocManAddr,
                         &ManAddrTlvTxEnable) != SNMP_SUCCESS)
                    {
                    }

                    i4PrevLocManAddrSubtype = i4LocManAddrSubtype;
                    MEMCPY (PrevLocManAddr.pu1_OctetList,
                            LocManAddr.pu1_OctetList, LocManAddr.i4_Length);
                    PrevLocManAddr.i4_Length = LocManAddr.i4_Length;

                }
                while (nmhGetNextIndexLldpLocManAddrTable
                       (i4PrevLocManAddrSubtype, &i4LocManAddrSubtype,
                        &PrevLocManAddr, &LocManAddr) == SNMP_SUCCESS);
            }
            /* mask the bits to be reset */
            *pu1Tlv = (UINT1) (~(*pu1Tlv));
            /* mask/reset bit 7, bit 6, bit 5 - these bits are unused, hence should
             * be reset always */
            *pu1Tlv = *pu1Tlv & LLDP_MAX_BASIC_TLV_TXENABLE;
            /* mask new value with current value */
            *pu1Tlv = (pPortInfo->PortConfigTable.u1TLVsTxEnable) & (*pu1Tlv);
            LldpTlvEnabled.pu1_OctetList = pu1Tlv;
            LldpTlvEnabled.i4_Length = sizeof (UINT1);
            if (i4MgmtAddrSubType != 0)
            {
                /* Disables propagation of the given ipv4, ipv6 address in the
                 * Management address TLV for a particular port */
                if (LldpCliSetMgmtAddrTLV
                    (CliHandle, i4IfIndex, i4MgmtAddrSubType, pu1ManAddr,
                     LLDP_FALSE) != CLI_SUCCESS)
                {
                    return CLI_FAILURE;
                }
            }
            if (nmhTestv2LldpV2PortConfigTLVsTxEnable
                (&u4ErrorCode, i4IfIndex, u4DestMacAddrIndex,
                 &LldpTlvEnabled) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to set PortConfigTLVsTxEnable\r\n");
                return CLI_FAILURE;
            }
            if (nmhSetLldpV2PortConfigTLVsTxEnable
                (i4IfIndex, u4DestMacAddrIndex,
                 &LldpTlvEnabled) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to set PortConfigTLVsTxEnable\r\n");
                return CLI_FAILURE;
            }

            i4PrevIfIndex = i4IfIndex;
            u4PrevDestMacAddInd = u4DestMacAddrIndex;
        }
    }
    while ((nmhGetNextIndexLldpV2PortConfigTable (i4PrevIfIndex, &i4IfIndex,
                                                  u4PrevDestMacAddInd,
                                                  &u4DestMacAddrIndex) ==
            SNMP_SUCCESS) && (u1InterfaceFlag == OSIX_TRUE));

    return CLI_SUCCESS;
}

/******************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetMgmtAddrTLV
 *
 *     DESCRIPTION      : Configures propagation of the given ipv4, ipv6
 *                        addresses in the management address TLV for a
 *                        particular port
 *
 *     INPUT            : CliHandle         - CliContext ID
 *                        i4IfIndex         - Interface Index
 *                        i4MgmtAddrSubType - Management Address Subtype
 *                        u4ManAddr         - Management Address
 *                        i4Val             - Enable/Disable Management
 *                                            Address
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PRIVATE INT4
LldpCliSetMgmtAddrTLV (tCliHandle CliHandle, INT4 i4IfIndex,
                       INT4 i4MgmtAddrSubType, UINT1 *pu1ManAddr, INT4 i4Val)
{
    UINT1               au1PortListEnable[LLDP_MAN_ADDR_TX_EN_MAX_BYTES];
    UINT1               au1MgmtAddr[IPVX_IPV6_ADDR_LEN];
    UINT4               u4ErrorCode;
    tIp6Addr           *pTempIp6Addr = NULL;
    tSNMP_OCTET_STRING_TYPE MgmtAddr;
    tSNMP_OCTET_STRING_TYPE PortListEnable;

    MEMSET (&MgmtAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PortListEnable, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1PortListEnable, 0, LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
    MEMSET (au1MgmtAddr, 0, sizeof (au1MgmtAddr));

    PortListEnable.pu1_OctetList = (UINT1 *) &au1PortListEnable[0];
    MgmtAddr.pu1_OctetList = au1MgmtAddr;

    if (i4MgmtAddrSubType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (MgmtAddr.pu1_OctetList, pu1ManAddr, IPVX_IPV4_ADDR_LEN);
        MgmtAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    }
    if (i4MgmtAddrSubType == IPVX_ADDR_FMLY_IPV6)
    {
        MgmtAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
        pTempIp6Addr = str_to_ip6addr (pu1ManAddr);
        if (pTempIp6Addr != NULL)
        {
            MEMCPY (MgmtAddr.pu1_OctetList, (UINT1 *) pTempIp6Addr,
                    MgmtAddr.i4_Length);
        }
        else
        {
            CliPrintf (CliHandle,
                       "%%Unable to set LldpConfigManAddrPortsTxEnable\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhGetLldpConfigManAddrPortsTxEnable (i4MgmtAddrSubType,
                                              &MgmtAddr, &PortListEnable)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "%%Unable to set LldpConfigManAddrPortsTxEnable\r\n");
        return CLI_FAILURE;
    }
    if (i4Val == LLDP_TRUE)
    {
        OSIX_BITLIST_SET_BIT (PortListEnable.pu1_OctetList, i4IfIndex,
                              MEM_MAX_BYTES (PortListEnable.i4_Length,
                                             LLDP_MAN_ADDR_TX_EN_MAX_BYTES));
    }
    else
    {

        OSIX_BITLIST_RESET_BIT (PortListEnable.pu1_OctetList, i4IfIndex,
                                MEM_MAX_BYTES (PortListEnable.i4_Length,
                                               LLDP_MAN_ADDR_TX_EN_MAX_BYTES));
    }

    if (nmhTestv2LldpConfigManAddrPortsTxEnable (&u4ErrorCode,
                                                 i4MgmtAddrSubType,
                                                 &MgmtAddr,
                                                 &PortListEnable) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "%%Unable to set LldpConfigManAddrPortsTxEnable\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetLldpConfigManAddrPortsTxEnable (i4MgmtAddrSubType,
                                              &MgmtAddr, &PortListEnable)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "%%Unable to set LldpConfigManAddrPortsTxEnable\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetPortSubtype
 *
 *     DESCRIPTION      : Configures lldp port id subtype and port id value
 *
 *     INPUT            : CliHandle        -  CliContext ID
 *                        i4IfIndex        -  Interface Index
 *                        i4PortIdSubtype  -  Port id Sub Type
 *                        pu1PortId        -  Port id
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliSetPortSubtype (tCliHandle CliHandle, INT4 i4IfIndex,
                       INT4 i4PortIdSubtype, UINT1 *pu1PortId)
{
    UINT2               u2PortIdLen;
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE PortId;

    if (nmhTestv2FsLldpLocPortIdSubtype
        (&u4ErrorCode, i4IfIndex, i4PortIdSubtype) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsLldpLocPortIdSubtype (i4IfIndex, i4PortIdSubtype) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if ((i4PortIdSubtype == LLDP_PORT_ID_SUB_PORT_COMP)
        || (i4PortIdSubtype == LLDP_PORT_ID_SUB_LOCAL))
    {
        PortId.pu1_OctetList = pu1PortId;
        LldpUtilGetPortIdLen (i4PortIdSubtype, pu1PortId, &u2PortIdLen);
        PortId.i4_Length = u2PortIdLen;
        if (nmhTestv2FsLldpLocPortId (&u4ErrorCode, i4IfIndex, &PortId) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsLldpLocPortId (i4IfIndex, &PortId) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetDot1TLV
 *
 *     DESCRIPTION      : Configure dot1 TLV types on a port
 *
 *     INPUT            : CliHandle       -  CliContext ID
 *                        i4IfIndex       -  Inteface Index
 *                        i4PortVlanId    -  Port Vlan id TLV
 *                        pu2ProtoVlanId  -  Proto Vlan id TLV
 *                        pu1VlanName     -  Vlan Name TLV
 *                        i4SetVal        -  Tlv Enable or Disable Status
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliSetDot1TLV (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4PortVlanId,
                   UINT2 *pu2ProtoVlanId, UINT1 *pu1VlanName,
                   INT4 i4ProtoVlanAll, INT4 i4VlanNameAll, INT4 i4SetVal,
                   INT4 i4VidDigest, INT4 i4MgmtVidDigest, INT4 i4LinkAggTLV,
                   UINT4 u4DestMacAddrIndex, UINT1 u1InterfaceFlag)
{
    UINT4               u4ErrorCode = 0;
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    INT4                i4Index = i4IfIndex;
    INT4                i4PrevIfIndex = 0;
    UINT4               u4PrevDestMacAddInd = 0;
    UINT4               u4LocPortNum = 0;
    UINT1               u1LinkAggTLV = 0;
    tMacAddr            DestAddr;

    tLldpLocPortInfo    LocPort;
    MEMSET (&LocPort, 0, sizeof (tLldpLocPortInfo));

    MEMSET (&DestAddr, 0, sizeof (tMacAddr));

    if (i4LinkAggTLV == LLDP_V2_LINK_AGG_TLV)
    {
        u1LinkAggTLV |= LLDP_LINK_AGG_TLV_ENABLED;
        LldpCliSetDot3TLV (CliHandle, i4IfIndex, u1LinkAggTLV, i4SetVal);
    }

    do
    {
        if ((u1InterfaceFlag == OSIX_TRUE) && (i4IfIndex != i4Index))
        {
            i4PrevIfIndex = i4IfIndex;
            u4PrevDestMacAddInd = u4DestMacAddrIndex;
        }
        else if ((u1InterfaceFlag != OSIX_TRUE) ||
                 ((u1InterfaceFlag == OSIX_TRUE) && (i4IfIndex == i4Index)))
        {
            if (i4PortVlanId == LLDP_PORT_VLAN_ID_TLV)
            {
                if (nmhTestv2LldpV2Xdot1ConfigPortVlanTxEnable
                    (&u4ErrorCode, i4IfIndex, u4DestMacAddrIndex,
                     i4SetVal) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n%%Unable to set PortVlanTLVTxEnable\r\n");
                    return CLI_FAILURE;
                }
                if (nmhSetLldpV2Xdot1ConfigPortVlanTxEnable
                    (i4IfIndex, u4DestMacAddrIndex, i4SetVal) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n%%Unable to set PortVlanTLVTxEnable\r\n");
                    return CLI_FAILURE;
                }
            }
            if (i4ProtoVlanAll == LLDP_PROTO_VLAN_ID_TLV)
            {
                gu1TxAll = OSIX_TRUE;
                if (LldpCliSetAllProtoVlanIdTxVal (i4IfIndex, i4SetVal) ==
                    CLI_SUCCESS)
                {
                    nmhGetFslldpV2DestMacAddress (u4DestMacAddrIndex,
                                                  &DestAddr);
                    if (LldpUtlGetLocalPort (i4IfIndex, DestAddr, &u4LocPortNum)
                        == OSIX_FAILURE)
                    {
                        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                                  "i4IfIndex or DestMacAddr is Invalid  \r\n");
                    }
                    pLldpLocPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPortNum);
                    if (pLldpLocPortInfo != NULL)
                    {
                        if (LldpTxUtlHandleLocPortInfoChg (pLldpLocPortInfo)
                            != OSIX_SUCCESS)
                        {
                            LLDP_TRC (ALL_FAILURE_TRC,
                                      "LldpTxUtlHandleLocPortInfoChg: "
                                      "returns FAILURE \r\n");
                        }
                    }
                }
                else
                {
                    gu1TxAll = OSIX_FALSE;
                    return CLI_FAILURE;
                }
                gu1TxAll = OSIX_FALSE;
            }
            else if (pu2ProtoVlanId != NULL)
            {
                if (nmhTestv2LldpV2Xdot1ConfigProtoVlanTxEnable
                    (&u4ErrorCode, i4IfIndex,
                     *(INT4 *) (VOID *) (pu2ProtoVlanId),
                     i4SetVal) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n%%Unable to set ProtoVlanTLVTxEnable\r\n");
                    return CLI_FAILURE;
                }
                if (nmhSetLldpV2Xdot1ConfigProtoVlanTxEnable (i4IfIndex,
                                                              *(INT4 *) (VOID *)
                                                              pu2ProtoVlanId,
                                                              i4SetVal) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n%%Unable to set ProtoVlanTLVTxEnable\r\n");
                    return CLI_FAILURE;
                }
            }
            if (i4VlanNameAll == LLDP_VLAN_NAME_TLV)
            {
                gu1TxAll = OSIX_TRUE;

                /* nmhSet routine needs the VlanId value as Index, so
                 * Written utilty function to get vlanId and it calls
                 * nhmSet routine to set the value */
                if (LldpCliSetAllVlanNameTxVal (i4IfIndex, i4SetVal) ==
                    CLI_SUCCESS)
                {
                    nmhGetFslldpV2DestMacAddress (u4DestMacAddrIndex,
                                                  &DestAddr);
                    if (LldpUtlGetLocalPort (i4IfIndex, DestAddr, &u4LocPortNum)
                        == OSIX_FAILURE)
                    {
                        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                                  "i4IfIndex or DestMacAddr is Invalid  \r\n");
                    }
                    pLldpLocPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPortNum);
                    if (pLldpLocPortInfo != NULL)
                    {
                        if (LldpTxUtlHandleLocPortInfoChg (pLldpLocPortInfo)
                            != OSIX_SUCCESS)
                        {
                            LLDP_TRC (ALL_FAILURE_TRC,
                                      "LldpTxUtlHandleLocPortInfoChg: "
                                      "returns FAILURE \r\n");
                        }
                    }
                }
                else
                {
                    gu1TxAll = OSIX_FALSE;
                    return CLI_FAILURE;
                }
                gu1TxAll = OSIX_FALSE;
            }
            else if (pu1VlanName != NULL)
            {
                if (nmhTestv2LldpV2Xdot1ConfigVlanNameTxEnable
                    (&u4ErrorCode, i4IfIndex, (*(INT4 *) (VOID *) pu1VlanName),
                     i4SetVal) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n%%Unable to set VlanNameTLVTxEnable\r\n");
                    return CLI_FAILURE;
                }
                if (nmhSetLldpV2Xdot1ConfigVlanNameTxEnable (i4IfIndex,
                                                             (*(INT4 *) (VOID *)
                                                              pu1VlanName),
                                                             i4SetVal) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n%%Unable to set VlanNameTLVTxEnable\r\n");
                    return CLI_FAILURE;
                }
            }
            i4PrevIfIndex = i4IfIndex;
            u4PrevDestMacAddInd = u4DestMacAddrIndex;
            if (u1InterfaceFlag != OSIX_TRUE)
            {
                break;
            }
        }

    }
    while (nmhGetNextIndexLldpV2PortConfigTable (i4PrevIfIndex, &i4IfIndex,
                                                 u4PrevDestMacAddInd,
                                                 &u4DestMacAddrIndex) ==
           SNMP_SUCCESS);

    if (i4VidDigest == LLDP_VID_USAGE_DIGEST_TLV)
    {
        if (nmhTestv2LldpV2Xdot1ConfigVidUsageDigestTxEnable
            (&u4ErrorCode, i4Index, i4SetVal) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set UsageDigestTxEnable\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetLldpV2Xdot1ConfigVidUsageDigestTxEnable (i4Index, i4SetVal)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set UsageDigestTxEnable\r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (i4MgmtVidDigest == LLDP_MGMT_VID_TLV)
    {
        if (nmhTestv2LldpV2Xdot1ConfigManVidTxEnable
            (&u4ErrorCode, i4Index, i4SetVal) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set ManVidTxEnable\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetLldpV2Xdot1ConfigManVidTxEnable (i4Index, i4SetVal) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set ManVidTxEnable\r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetAllProtoVlanIdTxVal
 *
 *     DESCRIPTION      : Configures vlan Id transmission on a given port
 *
 *     INPUT            : i4IfIndex       -  Interface Index
 *                        i4TxStatusVal   -  Transmission status value
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliSetAllProtoVlanIdTxVal (INT4 i4IfIndex, INT4 i4TxStatusVal)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4LocPortNum = 0;
    INT4                i4PrevLocPortNum = 0;
    INT4                i4LocProtoVlanId = 0;
    INT4                i4PrevLocProtoVlanId = 0;

    if (nmhGetFirstIndexLldpXdot1ConfigProtoVlanTable (&i4LocPortNum,
                                                       &i4LocProtoVlanId)
        != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }
    do
    {
        if (i4IfIndex == i4LocPortNum)
        {
            if (nmhTestv2LldpXdot1ConfigProtoVlanTxEnable (&u4ErrorCode,
                                                           i4LocPortNum,
                                                           i4LocProtoVlanId,
                                                           i4TxStatusVal)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            if (nmhSetLldpXdot1ConfigProtoVlanTxEnable (i4IfIndex,
                                                        i4LocProtoVlanId,
                                                        i4TxStatusVal)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
        }
        i4PrevLocPortNum = i4LocPortNum;
        i4PrevLocProtoVlanId = i4LocProtoVlanId;
    }
    while (nmhGetNextIndexLldpXdot1ConfigProtoVlanTable (i4PrevLocPortNum,
                                                         &i4LocPortNum,
                                                         i4PrevLocProtoVlanId,
                                                         &i4LocProtoVlanId)
           == SNMP_SUCCESS);
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetAllVlanNameTxVal
 *
 *     DESCRIPTION      : Configures vlan name transmission on a given port
 *
 *     INPUT            : i4IfIndex       -  Inteface Index
 *                        i4TxStatusVal   -  Transmission status value
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliSetAllVlanNameTxVal (INT4 i4IfIndex, INT4 i4TxStatusVal)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4LocPortNum = 0;
    INT4                i4PrevLocPortNum = 0;
    INT4                i4PrevLocVlanId = 0;
    INT4                i4LocVlanId = 0;
    UINT1               au1VlanName[VLAN_STATIC_MAX_NAME_LEN];
    tCliHandle          CliHandle = 0;

    if (L2IwfIsPortInPortChannel ((UINT4) i4IfIndex) == L2IWF_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to set VlanNameTLVTxEnable:"
                   "Port is part of port channel\r\n");
        return CLI_SUCCESS;
    }

    if (nmhGetFirstIndexLldpXdot1ConfigVlanNameTable (&i4LocPortNum,
                                                      &i4LocVlanId)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    do
    {
        if (i4IfIndex == i4LocPortNum)
        {
            MEMSET (&au1VlanName[0], 0, VLAN_STATIC_MAX_NAME_LEN);
            if (LldpVlndbGetVlanName ((UINT4) i4LocPortNum,
                                      (UINT2) i4LocVlanId, &au1VlanName[0])
                == OSIX_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhTestv2LldpXdot1ConfigVlanNameTxEnable (&u4ErrorCode,
                                                          i4IfIndex,
                                                          i4LocVlanId,
                                                          i4TxStatusVal) !=
                SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            if (nmhSetLldpXdot1ConfigVlanNameTxEnable
                (i4IfIndex, i4LocVlanId, i4TxStatusVal) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

        }
        i4PrevLocPortNum = i4LocPortNum;
        i4PrevLocVlanId = i4LocVlanId;
    }
    while (nmhGetNextIndexLldpXdot1ConfigVlanNameTable (i4PrevLocPortNum,
                                                        &i4LocPortNum,
                                                        i4PrevLocVlanId,
                                                        &i4LocVlanId)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowRemProtoVlanInfo
 *
 *     DESCRIPTION      : Displays Dot1 protocol vlan info
 *
 *     INPUT            : CliHandle    - CliContext ID
 *                        i4IfIndex    - Interface Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 *****************************************************************************/

PRIVATE VOID
LldpCliShowRemProtoVlanInfo (tCliHandle CliHandle, UINT4 u4RemProtoTimeMark,
                             INT4 i4IfIndex, INT4 i4RemProtoIndex)
{
    UINT4               u4RemTimeMark = 0;
    UINT4               u4PrevRemTimeMark = 0;
    INT4                i4RemLocalPortNum = 0;
    INT4                i4PrevRemLocalPortNum = 0;
    INT4                i4RemIndex = 0;
    INT4                i4PrevRemIndex = 0;
    INT4                i4RemProtoVlanId = 0;
    INT4                i4PrevRemProtoVlanId = 0;
    INT4                i4RemProtoVlanSupported = 0;
    INT4                i4RemProtoVlanEnabled = 0;
    UINT4               u4RemDstMac = 0;
    UINT4               u4PrevRemDstMac = 0;
    BOOL1               bFlag = LLDP_FALSE;

    if (nmhGetFirstIndexLldpV2Xdot1RemProtoVlanTable (&u4RemTimeMark,
                                                      &i4RemLocalPortNum,
                                                      &u4RemDstMac,
                                                      &i4RemIndex,
                                                      (UINT4 *)
                                                      &i4RemProtoVlanId) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Protocol Vlan Tlv             : "
                   "Not Advertised\r\n");
        return;
    }
    do
    {
        /* If Proto Vlan Node is NULL then return */
        if (nmhGetLldpV2Xdot1RemProtoVlanSupported (u4RemTimeMark,
                                                    i4RemLocalPortNum,
                                                    u4RemDstMac,
                                                    i4RemIndex,
                                                    (UINT4) i4RemProtoVlanId,
                                                    &i4RemProtoVlanSupported)
            != SNMP_SUCCESS)
        {
            return;
        }
        if (nmhGetLldpV2Xdot1RemProtoVlanEnabled (u4RemTimeMark,
                                                  i4RemLocalPortNum,
                                                  u4RemDstMac,
                                                  i4RemIndex,
                                                  (UINT4) i4RemProtoVlanId,
                                                  &i4RemProtoVlanEnabled)
            != SNMP_SUCCESS)
        {
            return;
        }
        if (u4RemProtoTimeMark == u4RemTimeMark &&
            i4IfIndex == i4RemLocalPortNum && i4RemProtoIndex == i4RemIndex)
        {
            if (bFlag != LLDP_TRUE)
            {
                CliPrintf (CliHandle, "%-25s %-14s %-14s\r\n",
                           "Protocol Vlan Id", "Support", "Status");
                CliPrintf (CliHandle, "%-25s %-14s %-14s\r\n",
                           "----------------", "-------", "------");
            }
            CliPrintf (CliHandle, "%-25d %-14s %-14s",
                       i4RemProtoVlanId,
                       gapc1Support[i4RemProtoVlanSupported],
                       gapc1Enable[i4RemProtoVlanEnabled]);
            CliPrintf (CliHandle, "\r\n");
            bFlag = LLDP_TRUE;
        }
        u4PrevRemTimeMark = u4RemTimeMark;
        i4PrevRemLocalPortNum = i4RemLocalPortNum;
        i4PrevRemIndex = i4RemIndex;
        i4PrevRemProtoVlanId = i4RemProtoVlanId;
        u4PrevRemDstMac = u4RemDstMac;

    }
    while (nmhGetNextIndexLldpV2Xdot1RemProtoVlanTable (u4PrevRemTimeMark,
                                                        &u4RemTimeMark,
                                                        i4PrevRemLocalPortNum,
                                                        &i4RemLocalPortNum,
                                                        u4PrevRemDstMac,
                                                        &u4RemDstMac,
                                                        i4PrevRemIndex,
                                                        &i4RemIndex,
                                                        (UINT4)
                                                        i4PrevRemProtoVlanId,
                                                        (UINT4 *)
                                                        &i4RemProtoVlanId) ==
           SNMP_SUCCESS);
    if (bFlag == LLDP_FALSE)
    {
        CliPrintf (CliHandle, "Protocol Vlan Tlv             : "
                   "Not Advertised\r\n");
    }
    return;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowRemVlanNameInfo
 *
 *     DESCRIPTION      : Shows Dot1 Remote Vlan Information
 *
 *     INPUT            : CliHandle    -  CliContext ID
 *                        i4IfIndex    -  Interface Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 *****************************************************************************/

PRIVATE VOID
LldpCliShowRemVlanNameInfo (tCliHandle CliHandle, UINT4 u4RemVlanTimeMark,
                            INT4 i4IfIndex, INT4 i4RemVlanIndex)
{
    UINT1               au1VlanName[VLAN_STATIC_MAX_NAME_LEN + 1];
    INT4                i4RemLocalPortNum = 0;
    INT4                i4PrevRemLocalPortNum = 0;
    INT4                i4RemVlanId = 0;
    INT4                i4PrevRemVlanId = 0;
    INT4                i4RemIndex = 0;
    INT4                i4PrevRemIndex = 0;
    UINT4               u4RemTimeMark = 0;
    UINT4               u4PrevRemTimeMark = 0;
    tSNMP_OCTET_STRING_TYPE RemVlanName;
    UINT4               u4RemDstMac = 0;
    UINT4               u4PrevRemDstMac = 0;
    BOOL1               bFlag = LLDP_FALSE;

    MEMSET (&RemVlanName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1VlanName, 0, VLAN_STATIC_MAX_NAME_LEN + 1);

    RemVlanName.pu1_OctetList = au1VlanName;
    RemVlanName.i4_Length = VLAN_STATIC_MAX_NAME_LEN + 1;

    if (nmhGetFirstIndexLldpV2Xdot1RemVlanNameTable (&u4RemTimeMark,
                                                     &i4RemLocalPortNum,
                                                     &u4RemDstMac,
                                                     &i4RemIndex,
                                                     &i4RemVlanId)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Vlan Name Tlv                 : "
                   "Not Advertised\r\n");
        return;
    }
    do
    {
        MEMSET (RemVlanName.pu1_OctetList, 0, VLAN_STATIC_MAX_NAME_LEN + 1);
        if (nmhGetLldpV2Xdot1RemVlanName (u4RemTimeMark,
                                          i4RemLocalPortNum,
                                          u4RemDstMac,
                                          i4RemIndex,
                                          i4RemVlanId,
                                          &RemVlanName) != SNMP_SUCCESS)
        {
            /* If Vlan Node is NULL then return */
            return;
        }
        if (u4RemVlanTimeMark == u4RemTimeMark &&
            i4IfIndex == i4RemLocalPortNum && i4RemVlanIndex == i4RemIndex)
        {
            if (bFlag == LLDP_FALSE)
            {
                CliPrintf (CliHandle, "%-25s %-34s\r\n", "Vlan Id",
                           "Vlan Name");
                CliPrintf (CliHandle, "%-25s %-34s\r\n", "-------",
                           "---------");
            }
            CliPrintf (CliHandle, "%-25d %-34s\r\n", i4RemVlanId,
                       RemVlanName.pu1_OctetList);
            bFlag = LLDP_TRUE;
        }
        u4PrevRemTimeMark = u4RemTimeMark;
        i4PrevRemLocalPortNum = i4RemLocalPortNum;
        i4PrevRemIndex = i4RemIndex;
        i4PrevRemVlanId = i4RemVlanId;
        u4PrevRemDstMac = u4RemDstMac;
    }
    while (nmhGetNextIndexLldpV2Xdot1RemVlanNameTable (u4PrevRemTimeMark,
                                                       &u4RemTimeMark,
                                                       i4PrevRemLocalPortNum,
                                                       &i4RemLocalPortNum,
                                                       u4PrevRemDstMac,
                                                       &u4RemDstMac,
                                                       i4PrevRemIndex,
                                                       &i4RemIndex,
                                                       i4PrevRemVlanId,
                                                       &i4RemVlanId)
           == SNMP_SUCCESS);

    if (bFlag == LLDP_FALSE)
    {
        CliPrintf (CliHandle, "Vlan Name Tlv                 : "
                   "Not Advertised\r\n");
    }
    return;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetDot3TLV
 *
 *     DESCRIPTION      : Configure dot3 TLV types on a port
 *
 *     INPUT            : CliHandle      - CliContext ID
 *                        i4IfIndex      - Interface Index
 *                        u1Dot3Tlv      - Enabled or Disabled TLV set
 *                        i4SetVal       - Tlv Enable or Disable Status
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliSetDot3TLV (tCliHandle CliHandle, INT4 i4IfIndex,
                   UINT1 u1Dot3Tlv, INT4 i4SetVal)
{
    tSNMP_OCTET_STRING_TYPE LldpDot3TlvEnabled;
    tSNMP_OCTET_STRING_TYPE LldpDot3GetTlvEnabled;
    UINT1               u1Dot3TlvEnabled;
    UINT4               u4ErrorCode = 0;

    MEMSET (&LldpDot3TlvEnabled, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&LldpDot3GetTlvEnabled, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    LldpDot3TlvEnabled.pu1_OctetList = &u1Dot3Tlv;
    LldpDot3TlvEnabled.i4_Length = sizeof (UINT1);
    LldpDot3GetTlvEnabled.pu1_OctetList = &u1Dot3TlvEnabled;

    if (nmhTestv2LldpXdot3PortConfigTLVsTxEnable (&u4ErrorCode,
                                                  i4IfIndex,
                                                  &LldpDot3TlvEnabled)
        == SNMP_FAILURE)
    {
        if ((CLI_GET_ERR (&u4ErrorCode) == CLI_SUCCESS) && (0 == u4ErrorCode))
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set PortConfigDot3TLVsTxEnable\r\n");
        }
        return CLI_FAILURE;
    }
    if (nmhGetLldpXdot3PortConfigTLVsTxEnable (i4IfIndex,
                                               &LldpDot3GetTlvEnabled) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* To Set the Value */
    if (i4SetVal == OSIX_TRUE)
    {
        u1Dot3Tlv |= *(LldpDot3GetTlvEnabled.pu1_OctetList);
    }
    else
    {
        /* To Reset the value */
        u1Dot3Tlv = (UINT1) ~u1Dot3Tlv;
        u1Dot3Tlv &= *(LldpDot3GetTlvEnabled.pu1_OctetList);
    }
    if (nmhSetLldpXdot3PortConfigTLVsTxEnable (i4IfIndex, &LldpDot3TlvEnabled)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to set PortConfigDot3TLVsTxEnable\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LLdpShowRunningConfig                              */
/*                                                                           */
/*     DESCRIPTION      : Displays LLDP Configuration                        */
/*                                                                           */
/*     INPUT            : CliHandle      - CliContext ID                     */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
LldpShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{
    INT4                i4RetValFsLldpModuleStatus = 0;
    INT4                i4SystemControl = 0;
    UNUSED_PARAM (u4Module);

    CliRegisterLock (CliHandle, LldpLock, LldpUnLock);
    LldpLock ();

    nmhGetFsLldpSystemControl (&i4SystemControl);

    if (i4SystemControl != LLDP_START)
    {
        /* Returning here itself as the lldp protocol is
           shutdown */
        CliPrintf (CliHandle, "shutdown lldp\r\n");
        LldpUnLock ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    nmhGetFsLldpModuleStatus (&i4RetValFsLldpModuleStatus);

    /* If Global state enabled only, then print configurations */
    if (i4RetValFsLldpModuleStatus == LLDP_ENABLED)
    {
        LldpShowRunningConfigScalars (CliHandle);
        LldpShowRunningConfigInterface (CliHandle);
    }

    LldpUnLock ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : LldpShowRunningConfigScalars                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays scalar objects in LLDP      */
/*                                                                           */
/*                                                                           */
/*     INPUT            : CliHandle      - CliContext ID                     */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
LldpShowRunningConfigScalars (tCliHandle CliHandle)
{
    UINT1               au1ChassisId[LLDP_MAX_LEN_CHASSISID + 1];
    INT4                i4LldpStatus = 0;
    INT4                i4MsgTxInterval = 0;
    INT4                i4MsgTxHoldMultiplier = 0;
    INT4                i4ReinitDelay = 0;
    INT4                i4TxDelay = 0;
    INT4                i4NotificationInterval = 0;
    INT4                i4LocChassisIdSubtype = 0;
    UINT4               u4CreditMax = 0;
    UINT4               u4MsgFastTx = 0;
    UINT4               u4FastInit = 0;
    UINT4               u4MedFastStart = 0;
    INT4                i4Version = 0;
    UINT4               u4MgmtIpAddr = 0;
    UINT1               au1MgmtAddr[IPVX_IPV6_ADDR_LEN] = { 0 };
    UINT1               au1MgmtAddr6[IPVX_IPV6_ADDR_LEN] = { 0 };
    CHR1               *pu1Ipv4MgmtString = NULL;

    tSNMP_OCTET_STRING_TYPE LocChassisId;
    tSNMP_OCTET_STRING_TYPE Ipv4ManAddr;
    tSNMP_OCTET_STRING_TYPE Ipv6ManAddr;

    MEMSET (&LocChassisId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ChassisId, 0, LLDP_MAX_LEN_CHASSISID + 1);
    MEMSET (&Ipv4ManAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&Ipv6ManAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    LocChassisId.pu1_OctetList = (UINT1 *) &au1ChassisId[0];
    Ipv4ManAddr.pu1_OctetList = (UINT1 *) &au1MgmtAddr[0];
    Ipv6ManAddr.pu1_OctetList = (UINT1 *) &au1MgmtAddr6[0];
    CliPrintf (CliHandle, "\r\n");

    nmhGetFslldpv2Version (&i4Version);
    if (i4Version != LLDP_VERSION_05)
    {
        CliPrintf (CliHandle, "set lldp version v2\r\n");
    }
    nmhGetFsLldpModuleStatus (&i4LldpStatus);
    if (i4LldpStatus != LLDP_DISABLED)
    {
        CliPrintf (CliHandle, "set lldp enable\r\n");
    }

    nmhGetFsLldpConfiguredMgmtIpv4Address (&Ipv4ManAddr);
    if (Ipv4ManAddr.i4_Length != 0)
    {
        MEMCPY (&u4MgmtIpAddr, Ipv4ManAddr.pu1_OctetList,
                Ipv4ManAddr.i4_Length);
        u4MgmtIpAddr = OSIX_HTONL (u4MgmtIpAddr);
        CLI_CONVERT_IPADDR_TO_STR (pu1Ipv4MgmtString, u4MgmtIpAddr);
        CliPrintf (CliHandle, "set lldp management-address ipv4 %s\r\n",
                   pu1Ipv4MgmtString);
    }

    nmhGetFsLldpConfiguredMgmtIpv6Address (&Ipv6ManAddr);
    if (Ipv6ManAddr.i4_Length != 0)
    {
        CliPrintf (CliHandle, "set lldp management-address ipv6 %s\r\n",
                   Ip6PrintNtop ((tIp6Addr *) (VOID *) Ipv6ManAddr.
                                 pu1_OctetList));
    }

    nmhGetFsLldpTagStatus (&i4LldpStatus);
    if (i4LldpStatus != LLDP_TAG_DISABLE)
    {
        CliPrintf (CliHandle, "set lldp tag status enable\r\n");
    }

    nmhGetLldpV2MessageTxInterval ((UINT4 *) &i4MsgTxInterval);
    if (i4MsgTxInterval != LLDP_MSG_TX_INTERVAL)
    {
        CliPrintf (CliHandle, "lldp transmit-interval %d\r\n", i4MsgTxInterval);
    }

    nmhGetLldpV2MessageTxHoldMultiplier ((UINT4 *) &i4MsgTxHoldMultiplier);
    if (i4MsgTxHoldMultiplier != LLDP_MSG_TX_HOLD_MULTIPLIER)
    {
        CliPrintf (CliHandle, "lldp holdtime-multiplier %d\r\n",
                   i4MsgTxHoldMultiplier);

    }

    nmhGetLldpV2ReinitDelay ((UINT4 *) &i4ReinitDelay);
    if (i4ReinitDelay != LLDP_REINIT_DELAY)
    {
        CliPrintf (CliHandle, "lldp reinitialization-delay %d\r\n",
                   i4ReinitDelay);

    }

    nmhGetLldpV2NotificationInterval ((UINT4 *) &i4NotificationInterval);

    if (i4Version == LLDP_VERSION_05)
    {
        if (i4NotificationInterval != LLDP_NOTIFICATION_INTERVAL)
        {
            CliPrintf (CliHandle, "lldp notification-interval %d\r\n",
                       i4NotificationInterval);
        }
    }
    else
    {
        if (i4NotificationInterval != LLDPV2_NOTIFICATION_INTERVAL)
        {
            CliPrintf (CliHandle, "lldp notification-interval %d\r\n",
                       i4NotificationInterval);
        }
    }
    nmhGetFsLldpLocChassisIdSubtype (&i4LocChassisIdSubtype);
    if (i4LocChassisIdSubtype != LLDP_CHASS_ID_SUB_MAC_ADDR)
    {
        nmhGetFsLldpLocChassisId (&LocChassisId);
        switch (i4LocChassisIdSubtype)
        {
            case LLDP_CHASS_ID_SUB_CHASSIS_COMP:
                CliPrintf (CliHandle, "lldp chassis-id-subtype chassis-comp %s\
                           \r\n", LocChassisId.pu1_OctetList);
                break;

            case LLDP_CHASS_ID_SUB_IF_ALIAS:
                CliPrintf (CliHandle, "lldp chassis-id-subtype if-alias\r\n");
                break;

            case LLDP_CHASS_ID_SUB_PORT_COMP:
                CliPrintf (CliHandle,
                           "lldp chassis-id-subtype port-comp %s\r\n",
                           LocChassisId.pu1_OctetList);
                break;

            case LLDP_CHASS_ID_SUB_NW_ADDR:
                CliPrintf (CliHandle, "lldp chassis-id-subtype nw-addr\r\n");
                break;

            case LLDP_CHASS_ID_SUB_IF_NAME:
                CliPrintf (CliHandle, "lldp chassis-id-subtype if-name\r\n");
                break;

            case LLDP_CHASS_ID_SUB_LOCAL:
                CliPrintf (CliHandle, "lldp chassis-id-subtype local %s\r\n",
                           LocChassisId.pu1_OctetList);
                break;

            default:
                break;
        }
    }

    if (i4Version == LLDP_VERSION_05)
    {
        nmhGetLldpTxDelay (&i4TxDelay);
        if (i4TxDelay != LLDP_TX_DELAY)
        {
            CliPrintf (CliHandle, "lldp tx-delay %d\r\n", i4TxDelay);
        }
    }
    else
    {
        nmhGetLldpV2TxCreditMax (&u4CreditMax);
        if (u4CreditMax != LLDPV2_DEFAULT_CREDIT_MAX)
        {
            CliPrintf (CliHandle, "lldp txCreditMax %d\r\n", u4CreditMax);
        }
        nmhGetLldpV2MessageFastTx (&u4MsgFastTx);
        if (u4MsgFastTx != LLDPV2_DEFAULT_MESSAGE_FAST_TX)
        {
            CliPrintf (CliHandle, "lldp MessageFastTx %d\r\n", u4MsgFastTx);
        }
        nmhGetLldpV2TxFastInit (&u4FastInit);
        if (u4FastInit != LLDPV2_DEFAULT_TX_FAST_INIT)
        {
            CliPrintf (CliHandle, "lldp txFastInit %d\r\n", u4FastInit);
        }
        nmhGetLldpXMedFastStartRepeatCount (&u4MedFastStart);
        if (u4MedFastStart != LLDPV2_DEFAULT_MED_FAST_REPEAT_COUNT)
        {
            CliPrintf (CliHandle, "lldp medFastStartRepeatCount %d\r\n",
                       u4MedFastStart);
        }
    }

    CliPrintf (CliHandle, "\r\n");

    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : LldpShowRunningConfigInterface                     */
/*                                                                           */
/*     DESCRIPTION      : This function scans the interface tables  in LLDP  */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
LldpShowRunningConfigInterface (tCliHandle CliHandle)
{
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT1                i1OutCome = 0;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH] = { 0 };

    i1OutCome = nmhGetFirstIndexLldpV2LocPortTable (&i4IfIndex);

    while (i1OutCome != SNMP_FAILURE)
    {
        MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);
        CfaCliConfGetIfName (i4IfIndex, (INT1 *) au1NameStr);
        CliPrintf (CliHandle, "interface  %s\r\n", au1NameStr);

        LldpShowRunningConfigPhysicalInterfaceDetails (CliHandle, i4IfIndex);

        CliPrintf (CliHandle, "!\r\n");

        i1OutCome =
            nmhGetNextIndexLldpV2LocPortTable (i4IfIndex, &i4NextIfIndex);
        i4IfIndex = i4NextIfIndex;

    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME  : LldpShowRunningConfigPhysicalInterfaceDetails        */
/*                                                                           */
/*     DESCRIPTION    : This function displays the physical Interface details*/
/*                                                                           */
/*     INPUT          : CliHandle - Handle to the cli context                */
/*                      i4Index - Interface Index                            */
/*                                                                           */
/*     OUTPUT         : None                                                 */
/*                                                                           */
/*     RETURNS        : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
LldpShowRunningConfigPhysicalInterfaceDetails (tCliHandle CliHandle,
                                               INT4 i4Index)
{
    tSNMP_OCTET_STRING_TYPE LocPortId;
    UINT1               au1PortId[LLDP_MAX_LEN_PORTID + 1] = { 0 };
    INT4                i4DataPortType = 0;
    INT4                i4LldpData = 0;
    UINT4               u4DestMacAddr = 0;
    UINT4               u4PrevDestMacAddInd = 0;
    UINT1               au1String[LLDP_CLI_MAX_MAC_STRING_SIZE] = { 0 };
    UINT1               au1MacAddr[LLDP_CLI_MAX_MAC_STRING_SIZE] = { 0 };
    tMacAddr            DestAddr;
    UINT1               u1Flag = LLDP_FALSE;
    UINT4               u4DestIndex = 0, u4PrevDestMacIndex = 0;
    INT4                i4IfIndex = i4Index, i4PrevIfIndex = 0;

    MEMSET (&DestAddr, 0, MAC_ADDR_LEN);
    MEMSET (&LocPortId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1PortId, 0, (LLDP_MAX_LEN_PORTID + 1));

    LocPortId.pu1_OctetList = (UINT1 *) &au1PortId[0];

    nmhGetFsLldpSystemControl (&i4LldpData);

    if (i4LldpData != LLDP_START)
    {
        return;
    }
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_09)
    {
        if (nmhGetFirstIndexLldpV2PortConfigTable (&i4IfIndex, &u4DestIndex)
            == SNMP_FAILURE)
        {
            return;
        }
        do
        {
            nmhGetLldpV2DestMacAddress (u4DestIndex, &DestAddr);

            if (MEMCMP (DestAddr, gau1LldpMcastAddr, MAC_ADDR_LEN) != 0)
            {
                PrintMacAddress (DestAddr, au1String);
                if (i4IfIndex == i4Index)
                {
                    CliPrintf (CliHandle, "lldp dest-mac %s\r\n", au1String);
                }
            }
            i4PrevIfIndex = i4IfIndex;
            u4PrevDestMacIndex = u4DestIndex;
        }
        while (nmhGetNextIndexLldpV2PortConfigTable
               (i4PrevIfIndex, &i4IfIndex, u4PrevDestMacIndex,
                &u4DestIndex) == SNMP_SUCCESS);

    }
    nmhGetFsLldpLocPortIdSubtype (i4Index, &i4DataPortType);
    if (i4DataPortType != LLDP_PORT_ID_SUB_IF_ALIAS)
    {
        nmhGetFsLldpLocPortId (i4Index, &LocPortId);
        switch (i4DataPortType)
        {
            case LLDP_PORT_ID_SUB_PORT_COMP:
                CliPrintf (CliHandle, "lldp port-id-subtype port-comp %s\r\n",
                           LocPortId.pu1_OctetList);
                break;

            case LLDP_PORT_ID_SUB_MAC_ADDR:
                CliPrintf (CliHandle, "lldp port-id-subtype mac-addr\r\n");
                break;

            case LLDP_PORT_ID_SUB_IF_NAME:
                CliPrintf (CliHandle, "lldp port-id-subtype if-name\r\n");
                break;

            case LLDP_PORT_ID_SUB_LOCAL:
                CliPrintf (CliHandle, "lldp port-id-subtype local %s\r\n",
                           LocPortId.pu1_OctetList);
                break;
            default:
                break;
        }
    }

    u4DestMacAddr = DEFAULT_DEST_MAC_ADDR_INDEX;
    do
    {
        u1Flag = LLDP_FALSE;
        nmhGetFslldpV2DestMacAddress (u4DestMacAddr, &DestAddr);
        PrintMacAddress (DestAddr, au1MacAddr);

        if (nmhValidateIndexInstanceFslldpv2ConfigPortMapTable
            (i4Index, DestAddr) == SNMP_FAILURE)
        {
            u4PrevDestMacAddInd = u4DestMacAddr;
            continue;
        }

        nmhGetLldpV2PortConfigAdminStatus (i4Index, u4DestMacAddr, &i4LldpData);

        if (i4LldpData != LLDP_ADM_STAT_TX_AND_RX)
        {
            u1Flag = LLDP_TRUE;

            switch (i4LldpData)
            {
                case LLDP_ADM_STAT_TX_ONLY:
                    CliPrintf (CliHandle, "no lldp receive ");
                    break;

                case LLDP_ADM_STAT_RX_ONLY:
                    CliPrintf (CliHandle, "no lldp transmit ");
                    break;

                default:
                    break;
            }
        }

        if ((u4DestMacAddr != DEFAULT_DEST_MAC_ADDR_INDEX) && (u1Flag == 1))
        {
            u1Flag = LLDP_FALSE;

            if (i4LldpData == LLDP_ADM_STAT_DISABLED)
            {
                CliPrintf (CliHandle, "no lldp transmit ");
                CliPrintf (CliHandle, "mac-address %s\r\n", au1MacAddr);
                CliPrintf (CliHandle, "no lldp receive ");
                CliPrintf (CliHandle, "mac-address %s\r\n", au1MacAddr);
            }
            else
            {
                CliPrintf (CliHandle, "mac-address %s", au1MacAddr);
            }
        }
        else if (i4LldpData == LLDP_ADM_STAT_DISABLED)
        {
            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle, "no lldp transmit \n");
            CliPrintf (CliHandle, "no lldp receive ");
        }

        CliPrintf (CliHandle, "\r\n");
        nmhGetLldpV2PortConfigNotificationEnable (i4Index, u4DestMacAddr,
                                                  &i4LldpData);
        if (i4LldpData != LLDP_FALSE)
        {
            u1Flag = LLDP_TRUE;
            nmhGetFsLldpPortConfigNotificationType (i4Index, &i4DataPortType);

            switch (i4DataPortType)
            {
                case LLDP_REMOTE_CHG_NOTIFICATION:
                    CliPrintf (CliHandle,
                               "lldp notification remote-table-chg ");

                    break;

                case LLDP_MIS_CFG_NOTIFICATION:
                    if ((u4DestMacAddr != DEFAULT_DEST_MAC_ADDR_INDEX))
                    {
                        CliPrintf (CliHandle,
                                   "lldp notification mis-configuration ");
                    }
                    break;

                case LLDP_REMOTE_CHG_MIS_CFG_NOTIFICATION:
                    CliPrintf (CliHandle,
                               "lldp notification remote-table-chg mis-configuration ");
                    break;

                default:
                    break;
            }
        }

        if ((u4DestMacAddr != DEFAULT_DEST_MAC_ADDR_INDEX)
            && (u1Flag == LLDP_TRUE))
        {
            u1Flag = LLDP_FALSE;
            CliPrintf (CliHandle, "mac-address %s", au1MacAddr);
        }
        CliPrintf (CliHandle, "\r\n");

        u4PrevDestMacAddInd = u4DestMacAddr;

    }
    while (nmhGetNextIndexFslldpV2DestAddressTable (u4PrevDestMacAddInd,
                                                    &u4DestMacAddr) ==
           SNMP_SUCCESS);

    LldpShowRunningConfigBasicTLVDetails (CliHandle, i4Index);
    LldpShowRunningConfigTLVDetails (CliHandle, i4Index);
    /* LLDP-MED */
    LldpMedShowRunningAdminStatus (CliHandle, i4Index);
    LldpMedShowRunningConfigTLVInfo (CliHandle, i4Index);
    LldpMedShowRunningConfigNwPol (CliHandle, i4Index);
    LldpMedShowRunningConfigLocInfo (CliHandle, i4Index);

}

/*****************************************************************************/
/*     FUNCTION NAME  : LldpShowRunningConfigTLVDetails                      */
/*                                                                           */
/*     DESCRIPTION    : This function displays the TLV details               */
/*                                                                           */
/*     INPUT          : CliHandle - Handle to the cli context                */
/*                      i4Index - Interface Index                            */
/*                                                                           */
/*     OUTPUT         : None                                                 */
/*                                                                           */
/*     RETURNS        : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
LldpShowRunningConfigTLVDetails (tCliHandle CliHandle, INT4 i4Index)
{
    tSNMP_OCTET_STRING_TYPE TlvsTxEnable;
    INT4                i4LldpData = 0;
    INT4                i4TlvData = 0;
    INT4                i4LocPortNum = 0;
    INT4                i4PrevLocPortNum = 0;
    UINT4               u4LocProtoVlanId = 0;
    UINT4               u4VlanNameTlvCount = 0;
    INT4                i4PrevData = 0;
    INT4                i4LocVlanEnabled = 0;
    INT4                i4ProtoVlanTxEnabled = 0;
    INT4                i4LocVlanId = 0;
    INT4                i4LocVlanIdTemp = 0;
    INT4                i4VlanNameEnabled = 0;
    INT4                i4VidDigestEnabled = LLDP_FALSE;
    INT4                i4ManVidEnabled = LLDP_FALSE;
    UINT1               u1String = 0;
    UINT1               u1TlvTxEnable = 0;
    UINT1               u1ProtoVlanFlag = LLDP_FALSE;
    UINT1               u1VlanNameFlag = LLDP_FALSE;

    MEMSET (&TlvsTxEnable, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    TlvsTxEnable.pu1_OctetList = &u1String;
    TlvsTxEnable.i4_Length = sizeof (u1String);

    if (nmhGetFirstIndexLldpV2Xdot1LocProtoVlanTable (&i4LocPortNum,
                                                      &u4LocProtoVlanId)
        != SNMP_SUCCESS)
    {
        return;
    }

    do
    {
        nmhGetLldpV2Xdot1ConfigProtoVlanTxEnable (i4LocPortNum,
                                                  (INT4) u4LocProtoVlanId,
                                                  &i4LocVlanEnabled);
        if (i4Index == i4LocPortNum)
        {
            if (i4LocVlanEnabled == LLDP_TRUE)
            {
                i4ProtoVlanTxEnabled = LLDP_TRUE;
                if (u1ProtoVlanFlag == LLDP_FALSE)
                {
                    u1ProtoVlanFlag = LLDP_TRUE;
                    break;
                }
            }
        }
        i4PrevLocPortNum = i4LocPortNum;
        i4PrevData = (INT4) u4LocProtoVlanId;
    }
    while (nmhGetNextIndexLldpV2Xdot1LocProtoVlanTable (i4PrevLocPortNum,
                                                        &i4LocPortNum,
                                                        (UINT4) i4PrevData,
                                                        &u4LocProtoVlanId) ==
           SNMP_SUCCESS);
    i4LocPortNum = 0;
    i4PrevLocPortNum = 0;
    i4LocVlanEnabled = 0;
    i4PrevData = 0;

    if (nmhGetFirstIndexLldpV2Xdot1ConfigVlanNameTable (&i4LocPortNum,
                                                        &i4LocVlanId)
        != SNMP_SUCCESS)
    {
        return;
    }

    do
    {
        nmhGetLldpV2Xdot1ConfigVlanNameTxEnable (i4LocPortNum, i4LocVlanId,
                                                 &i4LocVlanEnabled);
        if (i4Index == i4LocPortNum)
        {
            if (i4LocVlanEnabled == LLDP_TRUE)
            {
                i4VlanNameEnabled = LLDP_TRUE;
                if (u1VlanNameFlag == LLDP_FALSE)
                {
                    u1VlanNameFlag = LLDP_TRUE;
                }
                i4LocVlanIdTemp = i4LocVlanId;
                u4VlanNameTlvCount++;
            }
        }
        i4PrevLocPortNum = i4LocPortNum;
        i4PrevData = i4LocVlanId;
    }
    while (nmhGetNextIndexLldpV2Xdot1ConfigVlanNameTable (i4PrevLocPortNum,
                                                          &i4LocPortNum,
                                                          i4PrevData,
                                                          &i4LocVlanId)
           == SNMP_SUCCESS);
    if (u4VlanNameTlvCount > 1)
    {
        i4LocVlanId = 0;
    }
    else
    {
        i4LocVlanId = i4LocVlanIdTemp;
    }

    nmhGetLldpXdot1ConfigPortVlanTxEnable (i4Index, &i4TlvData);
    nmhGetLldpV2Xdot1ConfigVidUsageDigestTxEnable (i4Index,
                                                   &i4VidDigestEnabled);
    nmhGetLldpV2Xdot1ConfigManVidTxEnable (i4Index, &i4ManVidEnabled);
    nmhGetLldpXdot3PortConfigTLVsTxEnable (i4Index, &TlvsTxEnable);
    MEMCPY (&u1TlvTxEnable, TlvsTxEnable.pu1_OctetList, TlvsTxEnable.i4_Length);
    nmhGetFsLldpModuleStatus (&i4LldpData);
    if ((i4LldpData != LLDP_DISABLED) &&
        (((i4TlvData == LLDP_TRUE) ||
          (i4ProtoVlanTxEnabled == LLDP_TRUE) ||
          (i4VlanNameEnabled == LLDP_TRUE)) ||
         (i4VidDigestEnabled == LLDP_TRUE) || (i4ManVidEnabled == LLDP_TRUE) ||
         ((gLldpGlobalInfo.i4Version == LLDP_VERSION_09)
          && (u1TlvTxEnable & LLDP_LINK_AGG_TLV_ENABLED))))
    {

        if (i4TlvData == LLDP_TRUE)
        {
            CliPrintf (CliHandle, "lldp tlv-select dot1tlv");
            CliPrintf (CliHandle, " port-vlan-id\r\n");
        }

        if (i4ProtoVlanTxEnabled == LLDP_TRUE)
        {
            CliPrintf (CliHandle, "lldp tlv-select dot1tlv");
            CliPrintf (CliHandle, " protocol-vlan-id");

            if (u1ProtoVlanFlag == LLDP_TRUE)
            {
                if (u4LocProtoVlanId == 0)
                {
                    CliPrintf (CliHandle, " all");
                }
                else
                {
                    CliPrintf (CliHandle, " %d", u4LocProtoVlanId);
                }
            }
            CliPrintf (CliHandle, "\r\n");
        }
        if (i4VlanNameEnabled == LLDP_TRUE)
        {
            CliPrintf (CliHandle, "lldp tlv-select dot1tlv");
            CliPrintf (CliHandle, " vlan-name");

            if (u1VlanNameFlag == LLDP_TRUE)
            {
                if (i4LocVlanId == 0)
                {
                    CliPrintf (CliHandle, " all");
                }
                else
                {
                    CliPrintf (CliHandle, " %d", i4LocVlanId);
                }
            }
            CliPrintf (CliHandle, "\r\n");
        }
        if (i4VidDigestEnabled == LLDP_TRUE)
        {
            CliPrintf (CliHandle, "lldp tlv-select dot1tlv");
            CliPrintf (CliHandle, " vid-usage-digest\r\n");
        }
        if (i4ManVidEnabled == LLDP_TRUE)
        {
            CliPrintf (CliHandle, "lldp tlv-select dot1tlv");
            CliPrintf (CliHandle, " mgmt-vid\r\n");
        }
        if (gLldpGlobalInfo.i4Version == LLDP_VERSION_09)
        {
            if (u1TlvTxEnable & LLDP_LINK_AGG_TLV_ENABLED)
            {
                CliPrintf (CliHandle, "lldp tlv-select dot1tlv");
                CliPrintf (CliHandle, " link-aggregation\r\n");
            }

        }
    }

    nmhGetLldpXdot3PortConfigTLVsTxEnable (i4Index, &TlvsTxEnable);
    MEMCPY (&u1TlvTxEnable, TlvsTxEnable.pu1_OctetList, TlvsTxEnable.i4_Length);

    if ((i4LldpData != LLDP_DISABLED) && (u1TlvTxEnable != 0))
    {
        if ((u1TlvTxEnable & LLDP_LINK_AGG_TLV_ENABLED)
            && (gLldpGlobalInfo.i4Version == LLDP_VERSION_09))
        {
            CliPrintf (CliHandle, "\r\n");
        }
        else
        {

            if (u1TlvTxEnable & LLDP_MAC_PHY_TLV_ENABLED)
            {
                CliPrintf (CliHandle, "lldp tlv-select dot3tlv");
                CliPrintf (CliHandle, " macphy-config");
                CliPrintf (CliHandle, "\r\n");
            }
            if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
            {
                if (u1TlvTxEnable & LLDP_LINK_AGG_TLV_ENABLED)
                {
                    CliPrintf (CliHandle, "lldp tlv-select dot3tlv");
                    CliPrintf (CliHandle, " link-aggregation");
                    CliPrintf (CliHandle, "\r\n");
                }
            }
            if (u1TlvTxEnable & LLDP_MAX_FRAME_SIZE_TLV_ENABLED)
            {
                CliPrintf (CliHandle, "lldp tlv-select dot3tlv");
                CliPrintf (CliHandle, " max-framesize");
                CliPrintf (CliHandle, "\r\n");
            }
        }
    }
}

/*****************************************************************************/
/*     FUNCTION NAME  : LldpShowRunningConfigBasicTLVDetails                 */
/*                                                                           */
/*     DESCRIPTION    : This function displays the Basic TLV details         */
/*                                                                           */
/*     INPUT          : CliHandle - Handle to the cli context                */
/*                      i4Index - Interface Index                            */
/*                                                                           */
/*     OUTPUT         : None                                                 */
/*                                                                           */
/*     RETURNS        : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
LldpShowRunningConfigBasicTLVDetails (tCliHandle CliHandle, INT4 i4Index)
{
    tSNMP_OCTET_STRING_TYPE TlvsTxEnable;
    tSNMP_OCTET_STRING_TYPE ManAddr;
    tSNMP_OCTET_STRING_TYPE PrevManAddr;
    tSNMP_OCTET_STRING_TYPE ManAddrTlvTxEnable;
    CHR1               *pc1DispManAddr = NULL;
    tLldpLocPortInfo   *pPortInfo = NULL;
    UINT4               u4ManAddr = 0;
    INT4                i4LldpData = 0;
    INT4                i4TlvData = 0;
    INT4                i4PrevData = 0;
    INT4                i4RetVal = 0;
    INT4                i4PrevIfIndex = 0;
    UINT4               u4PrevDestMacIndex = 0;
    INT4                i4Result = OSIX_FALSE;
    UINT4               u4LocPort = 0;
    UINT4               u4DestIndex = 0;
    UINT1               au1MacAddr[LLDP_CLI_MAX_MAC_STRING_SIZE] = { 0 };
    UINT1               au1ManAddrTlvTxEnable[LLDP_MAN_ADDR_TX_EN_MAX_BYTES] =
        { 0 };
    UINT1               au1ManAddr[LLDP_MAX_LEN_MAN_ADDR + 1] = { 0 };
    UINT1               au1PrevManAddr[LLDP_MAX_LEN_MAN_ADDR + 1] = { 0 };
    CHR1                ac1DispManAddr[LLDP_MAX_LEN_MAN_ADDR + 1] = { 0 };
    tMacAddr            DestAddr;
    UINT1               u1Flag = LLDP_FALSE;
    UINT1               u1String = 0;
    UINT1               u1TlvTxEnable = 0;
    INT4                i4ifIndex = i4Index;

    MEMSET (&TlvsTxEnable, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ManAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ManAddrTlvTxEnable, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ManAddrTlvTxEnable, 0, LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
    MEMSET (&ac1DispManAddr, 0, (LLDP_MAX_LEN_MAN_ADDR + 1));
    MEMSET (au1ManAddr, 0, (LLDP_MAX_LEN_MAN_ADDR + 1));
    MEMSET (&PrevManAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1PrevManAddr, 0, (LLDP_MAX_LEN_MAN_ADDR + 1));
    MEMSET (&DestAddr, 0, sizeof (tMacAddr));

    TlvsTxEnable.pu1_OctetList = &u1String;
    TlvsTxEnable.i4_Length = sizeof (u1String);
    ManAddr.pu1_OctetList = au1ManAddr;
    ManAddrTlvTxEnable.pu1_OctetList = au1ManAddrTlvTxEnable;
    ManAddrTlvTxEnable.i4_Length = LLDP_MAN_ADDR_TX_EN_MAX_BYTES;
    PrevManAddr.pu1_OctetList = au1PrevManAddr;

    if (nmhGetFirstIndexLldpV2PortConfigTable (&i4Index, &u4DestIndex)
        == SNMP_FAILURE)
    {
        return;
    }
    do
    {
        nmhGetLldpV2DestMacAddress (u4DestIndex, &DestAddr);

        nmhGetFsLldpModuleStatus (&i4LldpData);

        if (i4LldpData != LLDP_DISABLED)
        {
            i4RetVal =
                nmhGetLldpV2PortConfigTLVsTxEnable (i4Index, u4DestIndex,
                                                    &TlvsTxEnable);

            if (i4RetVal != SNMP_FAILURE)
            {
                PrintMacAddress (DestAddr, au1MacAddr);
                MEMCPY (&u1TlvTxEnable, TlvsTxEnable.pu1_OctetList,
                        TlvsTxEnable.i4_Length);
                if ((u1TlvTxEnable != 0) && (i4Index == i4ifIndex))
                {
                    u1Flag = LLDP_TRUE;

                    if (u1TlvTxEnable & LLDP_PORT_DESC_TLV_ENABLED)
                    {
                        CliPrintf (CliHandle, "lldp tlv-select basic-tlv");
                        CliPrintf (CliHandle, " port-descr");
                        if ((u4DestIndex != DEFAULT_DEST_MAC_ADDR_INDEX)
                            && (u1Flag == LLDP_TRUE))
                        {
                            CliPrintf (CliHandle, " mac-address %s",
                                       au1MacAddr);
                            CliPrintf (CliHandle, "\r\n");
                        }
                        else
                        {
                            CliPrintf (CliHandle, "\r\n");
                        }
                    }

                    if (u1TlvTxEnable & LLDP_SYS_NAME_TLV_ENABLED)
                    {
                        CliPrintf (CliHandle, "lldp tlv-select basic-tlv");
                        CliPrintf (CliHandle, " sys-name");
                        if ((u4DestIndex != DEFAULT_DEST_MAC_ADDR_INDEX)
                            && (u1Flag == LLDP_TRUE))
                        {
                            CliPrintf (CliHandle, " mac-address %s",
                                       au1MacAddr);
                            CliPrintf (CliHandle, "\r\n");
                        }
                        else
                        {
                            CliPrintf (CliHandle, "\r\n");
                        }
                    }

                    if (u1TlvTxEnable & LLDP_SYS_DESC_TLV_ENABLED)
                    {
                        CliPrintf (CliHandle, "lldp tlv-select basic-tlv");
                        CliPrintf (CliHandle, " sys-descr");
                        if ((u4DestIndex != DEFAULT_DEST_MAC_ADDR_INDEX)
                            && (u1Flag == LLDP_TRUE))
                        {
                            CliPrintf (CliHandle, " mac-address %s",
                                       au1MacAddr);
                            CliPrintf (CliHandle, "\r\n");
                        }
                        else
                        {
                            CliPrintf (CliHandle, "\r\n");
                        }
                    }
                    if (u1TlvTxEnable & LLDP_SYS_CAPAB_TLV_ENABLED)
                    {
                        CliPrintf (CliHandle, "lldp tlv-select basic-tlv");
                        CliPrintf (CliHandle, " sys-capab");
                        if ((u4DestIndex != DEFAULT_DEST_MAC_ADDR_INDEX)
                            && (u1Flag == LLDP_TRUE))
                        {
                            CliPrintf (CliHandle, " mac-address %s",
                                       au1MacAddr);
                            CliPrintf (CliHandle, "\r\n");
                        }
                        else
                        {
                            CliPrintf (CliHandle, "\r\n");
                        }
                    }
                }

            }

            nmhGetFslldpv2ConfigPortMapNum (i4Index, DestAddr,
                                            (INT4 *) &u4LocPort);
            pPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPort);
            if (pPortInfo == NULL)
            {
                LLDP_TRC (ALL_FAILURE_TRC,
                          " SNMPSTD: Agent Info not found \r\n");
                return;
            }

            if ((pPortInfo->u1ManAddrTlvTxEnabled == LLDP_TRUE)
                && (i4Index == i4ifIndex))
            {
                if (nmhGetFirstIndexLldpV2LocManAddrTable (&i4TlvData,
                                                           &ManAddr) ==
                    SNMP_FAILURE)
                {
                    return;
                }

                do
                {
                    MEMSET (&ManAddrTlvTxEnable, 0, sizeof
                            (tSNMP_OCTET_STRING_TYPE));

                    ManAddrTlvTxEnable.pu1_OctetList = au1ManAddrTlvTxEnable;

                    nmhGetLldpV2ManAddrConfigTxEnable (i4Index, u4DestIndex,
                                                       i4TlvData, &ManAddr,
                                                       &i4Result);
                    if (i4Result == OSIX_TRUE)
                    {
                        pc1DispManAddr = &ac1DispManAddr[0];
                        switch (i4TlvData)
                        {
                            case IPVX_ADDR_FMLY_IPV4:
                                PTR_FETCH4 (u4ManAddr, ManAddr.pu1_OctetList);
                                /* Convert octet values into strings of octets
                                 *** seperated by dot */
                                CLI_CONVERT_IPADDR_TO_STR (pc1DispManAddr,
                                                           u4ManAddr);
                                if (u1TlvTxEnable == 0)
                                {
                                    u1Flag = LLDP_TRUE;
                                }
                                CliPrintf (CliHandle,
                                           "lldp tlv-select basic-tlv");
                                CliPrintf (CliHandle, " mgmt-addr ipv4 %s",
                                           pc1DispManAddr);
                                if (u4DestIndex != DEFAULT_DEST_MAC_ADDR_INDEX)
                                {
                                    CliPrintf (CliHandle, " mac-address %s",
                                               au1MacAddr);
                                    CliPrintf (CliHandle, "\r\n");
                                }
                                else
                                {
                                    CliPrintf (CliHandle, "\r\n");
                                }
                                break;
                            case IPVX_ADDR_FMLY_IPV6:
                                STRNCPY (pc1DispManAddr,
                                         Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                       ManAddr.pu1_OctetList),
                                         STRLEN (Ip6PrintNtop
                                                 ((tIp6Addr *) (VOID *) ManAddr.
                                                  pu1_OctetList)));
                                pc1DispManAddr[STRLEN
                                               (Ip6PrintNtop
                                                ((tIp6Addr *) (VOID *) ManAddr.
                                                 pu1_OctetList))] = '\0';
                                if (u1TlvTxEnable == 0)
                                {
                                    u1Flag = LLDP_TRUE;
                                }
                                CliPrintf (CliHandle,
                                           "lldp tlv-select basic-tlv");
                                CliPrintf (CliHandle, " mgmt-addr ipv6 %s",
                                           pc1DispManAddr);
                                if (u4DestIndex != DEFAULT_DEST_MAC_ADDR_INDEX)
                                {
                                    CliPrintf (CliHandle, " mac-address %+28s",
                                               au1MacAddr);
                                    CliPrintf (CliHandle, "\r\n");
                                }
                                else
                                {
                                    CliPrintf (CliHandle, "\r\n");
                                }
                                break;
                            default:
                                u1Flag = LLDP_FALSE;
                                break;
                        }
                    }
                    i4PrevData = i4TlvData;
                    PrevManAddr.i4_Length = ManAddr.i4_Length;
                    MEMCPY (PrevManAddr.pu1_OctetList, ManAddr.pu1_OctetList,
                            ManAddr.i4_Length);
                }
                while (nmhGetNextIndexLldpLocManAddrTable (i4PrevData,
                                                           &i4TlvData,
                                                           &PrevManAddr,
                                                           &ManAddr) ==
                       SNMP_SUCCESS);
            }

        }

        i4PrevIfIndex = i4Index;
        u4PrevDestMacIndex = u4DestIndex;

    }
    while (nmhGetNextIndexLldpV2PortConfigTable
           (i4PrevIfIndex, &i4Index, u4PrevDestMacIndex,
            &u4DestIndex) == SNMP_SUCCESS);

}

/*****************************************************************************
 *     FUNCTION NAME  : LldpMedShowRunningAdminStatus                        *
 *                                                                           *
 *     DESCRIPTION    : This function displays the LLDP-MED Admin status     *
 *                                                                           *
 *     INPUT          : CliHandle - Handle to the cli context                *
 *                      i4Index - Interface Index                            *
 *                                                                           *
 *     OUTPUT         : None                                                 *
 *                                                                           *
 *     RETURNS        : None                                                 *
 *                                                                           *
 *****************************************************************************/
VOID
LldpMedShowRunningAdminStatus (tCliHandle CliHandle, INT4 i4Index)
{
    INT4                i4MedAdminStatus = 0;

    nmhGetFsLldpMedAdminStatus (i4Index, &i4MedAdminStatus);

    if (i4MedAdminStatus == LLDP_ENABLED)
    {
        CliPrintf (CliHandle, "set lldp-med enable");
        CliPrintf (CliHandle, "\r\n");
    }
    return;
}

/*****************************************************************************
 *     FUNCTION NAME  : LldpMedShowRunningConfigTLVInfo                      *
 *                                                                           *
 *     DESCRIPTION    : This function displays the LLDP MED TLV details      *
 *                                                                           *
 *     INPUT          : CliHandle - Handle to the cli context                *
 *                      i4Index - Interface Index                            *
 *                                                                           *
 *     OUTPUT         : None                                                 *
 *                                                                           *
 *     RETURNS        : None                                                 *
 *                                                                           *
 *****************************************************************************/
VOID
LldpMedShowRunningConfigTLVInfo (tCliHandle CliHandle, INT4 i4Index)
{
    tSNMP_OCTET_STRING_TYPE TlvsEnable;
    tMacAddr            DestAddr;
    UINT4               u4DestMacIndex = 0;
    UINT4               u4PrevDestMacIndex = 0;
    INT4                i4PrevIfIndex = 0;
    INT4                i4IfIndex = 0;
    INT4                i4RetVal = 0;
    INT4                i4LldpData = 0;
    UINT2               u2TlvsEnable = 0;
    UINT1               au1MacAddr[LLDP_CLI_MAX_MAC_STRING_SIZE] = { 0 };

    MEMSET (&TlvsEnable, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&DestAddr, 0, sizeof (tMacAddr));

    TlvsEnable.pu1_OctetList = (UINT1 *) &u2TlvsEnable;
    TlvsEnable.i4_Length = sizeof (u2TlvsEnable);

    i4IfIndex = i4Index;
    if (nmhGetFirstIndexFsLldpMedPortConfigTable (&i4Index, &u4DestMacIndex)
        != SNMP_SUCCESS)
    {
        return;
    }

    do
    {
        /* Get LLDP Module Status */
        nmhGetFsLldpModuleStatus (&i4LldpData);
        /* Get Destnation MacAddress from MacAddressIndex */
        nmhGetLldpV2DestMacAddress (u4DestMacIndex, &DestAddr);

        /* Check whether LLDP is enabled or disabled */
        if (i4LldpData != LLDP_DISABLED)
        {
            i4RetVal = nmhGetFsLldpMedPortConfigTLVsTxEnable (i4Index,
                                                              u4DestMacIndex,
                                                              &TlvsEnable);
            if (i4RetVal != SNMP_FAILURE)
            {
                /* Convert Octet to String type */
                PrintMacAddress (DestAddr, au1MacAddr);
                /* Check whether TlvsEnable is empty and Interface Index
                 * is same as the input given */
                if ((u2TlvsEnable != 0) && (i4Index == i4IfIndex))
                {
                    /* Check if Med Capability TLV is enabled */
                    if (u2TlvsEnable & LLDP_MED_CAPABILITY_TLV)
                    {
                        CliPrintf (CliHandle, "lldp med-tlv-select ");
                        CliPrintf (CliHandle, "med-capability ");

                        if (u4DestMacIndex != DEFAULT_DEST_MAC_ADDR_INDEX)
                        {
                            CliPrintf (CliHandle, "mac-address %s", au1MacAddr);
                            CliPrintf (CliHandle, "\r\n");
                        }
                        else
                        {
                            CliPrintf (CliHandle, "\r\n");
                        }
                    }

                    /* Check if Network Policy TLV is enabled */
                    if (u2TlvsEnable & LLDP_MED_NETWORK_POLICY_TLV)
                    {
                        CliPrintf (CliHandle, "lldp med-tlv-select ");
                        CliPrintf (CliHandle, "network-policy ");

                        if (u4DestMacIndex != DEFAULT_DEST_MAC_ADDR_INDEX)
                        {
                            CliPrintf (CliHandle, "mac-address %s", au1MacAddr);
                            CliPrintf (CliHandle, "\r\n");
                        }
                        else
                        {
                            CliPrintf (CliHandle, "\r\n");
                        }
                    }

                    /* Check if Inventory Management TLV is enabled */
                    if (u2TlvsEnable & LLDP_MED_INVENTORY_MGMT_TLV)
                    {
                        CliPrintf (CliHandle, "lldp med-tlv-select ");
                        CliPrintf (CliHandle, "inventory-management ");

                        if (u4DestMacIndex != DEFAULT_DEST_MAC_ADDR_INDEX)
                        {
                            CliPrintf (CliHandle, "mac-address %s", au1MacAddr);
                            CliPrintf (CliHandle, "\r\n");
                        }
                        else
                        {
                            CliPrintf (CliHandle, "\r\n");
                        }
                    }

                    /* Check if Location Identification TLV is enabled */
                    if (u2TlvsEnable & LLDP_MED_LOCATION_ID_TLV)
                    {
                        CliPrintf (CliHandle, "lldp med-tlv-select ");
                        CliPrintf (CliHandle, "location-id ");

                        if (u4DestMacIndex != DEFAULT_DEST_MAC_ADDR_INDEX)
                        {
                            CliPrintf (CliHandle, "mac-address %s", au1MacAddr);
                            CliPrintf (CliHandle, "\r\n");
                        }
                        else
                        {
                            CliPrintf (CliHandle, "\r\n");
                        }
                    }

                    /* Check if LLDP MED Power via MDI PSE is enabled */
                    if (u2TlvsEnable & LLDP_MED_PW_MDI_PSE_TLV)
                    {
                        CliPrintf (CliHandle, "lldp med-tlv-select ");
                        CliPrintf (CliHandle, "ex-power-via-mdi ");

                        if (u4DestMacIndex != DEFAULT_DEST_MAC_ADDR_INDEX)
                        {
                            CliPrintf (CliHandle, "mac-address %s", au1MacAddr);
                            CliPrintf (CliHandle, "\r\n");
                        }
                        else
                        {
                            CliPrintf (CliHandle, "\r\n");
                        }
                    }

                }
            }
        }
        i4PrevIfIndex = i4Index;
        u4PrevDestMacIndex = u4DestMacIndex;
    }
    /* Get the next Index of Port Config Table */
    while (nmhGetNextIndexFsLldpMedPortConfigTable (i4PrevIfIndex,
                                                    &i4Index,
                                                    u4PrevDestMacIndex,
                                                    &u4DestMacIndex)
           == SNMP_SUCCESS);
    return;

}

/*****************************************************************************
 *   FUNCTION NAME  : LldpMedShowRunningConfigNwpol                          *
 *                                                                           *
 *   DESCRIPTION    : This function displays the LLDP MED Policy Info details*
 *                                                                           *
 *   INPUT          : CliHandle - Handle to the cli context                  *
 *                    i4Index - Interface Index                              *
 *                                                                           *
 *   OUTPUT         : None                                                   *
 *                                                                           *
 *   RETURNS        : None                                                   *
 *                                                                           *
 *****************************************************************************/

VOID
LldpMedShowRunningConfigNwPol (tCliHandle CliHandle, INT4 i4Index)
{
    tSNMP_OCTET_STRING_TYPE PolicyAppType;
    tSNMP_OCTET_STRING_TYPE PrevPolicyAppType;
    INT4                i4PrevIfIndex = 0;
    INT4                i4ifIndex = 0;
    INT4                i4LldpData = 0;
    INT4                i4RowStatus = 0;
    INT4                i4PolicyUnknown = 0;
    INT4                i4VlanType = 0;
    INT4                i4VlanId = 0;
    INT4                i4Priority = 0;
    INT4                i4Dscp = 0;
    UINT1               u1PolicyAppType = 0;
    UINT1               u1PrevPolicyAppType = 0;

    MEMSET (&PolicyAppType, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PrevPolicyAppType, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    PolicyAppType.pu1_OctetList = &u1PolicyAppType;
    PolicyAppType.i4_Length = LLDP_MED_MAX_LEN_APP_TYPE;

    PrevPolicyAppType.pu1_OctetList = &u1PrevPolicyAppType;
    PrevPolicyAppType.i4_Length = LLDP_MED_MAX_LEN_APP_TYPE;

    i4ifIndex = i4Index;
    if (nmhGetFirstIndexFsLldpMedLocMediaPolicyTable (&i4Index,
                                                      &PolicyAppType) !=
        SNMP_SUCCESS)
    {
        return;
    }

    do
    {
        if (nmhGetFsLldpMedLocMediaPolicyRowStatus (i4Index,
                                                    &PolicyAppType,
                                                    &i4RowStatus) !=
            SNMP_SUCCESS)
        {
            return;
        }

        if ((u1PolicyAppType != 0) && (i4Index == i4ifIndex)
            && (i4RowStatus == ACTIVE))
        {
            nmhGetFsLldpModuleStatus (&i4LldpData);

            /* Check whether LLDP iS enabled or Disabled */
            if (i4LldpData != LLDP_DISABLED)
            {
                CliPrintf (CliHandle, "lldp med-app-type ");

                switch (u1PolicyAppType)
                {
                        /* Check if Voice Application is enabled */
                    case LLDP_MED_VOICE_APP:

                        CliPrintf (CliHandle, "voice ");
                        break;

                        /* Check if Voice Signaling Application is enabled */
                    case LLDP_MED_VOICE_SIGNALING_APP:

                        CliPrintf (CliHandle, "voiceSignaling ");
                        break;

                        /* Check if Guest Voice Application is enabled */
                    case LLDP_MED_GUEST_VOICE_APP:

                        CliPrintf (CliHandle, "guestVoice ");
                        break;

                        /* Check if Guest Voice Signaling Application is enabled */
                    case LLDP_MED_GUEST_VOICE_SIGNALING_APP:

                        CliPrintf (CliHandle, "guestVoiceSignaling ");
                        break;

                        /* Check if Soft Phone Voice Application is enabled */
                    case LLDP_MED_SOFT_PHONE_VOICE_APP:

                        CliPrintf (CliHandle, "softPhoneVoice ");
                        break;

                        /* Check if Video Conferencing Application is enabled */
                    case LLDP_MED_VIDEO_CONFERENCING_APP:

                        CliPrintf (CliHandle, "videoconferencing ");
                        break;

                        /* Check if Streaming Video Application is enabled */
                    case LLDP_MED_STREAMING_VIDEO_APP:

                        CliPrintf (CliHandle, "streamingVideo ");
                        break;

                        /* Check if Video Signaling  Application is enabled */
                    case LLDP_MED_VIDEO_SIGNALING_APP:

                        CliPrintf (CliHandle, "videoSignaling ");
                        break;

                    default:
                        break;

                }

                if (nmhGetFsLldpMedLocMediaPolicyUnknown (i4Index,
                                                          &PolicyAppType,
                                                          &i4PolicyUnknown) !=
                    SNMP_SUCCESS)
                {
                    return;
                }

                /* Check whether Unknown Policy flag is enabled */
                if ((BOOL1) i4PolicyUnknown == LLDP_MED_UNKNOWN_FLAG)
                {
                    CliPrintf (CliHandle, "none ");
                }
                else
                {
                    if (nmhGetFsLldpMedLocMediaPolicyTagged (i4Index,
                                                             &PolicyAppType,
                                                             &i4VlanType) !=
                        SNMP_SUCCESS)
                    {
                        return;
                    }
                    CliPrintf (CliHandle, "vlan ");

                    /* Check whether Vlan is Tagged or Untagged */
                    if ((BOOL1) i4VlanType != LLDP_MED_VLAN_TAGGED)
                    {
                        CliPrintf (CliHandle, "untagged ");
                    }
                    else
                    {
                        /* Get VlanId if Vlan is Tagged */
                        if (nmhGetFsLldpMedLocMediaPolicyVlanID (i4Index,
                                                                 &PolicyAppType,
                                                                 &i4VlanId)
                            != SNMP_SUCCESS)
                        {
                            return;
                        }
                        CliPrintf (CliHandle, "vlan-id %d ", i4VlanId);

                        /* Get Priority if Vlan is Tagged */
                        if (nmhGetFsLldpMedLocMediaPolicyPriority (i4Index,
                                                                   &PolicyAppType,
                                                                   &i4Priority)
                            != SNMP_SUCCESS)
                        {
                            return;
                        }
                        CliPrintf (CliHandle, "Priority %d ", i4Priority);

                    }

                    /* Get Dscp value */
                    if (nmhGetFsLldpMedLocMediaPolicyDscp (i4Index,
                                                           &PolicyAppType,
                                                           &i4Dscp)
                        != SNMP_SUCCESS)
                    {
                        return;
                    }
                    CliPrintf (CliHandle, "dscp %d ", i4Dscp);
                }
                CliPrintf (CliHandle, "\r\n");
            }
        }
        i4PrevIfIndex = i4Index;
        u1PrevPolicyAppType = 0;
        MEMSET (&PrevPolicyAppType, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

        PrevPolicyAppType.pu1_OctetList = &u1PrevPolicyAppType;
        PrevPolicyAppType.i4_Length = LLDP_MED_MAX_LEN_APP_TYPE;

        MEMCPY (PrevPolicyAppType.pu1_OctetList,
                PolicyAppType.pu1_OctetList, sizeof (UINT1));
        PrevPolicyAppType.i4_Length = PolicyAppType.i4_Length;

        u1PolicyAppType = 0;
        MEMSET (&PolicyAppType, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        PolicyAppType.pu1_OctetList = &u1PolicyAppType;
        PolicyAppType.i4_Length = LLDP_MED_MAX_LEN_APP_TYPE;
        i4Index = 0;

    }
    while (nmhGetNextIndexFsLldpMedLocMediaPolicyTable (i4PrevIfIndex,
                                                        &i4Index,
                                                        &PrevPolicyAppType,
                                                        &PolicyAppType)
           == SNMP_SUCCESS);

    return;
}

/*******************************************************************************
 *   FUNCTION NAME  : LldpMedShowRunningConfigLocInfo                          *
 *                                                                             *
 *   DESCRIPTION    : This function displays the LLDP MED Location Info details*
 *                                                                             *
 *   INPUT          : CliHandle - Handle to the cli context                    *
 *                    i4Index - Interface Index                                *
 *                                                                             *
 *   OUTPUT         : None                                                     *
 *                                                                             *
 *   RETURNS        : None                                                     *
 *                                                                             *
 *****************************************************************************/
VOID
LldpMedShowRunningConfigLocInfo (tCliHandle CliHandle, INT4 i4Index)
{
    tSNMP_OCTET_STRING_TYPE LldpMedLocationInfo;
    UINT1               au1LocInfo[LLDP_MED_MAX_LOC_LENGTH];
    UINT1               au1LocStr[LLDP_MED_MAX_LOC_LENGTH];
    UINT1               au1CaValue[LLDP_MED_MAX_CA_TYPE_LENGTH];
    UINT1               au1CountryCode[3];
    INT4                i4LocationSubType = 0;
    INT4                i4PrevLocationSubType = 0;
    INT4                i4PrevIfIndex = 0;
    INT4                i4ifIndex = 0;
    INT4                i4RowStatus = 0;
    INT4                i4LldpData = 0;

    MEMSET (&LldpMedLocationInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1LocInfo, 0, LLDP_MED_MAX_LOC_LENGTH);
    MEMSET (au1LocStr, 0, LLDP_MED_MAX_LOC_LENGTH);
    MEMSET (&au1CaValue, 0, LLDP_MED_MAX_CA_TYPE_LENGTH);
    MEMSET (&au1CountryCode, 0, 3);
    LldpMedLocationInfo.pu1_OctetList = au1LocInfo;
    LldpMedLocationInfo.i4_Length = LLDP_MED_MAX_LOC_LENGTH;

    i4ifIndex = i4Index;
    if (nmhGetFirstIndexFsLldpMedLocLocationTable (&i4Index,
                                                   &i4LocationSubType) !=
        SNMP_SUCCESS)
    {
        return;
    }

    do
    {
        if (nmhGetFsLldpMedLocLocationRowStatus (i4Index,
                                                 i4LocationSubType,
                                                 &i4RowStatus) != SNMP_SUCCESS)
        {
            return;
        }

        if ((i4Index == i4ifIndex))
        {
            nmhGetFsLldpModuleStatus (&i4LldpData);
            /* Check whether LLDP iS enabled or Disabled */
            if (i4LldpData != LLDP_DISABLED)
            {
                CliPrintf (CliHandle, "lldp med-location ");

                if (nmhGetLldpXMedLocLocationInfo (i4Index,
                                                   i4LocationSubType,
                                                   &LldpMedLocationInfo) !=
                    SNMP_SUCCESS)
                {
                    return;
                }

                switch (i4LocationSubType)
                {

                    case LLDP_MED_COORDINATE_LOC:

                        CliPrintf (CliHandle, "coordinate-location ");
                        CliPrintf (CliHandle, "location-id ");
                        /* converting Location Info to Octet string */
                        LldpCliLocToStr (au1LocInfo, au1LocStr,
                                         (UINT4) LldpMedLocationInfo.i4_Length);
                        CliPrintf (CliHandle, "%s ", au1LocStr);

                        break;

                    case LLDP_MED_CIVIC_LOC:

                        CliPrintf (CliHandle, "civic-location ");
                        CliPrintf (CliHandle, "location-id ");
                        /* converting Location Info to Octet string */
                        LldpCliLocToStr (au1LocInfo, au1LocStr,
                                         (UINT4) LldpMedLocationInfo.i4_Length);
                        CliPrintf (CliHandle, "%s ", au1LocStr);

                        break;

                    case LLDP_MED_ELIN_LOC:

                        CliPrintf (CliHandle, "elin-location ");
                        CliPrintf (CliHandle, "location-id ");
                        CliPrintf (CliHandle, "%s ", au1LocInfo);

                        break;

                    default:

                        break;

                }
                CliPrintf (CliHandle, "\r\n");
            }
        }

        i4PrevIfIndex = i4Index;
        i4PrevLocationSubType = i4LocationSubType;

        MEMSET (&LldpMedLocationInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1LocInfo, 0, 256);
        LldpMedLocationInfo.pu1_OctetList = au1LocInfo;
        LldpMedLocationInfo.i4_Length = LLDP_MED_MAX_LOC_LENGTH;
        i4Index = 0;
        i4LocationSubType = 0;
    }
    while (nmhGetNextIndexFsLldpMedLocLocationTable (i4PrevIfIndex,
                                                     &i4Index,
                                                     i4PrevLocationSubType,
                                                     &i4LocationSubType) ==
           SNMP_SUCCESS);
    return;
}

/*************************************************************************
 *
 *  FUNCTION NAME   : LldpMemRelease
 *
 *  DESCRIPTION     : This function Release the memory for the given
 *                    PoolId
 *  INPUT           : tMemPoolId - MemPool ID
 *                    u4RelCount - No of Release Count
 *                    ...        - Variable command argument list
 *
 *  OUTPUT          : None
 *
 *  RETURNS         : LLDP_TRUE/LLDP_FALSE
 *
 ***************************************************************************/

PRIVATE INT4
LldpMemRelease (tMemPoolId PoolIdInfo, INT4 u4RelCount, ...)
{
    va_list             toRelease;
    INT4                u4Index = 0;
    INT4                i4MemLeak = 0;
    UINT1              *pRelease = NULL;

    va_start (toRelease, u4RelCount);
    for (u4Index = 0; u4Index < u4RelCount; u4Index++)
    {
        pRelease = va_arg (toRelease, UINT1 *);
        if (MemReleaseMemBlock (PoolIdInfo, pRelease) == MEM_FAILURE)
        {
            i4MemLeak++;
            continue;
        }
    }
    va_end (toRelease);
    if (i4MemLeak > 0)
        return LLDP_FALSE;

    return LLDP_TRUE;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetDstMac
 *
 *     DESCRIPTION      : This function is used to set/reset the destination
 *                        MAC for the LLDP agent on this port.
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Status   - Lldp Module status
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliSetDstMac (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 *pu1MacAddr,
                  UINT1 u1RowStatus)
{
    UINT1               u1FatalError = OSIX_FALSE;

    if (LldpUtilSetDstMac (i4IfIndex, pu1MacAddr, u1RowStatus,
                           &u1FatalError) == OSIX_FAILURE)
    {
        if (u1FatalError == OSIX_TRUE)
        {
            CLI_FATAL_ERROR (CliHandle);
        }
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliShowOpt
 *
 *     DESCRIPTION      : This function is used to set/reset the destination
 *                        MAC for the LLDP agent on this port.
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Status   - Lldp Module status
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliShowOpt (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 *pu1MacAddr,
                UINT1 *pu1ShowOpt)
{
    UNUSED_PARAM (CliHandle);
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
    {
        if (pu1MacAddr != NULL)
        {
            CliPrintf (CliHandle,
                       "\r\n %%The Option is Valid in v2 Version only\r\n");
            return CLI_FAILURE;
        }

        else if (i4IfIndex == 0)
        {
            *pu1ShowOpt = LLDP_CLI_SHOW_ALL;
            pu1MacAddr = gau1LldpMcastAddr;
        }

        else
        {
            *pu1ShowOpt = LLDP_CLI_SHOW_INTERFACE;
            pu1MacAddr = gau1LldpMcastAddr;
        }
    }
    else
    {
        if ((pu1MacAddr == NULL) && (i4IfIndex == 0))
        {
            *pu1ShowOpt = LLDP_CLI_SHOW_ALL;
        }

        else if ((i4IfIndex != 0) && (pu1MacAddr == NULL))
        {
            *pu1ShowOpt = LLDP_CLI_SHOW_INTERFACE;
        }

        else
        {
            *pu1ShowOpt = LLDP_CLI_SHOW_MAC;
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetModuleVersion
 *
 *     DESCRIPTION      : This function will enable/disable Lldp module
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Status   - Lldp Version
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliSetModuleVersion (tCliHandle CliHandle, INT4 i4Status)
{

    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fslldpv2Version (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhSetFslldpv2Version (i4Status);

    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetTxCreditMax
 *
 *     DESCRIPTION      : The maximum number of consecutive LLDPDUs that can be
 *                        transmitted at any time.
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Val      - Lldp credit Max Value
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliSetTxCreditMax (tCliHandle CliHandle, UINT4 u4Val)
{

    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2LldpV2TxCreditMax (&u4ErrorCode, u4Val) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\n\rThe Module Version is wrong or the Module is Shutdown \r\n");
        return CLI_FAILURE;
    }

    nmhSetLldpV2TxCreditMax (u4Val);
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetMessageFastTx
 *
 *     DESCRIPTION      : The interval at which LLDP frames are transmitted on
 *                        behalf of this LLDP agent during fast transmission period.
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Val      - Lldp Tx-Fast Value
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliSetMessageFastTx (tCliHandle CliHandle, UINT4 u4Val)
{

    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2LldpV2MessageFastTx (&u4ErrorCode, u4Val) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\n\rThe Module Version is wrong or the Module is Shutdown \r\n");
        return CLI_FAILURE;
    }

    nmhSetLldpV2MessageFastTx (u4Val);
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetTxFastInit
 *
 *     DESCRIPTION      : The maximum number of consecutive LLDPDUs that can be
 *                        transmitted at any time.
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Val      - Lldp Holdtime Multiplier
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliSetTxFastInit (tCliHandle CliHandle, UINT4 u4Val)
{

    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2LldpV2TxFastInit (&u4ErrorCode, u4Val) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\n\rThe Module Version is wrong or the Module is Shutdown \r\n");
        return CLI_FAILURE;
    }

    nmhSetLldpV2TxFastInit (u4Val);
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetMedFastRepeatCount
 *
 *     DESCRIPTION      : The number of successive LLDP frame transmissions 
 *                        for one complete Fast Start interval. 
 *                        
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4Val      - Lldp MedFastStartRepeatCount Value
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
LldpCliSetMedFastRepeatCount (tCliHandle CliHandle, UINT4 u4Val)
{

    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2LldpXMedFastStartRepeatCount (&u4ErrorCode, u4Val) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\n\rThe RepeatCount value is wrong or the Module is Shutdown \r\n");
        return CLI_FAILURE;
    }

    nmhSetLldpXMedFastStartRepeatCount (u4Val);
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliGetMacAddrIndex
 *
 *     DESCRIPTION      : To Get the Destination Mac Addr Index from the
 *                        Mac address.
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Val      - Lldp Holdtime Multiplier
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpCliGetMacAddrIndex (tCliHandle CliHandle, INT4 i4IfIndex,
                        UINT1 *pau1MacAddr, UINT4 *pu4DestMacAddrIndex,
                        UINT1 *pu1InterfaceFlag)
{
    UINT4               u4LocPortNum = 0;
    UINT4               u4DestMacIndex = 0;
    UNUSED_PARAM (CliHandle);

    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
    {
        if (pau1MacAddr != NULL)
        {
            CliPrintf (CliHandle,
                       "\r%% Mac-address can be configured only in version v2\r\n");
            return CLI_FAILURE;    /*for 2005 Version mac address should not be used */
        }
        else
        {
            *pu4DestMacAddrIndex = LLDP_V1_DEST_MAC_ADDR_INDEX (i4IfIndex);
            *pu1InterfaceFlag = OSIX_TRUE;    /* the Option has to set to all agents in the Interface */
            return CLI_SUCCESS;
        }
    }
    else
    {
        if (pau1MacAddr == NULL)
        {
            u4DestMacIndex = DEFAULT_DEST_MAC_ADDR_INDEX;
            *pu1InterfaceFlag = OSIX_TRUE;    /* the Option has to set to all agents in the Interface */
            if (nmhValidateIndexInstanceLldpV2PortConfigTable
                (i4IfIndex, u4DestMacIndex) == SNMP_SUCCESS)
            {
                *pu4DestMacAddrIndex = u4DestMacIndex;
                return CLI_SUCCESS;
            }
        }
        else if (pau1MacAddr != NULL)
        {
            if (nmhGetFslldpv2ConfigPortMapNum
                (i4IfIndex, pau1MacAddr,
                 (INT4 *) &u4LocPortNum) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid mac-address\r\n");
                return CLI_FAILURE;
            }
            if (LldpUtilGetDestMacAddrTblIndex
                (u4LocPortNum, pu4DestMacAddrIndex) == OSIX_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid mac-address\r\n");
                return CLI_FAILURE;
            }
            if (nmhValidateIndexInstanceLldpV2PortConfigTable
                (i4IfIndex, *pu4DestMacAddrIndex) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid mac-address\r\n");
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

INT4
LldpCliOctetToIfName (tCliHandle CliHandle, CHR1 * pu1FirstLine,
                      tSNMP_OCTET_STRING_TYPE * pOctetStr,
                      UINT4 u4MaxPorts, UINT4 u4MaxLen,
                      UINT1 u1Column, UINT4 *pu4PagingStatus,
                      UINT1 u1MaxPortsPerLine)
{
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1Flag = CLI_FALSE;
    UINT4               u4Port = 0;
    UINT2               u2Len = 0;
    UINT1               u1Counter = 0;
    UINT1               au1BlankSpace[CLI_MAX_COLS];
    UINT1               u1NewLineFlag = CLI_FALSE;
    UINT1               au1ManAddrTlvTxEnable[LLDP_MAN_ADDR_TX_EN_MAX_BYTES] =
        { 0 };
    tLldpLocPortInfo   *pLocPortInfo = NULL;
    BOOL1               bResult = OSIX_FALSE;

    if (pOctetStr->i4_Length > (INT4) u4MaxLen)
    {
        CliPrintf (CliHandle, "%% Octet Length is too big\r\n");
        return CLI_FAILURE;
    }

    if (pu1FirstLine != NULL)
    {
        u1Flag = CLI_TRUE;
    }

    /* This portion is used if the PortList is EMPTY */
    MEMSET (au1BlankSpace, 0, CLI_MAX_COLS);

    /* This check is introduced to avoid possible array overrun when
       pOctetStr->i4_Length exceeds CLI_MAX_COLS */

    if (pOctetStr->i4_Length > CLI_MAX_COLS)
    {
        for (u2Len = 0; u2Len < pOctetStr->i4_Length; u2Len++)
        {
            if (pOctetStr->pu1_OctetList[u2Len] != '\0')
            {
                break;
            }
        }
        if (u2Len == pOctetStr->i4_Length)
        {
            if (u1Flag == CLI_TRUE)
            {
                *pu4PagingStatus =
                    (UINT4) CliPrintf (CliHandle, "%s", pu1FirstLine);
            }
            CliPrintf (CliHandle, "\r\n");
            return CLI_SUCCESS;
        }
    }

    else if (MEMCMP (pOctetStr->pu1_OctetList,
                     au1BlankSpace, MEM_MAX_BYTES (pOctetStr->i4_Length,
                                                   CLI_MAX_COLS)) == 0)
    {
        if (u1Flag == CLI_TRUE)
        {
            *pu4PagingStatus =
                (UINT4) CliPrintf (CliHandle, "%s", pu1FirstLine);
        }
        CliPrintf (CliHandle, "\r\n");
        return CLI_SUCCESS;
    }

    /* u1Flag should be CLI_TRUE only if user knows exactly what to be
     * printed as first line pu1FirstLine before PortList
     *
     * if printing is going to happen somewhere in the middle then
     * u1Flag should be CLI_FALSE with proper u1Column value
     */
    if (u1Flag == CLI_TRUE)
    {
        /* Give minimum space for printing u1MaxPortsPerLine ports together.
         * Say for u1MaxPortsPerLine = 4 , it takes 31(4*8 -1) spaces
         * for "Gi0/11, Gi0/12, Gi0/13, Gi0/14"
         */
        if (((UINT1) STRLEN (pu1FirstLine)) >=
            (CLI_MAX_COLS - (u1MaxPortsPerLine * 8) - 1))
        {
            CliPrintf (CliHandle, "%% First Line is too big\r\n");
            return CLI_FAILURE;
        }

        /* 0x20 is Ascii code for SPACE */
        MEMSET (au1BlankSpace, 0x20,
                MEM_MAX_BYTES (sizeof (au1BlankSpace), STRLEN (pu1FirstLine)));

        au1BlankSpace[MEM_MAX_BYTES
                      (((sizeof (au1BlankSpace)) - 1), STRLEN (pu1FirstLine))] =
            '\0';
    }
    else
    {
        /* Give minimum space for printing u1MaxPortsPerLine ports together. it
         * takes 31 spaces "Gi0/11, Gi0/12, Gi0/13, Gi0/14"
         */
        if (u1Column >= (CLI_MAX_COLS - (u1MaxPortsPerLine * 8) - 1))
        {
            CliPrintf (CliHandle, "%% Column position is out of range\r\n");
            return CLI_FAILURE;
        }
        /* 0x20 is Ascii code for SPACE */
        MEMSET (au1BlankSpace, 0x20, u1Column);
        au1BlankSpace[u1Column] = '\0';
    }

    if (pOctetStr->pu1_OctetList)
    {
        for (u4Port = 1; u4Port <= (UINT4) LLDP_MAX_DEST_INDEX; u4Port++)
        {
            if (CliIsMemberPort (pOctetStr->pu1_OctetList,
                                 (UINT4) pOctetStr->i4_Length,
                                 u4Port) == OSIX_SUCCESS)
            {
                pLocPortInfo = LLDP_GET_LOC_PORT_INFO (u4Port);
                if (pLocPortInfo == NULL)
                {
                    CliPrintf (CliHandle, "%% Invalid LLDP Agent %d\r\n",
                               u4Port);

                    return CLI_FAILURE;
                }
                OSIX_BITLIST_SET_BIT (au1ManAddrTlvTxEnable,
                                      pLocPortInfo->i4IfIndex,
                                      LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
            }
        }
        for (u4Port = 1; u4Port <= u4MaxPorts; u4Port++)
        {
            OSIX_BITLIST_IS_BIT_SET (au1ManAddrTlvTxEnable, u4Port,
                                     LLDP_MAN_ADDR_TX_EN_MAX_BYTES, bResult);
            if (bResult == OSIX_TRUE)
            {
                if (CfaCliGetIfName (u4Port, (INT1 *) au1NameStr) ==
                    CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "%% Invalid Port %d\r\n", u4Port);

                    return CLI_FAILURE;
                }

                if (u1Counter == 0)
                {
                    if (u1Flag == CLI_TRUE)
                    {
                        *pu4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                                              "%s %s",
                                                              pu1FirstLine,
                                                              au1NameStr);
                    }
                    else
                    {
                        /* The first line alone needs to be
                         * printed like this. from next line
                         * onwards follow the same flow of
                         * known printing path
                         */
                        *pu4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                                              " %s",
                                                              au1NameStr);
                        u1Flag = CLI_TRUE;

                    }
                }
                else
                {
                    *pu4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                                          ", %s", au1NameStr);
                }
                u1Counter++;
                u1NewLineFlag = CLI_FALSE;

                /* Printing upto u1MaxPortsPerLine ports per line */
                if (u1Counter == u1MaxPortsPerLine)
                {
                    CliPrintf (CliHandle, "\r\n");
                    pu1FirstLine = (CHR1 *) au1BlankSpace;
                    u1NewLineFlag = CLI_TRUE;
                    u1Counter = 0;
                }
            }
        }
    }

    /* Ensure a new line is printed at the end if new line is not
     * printed while coming out
     */
    if (u1NewLineFlag == CLI_FALSE)
    {
        CliPrintf (CliHandle, "\r\n");
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpUtilSetDstMac
 *
 *     DESCRIPTION      : This function is used to set/reset the destination
 *                        MAC for the LLDP agent on this port.
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Status   - Lldp Module status
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpUtilSetDstMac (INT4 i4IfIndex, UINT1 *pu1MacAddr,
                   UINT1 u1RowStatus, UINT1 *pu1FatalError)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4DestIndex = 0;
    tLldpv2DestAddrTbl  DestAddrTbl;
    tLldpv2DestAddrTbl *pDestAddrTbl = NULL;
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpLocPortInfo   *ppLocPortEntry = NULL;
    UINT4               u4LocPortNum = 0;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    MEMSET (&DestAddrTbl, 0, sizeof (tLldpv2DestAddrTbl));

    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
    {
        CLI_SET_ERR (LLDP_CLI_INVALID_ADDR_V1);
        *pu1FatalError = OSIX_FALSE;
        return OSIX_FAILURE;
    }
    else
    {

        pDestAddrTbl = (tLldpv2DestAddrTbl *)
            RBTreeGetFirst (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree);

        while (pDestAddrTbl != NULL)
        {
            if (MEMCMP (pDestAddrTbl->Lldpv2DestMacAddress,
                        pu1MacAddr, MAC_ADDR_LEN) == 0)
            {
                u4DestIndex = pDestAddrTbl->u4LlldpV2DestAddrTblIndex;
                break;
            }
            DestAddrTbl.u4LlldpV2DestAddrTblIndex =
                pDestAddrTbl->u4LlldpV2DestAddrTblIndex;
            pDestAddrTbl = (tLldpv2DestAddrTbl *)
                RBTreeGetNext (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree,
                               &DestAddrTbl, LldpDstMacAddrUtlRBCmpInfo);
        }

        pLldpAgentPortInfo =
            LldpGetPortMapTableNode ((UINT4) i4IfIndex,
                                     (tMacAddr *) pu1MacAddr);
        if (u1RowStatus == LLDP_DESTROY)
        {
            if ((pDestAddrTbl == NULL) || (pLldpAgentPortInfo == NULL))
            {
                CLI_SET_ERR (LLDP_CLI_DEST_MAC_NOT_FOUND);
                *pu1FatalError = OSIX_FALSE;
                return OSIX_FAILURE;
            }
        }

        if ((pLldpAgentPortInfo != NULL) && (u4DestIndex != 0))
        {
            if (u1RowStatus == LLDP_CREATE_AND_GO)
            {
                /*Entry is already present just need to Make active. */
                u1RowStatus = LLDP_ACTIVE;
            }
            if (u1RowStatus == LLDP_DESTROY)
            {
                u4LocPortNum =
                    LldpGetLocalPortFromIfIndexDestMacIfIndex ((UINT4)
                                                               i4IfIndex,
                                                               u4DestIndex);
                ppLocPortEntry = LLDP_GET_LOC_PORT_INFO (u4LocPortNum);
                if (ppLocPortEntry != NULL)
                {
                    if (LldpTxUtlDelProtoVlanDLL (ppLocPortEntry) !=
                        OSIX_SUCCESS)
                    {
                        return OSIX_FAILURE;
                    }

                }
            }
            if (nmhTestv2Fslldpv2ConfigPortRowStatus (&u4ErrorCode, i4IfIndex,
                                                      pu1MacAddr,
                                                      u1RowStatus) ==
                SNMP_FAILURE)
            {
                LLDP_TRC (ALL_FAILURE_TRC, "DestMacAddrTbl Index not fund\r\n");
                return OSIX_FAILURE;
            }

            if (nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr,
                                                   u1RowStatus) == SNMP_FAILURE)
            {
                LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                               "\r\n%%Unable to set LLDP Agent as active  %d\r\n",
                               i4IfIndex);
                *pu1FatalError = OSIX_TRUE;
                return OSIX_FAILURE;
            }
            if (nmhTestv2Fslldpv2DestRowStatus (&u4ErrorCode, u4DestIndex,
                                                u1RowStatus) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }

            if (nmhSetFslldpv2DestRowStatus (u4DestIndex, u1RowStatus) ==
                SNMP_FAILURE)
            {
                return OSIX_FAILURE;

            }
            if (u1RowStatus == LLDP_DESTROY)
            {
                gu4LldpDestMacAddrTblIndex--;
            }

            return OSIX_SUCCESS;
        }

        if ((u1RowStatus == LLDP_CREATE_AND_GO))
        {
            /*New Entry Need to Create and then make as active. */
            u1RowStatus = CREATE_AND_WAIT;
            if ((u4DestIndex == 0) || (pLldpAgentPortInfo == NULL))
            {
                u4DestIndex = gu4LldpDestMacAddrTblIndex;
                DestTbl.u4LlldpV2DestAddrTblIndex = u4DestIndex;
                while ((u4DestIndex > 1)
                       && (u4DestIndex <= gu4LldpDestMacAddrTblIndex))
                {
                    pTempDestTbl =
                        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                                          Lldpv2DestMacAddrTblRBTree,
                                                          (tRBElem *) &
                                                          DestTbl);
                    if (pTempDestTbl != NULL)
                    {
                        u4DestIndex = u4DestIndex - 1;
                        DestTbl.u4LlldpV2DestAddrTblIndex = u4DestIndex;
                    }
                    else
                    {
                        u4DestIndex = DestTbl.u4LlldpV2DestAddrTblIndex;
                        break;
                    }
                }

                if (nmhTestv2Fslldpv2DestRowStatus (&u4ErrorCode, u4DestIndex,
                                                    (INT4) u1RowStatus) ==
                    SNMP_FAILURE)
                {
                    return OSIX_FAILURE;
                }
                if (nmhSetFslldpv2DestRowStatus (u4DestIndex, u1RowStatus) ==
                    SNMP_FAILURE)
                {
                    return OSIX_FAILURE;

                }
                if (nmhTestv2FslldpV2DestMacAddress (&u4ErrorCode, u4DestIndex,
                                                     pu1MacAddr) ==
                    SNMP_FAILURE)
                {
                    CLI_SET_ERR (LLDP_CLI_INVALID_DESTINATION_MAC);
                    nmhSetFslldpv2DestRowStatus (u4DestIndex, LLDP_DESTROY);
                    gu4LldpDestMacAddrTblIndex--;
                    return OSIX_FAILURE;
                }

                if (nmhSetFslldpV2DestMacAddress (u4DestIndex, pu1MacAddr) ==
                    SNMP_FAILURE)
                {
                    nmhSetFslldpv2DestRowStatus (u4DestIndex, LLDP_DESTROY);
                    gu4LldpDestMacAddrTblIndex--;
                    return OSIX_FAILURE;
                }
            }
        }
        /*For LLDP_DESTROY with u4DestIndex equal to zero will fail below. */
        if (nmhTestv2Fslldpv2ConfigPortRowStatus (&u4ErrorCode, i4IfIndex,
                                                  pu1MacAddr,
                                                  (INT4) u1RowStatus) ==
            SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, LLDP_DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            return OSIX_FAILURE;
        }

        if (nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr,
                                               (INT4) u1RowStatus) ==
            SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, LLDP_DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                           "\r\n%%Unable to create the  LLDP Agent  %d\r\n",
                           i4IfIndex);
            *pu1FatalError = OSIX_TRUE;
            return OSIX_FAILURE;
        }
        if (nmhTestv2FsLldpLocPortDstMac (&u4ErrorCode, i4IfIndex, pu1MacAddr)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (LLDP_CLI_INVALID_DESTINATION_MAC);
            nmhSetFslldpv2DestRowStatus (u4DestIndex, LLDP_DESTROY);
            nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr,
                                               LLDP_DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            return OSIX_FAILURE;
        }

        if (nmhSetFsLldpLocPortDstMac (i4IfIndex, pu1MacAddr) == SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, LLDP_DESTROY);
            nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr,
                                               LLDP_DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                           "\r\n%%Unable to set Destination MAC address for port %d\r\n",
                           i4IfIndex);
            *pu1FatalError = OSIX_TRUE;
            return OSIX_FAILURE;
        }
        if (nmhTestv2Fslldpv2DestRowStatus (&u4ErrorCode, u4DestIndex,
                                            LLDP_ACTIVE) == SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, LLDP_DESTROY);
            nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr,
                                               LLDP_DESTROY);
            gu4LldpDestMacAddrTblIndex--;

            return OSIX_FAILURE;
        }
        if (nmhSetFslldpv2DestRowStatus (u4DestIndex, LLDP_ACTIVE) ==
            SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, LLDP_DESTROY);
            nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr,
                                               LLDP_DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            return OSIX_FAILURE;

        }
        if (nmhTestv2Fslldpv2ConfigPortRowStatus (&u4ErrorCode, i4IfIndex,
                                                  pu1MacAddr,
                                                  LLDP_ACTIVE) == SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, LLDP_DESTROY);
            nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr,
                                               LLDP_DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            return OSIX_FAILURE;
        }

        if (nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr,
                                               LLDP_ACTIVE) == SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, LLDP_DESTROY);
            nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr,
                                               LLDP_DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                           "\r\n%%Unable to set LLDP Agent as active  %d\r\n",
                           i4IfIndex);
            *pu1FatalError = OSIX_TRUE;
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/*******************************************************************************
 *
 *     FUNCTION NAME    : LldpMedCliParseLocOctetStr
 *
 *     DESCRIPTION      : This function parses the Location Info octet string
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        LocId      - Octet string
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *******************************************************************************/

PUBLIC INT4
LldpMedCliParseLocOctetStr (tCliHandle CliHandle, UINT1 *LocId)
{
    UINT1               u1Start = 0;
    UINT1               u1End = 0;
    INT2                i2valid_flag = 0;
    UINT1               u1Count = 0;

    u1End = (UINT1) CLI_STRLEN (LocId);

    /* Location ID format should be 1:2:3:4:5:6 or 01:02:03:04:05:06 */
    for (u1Start = 0; u1Start < u1End; u1Start++)
    {
        if (isxdigit (LocId[u1Start]))
        {
            i2valid_flag = 1;
            u1Count++;
            /* Only two digits should be there between ":" */
            if (u1Count > 2)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Please enter in octet string <aa:bb:cc:dd....> format\r\n");

                return CLI_FAILURE;
            }
        }
        else if (LocId[u1Start] == ':' && u1Count <= 2)
        {
            if (i2valid_flag)
            {
                i2valid_flag = 0;
                u1Count = 0;
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r\n%%Please enter in octet string <aa:bb:cc:dd....> format\r\n");

                return CLI_FAILURE;
            }
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\n%%Please enter in octet string <aa:bb:cc:dd....> format\r\n");

            return CLI_FAILURE;
        }
    }
    if (!i2valid_flag)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Please enter in octet string <aa:bb:cc:dd....> format\r\n");

        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 *
 *     FUNCTION NAME    : LldpCliDotStrToLocInfo
 *
 *     DESCRIPTION      : This function converts the octet string to location info
 *
 *     INPUT            : CliHandle    - CliContext ID
 *                        pu1DotStr    - Octet string
 *                        i4LocType    - Location subtype
 *
 *
 *     OUTPUT           : pu1Loc       - Location info
 *
 *     RETURNS          : CLI_FAILURE or CLI_SUCCESS
 *
 *******************************************************************************/

PUBLIC INT4
LldpCliDotStrToLocInfo (tCliHandle CliHandle,
                        UINT1 *pu1DotStr, INT4 i4LocType, UINT1 *pu1Loc)
{
    UINT4               u4Value = 0;
    UINT1               u1Char = 0;
    UINT1               u1Pow = 1;
    UINT1               u1dotCount = 0;
    INT2                i2Index = 0;
    INT1                i1CharCount = 1;
    INT2                i2StrIndex = 0;
    INT1                i1MaxChar = 0;
    INT2                i2BaseIndex = 0;

    if (!pu1DotStr)
    {
        CliPrintf (CliHandle, "\r\n%%Please enter valid octet string\r\n");
        return CLI_FAILURE;
    }
    /* Get legth of octet string */
    i2StrIndex = (INT2) (STRLEN (pu1DotStr) - 1);

    /* Count number of ":" in octet string */
    for (i2Index = 0; i2Index <= i2StrIndex; i2Index++)
    {
        if (pu1DotStr[i2Index] == ':')
            u1dotCount++;
    }
    /* check Location Info range */
    if (i4LocType == LLDP_MED_CIVIC_LOC)
    {
        if (((u1dotCount + 1) < LLDP_MED_MIN_CIVIC_LOC_LENGTH) ||
            ((u1dotCount + 1) > LLDP_MED_MAX_CIVIC_LOC_LENGTH))
        {
            LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                      "LldpMedCliSetLocConfig : Civic location "
                      "length not within supported range!!\r\n");
            CLI_SET_ERR (LLDP_MED_CLI_INVALID_CIVIC_LEN);
            return CLI_FAILURE;
        }
    }

    if (i4LocType == LLDP_MED_COORDINATE_LOC)
    {
        if ((u1dotCount + 1) != LLDP_MED_MAX_COORDINATE_LOC_LENGTH)
        {
            LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                      "LldpMedCliSetLocConfig : Coordinate "
                      "location length not within supported range!!\r\n");
            CLI_SET_ERR (LLDP_MED_CLI_INVALID_COORDINATE_LEN);
            return CLI_FAILURE;
        }
    }

    i2Index = (INT2) ((i2StrIndex - u1dotCount) / 2);
    i1MaxChar = (u1dotCount == 2) ? 2 : 1;
    i2BaseIndex = (INT2) (i2Index - i1MaxChar);

    /* Convert Octet string to corresponding location info string */
    for (; (i2StrIndex >= 0) && (i2Index >= 0); i2StrIndex--, i1CharCount++)
    {
        if (ISXDIGIT (pu1DotStr[i2StrIndex]))
        {
            if (!ISDIGIT (pu1DotStr[i2StrIndex]))
            {
                u1Char = (UINT1) (10 + ((pu1DotStr[i2StrIndex]) - 'a'));
            }
            else
            {
                u1Char = pu1DotStr[i2StrIndex];
            }

            u4Value = (UINT4) ((0x0f & u1Char) * u1Pow) + u4Value;
            u1Pow = (UINT1) (u1Pow * 16);
        }
        (pu1Loc)[i2Index] = (UINT1) u4Value;

        if (!(i1CharCount % 2))
        {
            i2Index--;
            u4Value = 0;
            u1Pow = 1;
        }
        if (pu1DotStr[i2StrIndex] == ':')
        {
            i2Index = i2BaseIndex;
            i2BaseIndex = (INT2) (i2BaseIndex - i1MaxChar);
            u1Pow = 1;
            u4Value = 0;
            i1CharCount = 0;
        }
    }
    /* Check to avoid invalid LCI length for CIVIC */
    if (i4LocType == LLDP_MED_CIVIC_LOC)
    {
        if ((pu1Loc)[0] != u1dotCount)
        {
            CliPrintf (CliHandle, "\r\n%%Please enter correct LCI length\r\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 *
 *     FUNCTION NAME    : LldpCliLocToStr
 *
 *     DESCRIPTION      : This function converts the location info to octet string
 *
 *     INPUT            : au1LocInfo   - Location info
 *                        u4length     - length of location info
 *
 *     OUTPUT           : pu1Octet     - Octet string
 *
 *     RETURNS          : None
 *
 *******************************************************************************/

VOID
LldpCliLocToStr (UINT1 *au1LocInfo, UINT1 *pu1Octet, UINT4 u4length)
{

    UINT1               u1Byte;

    if (!(au1LocInfo) || !(pu1Octet))
        return;

    for (u1Byte = 0; u1Byte < u4length; u1Byte++)
    {
        pu1Octet +=
            SPRINTF ((CHR1 *) pu1Octet, "%02x:", *(au1LocInfo + u1Byte));
    }
    SPRINTF ((CHR1 *) (pu1Octet - 1), "   ");

}

/*******************************************************************************
 *
 *     FUNCTION NAME    : LldpGetCivicLocPrompt
 *
 *     DESCRIPTION      : This function returns the Civic Location prompt to be displayed.
 *
 *     INPUT            : pi1ModeName - Mode to be configured.
 *
 *     OUTPUT           : pi1DispStr  - Prompt to be displayed.
 *
 *     RETURNS          : OSIX_TRUE or OSIX_FALSE
 *
 *******************************************************************************/

INT1
LldpGetCivicLocPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (LLDP_CIVIC_MODE);
    if ((!pi1DispStr) || (!pi1ModeName))

    {
        return OSIX_FALSE;
    }
    if (STRNCMP (pi1ModeName, LLDP_CIVIC_MODE, u4Len) != 0)

    {
        return OSIX_FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    /*
     * No need to take lock here, since it is taken by
     * Cli in cli_process_lldp_cmd.
     */
    STRCPY (pi1DispStr, "(config-civic-loc)#");
    return OSIX_TRUE;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpMedCliSetModuleStatus
 *
 *     DESCRIPTION      : This function will enable/disable LLDP-MED functionality in
 *                        particular port.
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex    - Interface Index
 *                        i4Status   - Lldp Module status
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpMedCliSetModuleStatus (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Status)
{

    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsLldpMedAdminStatus (&u4ErrorCode, i4IfIndex, i4Status) ==
        SNMP_FAILURE)
    {
        if (u4ErrorCode == SNMP_ERR_WRONG_VALUE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%TX_RX is disabled,So Unable to set LLDP-MED Admin Status\r\n");
            return CLI_FAILURE;
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set LLDP-MED Admin Status\r\n");
            return CLI_FAILURE;
        }

    }

    if (nmhSetFsLldpMedAdminStatus (i4IfIndex, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetTagStatus
 *
 *     DESCRIPTION      : This function specifies whether LLDP system will
 *                        send either tagged LLD-PDU or Untagged LLDPDU
 *                        through the Edge Virtual Bridge (EVB) - 
 *                        Uplink Access Ports (UAP).
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4SetTagStatus - Tagging Status Enable/Disable
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
LldpCliSetTagStatus (tCliHandle CliHandle, INT4 i4TagStatus)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsLldpTagStatus (&u4ErrorCode, i4TagStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsLldpTagStatus (i4TagStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetManagementIpAddress
 *
 *     DESCRIPTION      : This function sets the  management IPv4 address
 *                        to be carried in the Management Address TLV in
 *                        LLD-PDU.
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4MgmtAddrSubType - Management Address Sub Type
                          pu1ManAddr - Management IP Address
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT4
LldpCliSetManagementIpAddress (tCliHandle CliHandle, INT4 i4MgmtAddrSubType,
                               UINT1 *pu1ManAddr)
{
    tSNMP_OCTET_STRING_TYPE MgmtAddr;
    UINT1               au1MgmtAddr[IPVX_IPV6_ADDR_LEN];
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    MEMSET (au1MgmtAddr, 0, sizeof (au1MgmtAddr));
    MEMSET (&(MgmtAddr), 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (i4MgmtAddrSubType == IPVX_ADDR_FMLY_IPV4)
    {
        MgmtAddr.pu1_OctetList = au1MgmtAddr;
        MEMCPY (MgmtAddr.pu1_OctetList, pu1ManAddr, IPVX_IPV4_ADDR_LEN);
        MgmtAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    }
    if (nmhTestv2FsLldpConfiguredMgmtIpv4Address (&u4ErrorCode, &(MgmtAddr))
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsLldpConfiguredMgmtIpv4Address (&(MgmtAddr)) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpCliSetManagementIpv6Address
 *
 *     DESCRIPTION      : This function sets the  management IPv6 address
 *                        to be carried in the Management Address TLV in
 *                        LLD-PDU.
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4MgmtAddrSubType - Management Address Sub Type
                          pu1ManAddr - Management IP Address
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT4
LldpCliSetManagementIpv6Address (tCliHandle CliHandle, INT4 i4MgmtAddrSubType,
                                 tIp6Addr Ip6Addr)
{
    tSNMP_OCTET_STRING_TYPE MgmtAddr;
    UINT1               au1MgmtAddr[IPVX_IPV6_ADDR_LEN];
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4MgmtAddrSubType);

    MEMSET (au1MgmtAddr, 0, sizeof (au1MgmtAddr));

    MgmtAddr.pu1_OctetList = au1MgmtAddr;
    MEMCPY (MgmtAddr.pu1_OctetList, &Ip6Addr, IPVX_IPV6_ADDR_LEN);
    MgmtAddr.i4_Length = IPVX_IPV6_ADDR_LEN;

    if (nmhTestv2FsLldpConfiguredMgmtIpv6Address (&u4ErrorCode, &(MgmtAddr))
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsLldpConfiguredMgmtIpv6Address (&(MgmtAddr)) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

#endif /* _LLDCLI_C */

/*----------------------------------------------------------------------------*/
/*                            End of lldcli.c                                 */
/*----------------------------------------------------------------------------*/
