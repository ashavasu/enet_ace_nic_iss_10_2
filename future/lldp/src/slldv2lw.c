/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: slldv2lw.c,v 1.23 2017/10/11 13:42:03 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
#include "lldinc.h"
#include "lldcli.h"

UINT1               gau1NullManAddrPorts[LLDP_MAN_ADDR_TX_EN_MAX_BYTES];
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2MessageTxInterval
 Input       :  The Indices

                The Object 
                retValLldpV2MessageTxInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2MessageTxInterval (UINT4 *pu4RetValLldpV2MessageTxInterval)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        *pu4RetValLldpV2MessageTxInterval = 0;
        return SNMP_SUCCESS;
    }
    LldpUtlGetMessageTxInterval ((INT4 *) pu4RetValLldpV2MessageTxInterval);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2MessageTxHoldMultiplier
 Input       :  The Indices

                The Object 
                retValLldpV2MessageTxHoldMultiplier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2MessageTxHoldMultiplier (UINT4
                                     *pu4RetValLldpV2MessageTxHoldMultiplier)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        *pu4RetValLldpV2MessageTxHoldMultiplier = 0;
        return SNMP_SUCCESS;
    }
    LldpUtlGetMessageTxHoldMultiplier ((INT4 *)
                                       pu4RetValLldpV2MessageTxHoldMultiplier);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2ReinitDelay
 Input       :  The Indices

                The Object 
                retValLldpV2ReinitDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2ReinitDelay (UINT4 *pu4RetValLldpV2ReinitDelay)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        *pu4RetValLldpV2ReinitDelay = 0;
        return SNMP_SUCCESS;
    }
    LldpUtlGetReinitDelay ((INT4 *) pu4RetValLldpV2ReinitDelay);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2NotificationInterval
 Input       :  The Indices

                The Object 
                retValLldpV2NotificationInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2NotificationInterval (UINT4 *pu4RetValLldpV2NotificationInterval)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        *pu4RetValLldpV2NotificationInterval = 0;
        return SNMP_SUCCESS;
    }
    LldpUtlGetNotificationInterval ((INT4 *)
                                    pu4RetValLldpV2NotificationInterval);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2TxCreditMax
 Input       :  The Indices

                The Object 
                retValLldpV2TxCreditMax
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2TxCreditMax (UINT4 *pu4RetValLldpV2TxCreditMax)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        /* LLDP system is shutdown, so return default value */
        *pu4RetValLldpV2TxCreditMax = (INT4) LLDP_DEFAULT_CREDIT_MAX;
        return SNMP_SUCCESS;
    }

    *pu4RetValLldpV2TxCreditMax =
        (INT4) gLldpGlobalInfo.SysConfigInfo.u4TxCreditMax;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2MessageFastTx
 Input       :  The Indices

                The Object 
                retValLldpV2MessageFastTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2MessageFastTx (UINT4 *pu4RetValLldpV2MessageFastTx)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        /* LLDP system is shutdown, so return default value */
        *pu4RetValLldpV2MessageFastTx = (INT4) LLDP_DEFAULT_FAST_TX;
        return SNMP_SUCCESS;
    }

    *pu4RetValLldpV2MessageFastTx =
        (INT4) gLldpGlobalInfo.SysConfigInfo.u4MessageFastTx;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2TxFastInit
 Input       :  The Indices

                The Object 
                retValLldpV2TxFastInit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2TxFastInit (UINT4 *pu4RetValLldpV2TxFastInit)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        /* LLDP system is shutdown, so return default value */
        *pu4RetValLldpV2TxFastInit = (INT4) LLDP_DEFAULT_TX_FAST_INIT;
        return SNMP_SUCCESS;
    }

    *pu4RetValLldpV2TxFastInit =
        (INT4) gLldpGlobalInfo.SysConfigInfo.u4TxFastInit;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpV2MessageTxInterval
 Input       :  The Indices

                The Object 
                setValLldpV2MessageTxInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpV2MessageTxInterval (UINT4 u4SetValLldpV2MessageTxInterval)
{
    LldpUtlSetMessageTxInterval ((INT4) u4SetValLldpV2MessageTxInterval);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetLldpV2MessageTxHoldMultiplier
 Input       :  The Indices

                The Object 
                setValLldpV2MessageTxHoldMultiplier
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpV2MessageTxHoldMultiplier (UINT4
                                     u4SetValLldpV2MessageTxHoldMultiplier)
{
    LldpUtlSetMessageTxHoldMultiplier ((INT4)
                                       u4SetValLldpV2MessageTxHoldMultiplier);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetLldpV2ReinitDelay
 Input       :  The Indices

                The Object 
                setValLldpV2ReinitDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpV2ReinitDelay (UINT4 u4SetValLldpV2ReinitDelay)
{
    LldpUtlSetReinitDelay ((INT4) u4SetValLldpV2ReinitDelay);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetLldpV2NotificationInterval
 Input       :  The Indices

                The Object 
                setValLldpV2NotificationInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpV2NotificationInterval (UINT4 u4SetValLldpV2NotificationInterval)
{
    LldpUtlSetNotificationInterval ((INT4) u4SetValLldpV2NotificationInterval);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetLldpV2TxCreditMax
 Input       :  The Indices

                The Object 
                setValLldpV2TxCreditMax
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpV2TxCreditMax (UINT4 u4SetValLldpV2TxCreditMax)
{
    if (gLldpGlobalInfo.SysConfigInfo.u4TxCreditMax !=
        u4SetValLldpV2TxCreditMax)
    {
        /* Update the Tx Credit Max Value */
        gLldpGlobalInfo.SysConfigInfo.u4TxCreditMax = u4SetValLldpV2TxCreditMax;
        LldpTxUtlHandleLocSysInfoChg ();
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetLldpV2MessageFastTx
 Input       :  The Indices

                The Object 
                setValLldpV2MessageFastTx
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpV2MessageFastTx (UINT4 u4SetValLldpV2MessageFastTx)
{
    if (gLldpGlobalInfo.SysConfigInfo.u4MessageFastTx !=
        u4SetValLldpV2MessageFastTx)
    {
        /* Update the Tx Credit Max Value */
        gLldpGlobalInfo.SysConfigInfo.u4MessageFastTx =
            u4SetValLldpV2MessageFastTx;
        LldpTxUtlHandleLocSysInfoChg ();
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetLldpV2TxFastInit
 Input       :  The Indices

                The Object 
                setValLldpV2TxFastInit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpV2TxFastInit (UINT4 u4SetValLldpV2TxFastInit)
{
    if (gLldpGlobalInfo.SysConfigInfo.u4TxFastInit != u4SetValLldpV2TxFastInit)
    {
        /* Update the Tx Credit Max Value */
        gLldpGlobalInfo.SysConfigInfo.u4TxFastInit = u4SetValLldpV2TxFastInit;
        LldpTxUtlHandleLocSysInfoChg ();
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpV2MessageTxInterval
 Input       :  The Indices

                The Object 
                testValLldpV2MessageTxInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpV2MessageTxInterval (UINT4 *pu4ErrorCode,
                                  UINT4 u4TestValLldpV2MessageTxInterval)
{
    if (LldpUtlTestMessageTxInterval
        (pu4ErrorCode, (INT4) u4TestValLldpV2MessageTxInterval) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlTestMessageTxInterval failed !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2LldpV2MessageTxHoldMultiplier
 Input       :  The Indices

                The Object 
                testValLldpV2MessageTxHoldMultiplier
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpV2MessageTxHoldMultiplier (UINT4 *pu4ErrorCode,
                                        UINT4
                                        u4TestValLldpV2MessageTxHoldMultiplier)
{
    if (LldpUtlTestMessageTxHoldMultiplier
        (pu4ErrorCode,
         (INT4) u4TestValLldpV2MessageTxHoldMultiplier) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlTestMessageTxHoldMultiplier failed !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2LldpV2ReinitDelay
 Input       :  The Indices

                The Object 
                testValLldpV2ReinitDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpV2ReinitDelay (UINT4 *pu4ErrorCode,
                            UINT4 u4TestValLldpV2ReinitDelay)
{
    if (LldpUtlTestReinitDelay (pu4ErrorCode, (INT4) u4TestValLldpV2ReinitDelay)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlTestReinitDelay failed !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2LldpV2NotificationInterval
 Input       :  The Indices

                The Object 
                testValLldpV2NotificationInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpV2NotificationInterval (UINT4 *pu4ErrorCode,
                                     UINT4 u4TestValLldpV2NotificationInterval)
{
    if (LldpUtlTestNotificationInterval
        (pu4ErrorCode,
         (INT4) u4TestValLldpV2NotificationInterval) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlTestNotificationInterval failed !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2LldpV2TxCreditMax
 Input       :  The Indices

                The Object 
                testValLldpV2TxCreditMax
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpV2TxCreditMax (UINT4 *pu4ErrorCode,
                            UINT4 u4TestValLldpV2TxCreditMax)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValLldpV2TxCreditMax < LLDP_MIN_CREDIT_MAX) ||
        (u4TestValLldpV2TxCreditMax > LLDP_MAX_CREDIT_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: u4TestValLldpV2TxCreditMax"
                  "is not within the allowed range \r\n");
        return SNMP_FAILURE;
    }
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: u4TestValLldpV2TxCreditMax"
                  "TxCreditMax can be configured only in version v2 \r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2LldpV2MessageFastTx
 Input       :  The Indices

                The Object 
                testValLldpV2MessageFastTx
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpV2MessageFastTx (UINT4 *pu4ErrorCode,
                              UINT4 u4TestValLldpV2MessageFastTx)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValLldpV2MessageFastTx < LLDP_MIN_MESSAGE_FAST_TX) ||
        (u4TestValLldpV2MessageFastTx > LLDP_MAX_MESSAGE_FAST_TX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: u4TestValLldpV2MessageFastTx"
                  "is not within the allowed range \r\n");
        return SNMP_FAILURE;
    }
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: nmhTestv2LldpV2MessageFastTx"
                  "MessageFastTx can be configured only in version v2 \r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2LldpV2TxFastInit
 Input       :  The Indices

                The Object 
                testValLldpV2TxFastInit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpV2TxFastInit (UINT4 *pu4ErrorCode, UINT4 u4TestValLldpV2TxFastInit)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValLldpV2TxFastInit < LLDP_MIN_TX_FAST_INIT) ||
        (u4TestValLldpV2TxFastInit > LLDP_MAX_TX_FAST_INIT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: u4TestValLldpV2TxFastInit"
                  "is not within the allowed range \r\n");
        return SNMP_FAILURE;
    }
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: nmhTestv2LldpV2TxFastInit"
                  "TxFastInit can be configured only in version v2 \r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpV2MessageTxInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpV2MessageTxInterval (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2LldpV2MessageTxHoldMultiplier
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpV2MessageTxHoldMultiplier (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2LldpV2ReinitDelay
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpV2ReinitDelay (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2LldpV2NotificationInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpV2NotificationInterval (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2LldpV2TxCreditMax
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpV2TxCreditMax (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2LldpV2MessageFastTx
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpV2MessageFastTx (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2LldpV2TxFastInit
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpV2TxFastInit (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2PortConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2PortConfigTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2PortConfigTable (INT4 i4LldpV2PortConfigIfIndex,
                                               UINT4
                                               u4LldpV2PortConfigDestAddressIndex)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: u4LldpV2PortConfigDestAddressIndex is Wrong\r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlValidateIndexInstanceLldpPortConfigTable
        (i4LldpV2PortConfigIfIndex,
         pTempDestTbl->Lldpv2DestMacAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpPortConfigTable failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2PortConfigTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2PortConfigTable (INT4 *pi4LldpV2PortConfigIfIndex,
                                       UINT4
                                       *pu4LldpV2PortConfigDestAddressIndex)
{
    tLldpv2AgentToLocPort *pLldpv2AgentToLocPort = NULL;
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }

    pLldpv2AgentToLocPort = (tLldpv2AgentToLocPort *)
        RBTreeGetFirst (gLldpGlobalInfo.Lldpv2AgentMapTblRBTree);
    if (pLldpv2AgentToLocPort == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: Table is Empty\r\n");
        return SNMP_FAILURE;
    }
    *pi4LldpV2PortConfigIfIndex = pLldpv2AgentToLocPort->i4IfIndex;
    *pu4LldpV2PortConfigDestAddressIndex =
        pLldpv2AgentToLocPort->u4DstMacAddrTblIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2PortConfigTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                nextLldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
                nextLldpV2PortConfigDestAddressIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2PortConfigTable (INT4 i4LldpV2PortConfigIfIndex,
                                      INT4 *pi4NextLldpV2PortConfigIfIndex,
                                      UINT4 u4LldpV2PortConfigDestAddressIndex,
                                      UINT4
                                      *pu4NextLldpV2PortConfigDestAddressIndex)
{
    tLldpv2AgentToLocPort Lldpv2AgentToLocPort;
    tLldpv2AgentToLocPort *pLldpv2AgentToLocPort = NULL;
    tLldpLocPortTable   LocPortTable;
    tLldpLocPortTable  *pLocPortTable = NULL;

    MEMSET (&Lldpv2AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
    MEMSET (&LocPortTable, 0, sizeof (tLldpLocPortTable));
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
    {
        LocPortTable.i4IfIndex = i4LldpV2PortConfigIfIndex;
        pLocPortTable = (tLldpLocPortTable *)
            RBTreeGetNext (gLldpGlobalInfo.LldpPortInfoRBTree, &LocPortTable,
                           LldpPortInfoUtlRBCmpInfo);
        if (pLocPortTable == NULL)
        {
            return SNMP_FAILURE;
        }
        *pi4NextLldpV2PortConfigIfIndex = pLocPortTable->i4IfIndex;
        *pu4NextLldpV2PortConfigDestAddressIndex = DEFAULT_DEST_MAC_ADDR_INDEX;
    }
    else
    {

        Lldpv2AgentToLocPort.i4IfIndex = i4LldpV2PortConfigIfIndex;
        Lldpv2AgentToLocPort.u4DstMacAddrTblIndex =
            u4LldpV2PortConfigDestAddressIndex;
        pLldpv2AgentToLocPort =
            (tLldpv2AgentToLocPort *) RBTreeGetNext (gLldpGlobalInfo.
                                                     Lldpv2AgentMapTblRBTree,
                                                     &Lldpv2AgentToLocPort,
                                                     LldpAgentToLocPortIndexUtlRBCmpInfo);

        if (pLldpv2AgentToLocPort == NULL)
        {
            LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: Invalid Index\r\n");
            return SNMP_FAILURE;
        }
        *pi4NextLldpV2PortConfigIfIndex = pLldpv2AgentToLocPort->i4IfIndex;
        *pu4NextLldpV2PortConfigDestAddressIndex =
            pLldpv2AgentToLocPort->u4DstMacAddrTblIndex;

    }
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2PortConfigAdminStatus
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                retValLldpV2PortConfigAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2PortConfigAdminStatus (INT4 i4LldpV2PortConfigIfIndex,
                                   UINT4 u4LldpV2PortConfigDestAddressIndex,
                                   INT4 *pi4RetValLldpV2PortConfigAdminStatus)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlGetLldpPortConfigAdminStatus
        (i4LldpV2PortConfigIfIndex, pTempDestTbl->Lldpv2DestMacAddress,
         pi4RetValLldpV2PortConfigAdminStatus) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpPortConfigAdminStatus  Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetLldpV2PortConfigNotificationEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                retValLldpV2PortConfigNotificationEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2PortConfigNotificationEnable (INT4 i4LldpV2PortConfigIfIndex,
                                          UINT4
                                          u4LldpV2PortConfigDestAddressIndex,
                                          INT4
                                          *pi4RetValLldpV2PortConfigNotificationEnable)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlGetLldpPortConfigNotificationEnable (i4LldpV2PortConfigIfIndex,
                                                    pTempDestTbl->
                                                    Lldpv2DestMacAddress,
                                                    pi4RetValLldpV2PortConfigNotificationEnable)
        == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpPortConfigNotificationEnable Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2PortConfigTLVsTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                retValLldpV2PortConfigTLVsTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2PortConfigTLVsTxEnable (INT4 i4LldpV2PortConfigIfIndex,
                                    UINT4 u4LldpV2PortConfigDestAddressIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValLldpV2PortConfigTLVsTxEnable)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlGetLldpPortConfigTLVsTxEnable
        (i4LldpV2PortConfigIfIndex, pTempDestTbl->Lldpv2DestMacAddress,
         pRetValLldpV2PortConfigTLVsTxEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpPortConfigTLVsTxEnable Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpV2PortConfigAdminStatus
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                setValLldpV2PortConfigAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpV2PortConfigAdminStatus (INT4 i4LldpV2PortConfigIfIndex,
                                   UINT4 u4LldpV2PortConfigDestAddressIndex,
                                   INT4 i4SetValLldpV2PortConfigAdminStatus)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlSetLldpPortConfigAdminStatus
        (i4LldpV2PortConfigIfIndex, pTempDestTbl->Lldpv2DestMacAddress,
         i4SetValLldpV2PortConfigAdminStatus) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlSetLldpPortConfigAdminStatus Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetLldpV2PortConfigNotificationEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                setValLldpV2PortConfigNotificationEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpV2PortConfigNotificationEnable (INT4 i4LldpV2PortConfigIfIndex,
                                          UINT4
                                          u4LldpV2PortConfigDestAddressIndex,
                                          INT4
                                          i4SetValLldpV2PortConfigNotificationEnable)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlSetLldpPortConfigNotificationEnable (i4LldpV2PortConfigIfIndex,
                                                    pTempDestTbl->
                                                    Lldpv2DestMacAddress,
                                                    i4SetValLldpV2PortConfigNotificationEnable)
        == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlSetLldpPortConfigNotificationEnable Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetLldpV2PortConfigTLVsTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                setValLldpV2PortConfigTLVsTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpV2PortConfigTLVsTxEnable (INT4 i4LldpV2PortConfigIfIndex,
                                    UINT4 u4LldpV2PortConfigDestAddressIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pSetValLldpV2PortConfigTLVsTxEnable)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlSetLldpPortConfigTLVsTxEnable
        (i4LldpV2PortConfigIfIndex, pTempDestTbl->Lldpv2DestMacAddress,
         pSetValLldpV2PortConfigTLVsTxEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlSetLldpPortConfigTLVsTxEnable Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpV2PortConfigAdminStatus
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                testValLldpV2PortConfigAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpV2PortConfigAdminStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4LldpV2PortConfigIfIndex,
                                      UINT4 u4LldpV2PortConfigDestAddressIndex,
                                      INT4 i4TestValLldpV2PortConfigAdminStatus)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlTestLldpPortConfigAdminStatus
        (pu4ErrorCode, i4LldpV2PortConfigIfIndex,
         pTempDestTbl->Lldpv2DestMacAddress,
         i4TestValLldpV2PortConfigAdminStatus) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlTestLldpPortConfigAdminStatus Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2LldpV2PortConfigNotificationEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                testValLldpV2PortConfigNotificationEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpV2PortConfigNotificationEnable (UINT4 *pu4ErrorCode,
                                             INT4 i4LldpV2PortConfigIfIndex,
                                             UINT4
                                             u4LldpV2PortConfigDestAddressIndex,
                                             INT4
                                             i4TestValLldpV2PortConfigNotificationEnable)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlTestLldpPortConfigNotificationEnable
        (pu4ErrorCode, i4LldpV2PortConfigIfIndex,
         pTempDestTbl->Lldpv2DestMacAddress,
         i4TestValLldpV2PortConfigNotificationEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlTestLldpPortConfigNotificationEnable Failed  \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2LldpV2PortConfigTLVsTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                testValLldpV2PortConfigTLVsTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpV2PortConfigTLVsTxEnable (UINT4 *pu4ErrorCode,
                                       INT4 i4LldpV2PortConfigIfIndex,
                                       UINT4 u4LldpV2PortConfigDestAddressIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pTestValLldpV2PortConfigTLVsTxEnable)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlTestLldpPortConfigTLVsTxEnable
        (pu4ErrorCode, i4LldpV2PortConfigIfIndex,
         pTempDestTbl->Lldpv2DestMacAddress,
         pTestValLldpV2PortConfigTLVsTxEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlTestLldpPortConfigTLVsTxEnable Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpV2PortConfigTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpV2PortConfigTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2DestAddressTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2DestAddressTable
 Input       :  The Indices
                LldpV2AddressTableIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2DestAddressTable (UINT4 u4LldpV2AddressTableIndex)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2AddressTableIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2DestAddressTable
 Input       :  The Indices
                LldpV2AddressTableIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2DestAddressTable (UINT4 *pu4LldpV2AddressTableIndex)
{
    return (nmhGetNextIndexLldpV2DestAddressTable (LLDP_ZERO,
                                                   pu4LldpV2AddressTableIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2DestAddressTable
 Input       :  The Indices
                LldpV2AddressTableIndex
                nextLldpV2AddressTableIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2DestAddressTable (UINT4 u4LldpV2AddressTableIndex,
                                       UINT4 *pu4NextLldpV2AddressTableIndex)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2AddressTableIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGetNext (gLldpGlobalInfo.
                                              Lldpv2DestMacAddrTblRBTree,
                                              (tRBElem *) & DestTbl,
                                              LldpDstMacAddrUtlRBCmpInfo);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }
    *pu4NextLldpV2AddressTableIndex = pTempDestTbl->u4LlldpV2DestAddrTblIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2DestMacAddress
 Input       :  The Indices
                LldpV2AddressTableIndex

                The Object 
                retValLldpV2DestMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2DestMacAddress (UINT4 u4LldpV2AddressTableIndex,
                            tMacAddr * pRetValLldpV2DestMacAddress)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2AddressTableIndex;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }
    MEMCPY (pRetValLldpV2DestMacAddress, pTempDestTbl->Lldpv2DestMacAddress,
            MAC_ADDR_LEN);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2ManAddrConfigTxPortsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2ManAddrConfigTxPortsTable
 Input       :  The Indices
                LldpV2ManAddrConfigIfIndex
                LldpV2ManAddrConfigDestAddressIndex
                LldpV2ManAddrConfigLocManAddrSubtype
                LldpV2ManAddrConfigLocManAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2ManAddrConfigTxPortsTable (INT4
                                                         i4LldpV2ManAddrConfigIfIndex,
                                                         UINT4
                                                         u4LldpV2ManAddrConfigDestAddressIndex,
                                                         INT4
                                                         i4LldpV2ManAddrConfigLocManAddrSubtype,
                                                         tSNMP_OCTET_STRING_TYPE
                                                         *
                                                         pLldpV2ManAddrConfigLocManAddr)
{
    tLldpLocPortTable  *pLldpLocPortTable = NULL;
    tLldpLocPortTable   LldpLocPortTable;
    tLldpv2DestAddrTbl  DestAddrTbl;
    tLldpv2DestAddrTbl *pDestAddrTbl = NULL;
    UINT1               au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR];
    tLldpLocManAddrTable *pLocManAddrNode = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return SNMP_FAILURE;
    }

    if ((i4LldpV2ManAddrConfigIfIndex < LLDP_MIN_PORTS) ||
        ((UINT4) i4LldpV2ManAddrConfigIfIndex > LLDP_MAX_PORTS))
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: Interface index is not in valid range\r\n");
        return SNMP_FAILURE;
    }

    MEMSET (&LldpLocPortTable, 0, sizeof (tLldpLocPortTable));

    LldpLocPortTable.i4IfIndex = i4LldpV2ManAddrConfigIfIndex;
    pLldpLocPortTable = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpLocPortTable);

    if (pLldpLocPortTable == NULL)
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC, " SNMPSTD: Interface entry does not"
                       " exist for port %d\r\n", i4LldpV2ManAddrConfigIfIndex);
        return SNMP_FAILURE;
    }

    DestAddrTbl.u4LlldpV2DestAddrTblIndex =
        u4LldpV2ManAddrConfigDestAddressIndex;
    pDestAddrTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          &DestAddrTbl);

    if (pDestAddrTbl == NULL)
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                       " SNMPSTD: DestinationAddressTable entry does not"
                       " exist for Index %d\r\n",
                       u4LldpV2ManAddrConfigDestAddressIndex);
        return SNMP_FAILURE;
    }

    MEMCPY (&au1LocManAddr[0], pLldpV2ManAddrConfigLocManAddr->pu1_OctetList,
            pLldpV2ManAddrConfigLocManAddr->i4_Length);

    /* Check whether the Entry is exist or not, indices are validaed inside
     *      * the Get function */
    pLocManAddrNode = (tLldpLocManAddrTable *)
        LldpTxUtlGetLocManAddrNode (i4LldpV2ManAddrConfigLocManAddrSubtype,
                                    &au1LocManAddr[0]);
    if (pLocManAddrNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpTxUtlGetLocManAddrNode returns NULL \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2ManAddrConfigTxPortsTable
 Input       :  The Indices
                LldpV2ManAddrConfigIfIndex
                LldpV2ManAddrConfigDestAddressIndex
                LldpV2ManAddrConfigLocManAddrSubtype
                LldpV2ManAddrConfigLocManAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexLldpV2ManAddrConfigTxPortsTable (INT4
                                                 *pi4LldpV2ManAddrConfigIfIndex,
                                                 UINT4
                                                 *pu4LldpV2ManAddrConfigDestAddressIndex,
                                                 INT4
                                                 *pi4LldpV2ManAddrConfigLocManAddrSubtype,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pLldpV2ManAddrConfigLocManAddr)
{
    INT4                i4ManAddrLen = 0;
    tLldpLocManAddrTable *pFirstLocalManAddrNode = NULL;
    tLldpLocManAddrTable *pNextNode = NULL;
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    tLldpLocPortInfo    LldpLocPortInfo;
    UINT4               u4BitPos = 0;
    UINT1
                      au1LocManAddrPortsTxEnable[LLDP_MAN_ADDR_TX_EN_MAX_BYTES];
    UINT1               u1Result = OSIX_FALSE;

    MEMSET (gau1NullManAddrPorts, 0, LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return SNMP_FAILURE;
    }

    pFirstLocalManAddrNode = (tLldpLocManAddrTable *)
        RBTreeGetFirst (gLldpGlobalInfo.LocManAddrRBTree);

    if (pFirstLocalManAddrNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: No Local management address "
                  "config Entry exists (RBTreeGetFirst returns NULL)\r\n");
        return SNMP_FAILURE;
    }

    if (MEMCMP
        (pFirstLocalManAddrNode->au1LocManAddrPortsTxEnable,
         gau1NullManAddrPorts, LLDP_MAN_ADDR_TX_EN_MAX_BYTES) == 0)
    {
        while ((pNextNode = (tLldpLocManAddrTable *)
                RBTreeGetNext (gLldpGlobalInfo.LocManAddrRBTree,
                               pFirstLocalManAddrNode, NULL)) != NULL)
        {
            if (MEMCMP
                (pNextNode->au1LocManAddrPortsTxEnable, gau1NullManAddrPorts,
                 LLDP_MAN_ADDR_TX_EN_MAX_BYTES) != 0)
            {
                break;
            }
            pFirstLocalManAddrNode = pNextNode;
        }
        if (pNextNode == NULL)
        {
            LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: No Local management address "
                      "config Entry exists (RBTreeGetFirst returns NULL)\r\n");
            return SNMP_FAILURE;
        }
        pFirstLocalManAddrNode = pNextNode;
    }

    MEMCPY (au1LocManAddrPortsTxEnable,
            pFirstLocalManAddrNode->au1LocManAddrPortsTxEnable,
            LLDP_MAN_ADDR_TX_EN_MAX_BYTES);

    for (u4BitPos = 0; u4BitPos < LLDP_MAN_ADDR_TX_EN_MAX_BYTES; u4BitPos++)
    {
        OSIX_BITLIST_IS_BIT_SET (au1LocManAddrPortsTxEnable, u4BitPos,
                                 LLDP_MAN_ADDR_TX_EN_MAX_BYTES, u1Result);
        if (u1Result == OSIX_TRUE)
        {
            MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
            LldpLocPortInfo.u4LocPortNum = u4BitPos;
            pLldpLocPortInfo = (tLldpLocPortInfo *)
                RBTreeGet (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
                           &LldpLocPortInfo);
            if (pLldpLocPortInfo == NULL)
            {
                LLDP_TRC (ALL_FAILURE_TRC,
                          " SNMPSTD: GetNext for Man Addr Table not found\r\n");
                return SNMP_FAILURE;
            }
            *pi4LldpV2ManAddrConfigIfIndex = pLldpLocPortInfo->i4IfIndex;
            *pu4LldpV2ManAddrConfigDestAddressIndex =
                pLldpLocPortInfo->u4DstMacAddrTblIndex;
            *pi4LldpV2ManAddrConfigLocManAddrSubtype =
                pFirstLocalManAddrNode->i4LocManAddrSubtype;
            i4ManAddrLen =
                (INT4) LldpUtilGetManAddrLen (pFirstLocalManAddrNode->
                                              i4LocManAddrSubtype);
            MEMCPY (pLldpV2ManAddrConfigLocManAddr->pu1_OctetList,
                    pFirstLocalManAddrNode->au1LocManAddr, i4ManAddrLen);
            pLldpV2ManAddrConfigLocManAddr->i4_Length = i4ManAddrLen;
            return SNMP_SUCCESS;
        }

    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2ManAddrConfigTxPortsTable
 Input       :  The Indices
                LldpV2ManAddrConfigIfIndex
                nextLldpV2ManAddrConfigIfIndex
                LldpV2ManAddrConfigDestAddressIndex
                nextLldpV2ManAddrConfigDestAddressIndex
                LldpV2ManAddrConfigLocManAddrSubtype
                nextLldpV2ManAddrConfigLocManAddrSubtype
                LldpV2ManAddrConfigLocManAddr
                nextLldpV2ManAddrConfigLocManAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2ManAddrConfigTxPortsTable (INT4
                                                i4LldpV2ManAddrConfigIfIndex,
                                                INT4
                                                *pi4NextLldpV2ManAddrConfigIfIndex,
                                                UINT4
                                                u4LldpV2ManAddrConfigDestAddressIndex,
                                                UINT4
                                                *pu4NextLldpV2ManAddrConfigDestAddressIndex,
                                                INT4
                                                i4LldpV2ManAddrConfigLocManAddrSubtype,
                                                INT4
                                                *pi4NextLldpV2ManAddrConfigLocManAddrSubtype,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pLldpV2ManAddrConfigLocManAddr,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pNextLldpV2ManAddrConfigLocManAddr)
{
    tLldpLocManAddrTable *pNextLocManNode = NULL;
    tLldpLocManAddrTable *pCurrLocManNode = NULL;
    tLldpLocManAddrTable *pNextNode = NULL;
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    tLldpLocPortInfo    LldpLocPortInfo;
    UINT1               au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR];
    UINT1               u1Result = 0;
    UINT4               u4LocPortNum = 0;
    UINT4               u4BitPos = 0;
    INT4                i4ManAddrLen = 0;

    MEMSET (gau1NullManAddrPorts, 0, LLDP_MAN_ADDR_TX_EN_MAX_BYTES);

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return SNMP_FAILURE;
    }

    if (pLldpV2ManAddrConfigLocManAddr->i4_Length > LLDP_MAX_LEN_MAN_ADDR)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: Length exceed the maximum value !!\r\n");
        return SNMP_FAILURE;
    }
    else if (pLldpV2ManAddrConfigLocManAddr->i4_Length <= 0)
    {
        pNextLocManNode = (tLldpLocManAddrTable *)
            RBTreeGetFirst (gLldpGlobalInfo.LocManAddrRBTree);
        UNUSED_PARAM (pNextLocManNode);
    }
    else
    {
        MEMSET (&au1LocManAddr[0], 0, LLDP_MAX_LEN_MAN_ADDR);
        MEMCPY (&au1LocManAddr[0],
                pLldpV2ManAddrConfigLocManAddr->pu1_OctetList,
                pLldpV2ManAddrConfigLocManAddr->i4_Length);
        /* Get the Current Node */
        pCurrLocManNode = (tLldpLocManAddrTable *)
            LldpTxUtlGetLocManAddrNode (i4LldpV2ManAddrConfigLocManAddrSubtype,
                                        &au1LocManAddr[0]);
        if (pCurrLocManNode == NULL)
        {
            LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                      "LldpTxUtlGetLocManAddrNode returns NULL \r\n");
            UNUSED_PARAM (pNextLocManNode);
            return SNMP_FAILURE;
        }
        else
        {
            u4LocPortNum =
                LldpGetLocalPortFromIfIndexDestMacIfIndex
                ((UINT4) i4LldpV2ManAddrConfigIfIndex,
                 u4LldpV2ManAddrConfigDestAddressIndex);

            for (u4BitPos = u4LocPortNum + 1;
                 u4BitPos < LLDP_MAN_ADDR_TX_EN_MAX_BYTES; u4BitPos++)
            {
                OSIX_BITLIST_IS_BIT_SET (pCurrLocManNode->
                                         au1LocManAddrPortsTxEnable, u4BitPos,
                                         LLDP_MAN_ADDR_TX_EN_MAX_BYTES,
                                         u1Result);
                if (u1Result == OSIX_TRUE)
                {
                    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
                    LldpLocPortInfo.u4LocPortNum = u4BitPos;
                    pLldpLocPortInfo = (tLldpLocPortInfo *)
                        RBTreeGet (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
                                   &LldpLocPortInfo);
                    if (pLldpLocPortInfo == NULL)
                    {
                        LLDP_TRC (ALL_FAILURE_TRC,
                                  " SNMPSTD: GetNext for Man Addr Table not found\r\n");
                        UNUSED_PARAM (pNextLocManNode);
                        return SNMP_FAILURE;
                    }
                    *pi4NextLldpV2ManAddrConfigIfIndex =
                        pLldpLocPortInfo->i4IfIndex;
                    *pu4NextLldpV2ManAddrConfigDestAddressIndex =
                        pLldpLocPortInfo->u4DstMacAddrTblIndex;
                    *pi4NextLldpV2ManAddrConfigLocManAddrSubtype =
                        pCurrLocManNode->i4LocManAddrSubtype;
                    i4ManAddrLen =
                        (INT4) LldpUtilGetManAddrLen (pCurrLocManNode->
                                                      i4LocManAddrSubtype);
                    MEMCPY (pNextLldpV2ManAddrConfigLocManAddr->pu1_OctetList,
                            pCurrLocManNode->au1LocManAddr, i4ManAddrLen);
                    pNextLldpV2ManAddrConfigLocManAddr->i4_Length =
                        i4ManAddrLen;
                    UNUSED_PARAM (pNextLocManNode);
                    return SNMP_SUCCESS;
                }
            }
            /* Get the Next Node */
            if (LldpTxUtlGetNextManAddr (pCurrLocManNode, &pNextLocManNode)
                != OSIX_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            if (MEMCMP
                (pNextLocManNode->au1LocManAddrPortsTxEnable,
                 gau1NullManAddrPorts, LLDP_MAN_ADDR_TX_EN_MAX_BYTES) == 0)
            {
                while ((pNextNode = (tLldpLocManAddrTable *)
                        RBTreeGetNext (gLldpGlobalInfo.LocManAddrRBTree,
                                       pNextLocManNode, NULL)) != NULL)
                {
                    if (MEMCMP
                        (pNextNode->au1LocManAddrPortsTxEnable,
                         gau1NullManAddrPorts,
                         LLDP_MAN_ADDR_TX_EN_MAX_BYTES) != 0)
                    {
                        break;
                    }
                    pNextLocManNode = pNextNode;
                }
                if (pNextNode == NULL)
                {
                    LLDP_TRC (ALL_FAILURE_TRC,
                              " SNMPSTD: No Local management address "
                              "config Entry exists (RBTreeGetFirst returns NULL)\r\n");
                    return SNMP_FAILURE;
                }
                pNextLocManNode = pNextNode;
            }

            LLDP_CHK_NULL_PTR_RET (pNextLocManNode, SNMP_FAILURE);
            for (u4BitPos = 0; u4BitPos < LLDP_MAN_ADDR_TX_EN_MAX_BYTES;
                 u4BitPos++)
            {
                OSIX_BITLIST_IS_BIT_SET (pNextLocManNode->
                                         au1LocManAddrPortsTxEnable, u4BitPos,
                                         LLDP_MAN_ADDR_TX_EN_MAX_BYTES,
                                         u1Result);
                if (u1Result == OSIX_TRUE)
                {
                    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
                    LldpLocPortInfo.u4LocPortNum = u4BitPos;
                    pLldpLocPortInfo = (tLldpLocPortInfo *)
                        RBTreeGet (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
                                   &LldpLocPortInfo);
                    if (pLldpLocPortInfo == NULL)
                    {
                        LLDP_TRC (ALL_FAILURE_TRC,
                                  " SNMPSTD: GetNext for Man Addr Table not found\r\n");
                        return SNMP_FAILURE;
                    }
                    *pi4NextLldpV2ManAddrConfigIfIndex =
                        pLldpLocPortInfo->i4IfIndex;
                    *pu4NextLldpV2ManAddrConfigDestAddressIndex =
                        pLldpLocPortInfo->u4DstMacAddrTblIndex;
                    *pi4NextLldpV2ManAddrConfigLocManAddrSubtype =
                        pNextLocManNode->i4LocManAddrSubtype;
                    i4ManAddrLen =
                        (INT4) LldpUtilGetManAddrLen (pNextLocManNode->
                                                      i4LocManAddrSubtype);
                    MEMCPY (pNextLldpV2ManAddrConfigLocManAddr->pu1_OctetList,
                            pNextLocManNode->au1LocManAddr, i4ManAddrLen);
                    pNextLldpV2ManAddrConfigLocManAddr->i4_Length =
                        i4ManAddrLen;
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2ManAddrConfigTxEnable
 Input       :  The Indices
                LldpV2ManAddrConfigIfIndex
                LldpV2ManAddrConfigDestAddressIndex
                LldpV2ManAddrConfigLocManAddrSubtype
                LldpV2ManAddrConfigLocManAddr

                The Object 
                retValLldpV2ManAddrConfigTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2ManAddrConfigTxEnable (INT4 i4LldpV2ManAddrConfigIfIndex,
                                   UINT4 u4LldpV2ManAddrConfigDestAddressIndex,
                                   INT4 i4LldpV2ManAddrConfigLocManAddrSubtype,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pLldpV2ManAddrConfigLocManAddr,
                                   INT4 *pi4RetValLldpV2ManAddrConfigTxEnable)
{
    tLldpLocManAddrTable *pLocManAddrNode = NULL;
    tLldpv2DestAddrTbl  DestAddrTbl;
    tLldpv2DestAddrTbl *pDestAddrTbl = NULL;
    tLldpv2AgentToLocPort AgentToLocPort;
    tLldpv2AgentToLocPort *pAgentToLocPort = NULL;
    UINT1               au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR];
    UINT1               u1Result = OSIX_FALSE;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return SNMP_FAILURE;
    }

    MEMSET (&au1LocManAddr[0], 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMCPY (&au1LocManAddr[0], pLldpV2ManAddrConfigLocManAddr->pu1_OctetList,
            pLldpV2ManAddrConfigLocManAddr->i4_Length);

    pLocManAddrNode = (tLldpLocManAddrTable *)
        LldpTxUtlGetLocManAddrNode (i4LldpV2ManAddrConfigLocManAddrSubtype,
                                    &au1LocManAddr[0]);

    if (pLocManAddrNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpTxUtlGetLocManAddrNode returns NULL \r\n");
        return SNMP_FAILURE;
    }

    MEMSET (&DestAddrTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestAddrTbl.u4LlldpV2DestAddrTblIndex =
        u4LldpV2ManAddrConfigDestAddressIndex;
    pDestAddrTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          &DestAddrTbl);
    if (pDestAddrTbl != NULL)
    {
        MEMSET (&AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
        AgentToLocPort.i4IfIndex = i4LldpV2ManAddrConfigIfIndex;
        MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                pDestAddrTbl->Lldpv2DestMacAddress, MAC_ADDR_LEN);
        pAgentToLocPort =
            (tLldpv2AgentToLocPort *) RBTreeGet (gLldpGlobalInfo.
                                                 Lldpv2AgentToLocPortMapTblRBTree,
                                                 &AgentToLocPort);
        if (pAgentToLocPort != NULL)
        {
            OSIX_BITLIST_IS_BIT_SET (pLocManAddrNode->
                                     au1LocManAddrPortsTxEnable,
                                     pAgentToLocPort->u4LldpLocPort,
                                     LLDP_MAN_ADDR_TX_EN_MAX_BYTES, u1Result);

            if (u1Result == OSIX_TRUE)
            {
                *pi4RetValLldpV2ManAddrConfigTxEnable = OSIX_TRUE;
            }
            else
            {
                *pi4RetValLldpV2ManAddrConfigTxEnable = OSIX_FALSE;
            }

            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetLldpV2ManAddrConfigRowStatus
 Input       :  The Indices
                LldpV2ManAddrConfigIfIndex
                LldpV2ManAddrConfigDestAddressIndex
                LldpV2ManAddrConfigLocManAddrSubtype
                LldpV2ManAddrConfigLocManAddr

                The Object 
                retValLldpV2ManAddrConfigRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2ManAddrConfigRowStatus (INT4 i4LldpV2ManAddrConfigIfIndex,
                                    UINT4 u4LldpV2ManAddrConfigDestAddressIndex,
                                    INT4 i4LldpV2ManAddrConfigLocManAddrSubtype,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pLldpV2ManAddrConfigLocManAddr,
                                    INT4 *pi4RetValLldpV2ManAddrConfigRowStatus)
{
    tLldpLocManAddrTable *pLocManAddrNode = NULL;
    tLldpv2DestAddrTbl  DestAddrTbl;
    tLldpv2DestAddrTbl *pDestAddrTbl = NULL;
    tLldpv2AgentToLocPort AgentToLocPort;
    tLldpv2AgentToLocPort *pAgentToLocPort = NULL;
    UINT1               au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR];
    UINT1               u1Result = OSIX_FALSE;

    MEMSET (&au1LocManAddr[0], 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMCPY (&au1LocManAddr[0], pLldpV2ManAddrConfigLocManAddr->pu1_OctetList,
            pLldpV2ManAddrConfigLocManAddr->i4_Length);

    MEMSET (&AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
    MEMSET (&DestAddrTbl, 0, sizeof (tLldpv2DestAddrTbl));
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }

    DestAddrTbl.u4LlldpV2DestAddrTblIndex =
        u4LldpV2ManAddrConfigDestAddressIndex;
    pDestAddrTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          &DestAddrTbl);
    if (pDestAddrTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "nmhSetLldpV2ManAddrConfigRowStatus: "
                  "Destination Address table entry not found\r\n");
        return SNMP_FAILURE;
    }
    AgentToLocPort.i4IfIndex = i4LldpV2ManAddrConfigIfIndex;
    MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
            pDestAddrTbl->Lldpv2DestMacAddress, MAC_ADDR_LEN);
    pAgentToLocPort =
        (tLldpv2AgentToLocPort *) RBTreeGet (gLldpGlobalInfo.
                                             Lldpv2AgentToLocPortMapTblRBTree,
                                             &AgentToLocPort);
    if (pAgentToLocPort == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "nmhSetLldpV2ManAddrConfigRowStatus: "
                  "AgentToLocPort mapping entry not found\r\n");
        return SNMP_FAILURE;
    }
    pLocManAddrNode = (tLldpLocManAddrTable *)
        LldpTxUtlGetLocManAddrNode (i4LldpV2ManAddrConfigLocManAddrSubtype,
                                    &au1LocManAddr[0]);

    if (pLocManAddrNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpLocManAddrTable entry not found\r\n");
        return SNMP_FAILURE;
    }
    OSIX_BITLIST_IS_BIT_SET (pLocManAddrNode->au1LocManAddrRowstats,
                             pAgentToLocPort->u4LldpLocPort,
                             LLDP_MAN_ADDR_TX_EN_MAX_BYTES, u1Result);
    if (u1Result == OSIX_TRUE)
    {
        *pi4RetValLldpV2ManAddrConfigRowStatus = ACTIVE;
        return SNMP_SUCCESS;
    }
    LLDP_TRC (ALL_FAILURE_TRC, "LldpLocManAddrTable entry not found\r\n");
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpV2ManAddrConfigTxEnable
 Input       :  The Indices
                LldpV2ManAddrConfigIfIndex
                LldpV2ManAddrConfigDestAddressIndex
                LldpV2ManAddrConfigLocManAddrSubtype
                LldpV2ManAddrConfigLocManAddr

                The Object 
                setValLldpV2ManAddrConfigTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpV2ManAddrConfigTxEnable (INT4 i4LldpV2ManAddrConfigIfIndex,
                                   UINT4 u4LldpV2ManAddrConfigDestAddressIndex,
                                   INT4 i4LldpV2ManAddrConfigLocManAddrSubtype,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pLldpV2ManAddrConfigLocManAddr,
                                   INT4 i4SetValLldpV2ManAddrConfigTxEnable)
{
    tLldpLocManAddrTable *pLocManAddrNode = NULL;
    tLldpv2DestAddrTbl  DestAddrTbl;
    tLldpv2DestAddrTbl *pDestAddrTbl = NULL;
    tLldpv2AgentToLocPort AgentToLocPort;
    tLldpv2AgentToLocPort *pAgentToLocPort = NULL;
    UINT1               au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR];

    MEMSET (&au1LocManAddr[0], 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMCPY (&au1LocManAddr[0], pLldpV2ManAddrConfigLocManAddr->pu1_OctetList,
            pLldpV2ManAddrConfigLocManAddr->i4_Length);

    pLocManAddrNode = (tLldpLocManAddrTable *)
        LldpTxUtlGetLocManAddrNode (i4LldpV2ManAddrConfigLocManAddrSubtype,
                                    &au1LocManAddr[0]);

    if (pLocManAddrNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpTxUtlGetLocManAddrNode returns NULL \r\n");
        return SNMP_FAILURE;
    }
    MEMSET (&DestAddrTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestAddrTbl.u4LlldpV2DestAddrTblIndex =
        u4LldpV2ManAddrConfigDestAddressIndex;
    pDestAddrTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          &DestAddrTbl);
    if (pDestAddrTbl != NULL)
    {
        MEMSET (&AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
        AgentToLocPort.i4IfIndex = i4LldpV2ManAddrConfigIfIndex;
        MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                pDestAddrTbl->Lldpv2DestMacAddress, MAC_ADDR_LEN);
        pAgentToLocPort =
            (tLldpv2AgentToLocPort *) RBTreeGet (gLldpGlobalInfo.
                                                 Lldpv2AgentToLocPortMapTblRBTree,
                                                 &AgentToLocPort);
        if (pAgentToLocPort != NULL)
        {
            if (i4SetValLldpV2ManAddrConfigTxEnable == OSIX_TRUE)
            {
                OSIX_BITLIST_SET_BIT (pLocManAddrNode->
                                      au1LocManAddrPortsTxEnable,
                                      pAgentToLocPort->u4LldpLocPort,
                                      LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
            }
            else
            {
                OSIX_BITLIST_RESET_BIT (pLocManAddrNode->
                                        au1LocManAddrPortsTxEnable,
                                        pAgentToLocPort->u4LldpLocPort,
                                        LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
            }
            LldpTxUtlHandleLocSysInfoChg ();
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetLldpV2ManAddrConfigRowStatus
 Input       :  The Indices
                LldpV2ManAddrConfigIfIndex
                LldpV2ManAddrConfigDestAddressIndex
                LldpV2ManAddrConfigLocManAddrSubtype
                LldpV2ManAddrConfigLocManAddr

                The Object 
                setValLldpV2ManAddrConfigRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpV2ManAddrConfigRowStatus (INT4 i4LldpV2ManAddrConfigIfIndex,
                                    UINT4 u4LldpV2ManAddrConfigDestAddressIndex,
                                    INT4 i4LldpV2ManAddrConfigLocManAddrSubtype,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pLldpV2ManAddrConfigLocManAddr,
                                    INT4 i4SetValLldpV2ManAddrConfigRowStatus)
{
    tLldpLocManAddrTable *pLocManAddrNode = NULL;
    tLldpv2DestAddrTbl  DestAddrTbl;
    tLldpv2DestAddrTbl *pDestAddrTbl = NULL;
    tLldpv2AgentToLocPort AgentToLocPort;
    tLldpv2AgentToLocPort *pAgentToLocPort = NULL;
    UINT1               au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR];
    UINT1               u1Result = OSIX_FALSE;

    MEMSET (&au1LocManAddr[0], 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMCPY (&au1LocManAddr[0], pLldpV2ManAddrConfigLocManAddr->pu1_OctetList,
            pLldpV2ManAddrConfigLocManAddr->i4_Length);

    MEMSET (&AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
    MEMSET (&DestAddrTbl, 0, sizeof (tLldpv2DestAddrTbl));

    DestAddrTbl.u4LlldpV2DestAddrTblIndex =
        u4LldpV2ManAddrConfigDestAddressIndex;
    pDestAddrTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          &DestAddrTbl);
    if (pDestAddrTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "nmhSetLldpV2ManAddrConfigRowStatus: "
                  "Destination Address table entry not found\r\n");
        return SNMP_FAILURE;
    }
    AgentToLocPort.i4IfIndex = i4LldpV2ManAddrConfigIfIndex;
    MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
            pDestAddrTbl->Lldpv2DestMacAddress, MAC_ADDR_LEN);
    pAgentToLocPort =
        (tLldpv2AgentToLocPort *) RBTreeGet (gLldpGlobalInfo.
                                             Lldpv2AgentToLocPortMapTblRBTree,
                                             &AgentToLocPort);
    if (pAgentToLocPort == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "nmhSetLldpV2ManAddrConfigRowStatus: "
                  "AgentToLocPort mapping entry not found\r\n");
        return SNMP_FAILURE;
    }
    switch (i4SetValLldpV2ManAddrConfigRowStatus)
    {
        case LLDP_CREATE_AND_GO:    /* intentional fall through */
        case LLDP_CREATE_AND_WAIT:
            pLocManAddrNode = (tLldpLocManAddrTable *)
                LldpTxUtlGetLocManAddrNode
                (i4LldpV2ManAddrConfigLocManAddrSubtype, &au1LocManAddr[0]);

            if (pLocManAddrNode == NULL)
            {
                if ((pLocManAddrNode = (tLldpLocManAddrTable *)
                     (MemAllocMemBlk (gLldpGlobalInfo.LocManAddrPoolId))) ==
                    NULL)
                {
                    LLDP_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC |
                              LLDP_CRITICAL_TRC,
                              "LldpTxUtlAddIpv4Addr : Failed to allocate memory for "
                              "Local ManAddr Node\r\n");
                    return SNMP_FAILURE;
                }
                MEMSET (pLocManAddrNode, 0, sizeof (tLldpLocManAddrTable));
                pLocManAddrNode->i4LocManAddrSubtype =
                    i4LldpV2ManAddrConfigLocManAddrSubtype;
                MEMCPY (pLocManAddrNode->au1LocManAddr, au1LocManAddr,
                        LLDP_MAX_LEN_MAN_ADDR);

                if (RBTreeAdd
                    (gLldpGlobalInfo.LocManAddrRBTree,
                     (tRBElem *) pLocManAddrNode) != RB_SUCCESS)
                {
                    LLDP_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC |
                              LLDP_CRITICAL_TRC,
                              "LldpTxUtlAddIpv4Addr : Failed to Add the Node to "
                              "LocManAddrRBTree\r\n");
                    MemReleaseMemBlock (gLldpGlobalInfo.LocManAddrPoolId,
                                        (UINT1 *) pLocManAddrNode);
                    return SNMP_FAILURE;
                }
            }
            OSIX_BITLIST_SET_BIT (pLocManAddrNode->au1LocManAddrRowstats,
                                  pAgentToLocPort->u4LldpLocPort,
                                  LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
            OSIX_BITLIST_SET_BIT (pLocManAddrNode->au1LocManAddrPortsTxEnable,
                                  pAgentToLocPort->u4LldpLocPort,
                                  LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
            return SNMP_SUCCESS;

        case LLDP_ACTIVE:
            pLocManAddrNode = (tLldpLocManAddrTable *)
                LldpTxUtlGetLocManAddrNode
                (i4LldpV2ManAddrConfigLocManAddrSubtype, &au1LocManAddr[0]);

            if (pLocManAddrNode == NULL)
            {
                LLDP_TRC (ALL_FAILURE_TRC,
                          "LldpLocManAddrTable entry not found\r\n");
                return SNMP_FAILURE;
            }
            OSIX_BITLIST_SET_BIT (pLocManAddrNode->au1LocManAddrRowstats,
                                  pAgentToLocPort->u4LldpLocPort,
                                  LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
            return SNMP_SUCCESS;

        case LLDP_DESTROY:
            pLocManAddrNode = (tLldpLocManAddrTable *)
                LldpTxUtlGetLocManAddrNode
                (i4LldpV2ManAddrConfigLocManAddrSubtype, &au1LocManAddr[0]);

            if (pLocManAddrNode == NULL)
            {
                LLDP_TRC (ALL_FAILURE_TRC,
                          "LldpLocManAddrTable entry not found\r\n");
                return SNMP_FAILURE;
            }
            OSIX_BITLIST_IS_BIT_SET (pLocManAddrNode->au1LocManAddrRowstats,
                                     pAgentToLocPort->u4LldpLocPort,
                                     LLDP_MAN_ADDR_TX_EN_MAX_BYTES, u1Result);
            if (u1Result == OSIX_TRUE)
            {
                OSIX_BITLIST_RESET_BIT (pLocManAddrNode->au1LocManAddrRowstats,
                                        pAgentToLocPort->u4LldpLocPort,
                                        LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
                OSIX_BITLIST_RESET_BIT (pLocManAddrNode->
                                        au1LocManAddrPortsTxEnable,
                                        pAgentToLocPort->u4LldpLocPort,
                                        LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
            }
            return SNMP_SUCCESS;

    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpV2ManAddrConfigTxEnable
 Input       :  The Indices
                LldpV2ManAddrConfigIfIndex
                LldpV2ManAddrConfigDestAddressIndex
                LldpV2ManAddrConfigLocManAddrSubtype
                LldpV2ManAddrConfigLocManAddr

                The Object 
                testValLldpV2ManAddrConfigTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpV2ManAddrConfigTxEnable (UINT4 *pu4ErrorCode,
                                      INT4 i4LldpV2ManAddrConfigIfIndex,
                                      UINT4
                                      u4LldpV2ManAddrConfigDestAddressIndex,
                                      INT4
                                      i4LldpV2ManAddrConfigLocManAddrSubtype,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pLldpV2ManAddrConfigLocManAddr,
                                      INT4 i4TestValLldpV2ManAddrConfigTxEnable)
{
    tLldpLocManAddrTable *pLocManAddrNode = NULL;
    UINT1               au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR];

    UNUSED_PARAM (i4LldpV2ManAddrConfigIfIndex);
    UNUSED_PARAM (u4LldpV2ManAddrConfigDestAddressIndex);

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    MEMSET (&au1LocManAddr[0], 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMCPY (&au1LocManAddr[0], pLldpV2ManAddrConfigLocManAddr->pu1_OctetList,
            pLldpV2ManAddrConfigLocManAddr->i4_Length);

    pLocManAddrNode = (tLldpLocManAddrTable *)
        LldpTxUtlGetLocManAddrNode (i4LldpV2ManAddrConfigLocManAddrSubtype,
                                    &au1LocManAddr[0]);

    if (pLocManAddrNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpTxUtlGetLocManAddrNode returns NULL \r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValLldpV2ManAddrConfigTxEnable != OSIX_TRUE) &&
        (i4TestValLldpV2ManAddrConfigTxEnable != OSIX_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "pTestValLldpConfigManAddrPortsTxEnable is not valid \r\n");
        CLI_SET_ERR (LLDP_CLI_ERR_INVALID_MAN_ADDR_LEN);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2LldpV2ManAddrConfigRowStatus
 Input       :  The Indices
                LldpV2ManAddrConfigIfIndex
                LldpV2ManAddrConfigDestAddressIndex
                LldpV2ManAddrConfigLocManAddrSubtype
                LldpV2ManAddrConfigLocManAddr

                The Object 
                testValLldpV2ManAddrConfigRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpV2ManAddrConfigRowStatus (UINT4 *pu4ErrorCode,
                                       INT4 i4LldpV2ManAddrConfigIfIndex,
                                       UINT4
                                       u4LldpV2ManAddrConfigDestAddressIndex,
                                       INT4
                                       i4LldpV2ManAddrConfigLocManAddrSubtype,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pLldpV2ManAddrConfigLocManAddr,
                                       INT4
                                       i4TestValLldpV2ManAddrConfigRowStatus)
{

    UINT1               au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR];
    tLldpLocManAddrTable *pLocManAddrNode = NULL;

    UNUSED_PARAM (i4LldpV2ManAddrConfigIfIndex);
    UNUSED_PARAM (u4LldpV2ManAddrConfigDestAddressIndex);
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    MEMSET (au1LocManAddr, 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMCPY (au1LocManAddr, pLldpV2ManAddrConfigLocManAddr->pu1_OctetList,
            pLldpV2ManAddrConfigLocManAddr->i4_Length);

    if ((i4TestValLldpV2ManAddrConfigRowStatus < LLDP_ACTIVE) ||
        (i4TestValLldpV2ManAddrConfigRowStatus > LLDP_DESTROY))
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: ROWstatus value is out of range\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValLldpV2ManAddrConfigRowStatus)
    {

        case LLDP_ACTIVE:        /*fall through */
        case LLDP_DESTROY:
            pLocManAddrNode = (tLldpLocManAddrTable *)
                LldpTxUtlGetLocManAddrNode
                (i4LldpV2ManAddrConfigLocManAddrSubtype, &au1LocManAddr[0]);

            if (pLocManAddrNode == NULL)
            {
                LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                          "LldpTxUtlGetLocManAddrNode returns NULL \r\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpV2ManAddrConfigTxPortsTable
 Input       :  The Indices
                LldpV2ManAddrConfigIfIndex
                LldpV2ManAddrConfigDestAddressIndex
                LldpV2ManAddrConfigLocManAddrSubtype
                LldpV2ManAddrConfigLocManAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpV2ManAddrConfigTxPortsTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2StatsRemTablesLastChangeTime
 Input       :  The Indices

                The Object 
                retValLldpV2StatsRemTablesLastChangeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2StatsRemTablesLastChangeTime (UINT4
                                          *pu4RetValLldpV2StatsRemTablesLastChangeTime)
{
    LldpUtlGetStatsRemTablesLastChangeTime
        (pu4RetValLldpV2StatsRemTablesLastChangeTime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2StatsRemTablesInserts
 Input       :  The Indices

                The Object 
                retValLldpV2StatsRemTablesInserts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2StatsRemTablesInserts (UINT4 *pu4RetValLldpV2StatsRemTablesInserts)
{
    LldpUtlGetStatsRemTablesInserts (pu4RetValLldpV2StatsRemTablesInserts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2StatsRemTablesDeletes
 Input       :  The Indices

                The Object 
                retValLldpV2StatsRemTablesDeletes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2StatsRemTablesDeletes (UINT4 *pu4RetValLldpV2StatsRemTablesDeletes)
{
    LldpUtlGetStatsRemTablesDeletes (pu4RetValLldpV2StatsRemTablesDeletes);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2StatsRemTablesDrops
 Input       :  The Indices

                The Object 
                retValLldpV2StatsRemTablesDrops
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2StatsRemTablesDrops (UINT4 *pu4RetValLldpV2StatsRemTablesDrops)
{
    LldpUtlGetStatsRemTablesDrops (pu4RetValLldpV2StatsRemTablesDrops);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2StatsRemTablesAgeouts
 Input       :  The Indices

                The Object 
                retValLldpV2StatsRemTablesAgeouts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2StatsRemTablesAgeouts (UINT4 *pu4RetValLldpV2StatsRemTablesAgeouts)
{
    LldpUtlGetStatsRemTablesAgeouts (pu4RetValLldpV2StatsRemTablesAgeouts);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2StatsTxPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2StatsTxPortTable
 Input       :  The Indices
                LldpV2StatsTxIfIndex
                LldpV2StatsTxDestMACAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2StatsTxPortTable (INT4 i4LldpV2StatsTxIfIndex,
                                                UINT4
                                                u4LldpV2StatsTxDestMACAddress)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2StatsTxDestMACAddress;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlValidateIndexInstanceLldpStatsTxPortTable
        (i4LldpV2StatsTxIfIndex,
         pTempDestTbl->Lldpv2DestMacAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpUtlValidateIndexInstanceLldpStatsTxPortTable returns FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2StatsTxPortTable
 Input       :  The Indices
                LldpV2StatsTxIfIndex
                LldpV2StatsTxDestMACAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2StatsTxPortTable (INT4 *pi4LldpV2StatsTxIfIndex,
                                        UINT4 *pu4LldpV2StatsTxDestMACAddress)
{
    return (nmhGetNextIndexLldpV2StatsTxPortTable (LLDP_ZERO,
                                                   pi4LldpV2StatsTxIfIndex,
                                                   LLDP_ZERO,
                                                   pu4LldpV2StatsTxDestMACAddress));
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2StatsTxPortTable
 Input       :  The Indices
                LldpV2StatsTxIfIndex
                nextLldpV2StatsTxIfIndex
                LldpV2StatsTxDestMACAddress
                nextLldpV2StatsTxDestMACAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2StatsTxPortTable (INT4 i4LldpV2StatsTxIfIndex,
                                       INT4 *pi4NextLldpV2StatsTxIfIndex,
                                       UINT4 u4LldpV2StatsTxDestMACAddress,
                                       UINT4
                                       *pu4NextLldpV2StatsTxDestMACAddress)
{
    tLldpv2AgentToLocPort Lldpv2AgentToLocPort;
    tLldpv2AgentToLocPort *pLldpv2AgentToLocPort = NULL;

    MEMSET (&Lldpv2AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    Lldpv2AgentToLocPort.i4IfIndex = i4LldpV2StatsTxIfIndex;
    Lldpv2AgentToLocPort.u4DstMacAddrTblIndex = u4LldpV2StatsTxDestMACAddress;
    pLldpv2AgentToLocPort =
        (tLldpv2AgentToLocPort *) RBTreeGetNext (gLldpGlobalInfo.
                                                 Lldpv2AgentMapTblRBTree,
                                                 &Lldpv2AgentToLocPort,
                                                 LldpAgentToLocPortIndexUtlRBCmpInfo);

    if (pLldpv2AgentToLocPort == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextLldpV2StatsTxIfIndex = pLldpv2AgentToLocPort->i4IfIndex;
    *pu4NextLldpV2StatsTxDestMACAddress =
        pLldpv2AgentToLocPort->u4DstMacAddrTblIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2StatsTxPortFramesTotal
 Input       :  The Indices
                LldpV2StatsTxIfIndex
                LldpV2StatsTxDestMACAddress

                The Object 
                retValLldpV2StatsTxPortFramesTotal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2StatsTxPortFramesTotal (INT4 i4LldpV2StatsTxIfIndex,
                                    UINT4 u4LldpV2StatsTxDestMACAddress,
                                    UINT4
                                    *pu4RetValLldpV2StatsTxPortFramesTotal)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2StatsTxDestMACAddress;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlGetStatsTxPortFramesTotal
        (i4LldpV2StatsTxIfIndex, pu4RetValLldpV2StatsTxPortFramesTotal,
         pTempDestTbl->Lldpv2DestMacAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetStatsTxPortFramesTotal Failed!! \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2StatsTxLLDPDULengthErrors
 Input       :  The Indices
                LldpV2StatsTxIfIndex
                LldpV2StatsTxDestMACAddress

                The Object 
                retValLldpV2StatsTxLLDPDULengthErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2StatsTxLLDPDULengthErrors (INT4 i4LldpV2StatsTxIfIndex,
                                       UINT4 u4LldpV2StatsTxDestMACAddress,
                                       UINT4
                                       *pu4RetValLldpV2StatsTxLLDPDULengthErrors)
{
    tLldpLocPortInfo    LldpLocPortInfo;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    UINT4               u4LocPort = 0;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return SNMP_FAILURE;
    }

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2StatsTxDestMACAddress;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlGetLocalPort
        (i4LldpV2StatsTxIfIndex, pTempDestTbl->Lldpv2DestMacAddress,
         &u4LocPort) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return SNMP_FAILURE;
    }

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD pLldpLocPortInfo is NULL LldpLocPortInfo is Invalid \r\n");
        return SNMP_FAILURE;
    }

    *pu4RetValLldpV2StatsTxLLDPDULengthErrors =
        pLldpLocalPortInfo->StatsTxPortTable.u4LldpPDULengthErrors;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2StatsRxPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2StatsRxPortTable
 Input       :  The Indices
                LldpV2StatsRxDestIfIndex
                LldpV2StatsRxDestMACAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2StatsRxPortTable (INT4 i4LldpV2StatsRxDestIfIndex,
                                                UINT4
                                                u4LldpV2StatsRxDestMACAddress)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2StatsRxDestMACAddress;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlValidateIndexInstanceLldpStatsRxPortTable
        (i4LldpV2StatsRxDestIfIndex,
         pTempDestTbl->Lldpv2DestMacAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpStatsRxPortTable Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2StatsRxPortTable
 Input       :  The Indices
                LldpV2StatsRxDestIfIndex
                LldpV2StatsRxDestMACAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2StatsRxPortTable (INT4 *pi4LldpV2StatsRxDestIfIndex,
                                        UINT4 *pu4LldpV2StatsRxDestMACAddress)
{
    return (nmhGetNextIndexLldpV2StatsRxPortTable (LLDP_ZERO,
                                                   pi4LldpV2StatsRxDestIfIndex,
                                                   LLDP_ZERO,
                                                   pu4LldpV2StatsRxDestMACAddress));
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2StatsRxPortTable
 Input       :  The Indices
                LldpV2StatsRxDestIfIndex
                nextLldpV2StatsRxDestIfIndex
                LldpV2StatsRxDestMACAddress
                nextLldpV2StatsRxDestMACAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2StatsRxPortTable (INT4 i4LldpV2StatsRxDestIfIndex,
                                       INT4 *pi4NextLldpV2StatsRxDestIfIndex,
                                       UINT4 u4LldpV2StatsRxDestMACAddress,
                                       UINT4
                                       *pu4NextLldpV2StatsRxDestMACAddress)
{
    tLldpv2AgentToLocPort *pLldpv2AgentToLocPort = NULL;
    tLldpv2AgentToLocPort Lldpv2AgentToLocPort;

    MEMSET (&Lldpv2AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return SNMP_FAILURE;
    }
    Lldpv2AgentToLocPort.i4IfIndex = i4LldpV2StatsRxDestIfIndex;
    Lldpv2AgentToLocPort.u4DstMacAddrTblIndex = u4LldpV2StatsRxDestMACAddress;
    pLldpv2AgentToLocPort =
        (tLldpv2AgentToLocPort *) RBTreeGetNext (gLldpGlobalInfo.
                                                 Lldpv2AgentMapTblRBTree,
                                                 &Lldpv2AgentToLocPort,
                                                 LldpAgentToLocPortIndexUtlRBCmpInfo);

    if (pLldpv2AgentToLocPort != NULL)
    {
        *pi4NextLldpV2StatsRxDestIfIndex = pLldpv2AgentToLocPort->i4IfIndex;
        *pu4NextLldpV2StatsRxDestMACAddress =
            pLldpv2AgentToLocPort->u4DstMacAddrTblIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2StatsRxPortFramesDiscardedTotal
 Input       :  The Indices
                LldpV2StatsRxDestIfIndex
                LldpV2StatsRxDestMACAddress

                The Object 
                retValLldpV2StatsRxPortFramesDiscardedTotal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2StatsRxPortFramesDiscardedTotal (INT4 i4LldpV2StatsRxDestIfIndex,
                                             UINT4
                                             u4LldpV2StatsRxDestMACAddress,
                                             UINT4
                                             *pu4RetValLldpV2StatsRxPortFramesDiscardedTotal)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2StatsRxDestMACAddress;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlGetStatsRxPortFramesDiscardedTotal
        (i4LldpV2StatsRxDestIfIndex, pTempDestTbl->Lldpv2DestMacAddress,
         pu4RetValLldpV2StatsRxPortFramesDiscardedTotal) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetStatsRxPortFramesDiscardedTotal Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2StatsRxPortFramesErrors
 Input       :  The Indices
                LldpV2StatsRxDestIfIndex
                LldpV2StatsRxDestMACAddress

                The Object 
                retValLldpV2StatsRxPortFramesErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2StatsRxPortFramesErrors (INT4 i4LldpV2StatsRxDestIfIndex,
                                     UINT4 u4LldpV2StatsRxDestMACAddress,
                                     UINT4
                                     *pu4RetValLldpV2StatsRxPortFramesErrors)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2StatsRxDestMACAddress;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlGetStatsRxPortFramesErrors
        (i4LldpV2StatsRxDestIfIndex, pTempDestTbl->Lldpv2DestMacAddress,
         pu4RetValLldpV2StatsRxPortFramesErrors) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetStatsRxPortFramesErrors Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2StatsRxPortFramesTotal
 Input       :  The Indices
                LldpV2StatsRxDestIfIndex
                LldpV2StatsRxDestMACAddress

                The Object 
                retValLldpV2StatsRxPortFramesTotal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2StatsRxPortFramesTotal (INT4 i4LldpV2StatsRxDestIfIndex,
                                    UINT4 u4LldpV2StatsRxDestMACAddress,
                                    UINT4
                                    *pu4RetValLldpV2StatsRxPortFramesTotal)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2StatsRxDestMACAddress;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlGetStatsRxPortFramesTotal
        (i4LldpV2StatsRxDestIfIndex, pTempDestTbl->Lldpv2DestMacAddress,
         pu4RetValLldpV2StatsRxPortFramesTotal) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetStatsRxPortFramesTotal Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2StatsRxPortTLVsDiscardedTotal
 Input       :  The Indices
                LldpV2StatsRxDestIfIndex
                LldpV2StatsRxDestMACAddress

                The Object 
                retValLldpV2StatsRxPortTLVsDiscardedTotal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2StatsRxPortTLVsDiscardedTotal (INT4 i4LldpV2StatsRxDestIfIndex,
                                           UINT4 u4LldpV2StatsRxDestMACAddress,
                                           UINT4
                                           *pu4RetValLldpV2StatsRxPortTLVsDiscardedTotal)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2StatsRxDestMACAddress;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlGetStatsRxPortTLVsDiscardedTotal
        (i4LldpV2StatsRxDestIfIndex, pTempDestTbl->Lldpv2DestMacAddress,
         pu4RetValLldpV2StatsRxPortTLVsDiscardedTotal) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetStatsRxPortTLVsDiscardedTotal Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2StatsRxPortTLVsUnrecognizedTotal
 Input       :  The Indices
                LldpV2StatsRxDestIfIndex
                LldpV2StatsRxDestMACAddress

                The Object 
                retValLldpV2StatsRxPortTLVsUnrecognizedTotal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2StatsRxPortTLVsUnrecognizedTotal (INT4 i4LldpV2StatsRxDestIfIndex,
                                              UINT4
                                              u4LldpV2StatsRxDestMACAddress,
                                              UINT4
                                              *pu4RetValLldpV2StatsRxPortTLVsUnrecognizedTotal)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2StatsRxDestMACAddress;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlGetStatsRxPortTLVsUnrecognizedTotal (i4LldpV2StatsRxDestIfIndex,
                                                    pTempDestTbl->
                                                    Lldpv2DestMacAddress,
                                                    pu4RetValLldpV2StatsRxPortTLVsUnrecognizedTotal)
        == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetStatsRxPortTLVsUnrecognizedTotal Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2StatsRxPortAgeoutsTotal
 Input       :  The Indices
                LldpV2StatsRxDestIfIndex
                LldpV2StatsRxDestMACAddress

                The Object 
                retValLldpV2StatsRxPortAgeoutsTotal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2StatsRxPortAgeoutsTotal (INT4 i4LldpV2StatsRxDestIfIndex,
                                     UINT4 u4LldpV2StatsRxDestMACAddress,
                                     UINT4
                                     *pu4RetValLldpV2StatsRxPortAgeoutsTotal)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2StatsRxDestMACAddress;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }

    if (LldpUtlGetStatsRxPortAgeoutsTotal (i4LldpV2StatsRxDestIfIndex,
                                           pTempDestTbl->Lldpv2DestMacAddress,
                                           pu4RetValLldpV2StatsRxPortAgeoutsTotal)
        == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetStatsRxPortAgeoutsTotal Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2LocChassisIdSubtype
 Input       :  The Indices

                The Object 
                retValLldpV2LocChassisIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2LocChassisIdSubtype (INT4 *pi4RetValLldpV2LocChassisIdSubtype)
{
    LldpUtlGetLocChassisIdSubtype (pi4RetValLldpV2LocChassisIdSubtype);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2LocChassisId
 Input       :  The Indices

                The Object 
                retValLldpV2LocChassisId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2LocChassisId (tSNMP_OCTET_STRING_TYPE * pRetValLldpV2LocChassisId)
{
    LldpUtlGetLocChassisId (pRetValLldpV2LocChassisId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2LocSysName
 Input       :  The Indices

                The Object 
                retValLldpV2LocSysName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2LocSysName (tSNMP_OCTET_STRING_TYPE * pRetValLldpV2LocSysName)
{
    LldpUtlLocSysName (pRetValLldpV2LocSysName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2LocSysDesc
 Input       :  The Indices

                The Object 
                retValLldpV2LocSysDesc
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2LocSysDesc (tSNMP_OCTET_STRING_TYPE * pRetValLldpV2LocSysDesc)
{
    LldpUtlLocSysDesc (pRetValLldpV2LocSysDesc);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2LocSysCapSupported
 Input       :  The Indices

                The Object 
                retValLldpV2LocSysCapSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2LocSysCapSupported (tSNMP_OCTET_STRING_TYPE *
                                pRetValLldpV2LocSysCapSupported)
{
    LldpUtlLocSysCapSupported (pRetValLldpV2LocSysCapSupported);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2LocSysCapEnabled
 Input       :  The Indices

                The Object 
                retValLldpV2LocSysCapEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2LocSysCapEnabled (tSNMP_OCTET_STRING_TYPE *
                              pRetValLldpV2LocSysCapEnabled)
{
    LldpUtlLocSysCapEnabled (pRetValLldpV2LocSysCapEnabled);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2LocPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2LocPortTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2LocPortTable (INT4 i4LldpV2LocPortIfIndex)
{
    if (LldpUtlValidateIndexInstanceLldpLocPortTable (i4LldpV2LocPortIfIndex) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpLocPortTable FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2LocPortTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2LocPortTable (INT4 *pi4LldpV2LocPortIfIndex)
{
    if (LldpUtlGetFirstIndexLldpLocPortTable (pi4LldpV2LocPortIfIndex) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpLocPortTable FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2LocPortTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2LocPortTable (INT4 i4LldpV2LocPortIfIndex,
                                   INT4 *pi4NextLldpV2LocPortIfIndex)
{
    if (LldpUtlGetNextIndexLldpLocPortTable
        (i4LldpV2LocPortIfIndex, pi4NextLldpV2LocPortIfIndex) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpLocPortTable FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2LocPortIdSubtype
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2LocPortIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2LocPortIdSubtype (INT4 i4LldpV2LocPortIfIndex,
                              INT4 *pi4RetValLldpV2LocPortIdSubtype)
{
    if (LldpUtlGetLocPortIdSubtype
        (i4LldpV2LocPortIfIndex,
         pi4RetValLldpV2LocPortIdSubtype) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLocPortIdSubtype FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2LocPortId
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2LocPortId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2LocPortId (INT4 i4LldpV2LocPortIfIndex,
                       tSNMP_OCTET_STRING_TYPE * pRetValLldpV2LocPortId)
{
    if (LldpUtlLocPortId (i4LldpV2LocPortIfIndex, pRetValLldpV2LocPortId) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: LldpUtlLocPortId FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2LocPortDesc
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpV2LocPortDesc
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2LocPortDesc (INT4 i4LldpV2LocPortIfIndex,
                         tSNMP_OCTET_STRING_TYPE * pRetValLldpV2LocPortDesc)
{
    if (LldpUtlGetLocPortDesc (i4LldpV2LocPortIfIndex, pRetValLldpV2LocPortDesc)
        == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLocPortDesc FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2LocManAddrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2LocManAddrTable
 Input       :  The Indices
                LldpV2LocManAddrSubtype
                LldpV2LocManAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2LocManAddrTable (INT4 i4LldpV2LocManAddrSubtype,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pLldpV2LocManAddr)
{
    if (LldpUtlValidateIndexInstanceLldpLocManAddrTable
        (i4LldpV2LocManAddrSubtype, pLldpV2LocManAddr) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpLocManAddrTable FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2LocManAddrTable
 Input       :  The Indices
                LldpV2LocManAddrSubtype
                LldpV2LocManAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2LocManAddrTable (INT4 *pi4LldpV2LocManAddrSubtype,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pLldpV2LocManAddr)
{
    if (LldpUtlGetFirstIndexLldpLocManAddrTable
        (pi4LldpV2LocManAddrSubtype, pLldpV2LocManAddr) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpLocManAddrTable FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2LocManAddrTable
 Input       :  The Indices
                LldpV2LocManAddrSubtype
                nextLldpV2LocManAddrSubtype
                LldpV2LocManAddr
                nextLldpV2LocManAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2LocManAddrTable (INT4 i4LldpV2LocManAddrSubtype,
                                      INT4 *pi4NextLldpV2LocManAddrSubtype,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pLldpV2LocManAddr,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextLldpV2LocManAddr)
{
    if (LldpUtlGetNextIndexLldpLocManAddrTable
        (i4LldpV2LocManAddrSubtype, pi4NextLldpV2LocManAddrSubtype,
         pLldpV2LocManAddr, pNextLldpV2LocManAddr) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpLocManAddrTable FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2LocManAddrLen
 Input       :  The Indices
                LldpV2LocManAddrSubtype
                LldpV2LocManAddr

                The Object 
                retValLldpV2LocManAddrLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2LocManAddrLen (INT4 i4LldpV2LocManAddrSubtype,
                           tSNMP_OCTET_STRING_TYPE * pLldpV2LocManAddr,
                           UINT4 *pu4RetValLldpV2LocManAddrLen)
{
    if (LldpUtlGetLocManAddrLen (i4LldpV2LocManAddrSubtype, pLldpV2LocManAddr,
                                 (INT4 *) pu4RetValLldpV2LocManAddrLen) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLocManAddrLen FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2LocManAddrIfSubtype
 Input       :  The Indices
                LldpV2LocManAddrSubtype
                LldpV2LocManAddr

                The Object 
                retValLldpV2LocManAddrIfSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2LocManAddrIfSubtype (INT4 i4LldpV2LocManAddrSubtype,
                                 tSNMP_OCTET_STRING_TYPE * pLldpV2LocManAddr,
                                 INT4 *pi4RetValLldpV2LocManAddrIfSubtype)
{
    if (LldpUtlGetLocManAddrIfSubtype
        (i4LldpV2LocManAddrSubtype, pLldpV2LocManAddr,
         pi4RetValLldpV2LocManAddrIfSubtype) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLocManAddrIfSubtype FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2LocManAddrIfId
 Input       :  The Indices
                LldpV2LocManAddrSubtype
                LldpV2LocManAddr

                The Object 
                retValLldpV2LocManAddrIfId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2LocManAddrIfId (INT4 i4LldpV2LocManAddrSubtype,
                            tSNMP_OCTET_STRING_TYPE * pLldpV2LocManAddr,
                            UINT4 *pu4RetValLldpV2LocManAddrIfId)
{
    if (LldpUtlGetLocManAddrIfId (i4LldpV2LocManAddrSubtype, pLldpV2LocManAddr,
                                  (INT4 *) pu4RetValLldpV2LocManAddrIfId) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLocManAddrIfId FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2LocManAddrOID
 Input       :  The Indices
                LldpV2LocManAddrSubtype
                LldpV2LocManAddr

                The Object 
                retValLldpV2LocManAddrOID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2LocManAddrOID (INT4 i4LldpV2LocManAddrSubtype,
                           tSNMP_OCTET_STRING_TYPE * pLldpV2LocManAddr,
                           tSNMP_OID_TYPE * pRetValLldpV2LocManAddrOID)
{
    if (LldpUtlGetLocManAddrIfOID
        (i4LldpV2LocManAddrSubtype, pLldpV2LocManAddr,
         pRetValLldpV2LocManAddrOID) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLocManAddrIfOID FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2RemTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2RemTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2RemTable (UINT4 u4LldpV2RemTimeMark,
                                        INT4 i4LldpV2RemLocalIfIndex,
                                        UINT4 u4LldpV2RemLocalDestMACAddress,
                                        UINT4 u4LldpV2RemIndex)
{
    if (LldpUtlValidateIndexInstanceLldpRemTable
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, u4LldpV2RemIndex,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpRemTable FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2RemTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2RemTable (UINT4 *pu4LldpV2RemTimeMark,
                                INT4 *pi4LldpV2RemLocalIfIndex,
                                UINT4 *pu4LldpV2RemLocalDestMACAddress,
                                UINT4 *pu4LldpV2RemIndex)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNextIndexLldpV2RemTable (LLDP_ZERO,
                                           pu4LldpV2RemTimeMark,
                                           LLDP_ZERO,
                                           pi4LldpV2RemLocalIfIndex,
                                           LLDP_ZERO,
                                           pu4LldpV2RemLocalDestMACAddress,
                                           LLDP_ZERO, pu4LldpV2RemIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2RemTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2RemTable (UINT4 u4LldpV2RemTimeMark,
                               UINT4 *pu4NextLldpV2RemTimeMark,
                               INT4 i4LldpV2RemLocalIfIndex,
                               INT4 *pi4NextLldpV2RemLocalIfIndex,
                               UINT4 u4LldpV2RemLocalDestMACAddress,
                               UINT4 *pu4NextLldpV2RemLocalDestMACAddress,
                               UINT4 u4LldpV2RemIndex,
                               UINT4 *pu4NextLldpV2RemIndex)
{
    tLldpRemoteNode    *pTmpRemoteNode = NULL;
    tLldpRemoteNode    *pLldpRemoteNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    /* Allocate Memory for Remote Node */
    if ((pTmpRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_MANDATORY_TLV_TRC,
                  "SNMPSTD: Failed to Allocate Memory " "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return SNMP_FAILURE;
    }
    MEMSET (pTmpRemoteNode, 0, sizeof (tLldpRemoteNode));

    pTmpRemoteNode->u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    pTmpRemoteNode->i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    pTmpRemoteNode->u4DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    pTmpRemoteNode->i4RemIndex = u4LldpV2RemIndex;

    pLldpRemoteNode =
        (tLldpRemoteNode *) RBTreeGetNext (gLldpGlobalInfo.RemSysDataRBTree,
                                           (tRBElem *) pTmpRemoteNode, NULL);

    if (pLldpRemoteNode != NULL)
    {
        *pu4NextLldpV2RemTimeMark = pLldpRemoteNode->u4RemLastUpdateTime;
        *pi4NextLldpV2RemLocalIfIndex = pLldpRemoteNode->i4RemLocalPortNum;
        *pu4NextLldpV2RemIndex = pLldpRemoteNode->i4RemIndex;
        *pu4NextLldpV2RemLocalDestMACAddress =
            pLldpRemoteNode->u4DestAddrTblIndex;
        i1RetVal = SNMP_SUCCESS;
    }
    else
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Invalid Index Failed\r\n");
    }
    /* Release the memory allocated for Remote Node */
    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                        (UINT1 *) pTmpRemoteNode);
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2RemChassisIdSubtype
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2RemChassisIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2RemChassisIdSubtype (UINT4 u4LldpV2RemTimeMark,
                                 INT4 i4LldpV2RemLocalIfIndex,
                                 UINT4 u4LldpV2RemLocalDestMACAddress,
                                 UINT4 u4LldpV2RemIndex,
                                 INT4 *pi4RetValLldpV2RemChassisIdSubtype)
{
    if (LldpUtlGetRemChassisIdSubtype
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, u4LldpV2RemIndex,
         pi4RetValLldpV2RemChassisIdSubtype,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetRemChassisIdSubtype Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2RemChassisId
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2RemChassisId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2RemChassisId (UINT4 u4LldpV2RemTimeMark,
                          INT4 i4LldpV2RemLocalIfIndex,
                          UINT4 u4LldpV2RemLocalDestMACAddress,
                          UINT4 u4LldpV2RemIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValLldpV2RemChassisId)
{
    if (LldpUtlGetRemChassisId
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, u4LldpV2RemIndex,
         pRetValLldpV2RemChassisId,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetRemChassisId Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2RemPortIdSubtype
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2RemPortIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2RemPortIdSubtype (UINT4 u4LldpV2RemTimeMark,
                              INT4 i4LldpV2RemLocalIfIndex,
                              UINT4 u4LldpV2RemLocalDestMACAddress,
                              UINT4 u4LldpV2RemIndex,
                              INT4 *pi4RetValLldpV2RemPortIdSubtype)
{
    if (LldpUtlGetRemPortIdSubtype
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, u4LldpV2RemIndex,
         pi4RetValLldpV2RemPortIdSubtype,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetRemChassisId Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2RemPortId
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2RemPortId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2RemPortId (UINT4 u4LldpV2RemTimeMark, INT4 i4LldpV2RemLocalIfIndex,
                       UINT4 u4LldpV2RemLocalDestMACAddress,
                       UINT4 u4LldpV2RemIndex,
                       tSNMP_OCTET_STRING_TYPE * pRetValLldpV2RemPortId)
{
    if (LldpUtlGetRemPortId
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, u4LldpV2RemIndex,
         pRetValLldpV2RemPortId,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetRemChassisId Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2RemPortDesc
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2RemPortDesc
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2RemPortDesc (UINT4 u4LldpV2RemTimeMark,
                         INT4 i4LldpV2RemLocalIfIndex,
                         UINT4 u4LldpV2RemLocalDestMACAddress,
                         UINT4 u4LldpV2RemIndex,
                         tSNMP_OCTET_STRING_TYPE * pRetValLldpV2RemPortDesc)
{
    if (LldpUtlGetRemPortDesc
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, u4LldpV2RemIndex,
         pRetValLldpV2RemPortDesc,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetRemChassisId Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2RemSysName
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2RemSysName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2RemSysName (UINT4 u4LldpV2RemTimeMark, INT4 i4LldpV2RemLocalIfIndex,
                        UINT4 u4LldpV2RemLocalDestMACAddress,
                        UINT4 u4LldpV2RemIndex,
                        tSNMP_OCTET_STRING_TYPE * pRetValLldpV2RemSysName)
{
    if (LldpUtlGetRemSysName
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, u4LldpV2RemIndex,
         pRetValLldpV2RemSysName,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetRemChassisId Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2RemSysDesc
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2RemSysDesc
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2RemSysDesc (UINT4 u4LldpV2RemTimeMark, INT4 i4LldpV2RemLocalIfIndex,
                        UINT4 u4LldpV2RemLocalDestMACAddress,
                        UINT4 u4LldpV2RemIndex,
                        tSNMP_OCTET_STRING_TYPE * pRetValLldpV2RemSysDesc)
{
    if (LldpUtlGetRemSysDesc
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, u4LldpV2RemIndex,
         pRetValLldpV2RemSysDesc,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetRemChassisId Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2RemSysCapSupported
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocALifIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2RemSysCapSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2RemSysCapSupported (UINT4 u4LldpV2RemTimeMark,
                                INT4 i4LldpV2RemLocalIfIndex,
                                UINT4 u4LldpV2RemLocalDestMACAddress,
                                UINT4 u4LldpV2RemIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValLldpV2RemSysCapSupported)
{
    if (LldpUtlGetRemSysCapSupported
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, u4LldpV2RemIndex,
         pRetValLldpV2RemSysCapSupported,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetRemChassisId Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2RemSysCapEnabled
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2RemSysCapEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2RemSysCapEnabled (UINT4 u4LldpV2RemTimeMark,
                              INT4 i4LldpV2RemLocalIfIndex,
                              UINT4 u4LldpV2RemLocalDestMACAddress,
                              UINT4 u4LldpV2RemIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValLldpV2RemSysCapEnabled)
{
    if (LldpUtlGetRemSysCapEnabled
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, u4LldpV2RemIndex,
         pRetValLldpV2RemSysCapEnabled,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetRemChassisId Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2RemRemoteChanges
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2RemRemoteChanges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2RemRemoteChanges (UINT4 u4LldpV2RemTimeMark,
                              INT4 i4LldpV2RemLocalIfIndex,
                              UINT4 u4LldpV2RemLocalDestMACAddress,
                              UINT4 u4LldpV2RemIndex,
                              INT4 *pi4RetValLldpV2RemRemoteChanges)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    tLldpRemoteNode    *pTmpRemoteNode = NULL;
    tLldpRemoteNode    *pRemoteNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));

    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }
    /* Allocate Memory for Remote Node */
    if ((pTmpRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_MANDATORY_TLV_TRC,
                  "SNMPSTD: Failed to Allocate Memory " "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return SNMP_FAILURE;
    }
    MEMSET (pTmpRemoteNode, 0, sizeof (tLldpRemoteNode));

    pTmpRemoteNode->u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    pTmpRemoteNode->i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    pTmpRemoteNode->u4DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    pTmpRemoteNode->i4RemIndex = (INT4) u4LldpV2RemIndex;

    pRemoteNode =
        (tLldpRemoteNode *) RBTreeGet (gLldpGlobalInfo.RemSysDataRBTree,
                                       pTmpRemoteNode);
    if (pRemoteNode != NULL)
    {
        *pi4RetValLldpV2RemRemoteChanges = (INT4) pRemoteNode->bRemoteChanges;
        i1RetVal = SNMP_SUCCESS;
    }

    /* Release the memory allocated for Remote Node */
    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                        (UINT1 *) pTmpRemoteNode);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetLldpV2RemTooManyNeighbors
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpV2RemTooManyNeighbors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2RemTooManyNeighbors (UINT4 u4LldpV2RemTimeMark,
                                 INT4 i4LldpV2RemLocalIfIndex,
                                 UINT4 u4LldpV2RemLocalDestMACAddress,
                                 UINT4 u4LldpV2RemIndex,
                                 INT4 *pi4RetValLldpV2RemTooManyNeighbors)
{
    tLldpv2DestAddrTbl *pTempDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    tLldpRemoteNode    *pTmpRemoteNode;
    tLldpRemoteNode    *pRemoteNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    pTempDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pTempDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: pTempDestTbl is NULL invalid"
                  " u4LldpV2PortConfigDestAddressIndex \r\n");
        return SNMP_FAILURE;
    }

    /* Allocate Memory for Remote Node */
    if ((pTmpRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_MANDATORY_TLV_TRC,
                  "SNMPSTD: Failed to Allocate Memory " "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return SNMP_FAILURE;
    }
    MEMSET (pTmpRemoteNode, 0, sizeof (tLldpRemoteNode));
    pTmpRemoteNode->u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    pTmpRemoteNode->i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    pTmpRemoteNode->u4DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    pTmpRemoteNode->i4RemIndex = (INT4) u4LldpV2RemIndex;

    pRemoteNode =
        (tLldpRemoteNode *) RBTreeGet (gLldpGlobalInfo.RemSysDataRBTree,
                                       pTmpRemoteNode);
    if (pRemoteNode != NULL)
    {
        *pi4RetValLldpV2RemTooManyNeighbors =
            (INT4) pRemoteNode->bTooManyNeighbors;
        i1RetVal = SNMP_SUCCESS;
    }
    /* Release the memory allocated for Remote Node */
    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                        (UINT1 *) pTmpRemoteNode);
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : LldpV2RemManAddrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2RemManAddrTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpV2RemManAddrSubtype
                LldpV2RemManAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2RemManAddrTable (UINT4 u4LldpV2RemTimeMark,
                                               INT4 i4LldpV2RemLocalIfIndex,
                                               UINT4
                                               u4LldpV2RemLocalDestMACAddress,
                                               UINT4 u4LldpV2RemIndex,
                                               INT4 i4LldpV2RemManAddrSubtype,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pLldpV2RemManAddr)
{
    if (LldpUtlValidateIndexInstanceLldpRemManAddrTable
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, u4LldpV2RemIndex,
         i4LldpV2RemManAddrSubtype, pLldpV2RemManAddr,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpRemManAddrTable Failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2RemManAddrTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpV2RemManAddrSubtype
                LldpV2RemManAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2RemManAddrTable (UINT4 *pu4LldpV2RemTimeMark,
                                       INT4 *pi4LldpV2RemLocalIfIndex,
                                       UINT4 *pu4LldpV2RemLocalDestMACAddress,
                                       UINT4 *pu4LldpV2RemIndex,
                                       INT4 *pi4LldpV2RemManAddrSubtype,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pLldpV2RemManAddr)
{
    tLldpRemManAddressTable *pRemManAddrTbl;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return SNMP_FAILURE;
    }
    pRemManAddrTbl = (tLldpRemManAddressTable *)
        RBTreeGetFirst (gLldpGlobalInfo.RemManAddrRBTree);

    if (pRemManAddrTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: RBTreeGetFirst for Remote ManAddrTbl failed\r\n");
        return SNMP_FAILURE;
    }

    *pu4LldpV2RemTimeMark = pRemManAddrTbl->u4RemLastUpdateTime;
    *pi4LldpV2RemLocalIfIndex = pRemManAddrTbl->i4RemLocalPortNum;
    *pu4LldpV2RemLocalDestMACAddress = pRemManAddrTbl->u4DestAddrTblIndex;
    *pu4LldpV2RemIndex = pRemManAddrTbl->i4RemIndex;
    *pi4LldpV2RemManAddrSubtype = pRemManAddrTbl->i4RemManAddrSubtype;
    MEMCPY (pLldpV2RemManAddr->pu1_OctetList, pRemManAddrTbl->au1RemManAddr,
            (sizeof (pRemManAddrTbl->au1RemManAddr)));
    pLldpV2RemManAddr->i4_Length = sizeof (pRemManAddrTbl->au1RemManAddr);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2RemManAddrTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
                LldpV2RemManAddrSubtype
                nextLldpV2RemManAddrSubtype
                LldpV2RemManAddr
                nextLldpV2RemManAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2RemManAddrTable (UINT4 u4LldpV2RemTimeMark,
                                      UINT4 *pu4NextLldpV2RemTimeMark,
                                      INT4 i4LldpV2RemLocalIfIndex,
                                      INT4 *pi4NextLldpV2RemLocalIfIndex,
                                      UINT4 u4LldpV2RemLocalDestMACAddress,
                                      UINT4
                                      *pu4NextLldpV2RemLocalDestMACAddress,
                                      UINT4 u4LldpV2RemIndex,
                                      UINT4 *pu4NextLldpV2RemIndex,
                                      INT4 i4LldpV2RemManAddrSubtype,
                                      INT4 *pi4NextLldpV2RemManAddrSubtype,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pLldpV2RemManAddr,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextLldpV2RemManAddr)
{
    tLldpRemManAddressTable *pRemManAddrTbl;
    tLldpRemManAddressTable RemManAddrTbl;

    MEMSET (&RemManAddrTbl, 0, sizeof (tLldpRemManAddressTable));

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return SNMP_FAILURE;
    }
    RemManAddrTbl.u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    RemManAddrTbl.i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    RemManAddrTbl.u4DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    RemManAddrTbl.i4RemIndex = (INT4) u4LldpV2RemIndex;
    RemManAddrTbl.i4RemManAddrSubtype = i4LldpV2RemManAddrSubtype;
    MEMCPY (RemManAddrTbl.au1RemManAddr, pLldpV2RemManAddr->pu1_OctetList,
            pLldpV2RemManAddr->i4_Length);

    pRemManAddrTbl = (tLldpRemManAddressTable *)
        RBTreeGetNext (gLldpGlobalInfo.RemManAddrRBTree, &RemManAddrTbl, NULL);
    if (pRemManAddrTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: RBTreeGetNext for Remote ManAddrTbl failed\r\n");
        return SNMP_FAILURE;
    }
    *pu4NextLldpV2RemTimeMark = pRemManAddrTbl->u4RemLastUpdateTime;
    *pi4NextLldpV2RemLocalIfIndex = pRemManAddrTbl->i4RemLocalPortNum;
    *pu4NextLldpV2RemLocalDestMACAddress = pRemManAddrTbl->u4DestAddrTblIndex;
    *pu4NextLldpV2RemIndex = pRemManAddrTbl->i4RemIndex;
    *pi4NextLldpV2RemManAddrSubtype = pRemManAddrTbl->i4RemManAddrSubtype;
    MEMCPY (pNextLldpV2RemManAddr->pu1_OctetList, pRemManAddrTbl->au1RemManAddr,
            MEM_MAX_BYTES (sizeof (pRemManAddrTbl->au1RemManAddr),
                           (LLDP_MAX_LEN_MAN_ADDR + 1)));
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2RemManAddrIfSubtype
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpV2RemManAddrSubtype
                LldpV2RemManAddr

                The Object 
                retValLldpV2RemManAddrIfSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2RemManAddrIfSubtype (UINT4 u4LldpV2RemTimeMark,
                                 INT4 i4LldpV2RemLocalIfIndex,
                                 UINT4 u4LldpV2RemLocalDestMACAddress,
                                 UINT4 u4LldpV2RemIndex,
                                 INT4 i4LldpV2RemManAddrSubtype,
                                 tSNMP_OCTET_STRING_TYPE * pLldpV2RemManAddr,
                                 INT4 *pi4RetValLldpV2RemManAddrIfSubtype)
{
    if (LldpUtlGetRemManAddrIfSubtype
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, u4LldpV2RemIndex,
         i4LldpV2RemManAddrSubtype, pLldpV2RemManAddr,
         pi4RetValLldpV2RemManAddrIfSubtype,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetRemManAddrIfSubtype Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2RemManAddrIfId
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpV2RemManAddrSubtype
                LldpV2RemManAddr

                The Object 
                retValLldpV2RemManAddrIfId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2RemManAddrIfId (UINT4 u4LldpV2RemTimeMark,
                            INT4 i4LldpV2RemLocalIfIndex,
                            UINT4 u4LldpV2RemLocalDestMACAddress,
                            UINT4 u4LldpV2RemIndex,
                            INT4 i4LldpV2RemManAddrSubtype,
                            tSNMP_OCTET_STRING_TYPE * pLldpV2RemManAddr,
                            UINT4 *pu4RetValLldpV2RemManAddrIfId)
{
    if (LldpUtlGetRemManAddrIfId
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, (INT4) u4LldpV2RemIndex,
         i4LldpV2RemManAddrSubtype, pLldpV2RemManAddr,
         (INT4 *) pu4RetValLldpV2RemManAddrIfId,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetRemManAddrIfId Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpV2RemManAddrOID
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpV2RemManAddrSubtype
                LldpV2RemManAddr

                The Object 
                retValLldpV2RemManAddrOID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2RemManAddrOID (UINT4 u4LldpV2RemTimeMark,
                           INT4 i4LldpV2RemLocalIfIndex,
                           UINT4 u4LldpV2RemLocalDestMACAddress,
                           UINT4 u4LldpV2RemIndex,
                           INT4 i4LldpV2RemManAddrSubtype,
                           tSNMP_OCTET_STRING_TYPE * pLldpV2RemManAddr,
                           tSNMP_OID_TYPE * pRetValLldpV2RemManAddrOID)
{
    if (LldpUtlGetRemManAddrOID
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, u4LldpV2RemIndex,
         i4LldpV2RemManAddrSubtype, pLldpV2RemManAddr,
         pRetValLldpV2RemManAddrOID,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD:LldpUtlGetRemManAddrOID Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2RemUnknownTLVTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2RemUnknownTLVTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpV2RemUnknownTLVType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2RemUnknownTLVTable (UINT4 u4LldpV2RemTimeMark,
                                                  INT4 i4LldpV2RemLocalIfIndex,
                                                  UINT4
                                                  u4LldpV2RemLocalDestMACAddress,
                                                  UINT4 u4LldpV2RemIndex,
                                                  UINT4
                                                  u4LldpV2RemUnknownTLVType)
{

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return SNMP_FAILURE;
    }
    if (LldpUtlValidateIndexInstanceRemUnknownTLVTable
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, u4LldpV2RemIndex,
         u4LldpV2RemUnknownTLVType,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceRemUnknownTLVTable Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2RemUnknownTLVTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpV2RemUnknownTLVType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2RemUnknownTLVTable (UINT4 *pu4LldpV2RemTimeMark,
                                          INT4 *pi4LldpV2RemLocalIfIndex,
                                          UINT4
                                          *pu4LldpV2RemLocalDestMACAddress,
                                          UINT4 *pu4LldpV2RemIndex,
                                          UINT4 *pu4LldpV2RemUnknownTLVType)
{
    return (nmhGetNextIndexLldpV2RemUnknownTLVTable (LLDP_ZERO,
                                                     pu4LldpV2RemTimeMark,
                                                     LLDP_ZERO,
                                                     pi4LldpV2RemLocalIfIndex,
                                                     LLDP_ZERO,
                                                     pu4LldpV2RemLocalDestMACAddress,
                                                     LLDP_ZERO,
                                                     pu4LldpV2RemIndex,
                                                     LLDP_ZERO,
                                                     pu4LldpV2RemUnknownTLVType));
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2RemUnknownTLVTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
                LldpV2RemUnknownTLVType
                nextLldpV2RemUnknownTLVType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2RemUnknownTLVTable (UINT4 u4LldpV2RemTimeMark,
                                         UINT4 *pu4NextLldpV2RemTimeMark,
                                         INT4 i4LldpV2RemLocalIfIndex,
                                         INT4 *pi4NextLldpV2RemLocalIfIndex,
                                         UINT4 u4LldpV2RemLocalDestMACAddress,
                                         UINT4
                                         *pu4NextLldpV2RemLocalDestMACAddress,
                                         UINT4 u4LldpV2RemIndex,
                                         UINT4 *pu4NextLldpV2RemIndex,
                                         UINT4 u4LldpV2RemUnknownTLVType,
                                         UINT4 *pu4NextLldpV2RemUnknownTLVType)
{
    tLldpRemUnknownTLVTable *pRemUnknownTLVTable = NULL;
    tLldpRemUnknownTLVTable RemUnknownTLVTable;

    MEMSET (&RemUnknownTLVTable, 0, sizeof (tLldpRemUnknownTLVTable));

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return SNMP_FAILURE;
    }
    RemUnknownTLVTable.u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    RemUnknownTLVTable.i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    RemUnknownTLVTable.u4DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    RemUnknownTLVTable.i4RemIndex = u4LldpV2RemIndex;
    RemUnknownTLVTable.i4RemUnknownTLVType = u4LldpV2RemUnknownTLVType;

    pRemUnknownTLVTable = (tLldpRemUnknownTLVTable *)
        RBTreeGetNext (gLldpGlobalInfo.RemUnknownTLVRBTree, &RemUnknownTLVTable,
                       NULL);
    if (pRemUnknownTLVTable == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: GetNext for remote unknown TLV table failed\r\n");
        return SNMP_FAILURE;
    }
    *pu4NextLldpV2RemTimeMark = pRemUnknownTLVTable->u4RemLastUpdateTime;
    *pi4NextLldpV2RemLocalIfIndex = pRemUnknownTLVTable->i4RemLocalPortNum;
    *pu4NextLldpV2RemLocalDestMACAddress =
        pRemUnknownTLVTable->u4DestAddrTblIndex;
    *pu4NextLldpV2RemIndex = pRemUnknownTLVTable->i4RemIndex;
    *pu4NextLldpV2RemUnknownTLVType = pRemUnknownTLVTable->i4RemUnknownTLVType;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2RemUnknownTLVInfo
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpV2RemUnknownTLVType

                The Object 
                retValLldpV2RemUnknownTLVInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2RemUnknownTLVInfo (UINT4 u4LldpV2RemTimeMark,
                               INT4 i4LldpV2RemLocalIfIndex,
                               UINT4 u4LldpV2RemLocalDestMACAddress,
                               UINT4 u4LldpV2RemIndex,
                               UINT4 u4LldpV2RemUnknownTLVType,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValLldpV2RemUnknownTLVInfo)
{
    if (LldpUtlGetRemUnknownTLVInfo
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, u4LldpV2RemIndex,
         u4LldpV2RemUnknownTLVType, pRetValLldpV2RemUnknownTLVInfo,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetRemUnknownTLVInfo failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpV2RemOrgDefInfoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpV2RemOrgDefInfoTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpV2RemOrgDefInfoOUI
                LldpV2RemOrgDefInfoSubtype
                LldpV2RemOrgDefInfoIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpV2RemOrgDefInfoTable (UINT4 u4LldpV2RemTimeMark,
                                                  INT4 i4LldpV2RemLocalIfIndex,
                                                  UINT4
                                                  u4LldpV2RemLocalDestMACAddress,
                                                  UINT4 u4LldpV2RemIndex,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pLldpV2RemOrgDefInfoOUI,
                                                  UINT4
                                                  u4LldpV2RemOrgDefInfoSubtype,
                                                  UINT4
                                                  u4LldpV2RemOrgDefInfoIndex)
{
    if (LldpUtlValidateIndexInstanceRemOrgDefInfoTable
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, u4LldpV2RemIndex,
         pLldpV2RemOrgDefInfoOUI, u4LldpV2RemOrgDefInfoSubtype,
         u4LldpV2RemOrgDefInfoIndex,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceRemOrgDefInfoTable Failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpV2RemOrgDefInfoTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpV2RemOrgDefInfoOUI
                LldpV2RemOrgDefInfoSubtype
                LldpV2RemOrgDefInfoIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpV2RemOrgDefInfoTable (UINT4 *pu4LldpV2RemTimeMark,
                                          INT4 *pi4LldpV2RemLocalIfIndex,
                                          UINT4
                                          *pu4LldpV2RemLocalDestMACAddress,
                                          UINT4 *pu4LldpV2RemIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pLldpV2RemOrgDefInfoOUI,
                                          UINT4 *pu4LldpV2RemOrgDefInfoSubtype,
                                          UINT4 *pu4LldpV2RemOrgDefInfoIndex)
{
    tLldpRemOrgDefInfoTable *pRemOrgDefTbl = NULL;

    pRemOrgDefTbl = (tLldpRemOrgDefInfoTable *)
        RBTreeGetFirst (gLldpGlobalInfo.RemOrgDefInfoRBTree);
    if (pRemOrgDefTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: GetFirst for remote OrgDefInfoTable Failed \r\n");
        return SNMP_FAILURE;
    }
    *pu4LldpV2RemTimeMark = pRemOrgDefTbl->u4RemLastUpdateTime;
    *pi4LldpV2RemLocalIfIndex = pRemOrgDefTbl->i4RemLocalPortNum;
    *pu4LldpV2RemLocalDestMACAddress = pRemOrgDefTbl->u4DestAddrTblIndex;
    *pu4LldpV2RemIndex = pRemOrgDefTbl->i4RemIndex;
    MEMCPY (pLldpV2RemOrgDefInfoOUI->pu1_OctetList,
            pRemOrgDefTbl->au1RemOrgDefInfoOUI,
            MEM_MAX_BYTES (sizeof (pRemOrgDefTbl->au1RemOrgDefInfoOUI),
                           (LLDP_MAX_LEN_OUI + 1)));
    pLldpV2RemOrgDefInfoOUI->i4_Length =
        MEM_MAX_BYTES (sizeof (pRemOrgDefTbl->au1RemOrgDefInfoOUI),
                       (LLDP_MAX_LEN_OUI + 1));

    *pu4LldpV2RemOrgDefInfoSubtype = pRemOrgDefTbl->i4RemOrgDefInfoSubtype;
    *pu4LldpV2RemOrgDefInfoIndex = pRemOrgDefTbl->i4RemOrgDefInfoIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpV2RemOrgDefInfoTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
                LldpV2RemOrgDefInfoOUI
                nextLldpV2RemOrgDefInfoOUI
                LldpV2RemOrgDefInfoSubtype
                nextLldpV2RemOrgDefInfoSubtype
                LldpV2RemOrgDefInfoIndex
                nextLldpV2RemOrgDefInfoIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values.  The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpV2RemOrgDefInfoTable (UINT4 u4LldpV2RemTimeMark,
                                         UINT4 *pu4NextLldpV2RemTimeMark,
                                         INT4 i4LldpV2RemLocalIfIndex,
                                         INT4 *pi4NextLldpV2RemLocalIfIndex,
                                         UINT4 u4LldpV2RemLocalDestMACAddress,
                                         UINT4
                                         *pu4NextLldpV2RemLocalDestMACAddress,
                                         UINT4 u4LldpV2RemIndex,
                                         UINT4 *pu4NextLldpV2RemIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pLldpV2RemOrgDefInfoOUI,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNextLldpV2RemOrgDefInfoOUI,
                                         UINT4 u4LldpV2RemOrgDefInfoSubtype,
                                         UINT4
                                         *pu4NextLldpV2RemOrgDefInfoSubtype,
                                         UINT4 u4LldpV2RemOrgDefInfoIndex,
                                         UINT4 *pu4NextLldpV2RemOrgDefInfoIndex)
{
    tLldpRemOrgDefInfoTable *pRemOrgDefTbl = NULL;
    tLldpRemOrgDefInfoTable RemOrgDefTbl;

    MEMSET (&RemOrgDefTbl, 0, sizeof (tLldpRemOrgDefInfoTable));
    RemOrgDefTbl.u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    RemOrgDefTbl.i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    RemOrgDefTbl.u4DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    RemOrgDefTbl.i4RemIndex = (INT4) u4LldpV2RemIndex;
    MEMCPY (RemOrgDefTbl.au1RemOrgDefInfoOUI,
            pLldpV2RemOrgDefInfoOUI->pu1_OctetList,
            (UINT4) pLldpV2RemOrgDefInfoOUI->i4_Length);
    RemOrgDefTbl.i4RemOrgDefInfoSubtype = (INT4) u4LldpV2RemOrgDefInfoSubtype;
    RemOrgDefTbl.i4RemOrgDefInfoIndex = (INT4) u4LldpV2RemOrgDefInfoIndex;

    pRemOrgDefTbl = (tLldpRemOrgDefInfoTable *)
        RBTreeGetNext (gLldpGlobalInfo.RemOrgDefInfoRBTree, &RemOrgDefTbl,
                       NULL);
    if (pRemOrgDefTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: GetFirst for remote OrgDefInfoTable Failed \r\n");
        return SNMP_FAILURE;
    }
    *pu4NextLldpV2RemTimeMark = pRemOrgDefTbl->u4RemLastUpdateTime;
    *pi4NextLldpV2RemLocalIfIndex = pRemOrgDefTbl->i4RemLocalPortNum;
    *pu4NextLldpV2RemLocalDestMACAddress = pRemOrgDefTbl->u4DestAddrTblIndex;
    *pu4NextLldpV2RemIndex = (UINT4) pRemOrgDefTbl->i4RemIndex;
    MEMCPY (pNextLldpV2RemOrgDefInfoOUI->pu1_OctetList,
            pRemOrgDefTbl->au1RemOrgDefInfoOUI,
            MEM_MAX_BYTES (sizeof (pRemOrgDefTbl->au1RemOrgDefInfoOUI),
                           (LLDP_MAX_LEN_OUI + 1)));
    pNextLldpV2RemOrgDefInfoOUI->i4_Length =
        MEM_MAX_BYTES (sizeof (pRemOrgDefTbl->au1RemOrgDefInfoOUI),
                       (LLDP_MAX_LEN_OUI + 1));

    *pu4NextLldpV2RemOrgDefInfoSubtype =
        (UINT4) pRemOrgDefTbl->i4RemOrgDefInfoSubtype;
    *pu4NextLldpV2RemOrgDefInfoIndex =
        (UINT4) pRemOrgDefTbl->i4RemOrgDefInfoIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpV2RemOrgDefInfo
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpV2RemOrgDefInfoOUI
                LldpV2RemOrgDefInfoSubtype
                LldpV2RemOrgDefInfoIndex

                The Object 
                retValLldpV2RemOrgDefInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpV2RemOrgDefInfo (UINT4 u4LldpV2RemTimeMark,
                           INT4 i4LldpV2RemLocalIfIndex,
                           UINT4 u4LldpV2RemLocalDestMACAddress,
                           UINT4 u4LldpV2RemIndex,
                           tSNMP_OCTET_STRING_TYPE * pLldpV2RemOrgDefInfoOUI,
                           UINT4 u4LldpV2RemOrgDefInfoSubtype,
                           UINT4 u4LldpV2RemOrgDefInfoIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValLldpV2RemOrgDefInfo)
{
    if (LldpUtlGetRemOrgDefInfo
        (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex, u4LldpV2RemIndex,
         pLldpV2RemOrgDefInfoOUI, u4LldpV2RemOrgDefInfoSubtype,
         u4LldpV2RemOrgDefInfoIndex, pRetValLldpV2RemOrgDefInfo,
         u4LldpV2RemLocalDestMACAddress) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetRemOrgDefInfo failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
