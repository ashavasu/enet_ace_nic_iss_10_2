/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: stdot1lw.c,v 1.21 2013/06/07 12:40:31 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "lldinc.h"
#include "lldcli.h"
#include "sd1lv2cli.h"
/* LOW LEVEL Routines for Table : LldpXdot1ConfigPortVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1ConfigPortVlanTable
 Input       :  The Indices
                LldpPortConfigPortNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1ConfigPortVlanTable (INT4
                                                      i4LldpPortConfigPortNum)
{
    if (LldpUtlValidateIndexInstanceLldpXdot1ConfigPortVlanTable
        (i4LldpPortConfigPortNum, gau1LldpMcastAddr) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpUtlValidateIndexInstanceLldpXdot1ConfigPortVlanTable returns FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1ConfigPortVlanTable
 Input       :  The Indices
                LldpPortConfigPortNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1ConfigPortVlanTable (INT4 *pi4LldpPortConfigPortNum)
{
    if (LldpUtlGetFirstIndexLldpXdot1ConfigPortVlanTable
        (pi4LldpPortConfigPortNum) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpUtlGetFirstIndexLldpXdot1ConfigPortVlanTable returns FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1ConfigPortVlanTable
 Input       :  The Indices
                LldpPortConfigPortNum
                nextLldpPortConfigPortNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1ConfigPortVlanTable (INT4 i4LldpPortConfigPortNum,
                                             INT4 *pi4NextLldpPortConfigPortNum)
{
    if (LldpUtlGetNextIndexLldpXdot1ConfigPortVlanTable
        (i4LldpPortConfigPortNum, pi4NextLldpPortConfigPortNum) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpUtlGetNextIndexLldpXdot1ConfigPortVlanTable returns FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1ConfigPortVlanTxEnable
 Input       :  The Indices
                LldpPortConfigPortNum

                The Object 
                retValLldpXdot1ConfigPortVlanTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1ConfigPortVlanTxEnable (INT4 i4LldpPortConfigPortNum,
                                       INT4
                                       *pi4RetValLldpXdot1ConfigPortVlanTxEnable)
{
    if (LldpUtlGetLldpXdot1ConfigPortVlanTxEnable
        (i4LldpPortConfigPortNum, gau1LldpMcastAddr,
         pi4RetValLldpXdot1ConfigPortVlanTxEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpUtlGetLldpXdot1ConfigPortVlanTxEnable returns FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXdot1ConfigPortVlanTxEnable
 Input       :  The Indices
                LldpPortConfigPortNum

                The Object 
                setValLldpXdot1ConfigPortVlanTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpXdot1ConfigPortVlanTxEnable (INT4 i4LldpPortConfigPortNum,
                                       INT4
                                       i4SetValLldpXdot1ConfigPortVlanTxEnable)
{
    if (LldpUtlSetLldpXdot1ConfigPortVlanTxEnable
        (i4LldpPortConfigPortNum, gau1LldpMcastAddr,
         i4SetValLldpXdot1ConfigPortVlanTxEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpUtlSetLldpXdot1ConfigPortVlanTxEnable returns FAILURE \r\n");
        return SNMP_FAILURE;
    }

    /*Sending Trigger for MSR */
    LLDP_MSR_NOTIFY_DOT1_CONFIG_PORT_VLAN_TBL
        (LldpV2Xdot1ConfigPortVlanTxEnable,
         (sizeof (LldpV2Xdot1ConfigPortVlanTxEnable) / sizeof (UINT4)),
         i4LldpPortConfigPortNum, i4SetValLldpXdot1ConfigPortVlanTxEnable);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXdot1ConfigPortVlanTxEnable
 Input       :  The Indices
                LldpPortConfigPortNum

                The Object 
                testValLldpXdot1ConfigPortVlanTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpXdot1ConfigPortVlanTxEnable (UINT4 *pu4ErrorCode,
                                          INT4 i4LldpPortConfigPortNum,
                                          INT4
                                          i4TestValLldpXdot1ConfigPortVlanTxEnable)
{
    if (LldpUtlTestLldpXdot1ConfigPortVlanTxEnable
        (pu4ErrorCode, i4LldpPortConfigPortNum,
         i4TestValLldpXdot1ConfigPortVlanTxEnable,
         gau1LldpMcastAddr) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpUtlTestLldpXdot1ConfigPortVlanTxEnable returns FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1LocVlanNameTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1LocVlanNameTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1LocVlanNameTable (INT4 i4LldpLocPortNum,
                                                   INT4 i4LldpXdot1LocVlanId)
{
    if (LldpUtlValidateIndexInstanceLldpXdot1LocVlanNameTable (i4LldpLocPortNum,
                                                               i4LldpXdot1LocVlanId)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot1LocVlanNameTable failed!! \n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1LocVlanNameTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1LocVlanNameTable (INT4 *pi4LldpLocPortNum,
                                           INT4 *pi4LldpXdot1LocVlanId)
{
    if (LldpUtlGetFirstIndexLldpXdot1LocVlanNameTable (pi4LldpLocPortNum,
                                                       pi4LldpXdot1LocVlanId) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpXdot1LocVlanNameTable returns failure!! \n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1LocVlanNameTable
 Input       :  The Indices
                LldpLocPortNum
                nextLldpLocPortNum
                LldpXdot1LocVlanId
                nextLldpXdot1LocVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1LocVlanNameTable (INT4 i4LldpLocPortNum,
                                          INT4 *pi4NextLldpLocPortNum,
                                          INT4 i4LldpXdot1LocVlanId,
                                          INT4 *pi4NextLldpXdot1LocVlanId)
{
    if (LldpUtlGetNextIndexLldpXdot1LocVlanNameTable (i4LldpLocPortNum,
                                                      pi4NextLldpLocPortNum,
                                                      i4LldpXdot1LocVlanId,
                                                      pi4NextLldpXdot1LocVlanId)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpXdot1LocVlanNameTable returns failure!! \n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1LocVlanName
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocVlanId

                The Object 
                retValLldpXdot1LocVlanName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1LocVlanName (INT4 i4LldpLocPortNum, INT4 i4LldpXdot1LocVlanId,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValLldpXdot1LocVlanName)
{
    if (LldpUtlGetLldpXdot1LocVlanName (i4LldpLocPortNum, i4LldpXdot1LocVlanId,
                                        pRetValLldpXdot1LocVlanName)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot1LocVlanName failed!! \n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1ConfigVlanNameTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1ConfigVlanNameTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1ConfigVlanNameTable (INT4 i4LldpLocPortNum,
                                                      INT4 i4LldpXdot1LocVlanId)
{
    if (LldpUtlValidateIndexInstanceLldpXdot1ConfigVlanNameTable
        (i4LldpLocPortNum, i4LldpXdot1LocVlanId) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot1ConfigVlanNameTable failed!! \n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1ConfigVlanNameTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1ConfigVlanNameTable (INT4 *pi4LldpLocPortNum,
                                              INT4 *pi4LldpXdot1LocVlanId)
{
    if (LldpUtlGetFirstIndexLldpXdot1ConfigVlanNameTable (pi4LldpLocPortNum,
                                                          pi4LldpXdot1LocVlanId)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpXdot1ConfigVlanNameTable failed!! \n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1ConfigVlanNameTable
 Input       :  The Indices
                LldpLocPortNum
                nextLldpLocPortNum
                LldpXdot1LocVlanId
                nextLldpXdot1LocVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1ConfigVlanNameTable (INT4 i4LldpLocPortNum,
                                             INT4 *pi4NextLldpLocPortNum,
                                             INT4 i4LldpXdot1LocVlanId,
                                             INT4 *pi4NextLldpXdot1LocVlanId)
{
    if (LldpUtlGetNextIndexLldpXdot1ConfigVlanNameTable (i4LldpLocPortNum,
                                                         pi4NextLldpLocPortNum,
                                                         i4LldpXdot1LocVlanId,
                                                         pi4NextLldpXdot1LocVlanId)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpXdot1ConfigVlanNameTable failed!! \n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1ConfigVlanNameTxEnable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocVlanId

                The Object 
                retValLldpXdot1ConfigVlanNameTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1ConfigVlanNameTxEnable (INT4 i4LldpLocPortNum,
                                       INT4 i4LldpXdot1LocVlanId,
                                       INT4
                                       *pi4RetValLldpXdot1ConfigVlanNameTxEnable)
{
    if (LldpUtlGetLldpXdot1ConfigVlanNameTxEnable (i4LldpLocPortNum,
                                                   i4LldpXdot1LocVlanId,
                                                   pi4RetValLldpXdot1ConfigVlanNameTxEnable)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot1ConfigVlanNameTxEnable failed!! \n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXdot1ConfigVlanNameTxEnable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocVlanId

                The Object 
                setValLldpXdot1ConfigVlanNameTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpXdot1ConfigVlanNameTxEnable (INT4 i4LldpLocPortNum,
                                       INT4 i4LldpXdot1LocVlanId,
                                       INT4
                                       i4SetValLldpXdot1ConfigVlanNameTxEnable)
{
    if (LldpUtlSetLldpXdot1ConfigVlanNameTxEnable (i4LldpLocPortNum,
                                                   i4LldpXdot1LocVlanId,
                                                   i4SetValLldpXdot1ConfigVlanNameTxEnable)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot1ConfigVlanNameTxEnable failed!! \n");
        return SNMP_FAILURE;
    }

    /*Sending Trigger for MSR */
    LLDP_MSR_NOTIFY_DOT1_CONFIG_VLAN_TBL (LldpV2Xdot1ConfigVlanNameTxEnable,
                                          (sizeof
                                           (LldpV2Xdot1ConfigVlanNameTxEnable)
                                           / sizeof (UINT4)), i4LldpLocPortNum,
                                          i4LldpXdot1LocVlanId,
                                          i4SetValLldpXdot1ConfigVlanNameTxEnable);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXdot1ConfigVlanNameTxEnable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocVlanId

                The Object 
                testValLldpXdot1ConfigVlanNameTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpXdot1ConfigVlanNameTxEnable (UINT4 *pu4ErrorCode,
                                          INT4 i4LldpLocPortNum,
                                          INT4 i4LldpXdot1LocVlanId,
                                          INT4
                                          i4TestValLldpXdot1ConfigVlanNameTxEnable)
{
    if (LldpUtlTestLldpXdot1ConfigVlanNameTxEnable (pu4ErrorCode,
                                                    i4LldpLocPortNum,
                                                    i4LldpXdot1LocVlanId,
                                                    i4TestValLldpXdot1ConfigVlanNameTxEnable)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlTestLldpXdot1ConfigVlanNameTxEnable returns FAILURE !!\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1LocProtoVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1LocProtoVlanTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocProtoVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1LocProtoVlanTable (INT4 i4LldpLocPortNum,
                                                    INT4
                                                    i4LldpXdot1LocProtoVlanId)
{
    if (LldpUtlValidateIndexInstanceLldpXdot1LocProtoVlanTable
        (i4LldpLocPortNum, i4LldpXdot1LocProtoVlanId) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot1LocProtoVlanTable failed !!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1LocProtoVlanTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocProtoVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1LocProtoVlanTable (INT4 *pi4LldpLocPortNum,
                                            INT4 *pi4LldpXdot1LocProtoVlanId)
{
    if (LldpUtlGetFirstIndexLldpXdot1LocProtoVlanTable (pi4LldpLocPortNum,
                                                        pi4LldpXdot1LocProtoVlanId)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpGetFirstIndexLldpXdot1LocProtoVlanTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1LocProtoVlanTable
 Input       :  The Indices
                LldpLocPortNum
                nextLldpLocPortNum
                LldpXdot1LocProtoVlanId
                nextLldpXdot1LocProtoVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1LocProtoVlanTable (INT4 i4LldpLocPortNum,
                                           INT4 *pi4NextLldpLocPortNum,
                                           INT4 i4LldpXdot1LocProtoVlanId,
                                           INT4 *pi4NextLldpXdot1LocProtoVlanId)
{
    if (LldpUtlGetNextIndexLldpXdot1LocProtoVlanTable (i4LldpLocPortNum,
                                                       pi4NextLldpLocPortNum,
                                                       i4LldpXdot1LocProtoVlanId,
                                                       pi4NextLldpXdot1LocProtoVlanId)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpGetNextIndexLldpXdot1LocProtoVlanTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1LocProtoVlanSupported
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocProtoVlanId

                The Object 
                retValLldpXdot1LocProtoVlanSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1LocProtoVlanSupported (INT4 i4LldpLocPortNum,
                                      INT4 i4LldpXdot1LocProtoVlanId,
                                      INT4
                                      *pi4RetValLldpXdot1LocProtoVlanSupported)
{
    LldpUtlGetLldpXdot1LocProtoVlanSupported (i4LldpLocPortNum,
                                              i4LldpXdot1LocProtoVlanId,
                                              pi4RetValLldpXdot1LocProtoVlanSupported);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXdot1LocProtoVlanEnabled
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocProtoVlanId

                The Object 
                retValLldpXdot1LocProtoVlanEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1LocProtoVlanEnabled (INT4 i4LldpLocPortNum,
                                    INT4 i4LldpXdot1LocProtoVlanId,
                                    INT4 *pi4RetValLldpXdot1LocProtoVlanEnabled)
{
    if (LldpUtlGetLldpXdot1LocProtoVlanEnabled
        (i4LldpLocPortNum, i4LldpXdot1LocProtoVlanId,
         pi4RetValLldpXdot1LocProtoVlanEnabled) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpGetFirstIndexLldpXdot1LocProtoVlanTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1ConfigProtoVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1ConfigProtoVlanTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocProtoVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1ConfigProtoVlanTable (INT4 i4LldpLocPortNum,
                                                       INT4
                                                       i4LldpXdot1LocProtoVlanId)
{
    if (LldpUtlValidateIndexInstanceLldpXdot1ConfigProtoVlanTable
        (i4LldpLocPortNum, i4LldpXdot1LocProtoVlanId) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot1ConfigProtoVlanTable failed !!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1ConfigProtoVlanTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocProtoVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1ConfigProtoVlanTable (INT4 *pi4LldpLocPortNum,
                                               INT4 *pi4LldpXdot1LocProtoVlanId)
{
    if (LldpUtlGetFirstIndexLldpXdot1ConfigProtoVlanTable (pi4LldpLocPortNum,
                                                           pi4LldpXdot1LocProtoVlanId)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpXdot1ConfigProtoVlanTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1ConfigProtoVlanTable
 Input       :  The Indices
                LldpLocPortNum
                nextLldpLocPortNum
                LldpXdot1LocProtoVlanId
                nextLldpXdot1LocProtoVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1ConfigProtoVlanTable (INT4 i4LldpLocPortNum,
                                              INT4 *pi4NextLldpLocPortNum,
                                              INT4 i4LldpXdot1LocProtoVlanId,
                                              INT4
                                              *pi4NextLldpXdot1LocProtoVlanId)
{
    if (LldpUtlGetNextIndexLldpXdot1ConfigProtoVlanTable
        (i4LldpLocPortNum, pi4NextLldpLocPortNum, i4LldpXdot1LocProtoVlanId,
         pi4NextLldpXdot1LocProtoVlanId) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD:LldpUtlGetNextIndexLldpXdot1ConfigProtoVlanTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1ConfigProtoVlanTxEnable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocProtoVlanId

                The Object 
                retValLldpXdot1ConfigProtoVlanTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1ConfigProtoVlanTxEnable (INT4 i4LldpLocPortNum,
                                        INT4 i4LldpXdot1LocProtoVlanId,
                                        INT4
                                        *pi4RetValLldpXdot1ConfigProtoVlanTxEnable)
{
    if (LldpUtlGetLldpXdot1ConfigProtoVlanTxEnable
        (i4LldpLocPortNum, i4LldpXdot1LocProtoVlanId,
         pi4RetValLldpXdot1ConfigProtoVlanTxEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD:LldpUtlGetLldpXdot1ConfigProtoVlanTxEnable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXdot1ConfigProtoVlanTxEnable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocProtoVlanId

                The Object 
                setValLldpXdot1ConfigProtoVlanTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpXdot1ConfigProtoVlanTxEnable (INT4 i4LldpLocPortNum,
                                        INT4 i4LldpXdot1LocProtoVlanId,
                                        INT4
                                        i4SetValLldpXdot1ConfigProtoVlanTxEnable)
{
    if (LldpUtlSetLldpXdot1ConfigProtoVlanTxEnable (i4LldpLocPortNum,
                                                    i4LldpXdot1LocProtoVlanId,
                                                    i4SetValLldpXdot1ConfigProtoVlanTxEnable)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD:LldpUtlSetLldpXdot1ConfigProtoVlanTxEnable Failed!!\n");
        return SNMP_FAILURE;
    }

    /*Sending Trigger for MSR */
    LLDP_MSR_NOTIFY_DOT1_CONFIG_PROTO_VLAN_TBL
        (LldpV2Xdot1ConfigProtoVlanTxEnable,
         (sizeof (LldpV2Xdot1ConfigProtoVlanTxEnable) / sizeof (UINT4)),
         i4LldpLocPortNum, i4LldpXdot1LocProtoVlanId,
         i4SetValLldpXdot1ConfigProtoVlanTxEnable);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXdot1ConfigProtoVlanTxEnable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocProtoVlanId

                The Object 
                testValLldpXdot1ConfigProtoVlanTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpXdot1ConfigProtoVlanTxEnable (UINT4 *pu4ErrorCode,
                                           INT4 i4LldpLocPortNum,
                                           INT4 i4LldpXdot1LocProtoVlanId,
                                           INT4
                                           i4TestValLldpXdot1ConfigProtoVlanTxEnable)
{
    if (LldpUtlTestv2LldpXdot1ConfigProtoVlanTxEnable
        (pu4ErrorCode, i4LldpLocPortNum, i4LldpXdot1LocProtoVlanId,
         i4TestValLldpXdot1ConfigProtoVlanTxEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD:LldpUtlTestv2LldpXdot1ConfigProtoVlanTxEnable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1LocProtocolTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1LocProtocolTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocProtocolIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceLldpXdot1LocProtocolTable
    (INT4 i4LldpLocPortNum, INT4 i4LldpXdot1LocProtocolIndex)
{
    /* Not supported in this release */
    UNUSED_PARAM (i4LldpLocPortNum);
    UNUSED_PARAM (i4LldpXdot1LocProtocolIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1LocProtocolTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocProtocolIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1LocProtocolTable (INT4 *pi4LldpLocPortNum,
                                           INT4 *pi4LldpXdot1LocProtocolIndex)
{
    /* Not supported in this release */
    UNUSED_PARAM (pi4LldpLocPortNum);
    UNUSED_PARAM (pi4LldpXdot1LocProtocolIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1LocProtocolTable
 Input       :  The Indices
                LldpLocPortNum
                nextLldpLocPortNum
                LldpXdot1LocProtocolIndex
                nextLldpXdot1LocProtocolIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexLldpXdot1LocProtocolTable
    (INT4 i4LldpLocPortNum,
     INT4 *pi4NextLldpLocPortNum,
     INT4 i4LldpXdot1LocProtocolIndex, INT4 *pi4NextLldpXdot1LocProtocolIndex)
{
    /* Not supported in this release */
    UNUSED_PARAM (i4LldpLocPortNum);
    UNUSED_PARAM (pi4NextLldpLocPortNum);
    UNUSED_PARAM (i4LldpXdot1LocProtocolIndex);
    UNUSED_PARAM (pi4NextLldpXdot1LocProtocolIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1LocProtocolId
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocProtocolIndex

                The Object 
                retValLldpXdot1LocProtocolId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetLldpXdot1LocProtocolId
    (INT4 i4LldpLocPortNum,
     INT4 i4LldpXdot1LocProtocolIndex,
     tSNMP_OCTET_STRING_TYPE * pRetValLldpXdot1LocProtocolId)
{
    /* Not supported in this release */
    UNUSED_PARAM (i4LldpLocPortNum);
    UNUSED_PARAM (i4LldpXdot1LocProtocolIndex);
    UNUSED_PARAM (pRetValLldpXdot1LocProtocolId);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : LldpXdot1ConfigProtocolTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1ConfigProtocolTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocProtocolIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceLldpXdot1ConfigProtocolTable
    (INT4 i4LldpLocPortNum, INT4 i4LldpXdot1LocProtocolIndex)
{
    /* Not supported in this release */
    UNUSED_PARAM (i4LldpLocPortNum);
    UNUSED_PARAM (i4LldpXdot1LocProtocolIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1ConfigProtocolTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocProtocolIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexLldpXdot1ConfigProtocolTable
    (INT4 *pi4LldpLocPortNum, INT4 *pi4LldpXdot1LocProtocolIndex)
{
    /* Not supported in this release */
    UNUSED_PARAM (pi4LldpLocPortNum);
    UNUSED_PARAM (pi4LldpXdot1LocProtocolIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1ConfigProtocolTable
 Input       :  The Indices
                LldpLocPortNum
                nextLldpLocPortNum
                LldpXdot1LocProtocolIndex
                nextLldpXdot1LocProtocolIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexLldpXdot1ConfigProtocolTable
    (INT4 i4LldpLocPortNum, INT4 *pi4NextLldpLocPortNum,
     INT4 i4LldpXdot1LocProtocolIndex, INT4 *pi4NextLldpXdot1LocProtocolIndex)
{
    /* Not supported in this release */
    UNUSED_PARAM (i4LldpLocPortNum);
    UNUSED_PARAM (pi4NextLldpLocPortNum);
    UNUSED_PARAM (i4LldpXdot1LocProtocolIndex);
    UNUSED_PARAM (pi4NextLldpXdot1LocProtocolIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1ConfigProtocolTxEnable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocProtocolIndex

                The Object 
                retValLldpXdot1ConfigProtocolTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetLldpXdot1ConfigProtocolTxEnable
    (INT4 i4LldpLocPortNum,
     INT4 i4LldpXdot1LocProtocolIndex,
     INT4 *pi4RetValLldpXdot1ConfigProtocolTxEnable)
{
    /* Not supported in this release */
    UNUSED_PARAM (i4LldpLocPortNum);
    UNUSED_PARAM (i4LldpXdot1LocProtocolIndex);
    UNUSED_PARAM (pi4RetValLldpXdot1ConfigProtocolTxEnable);
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXdot1ConfigProtocolTxEnable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocProtocolIndex

                The Object 
                setValLldpXdot1ConfigProtocolTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetLldpXdot1ConfigProtocolTxEnable
    (INT4 i4LldpLocPortNum,
     INT4 i4LldpXdot1LocProtocolIndex,
     INT4 i4SetValLldpXdot1ConfigProtocolTxEnable)
{
    /* Not supported in this release */
    UNUSED_PARAM (i4LldpLocPortNum);
    UNUSED_PARAM (i4LldpXdot1LocProtocolIndex);
    UNUSED_PARAM (i4SetValLldpXdot1ConfigProtocolTxEnable);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXdot1ConfigProtocolTxEnable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocProtocolIndex

                The Object 
                testValLldpXdot1ConfigProtocolTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2LldpXdot1ConfigProtocolTxEnable
    (UINT4 *pu4ErrorCode,
     INT4 i4LldpLocPortNum,
     INT4 i4LldpXdot1LocProtocolIndex,
     INT4 i4TestValLldpXdot1ConfigProtocolTxEnable)
{
    /* Not supported in this release */
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4LldpLocPortNum);
    UNUSED_PARAM (i4LldpXdot1LocProtocolIndex);
    UNUSED_PARAM (i4TestValLldpXdot1ConfigProtocolTxEnable);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : LldpXdot1LocTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1LocTable
 Input       :  The Indices
                LldpLocPortNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1LocTable (INT4 i4LldpLocPortNum)
{
    if (LldpUtlValidateIndexInstanceLldpXdot1LocTable (i4LldpLocPortNum) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot1LocTable failed !!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1LocTable
 Input       :  The Indices
                LldpLocPortNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1LocTable (INT4 *pi4LldpLocPortNum)
{
    if (LldpUtlGetFirstIndexLldpXdot1LocTable (pi4LldpLocPortNum))
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpXdot1LocTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1LocTable
 Input       :  The Indices
                LldpLocPortNum
                nextLldpLocPortNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1LocTable (INT4 i4LldpLocPortNum,
                                  INT4 *pi4NextLldpLocPortNum)
{
    if (LldpUtlGetNextIndexLldpXdot1LocTable
        (i4LldpLocPortNum, pi4NextLldpLocPortNum) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpXdot1LocTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1LocPortVlanId
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValLldpXdot1LocPortVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1LocPortVlanId (INT4 i4LldpLocPortNum,
                              INT4 *pi4RetValLldpXdot1LocPortVlanId)
{
    if (LldpUtlGetLldpXdot1LocPortVlanId (i4LldpLocPortNum,
                                          pi4RetValLldpXdot1LocPortVlanId) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot1LocPortVlanId Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1RemTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1RemTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1RemTable (UINT4 u4LldpRemTimeMark,
                                           INT4 i4LldpRemLocalPortNum,
                                           INT4 i4LldpRemIndex)
{
    if (LldpUtlValidateIndexInstanceLldpXdot1RemTable (u4LldpRemTimeMark,
                                                       i4LldpRemLocalPortNum,
                                                       i4LldpRemIndex,
                                                       (UINT4)
                                                       LLDP_V1_DEST_MAC_ADDR_INDEX
                                                       (i4LldpRemLocalPortNum))
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpXdot1RemTable Index validation failed !!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1RemTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1RemTable (UINT4 *pu4LldpRemTimeMark,
                                   INT4 *pi4LldpRemLocalPortNum,
                                   INT4 *pi4LldpRemIndex)
{
    if (LldpUtlGetFirstIndexLldpXdot1RemTable
        (pu4LldpRemTimeMark, pi4LldpRemLocalPortNum,
         pi4LldpRemIndex) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpXdot1RemTable failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1RemTable
 Input       :  The Indices
                LldpRemTimeMark
                nextLldpRemTimeMark
                LldpRemLocalPortNum
                nextLldpRemLocalPortNum
                LldpRemIndex
                nextLldpRemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1RemTable (UINT4 u4LldpRemTimeMark,
                                  UINT4 *pu4NextLldpRemTimeMark,
                                  INT4 i4LldpRemLocalPortNum,
                                  INT4 *pi4NextLldpRemLocalPortNum,
                                  INT4 i4LldpRemIndex,
                                  INT4 *pi4NextLldpRemIndex)
{

    if (LldpUtlGetNextIndexLldpXdot1RemTable
        (u4LldpRemTimeMark, pu4NextLldpRemTimeMark, i4LldpRemLocalPortNum,
         pi4NextLldpRemLocalPortNum, i4LldpRemIndex, pi4NextLldpRemIndex,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpXdot1RemTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1RemPortVlanId
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXdot1RemPortVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1RemPortVlanId (UINT4 u4LldpRemTimeMark,
                              INT4 i4LldpRemLocalPortNum,
                              INT4 i4LldpRemIndex,
                              INT4 *pi4RetValLldpXdot1RemPortVlanId)
{
    if (LldpUtlGetLldpXdot1RemPortVlanId
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         pi4RetValLldpXdot1RemPortVlanId,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot1RemPortVlanId Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1RemProtoVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1RemProtoVlanTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpXdot1RemProtoVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1RemProtoVlanTable (UINT4 u4LldpRemTimeMark,
                                                    INT4 i4LldpRemLocalPortNum,
                                                    INT4 i4LldpRemIndex,
                                                    INT4
                                                    i4LldpXdot1RemProtoVlanId)
{
    if (LldpUtlValidateIndexInstanceLldpXdot1RemProtoVlanTable
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         i4LldpXdot1RemProtoVlanId,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot1RemProtoVlanTable failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1RemProtoVlanTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpXdot1RemProtoVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1RemProtoVlanTable (UINT4 *pu4LldpRemTimeMark,
                                            INT4 *pi4LldpRemLocalPortNum,
                                            INT4 *pi4LldpRemIndex,
                                            INT4 *pi4LldpXdot1RemProtoVlanId)
{
    if (LldpUtlGetFirstIndexLldpXdot1RemProtoVlanTable (pu4LldpRemTimeMark,
                                                        pi4LldpRemLocalPortNum,
                                                        pi4LldpRemIndex,
                                                        pi4LldpXdot1RemProtoVlanId)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpXdot1RemProtoVlanTable Failed !!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1RemProtoVlanTable
 Input       :  The Indices
                LldpRemTimeMark
                nextLldpRemTimeMark
                LldpRemLocalPortNum
                nextLldpRemLocalPortNum
                LldpRemIndex
                nextLldpRemIndex
                LldpXdot1RemProtoVlanId
                nextLldpXdot1RemProtoVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1RemProtoVlanTable (UINT4 u4LldpRemTimeMark,
                                           UINT4 *pu4NextLldpRemTimeMark,
                                           INT4 i4LldpRemLocalPortNum,
                                           INT4 *pi4NextLldpRemLocalPortNum,
                                           INT4 i4LldpRemIndex,
                                           INT4 *pi4NextLldpRemIndex,
                                           INT4 i4LldpXdot1RemProtoVlanId,
                                           INT4 *pi4NextLldpXdot1RemProtoVlanId)
{
    if (LldpUtlGetNextIndexLldpXdot1RemProtoVlanTable
        (u4LldpRemTimeMark, pu4NextLldpRemTimeMark, i4LldpRemLocalPortNum,
         pi4NextLldpRemLocalPortNum, i4LldpRemIndex, pi4NextLldpRemIndex,
         i4LldpXdot1RemProtoVlanId, pi4NextLldpXdot1RemProtoVlanId,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpXdot1RemProtoVlanTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1RemProtoVlanSupported
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpXdot1RemProtoVlanId

                The Object 
                retValLldpXdot1RemProtoVlanSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1RemProtoVlanSupported (UINT4 u4LldpRemTimeMark,
                                      INT4 i4LldpRemLocalPortNum,
                                      INT4 i4LldpRemIndex,
                                      INT4 i4LldpXdot1RemProtoVlanId,
                                      INT4
                                      *pi4RetValLldpXdot1RemProtoVlanSupported)
{
    if (LldpUtlGetLldpXdot1RemProtoVlanSupported
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         i4LldpXdot1RemProtoVlanId, pi4RetValLldpXdot1RemProtoVlanSupported,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)

    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot1RemProtoVlanSupported Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXdot1RemProtoVlanEnabled
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpXdot1RemProtoVlanId

                The Object 
                retValLldpXdot1RemProtoVlanEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1RemProtoVlanEnabled (UINT4 u4LldpRemTimeMark,
                                    INT4 i4LldpRemLocalPortNum,
                                    INT4 i4LldpRemIndex,
                                    INT4 i4LldpXdot1RemProtoVlanId,
                                    INT4 *pi4RetValLldpXdot1RemProtoVlanEnabled)
{
    if (LldpUtlGetLldpXdot1RemProtoVlanEnabled (u4LldpRemTimeMark,
                                                i4LldpRemLocalPortNum,
                                                i4LldpRemIndex,
                                                i4LldpXdot1RemProtoVlanId,
                                                pi4RetValLldpXdot1RemProtoVlanEnabled,
                                                LLDP_V1_DEST_MAC_ADDR_INDEX
                                                (i4LldpRemLocalPortNum)) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot1RemProtoVlanEnabled Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1RemVlanNameTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1RemVlanNameTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpXdot1RemVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1RemVlanNameTable (UINT4 u4LldpRemTimeMark,
                                                   INT4 i4LldpRemLocalPortNum,
                                                   INT4 i4LldpRemIndex,
                                                   INT4 i4LldpXdot1RemVlanId)
{
    if (LldpUtlValidateIndexInstanceLldpXdot1RemVlanNameTable
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         i4LldpXdot1RemVlanId,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot1RemVlanNameTable Failed !!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1RemVlanNameTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpXdot1RemVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1RemVlanNameTable (UINT4 *pu4LldpRemTimeMark,
                                           INT4 *pi4LldpRemLocalPortNum,
                                           INT4 *pi4LldpRemIndex,
                                           INT4 *pi4LldpXdot1RemVlanId)
{
    if (LldpUtlGetFirstIndexLldpXdot1RemVlanNameTable
        (pu4LldpRemTimeMark, pi4LldpRemLocalPortNum, pi4LldpRemIndex,
         pi4LldpXdot1RemVlanId) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpXdot1RemVlanNameTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1RemVlanNameTable
 Input       :  The Indices
                LldpRemTimeMark
                nextLldpRemTimeMark
                LldpRemLocalPortNum
                nextLldpRemLocalPortNum
                LldpRemIndex
                nextLldpRemIndex
                LldpXdot1RemVlanId
                nextLldpXdot1RemVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1RemVlanNameTable (UINT4 u4LldpRemTimeMark,
                                          UINT4 *pu4NextLldpRemTimeMark,
                                          INT4 i4LldpRemLocalPortNum,
                                          INT4 *pi4NextLldpRemLocalPortNum,
                                          INT4 i4LldpRemIndex,
                                          INT4 *pi4NextLldpRemIndex,
                                          INT4 i4LldpXdot1RemVlanId,
                                          INT4 *pi4NextLldpXdot1RemVlanId)
{
    if (LldpUtlGetNextIndexLldpXdot1RemVlanNameTable
        (u4LldpRemTimeMark, pu4NextLldpRemTimeMark, i4LldpRemLocalPortNum,
         pi4NextLldpRemLocalPortNum, i4LldpRemIndex, pi4NextLldpRemIndex,
         i4LldpXdot1RemVlanId, pi4NextLldpXdot1RemVlanId,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpXdot1RemVlanNameTable Failed!!\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1RemVlanName
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpXdot1RemVlanId

                The Object 
                retValLldpXdot1RemVlanName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1RemVlanName (UINT4 u4LldpRemTimeMark,
                            INT4 i4LldpRemLocalPortNum,
                            INT4 i4LldpRemIndex,
                            INT4 i4LldpXdot1RemVlanId,
                            tSNMP_OCTET_STRING_TYPE
                            * pRetValLldpXdot1RemVlanName)
{
    if (LldpUtlGetLldpXdot1RemVlanName (u4LldpRemTimeMark,
                                        i4LldpRemLocalPortNum,
                                        i4LldpRemIndex,
                                        i4LldpXdot1RemVlanId,
                                        pRetValLldpXdot1RemVlanName,
                                        LLDP_V1_DEST_MAC_ADDR_INDEX
                                        (i4LldpRemLocalPortNum)) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot1RemVlanName Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1RemProtocolTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1RemProtocolTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpXdot1RemProtocolIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1RemProtocolTable (UINT4 u4LldpRemTimeMark,
                                                   INT4 i4LldpRemLocalPortNum,
                                                   INT4 i4LldpRemIndex,
                                                   INT4
                                                   i4LldpXdot1RemProtocolIndex)
{
    if (LldpUtlValidateIndexInstanceLldpXdot1RemProtocolTable
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         i4LldpXdot1RemProtocolIndex,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpXdot1RemProtocolTable failed !!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1RemProtocolTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpXdot1RemProtocolIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1RemProtocolTable (UINT4 *pu4LldpRemTimeMark,
                                           INT4 *pi4LldpRemLocalPortNum,
                                           INT4 *pi4LldpRemIndex,
                                           INT4 *pi4LldpXdot1RemProtocolIndex)
{
    if (LldpUtlGetFirstIndexLldpXdot1RemProtocolTable
        (pu4LldpRemTimeMark, pi4LldpRemLocalPortNum, pi4LldpRemIndex,
         pi4LldpXdot1RemProtocolIndex) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpXdot1RemProtocolTable Failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1RemProtocolTable
 Input       :  The Indices
                LldpRemTimeMark
                nextLldpRemTimeMark
                LldpRemLocalPortNum
                nextLldpRemLocalPortNum
                LldpRemIndex
                nextLldpRemIndex
                LldpXdot1RemProtocolIndex
                nextLldpXdot1RemProtocolIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1RemProtocolTable (UINT4 u4LldpRemTimeMark,
                                          UINT4 *pu4NextLldpRemTimeMark,
                                          INT4 i4LldpRemLocalPortNum,
                                          INT4 *pi4NextLldpRemLocalPortNum,
                                          INT4 i4LldpRemIndex,
                                          INT4 *pi4NextLldpRemIndex,
                                          INT4 i4LldpXdot1RemProtocolIndex,
                                          INT4
                                          *pi4NextLldpXdot1RemProtocolIndex)
{
    if (LldpUtlGetNextIndexLldpXdot1RemProtocolTable
        (u4LldpRemTimeMark, pu4NextLldpRemTimeMark, i4LldpRemLocalPortNum,
         pi4NextLldpRemLocalPortNum, i4LldpRemIndex, pi4NextLldpRemIndex,
         i4LldpXdot1RemProtocolIndex, pi4NextLldpXdot1RemProtocolIndex,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpXdot1RemProtocolTable failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1RemProtocolId
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpXdot1RemProtocolIndex

                The Object 
                retValLldpXdot1RemProtocolId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1RemProtocolId (UINT4 u4LldpRemTimeMark,
                              INT4 i4LldpRemLocalPortNum,
                              INT4 i4LldpRemIndex,
                              INT4 i4LldpXdot1RemProtocolIndex,
                              tSNMP_OCTET_STRING_TYPE
                              * pRetValLldpXdot1RemProtocolId)
{
    if (LldpUtlGetLldpXdot1RemProtocolId (u4LldpRemTimeMark,
                                          i4LldpRemLocalPortNum,
                                          i4LldpRemIndex,
                                          i4LldpXdot1RemProtocolIndex,
                                          pRetValLldpXdot1RemProtocolId,
                                          LLDP_V1_DEST_MAC_ADDR_INDEX
                                          (i4LldpRemLocalPortNum)) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLldpXdot1RemProtocolId failed!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpXdot1ConfigPortVlanTable
 Input       :  The Indices
                LldpPortConfigPortNum
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXdot1ConfigPortVlanTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpXdot1ConfigVlanNameTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocVlanId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXdot1ConfigVlanNameTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpXdot1ConfigProtoVlanTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocProtoVlanId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXdot1ConfigProtoVlanTable (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpXdot1ConfigProtocolTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXdot1LocProtocolIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXdot1ConfigProtocolTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
