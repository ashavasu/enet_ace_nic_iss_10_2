/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldtxsm.c,v 1.32 2017/12/26 11:05:46 siva Exp $
 *
 * Description: This file contains LLDP SEM related functions
 *****************************************************************************/
#include "lldinc.h"
#include "lldtxsm.h"

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxSmMachine
 *
 *    DESCRIPTION      : Entry point to LLDP Transmit State Event Machine.
 *                       This function calls the appropriate Tx state routine
 *                       based on the current state and event received.
 *                       Tx State-Event Machine is running on per port
 *                       basis. So current state of a port is maintained in
 *                       the Local Port Info structure.
 *
 *    INPUT            : pPortInfo - pointer to port information structure
 *                       u1Event - Event     
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PUBLIC VOID
LldpTxSmMachine (tLldpLocPortInfo * pPortInfo, UINT1 u1Event)
{
    UINT1               u1ActionProcIdx = 0;
    UINT1               u1CurrentState = 0;

    /* get the current state */
    u1CurrentState = (UINT1) pPortInfo->i4TxSemCurrentState;
    /* Get the index of the action procedure */
    if ((u1Event < LLDP_TX_MAX_EVENTS) &&
        (u1CurrentState <= LLDP_TX_MAX_STATES))
    {
        if (u1CurrentState != 0)
        {
            u1ActionProcIdx = gau1LldpTxSem[u1Event][u1CurrentState - 1];

            LLDP_TRC_ARG3 (CONTROL_PLANE_TRC,
                           "TX-SEM[%d], STATE: %s, EVENT: %s\r\n",
                           pPortInfo->u4LocPortNum,
                           gau1LldpTxStateStr[u1CurrentState - 1],
                           gau1LldpTxEvntStr[u1Event]);

            if (u1ActionProcIdx < LLDP_MAX_TXSEM_FN_PTRS)
            {
                /* Call corresponding function pointer */
                (*gaLldpTxActionProc[u1ActionProcIdx]) (pPortInfo);
            }
        }
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxSmEventIgnore
 *
 *    DESCRIPTION      : Handle Invalid event received
 *
 *    INPUT            : pPortInfo - Port on which this event has occured
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PRIVATE VOID
LldpTxSmEventIgnore (tLldpLocPortInfo * pPortInfo)
{
    LLDP_TRC_ARG1 (INIT_SHUT_TRC | CONTROL_PLANE_TRC,
                   "TX-SEM[%d]: Event Impossible.\r\n",
                   pPortInfo->u4LocPortNum);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxSmStateLldpInit
 *
 *    DESCRIPTION      : LLDP Tx module Initialization
 *
 *    INPUT            : pPortInfo - Port on which this event has occured
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PRIVATE VOID
LldpTxSmStateLldpInit (tLldpLocPortInfo * pPortInfo)
{
    /* mark current state as LLDP_TX_INIT */
    pPortInfo->i4TxSemCurrentState = LLDP_TX_INIT;
    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                   "TX-SEM[%d]: State = LLDP_TX_INIT.\r\n",
                   pPortInfo->u4LocPortNum);

    /* If the lldp admin status is TX or TXRX, then transit to idle state */
    if (((pPortInfo->PortConfigTable.i4AdminStatus == LLDP_ADM_STAT_TX_ONLY) ||
         (pPortInfo->PortConfigTable.i4AdminStatus == LLDP_ADM_STAT_TX_AND_RX))
        && (pPortInfo->u1OperStatus == CFA_IF_UP) &&
        (gLldpGlobalInfo.u1ModuleStatus == LLDP_ENABLED))
    {
        LldpTxSmMachine (pPortInfo, (UINT1) LLDP_TX_EV_TXMOD_ADMIN_UP);
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxSmStateIdle
 *
 *    DESCRIPTION      : LLDP Tx module Idle state
 *
 *    INPUT            : pPortInfo - Port on which this event has occured
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PRIVATE VOID
LldpTxSmStateIdle (tLldpLocPortInfo * pPortInfo)
{
#define LLDP_NUM_OF_MILLISEC_PER_STUP 10

    UINT4               u4MsgInterval = 0;
    UINT4               u4TxDelay = 0;
    UINT4               u4MilliSec = 0;
    UINT4               u4JitteredMsgInterval = 0;
    /* mark current state as LLDP_TX_IDLE */
    pPortInfo->i4TxSemCurrentState = LLDP_TX_IDLE;
    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                   "TX-SEM[%d]: State = LLDP_TX_IDLE.\r\n",
                   pPortInfo->u4LocPortNum);
#ifdef L2RED_WANTED
    /* Timer can be started only in active node
     * So check the node status. If the node status
     * is not Active then return */
    if (LLDP_RED_NODE_STATUS () != RM_ACTIVE)
    {
        return;
    }
#endif

    /* set the transmit delay timer status as NOT_RUNNING */
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
    {
        pPortInfo->TxDelayTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;
    }
    /* Configuration of LLDP is allowed even if the LLDP module is in
     * Disabled state. So if user configures(local system information changed)
     * bSomethingChangedLocal is set to OSIX_TRUE, and if the user enables LLDP 
     * module(TxAdmin UP event will be posted to LLDP, which inturn will call
     * this function LldpTxSmStateIdle), so if we bSomethingChangedLocal set 
     * to OSIX_FALSE the locally modified value will not be transmitted.
     * Hence bSomethingChangedLocal should not be reset here.*/

    /*u4MsgInterval = pPortInfo->u4MsgInterval; */
    /*u4MsgInterval = gLldpGlobalInfo.SysConfigInfo.i4MsgTxInterval; */

    if ((gLldpGlobalInfo.i4Version == LLDP_VERSION_09) &&
        ((pPortInfo->u1TxFast > 0)))
    {
        u4MsgInterval = gLldpGlobalInfo.SysConfigInfo.u4MessageFastTx;
    }
    else                        /*if (pPortInfo->u1TxFast == 0) */
    {
        u4MsgInterval = gLldpGlobalInfo.SysConfigInfo.i4MsgTxInterval;
    }
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_09)
    {
        u4TxDelay = LLDP_ONE;
    }
    else
    {
        u4TxDelay = (UINT4) gLldpGlobalInfo.SysConfigInfo.i4TxDelay;
    }
    /* staggering implementation */
    /* when starting timer for the first time we should use jitter and 
     * we should use jittered msg interval. Timer API TmrStartTimer is 
     * used for setting interval in ticks(not seconds), so that the expiry of 
     * the timer will be disatributed across interval */
    if (pPortInfo->bMsgIntvalJittered == OSIX_FALSE)
    {
        pPortInfo->bMsgIntvalJittered = OSIX_TRUE;
        /* get jitterd message interval */
        u4JitteredMsgInterval = LldpTxUtlJitter ((u4MsgInterval *
                                                  SYS_NUM_OF_TIME_UNITS_IN_A_SEC),
                                                 (UINT4) LLDP_JITTER_PERCENT);
        /* now u4MsgInterval = jittered message interval in STUPS */
        /* get the remaining stups(stups in the range 1 to 99 will not be 
         * converted in sec, because 1sec = 100 stups) and convert the 
         * remaining stups to milli sec */
        u4MilliSec = u4JitteredMsgInterval % SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
        /* 1 stup = 10 millisec */
        u4MilliSec = u4MilliSec * LLDP_NUM_OF_MILLISEC_PER_STUP;
        /* convert u4MsgInterval to seconds */
        u4MsgInterval = u4JitteredMsgInterval / SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    }
    /* start Message Interval Timer */
    if (TmrStart (gLldpGlobalInfo.TmrListId, &(pPortInfo->TtrTmr),
                  LLDP_TX_TMR_TTR, u4MsgInterval, u4MilliSec) != TMR_SUCCESS)
    {
        LLDP_TRC_ARG1 (OS_RESOURCE_TRC | ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                       "TX-SEM[%d]: TtrTmr Start Timer FAILED !!!\r\n",
                       pPortInfo->u4LocPortNum);
    }
    /* set the transmit interval timer(TtrTmr) status as
     * RUNNING */
    pPortInfo->TtrTmrNode.u1Status = LLDP_TMR_RUNNING;
    /* start TxDelay timer */
    u4MilliSec = 0;
    if (pPortInfo->TxDelayTmrNode.u1Status != LLDP_TMR_RUNNING)
    {
        if (TmrStart (gLldpGlobalInfo.TmrListId, &(pPortInfo->TxDelayTmr),
                      LLDP_TX_TMR_TX_DELAY, u4TxDelay,
                      u4MilliSec) != TMR_SUCCESS)
        {
            LLDP_TRC_ARG1 (OS_RESOURCE_TRC | ALL_FAILURE_TRC |
                           CONTROL_PLANE_TRC,
                           "TX-SEM[%d]: TxDelayTmr Start Timer FAILED !!!\r\n",
                           pPortInfo->u4LocPortNum);
        }
        /* set the transmit delay timer(TxDelay) status as
         * RUNNING */
        pPortInfo->TxDelayTmrNode.u1Status = LLDP_TMR_RUNNING;
    }
    if ((pPortInfo->bTxNow == OSIX_TRUE) && (pPortInfo->u1Credit > 0))
    {
        LldpTxSmMachine (pPortInfo, LLDP_TX_EV_TX_NOW);
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxSmStateShutFrame
 *
 *    DESCRIPTION      : LLDP Tx module - transmission of shutdown frame
 *
 *    INPUT            : pPortInfo - Port on which this event has occured
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PRIVATE VOID
LldpTxSmStateShutFrame (tLldpLocPortInfo * pPortInfo)
{
    UINT4               u4ReInitDelay = 0;
    UINT4               u4MilliSec = 0;
    UINT1               u1OperStatus = 0;
    tLldpLocPortInfo    portInfo;    /* port info used to transmit 
                                       the shut down frame */

#ifdef L2RED_WANTED
    tLldpRedTmrSyncUpInfo TmrSyncUpInfo;

    MEMSET (&TmrSyncUpInfo, 0, sizeof (tLldpRedTmrSyncUpInfo));
#endif
    /* memset the port info */
    MEMSET (&portInfo, 0, sizeof (tLldpLocPortInfo));

    /* mark current state as LLDP_TX_SHUT_FRAME */
    pPortInfo->i4TxSemCurrentState = LLDP_TX_SHUT_FRAME;

    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                   "TX-SEM[%d]: State = LLDP_TX_SHUT_FRAME\r\n",
                   pPortInfo->u4LocPortNum);
    u4ReInitDelay = (UINT4) gLldpGlobalInfo.SysConfigInfo.i4ReInitDelay;
    if (pPortInfo->TtrTmrNode.u1Status == LLDP_TMR_RUNNING)
    {
        if (TmrStopTimer (gLldpGlobalInfo.TmrListId,
                          &(pPortInfo->TtrTmr.TimerNode)) != TMR_SUCCESS)
        {
            LLDP_TRC_ARG1 (OS_RESOURCE_TRC | ALL_FAILURE_TRC
                           | CONTROL_PLANE_TRC,
                           "TX-SEM[%d]: In LldpTxSmStateShutFrame "
                           "TmrStopTimer(TtrTmr) returns FAILURE!!!\r\n",
                           pPortInfo->u4LocPortNum);
        }
        /* set the transmit interval timer status as NOT_RUNNING */
        pPortInfo->TtrTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;
    }

    /* stop transmit delay timer only if timer is running */
    if (pPortInfo->TxDelayTmrNode.u1Status == LLDP_TMR_RUNNING)
    {

        if (TmrStopTimer (gLldpGlobalInfo.TmrListId,
                          &(pPortInfo->TxDelayTmr.TimerNode)) != TMR_SUCCESS)
        {
            LLDP_TRC_ARG1 (OS_RESOURCE_TRC | ALL_FAILURE_TRC
                           | CONTROL_PLANE_TRC,
                           "TX-SEM[%d]: In LldpTxSmStateShutFrame "
                           "TmrStopTimer(TxDelayTmr) returns FAILURE!!!\r\n",
                           pPortInfo->u4LocPortNum);
        }
        /* set the transmit delay timer status as NOT_RUNNING */
        pPortInfo->TxDelayTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;
    }

    /* In Standby node, whenever TxSem receivs any of the following events
     * 1. TxAdmin is DOWN(RxOnly/Disabled) (or)
     * 2. Module status is Disabled (or)
     * 3. System control is Shutdwon
     * TxSem is called with TxAdminDown event(in turn transits to 
     * SHUT_FRAME state). 
     * Incase of LLDP system control shutdown, the local port information 
     * strucutre is cleared and the correspnding memory is released(in the 
     * global shutdown timer expiry event). In all other cases, TxSem transits 
     * to INIT state directly. 
     * */

    /* If SHUT_FRAME state transition is not because of System control shutdown
     * then move to INIT state directly */
#ifdef L2RED_WANTED
    if (LLDP_RED_NODE_STATUS () == RM_STANDBY)
    {
        if (LLDP_SYSTEM_CONTROL () != LLDP_SHUTDOWN_INPROGRESS)
        {
            LldpTxSmMachine (pPortInfo, LLDP_TX_EV_REINIT_DELAY);
        }
        else if (LLDP_SYSTEM_CONTROL () == LLDP_SHUTDOWN_INPROGRESS)
        {
            if (LldpTxUtlClearPortInfo (pPortInfo) != OSIX_SUCCESS)
            {
                LLDP_TRC (INIT_SHUT_TRC, "LldpIfCreate: "
                          "LldpTxUtlClearPortInfo returns FAILURE!!!\r\n");
            }
            /* Whenever LLDP is shutdown LLDP TxSEM sends shutdown frame and
             * starts re-init timer. On the expiry of the re-init delay timer 
             * De-Registers with RM. But in standby node 
             * before receiving re-init delay timer start sync up from active
             * node, it De-Registers with RM, because of this the timer start
             * sync up sent by active node is not processed in standby node, and
             * the sub sequent re-init timer expiry function sync up message 
             * also not processed in standby node. To avoid standby node 
             * getting De-Registered with RM before receiving re-init delay 
             * timer sync up message, set the flag bGlobShutWhileTmrSyncUpRvcd
             * to TRUE whenever LLDP TxSEM hits SHUT_FRAME state */
            gLldpRedGlobalInfo.bGlobShutWhileTmrSyncUpRvcd = OSIX_TRUE;
        }
        return;
    }
#endif

    /* Reset txdelay timer status as LLDP_TIMER_NOT_RUNNING before moving 
     * to INIT_STATE */
    pPortInfo->TxDelayTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;

    /* Transmit shutdown frame, even if no information frames are transmitted 
     * from this port(ie. even if u4TxFramesTotal == 0). And this doesn't 
     * affect the protocol functionality, because whenever a shutdown frame is 
     * received for a remote entry, which is not present in the remote table 
     * the received shutdown frame is discarded(but frames discarded counter is 
     * not incremented) */

    /* Copy the contents of the pPortInfo to portInfo. This is because
     * shutdown frame should be sent separately. If pre formed buffer is
     * constructed again and is overwritten, then the buffer will not be
     * re-constructed after the admin is enabled since local variables are
     * not affected. So, the shutdown frame is sent separately. */
    portInfo.i4IfIndex = pPortInfo->i4IfIndex;
    MEMCPY (portInfo.PortConfigTable.u1DstMac,
            pPortInfo->PortConfigTable.u1DstMac, MAC_ADDR_LEN);
    portInfo.u4LocPortNum = pPortInfo->u4LocPortNum;
    portInfo.u1ManAddrTlvTxEnabled = pPortInfo->u1ManAddrTlvTxEnabled;

    /* transmit the shutdown frame 
     * The frame should be transmitted only when the port is oper up. */
    if ((LldpPortGetIfOperStatus (portInfo.i4IfIndex,
                                  &u1OperStatus) == OSIX_SUCCESS) &&
        (u1OperStatus == CFA_IF_UP))
    {
        portInfo.pPreFormedLldpdu =
            (UINT1 *) MemAllocMemBlk (gLldpGlobalInfo.LldpPduInfoPoolId);
        if (portInfo.pPreFormedLldpdu == NULL)
        {
            LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                           "In LldpTxSmStateShutFrame: Memory Allocation failed "
                           "for PreFormedLldpdu for Port: %d\r\n",
                           portInfo.i4IfIndex);
            return;
        }
        portInfo.u4PreFormedLldpduLen = 0;

        if (LldpTxUtlConstructPreformedBuf (&portInfo, LLDP_SHUTDOWN_FRAME)
            != OSIX_SUCCESS)
        {
            LLDP_TRC_ARG1 (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           "TX-SEM[%d]: In LldpTxSmStateShutFrame "
                           "Construction of preformed buffer Failed"
                           "\r\n", portInfo.u4LocPortNum);
            MemReleaseMemBlock (gLldpGlobalInfo.LldpPduInfoPoolId,
                                portInfo.pPreFormedLldpdu);
            LldpTxSmStateLldpInit (pPortInfo);
            return;
        }
        if (LldpTxFrame (&portInfo) != OSIX_SUCCESS)
        {
            LLDP_TRC_ARG1 (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           "TX-SEM[%d]: In LldpTxSmStateShutFrame "
                           "LldpTxFrame returns FAILURE!!!\r\n",
                           portInfo.u4LocPortNum);
            MemReleaseMemBlock (gLldpGlobalInfo.LldpPduInfoPoolId,
                                portInfo.pPreFormedLldpdu);
            LldpTxSmStateLldpInit (pPortInfo);
            return;
        }
        MemReleaseMemBlock (gLldpGlobalInfo.LldpPduInfoPoolId,
                            portInfo.pPreFormedLldpdu);
        LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                       "TX-SEM[%d]: LldpTxFrame Shutdown frame transmitted "
                       "successfully\r\n", portInfo.u4LocPortNum);
    }
    else
    {

        /* Since the port is administratively down, transit to init
         * state without transmitting the packet */
        /* Call state event machine(Transit to LLDP_TX_INIT state) */
        LldpTxSmMachine (pPortInfo, LLDP_TX_EV_REINIT_DELAY);
        return;
    }

    /* ShutWhile timer should not be started if shutframe is called as a
     * part of disabling the module. The global shutWhile should be started.
     * This timer should be started only once and not for all ports. */

    u4MilliSec = 0;
    if (LLDP_MODULE_STATUS () == LLDP_DISABLED)
    {

        /* Start the Global Re-Init Timer
         * The timer should be started if the flag is set. If the flag is
         * not set, then there is no port in idle state. In this case,
         * shutdown the module and free the memory */

        if (LLDP_GLOB_SHUT_WHILE_TMR_STATUS () == LLDP_TMR_NOT_RUNNING)
        {
            if (TmrStart (gLldpGlobalInfo.TmrListId,
                          &(gLldpGlobalInfo.GlobalShutWhileTmr),
                          LLDP_TX_TMR_GLOBAL_SHUT_WHILE,
                          u4ReInitDelay, u4MilliSec) != TMR_SUCCESS)
            {
                LLDP_TRC_ARG1 (OS_RESOURCE_TRC | ALL_FAILURE_TRC
                               | CONTROL_PLANE_TRC,
                               "TX-SEM[%d]: Global ShutWhileTmr Start "
                               "Timer FAILED !!!\r\n", pPortInfo->u4LocPortNum);
                LldpTxSmStateLldpInit (pPortInfo);
                return;
            }
            LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                           "TX-SEM[%d]: Global ShutWhileTmr Started"
                           " successfully\r\n", pPortInfo->u4LocPortNum);

            LLDP_GLOB_SHUT_WHILE_TMR_STATUS () = LLDP_TMR_RUNNING;

#ifdef L2RED_WANTED
            /* Fill global shutwhile timer start sync up information */
            TmrSyncUpInfo.u1TmrType = (UINT1) LLDP_TX_TMR_GLOBAL_SHUT_WHILE;
            TmrSyncUpInfo.u4TmrInterval =
                (UINT4) gLldpGlobalInfo.SysConfigInfo.i4ReInitDelay;
            TmrSyncUpInfo.pMsapRBIndex = NULL;
            /* Send global shutwhile timer start sync up information */
            LldpRedSendSyncUpMsg ((UINT1) LLDP_RED_TMR_START_MSG,
                                  (VOID *) &TmrSyncUpInfo);
#endif

            /* Remove the port specific info from port table
             * if module is shut down */
            if (LLDP_SYSTEM_CONTROL () == LLDP_SHUTDOWN_INPROGRESS)
            {
                if (LldpTxUtlClearPortInfo (pPortInfo) != OSIX_SUCCESS)
                {
                    LLDP_TRC (INIT_SHUT_TRC, "LldpIfCreate: "
                              "LldpTxUtlClearPortInfo returns FAILURE!!!\r\n");
                }
            }
        }
    }
    else
    {
        /* start Re-Init Delay/ShutWhile Timer */
        if (TmrStart (gLldpGlobalInfo.TmrListId, &(pPortInfo->ShutWhileTmr),
                      LLDP_TX_TMR_SHUT_WHILE, u4ReInitDelay, u4MilliSec)
            != TMR_SUCCESS)
        {
            LLDP_TRC_ARG1 (OS_RESOURCE_TRC | ALL_FAILURE_TRC
                           | CONTROL_PLANE_TRC,
                           "TX-SEM[%d]: ShutWhileTmr Start "
                           "Timer FAILED !!!\r\n", pPortInfo->u4LocPortNum);
            LldpTxSmStateLldpInit (pPortInfo);
            return;
        }
        pPortInfo->ShutWhileTmrNode.u1Status = LLDP_TMR_RUNNING;
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxSmStateInfoFrame
 *
 *    DESCRIPTION      : LLDP Tx module - transmission of information frame
 *
 *    INPUT            : pPortInfo - Port on which this event has occured
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PRIVATE VOID
LldpTxSmStateInfoFrame (tLldpLocPortInfo * pPortInfo)
{
    /* mark current state as LLDP_TX_INFO_FRAME */
    pPortInfo->i4TxSemCurrentState = LLDP_TX_INFO_FRAME;
    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                   "TX-SEM[%d]: State = LLDP_TX_INFO_FRAME\r\n",
                   pPortInfo->u4LocPortNum);

    /* we need stop msg interval timer or/and txdelay timer 
     * but this scenario will never occur, because timers are stopped in the 
     * timer expiry handlers if the other timer is running */

    /* transmit the info frame */
#ifdef L2RED_WANTED
    if (LLDP_RED_NODE_STATUS () != RM_ACTIVE)
    {
        pPortInfo->bTxNow = OSIX_FALSE;
        return;
    }
#endif
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_09)
    {
        /*Decrement (txCredit); */
        if (pPortInfo->u1Credit > 0)
        {
            if (LldpTxFrame (pPortInfo) != OSIX_SUCCESS)
            {
                LLDP_TRC_ARG1 (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                               "TX-SEM[%d]: LldpTxFrame returns FAILURE\r\n",
                               pPortInfo->u4LocPortNum);
            }
            pPortInfo->u1Credit--;
        }
        else
        {
            /* Not enough Credit left to Send frame.
             * Frame will be sent when credit is incremented after tick */
            pPortInfo->bSomethingChangedLocal = OSIX_TRUE;
        }
    }
    else
    {
        if (LldpTxFrame (pPortInfo) != OSIX_SUCCESS)
        {
            LLDP_TRC_ARG1 (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           "TX-SEM[%d]: LldpTxFrame returns FAILURE\r\n",
                           pPortInfo->u4LocPortNum);
        }
    }
    pPortInfo->bTxNow = OSIX_FALSE;
    /* UCT */
    LldpTxSmStateIdle (pPortInfo);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxTmrSmMachine
 *
 *    DESCRIPTION      : Entry point to LLDP Transmit Timer State Event Machine.
 *                       This function calls the appropriate Tx timer state routine
 *                       based on the current state and event received.
 *                       Tx timer State-Event Machine is running on per port
 *                       basis. So current state of a port is maintained in
 *                       the Local Port Info structure.
 *
 *    INPUT            : pPortInfo - pointer to port information structure
 *                       u1Event - Event     
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PUBLIC VOID
LldpTxTmrSmMachine (tLldpLocPortInfo * pPortInfo, UINT1 u1Event)
{
    UINT1               u1ActionProcIdx = 0;
    UINT1               u1CurrentState = 0;

    /* get the current state */
    u1CurrentState = (UINT1) pPortInfo->i4TxTimerSemCurrentState;
    /* Get the index of the action procedure */
    if ((u1Event < LLDP_TX_TMR_MAX_EVENTS) &&
        (u1CurrentState < LLDP_TX_TMR_MAX_STATES))
    {
        if (u1CurrentState != 0)
        {
            u1ActionProcIdx = gau1LldpTxTmrSem[u1Event][u1CurrentState - 1];

            LLDP_TRC_ARG3 (CONTROL_PLANE_TRC,
                           "TX-Timer-SEM[%d], STATE: %s, EVENT: %s\r\n",
                           pPortInfo->u4LocPortNum,
                           gau1LldpTxTmrStateStr[u1CurrentState - 1],
                           gau1LldpTxTmrEvntStr[u1Event]);

            if (u1ActionProcIdx < LLDP_MAX_TXTMRSEM_FN_PTRS)
            {
                /* Call corresponding function pointer */
                (*gaLldpTxTmrActionProc[u1ActionProcIdx]) (pPortInfo);
            }
        }
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxTmrSmEventIgnore
 *
 *    DESCRIPTION      : Handle Invalid event received
 *
 *    INPUT            : pPortInfo - Port on which this event has occured
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PRIVATE VOID
LldpTxTmrSmEventIgnore (tLldpLocPortInfo * pPortInfo)
{
    LLDP_TRC_ARG1 (INIT_SHUT_TRC | CONTROL_PLANE_TRC,
                   "TXTMR-SEM[%d]: Event Impossible.\r\n",
                   pPortInfo->u4LocPortNum);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxTimerSmStateLldpInit
 *
 *    DESCRIPTION      : LLDP Tx module Initialization
 *
 *    INPUT            : pPortInfo - Port on which this event has occured
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PRIVATE VOID
LldpTxTmrSmStateLldpInit (tLldpLocPortInfo * pPortInfo)
{
    /* mark current state as LLDP_TX_INIT */
    pPortInfo->i4TxTimerSemCurrentState = LLDP_TX_TMR_INIT;
    pPortInfo->bTxNow = OSIX_FALSE;

    /* As per the transmit timer state machine of IEEE 802.1AB 2009 [section 9.2.10] 
     * the bSomethingChangedLocal flag should be set to FALSE while entering 
     * into the TX_TIMER_INITIALIZE state; But it will create the problem in the 
     * following scenario.
     * SCENARIO: 
     * 1) If we enable an LLDP agent and configure some parameter (Eg: Enabling some 
     * Optional TLVs), then a new LLDPDU will be constructed only if the txDelay timer
     * expired; otherwise only the flag pPortInfo->bSomethingChangedLocal will be set 
     * to OSIX_TRUE, so that a new LLDPDU will be constructed later.
     * 2) But suddenly if we configure the admin status of that agent to DISABLE, then
     * that agent's timer state machine will enter into TX_TIMER_INITIALIZE state,
     * where the flag pPortInfo->bSomethingChangedLocal will be set to OSIX_FALSE.
     * 3) So the configuration done previously would be lost; So need to comment the
     * below lines*/

    /*if (gLldpGlobalInfo.i4Version == LLDP_VERSION_09)
       {
       pPortInfo->bSomethingChangedLocal = OSIX_FALSE;
       } */
    pPortInfo->u1Credit = gLldpGlobalInfo.SysConfigInfo.u4TxCreditMax;
    pPortInfo->bNeighborDetected = OSIX_FALSE;
    pPortInfo->bTxTick = OSIX_FALSE;
    pPortInfo->u1TxFast = 0;

    /* If the lldp admin status is TX or TXRX, then transit to idle state */
    if (((pPortInfo->PortConfigTable.i4AdminStatus == LLDP_ADM_STAT_TX_ONLY) ||
         (pPortInfo->PortConfigTable.i4AdminStatus == LLDP_ADM_STAT_TX_AND_RX))
        && (pPortInfo->u1OperStatus == CFA_IF_UP) &&
        (gLldpGlobalInfo.u1ModuleStatus == LLDP_ENABLED))
    {
        LldpTxTmrSmMachine (pPortInfo, (UINT1) LLDP_TX_EV_TXMOD_ADMIN_UP);
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxTmrSmStateIdle
 *
 *    DESCRIPTION      : LLDP Tx module Idle state
 *
 *    INPUT            : pPortInfo - Port on which this event has occured
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PRIVATE VOID
LldpTxTmrSmStateIdle (tLldpLocPortInfo * pPortInfo)
{
#define LLDP_NUM_OF_MILLISEC_PER_STUP 10

    /* mark current state as LLDP_TX_IDLE */
    pPortInfo->i4TxTimerSemCurrentState = LLDP_TX_TMR_IDLE;
    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                   "TX-SEM[%d]: State = LLDP_TX_TMR_IDLE.\r\n",
                   pPortInfo->u4LocPortNum);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxTmrSmEventTmrExpiry
 *
 *    DESCRIPTION      : Handle Timer expiry event received
 *
 *    INPUT            : pPortInfo - Port on which this event has occured
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PRIVATE VOID
LldpTxTmrSmEventTmrExpiry (tLldpLocPortInfo * pPortInfo)
{
    pPortInfo->i4TxTimerSemCurrentState = LLDP_TX_TMR_EXPIRY;
    LLDP_TRC_ARG1 (INIT_SHUT_TRC,
                   "TXTMR-SEM[%d]: Event Timer Expiry.\r\n",
                   pPortInfo->u4LocPortNum);
    if (pPortInfo->u1TxFast > 0)
    {
        pPortInfo->u1TxFast--;
    }

    LldpTxTmrSmEventTmrSignalTx (pPortInfo);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxTmrSmEventTmrTxTick
 *
 *    DESCRIPTION      : Handle Timer Tx Tick event received
 *
 *    INPUT            : pPortInfo - Port on which this event has occured
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PRIVATE VOID
LldpTxTmrSmEventTmrTxTick (tLldpLocPortInfo * pPortInfo)
{
    UINT4               u4MilliSec = 0;
    UINT4               u4TxDelay = LLDP_ONE;

    pPortInfo->i4TxTimerSemCurrentState = LLDP_TX_TMR_TX_TICK;
    LLDP_TRC_ARG1 (INIT_SHUT_TRC,
                   "TXTMR-SEM[%d]: Event Timer Tx Tick.\r\n",
                   pPortInfo->u4LocPortNum);
    pPortInfo->bTxTick = OSIX_FALSE;
    LldpTxUtlAddCredit (pPortInfo);
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_09)
    {
        /* start TxDelay timer */
        if (TmrStart (gLldpGlobalInfo.TmrListId, &(pPortInfo->TxDelayTmr),
                      LLDP_TX_TMR_TX_DELAY, u4TxDelay,
                      u4MilliSec) != TMR_SUCCESS)
        {
            LLDP_TRC_ARG1 (OS_RESOURCE_TRC | ALL_FAILURE_TRC |
                           CONTROL_PLANE_TRC,
                           "TX-SEM[%d]: TxDelayTmr Start Timer FAILED !!!\r\n",
                           pPortInfo->u4LocPortNum);
        }
        /* set the transmit delay timer(TxDelay) status as
         * RUNNING */
        pPortInfo->TxDelayTmrNode.u1Status = LLDP_TMR_RUNNING;
    }
    LldpTxTmrSmStateIdle (pPortInfo);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxTmrSmEventTmrSignalTx
 *
 *    DESCRIPTION      : Handle Timer SignalTx event received
 *
 *    INPUT            : pPortInfo - Port on which this event has occured
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PRIVATE VOID
LldpTxTmrSmEventTmrSignalTx (tLldpLocPortInfo * pPortInfo)
{
    pPortInfo->i4TxTimerSemCurrentState = LLDP_TX_TMR_SIGNAL_TX;
    LLDP_TRC_ARG1 (INIT_SHUT_TRC,
                   "TXTMR-SEM[%d]: Event Signal Tx .\r\n",
                   pPortInfo->u4LocPortNum);
    pPortInfo->bTxNow = OSIX_TRUE;
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_09)
    {
        if (pPortInfo->bSomethingChangedLocal == OSIX_TRUE)
        {
            if (LldpTxUtlConstructPreformedBuf (pPortInfo, LLDP_INFO_FRAME)
                != OSIX_SUCCESS)
            {
                LLDP_TRC (ALL_FAILURE_TRC, "LldpTxTmrSmEventTmrSignalTx: "
                          "Construction of preformed buffer Failed.\r\n");
                return;
            }
        }

        pPortInfo->bSomethingChangedLocal = OSIX_FALSE;
    }
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_09)
    {
        if (pPortInfo->u1TxFast > 0)
        {
            if (pPortInfo->bMedCapable == LLDP_MED_TRUE)
            {
                pPortInfo->u4MsgInterval = LLDP_ONE;
            }
            else
            {
                pPortInfo->u4MsgInterval =
                    gLldpGlobalInfo.SysConfigInfo.u4MessageFastTx;
            }
        }
    }
    else                        /*if (pPortInfo->u1TxFast == 0) */
    {
        pPortInfo->u4MsgInterval =
            gLldpGlobalInfo.SysConfigInfo.i4MsgTxInterval;
    }
    if (pPortInfo->TtrTmrNode.u1Status == LLDP_TMR_RUNNING)
    {
        if (TmrStopTimer (gLldpGlobalInfo.TmrListId,
                          &(pPortInfo->TtrTmr.TimerNode)) != TMR_SUCCESS)
        {
            LLDP_TRC_ARG1 (OS_RESOURCE_TRC | ALL_FAILURE_TRC
                           | CONTROL_PLANE_TRC,
                           "TX-SEM[%d]: In LldpTxTmrSmEventTmrSignalTx "
                           "TmrStopTimer(TtrTmr) returns FAILURE!!!\r\n",
                           pPortInfo->u4LocPortNum);
        }
        /* set the transmit interval timer status as NOT_RUNNING */
        pPortInfo->TtrTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;
    }

#ifdef L2RED_WANTED
    if (LLDP_RED_NODE_STATUS () != RM_ACTIVE)
    {
        LldpTxTmrSmStateIdle (pPortInfo);
        return;
    }
#endif
    if (pPortInfo->bNeighborDetected == OSIX_TRUE)
    {
        pPortInfo->bNeighborDetected = OSIX_FALSE;
        if (pPortInfo->i1FastTxRateLimit > 0)
        {
            pPortInfo->i1FastTxRateLimit--;
            LldpTxSmMachine (pPortInfo, LLDP_TX_EV_TX_NOW);
        }
        else
        {
            /* UCT */
            LldpTxSmStateIdle (pPortInfo);
        }
    }
    else
    {
        LldpTxSmMachine (pPortInfo, LLDP_TX_EV_TX_NOW);
    }
    LldpTxTmrSmStateIdle (pPortInfo);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxTmrSmEventTxFastStart
 *
 *    DESCRIPTION      : Handle Timer TxFastStart event received
 *
 *    INPUT            : pPortInfo - Port on which this event has occured
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PRIVATE VOID
LldpTxTmrSmEventTxFastStart (tLldpLocPortInfo * pPortInfo)
{
    pPortInfo->i4TxTimerSemCurrentState = LLDP_TX_TMR_TX_FAST_START;
    LLDP_TRC_ARG1 (INIT_SHUT_TRC,
                   "TXTMR-SEM[%d]: Event Timer TxFastStart.\r\n",
                   pPortInfo->u4LocPortNum);
    pPortInfo->bNeighborDetected = OSIX_TRUE;
    if (pPortInfo->u4FastTxSysUpTime == 0)
    {
        pPortInfo->u4FastTxSysUpTime = LLDP_SYS_UP_TIME ();
        pPortInfo->i1FastTxRateLimit =
            (INT1) gLldpGlobalInfo.SysConfigInfo.u4TxCreditMax;
    }
    else
    {
        if ((LLDP_SYS_UP_TIME () - pPortInfo->u4FastTxSysUpTime) >
            SYS_TIME_TICKS_IN_A_SEC)
        {
            pPortInfo->u4FastTxSysUpTime = LLDP_SYS_UP_TIME ();
            pPortInfo->i1FastTxRateLimit =
                (INT1) gLldpGlobalInfo.SysConfigInfo.u4TxCreditMax;
        }
    }

    if (pPortInfo->u1TxFast == 0)
    {
        if (pPortInfo->bMedCapable == LLDP_MED_TRUE)
        {
            pPortInfo->u1TxFast =
                (UINT1) gLldpGlobalInfo.SysConfigInfo.u4MedFastStart;
        }
        else
        {
            pPortInfo->u1TxFast =
                (UINT1) gLldpGlobalInfo.SysConfigInfo.u4TxFastInit;
        }
    }
    /* UCT */
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_09)
    {
        LldpTxTmrSmEventTmrExpiry (pPortInfo);
    }
    else
    {
        LldpTxTmrSmStateIdle (pPortInfo);
    }
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  lldtxsm.c                      */
/*-----------------------------------------------------------------------*/
