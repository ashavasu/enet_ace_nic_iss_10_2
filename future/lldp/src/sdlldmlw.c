/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: sdlldmlw.c,v 1.7 2017/11/22 12:34:04 siva Exp $
*
* Description: LLDP-MED Standard Low Level Routines
*********************************************************************/
# include "lr.h"
# include "fssnmp.h"
# include "lldinc.h"
# include "lldcli.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXMedLocDeviceClass
 Input       :  The Indices

                The Object 
                retValLldpXMedLocDeviceClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedLocDeviceClass (INT4 *pi4RetValLldpXMedLocDeviceClass)
{
    /* Check  Whether LLDP is Shutdown. If it is true, then we should not
     * configure */
    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    *pi4RetValLldpXMedLocDeviceClass =
        (INT4) gLldpGlobalInfo.LocSysInfo.u1MedLocDeviceClass;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXMedPortConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXMedPortConfigTable
 Input       :  The Indices
                LldpPortConfigPortNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXMedPortConfigTable (INT4 i4LldpPortConfigPortNum)
{
    /*Validate Port Info by passing IfIndex and globalMulticast Address to 
       LldpMedUtlValidateIndexPortConfTbl */
    if (LldpMedUtlValidateIndexPortConfTbl (i4LldpPortConfigPortNum,
                                            gau1LldpMcastAddr) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXMedPortConfigTable
 Input       :  The Indices
                LldpPortConfigPortNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXMedPortConfigTable (INT4 *pi4LldpPortConfigPortNum)
{
    tLldpLocPortTable  *pLocPortTbl = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    pLocPortTbl =
        (tLldpLocPortTable *) RBTreeGetFirst (gLldpGlobalInfo.
                                              LldpPortInfoRBTree);
    if (pLocPortTbl == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4LldpPortConfigPortNum = (INT4) pLocPortTbl->i4IfIndex;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXMedPortConfigTable
 Input       :  The Indices
                LldpPortConfigPortNum
                nextLldpPortConfigPortNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXMedPortConfigTable (INT4 i4LldpPortConfigPortNum, INT4
                                        *pi4NextLldpPortConfigPortNum)
{
    tLldpLocPortTable   CurrLocPortTbl;
    tLldpLocPortTable  *pNextLocPortTbl = NULL;

    CurrLocPortTbl.i4IfIndex = i4LldpPortConfigPortNum;

    pNextLocPortTbl =
        (tLldpLocPortTable *) RBTreeGetNext (gLldpGlobalInfo.LldpPortInfoRBTree,
                                             &CurrLocPortTbl,
                                             LldpPortInfoUtlRBCmpInfo);

    if (pNextLocPortTbl == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextLldpPortConfigPortNum = (INT4) pNextLocPortTbl->i4IfIndex;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXMedPortCapSupported
 Input       :  The Indices
                LldpPortConfigPortNum

                The Object 
                retValLldpXMedPortCapSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedPortCapSupported (INT4 i4LldpPortConfigPortNum,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValLldpXMedPortCapSupported)
{
    if (LldpMedUtlGetLocPortCapSupported (i4LldpPortConfigPortNum,
                                          gau1LldpMcastAddr,
                                          pRetValLldpXMedPortCapSupported) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetLldpXMedPortConfigTLVsTxEnable
 Input       :  The Indices
                LldpPortConfigPortNum

                The Object 
                retValLldpXMedPortConfigTLVsTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedPortConfigTLVsTxEnable (INT4 i4LldpPortConfigPortNum,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValLldpXMedPortConfigTLVsTxEnable)
{
    if (LldpMedUtlGetLldpPortConfigTLVsTxEnable (i4LldpPortConfigPortNum,
                                                 gau1LldpMcastAddr,
                                                 pRetValLldpXMedPortConfigTLVsTxEnable)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetLldpXMedPortConfigNotifEnable
 Input       :  The Indices
                LldpPortConfigPortNum

                The Object 
                retValLldpXMedPortConfigNotifEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedPortConfigNotifEnable (INT4 i4LldpPortConfigPortNum, INT4
                                     *pi4RetValLldpXMedPortConfigNotifEnable)
{
    if (LldpUtlGetLldpPortConfigNotificationEnable (i4LldpPortConfigPortNum,
                                                    gau1LldpMcastAddr,
                                                    pi4RetValLldpXMedPortConfigNotifEnable)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXMedPortConfigTLVsTxEnable
 Input       :  The Indices
                LldpPortConfigPortNum

                The Object 
                setValLldpXMedPortConfigTLVsTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpXMedPortConfigTLVsTxEnable (INT4 i4LldpPortConfigPortNum,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSetValLldpXMedPortConfigTLVsTxEnable)
{
    if (LldpMedUtlSetPortConfigTLVsTxEnable (i4LldpPortConfigPortNum,
                                             gau1LldpMcastAddr,
                                             pSetValLldpXMedPortConfigTLVsTxEnable)
        != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " nmhSetLldpXMedPortConfigTLVsTxEnable: "
                  "Unable to set \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetLldpXMedPortConfigNotifEnable
 Input       :  The Indices
                LldpPortConfigPortNum

                The Object 
                setValLldpXMedPortConfigNotifEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpXMedPortConfigNotifEnable (INT4 i4LldpPortConfigPortNum, INT4
                                     i4SetValLldpXMedPortConfigNotifEnable)
{
    if (LldpUtlSetLldpPortConfigNotificationEnable (i4LldpPortConfigPortNum,
                                                    gau1LldpMcastAddr,
                                                    i4SetValLldpXMedPortConfigNotifEnable)
        != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  "nmhSetLldpXMedPortConfigNotifEnable: " "Unable to set \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXMedPortConfigTLVsTxEnable
 Input       :  The Indices
                LldpPortConfigPortNum

                The Object 
                testValLldpXMedPortConfigTLVsTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpXMedPortConfigTLVsTxEnable (UINT4 *pu4ErrorCode, INT4
                                         i4LldpPortConfigPortNum,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pTestValLldpXMedPortConfigTLVsTxEnable)
{
    if (LldpMedUtlTestPortConfigTLVsTxEnable (pu4ErrorCode,
                                              i4LldpPortConfigPortNum,
                                              gau1LldpMcastAddr,
                                              pTestValLldpXMedPortConfigTLVsTxEnable)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2LldpXMedPortConfigNotifEnable
 Input       :  The Indices
                LldpPortConfigPortNum

                The Object 
                testValLldpXMedPortConfigNotifEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpXMedPortConfigNotifEnable (UINT4 *pu4ErrorCode, INT4
                                        i4LldpPortConfigPortNum,
                                        INT4
                                        i4TestValLldpXMedPortConfigNotifEnable)
{
    if (LldpUtlTestLldpPortConfigNotificationEnable (pu4ErrorCode,
                                                     i4LldpPortConfigPortNum,
                                                     gau1LldpMcastAddr,
                                                     i4TestValLldpXMedPortConfigNotifEnable)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpXMedPortConfigTable
 Input       :  The Indices
                LldpPortConfigPortNum
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXMedPortConfigTable (UINT4 *pu4ErrorCode, tSnmpIndexList
                                 * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXMedFastStartRepeatCount
 Input       :  The Indices

                The Object 
                retValLldpXMedFastStartRepeatCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedFastStartRepeatCount (UINT4
                                    *pu4RetValLldpXMedFastStartRepeatCount)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        /* LLDP system is shutdown, so return default value */
        *pu4RetValLldpXMedFastStartRepeatCount = (INT4)
            LLDPV2_DEFAULT_MED_FAST_REPEAT_COUNT;
        return SNMP_SUCCESS;
    }

    *pu4RetValLldpXMedFastStartRepeatCount =
        (UINT4) gLldpGlobalInfo.SysConfigInfo.u4MedFastStart;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXMedFastStartRepeatCount
 Input       :  The Indices

                The Object 
                setValLldpXMedFastStartRepeatCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpXMedFastStartRepeatCount (UINT4 u4SetValLldpXMedFastStartRepeatCount)
{
    if (gLldpGlobalInfo.SysConfigInfo.u4MedFastStart !=
        u4SetValLldpXMedFastStartRepeatCount)
    {
        /* Update the Fast start repeat count Value */
        gLldpGlobalInfo.SysConfigInfo.u4MedFastStart =
            u4SetValLldpXMedFastStartRepeatCount;
        LldpTxUtlHandleLocSysInfoChg ();
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXMedFastStartRepeatCount
 Input       :  The Indices

                The Object 
                testValLldpXMedFastStartRepeatCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpXMedFastStartRepeatCount (UINT4 *pu4ErrorCode, UINT4
                                       u4TestValLldpXMedFastStartRepeatCount)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " nmhTestv2LldpXMedFastStartRepeatCount: LLDP should be"
                  "started before accessing its " "MIB objects !!\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValLldpXMedFastStartRepeatCount < LLDP_MIN_MED_FAST_REPEAT_COUNT)
        || (u4TestValLldpXMedFastStartRepeatCount >
            LLDP_MAX_MED_FAST_REPEAT_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        LLDP_TRC (ALL_FAILURE_TRC, " nmhTestv2LldpXMedFastStartRepeatCount: "
                  "u4TestValLldpXMedFastStartRepeatCount"
                  "is not within the allowed range \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpXMedFastStartRepeatCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXMedFastStartRepeatCount (UINT4 *pu4ErrorCode, tSnmpIndexList
                                      * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXMedLocMediaPolicyTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXMedLocMediaPolicyTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXMedLocMediaPolicyAppType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXMedLocMediaPolicyTable (INT4 i4LldpLocPortNum,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pLldpXMedLocMediaPolicyAppType)
{
    if (LldpMedUtlValIndexLocNwPolTbl (i4LldpLocPortNum,
                                       pLldpXMedLocMediaPolicyAppType) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXMedLocMediaPolicyTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXMedLocMediaPolicyAppType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXMedLocMediaPolicyTable (INT4 *pi4LldpLocPortNum,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pLldpXMedLocMediaPolicyAppType)
{
    if (LldpMedUtlGetFirstIndexLocNwPolTbl (pi4LldpLocPortNum,
                                            pLldpXMedLocMediaPolicyAppType) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXMedLocMediaPolicyTable
 Input       :  The Indices
                LldpLocPortNum
                nextLldpLocPortNum
                LldpXMedLocMediaPolicyAppType
                nextLldpXMedLocMediaPolicyAppType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXMedLocMediaPolicyTable (INT4 i4LldpLocPortNum, INT4
                                            *pi4NextLldpLocPortNum,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pLldpXMedLocMediaPolicyAppType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pNextLldpXMedLocMediaPolicyAppType)
{
    if (LldpMedUtlGetNextIndexLocNwPolTbl (i4LldpLocPortNum,
                                           pi4NextLldpLocPortNum,
                                           pLldpXMedLocMediaPolicyAppType,
                                           pNextLldpXMedLocMediaPolicyAppType)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXMedLocMediaPolicyVlanID
 Input       :  The Indices
                LldpLocPortNum
                LldpXMedLocMediaPolicyAppType

                The Object 
                retValLldpXMedLocMediaPolicyVlanID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedLocMediaPolicyVlanID (INT4 i4LldpLocPortNum,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pLldpXMedLocMediaPolicyAppType,
                                    INT4 *pi4RetValLldpXMedLocMediaPolicyVlanID)
{
    if (LldpMedUtlGetLocNwPolVlanID (i4LldpLocPortNum,
                                     pLldpXMedLocMediaPolicyAppType,
                                     pi4RetValLldpXMedLocMediaPolicyVlanID) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedLocMediaPolicyPriority
 Input       :  The Indices
                LldpLocPortNum
                LldpXMedLocMediaPolicyAppType

                The Object 
                retValLldpXMedLocMediaPolicyPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedLocMediaPolicyPriority (INT4 i4LldpLocPortNum,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pLldpXMedLocMediaPolicyAppType,
                                      INT4
                                      *pi4RetValLldpXMedLocMediaPolicyPriority)
{
    if (LldpMedUtlGetLocNwPolPriority (i4LldpLocPortNum,
                                       pLldpXMedLocMediaPolicyAppType,
                                       pi4RetValLldpXMedLocMediaPolicyPriority)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedLocMediaPolicyDscp
 Input       :  The Indices
                LldpLocPortNum
                LldpXMedLocMediaPolicyAppType

                The Object 
                retValLldpXMedLocMediaPolicyDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedLocMediaPolicyDscp (INT4 i4LldpLocPortNum,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pLldpXMedLocMediaPolicyAppType,
                                  INT4 *pi4RetValLldpXMedLocMediaPolicyDscp)
{
    if (LldpMedUtlGetLocNwPolDscp (i4LldpLocPortNum,
                                   pLldpXMedLocMediaPolicyAppType,
                                   pi4RetValLldpXMedLocMediaPolicyDscp) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedLocMediaPolicyUnknown
 Input       :  The Indices
                LldpLocPortNum
                LldpXMedLocMediaPolicyAppType

                The Object 
                retValLldpXMedLocMediaPolicyUnknown
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedLocMediaPolicyUnknown (INT4 i4LldpLocPortNum,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pLldpXMedLocMediaPolicyAppType,
                                     INT4
                                     *pi4RetValLldpXMedLocMediaPolicyUnknown)
{
    if (LldpMedUtlGetLocNwPolUnknown (i4LldpLocPortNum,
                                      pLldpXMedLocMediaPolicyAppType,
                                      pi4RetValLldpXMedLocMediaPolicyUnknown) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedLocMediaPolicyTagged
 Input       :  The Indices
                LldpLocPortNum
                LldpXMedLocMediaPolicyAppType

                The Object 
                retValLldpXMedLocMediaPolicyTagged
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedLocMediaPolicyTagged (INT4 i4LldpLocPortNum,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pLldpXMedLocMediaPolicyAppType,
                                    INT4 *pi4RetValLldpXMedLocMediaPolicyTagged)
{
    if (LldpMedUtlGetLocNwPolTagged (i4LldpLocPortNum,
                                     pLldpXMedLocMediaPolicyAppType,
                                     pi4RetValLldpXMedLocMediaPolicyTagged) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXMedLocHardwareRev
 Input       :  The Indices

                The Object 
                retValLldpXMedLocHardwareRev
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedLocHardwareRev (tSNMP_OCTET_STRING_TYPE *
                              pRetValLldpXMedLocHardwareRev)
{
    /* Check  Whether LLDP is Shutdown. If it is true, then we should not get the information */
    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    /* Assign  the Hardware Revision Information to octelist of 
       pRetValLldpXMedLocHardwareRev- */
    MEMCPY (pRetValLldpXMedLocHardwareRev->pu1_OctetList, gLldpGlobalInfo.
            LocSysInfo.LocInventoryInfo.au1MedLocHardwareRev, MEM_MAX_BYTES
            (LLDP_MED_HW_REV_LEN,
             STRLEN (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.
                     au1MedLocHardwareRev)));
    pRetValLldpXMedLocHardwareRev->i4_Length =
        (INT4) STRLEN (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.
                       au1MedLocHardwareRev);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetLldpXMedLocFirmwareRev
 Input       :  The Indices

                The Object 
                retValLldpXMedLocFirmwareRev
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedLocFirmwareRev (tSNMP_OCTET_STRING_TYPE *
                              pRetValLldpXMedLocFirmwareRev)
{
    /* Check  Whether LLDP is Shutdown. If it is true, then we should not get the information */
    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    /* Assign  the Firmware Revision Information to octelist of
       pRetValLldpXMedLocFirmwareRev */
    MEMCPY (pRetValLldpXMedLocFirmwareRev->pu1_OctetList,
            gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.au1MedLocFirmwareRev,
            MEM_MAX_BYTES (LLDP_MED_FW_REV_LEN, STRLEN (gLldpGlobalInfo.
                                                        LocSysInfo.
                                                        LocInventoryInfo.
                                                        au1MedLocFirmwareRev)));

    pRetValLldpXMedLocFirmwareRev->i4_Length =
        (INT4) STRLEN (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.
                       au1MedLocFirmwareRev);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetLldpXMedLocSoftwareRev
 Input       :  The Indices

                The Object 
                retValLldpXMedLocSoftwareRev
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedLocSoftwareRev (tSNMP_OCTET_STRING_TYPE *
                              pRetValLldpXMedLocSoftwareRev)
{
    /* Check  Whether LLDP is Shutdown. If it is true, then we should not get the information */
    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    /* Assign  the Software Revision Information to octelist of
       pRetValLldpXMedLocSoftwareRev */
    MEMCPY (pRetValLldpXMedLocSoftwareRev->pu1_OctetList,
            gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.au1MedLocSoftwareRev,
            MEM_MAX_BYTES (LLDP_MED_SW_REV_LEN, STRLEN (gLldpGlobalInfo.
                                                        LocSysInfo.
                                                        LocInventoryInfo.
                                                        au1MedLocSoftwareRev)));
    pRetValLldpXMedLocSoftwareRev->i4_Length =
        (INT4) STRLEN (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.
                       au1MedLocSoftwareRev);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetLldpXMedLocSerialNum
 Input       :  The Indices

                The Object 
                retValLldpXMedLocSerialNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedLocSerialNum (tSNMP_OCTET_STRING_TYPE *
                            pRetValLldpXMedLocSerialNum)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    /* Assign  Serial Number */
    MEMCPY (pRetValLldpXMedLocSerialNum->pu1_OctetList,
            gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.au1MedLocSerialNum,
            MEM_MAX_BYTES (LLDP_MED_SER_NUM_LEN,
                           STRLEN (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.
                                   au1MedLocSerialNum)));
    pRetValLldpXMedLocSerialNum->i4_Length =
        (INT4) STRLEN (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.
                       au1MedLocSerialNum);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetLldpXMedLocMfgName
 Input       :  The Indices

                The Object 
                retValLldpXMedLocMfgName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedLocMfgName (tSNMP_OCTET_STRING_TYPE * pRetValLldpXMedLocMfgName)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    /* Assign  Manufacturer Name */
    MEMCPY (pRetValLldpXMedLocMfgName->pu1_OctetList,
            gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.au1MedLocMfgName,
            MEM_MAX_BYTES (LLDP_MED_MFG_NAME_LEN,
                           STRLEN (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.
                                   au1MedLocMfgName)));

    pRetValLldpXMedLocMfgName->i4_Length =
        (INT4) STRLEN (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.
                       au1MedLocMfgName);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetLldpXMedLocModelName
 Input       :  The Indices

                The Object 
                retValLldpXMedLocModelName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedLocModelName (tSNMP_OCTET_STRING_TYPE *
                            pRetValLldpXMedLocModelName)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    /* Assign  Model Name */
    MEMCPY (pRetValLldpXMedLocModelName->pu1_OctetList,
            gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.au1MedLocModelName,
            MEM_MAX_BYTES (LLDP_MED_MODEL_NAME_LEN, STRLEN (gLldpGlobalInfo.
                                                            LocSysInfo.
                                                            LocInventoryInfo.
                                                            au1MedLocModelName)));

    pRetValLldpXMedLocModelName->i4_Length =
        (INT4) STRLEN (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.
                       au1MedLocModelName);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetLldpXMedLocAssetID
 Input       :  The Indices

                The Object 
                retValLldpXMedLocAssetID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedLocAssetID (tSNMP_OCTET_STRING_TYPE * pRetValLldpXMedLocAssetID)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    /* Assign  AssetId */
    MEMCPY (pRetValLldpXMedLocAssetID->pu1_OctetList,
            gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.au1MedLocAssetId,
            MEM_MAX_BYTES (LLDP_MED_ASSET_ID_LEN,
                           STRLEN (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.
                                   au1MedLocAssetId)));

    pRetValLldpXMedLocAssetID->i4_Length =
        (INT4) STRLEN (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.
                       au1MedLocAssetId);
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : LldpXMedLocLocationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXMedLocLocationTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXMedLocLocationSubtype
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXMedLocLocationTable (INT4 i4LldpLocPortNum,
                                                  INT4
                                                  i4LldpXMedLocLocationSubtype)
{
    /* Checking Whether LLDP is Shutdown. If it is true, then we should not
     * configure */
    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    /* Validate Interface Index */
    if (LldpTxUtlValidateInterfaceIndex (i4LldpLocPortNum) != OSIX_SUCCESS)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " nmhValidateIndexInstanceLldpXMedLocLocationTable: "
                  " Interface Index is invalid \r\n");
        return SNMP_FAILURE;
    }
    /* Validate Location subtype */
    if ((i4LldpXMedLocLocationSubtype < LLDP_MED_COORDINATE_LOC) ||
        (i4LldpXMedLocLocationSubtype > LLDP_MED_ELIN_LOC))
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " Location Sub Type is not in range ");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXMedLocLocationTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXMedLocLocationSubtype
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXMedLocLocationTable (INT4 *pi4LldpLocPortNum, INT4
                                          *pi4LldpXMedLocLocationSubtype)
{
    tLldpMedLocLocationInfo *pLldpMedLocLocation = NULL;

    /* Checking Whether LLDP is Shutdown. If it is true, then we should not
     * configure */
    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    /* Get First Index of PolicyTable */
    pLldpMedLocLocation =
        (tLldpMedLocLocationInfo *) RBTreeGetFirst (gLldpGlobalInfo.
                                                    LldpMedLocLocationInfoRBTree);
    if (pLldpMedLocLocation == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " nmhGetFirstIndexLldpXMedLocLocationTable: "
                  " Failed to get Location Table Index \r\n");

        return SNMP_FAILURE;
    }

    /* Assigning IfIndex and Policy App type */
    *pi4LldpLocPortNum = (INT4) pLldpMedLocLocation->u4IfIndex;
    *pi4LldpXMedLocLocationSubtype =
        (INT4) pLldpMedLocLocation->u1LocationDataFormat;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXMedLocLocationTable
 Input       :  The Indices
                LldpLocPortNum
                nextLldpLocPortNum
                LldpXMedLocLocationSubtype
                nextLldpXMedLocLocationSubtype
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXMedLocLocationTable (INT4 i4LldpLocPortNum, INT4
                                         *pi4NextLldpLocPortNum,
                                         INT4 i4LldpXMedLocLocationSubtype,
                                         INT4
                                         *pi4NextLldpXMedLocLocationSubtype)
{
    tLldpMedLocLocationInfo CurrLocLocation;
    tLldpMedLocLocationInfo *pNextLocLocation = NULL;

    MEMSET (&CurrLocLocation, 0, sizeof (tLldpMedLocLocationInfo));
    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    /* Assigning the Current IfIndex */
    CurrLocLocation.u4IfIndex = (UINT4) i4LldpLocPortNum;
    CurrLocLocation.u1LocationDataFormat = (UINT1) i4LldpXMedLocLocationSubtype;

    /* Get Next Index of Local Location Table */
    pNextLocLocation =
        (tLldpMedLocLocationInfo *) RBTreeGetNext (gLldpGlobalInfo.
                                                   LldpMedLocLocationInfoRBTree,
                                                   &CurrLocLocation,
                                                   LldpMedLocLocationInfoRBCmpInfo);

    if (pNextLocLocation == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " nmhGetNextIndexLldpXMedLocLocationTable: "
                  " Failed to get Location Table Index \r\n");

        return SNMP_FAILURE;
    }

    *pi4NextLldpLocPortNum = (INT4) pNextLocLocation->u4IfIndex;
    *pi4NextLldpXMedLocLocationSubtype =
        (INT4) pNextLocLocation->u1LocationDataFormat;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXMedLocLocationInfo
 Input       :  The Indices
                LldpLocPortNum
                LldpXMedLocLocationSubtype

                The Object 
                retValLldpXMedLocLocationInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedLocLocationInfo (INT4 i4LldpLocPortNum, INT4
                               i4LldpXMedLocLocationSubtype,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValLldpXMedLocLocationInfo)
{
    tLldpMedLocLocationInfo LldpMedLocLocation;
    tLldpMedLocLocationInfo *pLldpMedLocLocation = NULL;

    MEMSET (&LldpMedLocLocation, 0, sizeof (tLldpMedLocLocationInfo));
    LldpMedLocLocation.u4IfIndex = (UINT4) i4LldpLocPortNum;
    LldpMedLocLocation.u1LocationDataFormat =
        (UINT1) i4LldpXMedLocLocationSubtype;

    pLldpMedLocLocation =
        (tLldpMedLocLocationInfo *) RBTreeGet (gLldpGlobalInfo.
                                               LldpMedLocLocationInfoRBTree,
                                               (tRBElem *) &
                                               LldpMedLocLocation);

    if (pLldpMedLocLocation == NULL)
    {
        return SNMP_FAILURE;
    }
    if (i4LldpXMedLocLocationSubtype == LLDP_MED_COORDINATE_LOC)
    {
        MEMCPY (pRetValLldpXMedLocLocationInfo->pu1_OctetList,
                pLldpMedLocLocation->au1LocationID,
                LLDP_MED_MAX_COORDINATE_LOC_LENGTH);
        pRetValLldpXMedLocLocationInfo->i4_Length =
            (INT4) LLDP_MED_MAX_COORDINATE_LOC_LENGTH;
    }
    else if (i4LldpXMedLocLocationSubtype == LLDP_MED_CIVIC_LOC)
    {
        MEMCPY (pRetValLldpXMedLocLocationInfo->pu1_OctetList,
                pLldpMedLocLocation->au1LocationID,
                (pLldpMedLocLocation->au1LocationID[0] + 1));
        pRetValLldpXMedLocLocationInfo->i4_Length =
            (INT4) (pLldpMedLocLocation->au1LocationID[0] + 1);
    }
    else
    {
        MEMCPY (pRetValLldpXMedLocLocationInfo->pu1_OctetList,
                pLldpMedLocLocation->au1LocationID,
                STRLEN (pLldpMedLocLocation->au1LocationID));
        pRetValLldpXMedLocLocationInfo->i4_Length =
            (INT4) STRLEN (pLldpMedLocLocation->au1LocationID);
    }

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXMedLocLocationInfo
 Input       :  The Indices
                LldpLocPortNum
                LldpXMedLocLocationSubtype

                The Object 
                setValLldpXMedLocLocationInfo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpXMedLocLocationInfo (INT4 i4LldpLocPortNum, INT4
                               i4LldpXMedLocLocationSubtype,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValLldpXMedLocLocationInfo)
{
    tLldpMedLocLocationInfo LldpMedLocLocation;
    tLldpMedLocLocationInfo *pLldpMedLocLocation = NULL;

    MEMSET (&LldpMedLocLocation, 0, sizeof (tLldpMedLocLocationInfo));
    LldpMedLocLocation.u4IfIndex = (UINT4) i4LldpLocPortNum;
    LldpMedLocLocation.u1LocationDataFormat =
        (UINT1) i4LldpXMedLocLocationSubtype;

    pLldpMedLocLocation =
        (tLldpMedLocLocationInfo *) RBTreeGet (gLldpGlobalInfo.
                                               LldpMedLocLocationInfoRBTree,
                                               (tRBElem *) &
                                               LldpMedLocLocation);
    if (pLldpMedLocLocation == NULL)
    {
        return SNMP_FAILURE;

    }
    if (MEMCMP (pSetValLldpXMedLocLocationInfo->pu1_OctetList,
                pLldpMedLocLocation->au1LocationID,
                pSetValLldpXMedLocLocationInfo->i4_Length) == 0)
    {
        return SNMP_SUCCESS;
    }
    MEMCPY (pLldpMedLocLocation->au1LocationID,
            pSetValLldpXMedLocLocationInfo->pu1_OctetList,
            pSetValLldpXMedLocLocationInfo->i4_Length);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXMedLocLocationInfo
 Input       :  The Indices
                LldpLocPortNum
                LldpXMedLocLocationSubtype

                The Object 
                testValLldpXMedLocLocationInfo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpXMedLocLocationInfo (UINT4 *pu4ErrorCode, INT4
                                  i4LldpLocPortNum,
                                  INT4 i4LldpXMedLocLocationSubtype,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValLldpXMedLocLocationInfo)
{
    tLldpMedLocLocationInfo *pLocLocationInfo = NULL;
    tLldpMedLocLocationInfo LocLocationInfo;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhTestv2LldpXMedLocLocationInfo: "
                  "LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    MEMSET (&LocLocationInfo, 0, sizeof (tLldpMedLocLocationInfo));

    LocLocationInfo.u4IfIndex = (UINT4) i4LldpLocPortNum;
    LocLocationInfo.u1LocationDataFormat = (UINT1) i4LldpXMedLocLocationSubtype;

    pLocLocationInfo = (tLldpMedLocLocationInfo *)
        RBTreeGet (gLldpGlobalInfo.
                   LldpMedLocLocationInfoRBTree, (tRBElem *) & LocLocationInfo);

    if (pLocLocationInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    else
    {
        if (STRLEN (pLocLocationInfo->au1LocationID) != 0)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if (i4LldpXMedLocLocationSubtype == LLDP_MED_COORDINATE_LOC)
        {
            if (pTestValLldpXMedLocLocationInfo->i4_Length !=
                LLDP_MED_MAX_COORDINATE_LOC_LENGTH)
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          " SNMPSTD: LldpXMedLocLocationInfo Coordinate location length not "
                          "within supported range!!\r\n");
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return SNMP_FAILURE;
            }
        }
        else if (i4LldpXMedLocLocationSubtype == LLDP_MED_ELIN_LOC)
        {
            if ((pTestValLldpXMedLocLocationInfo->i4_Length <
                 LLDP_MED_MIN_ELIN_LOC_LENGTH)
                || (pTestValLldpXMedLocLocationInfo->i4_Length >
                    LLDP_MED_MAX_ELIN_LOC_LENGTH))
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          " SNMPSTD: LldpXMedLocLocationInfo Elin location length not "
                          "within supported range!!\r\n");
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return SNMP_FAILURE;

            }

        }
        else if (i4LldpXMedLocLocationSubtype == LLDP_MED_CIVIC_LOC)
        {
            if ((pTestValLldpXMedLocLocationInfo->i4_Length <
                 LLDP_MED_MIN_CIVIC_LOC_LENGTH)
                || (pTestValLldpXMedLocLocationInfo->i4_Length >
                    LLDP_MED_MAX_CIVIC_LOC_LENGTH))
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          " SNMPSTD: LldpXMedLocLocationInfo Civic location length not "
                          "within supported range!!\r\n");
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return SNMP_FAILURE;
            }
        }

        /* Allow to edit the Location info if and only if
         * Row Status is NOT_IN_SERVICE*/
        if (pLocLocationInfo->i4RowStatus != NOT_IN_SERVICE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpXMedLocLocationTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXMedLocLocationSubtype
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXMedLocLocationTable (UINT4 *pu4ErrorCode, tSnmpIndexList
                                  * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXMedLocXPoEDeviceType
 Input       :  The Indices

                The Object 
                retValLldpXMedLocXPoEDeviceType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedLocXPoEDeviceType (INT4 *pi4RetValLldpXMedLocXPoEDeviceType)
{
    /* Check  Whether LLDP is Shutdown. If it is true, then we should not
     * configure */
    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    if (gLldpGlobalInfo.LocSysInfo.u1MedLocPoEDeviceType == LLDP_MED_PSE_DEVICE)
    {
        *pi4RetValLldpXMedLocXPoEDeviceType = LLDP_MED_PSE;
    }
    else if (gLldpGlobalInfo.LocSysInfo.u1MedLocPoEDeviceType ==
             LLDP_MED_PD_DEVICE)
    {
        *pi4RetValLldpXMedLocXPoEDeviceType = LLDP_MED_PD;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXMedLocXPoEPSEPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXMedLocXPoEPSEPortTable
 Input       :  The Indices
                LldpLocPortNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXMedLocXPoEPSEPortTable (INT4 i4LldpLocPortNum)
{
    /* Checking Whether LLDP is Shutdown. If it is true, then we should not
     * configure */
    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    /* Validate Interface Index */
    if (LldpTxUtlValidateInterfaceIndex (i4LldpLocPortNum) != OSIX_SUCCESS)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " nmhValidateIndexInstanceLldpXMedLocXPoEPSEPortTable: "
                  " Interface Index is invalid \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXMedLocXPoEPSEPortTable
 Input       :  The Indices
                LldpLocPortNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXMedLocXPoEPSEPortTable (INT4 *pi4LldpLocPortNum)
{
    tLldpLocPortTable  *pLocPortTbl = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    pLocPortTbl =
        (tLldpLocPortTable *) RBTreeGetFirst (gLldpGlobalInfo.
                                              LldpPortInfoRBTree);
    if (pLocPortTbl == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4LldpLocPortNum = (INT4) pLocPortTbl->i4IfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXMedLocXPoEPSEPortTable
 Input       :  The Indices
                LldpLocPortNum
                nextLldpLocPortNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXMedLocXPoEPSEPortTable (INT4 i4LldpLocPortNum, INT4
                                            *pi4NextLldpLocPortNum)
{
    tLldpLocPortTable   CurrLocPortTbl;
    tLldpLocPortTable  *pNextLocPortTbl = NULL;

    CurrLocPortTbl.i4IfIndex = i4LldpLocPortNum;

    pNextLocPortTbl =
        (tLldpLocPortTable *) RBTreeGetNext (gLldpGlobalInfo.LldpPortInfoRBTree,
                                             &CurrLocPortTbl,
                                             LldpPortInfoUtlRBCmpInfo);

    if (pNextLocPortTbl == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextLldpLocPortNum = (INT4) pNextLocPortTbl->i4IfIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXMedLocXPoEPSEPortPowerAv
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValLldpXMedLocXPoEPSEPortPowerAv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedLocXPoEPSEPortPowerAv (INT4 i4LldpLocPortNum, UINT4
                                     *pu4RetValLldpXMedLocXPoEPSEPortPowerAv)
{
    tLldpLocPortTable  *pLocPortTbl = NULL;
    tLldpLocPortTable   LocPortTbl;

    MEMSET (&LocPortTbl, 0, sizeof (tLldpLocPortTable));

    LocPortTbl.i4IfIndex = i4LldpLocPortNum;
    pLocPortTbl =
        (tLldpLocPortTable *) RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree,
                                         (tRBElem *) & LocPortTbl);
    if (pLocPortTbl == NULL)
    {
        return SNMP_FAILURE;

    }

    *pu4RetValLldpXMedLocXPoEPSEPortPowerAv =
        (UINT4) pLocPortTbl->MedPSEPowerInfo.u2PsePowerAv;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedLocXPoEPSEPortPDPriority
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValLldpXMedLocXPoEPSEPortPDPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedLocXPoEPSEPortPDPriority (INT4 i4LldpLocPortNum, INT4
                                        *pi4RetValLldpXMedLocXPoEPSEPortPDPriority)
{
    tLldpLocPortTable  *pLocPortTbl = NULL;
    tLldpLocPortTable   LocPortTbl;
    MEMSET (&LocPortTbl, 0, sizeof (tLldpLocPortTable));
    LocPortTbl.i4IfIndex = i4LldpLocPortNum;
    pLocPortTbl =
        (tLldpLocPortTable *) RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree,
                                         (tRBElem *) & LocPortTbl);
    if (pLocPortTbl == NULL)
    {
        return SNMP_FAILURE;

    }
    if (pLocPortTbl->MedPSEPowerInfo.u1PsePowerPriority ==
        LLDP_MED_PRI_CRITICAL)
    {
        *pi4RetValLldpXMedLocXPoEPSEPortPDPriority = LLDP_MED_PRI_CRITICAL_MIB;
    }
    else if (pLocPortTbl->MedPSEPowerInfo.u1PsePowerPriority ==
             LLDP_MED_PRI_HIGH)
    {
        *pi4RetValLldpXMedLocXPoEPSEPortPDPriority = LLDP_MED_PRI_HIGH_MIB;
    }
    else if (pLocPortTbl->MedPSEPowerInfo.u1PsePowerPriority ==
             LLDP_MED_PRI_LOW)
    {
        *pi4RetValLldpXMedLocXPoEPSEPortPDPriority = LLDP_MED_PRI_LOW_MIB;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXMedLocXPoEPSEPowerSource
 Input       :  The Indices

                The Object 
                retValLldpXMedLocXPoEPSEPowerSource
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedLocXPoEPSEPowerSource (INT4
                                     *pi4RetValLldpXMedLocXPoEPSEPowerSource)
{
    /* Check  Whether LLDP is Shutdown. If it is true, then we should not
     * configure */
    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    if (gLldpGlobalInfo.LocSysInfo.u1MedLocPSEPowerSource ==
        LLDP_MED_PSE_PRIMARY)
    {
        *pi4RetValLldpXMedLocXPoEPSEPowerSource = LLDP_MED_PSE_PRIMARY_MIB;
    }
    else if (gLldpGlobalInfo.LocSysInfo.u1MedLocPSEPowerSource ==
             LLDP_MED_PSE_BACK_UP)
    {
        *pi4RetValLldpXMedLocXPoEPSEPowerSource = LLDP_MED_PSE_BACK_UP_MIB;
    }
    else if (gLldpGlobalInfo.LocSysInfo.u1MedLocPSEPowerSource ==
             LLDP_MED_PSE_UNKNOWN)
    {
        *pi4RetValLldpXMedLocXPoEPSEPowerSource = LLDP_MED_PSE_UNKNOWN_MIB;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedLocXPoEPDPowerReq
 Input       :  The Indices

                The Object 
                retValLldpXMedLocXPoEPDPowerReq
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedLocXPoEPDPowerReq (UINT4 *pu4RetValLldpXMedLocXPoEPDPowerReq)
{
    UNUSED_PARAM (pu4RetValLldpXMedLocXPoEPDPowerReq);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedLocXPoEPDPowerSource
 Input       :  The Indices

                The Object 
                retValLldpXMedLocXPoEPDPowerSource
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedLocXPoEPDPowerSource (INT4 *pi4RetValLldpXMedLocXPoEPDPowerSource)
{
    UNUSED_PARAM (pi4RetValLldpXMedLocXPoEPDPowerSource);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedLocXPoEPDPowerPriority
 Input       :  The Indices

                The Object 
                retValLldpXMedLocXPoEPDPowerPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedLocXPoEPDPowerPriority (INT4
                                      *pi4RetValLldpXMedLocXPoEPDPowerPriority)
{
    UNUSED_PARAM (pi4RetValLldpXMedLocXPoEPDPowerPriority);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXMedRemCapabilitiesTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXMedRemCapabilitiesTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXMedRemCapabilitiesTable (UINT4
                                                      u4LldpRemTimeMark,
                                                      INT4
                                                      i4LldpRemLocalPortNum,
                                                      INT4 i4LldpRemIndex)
{

    if (LldpMedUtlValIndexRemCapInvTbl (u4LldpRemTimeMark,
                                        i4LldpRemLocalPortNum,
                                        LLDP_V1_DEST_MAC_ADDR_INDEX
                                        (i4LldpRemLocalPortNum),
                                        i4LldpRemIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXMedRemCapabilitiesTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXMedRemCapabilitiesTable (UINT4 *pu4LldpRemTimeMark,
                                              INT4 *pi4LldpRemLocalPortNum,
                                              INT4 *pi4LldpRemIndex)
{
    UINT4               u4NextDestIndex = 0;

    if (LldpMedUtlGetFirstIndexRemCapTbl (pu4LldpRemTimeMark,
                                          pi4LldpRemLocalPortNum,
                                          &u4NextDestIndex,
                                          pi4LldpRemIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXMedRemCapabilitiesTable
 Input       :  The Indices
                LldpRemTimeMark
                nextLldpRemTimeMark
                LldpRemLocalPortNum
                nextLldpRemLocalPortNum
                LldpRemIndex
                nextLldpRemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXMedRemCapabilitiesTable (UINT4 u4LldpRemTimeMark, UINT4
                                             *pu4NextLldpRemTimeMark,
                                             INT4 i4LldpRemLocalPortNum,
                                             INT4 *pi4NextLldpRemLocalPortNum,
                                             INT4 i4LldpRemIndex,
                                             INT4 *pi4NextLldpRemIndex)
{

    UINT4               u4NextDestIndex = 0;

    if (LldpMedUtlGetNextIndexRemCapTbl (u4LldpRemTimeMark,
                                         pu4NextLldpRemTimeMark,
                                         i4LldpRemLocalPortNum,
                                         pi4NextLldpRemLocalPortNum,
                                         LLDP_V1_DEST_MAC_ADDR_INDEX
                                         (i4LldpRemLocalPortNum),
                                         &u4NextDestIndex,
                                         i4LldpRemIndex,
                                         pi4NextLldpRemIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXMedRemCapSupported
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXMedRemCapSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemCapSupported (UINT4 u4LldpRemTimeMark, INT4
                               i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValLldpXMedRemCapSupported)
{

    if (LldpMedUtlGetRemCapSupported (u4LldpRemTimeMark,
                                      i4LldpRemLocalPortNum,
                                      LLDP_V1_DEST_MAC_ADDR_INDEX
                                      (i4LldpRemLocalPortNum),
                                      i4LldpRemIndex,
                                      pRetValLldpXMedRemCapSupported) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedRemCapCurrent
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXMedRemCapCurrent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemCapCurrent (UINT4 u4LldpRemTimeMark, INT4
                             i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValLldpXMedRemCapCurrent)
{

    if (LldpMedUtlGetRemCapCurrent (u4LldpRemTimeMark,
                                    i4LldpRemLocalPortNum,
                                    LLDP_V1_DEST_MAC_ADDR_INDEX
                                    (i4LldpRemLocalPortNum),
                                    i4LldpRemIndex,
                                    pRetValLldpXMedRemCapCurrent) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedRemDeviceClass
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXMedRemDeviceClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemDeviceClass (UINT4 u4LldpRemTimeMark, INT4
                              i4LldpRemLocalPortNum, INT4 i4LldpRemIndex, INT4
                              *pi4RetValLldpXMedRemDeviceClass)
{

    if (LldpMedUtlGetRemDeviceClass (u4LldpRemTimeMark,
                                     i4LldpRemLocalPortNum,
                                     LLDP_V1_DEST_MAC_ADDR_INDEX
                                     (i4LldpRemLocalPortNum),
                                     i4LldpRemIndex,
                                     pi4RetValLldpXMedRemDeviceClass) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXMedRemMediaPolicyTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXMedRemMediaPolicyTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpXMedRemMediaPolicyAppType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXMedRemMediaPolicyTable (UINT4
                                                     u4LldpRemTimeMark,
                                                     INT4 i4LldpRemLocalPortNum,
                                                     INT4 i4LldpRemIndex,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pLldpXMedRemMediaPolicyAppType)
{

    if (LldpMedUtlValIndexRemNwPolTbl
        (u4LldpRemTimeMark,
         i4LldpRemLocalPortNum,
         LLDP_V1_DEST_MAC_ADDR_INDEX
         (i4LldpRemLocalPortNum),
         i4LldpRemIndex, pLldpXMedRemMediaPolicyAppType) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXMedRemMediaPolicyTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpXMedRemMediaPolicyAppType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXMedRemMediaPolicyTable (UINT4 *pu4LldpRemTimeMark,
                                             INT4 *pi4LldpRemLocalPortNum,
                                             INT4 *pi4LldpRemIndex,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pLldpXMedRemMediaPolicyAppType)
{

    UINT4               u4NextDestIndex = 0;

    if (LldpMedUtlGetFirstIndexRemNwPolTbl
        (pu4LldpRemTimeMark,
         pi4LldpRemLocalPortNum,
         &u4NextDestIndex,
         pi4LldpRemIndex, pLldpXMedRemMediaPolicyAppType) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXMedRemMediaPolicyTable
 Input       :  The Indices
                LldpRemTimeMark
                nextLldpRemTimeMark
                LldpRemLocalPortNum
                nextLldpRemLocalPortNum
                LldpRemIndex
                nextLldpRemIndex
                LldpXMedRemMediaPolicyAppType
                nextLldpXMedRemMediaPolicyAppType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXMedRemMediaPolicyTable (UINT4 u4LldpRemTimeMark, UINT4
                                            *pu4NextLldpRemTimeMark,
                                            INT4 i4LldpRemLocalPortNum,
                                            INT4 *pi4NextLldpRemLocalPortNum,
                                            INT4 i4LldpRemIndex,
                                            INT4 *pi4NextLldpRemIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pLldpXMedRemMediaPolicyAppType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pNextLldpXMedRemMediaPolicyAppType)
{

    UINT4               u4NextDestIndex = 0;

    if (LldpMedUtlGetNextIndexRemNwPolTbl
        (u4LldpRemTimeMark,
         pu4NextLldpRemTimeMark,
         i4LldpRemLocalPortNum,
         pi4NextLldpRemLocalPortNum,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum),
         &u4NextDestIndex,
         i4LldpRemIndex,
         pi4NextLldpRemIndex,
         pLldpXMedRemMediaPolicyAppType,
         pNextLldpXMedRemMediaPolicyAppType) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXMedRemMediaPolicyVlanID
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpXMedRemMediaPolicyAppType

                The Object 
                retValLldpXMedRemMediaPolicyVlanID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemMediaPolicyVlanID (UINT4 u4LldpRemTimeMark, INT4
                                    i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pLldpXMedRemMediaPolicyAppType,
                                    INT4 *pi4RetValLldpXMedRemMediaPolicyVlanID)
{
    if (LldpMedUtlGetRemNwPolVlanID
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum),
         i4LldpRemIndex, pLldpXMedRemMediaPolicyAppType,
         pi4RetValLldpXMedRemMediaPolicyVlanID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedRemMediaPolicyPriority
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpXMedRemMediaPolicyAppType

                The Object 
                retValLldpXMedRemMediaPolicyPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemMediaPolicyPriority (UINT4 u4LldpRemTimeMark, INT4
                                      i4LldpRemLocalPortNum,
                                      INT4 i4LldpRemIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pLldpXMedRemMediaPolicyAppType,
                                      INT4
                                      *pi4RetValLldpXMedRemMediaPolicyPriority)
{

    if (LldpMedUtlGetRemNwPolPriority
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum),
         i4LldpRemIndex, pLldpXMedRemMediaPolicyAppType,
         pi4RetValLldpXMedRemMediaPolicyPriority) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedRemMediaPolicyDscp
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpXMedRemMediaPolicyAppType

                The Object 
                retValLldpXMedRemMediaPolicyDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemMediaPolicyDscp (UINT4 u4LldpRemTimeMark, INT4
                                  i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pLldpXMedRemMediaPolicyAppType,
                                  INT4 *pi4RetValLldpXMedRemMediaPolicyDscp)
{

    if (LldpMedUtlGetRemNwPolDscp (u4LldpRemTimeMark,
                                   i4LldpRemLocalPortNum,
                                   LLDP_V1_DEST_MAC_ADDR_INDEX
                                   (i4LldpRemLocalPortNum), i4LldpRemIndex,
                                   pLldpXMedRemMediaPolicyAppType,
                                   pi4RetValLldpXMedRemMediaPolicyDscp) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedRemMediaPolicyUnknown
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpXMedRemMediaPolicyAppType

                The Object 
                retValLldpXMedRemMediaPolicyUnknown
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemMediaPolicyUnknown (UINT4 u4LldpRemTimeMark, INT4
                                     i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pLldpXMedRemMediaPolicyAppType,
                                     INT4
                                     *pi4RetValLldpXMedRemMediaPolicyUnknown)
{
    if (LldpMedUtlGetRemNwPolUnknown (u4LldpRemTimeMark,
                                      i4LldpRemLocalPortNum,
                                      LLDP_V1_DEST_MAC_ADDR_INDEX
                                      (i4LldpRemLocalPortNum), i4LldpRemIndex,
                                      pLldpXMedRemMediaPolicyAppType,
                                      pi4RetValLldpXMedRemMediaPolicyUnknown) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedRemMediaPolicyTagged
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpXMedRemMediaPolicyAppType

                The Object 
                retValLldpXMedRemMediaPolicyTagged
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemMediaPolicyTagged (UINT4 u4LldpRemTimeMark, INT4
                                    i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pLldpXMedRemMediaPolicyAppType,
                                    INT4 *pi4RetValLldpXMedRemMediaPolicyTagged)
{

    if (LldpMedUtlGetRemNwPolTagged (u4LldpRemTimeMark,
                                     i4LldpRemLocalPortNum,
                                     LLDP_V1_DEST_MAC_ADDR_INDEX
                                     (i4LldpRemLocalPortNum), i4LldpRemIndex,
                                     pLldpXMedRemMediaPolicyAppType,
                                     pi4RetValLldpXMedRemMediaPolicyTagged) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXMedRemInventoryTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXMedRemInventoryTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXMedRemInventoryTable (UINT4 u4LldpRemTimeMark,
                                                   INT4 i4LldpRemLocalPortNum,
                                                   INT4 i4LldpRemIndex)
{

    if (LldpMedUtlValIndexRemCapInvTbl (u4LldpRemTimeMark,
                                        i4LldpRemLocalPortNum,
                                        LLDP_V1_DEST_MAC_ADDR_INDEX
                                        (i4LldpRemLocalPortNum),
                                        i4LldpRemIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXMedRemInventoryTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXMedRemInventoryTable (UINT4 *pu4LldpRemTimeMark, INT4
                                           *pi4LldpRemLocalPortNum,
                                           INT4 *pi4LldpRemIndex)
{
    UINT4               u4DestMacIndex = 0;
    if (LldpMedUtlGetFirstIndexRemInvTbl (pu4LldpRemTimeMark,
                                          pi4LldpRemLocalPortNum,
                                          &u4DestMacIndex,
                                          pi4LldpRemIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXMedRemInventoryTable
 Input       :  The Indices
                LldpRemTimeMark
                nextLldpRemTimeMark
                LldpRemLocalPortNum
                nextLldpRemLocalPortNum
                LldpRemIndex
                nextLldpRemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXMedRemInventoryTable (UINT4 u4LldpRemTimeMark, UINT4
                                          *pu4NextLldpRemTimeMark,
                                          INT4 i4LldpRemLocalPortNum,
                                          INT4 *pi4NextLldpRemLocalPortNum,
                                          INT4 i4LldpRemIndex,
                                          INT4 *pi4NextLldpRemIndex)
{

    UINT4               u4DestMacIndex = 0;

    if (LldpMedUtlGetNextIndexRemInvTbl (u4LldpRemTimeMark,
                                         pu4NextLldpRemTimeMark,
                                         i4LldpRemLocalPortNum,
                                         pi4NextLldpRemLocalPortNum,
                                         LLDP_V1_DEST_MAC_ADDR_INDEX
                                         (i4LldpRemLocalPortNum),
                                         &u4DestMacIndex,
                                         i4LldpRemIndex,
                                         pi4NextLldpRemIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXMedRemHardwareRev
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXMedRemHardwareRev
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemHardwareRev (UINT4 u4LldpRemTimeMark, INT4
                              i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValLldpXMedRemHardwareRev)
{

    if (LldpMedUtlGetRemHardwareRev (u4LldpRemTimeMark, i4LldpRemLocalPortNum,
                                     LLDP_V1_DEST_MAC_ADDR_INDEX
                                     (i4LldpRemLocalPortNum), i4LldpRemIndex,
                                     pRetValLldpXMedRemHardwareRev) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedRemFirmwareRev
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXMedRemFirmwareRev
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemFirmwareRev (UINT4 u4LldpRemTimeMark, INT4
                              i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValLldpXMedRemFirmwareRev)
{

    if (LldpMedUtlGetRemFirmwareRev (u4LldpRemTimeMark, i4LldpRemLocalPortNum,
                                     LLDP_V1_DEST_MAC_ADDR_INDEX
                                     (i4LldpRemLocalPortNum),
                                     i4LldpRemIndex,
                                     pRetValLldpXMedRemFirmwareRev) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedRemSoftwareRev
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXMedRemSoftwareRev
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemSoftwareRev (UINT4 u4LldpRemTimeMark, INT4
                              i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValLldpXMedRemSoftwareRev)
{

    if (LldpMedUtlGetRemSoftwareRev (u4LldpRemTimeMark,
                                     i4LldpRemLocalPortNum,
                                     LLDP_V1_DEST_MAC_ADDR_INDEX
                                     (i4LldpRemLocalPortNum), i4LldpRemIndex,
                                     pRetValLldpXMedRemSoftwareRev) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedRemSerialNum
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXMedRemSerialNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemSerialNum (UINT4 u4LldpRemTimeMark, INT4
                            i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValLldpXMedRemSerialNum)
{

    if (LldpMedUtlGetRemSerialNum (u4LldpRemTimeMark,
                                   i4LldpRemLocalPortNum,
                                   LLDP_V1_DEST_MAC_ADDR_INDEX
                                   (i4LldpRemLocalPortNum), i4LldpRemIndex,
                                   pRetValLldpXMedRemSerialNum) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedRemMfgName
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXMedRemMfgName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemMfgName (UINT4 u4LldpRemTimeMark, INT4
                          i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValLldpXMedRemMfgName)
{

    if (LldpMedUtlGetRemMfgName (u4LldpRemTimeMark,
                                 i4LldpRemLocalPortNum,
                                 LLDP_V1_DEST_MAC_ADDR_INDEX
                                 (i4LldpRemLocalPortNum), i4LldpRemIndex,
                                 pRetValLldpXMedRemMfgName) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedRemModelName
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXMedRemModelName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemModelName (UINT4 u4LldpRemTimeMark, INT4
                            i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValLldpXMedRemModelName)
{

    if (LldpMedUtlGetRemModelName (u4LldpRemTimeMark,
                                   i4LldpRemLocalPortNum,
                                   LLDP_V1_DEST_MAC_ADDR_INDEX
                                   (i4LldpRemLocalPortNum), i4LldpRemIndex,
                                   pRetValLldpXMedRemModelName) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedRemAssetID
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXMedRemAssetID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemAssetID (UINT4 u4LldpRemTimeMark, INT4
                          i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValLldpXMedRemAssetID)
{

    if (LldpMedUtlGetRemAssetID (u4LldpRemTimeMark, i4LldpRemLocalPortNum,
                                 LLDP_V1_DEST_MAC_ADDR_INDEX
                                 (i4LldpRemLocalPortNum), i4LldpRemIndex,
                                 pRetValLldpXMedRemAssetID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXMedRemLocationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXMedRemLocationTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpXMedRemLocationSubtype
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXMedRemLocationTable (UINT4 u4LldpRemTimeMark,
                                                  INT4 i4LldpRemLocalPortNum,
                                                  INT4 i4LldpRemIndex,
                                                  INT4
                                                  i4LldpXMedRemLocationSubtype)
{

    if (LldpMedUtlValIndexRemLocationTbl
        (u4LldpRemTimeMark,
         i4LldpRemLocalPortNum,
         LLDP_V1_DEST_MAC_ADDR_INDEX
         (i4LldpRemLocalPortNum),
         i4LldpRemIndex, i4LldpXMedRemLocationSubtype) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXMedRemLocationTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpXMedRemLocationSubtype
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXMedRemLocationTable (UINT4 *pu4LldpRemTimeMark, INT4
                                          *pi4LldpRemLocalPortNum,
                                          INT4 *pi4LldpRemIndex,
                                          INT4 *pi4LldpXMedRemLocationSubtype)
{

    UINT4               u4NextDestIndex = 0;

    if (LldpMedUtlGetFirstIndexRemLocationTbl
        (pu4LldpRemTimeMark,
         pi4LldpRemLocalPortNum,
         &u4NextDestIndex,
         pi4LldpRemIndex, pi4LldpXMedRemLocationSubtype) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXMedRemLocationTable
 Input       :  The Indices
                LldpRemTimeMark
                nextLldpRemTimeMark
                LldpRemLocalPortNum
                nextLldpRemLocalPortNum
                LldpRemIndex
                nextLldpRemIndex
                LldpXMedRemLocationSubtype
                nextLldpXMedRemLocationSubtype
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXMedRemLocationTable (UINT4 u4LldpRemTimeMark, UINT4
                                         *pu4NextLldpRemTimeMark,
                                         INT4 i4LldpRemLocalPortNum,
                                         INT4 *pi4NextLldpRemLocalPortNum,
                                         INT4 i4LldpRemIndex,
                                         INT4 *pi4NextLldpRemIndex,
                                         INT4 i4LldpXMedRemLocationSubtype,
                                         INT4
                                         *pi4NextLldpXMedRemLocationSubtype)
{

    UINT4               u4NextDestIndex = 0;

    if (LldpMedUtlGetNextIndexRemLocationTbl
        (u4LldpRemTimeMark,
         pu4NextLldpRemTimeMark,
         i4LldpRemLocalPortNum,
         pi4NextLldpRemLocalPortNum,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum),
         &u4NextDestIndex,
         i4LldpRemIndex,
         pi4NextLldpRemIndex,
         i4LldpXMedRemLocationSubtype,
         pi4NextLldpXMedRemLocationSubtype) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXMedRemLocationInfo
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpXMedRemLocationSubtype

                The Object 
                retValLldpXMedRemLocationInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemLocationInfo (UINT4 u4LldpRemTimeMark, INT4
                               i4LldpRemLocalPortNum, INT4 i4LldpRemIndex, INT4
                               i4LldpXMedRemLocationSubtype,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValLldpXMedRemLocationInfo)
{
    if (LldpMedUtlGetRemLocationInfo
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum),
         i4LldpRemIndex, i4LldpXMedRemLocationSubtype,
         pRetValLldpXMedRemLocationInfo) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXMedRemXPoETable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXMedRemXPoETable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXMedRemXPoETable (UINT4 u4LldpRemTimeMark,
                                              INT4 i4LldpRemLocalPortNum,
                                              INT4 i4LldpRemIndex)
{

    if (LldpMedUtlValIndexRemCapInvTbl (u4LldpRemTimeMark,
                                        i4LldpRemLocalPortNum,
                                        LLDP_V1_DEST_MAC_ADDR_INDEX
                                        (i4LldpRemLocalPortNum),
                                        i4LldpRemIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXMedRemXPoETable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXMedRemXPoETable (UINT4 *pu4LldpRemTimeMark, INT4
                                      *pi4LldpRemLocalPortNum,
                                      INT4 *pi4LldpRemIndex)
{
    UINT4               u4NextDestIndex = 0;

    if (LldpMedUtlGetFirstIndexRemCapTbl (pu4LldpRemTimeMark,
                                          pi4LldpRemLocalPortNum,
                                          &u4NextDestIndex,
                                          pi4LldpRemIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXMedRemXPoETable
 Input       :  The Indices
                LldpRemTimeMark
                nextLldpRemTimeMark
                LldpRemLocalPortNum
                nextLldpRemLocalPortNum
                LldpRemIndex
                nextLldpRemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXMedRemXPoETable (UINT4 u4LldpRemTimeMark, UINT4
                                     *pu4NextLldpRemTimeMark,
                                     INT4 i4LldpRemLocalPortNum,
                                     INT4 *pi4NextLldpRemLocalPortNum,
                                     INT4 i4LldpRemIndex,
                                     INT4 *pi4NextLldpRemIndex)
{
    UINT4               u4NextDestIndex = 0;

    if (LldpMedUtlGetNextIndexRemCapTbl (u4LldpRemTimeMark,
                                         pu4NextLldpRemTimeMark,
                                         i4LldpRemLocalPortNum,
                                         pi4NextLldpRemLocalPortNum,
                                         LLDP_V1_DEST_MAC_ADDR_INDEX
                                         (i4LldpRemLocalPortNum),
                                         &u4NextDestIndex,
                                         i4LldpRemIndex,
                                         pi4NextLldpRemIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXMedRemXPoEDeviceType
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXMedRemXPoEDeviceType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemXPoEDeviceType (UINT4 u4LldpRemTimeMark, INT4
                                 i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                                 INT4 *pi4RetValLldpXMedRemXPoEDeviceType)
{
    if (LldpMedUtlGetRemPoEDeviceType (u4LldpRemTimeMark,
                                       i4LldpRemLocalPortNum,
                                       LLDP_V1_DEST_MAC_ADDR_INDEX
                                       (i4LldpRemLocalPortNum),
                                       i4LldpRemIndex,
                                       pi4RetValLldpXMedRemXPoEDeviceType) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXMedRemXPoEPSETable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXMedRemXPoEPSETable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXMedRemXPoEPSETable (UINT4 u4LldpRemTimeMark,
                                                 INT4 i4LldpRemLocalPortNum,
                                                 INT4 i4LldpRemIndex)
{
    UNUSED_PARAM (u4LldpRemTimeMark);
    UNUSED_PARAM (i4LldpRemLocalPortNum);
    UNUSED_PARAM (i4LldpRemIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXMedRemXPoEPSETable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXMedRemXPoEPSETable (UINT4 *pu4LldpRemTimeMark, INT4
                                         *pi4LldpRemLocalPortNum,
                                         INT4 *pi4LldpRemIndex)
{
    UNUSED_PARAM (pu4LldpRemTimeMark);
    UNUSED_PARAM (pi4LldpRemLocalPortNum);
    UNUSED_PARAM (pi4LldpRemIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXMedRemXPoEPSETable
 Input       :  The Indices
                LldpRemTimeMark
                nextLldpRemTimeMark
                LldpRemLocalPortNum
                nextLldpRemLocalPortNum
                LldpRemIndex
                nextLldpRemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXMedRemXPoEPSETable (UINT4 u4LldpRemTimeMark, UINT4
                                        *pu4NextLldpRemTimeMark,
                                        INT4 i4LldpRemLocalPortNum,
                                        INT4 *pi4NextLldpRemLocalPortNum,
                                        INT4 i4LldpRemIndex,
                                        INT4 *pi4NextLldpRemIndex)
{
    UNUSED_PARAM (u4LldpRemTimeMark);
    UNUSED_PARAM (pu4NextLldpRemTimeMark);
    UNUSED_PARAM (i4LldpRemLocalPortNum);
    UNUSED_PARAM (pi4NextLldpRemLocalPortNum);
    UNUSED_PARAM (i4LldpRemIndex);
    UNUSED_PARAM (pi4NextLldpRemIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXMedRemXPoEPSEPowerAv
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXMedRemXPoEPSEPowerAv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemXPoEPSEPowerAv (UINT4 u4LldpRemTimeMark, INT4
                                 i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                                 UINT4 *pu4RetValLldpXMedRemXPoEPSEPowerAv)
{
    UNUSED_PARAM (u4LldpRemTimeMark);
    UNUSED_PARAM (i4LldpRemLocalPortNum);
    UNUSED_PARAM (i4LldpRemIndex);
    UNUSED_PARAM (pu4RetValLldpXMedRemXPoEPSEPowerAv);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedRemXPoEPSEPowerSource
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXMedRemXPoEPSEPowerSource
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemXPoEPSEPowerSource (UINT4 u4LldpRemTimeMark, INT4
                                     i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                                     INT4
                                     *pi4RetValLldpXMedRemXPoEPSEPowerSource)
{
    UNUSED_PARAM (u4LldpRemTimeMark);
    UNUSED_PARAM (i4LldpRemLocalPortNum);
    UNUSED_PARAM (i4LldpRemIndex);
    UNUSED_PARAM (pi4RetValLldpXMedRemXPoEPSEPowerSource);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedRemXPoEPSEPowerPriority
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXMedRemXPoEPSEPowerPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemXPoEPSEPowerPriority (UINT4 u4LldpRemTimeMark, INT4
                                       i4LldpRemLocalPortNum,
                                       INT4 i4LldpRemIndex,
                                       INT4
                                       *pi4RetValLldpXMedRemXPoEPSEPowerPriority)
{
    UNUSED_PARAM (u4LldpRemTimeMark);
    UNUSED_PARAM (i4LldpRemLocalPortNum);
    UNUSED_PARAM (i4LldpRemIndex);
    UNUSED_PARAM (pi4RetValLldpXMedRemXPoEPSEPowerPriority);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXMedRemXPoEPDTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXMedRemXPoEPDTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXMedRemXPoEPDTable (UINT4 u4LldpRemTimeMark,
                                                INT4 i4LldpRemLocalPortNum,
                                                INT4 i4LldpRemIndex)
{
    if (LldpMedUtlValIndexRemCapInvTbl (u4LldpRemTimeMark,
                                        i4LldpRemLocalPortNum,
                                        LLDP_V1_DEST_MAC_ADDR_INDEX
                                        (i4LldpRemLocalPortNum),
                                        i4LldpRemIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXMedRemXPoEPDTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXMedRemXPoEPDTable (UINT4 *pu4LldpRemTimeMark, INT4
                                        *pi4LldpRemLocalPortNum,
                                        INT4 *pi4LldpRemIndex)
{
    UINT4               u4NextDestIndex = 0;

    if (LldpMedUtlGetFirstIndexRemCapTbl (pu4LldpRemTimeMark,
                                          pi4LldpRemLocalPortNum,
                                          &u4NextDestIndex,
                                          pi4LldpRemIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXMedRemXPoEPDTable
 Input       :  The Indices
                LldpRemTimeMark
                nextLldpRemTimeMark
                LldpRemLocalPortNum
                nextLldpRemLocalPortNum
                LldpRemIndex
                nextLldpRemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXMedRemXPoEPDTable (UINT4 u4LldpRemTimeMark, UINT4
                                       *pu4NextLldpRemTimeMark,
                                       INT4 i4LldpRemLocalPortNum,
                                       INT4 *pi4NextLldpRemLocalPortNum,
                                       INT4 i4LldpRemIndex,
                                       INT4 *pi4NextLldpRemIndex)
{
    UINT4               u4NextDestIndex = 0;

    if (LldpMedUtlGetNextIndexRemCapTbl (u4LldpRemTimeMark,
                                         pu4NextLldpRemTimeMark,
                                         i4LldpRemLocalPortNum,
                                         pi4NextLldpRemLocalPortNum,
                                         LLDP_V1_DEST_MAC_ADDR_INDEX
                                         (i4LldpRemLocalPortNum),
                                         &u4NextDestIndex,
                                         i4LldpRemIndex,
                                         pi4NextLldpRemIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXMedRemXPoEPDPowerReq
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXMedRemXPoEPDPowerReq
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemXPoEPDPowerReq (UINT4 u4LldpRemTimeMark, INT4
                                 i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                                 UINT4 *pu4RetValLldpXMedRemXPoEPDPowerReq)
{
    if (LldpMedUtlGetRemPoEPDPowerReq (u4LldpRemTimeMark,
                                       i4LldpRemLocalPortNum,
                                       LLDP_V1_DEST_MAC_ADDR_INDEX
                                       (i4LldpRemLocalPortNum),
                                       i4LldpRemIndex,
                                       pu4RetValLldpXMedRemXPoEPDPowerReq) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedRemXPoEPDPowerSource
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXMedRemXPoEPDPowerSource
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemXPoEPDPowerSource (UINT4 u4LldpRemTimeMark, INT4
                                    i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                                    INT4 *pi4RetValLldpXMedRemXPoEPDPowerSource)
{
    if (LldpMedUtlGetRemPoEPDPowerSource (u4LldpRemTimeMark,
                                          i4LldpRemLocalPortNum,
                                          LLDP_V1_DEST_MAC_ADDR_INDEX
                                          (i4LldpRemLocalPortNum),
                                          i4LldpRemIndex,
                                          pi4RetValLldpXMedRemXPoEPDPowerSource)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXMedRemXPoEPDPowerPriority
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpXMedRemXPoEPDPowerPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXMedRemXPoEPDPowerPriority (UINT4 u4LldpRemTimeMark, INT4
                                      i4LldpRemLocalPortNum,
                                      INT4 i4LldpRemIndex,
                                      INT4
                                      *pi4RetValLldpXMedRemXPoEPDPowerPriority)
{
    if (LldpMedUtlGetRemPoEPDPowerPriority (u4LldpRemTimeMark,
                                            i4LldpRemLocalPortNum,
                                            LLDP_V1_DEST_MAC_ADDR_INDEX
                                            (i4LldpRemLocalPortNum),
                                            i4LldpRemIndex,
                                            pi4RetValLldpXMedRemXPoEPDPowerPriority)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
