/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldutil.c,v 1.83 2017/12/16 11:48:18 siva Exp $
 *
 * Description: This file contains the utility procedures used by LLDP 
 *              module.
 *********************************************************************/
#ifndef _LLDPUTIL_C_
#define _LLDPUTIL_C_

#include "lldinc.h"

PRIVATE INT4 LldpUtlRBFreeAppl PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
PRIVATE INT4 LldpMedUtlRBFreeLocPolicy PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
PRIVATE INT4 LldpMedUtlRBFreeRemPolicy PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
PRIVATE INT4        LldpMedUtlRBFreeLocLocation
PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
PRIVATE INT4        LldpMedUtlRBFreeRemLocation
PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
extern UINT4        LldpV2ManAddrConfigTxEnable[13];
extern UINT4        LldpV2ManAddrConfigRowStatus[13];
#ifdef DCBX_WANTED
extern VOID DcbxApiApplCallbkFunc PROTO ((tLldpAppTlv *));
#endif
extern INT4         DcbxApiVerifyTlvisDcbx (UINT2, UINT1, UINT1 *);
extern VOID         L2IwfResetAllVlanNameIfTxEnabled (VOID);
/****************************************************************************
 *
 *    FUNCTION NAME    : LldpLock
 *
 *    DESCRIPTION      : Function to take the mutual exclusion protocol
 *                       semaphore.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpLock (VOID)
{
    if (OsixSemTake (gLldpGlobalInfo.SemId) == OSIX_FAILURE)
    {
        LLDP_TRC (OS_RESOURCE_TRC | LLDP_CRITICAL_TRC,
                  "LldpLock: Take sem FAILED !!!\r\n");
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUnLock
 *
 *    DESCRIPTION      : Function to release the mutual exclusion protocol
 *                       semaphore.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpUnLock (VOID)
{
    OsixSemGive (gLldpGlobalInfo.SemId);
    return (SNMP_SUCCESS);
}

/******************************************************************************
 * Function Name      : LldpUtilPktFree
 *
 * Description        : Free CRU Buffer memory
 *
 * Input(s)           : pBuf - pointer to CRU Buffer
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
LldpUtilPktFree (tCRU_BUF_CHAIN_HEADER * pBuf)
{

    if (CRU_BUF_Release_MsgBufChain (pBuf, OSIX_FALSE) == CRU_FAILURE)
    {
        LLDP_TRC (BUFFER_TRC | ALL_FAILURE_TRC,
                  "LldpUtlPktFree: "
                  "Received frame CRU Buffer Release Failed\r\n");
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtilGetTraceOptionValue
 *
 *    DESCRIPTION      : This function process given trace input and sets the
 *                       corresponding option bit.
 *
 *    INPUT            : pu1TraceInput - trace string.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : Option Value.
 *
 ****************************************************************************/
PUBLIC UINT4
LldpUtilGetTraceOptionValue (UINT1 *pu1TraceInput, INT4 i4Tracelen)
{
#define LLDP_MAX_TRACE_TOKENS      35
#define LLDP_MAX_TRACE_TOKEN_SIZE  20
#define LLDP_TRACE_TOKEN_DELIMITER ' '    /* space */
    UINT1
         
         
                  aau1Tokens[LLDP_MAX_TRACE_TOKENS][LLDP_MAX_TRACE_TOKEN_SIZE];
    UINT1              *apu1Tokens[LLDP_MAX_TRACE_TOKENS];
    UINT1               u1TlvTokenPresent = OSIX_FALSE;
    UINT1               u1Count = 0;
    UINT1               u1TokenCount = 0;
    UINT4               u4TraceOption = LLDP_INVALID_TRC;

    MEMSET (aau1Tokens, 0, sizeof (aau1Tokens));

    /* Enable */
    if (!STRNCMP (pu1TraceInput, "enable ", STRLEN ("enable ")))
    {
        pu1TraceInput += STRLEN ("enable ");
        i4Tracelen -= STRLEN ("enable ");
    }
    /* Disable */
    else if (!STRNCMP (pu1TraceInput, "disable ", STRLEN ("disable ")))
    {
        pu1TraceInput += STRLEN ("disable ");
        i4Tracelen -= STRLEN ("disable ");
    }
    else
    {
        return LLDP_INVALID_TRC;
    }

    if (!STRNCMP (pu1TraceInput, "all", STRLEN ("all")))
    {
        /* For traces,
         * All trace         : all
         * All failure trace : all-fail 
         * the first 3 characters are common(all), so verify that
         * the trace length is equal to 3(strlen("all")), if yes then set
         * all tlv trace and return, else if the trace length is greater
         * than STRLEN ("all"), then verify whether the trace is "all-fail" 
         * or not. If the trace is not "all-fail" then the trace is considered
         * as invalid invalid trace so return invalid trace */
        if (i4Tracelen == STRLEN ("all"))
        {
            u4TraceOption = LLDP_ALL_TRC;
            return u4TraceOption;
        }
        else if (i4Tracelen > ((INT4) STRLEN ("all")))
        {
            if (STRNCMP (pu1TraceInput, "all-fail", STRLEN ("all-fail")) != 0)
            {
                return LLDP_INVALID_TRC;
            }
        }
    }

    /* assign memory address for all the pointers in token array(apu1Tokens) */
    for (u1Count = 0; u1Count < LLDP_MAX_TRACE_TOKENS; u1Count++)
    {
        apu1Tokens[u1Count] = aau1Tokens[u1Count];
    }

    /* get the tokens from the trace input buffer */
    LldpUtilSplitStrToTokens (pu1TraceInput, i4Tracelen,
                              (UINT1) LLDP_TRACE_TOKEN_DELIMITER,
                              (UINT2) LLDP_MAX_TRACE_TOKENS, apu1Tokens,
                              &u1TokenCount);

    /* get tokens one by one from the token array and set the 
     * trace options based on the tokens */
    for (u1Count = 0; ((u1Count < u1TokenCount) &&
                       (u1Count < LLDP_MAX_TRACE_TOKENS)); u1Count++)
    {
        /* token "tlv" is not a trace option so, if token "tlv" is 
         * found continue the iteration without setting the trace option */
        if (!STRNCMP (apu1Tokens[u1Count], "tlv", STRLEN ("tlv")))
        {
            /* if more than one tlv token present succesively in the trace 
             * string then return invalid trace option */
            if (u1TlvTokenPresent == OSIX_TRUE)
            {
                u4TraceOption = LLDP_INVALID_TRC;
                return u4TraceOption;
            }
            u1TlvTokenPresent = OSIX_TRUE;
            continue;
        }

        /* set the trace option based on the give token */
        LldpUtilSetTraceOption (apu1Tokens[u1Count], &u4TraceOption,
                                &u1TlvTokenPresent);
        /* if invalid trace option is returned by the function 
         * LldpUtilSetTraceOption, then dont continue the for loop, just
         * return with invalid trace option */
        if (u4TraceOption == LLDP_INVALID_TRC)
        {
            return u4TraceOption;
        }
    }
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
    {
        if ((u4TraceOption == LLDP_VID_DIGEST_TRC)
            || (u4TraceOption == LLDP_MGMT_VID_TRC))
        {
            printf
                ("%% Wrong Command these Options are supported in LLDPv2 Version\r\n");
            return LLDP_INVALID_TRC;
        }
    }
    return u4TraceOption;
}

/******************************************************************************
 * Function Name      : LldpUtilSetTraceOption 
 *
 * Description        : This function sets the trace option based on the given
 *                      trace token
 *
 * Input(s)           : apu1Token      - Poiner to token array 
 *                      pu4TraceOption - Pointer to trace option 
 *                      pu1TlvTokenPresent - Pointer to flag which indicates
 *                                           whether tlv token is present or not
 *
 * Output(s)          : pu4TraceOption - Pointer to trace option 
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
LldpUtilSetTraceOption (UINT1 *pu1Token, UINT4 *pu4TraceOption,
                        UINT1 *pu1TlvTokenPresent)
{
    if (!STRNCMP (pu1Token, "mgmt-vid", STRLEN ("mgmt-vid")))
    {
        if (*pu1TlvTokenPresent == OSIX_FALSE)
        {
            *pu4TraceOption = LLDP_INVALID_TRC;
            return;
        }
        *pu4TraceOption |= LLDP_MGMT_VID_TRC;
    }
    else if (!STRNCMP (pu1Token, "init-shut", STRLEN ("init-shut")))
    {
        *pu4TraceOption |= INIT_SHUT_TRC;
        /* reset the flag pu1TlvTokenPresent to OSIX_FALSE. The reason is 
         * consider the following scenario,
         * "enable tlv chassis-id ttl init-shut port-id"
         * in the above trace input port-id is a tlv trace but token "tlv" is
         * missing before port-id hence it is invalid trace. 
         * So, if the pu1TlvTokenPresent is not reset here it will be considered
         * as valid trace because pu1TlvTokenPresent is set to OSIX_TRUE
         * wnenever token "tlv" is read, so at this point of time 
         * pu1TlvTokenPresent is OSIX_TRUE. */
        *pu1TlvTokenPresent = OSIX_FALSE;
    }
    else if (!STRNCMP (pu1Token, "mgmt-addr", STRLEN ("mgmt-addr")))
    {
        *pu4TraceOption |= LLDP_MAN_ADDR_TRC;
    }
    else if (!STRNCMP (pu1Token, "mgmt", STRLEN ("mgmt")))
    {
        *pu4TraceOption |= MGMT_TRC;
        *pu1TlvTokenPresent = OSIX_FALSE;
    }
    else if (!STRNCMP (pu1Token, "data-path", STRLEN ("data-path")))
    {
        *pu4TraceOption |= DATA_PATH_TRC;
        *pu1TlvTokenPresent = OSIX_FALSE;
    }
    else if (!STRNCMP (pu1Token, "ctrl", STRLEN ("ctrl")))
    {
        *pu4TraceOption |= CONTROL_PLANE_TRC;
        *pu1TlvTokenPresent = OSIX_FALSE;
    }
    else if (!STRNCMP (pu1Token, "pkt-dump", STRLEN ("pkt-dump")))
    {
        *pu4TraceOption |= DUMP_TRC;
        *pu1TlvTokenPresent = OSIX_FALSE;
    }
    else if (!STRNCMP (pu1Token, "resource", STRLEN ("resource")))
    {
        *pu4TraceOption |= OS_RESOURCE_TRC;
        *pu1TlvTokenPresent = OSIX_FALSE;
    }
    else if (!STRNCMP (pu1Token, "all-fail", STRLEN ("all-fail")))
    {
        *pu4TraceOption |= ALL_FAILURE_TRC;
        *pu1TlvTokenPresent = OSIX_FALSE;
    }
    else if (!STRNCMP (pu1Token, "buf", STRLEN ("buf")))
    {
        *pu4TraceOption |= BUFFER_TRC;
        *pu1TlvTokenPresent = OSIX_FALSE;
    }
    else if (!STRNCMP (pu1Token, "critical", STRLEN ("critical")))
    {
        *pu4TraceOption |= LLDP_CRITICAL_TRC;
        *pu1TlvTokenPresent = OSIX_FALSE;
    }
    /* here token "all" is equal to "all tlv", so enable all tlv trace 
     * before setting tlv traces, verify whether tlv token present in the
     * trace string, if yes the set the tlv trace, otherwise return invalid
     * trace */
    else if (!STRNCMP (pu1Token, "all", STRLEN ("all")))
    {
        if (*pu1TlvTokenPresent == OSIX_FALSE)
        {
            *pu4TraceOption = LLDP_INVALID_TRC;
            return;
        }
        *pu4TraceOption |= LLDP_ALL_TLV_TRC;
    }
    else if (!STRNCMP (pu1Token, "mandatory-tlv", STRLEN ("mandatory-tlv")))
    {
        if (*pu1TlvTokenPresent == OSIX_FALSE)
        {
            *pu4TraceOption = LLDP_INVALID_TRC;
            return;
        }
        *pu4TraceOption |= LLDP_MANDATORY_TLV_TRC;
    }
    else if (!STRNCMP (pu1Token, "port-descr", STRLEN ("port-descr")))
    {
        if (*pu1TlvTokenPresent == OSIX_FALSE)
        {
            *pu4TraceOption = LLDP_INVALID_TRC;
            return;
        }
        *pu4TraceOption |= LLDP_PORT_DESC_TRC;
    }
    else if (!STRNCMP (pu1Token, "sys-name", STRLEN ("sys-name")))
    {
        if (*pu1TlvTokenPresent == OSIX_FALSE)
        {
            *pu4TraceOption = LLDP_INVALID_TRC;
            return;
        }
        *pu4TraceOption |= LLDP_SYS_NAME_TRC;
    }
    else if (!STRNCMP (pu1Token, "sys-descr", STRLEN ("sys-descr")))
    {
        if (*pu1TlvTokenPresent == OSIX_FALSE)
        {
            *pu4TraceOption = LLDP_INVALID_TRC;
            return;
        }
        *pu4TraceOption |= LLDP_SYS_DESC_TRC;
    }
    else if (!STRNCMP (pu1Token, "sys-capab", STRLEN ("sys-capab")))
    {
        if (*pu1TlvTokenPresent == OSIX_FALSE)
        {
            *pu4TraceOption = LLDP_INVALID_TRC;
            return;
        }
        *pu4TraceOption |= LLDP_SYS_CAPAB_TRC;
    }
    else if (!STRNCMP (pu1Token, "port-vlan", STRLEN ("port-vlan")))
    {
        if (*pu1TlvTokenPresent == OSIX_FALSE)
        {
            *pu4TraceOption = LLDP_INVALID_TRC;
            return;
        }
        *pu4TraceOption |= LLDP_PORT_VLAN_TRC;
    }
    else if (!STRNCMP (pu1Token, "ppvlan", STRLEN ("ppvlan")))
    {
        if (*pu1TlvTokenPresent == OSIX_FALSE)
        {
            *pu4TraceOption = LLDP_INVALID_TRC;
            return;
        }
        *pu4TraceOption |= LLDP_PPVLAN_TRC;
    }
    else if (!STRNCMP (pu1Token, "vlan-name", STRLEN ("vlan-name")))
    {
        if (*pu1TlvTokenPresent == OSIX_FALSE)
        {
            *pu4TraceOption = LLDP_INVALID_TRC;
            return;
        }
        *pu4TraceOption |= LLDP_VLAN_NAME_TRC;
    }
    else if (!STRNCMP (pu1Token, "proto-id", STRLEN ("proto-id")))
    {
        if (*pu1TlvTokenPresent == OSIX_FALSE)
        {
            *pu4TraceOption = LLDP_INVALID_TRC;
            return;
        }
        *pu4TraceOption |= LLDP_PROTO_ID_TRC;
    }
    else if (!STRNCMP (pu1Token, "mac-phy", STRLEN ("mac-phy")))
    {
        if (*pu1TlvTokenPresent == OSIX_FALSE)
        {
            *pu4TraceOption = LLDP_INVALID_TRC;
            return;
        }
        *pu4TraceOption |= LLDP_MAC_PHY_TRC;
    }
    else if (!STRNCMP (pu1Token, "pwr-mdi", STRLEN ("pwr-mdi")))
    {
        if (*pu1TlvTokenPresent == OSIX_FALSE)
        {
            *pu4TraceOption = LLDP_INVALID_TRC;
            return;
        }
        *pu4TraceOption |= LLDP_PWR_MDI_TRC;
    }
    else if (!STRNCMP (pu1Token, "lagg", STRLEN ("lagg")))
    {
        if (*pu1TlvTokenPresent == OSIX_FALSE)
        {
            *pu4TraceOption = LLDP_INVALID_TRC;
            return;
        }
        *pu4TraceOption |= LLDP_LAGG_TRC;
    }
    else if (!STRNCMP (pu1Token, "max-frame", STRLEN ("max-frame")))
    {
        if (*pu1TlvTokenPresent == OSIX_FALSE)
        {
            *pu4TraceOption = LLDP_INVALID_TRC;
            return;
        }
        *pu4TraceOption |= LLDP_MAX_FRAME_TRC;
    }
    else if (!STRNCMP (pu1Token, "vid-digest", STRLEN ("vid-digest")))
    {
        if (*pu1TlvTokenPresent == OSIX_FALSE)
        {
            *pu4TraceOption = LLDP_INVALID_TRC;
            return;
        }
        *pu4TraceOption |= LLDP_VID_DIGEST_TRC;
    }
    else if (!STRNCMP (pu1Token, "med-capability", STRLEN ("med-capability")))
    {
        if (*pu1TlvTokenPresent == OSIX_FALSE)
        {
            *pu4TraceOption = LLDP_INVALID_TRC;
            return;
        }
        *pu4TraceOption |= LLDP_MED_CAPAB_TRC;
    }
    else if (!STRNCMP (pu1Token, "network-policy", STRLEN ("network-policy")))
    {
        if (*pu1TlvTokenPresent == OSIX_FALSE)
        {
            *pu4TraceOption = LLDP_INVALID_TRC;
            return;
        }
        *pu4TraceOption |= LLDP_MED_NW_POL_TRC;
    }
    else if (!STRNCMP
             (pu1Token, "inventory-management",
              STRLEN ("inventory-management")))
    {
        if (*pu1TlvTokenPresent == OSIX_FALSE)
        {
            *pu4TraceOption = LLDP_INVALID_TRC;
            return;
        }
        *pu4TraceOption |= LLDP_MED_INV_MGMT_TRC;
    }
    else if (!STRNCMP (pu1Token, "location-id", STRLEN ("location-id")))
    {
        if (*pu1TlvTokenPresent == OSIX_FALSE)
        {
            *pu4TraceOption = LLDP_INVALID_TRC;
            return;
        }
        *pu4TraceOption |= LLDP_MED_LOC_ID_TRC;
    }
    else if (!STRNCMP (pu1Token, "ex-power-mdi", STRLEN ("ex-power-mdi")))
    {
        if (*pu1TlvTokenPresent == OSIX_FALSE)
        {
            *pu4TraceOption = LLDP_INVALID_TRC;
            return;
        }
        *pu4TraceOption |= LLDP_MED_EX_POW_TRC;
    }
    else if (!STRNCMP (pu1Token, "redundancy", STRLEN ("redundancy")))
    {
        *pu4TraceOption |= LLDP_RED_TRC;
        *pu1TlvTokenPresent = OSIX_FALSE;
    }
    else
    {
        *pu4TraceOption = LLDP_INVALID_TRC;
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtilGetTraceInputValue
 *
 *    DESCRIPTION      : This function gives the trace string depending upon the
 *                       trace option.
 *
 *    INPUT            : pu1TraceInput - trace string.
 *                       u4TraceOption  - trace option.
 *
 *    OUTPUT           : Gives the string format trace option.
 *
 *    RETURNS          : trace string length.
 *
 ****************************************************************************/

PUBLIC UINT4
LldpUtilGetTraceInputValue (UINT1 *pu1TraceInput, UINT4 u4TraceOption)
{
#define LLDP_TLV_TRC_BIT 15
    UINT1               u2BitNumber = 0;
    UINT1               u1MaxBits = 0;
    UINT4               u4TrcLen = 0;
    BOOL1               bTlvStrToBeAdded = OSIX_FALSE;

    /* store starting address of trace input buffer pointer */
    UINT1              *pu1TmpTraceInput = pu1TraceInput;

    /* trace string */
    UINT1               au1TraceString[32][22] =
        { " init-shut", " mgmt", " data-path", " ctrl", " pkt-dump",
        " resource", " all-fail", " buf", " inventory-management",
        " vid-digest",
        " mgmt-vid", " med-capability", " network-policy", " critical",
        " redundancy",
        " mandatory-tlv", " location-id", " ex-power-mdi", " port-descr",
        " sys-name",
        " sys-descr", " sys-capab", " mgmt-addr", " port-vlan", " ppvlan",
        " vlan-name", " proto-id", " mac-phy", " pwr-mdi", " lagg",
        " max-frame", " null"
    };

    /* All trace */
    if ((u4TraceOption & LLDP_ALL_TRC) == LLDP_ALL_TRC)
    {
        u4TrcLen = STRLEN ("all");
        STRNCPY (pu1TraceInput, "all", u4TrcLen);
        pu1TraceInput[u4TrcLen] = '\0';
        return u4TrcLen;
    }
    u1MaxBits = sizeof (u4TraceOption) * BITS_PER_BYTE;
    if (u4TraceOption >= LLDP_MANDATORY_TLV_TRC)
    {
        bTlvStrToBeAdded = OSIX_TRUE;
    }

    for (u2BitNumber = 0; u2BitNumber < u1MaxBits; u2BitNumber++)
    {
        if ((u4TraceOption >> u2BitNumber) & OSIX_TRUE)
        {
            /* if it is the first trace input string being copied to the
             * buffer, then " "(space) is not required in the beginning of the
             * string, so copy from 2nd byte of the string */
            if (pu1TraceInput == pu1TmpTraceInput)
            {
                /* if trace option has tlv trace enabled, then prepend string
                 * " tlv" before adding first tlv trace string to the trace 
                 * input */
                if ((bTlvStrToBeAdded == OSIX_TRUE) &&
                    ((u2BitNumber >= LLDP_TLV_TRC_BIT)
                     || ((u2BitNumber >= 8) && (u2BitNumber <= 12))))
                {
                    bTlvStrToBeAdded = OSIX_FALSE;
                    STRNCPY (pu1TraceInput, "tlv ", STRLEN ("tlv "));
                    pu1TraceInput = (pu1TraceInput + STRLEN ("tlv "));
                    u4TrcLen += STRLEN ("tlv ");
                    if ((u4TraceOption & LLDP_ALL_TLV_TRC) == LLDP_ALL_TLV_TRC)
                    {
                        STRNCPY (pu1TraceInput, "all", STRLEN ("all"));
                        pu1TraceInput = (pu1TraceInput + STRLEN ("all"));
                        u4TrcLen += STRLEN ("all");
                        break;
                    }
                }

                STRNCPY (pu1TraceInput, (au1TraceString[u2BitNumber] + 1),
                         (STRLEN (au1TraceString[u2BitNumber]) - 1));

                pu1TraceInput += (STRLEN (au1TraceString[u2BitNumber]) - 1);

                u4TrcLen += (STRLEN (au1TraceString[u2BitNumber]) - 1);
                continue;
            }

            /* if trace option has tlv trace enabled, then prepend string
             * " tlv" before adding first tlv trace string to the trace 
             * input */
            if ((bTlvStrToBeAdded == OSIX_TRUE) &&
                ((u2BitNumber >= LLDP_TLV_TRC_BIT)
                 || ((u2BitNumber >= 8) && (u2BitNumber <= 12))))
            {
                bTlvStrToBeAdded = OSIX_FALSE;
                STRNCPY (pu1TraceInput, " tlv", STRLEN (" tlv"));
                pu1TraceInput = (pu1TraceInput + STRLEN (" tlv"));
                u4TrcLen += STRLEN (" tlv");

                if ((u4TraceOption & LLDP_ALL_TLV_TRC) == LLDP_ALL_TLV_TRC)
                {
                    STRNCPY (pu1TraceInput, " all", STRLEN (" all"));
                    pu1TraceInput = (pu1TraceInput + STRLEN (" all"));
                    u4TrcLen += STRLEN (" all");
                    break;
                }
            }

            STRNCPY (pu1TraceInput, au1TraceString[u2BitNumber],
                     STRLEN (au1TraceString[u2BitNumber]));
            pu1TraceInput += STRLEN (au1TraceString[u2BitNumber]);
            u4TrcLen += STRLEN (au1TraceString[u2BitNumber]);
        }
    }

    return u4TrcLen;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtilClearStats
 *
 *    DESCRIPTION      : This function clears the LLDP counters
 *
 *    INPUT            : pPortInfo  - Local Port Entry
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/

PUBLIC VOID
LldpUtilClearStats (tLldpLocPortInfo * pPortInfo)
{
    LLDP_RESET_ALL_COUNTERS (pPortInfo);
    LLDP_RESET_CNTR_TX_FRAMES (pPortInfo);
    return;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpUtilCreateRBTree
 *
 *    DESCRIPTION      : Function Creates all RBtrees in LLDP module
 *                       following RBTrees are Created -
 *                       1. LocManAddrRBTree
 *                       2. LocProtoIdInfoRBTree                        
 *                       3. RemSysDataRBTree
 *                       4. RemMSAPRBTree
 *                       5. RemManAddrRBTree
 *                       6. RemOrgDefInfoRBTree
 *                       7. RemUnknownTLVRBTree
 *                       8. RemVlanNameInfoRBTree
 *                       9. RemProtoVlanRBTree
 *                       10. RemProtoIdRBTree
 *                       11. ApplnRBTree
 *                       12. LocPolicyInfoRBTree
 *                       13. RemPolicyInfoRBTree
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 ****************************************************************************/
PUBLIC VOID
LldpUtilCreateRBTree (VOID)
{
    UINT4               u4RBNodeOffset = 0;

    /* 1. Create LocManAddrRBTree */
    u4RBNodeOffset = FSAP_OFFSETOF (tLldpLocManAddrTable, LocManAddrRBNode);
    gLldpGlobalInfo.LocManAddrRBTree =
        RBTreeCreateEmbedded (u4RBNodeOffset, LldpTxUtlRBCmpLocManAddr);

    /* 2. Create LocProtoIdInfoRBTree */
    u4RBNodeOffset = FSAP_OFFSETOF (tLldpxdot1LocProtoIdInfo,
                                    NextLocProtoIdRBNode);
    gLldpGlobalInfo.LocProtoIdInfoRBTree =
        RBTreeCreateEmbedded (u4RBNodeOffset, LldpTxUtlRBCmpLocProtoIdInfo);

    /* PortVlanTable - RBTree is not created since this table is maintained in
     * l2iwf */

    /* 3. Create RemSysDataRBTree */
    u4RBNodeOffset = FSAP_OFFSETOF (tLldpRemoteNode, RemSysDataRBNode);
    gLldpGlobalInfo.RemSysDataRBTree =
        RBTreeCreateEmbedded (u4RBNodeOffset, LldpRxUtlRBCmpSysData);

    /* 4. Create RemMSAPRBTree */
    u4RBNodeOffset = FSAP_OFFSETOF (tLldpRemoteNode, RemMSAPRBNode);
    gLldpGlobalInfo.RemMSAPRBTree =
        RBTreeCreateEmbedded (u4RBNodeOffset, LldpRxUtlRBCmpMSAP);

    /* 5. Create RemManAddrRBTree */
    u4RBNodeOffset = FSAP_OFFSETOF (tLldpRemManAddressTable, RemManAddrRBNode);
    gLldpGlobalInfo.RemManAddrRBTree =
        RBTreeCreateEmbedded (u4RBNodeOffset, LldpRxUtlRBCmpManAddr);

    /* 6. Create RemOrgDefInfoRBTree */
    u4RBNodeOffset =
        FSAP_OFFSETOF (tLldpRemOrgDefInfoTable, RemOrgDefInfoRBNode);
    gLldpGlobalInfo.RemOrgDefInfoRBTree =
        RBTreeCreateEmbedded (u4RBNodeOffset, LldpRxUtlRBCmpOrgDefInfo);

    /* 7. Create RemUnknownTLVRBTree */
    u4RBNodeOffset =
        FSAP_OFFSETOF (tLldpRemUnknownTLVTable, RemUnknownTLVRBNode);
    gLldpGlobalInfo.RemUnknownTLVRBTree =
        RBTreeCreateEmbedded (u4RBNodeOffset, LldpRxUtlRBCmpUnknownTLV);

    /* 8. Create RemVlanNameInfoRBTree */
    u4RBNodeOffset =
        FSAP_OFFSETOF (tLldpxdot1RemVlanNameInfo, RemVlanNameRBNode);
    gLldpGlobalInfo.RemVlanNameInfoRBTree =
        RBTreeCreateEmbedded (u4RBNodeOffset, LldpRxUtlRBCmpVlanNameInfo);

    /* 9. Create RemProtoVlanRBTree */
    u4RBNodeOffset =
        FSAP_OFFSETOF (tLldpxdot1RemProtoVlanInfo, RemProtoVlanRBNode);
    gLldpGlobalInfo.RemProtoVlanRBTree =
        RBTreeCreateEmbedded (u4RBNodeOffset, LldpRxUtlRBCmpProtoVlanInfo);

    /* 10. Create RemProtoIdRBTree */
    u4RBNodeOffset = FSAP_OFFSETOF (tLldpxdot1RemProtoIdInfo, RemProtoIdRBNode);
    gLldpGlobalInfo.RemProtoIdRBTree =
        RBTreeCreateEmbedded (u4RBNodeOffset, LldpRxUtlRBCmpProtoIdInfo);

    /* 11. Create ApplnRBTree */
    u4RBNodeOffset = FSAP_OFFSETOF (tLldpAppInfo, AppRBNode);
    gLldpGlobalInfo.AppRBTree = RBTreeCreateEmbedded (u4RBNodeOffset,
                                                      LldpAppUtlRBCmpInfo);

    /* 12. Create PortInfoRBTree */
    u4RBNodeOffset = FSAP_OFFSETOF (tLldpLocPortTable, PortNode);
    gLldpGlobalInfo.LldpPortInfoRBTree = RBTreeCreateEmbedded (u4RBNodeOffset,
                                                               LldpPortInfoUtlRBCmpInfo);

    /* 13. Destination MAC address table RBTree */
    u4RBNodeOffset = FSAP_OFFSETOF (tLldpv2DestAddrTbl, DestMacAddrTblNode);
    gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree =
        RBTreeCreateEmbedded (u4RBNodeOffset, LldpDstMacAddrUtlRBCmpInfo);

    /* 14. Agent To Local PortMap table RBTree */
    u4RBNodeOffset =
        FSAP_OFFSETOF (tLldpv2AgentToLocPort, LldpAgentToLocPortNode);
    gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree =
        RBTreeCreateEmbedded (u4RBNodeOffset, LldpAgentToLocPortUtlRBCmpInfo);

    /* 15. Agent To Local PortMap table RBTree */
    u4RBNodeOffset =
        FSAP_OFFSETOF (tLldpv2AgentToLocPort, LldpAgentToLocPortIndexNode);
    gLldpGlobalInfo.Lldpv2AgentMapTblRBTree =
        RBTreeCreateEmbedded (u4RBNodeOffset,
                              LldpAgentToLocPortIndexUtlRBCmpInfo);

    /* 16. Agent RBTree */
    u4RBNodeOffset = FSAP_OFFSETOF (tLldpLocPortInfo, LocPortAgentRBNode);
    gLldpGlobalInfo.LldpLocPortAgentInfoRBTree =
        RBTreeCreateEmbedded (u4RBNodeOffset, LldpAgentInfoUtlRBCmpInfo);

    /* 17. Create RBTree for local Network Policy */
    u4RBNodeOffset = FSAP_OFFSETOF (tLldpMedLocNwPolicyInfo, LocPolicyNode);
    gLldpGlobalInfo.LldpMedLocNwPolicyInfoRBTree =
        RBTreeCreateEmbedded (u4RBNodeOffset, LldpMedLocNwPolicyInfoRBCmpInfo);

    /* 18. Create RBTree for Remote Network Policy */
    u4RBNodeOffset = FSAP_OFFSETOF (tLldpMedRemNwPolicyInfo, RemPolicyNode);
    gLldpGlobalInfo.LldpMedRemNwPolicyInfoRBTree =
        RBTreeCreateEmbedded (u4RBNodeOffset, LldpMedRemNwPolicyInfoRBCmpInfo);

    /* 19. Create RBTree for Local Location information */
    u4RBNodeOffset = FSAP_OFFSETOF (tLldpMedLocLocationInfo, LocLocationNode);
    gLldpGlobalInfo.LldpMedLocLocationInfoRBTree =
        RBTreeCreateEmbedded (u4RBNodeOffset, LldpMedLocLocationInfoRBCmpInfo);

    /* 20. Create RBTree for Remote Location information */
    u4RBNodeOffset = FSAP_OFFSETOF (tLldpMedRemLocationInfo, RemLocationNode);
    gLldpGlobalInfo.LldpMedRemLocationInfoRBTree =
        RBTreeCreateEmbedded (u4RBNodeOffset, LldpMedRemLocationInfoRBCmpInfo);

    return;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpUtilDeleteRBTree
 *
 *    DESCRIPTION      : Function Deletes all RBtrees in LLDP module
 *                       following RBTrees are Created -
 *                       1. LocManAddrRBTree
 *                       2. LocProtoIdInfoRBTree
 *                       3. RemSysDataRBTree
 *                       4. RemMSAPRBTree
 *                       5. RemManAddrRBTree
 *                       6. RemOrgDefInfoRBTree
 *                       7. RemUnknownTLVRBTree
 *                       8. RemVlanNameInfoRBTree
 *                       9. RemProtoVlanRBTree
 *                       10. RemProtoIdRBTree
 *                       11. ApplnRBTree
 *                       12. LocPolicyInfoRBTree
 *                       13. RemPolicyInfoRBTree
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 ****************************************************************************/
PUBLIC VOID
LldpUtilDeleteRBTree (VOID)
{
    /* 1. Delete LocManAddrRBTree */
    if (gLldpGlobalInfo.LocManAddrRBTree != 0)
    {
        RBTreeDestroy (gLldpGlobalInfo.LocManAddrRBTree,
                       LldpTxUtlRBFreeLocManAddr, 0);
    }

    /* 2. Delete LocProtoIdInfoRBTree */
    if (gLldpGlobalInfo.LocProtoIdInfoRBTree != 0)
    {
        RBTreeDestroy (gLldpGlobalInfo.LocProtoIdInfoRBTree,
                       LldpTxUtlRBFreeLocProtoId, 0);
    }

    /* PortVlanTable - RBTree is maintained in l2iwf, so no need to delete
     * here, reset the transmission of vlan name tlv */
    L2IwfResetAllVlanNameIfTxEnabled ();

    /* 3. Delete RemManAddrRBTree */
    if (gLldpGlobalInfo.RemManAddrRBTree != 0)
    {
        RBTreeDestroy (gLldpGlobalInfo.RemManAddrRBTree, LldpRxUtlRBFreeManAddr,
                       0);
    }

    /* 4. Delete RemOrgDefInfoRBTree */
    if (gLldpGlobalInfo.RemOrgDefInfoRBTree != 0)
    {
        RBTreeDestroy (gLldpGlobalInfo.RemOrgDefInfoRBTree,
                       LldpRxUtlRBFreeOrgDefInfo, 0);
    }

    /* 5. Delete RemUnknownTLVRBTree */
    if (gLldpGlobalInfo.RemUnknownTLVRBTree != 0)
    {
        RBTreeDestroy (gLldpGlobalInfo.RemUnknownTLVRBTree,
                       LldpRxUtlRBFreeUnknownTLV, 0);
    }

    /* 6. Delete RemVlanNameInfoRBTree */
    if (gLldpGlobalInfo.RemVlanNameInfoRBTree != 0)
    {
        RBTreeDestroy (gLldpGlobalInfo.RemVlanNameInfoRBTree,
                       LldpRxUtlRBFreeVlanNameInfo, 0);
    }

    /* 7. Delete RemProtoVlanRBTree */
    if (gLldpGlobalInfo.RemProtoVlanRBTree != 0)
    {
        RBTreeDestroy (gLldpGlobalInfo.RemProtoVlanRBTree,
                       LldpRxUtlRBFreeProtoVlanInfo, 0);
    }
    /* 8. Delete RemProtoIdRBTree */
    if (gLldpGlobalInfo.RemProtoIdRBTree != 0)
    {
        RBTreeDestroy (gLldpGlobalInfo.RemProtoIdRBTree,
                       LldpRxUtlRBFreeProtoIdInfo, 0);
    }
    /* 9. Delete RemSysDataRBTree */
    if (gLldpGlobalInfo.RemSysDataRBTree != 0)
    {
        RBTreeDestroy (gLldpGlobalInfo.RemSysDataRBTree, LldpRxUtlRBFreeSysData,
                       0);
    }

    /* 10. Delete RemMSAPRBTree */
    if (gLldpGlobalInfo.RemMSAPRBTree != 0)
    {
        RBTreeDestroy (gLldpGlobalInfo.RemMSAPRBTree, LldpRxUtlRBFreeMSAP, 0);
    }

    /* 11. Delete ApplnRBTree */
    if (gLldpGlobalInfo.AppRBTree != 0)
    {
        RBTreeDestroy (gLldpGlobalInfo.AppRBTree, LldpUtlRBFreeAppl, 0);
    }
    /* 12. Delete RBTree for local Network Policy */
    if (gLldpGlobalInfo.LldpMedLocNwPolicyInfoRBTree != 0)
    {
        RBTreeDestroy (gLldpGlobalInfo.LldpMedLocNwPolicyInfoRBTree,
                       LldpMedUtlRBFreeLocPolicy, 0);
    }
    /* 13. Delete RBTree for Remote Network Policy */
    if (gLldpGlobalInfo.LldpMedRemNwPolicyInfoRBTree != 0)
    {
        RBTreeDestroy (gLldpGlobalInfo.LldpMedRemNwPolicyInfoRBTree,
                       LldpMedUtlRBFreeRemPolicy, 0);
    }
    /* 14. Delete RBTree for local Location Information */
    if (gLldpGlobalInfo.LldpMedLocLocationInfoRBTree != 0)
    {
        RBTreeDestroy (gLldpGlobalInfo.LldpMedLocLocationInfoRBTree,
                       LldpMedUtlRBFreeLocLocation, 0);
    }
    /* 15. Delete RBTree for Remote Location Information */
    if (gLldpGlobalInfo.LldpMedRemLocationInfoRBTree != 0)
    {
        RBTreeDestroy (gLldpGlobalInfo.LldpMedRemLocationInfoRBTree,
                       LldpMedUtlRBFreeRemLocation, 0);
    }
    return;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpUtilGetManAddrLen
 *
 *    DESCRIPTION      : This function returns the length of the Management
 *                       Address based on the subtype. 
 *
 *    INPUT            : i4ManAddrSubtype - Subtype of Management address
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : Management Address Length
 ****************************************************************************/
PUBLIC UINT2
LldpUtilGetManAddrLen (INT4 i4ManAddrSubtype)
{
    UINT2               u2ManAddrLen = 0;
    switch (i4ManAddrSubtype)
    {
        case IPVX_ADDR_FMLY_IPV4:    /* Ipv4 Address */
            u2ManAddrLen = IPVX_IPV4_ADDR_LEN;
            break;
        case IPVX_ADDR_FMLY_IPV6:    /* Ipv6 Address */
            u2ManAddrLen = IPVX_IPV6_ADDR_LEN;
            break;
        default:
            u2ManAddrLen = LLDP_MAX_LEN_MAN_ADDR;
            break;
    }
    return u2ManAddrLen;
}

/*****************************************************************************
 *
 * Function     : LldpUtilGetChassisIdLen
 *
 * Description  : This function gets the chassis id length based on the given
 *                subtype
 *
 * Input        : i4Subtype - chassis id subtype
 *                pu1ChassId - pointer to chassis id
 *
 * Output       : pu2Length - pointer to chassis id length
 *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
LldpUtilGetChassisIdLen (INT4 i4Subtype, UINT1 *pu1ChassId, UINT2 *pu2Length)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    switch (i4Subtype)
    {
        case LLDP_CHASS_ID_SUB_CHASSIS_COMP:
        case LLDP_CHASS_ID_SUB_IF_ALIAS:
        case LLDP_CHASS_ID_SUB_PORT_COMP:
        case LLDP_CHASS_ID_SUB_IF_NAME:
        case LLDP_CHASS_ID_SUB_LOCAL:
            *pu2Length = (UINT2) LLDP_STRLEN (pu1ChassId,
                                              LLDP_MAX_LEN_CHASSISID);
            break;
        case LLDP_CHASS_ID_SUB_MAC_ADDR:
            *pu2Length = MAC_ADDR_LEN;
            break;
        case LLDP_CHASS_ID_SUB_NW_ADDR:
            /* Length of Network address + its subtype */
            *pu2Length = IPVX_IPV4_ADDR_LEN + 1;
            break;
        default:
            i4RetVal = OSIX_FAILURE;
            LLDP_TRC (ALL_FAILURE_TRC, "LldpUtilGetChassisIdLen: "
                      "Invalid switch case\r\n");
    }
    return i4RetVal;
}

/*****************************************************************************
 *
 * Function     : LldpUtilGetPortIdLen
 *
 * Description  : This function gets the port id length based on the given
 *                subtype
 *
 * Input        : i4Subtype - port id subtype
 *                pu1PortId - pointer to port id
 *
 * Output       : pu2Length - pointer to port id length
 *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
LldpUtilGetPortIdLen (INT4 i4Subtype, UINT1 *pu1PortId, UINT2 *pu2Length)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    switch (i4Subtype)
    {
        case LLDP_PORT_ID_SUB_IF_ALIAS:
        case LLDP_PORT_ID_SUB_PORT_COMP:
        case LLDP_PORT_ID_SUB_IF_NAME:
        case LLDP_PORT_ID_SUB_LOCAL:
            *pu2Length = (UINT2) LLDP_STRLEN (pu1PortId, LLDP_MAX_LEN_PORTID);
            break;
        case LLDP_PORT_ID_SUB_MAC_ADDR:
            *pu2Length = MAC_ADDR_LEN;
            break;
        case LLDP_PORT_ID_SUB_NW_ADDR:
            /* Length of Network address + its subtype */
            *pu2Length = IPVX_IPV4_ADDR_LEN + 1;
            break;
        case LLDP_PORT_ID_SUB_AGENT_CKT_ID:
            *pu2Length =
                (UINT2) (LLDP_STRLEN (pu1PortId, LLDP_MAX_LEN_PORTID) + 1);
            break;
        default:
            i4RetVal = OSIX_FAILURE;
            LLDP_TRC (ALL_FAILURE_TRC, "LldpUtilGetPortIdLen: "
                      "Invalid switch case\r\n");
    }
    return i4RetVal;
}

/*****************************************************************************
 *
 * Function     : LldpUtiliEncodeManAddrOid
 *
 * Description  : This function encode the OID list to ASN.1 formatted OID string
 *                In Management address TLV OID is stored as ASN.1 encoded 
 *                format. During construction of Management Address TLV this
 *                function is called to encode the OID.
 *
 * Input        : pInOidStr - OID string
 * 
 * Output       : pu1OutOidStr - ASN.1 encode OID String
 *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
LldpUtilEncodeManAddrOid (tLldpManAddrOid * pInOidStr, UINT1 *pu1OutOidStr)
{
    UINT4               u4Count = 0;
    UINT4               u4Len = 0;
    UINT1              *pu1StartPtr = NULL;
    UINT1               u1ErrorCode = 0;

    tSNMP_OID_TYPE     *pOID;

    pOID = alloc_oid (LLDP_MAX_LEN_MAN_OID);

    if (pOID == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMCPY (pOID->pu4_OidList, pInOidStr->au4_OidList,
            (LLDP_MAX_LEN_MAN_OID * sizeof (UINT4)));
    pOID->u4_Length = pInOidStr->u4_Length;

    /* If the global backward compatibility flag is enabled,
     * ASN.1 OID encoding is done in the old manner, by default the global variable
     * will be set to OSIX_FALSE*/

    if (gu4LldpASNBackwardCompatibility == OSIX_TRUE)
    {
        u4Len = pInOidStr->u4_Length;
        pu1StartPtr = pu1OutOidStr;

        /* TODO: Need to modify this function to support the actual
         * ASN.1 OID encoding. The OID of IfIndex is sent in the mgmt addr tlv
         * i.e., (1.3.6.1.2.1.2.2.1.1). Since the values in the OID is
         * within max UINT1 value (255), this implementation works. */
        for (u4Count = 0; u4Count < u4Len; u4Count++)
        {
            *(pu1OutOidStr + u4Count + 1) =
                (UINT1) (pInOidStr->au4_OidList[u4Count]);
        }

        *(pu1StartPtr + 0) = (UINT1) u4Len;
    }
    else
    {
        /* Calling the new ASN.1 encoding method */
        if (SnmpUtilEncodeOid (pOID, pu1OutOidStr, &u1ErrorCode)
            == OSIX_FAILURE)
        {
            free_oid (pOID);
            if (u1ErrorCode == 1)
            {
                LLDP_TRC (LLDP_CRITICAL_TRC, "LldpUtilEncodeManAddrOid:"
                          "1st OID component is 0 or 1; So 2nd OID component should"
                          "be within the range of 0 to 39!!!!!!\r\n");
                return OSIX_FAILURE;
            }
            else if (u1ErrorCode == 2)
            {
                LLDP_TRC (LLDP_CRITICAL_TRC, "LldpUtilEncodeManAddrOid:"
                          "1st OID component is not in the range 0 to 2!!\r\n");
                return OSIX_FAILURE;
            }
            else
            {
                LLDP_TRC (LLDP_CRITICAL_TRC, "LldpUtilEncodeManAddrOid:"
                          "Given OID cannot be encoded within 128 bytes!!!\r\n");
                return OSIX_FAILURE;
            }
        }
    }
    free_oid (pOID);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 * Function     : LldpUtilDecodeManAddrOid
 *
 * Description  : This function decode the ASN.1 encoded oid string. OID string
 *                in the management address TLV are encoded in ASN.1 format.
 *                This function helps to decode that OID.
 *
 * Input        : pu1InOidStr - ASN.1 encode OID string
 *
 * Output       : pOutOidStr  - Decoded OID string
 *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
LldpUtilDecodeManAddrOid (UINT1 *pu1InOidStr, tLldpManAddrOid * pOutOidStr)
{
    INT4                i4Len = 0;
    INT4                i4Seq = 0;

    tSNMP_OID_TYPE     *pOID;

    pOID = alloc_oid (LLDP_MAX_LEN_MAN_OID);

    if (pOID == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMCPY (pOID->pu4_OidList, pOutOidStr->au4_OidList,
            (LLDP_MAX_LEN_MAN_OID * sizeof (UINT4)));
    pOID->u4_Length = pOutOidStr->u4_Length;

    /* If the global backward compatibility flag is enabled,
     * ASN.1 OID decoding is done in the old manner, by default the global variable
     * will be set to OSIX_FALSE*/

    if (gu4LldpASNBackwardCompatibility == OSIX_TRUE)
    {
        i4Len = *(pu1InOidStr + 0);

        if (i4Len == 0)
        {
            pOutOidStr->u4_Length = 0;
            free_oid (pOID);
            return OSIX_SUCCESS;
        }
        else if (i4Len > LLDP_MAX_LEN_MAN_OID)
        {
            free_oid (pOID);
            return OSIX_FAILURE;
        }

        pOutOidStr->u4_Length = 0;
        pu1InOidStr++;
        for (i4Seq = 0; i4Seq < i4Len; i4Seq++)
        {
            pOutOidStr->au4_OidList[pOutOidStr->u4_Length] =
                (pOutOidStr->au4_OidList[pOutOidStr->u4_Length] << 7) +
                (*pu1InOidStr & 0x7F);
            if ((*pu1InOidStr++ & 0x80) == 0)
            {
                pOutOidStr->u4_Length++;
                if (i4Seq < i4Len - 2
                    && (pOutOidStr->u4_Length < LLDP_MAX_LEN_MAN_OID))
                {
                    pOutOidStr->au4_OidList[pOutOidStr->u4_Length] = 0;
                }
                /*If pOutOidStr->u4_Length reaches LLDP_MAX_LEN_MAN_OID,
                 * just break to avoid array overrun*/
                if (!(pOutOidStr->u4_Length < LLDP_MAX_LEN_MAN_OID))
                {
                    break;
                }
            }
        }
    }
    else
    {
        /* Calling the new ASN.1 decoding method */
        SnmpUtilDecodeOid (pu1InOidStr, pOID);
        MEMCPY (pOutOidStr->au4_OidList, pOID->pu4_OidList,
                (LLDP_MAX_LEN_MAN_OID * sizeof (UINT4)));
        pOutOidStr->u4_Length = pOID->u4_Length;
    }

    free_oid (pOID);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 * Function     : LldpUtilDeletePortInfo 
 *
 * Description  : This function deletes the port information structure.
 *                ie, function stops all the running timers for the given
 *                port, deletes the neighbors information associated with the
 *                port and deletes the port information structure.
 *
 * Input        : pPortInfo - pointer to port information strucutre 
 *
 * Output       : None 
 *
 * Returns      : None
 *
 *****************************************************************************/
PUBLIC VOID
LldpUtilDeletePortInfo (tLldpLocPortInfo * pPortInfo)
{
    tLldpv2AgentToLocPort AgentToLocPort;
    tLldpv2AgentToLocPort *pAgentToLocPort = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    tLldpAppInfo        TmpLldpAppNode;
    tLldpAppInfo       *pLldpAppNode = NULL;
    tLldpv2DestAddrTbl *pDestTbl = NULL;
    tMacAddr            MacAddr;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));

    /* Stop message interval timer */
    if (pPortInfo->TtrTmrNode.u1Status == LLDP_TMR_RUNNING)
    {
        if (TmrStopTimer (gLldpGlobalInfo.TmrListId,
                          &(pPortInfo->TtrTmr.TimerNode)) != TMR_SUCCESS)
        {
            LLDP_TRC_ARG1 (OS_RESOURCE_TRC | ALL_FAILURE_TRC
                           | CONTROL_PLANE_TRC,
                           "LldpUtilDeletePortInfo: "
                           "TmrStopTimer(TtrTmr) returns FAILURE!!!\r\n",
                           pPortInfo->u4LocPortNum);
        }
        /* set the transmit interval timer status as NOT_RUNNING */
        pPortInfo->TtrTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;
    }
    /* stop transmit delay timer */
    if (pPortInfo->TxDelayTmrNode.u1Status == LLDP_TMR_RUNNING)
    {
        if (TmrStopTimer (gLldpGlobalInfo.TmrListId,
                          &(pPortInfo->TxDelayTmr.TimerNode)) != TMR_SUCCESS)
        {
            LLDP_TRC_ARG1 (OS_RESOURCE_TRC | ALL_FAILURE_TRC
                           | CONTROL_PLANE_TRC,
                           "LldpUtilDeletePortInfo: "
                           "TmrStopTimer(TxDelayTmr) returns FAILURE!!!\r\n",
                           pPortInfo->u4LocPortNum);
        }
        /* set the transmit delay timer status as NOT_RUNNING */
        pPortInfo->TxDelayTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;
    }
    /* stop shutdown while timer */
    if (pPortInfo->ShutWhileTmrNode.u1Status == LLDP_TMR_RUNNING)
    {
        if (TmrStopTimer (gLldpGlobalInfo.TmrListId,
                          &(pPortInfo->ShutWhileTmr.TimerNode)) != TMR_SUCCESS)
        {
            LLDP_TRC_ARG1 (OS_RESOURCE_TRC | ALL_FAILURE_TRC
                           | CONTROL_PLANE_TRC,
                           "LldpUtilDeletePortInfo: "
                           "TmrStopTimer(ShutWhileTmr) returns FAILURE!!!\r\n",
                           pPortInfo->u4LocPortNum);
        }
        /* set the shutdown while timer status as NOT_RUNNING */
        pPortInfo->ShutWhileTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;
    }

    /* Delete all the Remote Node associated with current port. This function 
     * takes care of deleting rxinfo age timer if running */
    LldpRxUtlDelRemInfoForPort (pPortInfo);

    if (LldpTxUtlClearPortInfo (pPortInfo) != OSIX_SUCCESS)
    {
        LLDP_TRC (INIT_SHUT_TRC, "LldpUtilDeletePortInfo: "
                  "LldpTxUtlClearPortInfo returns FAILURE!!!\r\n");
    }

    /* When deletion indication is received, the port info
     * structure must be deleted. */
    /* Release the memory allocated for pPreFormedLldpdu Linear Buffer */
    if (pPortInfo->pPreFormedLldpdu != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.LldpPduInfoPoolId,
                            (UINT1 *) (pPortInfo->pPreFormedLldpdu));
        pPortInfo->pPreFormedLldpdu = NULL;
        pPortInfo->u4PreFormedLldpduLen = 0;
    }

    MEMSET (&AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
    /*MEMSET(&DestAddrTbl, 0, sizeof (tLldpv2DestAddrTbl)); */
    MEMSET (MacAddr, 0, MAC_ADDR_LEN);
    AgentToLocPort.i4IfIndex = pPortInfo->i4IfIndex;

    /* -- Deleting the APP registaration information that are 
     * present on this port */
    MEMSET (&TmpLldpAppNode, 0, sizeof (tLldpAppInfo));
    TmpLldpAppNode.u4PortId = (UINT4) AgentToLocPort.i4IfIndex;

    while ((pLldpAppNode = (tLldpAppInfo *) RBTreeGetNext
            (gLldpGlobalInfo.AppRBTree, (tRBElem *) & TmpLldpAppNode, NULL))
           != NULL)
    {
        if (pLldpAppNode->u4PortId != (UINT4) AgentToLocPort.i4IfIndex)
        {
            break;
        }
        /* To remove a particular APP for the Port */
        if (pLldpAppNode->u4LldpInstSelector == pPortInfo->u4DstMacAddrTblIndex)
        {
            RBTreeRemove (gLldpGlobalInfo.AppRBTree, (UINT1 *) pLldpAppNode);

            MemReleaseMemBlock (gLldpGlobalInfo.AppTlvPoolId,
                                (UINT1 *) pLldpAppNode->pu1TxAppTlv);
            MemReleaseMemBlock (gLldpGlobalInfo.AppTblPoolId,
                                (UINT1 *) pLldpAppNode);
        }
        TmpLldpAppNode.u4PortId = pLldpAppNode->u4PortId;
        MEMCPY (&(TmpLldpAppNode.LldpAppId), &(pLldpAppNode->LldpAppId),
                sizeof (TmpLldpAppNode.LldpAppId));
    }

    DestTbl.u4LlldpV2DestAddrTblIndex = pPortInfo->u4DstMacAddrTblIndex;
    pDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree, &DestTbl);
    if (pDestTbl != NULL)
    {
        MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                pDestTbl->Lldpv2DestMacAddress, MAC_ADDR_LEN);
        pAgentToLocPort = (tLldpv2AgentToLocPort *)
            RBTreeGet (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                       &AgentToLocPort);
        if (pAgentToLocPort != NULL)
        {
            RBTreeRem (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                       pAgentToLocPort);
            RBTreeRem (gLldpGlobalInfo.Lldpv2AgentMapTblRBTree,
                       pAgentToLocPort);
            MemReleaseMemBlock
                (gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId,
                 (UINT1 *) pAgentToLocPort);
        }

    }
    /* release the memory allocated for port info structure */
    RBTreeRem (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree, pPortInfo);
    MemReleaseMemBlock (gLldpGlobalInfo.LocPortInfoPoolId, (UINT1 *) pPortInfo);
    /* reset the pointer corresponding to this port in the global
     * information strcture to NULL */
    pPortInfo = NULL;
    /*Deleting destination table that is unsed by this interface  only */
    if ((pDestTbl != NULL) &&
        (LldpUtlCheckDestMacAddrIndexUsage
         (pDestTbl->u4LlldpV2DestAddrTblIndex) != OSIX_TRUE))
    {
        if ((MEMCMP
             (pDestTbl->Lldpv2DestMacAddress, gau1LldpMcastAddr,
              MAC_ADDR_LEN)) != 0)
        {
            RBTreeRemove (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree, pDestTbl);
            MemReleaseMemBlock (gLldpGlobalInfo.LldpDestMacAddrPoolId,
                                (UINT1 *) pDestTbl);
        }
    }
    return;
}

/******************************************************************************
 * Function Name      : LldpUtilSplitStrToTokens 
 *
 * Description        : This function splits the given string into tokens
 *                      based on the given token delimiter and stores the
 *                      tokens in token array and returns the token array.
 *
 * Input(s)           : pu1InputStr  - Pointer to input string
 *                      i4Strlen     - String length
 *                      u1Delimiter  - Delimiter by which the token has to be
 *                                     seperated
 *                      u2MaxToken    - Max token count
 *                      apu1Token     - Poiner to token array
 *                      pu1TokenCount - Token count
 *
 * Output(s)          : apu1Token     - Poiner to token array
 *                      pu1TokenCount - Token count
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
LldpUtilSplitStrToTokens (UINT1 *pu1InputStr, INT4 i4Strlen,
                          UINT1 u1Delimiter, UINT2 u2MaxToken,
                          UINT1 *apu1Token[], UINT1 *pu1TokenCount)
{
    UINT1               u1TokenSize = 0;
    UINT1               u1TokenCount = 0;

    while ((i4Strlen > 0) && (u1TokenCount < u2MaxToken))
    {
        /* reset token size to zero */
        u1TokenSize = 0;
        /* scan for delimiter */
        while ((i4Strlen > 0) && (*(pu1InputStr + u1TokenSize) != u1Delimiter))
        {
            i4Strlen--;
            u1TokenSize++;
        }
        /* since the loop breaks whenever delimiter is found the string length
         * and token size shoud be updated once(outside the loop) by the size 
         * of delimiter(which is 1byte) */
        i4Strlen--;
        u1TokenSize++;
        /* copy the token excluding delimiter */
        STRNCPY (apu1Token[u1TokenCount], pu1InputStr,
                 (u1TokenSize - sizeof (u1Delimiter)));
        /* move the input string pointer to point to next token */
        pu1InputStr += u1TokenSize;
        /* increment the token count */
        u1TokenCount++;
    }
    /* update the token count */
    *pu1TokenCount = u1TokenCount;
    return;
}

/******************************************************************************
 * Function Name      : LldpRxUtlSendAppAgeout 
 *
 * Description        : This function posts ageout to all the applications 
 *                      registered on the given port
 *
 * Input(s)           : pLocPortInfo - Pointer to local port structure
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
LldpUtlSendAppAgeout (tLldpLocPortInfo * pPortInfo)
{
    tLldpAppInfo        TmpLldpAppNode;
    tLldpAppInfo       *pLldpAppNode = NULL;
    tLldpAppTlv         LldpAppTlv;
    UINT1               au1LldpMcastAddr[MAC_ADDR_LEN] =
        LLDP_DEST_MCAST_MAC_ADDR;

    /* 1. Get the applications registered in pPortInfo->u4LocPortNum
     * 2. Send AGE OUT indication to applications registered.
     * 3. Proceed to the Next application, Repeat till all the registered
     *    applications are traversed.
     */

    MEMSET (&TmpLldpAppNode, 0, sizeof (tLldpAppInfo));
    MEMSET (&LldpAppTlv, 0, sizeof (tLldpAppTlv));

    /* Get the first application register on this port by specifying
     * only port number as part of the index of application RB Tree */
    TmpLldpAppNode.u4PortId = (UINT4) pPortInfo->i4IfIndex;

    pLldpAppNode = (tLldpAppInfo *) RBTreeGetNext (gLldpGlobalInfo.AppRBTree,
                                                   (tRBElem *) & TmpLldpAppNode,
                                                   NULL);

    if (pLldpAppNode == NULL)
    {
        /* No application is registered, so return */
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                       "\r(LldpUtlSendAppAgeout) No application "
                       "registered on port %d\r\n", pPortInfo->i4IfIndex);
        return;
    }

    LldpAppTlv.u4RxAppTlvPortId = (UINT4) pPortInfo->i4IfIndex;
    LldpAppTlv.u4MsgType = L2IWF_LLDP_APPL_TLV_AGED;

    /* Remote node will be NULL if module itself is shutdown
     * so RemIndex will be 0 if module is shutdown */
    if (pPortInfo->pBackPtrRemoteNode != NULL)
    {
        LldpAppTlv.i4RemIndex = pPortInfo->pBackPtrRemoteNode->i4RemIndex;
        LldpAppTlv.u4RxAppTlvTimeStamp =
            pPortInfo->pBackPtrRemoteNode->u4RemLastUpdateTime;

        /* Since LLDPv2 is not implemented, destination MAC has
         * to be hardcoded. */
        MEMCPY (&LldpAppTlv.RemLocalDestMacAddr, au1LldpMcastAddr,
                MAC_ADDR_LEN);
    }

    do
    {
        /* Ensure only applications registered on the current local port
         * only is processed. If the port number changes it implies all
         * the registered applications are processed.*/

        if (pLldpAppNode->u4PortId != (UINT4) pPortInfo->i4IfIndex)
        {
            LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                           "\r(LldpUtlSendAppAgeout) All application "
                           " nodes processed for port"
                           "%d\r\n", pPortInfo->i4IfIndex);
            break;
        }
        if (pLldpAppNode->u1AppTlvRxStatus == OSIX_TRUE)
        {
            /* Fill the application Id */
            MEMCPY (&LldpAppTlv.LldpAppId,
                    &pLldpAppNode->LldpAppId, sizeof (tLldpAppId));

            /* If the pPortInfo->u4LocPortNum is part of any LAG then
             * send this event to be on the port channel instead
             * of physical ports */
            if (LldpPortIsPortInPortChannel (LldpAppTlv.u4RxAppTlvPortId)
                == OSIX_SUCCESS)
            {
                LldpPortGetPortChannelForPort
                    (pPortInfo, &LldpAppTlv.u4RxAppTlvPortId);

                LLDP_TRC_ARG2 (CONTROL_PLANE_TRC,
                               "\r(LldpUtlSendAppAgeout) TLV received "
                               " on port %d which is a member of port channel "
                               "%d\r\n", pPortInfo->i4IfIndex,
                               LldpAppTlv.u4RxAppTlvPortId);
            }

            /* Pass the message to the application through the callback function */
            pLldpAppNode->pAppCallBackFn (&LldpAppTlv);
        }

        /* Get the next application in the port */
        pLldpAppNode =
            (tLldpAppInfo *) RBTreeGetNext (gLldpGlobalInfo.AppRBTree,
                                            (tRBElem *) pLldpAppNode, NULL);
    }
    while (pLldpAppNode != NULL);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpAppUtlRBCmpInfo
 *
 *    DESCRIPTION      : RBTree compare function for ApplnRBTree
 *                       Index of this table
 *                       1. u4PortId
 *                       2. u2TlvType
 *                       3. au1OUI
 *                       4. u1SubType
 *
 *    INPUT            : pElem1 - Pointer to the RemoteNode node1
 *                       pElem2 - Pointer to the RemoteNode node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : LLDP_RB_EQUAL   - if all the keys matched for both 
 *                                         the nodes.
 *                       LLDP_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       LLDP_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
LldpAppUtlRBCmpInfo (tRBElem * pElem1, tRBElem * pElem2)
{
    tLldpAppInfo       *pRemEntry1 = NULL;
    tLldpAppInfo       *pRemEntry2 = NULL;

    pRemEntry1 = (tLldpAppInfo *) pElem1;
    pRemEntry2 = (tLldpAppInfo *) pElem2;

    /* Key match logic:
     * Port Id is the mandatory key for this table. Other fields can be '0'
     * which will be treated as 'any' and remaining keys will be matched.
     */

    /* Compare the first key */
    if (pRemEntry1->u4PortId > pRemEntry2->u4PortId)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4PortId < pRemEntry2->u4PortId)
    {
        return LLDP_RB_LESS;
    }

    return (MEMCMP (&pRemEntry1->LldpAppId,
                    &pRemEntry2->LldpAppId, sizeof (tLldpAppId)));
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlRBFreeAppl
 *
 *    DESCRIPTION      : Frees the memory pool for application RB Tree 
 *                       ApplnRBTree
 *
 *    INPUT            : pRBElem - RB Tree Pointer 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          :
 *
 ****************************************************************************/
PRIVATE INT4
LldpUtlRBFreeAppl (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.AppTblPoolId, (UINT1 *) pRBElem);
        pRBElem = NULL;

    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlRBFreeLocPolicy
 *
 *    DESCRIPTION      : Frees the memory pool for RB Tree
 *                       LocPolicyInfoRBTree
 *
 *    INPUT            : pRBElem - RB Tree Pointer
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
PRIVATE INT4
LldpMedUtlRBFreeLocPolicy (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.LldpMedLocNwPolicyPoolId,
                            (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlRBFreeRemPolicy
 *
 *    DESCRIPTION      : Frees the memory pool for RB Tree
 *                       RemPolicyInfoRBTree
 *
 *    INPUT            : pRBElem - RB Tree Pointer
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
PRIVATE INT4
LldpMedUtlRBFreeRemPolicy (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.LldpMedRemNwPolicyPoolId,
                            (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlRBFreeLocLocation
 *
 *    DESCRIPTION      : Frees the memory pool for RB Tree
 *                       LocLocationInfoRBTree
 *
 *    INPUT            : pRBElem - RB Tree Pointer
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
PRIVATE INT4
LldpMedUtlRBFreeLocLocation (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.LldpMedLocLocationPoolId,
                            (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlRBFreeRemLocation
 *
 *    DESCRIPTION      : Frees the memory pool for RB Tree
 *                       RemLocationInfoRBTree
 *
 *    INPUT            : pRBElem - RB Tree Pointer
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
PRIVATE INT4
LldpMedUtlRBFreeRemLocation (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.LldpMedRemLocationPoolId,
                            (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

PUBLIC tLldpLocPortInfo *
LldpGetLocPortInfo (UINT4 u4LocPortNum)
{
    tLldpLocPortInfo    LldpLocPortInfo;

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    LldpLocPortInfo.u4LocPortNum = u4LocPortNum;
    return ((tLldpLocPortInfo *)
            RBTreeGet (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
                       (tRBElem *) & LldpLocPortInfo));
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpPortInfoUtlRBCmpInfo
 *
 *    DESCRIPTION      : RBTree compare function for PortRBTree
 *                       Index of this table
 *                       1. u4PortId
 *
 *    INPUT            : pElem1 - Pointer to the Node node1
 *                       pElem2 - Pointer to the Node node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : LLDP_RB_EQUAL   - if all the keys matched for both 
 *                                         the nodes.
 *                       LLDP_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       LLDP_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
LldpPortInfoUtlRBCmpInfo (tRBElem * pElem1, tRBElem * pElem2)
{
    tLldpLocPortTable  *pRemEntry1 = NULL;
    tLldpLocPortTable  *pRemEntry2 = NULL;

    pRemEntry1 = (tLldpLocPortTable *) pElem1;
    pRemEntry2 = (tLldpLocPortTable *) pElem2;

    /* Key match logic:
     * Interface Index is the mandatory key for this table. 
     */

    /* Compare the key */
    if (pRemEntry1->i4IfIndex > pRemEntry2->i4IfIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4IfIndex < pRemEntry2->i4IfIndex)
    {
        return LLDP_RB_LESS;
    }

    return (LLDP_RB_EQUAL);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpDstMacAddrUtlRBCmpInfo
 *
 *    DESCRIPTION      : RBTree compare function for DestMacAddrTblRBTree
 *                       Index of this table
 *                       1. Destination Address table index
 *
 *    INPUT            : pElem1 - Pointer to the Node node1
 *                       pElem2 - Pointer to the Node node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : LLDP_RB_EQUAL   - if all the keys matched for both 
 *                                         the nodes.
 *                       LLDP_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       LLDP_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
LldpDstMacAddrUtlRBCmpInfo (tRBElem * pElem1, tRBElem * pElem2)
{
    tLldpv2DestAddrTbl *pRemEntry1 = NULL;
    tLldpv2DestAddrTbl *pRemEntry2 = NULL;

    pRemEntry1 = (tLldpv2DestAddrTbl *) pElem1;
    pRemEntry2 = (tLldpv2DestAddrTbl *) pElem2;

    /* Key match logic:
     * Interface Index is the mandatory key for this table. 
     */

    /* Compare the key */
    if (pRemEntry1->u4LlldpV2DestAddrTblIndex >
        pRemEntry2->u4LlldpV2DestAddrTblIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4LlldpV2DestAddrTblIndex <
             pRemEntry2->u4LlldpV2DestAddrTblIndex)
    {
        return LLDP_RB_LESS;
    }

    return (LLDP_RB_EQUAL);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpAgentToLocPortUtlRBCmpInfo
 *
 *    DESCRIPTION      : RBTree compare function for LldpAgentToLocPortUtlRBCmpInfo
 *                       Index of this table
 *                       1. IfIndex
 *                       2. Destination MAC Address
 *
 *    INPUT            : pElem1 - Pointer to the Node node1
 *                       pElem2 - Pointer to the Node node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : LLDP_RB_EQUAL   - if all the keys matched for both 
 *                                         the nodes.
 *                       LLDP_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       LLDP_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
LldpAgentToLocPortUtlRBCmpInfo (tRBElem * pElem1, tRBElem * pElem2)
{
    tLldpv2AgentToLocPort *pRemEntry1 = NULL;
    tLldpv2AgentToLocPort *pRemEntry2 = NULL;

    pRemEntry1 = (tLldpv2AgentToLocPort *) pElem1;
    pRemEntry2 = (tLldpv2AgentToLocPort *) pElem2;

    /* Key match logic:
     * Interface Index is the mandatory key for this table. 
     */

    /* Compare the key */
    if (pRemEntry1->i4IfIndex > pRemEntry2->i4IfIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4IfIndex < pRemEntry2->i4IfIndex)
    {
        return LLDP_RB_LESS;
    }
    else if (MEMCMP (pRemEntry1->Lldpv2DestMacAddress,
                     pRemEntry2->Lldpv2DestMacAddress, MAC_ADDR_LEN) > 0)
    {
        return LLDP_RB_GREATER;
    }
    else if (MEMCMP (pRemEntry1->Lldpv2DestMacAddress,
                     pRemEntry2->Lldpv2DestMacAddress, MAC_ADDR_LEN) < 0)
    {
        return LLDP_RB_LESS;
    }
    return (LLDP_RB_EQUAL);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpAgentToLocPortIndexUtlRBCmpInfo 
 *
 *    DESCRIPTION      : RBTree compare function for AgentRBTree
 *                       Index of this table
 *                       1. i4IfIndex
 *                       2. u4DstMacAddrTblIndex
 *
 *    INPUT            : pElem1 - Pointer to the Node node1
 *                       pElem2 - Pointer to the Node node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : LLDP_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       LLDP_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       LLDP_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/

PUBLIC INT4
LldpAgentToLocPortIndexUtlRBCmpInfo (tRBElem * pElem1, tRBElem * pElem2)
{
    tLldpv2AgentToLocPort *pRemEntry1 = NULL;
    tLldpv2AgentToLocPort *pRemEntry2 = NULL;

    pRemEntry1 = (tLldpv2AgentToLocPort *) pElem1;
    pRemEntry2 = (tLldpv2AgentToLocPort *) pElem2;

    if (pRemEntry1->i4IfIndex > pRemEntry2->i4IfIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4IfIndex < pRemEntry2->i4IfIndex)
    {
        return LLDP_RB_LESS;
    }
    else if (pRemEntry1->u4DstMacAddrTblIndex >
             pRemEntry2->u4DstMacAddrTblIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4DstMacAddrTblIndex <
             pRemEntry2->u4DstMacAddrTblIndex)
    {
        return LLDP_RB_LESS;
    }

    return (LLDP_RB_EQUAL);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpAgentInfoUtlRBCmpInfo
 *
 *    DESCRIPTION      : RBTree compare function for AgentRBTree
 *                       Index of this table
 *                       1. u4LocPortNum
 *
 *    INPUT            : pElem1 - Pointer to the Node node1
 *                       pElem2 - Pointer to the Node node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : LLDP_RB_EQUAL   - if all the keys matched for both 
 *                                         the nodes.
 *                       LLDP_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       LLDP_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
LldpAgentInfoUtlRBCmpInfo (tRBElem * pElem1, tRBElem * pElem2)
{
    tLldpLocPortInfo   *pRemEntry1 = NULL;
    tLldpLocPortInfo   *pRemEntry2 = NULL;

    pRemEntry1 = (tLldpLocPortInfo *) pElem1;
    pRemEntry2 = (tLldpLocPortInfo *) pElem2;

    /* Key match logic:
     * Interface Index is the mandatory key for this table. 
     */

    /* Compare the key */
    if (pRemEntry1->u4LocPortNum > pRemEntry2->u4LocPortNum)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4LocPortNum < pRemEntry2->u4LocPortNum)
    {
        return LLDP_RB_LESS;
    }

    return (LLDP_RB_EQUAL);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedLocNwPolicyInfoRBCmpInfo 
 *
 *    DESCRIPTION      : RBTree compare function for LocPolicyInfoRBTree
 *                       Index of this table
 *                       1. u4IfIndex
 *                       2. u1PolicyAppType
 *
 *    INPUT            : pElem1 - Pointer to the Node node1
 *                       pElem2 - Pointer to the Node node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : LLDP_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       LLDP_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       LLDP_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
LldpMedLocNwPolicyInfoRBCmpInfo (tRBElem * pElem1, tRBElem * pElem2)
{
    tLldpMedLocNwPolicyInfo *pLocEntry1 = NULL;
    tLldpMedLocNwPolicyInfo *pLocEntry2 = NULL;

    pLocEntry1 = (tLldpMedLocNwPolicyInfo *) pElem1;
    pLocEntry2 = (tLldpMedLocNwPolicyInfo *) pElem2;

    /* Key match logic:
     * Interface Index is the mandatory key for this table.
     */

    /* Compare the key */
    if (pLocEntry1->u4IfIndex > pLocEntry2->u4IfIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pLocEntry1->u4IfIndex < pLocEntry2->u4IfIndex)
    {
        return LLDP_RB_LESS;
    }
    else if (pLocEntry1->u1LocPolicyAppType > pLocEntry2->u1LocPolicyAppType)
    {
        return LLDP_RB_GREATER;
    }
    else if (pLocEntry1->u1LocPolicyAppType < pLocEntry2->u1LocPolicyAppType)
    {
        return LLDP_RB_LESS;
    }

    return (LLDP_RB_EQUAL);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedRemNwPolicyInfoRBCmpInfo
 *
 *    DESCRIPTION      : RBTree compare function for RemPolicyInfoRBTree
 *                       Index of this table
 *                       1. i4RemLocalPortNum
 *                       2. i4RemIndex
 *                       3. u4RemLastUpdateTime
 *                       4. u4DestAddrTblIndex
 *                       5. u1PolicyAppType
 *
 *    INPUT            : pElem1 - Pointer to the Node node1
 *                       pElem2 - Pointer to the Node node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : LLDP_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       LLDP_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       LLDP_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/

PUBLIC INT4
LldpMedRemNwPolicyInfoRBCmpInfo (tRBElem * pElem1, tRBElem * pElem2)
{
    tLldpMedRemNwPolicyInfo *pRemEntry1 = NULL;
    tLldpMedRemNwPolicyInfo *pRemEntry2 = NULL;

    pRemEntry1 = (tLldpMedRemNwPolicyInfo *) pElem1;
    pRemEntry2 = (tLldpMedRemNwPolicyInfo *) pElem2;

    /* Key match logic:
     * Interface Index is the mandatory key for this table.
     */

    /* Compare the key */
    if (pRemEntry1->u4RemLastUpdateTime > pRemEntry2->u4RemLastUpdateTime)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4RemLastUpdateTime < pRemEntry2->u4RemLastUpdateTime)
    {
        return LLDP_RB_LESS;
    }

    if (pRemEntry1->i4RemLocalPortNum > pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemLocalPortNum < pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_LESS;
    }

    if (pRemEntry1->u4RemDestAddrTblIndex > pRemEntry2->u4RemDestAddrTblIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4RemDestAddrTblIndex <
             pRemEntry2->u4RemDestAddrTblIndex)
    {
        return LLDP_RB_LESS;
    }

    if (pRemEntry1->i4RemIndex > pRemEntry2->i4RemIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemIndex < pRemEntry2->i4RemIndex)
    {
        return LLDP_RB_LESS;
    }

    if (pRemEntry1->u1RemPolicyAppType > pRemEntry2->u1RemPolicyAppType)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u1RemPolicyAppType < pRemEntry2->u1RemPolicyAppType)
    {
        return LLDP_RB_LESS;
    }

    return (LLDP_RB_EQUAL);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedLocLocationInfoRBCmpInfo
 *
 *    DESCRIPTION      : RBTree compare function for LocLocationInfoRBTree
 *                       Index of this table
 *                       1. u4IfIndex
 *                       2. u1LocationDataFormat
 *
 *    INPUT            : pElem1 - Pointer to the Node node1
 *                       pElem2 - Pointer to the Node node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : LLDP_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       LLDP_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       LLDP_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
LldpMedLocLocationInfoRBCmpInfo (tRBElem * pElem1, tRBElem * pElem2)
{
    tLldpMedLocLocationInfo *pLocEntry1 = NULL;
    tLldpMedLocLocationInfo *pLocEntry2 = NULL;

    pLocEntry1 = (tLldpMedLocLocationInfo *) pElem1;
    pLocEntry2 = (tLldpMedLocLocationInfo *) pElem2;

    /* Key match logic:
     * Interface Index is the mandatory key for this table.
     */

    /* Compare the key */
    if (pLocEntry1->u4IfIndex > pLocEntry2->u4IfIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pLocEntry1->u4IfIndex < pLocEntry2->u4IfIndex)
    {
        return LLDP_RB_LESS;
    }
    else if (pLocEntry1->u1LocationDataFormat >
             pLocEntry2->u1LocationDataFormat)
    {
        return LLDP_RB_GREATER;
    }
    else if (pLocEntry1->u1LocationDataFormat <
             pLocEntry2->u1LocationDataFormat)
    {
        return LLDP_RB_LESS;
    }

    return (LLDP_RB_EQUAL);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedRemLocationInfoRBCmpInfo
 *
 *    DESCRIPTION      : RBTree compare function for RemLocationInfoRBTree
 *                       Index of this table
 *                       1. i4RemLocalPortNum
 *                       2. i4RemIndex
 *                       3. u4RemLastUpdateTime
 *                       4. u4DestAddrTblIndex
 *                       5. u1RemLocationSubType
 *
 *    INPUT            : pElem1 - Pointer to the Node node1
 *                       pElem2 - Pointer to the Node node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : LLDP_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       LLDP_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       LLDP_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/

PUBLIC INT4
LldpMedRemLocationInfoRBCmpInfo (tRBElem * pElem1, tRBElem * pElem2)
{
    tLldpMedRemLocationInfo *pRemEntry1 = NULL;
    tLldpMedRemLocationInfo *pRemEntry2 = NULL;

    pRemEntry1 = (tLldpMedRemLocationInfo *) pElem1;
    pRemEntry2 = (tLldpMedRemLocationInfo *) pElem2;

    /* Key match logic:
     * Interface Index is the mandatory key for this table.
     */

    /* Compare the key */
    if (pRemEntry1->u4RemLastUpdateTime > pRemEntry2->u4RemLastUpdateTime)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4RemLastUpdateTime < pRemEntry2->u4RemLastUpdateTime)
    {
        return LLDP_RB_LESS;
    }

    if (pRemEntry1->i4RemLocalPortNum > pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemLocalPortNum < pRemEntry2->i4RemLocalPortNum)
    {
        return LLDP_RB_LESS;
    }

    if (pRemEntry1->u4RemDestAddrTblIndex > pRemEntry2->u4RemDestAddrTblIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u4RemDestAddrTblIndex <
             pRemEntry2->u4RemDestAddrTblIndex)
    {
        return LLDP_RB_LESS;
    }

    if (pRemEntry1->i4RemIndex > pRemEntry2->i4RemIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->i4RemIndex < pRemEntry2->i4RemIndex)
    {
        return LLDP_RB_LESS;
    }

    if (pRemEntry1->u1RemLocationSubType > pRemEntry2->u1RemLocationSubType)
    {
        return LLDP_RB_GREATER;
    }
    else if (pRemEntry1->u1RemLocationSubType <
             pRemEntry2->u1RemLocationSubType)
    {
        return LLDP_RB_LESS;
    }

    return (LLDP_RB_EQUAL);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLocalPort
 *
 *    DESCRIPTION      : To get the Loc Port from the ifIndex & Dest Macaddr 
 *                       
 *    INPUT            : i4IfIndex - IfIndex of the agent
 *                       DestMacAddr - Dest Mac address of the agent
 *                       *pu4LocalPort - return pointer which contains the 
 *                       agent information
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLocalPort (INT4 i4IfIndex, tMacAddr DestMacAddr, UINT4 *pu4LocalPort)
{
    tLldpv2AgentToLocPort Lldpv2AgentToLocPort;
    tLldpv2AgentToLocPort *pLldpv2AgentToLocPort = NULL;

    MEMSET (&Lldpv2AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
    Lldpv2AgentToLocPort.i4IfIndex = i4IfIndex;
    MEMCPY (Lldpv2AgentToLocPort.Lldpv2DestMacAddress, DestMacAddr,
            MAC_ADDR_LEN);

    pLldpv2AgentToLocPort = (tLldpv2AgentToLocPort *)
        RBTreeGet (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                   (tRBElem *) & Lldpv2AgentToLocPort);
    if (pLldpv2AgentToLocPort == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD pLldpv2AgentToLocPort is NULL  Lldpv2AgentToLocPort is Invalid \r\n");
        return OSIX_FAILURE;
    }

    *pu4LocalPort = pLldpv2AgentToLocPort->u4LldpLocPort;
    return OSIX_SUCCESS;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetDestMacTblIndex
 *
 *    DESCRIPTION      : To get Destination Mac Table Index from the Macaddress
 *                       
 *    INPUT            : 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetDestMacTblIndex (tMacAddr DestMacAddr, UINT4 *pu4DestMacIndex)
{
    tLldpv2DestAddrTbl  Lldpv2DestMacAddr;
    tLldpv2DestAddrTbl *pLldpv2DestMacAddr = NULL;

    MEMSET (&Lldpv2DestMacAddr, 0, sizeof (tLldpv2DestAddrTbl));
    MEMCPY (Lldpv2DestMacAddr.Lldpv2DestMacAddress, DestMacAddr, MAC_ADDR_LEN);

    pLldpv2DestMacAddr = (tLldpv2DestAddrTbl *)
        RBTreeGetFirst (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree);
    while (pLldpv2DestMacAddr != NULL)
    {
        if (MEMCMP
            (pLldpv2DestMacAddr->Lldpv2DestMacAddress, DestMacAddr,
             MAC_ADDR_LEN) == 0)
        {
            *pu4DestMacIndex = pLldpv2DestMacAddr->u4LlldpV2DestAddrTblIndex;
            return OSIX_SUCCESS;

        }
        Lldpv2DestMacAddr.u4LlldpV2DestAddrTblIndex =
            pLldpv2DestMacAddr->u4LlldpV2DestAddrTblIndex;
        pLldpv2DestMacAddr =
            (tLldpv2DestAddrTbl *) RBTreeGetNext (gLldpGlobalInfo.
                                                  Lldpv2DestMacAddrTblRBTree,
                                                  &Lldpv2DestMacAddr,
                                                  LldpDstMacAddrUtlRBCmpInfo);
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpPortConfigAdminStatus
 *
 *    DESCRIPTION      : To get the LLDP Port Config Admin Status
 *                       
 *    INPUT            : u4LocPort - Local agent information
 *                       *pi4PortConfigAdminStatus - Pointer for the return value
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpPortConfigAdminStatus (INT4 i4LldpPortConfigPortNum,
                                     tMacAddr DestMacAddr,
                                     INT4 *pi4PortConfigAdminStatus)
{
    tLldpLocPortInfo    LldpLocPortInfo;
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    UINT4               u4LocPort = 0;

    if (LldpUtlGetLocalPort (i4LldpPortConfigPortNum, DestMacAddr, &u4LocPort)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD pLldpLocPortInfo is NULL LldpLocPortInfo is Invalid \r\n");
        return OSIX_FAILURE;
    }

    *pi4PortConfigAdminStatus = pLldpLocPortInfo->PortConfigTable.i4AdminStatus;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpPortConfigNotificationEnable
 *
 *    DESCRIPTION      : To get the LLDP Port Config Notification Enable
 *                       
 *    INPUT            : u4LocPort - Local agent information
 *                       *pi4PortConfigNotificationEnable - Pointer for the return value
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpPortConfigNotificationEnable (INT4 i4LldpPortConfigPortNum,
                                            tMacAddr DestMacAddr,
                                            INT4
                                            *pi4PortConfigNotificationEnable)
{
    tLldpLocPortInfo    LldpLocPortInfo;
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    UINT4               u4LocPort = 0;

    if (LldpUtlGetLocalPort (i4LldpPortConfigPortNum, DestMacAddr, &u4LocPort)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD pLldpLocPortInfo is NULL LldpLocPortInfo is Invalid \r\n");
        return OSIX_FAILURE;
    }

    *pi4PortConfigNotificationEnable =
        pLldpLocPortInfo->PortConfigTable.u1NotificationEnable;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpPortConfigTLVsTxEnable 
 *
 *    DESCRIPTION      : To get the LLDP Basic TLV enable Flag per agent
 *                       
 *    INPUT            : u4LocPort - Local agent information
 *                       *pRetValLldpPortConfigTLVsTxEnable - Pointer for the return value
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpPortConfigTLVsTxEnable (INT4 i4LldpPortConfigPortNum,
                                      tMacAddr DestMacAddr,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValLldpPortConfigTLVsTxEnable)
{
    tLldpLocPortInfo    LldpLocPortInfo;
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    UINT4               u4LocPort = 0;

    if (LldpUtlGetLocalPort (i4LldpPortConfigPortNum, DestMacAddr, &u4LocPort)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD pLldpLocPortInfo is NULL LldpLocPortInfo is Invalid \r\n");
        return OSIX_FAILURE;
    }

    /* copy the value to be returned  */
    MEMCPY (pRetValLldpPortConfigTLVsTxEnable->pu1_OctetList,
            &(pLldpLocPortInfo->PortConfigTable.u1TLVsTxEnable),
            sizeof (pLldpLocPortInfo->PortConfigTable.u1TLVsTxEnable));
    /* store the length of the string */
    pRetValLldpPortConfigTLVsTxEnable->i4_Length =
        sizeof (pLldpLocPortInfo->PortConfigTable.u1TLVsTxEnable);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlSetLldpPortConfigAdminStatus 
 *
 *    DESCRIPTION      : To Set the port Admin status for per agent
 *                       
 *    INPUT            : u4LocPort - Local agent information
 *                       i4SetValLldpPortConfigAdminStatus - Value to be set by User
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlSetLldpPortConfigAdminStatus (INT4 i4LldpPortConfigPortNum,
                                     tMacAddr DestMacAddr,
                                     INT4 i4SetValLldpPortConfigAdminStatus)
{

    tLldpLocPortInfo    LldpLocPortInfo;
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    INT4                i4CurrentAdminStatus = 0;
    UINT1               u1Event = 0;
    UINT4               u4LocPort = 0;

    if (LldpUtlGetLocalPort (i4LldpPortConfigPortNum, DestMacAddr, &u4LocPort)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD pLldpLocPortInfo is NULL LldpLocPortInfo is Invalid \r\n");
        return OSIX_FAILURE;
    }

    /* get current admin status */
    i4CurrentAdminStatus = pLldpLocPortInfo->PortConfigTable.i4AdminStatus;
    /* if current admin status and new admin status are same return success */
    if (i4CurrentAdminStatus == i4SetValLldpPortConfigAdminStatus)
    {
        return OSIX_SUCCESS;
    }
    switch (i4CurrentAdminStatus)
    {
        case LLDP_ADM_STAT_TX_ONLY:
            if (i4SetValLldpPortConfigAdminStatus == LLDP_ADM_STAT_RX_ONLY)
            {
                pLldpLocPortInfo->PortConfigTable.i4AdminStatus =
                    LLDP_ADM_STAT_RX_ONLY;
                if (gLldpGlobalInfo.u1ModuleStatus == LLDP_ENABLED)
                {
                    /* send TxAdmin down event */
                    u1Event = LLDP_TX_EV_TXMOD_ADMIN_DOWN;
                    LldpTxSmMachine (pLldpLocPortInfo, u1Event);
                    LldpTxTmrSmMachine (pLldpLocPortInfo, u1Event);
#ifdef DCBX_WANTED
                    LldpUtlNotifyDcbxOnTxRxStatChg (i4LldpPortConfigPortNum,
                                                    L2IWF_LLDP_TX_DISABLE_NOTIFY);
#endif
                    /* send RxAdmin up event */
                    u1Event = LLDP_RX_EV_RXMOD_ADMIN_UP;
                    LldpRxSemRun (pLldpLocPortInfo, u1Event);
#ifdef DCBX_WANTED
                    LldpUtlNotifyDcbxOnTxRxStatChg (i4LldpPortConfigPortNum,
                                                    L2IWF_LLDP_RX_ENABLE_NOTIFY);
#endif
                }
                else
                {
                    LLDP_TRC (ALL_FAILURE_TRC,
                              " SNMPSTD: LLDP module is disable, unable to trigger the state machine \n");
                }
            }
            else if (i4SetValLldpPortConfigAdminStatus ==
                     LLDP_ADM_STAT_TX_AND_RX)
            {
                pLldpLocPortInfo->PortConfigTable.i4AdminStatus =
                    LLDP_ADM_STAT_TX_AND_RX;
                if (gLldpGlobalInfo.u1ModuleStatus == LLDP_ENABLED)
                {
                    /*  send RxAdmin up event */
                    u1Event = LLDP_RX_EV_RXMOD_ADMIN_UP;
                    LldpRxSemRun (pLldpLocPortInfo, u1Event);
#ifdef DCBX_WANTED
                    LldpUtlNotifyDcbxOnTxRxStatChg (i4LldpPortConfigPortNum,
                                                    L2IWF_LLDP_RX_ENABLE_NOTIFY);
#endif
                }
                else
                {
                    LLDP_TRC (ALL_FAILURE_TRC,
                              " SNMPSTD: LLDP module is disable, unable to trigger the state machine \n");
                }
            }
            else if (i4SetValLldpPortConfigAdminStatus ==
                     LLDP_ADM_STAT_DISABLED)
            {
                pLldpLocPortInfo->PortConfigTable.i4AdminStatus =
                    LLDP_ADM_STAT_DISABLED;
                if (gLldpGlobalInfo.u1ModuleStatus == LLDP_ENABLED)
                {
                    /* send TxAdmin down event */
                    u1Event = LLDP_TX_EV_TXMOD_ADMIN_DOWN;
                    LldpTxSmMachine (pLldpLocPortInfo, u1Event);
                    LldpTxTmrSmMachine (pLldpLocPortInfo, u1Event);
#ifdef DCBX_WANTED
                    LldpUtlNotifyDcbxOnTxRxStatChg (i4LldpPortConfigPortNum,
                                                    L2IWF_LLDP_TX_DISABLE_NOTIFY);
#endif
                }
                else
                {
                    LLDP_TRC (ALL_FAILURE_TRC,
                              " SNMPSTD: LLDP module is disable, unable to trigger the state machine \n");
                }
            }
            break;
        case LLDP_ADM_STAT_RX_ONLY:
            if (i4SetValLldpPortConfigAdminStatus == LLDP_ADM_STAT_TX_ONLY)
            {
                pLldpLocPortInfo->PortConfigTable.i4AdminStatus =
                    LLDP_ADM_STAT_TX_ONLY;
                if (gLldpGlobalInfo.u1ModuleStatus == LLDP_ENABLED)
                {
                    /* send RxAdmin down event */
                    u1Event = LLDP_RX_EV_RXMOD_ADMIN_DOWN;
                    LldpRxSemRun (pLldpLocPortInfo, u1Event);
#ifdef DCBX_WANTED
                    LldpUtlNotifyDcbxOnTxRxStatChg (i4LldpPortConfigPortNum,
                                                    L2IWF_LLDP_RX_DISABLE_NOTIFY);
#endif
                    /* send TxAdmin up event */
                    u1Event = LLDP_TX_EV_TXMOD_ADMIN_UP;
                    LldpTxSmMachine (pLldpLocPortInfo, u1Event);
                    LldpTxTmrSmMachine (pLldpLocPortInfo, u1Event);
#ifdef DCBX_WANTED
                    LldpUtlNotifyDcbxOnTxRxStatChg (i4LldpPortConfigPortNum,
                                                    L2IWF_LLDP_TX_ENABLE_NOTIFY);
#endif
                }
                else
                {
                    LLDP_TRC (ALL_FAILURE_TRC,
                              " SNMPSTD: LLDP module is disable, unable to trigger the state machine \n");
                }
            }
            else if (i4SetValLldpPortConfigAdminStatus ==
                     LLDP_ADM_STAT_TX_AND_RX)
            {
                pLldpLocPortInfo->PortConfigTable.i4AdminStatus =
                    LLDP_ADM_STAT_TX_AND_RX;
                if (gLldpGlobalInfo.u1ModuleStatus == LLDP_ENABLED)
                {
                    /*  send TxAdmin up event */
                    u1Event = LLDP_TX_EV_TXMOD_ADMIN_UP;
                    LldpTxSmMachine (pLldpLocPortInfo, u1Event);
                    LldpTxTmrSmMachine (pLldpLocPortInfo, u1Event);
#ifdef DCBX_WANTED
                    LldpUtlNotifyDcbxOnTxRxStatChg (i4LldpPortConfigPortNum,
                                                    L2IWF_LLDP_TX_ENABLE_NOTIFY);
#endif
                }
                else
                {
                    LLDP_TRC (ALL_FAILURE_TRC,
                              " SNMPSTD: LLDP module is disable, unable to trigger the state machine \n");
                }
            }
            else if (i4SetValLldpPortConfigAdminStatus ==
                     LLDP_ADM_STAT_DISABLED)
            {
                pLldpLocPortInfo->PortConfigTable.i4AdminStatus =
                    LLDP_ADM_STAT_DISABLED;
                if (gLldpGlobalInfo.u1ModuleStatus == LLDP_ENABLED)
                {
                    /* send RxAdmin down event */
                    u1Event = LLDP_RX_EV_RXMOD_ADMIN_DOWN;
                    LldpRxSemRun (pLldpLocPortInfo, u1Event);
#ifdef DCBX_WANTED
                    LldpUtlNotifyDcbxOnTxRxStatChg (i4LldpPortConfigPortNum,
                                                    L2IWF_LLDP_RX_DISABLE_NOTIFY);
#endif
                }
                else
                {
                    LLDP_TRC (ALL_FAILURE_TRC,
                              " SNMPSTD: LLDP module is disable, unable to trigger the state machine \n");
                }
            }
            break;
        case LLDP_ADM_STAT_TX_AND_RX:
            if (i4SetValLldpPortConfigAdminStatus == LLDP_ADM_STAT_TX_ONLY)
            {
                pLldpLocPortInfo->PortConfigTable.i4AdminStatus =
                    LLDP_ADM_STAT_TX_ONLY;
                if (gLldpGlobalInfo.u1ModuleStatus == LLDP_ENABLED)
                {
                    /* send RxAdmin down event */
                    u1Event = LLDP_RX_EV_RXMOD_ADMIN_DOWN;
                    LldpRxSemRun (pLldpLocPortInfo, u1Event);
#ifdef DCBX_WANTED
                    LldpUtlNotifyDcbxOnTxRxStatChg (i4LldpPortConfigPortNum,
                                                    L2IWF_LLDP_RX_DISABLE_NOTIFY);
#endif
                }
                else
                {
                    LLDP_TRC (ALL_FAILURE_TRC,
                              " SNMPSTD: LLDP module is disable, unable to trigger the state machine \n");
                }
            }
            else if (i4SetValLldpPortConfigAdminStatus == LLDP_ADM_STAT_RX_ONLY)
            {
                pLldpLocPortInfo->PortConfigTable.i4AdminStatus =
                    LLDP_ADM_STAT_RX_ONLY;
                if (gLldpGlobalInfo.u1ModuleStatus == LLDP_ENABLED)
                {
                    /* send TxAdmin down event */
                    u1Event = LLDP_TX_EV_TXMOD_ADMIN_DOWN;
                    LldpTxSmMachine (pLldpLocPortInfo, u1Event);
                    LldpTxTmrSmMachine (pLldpLocPortInfo, u1Event);
#ifdef DCBX_WANTED
                    LldpUtlNotifyDcbxOnTxRxStatChg (i4LldpPortConfigPortNum,
                                                    L2IWF_LLDP_TX_DISABLE_NOTIFY);
#endif
                }
                else
                {
                    LLDP_TRC (ALL_FAILURE_TRC,
                              " SNMPSTD: LLDP module is disable, unable to trigger the state machine \n");
                }
            }
            else if (i4SetValLldpPortConfigAdminStatus ==
                     LLDP_ADM_STAT_DISABLED)
            {
                pLldpLocPortInfo->PortConfigTable.i4AdminStatus =
                    LLDP_ADM_STAT_DISABLED;
                if (gLldpGlobalInfo.u1ModuleStatus == LLDP_ENABLED)
                {
                    /* send TxAdmin down event */
                    u1Event = LLDP_TX_EV_TXMOD_ADMIN_DOWN;
                    LldpTxSmMachine (pLldpLocPortInfo, u1Event);
                    LldpTxTmrSmMachine (pLldpLocPortInfo, u1Event);
#ifdef DCBX_WANTED
                    LldpUtlNotifyDcbxOnTxRxStatChg (i4LldpPortConfigPortNum,
                                                    L2IWF_LLDP_TX_DISABLE_NOTIFY);
#endif
                    /* send RxAdmin down event */
                    u1Event = LLDP_RX_EV_RXMOD_ADMIN_DOWN;
                    LldpRxSemRun (pLldpLocPortInfo, u1Event);
#ifdef DCBX_WANTED
                    LldpUtlNotifyDcbxOnTxRxStatChg (i4LldpPortConfigPortNum,
                                                    L2IWF_LLDP_RX_DISABLE_NOTIFY);
#endif
                }
                else
                {
                    LLDP_TRC (ALL_FAILURE_TRC,
                              " SNMPSTD: LLDP module is disable, unable to trigger the state machine \n");
                }
            }
            break;
        case LLDP_ADM_STAT_DISABLED:
            if (i4SetValLldpPortConfigAdminStatus == LLDP_ADM_STAT_TX_ONLY)
            {
                pLldpLocPortInfo->PortConfigTable.i4AdminStatus =
                    LLDP_ADM_STAT_TX_ONLY;
                if (gLldpGlobalInfo.u1ModuleStatus == LLDP_ENABLED)
                {
                    /* send TxAdmin up event */
                    u1Event = LLDP_TX_EV_TXMOD_ADMIN_UP;
                    LldpTxSmMachine (pLldpLocPortInfo, u1Event);
                    LldpTxTmrSmMachine (pLldpLocPortInfo, u1Event);
#ifdef DCBX_WANTED
                    LldpUtlNotifyDcbxOnTxRxStatChg (i4LldpPortConfigPortNum,
                                                    L2IWF_LLDP_TX_ENABLE_NOTIFY);
#endif
                }
                else
                {
                    LLDP_TRC (ALL_FAILURE_TRC,
                              " SNMPSTD: LLDP module is disable, unable to trigger the state machine \n");
                }
            }
            else if (i4SetValLldpPortConfigAdminStatus == LLDP_ADM_STAT_RX_ONLY)
            {
                pLldpLocPortInfo->PortConfigTable.i4AdminStatus =
                    LLDP_ADM_STAT_RX_ONLY;
                if (gLldpGlobalInfo.u1ModuleStatus == LLDP_ENABLED)
                {
                    /* send RxAdmin up event */
                    u1Event = LLDP_RX_EV_RXMOD_ADMIN_UP;
                    LldpRxSemRun (pLldpLocPortInfo, u1Event);
#ifdef DCBX_WANTED
                    LldpUtlNotifyDcbxOnTxRxStatChg (i4LldpPortConfigPortNum,
                                                    L2IWF_LLDP_RX_ENABLE_NOTIFY);
#endif
                }
                else
                {
                    LLDP_TRC (ALL_FAILURE_TRC,
                              " SNMPSTD: LLDP module is disable, unable to trigger the state machine \n");
                }
            }
            else if (i4SetValLldpPortConfigAdminStatus ==
                     LLDP_ADM_STAT_TX_AND_RX)
            {
                pLldpLocPortInfo->PortConfigTable.i4AdminStatus =
                    LLDP_ADM_STAT_TX_AND_RX;
                if (gLldpGlobalInfo.u1ModuleStatus == LLDP_ENABLED)
                {
                    /* send TxAdmin up event */
                    u1Event = LLDP_TX_EV_TXMOD_ADMIN_UP;
                    LldpTxSmMachine (pLldpLocPortInfo, u1Event);
                    LldpTxTmrSmMachine (pLldpLocPortInfo, u1Event);
#ifdef DCBX_WANTED
                    LldpUtlNotifyDcbxOnTxRxStatChg (i4LldpPortConfigPortNum,
                                                    L2IWF_LLDP_TX_ENABLE_NOTIFY);
#endif
                    /* send RxAdmin up event */
                    u1Event = LLDP_RX_EV_RXMOD_ADMIN_UP;
                    LldpRxSemRun (pLldpLocPortInfo, u1Event);
#ifdef DCBX_WANTED
                    LldpUtlNotifyDcbxOnTxRxStatChg (i4LldpPortConfigPortNum,
                                                    L2IWF_LLDP_RX_ENABLE_NOTIFY);
#endif
                }
                else
                {
                    LLDP_TRC (ALL_FAILURE_TRC,
                              " SNMPSTD: LLDP module is disable, unable to trigger the state machine \n");
                }
            }
            break;
        default:
            LLDP_TRC (ALL_FAILURE_TRC,
                      " SNMPSTD: i4SetValLldpPortConfigAdminStatus invalid \n");
            break;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlSetLldpPortConfigNotificationEnable 
 *
 *    DESCRIPTION      : To Set the port Notofication status for per agent
 *                       
 *    INPUT            : u4LocPort - Local agent information
 *                       i4SetValLldpPortConfigNotificationEnable - 
 *                                   Value to be Set by user
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlSetLldpPortConfigNotificationEnable (INT4 i4LldpPortConfigPortNum,
                                            tMacAddr DestMacAddr,
                                            INT4
                                            i4SetValLldpPortConfigNotificationEnable)
{
    tLldpLocPortInfo    LldpLocPortInfo;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    UINT4               u4IfIndex = 0;
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    BOOL1               bNotifEnableFlag = OSIX_FALSE;
    UINT4               u4LocPort = 0;

    if (LldpUtlGetLocalPort (i4LldpPortConfigPortNum, DestMacAddr, &u4LocPort)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD pLldpLocalPortInfo is NULL LldpLocPortInfo is Invalid \r\n");
        return OSIX_FAILURE;
    }

    /* if current value and new value are same, then dont set the value
     * just return success */
    if (pLldpLocalPortInfo->PortConfigTable.u1NotificationEnable == (UINT1)
        i4SetValLldpPortConfigNotificationEnable)
    {
        return OSIX_SUCCESS;
    }
    pLldpLocalPortInfo->PortConfigTable.u1NotificationEnable = (UINT1)
        i4SetValLldpPortConfigNotificationEnable;

    /* Set the global notification enable flag to TRUE when notification
     * is enabled for the first port.*/
    if ((i4SetValLldpPortConfigNotificationEnable == LLDP_TRUE) &&
        (gLldpGlobalInfo.bNotificationEnable != OSIX_TRUE))
    {
        gLldpGlobalInfo.bNotificationEnable = OSIX_TRUE;
    }

    /* Scan through all the valid ports and if notification is disabled
     * on all the ports, the notification interval timer need to be stopped*/
    if (i4SetValLldpPortConfigNotificationEnable == LLDP_FALSE)
    {
        pLldpLocPortInfo = LLDP_GET_LOC_PORT_INFO (u4IfIndex);
        while (pLldpLocPortInfo != NULL)
        {
            {
                if (pLldpLocPortInfo->PortConfigTable.u1NotificationEnable ==
                    LLDP_TRUE)
                {
                    bNotifEnableFlag = OSIX_TRUE;
                    break;
                }
            }
            LldpLocPortInfo.u4LocPortNum = pLldpLocPortInfo->u4LocPortNum;
            pLldpLocPortInfo =
                RBTreeGetNext (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
                               &LldpLocPortInfo, LldpAgentInfoUtlRBCmpInfo);
        }
        /* Set the global notification enable flag to FALSE and stop the timer
         * if sending the notification has been disabled on all the ports.*/
        if (bNotifEnableFlag != OSIX_TRUE)
        {
            gLldpGlobalInfo.bNotificationEnable = OSIX_FALSE;
            if ((LLDP_MODULE_STATUS () == LLDP_ENABLED) &&
                (LLDP_NOTIF_INTVAL_TMR_STATUS () == LLDP_TMR_RUNNING))
            {
                if (TmrStop (gLldpGlobalInfo.TmrListId,
                             &(gLldpGlobalInfo.NotifIntervalTmr))
                    != TMR_SUCCESS)
                {
                    LLDP_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                              "nmhSetLldpPortConfigNotificationEnable: Stopping"
                              " the notification interval timer failed \r\n");
                }
                LLDP_NOTIF_INTVAL_TMR_STATUS () = LLDP_TMR_NOT_RUNNING;
            }
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlSetLldpPortConfigTLVsTxEnable 
 *
 *    DESCRIPTION      : To Set the port Tx TLV status for per agent
 *                       
 *    INPUT            : u4LocPort - Local agent information
 *                       pSetValLldpPortConfigTLVsTxEnable - 
 *                                   Value to be Set by user
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlSetLldpPortConfigTLVsTxEnable (INT4 i4LldpPortConfigPortNum,
                                      tMacAddr DestMacAddr,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSetValLldpPortConfigTLVsTxEnable)
{
    tLldpLocPortInfo    LldpLocPortInfo;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    UINT4               u4LocPort = 0;
    UINT1               u1NewTLVsTxEnable = 0;
    UINT1               u1CurrTLVsTxEnable = 0;

    if (LldpUtlGetLocalPort (i4LldpPortConfigPortNum, DestMacAddr, &u4LocPort)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD pLldpLocPortInfo is NULL LldpLocPortInfo is Invalid \r\n");
        return OSIX_FAILURE;
    }

    /* if current value and new value are same, then dont set the value
     * just return success */
    MEMCPY (&u1NewTLVsTxEnable,
            pSetValLldpPortConfigTLVsTxEnable->pu1_OctetList,
            pSetValLldpPortConfigTLVsTxEnable->i4_Length);

    u1CurrTLVsTxEnable = pLldpLocalPortInfo->PortConfigTable.u1TLVsTxEnable;

    if (u1CurrTLVsTxEnable == u1NewTLVsTxEnable)
    {
        return OSIX_SUCCESS;
    }
    pLldpLocalPortInfo->PortConfigTable.u1TLVsTxEnable = u1NewTLVsTxEnable;

    /* Since TLVs to be transmitted on this port is changed, constrcut the
     * preformed buffer for that port - which is equal to change in
     * local system information for the port. Hence handle the change */
    if (LldpTxUtlHandleLocPortInfoChg (pLldpLocalPortInfo) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "SNMPSTD: "
                  "LldpTxUtlHandleLocPortInfoChg failed\r\n");
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlTestLldpPortConfigAdminStatus 
 *
 *    DESCRIPTION      : To Test the port Tx TLV status for per agent
 *                       
 *    INPUT            : u4LocPort - Local agent information
 *                       pSetValLldpPortConfigTLVsTxEnable - 
 *                                   Value to be Set by user
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlTestLldpPortConfigAdminStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4LldpPortConfigPortNum,
                                      tMacAddr DestMacAddr,
                                      INT4 i4TestValLldpPortConfigAdminStatus)
{
    tLldpLocPortInfo   *pLocPortInfo = NULL;
    UINT4               u4LocPort = 0;
    INT4                i4AdminStatus = 0;

    if (LldpUtlGetLocalPort (i4LldpPortConfigPortNum, DestMacAddr, &u4LocPort)
        == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    pLocPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPort);
    if (pLocPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: LldpTxUtlValidatePortIndex "
                  "returns FAILURE \r\n");
        return OSIX_FAILURE;
    }

    LldpMedGetAdminStatus (i4LldpPortConfigPortNum, &i4AdminStatus);

    if ((i4TestValLldpPortConfigAdminStatus == LLDP_ADM_STAT_TX_ONLY) ||
        (i4TestValLldpPortConfigAdminStatus == LLDP_ADM_STAT_RX_ONLY) ||
        (i4TestValLldpPortConfigAdminStatus == LLDP_ADM_STAT_DISABLED))
    {
        /* Check Whether any LLDP-MED TLV is enabled */
        if ((pLocPortInfo->LocMedCapInfo.u2MedLocCapTxEnable &
             LLDP_MED_CAPABILITY_TLV) && (i4AdminStatus == LLDP_ENABLED))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            LLDP_TRC (ALL_FAILURE_TRC, " LldpUtlTestLldpPortConfigAdminStatus:"
                      " Unable to disable Tx only and Rx only when Med Tlvs"
                      " are enabled");
            return OSIX_FAILURE;
        }
    }

    /* test i4TestValLldpPortConfigAdminStatus */
    if ((i4TestValLldpPortConfigAdminStatus != LLDP_ADM_STAT_TX_ONLY) &&
        (i4TestValLldpPortConfigAdminStatus != LLDP_ADM_STAT_RX_ONLY) &&
        (i4TestValLldpPortConfigAdminStatus != LLDP_ADM_STAT_TX_AND_RX) &&
        (i4TestValLldpPortConfigAdminStatus != LLDP_ADM_STAT_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "i4TestValLldpPortConfigAdminStatus is not valid \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlTestLldpPortConfigNotificationEnable 
 *
 *    DESCRIPTION      : To Test the port Tx TLV status for per agent
 *                       
 *    INPUT            : u4LocPort - Local agent information
 *                       i4TestValLldpPortConfigNotificationEnable - 
 *                                   Value to be tested
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlTestLldpPortConfigNotificationEnable (UINT4 *pu4ErrorCode,
                                             INT4 i4LldpPortConfigPortNum,
                                             tMacAddr DestMacAddr,
                                             INT4
                                             i4TestValLldpPortConfigNotificationEnable)
{
    tLldpLocPortInfo   *pLocPortInfo = NULL;
    UINT4               u4LocPort = 0;

    if (LldpUtlGetLocalPort (i4LldpPortConfigPortNum, DestMacAddr, &u4LocPort)
        == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    pLocPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPort);
    if (pLocPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "Unable to get local port Info \r\n");
        return OSIX_FAILURE;
    }
    /* test i4TestValLldpPortConfigNotificationEnable */
    if ((i4TestValLldpPortConfigNotificationEnable != LLDP_TRUE) &&
        (i4TestValLldpPortConfigNotificationEnable != LLDP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "i4TestValLldpPortConfigNotificationEnable is not valid \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlTestLldpPortConfigTLVsTxEnable  
 *
 *    DESCRIPTION      : To Test the port Tx TLV status for per agent
 *                       
 *    INPUT            : u4LocPort - Local agent information
 *                       i4TestValLldpPortConfigNotificationEnable - 
 *                                   Value to be tested
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlTestLldpPortConfigTLVsTxEnable (UINT4 *pu4ErrorCode,
                                       INT4 i4LldpPortConfigPortNum,
                                       tMacAddr DestMacAddr,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pTestValLldpPortConfigTLVsTxEnable)
{
    tLldpLocPortInfo   *pLocPortInfo = NULL;
    UINT4               u4LocPort = 0;
    UINT1               u1TlvTxEnable = 0;

    if (LldpUtlGetLocalPort (i4LldpPortConfigPortNum, DestMacAddr, &u4LocPort)
        == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    pLocPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPort);
    if (pLocPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: LldpTxUtlValidatePortIndex "
                  "returns FAILURE \r\n");
        return OSIX_FAILURE;
    }

    if (pTestValLldpPortConfigTLVsTxEnable->i4_Length >
        LLDP_MAX_LEN_BASIC_TLV_TXENABLE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpPortConfigTLVsTxEnable length Exceeds "
                  "max length!!\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return OSIX_FAILURE;
    }
    MEMCPY (&u1TlvTxEnable, pTestValLldpPortConfigTLVsTxEnable->pu1_OctetList,
            sizeof (u1TlvTxEnable));

    /* test pTestValLldpPortConfigTLVsTxEnable */
    /*                  0 1 2 3  0 */
    /* TLV tx enable ie 1 1 1 1  0 0 0 0 - if any bit other than bit0 to bit3
     * is set return FALILURE */
    if (u1TlvTxEnable & ~LLDP_MAX_BASIC_TLV_TXENABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "pTestValLldpPortConfigTLVsTxEnable is not valid \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpLocManAddrTable 
 *
 *    DESCRIPTION      : To Validate the Local Management address 
 *                       
 *    INPUT            : i4LldpLocManAddrSubtype - Management address subtype
 *                       pLldpLocManAddr - Management address
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpLocManAddrTable (INT4 i4LldpLocManAddrSubtype,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pLldpLocManAddr)
{
    UINT1               au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR];
    tLldpLocManAddrTable *pManAddrNode = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }

    if (pLldpLocManAddr->i4_Length > LLDP_MAX_LEN_MAN_ADDR)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: Length exceed the maximum value !!\r\n");
        return OSIX_FAILURE;
    }

    MEMCPY (&au1LocManAddr[0], pLldpLocManAddr->pu1_OctetList,
            pLldpLocManAddr->i4_Length);

    /* Check whether the Entry is exist or not, indices are validated inside
     * the Get function */
    pManAddrNode = (tLldpLocManAddrTable *)
        LldpTxUtlGetLocManAddrNode (i4LldpLocManAddrSubtype, &au1LocManAddr[0]);
    if (pManAddrNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpTxUtlGetLocManAddrNode returns NULL\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpLocManAddrTable
 *
 *    DESCRIPTION      : To Get First Index of  the Local Management address table
 *                       
 *    INPUT            : i4LldpLocManAddrSubtype - Management address subtype
 *                       pLldpLocManAddr - Management address
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpLocManAddrTable (INT4 *pi4LldpLocManAddrSubtype,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pLldpLocManAddr)
{
    INT4                i4ManAddrLen = 0;
    tLldpLocManAddrTable *pFirstLocalManAddrNode = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }

    pFirstLocalManAddrNode = (tLldpLocManAddrTable *)
        RBTreeGetFirst (gLldpGlobalInfo.LocManAddrRBTree);

    if (pFirstLocalManAddrNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: No Local management address Entry"
                  "exists (RBTreeGetFirst returns NULL)\r\n");
        return OSIX_FAILURE;
    }

    *pi4LldpLocManAddrSubtype = pFirstLocalManAddrNode->i4LocManAddrSubtype;
    i4ManAddrLen = (INT4)
        LldpUtilGetManAddrLen (pFirstLocalManAddrNode->i4LocManAddrSubtype);
    MEMCPY (pLldpLocManAddr->pu1_OctetList,
            pFirstLocalManAddrNode->au1LocManAddr, i4ManAddrLen);
    pLldpLocManAddr->i4_Length = i4ManAddrLen;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexLldpLocManAddrTable
 *
 *    DESCRIPTION      : To Get next Index of  the Local Management address table
 *                       
 *    INPUT            : i4LldpLocManAddrSubtype - Management address subtype
 *                       pLldpLocManAddr - Management address
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexLldpLocManAddrTable (INT4 i4LldpLocManAddrSubtype,
                                        INT4 *pi4NextLldpLocManAddrSubtype,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pLldpLocManAddr,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextLldpLocManAddr)
{
    tLldpLocManAddrTable *pNextLocManNode = NULL;
    tLldpLocManAddrTable *pCurrLocManNode = NULL;
    UINT1               au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR];
    INT4                i4ManAddrLen = 0;

    if (pLldpLocManAddr->i4_Length > LLDP_MAX_LEN_MAN_ADDR)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: Length exceed the maximum value !!\r\n");
        return OSIX_FAILURE;
    }
    else if (pLldpLocManAddr->i4_Length <= 0)
    {
        pNextLocManNode = (tLldpLocManAddrTable *)
            RBTreeGetFirst (gLldpGlobalInfo.LocManAddrRBTree);
    }
    else
    {
        MEMSET (&au1LocManAddr[0], 0, LLDP_MAX_LEN_MAN_ADDR);
        MEMCPY (&au1LocManAddr[0], pLldpLocManAddr->pu1_OctetList,
                pLldpLocManAddr->i4_Length);
        /* Get the Current Node */
        pCurrLocManNode = (tLldpLocManAddrTable *)
            LldpTxUtlGetLocManAddrNode (i4LldpLocManAddrSubtype,
                                        &au1LocManAddr[0]);

        if (pCurrLocManNode == NULL)
        {
            LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                      "LldpTxUtlGetLocManAddrNode returns NULL\r\n");
            return OSIX_FAILURE;
        }

        /* Get the Next Node */
        if (LldpTxUtlGetNextManAddr (pCurrLocManNode, &pNextLocManNode)
            != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }

    LLDP_CHK_NULL_PTR_RET (pNextLocManNode, SNMP_FAILURE);

    /* Return the Next Node Indices */
    *pi4NextLldpLocManAddrSubtype = pNextLocManNode->i4LocManAddrSubtype;
    i4ManAddrLen = (INT4)
        LldpUtilGetManAddrLen (pNextLocManNode->i4LocManAddrSubtype);
    MEMCPY (pNextLldpLocManAddr->pu1_OctetList,
            pNextLocManNode->au1LocManAddr, i4ManAddrLen);
    pNextLldpLocManAddr->i4_Length = i4ManAddrLen;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLocManAddrLen
 *
 *    DESCRIPTION      : To Get the Local Management address length 
 *                       
 *    INPUT            : i4LldpLocManAddrSubtype - Management address subtype
 *                       pLldpLocManAddr - Management address
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLocManAddrLen (INT4 i4LldpLocManAddrSubtype,
                         tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr,
                         INT4 *pi4RetValLldpLocManAddrLen)
{
    tLldpLocManAddrTable *pLocManAddrNode = NULL;
    UINT1               au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR];

    MEMSET (&au1LocManAddr[0], 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMCPY (&au1LocManAddr[0], pLldpLocManAddr->pu1_OctetList,
            pLldpLocManAddr->i4_Length);

    pLocManAddrNode = (tLldpLocManAddrTable *)
        LldpTxUtlGetLocManAddrNode (i4LldpLocManAddrSubtype, &au1LocManAddr[0]);

    if (pLocManAddrNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpTxUtlGetLocManAddrNode returns NULL \r\n");
        return OSIX_FAILURE;
    }

    *pi4RetValLldpLocManAddrLen = pLocManAddrNode->i4LocManAddrLen;
    return OSIX_SUCCESS;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLocManAddrIfSubtype
 *
 *    DESCRIPTION      : To Get the Local Management address length 
 *                       
 *    INPUT            : i4LldpLocManAddrSubtype - Management address subtype
 *                       pLldpLocManAddr - Management address
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLocManAddrIfSubtype (INT4 i4LldpLocManAddrSubtype,
                               tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr,
                               INT4 *pi4RetValLldpLocManAddrIfSubtype)
{
    tLldpLocManAddrTable *pLocManAddrNode = NULL;
    UINT1               au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR];

    MEMSET (&au1LocManAddr[0], 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMCPY (&au1LocManAddr[0], pLldpLocManAddr->pu1_OctetList,
            pLldpLocManAddr->i4_Length);

    pLocManAddrNode = (tLldpLocManAddrTable *)
        LldpTxUtlGetLocManAddrNode (i4LldpLocManAddrSubtype, &au1LocManAddr[0]);

    if (pLocManAddrNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpTxUtlGetLocManAddrNode returns NULL \r\n");
        return OSIX_FAILURE;
    }

    *pi4RetValLldpLocManAddrIfSubtype = pLocManAddrNode->i4LocManAddrIfSubtype;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLocManAddrIfId
 *
 *    DESCRIPTION      : To Get the Local Management address length 
 *                       
 *    INPUT            : i4LldpLocManAddrSubtype - Management address subtype
 *                       pLldpLocManAddr - Management address
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLocManAddrIfId (INT4 i4LldpLocManAddrSubtype,
                          tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr,
                          INT4 *pi4RetValLldpLocManAddrIfId)
{
    tLldpLocManAddrTable *pLocManAddrNode = NULL;
    UINT1               au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR];

    MEMSET (&au1LocManAddr[0], 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMCPY (&au1LocManAddr[0], pLldpLocManAddr->pu1_OctetList,
            pLldpLocManAddr->i4_Length);

    pLocManAddrNode = (tLldpLocManAddrTable *)
        LldpTxUtlGetLocManAddrNode (i4LldpLocManAddrSubtype, &au1LocManAddr[0]);

    if (pLocManAddrNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpTxUtlGetLocManAddrNode returns NULL \r\n");
        return OSIX_FAILURE;
    }

    *pi4RetValLldpLocManAddrIfId = pLocManAddrNode->i4LocManAddrIfId;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLocManAddrIfOID
 *
 *    DESCRIPTION      : To Get the Local Management address OID
 *                       
 *    INPUT            : i4LldpLocManAddrSubtype - Management address subtype
 *                       pLldpLocManAddr - Management address
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLocManAddrIfOID (INT4 i4LldpLocManAddrSubtype,
                           tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr,
                           tSNMP_OID_TYPE * pRetValLldpLocManAddrOID)
{
    tLldpLocManAddrTable *pLocManAddrNode = NULL;
    UINT1               au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR];

    MEMSET (&au1LocManAddr[0], 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMCPY (&au1LocManAddr[0], pLldpLocManAddr->pu1_OctetList,
            pLldpLocManAddr->i4_Length);

    pLocManAddrNode = (tLldpLocManAddrTable *)
        LldpTxUtlGetLocManAddrNode (i4LldpLocManAddrSubtype, &au1LocManAddr[0]);

    if (pLocManAddrNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpTxUtlGetLocManAddrNode returns NULL \r\n");
        return OSIX_FAILURE;
    }

    pRetValLldpLocManAddrOID->u4_Length =
        pLocManAddrNode->LocManAddrOID.u4_Length;
    MEMCPY (pRetValLldpLocManAddrOID->pu4_OidList,
            pLocManAddrNode->LocManAddrOID.au4_OidList,
            MEM_MAX_BYTES ((pRetValLldpLocManAddrOID->u4_Length
                            * sizeof (UINT4)), LLDP_MAX_LEN_MAN_OID));

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetConfigManAddrPortsTxEnable
 *
 *    DESCRIPTION      : To Get the Local Management address OID
 *                       
 *    INPUT            : i4LldpLocManAddrSubtype - Management address subtype
 *                       pLldpLocManAddr - Management address
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetConfigManAddrPortsTxEnable (INT4 i4LldpLocManAddrSubtype,
                                      tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValLldpConfigManAddrPortsTxEnable)
{
    tLldpLocManAddrTable *pLocManAddrNode = NULL;
    UINT1               au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR];

    MEMSET (&au1LocManAddr[0], 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMCPY (&au1LocManAddr[0], pLldpLocManAddr->pu1_OctetList,
            pLldpLocManAddr->i4_Length);

    pLocManAddrNode = (tLldpLocManAddrTable *)
        LldpTxUtlGetLocManAddrNode (i4LldpLocManAddrSubtype, &au1LocManAddr[0]);

    if (pLocManAddrNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpTxUtlGetLocManAddrNode returns NULL \r\n");
        return OSIX_FAILURE;
    }

    MEMCPY (pRetValLldpConfigManAddrPortsTxEnable->pu1_OctetList,
            pLocManAddrNode->au1LocManAddrPortsTxEnable,
            LLDP_MAN_ADDR_TX_EN_MAX_BYTES);

    pRetValLldpConfigManAddrPortsTxEnable->i4_Length =
        LLDP_MAN_ADDR_TX_EN_MAX_BYTES;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlSetConfigManAddrPortsTxEnable
 *
 *    DESCRIPTION      : To Set the Local Management address for the Tx port
 *                       
 *    INPUT            : i4LldpLocManAddrSubtype - Management address subtype
 *                       pLldpLocManAddr - Management address
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlSetConfigManAddrPortsTxEnable (INT4 i4LldpLocManAddrSubtype,
                                      tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSetValLldpConfigManAddrPortsTxEnable)
{
    tLldpLocManAddrTable *pLocManAddrNode = NULL;
    UINT1               au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR];
    tLldpLocPortInfo   *pPortInfo = NULL;
    tLldpLocPortInfo    PortInfo;
    BOOL1               bResult = OSIX_FALSE;

    MEMSET (&au1LocManAddr[0], 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMCPY (&au1LocManAddr[0], pLldpLocManAddr->pu1_OctetList,
            pLldpLocManAddr->i4_Length);

    pLocManAddrNode = (tLldpLocManAddrTable *)
        LldpTxUtlGetLocManAddrNode (i4LldpLocManAddrSubtype, &au1LocManAddr[0]);

    if (pLocManAddrNode == NULL)
    {
        if ((pLocManAddrNode = (tLldpLocManAddrTable *)
             (MemAllocMemBlk (gLldpGlobalInfo.LocManAddrPoolId))) == NULL)
        {
            LLDP_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC | LLDP_CRITICAL_TRC,
                      "LldpTxUtlAddIpv4Addr : Failed to allocate memory for "
                      "Local ManAddr Node\r\n");
            return OSIX_FAILURE;
        }
        MEMSET (pLocManAddrNode, 0, sizeof (tLldpLocManAddrTable));
        pLocManAddrNode->i4LocManAddrSubtype = i4LldpLocManAddrSubtype;
        MEMCPY (pLocManAddrNode->au1LocManAddr, au1LocManAddr,
                LLDP_MAX_LEN_MAN_ADDR);

        if (RBTreeAdd
            (gLldpGlobalInfo.LocManAddrRBTree,
             (tRBElem *) pLocManAddrNode) != RB_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC | LLDP_CRITICAL_TRC,
                      "LldpTxUtlAddIpv4Addr : Failed to Add the Node to "
                      "LocManAddrRBTree\r\n");
            MemReleaseMemBlock (gLldpGlobalInfo.LocManAddrPoolId,
                                (UINT1 *) pLocManAddrNode);
            return OSIX_FAILURE;
        }
    }

    MEMCPY (pLocManAddrNode->au1LocManAddrPortsTxEnable,
            pSetValLldpConfigManAddrPortsTxEnable->pu1_OctetList,
            pSetValLldpConfigManAddrPortsTxEnable->i4_Length);

    MEMCPY (pLocManAddrNode->au1LocManAddrRowstats,
            pSetValLldpConfigManAddrPortsTxEnable->pu1_OctetList,
            pSetValLldpConfigManAddrPortsTxEnable->i4_Length);
    MEMSET (&PortInfo, 0, sizeof (tLldpLocPortInfo));
    pPortInfo = (tLldpLocPortInfo *)
        RBTreeGetFirst (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree);
    while (pPortInfo != NULL)

    {
        OSIX_BITLIST_IS_BIT_SET (pLocManAddrNode->au1LocManAddrPortsTxEnable,
                                 (UINT2) pPortInfo->u4LocPortNum,
                                 (INT4)
                                 sizeof (pLocManAddrNode->
                                         au1LocManAddrPortsTxEnable), bResult);
        if (bResult == OSIX_TRUE)
        {
            pPortInfo->u1ManAddrTlvTxEnabled = LLDP_TRUE;
        }
        else
        {
            pPortInfo->u1ManAddrTlvTxEnabled = LLDP_FALSE;
        }

        PortInfo.u4LocPortNum = pPortInfo->u4LocPortNum;
        pPortInfo = (tLldpLocPortInfo *)
            RBTreeGetNext (gLldpGlobalInfo.
                           LldpLocPortAgentInfoRBTree,
                           &PortInfo, LldpAgentInfoUtlRBCmpInfo);
    }
    LldpTxUtlHandleLocSysInfoChg ();
    return OSIX_SUCCESS;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlTestConfigManAddrPortsTxEnable
 *
 *    DESCRIPTION      : To Set the Local Management address for the Tx port
 *                       
 *    INPUT            : i4LldpLocManAddrSubtype - Management address subtype
 *                       pLldpLocManAddr - Management address
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlTestConfigManAddrPortsTxEnable (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pTestValLldpConfigManAddrPortsTxEnable)
{

    if (pTestValLldpConfigManAddrPortsTxEnable->i4_Length >
        LLDP_MAN_ADDR_TX_EN_MAX_BYTES)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "pTestValLldpConfigManAddrPortsTxEnable is not valid \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetStatsRemTablesLastChangeTime
 *
 *    DESCRIPTION      : To Get status of the remote table updated lastly
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetStatsRemTablesLastChangeTime (UINT4
                                        *pu4RetValLldpStatsRemTablesLastChangeTime)
{
    /* remote table statistics are not cleared when the module is
     * shutdown. so return the actual remote table last change time
     * even if the module is shutdown */
    *pu4RetValLldpStatsRemTablesLastChangeTime =
        LLDP_REMOTE_STAT_LAST_CHNG_TIME;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetStatsRemTablesInserts
 *
 *    DESCRIPTION      : To Get status of the remote table Inserted a new neighbor
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetStatsRemTablesInserts (UINT4 *pu4RetValLldpStatsRemTablesInserts)
{
    /* remote table statistics are not cleared when the module is
     * shutdown. so return the actual remote table insert count
     * even if the module is shutdown */
    *pu4RetValLldpStatsRemTablesInserts = LLDP_REMOTE_STAT_TABLES_INSERTS;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetStatsRemTablesDeletes
 *
 *    DESCRIPTION      : To Get status of the remote table Deletes the Information
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetStatsRemTablesDeletes (UINT4 *pu4RetValLldpStatsRemTablesDeletes)
{
    /* remote table statistics are not cleared when the module is
     * shutdown. so return the actual remote table delete count
     * even if the module is shutdown */
    *pu4RetValLldpStatsRemTablesDeletes = LLDP_REMOTE_STAT_TABLES_DELETES;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetStatsRemTablesDrops
 *
 *    DESCRIPTION      : To Get status of the remote table Drop count
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetStatsRemTablesDrops (UINT4 *pu4RetValLldpStatsRemTablesDrops)
{
    /* remote table statistics are not cleared when the module is
     * shutdown. so return the actual remote table drop count
     * even if the module is shutdown */
    *pu4RetValLldpStatsRemTablesDrops = LLDP_REMOTE_STAT_TABLES_DROPS;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetStatsRemTablesAgeouts
 *
 *    DESCRIPTION      : To Get status of the remote table Age-out count
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetStatsRemTablesAgeouts (UINT4 *pu4RetValLldpStatsRemTablesAgeouts)
{
    /* remote table statistics are not cleared when the module is
     * shutdown. so return the actual remote table age-out count
     * even if the module is shutdown */
    *pu4RetValLldpStatsRemTablesAgeouts = LLDP_REMOTE_STAT_TABLES_AGEOUTS;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetStatsTxPortFramesTotal
 *
 *    DESCRIPTION      : To Get status of the total Tx port frames 
 *
 *    INPUT            : i4LldpStatsTxPortNum - Tx port Number
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetStatsTxPortFramesTotal (INT4 i4LldpPortConfigPortNum,
                                  UINT4 *pu4RetValLldpStatsTxPortFramesTotal,
                                  tMacAddr DestMacAddr)
{
    tLldpLocPortInfo    LldpLocPortInfo;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    UINT4               u4LocPort = 0;

    if (LldpUtlGetLocalPort (i4LldpPortConfigPortNum, DestMacAddr, &u4LocPort)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD pLldpLocPortInfo is NULL LldpLocPortInfo is Invalid \r\n");
        return OSIX_FAILURE;
    }

    *pu4RetValLldpStatsTxPortFramesTotal =
        pLldpLocalPortInfo->StatsTxPortTable.u4TxFramesTotal;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetStatsRxPortFramesDiscardedTotal
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : i4LldpStatsTxPortNum - Tx port Number
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetStatsRxPortFramesDiscardedTotal (INT4 i4LldpStatsRxPortNum,
                                           tMacAddr DestMacAddr,
                                           UINT4
                                           *pu4RetValLldpStatsRxPortFramesDiscardedTotal)
{
    tLldpLocPortInfo    LldpLocPortInfo;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    UINT4               u4LocPort = 0;

    if (LldpUtlGetLocalPort (i4LldpStatsRxPortNum, DestMacAddr, &u4LocPort) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD pLldpLocPortInfo is NULL LldpLocPortInfo is Invalid \r\n");
        return OSIX_FAILURE;
    }

    *pu4RetValLldpStatsRxPortFramesDiscardedTotal =
        (UINT4) pLldpLocalPortInfo->StatsRxPortTable.i4FramesDiscardedTotal;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetStatsRxPortFramesErrors
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : i4LldpStatsTxPortNum - Tx port Number
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetStatsRxPortFramesErrors (INT4 i4LldpStatsRxPortNum,
                                   tMacAddr DestMacAddr,
                                   UINT4 *pu4RetValLldpStatsRxPortFramesErrors)
{
    tLldpLocPortInfo    LldpLocPortInfo;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    UINT4               u4LocPort = 0;

    if (LldpUtlGetLocalPort (i4LldpStatsRxPortNum, DestMacAddr, &u4LocPort) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD pLldpLocPortInfo is NULL or LldpLocPortInfo is Invalid \r\n");
        return OSIX_FAILURE;
    }

    *pu4RetValLldpStatsRxPortFramesErrors =
        pLldpLocalPortInfo->StatsRxPortTable.u4FramesErrors;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetStatsRxPortFramesTotal
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : i4LldpStatsTxPortNum - Tx port Number
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetStatsRxPortFramesTotal (INT4 i4LldpStatsRxPortNum,
                                  tMacAddr DestMacAddr,
                                  UINT4 *pu4RetValLldpStatsRxPortFramesTotal)
{
    tLldpLocPortInfo    LldpLocPortInfo;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    UINT4               u4LocPort = 0;

    if (LldpUtlGetLocalPort (i4LldpStatsRxPortNum, DestMacAddr, &u4LocPort) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD pLldpLocPortInfo is NULL or LldpLocPortInfo is Invalid \r\n");
        return OSIX_FAILURE;
    }
    *pu4RetValLldpStatsRxPortFramesTotal =
        pLldpLocalPortInfo->StatsRxPortTable.u4RxFramesTotal;

    return OSIX_SUCCESS;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetStatsRxPortTLVsDiscardedTotal
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : i4LldpStatsTxPortNum - Tx port Number
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetStatsRxPortTLVsDiscardedTotal (INT4 i4LldpStatsRxPortNum,
                                         tMacAddr DestMacAddr,
                                         UINT4
                                         *pu4RetValLldpStatsRxPortTLVsDiscardedTotal)
{
    tLldpLocPortInfo    LldpLocPortInfo;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    UINT4               u4LocPort = 0;

    if (LldpUtlGetLocalPort (i4LldpStatsRxPortNum, DestMacAddr, &u4LocPort) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD pLldpLocPortInfo is NULL or LldpLocPortInfo is Invalid \r\n");
        return OSIX_FAILURE;
    }

    *pu4RetValLldpStatsRxPortTLVsDiscardedTotal =
        pLldpLocalPortInfo->StatsRxPortTable.u4TLVsDiscardedTotal;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetStatsRxPortTLVsUnrecognizedTotal
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : i4LldpStatsTxPortNum - Tx port Number
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetStatsRxPortTLVsUnrecognizedTotal (INT4 i4LldpStatsRxPortNum,
                                            tMacAddr DestMacAddr,
                                            UINT4
                                            *pu4RetValLldpStatsRxPortTLVsUnrecognizedTotal)
{
    tLldpLocPortInfo    LldpLocPortInfo;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    UINT4               u4LocPort = 0;

    if (LldpUtlGetLocalPort (i4LldpStatsRxPortNum, DestMacAddr, &u4LocPort) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD pLldpLocPortInfo is NULL or LldpLocPortInfo is Invalid \r\n");
        return OSIX_FAILURE;
    }

    *pu4RetValLldpStatsRxPortTLVsUnrecognizedTotal =
        pLldpLocalPortInfo->StatsRxPortTable.u4TLVsUnrecognizedTotal;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetStatsRxPortAgeoutsTotal
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : i4LldpStatsTxPortNum - Tx port Number
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetStatsRxPortAgeoutsTotal (INT4 i4LldpStatsRxPortNum,
                                   tMacAddr DestMacAddr,
                                   UINT4 *pu4RetValLldpStatsRxPortAgeoutsTotal)
{
    tLldpLocPortInfo    LldpLocPortInfo;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    UINT4               u4LocPort = 0;

    if (LldpUtlGetLocalPort (i4LldpStatsRxPortNum, DestMacAddr, &u4LocPort) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD pLldpLocPortInfo is NULL or LldpLocPortInfo is Invalid \r\n");
        return OSIX_FAILURE;
    }

    *pu4RetValLldpStatsRxPortAgeoutsTotal =
        pLldpLocalPortInfo->StatsRxPortTable.u4AgeoutsTotal;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLocChassisIdSubtype
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLocChassisIdSubtype (INT4 *pi4RetValLldpLocChassisIdSubtype)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        /* LLDP is shutdown, so return default value */
        *pi4RetValLldpLocChassisIdSubtype = (INT4) LLDP_CHASS_ID_SUB_MAC_ADDR;
        return OSIX_SUCCESS;
    }

    *pi4RetValLldpLocChassisIdSubtype =
        gLldpGlobalInfo.LocSysInfo.i4LocChassisIdSubtype;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLocChassisId
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLocChassisId (tSNMP_OCTET_STRING_TYPE * pRetValLldpLocChassisId)
{
    UINT2               u2ChassisIdLen = 0;
    tMacAddr            SwitchMacAddr;

    MEMSET (SwitchMacAddr, 0, sizeof (tMacAddr));

    if (LLDP_IS_SHUTDOWN ())
    {
        /* return default chasssis id - switch mac address */
        LldpPortGetSysMacAddr (SwitchMacAddr);
        u2ChassisIdLen = (UINT2) MAC_ADDR_LEN;
        MEMCPY (pRetValLldpLocChassisId->pu1_OctetList, SwitchMacAddr,
                u2ChassisIdLen);
        pRetValLldpLocChassisId->i4_Length = u2ChassisIdLen;
        return OSIX_SUCCESS;
    }

    if (LldpUtilGetChassisIdLen
        (gLldpGlobalInfo.LocSysInfo.i4LocChassisIdSubtype,
         gLldpGlobalInfo.LocSysInfo.au1LocChassisId,
         &u2ChassisIdLen) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD:LldpUtilGetChassisIdLen returns FAILURE !!\r\n");
        return OSIX_FAILURE;
    }
    MEMCPY (pRetValLldpLocChassisId->pu1_OctetList,
            gLldpGlobalInfo.LocSysInfo.au1LocChassisId,
            MEM_MAX_BYTES (u2ChassisIdLen, LLDP_MAX_LEN_CHASSISID));
    pRetValLldpLocChassisId->i4_Length = u2ChassisIdLen;
    return OSIX_SUCCESS;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlLocSysName
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlLocSysName (tSNMP_OCTET_STRING_TYPE * pRetValLldpLocSysName)
{
    UINT2               u2SysNameLen = 0;

    /* system name has no default value, so return the actual value even
     * when the module is shutdown */
    u2SysNameLen =
        (UINT2) (LLDP_STRLEN (gLldpGlobalInfo.LocSysInfo.au1LocSysName,
                              LLDP_MAX_LEN_SYSNAME));
    MEMCPY (pRetValLldpLocSysName->pu1_OctetList,
            gLldpGlobalInfo.LocSysInfo.au1LocSysName, u2SysNameLen);
    pRetValLldpLocSysName->i4_Length = u2SysNameLen;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlLocSysDesc
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlLocSysDesc (tSNMP_OCTET_STRING_TYPE * pRetValLldpLocSysDesc)
{
    UINT2               u2SysDescLen = 0;

    /* system description has no default value, so return the actual value even
     * when the module is shutdown */
    u2SysDescLen =
        (UINT2) (LLDP_STRLEN (gLldpGlobalInfo.LocSysInfo.au1LocSysDesc,
                              LLDP_MAX_LEN_SYSDESC));
    MEMCPY (pRetValLldpLocSysDesc->pu1_OctetList,
            gLldpGlobalInfo.LocSysInfo.au1LocSysDesc, u2SysDescLen);
    pRetValLldpLocSysDesc->i4_Length = u2SysDescLen;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlLocSysCapSupported
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlLocSysCapSupported (tSNMP_OCTET_STRING_TYPE *
                           pRetValLldpLocSysCapSupported)
{
    /* system capabilities supported has no default value, so return the actual
     * value even when the module is shutdown */
    MEMCPY (pRetValLldpLocSysCapSupported->pu1_OctetList,
            &(gLldpGlobalInfo.LocSysInfo.au1LocSysCapSupported),
            ISS_SYS_CAPABILITIES_LEN);
    pRetValLldpLocSysCapSupported->i4_Length = ISS_SYS_CAPABILITIES_LEN;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlLocSysCapEnabled
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlLocSysCapEnabled (tSNMP_OCTET_STRING_TYPE * pRetValLldpLocSysCapEnabled)
{
    /* system capabilities enabled has no default value, so return the actual
     * value even when the module is shutdown */
    MEMCPY (pRetValLldpLocSysCapEnabled->pu1_OctetList,
            &(gLldpGlobalInfo.LocSysInfo.au1LocSysCapEnabled),
            ISS_SYS_CAPABILITIES_LEN);
    pRetValLldpLocSysCapEnabled->i4_Length = ISS_SYS_CAPABILITIES_LEN;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlLocPortIdSubtype
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLocPortIdSubtype (INT4 i4LldpLocPortNum,
                            INT4 *pi4RetValLldpLocPortIdSubtype)
{
    tLldpLocPortTable  *pPortInfo = NULL;
    tLldpLocPortTable   PortEntry;

    MEMSET (&PortEntry, 0, sizeof (tLldpLocPortTable));
    PortEntry.i4IfIndex = i4LldpLocPortNum;
    pPortInfo = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, (tRBElem *) & PortEntry);
    if (pPortInfo == NULL)        /*For Klocwork */
    {
        return OSIX_FAILURE;
    }

    *pi4RetValLldpLocPortIdSubtype = pPortInfo->i4LocPortIdSubtype;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlLocPortId
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlLocPortId (INT4 i4LldpLocPortNum,
                  tSNMP_OCTET_STRING_TYPE * pRetValLldpLocPortId)
{
    tLldpLocPortTable  *pPortInfo = NULL;
    tLldpLocPortTable   PortEntry;
    UINT2               u2PortIdLen = 0;

    MEMSET (&PortEntry, 0, sizeof (tLldpLocPortTable));
    PortEntry.i4IfIndex = i4LldpLocPortNum;
    pPortInfo = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, (tRBElem *) & PortEntry);
    if (pPortInfo == NULL)        /*For Klocwork */
    {
        return OSIX_FAILURE;
    }

    if (LldpUtilGetPortIdLen (pPortInfo->i4LocPortIdSubtype,
                              pPortInfo->au1LocPortId,
                              &u2PortIdLen) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtilGetPortIdLen is failed \r\n");
        return OSIX_FAILURE;
    }
    MEMCPY (pRetValLldpLocPortId->pu1_OctetList,
            pPortInfo->au1LocPortId,
            MEM_MAX_BYTES (u2PortIdLen, LLDP_MAX_LEN_PORTID));
    pRetValLldpLocPortId->i4_Length = u2PortIdLen;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLocPortDesc
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLocPortDesc (INT4 i4LldpLocPortNum,
                       tSNMP_OCTET_STRING_TYPE * pRetValLldpLocPortDesc)
{
    tLldpLocPortTable  *pPortInfo = NULL;
    tLldpLocPortTable   PortEntry;
    UINT2               u2PortDescLen = 0;

    MEMSET (&PortEntry, 0, sizeof (tLldpLocPortTable));
    PortEntry.i4IfIndex = i4LldpLocPortNum;
    pPortInfo = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, (tRBElem *) & PortEntry);
    if (pPortInfo == NULL)        /*For Klocwork */
    {
        return OSIX_FAILURE;
    }

    u2PortDescLen =
        (UINT2) (LLDP_STRLEN (pPortInfo->au1LocPortDesc,
                              LLDP_MAX_LEN_PORTDESC));
    MEMCPY (pRetValLldpLocPortDesc->pu1_OctetList,
            pPortInfo->au1LocPortDesc, u2PortDescLen);
    pRetValLldpLocPortDesc->i4_Length = u2PortDescLen;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetRemChassisIdSubtype
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetRemChassisIdSubtype (UINT4 u4LldpRemTimeMark,
                               INT4 i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                               INT4 *pi4RetValLldpRemChassisIdSubtype,
                               UINT4 u4DestIndex)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    pRemoteNode = LldpRxUtlGetRemoteNode (u4LldpRemTimeMark,
                                          (INT4) i4LldpRemLocalPortNum,
                                          u4DestIndex, i4LldpRemIndex);
    if (pRemoteNode != NULL)
    {
        *pi4RetValLldpRemChassisIdSubtype = pRemoteNode->i4RemChassisIdSubtype;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetRemChassisId
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetRemChassisId (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                        INT4 i4LldpRemIndex,
                        tSNMP_OCTET_STRING_TYPE * pRetValLldpRemChassisId,
                        UINT4 u4DestMacAddr)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT2               u2ChassIdLen = 0;
    pRemoteNode = LldpRxUtlGetRemoteNode (u4LldpRemTimeMark,
                                          i4LldpRemLocalPortNum, u4DestMacAddr,
                                          i4LldpRemIndex);
    if (pRemoteNode != NULL)
    {
        if (LldpUtilGetChassisIdLen (pRemoteNode->i4RemChassisIdSubtype,
                                     pRemoteNode->au1RemChassisId,
                                     &u2ChassIdLen) != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD:LldpUtilGetChassisIdLen "
                      "returns FAILURE !!\r\n");
            return OSIX_FAILURE;
        }

        MEMCPY (pRetValLldpRemChassisId->pu1_OctetList,
                pRemoteNode->au1RemChassisId,
                MEM_MAX_BYTES (u2ChassIdLen, LLDP_MAX_LEN_CHASSISID));
        pRetValLldpRemChassisId->i4_Length = u2ChassIdLen;

        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetRemPortIdSubtype
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetRemPortIdSubtype (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                            INT4 i4LldpRemIndex,
                            INT4 *pi4RetValLldpRemPortIdSubtype,
                            UINT4 u4DestMacAddr)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    pRemoteNode = LldpRxUtlGetRemoteNode (u4LldpRemTimeMark,
                                          i4LldpRemLocalPortNum, u4DestMacAddr,
                                          i4LldpRemIndex);
    if (pRemoteNode != NULL)
    {
        *pi4RetValLldpRemPortIdSubtype = pRemoteNode->i4RemPortIdSubtype;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetRemPortId 
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetRemPortId (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                     INT4 i4LldpRemIndex,
                     tSNMP_OCTET_STRING_TYPE * pRetValLldpRemPortId,
                     UINT4 u4DestMacAddr)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT2               u2PortIdLen = 0;
    pRemoteNode = LldpRxUtlGetRemoteNode (u4LldpRemTimeMark,
                                          i4LldpRemLocalPortNum, u4DestMacAddr,
                                          i4LldpRemIndex);
    if (pRemoteNode != NULL)
    {
        if (LldpUtilGetPortIdLen (pRemoteNode->i4RemPortIdSubtype,
                                  pRemoteNode->au1RemPortId,
                                  &u2PortIdLen) != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC,
                      " SNMPSTD: LldpUtilGetPortIdLen is failed \r\n");
            return OSIX_FAILURE;
        }

        MEMCPY (pRetValLldpRemPortId->pu1_OctetList,
                pRemoteNode->au1RemPortId,
                MEM_MAX_BYTES (u2PortIdLen, LLDP_MAX_LEN_PORTID));
        pRetValLldpRemPortId->i4_Length = u2PortIdLen;

        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetRemPortDesc
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetRemPortDesc (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                       INT4 i4LldpRemIndex,
                       tSNMP_OCTET_STRING_TYPE * pRetValLldpRemPortDesc,
                       UINT4 u4DestMacAddr)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    INT4                i4PortDescLen = 0;
    pRemoteNode = LldpRxUtlGetRemoteNode (u4LldpRemTimeMark,
                                          i4LldpRemLocalPortNum, u4DestMacAddr,
                                          i4LldpRemIndex);
    if (pRemoteNode != NULL)
    {
        i4PortDescLen =
            (INT4) LLDP_STRLEN (pRemoteNode->au1RemPortDesc,
                                LLDP_MAX_LEN_PORTDESC);
        MEMCPY (pRetValLldpRemPortDesc->pu1_OctetList,
                pRemoteNode->au1RemPortDesc, i4PortDescLen);
        pRetValLldpRemPortDesc->i4_Length = i4PortDescLen;

        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetRemSysName
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetRemSysName (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                      INT4 i4LldpRemIndex,
                      tSNMP_OCTET_STRING_TYPE * pRetValLldpRemSysName,
                      UINT4 u4DestMacAddr)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    INT4                i4SysNameLen = 0;
    pRemoteNode = LldpRxUtlGetRemoteNode (u4LldpRemTimeMark,
                                          i4LldpRemLocalPortNum, u4DestMacAddr,
                                          i4LldpRemIndex);
    if (pRemoteNode != NULL)
    {
        i4SysNameLen =
            (INT4) LLDP_STRLEN (pRemoteNode->au1RemSysName,
                                LLDP_MAX_LEN_SYSNAME);
        MEMCPY (pRetValLldpRemSysName->pu1_OctetList,
                pRemoteNode->au1RemSysName, i4SysNameLen);
        pRetValLldpRemSysName->i4_Length = i4SysNameLen;

        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetRemSysDesc
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetRemSysDesc (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                      INT4 i4LldpRemIndex,
                      tSNMP_OCTET_STRING_TYPE * pRetValLldpRemSysDesc,
                      UINT4 u4DestMacAddr)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    INT4                i4SysDescLen = 0;
    pRemoteNode = LldpRxUtlGetRemoteNode (u4LldpRemTimeMark,
                                          i4LldpRemLocalPortNum, u4DestMacAddr,
                                          i4LldpRemIndex);
    if (pRemoteNode != NULL)
    {
        i4SysDescLen =
            (INT4) LLDP_STRLEN (pRemoteNode->au1RemSysDesc,
                                LLDP_MAX_LEN_SYSDESC);
        MEMCPY (pRetValLldpRemSysDesc->pu1_OctetList,
                pRemoteNode->au1RemSysDesc, i4SysDescLen);
        pRetValLldpRemSysDesc->i4_Length = i4SysDescLen;

        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetRemSysCapSupported
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetRemSysCapSupported (UINT4 u4LldpRemTimeMark,
                              INT4 i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValLldpRemSysCapSupported,
                              UINT4 u4DestMacAddr)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    pRemoteNode = LldpRxUtlGetRemoteNode (u4LldpRemTimeMark,
                                          i4LldpRemLocalPortNum, u4DestMacAddr,
                                          i4LldpRemIndex);
    if (pRemoteNode != NULL)
    {
        pRetValLldpRemSysCapSupported->i4_Length = ISS_SYS_CAPABILITIES_LEN;

        MEMCPY (pRetValLldpRemSysCapSupported->pu1_OctetList,
                pRemoteNode->au1RemSysCapSupported,
                pRetValLldpRemSysCapSupported->i4_Length);

        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetRemSysCapEnabled
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetRemSysCapEnabled (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                            INT4 i4LldpRemIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValLldpRemSysCapEnabled, UINT4 u4DestMacAddr)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    pRemoteNode = LldpRxUtlGetRemoteNode (u4LldpRemTimeMark,
                                          i4LldpRemLocalPortNum, u4DestMacAddr,
                                          i4LldpRemIndex);
    if (pRemoteNode != NULL)
    {
        pRetValLldpRemSysCapEnabled->i4_Length = ISS_SYS_CAPABILITIES_LEN;

        MEMCPY (pRetValLldpRemSysCapEnabled->pu1_OctetList,
                pRemoteNode->au1RemSysCapEnabled,
                pRetValLldpRemSysCapEnabled->i4_Length);

        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetRemManAddrIfSubtype
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetRemManAddrIfSubtype (UINT4 u4LldpRemTimeMark,
                               INT4 i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                               INT4 i4LldpRemManAddrSubtype,
                               tSNMP_OCTET_STRING_TYPE * pLldpRemManAddr,
                               INT4 *pi4RetValLldpRemManAddrIfSubtype,
                               UINT4 u4DestMacAddr)
{
    tLldpRemManAddressTable *pRemManAddrNode = NULL;
    UINT1               au1RemManAddr[LLDP_MAX_LEN_MAN_ADDR];
    MEMSET (&au1RemManAddr[0], 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMCPY (&au1RemManAddr[0], pLldpRemManAddr->pu1_OctetList,
            pLldpRemManAddr->i4_Length);

    pRemManAddrNode = LldpRxUtlGetRemManAddrNode (u4LldpRemTimeMark,
                                                  i4LldpRemLocalPortNum,
                                                  u4DestMacAddr, i4LldpRemIndex,
                                                  i4LldpRemManAddrSubtype,
                                                  &au1RemManAddr[0]);

    if (pRemManAddrNode != NULL)
    {
        *pi4RetValLldpRemManAddrIfSubtype =
            pRemManAddrNode->i4RemManAddrIfSubtype;
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetRemRemoteChanges
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetRemRemoteChanges (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                            INT4 i4LldpRemIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pi4RetValLldpV2RemRemoteChanges,
                            UINT4 u4DestMacAddr)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    pRemoteNode = LldpRxUtlGetRemoteNode (u4LldpRemTimeMark,
                                          i4LldpRemLocalPortNum, u4DestMacAddr,
                                          i4LldpRemIndex);
    if (pRemoteNode != NULL)
    {
        *pi4RetValLldpV2RemRemoteChanges->pu1_OctetList =
            (UINT1) pRemoteNode->bRemoteChanges;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetRemManAddrIfId
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetRemManAddrIfId (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                          INT4 i4LldpRemIndex, INT4 i4LldpRemManAddrSubtype,
                          tSNMP_OCTET_STRING_TYPE * pLldpRemManAddr,
                          INT4 *pi4RetValLldpRemManAddrIfId,
                          UINT4 u4DestMacAddr)
{
    tLldpRemManAddressTable *pRemManAddrNode = NULL;
    UINT1               au1RemManAddr[LLDP_MAX_LEN_MAN_ADDR];
    MEMSET (&au1RemManAddr[0], 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMCPY (&au1RemManAddr[0], pLldpRemManAddr->pu1_OctetList,
            pLldpRemManAddr->i4_Length);

    pRemManAddrNode = LldpRxUtlGetRemManAddrNode (u4LldpRemTimeMark,
                                                  i4LldpRemLocalPortNum,
                                                  u4DestMacAddr,
                                                  i4LldpRemIndex,
                                                  i4LldpRemManAddrSubtype,
                                                  &au1RemManAddr[0]);

    if (pRemManAddrNode != NULL)
    {
        *pi4RetValLldpRemManAddrIfId = pRemManAddrNode->i4RemManAddrIfId;
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetRemManAddrOID
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetRemManAddrOID (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                         INT4 i4LldpRemIndex, INT4 i4LldpRemManAddrSubtype,
                         tSNMP_OCTET_STRING_TYPE * pLldpRemManAddr,
                         tSNMP_OID_TYPE * pRetValLldpRemManAddrOID,
                         UINT4 u4DestMacAddr)
{
    tLldpRemManAddressTable *pRemManAddrNode = NULL;
    UINT1               au1RemManAddr[LLDP_MAX_LEN_MAN_ADDR];
    MEMSET (&au1RemManAddr[0], 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMCPY (&au1RemManAddr[0], pLldpRemManAddr->pu1_OctetList,
            pLldpRemManAddr->i4_Length);

    pRemManAddrNode = LldpRxUtlGetRemManAddrNode (u4LldpRemTimeMark,
                                                  i4LldpRemLocalPortNum,
                                                  u4DestMacAddr,
                                                  i4LldpRemIndex,
                                                  i4LldpRemManAddrSubtype,
                                                  &au1RemManAddr[0]);

    if (pRemManAddrNode != NULL)
    {
        pRetValLldpRemManAddrOID->u4_Length =
            pRemManAddrNode->RemManAddrOID.u4_Length;

        MEMCPY (pRetValLldpRemManAddrOID->pu4_OidList,
                pRemManAddrNode->RemManAddrOID.au4_OidList,
                MEM_MAX_BYTES ((pRetValLldpRemManAddrOID->
                                u4_Length * sizeof (UINT4)),
                               (LLDP_MAX_LEN_MAN_OID * sizeof (UINT4))));

        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetRemUnknownTLVInfo
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetRemUnknownTLVInfo (UINT4 u4LldpRemTimeMark,
                             INT4 i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                             INT4 i4LldpRemUnknownTLVType,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValLldpRemUnknownTLVInfo, UINT4 u4DestMacAddr)
{
    tLldpRemUnknownTLVTable *pRemUnknownTlv = NULL;
    INT4                i4UnknownTlvInfoLen = 0;
    pRemUnknownTlv = LldpRxUtlGetRemUnknownTlvInfo (u4LldpRemTimeMark,
                                                    i4LldpRemLocalPortNum,
                                                    u4DestMacAddr,
                                                    i4LldpRemIndex,
                                                    i4LldpRemUnknownTLVType);

    if (pRemUnknownTlv != NULL)
    {
        i4UnknownTlvInfoLen = pRemUnknownTlv->u2RemUnknownTLVInfoLen;
        MEMCPY (pRetValLldpRemUnknownTLVInfo->pu1_OctetList,
                pRemUnknownTlv->au1RemUnknownTLVInfo, i4UnknownTlvInfoLen);
        pRetValLldpRemUnknownTLVInfo->i4_Length = i4UnknownTlvInfoLen;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetRemOrgDefInfo
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetRemOrgDefInfo (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                         INT4 i4LldpRemIndex,
                         tSNMP_OCTET_STRING_TYPE * pLldpRemOrgDefInfoOUI,
                         INT4 i4LldpRemOrgDefInfoSubtype,
                         INT4 i4LldpRemOrgDefInfoIndex,
                         tSNMP_OCTET_STRING_TYPE * pRetValLldpRemOrgDefInfo,
                         UINT4 u4DestMacAddr)
{
    tLldpRemOrgDefInfoTable *pOrgDefInfo = NULL;
    pOrgDefInfo =
        LldpRxUtlGetRemOrgDefInfo (u4LldpRemTimeMark,
                                   i4LldpRemLocalPortNum,
                                   u4DestMacAddr,
                                   i4LldpRemIndex,
                                   pLldpRemOrgDefInfoOUI->pu1_OctetList,
                                   i4LldpRemOrgDefInfoSubtype,
                                   i4LldpRemOrgDefInfoIndex);
    if (pOrgDefInfo != NULL)
    {
        MEMCPY (pRetValLldpRemOrgDefInfo->pu1_OctetList,
                pOrgDefInfo->au1RemOrgDefInfo,
                MEM_MAX_BYTES (pOrgDefInfo->u2RemOrgDefInfoLen,
                               LLDP_MAX_LEN_ORGDEF_INFO));
        pRetValLldpRemOrgDefInfo->i4_Length = pOrgDefInfo->u2RemOrgDefInfoLen;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetMessageTxInterval
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetMessageTxInterval (INT4 *pi4RetValLldpMessageTxInterval)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        /* LLDP is shutdown, so return default value */
        *pi4RetValLldpMessageTxInterval = (INT4) LLDP_MSG_TX_INTERVAL;
        return OSIX_SUCCESS;
    }

    *pi4RetValLldpMessageTxInterval =
        gLldpGlobalInfo.SysConfigInfo.i4MsgTxInterval;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetMessageTxHoldMultiplier
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetMessageTxHoldMultiplier (INT4 *pi4RetValLldpMessageTxHoldMultiplier)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        /* LLDP system is shutdown, so return default value */
        *pi4RetValLldpMessageTxHoldMultiplier =
            (INT4) LLDP_MSG_TX_HOLD_MULTIPLIER;
        return OSIX_SUCCESS;
    }

    *pi4RetValLldpMessageTxHoldMultiplier =
        gLldpGlobalInfo.SysConfigInfo.i4MsgTxHoldMultiplier;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetReinitDelay
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetReinitDelay (INT4 *pi4RetValLldpReinitDelay)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        /* LLDP system is shutdown, so return default value */
        *pi4RetValLldpReinitDelay = (INT4) LLDP_REINIT_DELAY;
        return OSIX_SUCCESS;
    }
    *pi4RetValLldpReinitDelay = gLldpGlobalInfo.SysConfigInfo.i4ReInitDelay;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetTxDelay
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetTxDelay (INT4 *pi4RetValLldpTxDelay)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        /* LLDP system is shutdown, so return default value */
        *pi4RetValLldpTxDelay = (INT4) LLDP_TX_DELAY;
        return OSIX_SUCCESS;
    }

    *pi4RetValLldpTxDelay = gLldpGlobalInfo.SysConfigInfo.i4TxDelay;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNotificationInterval
 *
 *    DESCRIPTION      : To Get status of Rx frames discarded totally
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNotificationInterval (INT4 *pi4RetValLldpNotificationInterval)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        /* LLDP system is shutdown, so return default value */
        *pi4RetValLldpNotificationInterval = (INT4) LLDP_NOTIFICATION_INTERVAL;
        return OSIX_SUCCESS;
    }

    *pi4RetValLldpNotificationInterval =
        gLldpGlobalInfo.SysConfigInfo.i4NotificationInterval;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlSetMessageTxInterval
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlSetMessageTxInterval (INT4 i4SetValLldpMessageTxInterval)
{
    if (gLldpGlobalInfo.SysConfigInfo.i4MsgTxInterval !=
        i4SetValLldpMessageTxInterval)
    {
        /* Update the Transmitt interval */
        gLldpGlobalInfo.SysConfigInfo.i4MsgTxInterval =
            i4SetValLldpMessageTxInterval;
        LldpTxUtlHandleLocSysInfoChg ();
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlSetMessageTxHoldMultiplier
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlSetMessageTxHoldMultiplier (INT4 i4SetValLldpMessageTxHoldMultiplier)
{
    if (gLldpGlobalInfo.SysConfigInfo.i4MsgTxHoldMultiplier !=
        i4SetValLldpMessageTxHoldMultiplier)
    {
        /* Update the Hold Time Multiplier */
        gLldpGlobalInfo.SysConfigInfo.i4MsgTxHoldMultiplier =
            i4SetValLldpMessageTxHoldMultiplier;
        LldpTxUtlHandleLocSysInfoChg ();
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlSetReinitDelay
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlSetReinitDelay (INT4 i4SetValLldpReinitDelay)
{
    if (gLldpGlobalInfo.SysConfigInfo.i4ReInitDelay != i4SetValLldpReinitDelay)
    {
        /* Update the Reinit Delay */
        gLldpGlobalInfo.SysConfigInfo.i4ReInitDelay = i4SetValLldpReinitDelay;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlSetTxDelay
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlSetTxDelay (INT4 i4SetValLldpTxDelay)
{
    if (gLldpGlobalInfo.SysConfigInfo.i4TxDelay != i4SetValLldpTxDelay)
    {
        /* Update the TxDelay */
        gLldpGlobalInfo.SysConfigInfo.i4TxDelay = i4SetValLldpTxDelay;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlSetNotificationInterval
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlSetNotificationInterval (INT4 i4SetValLldpNotificationInterval)
{
    if (gLldpGlobalInfo.SysConfigInfo.i4NotificationInterval !=
        i4SetValLldpNotificationInterval)
    {
        /* Update the Notification Interval */
        gLldpGlobalInfo.SysConfigInfo.i4NotificationInterval =
            i4SetValLldpNotificationInterval;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlTestMessageTxInterval
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlTestMessageTxInterval (UINT4 *pu4ErrorCode,
                              INT4 i4TestValLldpMessageTxInterval)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }

    if ((i4TestValLldpMessageTxInterval < LLDP_MIN_MSG_INTERVAL) ||
        (i4TestValLldpMessageTxInterval > LLDP_MAX_MSG_INTERVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: i4TestValLldpMessageTxInterval is"
                  " not within the allowed range \r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlTestMessageTxHoldMultiplier
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlTestMessageTxHoldMultiplier (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValLldpMessageTxHoldMultiplier)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }
    if ((i4TestValLldpMessageTxHoldMultiplier < LLDP_MIN_HOLD_COUNT) ||
        (i4TestValLldpMessageTxHoldMultiplier > LLDP_MAX_HOLD_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "i4TestValLldpMessageTxHoldMultiplier is not within the "
                  "allowed range \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlTestReinitDelay 
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlTestReinitDelay (UINT4 *pu4ErrorCode, INT4 i4TestValLldpReinitDelay)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }

    if ((i4TestValLldpReinitDelay < LLDP_MIN_REINIT_DELAY) ||
        (i4TestValLldpReinitDelay > LLDP_MAX_REINIT_DELAY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: i4TestValLldpReinitDelay is not "
                  "within the allowed range \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlTestTxDelay
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlTestTxDelay (UINT4 *pu4ErrorCode, INT4 i4TestValLldpTxDelay)
{
    INT4                i4MsgInterval = 0;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_09)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (LLDP_CLI_INVALID_TX_DELAY_V2);
        return OSIX_FAILURE;
    }

    i4MsgInterval = gLldpGlobalInfo.SysConfigInfo.i4MsgTxInterval;

    /* 1 <= lldpTxDelay <= (0.25 * lldpMessageTxInterval) */
    if ((i4TestValLldpTxDelay < LLDP_MIN_TXDELAY) ||
        (i4TestValLldpTxDelay > LLDP_MAX_TXDELAY) ||
        (i4TestValLldpTxDelay > ((INT4) (0.25 * i4MsgInterval))))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: i4TestValLldpTxDelay is not "
                  "within the allowed range \r\n");
        CLI_SET_ERR (LLDP_CLI_ERR_INVALID_TX_DELAY);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlTestNotificationInterval
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlTestNotificationInterval (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValLldpNotificationInterval)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }

    if ((i4TestValLldpNotificationInterval < LLDP_MIN_NOTIFY_INTVAL) ||
        (i4TestValLldpNotificationInterval > LLDP_MAX_NOTIFY_INTVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: i4TestValLldpNotificationInterval"
                  "is not within the allowed range \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpPortConfigTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpPortConfigTable (INT4 i4LldpPortConfigPortNum,
                                                 tMacAddr DestMacAddr)
{
    tLldpLocPortInfo   *pLocPortInfo = NULL;
    UINT4               u4LocPort = 0;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }

    if (LldpUtlGetLocalPort (i4LldpPortConfigPortNum, DestMacAddr, &u4LocPort)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    pLocPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPort);
    if (pLocPortInfo == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: LldpTxUtlValidatePortIndex "
                  "returns FAILURE \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpPortConfigTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpPortConfigTable (INT4 *pi4LldpPortConfigPortNum)
{
    tLldpLocPortTable  *pLocPortTbl = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }

    pLocPortTbl =
        (tLldpLocPortTable *) RBTreeGetFirst (gLldpGlobalInfo.
                                              LldpPortInfoRBTree);
    if (pLocPortTbl != NULL)
    {
        *pi4LldpPortConfigPortNum = pLocPortTbl->i4IfIndex;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexLldpPortConfigTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexLldpPortConfigTable (INT4 i4LldpPortConfigPortNum,
                                        INT4 *pi4NextLldpPortConfigPortNum)
{
    tLldpLocPortTable   CurrLocPortTbl;
    tLldpLocPortTable  *pNextLocPortTbl = NULL;

    CurrLocPortTbl.i4IfIndex = i4LldpPortConfigPortNum;

    pNextLocPortTbl =
        (tLldpLocPortTable *) RBTreeGetNext (gLldpGlobalInfo.LldpPortInfoRBTree,
                                             &CurrLocPortTbl,
                                             LldpPortInfoUtlRBCmpInfo);

    if (pNextLocPortTbl != NULL)
    {
        *pi4NextLldpPortConfigPortNum = pNextLocPortTbl->i4IfIndex;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceConfigManAddrTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceConfigManAddrTable (INT4 i4LldpLocManAddrSubtype,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pLldpLocManAddr)
{
    UINT1               au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR];
    tLldpLocManAddrTable *pLocManAddrNode = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }

    MEMCPY (&au1LocManAddr[0], pLldpLocManAddr->pu1_OctetList,
            pLldpLocManAddr->i4_Length);

    /* Check whether the Entry is exist or not, indices are validaed inside
     * the Get function */
    pLocManAddrNode = (tLldpLocManAddrTable *)
        LldpTxUtlGetLocManAddrNode (i4LldpLocManAddrSubtype, &au1LocManAddr[0]);
    if (pLocManAddrNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpTxUtlGetLocManAddrNode returns NULL \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpConfigManAddrTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpConfigManAddrTable (INT4 *pi4LldpLocManAddrSubtype,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pLldpLocManAddr)
{
    INT4                i4ManAddrLen = 0;
    tLldpLocManAddrTable *pFirstLocalManAddrNode = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }

    pFirstLocalManAddrNode = (tLldpLocManAddrTable *)
        RBTreeGetFirst (gLldpGlobalInfo.LocManAddrRBTree);

    if (pFirstLocalManAddrNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: No Local management address "
                  "config Entry exists (RBTreeGetFirst returns NULL)\r\n");
        return OSIX_FAILURE;
    }

    *pi4LldpLocManAddrSubtype = pFirstLocalManAddrNode->i4LocManAddrSubtype;
    i4ManAddrLen = (INT4)
        LldpUtilGetManAddrLen (pFirstLocalManAddrNode->i4LocManAddrSubtype);
    MEMCPY (pLldpLocManAddr->pu1_OctetList,
            pFirstLocalManAddrNode->au1LocManAddr, i4ManAddrLen);
    pLldpLocManAddr->i4_Length = i4ManAddrLen;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexLldpConfigManAddrTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexLldpConfigManAddrTable (INT4 i4LldpLocManAddrSubtype,
                                           INT4 *pi4NextLldpLocManAddrSubtype,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pLldpLocManAddr,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pNextLldpLocManAddr)
{
    tLldpLocManAddrTable *pNextLocManNode = NULL;
    tLldpLocManAddrTable *pCurrLocManNode = NULL;
    UINT1               au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR];
    INT4                i4ManAddrLen = 0;

    if (pLldpLocManAddr->i4_Length > LLDP_MAX_LEN_MAN_ADDR)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: Length exceed the maximum value !!\r\n");
        return OSIX_FAILURE;
    }
    else if (pLldpLocManAddr->i4_Length <= 0)
    {
        pNextLocManNode = (tLldpLocManAddrTable *)
            RBTreeGetFirst (gLldpGlobalInfo.LocManAddrRBTree);
    }
    else
    {
        MEMSET (&au1LocManAddr[0], 0, LLDP_MAX_LEN_MAN_ADDR);
        MEMCPY (&au1LocManAddr[0], pLldpLocManAddr->pu1_OctetList,
                pLldpLocManAddr->i4_Length);
        /* Get the Current Node */
        pCurrLocManNode = (tLldpLocManAddrTable *)
            LldpTxUtlGetLocManAddrNode (i4LldpLocManAddrSubtype,
                                        &au1LocManAddr[0]);
        if (pCurrLocManNode == NULL)
        {
            LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                      "LldpTxUtlGetLocManAddrNode returns NULL \r\n");
            return OSIX_FAILURE;
        }
        /* Get the Next Node */
        if (LldpTxUtlGetNextManAddr (pCurrLocManNode, &pNextLocManNode)
            != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }

    LLDP_CHK_NULL_PTR_RET (pNextLocManNode, SNMP_FAILURE);

    /* Return the Next Node Indices */
    *pi4NextLldpLocManAddrSubtype = pNextLocManNode->i4LocManAddrSubtype;
    i4ManAddrLen = (INT4)
        LldpUtilGetManAddrLen (pNextLocManNode->i4LocManAddrSubtype);
    MEMCPY (pNextLldpLocManAddr->pu1_OctetList,
            pNextLocManNode->au1LocManAddr, i4ManAddrLen);
    pNextLldpLocManAddr->i4_Length = i4ManAddrLen;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpStatsTxPortTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpStatsTxPortTable (INT4 i4LldpStatsTxPortNum,
                                                  tMacAddr DestMacAddr)
{
    tLldpLocPortInfo   *pLocPort = NULL;
    UINT4               u4LocPort = 0;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }

    if (LldpUtlGetLocalPort (i4LldpStatsTxPortNum, DestMacAddr, &u4LocPort) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    pLocPort = LLDP_GET_LOC_PORT_INFO (u4LocPort);
    if (pLocPort == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpTxUtlValidatePortIndex returns FAILURE \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpStatsTxPortTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpStatsTxPortTable (INT4 *pi4LldpStatsTxPortNum)
{
    tLldpLocPortInfo   *pLocPortTbl = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }

    pLocPortTbl =
        (tLldpLocPortInfo *) RBTreeGetFirst (gLldpGlobalInfo.
                                             LldpLocPortAgentInfoRBTree);
    if (pLocPortTbl != NULL)
    {
        *pi4LldpStatsTxPortNum = (INT4) pLocPortTbl->u4LocPortNum;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextLldpStatsTxPortTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextLldpStatsTxPortTable (INT4 i4LldpStatsTxPortNum,
                                    INT4 *pi4NextLldpStatsTxPortNum)
{
    tLldpLocPortInfo    CurrLocPortTbl;
    tLldpLocPortInfo   *pNextLocPortTbl = NULL;

    CurrLocPortTbl.u4LocPortNum = (UINT4) i4LldpStatsTxPortNum;

    pNextLocPortTbl =
        (tLldpLocPortInfo *) RBTreeGetNext (gLldpGlobalInfo.
                                            LldpLocPortAgentInfoRBTree,
                                            &CurrLocPortTbl,
                                            LldpAgentInfoUtlRBCmpInfo);

    if (pNextLocPortTbl != NULL)
    {
        *pi4NextLldpStatsTxPortNum = (INT4) pNextLocPortTbl->u4LocPortNum;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpStatsRxPortTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpStatsRxPortTable (INT4 i4LldpStatsRxPortNum,
                                                  tMacAddr DestMacAddr)
{
    UINT4               u4LocPort = 0;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }

    if (LldpUtlGetLocalPort (i4LldpStatsRxPortNum, DestMacAddr, &u4LocPort) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    if (LldpTxUtlValidatePortIndex (u4LocPort) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpTxUtlValidatePortIndex returns FAILURE \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexStatsRxPortTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexStatsRxPortTable (INT4 *pi4LldpStatsRxPortNum)
{
    tLldpLocPortInfo   *pLocPortTbl = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }

    pLocPortTbl =
        (tLldpLocPortInfo *) RBTreeGetFirst (gLldpGlobalInfo.
                                             LldpLocPortAgentInfoRBTree);
    if (pLocPortTbl != NULL)
    {
        *pi4LldpStatsRxPortNum = (INT4) pLocPortTbl->u4LocPortNum;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextLldpStatsRxPortTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextLldpStatsRxPortTable (INT4 i4LldpStatsRxPortNum,
                                    INT4 *pi4NextLldpStatsRxPortNum)
{
    tLldpLocPortInfo    CurrLocPortTbl;
    tLldpLocPortInfo   *pNextLocPortTbl = NULL;

    CurrLocPortTbl.u4LocPortNum = (UINT4) i4LldpStatsRxPortNum;

    pNextLocPortTbl =
        (tLldpLocPortInfo *) RBTreeGetNext (gLldpGlobalInfo.
                                            LldpLocPortAgentInfoRBTree,
                                            &CurrLocPortTbl,
                                            LldpAgentInfoUtlRBCmpInfo);

    if (pNextLocPortTbl != NULL)
    {
        *pi4NextLldpStatsRxPortNum = (INT4) pNextLocPortTbl->u4LocPortNum;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpLocPortTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpLocPortTable (INT4 i4LldpLocPortNum)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }

    if (LldpTxUtlValidateInterfaceIndex (i4LldpLocPortNum) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: LldpTxUtlValidatePortIndex"
                  " returns FAILURE \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpLocPortTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpLocPortTable (INT4 *pi4LldpLocPortNum)
{
    tLldpLocPortTable  *pLocPortTbl = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }

    pLocPortTbl =
        (tLldpLocPortTable *) RBTreeGetFirst (gLldpGlobalInfo.
                                              LldpPortInfoRBTree);
    if (pLocPortTbl != NULL)
    {
        *pi4LldpLocPortNum = pLocPortTbl->i4IfIndex;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexLldpLocPortTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexLldpLocPortTable (INT4 i4LldpLocPortNum,
                                     INT4 *pi4NextLldpLocPortNum)
{
    tLldpLocPortTable   CurrLocPortTbl;
    tLldpLocPortTable  *pNextLocPortTbl = NULL;

    CurrLocPortTbl.i4IfIndex = i4LldpLocPortNum;

    pNextLocPortTbl =
        (tLldpLocPortTable *) RBTreeGetNext (gLldpGlobalInfo.LldpPortInfoRBTree,
                                             &CurrLocPortTbl,
                                             LldpPortInfoUtlRBCmpInfo);

    if (pNextLocPortTbl != NULL)
    {
        *pi4NextLldpLocPortNum = pNextLocPortTbl->i4IfIndex;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpRemTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpRemTable (UINT4 u4LldpRemTimeMark,
                                          INT4 i4LldpRemLocalPortNum,
                                          INT4 i4LldpRemIndex,
                                          UINT4 u4DestMacAddr)
{

    if (LLDP_IS_SHUTDOWN ())
    {
        return OSIX_FAILURE;
    }

    if (LldpRxUtlValidateRemTableIndices (u4LldpRemTimeMark,
                                          i4LldpRemLocalPortNum, u4DestMacAddr,
                                          i4LldpRemIndex) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD LldpRxUtlValidateRemTableIndices returns Failure  \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpRemTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpRemTable (UINT4 *pu4LldpRemTimeMark,
                                  INT4 *pi4LldpRemLocalPortNum,
                                  INT4 *pi4LldpRemIndex)
{
    tLldpRemoteNode    *pFirstRemNode = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }

    pFirstRemNode = (tLldpRemoteNode *)
        RBTreeGetFirst (gLldpGlobalInfo.RemSysDataRBTree);

    if (pFirstRemNode != NULL)
    {
        *pu4LldpRemTimeMark = pFirstRemNode->u4RemLastUpdateTime;
        *pi4LldpRemLocalPortNum = pFirstRemNode->i4RemLocalPortNum;
        *pi4LldpRemIndex = pFirstRemNode->i4RemIndex;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexLldpRemTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexLldpRemTable (UINT4 u4LldpRemTimeMark,
                                 UINT4 *pu4NextLldpRemTimeMark,
                                 INT4 i4LldpRemLocalPortNum,
                                 INT4 *pi4NextLldpRemLocalPortNum,
                                 UINT4 u4DestIndex,
                                 UINT4 *pu4NextDestIndex,
                                 INT4 i4LldpRemIndex, INT4 *pi4NextLldpRemIndex)
{
    tLldpRemoteNode    *pNextRemNode = NULL;
    tLldpRemoteNode    *pCurrRemNode = NULL;
    tLldpRemoteNode    *pTmpRemNode = NULL;
    /* Get the Current Node */
    pCurrRemNode = LldpRxUtlGetRemoteNode (u4LldpRemTimeMark,
                                           i4LldpRemLocalPortNum,
                                           u4DestIndex, i4LldpRemIndex);
    if (pCurrRemNode == NULL)
    {
        if ((pTmpRemNode = (tLldpRemoteNode *)
             (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
        {
            LLDP_TRC (ALL_FAILURE_TRC | LLDP_MAN_ADDR_TRC | LLDP_CRITICAL_TRC,
                      "\tMemory Allocation Failure for New TmpRem Node" "\r\n");
            LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
            return OSIX_FAILURE;
        }
        MEMSET (pTmpRemNode, 0, sizeof (tLldpRemoteNode));
        pCurrRemNode = pTmpRemNode;
    }
    /* Get the Next Node */
    if (LldpRxUtlGetNextRemoteNode (pCurrRemNode, &pNextRemNode)
        == OSIX_SUCCESS)
    {
        /* Return the Next Node Indices */
        *pu4NextLldpRemTimeMark = pNextRemNode->u4RemLastUpdateTime;
        *pi4NextLldpRemLocalPortNum = pNextRemNode->i4RemLocalPortNum;
        *pi4NextLldpRemIndex = pNextRemNode->i4RemIndex;
        *pu4NextDestIndex = pNextRemNode->u4DestAddrTblIndex;

        if (pTmpRemNode != NULL)
        {
            MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                                (UINT1 *) pTmpRemNode);
        }
        return OSIX_SUCCESS;
    }
    if (pTmpRemNode != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                            (UINT1 *) pTmpRemNode);
    }

    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpRemManAddrTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpRemManAddrTable (UINT4 u4LldpRemTimeMark,
                                                 INT4 i4LldpRemLocalPortNum,
                                                 INT4 i4LldpRemIndex,
                                                 INT4 i4LldpRemManAddrSubtype,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pLldpRemManAddr,
                                                 UINT4 u4DestMacAddr)
{
    UINT1               au1RemManAddr[LLDP_MAX_LEN_MAN_ADDR];
    /*UINT4               u4LocPort = 0; */

    if (LLDP_IS_SHUTDOWN ())
    {
        return OSIX_FAILURE;
    }

    MEMSET (&au1RemManAddr, 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMCPY (&au1RemManAddr[0], pLldpRemManAddr->pu1_OctetList,
            pLldpRemManAddr->i4_Length);
    /* Check whether the Entry is exist or not, indices are validaed inside
     * the Get function */
    if (LldpRxUtlGetRemManAddrNode (u4LldpRemTimeMark,
                                    i4LldpRemLocalPortNum,
                                    u4DestMacAddr,
                                    i4LldpRemIndex,
                                    i4LldpRemManAddrSubtype,
                                    &au1RemManAddr[0]) != NULL)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpRemManAddrTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpRemManAddrTable (UINT4 *pu4LldpRemTimeMark,
                                         INT4 *pi4LldpRemLocalPortNum,
                                         INT4 *pi4LldpRemIndex,
                                         INT4 *pi4LldpRemManAddrSubtype,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pLldpRemManAddr)
{
    INT4                i4ManAddrLen = 0;
    tLldpRemManAddressTable *pFirstRemoteManAddrNode = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }

    pFirstRemoteManAddrNode = (tLldpRemManAddressTable *)
        RBTreeGetFirst (gLldpGlobalInfo.RemManAddrRBTree);

    if (pFirstRemoteManAddrNode != NULL)
    {
        *pu4LldpRemTimeMark = pFirstRemoteManAddrNode->u4RemLastUpdateTime;
        *pi4LldpRemLocalPortNum = pFirstRemoteManAddrNode->i4RemLocalPortNum;
        *pi4LldpRemIndex = pFirstRemoteManAddrNode->i4RemIndex;
        *pi4LldpRemManAddrSubtype =
            pFirstRemoteManAddrNode->i4RemManAddrSubtype;
        i4ManAddrLen = (INT4)
            LldpUtilGetManAddrLen (pFirstRemoteManAddrNode->
                                   i4RemManAddrSubtype);
        MEMCPY (pLldpRemManAddr->pu1_OctetList,
                pFirstRemoteManAddrNode->au1RemManAddr, i4ManAddrLen);
        pLldpRemManAddr->i4_Length = i4ManAddrLen;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexRemManAddrTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexRemManAddrTable (UINT4 u4LldpRemTimeMark,
                                    UINT4 *pu4NextLldpRemTimeMark,
                                    INT4 i4LldpRemLocalPortNum,
                                    INT4 *pi4NextLldpRemLocalPortNum,
                                    UINT4 u4DestIndex,
                                    UINT4 *pu4NextDestIndex,
                                    INT4 i4LldpRemIndex,
                                    INT4 *pi4NextLldpRemIndex,
                                    INT4 i4LldpRemManAddrSubtype,
                                    INT4 *pi4NextLldpRemManAddrSubtype,
                                    tSNMP_OCTET_STRING_TYPE * pLldpRemManAddr,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextLldpRemManAddr)
{
    tLldpRemManAddressTable *pNextRemManNode = NULL;
    tLldpRemManAddressTable *pCurrRemManNode = NULL;
    UINT1               au1RemManAddr[LLDP_MAX_LEN_MAN_ADDR];
    INT4                i4ManAddrLen = 0;

    if (pLldpRemManAddr->i4_Length > LLDP_MAX_LEN_MAN_ADDR)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: Length exceed the maximum value !!\r\n");
        return OSIX_FAILURE;
    }
    else if (pLldpRemManAddr->i4_Length <= 0)
    {
        pNextRemManNode = (tLldpRemManAddressTable *)
            RBTreeGetFirst (gLldpGlobalInfo.RemManAddrRBTree);
    }
    else
    {
        MEMSET (&au1RemManAddr[0], 0, LLDP_MAX_LEN_MAN_ADDR);
        MEMCPY (&au1RemManAddr[0], pLldpRemManAddr->pu1_OctetList,
                pLldpRemManAddr->i4_Length);
        /* Get the Current Node */
        pCurrRemManNode = LldpRxUtlGetRemManAddrNode (u4LldpRemTimeMark,
                                                      i4LldpRemLocalPortNum,
                                                      u4DestIndex,
                                                      i4LldpRemIndex,
                                                      i4LldpRemManAddrSubtype,
                                                      &au1RemManAddr[0]);
        if (pCurrRemManNode == NULL)
        {
            LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                      "LldpRxUtlGetRemManAddrNode returns NULL\r\n");
            return OSIX_FAILURE;
        }

        /* Get the Next Node */
        if (LldpRxUtlGetNextManAddr (pCurrRemManNode, &pNextRemManNode)
            != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                      "LldpRxUtlGetNextManAddr Failed\r\n");
            return OSIX_FAILURE;
        }
    }

    LLDP_CHK_NULL_PTR_RET (pNextRemManNode, SNMP_FAILURE);
    /* Return the Next Node Indices */
    *pu4NextLldpRemTimeMark = pNextRemManNode->u4RemLastUpdateTime;
    *pi4NextLldpRemLocalPortNum = pNextRemManNode->i4RemLocalPortNum;
    *pi4NextLldpRemIndex = pNextRemManNode->i4RemIndex;
    *pi4NextLldpRemManAddrSubtype = pNextRemManNode->i4RemManAddrSubtype;
    *pu4NextDestIndex = pNextRemManNode->u4DestAddrTblIndex;
    i4ManAddrLen = (INT4)
        LldpUtilGetManAddrLen (pNextRemManNode->i4RemManAddrSubtype);
    MEMCPY (pNextLldpRemManAddr->pu1_OctetList,
            pNextRemManNode->au1RemManAddr, i4ManAddrLen);
    pNextLldpRemManAddr->i4_Length = i4ManAddrLen;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceRemUnknownTLVTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceRemUnknownTLVTable (UINT4 u4LldpRemTimeMark,
                                                INT4 i4LldpRemLocalPortNum,
                                                INT4 i4LldpRemIndex,
                                                INT4 i4LldpRemUnknownTLVType,
                                                UINT4 u4DestMacAddr)
{

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }
    /* Check whether the Entry is exist or not.
     * Validation of indices are done inside Get routine*/
    if (LldpRxUtlGetRemUnknownTlvInfo (u4LldpRemTimeMark,
                                       i4LldpRemLocalPortNum, u4DestMacAddr,
                                       i4LldpRemIndex,
                                       i4LldpRemUnknownTLVType) != NULL)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpRemUnknownTLVTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpRemUnknownTLVTable (UINT4 *pu4LldpRemTimeMark,
                                            INT4 *pi4LldpRemLocalPortNum,
                                            INT4 *pi4LldpRemIndex,
                                            INT4 *pi4LldpRemUnknownTLVType)
{
    tLldpRemUnknownTLVTable *pFirstRemUnknownTlv = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }

    pFirstRemUnknownTlv = (tLldpRemUnknownTLVTable *)
        RBTreeGetFirst (gLldpGlobalInfo.RemUnknownTLVRBTree);

    if (pFirstRemUnknownTlv != NULL)
    {
        *pu4LldpRemTimeMark = pFirstRemUnknownTlv->u4RemLastUpdateTime;
        *pi4LldpRemLocalPortNum = pFirstRemUnknownTlv->i4RemLocalPortNum;
        *pi4LldpRemIndex = pFirstRemUnknownTlv->i4RemIndex;
        *pi4LldpRemUnknownTLVType = pFirstRemUnknownTlv->i4RemUnknownTLVType;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexRemUnknownTLVTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexRemUnknownTLVTable (UINT4 u4LldpRemTimeMark,
                                       UINT4 *pu4NextLldpRemTimeMark,
                                       INT4 i4LldpRemLocalPortNum,
                                       INT4 *pi4NextLldpRemLocalPortNum,
                                       UINT4 u4DestIndex,
                                       UINT4 *pu4NextDestIndex,
                                       INT4 i4LldpRemIndex,
                                       INT4 *pi4NextLldpRemIndex,
                                       INT4 i4LldpRemUnknownTLVType,
                                       INT4 *pi4NextLldpRemUnknownTLVType)
{
    tLldpRemUnknownTLVTable *pNextUnknownTlv = NULL;
    tLldpRemUnknownTLVTable *pCurrUnknowTlv = NULL;

    /* Get the Current Node */
    pCurrUnknowTlv = LldpRxUtlGetRemUnknownTlvInfo (u4LldpRemTimeMark,
                                                    i4LldpRemLocalPortNum,
                                                    u4DestIndex,
                                                    i4LldpRemIndex,
                                                    i4LldpRemUnknownTLVType);
    if (pCurrUnknowTlv != NULL)
    {
        /* Get the Next Node */
        if (LldpRxUtlGetNextUnknownTLV (pCurrUnknowTlv, &pNextUnknownTlv)
            == OSIX_SUCCESS)
        {
            /* Return the Next Node Indices */
            *pu4NextLldpRemTimeMark = pNextUnknownTlv->u4RemLastUpdateTime;
            *pi4NextLldpRemLocalPortNum = pNextUnknownTlv->i4RemLocalPortNum;
            *pi4NextLldpRemIndex = pNextUnknownTlv->i4RemIndex;
            *pu4NextDestIndex = pNextUnknownTlv->u4DestAddrTblIndex;
            *pi4NextLldpRemUnknownTLVType =
                pNextUnknownTlv->i4RemUnknownTLVType;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceRemOrgDefInfoTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceRemOrgDefInfoTable (UINT4 u4LldpRemTimeMark,
                                                INT4 i4LldpRemLocalPortNum,
                                                INT4 i4LldpRemIndex,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pLldpRemOrgDefInfoOUI,
                                                INT4 i4LldpRemOrgDefInfoSubtype,
                                                INT4 i4LldpRemOrgDefInfoIndex,
                                                UINT4 u4DestMacAddr)
{

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }
    /* Check whether the Entry is exist or not .
     * Validation of indices are done inside Get routine*/
    if (LldpRxUtlGetRemOrgDefInfo (u4LldpRemTimeMark,
                                   i4LldpRemLocalPortNum,
                                   u4DestMacAddr,
                                   i4LldpRemIndex,
                                   pLldpRemOrgDefInfoOUI->pu1_OctetList,
                                   i4LldpRemOrgDefInfoSubtype,
                                   i4LldpRemOrgDefInfoIndex) != NULL)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpRemOrgDefInfoTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpRemOrgDefInfoTable (UINT4 *pu4LldpRemTimeMark,
                                            INT4 *pi4LldpRemLocalPortNum,
                                            INT4 *pi4LldpRemIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pLldpRemOrgDefInfoOUI,
                                            INT4 *pi4LldpRemOrgDefInfoSubtype,
                                            INT4 *pi4LldpRemOrgDefInfoIndex)
{
    tLldpRemOrgDefInfoTable *pFirstRemOrgDefInfo = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pLldpRemOrgDefInfoOUI->pu1_OctetList, 0, LLDP_MAX_LEN_OUI);
    pFirstRemOrgDefInfo = (tLldpRemOrgDefInfoTable *)
        RBTreeGetFirst (gLldpGlobalInfo.RemOrgDefInfoRBTree);

    if (pFirstRemOrgDefInfo != NULL)
    {
        *pu4LldpRemTimeMark = pFirstRemOrgDefInfo->u4RemLastUpdateTime;
        *pi4LldpRemLocalPortNum = pFirstRemOrgDefInfo->i4RemLocalPortNum;
        *pi4LldpRemIndex = pFirstRemOrgDefInfo->i4RemIndex;
        MEMCPY (pLldpRemOrgDefInfoOUI->pu1_OctetList,
                pFirstRemOrgDefInfo->au1RemOrgDefInfoOUI, LLDP_MAX_LEN_OUI);
        pLldpRemOrgDefInfoOUI->i4_Length = LLDP_MAX_LEN_OUI;
        *pi4LldpRemOrgDefInfoSubtype =
            pFirstRemOrgDefInfo->i4RemOrgDefInfoSubtype;
        *pi4LldpRemOrgDefInfoIndex = pFirstRemOrgDefInfo->i4RemOrgDefInfoIndex;

        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexRemOrgDefInfoTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexRemOrgDefInfoTable (UINT4 u4LldpRemTimeMark,
                                       UINT4 *pu4NextLldpRemTimeMark,
                                       INT4 i4LldpRemLocalPortNum,
                                       INT4 *pi4NextLldpRemLocalPortNum,
                                       UINT4 u4DestIndex,
                                       UINT4 *pu4NextDestIndex,
                                       INT4 i4LldpRemIndex,
                                       INT4 *pi4NextLldpRemIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pLldpRemOrgDefInfoOUI,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextLldpRemOrgDefInfoOUI,
                                       INT4 i4LldpRemOrgDefInfoSubtype,
                                       INT4 *pi4NextLldpRemOrgDefInfoSubtype,
                                       INT4 i4LldpRemOrgDefInfoIndex,
                                       INT4 *pi4NextLldpRemOrgDefInfoIndex)
{
    tLldpRemOrgDefInfoTable *pNextOrgDefInfo = NULL;
    tLldpRemOrgDefInfoTable *pCurrOrgDefInfo = NULL;

    if (pLldpRemOrgDefInfoOUI->i4_Length > LLDP_MAX_LEN_OUI)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: Length exceed the maximum value !!\r\n");
        return OSIX_FAILURE;
    }
    else if (pLldpRemOrgDefInfoOUI->i4_Length <= 0)
    {
        pNextOrgDefInfo = (tLldpRemOrgDefInfoTable *)
            RBTreeGetFirst (gLldpGlobalInfo.RemOrgDefInfoRBTree);
    }
    else
    {
        /* Get the Current Node */
        pCurrOrgDefInfo =
            LldpRxUtlGetRemOrgDefInfo (u4LldpRemTimeMark,
                                       i4LldpRemLocalPortNum,
                                       u4DestIndex,
                                       i4LldpRemIndex,
                                       pLldpRemOrgDefInfoOUI->pu1_OctetList,
                                       i4LldpRemOrgDefInfoSubtype,
                                       i4LldpRemOrgDefInfoIndex);
        if (pCurrOrgDefInfo == NULL)

        {
            LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                      "LldpRxUtlGetRemOrgDefInfo returns NULL\r\n");
            return OSIX_FAILURE;
        }

        /* Get the Next Node */
        if (LldpRxUtlGetNextOrgDefInfo (pCurrOrgDefInfo, &pNextOrgDefInfo)
            != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                      "LldpRxUtlGetNextOrgDefInfo Failed\r\n");
            return OSIX_FAILURE;
        }
    }

    LLDP_CHK_NULL_PTR_RET (pNextOrgDefInfo, SNMP_FAILURE);

    /* Return the Next Node Indices */
    *pu4NextLldpRemTimeMark = pNextOrgDefInfo->u4RemLastUpdateTime;
    *pi4NextLldpRemLocalPortNum = pNextOrgDefInfo->i4RemLocalPortNum;
    *pi4NextLldpRemIndex = pNextOrgDefInfo->i4RemIndex;
    MEMCPY (pNextLldpRemOrgDefInfoOUI->pu1_OctetList,
            pNextOrgDefInfo->au1RemOrgDefInfoOUI, LLDP_MAX_LEN_OUI);
    pNextLldpRemOrgDefInfoOUI->i4_Length = LLDP_MAX_LEN_OUI;
    *pi4NextLldpRemOrgDefInfoSubtype = pNextOrgDefInfo->i4RemOrgDefInfoSubtype;
    *pi4NextLldpRemOrgDefInfoIndex = pNextOrgDefInfo->i4RemOrgDefInfoIndex;
    *pu4NextDestIndex = pNextOrgDefInfo->u4DestAddrTblIndex;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpXdot1ConfigPortVlanTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpXdot1ConfigPortVlanTable (INT4
                                                          i4LldpPortConfigPortNum,
                                                          tMacAddr DestMacAddr)
{
    tLldpLocPortInfo   *pLocPortInfo = NULL;
    UINT4               u4LocPort = 0;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    if (LldpUtlGetLocalPort (i4LldpPortConfigPortNum, DestMacAddr, &u4LocPort)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    pLocPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPort);
    if (pLocPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: ConfigPortVlanTable Index validation failed !!\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpXdot1ConfigPortVlanTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpXdot1ConfigPortVlanTable (INT4
                                                  *pi4LldpPortConfigPortNum)
{
    tLldpLocPortInfo   *pLocPortTbl = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }

    pLocPortTbl =
        (tLldpLocPortInfo *) RBTreeGetFirst (gLldpGlobalInfo.
                                             LldpLocPortAgentInfoRBTree);
    if (pLocPortTbl != NULL)
    {
        *pi4LldpPortConfigPortNum = pLocPortTbl->i4IfIndex;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexLldpXdot1ConfigPortVlanTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexLldpXdot1ConfigPortVlanTable (INT4 i4LldpPortConfigPortNum,
                                                 INT4
                                                 *pi4NextLldpPortConfigPortNum)
{
    tLldpLocPortInfo    CurrLocPortTbl;
    tLldpLocPortInfo   *pNextLocPortTbl = NULL;

    CurrLocPortTbl.u4LocPortNum = (UINT4) i4LldpPortConfigPortNum;
    CurrLocPortTbl.u4DstMacAddrTblIndex = 0;

    pNextLocPortTbl =
        (tLldpLocPortInfo *) RBTreeGetNext (gLldpGlobalInfo.
                                            LldpLocPortAgentInfoRBTree,
                                            &CurrLocPortTbl,
                                            LldpAgentInfoUtlRBCmpInfo);

    if (pNextLocPortTbl != NULL)
    {
        *pi4NextLldpPortConfigPortNum = (INT4) pNextLocPortTbl->u4LocPortNum;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot1ConfigPortVlanTxEnable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot1ConfigPortVlanTxEnable (INT4 i4LldpPortConfigPortNum,
                                           tMacAddr DestMacAddr,
                                           INT4
                                           *pi4RetValLldpXdot1ConfigPortVlanTxEnable)
{
    tLldpLocPortInfo    LldpLocPortInfo;
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    UINT4               u4LocPort = 0;

    if (LldpUtlGetLocalPort (i4LldpPortConfigPortNum, DestMacAddr, &u4LocPort)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD pLldpLocPortInfo is NULL LldpLocPortInfo is Invalid \r\n");
        return OSIX_FAILURE;
    }

    *pi4RetValLldpXdot1ConfigPortVlanTxEnable =
        (INT4) pLldpLocPortInfo->u1PortVlanTxEnable;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlSetLldpXdot1ConfigPortVlanTxEnable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlSetLldpXdot1ConfigPortVlanTxEnable (INT4 i4LldpPortConfigPortNum,
                                           tMacAddr DestMacAddr,
                                           INT4
                                           i4SetValLldpXdot1ConfigPortVlanTxEnable)
{
    tLldpLocPortInfo    LldpLocPortInfo;
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    UINT4               u4LocPort = 0;

    if (LldpUtlGetLocalPort (i4LldpPortConfigPortNum, DestMacAddr, &u4LocPort)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD pLldpLocPortInfo is NULL LldpLocPortInfo is Invalid \r\n");
        return OSIX_FAILURE;
    }

    if (pLldpLocPortInfo->u1PortVlanTxEnable !=
        (BOOL1) i4SetValLldpXdot1ConfigPortVlanTxEnable)
    {
        pLldpLocPortInfo->u1PortVlanTxEnable =
            (UINT1) i4SetValLldpXdot1ConfigPortVlanTxEnable;
        LLDP_TRC_ARG2 (MGMT_TRC, " SNMPSTD: Configured PortVlanTxEnable"
                       " value %d for port %d \n",
                       i4SetValLldpXdot1ConfigPortVlanTxEnable,
                       i4LldpPortConfigPortNum);
        if (LldpTxUtlHandleLocPortInfoChg (pLldpLocPortInfo) != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC, "SNMPSTD: "
                      "LldpTxUtlHandleLocPortInfoChg failed\r\n");
        }
    }
    else
    {
        LLDP_TRC_ARG2 (MGMT_TRC,
                       " SNMPSTD: PortVlanTxEnable Value %d has been"
                       " configured already for port %d\n",
                       i4SetValLldpXdot1ConfigPortVlanTxEnable,
                       i4LldpPortConfigPortNum);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlTestLldpXdot1ConfigPortVlanTxEnable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlTestLldpXdot1ConfigPortVlanTxEnable (UINT4 *pu4ErrorCode,
                                            INT4 i4LldpPortConfigPortNum,
                                            INT4
                                            i4TestValLldpXdot1ConfigPortVlanTxEnable,
                                            tMacAddr DestMacAddr)
{
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    UINT4               u4LocPort = 0;

    if (LldpUtlGetLocalPort (i4LldpPortConfigPortNum, DestMacAddr, &u4LocPort)
        == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    pLldpLocPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPort);
    if (pLldpLocPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: Local Agent not created\r\n");
        return OSIX_FAILURE;
    }

    if ((i4TestValLldpXdot1ConfigPortVlanTxEnable != LLDP_TRUE) &&
        (i4TestValLldpXdot1ConfigPortVlanTxEnable != LLDP_FALSE))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Use '1' or '2'for setting 'PortVlanTxEnable' \n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpXdot1LocVlanNameTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpXdot1LocVlanNameTable (INT4 i4LldpLocPortNum,
                                                       INT4
                                                       i4LldpXdot1LocVlanId)
{

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    /* Vlan name information is maintained as an RBTree "PortVlan Table" in
     * L2IWF */
    if (LldpVlndbValidateVlanNameTable ((UINT4) i4LldpLocPortNum,
                                        (UINT2) i4LldpXdot1LocVlanId)
        != OSIX_SUCCESS)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Loc Vlan Name Table Validation failed!! \n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpXdot1LocVlanNameTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpXdot1LocVlanNameTable (INT4 *pi4LldpLocPortNum,
                                               INT4 *pi4LldpXdot1LocVlanId)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    if (LldpVlndbGetFirstVlanNameTblInd (pi4LldpLocPortNum,
                                         pi4LldpXdot1LocVlanId) == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexLldpXdot1LocVlanNameTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexLldpXdot1LocVlanNameTable (INT4 i4LldpLocPortNum,
                                              INT4 *pi4NextLldpLocPortNum,
                                              INT4 i4LldpXdot1LocVlanId,
                                              INT4 *pi4NextLldpXdot1LocVlanId)
{

    if (LldpVlndbGetNextVlanNameTblInd (i4LldpLocPortNum,
                                        pi4NextLldpLocPortNum,
                                        i4LldpXdot1LocVlanId,
                                        pi4NextLldpXdot1LocVlanId)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpVlndbGetNextVlanNameTblInd returns failure!! \n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot1LocVlanName
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot1LocVlanName (INT4 i4LldpLocPortNum,
                                INT4 i4LldpXdot1LocVlanId,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValLldpXdot1LocVlanName)
{
    UINT1               au1VlanName[VLAN_STATIC_MAX_NAME_LEN];

    MEMSET (au1VlanName, 0, VLAN_STATIC_MAX_NAME_LEN);
    MEMSET (pRetValLldpXdot1LocVlanName->pu1_OctetList, 0,
            VLAN_STATIC_MAX_NAME_LEN);
    if (LldpVlndbGetVlanName ((UINT4) i4LldpLocPortNum,
                              (UINT2) i4LldpXdot1LocVlanId, &au1VlanName[0])
        == OSIX_SUCCESS)
    {
        pRetValLldpXdot1LocVlanName->i4_Length =
            (INT4) LLDP_STRLEN (au1VlanName, VLAN_STATIC_MAX_NAME_LEN);
        MEMCPY (pRetValLldpXdot1LocVlanName->pu1_OctetList, au1VlanName,
                pRetValLldpXdot1LocVlanName->i4_Length);

        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpXdot1ConfigVlanNameTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpXdot1ConfigVlanNameTable (INT4 i4LldpLocPortNum,
                                                          INT4
                                                          i4LldpXdot1LocVlanId)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    if (LldpVlndbValidateVlanNameTable ((UINT4) i4LldpLocPortNum,
                                        (UINT2) i4LldpXdot1LocVlanId)
        != OSIX_SUCCESS)
    {
        LLDP_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                       " SNMPSTD: ConfigVlanNameTable Index validation failed"
                       " for port %d and vlan %d!!\n", i4LldpLocPortNum,
                       i4LldpXdot1LocVlanId);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpXdot1ConfigVlanNameTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpXdot1ConfigVlanNameTable (INT4 *pi4LldpLocPortNum,
                                                  INT4 *pi4LldpXdot1LocVlanId)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    if (LldpVlndbGetFirstVlanNameTblInd (pi4LldpLocPortNum,
                                         pi4LldpXdot1LocVlanId) == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexLldpXdot1ConfigVlanNameTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexLldpXdot1ConfigVlanNameTable (INT4 i4LldpLocPortNum,
                                                 INT4 *pi4NextLldpLocPortNum,
                                                 INT4 i4LldpXdot1LocVlanId,
                                                 INT4
                                                 *pi4NextLldpXdot1LocVlanId)
{

    if (LldpVlndbGetNextVlanNameTblInd (i4LldpLocPortNum,
                                        pi4NextLldpLocPortNum,
                                        i4LldpXdot1LocVlanId,
                                        pi4NextLldpXdot1LocVlanId)
        == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot1ConfigVlanNameTxEnable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot1ConfigVlanNameTxEnable (INT4 i4LldpLocPortNum,
                                           INT4 i4LldpXdot1LocVlanId,
                                           INT4
                                           *pi4RetValLldpXdot1ConfigVlanNameTxEnable)
{
    UINT1               u1ConfigVlanNameTxEnable = LLDP_FALSE;

    if (LldpVlndbGetVlanNameTxStatus ((UINT4) i4LldpLocPortNum,
                                      (UINT2) i4LldpXdot1LocVlanId,
                                      &u1ConfigVlanNameTxEnable) ==
        OSIX_SUCCESS)
    {
        *pi4RetValLldpXdot1ConfigVlanNameTxEnable =
            (INT4) u1ConfigVlanNameTxEnable;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlSetLldpXdot1ConfigVlanNameTxEnable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlSetLldpXdot1ConfigVlanNameTxEnable (INT4 i4LldpLocPortNum,
                                           INT4 i4LldpXdot1LocVlanId,
                                           INT4
                                           i4SetValLldpXdot1ConfigVlanNameTxEnable)
{
    tLldpLocPortInfo   *pLldpLocPortInfo;
    UINT1               u1ConfTxEnable = OSIX_FALSE;

    if (LldpVlndbSetVlanNameTxStatus ((UINT4) i4LldpLocPortNum,
                                      (UINT2) i4LldpXdot1LocVlanId,
                                      (UINT1)
                                      i4SetValLldpXdot1ConfigVlanNameTxEnable,
                                      &u1ConfTxEnable) == OSIX_SUCCESS)
    {
        /* The global varible gu1TxAll is set to true if the transmission is to
         * be enabled for all the vlans inorder to avoid the formation of
         * pre-formed buffer multiple times. In that case the preformed buffer
         * will be constructed in CliSetDot1Tlv. The same should be taken care
         * while implementing the WEB. Form the pre-formed buffer only if this
         * function is called for a particular vlan and the value to be set has
         * not been configured already
         */

        if ((gu1TxAll == OSIX_FALSE) && (u1ConfTxEnable == OSIX_TRUE))
        {
            if (LldpTxUtlGetLocPortEntry ((UINT4) i4LldpLocPortNum,
                                          &pLldpLocPortInfo) == OSIX_SUCCESS)
            {
                if (LldpTxUtlHandleLocPortInfoChg (pLldpLocPortInfo)
                    != OSIX_SUCCESS)
                {
                    LLDP_TRC (ALL_FAILURE_TRC, "SNMPSTD: "
                              "LldpTxUtlHandleLocPortInfoChg failed\r\n");
                }
            }
        }
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlDisableAllLldpXdot1ConfigVlanNameTxEnable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlDisableAllLldpXdot1ConfigVlanNameTxEnable (INT4 i4IfIndex,
                                                  INT4
                                                  i4SetValLldpXdot1ConfigVlanNameTxEnable)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4LocPortNum = 0;
    INT4                i4PrevLocPortNum = 0;
    INT4                i4PrevLocVlanId = 0;
    INT4                i4LocVlanId = 0;
    UINT1               au1VlanName[VLAN_STATIC_MAX_NAME_LEN];

    if (L2IwfIsPortInPortChannel ((UINT4) i4IfIndex) == L2IWF_SUCCESS)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "\r\n%%Unable to set VlanNameTLVTxEnable:"
                  "Port is part of port channel\r\n");
        return OSIX_SUCCESS;
    }

    if (nmhGetFirstIndexLldpXdot1ConfigVlanNameTable (&i4LocPortNum,
                                                      &i4LocVlanId)
        != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    do
    {
        if (i4IfIndex == i4LocPortNum)
        {
            MEMSET (&au1VlanName[0], 0, VLAN_STATIC_MAX_NAME_LEN);
            if (LldpVlndbGetVlanName ((UINT4) i4LocPortNum,
                                      (UINT2) i4LocVlanId, &au1VlanName[0])
                == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            if (nmhTestv2LldpXdot1ConfigVlanNameTxEnable (&u4ErrorCode,
                                                          i4IfIndex,
                                                          i4LocVlanId,
                                                          i4SetValLldpXdot1ConfigVlanNameTxEnable)
                != SNMP_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            if (nmhSetLldpXdot1ConfigVlanNameTxEnable
                (i4IfIndex, i4LocVlanId,
                 i4SetValLldpXdot1ConfigVlanNameTxEnable) != SNMP_SUCCESS)
            {
                return OSIX_FAILURE;
            }

        }
        i4PrevLocPortNum = i4LocPortNum;
        i4PrevLocVlanId = i4LocVlanId;
    }
    while (nmhGetNextIndexLldpXdot1ConfigVlanNameTable (i4PrevLocPortNum,
                                                        &i4LocPortNum,
                                                        i4PrevLocVlanId,
                                                        &i4LocVlanId)
           == SNMP_SUCCESS);

    return OSIX_SUCCESS;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlTestLldpXdot1ConfigVlanNameTxEnable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlTestLldpXdot1ConfigVlanNameTxEnable (UINT4 *pu4ErrorCode,
                                            INT4 i4LldpLocPortNum,
                                            INT4 i4LldpXdot1LocVlanId,
                                            INT4
                                            i4TestValLldpXdot1ConfigVlanNameTxEnable)
{

    if ((i4TestValLldpXdot1ConfigVlanNameTxEnable != LLDP_TRUE) &&
        (i4TestValLldpXdot1ConfigVlanNameTxEnable != LLDP_FALSE))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Use '1' or '2'for setting 'VlanNameTxEnable'"
                  " \n");
        return OSIX_FAILURE;
    }

    /* Transmission status can be set only if vlan name is configured for the
     * given vlan id */
    if (LldpVlndbValidateVlanNameEntry ((UINT4) i4LldpLocPortNum,
                                        (UINT2) i4LldpXdot1LocVlanId)
        == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Either the port vlan entry may not be existing or"
                  "vlan name might not have been configured \n");
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpXdot1LocProtoVlanTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpXdot1LocProtoVlanTable (INT4 i4LldpLocPortNum,
                                                        INT4
                                                        i4LldpXdot1LocProtoVlanId)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    if (LldpTxUtlGetProtoVlanEntry ((UINT4) i4LldpLocPortNum,
                                    (UINT2) i4LldpXdot1LocProtoVlanId) == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LocProtoVlanTable Index validation failed !!\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpXdot1LocProtoVlanTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpXdot1LocProtoVlanTable (INT4 *pi4LldpLocPortNum,
                                                INT4
                                                *pi4LldpXdot1LocProtoVlanId)
{
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    tLldpxdot1LocProtoVlanInfo *pProtoVlanEntry = NULL;
    tTMO_DLL           *pDll = NULL;
    UINT4               u4Index = 0;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    /* Get the First Used Index */
    for (u4Index = LLDP_MIN_PORTS; u4Index <= LLDP_MAX_PORTS; u4Index++)
    {
        if (LldpTxUtlGetLocPortEntry (u4Index, &pLldpLocPortInfo)
            == OSIX_SUCCESS)
        {
            pDll = &(pLldpLocPortInfo->LocProtoVlanDllList);
            pProtoVlanEntry =
                (tLldpxdot1LocProtoVlanInfo *) TMO_DLL_First (pDll);
            if (pProtoVlanEntry != NULL)
            {
                *pi4LldpLocPortNum = (INT4) u4Index;
                *pi4LldpXdot1LocProtoVlanId =
                    (INT4) pProtoVlanEntry->u2ProtoVlanId;
                return OSIX_SUCCESS;
            }
        }
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexLldpXdot1LocProtoVlanTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexLldpXdot1LocProtoVlanTable (INT4 i4LldpLocPortNum,
                                               INT4 *pi4NextLldpLocPortNum,
                                               INT4 i4LldpXdot1LocProtoVlanId,
                                               INT4
                                               *pi4NextLldpXdot1LocProtoVlanId)
{
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    tLldpxdot1LocProtoVlanInfo *pProtoVlanEntry = NULL;
    tLldpxdot1LocProtoVlanInfo *pNextProtoVlanEntry = NULL;
    tTMO_DLL           *pDll = NULL;
    tTMO_DLL_NODE      *pNextNode = NULL;
    UINT4               u4IfIndex = 0;
    INT1                i1RetVal = OSIX_FAILURE;

    u4IfIndex = (UINT4) i4LldpLocPortNum;

    if ((i4LldpLocPortNum >= LLDP_MIN_PORTS) &&
        ((UINT4) i4LldpLocPortNum <= LLDP_MAX_PORTS))
    {
        if (LldpTxUtlGetLocPortEntry (u4IfIndex, &pLldpLocPortInfo) !=
            OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        if (pLldpLocPortInfo == NULL)
        {
            return OSIX_FAILURE;
        }

        pDll = &(pLldpLocPortInfo->LocProtoVlanDllList);
        pProtoVlanEntry = LldpTxUtlGetProtoVlanEntry
            (u4IfIndex, (UINT2) i4LldpXdot1LocProtoVlanId);

        if ((pProtoVlanEntry != NULL) &&
            ((pNextNode =
              TMO_DLL_Next (pDll,
                            &pProtoVlanEntry->NextLocProtoVlanNode)) != NULL))
        {
            pNextProtoVlanEntry = (tLldpxdot1LocProtoVlanInfo *) pNextNode;
            *pi4NextLldpLocPortNum = i4LldpLocPortNum;
            *pi4NextLldpXdot1LocProtoVlanId =
                (INT4) pNextProtoVlanEntry->u2ProtoVlanId;
            i1RetVal = OSIX_SUCCESS;
        }
        else
        {
            /* Next protocol vlan entry does not exist for the given port number
             * and vlan id. Hence get the next port number and check if protocol
             *  vlan entry exists, if so return the vlan id*/

            for (u4IfIndex = u4IfIndex + 1; u4IfIndex <= LLDP_MAX_PORTS;
                 u4IfIndex++)
            {
                if (LldpTxUtlGetLocPortEntry (u4IfIndex, &pLldpLocPortInfo) !=
                    OSIX_SUCCESS)
                {
                    return OSIX_FAILURE;
                }
                if (pLldpLocPortInfo == NULL)
                {
                    continue;
                }

                pDll = &(pLldpLocPortInfo->LocProtoVlanDllList);
                if ((pNextProtoVlanEntry =
                     (tLldpxdot1LocProtoVlanInfo *) TMO_DLL_First (pDll))
                    != NULL)
                {
                    *pi4NextLldpLocPortNum = (INT4) u4IfIndex;
                    *pi4NextLldpXdot1LocProtoVlanId =
                        (INT4) pNextProtoVlanEntry->u2ProtoVlanId;
                    i1RetVal = OSIX_SUCCESS;
                    break;
                }
            }
        }
    }
    return i1RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot1LocProtoVlanSupported
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot1LocProtoVlanSupported (INT4 i4LldpLocPortNum,
                                          INT4 i4LldpXdot1LocProtoVlanId,
                                          INT4
                                          *pi4RetValLldpXdot1LocProtoVlanSupported)
{
    UNUSED_PARAM (i4LldpLocPortNum);
    UNUSED_PARAM (i4LldpXdot1LocProtoVlanId);

    /* Since protocol vlan support is for the system, data structure is not
     * maintained on per port per protocol vlan basis */

    *pi4RetValLldpXdot1LocProtoVlanSupported =
        gLldpGlobalInfo.u1ProtoVlanSupported;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot1LocProtoVlanEnabled
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot1LocProtoVlanEnabled (INT4 i4LldpLocPortNum,
                                        INT4 i4LldpXdot1LocProtoVlanId,
                                        INT4
                                        *pi4RetValLldpXdot1LocProtoVlanEnabled)
{
    tLldpLocPortInfo   *pLldpLocPortInfo;
    UNUSED_PARAM (i4LldpXdot1LocProtoVlanId);

    /* Since protocol vlan is enabled or disabled on per port basis, the data
     * structure is maintained on a per port basis */
    if (LldpTxUtlGetLocPortEntry ((UINT4) i4LldpLocPortNum,
                                  &pLldpLocPortInfo) == OSIX_SUCCESS)
    {
        *pi4RetValLldpXdot1LocProtoVlanEnabled =
            (INT4) pLldpLocPortInfo->u1ProtoVlanEnabled;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpXdot1ConfigProtoVlanTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpXdot1ConfigProtoVlanTable (INT4
                                                           i4LldpLocPortNum,
                                                           INT4
                                                           i4LldpXdot1LocProtoVlanId)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    if (LldpTxUtlGetProtoVlanEntry ((UINT4) i4LldpLocPortNum,
                                    (UINT2) i4LldpXdot1LocProtoVlanId) == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: ConfigProtoVlanTable Index validation failed !!\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpXdot1ConfigProtoVlanTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpXdot1ConfigProtoVlanTable (INT4 *pi4LldpLocPortNum,
                                                   INT4
                                                   *pi4LldpXdot1LocProtoVlanId)
{
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    tLldpxdot1LocProtoVlanInfo *pProtoVlanEntry = NULL;
    tTMO_DLL           *pDll = NULL;
    UINT4               u4Index = 0;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    /* Get the First Used Index */
    for (u4Index = LLDP_MIN_PORTS; u4Index <= LLDP_MAX_PORTS; u4Index++)
    {
        if (LldpTxUtlGetLocPortEntry (u4Index, &pLldpLocPortInfo)
            == OSIX_SUCCESS)
        {
            pDll = &(pLldpLocPortInfo->LocProtoVlanDllList);
            if ((pProtoVlanEntry =
                 (tLldpxdot1LocProtoVlanInfo *) TMO_DLL_First (pDll)) != NULL)
            {
                *pi4LldpLocPortNum = (INT4) u4Index;
                *pi4LldpXdot1LocProtoVlanId =
                    (INT4) pProtoVlanEntry->u2ProtoVlanId;
                return OSIX_SUCCESS;
            }
        }
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexLldpXdot1ConfigProtoVlanTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexLldpXdot1ConfigProtoVlanTable (INT4 i4LldpLocPortNum,
                                                  INT4 *pi4NextLldpLocPortNum,
                                                  INT4
                                                  i4LldpXdot1LocProtoVlanId,
                                                  INT4
                                                  *pi4NextLldpXdot1LocProtoVlanId)
{
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    tLldpxdot1LocProtoVlanInfo *pProtoVlanEntry = NULL;
    tLldpxdot1LocProtoVlanInfo *pNextProtoVlanEntry = NULL;
    tTMO_DLL           *pDll = NULL;
    tTMO_DLL_NODE      *pNextNode = NULL;
    UINT4               u4IfIndex = 0;
    INT1                i1RetVal = OSIX_FAILURE;

    u4IfIndex = (UINT4) i4LldpLocPortNum;

    if ((i4LldpLocPortNum >= LLDP_MIN_PORTS) &&
        ((UINT4) i4LldpLocPortNum <= LLDP_MAX_PORTS))
    {
        if (LldpTxUtlGetLocPortEntry (u4IfIndex, &pLldpLocPortInfo) !=
            OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        pDll = &(pLldpLocPortInfo->LocProtoVlanDllList);
        pProtoVlanEntry = LldpTxUtlGetProtoVlanEntry
            (u4IfIndex, (UINT2) i4LldpXdot1LocProtoVlanId);

        if ((pProtoVlanEntry != NULL) &&
            ((pNextNode = TMO_DLL_Next (pDll,
                                        &pProtoVlanEntry->NextLocProtoVlanNode))
             != NULL))
        {
            pNextProtoVlanEntry = (tLldpxdot1LocProtoVlanInfo *) pNextNode;
            *pi4NextLldpLocPortNum = i4LldpLocPortNum;
            *pi4NextLldpXdot1LocProtoVlanId =
                (INT4) pNextProtoVlanEntry->u2ProtoVlanId;
            i1RetVal = OSIX_SUCCESS;
        }
        else
        {
            /* Next protocol vlan entry does not exist for the given port number
             * and vlan id. Hence get the next port number and check
             * if protocol vlan entry exists, if so return the vlan id*/

            for (u4IfIndex = u4IfIndex + 1; u4IfIndex <= LLDP_MAX_PORTS;
                 u4IfIndex++)
            {
                if (LldpTxUtlGetLocPortEntry (u4IfIndex, &pLldpLocPortInfo) !=
                    OSIX_SUCCESS)
                {
                    continue;
                }

                pDll = &(pLldpLocPortInfo->LocProtoVlanDllList);
                if ((pNextProtoVlanEntry =
                     (tLldpxdot1LocProtoVlanInfo *) TMO_DLL_First (pDll))
                    != NULL)
                {
                    *pi4NextLldpLocPortNum = (INT4) u4IfIndex;
                    *pi4NextLldpXdot1LocProtoVlanId =
                        (INT4) pNextProtoVlanEntry->u2ProtoVlanId;
                    i1RetVal = OSIX_SUCCESS;
                    break;
                }
            }
        }
    }
    return i1RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot1ConfigProtoVlanTxEnable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot1ConfigProtoVlanTxEnable (INT4 i4LldpLocPortNum,
                                            INT4 i4LldpXdot1LocProtoVlanId,
                                            INT4
                                            *pi4RetValLldpXdot1ConfigProtoVlanTxEnable)
{
    tLldpxdot1LocProtoVlanInfo *pProtoVlanEntry = NULL;

    pProtoVlanEntry = LldpTxUtlGetProtoVlanEntry ((UINT4) i4LldpLocPortNum,
                                                  (UINT2)
                                                  i4LldpXdot1LocProtoVlanId);
    if (pProtoVlanEntry != NULL)
    {
        *pi4RetValLldpXdot1ConfigProtoVlanTxEnable =
            (INT4) pProtoVlanEntry->u1ProtoVlanTxEnabled;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlSetLldpXdot1ConfigProtoVlanTxEnable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlSetLldpXdot1ConfigProtoVlanTxEnable (INT4 i4LldpLocPortNum,
                                            INT4 i4LldpXdot1LocProtoVlanId,
                                            INT4
                                            i4SetValLldpXdot1ConfigProtoVlanTxEnable)
{
    tLldpLocPortInfo   *pLldpLocPortInfo;
    tLldpxdot1LocProtoVlanInfo *pProtoVlanEntry = NULL;
    INT1                i1RetVal = OSIX_FAILURE;

    pProtoVlanEntry =
        LldpTxUtlGetProtoVlanEntry ((UINT4) i4LldpLocPortNum,
                                    (UINT2) i4LldpXdot1LocProtoVlanId);
    if (pProtoVlanEntry != NULL)
    {
        if (pProtoVlanEntry->u1ProtoVlanTxEnabled !=
            (UINT1) i4SetValLldpXdot1ConfigProtoVlanTxEnable)
        {
            pProtoVlanEntry->u1ProtoVlanTxEnabled =
                (UINT1) i4SetValLldpXdot1ConfigProtoVlanTxEnable;
            LLDP_TRC_ARG3 (MGMT_TRC, " SNMPSTD: Configured ProtoVlanTxEnable "
                           "value %d for port %d and vlan id %d\n",
                           i4SetValLldpXdot1ConfigProtoVlanTxEnable,
                           i4LldpLocPortNum, i4LldpXdot1LocProtoVlanId);

            /* This global varible is set to true if the transmission is to be
             * enabled for all the vlans inorder to avoid the formation of
             * pre-formed buffer multiple times. In this case the preformed
             * buffer will be constructed in CliSetDot1Tlv. The same should be
             * taken care while implementing the WEB.*/
            if (gu1TxAll == OSIX_FALSE)
            {
                if (LldpTxUtlGetLocPortEntry ((UINT4) i4LldpLocPortNum,
                                              &pLldpLocPortInfo) ==
                    OSIX_SUCCESS)
                {
                    if (LldpTxUtlHandleLocPortInfoChg (pLldpLocPortInfo)
                        != OSIX_SUCCESS)
                    {
                        LLDP_TRC (ALL_FAILURE_TRC, "SNMPSTD: "
                                  "LldpTxUtlHandleLocPortInfoChg failed\r\n");
                    }
                }
            }

        }
        else
        {
            LLDP_TRC_ARG3 (MGMT_TRC, " SNMPSTD: ProtoVlanTxEnable Value %d has "
                           "been configured already for port %d and "
                           "vlan id %d \n",
                           i4SetValLldpXdot1ConfigProtoVlanTxEnable,
                           i4LldpLocPortNum, i4LldpXdot1LocProtoVlanId);
        }
        i1RetVal = OSIX_SUCCESS;
    }

    return i1RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlTestv2LldpXdot1ConfigProtoVlanTxEnable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlTestv2LldpXdot1ConfigProtoVlanTxEnable (UINT4 *pu4ErrorCode,
                                               INT4 i4LldpLocPortNum,
                                               INT4 i4LldpXdot1LocProtoVlanId,
                                               INT4
                                               i4TestValLldpXdot1ConfigProtoVlanTxEnable)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return OSIX_FAILURE;
    }

    if (LldpTxUtlGetProtoVlanEntry ((UINT4) i4LldpLocPortNum,
                                    (UINT2) i4LldpXdot1LocProtoVlanId) != NULL)
    {
        if ((i4TestValLldpXdot1ConfigProtoVlanTxEnable != LLDP_TRUE) &&
            (i4TestValLldpXdot1ConfigProtoVlanTxEnable != LLDP_FALSE))
        {
            *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
            LLDP_TRC (ALL_FAILURE_TRC,
                      " SNMPSTD: Use '1' or '2'for setting 'ProtoVlanTxEnable'"
                      " \n");
            return OSIX_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpXdot1LocTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpXdot1LocTable (INT4 i4LldpLocPortNum)
{

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    if (LldpTxUtlValidateInterfaceIndex (i4LldpLocPortNum) != OSIX_SUCCESS)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpXdot1LocTable Index validation failed !!\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpXdot1LocTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpXdot1LocTable (INT4 *pi4LldpLocPortNum)
{
    tLldpLocPortTable  *pLocPortTable = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    /* Get the First Used Index */
    pLocPortTable = (tLldpLocPortTable *)
        RBTreeGetFirst (gLldpGlobalInfo.LldpPortInfoRBTree);
    if (pLocPortTable == NULL)
    {
        return OSIX_FAILURE;
    }
    *pi4LldpLocPortNum = pLocPortTable->i4IfIndex;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexLldpXdot1LocTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexLldpXdot1LocTable (INT4 i4LldpLocPortNum,
                                      INT4 *pi4NextLldpLocPortNum)
{
    INT1                i1RetVal = OSIX_FAILURE;
    tLldpLocPortTable  *pLocPortTable = NULL;
    tLldpLocPortTable   LocPortTable;

    MEMSET (&LocPortTable, 0, sizeof (tLldpLocPortTable));

    LocPortTable.i4IfIndex = i4LldpLocPortNum;
    pLocPortTable = (tLldpLocPortTable *)
        RBTreeGetNext (gLldpGlobalInfo.LldpPortInfoRBTree, &LocPortTable,
                       LldpPortInfoUtlRBCmpInfo);
    if (pLocPortTable != NULL)
    {
        *pi4NextLldpLocPortNum = pLocPortTable->i4IfIndex;
        return OSIX_SUCCESS;
    }
    return i1RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot1LocPortVlanId
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot1LocPortVlanId (INT4 i4LldpLocPortNum,
                                  INT4 *pi4RetValLldpXdot1LocPortVlanId)
{
    tLldpLocPortInfo   *pLldpLocPortInfo;

    if (LldpTxUtlGetLocPortEntry ((UINT4) i4LldpLocPortNum,
                                  &pLldpLocPortInfo) == OSIX_SUCCESS)
    {
        *pi4RetValLldpXdot1LocPortVlanId = (INT4) pLldpLocPortInfo->u2PVid;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpXdot1RemTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpXdot1RemTable (UINT4 u4LldpRemTimeMark,
                                               INT4 i4LldpRemLocalPortNum,
                                               INT4 i4LldpRemIndex,
                                               UINT4 u4DestMacAddr)
{

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }
    if (LldpRxUtlValidateRemTableIndices (u4LldpRemTimeMark,
                                          i4LldpRemLocalPortNum,
                                          u4DestMacAddr,
                                          i4LldpRemIndex) != OSIX_SUCCESS)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpXdot1RemTable Index validation failed !!\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpXdot1RemTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpXdot1RemTable (UINT4 *pu4LldpRemTimeMark,
                                       INT4 *pi4LldpRemLocalPortNum,
                                       INT4 *pi4LldpRemIndex)
{
    tLldpRemoteNode    *pRemNode = NULL;
    tLldpRemoteNode    *pNextRemNode = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    pRemNode = (tLldpRemoteNode *) RBTreeGetFirst
        (gLldpGlobalInfo.RemSysDataRBTree);

    if (pRemNode != NULL)
    {
        if (pRemNode->bRcvdPVidTlv == OSIX_TRUE)
        {
            *pu4LldpRemTimeMark = pRemNode->u4RemLastUpdateTime;
            *pi4LldpRemLocalPortNum = pRemNode->i4RemLocalPortNum;
            *pi4LldpRemIndex = pRemNode->i4RemIndex;
            return OSIX_SUCCESS;
        }
        else
        {
            while ((pNextRemNode = (tLldpRemoteNode *) RBTreeGetNext
                    (gLldpGlobalInfo.RemSysDataRBTree, (tRBElem *) pRemNode,
                     NULL)) != NULL)
            {
                if (pNextRemNode->bRcvdPVidTlv == OSIX_TRUE)
                {
                    *pu4LldpRemTimeMark = pNextRemNode->u4RemLastUpdateTime;
                    *pi4LldpRemLocalPortNum = pNextRemNode->i4RemLocalPortNum;
                    *pi4LldpRemIndex = pNextRemNode->i4RemIndex;
                    return OSIX_SUCCESS;
                }
                pRemNode = pNextRemNode;
            }
        }
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexLldpXdot1RemTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexLldpXdot1RemTable (UINT4 u4LldpRemTimeMark,
                                      UINT4 *pu4NextLldpRemTimeMark,
                                      INT4 i4LldpRemLocalPortNum,
                                      INT4 *pi4NextLldpRemLocalPortNum,
                                      INT4 i4LldpRemIndex,
                                      INT4 *pi4NextLldpRemIndex,
                                      UINT4 u4DestMacAddr)
{
    tLldpRemoteNode    *pRemNode = NULL;
    tLldpRemoteNode    *pNextRemNode = NULL;
    INT1                i1RetVal = OSIX_FAILURE;

    pRemNode = LldpRxUtlGetRemoteNode (u4LldpRemTimeMark, i4LldpRemLocalPortNum,
                                       u4DestMacAddr, i4LldpRemIndex);
    if (pRemNode != NULL)
    {
        while ((pNextRemNode = (tLldpRemoteNode *) RBTreeGetNext
                (gLldpGlobalInfo.RemSysDataRBTree, (tRBElem *) pRemNode, NULL))
               != NULL)
        {
            if (pNextRemNode->bRcvdPVidTlv == OSIX_TRUE)
            {
                *pu4NextLldpRemTimeMark = pNextRemNode->u4RemLastUpdateTime;
                *pi4NextLldpRemLocalPortNum = pNextRemNode->i4RemLocalPortNum;
                *pi4NextLldpRemIndex = pNextRemNode->i4RemIndex;
                i1RetVal = OSIX_SUCCESS;
                break;
            }
            pRemNode = pNextRemNode;
        }
    }
    return i1RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot1RemPortVlanId
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot1RemPortVlanId (UINT4 u4LldpRemTimeMark,
                                  INT4 i4LldpRemLocalPortNum,
                                  INT4 i4LldpRemIndex,
                                  INT4 *pi4RetValLldpXdot1RemPortVlanId,
                                  UINT4 u4DestMacAddr)
{
    tLldpRemoteNode    *pRemNode = NULL;
    INT1                i1RetVal = OSIX_FAILURE;
    pRemNode = LldpRxUtlGetRemoteNode (u4LldpRemTimeMark,
                                       i4LldpRemLocalPortNum, u4DestMacAddr,
                                       i4LldpRemIndex);
    if ((pRemNode != NULL) && (pRemNode->bRcvdPVidTlv == OSIX_TRUE))
    {
        *pi4RetValLldpXdot1RemPortVlanId = pRemNode->u2PVId;
        i1RetVal = OSIX_SUCCESS;
    }
    return i1RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpXdot1RemProtoVlanTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpXdot1RemProtoVlanTable (UINT4 u4LldpRemTimeMark,
                                                        INT4
                                                        i4LldpRemLocalPortNum,
                                                        INT4 i4LldpRemIndex,
                                                        INT4
                                                        i4LldpXdot1RemProtoVlanId,
                                                        UINT4 u4DestMacAddr)
{

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }
    if (LldpRxUtlValidateProtVlanIndices (u4LldpRemTimeMark,
                                          i4LldpRemLocalPortNum,
                                          u4DestMacAddr, i4LldpRemIndex,
                                          (UINT2) i4LldpXdot1RemProtoVlanId)
        != OSIX_SUCCESS)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpXdot1RemProtoVlanTable Index validation"
                  " failed !!\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpXdot1RemProtoVlanTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpXdot1RemProtoVlanTable (UINT4 *pu4LldpRemTimeMark,
                                                INT4 *pi4LldpRemLocalPortNum,
                                                INT4 *pi4LldpRemIndex,
                                                INT4
                                                *pi4LldpXdot1RemProtoVlanId)
{
    tLldpxdot1RemProtoVlanInfo *pProtoVlanNode = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    pProtoVlanNode = (tLldpxdot1RemProtoVlanInfo *) RBTreeGetFirst
        (gLldpGlobalInfo.RemProtoVlanRBTree);

    if (pProtoVlanNode != NULL)
    {
        *pu4LldpRemTimeMark = pProtoVlanNode->u4RemLastUpdateTime;
        *pi4LldpRemLocalPortNum = pProtoVlanNode->i4RemLocalPortNum;
        *pi4LldpRemIndex = pProtoVlanNode->i4RemIndex;
        *pi4LldpXdot1RemProtoVlanId = (INT4) pProtoVlanNode->u2ProtoVlanId;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexLldpXdot1RemProtoVlanTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexLldpXdot1RemProtoVlanTable (UINT4 u4LldpRemTimeMark,
                                               UINT4 *pu4NextLldpRemTimeMark,
                                               INT4 i4LldpRemLocalPortNum,
                                               INT4 *pi4NextLldpRemLocalPortNum,
                                               INT4 i4LldpRemIndex,
                                               INT4 *pi4NextLldpRemIndex,
                                               INT4 i4LldpXdot1RemProtoVlanId,
                                               INT4
                                               *pi4NextLldpXdot1RemProtoVlanId,
                                               UINT4 u4DestMacAddr)
{
    tLldpxdot1RemProtoVlanInfo *pProtoVlanNode = NULL;
    tLldpxdot1RemProtoVlanInfo *pNextProtoVlanNode = NULL;

    if (LldpRxUtlGetRemProtoVlanNode (u4LldpRemTimeMark,
                                      i4LldpRemLocalPortNum,
                                      u4DestMacAddr, i4LldpRemIndex,
                                      (UINT2) i4LldpXdot1RemProtoVlanId,
                                      &pProtoVlanNode) == OSIX_SUCCESS)
    {
        pNextProtoVlanNode = (tLldpxdot1RemProtoVlanInfo *) RBTreeGetNext
            (gLldpGlobalInfo.RemProtoVlanRBTree, (tRBElem *) pProtoVlanNode,
             NULL);
        if (pNextProtoVlanNode != NULL)
        {
            *pu4NextLldpRemTimeMark = pNextProtoVlanNode->u4RemLastUpdateTime;
            *pi4NextLldpRemLocalPortNum = pNextProtoVlanNode->i4RemLocalPortNum;
            *pi4NextLldpRemIndex = pNextProtoVlanNode->i4RemIndex;
            *pi4NextLldpXdot1RemProtoVlanId =
                (INT4) pNextProtoVlanNode->u2ProtoVlanId;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot1RemProtoVlanSupported
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot1RemProtoVlanSupported (UINT4 u4LldpRemTimeMark,
                                          INT4 i4LldpRemLocalPortNum,
                                          INT4 i4LldpRemIndex,
                                          INT4 i4LldpXdot1RemProtoVlanId,
                                          INT4
                                          *pi4RetValLldpXdot1RemProtoVlanSupported,
                                          UINT4 u4DestMacAddr)
{
    tLldpxdot1RemProtoVlanInfo *pProtoVlanNode = NULL;
    if (LldpRxUtlGetRemProtoVlanNode (u4LldpRemTimeMark,
                                      i4LldpRemLocalPortNum,
                                      u4DestMacAddr,
                                      i4LldpRemIndex,
                                      (UINT2) i4LldpXdot1RemProtoVlanId,
                                      &pProtoVlanNode) == OSIX_SUCCESS)

    {
        *pi4RetValLldpXdot1RemProtoVlanSupported =
            (INT4) pProtoVlanNode->u1ProtoVlanSupported;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot1RemProtoVlanEnabled
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot1RemProtoVlanEnabled (UINT4 u4LldpRemTimeMark,
                                        INT4 i4LldpRemLocalPortNum,
                                        INT4 i4LldpRemIndex,
                                        INT4 i4LldpXdot1RemProtoVlanId,
                                        INT4
                                        *pi4RetValLldpXdot1RemProtoVlanEnabled,
                                        UINT4 u4DestMacAddr)
{
    tLldpxdot1RemProtoVlanInfo *pProtoVlanNode = NULL;
    if (LldpRxUtlGetRemProtoVlanNode (u4LldpRemTimeMark,
                                      i4LldpRemLocalPortNum,
                                      u4DestMacAddr,
                                      i4LldpRemIndex,
                                      (UINT2) i4LldpXdot1RemProtoVlanId,
                                      &pProtoVlanNode) == OSIX_SUCCESS)
    {
        *pi4RetValLldpXdot1RemProtoVlanEnabled =
            (INT4) pProtoVlanNode->u1ProtoVlanEnabled;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpXdot1RemVlanNameTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpXdot1RemVlanNameTable (UINT4 u4LldpRemTimeMark,
                                                       INT4
                                                       i4LldpRemLocalPortNum,
                                                       INT4 i4LldpRemIndex,
                                                       INT4
                                                       i4LldpXdot1RemVlanId,
                                                       UINT4 u4DestMacAddr)
{

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }
    if (LldpRxUtlValidateVlanNameIndices (u4LldpRemTimeMark,
                                          i4LldpRemLocalPortNum, u4DestMacAddr,
                                          i4LldpRemIndex,
                                          (UINT2) i4LldpXdot1RemVlanId)
        != OSIX_SUCCESS)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpXdot1RemVlanNameTable Index validation"
                  " failed !!\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpXdot1RemVlanNameTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpXdot1RemVlanNameTable (UINT4 *pu4LldpRemTimeMark,
                                               INT4 *pi4LldpRemLocalPortNum,
                                               INT4 *pi4LldpRemIndex,
                                               INT4 *pi4LldpXdot1RemVlanId)
{
    tLldpxdot1RemVlanNameInfo *pVlanNode = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    pVlanNode = (tLldpxdot1RemVlanNameInfo *) RBTreeGetFirst
        (gLldpGlobalInfo.RemVlanNameInfoRBTree);

    if (pVlanNode != NULL)
    {
        *pu4LldpRemTimeMark = pVlanNode->u4RemLastUpdateTime;
        *pi4LldpRemLocalPortNum = pVlanNode->i4RemLocalPortNum;
        *pi4LldpRemIndex = pVlanNode->i4RemIndex;
        *pi4LldpXdot1RemVlanId = (INT4) pVlanNode->u2VlanId;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexLldpXdot1RemVlanNameTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexLldpXdot1RemVlanNameTable (UINT4 u4LldpRemTimeMark,
                                              UINT4 *pu4NextLldpRemTimeMark,
                                              INT4 i4LldpRemLocalPortNum,
                                              INT4 *pi4NextLldpRemLocalPortNum,
                                              INT4 i4LldpRemIndex,
                                              INT4 *pi4NextLldpRemIndex,
                                              INT4 i4LldpXdot1RemVlanId,
                                              INT4 *pi4NextLldpXdot1RemVlanId,
                                              UINT4 u4DestMacAddr)
{
    tLldpxdot1RemVlanNameInfo *pVlanNode = NULL;
    tLldpxdot1RemVlanNameInfo *pNextVlanNode = NULL;

    if (LldpRxUtlGetRemVlanNameNode (u4LldpRemTimeMark,
                                     i4LldpRemLocalPortNum,
                                     u4DestMacAddr,
                                     i4LldpRemIndex,
                                     (UINT2) i4LldpXdot1RemVlanId,
                                     &pVlanNode) == OSIX_SUCCESS)
    {
        pNextVlanNode = (tLldpxdot1RemVlanNameInfo *) RBTreeGetNext
            (gLldpGlobalInfo.RemVlanNameInfoRBTree, (tRBElem *) pVlanNode,
             NULL);
        if (pNextVlanNode != NULL)
        {
            *pu4NextLldpRemTimeMark = pNextVlanNode->u4RemLastUpdateTime;
            *pi4NextLldpRemLocalPortNum = pNextVlanNode->i4RemLocalPortNum;
            *pi4NextLldpRemIndex = pNextVlanNode->i4RemIndex;
            *pi4NextLldpXdot1RemVlanId = (INT4) pNextVlanNode->u2VlanId;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot1RemVlanName
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot1RemVlanName (UINT4 u4LldpRemTimeMark,
                                INT4 i4LldpRemLocalPortNum,
                                INT4 i4LldpRemIndex,
                                INT4 i4LldpXdot1RemVlanId,
                                tSNMP_OCTET_STRING_TYPE
                                * pRetValLldpXdot1RemVlanName,
                                UINT4 u4DestMacAddr)
{
    tLldpxdot1RemVlanNameInfo *pVlanNode = NULL;
    INT4                i4VlanNameLen;
    if (LldpRxUtlGetRemVlanNameNode (u4LldpRemTimeMark,
                                     i4LldpRemLocalPortNum,
                                     u4DestMacAddr,
                                     i4LldpRemIndex,
                                     (UINT2) i4LldpXdot1RemVlanId,
                                     &pVlanNode) == OSIX_SUCCESS)
    {
        i4VlanNameLen = (INT4) LLDP_STRLEN (pVlanNode->au1VlanName,
                                            VLAN_STATIC_MAX_NAME_LEN);
        MEMCPY (pRetValLldpXdot1RemVlanName->pu1_OctetList,
                pVlanNode->au1VlanName, i4VlanNameLen);
        pRetValLldpXdot1RemVlanName->i4_Length = i4VlanNameLen;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpXdot1RemProtocolTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpXdot1RemProtocolTable (UINT4 u4LldpRemTimeMark,
                                                       INT4
                                                       i4LldpRemLocalPortNum,
                                                       INT4 i4LldpRemIndex,
                                                       INT4
                                                       i4LldpXdot1RemProtocolIndex,
                                                       UINT4 u4DestMacAddr)
{

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }
    if (LldpRxUtlValidateProtoIdIndices (u4LldpRemTimeMark,
                                         i4LldpRemLocalPortNum, u4DestMacAddr,
                                         i4LldpRemIndex,
                                         (UINT4) i4LldpXdot1RemProtocolIndex)
        != OSIX_SUCCESS)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpXdot1RemProtocolTable Index validation"
                  " failed !!\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpXdot1RemProtocolTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpXdot1RemProtocolTable (UINT4 *pu4LldpRemTimeMark,
                                               INT4 *pi4LldpRemLocalPortNum,
                                               INT4 *pi4LldpRemIndex,
                                               INT4
                                               *pi4LldpXdot1RemProtocolIndex)
{
    tLldpxdot1RemProtoIdInfo *pProtoIdNode = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    pProtoIdNode = (tLldpxdot1RemProtoIdInfo *) RBTreeGetFirst
        (gLldpGlobalInfo.RemProtoIdRBTree);

    if (pProtoIdNode != NULL)
    {
        *pu4LldpRemTimeMark = pProtoIdNode->u4RemLastUpdateTime;
        *pi4LldpRemLocalPortNum = pProtoIdNode->i4RemLocalPortNum;
        *pi4LldpRemIndex = pProtoIdNode->i4RemIndex;
        *pi4LldpXdot1RemProtocolIndex = (INT4) pProtoIdNode->u4ProtocolIndex;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexLldpXdot1RemProtocolTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexLldpXdot1RemProtocolTable (UINT4 u4LldpRemTimeMark,
                                              UINT4 *pu4NextLldpRemTimeMark,
                                              INT4 i4LldpRemLocalPortNum,
                                              INT4 *pi4NextLldpRemLocalPortNum,
                                              INT4 i4LldpRemIndex,
                                              INT4 *pi4NextLldpRemIndex,
                                              INT4 i4LldpXdot1RemProtocolIndex,
                                              INT4
                                              *pi4NextLldpXdot1RemProtocolIndex,
                                              UINT4 u4DestMacAddr)
{
    tLldpxdot1RemProtoIdInfo *pProtoIdNode = NULL;
    tLldpxdot1RemProtoIdInfo *pNextProtoIdNode = NULL;

    if (LldpRxUtlGetRemProtoIdNode (u4LldpRemTimeMark,
                                    i4LldpRemLocalPortNum, u4DestMacAddr,
                                    i4LldpRemIndex,
                                    (UINT4) i4LldpXdot1RemProtocolIndex,
                                    &pProtoIdNode) == OSIX_SUCCESS)
    {
        pNextProtoIdNode = (tLldpxdot1RemProtoIdInfo *) RBTreeGetNext
            (gLldpGlobalInfo.RemProtoIdRBTree, (tRBElem *) pProtoIdNode, NULL);
        if (pNextProtoIdNode != NULL)
        {
            *pu4NextLldpRemTimeMark = pNextProtoIdNode->u4RemLastUpdateTime;
            *pi4NextLldpRemLocalPortNum = pNextProtoIdNode->i4RemLocalPortNum;
            *pi4NextLldpRemIndex = pNextProtoIdNode->i4RemIndex;
            *pi4NextLldpXdot1RemProtocolIndex =
                (INT4) pNextProtoIdNode->u4ProtocolIndex;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot1RemProtocolId
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot1RemProtocolId (UINT4 u4LldpRemTimeMark,
                                  INT4 i4LldpRemLocalPortNum,
                                  INT4 i4LldpRemIndex,
                                  INT4 i4LldpXdot1RemProtocolIndex,
                                  tSNMP_OCTET_STRING_TYPE
                                  * pRetValLldpXdot1RemProtocolId,
                                  UINT4 u4DestMacAddr)
{
    tLldpxdot1RemProtoIdInfo *pProtoIdNode = NULL;
    UINT1               u1ProtoIdLen;
    if (LldpRxUtlGetRemProtoIdNode (u4LldpRemTimeMark,
                                    i4LldpRemLocalPortNum, u4DestMacAddr,
                                    i4LldpRemIndex,
                                    (UINT4) i4LldpXdot1RemProtocolIndex,
                                    &pProtoIdNode) == OSIX_SUCCESS)
    {
        u1ProtoIdLen = (UINT1) (LLDP_STRLEN (pProtoIdNode->au1ProtocolId,
                                             LLDP_MAX_LEN_PROTOID));
        MEMCPY (pRetValLldpXdot1RemProtocolId->pu1_OctetList,
                pProtoIdNode->au1ProtocolId, u1ProtoIdLen);
        pRetValLldpXdot1RemProtocolId->i4_Length = (INT4) u1ProtoIdLen;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpXdot3PortConfigTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpXdot3PortConfigTable (INT4
                                                      i4LldpPortConfigPortNum,
                                                      tMacAddr DestMacAddr)
{
    UINT4               u4LocPort = 0;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    if (LldpUtlGetLocalPort (i4LldpPortConfigPortNum, DestMacAddr, &u4LocPort)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    if (LldpTxUtlValidatePortIndex (u4LocPort) == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpXdot3PortConfigTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpXdot3PortConfigTable (INT4 *pi4LldpPortConfigPortNum)
{
    tLldpLocPortTable  *pLocPortTbl = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return OSIX_FAILURE;
    }

    pLocPortTbl =
        (tLldpLocPortTable *) RBTreeGetFirst (gLldpGlobalInfo.
                                              LldpPortInfoRBTree);
    if (pLocPortTbl != NULL)
    {
        *pi4LldpPortConfigPortNum = pLocPortTbl->i4IfIndex;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexLldpXdot3PortConfigTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexLldpXdot3PortConfigTable (INT4 i4LldpPortConfigPortNum,
                                             INT4 *pi4NextLldpPortConfigPortNum)
{
    tLldpLocPortTable   CurrLocPortTbl;
    tLldpLocPortTable  *pNextLocPortTbl = NULL;

    CurrLocPortTbl.i4IfIndex = i4LldpPortConfigPortNum;

    pNextLocPortTbl =
        (tLldpLocPortTable *) RBTreeGetNext (gLldpGlobalInfo.LldpPortInfoRBTree,
                                             &CurrLocPortTbl,
                                             LldpPortInfoUtlRBCmpInfo);

    if (pNextLocPortTbl != NULL)
    {
        *pi4NextLldpPortConfigPortNum = pNextLocPortTbl->i4IfIndex;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot3PortConfigTLVsTxEnable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot3PortConfigTLVsTxEnable (INT4 i4LldpPortConfigPortNum,
                                           tSNMP_OCTET_STRING_TYPE
                                           *
                                           pRetValLldpXdot3PortConfigTLVsTxEnable,
                                           tMacAddr DestMacAddr)
{
    tLldpLocPortInfo   *pLldpLocPortInfo;
    UINT4               u4LocPort = 0;

    if (LldpUtlGetLocalPort (i4LldpPortConfigPortNum, DestMacAddr, &u4LocPort)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }
    pLldpLocPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPort);
    if (pLldpLocPortInfo != NULL)
    {
        pRetValLldpXdot3PortConfigTLVsTxEnable->pu1_OctetList[0] =
            pLldpLocPortInfo->Dot3LocPortInfo.u1TLVTxEnable;
        pRetValLldpXdot3PortConfigTLVsTxEnable->i4_Length = sizeof (UINT1);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlSetLldpXdot3PortConfigTLVsTxEnable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlSetLldpXdot3PortConfigTLVsTxEnable (INT4 i4LldpPortConfigPortNum,
                                           tSNMP_OCTET_STRING_TYPE
                                           *
                                           pSetValLldpXdot3PortConfigTLVsTxEnable,
                                           tMacAddr DestMacAddr)
{
    tLldpLocPortInfo   *pLldpLocPortInfo;
    UINT4               u4LocPort = 0;

    if (LldpUtlGetLocalPort (i4LldpPortConfigPortNum, DestMacAddr, &u4LocPort)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    pLldpLocPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPort);
    if (pLldpLocPortInfo != NULL)
    {
        if (pLldpLocPortInfo->Dot3LocPortInfo.u1TLVTxEnable !=
            pSetValLldpXdot3PortConfigTLVsTxEnable->pu1_OctetList[0])
        {
            pLldpLocPortInfo->Dot3LocPortInfo.u1TLVTxEnable =
                pSetValLldpXdot3PortConfigTLVsTxEnable->pu1_OctetList[0];
            LLDP_TRC_ARG1 (MGMT_TRC, " SNMPSTD: Configured Dot3TxEnable for"
                           " port %d\n", i4LldpPortConfigPortNum);
            if (LldpTxUtlHandleLocPortInfoChg (pLldpLocPortInfo)
                != OSIX_SUCCESS)
            {
                LLDP_TRC (ALL_FAILURE_TRC, "SNMPSTD: "
                          "LldpTxUtlHandleLocPortInfoChg failed\r\n");
            }
        }
        else
        {
            LLDP_TRC_ARG1 (MGMT_TRC, "SNMPSTD:"
                           " nmhSetLldpXdot3PortConfigTLVsTxEnableValue: "
                           "Same value configured already for port %d\n",
                           i4LldpPortConfigPortNum);
        }
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlTestLldpXdot3PortConfigTLVsTxEnable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlTestLldpXdot3PortConfigTLVsTxEnable (UINT4 *pu4ErrorCode,
                                            INT4 i4LldpPortConfigPortNum,
                                            tSNMP_OCTET_STRING_TYPE
                                            *
                                            pTestValLldpXdot3PortConfigTLVsTxEnable,
                                            tMacAddr DestMacAddr)
{
    UINT4               u4LocPort = 0;
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;

    if (LldpUtlGetLocalPort (i4LldpPortConfigPortNum, DestMacAddr, &u4LocPort)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }
    pLldpLocPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPort);
    if (pLldpLocPortInfo != NULL)
    {
        if ((pTestValLldpXdot3PortConfigTLVsTxEnable->i4_Length
             != sizeof (UINT1)) ||
            (pTestValLldpXdot3PortConfigTLVsTxEnable->pu1_OctetList[0]
             & ~LLDP_DOT3TLV_BMAP))
        {
            *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
            LLDP_TRC (ALL_FAILURE_TRC,
                      " SNMPSTD: Only the first four bits should be used to set"
                      " or reset the transmission flag \n");
            return OSIX_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }
    if ((pTestValLldpXdot3PortConfigTLVsTxEnable->pu1_OctetList[0] &
         LLDP_PWR_VIA_MDI_TLV_ENABLED) == LLDP_PWR_VIA_MDI_TLV_ENABLED)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: Power via MDI TLV is not supported\n");
        return OSIX_FAILURE;
    }
    if ((pTestValLldpXdot3PortConfigTLVsTxEnable->pu1_OctetList[0] &
         LLDP_LINK_AGG_TLV_ENABLED) == LLDP_LINK_AGG_TLV_ENABLED)
    {
        if (LLdpLaIsPortInIcclIf ((UINT4) i4LldpPortConfigPortNum) ==
            OSIX_SUCCESS)
        {
            *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
            LLDP_TRC (ALL_FAILURE_TRC,
                      " SNMPSTD: Link Aggregation TLV cannot be set on"
                      " ICCL interface\n");
            CLI_SET_ERR (LLDP_CLI_ERR_INVALID_LA_TLV_ON_ICCL);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpXdot3LocPortTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpXdot3LocPortTable (INT4 i4LldpLocPortNum)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    if (LldpTxUtlValidateInterfaceIndex (i4LldpLocPortNum) == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpXdot3LocPortTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpXdot3LocPortTable (INT4 *pi4LldpLocPortNum)
{
    tLldpLocPortTable  *pLocPortTable = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    /* Get the First Used Index */
    pLocPortTable = (tLldpLocPortTable *)
        RBTreeGetFirst (gLldpGlobalInfo.LldpPortInfoRBTree);
    if (pLocPortTable != NULL)
    {
        *pi4LldpLocPortNum = pLocPortTable->i4IfIndex;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexLldpXdot3LocPortTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexLldpXdot3LocPortTable (INT4 i4LldpLocPortNum,
                                          INT4 *pi4NextLldpLocPortNum)
{
    INT1                i1RetVal = OSIX_FAILURE;
    tLldpLocPortTable  *pLocPortTable = NULL;
    tLldpLocPortTable   LocPortTable;

    MEMSET (&LocPortTable, 0, sizeof (tLldpLocPortTable));

    if ((UINT4) i4LldpLocPortNum >= LLDP_MAX_PORTS)
    {
        return OSIX_FAILURE;
    }

    if (i4LldpLocPortNum < LLDP_MIN_PORTS)
    {
        i4LldpLocPortNum = LLDP_MIN_PORTS - 1;
    }

    LocPortTable.i4IfIndex = i4LldpLocPortNum;
    pLocPortTable = (tLldpLocPortTable *)
        RBTreeGetNext (gLldpGlobalInfo.LldpPortInfoRBTree, &LocPortTable,
                       LldpPortInfoUtlRBCmpInfo);
    if (pLocPortTable != NULL)
    {
        *pi4NextLldpLocPortNum = pLocPortTable->i4IfIndex;
        i1RetVal = OSIX_SUCCESS;
    }
    return i1RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot3LocPortAutoNegSupported
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot3LocPortAutoNegSupported (INT4 i4LldpLocPortNum,
                                            INT4
                                            *pi4RetValLldpXdot3LocPortAutoNegSupported)
{
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    tLldpxdot3AutoNegInfo *pDot3AutoNegInfo = NULL;

    if (LldpTxUtlGetLocPortEntry ((UINT4) i4LldpLocPortNum,
                                  &pLldpLocPortInfo) == OSIX_SUCCESS)
    {
        pDot3AutoNegInfo = &LLDP_LOC_PORT_AUTO_NEG_INFO (pLldpLocPortInfo);
        *pi4RetValLldpXdot3LocPortAutoNegSupported =
            (INT4) pDot3AutoNegInfo->u1AutoNegSupport;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot3LocPortAutoNegEnabled
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot3LocPortAutoNegEnabled (INT4 i4LldpLocPortNum,
                                          INT4
                                          *pi4RetValLldpXdot3LocPortAutoNegEnabled)
{
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    tLldpxdot3AutoNegInfo *pDot3AutoNegInfo = NULL;

    if (LldpTxUtlGetLocPortEntry ((UINT4) i4LldpLocPortNum,
                                  &pLldpLocPortInfo) == OSIX_SUCCESS)
    {
        pDot3AutoNegInfo = &LLDP_LOC_PORT_AUTO_NEG_INFO (pLldpLocPortInfo);
        *pi4RetValLldpXdot3LocPortAutoNegEnabled =
            (INT4) pDot3AutoNegInfo->u1AutoNegEnabled;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot3LocPortAutoNegAdvertisedCap
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot3LocPortAutoNegAdvertisedCap (INT4 i4LldpLocPortNum,
                                                tSNMP_OCTET_STRING_TYPE
                                                *
                                                pRetValLldpXdot3LocPortAutoNegAdvertisedCap)
{
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    tLldpxdot3AutoNegInfo *pDot3AutoNegInfo = NULL;
    UINT2               u2AutoNegAdvtCap = 0;

    if (LldpTxUtlGetLocPortEntry ((UINT4) i4LldpLocPortNum,
                                  &pLldpLocPortInfo) == OSIX_SUCCESS)
    {
        pDot3AutoNegInfo = &LLDP_LOC_PORT_AUTO_NEG_INFO (pLldpLocPortInfo);
        u2AutoNegAdvtCap = pDot3AutoNegInfo->u2AutoNegAdvtCap;
        pRetValLldpXdot3LocPortAutoNegAdvertisedCap->pu1_OctetList[1] =
            (UINT1) (u2AutoNegAdvtCap & 0x00ff);
        pRetValLldpXdot3LocPortAutoNegAdvertisedCap->pu1_OctetList[0] =
            (UINT1) ((u2AutoNegAdvtCap & 0xff00) >> 8);

        pRetValLldpXdot3LocPortAutoNegAdvertisedCap->i4_Length = sizeof (UINT2);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot3LocPortOperMauType
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot3LocPortOperMauType (INT4 i4LldpLocPortNum,
                                       INT4
                                       *pi4RetValLldpXdot3LocPortOperMauType)
{
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    tLldpxdot3AutoNegInfo *pDot3AutoNegInfo = NULL;

    if (LldpTxUtlGetLocPortEntry ((UINT4) i4LldpLocPortNum,
                                  &pLldpLocPortInfo) == OSIX_SUCCESS)
    {
        pDot3AutoNegInfo = &LLDP_LOC_PORT_AUTO_NEG_INFO (pLldpLocPortInfo);
        *pi4RetValLldpXdot3LocPortOperMauType =
            (INT4) pDot3AutoNegInfo->u2OperMauType;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpXdot3LocLinkAggTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpXdot3LocLinkAggTable (INT4 i4LldpLocPortNum)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    if (LldpTxUtlValidateInterfaceIndex (i4LldpLocPortNum) == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpXdot3LocLinkAggTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpXdot3LocLinkAggTable (INT4 *pi4LldpLocPortNum)
{
    tLldpLocPortTable  *pLocPortTable = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    /* Get the First Used Index */
    pLocPortTable = (tLldpLocPortTable *)
        RBTreeGetFirst (gLldpGlobalInfo.LldpPortInfoRBTree);
    if (pLocPortTable != NULL)
    {
        *pi4LldpLocPortNum = pLocPortTable->i4IfIndex;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexLldpXdot3LocLinkAggTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexLldpXdot3LocLinkAggTable (INT4 i4LldpLocPortNum,
                                             INT4 *pi4NextLldpLocPortNum)
{
    tLldpLocPortTable  *pLocPortTable = NULL;
    tLldpLocPortTable   LocPortTable;

    INT1                i1RetVal = OSIX_FAILURE;

    if ((UINT4) i4LldpLocPortNum >= LLDP_MAX_PORTS)
    {
        return OSIX_FAILURE;
    }

    if (i4LldpLocPortNum < LLDP_MIN_PORTS)
    {
        i4LldpLocPortNum = LLDP_MIN_PORTS - 1;
    }

    LocPortTable.i4IfIndex = i4LldpLocPortNum;
    pLocPortTable = (tLldpLocPortTable *)
        RBTreeGetNext (gLldpGlobalInfo.LldpPortInfoRBTree, &LocPortTable,
                       LldpPortInfoUtlRBCmpInfo);
    if (pLocPortTable != NULL)
    {
        *pi4NextLldpLocPortNum = pLocPortTable->i4IfIndex;
        i1RetVal = OSIX_SUCCESS;
    }
    return i1RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot3LocLinkAggStatus
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot3LocLinkAggStatus (INT4 i4LldpLocPortNum,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pRetValLldpXdot3LocLinkAggStatus)
{
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;

    MEMSET (pRetValLldpXdot3LocLinkAggStatus->pu1_OctetList, 0, sizeof (UINT1));
    if (LldpTxUtlGetLocPortEntry ((UINT4) i4LldpLocPortNum,
                                  &pLldpLocPortInfo) == OSIX_SUCCESS)
    {
        pRetValLldpXdot3LocLinkAggStatus->pu1_OctetList[0] =
            pLldpLocPortInfo->Dot3LocPortInfo.u1AggStatus;
        pRetValLldpXdot3LocLinkAggStatus->i4_Length = sizeof (UINT1);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot3LocLinkAggPortId
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot3LocLinkAggPortId (INT4 i4LldpLocPortNum,
                                     INT4 *pi4RetValLldpXdot3LocLinkAggPortId)
{
    tLldpLocPortInfo   *pLldpLocPortInfo;

    if (LldpTxUtlGetLocPortEntry ((UINT4) i4LldpLocPortNum,
                                  &pLldpLocPortInfo) == OSIX_SUCCESS)
    {
        *pi4RetValLldpXdot3LocLinkAggPortId =
            (INT4) pLldpLocPortInfo->Dot3LocPortInfo.u4AggPortId;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpXdot3LocMaxFrameSizeTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpXdot3LocMaxFrameSizeTable (INT4
                                                           i4LldpLocPortNum)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    if (LldpTxUtlValidateInterfaceIndex (i4LldpLocPortNum) == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpXdot3LocMaxFrameSizeTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpXdot3LocMaxFrameSizeTable (INT4 *pi4LldpLocPortNum)
{
    tLldpLocPortTable  *pLocPortTable = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    /* Get the First Used Index */
    pLocPortTable = (tLldpLocPortTable *)
        RBTreeGetFirst (gLldpGlobalInfo.LldpPortInfoRBTree);
    if (pLocPortTable != NULL)
    {
        *pi4LldpLocPortNum = pLocPortTable->i4IfIndex;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexLldpXdot3LocMaxFrameSizeTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexLldpXdot3LocMaxFrameSizeTable (INT4 i4LldpLocPortNum,
                                                  INT4 *pi4NextLldpLocPortNum)
{
    tLldpLocPortTable  *pLocPortTable = NULL;
    tLldpLocPortTable   LocPortTable;
    INT1                i1RetVal = OSIX_FAILURE;

    if ((UINT4) i4LldpLocPortNum >= LLDP_MAX_PORTS)
    {
        return OSIX_FAILURE;
    }

    if (i4LldpLocPortNum < LLDP_MIN_PORTS)
    {
        i4LldpLocPortNum = LLDP_MIN_PORTS - 1;
    }

    LocPortTable.i4IfIndex = i4LldpLocPortNum;
    pLocPortTable = (tLldpLocPortTable *)
        RBTreeGetNext (gLldpGlobalInfo.LldpPortInfoRBTree, &LocPortTable,
                       LldpPortInfoUtlRBCmpInfo);
    if (pLocPortTable != NULL)
    {
        *pi4NextLldpLocPortNum = pLocPortTable->i4IfIndex;
        i1RetVal = OSIX_SUCCESS;
    }
    return i1RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot3LocMaxFrameSize
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot3LocMaxFrameSize (INT4 i4LldpLocPortNum,
                                    INT4 *pi4RetValLldpXdot3LocMaxFrameSize)
{
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;

    if (LldpTxUtlGetLocPortEntry ((UINT4) i4LldpLocPortNum,
                                  &pLldpLocPortInfo) == OSIX_SUCCESS)
    {
        *pi4RetValLldpXdot3LocMaxFrameSize =
            (INT4) pLldpLocPortInfo->Dot3LocPortInfo.u2MaxFrameSize;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpXdot3RemPortTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpXdot3RemPortTable (UINT4 u4LldpRemTimeMark,
                                                   INT4 i4LldpRemLocalPortNum,
                                                   INT4 i4LldpRemIndex,
                                                   UINT4 u4DestMacAddr)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    if (LldpRxUtlValidateDot3RemTableInd (u4LldpRemTimeMark,
                                          i4LldpRemLocalPortNum,
                                          u4DestMacAddr,
                                          i4LldpRemIndex) == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpXdot3RemPortTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpXdot3RemPortTable (UINT4 *pu4LldpRemTimeMark,
                                           INT4 *pi4LldpRemLocalPortNum,
                                           INT4 *pi4LldpRemIndex)
{

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    if (LldpRxUtlGetFirstDot3RemTableInd (pu4LldpRemTimeMark,
                                          pi4LldpRemLocalPortNum,
                                          pi4LldpRemIndex,
                                          LLDP_MAC_PHY_CONFIG_STATUS_TLV)
        == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexLldpXdot3RemPortTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexLldpXdot3RemPortTable (UINT4 u4LldpRemTimeMark,
                                          UINT4 *pu4NextLldpRemTimeMark,
                                          INT4 i4LldpRemLocalPortNum,
                                          INT4 *pi4NextLldpRemLocalPortNum,
                                          INT4 i4LldpRemIndex,
                                          INT4 *pi4NextLldpRemIndex,
                                          tMacAddr DestMacAddr)
{
    UINT4               u4LocPort = 0;

    if (LldpUtlGetLocalPort (i4LldpRemLocalPortNum, DestMacAddr, &u4LocPort) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    if (LldpRxUtlGetNextDot3RemTableInd (u4LldpRemTimeMark,
                                         pu4NextLldpRemTimeMark,
                                         (INT4) u4LocPort,
                                         pi4NextLldpRemLocalPortNum,
                                         i4LldpRemIndex, pi4NextLldpRemIndex,
                                         LLDP_MAC_PHY_CONFIG_STATUS_TLV)
        == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot3RemPortAutoNegSupported
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot3RemPortAutoNegSupported (UINT4 u4LldpRemTimeMark,
                                            INT4 i4LldpRemLocalPortNum,
                                            INT4 i4LldpRemIndex,
                                            INT4
                                            *pi4RetValLldpXdot3RemPortAutoNegSupported,
                                            UINT4 u4DestMacAddr)
{
    tLldpRemoteNode    *pRemNode = NULL;
    tLldpxdot3RemPortInfo *pDot3RemPortInfo;
    pRemNode = LldpRxUtlGetRemoteNode (u4LldpRemTimeMark, i4LldpRemLocalPortNum,
                                       u4DestMacAddr, i4LldpRemIndex);
    if ((pRemNode != NULL) && (pRemNode->bRcvdMacPhyTlv == OSIX_TRUE))
    {
        pDot3RemPortInfo = pRemNode->pDot3RemPortInfo;
        if (pDot3RemPortInfo != NULL)
        {
            *pi4RetValLldpXdot3RemPortAutoNegSupported =
                (INT4) pDot3RemPortInfo->RemDot3AutoNegInfo.u1AutoNegSupport;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot3RemPortAutoNegEnabled
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot3RemPortAutoNegEnabled (UINT4 u4LldpRemTimeMark,
                                          INT4 i4LldpRemLocalPortNum,
                                          INT4 i4LldpRemIndex,
                                          INT4
                                          *pi4RetValLldpXdot3RemPortAutoNegEnabled,
                                          UINT4 u4DestMacAddr)
{
    tLldpRemoteNode    *pRemNode = NULL;
    tLldpxdot3RemPortInfo *pDot3RemPortInfo;
    pRemNode = LldpRxUtlGetRemoteNode (u4LldpRemTimeMark, i4LldpRemLocalPortNum,
                                       u4DestMacAddr, i4LldpRemIndex);
    if ((pRemNode != NULL) && (pRemNode->bRcvdMacPhyTlv == OSIX_TRUE))
    {
        pDot3RemPortInfo = pRemNode->pDot3RemPortInfo;
        if (pDot3RemPortInfo != NULL)
        {
            *pi4RetValLldpXdot3RemPortAutoNegEnabled =
                (INT4) pDot3RemPortInfo->RemDot3AutoNegInfo.u1AutoNegEnabled;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot3RemPortAutoNegAdvertisedCap
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot3RemPortAutoNegAdvertisedCap (UINT4 u4LldpRemTimeMark,
                                                INT4 i4LldpRemLocalPortNum,
                                                INT4 i4LldpRemIndex,
                                                tSNMP_OCTET_STRING_TYPE
                                                *
                                                pRetValLldpXdot3RemPortAutoNegAdvertisedCap,
                                                UINT4 u4DestMacAddr)
{
    tLldpRemoteNode    *pRemNode = NULL;
    tLldpxdot3RemPortInfo *pDot3RemPortInfo;
    UINT2               u2AutoNegAdvtCap = 0;
    pRemNode = LldpRxUtlGetRemoteNode (u4LldpRemTimeMark, i4LldpRemLocalPortNum,
                                       u4DestMacAddr, i4LldpRemIndex);
    if ((pRemNode != NULL) && (pRemNode->bRcvdMacPhyTlv == OSIX_TRUE))
    {
        pDot3RemPortInfo = pRemNode->pDot3RemPortInfo;
        if (pDot3RemPortInfo != NULL)
        {
            u2AutoNegAdvtCap =
                pDot3RemPortInfo->RemDot3AutoNegInfo.u2AutoNegAdvtCap;
            pRetValLldpXdot3RemPortAutoNegAdvertisedCap->pu1_OctetList[1]
                = (UINT1) (u2AutoNegAdvtCap & 0x00ff);
            pRetValLldpXdot3RemPortAutoNegAdvertisedCap->pu1_OctetList[0]
                = (UINT1) ((u2AutoNegAdvtCap & 0xff00) >> 8);

            pRetValLldpXdot3RemPortAutoNegAdvertisedCap->i4_Length =
                sizeof (UINT2);
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot3RemPortOperMauType
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot3RemPortOperMauType (UINT4 u4LldpRemTimeMark,
                                       INT4 i4LldpRemLocalPortNum,
                                       INT4 i4LldpRemIndex,
                                       INT4
                                       *pi4RetValLldpXdot3RemPortOperMauType,
                                       UINT4 u4DestMacAddr)
{
    tLldpRemoteNode    *pRemNode = NULL;
    tLldpxdot3RemPortInfo *pDot3RemPortInfo;
    pRemNode = LldpRxUtlGetRemoteNode (u4LldpRemTimeMark, i4LldpRemLocalPortNum,
                                       u4DestMacAddr, i4LldpRemIndex);
    if ((pRemNode != NULL) && (pRemNode->bRcvdMacPhyTlv == OSIX_TRUE))
    {
        pDot3RemPortInfo = pRemNode->pDot3RemPortInfo;
        if (pDot3RemPortInfo != NULL)
        {
            *pi4RetValLldpXdot3RemPortOperMauType =
                (INT4) pDot3RemPortInfo->RemDot3AutoNegInfo.u2OperMauType;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpXdot3RemPowerTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpXdot3RemPowerTable (UINT4 u4LldpRemTimeMark,
                                                    INT4 i4LldpRemLocalPortNum,
                                                    INT4 i4LldpRemIndex,
                                                    UINT4 u4DestMacAddr)
{

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    if (LldpRxUtlValidateDot3RemTableInd (u4LldpRemTimeMark,
                                          i4LldpRemLocalPortNum, u4DestMacAddr,
                                          i4LldpRemIndex) == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpXdot3RemLinkAggTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpXdot3RemLinkAggTable (UINT4 u4LldpRemTimeMark,
                                                      INT4
                                                      i4LldpRemLocalPortNum,
                                                      INT4 i4LldpRemIndex,
                                                      UINT4 u4DestMacAddr)
{

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }
    if (LldpRxUtlValidateDot3RemTableInd (u4LldpRemTimeMark,
                                          i4LldpRemLocalPortNum, u4DestMacAddr,
                                          i4LldpRemIndex) == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpXdot3RemLinkAggTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpXdot3RemLinkAggTable (UINT4 *pu4LldpRemTimeMark,
                                              INT4 *pi4LldpRemLocalPortNum,
                                              INT4 *pi4LldpRemIndex)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    if (LldpRxUtlGetFirstDot3RemTableInd (pu4LldpRemTimeMark,
                                          pi4LldpRemLocalPortNum,
                                          pi4LldpRemIndex, LLDP_LINK_AGG_TLV)
        == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexLldpXdot3RemLinkAggTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexLldpXdot3RemLinkAggTable (UINT4 u4LldpRemTimeMark,
                                             UINT4 *pu4NextLldpRemTimeMark,
                                             INT4 i4LldpRemLocalPortNum,
                                             INT4 *pi4NextLldpRemLocalPortNum,
                                             INT4 i4LldpRemIndex,
                                             INT4 *pi4NextLldpRemIndex,
                                             tMacAddr DestMacAddr)
{
    UINT4               u4LocPort = 0;

    if (LldpUtlGetLocalPort (i4LldpRemLocalPortNum, DestMacAddr, &u4LocPort) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    if (LldpRxUtlGetNextDot3RemTableInd (u4LldpRemTimeMark,
                                         pu4NextLldpRemTimeMark,
                                         u4LocPort,
                                         pi4NextLldpRemLocalPortNum,
                                         i4LldpRemIndex, pi4NextLldpRemIndex,
                                         (UINT1) LLDP_LINK_AGG_TLV) ==
        OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot3RemLinkAggStatus
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot3RemLinkAggStatus (UINT4 u4LldpRemTimeMark,
                                     INT4 i4LldpRemLocalPortNum,
                                     INT4 i4LldpRemIndex,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pRetValLldpXdot3RemLinkAggStatus,
                                     UINT4 u4DestMacAddr)
{
    tLldpRemoteNode    *pRemNode = NULL;
    tLldpxdot3RemPortInfo *pDot3RemPortInfo = NULL;
    pRemNode = LldpRxUtlGetRemoteNode (u4LldpRemTimeMark, i4LldpRemLocalPortNum,
                                       u4DestMacAddr, i4LldpRemIndex);
    if ((pRemNode != NULL) && (pRemNode->bRcvdLinkAggTlv == OSIX_TRUE))
    {
        if (pRemNode->pDot3RemPortInfo != NULL)
        {
            pDot3RemPortInfo = pRemNode->pDot3RemPortInfo;
            pRetValLldpXdot3RemLinkAggStatus->pu1_OctetList[0] =
                pDot3RemPortInfo->u1AggStatus;
            pRetValLldpXdot3RemLinkAggStatus->i4_Length = sizeof (UINT1);
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot3RemLinkAggPortId
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot3RemLinkAggPortId (UINT4 u4LldpRemTimeMark,
                                     INT4 i4LldpRemLocalPortNum,
                                     INT4 i4LldpRemIndex,
                                     INT4 *pi4RetValLldpXdot3RemLinkAggPortId,
                                     UINT4 u4DestMacAddr)
{
    tLldpRemoteNode    *pRemNode = NULL;
    pRemNode = LldpRxUtlGetRemoteNode (u4LldpRemTimeMark, i4LldpRemLocalPortNum,
                                       u4DestMacAddr, i4LldpRemIndex);
    if ((pRemNode != NULL) && (pRemNode->bRcvdLinkAggTlv == OSIX_TRUE))
    {
        if (pRemNode->pDot3RemPortInfo != NULL)
        {
            *pi4RetValLldpXdot3RemLinkAggPortId =
                (INT4) pRemNode->pDot3RemPortInfo->u4AggPortId;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlValidateIndexInstanceLldpXdot3RemMaxFrameSizeTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlValidateIndexInstanceLldpXdot3RemMaxFrameSizeTable (UINT4
                                                           u4LldpRemTimeMark,
                                                           INT4
                                                           i4LldpRemLocalPortNum,
                                                           INT4 i4LldpRemIndex,
                                                           UINT4 u4DestMacAddr)
{

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }
    if (LldpRxUtlValidateDot3RemTableInd
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum,
         u4DestMacAddr, i4LldpRemIndex) == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetFirstIndexLldpXdot3RemMaxFrameSizeTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetFirstIndexLldpXdot3RemMaxFrameSizeTable (UINT4 *pu4LldpRemTimeMark,
                                                   INT4 *pi4LldpRemLocalPortNum,
                                                   INT4 *pi4LldpRemIndex)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\n");
        return OSIX_FAILURE;
    }

    if (LldpRxUtlGetFirstDot3RemTableInd (pu4LldpRemTimeMark,
                                          pi4LldpRemLocalPortNum,
                                          pi4LldpRemIndex,
                                          LLDP_MAX_FRAME_SIZE_TLV)
        == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetNextIndexLldpXdot3RemMaxFrameSizeTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetNextIndexLldpXdot3RemMaxFrameSizeTable (UINT4 u4LldpRemTimeMark,
                                                  UINT4 *pu4NextLldpRemTimeMark,
                                                  INT4 i4LldpRemLocalPortNum,
                                                  INT4
                                                  *pi4NextLldpRemLocalPortNum,
                                                  INT4 i4LldpRemIndex,
                                                  INT4 *pi4NextLldpRemIndex,
                                                  tMacAddr DestMacAddr)
{
    UINT4               u4LocPort = 0;

    if (LldpUtlGetLocalPort (i4LldpRemLocalPortNum, DestMacAddr, &u4LocPort) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    if (LldpRxUtlGetNextDot3RemTableInd (u4LldpRemTimeMark,
                                         pu4NextLldpRemTimeMark,
                                         u4LocPort,
                                         pi4NextLldpRemLocalPortNum,
                                         i4LldpRemIndex, pi4NextLldpRemIndex,
                                         (UINT1) LLDP_MAX_FRAME_SIZE_TLV)
        == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetLldpXdot3RemMaxFrameSize
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetLldpXdot3RemMaxFrameSize (UINT4 u4LldpRemTimeMark,
                                    INT4 i4LldpRemLocalPortNum,
                                    INT4 i4LldpRemIndex,
                                    INT4 *pi4RetValLldpXdot3RemMaxFrameSize,
                                    UINT4 u4DestMacAddr)
{
    tLldpRemoteNode    *pRemNode = NULL;

    pRemNode = LldpRxUtlGetRemoteNode (u4LldpRemTimeMark, i4LldpRemLocalPortNum,
                                       u4DestMacAddr, i4LldpRemIndex);
    if ((pRemNode != NULL) && (pRemNode->bRcvdMaxFrameTlv == OSIX_TRUE))
    {
        if (pRemNode->pDot3RemPortInfo != NULL)
        {
            *pi4RetValLldpXdot3RemMaxFrameSize =
                (INT4) pRemNode->pDot3RemPortInfo->u2MaxFrameSize;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpCreateDestMacAddressEnty
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpCreateDestMacAddressEnty (INT4 i4IfIndex, tMacAddr DestMacAddr)
{
    tLldpv2DestAddrTbl  DestMacAddrTbl;
    tLldpv2DestAddrTbl *pDestMacAddrTbl = NULL;
    tLldpv2AgentToLocPort *pAgentMappingEntry = NULL;
    tLldpLocPortInfo   *pPortEntry = NULL;
    UINT4               u4DestMacAddrTblIndex = 0;

    MEMSET (&DestMacAddrTbl, 0, sizeof (tLldpv2DestAddrTbl));

    pDestMacAddrTbl = (tLldpv2DestAddrTbl *)
        RBTreeGetFirst (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree);
    while (pDestMacAddrTbl != NULL)
    {
        if (MEMCMP (pDestMacAddrTbl->Lldpv2DestMacAddress, DestMacAddr,
                    MAC_ADDR_LEN) == 0)
        {
            u4DestMacAddrTblIndex = pDestMacAddrTbl->u4LlldpV2DestAddrTblIndex;
            break;
        }
        DestMacAddrTbl.u4LlldpV2DestAddrTblIndex =
            pDestMacAddrTbl->u4LlldpV2DestAddrTblIndex;
        pDestMacAddrTbl = (tLldpv2DestAddrTbl *)
            RBTreeGetNext (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree,
                           &DestMacAddrTbl, LldpDstMacAddrUtlRBCmpInfo);
    }

    if (u4DestMacAddrTblIndex == 0)
    {
        LLDP_TRC ((MGMT_TRC | ALL_FAILURE_TRC), " Destination "
                  "Address table entry not found!!!\r\n");
        return OSIX_FAILURE;
    }

    if ((pAgentMappingEntry =
         (tLldpv2AgentToLocPort *) MemAllocMemBlk (gLldpGlobalInfo.
                                                   LldpAgentToLocPortMappingPoolId))
        == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC, " Alloc mem block "
                  "FAILED for tLldpv2AgentToLocPort!!!\r\n");
        return OSIX_FAILURE;

    }

    pAgentMappingEntry->i4IfIndex = i4IfIndex;
    MEMCPY (pAgentMappingEntry->Lldpv2DestMacAddress, DestMacAddr,
            MAC_ADDR_LEN);
    pAgentMappingEntry->u4DstMacAddrTblIndex = u4DestMacAddrTblIndex;
    if (RBTreeAdd
        (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
         pAgentMappingEntry) != RB_SUCCESS)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC, "Entry already present  "
                  "FAILED for tLldpv2AgentToLocPort!!!\r\n");
        MemReleaseMemBlock (gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId,
                            (UINT1 *) pAgentMappingEntry);
        return OSIX_FAILURE;
    }
    if (RBTreeAdd
        (gLldpGlobalInfo.Lldpv2AgentMapTblRBTree,
         pAgentMappingEntry) != RB_SUCCESS)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC, "Entry already present  "
                  "FAILED for tLldpv2AgentToLocPort!!!\r\n");
        MemReleaseMemBlock (gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId,
                            (UINT1 *) pAgentMappingEntry);
        return OSIX_FAILURE;
    }

    pAgentMappingEntry->u4LldpLocPort = gu4LldpAgentNumber++;
    pPortEntry = LLDP_GET_LOC_PORT_INFO (pAgentMappingEntry->u4LldpLocPort);

    /* pPortEntry is not NULL, port with same index already exists */
    if (pPortEntry != NULL)
    {
        RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                      pAgentMappingEntry);
        RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentMapTblRBTree,
                      pAgentMappingEntry);
        MemReleaseMemBlock (gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId,
                            (UINT1 *) pAgentMappingEntry);
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "CliDestMacAddrAdd:"
                  "Port with same index already exists.Another entry cannot be created\r\n");
        return OSIX_SUCCESS;
    }

    /* Allocate memory for the new port entry */
    if ((pPortEntry =
         (tLldpLocPortInfo *) MemAllocMemBlk (gLldpGlobalInfo.
                                              LocPortInfoPoolId)) == NULL)
    {
        gu4LldpAgentNumber--;
        RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                      pAgentMappingEntry);
        RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentMapTblRBTree,
                      pAgentMappingEntry);
        MemReleaseMemBlock (gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId,
                            (UINT1 *) pAgentMappingEntry);
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "Memory allocation failed for agent\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pPortEntry, 0, sizeof (tLldpLocPortInfo));

    /* Update the interface specific paramaters obtained from lower layer. */
    pPortEntry->u4LocPortNum = pAgentMappingEntry->u4LldpLocPort;
    pPortEntry->i4IfIndex = i4IfIndex;
    MEMCPY (pPortEntry->PortConfigTable.u1DstMac, DestMacAddr, MAC_ADDR_LEN);
    pPortEntry->u4DstMacAddrTblIndex = u4DestMacAddrTblIndex;
    /* Populate port related info */
    if (LldpIfInit (pPortEntry) != OSIX_SUCCESS)
    {
        gu4LldpAgentNumber--;
        RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                      pAgentMappingEntry);
        RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentMapTblRBTree,
                      pAgentMappingEntry);

        LLDP_TRC (INIT_SHUT_TRC,
                  "CLI DestMacAddrAdd: LldpIfInit FAILED" "!!!\r\n");

        /* Release Memory allocated for holding PDU Info */
        if (pPortEntry->pPreFormedLldpdu != NULL)
        {
            MemReleaseMemBlock (gLldpGlobalInfo.LldpPduInfoPoolId,
                                (UINT1 *) (pPortEntry->pPreFormedLldpdu));
            pPortEntry->pPreFormedLldpdu = NULL;
            pPortEntry->u4PreFormedLldpduLen = 0;
        }

        /* delete the port inforamtion and release the memory */
        if (LldpTxUtlClearPortInfo (pPortEntry) != OSIX_SUCCESS)
        {
            LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                      "LldpTxUtlClearPortInfo returns FAILURE!!!\r\n");
        }
        /* release the memory allocated for port info structure */
        MemReleaseMemBlock (gLldpGlobalInfo.LocPortInfoPoolId,
                            (UINT1 *) pPortEntry);
        MemReleaseMemBlock (gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId,
                            (UINT1 *) pAgentMappingEntry);
        return OSIX_FAILURE;
    }
    if (LldpIfUpdateIfDesc (pPortEntry) != OSIX_SUCCESS)
    {
        gu4LldpAgentNumber--;
        RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                      pAgentMappingEntry);
        RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentMapTblRBTree,
                      pAgentMappingEntry);

        LLDP_TRC (ALL_FAILURE_TRC, "LldpIfCreate: LldpIfUpdateIfDesc"
                  "returns FAILURE\r\n");

        /* Release Memory allocated for holding PDU Info */
        if (pPortEntry->pPreFormedLldpdu != NULL)
        {
            MemReleaseMemBlock (gLldpGlobalInfo.LldpPduInfoPoolId,
                                (UINT1 *) (pPortEntry->pPreFormedLldpdu));
            pPortEntry->pPreFormedLldpdu = NULL;
            pPortEntry->u4PreFormedLldpduLen = 0;
        }

        /* delete port related information and releas memory */
        if (LldpTxUtlClearPortInfo (pPortEntry) != OSIX_SUCCESS)
        {
            LLDP_TRC (INIT_SHUT_TRC, "LldpIfCreate: "
                      "LldpIfUpdateIfDesc returns FAILURE!!!\r\n");
        }
        /* release the memory allocated for port info structure */
        MemReleaseMemBlock (gLldpGlobalInfo.LocPortInfoPoolId,
                            (UINT1 *) pPortEntry);
        MemReleaseMemBlock (gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId,
                            (UINT1 *) pAgentMappingEntry);
        return OSIX_FAILURE;
    }

    /* Add the local port entry to the local port table ie agent entry */
    if (LldpIfAddToPortTable (pPortEntry) != OSIX_SUCCESS)
    {
        gu4LldpAgentNumber--;
        RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                      pAgentMappingEntry);
        RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentMapTblRBTree,
                      pAgentMappingEntry);

        LLDP_TRC (INIT_SHUT_TRC, "LldpIfCreate: LldpIfInit FAILED!!!\r\n");

        /* Release Memory allocated for holding PDU Info */
        if (pPortEntry->pPreFormedLldpdu != NULL)
        {
            MemReleaseMemBlock (gLldpGlobalInfo.LldpPduInfoPoolId,
                                (UINT1 *) (pPortEntry->pPreFormedLldpdu));
            pPortEntry->pPreFormedLldpdu = NULL;
            pPortEntry->u4PreFormedLldpduLen = 0;
        }

        /* delete port related information and releas memory */
        if (LldpTxUtlClearPortInfo (pPortEntry) != OSIX_SUCCESS)
        {
            LLDP_TRC (INIT_SHUT_TRC, "LldpIfCreate: "
                      "LldpTxUtlClearPortInfo returns FAILURE!!!\r\n");
        }
        /* release the memory allocated for port info structure */
        MemReleaseMemBlock (gLldpGlobalInfo.LocPortInfoPoolId,
                            (UINT1 *) pPortEntry);
        MemReleaseMemBlock (gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId,
                            (UINT1 *) pAgentMappingEntry);
        return OSIX_FAILURE;
    }

    /* While starting LLDP module, indication has to be sent regarding
     * the interface that are made operationally up. This leads to the
     * transition of Rx state machine from wait for operational state to
     * initialise state */

    if ((LldpPortGetIfOperStatus ((UINT4) pPortEntry->i4IfIndex,
                                  &pPortEntry->u1OperStatus)
         == OSIX_SUCCESS) && (pPortEntry->u1OperStatus == CFA_IF_UP))
    {
        LldpRxSemRun (pPortEntry, LLDP_RX_EV_PORT_OPER_UP);
        /* Check LLDP is enabled or not */
        if (LLDP_MODULE_STATUS () == LLDP_ENABLED)
        {
            LldpTxTmrSmMachine (pPortEntry, LLDP_TX_TMR_EV_PORT_OPER_UP);
            LldpTxSmMachine (pPortEntry, LLDP_TX_EV_PORT_OPER_UP);
        }
    }

    return OSIX_SUCCESS;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpDeleteDestMacAddrEntry
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpDeleteDestMacAddrEntry (INT4 i4IfIndex, tMacAddr DestMacAddr)
{
    /*tLldpv2DestAddrTbl *pDestMacAddrTbl = NULL; */
    tLldpv2DestAddrTbl  DestMacAddrTbl;
    tLldpv2AgentToLocPort *pAgentMappingEntry = NULL;
    tLldpv2AgentToLocPort AgentMappingEntry;
    tLldpLocPortInfo   *pPortEntry = NULL;
    tLldpLocPortInfo    PortEntry;
    tLldpAppInfo       *pAppInfo = NULL;
    tLldpAppInfo        AppInfo;

    MEMSET (&DestMacAddrTbl, 0, sizeof (tLldpv2DestAddrTbl));
    MEMSET (&AgentMappingEntry, 0, sizeof (tLldpv2AgentToLocPort));
    MEMSET (&PortEntry, 0, sizeof (tLldpLocPortInfo));
    MEMSET (&AppInfo, 0, sizeof (tLldpAppInfo));

    AgentMappingEntry.i4IfIndex = i4IfIndex;
    MEMCPY (AgentMappingEntry.Lldpv2DestMacAddress, DestMacAddr, MAC_ADDR_LEN);

    pAgentMappingEntry =
        (tLldpv2AgentToLocPort *) RBTreeGet (gLldpGlobalInfo.
                                             Lldpv2AgentToLocPortMapTblRBTree,
                                             (tRBElem *) & AgentMappingEntry);

    if (pAgentMappingEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    PortEntry.u4LocPortNum = pAgentMappingEntry->u4LldpLocPort;

    pPortEntry =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & PortEntry);

    if (pPortEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    /* Stop message interval timer */
    if (pPortEntry->TtrTmrNode.u1Status == LLDP_TMR_RUNNING)
    {
        if (TmrStopTimer (gLldpGlobalInfo.TmrListId,
                          &(pPortEntry->TtrTmr.TimerNode)) != TMR_SUCCESS)
        {
            LLDP_TRC_ARG1 (OS_RESOURCE_TRC | ALL_FAILURE_TRC
                           | CONTROL_PLANE_TRC,
                           "LldpDeleteDestMacAddrEntry: port %d "
                           "TmrStopTimer(TtrTmr) returns FAILURE!!!\r\n",
                           pPortEntry->u4LocPortNum);
        }
        /* set the transmit interval timer status as NOT_RUNNING */
        pPortEntry->TtrTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;
    }
    /* stop transmit delay timer */
    if (pPortEntry->TxDelayTmrNode.u1Status == LLDP_TMR_RUNNING)
    {
        if (TmrStopTimer (gLldpGlobalInfo.TmrListId,
                          &(pPortEntry->TxDelayTmr.TimerNode)) != TMR_SUCCESS)
        {
            LLDP_TRC_ARG1 (OS_RESOURCE_TRC | ALL_FAILURE_TRC
                           | CONTROL_PLANE_TRC,
                           "LldpDeleteDestMacAddrEntry: port %d "
                           "TmrStopTimer(TxDelayTmr) returns FAILURE!!!\r\n",
                           pPortEntry->u4LocPortNum);
        }
        /* set the transmit delay timer status as NOT_RUNNING */
        pPortEntry->TxDelayTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;
    }
    /* stop shutdown while timer */
    if (pPortEntry->ShutWhileTmrNode.u1Status == LLDP_TMR_RUNNING)
    {
        if (TmrStopTimer (gLldpGlobalInfo.TmrListId,
                          &(pPortEntry->ShutWhileTmr.TimerNode)) != TMR_SUCCESS)
        {
            LLDP_TRC_ARG1 (OS_RESOURCE_TRC | ALL_FAILURE_TRC
                           | CONTROL_PLANE_TRC,
                           "LldpDeleteDestMacAddrEntry: port %d"
                           "TmrStopTimer(ShutWhileTmr) returns FAILURE!!!\r\n",
                           pPortEntry->u4LocPortNum);
        }
        /* set the shutdown while timer status as NOT_RUNNING */
        pPortEntry->ShutWhileTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;
    }

    /* Delete all applications registered to this agent */
    pAppInfo = (tLldpAppInfo *) RBTreeGetFirst (gLldpGlobalInfo.AppRBTree);
    while (pAppInfo != NULL)
    {
        AppInfo.u4PortId = pAppInfo->u4PortId;
        AppInfo.u4LldpInstSelector = pAppInfo->u4LldpInstSelector;
        MEMCPY (&AppInfo.LldpAppId, &(pAppInfo->LldpAppId),
                sizeof (AppInfo.LldpAppId));
        if ((pAppInfo->u4PortId == (UINT4) pPortEntry->i4IfIndex)
            && (pAppInfo->u4LldpInstSelector ==
                pPortEntry->u4DstMacAddrTblIndex))
        {
            RBTreeRemove (gLldpGlobalInfo.AppRBTree, pAppInfo);
        }
        pAppInfo = (tLldpAppInfo *) RBTreeGetNext (gLldpGlobalInfo.AppRBTree,
                                                   (tRBElem *) & AppInfo, NULL);
    }

    /* Delete all the Remote Node associated with current port. This function
     * takes care of deleting rxinfo age timer if running */
    LldpRxUtlDelRemInfoForPort (pPortEntry);

    RBTreeRemove (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree, pPortEntry);
    RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                  pAgentMappingEntry);
    RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentMapTblRBTree, pAgentMappingEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtilGetDestMacAddrTblIndex
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtilGetDestMacAddrTblIndex (UINT4 u4LocPortNum,
                                UINT4 *pu4DestMacAddrTblIndex)
{
    tLldpLocPortInfo    LldpLocPortInfo;
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));

    LldpLocPortInfo.u4LocPortNum = u4LocPortNum;
    pLldpLocPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        &LldpLocPortInfo);

    if (pLldpLocPortInfo == NULL)
    {
        return OSIX_FAILURE;
    }
    *pu4DestMacAddrTblIndex = pLldpLocPortInfo->u4DstMacAddrTblIndex;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtilCreateDestMacAddrTable
 *
 *    DESCRIPTION      : NONE
 *
 *    INPUT            : None
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
PUBLIC VOID
LldpUtilCreateDestMacAddrTable (VOID)
{
    tLldpv2DestAddrTbl *pDestMacAddrTbl = NULL;

    pDestMacAddrTbl =
        (tLldpv2DestAddrTbl
         *) (MemAllocMemBlk (gLldpGlobalInfo.LldpDestMacAddrPoolId));
    if (pDestMacAddrTbl == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Memory allocation failed for Destination Mac address table  \r\n");
        return;
    }
    MEMSET (pDestMacAddrTbl, 0, sizeof (tLldpv2DestAddrTbl));
    MEMCPY (pDestMacAddrTbl->Lldpv2DestMacAddress, gau1LldpMcastAddr,
            MAC_ADDR_LEN);
    if (gu4LldpDestMacAddrTblIndex < LLDP_MAX_DEST_ADDR_TBL_INDEX)
    {
        pDestMacAddrTbl->u4LlldpV2DestAddrTblIndex = gu4LldpDestMacAddrTblIndex;
        gu4LldpDestMacAddrTblIndex++;
    }
    if (RBTreeAdd (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree,
                   (tRBElem *) & (pDestMacAddrTbl->DestMacAddrTblNode)) !=
        RB_SUCCESS)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " RBTree Add failed for Destination Mac address table  \r\n");
    }
    return;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddCredit
 *
 *    DESCRIPTION      : State machine function to add the txCredit
 *                       function
 *
 *
 *    INPUT            : pLldpLocPortInfo - pointer to the LldpLocPortInfo
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PUBLIC VOID
LldpTxUtlAddCredit (tLldpLocPortInfo * pPortInfo)
{
    if (pPortInfo->u1Credit < gLldpGlobalInfo.SysConfigInfo.u4TxCreditMax)
    {
        pPortInfo->u1Credit++;
    }
    return;
}

PUBLIC UINT1
LldpUtlCheckDestMacAddrIndexUsage (UINT4 u4DestMacAddrIndex)
{
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    tLldpLocPortInfo    LldpLocPortInfo;

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));

    pLldpLocPortInfo = (tLldpLocPortInfo *)
        RBTreeGetFirst (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree);
    while (pLldpLocPortInfo != NULL)
    {
        if (pLldpLocPortInfo->u4DstMacAddrTblIndex == u4DestMacAddrIndex)
        {
            return OSIX_TRUE;
        }
        LldpLocPortInfo.u4LocPortNum = pLldpLocPortInfo->u4LocPortNum;
        pLldpLocPortInfo = (tLldpLocPortInfo *)
            RBTreeGetNext (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
                           &LldpLocPortInfo, LldpAgentInfoUtlRBCmpInfo);
    }
    return OSIX_FALSE;

}

UINT4
LldpV1DestMacAddrIndex (INT4 i4IfIndex)
{
    UINT4               u4DestIndex = 0;
    tLldpLocPortInfo   *pLldpLocPort = NULL;
    tLldpv2AgentToLocPort *pAgentToLocPort = NULL;
    tLldpv2AgentToLocPort AgentToLocPort;

    MEMSET (&AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
    AgentToLocPort.i4IfIndex = i4IfIndex;

    pAgentToLocPort = (tLldpv2AgentToLocPort *)
        RBTreeGetNext (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                       &AgentToLocPort, LldpAgentToLocPortUtlRBCmpInfo);

    if ((pAgentToLocPort != NULL) && (pAgentToLocPort->i4IfIndex == i4IfIndex))
    {
        pLldpLocPort = LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);
        if (pLldpLocPort != NULL)
        {
            return pLldpLocPort->u4DstMacAddrTblIndex;
        }
    }
    return u4DestIndex;
}

UINT4
LldpV1LocPortNum (INT4 i4IfIndex)
{
    UINT4               u4LocPortNum = 0;
    tLldpLocPortInfo   *pLldpLocPort = NULL;
    tLldpv2AgentToLocPort *pAgentToLocPort = NULL;
    tLldpv2AgentToLocPort AgentToLocPort;

    MEMSET (&AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
    AgentToLocPort.i4IfIndex = i4IfIndex;

    pAgentToLocPort = (tLldpv2AgentToLocPort *)
        RBTreeGetNext (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                       &AgentToLocPort, LldpAgentToLocPortUtlRBCmpInfo);

    if ((pAgentToLocPort != NULL) && (pAgentToLocPort->i4IfIndex == i4IfIndex))
    {
        pLldpLocPort = LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);
        if (pLldpLocPort != NULL)
        {
            return pLldpLocPort->u4LocPortNum;
        }
    }
    return u4LocPortNum;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   LldpUtilIncMsrForFsScalars                        */
/*                                                                         */
/*     Description   :   This function sends notifications to MSR for the  */
/*                       Lldp Scalars                                      */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pu4ObjectId    - OID                             */
/*                        u4OidLen       - Length of the OID               */
/*                        u4SetVal        - Configured value that has to   */
/*                                          be notified                    */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     None                                            */
/*                                                                         */
/***************************************************************************/
VOID
LldpUtilIndMsrForFsScalars (UINT4 *pu4ObjectId, UINT4 u4OidLen, UINT4 u4SetVal)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = LLDP_ZERO;

    UNUSED_PARAM (u4OidLen);
    MEMSET (&SnmpNotifyInfo, LLDP_ZERO, sizeof (tSnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, pu4ObjectId,
                          u4SeqNum,
                          FALSE, LldpLock, LldpUnLock, LLDP_ZERO, SNMP_SUCCESS);
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u", u4SetVal));
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   LldpUtilUptInfoForStdPortConfigTbl                */
/*                                                                         */
/*     Description   :   This function Updates the SNMP struture for       */
/*                       sending Trigger to MSR corresponding to the Port  */
/*                       Config Table                                      */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pu4ObjectId    - OID                             */
/*                        u4OidLen       - Length of the OID               */
/*                        i4Index        - Interface Index where the entry */
/*                                  is added                        */
/*                        pSnmpNotifyInfo - Configured value that has to   */
/*                                          be notified                    */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     None                                            */
/*                                                                         */
/***************************************************************************/
VOID
LldpUtilUptInfoForStdPortConfigTbl (UINT4 *pu4ObjectId, UINT4 u4OidLen,
                                    tSnmpNotifyInfo * pSnmpNotifyInfo)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = LLDP_ZERO;

    MEMSET (&SnmpNotifyInfo, LLDP_ZERO, sizeof (tSnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);
    MEMCPY (&SnmpNotifyInfo, pSnmpNotifyInfo, sizeof (tSnmpNotifyInfo));
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, pu4ObjectId,
                          u4SeqNum,
                          FALSE, LldpLock, LldpUnLock, LLDP_TWO, SNMP_SUCCESS);
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    MEMCPY (pSnmpNotifyInfo, &SnmpNotifyInfo, sizeof (tSnmpNotifyInfo));
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   LldpUtilIndMsrForStdManAddrTxTbl                  */
/*                                                                         */
/*     Description   :   This function Send Trigger for MSR for Man Address*/
/*                       Port Tx Table                                     */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :   i4LldpLocManAddrSubtype                           */
/*                       pLldpLocManAddr                                   */
/*                       pSetValLldpConfigManAddrPortsTxEnable             */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     None                                            */
/*                                                                         */
/***************************************************************************/
VOID
LldpUtilIndMsrForStdManAddrTxTbl (INT4 i4LldpLocManAddrSubtype,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pLldpLocManAddr,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValLldpConfigManAddrPortsTxEnable)
{
    tLldpLocPortInfo   *pLocPortInfo = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = LLDP_ZERO;
    INT4                i4SetVal = LLDP_ZERO;
    UINT1               u1Result = LLDP_ZERO;
    UINT4               u4LocPort = LLDP_ZERO;

    MEMSET (&SnmpNotifyInfo, LLDP_ZERO, sizeof (tSnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);
    for (u4LocPort = 1; u4LocPort <= LLDP_MAX_LEN_MAN_ADDR * BITS_PER_BYTE;
         u4LocPort++)
    {
        OSIX_BITLIST_IS_BIT_SET (pSetValLldpConfigManAddrPortsTxEnable->
                                 pu1_OctetList, u4LocPort,
                                 MEM_MAX_BYTES (LLDP_MAN_ADDR_TX_EN_MAX_BYTES,
                                                pSetValLldpConfigManAddrPortsTxEnable->
                                                i4_Length), u1Result);
        if (u1Result == OSIX_TRUE)
        {
            /*Get the local port information to take the ifindex */
            pLocPortInfo = LldpGetLocPortInfo (u4LocPort);
            if (pLocPortInfo == NULL)
            {
                LLDP_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC | LLDP_CRITICAL_TRC,
                          "LldpUtilIndMsrForStdManAddrTxTbl: No Entry is found for the local port\r\n");
                return;
            }

            /*Setting the RowStatus of ManAddressPortTx Table as CREATE and GO */
            i4SetVal = LLDP_CREATE_AND_GO;
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, LldpV2ManAddrConfigRowStatus,
                                  u4SeqNum,
                                  TRUE, LldpLock, LldpUnLock,
                                  LLDP_FOUR, SNMP_SUCCESS);

            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i %s %i",
                              pLocPortInfo->i4IfIndex,
                              DEFAULT_DEST_MAC_ADDR_INDEX,
                              i4LldpLocManAddrSubtype, pLldpLocManAddr,
                              i4SetVal));

            /*Setting the Transmisson enable Flag of ManAddressPortTx Table */
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, LldpV2ManAddrConfigTxEnable,
                                  u4SeqNum,
                                  FALSE, LldpLock, LldpUnLock,
                                  LLDP_FOUR, SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i %s %i",
                              pLocPortInfo->i4IfIndex,
                              DEFAULT_DEST_MAC_ADDR_INDEX,
                              i4LldpLocManAddrSubtype, pLldpLocManAddr,
                              u1Result));
            i4SetVal = LLDP_ACTIVE;
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, LldpV2ManAddrConfigRowStatus,
                                  u4SeqNum,
                                  TRUE, LldpLock, LldpUnLock,
                                  LLDP_FOUR, SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i %s %i",
                              pLocPortInfo->i4IfIndex,
                              DEFAULT_DEST_MAC_ADDR_INDEX,
                              i4LldpLocManAddrSubtype, pLldpLocManAddr,
                              i4SetVal));

        }
    }

    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   LldpUtilIndMsrForDot1ConfigPortVlanTbl            */
/*                                                                         */
/*     Description   :   This function Send Trigger for MSR for            */
/*                       Dot1ConfigPortVlan Table                          */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pu4ObjectId    - OID                             */
/*                        u4OidLen       - Length of the OID               */
/*                        i4Index        - Interface Index where the entry */
/*                                  is added                               */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     None                                            */
/*                                                                         */
/***************************************************************************/
VOID
LldpUtilIndMsrForDot1ConfigPortVlanTbl (UINT4 *pu4ObjectId, UINT4 u4OidLen,
                                        INT4 i4Index, INT4 i4SetVal)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = LLDP_ZERO;

    MEMSET (&SnmpNotifyInfo, LLDP_ZERO, sizeof (tSnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = LldpLock;
    SnmpNotifyInfo.pUnLockPointer = LldpUnLock;
    SnmpNotifyInfo.u4Indices = LLDP_TWO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", i4Index,
                      DEFAULT_DEST_MAC_ADDR_INDEX, i4SetVal));
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   LldpUtilIndMsrForDot1ConfigVlanNameTbl            */
/*                                                                         */
/*     Description   :   This function Send Trigger for MSR for            */
/*                       Dot1ConfigPortVlan Table                          */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pu4ObjectId    - OID                             */
/*                        u4OidLen       - Length of the OID               */
/*                        i4Index        - Interface Index where the entry */
/*                                         is added                        */
/*                        VlanId         - Vlan Id is Secondary Index      */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     None                                            */
/*                                                                         */
/***************************************************************************/
VOID
LldpUtilIndMsrForDot1ConfigVlanNameTbl (UINT4 *pu4ObjectId, UINT4 u4OidLen,
                                        INT4 i4Index, INT4 VlanId,
                                        INT4 i4SetVal)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = LLDP_ZERO;

    MEMSET (&SnmpNotifyInfo, LLDP_ZERO, sizeof (tSnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = LldpLock;
    SnmpNotifyInfo.pUnLockPointer = LldpUnLock;
    SnmpNotifyInfo.u4Indices = LLDP_TWO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i", i4Index, VlanId, i4SetVal));
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   LldpUtilIndMsrForDot1ConfigProtoVlanTbl           */
/*                                                                         */
/*     Description   :   This function Send Trigger for MSR for            */
/*                       Dot1ConfigProtoVlan Table                         */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pu4ObjectId    - OID                             */
/*                        u4OidLen       - Length of the OID               */
/*                        i4Index        - Interface Index where the entry */
/*                                         is added                        */
/*                        VlanId         - Vlan Id is Secondary Index      */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     None                                            */
/*                                                                         */
/***************************************************************************/
VOID
LldpUtilIndMsrForDot1ConfigProtoVlanTbl (UINT4 *pu4ObjectId, UINT4 u4OidLen,
                                         INT4 i4Index, INT4 VlanId,
                                         INT4 i4SetVal)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = LLDP_ZERO;

    MEMSET (&SnmpNotifyInfo, LLDP_ZERO, sizeof (tSnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = LldpLock;
    SnmpNotifyInfo.pUnLockPointer = LldpUnLock;
    SnmpNotifyInfo.u4Indices = LLDP_TWO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i", i4Index, VlanId, i4SetVal));
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   LldpUtilIndMsrForDot3PortConfigTbl                */
/*                                                                         */
/*     Description   :   This function Send Trigger for MSR for            */
/*                       Dot3PortConfig Table                              */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pu4ObjectId    - OID                             */
/*                        u4OidLen       - Length of the OID               */
/*                        i4Index        - Interface Index where the entry */
/*                                  is added                               */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     None                                            */
/*                                                                         */
/***************************************************************************/
VOID
LldpUtilIndMsrForDot3PortConfigTbl (UINT4 *pu4ObjectId, UINT4 u4OidLen,
                                    INT4 i4Index,
                                    tSNMP_OCTET_STRING_TYPE * pSetVal)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = LLDP_ZERO;

    MEMSET (&SnmpNotifyInfo, LLDP_ZERO, sizeof (tSnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = LldpLock;
    SnmpNotifyInfo.pUnLockPointer = LldpUnLock;
    SnmpNotifyInfo.u4Indices = LLDP_TWO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %s", i4Index,
                      DEFAULT_DEST_MAC_ADDR_INDEX, pSetVal));
    return;
}

INT4
LldpUtlCalcCRC32 (UINT1 *pu1Input, UINT4 *pu4Result)
{
    /* This function should be updated in util when an application
     *     for VID usage digest is identified */
    UNUSED_PARAM (pu1Input);
    *pu4Result = 0;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpGetNextConfigPortMapNode
 *
 *    DESCRIPTION      : This table will return the Next Entry of Port Map Table
 *
 *
 *    INPUT            : pLldpAgentPortInfo - pointer to the Agent to local port
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : tLldpv2AgentToLocPort structure
 *
 ****************************************************************************/
tLldpv2AgentToLocPort *
LldpGetNextConfigPortMapNode (tLldpv2AgentToLocPort * pLldpAgentPortEntry)
{

    tLldpv2AgentToLocPort LldpAgentPortInfo;
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;

    MEMSET (&LldpAgentPortInfo, 0, sizeof (tLldpv2AgentToLocPort));

    LldpAgentPortInfo.i4IfIndex = pLldpAgentPortEntry->i4IfIndex;
    MEMCPY (LldpAgentPortInfo.Lldpv2DestMacAddress,
            pLldpAgentPortEntry->Lldpv2DestMacAddress, MAC_ADDR_LEN);

    pLldpAgentPortInfo = (tLldpv2AgentToLocPort *) RBTreeGetNext
        (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
         (tRBElem *) & LldpAgentPortInfo, LldpAgentToLocPortUtlRBCmpInfo);

    return pLldpAgentPortInfo;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpGetPortMapTableNode
 *
 *    DESCRIPTION      : This function  will take ifIndex and  MAC address
 *                       and will return pointer to tLldpv2AgentToLocPort
 *                       structure.
 *
 *
 *    INPUT            : u4IfIndex - Interface index
 *                       pMacAddr - pointer to MAC address.
 *
 *    OUTPUT           : pointer to tLldpv2AgentToLocPort structure.
 *
 *    RETURNS          : tLldpv2AgentToLocPort structure.
 *
 ****************************************************************************/

tLldpv2AgentToLocPort *
LldpGetPortMapTableNode (UINT4 u4IfIndex, tMacAddr * pMacAddr)
{

    tLldpv2AgentToLocPort LldpAgentPortInfo;
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;

    MEMSET (&LldpAgentPortInfo, 0, sizeof (tLldpv2AgentToLocPort));

    LldpAgentPortInfo.i4IfIndex = (INT4) u4IfIndex;
    MEMCPY (LldpAgentPortInfo.Lldpv2DestMacAddress, pMacAddr, MAC_ADDR_LEN);

    pLldpAgentPortInfo = (tLldpv2AgentToLocPort *) RBTreeGet
        (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
         (tRBElem *) & LldpAgentPortInfo);

    return pLldpAgentPortInfo;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpGetMacFromLldpLocalPort
 *
 *    DESCRIPTION      : This function will retur MAC adress from local port number.
 *
 *
 *    INPUT            : u4LldpLocPort - local port number
 *                        pMacAddr - pointer to mac address.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpGetMacFromLldpLocalPort (UINT4 u4LldpLocPort, tMacAddr * pMacAddr)
{

    tLldpLocPortInfo   *pLldpLocPort = NULL;
    tLldpv2DestAddrTbl  DestMacAddrTbl;
    tLldpv2DestAddrTbl *pDestMacAddrTbl = NULL;

    pLldpLocPort = LLDP_GET_LOC_PORT_INFO (u4LldpLocPort);
    if (pLldpLocPort != NULL)
    {
        MEMSET (&DestMacAddrTbl, 0, sizeof (tLldpv2DestAddrTbl));
        DestMacAddrTbl.u4LlldpV2DestAddrTblIndex =
            pLldpLocPort->u4DstMacAddrTblIndex;

        pDestMacAddrTbl =
            (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                              Lldpv2DestMacAddrTblRBTree,
                                              &DestMacAddrTbl);
        if (pDestMacAddrTbl != NULL)
        {
            MEMSET (pMacAddr, 0, MAC_ADDR_LEN);
            MEMCPY (pMacAddr, pDestMacAddrTbl->Lldpv2DestMacAddress,
                    MAC_ADDR_LEN);
            return OSIX_SUCCESS;
        }

    }

    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpGetDestAddrTblIndexFromMacAddr
 *
 *    DESCRIPTION      : This will return destination mac if index from MAC address.
 *
 *
 *    INPUT            : pMacAddr - pointer to mac address.
 *                       pu4DstMacAddrTblIndex - pointer to destination ifIndex.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpGetDestAddrTblIndexFromMacAddr (tMacAddr * pMacAddr,
                                    UINT4 *pu4DstMacAddrTblIndex)
{

    tLldpv2DestAddrTbl  DestMacAddrTbl;
    tLldpv2DestAddrTbl *pDestMacAddrTbl = NULL;

    *pu4DstMacAddrTblIndex = 0;
    MEMSET (&DestMacAddrTbl, 0, sizeof (tLldpv2DestAddrTbl));

    while ((pDestMacAddrTbl = (tLldpv2DestAddrTbl *) RBTreeGetNext
            (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree,
             (tRBElem *) & DestMacAddrTbl, LldpDstMacAddrUtlRBCmpInfo)) != NULL)
    {
        if (MEMCMP
            (pMacAddr, pDestMacAddrTbl->Lldpv2DestMacAddress,
             MAC_ADDR_LEN) == 0)
        {
            break;
        }
        MEMCPY (&DestMacAddrTbl, pDestMacAddrTbl, sizeof (tLldpv2DestAddrTbl));
    }

    if (pDestMacAddrTbl != NULL)
    {
        *pu4DstMacAddrTblIndex = pDestMacAddrTbl->u4LlldpV2DestAddrTblIndex;
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpGetDestAddrTblIndexFromMacAddr
 *
 *    DESCRIPTION      : This function will return mac address from dest. ifIndex.
 *
 *
 *    INPUT            : u4DstMacAddrTblIndex - destination mac if index
 *                       pMacAddr - pointer to mac address.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpGetMacAddrFromDestAddrTblIndex (UINT4 u4DstMacAddrTblIndex,
                                    tMacAddr * pMacAddr)
{

    tLldpv2DestAddrTbl  DestMacAddrTbl;
    tLldpv2DestAddrTbl *pDestMacAddrTbl = NULL;

    MEMSET (&DestMacAddrTbl, 0, sizeof (tLldpv2DestAddrTbl));

    DestMacAddrTbl.u4LlldpV2DestAddrTblIndex = u4DstMacAddrTblIndex;

    pDestMacAddrTbl = (tLldpv2DestAddrTbl *) RBTreeGet
        (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree, &DestMacAddrTbl);

    if (pDestMacAddrTbl != NULL)
    {
        MEMCPY (pMacAddr, pDestMacAddrTbl->Lldpv2DestMacAddress, MAC_ADDR_LEN);
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpGetLocalPortFromIfIndexDestMacIfIndex
 *
 *    DESCRIPTION      : This function will return Local port number for ifIndex
 *                       and destination Mac index
 *
 *
 *    INPUT            : u4IfIndex - IfIndex
 *                       u4DstMacAddrTblIndex - Destination Mac adress ifIndex.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : Local port Number/Zero
 *
 ****************************************************************************/
UINT4
LldpGetLocalPortFromIfIndexDestMacIfIndex (UINT4 u4IfIndex,
                                           UINT4 u4DstMacAddrTblIndex)
{
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;
    tMacAddr            MacAddr;

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    if (LldpGetMacAddrFromDestAddrTblIndex (u4DstMacAddrTblIndex,
                                            &MacAddr) != OSIX_SUCCESS)
    {
        return 0;
    }

    pLldpAgentPortInfo = LldpGetPortMapTableNode (u4IfIndex, &MacAddr);

    if (pLldpAgentPortInfo == NULL)
    {
        return 0;
    }

    return pLldpAgentPortInfo->u4LldpLocPort;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlRBFreeAgentTable
 *
 *    DESCRIPTION      : Frees the memory pool for application RB Tree 
 *                       PortTableRBTree
 *
 *    INPUT            : pRBElem - RB Tree Pointer 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          :
 *
 ****************************************************************************/
INT4
LldpUtlRBFreeAgentTable (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.LocPortInfoPoolId,
                            (UINT1 *) pRBElem);
        pRBElem = NULL;

    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlRBFreePortTable
 *
 *    DESCRIPTION      : Frees the memory pool for application RB Tree 
 *                       PortTableRBTree
 *
 *    INPUT            : pRBElem - RB Tree Pointer 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          :
 *
 ****************************************************************************/
INT4
LldpUtlRBFreePortTable (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                            (UINT1 *) pRBElem);
        pRBElem = NULL;

    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlRBFreeDestAddrTable
 
 *    DESCRIPTION      : Frees the memory pool for application RB Tree 
 *                       DestAddrTableRBTree
 *
 *    INPUT            : pRBElem - RB Tree Pointer 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          :
 *
 ****************************************************************************/
INT4
LldpUtlRBFreeDestAddrTable (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.LldpDestMacAddrPoolId,
                            (UINT1 *) pRBElem);
        pRBElem = NULL;

    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlRBFreeAgentToLocPortTable 
 
 *    DESCRIPTION      : Frees the memory pool for application RB Tree 
 *                       DestAddrTableRBTree
 *
 *    INPUT            : pRBElem - RB Tree Pointer 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          :
 *
 ****************************************************************************/
INT4
LldpUtlRBFreeAgentToLocPortTable (tRBElem * pRBElem, UINT4 u4Arg)
{
    /* This Free Fn is intentionally left blank */
    /* As this Agent Map Node is maintained in both the RBTrees -
     * Lldpv2AgentToLocPortMapTblRBTree and Lldpv2AgentMapTblRBTree
     * so memory for this Agent Map node cannot be removed here.
     * Procedure to Release a Remote Node Memory is as follows -
     * STEP 1: Detach the Agent Map Node from Lldpv2AgentToLocPortMapTblRBTree.
     * STEP 2: Detach the Agent Map Node from Lldpv2AgentMapTblRBTree.
     * STEP 3: Release the memory of Agent Map Node.
     */
    UNUSED_PARAM (u4Arg);
    UNUSED_PARAM (pRBElem);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlRBFreeAgentMapTable 
 
 *    DESCRIPTION      : Frees the memory pool for application RB Tree 
 *                       DestAddrTableRBTree
 *
 *    INPUT            : pRBElem - RB Tree Pointer 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          :
 *
 ****************************************************************************/
INT4
LldpUtlRBFreeAgentMapTable (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId,
                            (UINT1 *) pRBElem);
        pRBElem = NULL;

    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetDupVlanNameCount
 
 *    DESCRIPTION      : This function calculates the number of duplicate 
 *                       VLAN Name TLVs present in the received LLDPDU.
 *                       This function is written explicitly for the purpose
 *                       of incrementing the error counters during parsing of
 *                       the refresh frame which contains VLAN Name TLVs in its 
 *                       LLDPDU.
 *
 *    INPUT            : pu1Lldpdu - Pointer to the received LLDPDU 
 *                       u2LldpduLen - Length of the received LLDPDU
 *
 *    OUTPUT           : pu4DupVlanNameCount - The number of duplicate VLAN Name
 *                       TLVs.
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpUtlGetDupVlanNameCount (UINT1 *pu1Lldpdu, UINT2 u2LldpduLen,
                            UINT4 *pu4DupVlanNameCount)
{
    UINT4               u4ErrorCount = 0;
    UINT4               u4Offset = 0;
    UINT2               u2TlvType = 0;
    UINT2               u2TlvLength = 0;
    UINT2               u2ProcPduLen = 0;
    UINT1               u1TlvSubType = 0;
    UINT1              *pu1TlvInfo = NULL;
    UINT1               au1RxOUI[LLDP_MAX_LEN_OUI] = { 0 };
    tLldpxdot1RemVlanNameInfo VlanNameInfo;
    tLldpVlanNameTlvCounter *pVlanNameTlvCounter = NULL;
    UINT1              *pu1Tlv = NULL;

    MEMSET (&VlanNameInfo, 0, sizeof (tLldpxdot1RemVlanNameInfo));

    if (NULL == pu1Lldpdu)
    {
        /* No LLDP Pdu found for processing */
        LLDP_TRC (ALL_FAILURE_TRC,
                  "LldpUtlGetDupVlanNameCount:  No LLDP Pdu found for"
                  "processing\r\n");
        return OSIX_FAILURE;
    }
    /*Allocate memory for VLAN Name TLV counter */
    if ((pVlanNameTlvCounter = (tLldpVlanNameTlvCounter *)
         (MemAllocMemBlk (gLldpGlobalInfo.LldpVlanNameTlvCounter))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_PORT_DESC_TRC,
                  "LldpUtlGetDupVlanNameCount: Failed to Allocate Memory "
                  "for the duplicate VLAN Name TLV counter \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }
    pu1Tlv = pu1Lldpdu;

    MEMSET (pVlanNameTlvCounter, 0, sizeof (tLldpVlanNameTlvCounter));

    do
    {
        /* Get the New TLV Type from the TLV */
        LLDP_GET_TLV_TYPE_LENGTH (pu1Lldpdu, &u2TlvType, &u2TlvLength);

        u2ProcPduLen = (UINT2) ((pu1Lldpdu - pu1Tlv) + LLDP_TLV_HEADER_LEN +
                                u2TlvLength);
        /* Check the LLDPDU Physical Boundary */
        if (u2ProcPduLen > (UINT2) LLDP_MAX_PDU_SIZE)
        {
            break;
        }

        /* Check if the processed PDU length is more than the rcvd PDU
         * length. */
        if (u2ProcPduLen > u2LldpduLen)
        {
            break;
        }

        if (u2TlvType == LLDP_ORG_SPEC_TLV)
        {
            /* Move the pointer to TLV info */
            pu1TlvInfo = pu1Lldpdu + LLDP_TLV_HEADER_LEN;

            /* Get the OUI */
            LLDP_LBUF_GET_STRING (pu1TlvInfo, au1RxOUI, u4Offset,
                                  LLDP_MAX_LEN_OUI);

            /* Get the Tlv Subtype */
            LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, u4Offset, u1TlvSubType);

            /* Check if the TLV is 802.1 organizationally specific TLV */
            if (MEMCMP (au1RxOUI, gau1LldpDot1OUI, LLDP_MAX_LEN_OUI) == 0)
            {
                if (u1TlvSubType == LLDP_VLAN_NAME_TLV)
                {
                    /* We have to process the 802.1 VLAN Name TLV, only if it
                     * passes in the basic validation; otherwise the counters
                     * statTLVsDiscarded would be incremented twice */
                    if (LldpDot1TlvProcVlanNameTlv (pu1TlvInfo, u2TlvLength) ==
                        OSIX_SUCCESS)
                    {
                        /* Decode the Tlv Information */
                        LldpDot1TlvDecodeVlanNameInfo (pu1TlvInfo,
                                                       &VlanNameInfo);

                        if (VlanNameInfo.u2VlanId > 0)
                        {
                            pVlanNameTlvCounter->
                                au1VlanNameTlvCount[VlanNameInfo.u2VlanId - 1] =
                                (UINT1) (pVlanNameTlvCounter->
                                         au1VlanNameTlvCount[VlanNameInfo.
                                                             u2VlanId - 1] + 1);

                            if (pVlanNameTlvCounter->au1VlanNameTlvCount
                                [VlanNameInfo.u2VlanId - 1] > 1)
                            {
                                u4ErrorCount += 1;
                            }
                        }
                    }
                    else
                    {
                        /* If the TLV fails in basic validation then the entire
                         * LLDPDU will be discarded and the error counters will
                         * be incremented in the place where the LLDPDU processing
                         * initiated; so no need to process further */
                        break;
                    }
                }
            }
        }
        MEMSET (&VlanNameInfo, 0, sizeof (tLldpxdot1RemVlanNameInfo));
        pu1Lldpdu = pu1Lldpdu + LLDP_TLV_HEADER_LEN + u2TlvLength;
    }
    while (u2TlvType != LLDP_END_OF_LLDPDU_TLV);

    *pu4DupVlanNameCount = u4ErrorCount;

    /* Release the memory allocated for the VLAN Name TLV counter */
    MemReleaseMemBlock (gLldpGlobalInfo.LldpVlanNameTlvCounter,
                        (UINT1 *) pVlanNameTlvCounter);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetDupMgmtAddrCount
 
 *    DESCRIPTION      : This function calculates the number of duplicate 
 *                       Mgmt. Addr. TLVs present in the received LLDPDU.
 *                       This function is written explicitly for the purpose
 *                       of incrementing the error counters during parsing of
 *                       the refresh frame which contains Mgmt. Add. TLVs in 
 *                       its LLDPDU.
 *
 *    INPUT            : pu1Lldpdu - Pointer to the received LLDPDU 
 *
 *    OUTPUT           : pi4DupMgmtAddrCount - The number of duplicate 
 *                       Management Address TLVs.
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpUtlGetDupMgmtAddrCount (tLldpLocPortInfo * pLocPortInfo,
                            INT4 *pi4DupMgmtAddrCount)
{
    INT4                i4ErrorCount = 0;
    UINT2               u2TlvType = 0;
    UINT2               u2TlvLength = 0;
    UINT2               u2ProcPduLen = 0;
    UINT1              *pu1TlvInfo = NULL;
    UINT1              *pu1Tlv = NULL;
    UINT1               u1ManAddrStrLength = 0;
    UINT1               au1ManAddrInfo[LLDP_DUP_MGMT_TLV_CNT]
        [LLDP_MAX_LEN_MAN_ADDR + LLDP_SUBTYPE_LEN];
    INT1                i1ManAddrIndex = 0;
    INT1                i1Index = 0;
    BOOL1               bFlag = OSIX_FALSE;

    if (NULL == pLocPortInfo)
    {
        /* No LLDP Pdu found for processing */
        LLDP_TRC (ALL_FAILURE_TRC,
                  "LldpUtlGetDupMgmtAddrCount:  No Agent information found"
                  "\r\n");
        return OSIX_FAILURE;
    }

    pu1Tlv = pLocPortInfo->pu1RxLldpdu;

    if (NULL == pu1Tlv)
    {
        /* No LLDP Pdu found for processing */
        LLDP_TRC (ALL_FAILURE_TRC, "LldpUtlGetDupMgmtAddrCount:  No LLDP"
                  "Pdu found for processing\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (au1ManAddrInfo, 0, sizeof (au1ManAddrInfo));

    do
    {
        /* Get the New TLV Type from the TLV */
        LLDP_GET_TLV_TYPE_LENGTH (pu1Tlv, &u2TlvType, &u2TlvLength);

        u2ProcPduLen = (UINT2) ((pu1Tlv - pLocPortInfo->pu1RxLldpdu) +
                                LLDP_TLV_HEADER_LEN + u2TlvLength);
        /* Check the LLDPDU Physical Boundary */
        if (u2ProcPduLen > (UINT2) LLDP_MAX_PDU_SIZE)
        {
            break;
        }

        /* Check if the processed PDU length is more than the rcvd PDU
         * length. */
        if (u2ProcPduLen > pLocPortInfo->u2RxPduLen)
        {
            break;
        }

        if (u2TlvType == LLDP_MAN_ADDR_TLV)
        {
            /* We have to process Management address TLV, only if it 
             * passess in the basic validation; otherwise the counters 
             * statTLVsDiscarded would be incremented twice */

            if (LldpTlvProcManAddrTlv (pLocPortInfo, pu1Tlv, u2TlvLength)
                == OSIX_SUCCESS)
            {
                /* Move the pointer to TLV info */
                pu1TlvInfo = pu1Tlv + LLDP_TLV_HEADER_LEN;

                LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, 0, u1ManAddrStrLength);

                if (u1ManAddrStrLength != 0)
                {
                    MEMCPY (&(au1ManAddrInfo[i1ManAddrIndex]), pu1TlvInfo,
                            MEM_MAX_BYTES (u1ManAddrStrLength,
                                           (LLDP_MAX_LEN_MAN_ADDR +
                                            LLDP_SUBTYPE_LEN)));
                    i1ManAddrIndex++;
                    if (i1ManAddrIndex > 1
                        && i1ManAddrIndex < LLDP_DUP_MGMT_TLV_CNT)
                    {
                        for (i1Index = (INT1) (i1ManAddrIndex - 2);
                             i1Index >= 0; i1Index--)
                        {
                            if (MEMCMP (&(au1ManAddrInfo[i1ManAddrIndex - 1]),
                                        &(au1ManAddrInfo[i1Index]),
                                        (LLDP_MAX_LEN_MAN_ADDR +
                                         LLDP_SUBTYPE_LEN)) == OSIX_SUCCESS)
                            {
                                bFlag = OSIX_TRUE;
                                i4ErrorCount++;
                            }
                        }
                        if (bFlag == OSIX_TRUE)
                        {
                            bFlag = OSIX_FALSE;
                            i1ManAddrIndex--;
                            MEMSET (&(au1ManAddrInfo[i1ManAddrIndex]), 0,
                                    (LLDP_MAX_LEN_MAN_ADDR + LLDP_SUBTYPE_LEN));
                        }
                    }
                }
                else if (u1ManAddrStrLength == 0 && i1ManAddrIndex == 0)
                {
                    /* If the first Mgmt. Addr TLV contains parsing error, then the
                     * error counters would have been incremented in LldpRxProcessTlv
                     * routine, where the parsing of the first mgmt. addr TLV is done 
                     * for the first time */
                    i4ErrorCount--;
                }
                u1ManAddrStrLength = 0;
            }
            else
            {
                /* If the TLV fails in basic validation then the entire
                 * LLDPDU will be discarded and the error counters will
                 * be incremented in the place where the LLDPDU processing
                 * initiated; so no need to process further */
                break;
            }
        }
        /* Update the Next Pointer */
        pu1Tlv = pu1Tlv + LLDP_TLV_HEADER_LEN + u2TlvLength;
    }
    while (u2TlvType != LLDP_END_OF_LLDPDU_TLV);

    *pi4DupMgmtAddrCount = i4ErrorCount;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetVidUsageDigestTlvCnt
 * 
 *    DESCRIPTION      : This function counts the number of VidUsageDigest
 *                       TLV present in an LLDPDU. 
 *
 *    INPUT            : pu1Lldpdu - Pointer to the received LLDPDU 
 *                       u2LldpduLen - Length of the received LLDPDU
 *
 *    OUTPUT           : pu4VidDigestTlvCount - The number of VID Usage Digest
 *                       TLVs present in the LLDPDU.
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpUtlGetVidUsageDigestTlvCnt (UINT1 *pu1Lldpdu, UINT2 u2LldpduLen,
                                UINT4 *pu4VidDigestTlvCount)
{
    UINT4               u4Offset = 0;
    UINT4               u4VidDigestTlvCount = 0;
    UINT2               u2TlvType = 0;
    UINT2               u2TlvLength = 0;
    UINT2               u2ProcPduLen = 0;
    UINT1               u1TlvSubType = 0;
    UINT1              *pu1TlvInfo = NULL;
    UINT1               au1RxOUI[LLDP_MAX_LEN_OUI] = { 0 };
    UINT1              *pu1Tlv = NULL;

    if (NULL == pu1Lldpdu)
    {
        /* No LLDP Pdu found for processing */
        LLDP_TRC (ALL_FAILURE_TRC,
                  "LldpUtlGetVidUsageDigestMgmtVidTlvsCnt :  No LLDP Pdu found for"
                  "processing\r\n");
        return OSIX_FAILURE;
    }
    pu1Tlv = pu1Lldpdu;

    do
    {
        /* Get the New TLV Type from the TLV */
        LLDP_GET_TLV_TYPE_LENGTH (pu1Lldpdu, &u2TlvType, &u2TlvLength);

        u2ProcPduLen = (UINT2) ((pu1Lldpdu - pu1Tlv) + LLDP_TLV_HEADER_LEN +
                                u2TlvLength);
        /* Check the LLDPDU Physical Boundary */
        if (u2ProcPduLen > (UINT2) LLDP_MAX_PDU_SIZE)
        {
            return OSIX_FAILURE;
        }

        /* Check if the processed PDU length is more than the rcvd PDU
         * length. */
        if (u2ProcPduLen > u2LldpduLen)
        {
            return OSIX_FAILURE;
        }

        if (u2TlvType == LLDP_ORG_SPEC_TLV)
        {
            /* Move the pointer to TLV info */
            pu1TlvInfo = pu1Lldpdu + LLDP_TLV_HEADER_LEN;

            /* Get the OUI */
            LLDP_LBUF_GET_STRING (pu1TlvInfo, au1RxOUI, u4Offset,
                                  LLDP_MAX_LEN_OUI);

            /* Get the Tlv Subtype */
            LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, u4Offset, u1TlvSubType);

            /* Check if the TLV is 802.1 organizationally specific TLV */
            if (MEMCMP (au1RxOUI, gau1LldpDot1OUI, LLDP_MAX_LEN_OUI) == 0)
            {
                if (u1TlvSubType == LLDP_VID_USAGE_DIGEST_TLV)
                {
                    /* We have to process the 802.1 VID USAGE DIGEST TLV, only 
                     * if it passes in the basic validation; otherwise the 
                     * counters statTLVsDiscarded would be incremented twice */
                    if (LldpDot1TlvProcVidUsageDigestTlv
                        (pu1TlvInfo, u2TlvLength) == OSIX_SUCCESS)
                    {
                        u4VidDigestTlvCount += 1;
                    }
                    else
                    {
                        /* If the TLV fails in basic validation then the entire
                         * LLDPDU will be discarded and the error counters will
                         * be incremented in the place where the LLDPDU processing
                         * initiated; so no need to process further */
                        return OSIX_FAILURE;
                    }
                }
            }
        }
        pu1Lldpdu = pu1Lldpdu + LLDP_TLV_HEADER_LEN + u2TlvLength;
    }
    while (u2TlvType != LLDP_END_OF_LLDPDU_TLV);

    *pu4VidDigestTlvCount = u4VidDigestTlvCount;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetMgmtVidTlvCnt
 
 *    DESCRIPTION      : This function counts the number of Management VID
 *                       TLV present in an LLDPDU. 
 *
 *    INPUT            : pu1Lldpdu - Pointer to the received LLDPDU 
 *                       u2LldpduLen - Length of the received LLDPDU
 *
 *    OUTPUT           : pu4MgmtVidTlvCount   - The number of Management VID
 *                       TLVs present in the LLDPDU.
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpUtlGetMgmtVidTlvCnt (UINT1 *pu1Lldpdu, UINT2 u2LldpduLen,
                         UINT4 *pu4MgmtVidTlvCount)
{
    UINT4               u4Offset = 0;
    UINT4               u4MgmtVidTlvCount = 0;
    UINT2               u2TlvType = 0;
    UINT2               u2TlvLength = 0;
    UINT2               u2ProcPduLen = 0;
    UINT1               u1TlvSubType = 0;
    UINT1              *pu1TlvInfo = NULL;
    UINT1               au1RxOUI[LLDP_MAX_LEN_OUI] = { 0 };
    UINT1              *pu1Tlv = NULL;

    if (NULL == pu1Lldpdu)
    {
        /* No LLDP Pdu found for processing */
        LLDP_TRC (ALL_FAILURE_TRC,
                  "LldpUtlGetVidUsageDigestMgmtVidTlvsCnt :  No LLDP Pdu found for"
                  "processing\r\n");
        return OSIX_FAILURE;
    }
    pu1Tlv = pu1Lldpdu;

    do
    {
        /* Get the New TLV Type from the TLV */
        LLDP_GET_TLV_TYPE_LENGTH (pu1Lldpdu, &u2TlvType, &u2TlvLength);

        u2ProcPduLen = (UINT2) ((pu1Lldpdu - pu1Tlv) + LLDP_TLV_HEADER_LEN +
                                u2TlvLength);
        /* Check the LLDPDU Physical Boundary */
        if (u2ProcPduLen > (UINT2) LLDP_MAX_PDU_SIZE)
        {
            return OSIX_FAILURE;
        }

        /* Check if the processed PDU length is more than the rcvd PDU
         * length. */
        if (u2ProcPduLen > u2LldpduLen)
        {
            return OSIX_FAILURE;
        }

        if (u2TlvType == LLDP_ORG_SPEC_TLV)
        {
            /* Move the pointer to TLV info */
            pu1TlvInfo = pu1Lldpdu + LLDP_TLV_HEADER_LEN;

            /* Get the OUI */
            LLDP_LBUF_GET_STRING (pu1TlvInfo, au1RxOUI, u4Offset,
                                  LLDP_MAX_LEN_OUI);

            /* Get the Tlv Subtype */
            LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, u4Offset, u1TlvSubType);

            /* Check if the TLV is 802.1 organizationally specific TLV */
            if (MEMCMP (au1RxOUI, gau1LldpDot1OUI, LLDP_MAX_LEN_OUI) == 0)
            {
                if (u1TlvSubType == LLDP_MGMT_VID_TLV)
                {
                    /* We have to process the 802.1 Mgmt VID TLV, only 
                     * if it passes in the basic validation; otherwise the 
                     * counters statTLVsDiscarded would be incremented twice */
                    if (LldpDot1TlvProcMgmtVidTlv (pu1TlvInfo, u2TlvLength)
                        == OSIX_SUCCESS)
                    {
                        u4MgmtVidTlvCount += 1;
                    }
                    else
                    {
                        /* If the TLV fails in basic validation then the entire
                         * LLDPDU will be discarded and the error counters will
                         * be incremented in the place where the LLDPDU processing
                         * initiated; so no need to process further */
                        return OSIX_FAILURE;
                    }
                }
            }
        }
        pu1Lldpdu = pu1Lldpdu + LLDP_TLV_HEADER_LEN + u2TlvLength;
    }
    while (u2TlvType != LLDP_END_OF_LLDPDU_TLV);

    *pu4MgmtVidTlvCount = u4MgmtVidTlvCount;

    return OSIX_SUCCESS;
}

#ifndef CLI_WANTED

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpUtilSetDstMac
 *
 *     DESCRIPTION      : This function is used to set/reset the destination
 *                        MAC for the LLDP agent on this port.
 *     
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Status   - Lldp Module status
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpUtilSetDstMac (INT4 i4IfIndex, UINT1 *pu1MacAddr,
                   UINT1 u1RowStatus, UINT1 *pu1FatalError)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4DestIndex = 0;
    tLldpv2DestAddrTbl  DestAddrTbl;
    tLldpv2DestAddrTbl *pDestAddrTbl = NULL;
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;

    MEMSET (&DestAddrTbl, 0, sizeof (tLldpv2DestAddrTbl));

    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
    {

        if (u1RowStatus == LLDP_DESTROY)
        {
            MEMCPY (pu1MacAddr, gau1LldpMcastAddr, sizeof (tMacAddr));
        }
        if (nmhTestv2FsLldpLocPortDstMac (&u4ErrorCode, i4IfIndex, pu1MacAddr)
            == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhSetFsLldpLocPortDstMac (i4IfIndex, pu1MacAddr) == SNMP_FAILURE)
        {
            LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                           "\r\n%%Unable to set Destination MAC address for port %d\r\n",
                           i4IfIndex);
            *pu1FatalError = OSIX_TRUE;
            return OSIX_FAILURE;
        }
    }
    else
    {

        pDestAddrTbl = (tLldpv2DestAddrTbl *)
            RBTreeGetFirst (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree);

        while (pDestAddrTbl != NULL)
        {
            if (MEMCMP (pDestAddrTbl->Lldpv2DestMacAddress,
                        pu1MacAddr, MAC_ADDR_LEN) == 0)
            {
                u4DestIndex = pDestAddrTbl->u4LlldpV2DestAddrTblIndex;
                break;
            }
            DestAddrTbl.u4LlldpV2DestAddrTblIndex =
                pDestAddrTbl->u4LlldpV2DestAddrTblIndex;
            pDestAddrTbl = (tLldpv2DestAddrTbl *)
                RBTreeGetNext (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree,
                               &DestAddrTbl, LldpDstMacAddrUtlRBCmpInfo);
        }

        pLldpAgentPortInfo =
            LldpGetPortMapTableNode ((UINT4) i4IfIndex,
                                     (tMacAddr *) pu1MacAddr);

        if (u1RowStatus == LLDP_DESTROY)
        {
            if ((pDestAddrTbl == NULL) || (pLldpAgentPortInfo == NULL))
            {
                LLDP_TRC (ALL_FAILURE_TRC | LLDP_CRITICAL_TRC,
                          "DestMacAddr Not Found\r\n");
                return OSIX_FAILURE;
            }
        }

        if ((pLldpAgentPortInfo != NULL) && (u4DestIndex != 0))
        {
            if (u1RowStatus == LLDP_CREATE_AND_GO)
            {
                /*Entry is already present just need to Make active. */
                u1RowStatus = LLDP_ACTIVE;
            }

            if (nmhTestv2Fslldpv2ConfigPortRowStatus (&u4ErrorCode, i4IfIndex,
                                                      pu1MacAddr,
                                                      u1RowStatus) ==
                SNMP_FAILURE)
            {
                LLDP_TRC (ALL_FAILURE_TRC, "DestMacAddrTbl Index not fund\r\n");
                return OSIX_FAILURE;
            }

            if (nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr,
                                                   u1RowStatus) == SNMP_FAILURE)
            {
                LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                               "\r\n%%Unable to set LLDP Agent as active  %d\r\n",
                               i4IfIndex);
                *pu1FatalError = OSIX_TRUE;
                return OSIX_FAILURE;
            }
            if (nmhTestv2Fslldpv2DestRowStatus (&u4ErrorCode, u4DestIndex,
                                                u1RowStatus) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }
            if (nmhSetFslldpv2DestRowStatus (u4DestIndex, u1RowStatus) ==
                SNMP_FAILURE)
            {
                return OSIX_FAILURE;

            }
            if (u1RowStatus == LLDP_DESTROY)
            {
                gu4LldpDestMacAddrTblIndex--;
            }

            return OSIX_SUCCESS;
        }

        if ((u1RowStatus == LLDP_CREATE_AND_GO))
        {
            /*New Entry Need to Create and then make as active. */
            u1RowStatus = CREATE_AND_WAIT;
            if (u4DestIndex == 0)
            {
                u4DestIndex = gu4LldpDestMacAddrTblIndex;
                if (nmhTestv2Fslldpv2DestRowStatus (&u4ErrorCode, u4DestIndex,
                                                    (INT4) u1RowStatus) ==
                    SNMP_FAILURE)
                {
                    return OSIX_FAILURE;
                }
                if (nmhSetFslldpv2DestRowStatus (u4DestIndex, u1RowStatus) ==
                    SNMP_FAILURE)
                {
                    return OSIX_FAILURE;

                }
                if (nmhTestv2FslldpV2DestMacAddress (&u4ErrorCode, u4DestIndex,
                                                     pu1MacAddr) ==
                    SNMP_FAILURE)
                {
                    nmhSetFslldpv2DestRowStatus (u4DestIndex, LLDP_DESTROY);
                    gu4LldpDestMacAddrTblIndex--;
                    return OSIX_FAILURE;
                }

                if (nmhSetFslldpV2DestMacAddress (u4DestIndex, pu1MacAddr) ==
                    SNMP_FAILURE)
                {
                    nmhSetFslldpv2DestRowStatus (u4DestIndex, LLDP_DESTROY);
                    gu4LldpDestMacAddrTblIndex--;

                    return OSIX_FAILURE;
                }
            }
        }
        /*For LLDP_DESTROY with u4DestIndex equal to zero will fail below. */
        if (nmhTestv2Fslldpv2ConfigPortRowStatus (&u4ErrorCode, i4IfIndex,
                                                  pu1MacAddr,
                                                  (INT4) u1RowStatus) ==
            SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, LLDP_DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            return OSIX_FAILURE;
        }

        if (nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr,
                                               (INT4) u1RowStatus) ==
            SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, LLDP_DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                           "\r\n%%Unable to create the  LLDP Agent  %d\r\n",
                           i4IfIndex);
            *pu1FatalError = OSIX_TRUE;
            return OSIX_FAILURE;
        }
        if (nmhTestv2FsLldpLocPortDstMac (&u4ErrorCode, i4IfIndex, pu1MacAddr)
            == SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, LLDP_DESTROY);
            nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr,
                                               LLDP_DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            return OSIX_FAILURE;
        }

        if (nmhSetFsLldpLocPortDstMac (i4IfIndex, pu1MacAddr) == SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, LLDP_DESTROY);
            nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr,
                                               LLDP_DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                           "\r\n%%Unable to set Destination MAC address for port %d\r\n",
                           i4IfIndex);
            *pu1FatalError = OSIX_TRUE;
            return OSIX_FAILURE;
        }

        if (nmhTestv2Fslldpv2DestRowStatus (&u4ErrorCode, u4DestIndex,
                                            LLDP_ACTIVE) == SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, LLDP_DESTROY);
            nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr,
                                               LLDP_DESTROY);
            gu4LldpDestMacAddrTblIndex--;

            return OSIX_FAILURE;
        }
        if (nmhSetFslldpv2DestRowStatus (u4DestIndex, LLDP_ACTIVE) ==
            SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, LLDP_DESTROY);
            nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr,
                                               LLDP_DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            return OSIX_FAILURE;

        }
        if (nmhTestv2Fslldpv2ConfigPortRowStatus (&u4ErrorCode, i4IfIndex,
                                                  pu1MacAddr,
                                                  LLDP_ACTIVE) == SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, LLDP_DESTROY);
            nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr,
                                               LLDP_DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            return OSIX_FAILURE;
        }

        if (nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr,
                                               LLDP_ACTIVE) == SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, LLDP_DESTROY);
            nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr,
                                               LLDP_DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                           "\r\n%%Unable to set LLDP Agent as active  %d\r\n",
                           i4IfIndex);
            *pu1FatalError = OSIX_TRUE;
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}
#endif
/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlTlvProcManAddrTlv
 *
 *    DESCRIPTION      : This function Process the Remote System Management
 *                       Address TLV.
 *                       Decode the Information from the TLV Information
 *                       field  and then Validate the information. Also notify
 *                       if any protocol misconfiguration errors.
 *
 *    INPUT            : pLocPortInfo - Port Info Structure
 *                       pu1Tlv - Buffer Points to the TLV
 *                       u2TlvLength - Length of the Information Field
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpUtlTlvProcManAddrTlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv,
                          UINT2 u2TlvLength)
{
    tLldpRemManAddressTable ManAddrNode;
    UINT1              *pu1TlvInfo = NULL;
    UINT1              *pu1TmpTlvInfo = NULL;
    UINT1               u1ManAddrStrLength = 0;    /*(Subtype + Address) */
    UINT2               u2DecodedLen = 0;
    BOOL1               bResult = OSIX_FALSE;

    MEMSET (&ManAddrNode, 0, sizeof (tLldpRemManAddressTable));

    LLDP_TRC (LLDP_MAN_ADDR_TRC, "LldpUtlTlvProcManAddrTlv: "
              "Processing Management Address TLV :\r\n");

    pu1TlvInfo = pu1Tlv + LLDP_TLV_HEADER_LEN;
    pu1TmpTlvInfo = pu1TlvInfo;

    OSIX_BITLIST_IS_BIT_SET (pLocPortInfo->au1DupTlvChkFlag,
                             (UINT2) LLDP_DUP_MGMT_ADDR_TLV,
                             (INT4) LLDP_MAX_DUP_TLV_ARRAY_SIZE, bResult);

    if ((u2TlvLength < LLDP_MIN_MAN_ADDR_INFO_LEN)
        || (u2TlvLength > LLDP_MAX_MAN_ADDR_INFO_LEN))
    {
        if ((pLocPortInfo->u1RxFrameType != LLDP_RX_FRAME_REFRESH) ||
            (bResult == OSIX_FALSE))
        {
            LLDP_TRC (ALL_FAILURE_TRC | LLDP_MAN_ADDR_TRC,
                      "Discarding LLDPDU as obtained Management Address TLV"
                      "length is not within the valid range (9-167) \r\n");
        }
        return OSIX_FAILURE;
    }

    LLDP_LBUF_GET_1_BYTE (pu1TmpTlvInfo, 0, u1ManAddrStrLength);

    if ((u1ManAddrStrLength > LLDP_MAX_LEN_MAN_ADDR + LLDP_SUBTYPE_LEN)
        || (u1ManAddrStrLength < LLDP_MIN_LEN_MAN_ADDR + LLDP_SUBTYPE_LEN))
    {
        return OSIX_SUCCESS;
    }
    /* Decode the Tlv Information */
    if (LldpTlvDecodeManAddrInfo (pu1TlvInfo, u2TlvLength,
                                  &ManAddrNode, &u2DecodedLen) == OSIX_FAILURE)
    {
        return OSIX_SUCCESS;
    }

    /* validate the length [std IEEE 802.1AB-2005, clause 10.3.2.1-(c)] */
    if (u2DecodedLen > u2TlvLength)
    {
        if (pLocPortInfo->u1RxFrameType != LLDP_RX_FRAME_REFRESH ||
            bResult == OSIX_FALSE)
        {
            LLDP_TRC (ALL_FAILURE_TRC | LLDP_MAN_ADDR_TRC,
                      "LldpUtlTlvProcManAddrTlv: "
                      "Actual Length of Management Address TLV information is "
                      "greater than the length mentioned in the lenth field.\r\n");
        }
        return OSIX_FAILURE;
    }
    if ((ManAddrNode.i4RemManAddrIfSubtype != LLDP_MAN_ADDR_IF_SUB_UNKNOWN) &&
        (ManAddrNode.i4RemManAddrIfSubtype != LLDP_MAN_ADDR_IF_SUB_IFINDEX) &&
        (ManAddrNode.i4RemManAddrIfSubtype != LLDP_MAN_ADDR_IF_SUB_SYSPORTNUM))
    {
        if (pLocPortInfo->u1RxFrameType != LLDP_RX_FRAME_REFRESH ||
            bResult == OSIX_FALSE)
        {
            LLDP_TRC (ALL_FAILURE_TRC | LLDP_MAN_ADDR_TRC,
                      "\tMan Addr TLV Error. Unsupported If Numberibng Subtype is "
                      "advertised. Discarding the TLV\r\n");
        }
        return OSIX_SUCCESS;
    }
    LLDP_TRC (LLDP_MAN_ADDR_TRC, "\tValidated Management Address TLV\r\n");
    return OSIX_SUCCESS;
}

/********************************** LLDP-MED ******************************************************/

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlValidateIndexPortConfTbl
 *
 *    DESCRIPTION      : To Validate the Port Config Table
 *
 *    INPUT            : i4LldpV2PortConfigIfIndex - Interface Index
 *                       DestMacAddr - Destination MacAddress of the agent
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlValidateIndexPortConfTbl (INT4
                                    i4LldpV2PortConfigIfIndex,
                                    tMacAddr DestMacAddr)
{
    tLldpLocPortInfo   *pLldpMedLocPortInfo = NULL;
    UINT4               u4LocPort = 0;

    /* Checking Whether LLDP is Shutdown. If it is true, then we should not
     * configure */
    if (LLDP_IS_SHUTDOWN ())
    {
        return OSIX_FAILURE;
    }

    /* Get the Localport information by passing the arguments IfIndex and
     * Destination MacAddress*/
    if (LldpUtlGetLocalPort (i4LldpV2PortConfigIfIndex, DestMacAddr,
                             &u4LocPort) != OSIX_SUCCESS)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "LldpMedUtlValidateIndexPortConfTbl: "
                  "Unable to get the local port, either"
                  "Port Number or Dest mac is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    pLldpMedLocPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPort);
    if (pLldpMedLocPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "LldpMedUtlValidateIndexPortConfTbl: "
                  "Unable to get the Port Info \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetFirstIndexPortConfTbl
 *
 *    DESCRIPTION      : To Get First Index of  the Port Config table
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : pi4LldpV2PortConfigIfIndex
 *                       pu4LldpV2PortConfigDestAddressIndex
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetFirstIndexPortConfTbl (INT4 *pi4LldpV2PortConfigIfIndex,
                                    UINT4 *pu4LldpV2PortConfigDestAddressIndex)
{
    tLldpv2AgentToLocPort *pLldpv2AgentToLocPort = NULL;

    /* Checking Whether LLDP is Shutdown. If it is true, then we should not
     * configure */
    if (LLDP_IS_SHUTDOWN ())
    {
        return OSIX_FAILURE;
    }

    pLldpv2AgentToLocPort = (tLldpv2AgentToLocPort *)
        RBTreeGetFirst (gLldpGlobalInfo.Lldpv2AgentMapTblRBTree);
    if (pLldpv2AgentToLocPort == NULL)
    {
        return OSIX_FAILURE;
    }
    *pi4LldpV2PortConfigIfIndex = pLldpv2AgentToLocPort->i4IfIndex;
    *pu4LldpV2PortConfigDestAddressIndex =
        pLldpv2AgentToLocPort->u4DstMacAddrTblIndex;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetNextIndexPortConfTbl
 *
 *    DESCRIPTION      : To Get next Index of the Port Config table
 *
 *    INPUT            : i4LldpV2PortConfigIfIndex - Interface Index
 *                       u4LldpV2PortConfigDestAddressIndex - 
 *                       Destination Mac Address Index of the agent
 *
 *    OUTPUT           : pi4NextLldpV2PortConfigIfIndex
 *                       pu4NextLldpV2PortConfigDestAddressIndex
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetNextIndexPortConfTbl (INT4 i4LldpV2PortConfigIfIndex, INT4
                                   *pi4NextLldpV2PortConfigIfIndex,
                                   UINT4 u4LldpV2PortConfigDestAddressIndex,
                                   UINT4
                                   *pu4NextLldpV2PortConfigDestAddressIndex)
{
    tLldpv2AgentToLocPort Lldpv2AgentToLocPort;
    tLldpv2AgentToLocPort *pLldpv2AgentToLocPort = NULL;
    MEMSET (&Lldpv2AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
    if (LLDP_IS_SHUTDOWN ())
    {
        return OSIX_FAILURE;
    }

    Lldpv2AgentToLocPort.i4IfIndex = i4LldpV2PortConfigIfIndex;
    Lldpv2AgentToLocPort.u4DstMacAddrTblIndex =
        u4LldpV2PortConfigDestAddressIndex;
    pLldpv2AgentToLocPort =
        (tLldpv2AgentToLocPort *) RBTreeGetNext (gLldpGlobalInfo.
                                                 Lldpv2AgentMapTblRBTree,
                                                 &Lldpv2AgentToLocPort,
                                                 LldpAgentToLocPortIndexUtlRBCmpInfo);

    if (pLldpv2AgentToLocPort == NULL)
    {
        return OSIX_FAILURE;
    }

    /* Assigning IfIndex and DestinationMacAddressIndex */
    *pi4NextLldpV2PortConfigIfIndex = pLldpv2AgentToLocPort->i4IfIndex;
    *pu4NextLldpV2PortConfigDestAddressIndex =
        pLldpv2AgentToLocPort->u4DstMacAddrTblIndex;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetLocPortCapSupported
 *
 *    DESCRIPTION      : To Get the Capabilities supported by LLDP-MED on 
 *                       a specific port
 *
 *    INPUT            : i4LldpV2PortConfigIfIndex - IfIndex of the agent
 *                         DestMacAddr - Destination MacAddress of the agent
 *
 *    OUTPUT           : pRetValFsLldpMedPortCapSupported
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetLocPortCapSupported (INT4 i4LldpV2PortConfigIfIndex, tMacAddr
                                  DestMacAddr, tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsLldpMedPortCapSupported)
{
    tLldpLocPortInfo    LldpMedLocPortInfo;
    tLldpLocPortInfo   *pLldpMedLocPortInfo = NULL;
    UINT4               u4LocPort = 0;

    MEMSET (&LldpMedLocPortInfo, 0, sizeof (tLldpLocPortInfo));

    /* Get the Localport information by passing the arguments IfIndex and
     * Destination MacAddress */
    if (LldpUtlGetLocalPort (i4LldpV2PortConfigIfIndex, DestMacAddr,
                             &u4LocPort) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    LldpMedLocPortInfo.u4LocPortNum = u4LocPort;

    /* Get Port Information using RBTreeGet */
    pLldpMedLocPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpMedLocPortInfo);
    if (pLldpMedLocPortInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    /* Assigning Capabilities Supported to octet list of
     * pRetValFsLldpMedPortCapSupported */
    MEMCPY (pRetValFsLldpMedPortCapSupported->pu1_OctetList,
            (UINT1 *) &pLldpMedLocPortInfo->LocMedCapInfo.
            u2MedLocCapTxSupported,
            sizeof (pLldpMedLocPortInfo->LocMedCapInfo.u2MedLocCapTxSupported));
    pRetValFsLldpMedPortCapSupported->i4_Length =
        sizeof (pLldpMedLocPortInfo->LocMedCapInfo.u2MedLocCapTxSupported);

    return OSIX_SUCCESS;
}

/*************************************************************************
 * FUNCTION NAME    : LldpMedUtlGetLldpPortConfigTLVsTxEnable
 *
 * DESCRIPTION      : To get the Tlvs enabled for specific port and
 *                    Destination MacAddress
 *
 * INPUT            : i4LldpV2PortConfigIfIndex - Interface Index
 *                    DestMacAddr - Dest Mac address of the agent
 *
 * OUTPUT           : pRetValFsLldpXMedPortConfigTLVsTxEnable
 *
 * RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *************************************************************************/
INT4
LldpMedUtlGetLldpPortConfigTLVsTxEnable (INT4 i4LldpV2PortConfigIfIndex,
                                         tMacAddr DestMacAddr,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValFsLldpMedPortConfigTLVsTxEnable)
{

    tLldpLocPortInfo    LldpMedLocPortInfo;
    tLldpLocPortInfo   *pLldpMedLocPortInfo = NULL;
    UINT4               u4LocPort = 0;

    /* Get the Localport information by passing the arguments IfIndex and
     * Destination MacAddress */
    if (LldpUtlGetLocalPort (i4LldpV2PortConfigIfIndex, DestMacAddr,
                             &u4LocPort) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    MEMSET (&LldpMedLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    LldpMedLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpMedLocPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpMedLocPortInfo);

    if (pLldpMedLocPortInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    /* Assigning TLVsEnabled */
    MEMCPY (pRetValFsLldpMedPortConfigTLVsTxEnable->pu1_OctetList,
            &(pLldpMedLocPortInfo->LocMedCapInfo.u2MedLocCapTxEnable),
            sizeof (pLldpMedLocPortInfo->LocMedCapInfo.u2MedLocCapTxEnable));

    pRetValFsLldpMedPortConfigTLVsTxEnable->i4_Length =
        sizeof (pLldpMedLocPortInfo->LocMedCapInfo.u2MedLocCapTxEnable);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlSetPortConfigTLVsTxEnable
 *
 *    DESCRIPTION      : This function will set the TLV status to either
 *                       Enable or Disable
 *
 *    INPUT            : i4LldpV2PortConfigIfIndex - Interface Index 
 *                       DestMacAddr - Destination MacAddress of the agent
 *
 *    OUTPUT           : pSetValFsLldpXMedPortConfigTLVsTxEnable
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlSetPortConfigTLVsTxEnable (INT4 i4LldpV2PortConfigIfIndex,
                                     tMacAddr DestMacAddr,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValFsLldpMedPortConfigTLVsTxEnable)
{
    tLldpLocPortInfo    LldpMedLocPortInfo;
    tLldpLocPortInfo   *pLldpMedLocPortInfo = NULL;
    INT4                i4AdminStatus = 0;
    UINT4               u4LocPort = 0;
    UINT2               u2NewTLVsTxEnable = 0;
    UINT2               u2CurrTLVsTxEnable = 0;

    /* Get the Localport information by passing the arguments IfIndex and
     * Destination MacAddress */
    if (LldpUtlGetLocalPort (i4LldpV2PortConfigIfIndex, DestMacAddr,
                             &u4LocPort) != OSIX_SUCCESS)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " LldpMedUtlSetPortConfigTLVsTxEnable: "
                  " Unable to get the Local Port, either"
                  " Port Number or Dest mac is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&LldpMedLocPortInfo, 0, sizeof (tLldpLocPortInfo));

    LldpMedLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpMedLocPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpMedLocPortInfo);

    if (pLldpMedLocPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "LldpMedUtlSetPortConfigTLVsTxEnable:"
                  "Unable to get the Port Info \r\n");
        return OSIX_FAILURE;
    }

    MEMCPY (&u2NewTLVsTxEnable,
            pSetValFsLldpMedPortConfigTLVsTxEnable->pu1_OctetList,
            pSetValFsLldpMedPortConfigTLVsTxEnable->i4_Length);

    u2CurrTLVsTxEnable = pLldpMedLocPortInfo->LocMedCapInfo.u2MedLocCapTxEnable;

    /* if current value and new value are same, then dont set the value
     * just return success */
    if (u2CurrTLVsTxEnable == u2NewTLVsTxEnable)
    {
        return OSIX_SUCCESS;
    }

    pLldpMedLocPortInfo->LocMedCapInfo.u2MedLocCapTxEnable = u2NewTLVsTxEnable;

    /* Since TLVs to be transmitted on this port is changed, construct the
     * preformed buffer for that port - which is equal to change in
     * local system information for the port. Hence handle the change */
    LldpMedGetAdminStatus (i4LldpV2PortConfigIfIndex, &i4AdminStatus);
    if ((i4AdminStatus == LLDP_ENABLED)
        && (pLldpMedLocPortInfo->bMedCapable == LLDP_MED_TRUE))
    {
        if (LldpTxUtlHandleLocPortInfoChg (pLldpMedLocPortInfo) != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC, "LldpMedUtlSetPortConfigTLVsTxEnable: "
                      "Unable to handle the Changes \r\n");
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlTestPortConfigTLVsTxEnable
 *
 *    DESCRIPTION      : This function tests the Port Info and the Tlvs
 *                       selected
 *
 *    INPUT            : i4LldpV2PortConfigIfIndex - Interface Index  
 *                       DestMacAddr - Destination MacAddress of the agent
 *
 *    OUTPUT           : pTestValFsLldpXMedPortConfigTLVsTxEnable 
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlTestPortConfigTLVsTxEnable (UINT4 *pu4ErrorCode,
                                      INT4 i4LldpV2PortConfigIfIndex,
                                      tMacAddr DestMacAddr,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValFsLldpMedPortConfigTLVsTxEnable)
{
    tLldpLocPortInfo   *pLocPortInfo = NULL;
    UINT2               u2TlvTxEnable = 0;
    UINT4               u4LocPort = 0;
    INT4                i4AdminStatus = 0;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " LldpMedUtlTestPortConfigTLVsTxEnable: "
                  "LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }

    if (LldpUtlGetLocalPort (i4LldpV2PortConfigIfIndex, DestMacAddr,
                             &u4LocPort) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " LldpMedUtlTestPortConfigTLVsTxEnable: "
                  " Unable to get the Local Port, either"
                  " Port Number or Dest mac is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    pLocPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPort);
    if (pLocPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "LldpMedUtlTestPortConfigTLVsTxEnable: "
                  "Unable to get the Port Info \r\n");
        return OSIX_FAILURE;
    }

    MEMCPY (&u2TlvTxEnable,
            pTestValFsLldpMedPortConfigTLVsTxEnable->pu1_OctetList,
            sizeof (u2TlvTxEnable));

    /* Check whether any TLVs are enabled other than supported TLVs */
    if (u2TlvTxEnable & ~LLDP_MED_DEF_TLV_SUPPORTED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "LldpMedUtlTestPortConfigTLVsTxEnable: "
                  "Selected Tlvs are not valid \r\n");

        return OSIX_FAILURE;
    }

    /* Get AdminStatus for checking TX and RX */
    if (nmhGetLldpV2PortConfigAdminStatus (i4LldpV2PortConfigIfIndex,
                                           pLocPortInfo->u4DstMacAddrTblIndex,
                                           &i4AdminStatus) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Check for TX and RX */
    if (i4AdminStatus != LLDP_ADM_STAT_TX_AND_RX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "LldpMedUtlTestPortConfigTLVsTxEnable: "
                  "AdminStatus should be both TX and RX to set the Tlvs \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlValIndexLocNwPolTbl 
 *
 *    DESCRIPTION      : To Validate the Local policy Table
 *
 *    INPUT            : i4LldpV2LocPortIfIndex - Interface Index
 *                       pFsLldpMedLocMediaPolicyAppType - Application Type
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlValIndexLocNwPolTbl (INT4 i4LldpV2LocPortIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsLldpMedLocMediaPolicyAppType)
{
    UINT1               u1AppType = 0;

    /* Checking Whether LLDP is Shutdown. If it is true, then we should not
     * configure */
    if (LLDP_IS_SHUTDOWN ())
    {
        return OSIX_FAILURE;
    }

    /* Validate Interface Index */
    if (LldpTxUtlValidateInterfaceIndex (i4LldpV2LocPortIfIndex) !=
        OSIX_SUCCESS)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " LldpMedUtlValIndexLocNwPolTbl: "
                  " Interface Index is invalid \r\n");
        return OSIX_FAILURE;
    }

    u1AppType = *pFsLldpMedLocMediaPolicyAppType->pu1_OctetList;

    /* Validate Application Type */
    if ((u1AppType < LLDP_MED_MIN_APP_TYPE) ||
        (u1AppType > LLDP_MED_MAX_APP_TYPE))
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " Application Type is not in range ");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetFirstIndexLocNwPolTbl
 *
 *    DESCRIPTION      : To Get the First Index of Local Policy table
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : pi4LldpV2LocPortIfIndex - Interface Index
 *                       pFsLldpMedLocMediaPolicyAppType - Application Type
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetFirstIndexLocNwPolTbl (INT4 *pi4LldpV2LocPortIfIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsLldpMedLocMediaPolicyAppType)
{
    tLldpMedLocNwPolicyInfo *pLldpMedLocPolicy = NULL;

    /* Checking Whether LLDP is Shutdown. If it is true, then we should not
     * configure */
    if (LLDP_IS_SHUTDOWN ())
    {
        return OSIX_FAILURE;
    }

    /* Get First Index of PolicyTable */
    pLldpMedLocPolicy =
        (tLldpMedLocNwPolicyInfo *) RBTreeGetFirst (gLldpGlobalInfo.
                                                    LldpMedLocNwPolicyInfoRBTree);
    if (pLldpMedLocPolicy == NULL)
    {
        return OSIX_FAILURE;
    }

    /* Assigning IfIndex and Policy App type */
    *pi4LldpV2LocPortIfIndex = (INT4) pLldpMedLocPolicy->u4IfIndex;
    *pFsLldpMedLocMediaPolicyAppType->pu1_OctetList =
        pLldpMedLocPolicy->u1LocPolicyAppType;
    pFsLldpMedLocMediaPolicyAppType->i4_Length =
        sizeof (pLldpMedLocPolicy->u1LocPolicyAppType);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetNextIndexLocNwPolTbl
 *
 *    DESCRIPTION      : To Get the next Index of Local Policy table
 *
 *    INPUT            : i4LldpV2LocPortIfIndex - Interface Index
 *                       pFsLldpMedLocMediaPolicyAppType - Application Type
 *
 *    OUTPUT           : pi4NextLldpV2LocPortIfIndex
 *                       pNextFsLldpMedLocMediaPolicyAppType
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetNextIndexLocNwPolTbl (INT4 i4LldpV2LocPortIfIndex,
                                   INT4 *pi4NextLldpV2LocPortIfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsLldpMedLocMediaPolicyAppType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextFsLldpMedLocMediaPolicyAppType)
{
    tLldpMedLocNwPolicyInfo CurrLocPolicy;
    tLldpMedLocNwPolicyInfo *pNextLocPolicy = NULL;

    MEMSET (&CurrLocPolicy, 0, sizeof (tLldpMedLocNwPolicyInfo));
    if (LLDP_IS_SHUTDOWN ())
    {
        return OSIX_FAILURE;
    }

    /* Assigning the Current IfIndex */
    CurrLocPolicy.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    CurrLocPolicy.u1LocPolicyAppType =
        *(pFsLldpMedLocMediaPolicyAppType->pu1_OctetList);

    /* Get Next Index of Local Policy Table */
    pNextLocPolicy =
        (tLldpMedLocNwPolicyInfo *) RBTreeGetNext (gLldpGlobalInfo.
                                                   LldpMedLocNwPolicyInfoRBTree,
                                                   &CurrLocPolicy,
                                                   LldpMedLocNwPolicyInfoRBCmpInfo);

    if (pNextLocPolicy == NULL)
    {
        return OSIX_FAILURE;
    }

    *pi4NextLldpV2LocPortIfIndex = (INT4) pNextLocPolicy->u4IfIndex;
    *pNextFsLldpMedLocMediaPolicyAppType->pu1_OctetList =
        pNextLocPolicy->u1LocPolicyAppType;
    pNextFsLldpMedLocMediaPolicyAppType->i4_Length =
        sizeof (pNextLocPolicy->u1LocPolicyAppType);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetLocNwPolVlanID
 *
 *    DESCRIPTION      : To get the VlanId of Local Policy table
 *
 *    INPUT            : i4LldpV2LocPortIfIndex - Interface Index
 *                       pFsLldpMedLocMediaPolicyAppType - Application Type
 *
 *    OUTPUT           : pi4RetValFsLldpMedLocMediaPolicyVlanID 
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetLocNwPolVlanID (INT4 i4LldpV2LocPortIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsLldpMedLocMediaPolicyAppType,
                             INT4 *pi4RetValFsLldpMedLocMediaPolicyVlanID)
{
    tLldpMedLocNwPolicyInfo LldpMedLocPolicy;
    tLldpMedLocNwPolicyInfo *pLldpMedLocPolicy = NULL;

    MEMSET (&LldpMedLocPolicy, 0, sizeof (tLldpMedLocNwPolicyInfo));
    LldpMedLocPolicy.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    LldpMedLocPolicy.u1LocPolicyAppType =
        *(pFsLldpMedLocMediaPolicyAppType->pu1_OctetList);

    pLldpMedLocPolicy =
        (tLldpMedLocNwPolicyInfo *) RBTreeGet (gLldpGlobalInfo.
                                               LldpMedLocNwPolicyInfoRBTree,
                                               (tRBElem *) & LldpMedLocPolicy);

    if (pLldpMedLocPolicy == NULL)
    {
        return OSIX_FAILURE;
    }

    *pi4RetValFsLldpMedLocMediaPolicyVlanID = (INT4)
        pLldpMedLocPolicy->u2LocPolicyVlanId;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetLocNwPolPriority
 *
 *    DESCRIPTION      : To get the priority of Local Policy Table
 *
 *    INPUT            : i4LldpV2LocPortIfIndex - Interface Index
 *                       pFsLldpMedLocMediaPolicyAppType - Application Type
 *
 *    OUTPUT           : pi4RetValFsLldpMedLocMediaPolicyPriority
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetLocNwPolPriority (INT4 i4LldpV2LocPortIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsLldpMedLocMediaPolicyAppType,
                               INT4 *pi4RetValFsLldpMedLocMediaPolicyPriority)
{
    tLldpMedLocNwPolicyInfo LldpMedLocPolicy;
    tLldpMedLocNwPolicyInfo *pLldpMedLocPolicy = NULL;

    MEMSET (&LldpMedLocPolicy, 0, sizeof (tLldpMedLocNwPolicyInfo));
    LldpMedLocPolicy.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    LldpMedLocPolicy.u1LocPolicyAppType =
        *(pFsLldpMedLocMediaPolicyAppType->pu1_OctetList);

    pLldpMedLocPolicy =
        (tLldpMedLocNwPolicyInfo *) RBTreeGet (gLldpGlobalInfo.
                                               LldpMedLocNwPolicyInfoRBTree,
                                               (tRBElem *) & LldpMedLocPolicy);

    if (pLldpMedLocPolicy == NULL)
    {
        return OSIX_FAILURE;
    }

    *pi4RetValFsLldpMedLocMediaPolicyPriority = (INT4)
        pLldpMedLocPolicy->u1LocPolicyPriority;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetLocNwPolDscp
 *
 *    DESCRIPTION      : To get the Dscp value of Local Policy Table
 *
 *    INPUT            : i4LldpV2LocPortIfIndex - Interface Index
 *                       pFsLldpMedLocMediaPolicyAppType - Application Type
 *
 *    OUTPUT           : pi4RetValFsLldpMedLocMediaPolicyDscp
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetLocNwPolDscp (INT4 i4LldpV2LocPortIfIndex,
                           tSNMP_OCTET_STRING_TYPE *
                           pFsLldpMedLocMediaPolicyAppType,
                           INT4 *pi4RetValFsLldpMedLocMediaPolicyDscp)
{
    tLldpMedLocNwPolicyInfo LldpMedLocPolicy;
    tLldpMedLocNwPolicyInfo *pLldpMedLocPolicy = NULL;

    MEMSET (&LldpMedLocPolicy, 0, sizeof (tLldpMedLocNwPolicyInfo));
    LldpMedLocPolicy.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    LldpMedLocPolicy.u1LocPolicyAppType =
        *(pFsLldpMedLocMediaPolicyAppType->pu1_OctetList);

    pLldpMedLocPolicy =
        (tLldpMedLocNwPolicyInfo *) RBTreeGet (gLldpGlobalInfo.
                                               LldpMedLocNwPolicyInfoRBTree,
                                               (tRBElem *) & LldpMedLocPolicy);

    if (pLldpMedLocPolicy == NULL)
    {
        return OSIX_FAILURE;
    }

    *pi4RetValFsLldpMedLocMediaPolicyDscp =
        (INT4) pLldpMedLocPolicy->u1LocPolicyDscp;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetLocNwPolUnknown
 *
 *    DESCRIPTION      : To get the Unknown Policy Flag of Local Policy Table
 *
 *    INPUT            : i4LldpV2LocPortIfIndex - Interface Index
 *                       pFsLldpMedLocMediaPolicyAppType - Application Type
 *
 *    OUTPUT           : pi4RetValFsLldpMedLocMediaPolicyUnknown
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetLocNwPolUnknown (INT4 i4LldpV2LocPortIfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsLldpMedLocMediaPolicyAppType,
                              INT4 *pi4RetValFsLldpMedLocMediaPolicyUnknown)
{
    tLldpMedLocNwPolicyInfo LldpMedLocPolicy;
    tLldpMedLocNwPolicyInfo *pLldpMedLocPolicy = NULL;

    MEMSET (&LldpMedLocPolicy, 0, sizeof (tLldpMedLocNwPolicyInfo));
    LldpMedLocPolicy.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    LldpMedLocPolicy.u1LocPolicyAppType =
        *(pFsLldpMedLocMediaPolicyAppType->pu1_OctetList);

    pLldpMedLocPolicy =
        (tLldpMedLocNwPolicyInfo *) RBTreeGet (gLldpGlobalInfo.
                                               LldpMedLocNwPolicyInfoRBTree,
                                               (tRBElem *) & LldpMedLocPolicy);

    if (pLldpMedLocPolicy == NULL)
    {
        return OSIX_FAILURE;
    }

    *pi4RetValFsLldpMedLocMediaPolicyUnknown = (INT4)
        pLldpMedLocPolicy->bLocPolicyUnKnown;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetLocNwPolTagged
 *
 *    DESCRIPTION      : To get the Vlan Type of Local Policy Table
 *
 *    INPUT            : i4LldpV2LocPortIfIndex - Interface Index
 *                       pFsLldpMedLocMediaPolicyAppType - Application Type
 *
 *    OUTPUT           : pi4RetValFsLldpMedLocMediaPolicyTagged
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetLocNwPolTagged (INT4 i4LldpV2LocPortIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsLldpMedLocMediaPolicyAppType,
                             INT4 *pi4RetValFsLldpMedLocMediaPolicyTagged)
{
    tLldpMedLocNwPolicyInfo LldpMedLocPolicy;
    tLldpMedLocNwPolicyInfo *pLldpMedLocPolicy = NULL;

    MEMSET (&LldpMedLocPolicy, 0, sizeof (tLldpMedLocNwPolicyInfo));
    LldpMedLocPolicy.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    LldpMedLocPolicy.u1LocPolicyAppType =
        *(pFsLldpMedLocMediaPolicyAppType->pu1_OctetList);

    pLldpMedLocPolicy =
        (tLldpMedLocNwPolicyInfo *) RBTreeGet (gLldpGlobalInfo.
                                               LldpMedLocNwPolicyInfoRBTree,
                                               (tRBElem *) & LldpMedLocPolicy);

    if (pLldpMedLocPolicy == NULL)
    {
        return OSIX_FAILURE;
    }

    *pi4RetValFsLldpMedLocMediaPolicyTagged = (INT4)
        pLldpMedLocPolicy->bLocPolicyTagged;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetLocNwPolRowStatus
 *
 *    DESCRIPTION      : To get the Row Status of Local Policy Table
 *
 *    INPUT            : i4LldpV2LocPortIfIndex - Interface Index
 *                       pFsLldpMedLocMediaPolicyAppType - Application Type
 *
 *    OUTPUT           : pi4RetValFsLldpMedLocMediaPolicyRowStatus
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetLocNwPolRowStatus (INT4 i4LldpV2LocPortIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsLldpMedLocMediaPolicyAppType,
                                INT4 *pi4RetValFsLldpMedLocMediaPolicyRowStatus)
{
    tLldpMedLocNwPolicyInfo LldpMedLocPolicy;
    tLldpMedLocNwPolicyInfo *pLldpMedLocPolicy = NULL;

    MEMSET (&LldpMedLocPolicy, 0, sizeof (tLldpMedLocNwPolicyInfo));
    LldpMedLocPolicy.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    LldpMedLocPolicy.u1LocPolicyAppType =
        *(pFsLldpMedLocMediaPolicyAppType->pu1_OctetList);

    pLldpMedLocPolicy =
        (tLldpMedLocNwPolicyInfo *) RBTreeGet (gLldpGlobalInfo.
                                               LldpMedLocNwPolicyInfoRBTree,
                                               (tRBElem *) & LldpMedLocPolicy);

    if (pLldpMedLocPolicy == NULL)
    {
        return OSIX_FAILURE;
    }

    *pi4RetValFsLldpMedLocMediaPolicyRowStatus = pLldpMedLocPolicy->i4RowStatus;
    return OSIX_SUCCESS;
}

/********************** Remote Tables *******************************/

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlValIndexRemCapInvTbl
 *    DESCRIPTION      : Validates the Index of Remote Capabilities and Inventory Table
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark 
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       i4LldpV2RemIndex - Remote Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination 
 *                       MacAddress
 *    OUTPUT           : NONE
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlValIndexRemCapInvTbl (UINT4 u4LldpV2RemTimeMark,
                                INT4 i4LldpV2RemLocalIfIndex,
                                UINT4 u4LldpV2RemLocalDestMACAddress,
                                INT4 i4LldpV2RemIndex)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        return OSIX_FAILURE;
    }

    if (LldpRxUtlValidateRemTableIndices (u4LldpV2RemTimeMark,
                                          i4LldpV2RemLocalIfIndex,
                                          u4LldpV2RemLocalDestMACAddress,
                                          i4LldpV2RemIndex) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetFirstIndexRemCapTbl
 *
 *    DESCRIPTION      : To get the First Index of Remote Capabilities Table
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : pu4LldpV2RemTimeMark - Time Mark 
 *                       pi4LldpV2RemLocalIfIndex - Interface Index
 *                       pu4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       pi4LldpV2RemIndex - Remote Index
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetFirstIndexRemCapTbl (UINT4 *pu4LldpV2RemTimeMark,
                                  INT4 *pi4LldpV2RemLocalIfIndex,
                                  UINT4 *pu4LldpV2RemLocalDestMACAddress,
                                  INT4 *pi4LldpV2RemIndex)
{
    tLldpRemoteNode    *pRemCapNode = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        return OSIX_FAILURE;
    }

    pRemCapNode = (tLldpRemoteNode *)
        RBTreeGetFirst (gLldpGlobalInfo.RemSysDataRBTree);

    if (pRemCapNode != NULL)
    {
        *pu4LldpV2RemTimeMark = pRemCapNode->u4RemLastUpdateTime;
        *pi4LldpV2RemLocalIfIndex = pRemCapNode->i4RemLocalPortNum;
        *pu4LldpV2RemLocalDestMACAddress = pRemCapNode->u4DestAddrTblIndex;
        *pi4LldpV2RemIndex = pRemCapNode->i4RemIndex;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetNextIndexRemCapTbl 
 *    DESCRIPTION      : To get the Next Index of Remote Capabilities Table
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *    OUTPUT           : pu4NextLldpV2RemTimeMark 
 *                       pi4NextLldpV2RemLocalIfIndex
 *                       pu4NextLldpV2RemLocalDestMACAddress
 *                       pi4NextLldpV2RemIndex
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetNextIndexRemCapTbl (UINT4 u4LldpV2RemTimeMark,
                                 UINT4 *pu4NextLldpV2RemTimeMark,
                                 INT4 i4LldpV2RemLocalIfIndex,
                                 INT4 *pi4NextLldpV2RemLocalIfIndex,
                                 UINT4 u4LldpV2RemLocalDestMACAddress,
                                 UINT4 *pu4NextLldpV2RemLocalDestMACAddress,
                                 INT4 i4LldpV2RemIndex,
                                 INT4 *pi4NextLldpV2RemIndex)
{
    tLldpRemoteNode    *pLldpRemoteNode = NULL;
    tLldpRemoteNode    *pRemoteNode = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        return OSIX_FAILURE;
    }

    if ((pRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_CRITICAL_TRC,
                  "\tMemory Allocation Failure for New pRemoteNode\r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }

    MEMSET (pRemoteNode, 0, sizeof (tLldpRemoteNode));

    pRemoteNode->u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    pRemoteNode->i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    pRemoteNode->u4DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    pRemoteNode->i4RemIndex = i4LldpV2RemIndex;
    pLldpRemoteNode =
        (tLldpRemoteNode *) RBTreeGetNext (gLldpGlobalInfo.RemSysDataRBTree,
                                           (tRBElem *) pRemoteNode, NULL);

    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId, (UINT1 *) pRemoteNode);
    if (pLldpRemoteNode != NULL)
    {
        *pu4NextLldpV2RemTimeMark = pLldpRemoteNode->u4RemLastUpdateTime;
        *pi4NextLldpV2RemLocalIfIndex = pLldpRemoteNode->i4RemLocalPortNum;
        *pu4NextLldpV2RemLocalDestMACAddress =
            pLldpRemoteNode->u4DestAddrTblIndex;
        *pi4NextLldpV2RemIndex = pLldpRemoteNode->i4RemIndex;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;

}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetRemCapSupported
 *
 *    DESCRIPTION      : To Get the supported Capabilities of Remote Node
 *
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *
 *    OUTPUT           : pRetValFsLldpXMedRemCapSupported
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetRemCapSupported (UINT4 u4LldpV2RemTimeMark,
                              INT4 i4LldpV2RemLocalIfIndex,
                              UINT4 u4LldpV2RemLocalDestMACAddress,
                              INT4 i4LldpV2RemIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsLldpXMedRemCapSupported)
{
    tLldpRemoteNode    *pRemCapNode = NULL;
    UINT1               au1InMedRemCapSupported[ISS_SYS_CAPABILITIES_LEN];
    UINT1               au1OutMedRemCapSupported[ISS_SYS_CAPABILITIES_LEN];
    tLldpOctetStringType InTempMedRemCapSupported;
    tLldpOctetStringType OutTempMedRemCapSupported;

    MEMSET (au1InMedRemCapSupported, 0, ISS_SYS_CAPABILITIES_LEN);
    MEMSET (au1OutMedRemCapSupported, 0, ISS_SYS_CAPABILITIES_LEN);

    pRemCapNode = LldpRxUtlGetRemoteNode (u4LldpV2RemTimeMark,
                                          i4LldpV2RemLocalIfIndex,
                                          u4LldpV2RemLocalDestMACAddress,
                                          i4LldpV2RemIndex);
    if (pRemCapNode != NULL)
    {
        InTempMedRemCapSupported.pu1_OctetList = au1InMedRemCapSupported;
        OutTempMedRemCapSupported.pu1_OctetList = au1OutMedRemCapSupported;
        InTempMedRemCapSupported.i4_Length = ISS_SYS_CAPABILITIES_LEN;
        OutTempMedRemCapSupported.i4_Length = ISS_SYS_CAPABILITIES_LEN;
        au1InMedRemCapSupported[1] =
            (UINT1) pRemCapNode->RemMedCapableInfo.u2MedRemCapSupported;

        pRetValFsLldpXMedRemCapSupported->i4_Length =
            sizeof (au1InMedRemCapSupported);
        LldpReverseOctetString (&InTempMedRemCapSupported,
                                &OutTempMedRemCapSupported);

        MEMCPY (pRetValFsLldpXMedRemCapSupported->pu1_OctetList,
                OutTempMedRemCapSupported.pu1_OctetList,
                ISS_SYS_CAPABILITIES_LEN);

        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetRemCapCurrent
 *
 *    DESCRIPTION      : To Get the Current Enabled Capabilities of Remote Node
 *
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *
 *    OUTPUT           : pRetValFsLldpXMedRemCapCurrent
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetRemCapCurrent (UINT4 u4LldpV2RemTimeMark,
                            INT4 i4LldpV2RemLocalIfIndex,
                            UINT4 u4LldpV2RemLocalDestMACAddress,
                            INT4 i4LldpV2RemIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsLldpXMedRemCapCurrent)
{
    tLldpRemoteNode    *pRemCapNode = NULL;
    UINT1               au1InMedRemCapEnable[ISS_SYS_CAPABILITIES_LEN];
    UINT1               au1OutMedRemCapEnable[ISS_SYS_CAPABILITIES_LEN];
    tLldpOctetStringType InTempMedRemCapEnable;
    tLldpOctetStringType OutTempMedRemCapEnable;

    MEMSET (au1InMedRemCapEnable, 0, ISS_SYS_CAPABILITIES_LEN);
    MEMSET (au1OutMedRemCapEnable, 0, ISS_SYS_CAPABILITIES_LEN);

    pRemCapNode = LldpRxUtlGetRemoteNode (u4LldpV2RemTimeMark,
                                          i4LldpV2RemLocalIfIndex,
                                          u4LldpV2RemLocalDestMACAddress,
                                          i4LldpV2RemIndex);
    if (pRemCapNode != NULL)
    {
        InTempMedRemCapEnable.pu1_OctetList = au1InMedRemCapEnable;
        OutTempMedRemCapEnable.pu1_OctetList = au1OutMedRemCapEnable;
        InTempMedRemCapEnable.i4_Length = ISS_SYS_CAPABILITIES_LEN;
        OutTempMedRemCapEnable.i4_Length = ISS_SYS_CAPABILITIES_LEN;
        au1InMedRemCapEnable[1] =
            (UINT1) pRemCapNode->RemMedCapableInfo.u2MedRemCapEnable;

        pRetValFsLldpXMedRemCapCurrent->i4_Length =
            sizeof (au1InMedRemCapEnable);

        LldpReverseOctetString (&InTempMedRemCapEnable,
                                &OutTempMedRemCapEnable);
        MEMCPY (pRetValFsLldpXMedRemCapCurrent->pu1_OctetList,
                OutTempMedRemCapEnable.pu1_OctetList, ISS_SYS_CAPABILITIES_LEN);

        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetRemDeviceClass
 *    DESCRIPTION      : To Get the Device Class of Remote Node
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *    OUTPUT           : pi4RetValFsLldpXMedRemDeviceClass
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetRemDeviceClass (UINT4 u4LldpV2RemTimeMark,
                             INT4 i4LldpV2RemLocalIfIndex,
                             UINT4 u4LldpV2RemLocalDestMACAddress,
                             INT4 i4LldpV2RemIndex,
                             INT4 *pi4RetValFsLldpXMedRemDeviceClass)
{
    tLldpRemoteNode    *pRemDeviceNode = NULL;
    pRemDeviceNode = LldpRxUtlGetRemoteNode (u4LldpV2RemTimeMark,
                                             i4LldpV2RemLocalIfIndex,
                                             u4LldpV2RemLocalDestMACAddress,
                                             i4LldpV2RemIndex);
    if (pRemDeviceNode != NULL)
    {
        *pi4RetValFsLldpXMedRemDeviceClass = (INT4)
            pRemDeviceNode->RemMedCapableInfo.u1MedRemDeviceClass;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetFirstIndexRemInvTbl
 *    DESCRIPTION      : To get the First Index of Remote Inventory Table
 *    INPUT            : NONE
 *    OUTPUT           : pu4LldpV2RemTimeMark - Time Mark
 *                       pi4LldpV2RemLocalIfIndex - Interface Index
 *                       pu4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       pi4LldpV2RemIndex - Remote Index
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetFirstIndexRemInvTbl (UINT4 *pu4LldpV2RemTimeMark,
                                  INT4 *pi4LldpV2RemLocalIfIndex,
                                  UINT4 *pu4LldpV2RemLocalDestMACAddress,
                                  INT4 *pi4LldpV2RemIndex)
{
    tLldpRemoteNode    *pRemInvNode = NULL;
    if (LLDP_IS_SHUTDOWN ())
    {
        return OSIX_FAILURE;
    }

    pRemInvNode = (tLldpRemoteNode *)
        RBTreeGetFirst (gLldpGlobalInfo.RemSysDataRBTree);

    if (pRemInvNode != NULL)
    {
        *pu4LldpV2RemTimeMark = pRemInvNode->u4RemLastUpdateTime;
        *pi4LldpV2RemLocalIfIndex = pRemInvNode->i4RemLocalPortNum;
        *pu4LldpV2RemLocalDestMACAddress = pRemInvNode->u4DestAddrTblIndex;
        *pi4LldpV2RemIndex = pRemInvNode->i4RemIndex;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetNextIndexRemInvTbl
 *    DESCRIPTION      : To get the Next Index of the Remote Inventory Table
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       pLldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *    OUTPUT           : pu4NextLldpV2RemTimeMark
 *                       pi4NextLldpV2RemLocalIfIndex
 *                       pu4NextLldpV2RemLocalDestMACAddress
 *                       pi4NextLldpV2RemIndex
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetNextIndexRemInvTbl (UINT4 u4LldpV2RemTimeMark,
                                 UINT4 *pu4NextLldpV2RemTimeMark,
                                 INT4 i4LldpV2RemLocalIfIndex,
                                 INT4 *pi4NextLldpV2RemLocalIfIndex,
                                 UINT4 u4LldpV2RemLocalDestMACAddress,
                                 UINT4 *pu4NextLldpV2RemLocalDestMACAddress,
                                 INT4 i4LldpV2RemIndex,
                                 INT4 *pi4NextLldpV2RemIndex)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    tLldpRemoteNode    *pLldpRemoteNode = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        return OSIX_FAILURE;
    }

    if ((pRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_CRITICAL_TRC,
                  "\tMemory Allocation Failure for New pRemoteNode\r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }

    MEMSET (pRemoteNode, 0, sizeof (tLldpRemoteNode));

    pRemoteNode->u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    pRemoteNode->i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    pRemoteNode->u4DestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    pRemoteNode->i4RemIndex = i4LldpV2RemIndex;

    pLldpRemoteNode =
        (tLldpRemoteNode *) RBTreeGetNext (gLldpGlobalInfo.RemSysDataRBTree,
                                           (tRBElem *) pRemoteNode, NULL);
    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId, (UINT1 *) pRemoteNode);

    if (pLldpRemoteNode != NULL)
    {
        *pu4NextLldpV2RemTimeMark = pLldpRemoteNode->u4RemLastUpdateTime;
        *pi4NextLldpV2RemLocalIfIndex = pLldpRemoteNode->i4RemLocalPortNum;
        *pi4NextLldpV2RemIndex = pLldpRemoteNode->i4RemIndex;
        *pu4NextLldpV2RemLocalDestMACAddress =
            pLldpRemoteNode->u4DestAddrTblIndex;
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetRemHardwareRev
 *    DESCRIPTION      : To Get the Hardware Revision of Remote Node
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *    OUTPUT           : pRetValFsLldpXMedRemHardwareRev
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetRemHardwareRev (UINT4 u4LldpV2RemTimeMark,
                             INT4 i4LldpV2RemLocalIfIndex,
                             UINT4 u4LldpV2RemLocalDestMACAddress,
                             INT4 i4LldpV2RemIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsLldpXMedRemHardwareRev)
{
    tLldpRemoteNode    *pRemInvNode = NULL;
    pRemInvNode = LldpRxUtlGetRemoteNode (u4LldpV2RemTimeMark,
                                          i4LldpV2RemLocalIfIndex,
                                          u4LldpV2RemLocalDestMACAddress,
                                          i4LldpV2RemIndex);
    if (pRemInvNode != NULL)
    {
        MEMCPY (pRetValFsLldpXMedRemHardwareRev->pu1_OctetList,
                pRemInvNode->RemInventoryInfo.au1MedRemHardwareRev,
                MEM_MAX_BYTES (LLDP_MED_HW_REV_LEN,
                               STRLEN (pRemInvNode->RemInventoryInfo.
                                       au1MedRemHardwareRev)));
        pRetValFsLldpXMedRemHardwareRev->i4_Length =
            (INT4) STRLEN (pRemInvNode->RemInventoryInfo.au1MedRemHardwareRev);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetRemFirmwareRev
 *    DESCRIPTION      : To Get the Firmware Revision of Remote Node
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *    OUTPUT           : pRetValFsLldpXMedRemFirmwareRev
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetRemFirmwareRev (UINT4 u4LldpV2RemTimeMark,
                             INT4 i4LldpV2RemLocalIfIndex,
                             UINT4 u4LldpV2RemLocalDestMACAddress,
                             INT4 i4LldpV2RemIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsLldpXMedRemFirmwareRev)
{
    tLldpRemoteNode    *pRemInvNode = NULL;
    pRemInvNode = LldpRxUtlGetRemoteNode (u4LldpV2RemTimeMark,
                                          i4LldpV2RemLocalIfIndex,
                                          u4LldpV2RemLocalDestMACAddress,
                                          i4LldpV2RemIndex);
    if (pRemInvNode != NULL)
    {
        MEMCPY (pRetValFsLldpXMedRemFirmwareRev->pu1_OctetList,
                pRemInvNode->RemInventoryInfo.au1MedRemFirmwareRev,
                MEM_MAX_BYTES (LLDP_MED_FW_REV_LEN, STRLEN
                               (pRemInvNode->RemInventoryInfo.
                                au1MedRemFirmwareRev)));
        pRetValFsLldpXMedRemFirmwareRev->i4_Length =
            (INT4) STRLEN (pRemInvNode->RemInventoryInfo.au1MedRemFirmwareRev);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetRemSoftwareRev
 *    DESCRIPTION      : To Get the Software Revision of Remote Node
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *    OUTPUT           : pRetValFsLldpXMedRemSoftwareRev
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetRemSoftwareRev (UINT4 u4LldpV2RemTimeMark,
                             INT4 i4LldpV2RemLocalIfIndex,
                             UINT4 u4LldpV2RemLocalDestMACAddress,
                             INT4 i4LldpV2RemIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsLldpXMedRemSoftwareRev)
{
    tLldpRemoteNode    *pRemInvNode = NULL;
    pRemInvNode = LldpRxUtlGetRemoteNode (u4LldpV2RemTimeMark,
                                          i4LldpV2RemLocalIfIndex,
                                          u4LldpV2RemLocalDestMACAddress,
                                          i4LldpV2RemIndex);
    if (pRemInvNode != NULL)
    {
        MEMCPY (pRetValFsLldpXMedRemSoftwareRev->pu1_OctetList,
                pRemInvNode->RemInventoryInfo.au1MedRemSoftwareRev,
                MEM_MAX_BYTES (LLDP_MED_SW_REV_LEN,
                               STRLEN (pRemInvNode->RemInventoryInfo.
                                       au1MedRemSoftwareRev)));
        pRetValFsLldpXMedRemSoftwareRev->i4_Length =
            (INT4) STRLEN (pRemInvNode->RemInventoryInfo.au1MedRemSoftwareRev);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetRemSerialNum
 *    DESCRIPTION      : To Get the Serial Number of Remote Node
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *    OUTPUT           : pRetValFsLldpXMedRemSerialNum 
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetRemSerialNum (UINT4 u4LldpV2RemTimeMark,
                           INT4 i4LldpV2RemLocalIfIndex,
                           UINT4 u4LldpV2RemLocalDestMACAddress,
                           INT4 i4LldpV2RemIndex,
                           tSNMP_OCTET_STRING_TYPE *
                           pRetValFsLldpXMedRemSerialNum)
{
    tLldpRemoteNode    *pRemInvNode = NULL;
    pRemInvNode = LldpRxUtlGetRemoteNode (u4LldpV2RemTimeMark,
                                          i4LldpV2RemLocalIfIndex,
                                          u4LldpV2RemLocalDestMACAddress,
                                          i4LldpV2RemIndex);
    if (pRemInvNode != NULL)
    {
        MEMCPY (pRetValFsLldpXMedRemSerialNum->pu1_OctetList,
                pRemInvNode->RemInventoryInfo.au1MedRemSerialNum,
                MEM_MAX_BYTES (LLDP_MED_SER_NUM_LEN,
                               STRLEN (pRemInvNode->RemInventoryInfo.
                                       au1MedRemSerialNum)));
        pRetValFsLldpXMedRemSerialNum->i4_Length =
            (INT4) STRLEN (pRemInvNode->RemInventoryInfo.au1MedRemSerialNum);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetRemMfgName
 *    DESCRIPTION      : To Get the Manufacturer Name of Remote Node
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *    OUTPUT           : pRetValFsLldpXMedRemMfgName
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetRemMfgName (UINT4 u4LldpV2RemTimeMark,
                         INT4 i4LldpV2RemLocalIfIndex,
                         UINT4 u4LldpV2RemLocalDestMACAddress,
                         INT4 i4LldpV2RemIndex,
                         tSNMP_OCTET_STRING_TYPE * pRetValFsLldpXMedRemMfgName)
{
    tLldpRemoteNode    *pRemInvNode = NULL;
    pRemInvNode = LldpRxUtlGetRemoteNode (u4LldpV2RemTimeMark,
                                          i4LldpV2RemLocalIfIndex,
                                          u4LldpV2RemLocalDestMACAddress,
                                          i4LldpV2RemIndex);
    if (pRemInvNode != NULL)
    {
        MEMCPY (pRetValFsLldpXMedRemMfgName->pu1_OctetList,
                pRemInvNode->RemInventoryInfo.au1MedRemMfgName,
                MEM_MAX_BYTES (LLDP_MED_MFG_NAME_LEN,
                               STRLEN (pRemInvNode->RemInventoryInfo.
                                       au1MedRemMfgName)));
        pRetValFsLldpXMedRemMfgName->i4_Length =
            (INT4) STRLEN (pRemInvNode->RemInventoryInfo.au1MedRemMfgName);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetRemModelName 
 *    DESCRIPTION      : To Get the Model Name of Remote Node
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *    OUTPUT           : pRetValFsLldpXMedRemModelName
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetRemModelName (UINT4 u4LldpV2RemTimeMark,
                           INT4 i4LldpV2RemLocalIfIndex,
                           UINT4 u4LldpV2RemLocalDestMACAddress,
                           INT4 i4LldpV2RemIndex,
                           tSNMP_OCTET_STRING_TYPE *
                           pRetValFsLldpXMedRemModelName)
{
    tLldpRemoteNode    *pRemInvNode = NULL;
    pRemInvNode = LldpRxUtlGetRemoteNode (u4LldpV2RemTimeMark,
                                          i4LldpV2RemLocalIfIndex,
                                          u4LldpV2RemLocalDestMACAddress,
                                          i4LldpV2RemIndex);
    if (pRemInvNode != NULL)
    {
        MEMCPY (pRetValFsLldpXMedRemModelName->pu1_OctetList,
                pRemInvNode->RemInventoryInfo.au1MedRemModelName,
                MEM_MAX_BYTES (LLDP_MED_MODEL_NAME_LEN,
                               STRLEN (pRemInvNode->RemInventoryInfo.
                                       au1MedRemModelName)));
        pRetValFsLldpXMedRemModelName->i4_Length =
            (INT4) STRLEN (pRemInvNode->RemInventoryInfo.au1MedRemModelName);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetRemAssetID
 *    DESCRIPTION      : To Get the AssetID of Remote Node
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *    OUTPUT           : pRetValFsLldpXMedRemAssetID
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetRemAssetID (UINT4 u4LldpV2RemTimeMark,
                         INT4 i4LldpV2RemLocalIfIndex,
                         UINT4 u4LldpV2RemLocalDestMACAddress,
                         INT4 i4LldpV2RemIndex,
                         tSNMP_OCTET_STRING_TYPE * pRetValFsLldpXMedRemAssetID)
{
    tLldpRemoteNode    *pRemInvNode = NULL;
    pRemInvNode = LldpRxUtlGetRemoteNode (u4LldpV2RemTimeMark,
                                          i4LldpV2RemLocalIfIndex,
                                          u4LldpV2RemLocalDestMACAddress,
                                          i4LldpV2RemIndex);
    if (pRemInvNode != NULL)
    {
        MEMCPY (pRetValFsLldpXMedRemAssetID->pu1_OctetList,
                pRemInvNode->RemInventoryInfo.au1MedRemAssetId,
                MEM_MAX_BYTES (LLDP_MED_ASSET_ID_LEN,
                               STRLEN (pRemInvNode->RemInventoryInfo.
                                       au1MedRemAssetId)));
        pRetValFsLldpXMedRemAssetID->i4_Length =
            (INT4) STRLEN (pRemInvNode->RemInventoryInfo.au1MedRemAssetId);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlValIndexRemNwPolTbl
 *
 *    DESCRIPTION      : To Validate the Remote policy Table
 *
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *                       pFsLldpXMedRemMediaPolicyAppType - Application Type
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4 
     
     
     
     
     
     
     
    LldpMedUtlValIndexRemNwPolTbl
    (UINT4 u4LldpV2RemTimeMark,
     INT4 i4LldpV2RemLocalIfIndex,
     UINT4 u4LldpV2RemLocalDestMACAddress,
     INT4 i4LldpV2RemIndex,
     tSNMP_OCTET_STRING_TYPE * pFsLldpXMedRemMediaPolicyAppType)
{

    if (LLDP_IS_SHUTDOWN ())
    {
        return OSIX_FAILURE;
    }

    if (LldpRxUtlValidateRemTableIndices (u4LldpV2RemTimeMark,
                                          i4LldpV2RemLocalIfIndex,
                                          u4LldpV2RemLocalDestMACAddress,
                                          i4LldpV2RemIndex) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (pFsLldpXMedRemMediaPolicyAppType);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetFirstIndexRemNwPolTbl
 *    DESCRIPTION      : To get the First Index of Remote Media Policy Table
 *    INPUT            : NONE
 *    OUTPUT           : pu4LldpV2RemTimeMark - Time Mark
 *                       pi4LldpV2RemLocalIfIndex - Interface Index
 *                       pu4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       pi4LldpV2RemIndex - Remote Index
 *                       pFsLldpXMedRemMediaPolicyAppType - Application Type
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4 
     
     
     
     
     
     
     
    LldpMedUtlGetFirstIndexRemNwPolTbl
    (UINT4 *pu4LldpV2RemTimeMark,
     INT4 *pi4LldpV2RemLocalIfIndex,
     UINT4 *pu4LldpV2RemLocalDestMACAddress,
     INT4 *pi4LldpV2RemIndex,
     tSNMP_OCTET_STRING_TYPE * pFsLldpXMedRemMediaPolicyAppType)
{
    tLldpMedRemNwPolicyInfo *pRemPolicy = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        return OSIX_FAILURE;
    }

    pRemPolicy = (tLldpMedRemNwPolicyInfo *)
        RBTreeGetFirst (gLldpGlobalInfo.LldpMedRemNwPolicyInfoRBTree);
    if (pRemPolicy != NULL)
    {
        *pu4LldpV2RemTimeMark = pRemPolicy->u4RemLastUpdateTime;
        *pi4LldpV2RemLocalIfIndex = pRemPolicy->i4RemLocalPortNum;
        *pu4LldpV2RemLocalDestMACAddress = pRemPolicy->u4RemDestAddrTblIndex;
        *pi4LldpV2RemIndex = pRemPolicy->i4RemIndex;
        *pFsLldpXMedRemMediaPolicyAppType->pu1_OctetList =
            pRemPolicy->u1RemPolicyAppType;
        pFsLldpXMedRemMediaPolicyAppType->i4_Length =
            sizeof (pRemPolicy->u1RemPolicyAppType);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetNextIndexRemNwPolTbl 
 *    DESCRIPTION      : To get the Next Index of the Remote Media Policy Table
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *                       pFsLldpXMedRemMediaPolicyAppType - Application Type
 *
 *    OUTPUT           : pu4NextLldpV2RemTimeMark
 *                       pi4NextLldpV2RemLocalIfIndex
 *                       pu4NextLldpV2RemLocalDestMACAddress
 *                       pi4NextLldpV2RemIndex
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4 
     
     
     
     
     
     
     
    LldpMedUtlGetNextIndexRemNwPolTbl
    (UINT4 u4LldpV2RemTimeMark,
     UINT4 *pu4NextLldpV2RemTimeMark,
     INT4 i4LldpV2RemLocalIfIndex,
     INT4 *pi4NextLldpV2RemLocalIfIndex,
     UINT4 u4LldpV2RemLocalDestMACAddress,
     UINT4 *pu4NextLldpV2RemLocalDestMACAddress,
     INT4 i4LldpV2RemIndex,
     INT4 *pi4NextLldpV2RemIndex,
     tSNMP_OCTET_STRING_TYPE
     * pFsLldpXMedRemMediaPolicyAppType,
     tSNMP_OCTET_STRING_TYPE * pNextFsLldpXMedRemMediaPolicyAppType)
{
    tLldpMedRemNwPolicyInfo *pNextRemPolicy = NULL;
    tLldpMedRemNwPolicyInfo CurrRemPolicy;

    MEMSET (&CurrRemPolicy, 0, sizeof (tLldpMedRemNwPolicyInfo));
    if (LLDP_IS_SHUTDOWN ())
    {
        return OSIX_FAILURE;
    }

    CurrRemPolicy.u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    CurrRemPolicy.i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    CurrRemPolicy.u4RemDestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    CurrRemPolicy.i4RemIndex = i4LldpV2RemIndex;
    CurrRemPolicy.u1RemPolicyAppType =
        *pFsLldpXMedRemMediaPolicyAppType->pu1_OctetList;

    pNextRemPolicy =
        (tLldpMedRemNwPolicyInfo *) RBTreeGetNext (gLldpGlobalInfo.
                                                   LldpMedRemNwPolicyInfoRBTree,
                                                   &CurrRemPolicy, NULL);

    if (pNextRemPolicy != NULL)
    {
        *pu4NextLldpV2RemTimeMark = pNextRemPolicy->u4RemLastUpdateTime;
        *pi4NextLldpV2RemLocalIfIndex = pNextRemPolicy->i4RemLocalPortNum;
        *pu4NextLldpV2RemLocalDestMACAddress =
            pNextRemPolicy->u4RemDestAddrTblIndex;
        *pi4NextLldpV2RemIndex = pNextRemPolicy->i4RemIndex;
        MEMCPY (pNextFsLldpXMedRemMediaPolicyAppType->pu1_OctetList,
                &pNextRemPolicy->u1RemPolicyAppType,
                sizeof (pNextRemPolicy->u1RemPolicyAppType));
        pNextFsLldpXMedRemMediaPolicyAppType->i4_Length =
            sizeof (pNextRemPolicy->u1RemPolicyAppType);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetRemNwPolVlanID
 *    DESCRIPTION      : To get the VlanId of Remote Media Policy table
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *                       pFsLldpXMedRemMediaPolicyAppType - Application Type
 *    OUTPUT           : pi4RetValFsLldpXMedRemMediaPolicyVlanID
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetRemNwPolVlanID (UINT4 u4LldpV2RemTimeMark,
                             INT4 i4LldpV2RemLocalIfIndex,
                             UINT4 u4LldpV2RemLocalDestMACAddress,
                             INT4 i4LldpV2RemIndex,
                             tSNMP_OCTET_STRING_TYPE
                             * pFsLldpXMedRemMediaPolicyAppType,
                             INT4 *pi4RetValFsLldpXMedRemMediaPolicyVlanID)
{
    tLldpMedRemNwPolicyInfo RemPolicy;
    tLldpMedRemNwPolicyInfo *pRemPolicy = NULL;

    MEMSET (&RemPolicy, 0, sizeof (tLldpMedRemNwPolicyInfo));
    RemPolicy.u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    RemPolicy.i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    RemPolicy.u4RemDestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    RemPolicy.i4RemIndex = i4LldpV2RemIndex;
    RemPolicy.u1RemPolicyAppType =
        *pFsLldpXMedRemMediaPolicyAppType->pu1_OctetList;

    pRemPolicy =
        (tLldpMedRemNwPolicyInfo *) RBTreeGet (gLldpGlobalInfo.
                                               LldpMedRemNwPolicyInfoRBTree,
                                               (tRBElem *) & RemPolicy);

    if (pRemPolicy == NULL)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsLldpXMedRemMediaPolicyVlanID =
        (INT4) pRemPolicy->u2RemPolicyVlanId;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetRemNwPolPriority
 *    DESCRIPTION      : To get the Priority of Remote Media Policy table
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *                       pFsLldpXMedRemMediaPolicyAppType - Application Type
 *    OUTPUT           : pi4RetValFsLldpXMedRemMediaPolicyPriority
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetRemNwPolPriority (UINT4 u4LldpV2RemTimeMark,
                               INT4 i4LldpV2RemLocalIfIndex,
                               UINT4 u4LldpV2RemLocalDestMACAddress,
                               INT4 i4LldpV2RemIndex,
                               tSNMP_OCTET_STRING_TYPE
                               * pFsLldpXMedRemMediaPolicyAppType,
                               INT4 *pi4RetValFsLldpXMedRemMediaPolicyPriority)
{
    tLldpMedRemNwPolicyInfo RemPolicy;
    tLldpMedRemNwPolicyInfo *pRemPolicy = NULL;

    MEMSET (&RemPolicy, 0, sizeof (tLldpMedRemNwPolicyInfo));
    RemPolicy.u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    RemPolicy.i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    RemPolicy.u4RemDestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    RemPolicy.i4RemIndex = i4LldpV2RemIndex;
    RemPolicy.u1RemPolicyAppType =
        *pFsLldpXMedRemMediaPolicyAppType->pu1_OctetList;

    pRemPolicy =
        (tLldpMedRemNwPolicyInfo *) RBTreeGet (gLldpGlobalInfo.
                                               LldpMedRemNwPolicyInfoRBTree,
                                               (tRBElem *) & RemPolicy);

    if (pRemPolicy == NULL)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsLldpXMedRemMediaPolicyPriority =
        (INT4) pRemPolicy->u1RemPolicyPriority;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetRemNwPolDscp
 *    DESCRIPTION      : To get the Dscp value of Remote Media Policy table
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *                       pFsLldpXMedRemMediaPolicyAppType - Application Type
 *    OUTPUT           : pi4RetValFsLldpXMedRemMediaPolicyDscp
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetRemNwPolDscp (UINT4 u4LldpV2RemTimeMark,
                           INT4 i4LldpV2RemLocalIfIndex,
                           UINT4 u4LldpV2RemLocalDestMACAddress,
                           INT4 i4LldpV2RemIndex,
                           tSNMP_OCTET_STRING_TYPE
                           * pFsLldpXMedRemMediaPolicyAppType,
                           INT4 *pi4RetValFsLldpXMedRemMediaPolicyDscp)
{
    tLldpMedRemNwPolicyInfo RemPolicy;
    tLldpMedRemNwPolicyInfo *pRemPolicy = NULL;

    MEMSET (&RemPolicy, 0, sizeof (tLldpMedRemNwPolicyInfo));
    RemPolicy.u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    RemPolicy.i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    RemPolicy.u4RemDestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    RemPolicy.i4RemIndex = i4LldpV2RemIndex;
    RemPolicy.u1RemPolicyAppType =
        *pFsLldpXMedRemMediaPolicyAppType->pu1_OctetList;

    pRemPolicy =
        (tLldpMedRemNwPolicyInfo *) RBTreeGet (gLldpGlobalInfo.
                                               LldpMedRemNwPolicyInfoRBTree,
                                               (tRBElem *) & RemPolicy);

    if (pRemPolicy == NULL)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsLldpXMedRemMediaPolicyDscp = (INT4) pRemPolicy->u1RemPolicyDscp;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetRemNwPolTagged 
 *    DESCRIPTION      : To get the VlanType of Remote Media Policy table
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *                       pFsLldpXMedRemMediaPolicyAppType - Application Type
 *    OUTPUT           : pi4RetValFsLldpXMedRemMediaPolicyTagged
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetRemNwPolTagged (UINT4 u4LldpV2RemTimeMark,
                             INT4 i4LldpV2RemLocalIfIndex,
                             UINT4 u4LldpV2RemLocalDestMACAddress,
                             INT4 i4LldpV2RemIndex,
                             tSNMP_OCTET_STRING_TYPE
                             * pFsLldpXMedRemMediaPolicyAppType,
                             INT4 *pi4RetValFsLldpXMedRemMediaPolicyTagged)
{
    tLldpMedRemNwPolicyInfo RemPolicy;
    tLldpMedRemNwPolicyInfo *pRemPolicy = NULL;

    MEMSET (&RemPolicy, 0, sizeof (tLldpMedRemNwPolicyInfo));
    RemPolicy.u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    RemPolicy.i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    RemPolicy.u4RemDestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    RemPolicy.i4RemIndex = i4LldpV2RemIndex;
    RemPolicy.u1RemPolicyAppType =
        *pFsLldpXMedRemMediaPolicyAppType->pu1_OctetList;

    pRemPolicy =
        (tLldpMedRemNwPolicyInfo *) RBTreeGet (gLldpGlobalInfo.
                                               LldpMedRemNwPolicyInfoRBTree,
                                               (tRBElem *) & RemPolicy);

    if (pRemPolicy == NULL)
    {
        return OSIX_FAILURE;
    }

    *pi4RetValFsLldpXMedRemMediaPolicyTagged =
        (INT4) pRemPolicy->bRemPolicyTagged;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetRemNwPolUnknown
 *    DESCRIPTION      : To get the Unknown Policy Flag of Remote Media 
 *                       Policy table
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *                       pFsLldpXMedRemMediaPolicyAppType - Application Type
 *    OUTPUT           : pi4RetValFsLldpXMedRemMediaPolicyUnknown
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetRemNwPolUnknown (UINT4 u4LldpV2RemTimeMark,
                              INT4 i4LldpV2RemLocalIfIndex,
                              UINT4 u4LldpV2RemLocalDestMACAddress,
                              INT4 i4LldpV2RemIndex,
                              tSNMP_OCTET_STRING_TYPE
                              * pFsLldpXMedRemMediaPolicyAppType,
                              INT4 *pi4RetValFsLldpXMedRemMediaPolicyUnknown)
{
    tLldpMedRemNwPolicyInfo RemPolicy;
    tLldpMedRemNwPolicyInfo *pRemPolicy = NULL;

    MEMSET (&RemPolicy, 0, sizeof (tLldpMedRemNwPolicyInfo));
    RemPolicy.u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    RemPolicy.i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    RemPolicy.u4RemDestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    RemPolicy.i4RemIndex = i4LldpV2RemIndex;
    RemPolicy.u1RemPolicyAppType =
        *pFsLldpXMedRemMediaPolicyAppType->pu1_OctetList;

    pRemPolicy =
        (tLldpMedRemNwPolicyInfo *) RBTreeGet (gLldpGlobalInfo.
                                               LldpMedRemNwPolicyInfoRBTree,
                                               (tRBElem *) & RemPolicy);

    if (pRemPolicy == NULL)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsLldpXMedRemMediaPolicyUnknown =
        (INT4) pRemPolicy->bRemPolicyUnKnown;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlValIndexRemLocationTbl
 *
 *    DESCRIPTION      : To Validate the Location Table
 *
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *                       i4LldpXMedRemLocationSubtype - Location subtype
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4 
     
     
     
     
     
     
     
    LldpMedUtlValIndexRemLocationTbl
    (UINT4 u4LldpV2RemTimeMark,
     INT4 i4LldpV2RemLocalIfIndex,
     UINT4 u4LldpV2RemLocalDestMACAddress,
     INT4 i4LldpV2RemIndex, INT4 i4LldpXMedRemLocationSubtype)
{

    if (LLDP_IS_SHUTDOWN ())
    {
        return OSIX_FAILURE;
    }

    if (LldpRxUtlValidateRemTableIndices (u4LldpV2RemTimeMark,
                                          i4LldpV2RemLocalIfIndex,
                                          u4LldpV2RemLocalDestMACAddress,
                                          i4LldpV2RemIndex) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (i4LldpXMedRemLocationSubtype);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetFirstIndexRemLocationTbl
 *    DESCRIPTION      : To get the First Index of Remote Location Table
 *    INPUT            : NONE
 *    OUTPUT           : pu4LldpV2RemTimeMark - Time Mark
 *                       pi4LldpV2RemLocalIfIndex - Interface Index
 *                       pu4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       pi4LldpV2RemIndex - Remote Index
 *                       pi4LldpXMedRemLocationSubtype - Location subtype
 *                       
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4 
     
     
     
     
     
     
     
    LldpMedUtlGetFirstIndexRemLocationTbl
    (UINT4 *pu4LldpV2RemTimeMark,
     INT4 *pi4LldpV2RemLocalIfIndex,
     UINT4 *pu4LldpV2RemLocalDestMACAddress,
     INT4 *pi4LldpV2RemIndex, INT4 *pi4LldpXMedRemLocationSubtype)
{
    tLldpMedRemLocationInfo *pRemLocation = NULL;

    if (LLDP_IS_SHUTDOWN ())
    {
        return OSIX_FAILURE;
    }

    pRemLocation = (tLldpMedRemLocationInfo *)
        RBTreeGetFirst (gLldpGlobalInfo.LldpMedRemLocationInfoRBTree);
    if (pRemLocation != NULL)
    {
        *pu4LldpV2RemTimeMark = pRemLocation->u4RemLastUpdateTime;
        *pi4LldpV2RemLocalIfIndex = pRemLocation->i4RemLocalPortNum;
        *pu4LldpV2RemLocalDestMACAddress = pRemLocation->u4RemDestAddrTblIndex;
        *pi4LldpV2RemIndex = pRemLocation->i4RemIndex;
        *pi4LldpXMedRemLocationSubtype =
            (INT4) pRemLocation->u1RemLocationSubType;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetNextIndexRemLocationTbl
 *    DESCRIPTION      : To get the Next Index of the Remote Location Table
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *                       i4LldpXMedRemLocationSubtype - Location subtype
 *
 *    OUTPUT           : pu4NextLldpV2RemTimeMark
 *                       pi4NextLldpV2RemLocalIfIndex
 *                       pu4NextLldpV2RemLocalDestMACAddress
 *                       pi4NextLldpV2RemIndex
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4 
     
     
     
     
     
     
     
    LldpMedUtlGetNextIndexRemLocationTbl
    (UINT4 u4LldpV2RemTimeMark,
     UINT4 *pu4NextLldpV2RemTimeMark,
     INT4 i4LldpV2RemLocalIfIndex,
     INT4 *pi4NextLldpV2RemLocalIfIndex,
     UINT4 u4LldpV2RemLocalDestMACAddress,
     UINT4 *pu4NextLldpV2RemLocalDestMACAddress,
     INT4 i4LldpV2RemIndex,
     INT4 *pi4NextLldpV2RemIndex,
     INT4 i4LldpXMedRemLocationSubtype, INT4 *pi4NextLldpXMedRemLocationSubtype)
{
    tLldpMedRemLocationInfo *pNextRemLocation = NULL;
    tLldpMedRemLocationInfo CurrRemLocation;

    MEMSET (&CurrRemLocation, 0, sizeof (tLldpMedRemLocationInfo));
    if (LLDP_IS_SHUTDOWN ())
    {
        return OSIX_FAILURE;
    }

    CurrRemLocation.u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    CurrRemLocation.i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    CurrRemLocation.u4RemDestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    CurrRemLocation.i4RemIndex = i4LldpV2RemIndex;
    CurrRemLocation.u1RemLocationSubType = (UINT1) i4LldpXMedRemLocationSubtype;

    pNextRemLocation =
        (tLldpMedRemLocationInfo *) RBTreeGetNext (gLldpGlobalInfo.
                                                   LldpMedRemLocationInfoRBTree,
                                                   &CurrRemLocation, NULL);

    if (pNextRemLocation != NULL)
    {
        *pu4NextLldpV2RemTimeMark = pNextRemLocation->u4RemLastUpdateTime;
        *pi4NextLldpV2RemLocalIfIndex = pNextRemLocation->i4RemLocalPortNum;
        *pu4NextLldpV2RemLocalDestMACAddress =
            pNextRemLocation->u4RemDestAddrTblIndex;
        *pi4NextLldpV2RemIndex = pNextRemLocation->i4RemIndex;
        *pi4NextLldpXMedRemLocationSubtype =
            (INT4) pNextRemLocation->u1RemLocationSubType;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetRemLocationInfo
 *    DESCRIPTION      : To get the Location Info of Remote Location table
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *                       i4LldpXMedRemLocationSubtype - Location subtype
 *    OUTPUT           : pRetValLldpXMedRemLocationInfo
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetRemLocationInfo (UINT4 u4LldpV2RemTimeMark,
                              INT4 i4LldpV2RemLocalIfIndex,
                              UINT4 u4LldpV2RemLocalDestMACAddress,
                              INT4 i4LldpV2RemIndex,
                              INT4 i4LldpXMedRemLocationSubtype,
                              tSNMP_OCTET_STRING_TYPE
                              * pRetValLldpXMedRemLocationInfo)
{
    tLldpMedRemLocationInfo RemLocation;
    tLldpMedRemLocationInfo *pRemLocation = NULL;

    MEMSET (&RemLocation, 0, sizeof (tLldpMedRemLocationInfo));
    RemLocation.u4RemLastUpdateTime = u4LldpV2RemTimeMark;
    RemLocation.i4RemLocalPortNum = i4LldpV2RemLocalIfIndex;
    RemLocation.u4RemDestAddrTblIndex = u4LldpV2RemLocalDestMACAddress;
    RemLocation.i4RemIndex = i4LldpV2RemIndex;
    RemLocation.u1RemLocationSubType = (UINT1) i4LldpXMedRemLocationSubtype;

    pRemLocation =
        (tLldpMedRemLocationInfo *) RBTreeGet (gLldpGlobalInfo.
                                               LldpMedRemLocationInfoRBTree,
                                               (tRBElem *) & RemLocation);

    if (pRemLocation == NULL)
    {
        return OSIX_FAILURE;
    }
    if (i4LldpXMedRemLocationSubtype == LLDP_MED_COORDINATE_LOC)
    {
        MEMCPY (pRetValLldpXMedRemLocationInfo->pu1_OctetList,
                pRemLocation->au1RemLocationID,
                LLDP_MED_MAX_COORDINATE_LOC_LENGTH);
        pRetValLldpXMedRemLocationInfo->i4_Length =
            (INT4) LLDP_MED_MAX_COORDINATE_LOC_LENGTH;

    }
    else if (i4LldpXMedRemLocationSubtype == LLDP_MED_CIVIC_LOC)
    {
        MEMCPY (pRetValLldpXMedRemLocationInfo->pu1_OctetList,
                pRemLocation->au1RemLocationID,
                (pRemLocation->au1RemLocationID[0] + 1));
        pRetValLldpXMedRemLocationInfo->i4_Length =
            (INT4) (pRemLocation->au1RemLocationID[0] + 1);
    }
    else
    {
        MEMCPY (pRetValLldpXMedRemLocationInfo->pu1_OctetList,
                pRemLocation->au1RemLocationID,
                STRLEN (pRemLocation->au1RemLocationID));
        pRetValLldpXMedRemLocationInfo->i4_Length =
            (INT4) STRLEN (pRemLocation->au1RemLocationID);

    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetRemPoEDeviceType
 *
 *    DESCRIPTION      : To Get the PoE Device Type of Remote Node
 *
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *
 *    OUTPUT           : pi4RetValLldpXMedRemXPoEDeviceType
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetRemPoEDeviceType (UINT4 u4LldpV2RemTimeMark,
                               INT4 i4LldpV2RemLocalIfIndex,
                               UINT4 u4LldpV2RemLocalDestMACAddress,
                               INT4 i4LldpV2RemIndex,
                               INT4 *pi4RetValLldpXMedRemXPoEDeviceType)
{
    tLldpRemoteNode    *pRemPoeNode = NULL;

    pRemPoeNode = LldpRxUtlGetRemoteNode (u4LldpV2RemTimeMark,
                                          i4LldpV2RemLocalIfIndex,
                                          u4LldpV2RemLocalDestMACAddress,
                                          i4LldpV2RemIndex);
    if (pRemPoeNode == NULL)
    {
        return OSIX_FAILURE;
    }
    if (pRemPoeNode->RemMedCapableInfo.
        u2MedRemCapEnable & LLDP_MED_PW_MDI_PD_TLV)
    {
        if (pRemPoeNode->RemMedPowerMDIInfo.u1PoEDeviceType ==
            LLDP_MED_PSE_DEVICE)
        {
            *pi4RetValLldpXMedRemXPoEDeviceType = LLDP_MED_PSE;
        }
        else if (pRemPoeNode->RemMedPowerMDIInfo.u1PoEDeviceType ==
                 LLDP_MED_PD_DEVICE)
        {
            *pi4RetValLldpXMedRemXPoEDeviceType = LLDP_MED_PD;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetRemPoEPDPowerReq
 *
 *    DESCRIPTION      : To Get the PoEPD Power request of Remote Node
 *
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *
 *    OUTPUT           : pu4RetValLldpXMedRemXPoEPDPowerReq
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetRemPoEPDPowerReq (UINT4 u4LldpV2RemTimeMark,
                               INT4 i4LldpV2RemLocalIfIndex,
                               UINT4 u4LldpV2RemLocalDestMACAddress,
                               INT4 i4LldpV2RemIndex,
                               UINT4 *pu4RetValLldpXMedRemXPoEPDPowerReq)
{
    tLldpRemoteNode    *pRemPdNode = NULL;

    pRemPdNode = LldpRxUtlGetRemoteNode (u4LldpV2RemTimeMark,
                                         i4LldpV2RemLocalIfIndex,
                                         u4LldpV2RemLocalDestMACAddress,
                                         i4LldpV2RemIndex);
    if (pRemPdNode == NULL)
    {
        return OSIX_FAILURE;
    }
    if (pRemPdNode->RemMedCapableInfo.
        u2MedRemCapEnable & LLDP_MED_PW_MDI_PD_TLV)
    {
        *pu4RetValLldpXMedRemXPoEPDPowerReq =
            (UINT4) pRemPdNode->RemMedPowerMDIInfo.u2PowerValue;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetRemPoEPDPowerSource
 *
 *    DESCRIPTION      : To Get the PoEPD Power Source of Remote Node
 *
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *
 *    OUTPUT           : pu4RetValLldpXMedRemXPoEPDPowerSource
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetRemPoEPDPowerSource (UINT4 u4LldpV2RemTimeMark,
                                  INT4 i4LldpV2RemLocalIfIndex,
                                  UINT4 u4LldpV2RemLocalDestMACAddress,
                                  INT4 i4LldpV2RemIndex,
                                  INT4 *pi4RetValLldpXMedRemXPoEPDPowerSource)
{
    tLldpRemoteNode    *pRemPdNode = NULL;

    pRemPdNode = LldpRxUtlGetRemoteNode (u4LldpV2RemTimeMark,
                                         i4LldpV2RemLocalIfIndex,
                                         u4LldpV2RemLocalDestMACAddress,
                                         i4LldpV2RemIndex);
    if (pRemPdNode == NULL)
    {
        return OSIX_FAILURE;
    }
    if (pRemPdNode->RemMedCapableInfo.
        u2MedRemCapEnable & LLDP_MED_PW_MDI_PD_TLV)
    {
        if (pRemPdNode->RemMedPowerMDIInfo.u1PowerSource == LLDP_MED_PD_PSE)
        {
            *pi4RetValLldpXMedRemXPoEPDPowerSource = LLDP_MED_PD_PSE_MIB;
        }
        else if (pRemPdNode->RemMedPowerMDIInfo.u1PowerSource ==
                 LLDP_MED_PD_LOCAL)
        {
            *pi4RetValLldpXMedRemXPoEPDPowerSource = LLDP_MED_PD_LOCAL_MIB;
        }
        else if (pRemPdNode->RemMedPowerMDIInfo.u1PowerSource ==
                 LLDP_MED_PD_PSE_LOCAL)
        {
            *pi4RetValLldpXMedRemXPoEPDPowerSource = LLDP_MED_PD_PSE_LOCAL_MIB;
        }
        else if (pRemPdNode->RemMedPowerMDIInfo.u1PowerSource ==
                 LLDP_MED_PD_UNKNOWN)
        {
            *pi4RetValLldpXMedRemXPoEPDPowerSource = LLDP_MED_PD_UNKNOWN_MIB;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlGetRemPoEPDPowerPriority
 *
 *    DESCRIPTION      : To Get the PoEPD Power Priority of Remote Node
 *
 *    INPUT            : u4LldpV2RemTimeMark - Time Mark
 *                       i4LldpV2RemLocalIfIndex - Interface Index
 *                       u4LldpV2RemLocalDestMACAddress - Destination
 *                       MacAddress
 *                       i4LldpV2RemIndex - Remote Index
 *
 *    OUTPUT           : pu4RetValLldpXMedRemXPoEPDPowerPriority
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
INT4
LldpMedUtlGetRemPoEPDPowerPriority (UINT4 u4LldpV2RemTimeMark,
                                    INT4 i4LldpV2RemLocalIfIndex,
                                    UINT4 u4LldpV2RemLocalDestMACAddress,
                                    INT4 i4LldpV2RemIndex,
                                    INT4
                                    *pi4RetValLldpXMedRemXPoEPDPowerPriority)
{
    tLldpRemoteNode    *pRemPdNode = NULL;

    pRemPdNode = LldpRxUtlGetRemoteNode (u4LldpV2RemTimeMark,
                                         i4LldpV2RemLocalIfIndex,
                                         u4LldpV2RemLocalDestMACAddress,
                                         i4LldpV2RemIndex);
    if (pRemPdNode == NULL)
    {
        return OSIX_FAILURE;
    }
    if (pRemPdNode->RemMedCapableInfo.
        u2MedRemCapEnable & LLDP_MED_PW_MDI_PD_TLV)
    {
        if (pRemPdNode->RemMedPowerMDIInfo.u1PowerPriority ==
            LLDP_MED_PRI_CRITICAL)
        {
            *pi4RetValLldpXMedRemXPoEPDPowerPriority =
                LLDP_MED_PRI_CRITICAL_MIB;
        }
        else if (pRemPdNode->RemMedPowerMDIInfo.u1PowerPriority ==
                 LLDP_MED_PRI_HIGH)
        {
            *pi4RetValLldpXMedRemXPoEPDPowerPriority = LLDP_MED_PRI_HIGH_MIB;
        }
        else if (pRemPdNode->RemMedPowerMDIInfo.u1PowerPriority ==
                 LLDP_MED_PRI_LOW)
        {
            *pi4RetValLldpXMedRemXPoEPDPowerPriority = LLDP_MED_PRI_LOW_MIB;
        }
        else if (pRemPdNode->RemMedPowerMDIInfo.u1PowerPriority ==
                 LLDP_MED_PRI_UNKNOWN)
        {
            *pi4RetValLldpXMedRemXPoEPDPowerPriority = LLDP_MED_PRI_UNKNOWN_MIB;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlBitArrayShift
 *
 *    DESCRIPTION      : To Shift the array of bits based on input
 *
 *    INPUT            : pau1loc - Location Info
 *                       u1pos   - Number of bits to shift
 *                       u1count - Number of array elements to shift
 *
 *    OUTPUT           : pau1loc
 *
 *    RETURNS          : None
 ****************************************************************************/
PUBLIC VOID
LldpMedUtlBitArrayShift (UINT1 *pau1loc, UINT1 u1pos, UINT1 u1count)
{

    UINT1              *pu1from = NULL;
    UINT1              *pu1to = NULL;
    UINT1               u1Temp = 0;

    pu1from = pau1loc;
    pu1to = pau1loc;

    while (u1count-- > 1)
    {
        u1Temp = (UINT1) (*pu1from << u1pos);
        *pu1to++ = (UINT1) (u1Temp | (*++pu1from >> (8 - u1pos)));
    };
    *pu1to = (UINT1) (*pu1from << u1pos);

}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlCoordinateLocEncode
 *
 *    DESCRIPTION      : To encode Coordinate atributes to form LCI data format
 *
 *    INPUT            : u1latitude      - Latitude
 *                       u1latres        - Latitude-resolution
 *                       u1longitude     - Longitude
 *                       u1longres       - Longitude-resolution
 *                       u1alttype       - Altitude-type
 *                       u1altitude      - Altitude
 *                       u1altres        - Altitude-resolution
 *                       u1datum         - Datum
 *
 *    OUTPUT           : au1lat
 *
 *    RETURNS          : None
 ****************************************************************************/
PUBLIC VOID
LldpMedUtlCoordinateLocEncode (UINT1 *u1latitude, UINT1 u1latres,
                               UINT1 *u1longitude, UINT1 u1longres,
                               UINT1 u1alttype, UINT1 *u1altitude,
                               UINT1 u1altres, UINT1 u1datum, UINT1 au1lat[])
{
    FS_UINT8            u8val;
    DBL8                f4coord = 0;
    AR_UINT8            u8coord = 0;
    UINT4               u4coord = 0;
    UINT1               u1pos = 0;
    UINT1               u1count = 0;
    UINT1               u1coord = 0;

    /* Encode the attributes to form Coordinate Location LCI data format
       ------------------------------------------------------
       | LaRes     |            Latitude                     |
       | (6bit)    |            (34 bit)                     |
       ----------------------------------------------------- |

       ------------------------------------------------------|
       |  AT    | AltRes  |     Altitude                     |
       | (4 bit)| (6 bit) |     (34 bit)                     |
       ------------------------------------------------------|
       |     Datum     |
       ----------------
       |<---1 Octet--->|<------------4 Octets ---------------> */

    MEMSET (&u8val, 0, sizeof (FS_UINT8));
    au1lat[0] = (UINT1) (u1latres << 2);
    /* Convert latitude to 2's complement, 34-bit fixed point,
       9 bits decimal and 25 bits fractional part */
    if (*u1latitude == '-')
    {
        u1latitude++;
        SSCANF ((CONST CHR1 *) u1latitude, "%lf", &f4coord);
        u8coord = (AR_UINT8) ((pow (2, 34) - (f4coord * pow (2, 25))));

    }
    else
    {
        SSCANF ((CONST CHR1 *) u1latitude, "%lf", &f4coord);
        u8coord = (AR_UINT8) (f4coord * pow (2, 25));
    }
    MEMCPY (&u8val, &u8coord, sizeof (FS_UINT8));
    MEMCPY (&u1coord, &(u8val.u4Lo), sizeof (UINT1));
    /* Store Latitude-resolution */
    au1lat[0] = au1lat[0] | u1coord;
    u8val.u4Hi = OSIX_HTONL (u8val.u4Hi);
    /* Store Latitude */
    MEMCPY (&au1lat[1], &(u8val.u4Hi), sizeof (UINT4));
    au1lat[5] = (UINT1) (u1longres << 2);
    /* Convert longitude to 2's complement, 34-bit fixed point,
       9 bits decimal and 25 bits fractional part */
    if (*u1longitude == '-')
    {
        u1longitude++;
        SSCANF ((CONST CHR1 *) u1longitude, "%lf", &f4coord);
        u8coord = (AR_UINT8) (pow (2, 34) - (f4coord * pow (2, 25)));

    }
    else
    {
        SSCANF ((CONST CHR1 *) u1longitude, "%lf", &f4coord);
        u8coord = (AR_UINT8) (f4coord * pow (2, 25));
    }
    MEMCPY (&u8val, &u8coord, sizeof (FS_UINT8));
    MEMCPY (&u1coord, &(u8val.u4Lo), sizeof (UINT1));
    /* Store Longitude-resolution */
    au1lat[5] = au1lat[5] | u1coord;
    u8val.u4Hi = OSIX_HTONL (u8val.u4Hi);
    /* Store Longitude */
    MEMCPY (&au1lat[6], &(u8val.u4Hi), sizeof (UINT4));
    /* Store altitude type */
    au1lat[10] = u1alttype;
    /* Store altitude res */
    au1lat[11] = u1altres;
    /* Convert altitude to 2's complement, 30-bit fixed point,
       22 bits decimal and 8 bits fractional part */
    if (*u1altitude == '-')
    {
        u1altitude++;
        SSCANF ((CONST CHR1 *) u1altitude, "%lf", &f4coord);
        u4coord = (UINT4) (pow (2, 30) - (f4coord * pow (2, 8)));

    }
    else
    {
        SSCANF ((CONST CHR1 *) u1altitude, "%lf", &f4coord);
        u4coord = (UINT4) (f4coord * pow (2, 8));
    }
    u4coord = OSIX_HTONL (u4coord);
    /* Store altitude */
    MEMCPY (&au1lat[12], &(u4coord), sizeof (UINT4));
    /* Do bit shifting in array au1lat to get LCI data format */
    u1pos = 2;                    /* number of bits in a byte to shift */
    u1count = 4;                /* number of bytes to shift in au1lat */
    LldpMedUtlBitArrayShift (&au1lat[12], u1pos, u1count);
    u1pos = 2;
    u1count = 5;
    LldpMedUtlBitArrayShift (&au1lat[11], u1pos, u1count);
    u1pos = 4;
    u1count = 6;
    LldpMedUtlBitArrayShift (&au1lat[10], u1pos, u1count);
    au1lat[15] = u1datum;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlCoordinateLocDecode
 *
 *    DESCRIPTION      : To decodethe Coordinate atributes from LCI data format
 *
 *    INPUT            : au1LocationId   - Coordinate LCI data format
 *
 *    OUTPUT           : u1latitude      - Latitude
 *                       u1latres        - Latitude-resolution
 *                       u1longitude     - Longitude
 *                       u1longres       - Longitude-resolution
 *                       u1alttype       - Altitude-type
 *                       u1altitude      - Altitude
 *                       u1altres        - Altitude-resolution
 *                       u1datum         - Datum
 *
 *    RETURNS          : None
 ****************************************************************************/
PUBLIC VOID
LldpMedUtlCoordinateLocDecode (UINT1 *u1latres, DBL8 * d8latitude,
                               UINT1 *u1longres, DBL8 * d8longitude,
                               UINT1 *u1alttype, UINT1 *u1altres,
                               DBL8 * d8altitude, UINT1 *u1datum,
                               UINT1 au1LocationId[])
{
    AR_UINT8            u8coord = 0;
    UINT1               u1temp = 0;
    UINT4               u4coord = 0;

    /* Decode the attributes from Coordinate Location LCI data format
       ------------------------------------------------------
       | LaRes     |            Latitude                     |
       | (6bit)    |            (34 bit)                     |
       ----------------------------------------------------- |
       | LoRes     |            Longitude                    |
       | {6 bit)   |            (34 bit)                     |
       ------------------------------------------------------|
       |  AT    | AltRes  |     Altitude                     |
       | (4 bit)| (6 bit) |     (34 bit)                     |
       ------------------------------------------------------|
       |     Datum     |
       ----------------
       |<---1 Octet--->|<------------4 Octets ---------------> */

    /* check sign-bit of latitude */
    if (au1LocationId[0] & 2)
    {
        *d8latitude = -1;
    }
    else
    {
        *d8latitude = 0;
    }
    /* Get Latitude-resolution */
    *u1latres = au1LocationId[0] >> 2;
    au1LocationId[0] = au1LocationId[0] & 3;
    u1temp = au1LocationId[0];
    au1LocationId[0] = au1LocationId[4];
    au1LocationId[4] = u1temp;
    u1temp = au1LocationId[1];
    au1LocationId[1] = au1LocationId[3];
    au1LocationId[3] = u1temp;
    memcpy (&u8coord, &au1LocationId[0], 5);
    /* Get Latitude value from 34-bit fixed point */
    if (*d8latitude == -1)
    {
        *d8latitude =
            (DBL8) ((*d8latitude) * (pow (2, 34) - u8coord) / (pow (2, 25)));
    }
    else
    {
        *d8latitude = (DBL8) (u8coord / pow (2, 25));
    }
    /* check sign-bit of longitude */
    if (au1LocationId[5] & 2)
    {
        *d8longitude = -1;
    }
    else
    {
        *d8longitude = 0;
    }
    u8coord = 0;
    *u1longres = au1LocationId[5] >> 2;
    au1LocationId[5] = au1LocationId[5] & 3;
    u1temp = au1LocationId[5];
    au1LocationId[5] = au1LocationId[9];
    au1LocationId[9] = u1temp;
    u1temp = au1LocationId[6];
    au1LocationId[6] = au1LocationId[8];
    au1LocationId[8] = u1temp;
    memcpy (&u8coord, &au1LocationId[5], 5);
    /* Get Longitude value from 34-bit fixed point */
    if (*d8longitude == -1)
    {
        *d8longitude =
            (DBL8) ((*d8longitude) * (pow (2, 34) - u8coord) / (pow (2, 25)));
    }
    else
    {
        *d8longitude = (DBL8) (u8coord / pow (2, 25));
    }
    /* check sign-bit of altitude */
    if (au1LocationId[11] & (1 << 5))
    {
        *d8altitude = -1;
    }
    else
    {
        *d8altitude = 0;
    }
    /* Get altitude type value */
    *u1alttype = au1LocationId[10] >> 4;
    au1LocationId[10] = au1LocationId[10] & 0xF;
    au1LocationId[10] =
        (UINT1) ((au1LocationId[10] << 2) | (au1LocationId[11] >> 6));
    /* Get altitude-resolution value */
    *u1altres = au1LocationId[10];
    au1LocationId[11] = au1LocationId[11] & 0x3F;
    memcpy (&u4coord, &au1LocationId[11], 4);
    u4coord = OSIX_HTONL (u4coord);
    /* Get altitude value from 30-bit fixed point */
    if (*d8altitude == -1)
    {
        *d8altitude = (*d8altitude) * (pow (2, 30) - u4coord) / (pow (2, 8));
    }
    else
    {
        *d8altitude = (u4coord / pow (2, 8));
    }
    /* Get datum value */
    *u1datum = au1LocationId[15];
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlCaTypeToStr
 *
 *    DESCRIPTION      : This function is used to convert the CA type integer
 *                       to the corresponding string.
 *
 *    INPUT            : u1CaType        -  CA type integer
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : CA type string
 ****************************************************************************/
PUBLIC CONST CHR1  *
LldpMedUtlCaTypeToStr (UINT1 u1CaType)
{
    switch (u1CaType)
    {

        case LLDP_MED_CA_TYPE_LANG:

            return "Language";

        case LLDP_MED_CA_TYPE_STATE:

            return "State";

        case LLDP_MED_CA_TYPE_DISTRICT:

            return "District";

        case LLDP_MED_CA_TYPE_CITY:

            return "City";

        case LLDP_MED_CA_TYPE_CITY_DIVISION:

            return "City Division";

        case LLDP_MED_CA_TYPE_BLOCK:

            return "Block";

        case LLDP_MED_CA_TYPE_STREET:

            return "Street";

        case LLDP_MED_CA_TYPE_LEAD_STREET_DIR:

            return "Street Lead Direction";

        case LLDP_MED_CA_TYPE_TRAIL_STREET_DIR:

            return "Street Trail Direction";

        case LLDP_MED_CA_TYPE_STREET_SUFFIX:

            return "Street Suffix";

        case LLDP_MED_CA_TYPE_HOUSE_NO:

            return "House Number";

        case LLDP_MED_CA_TYPE_HOUSE_NO_SUFFIX:

            return "House Number Suffix";

        case LLDP_MED_CA_TYPE_LANDMARK:

            return "Landmark";

        case LLDP_MED_CA_TYPE_ADD_LOC_INFO:

            return "Additinal Location Info";

        case LLDP_MED_CA_TYPE_NAME:

            return "Name";

        case LLDP_MED_CA_TYPE_PINCODE:

            return "Pincode";

        case LLDP_MED_CA_TYPE_BUILDING:

            return "Building";

        case LLDP_MED_CA_TYPE_APARTMENT:

            return "Apartment";

        case LLDP_MED_CA_TYPE_FLOOR:

            return "Floor";

        case LLDP_MED_CA_TYPE_ROOM_NUMBER:

            return "Room Number";

        case LLDP_MED_CA_TYPE_PLACE_TYPE:

            return "Place Type";

        case LLDP_MED_CA_TYPE_POSTAL_NAME:

            return "Postal Name";

        case LLDP_MED_CA_TYPE_POST_BOX_NUMBER:

            return "Post Box Number";

        case LLDP_MED_CA_TYPE_ADDITIONAL_CODE:

            return "Additional Code";

        case LLDP_MED_CA_TYPE_SCRIPT:

            return "Script";

        default:

            return NULL;
    }

}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlRowStatusToStr
 *
 *    DESCRIPTION      : This function is used to convert the row status
 *                       integer to corresponding string
 *
 *
 *    INPUT            : i4RowStatus        -  Row status integer
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : Row Status string
 ****************************************************************************/

PUBLIC CONST CHR1  *
LldpMedUtlRowStatusToStr (INT4 i4RowStatus)
{
    switch (i4RowStatus)
    {

        case ACTIVE:

            return "Active";

        case NOT_IN_SERVICE:

            return "Inactive";

        default:

            return NULL;
    }
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtlCivicWhatToStr
 *
 *    DESCRIPTION      : This function is used to convert the Civic What field
 *                       integer to corresponding string
 *
 *    INPUT            : u1What        -  Civivc What field integer
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : Civivc What field string
 ****************************************************************************/
PUBLIC CONST CHR1  *
LldpMedUtlCivicWhatToStr (UINT1 u1What)
{
    switch (u1What)
    {

        case LLDP_MED_CIVIC_DHCP_SERVER:

            return "Location of the DHCP server";

        case LLDP_MED_CIVIC_CLOSE_DEVICE:

            return "Location of the closest network device";

        case LLDP_MED_CIVIC_CLIENT:

            return "Location of the client";

        default:

            return NULL;
    }
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedGetAdminStatus
 *
 *    DESCRIPTION      : This function is used to get the admin status of LLDP-MED
 *
 *    INPUT            : i4LldpV2PortConfigIfIndex - Port Number
 *
 *    OUTPUT           : pi4AdminStatus - LLDP-MED Admin Status
 *
 *    RETURNS          : NONE
 ****************************************************************************/
PUBLIC VOID
LldpMedGetAdminStatus (INT4 i4LldpV2PortConfigIfIndex, INT4 *pi4AdminStatus)
{
    if (LLDP_IS_SHUTDOWN ())
    {
        *pi4AdminStatus = (INT4) LLDP_DISABLED;
        return;
    }
    tLldpLocPortTable  *pLocPortTable = NULL;
    tLldpLocPortTable   LocPortTable;

    LocPortTable.i4IfIndex = i4LldpV2PortConfigIfIndex;
    pLocPortTable = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LocPortTable);
    if (pLocPortTable == NULL)
    {
        return;
    }
    *pi4AdminStatus = pLocPortTable->i4MedAdminStatus;
    return;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedHandleAdminStatusChng
 *
 *    DESCRIPTION      : This function is used to reset Med Capable flag in
 *                       particular port when the admin status is disabled.
 *                       and also reconstruct the buffer when the admin status
 *                       is enabled on port.
 *
 *    INPUT            : i4LldpLocPortNum - Port Number
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 ****************************************************************************/

PUBLIC VOID
LldpMedHandleAdminStatusChng (INT4 i4LldpLocPortNum, INT4 i4MedAdminStatus)
{
    tLldpv2AgentToLocPort Lldpv2AgentToLocPort;
    tLldpv2AgentToLocPort *pLldpv2AgentToLocPort = NULL;
    tLldpLocPortInfo   *pLocPortInfo = NULL;

    MEMSET (&Lldpv2AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
    Lldpv2AgentToLocPort.i4IfIndex = i4LldpLocPortNum;

    pLldpv2AgentToLocPort = (tLldpv2AgentToLocPort *)
        RBTreeGetNext (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                       (tRBElem *) & Lldpv2AgentToLocPort, NULL);
    while ((pLldpv2AgentToLocPort != NULL)
           && (pLldpv2AgentToLocPort->i4IfIndex == i4LldpLocPortNum))
    {
        pLocPortInfo =
            LLDP_GET_LOC_PORT_INFO (pLldpv2AgentToLocPort->u4LldpLocPort);
        if (pLocPortInfo == NULL)
        {
            LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                      "LldpMedUtilDisableMedFlag: "
                      "Unable to get the Port Info \r\n");
            return;
        }
        if (i4MedAdminStatus == LLDP_ENABLED)
        {
            /* If LLDP-MED Admin Status is enabled, Check for Med Capable Flag whether
             * it is enabled or not. If the Flag is enabled, reconstruct the buffer. Else the buffer
             * reconstruction will happen when the MedCapable Flag is set to True. */
            if (pLocPortInfo->bMedCapable == LLDP_MED_TRUE)
            {
                /*Change in local sys info */
                LldpTxUtlHandleLocSysInfoChg ();
            }
        }
        else if (i4MedAdminStatus == LLDP_DISABLED)
        {
            /* If LLDP-MED Admin Status is disabled, Set the MedCapable Flag as FALSE
             * and reconstruct the buffer only if the MedCapable flag is set to TRUE.*/
            if (pLocPortInfo->bMedCapable == LLDP_MED_TRUE)
            {
                pLocPortInfo->bMedCapable = LLDP_MED_FALSE;
                /*Change in local sys info */
                LldpTxUtlHandleLocSysInfoChg ();
            }
        }

        MEMSET (&Lldpv2AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
        Lldpv2AgentToLocPort.i4IfIndex = pLldpv2AgentToLocPort->i4IfIndex;
        MEMCPY (Lldpv2AgentToLocPort.Lldpv2DestMacAddress,
                pLldpv2AgentToLocPort->Lldpv2DestMacAddress, MAC_ADDR_LEN);
        pLldpv2AgentToLocPort =
            (tLldpv2AgentToLocPort *) RBTreeGetNext (gLldpGlobalInfo.
                                                     Lldpv2AgentToLocPortMapTblRBTree,
                                                     (tRBElem *) &
                                                     Lldpv2AgentToLocPort,
                                                     NULL);

    }
    if (i4MedAdminStatus == LLDP_DISABLED)
    {
        LldpMedUtilDelRemMedTlvsInfo (i4LldpLocPortNum);
    }
    return;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpMedUtilDelRemMedTlvsInfo
 *
 *    DESCRIPTION      : This function is to delete the LLDP-MED neighbor
 *                       information learned on the particular port
 *
 *    INPUT            : i4LldpLocPortNum - Port Number
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 ****************************************************************************/
PUBLIC VOID
LldpMedUtilDelRemMedTlvsInfo (INT4 i4LldpLocPortNum)
{
    tLldpRemoteNode    *pRemInfo = NULL;
    tLldpRemoteNode    *pNextRemInfo = NULL;

    if ((pRemInfo = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC, "LldpMedUtilDelRemMedTlvsInfo:"
                  "Failed to Allocate Memory for Remote Node\r\n");
        return;
    }

    /* Get the pointer to the Remote Node with the given indices */
    MEMSET (pRemInfo, 0, sizeof (tLldpRemoteNode));

    pRemInfo->i4RemLocalPortNum = i4LldpLocPortNum;
    pNextRemInfo =
        (tLldpRemoteNode *) RBTreeGetNext (gLldpGlobalInfo.RemSysDataRBTree,
                                           (tRBElem *) pRemInfo, NULL);
    while ((pNextRemInfo != NULL) &&
           (i4LldpLocPortNum == pNextRemInfo->i4RemLocalPortNum))
    {
        if (pNextRemInfo->RemMedCapableInfo.u2MedRemCapEnable != 0)
        {
            MEMSET (&pNextRemInfo->RemMedCapableInfo, 0,
                    sizeof (tLldpMedRemCapInfo));
            MEMSET (&pNextRemInfo->RemInventoryInfo, 0,
                    sizeof (tLldpMedRemInventoryInfo));
            MEMSET (&pNextRemInfo->RemMedPowerMDIInfo, 0,
                    sizeof (tLldpMedRemPowerMDIInfo));
            LldpMedRxUtlDelRemPolicyInfo (pNextRemInfo);
            LldpMedRxUtlDelRemLocationInfo (pNextRemInfo);
        }
        pNextRemInfo = (tLldpRemoteNode *) RBTreeGetNext
            (gLldpGlobalInfo.RemSysDataRBTree, (tRBElem *) pNextRemInfo, NULL);
    }
    if (pRemInfo != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId, (UINT1 *) pRemInfo);
    }
    return;
}

#ifdef DCBX_WANTED
/****************************************************************************
 *    FUNCTION NAME    : LldpUtlNotifyDcbxOnTxRxStatChg
 *
 *    DESCRIPTION      : This function will send notification to DCBx 
 *                       module when there is change in Tx or Rx or both status 
 *
 *    INPUT            : i4PortNum - Interface Index
 *                       i4TxRxNotify - Status 
 *
 *    OUTPUT           : None
 *    RETURNS          : None
 ****************************************************************************/
PUBLIC VOID
LldpUtlNotifyDcbxOnTxRxStatChg (INT4 i4PortNum, INT4 i4TxRxNotify)
{
    tLldpAppTlv         LldpAppTlv;

    MEMSET (&LldpAppTlv, 0, sizeof (tLldpAppTlv));

    LldpAppTlv.u4RxAppTlvPortId = (UINT4) i4PortNum;
    LldpAppTlv.u4MsgType = (UINT4) i4TxRxNotify;
    DcbxApiApplCallbkFunc (&LldpAppTlv);
    return;
}
#endif

/****************************************************************************
 *    FUNCTION NAME    : LldpUtlSvidUpdateOnUap
 *
 *    DESCRIPTION      : This function updates the newly created s-channel 
 *                       interface in the SVLAN ID array maintained for each
 *                       port in the structure 'tLldpLocPortTable'
 *                       
 *
 *    INPUT            : u4IfIndex - Interface Index
 *                       u4Svid    - Newly created SVLAN ID on the Interface
 *                                   Index
 *
 *    OUTPUT           : None
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
VOID
LldpUtlSvidUpdateOnUap (UINT4 u4IfIndex, UINT4 u4Svid)
{
    tLldpLocPortTable  *pPortInfo = NULL;
    tLldpLocPortTable   PortInfo;
    INT4                i4SbpCount = 0;
    INT4                i4ArrayFreeIndex = 0;
    UINT1               u1ArrayPopulateFlag = OSIX_FALSE;

    MEMSET (&PortInfo, 0, sizeof (PortInfo));

    PortInfo.i4IfIndex = (INT4) u4IfIndex;
    pPortInfo = RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &PortInfo);
    if (pPortInfo == NULL)
    {
        return;
    }
    while (i4SbpCount < VLAN_EVB_MAX_SBP_PER_UAP)
    {
        if (pPortInfo->au2SVlanIdArray[i4SbpCount] == 0)
        {
            if (u1ArrayPopulateFlag == OSIX_FALSE)
            {
                i4ArrayFreeIndex = i4SbpCount;
                u1ArrayPopulateFlag = OSIX_TRUE;
            }
        }
        if (pPortInfo->au2SVlanIdArray[i4SbpCount] == u4Svid)
        {
            return;
        }
        i4SbpCount++;

    }
    /* Marked as critical trace because will get updated from 
     * EVB when s-channel is created , so this trace message
     * will print on cli by default */
    LLDP_TRC_ARG2 (LLDP_CRITICAL_TRC, "Updated S-Channel "
                   "for transmission in Tagged LLDP frame "
                   "Svid: %u On UAP: %u \r\n", u4Svid, u4IfIndex);
    pPortInfo->au2SVlanIdArray[i4ArrayFreeIndex] = (UINT2) u4Svid;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpUtlSvidDeleteOnUap
 *
 *    DESCRIPTION      : This function updates the deleeted s-channel 
 *                       interface in the SVLAN ID array maintained for each
 *                       port in the structure 'tLldpLocPortTable'
 *                       
 *
 *    INPUT            : u4IfIndex - Interface Index
 *                       u4Svid    - Deleted SVLAN ID on the Interface
 *                                   Index
 *
 *    OUTPUT           : None
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 ****************************************************************************/
VOID
LldpUtlSvidDeleteOnUap (UINT4 u4IfIndex, UINT4 u4Svid)
{
    tLldpLocPortTable  *pPortInfo = NULL;
    tLldpLocPortTable   PortInfo;
    INT4                i4SbpCount = 0;

    MEMSET (&PortInfo, 0, sizeof (PortInfo));

    PortInfo.i4IfIndex = (INT4) u4IfIndex;
    pPortInfo = RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &PortInfo);
    if (pPortInfo == NULL)
    {
        return;
    }
    while (i4SbpCount < VLAN_EVB_MAX_SBP_PER_UAP)
    {
        if (pPortInfo->au2SVlanIdArray[i4SbpCount] == u4Svid)
        {
            /* Marked as critical trace because will get updated from 
             * EVB when s-channel is deleted , so this trace message
             * will print on cli by default */
            LLDP_TRC_ARG2 (LLDP_CRITICAL_TRC, "Deleted S-Channel"
                           "for transmission in Tagged LLDP frame"
                           "Svid: %u On UAP: %u \r\n", u4Svid, u4IfIndex);
            pPortInfo->au2SVlanIdArray[i4SbpCount] = 0;
            break;
        }
        i4SbpCount++;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpUtlGetStatsTxTaggedPortFramesTotal
 *
 *    DESCRIPTION      : To Get status of the total Tagged Tx
 *                       port frames 
 *
 *    INPUT            : i4LldpPortConfigPortNum - Tx port Number
 *
 *    OUTPUT           : pu4RetValLldpStatsTxTaggedPortFramesTotal - 
 *                       Total Tagged Tx Frames Transmistted on a port
 *
 *    RETURNS          : OSIX_FAILURE or OSIX_SUCCESS
 *
 ****************************************************************************/
INT4
LldpUtlGetStatsTxTaggedPortFramesTotal (INT4 i4LldpPortConfigPortNum,
                                        UINT4
                                        *pu4RetValLldpStatsTxTaggedPortFramesTotal,
                                        tMacAddr DestMacAddr)
{
    tLldpLocPortInfo    LldpLocPortInfo;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    UINT4               u4LocPort = 0;

    if (LldpUtlGetLocalPort (i4LldpPortConfigPortNum, DestMacAddr, &u4LocPort)
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to get the LldpUtlGetLocalPort either"
                  "i4LldpPortConfigPortNum or DestMacAddr is Invalid  \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD pLldpLocPortInfo is NULL LldpLocPortInfo is Invalid \r\n");
        return OSIX_FAILURE;
    }

    *pu4RetValLldpStatsTxTaggedPortFramesTotal =
        pLldpLocalPortInfo->StatsTxPortTable.u4TxTaggedFramesTotal;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpUtlTestLldpCdcpTxStatus
 *
 *    DESCRIPTION      : This function tests if CDCP agent is regestired in any port
 *
 *    INPUT            : None 
 * 
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS-If no application is registered with LLDP
 *                                    or If no CDCP application is registered. 
 *                       SNMP_FAILURE-Otherwise.
 *
 ****************************************************************************/
INT4
LldpUtlTestLldpCdcpTxStatus ()
{
    tLldpAppInfo        TmpLldpAppNode;
    tLldpAppInfo       *pLldpAppNode = NULL;

    MEMSET (&TmpLldpAppNode, 0, sizeof (tLldpAppInfo));

    while ((pLldpAppNode =
            (tLldpAppInfo *) RBTreeGetNext (gLldpGlobalInfo.AppRBTree,
                                            (tRBElem *) & TmpLldpAppNode,
                                            NULL)) != NULL)
    {
        if ((pLldpAppNode->LldpAppId.u2TlvType == LLDP_VLAN_EVB_CDCP_TLV_TYPE)
            && (pLldpAppNode->LldpAppId.u1SubType ==
                LLDP_VLAN_EVB_CDCP_TLV_SUBTYPE)
            &&
            (MEMCMP
             (pLldpAppNode->LldpAppId.au1OUI, gau1LldpDot1OUI,
              LLDP_MAX_LEN_OUI) == 0))
        {
            return SNMP_FAILURE;
        }
        /* copying the keys */
        TmpLldpAppNode.u4PortId = pLldpAppNode->u4PortId;
        MEMCPY (&(TmpLldpAppNode.LldpAppId), &(pLldpAppNode->LldpAppId),
                sizeof (tLldpAppId));
        /* Get the next application in the port */
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpIsDcbxApplicationRegistered
 *
 *    DESCRIPTION      : This function is to check if DCBX application is
 *                       registered on the particular agent on this interface.
 *
 *    INPUT            : u4IfIndex - Interface index
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_TRUE/OSIX_FALSE
 ****************************************************************************/

PUBLIC              BOOL1
LldpIsDcbxApplicationRegistered (UINT4 u4IfIndex)
{
    tLldpAppInfo        TmpLldpAppNode;
    tLldpAppInfo       *pLldpAppNode = NULL;

    MEMSET (&TmpLldpAppNode, 0, sizeof (tLldpAppInfo));
    /* Get the first application register on this port by specifying
     * only port number as part of the index of application RB Tree */
    TmpLldpAppNode.u4PortId = u4IfIndex;
    pLldpAppNode = (tLldpAppInfo *) RBTreeGetNext (gLldpGlobalInfo.AppRBTree,
                                                   (tRBElem *) & TmpLldpAppNode,
                                                   NULL);
    if (pLldpAppNode == NULL)
    {
        /*If no applications are registered, Obviously DCBX applications
         *are also not registered */
        return OSIX_FALSE;
    }
    do
    {
        if (pLldpAppNode->u4PortId != u4IfIndex)
        {
            /* The first application itself is registered on a
             * different interface */
            return OSIX_FALSE;
        }
#ifdef DCBX_WANTED
        if (DcbxApiVerifyTlvisDcbx (pLldpAppNode->LldpAppId.u2TlvType,
                                    pLldpAppNode->LldpAppId.u1SubType,
                                    pLldpAppNode->LldpAppId.au1OUI) ==
            OSIX_TRUE)
        {
            /*DCBX application is registered */
            return OSIX_TRUE;
        }
#endif
        /* Get the next application in the port */
        pLldpAppNode =
            (tLldpAppInfo *) RBTreeGetNext (gLldpGlobalInfo.AppRBTree,
                                            (tRBElem *) pLldpAppNode, NULL);
    }
    while (pLldpAppNode != NULL);

    /* None of the application registered belongs to DCBX protocol */
    return OSIX_FALSE;

}

/****************************************************************************
 *    FUNCTION NAME    : LldpIsMultipleAgentsOnInterface
 *
 *    DESCRIPTION      : This function is to check if multiple agents are
 *                       configured on a particular interface.
 *
 *    INPUT            : u4IfIndex - Interface index
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_TRUE/OSIX_FALSE
 ****************************************************************************/

PUBLIC              BOOL1
LldpIsMultipleAgentsOnInterface (INT4 i4IfIndex)
{
    UINT4               u4DestMacIndex = 0;
    UINT4               u4Count = 0;
    INT4                i4Index = i4IfIndex;
    INT4                i4PrevIfIndex = 0;
    UINT4               u4PrevDestMacIndex = 0;
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
    {
        u4DestMacIndex = LLDP_V1_DEST_MAC_ADDR_INDEX (i4IfIndex);
    }
    else
    {
        u4DestMacIndex = DEFAULT_DEST_MAC_ADDR_INDEX;
    }

    if (nmhValidateIndexInstanceLldpV2PortConfigTable
        (i4IfIndex, u4DestMacIndex) == SNMP_FAILURE)
    {
        u4DestMacIndex = LLDP_V1_DEST_MAC_ADDR_INDEX (i4IfIndex);
    }
    do
    {
        if (i4IfIndex == i4Index)
        {
            u4Count++;
        }
        else
        {
            break;
        }
        i4PrevIfIndex = i4IfIndex;
        u4PrevDestMacIndex = u4DestMacIndex;
    }
    while (nmhGetNextIndexLldpV2PortConfigTable
           (i4PrevIfIndex, &i4IfIndex, u4PrevDestMacIndex,
            &u4DestMacIndex) == SNMP_SUCCESS);
    if (u4Count > 1)
    {
        return OSIX_TRUE;
    }
    else
    {
        return OSIX_FALSE;
    }
}

#endif /* _LLDPUTIL_C_ */
/*---------------------------------------------------------------------------*/
/*                        End of the file  <lldutil.c>                        */
/*---------------------------------------------------------------------------*/
