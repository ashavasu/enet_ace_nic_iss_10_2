/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: stdlldwr.c,v 1.11 2015/11/06 11:32:23 siva Exp $
*
* Description: Contains LLDP standard mib wrapper Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "stdlldwr.h"
# include  "stdllddb.h"
# include  "lldinc.h"

VOID
RegisterSTDLLDP ()
{
    SNMPRegisterMibWithLock (&stdlldOID, &stdlldEntry, LldpLock, LldpUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&stdlldOID, (const UINT1 *) "stdlldp");
}

VOID
UnRegisterSTDLLD ()
{
    SNMPUnRegisterMib (&stdlldOID, &stdlldEntry);
    SNMPDelSysorEntry (&stdlldOID, (const UINT1 *) "stdlldp");
}

INT4
LldpMessageTxIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetLldpMessageTxInterval (&(pMultiData->i4_SLongValue)));
}

INT4
LldpMessageTxHoldMultiplierGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetLldpMessageTxHoldMultiplier (&(pMultiData->i4_SLongValue)));
}

INT4
LldpReinitDelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetLldpReinitDelay (&(pMultiData->i4_SLongValue)));
}

INT4
LldpTxDelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetLldpTxDelay (&(pMultiData->i4_SLongValue)));
}

INT4
LldpNotificationIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetLldpNotificationInterval (&(pMultiData->i4_SLongValue)));
}

INT4
LldpMessageTxIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetLldpMessageTxInterval (pMultiData->i4_SLongValue));
}

INT4
LldpMessageTxHoldMultiplierSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetLldpMessageTxHoldMultiplier (pMultiData->i4_SLongValue));
}

INT4
LldpReinitDelaySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetLldpReinitDelay (pMultiData->i4_SLongValue));
}

INT4
LldpTxDelaySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetLldpTxDelay (pMultiData->i4_SLongValue));
}

INT4
LldpNotificationIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetLldpNotificationInterval (pMultiData->i4_SLongValue));
}

INT4
LldpMessageTxIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2LldpMessageTxInterval
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
LldpMessageTxHoldMultiplierTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2LldpMessageTxHoldMultiplier
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
LldpReinitDelayTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2LldpReinitDelay (pu4Error, pMultiData->i4_SLongValue));
}

INT4
LldpTxDelayTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2LldpTxDelay (pu4Error, pMultiData->i4_SLongValue));
}

INT4
LldpNotificationIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2LldpNotificationInterval
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
LldpMessageTxIntervalDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2LldpMessageTxInterval
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
LldpMessageTxHoldMultiplierDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2LldpMessageTxHoldMultiplier
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
LldpReinitDelayDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2LldpReinitDelay (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
LldpTxDelayDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2LldpTxDelay (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
LldpNotificationIntervalDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2LldpNotificationInterval
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexLldpPortConfigTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpPortConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpPortConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpPortConfigAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpPortConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpPortConfigAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
LldpPortConfigNotificationEnableGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpPortConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpPortConfigNotificationEnable
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
LldpPortConfigTLVsTxEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpPortConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpPortConfigTLVsTxEnable
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
LldpPortConfigAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetLldpPortConfigAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
LldpPortConfigNotificationEnableSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetLldpPortConfigNotificationEnable
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
LldpPortConfigTLVsTxEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetLldpPortConfigTLVsTxEnable
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
LldpPortConfigAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2LldpPortConfigAdminStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
LldpPortConfigNotificationEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2LldpPortConfigNotificationEnable (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
LldpPortConfigTLVsTxEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2LldpPortConfigTLVsTxEnable (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->pOctetStrValue));

}

INT4
GetNextIndexLldpLocManAddrTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpLocManAddrTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpLocManAddrTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpPortConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2LldpPortConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
LldpLocManAddrLenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpLocManAddrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpLocManAddrLen (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
LldpLocManAddrIfSubtypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpLocManAddrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpLocManAddrIfSubtype (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
LldpLocManAddrIfIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpLocManAddrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpLocManAddrIfId (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
LldpLocManAddrOIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpLocManAddrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpLocManAddrOID (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiData->pOidValue));

}

INT4
GetNextIndexLldpConfigManAddrTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpConfigManAddrTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpConfigManAddrTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpConfigManAddrPortsTxEnableGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpConfigManAddrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpConfigManAddrPortsTxEnable
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
LldpConfigManAddrPortsTxEnableSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetLldpConfigManAddrPortsTxEnable
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
LldpConfigManAddrPortsTxEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2LldpConfigManAddrPortsTxEnable (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     pOctetStrValue,
                                                     pMultiData->
                                                     pOctetStrValue));

}

INT4
LldpConfigManAddrTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2LldpConfigManAddrTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
LldpStatsRemTablesLastChangeTimeGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetLldpStatsRemTablesLastChangeTime
            (&(pMultiData->u4_ULongValue)));
}

INT4
LldpStatsRemTablesInsertsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetLldpStatsRemTablesInserts (&(pMultiData->u4_ULongValue)));
}

INT4
LldpStatsRemTablesDeletesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetLldpStatsRemTablesDeletes (&(pMultiData->u4_ULongValue)));
}

INT4
LldpStatsRemTablesDropsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetLldpStatsRemTablesDrops (&(pMultiData->u4_ULongValue)));
}

INT4
LldpStatsRemTablesAgeoutsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetLldpStatsRemTablesAgeouts (&(pMultiData->u4_ULongValue)));
}

INT4
GetNextIndexLldpStatsTxPortTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpStatsTxPortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpStatsTxPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpStatsTxPortFramesTotalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpStatsTxPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpStatsTxPortFramesTotal
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexLldpStatsRxPortTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpStatsRxPortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpStatsRxPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpStatsRxPortFramesDiscardedTotalGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpStatsRxPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpStatsRxPortFramesDiscardedTotal
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
LldpStatsRxPortFramesErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpStatsRxPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpStatsRxPortFramesErrors
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
LldpStatsRxPortFramesTotalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpStatsRxPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpStatsRxPortFramesTotal
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
LldpStatsRxPortTLVsDiscardedTotalGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpStatsRxPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpStatsRxPortTLVsDiscardedTotal
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
LldpStatsRxPortTLVsUnrecognizedTotalGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpStatsRxPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpStatsRxPortTLVsUnrecognizedTotal
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
LldpStatsRxPortAgeoutsTotalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpStatsRxPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpStatsRxPortAgeoutsTotal
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
LldpLocChassisIdSubtypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetLldpLocChassisIdSubtype (&(pMultiData->i4_SLongValue)));
}

INT4
LldpLocChassisIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetLldpLocChassisId (pMultiData->pOctetStrValue));
}

INT4
LldpLocSysNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetLldpLocSysName (pMultiData->pOctetStrValue));
}

INT4
LldpLocSysDescGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetLldpLocSysDesc (pMultiData->pOctetStrValue));
}

INT4
LldpLocSysCapSupportedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetLldpLocSysCapSupported (pMultiData->pOctetStrValue));
}

INT4
LldpLocSysCapEnabledGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetLldpLocSysCapEnabled (pMultiData->pOctetStrValue));
}

INT4
GetNextIndexLldpLocPortTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpLocPortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpLocPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpLocPortIdSubtypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpLocPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpLocPortIdSubtype (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
LldpLocPortIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpLocPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpLocPortId (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiData->pOctetStrValue));

}

INT4
LldpLocPortDescGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpLocPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpLocPortDesc (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiData->pOctetStrValue));

}

INT4
GetNextIndexLldpRemTable (tSnmpIndex * pFirstMultiIndex,
                          tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpRemTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpRemTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpRemChassisIdSubtypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpRemTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpRemChassisIdSubtype (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
LldpRemChassisIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpRemTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpRemChassisId (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    pMultiData->pOctetStrValue));

}

INT4
LldpRemPortIdSubtypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpRemTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpRemPortIdSubtype (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
LldpRemPortIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpRemTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpRemPortId (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiIndex->pIndex[1].i4_SLongValue,
                                 pMultiIndex->pIndex[2].i4_SLongValue,
                                 pMultiData->pOctetStrValue));

}

INT4
LldpRemPortDescGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpRemTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpRemPortDesc (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   pMultiIndex->pIndex[2].i4_SLongValue,
                                   pMultiData->pOctetStrValue));

}

INT4
LldpRemSysNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpRemTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpRemSysName (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].i4_SLongValue,
                                  pMultiIndex->pIndex[2].i4_SLongValue,
                                  pMultiData->pOctetStrValue));

}

INT4
LldpRemSysDescGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpRemTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpRemSysDesc (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].i4_SLongValue,
                                  pMultiIndex->pIndex[2].i4_SLongValue,
                                  pMultiData->pOctetStrValue));

}

INT4
LldpRemSysCapSupportedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpRemTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpRemSysCapSupported (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
LldpRemSysCapEnabledGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpRemTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpRemSysCapEnabled (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
GetNextIndexLldpRemManAddrTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpRemManAddrTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pNextMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpRemManAddrTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].pOctetStrValue,
             pNextMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpRemManAddrIfSubtypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpRemManAddrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpRemManAddrIfSubtype (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiIndex->pIndex[3].i4_SLongValue,
                                           pMultiIndex->pIndex[4].
                                           pOctetStrValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
LldpRemManAddrIfIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpRemManAddrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpRemManAddrIfId (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].i4_SLongValue,
                                      pMultiIndex->pIndex[4].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
LldpRemManAddrOIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpRemManAddrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpRemManAddrOID (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     pMultiIndex->pIndex[3].i4_SLongValue,
                                     pMultiIndex->pIndex[4].pOctetStrValue,
                                     pMultiData->pOidValue));

}

INT4
GetNextIndexLldpRemUnknownTLVTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpRemUnknownTLVTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpRemUnknownTLVTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpRemUnknownTLVInfoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpRemUnknownTLVTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpRemUnknownTLVInfo (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
GetNextIndexLldpRemOrgDefInfoTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpRemOrgDefInfoTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pNextMultiIndex->pIndex[3].pOctetStrValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             &(pNextMultiIndex->pIndex[5].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpRemOrgDefInfoTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].pOctetStrValue,
             pNextMultiIndex->pIndex[3].pOctetStrValue,
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pFirstMultiIndex->pIndex[5].i4_SLongValue,
             &(pNextMultiIndex->pIndex[5].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpRemOrgDefInfoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpRemOrgDefInfoTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpRemOrgDefInfo (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     pMultiIndex->pIndex[3].pOctetStrValue,
                                     pMultiIndex->pIndex[4].i4_SLongValue,
                                     pMultiIndex->pIndex[5].i4_SLongValue,
                                     pMultiData->pOctetStrValue));

}
