/*****************************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: lldmtlv.c,v 1.7 2017/10/11 13:42:02 siva Exp $
 *
 * Description: This file contains LLDP-MED utility routines that is used by
 *              LLDP transmit module.
 *****************************************************************************/
#ifndef _LLDMTXUTL_C_
#define _LLDMTXUTL_C_

#include "lldinc.h"

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedAddMedOrgSpecTlvs
 *
 *    DESCRIPTION      : Adds Media related TLVs(MED Capabilities TLV,
 *                       Media Policy TLV and Inventory TLV) to the linear 
 *                       buffer
 *
 *    INPUT            : pPortInfo - Pointer to the port entry for which the
 *                                   CRU buffer is formed
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PUBLIC VOID
LldpMedAddMedOrgSpecTlvs (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                          UINT1 *pu1Buf, UINT2 *pu2NextOffset)
{
    UINT1              *pu1InitialPtr = pu1Buf;    /* Store the memory address where the
                                                   buffer pointer points to */
    UINT1              *pu1InventoryPtr = NULL;    /* Store the memory address of the pu1Buf
                                                   before adding Inventory TLV */
    UINT2               u2LocOffset = 0;    /* Offset in bytes of which the linear
                                               buffer is filled with LLDP-MED tlvs */

    *pu2NextOffset = 0;

    /* Check whether MED Capability TLV is enabled, If true then add the TLV */
    if (pPortInfo->LocMedCapInfo.u2MedLocCapTxEnable & LLDP_MED_CAPABILITY_TLV)
    {
        /* Add Med Capability TLV to the PDU */
        LldpMedAddMedCapTlv (pPortInfo, pu1BasePtr, pu1Buf, &u2LocOffset);
        if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
        {
            LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | LLDP_MED_CAPAB_TRC,
                      "\r(LldpMedAddMedOrgSpecTlvs) Max Pdu size "
                      "exceeded while adding Med Capability TLV \r\n");
            *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
            return;
        }

        /* Add all other TLVs Only when Med Capability TLV is enabled */
        /* Check whether Network Policy TLV is enabled */
        if (pPortInfo->LocMedCapInfo.u2MedLocCapTxEnable &
            LLDP_MED_NETWORK_POLICY_TLV)
        {
            /* Move buffer pointer to point to end of Med Capability tlv */
            LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);

            /* Add Network Policy tlv */
            LldpMedAddNwPolicyTlv (pPortInfo, pu1BasePtr, pu1Buf, &u2LocOffset);

            if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
            {
                LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                          LLDP_MED_NW_POL_TRC,
                          "\r(LldpMedAddMedOrgSpecTlvs) Max Pdu size exceeded"
                          "while adding Network Policy TLV \r\n");
                *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
                return;
            }
        }

        /* Check whether Location Identification TLV is enabled */
        if (pPortInfo->LocMedCapInfo.u2MedLocCapTxEnable &
            LLDP_MED_LOCATION_ID_TLV)
        {
            /* Move buffer pointer to point to end of Network Policy tlv */
            LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);

            /* Add Location Identification tlv */
            LldpMedAddLocIdTlv (pPortInfo, pu1BasePtr, pu1Buf, &u2LocOffset);

            if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
            {
                LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                          LLDP_MED_LOC_ID_TRC,
                          "\r(LldpMedAddMedOrgSpecTlvs) Max Pdu size exceeded"
                          "while adding Location Identification TLV \r\n");
                *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
                return;
            }
        }

        /* Check whether Extended Power Via MDI TLV is enabled */
        if (pPortInfo->LocMedCapInfo.u2MedLocCapTxEnable &
            LLDP_MED_PW_MDI_PSE_TLV)
        {
            /* Move buffer pointer to point to end of Location Identification tlv */
            LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);

            /* Add Extended Power-Via-MDI tlv */
            LldpMedAddPowMDITlv (pPortInfo, pu1BasePtr, pu1Buf, &u2LocOffset);

            if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
            {
                LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                          LLDP_MED_EX_POW_TRC,
                          "\r(LldpMedAddMedOrgSpecTlvs) Max Pdu size exceeded"
                          "while adding Extended Power-Via-MDI TLV \r\n");
                *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
                return;
            }

        }

        /* Check whether Inventory TLV is enabled, If true then add the TLV */
        if (pPortInfo->LocMedCapInfo.u2MedLocCapTxEnable &
            LLDP_MED_INVENTORY_MGMT_TLV)
        {
            /* Move buffer pointer to point to end of Extended Power-via-MDI tlv */
            LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);

            /* Add All Inventory set TLVs */
            /* It is mandatory to have all Inventory TLVs present in LLDPDU.
             * If maximum PDU length is exceeded after adding few Inventory 
             * TLVs, Removal of already added Inventory TLVs has to be taken 
             * care. For that, store the memory address of pu1Buf where it 
             * points to the begining of inventory TLVs.
             * If maximum size is exceeded in middle, get the length of
             * already added inventory TLVs (pu1Buf - pu1InventoryPtr) and 
             * do the memset for that length */

            pu1InventoryPtr = pu1Buf;

            /* Add Hardware Revision tlv */
            LldpMedAddHwRevisionTlv (pPortInfo, pu1BasePtr, pu1Buf,
                                     &u2LocOffset);

            if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
            {
                LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                          LLDP_MED_INV_MGMT_TRC,
                          "\r(LldpMedAddMedOrgSpecTlvs) Max Pdu size exceeded "
                          "while adding Hardware Revision Inventory TLV \r\n");
                *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
                return;
            }
            /* Move buffer pointer to point to end of Hardware Revision tlv */
            LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);

            /* Add Firmware Revision tlv */
            LldpMedAddFwRevisionTlv (pPortInfo, pu1BasePtr, pu1Buf,
                                     &u2LocOffset);

            if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
            {
                LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                          LLDP_MED_INV_MGMT_TRC,
                          "\r(LldpMedAddMedOrgSpecTlvs) Max Pdu size exceeded"
                          "while adding Firmware Revision Inventory TLV \r\n");
                /* Move the next offset to the begining of Inventory TLVs */
                *pu2NextOffset = (UINT2) (pu1InventoryPtr - pu1InitialPtr);
                /* Memset the length of Inventory TLVs added to the linear 
                 * buffer */
                MEMSET (pu1InventoryPtr, 0, (pu1Buf - pu1InventoryPtr));
                /* Copy pu1InventoryPtr to pu1Buf */
                pu1Buf = pu1InventoryPtr;
                return;
            }

            /* Move buffer pointer to point to end of Firmware Revision tlv */
            LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);

            /* Add Software Revision tlv */
            LldpMedAddSwRevisionTlv (pPortInfo, pu1BasePtr, pu1Buf,
                                     &u2LocOffset);

            if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
            {
                LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                          LLDP_MED_INV_MGMT_TRC,
                          "\r(LldpMedAddMedOrgSpecTlvs) Max Pdu size exceeded"
                          "while adding Software Revision Inventory TLV \r\n");
                /* Move the next offset to the begining of Inventory TLVs */
                *pu2NextOffset = (UINT2) (pu1InventoryPtr - pu1InitialPtr);
                /* Memset the length of Inventory TLVs added to the linear
                 * buffer */
                MEMSET (pu1InventoryPtr, 0, (pu1Buf - pu1InventoryPtr));
                /* Copy pu1InventoryPtr to pu1Buf */
                pu1Buf = pu1InventoryPtr;

                return;
            }

            /* Move buffer pointer to point to end of Software Revision tlv */
            LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);

            /* Add Serial Number tlv */
            LldpMedAddSerialNumberTlv (pPortInfo, pu1BasePtr, pu1Buf,
                                       &u2LocOffset);

            if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
            {
                LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                          LLDP_MED_INV_MGMT_TRC,
                          "\r(LldpMedAddMedOrgSpecTlvs) Max Pdu size exceeded "
                          "while adding Serial Number Inventory TLV \r\n");
                /* Move the next offset to the begining of Inventory TLVs */
                *pu2NextOffset = (UINT2) (pu1InventoryPtr - pu1InitialPtr);
                /* Memset the length of Inventory TLVs added to the linear
                 * buffer */
                MEMSET (pu1InventoryPtr, 0, (pu1Buf - pu1InventoryPtr));
                /* Copy pu1InventoryPtr to pu1Buf */
                pu1Buf = pu1InventoryPtr;

                return;
            }

            /* Move buffer pointer to point to end of Serial Number tlv */
            LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);

            /* Add Manufacturer Name tlv */
            LldpMedAddMfgNameTlv (pPortInfo, pu1BasePtr, pu1Buf, &u2LocOffset);

            if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
            {
                LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                          LLDP_MED_INV_MGMT_TRC,
                          "\r(LldpMedAddMedOrgSpecTlvs) Max Pdu size exceeded "
                          "while adding Manufacturer Name Inventory TLV \r\n");
                /* Move the next offset to the begining of Inventory TLVs */
                *pu2NextOffset = (UINT2) (pu1InventoryPtr - pu1InitialPtr);
                /* Memset the length of Inventory TLVs added to the linear
                 * buffer */
                MEMSET (pu1InventoryPtr, 0, (pu1Buf - pu1InventoryPtr));
                /* Copy pu1InventoryPtr to pu1Buf */
                pu1Buf = pu1InventoryPtr;

                return;
            }

            /* Move buffer pointer to point to end of Manufacturer Name tlv */
            LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);

            /* Add Model Name tlv */
            LldpMedAddModelNameTlv (pPortInfo, pu1BasePtr, pu1Buf,
                                    &u2LocOffset);

            if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
            {
                LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                          LLDP_MED_INV_MGMT_TRC,
                          "\r(LldpMedAddMedOrgSpecTlvs) Max Pdu size exceeded"
                          "while adding Model Name Inventory TLV \r\n");
                /* Move the next offset to the begining of Inventory TLVs */
                *pu2NextOffset = (UINT2) (pu1InventoryPtr - pu1InitialPtr);
                /* Memset the length of Inventory TLVs added to the linear
                 * buffer */
                MEMSET (pu1InventoryPtr, 0, (pu1Buf - pu1InventoryPtr));
                /* Copy pu1InventoryPtr to pu1Buf */
                pu1Buf = pu1InventoryPtr;

                return;
            }
            /* Move buffer pointer to point to end of Model Name tlv */
            LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);

            /* Add Asset ID tlv */
            LldpMedAddAssetIdTlv (pPortInfo, pu1BasePtr, pu1Buf, &u2LocOffset);

            if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
            {
                LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                          LLDP_MED_INV_MGMT_TRC,
                          "\r(LldpMedAddMedOrgSpecTlvs) Max Pdu size exceeded"
                          "while adding Asset ID Inventory TLV \r\n");
                /* Move the next offset to the begining of Inventory TLVs */
                *pu2NextOffset = (UINT2) (pu1InventoryPtr - pu1InitialPtr);
                /* Memset the length of Inventory TLVs added to the linear
                 * buffer */
                MEMSET (pu1InventoryPtr, 0, (pu1Buf - pu1InventoryPtr));
                /* Copy pu1InventoryPtr to pu1Buf */
                pu1Buf = pu1InventoryPtr;

                return;
            }
        }
    }
    /* Move to end of pdu */
    LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);
    *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedAddMedCapTlv
 *
 *    DESCRIPTION      : This function adds LLDP MED Capability TLV to the
 *                       linear buffer
 *
 *    INPUT            : pPortInfo    - Pointer to port information structure
 *                       pu1BasePtr   - Pointer to base address of linear buffer
 *                       pu1Buf       - Pointer to linear buffer in which LLDP 
 *                                      MED Capability tlv has to be addded
 *                       pu2LocOffset - Pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf       - Pointer to linear buffer in which LLDP
 *                                      MED Capability tlv is addded
 *                       pu2LocOffset - Pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PUBLIC VOID
LldpMedAddMedCapTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                     UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    UINT2               u2TlvHeader = 0;
    UINT2               u2CapSupported = 0;

    *pu2LocOffset = 0;
    LLDP_TRC_ARG1 (LLDP_MED_CAPAB_TRC,
                   "\rLldpMedAddMedCapTlv: Adding LLDP-MED Capability TLV to "
                   "the preformed buffer on port %d\r\n",
                   pPortInfo->u4LocPortNum);
    /* Check whether the linear buffer can accomodate LLDP MED Capability tlv */
    if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                    LLDP_MED_CAPABILITY_TLV_LEN) == OSIX_TRUE)
    {
        pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
        pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
        LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | LLDP_MED_CAPAB_TRC,
                  "LldpMedAddMedCapTlv: Max Pdu size exceeded!!!..\n");
        return;
    }

    /* Form the TLV header with type (127 for LLDP-MED TLVs) and length values */
    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_ORG_SPEC_TLV,
                          (UINT2) LLDP_MED_CAPABILITY_TLV_INFO_LEN,
                          &u2TlvHeader);

    /* Fill the first 2 bytes as TLV header */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* Fill the 3 bytes length of LLDP-MED OUI */
    MEMCPY (pu1Buf, gau1LldpMedOUI, LLDP_MAX_LEN_OUI);
    /* Increment buffer pointer to 3 bytes */
    LLDP_INCR_BUF_PTR (pu1Buf, (UINT2) LLDP_MAX_LEN_OUI);
    /* Fill the 1 byte length of Med Capability TLV subtype */
    LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_MED_CAPABILITY_SUBTYPE);
    /* Fill the 2 bytes length of LLDP-MED Capability Supported value */
    u2CapSupported = (u2CapSupported | (pPortInfo->LocMedCapInfo.
                                        u2MedLocCapTxSupported));

    LLDP_PUT_2BYTE (pu1Buf, u2CapSupported);
    /* Fill the 1 byte length of Local Device class value */
    LLDP_PUT_1BYTE (pu1Buf, gLldpGlobalInfo.LocSysInfo.u1MedLocDeviceClass);

    *pu2LocOffset = LLDP_MED_CAPABILITY_TLV_LEN;
    LLDP_TRC_ARG1 (LLDP_MED_CAPAB_TRC,
                   "\rLldpMedAddMedCapTlv: Added MedCapability TLV "
                   "to preformed buffer for port"
                   "%d successfully\r\n", pPortInfo->u4LocPortNum);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedAddNwPolicyTlv
 *
 *    DESCRIPTION      : This function adds LLDP-MED Network Policy TLVs to the
 *                       linear buffer
 *
 *    INPUT            : pPortInfo    - Pointer to port information structure
 *                       pu1BasePtr   - Pointer to base address of linear buffer
 *                       pu1Buf       - Pointer to linear buffer in which LLDP 
 *                                      MED Network Policy tlvs has to be added
 *                       pu2LocOffset - Pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf       - Pointer to linear buffer in which LLDP
 *                                      MED Network Policy tlv are addded
 *                       pu2LocOffset - Pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PUBLIC VOID
LldpMedAddNwPolicyTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                       UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    tLldpMedLocNwPolicyInfo NwPolicyInfo;
    tLldpMedLocNwPolicyInfo *pNwPolicyInfo = NULL;
    UINT4               u4PolicyInfo = 0;
    UINT2               u2TlvHeader = 0;
    UINT1              *pu1InitialPtr = pu1Buf;

    MEMSET (&NwPolicyInfo, 0, sizeof (tLldpMedLocNwPolicyInfo));

    *pu2LocOffset = 0;
    LLDP_TRC_ARG1 (LLDP_MED_NW_POL_TRC,
                   "\rLldpMedAddNwPolicyTlv: Adding LLDP-MED Network Policy TLV to"
                   " the preformed buffer on port %d\r\n",
                   pPortInfo->u4LocPortNum);

    /* Adding all the Policies created for the particular port */
    /* Get the first Index of Local Policy Table */
    NwPolicyInfo.u4IfIndex = (UINT4) pPortInfo->i4IfIndex;
    pNwPolicyInfo =
        (tLldpMedLocNwPolicyInfo *) RBTreeGetNext (gLldpGlobalInfo.
                                                   LldpMedLocNwPolicyInfoRBTree,
                                                   (tRBElem *) & NwPolicyInfo,
                                                   NULL);

    while ((pNwPolicyInfo != NULL) &&
           (pNwPolicyInfo->u4IfIndex == (UINT4) pPortInfo->i4IfIndex))
    {
        /* As per standard, policy with UNKNOWN bit set should not be advertised 
         * for Network Connectivity devices */
        if (((gLldpGlobalInfo.LocSysInfo.u1MedLocDeviceClass ==
              LLDP_MED_NW_CONNECTIVITY_DEVICE) &&
             (pNwPolicyInfo->bLocPolicyUnKnown == LLDP_MED_UNKNOWN_FLAG)) ||
            (pNwPolicyInfo->i4RowStatus != ACTIVE))
        {
            pNwPolicyInfo = (tLldpMedLocNwPolicyInfo *) RBTreeGetNext
                (gLldpGlobalInfo.
                 LldpMedLocNwPolicyInfoRBTree, (tRBElem *) pNwPolicyInfo, NULL);
            continue;
        }
        /* Check whether the linear buffer can accomodate Network Policy tlv */
        if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                        LLDP_MED_NW_POLICY_TLV_LEN) ==
            OSIX_TRUE)
        {
            pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
            pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
            *pu2LocOffset = (UINT2) (pu1Buf - pu1InitialPtr);
            LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | LLDP_MED_NW_POL_TRC,
                      "LldpMedAddNwPolicyTlv: Max Pdu size exceeded!!!..\n");
            return;
        }
        LLDP_FORM_TLV_HEADER ((UINT2) LLDP_ORG_SPEC_TLV,
                              (UINT2) LLDP_MED_NW_POLICY_INFO_LEN,
                              &u2TlvHeader);

        /* Put tlv header */
        LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
        /* Copy LLDP-MED OUI into linear buffer */
        MEMCPY (pu1Buf, gau1LldpMedOUI, LLDP_MAX_LEN_OUI);
        /* Increment buffer pointer */
        LLDP_INCR_BUF_PTR (pu1Buf, (UINT2) LLDP_MAX_LEN_OUI);
        /* Put Network Policy TLV  subtype in linear buffer */
        LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_MED_NW_POLICY_SUBTYPE);

        /* LLDP MED Network Policy info */
        /* Add application type value */
        u4PolicyInfo = pNwPolicyInfo->u1LocPolicyAppType;
        u4PolicyInfo = u4PolicyInfo << LLDP_MED_APP_TYPE_SHIFT_BIT;
        /* Add Policy unknown bit value */
        u4PolicyInfo |= pNwPolicyInfo->bLocPolicyUnKnown <<
            LLDP_MED_UNKNOWN_SHIFT_BIT;
        /* Add Policy Tagged bit value */
        u4PolicyInfo |= pNwPolicyInfo->bLocPolicyTagged <<
            LLDP_MED_TAGGED_SHIFT_BIT;
        /* Add Vlan Id value */
        u4PolicyInfo |= pNwPolicyInfo->u2LocPolicyVlanId <<
            LLDP_MED_VLAN_ID_SHIFT_BIT;
        /* Add Priority value */
        u4PolicyInfo |= pNwPolicyInfo->u1LocPolicyPriority <<
            LLDP_MED_PRIORITY_SHIFT_BIT;
        /* Add Dscp value */
        u4PolicyInfo |= pNwPolicyInfo->u1LocPolicyDscp;

        /* Put network policy info into linear buffer */
        LLDP_PUT_4BYTE (pu1Buf, u4PolicyInfo);

        pNwPolicyInfo = (tLldpMedLocNwPolicyInfo *) RBTreeGetNext
            (gLldpGlobalInfo.
             LldpMedLocNwPolicyInfoRBTree, (tRBElem *) pNwPolicyInfo, NULL);
    }

    *pu2LocOffset = (UINT2) (pu1Buf - pu1InitialPtr);
    LLDP_TRC_ARG1 (LLDP_MED_NW_POL_TRC,
                   "\rLldpMedAddNwPolicyTlv: Added Network Policy TLV "
                   "to preformed buffer for port"
                   "%d successfully\r\n", pPortInfo->u4LocPortNum);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedAddHwRevisionTlv
 *
 *    DESCRIPTION      : This function adds LLDP MED Hardware Revision TLV to the
 *                       linear buffer
 *
 *    INPUT            : pPortInfo    - Pointer to port information structure
 *                       pu1BasePtr   - Pointer to base address of linear buffer
 *                       pu1Buf       - Pointer to linear buffer in which
 *                                      Hardware Revision tlv has to be addded
 *                       pu2LocOffset - Pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf       - Pointer to linear buffer in which
 *                                      Hardware Revision tlv is addded
 *                       pu2LocOffset - Pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
PUBLIC VOID
LldpMedAddHwRevisionTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                         UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    UINT2               u2TlvHeader = 0;
    UINT2               u2HwRevTlvInfoLen = 0;
    UINT2               u2HwRevTlvLen = 0;

    *pu2LocOffset = 0;
    LLDP_TRC_ARG1 (LLDP_MED_INV_MGMT_TRC,
                   "\rLldpMedAddHwRevisionTlv: Adding Hardware Revision TLV to the"
                   " preformed buffer on port %d\r\n", pPortInfo->u4LocPortNum);

    LLDP_MED_GET_HW_REV_TLV_INFO_LEN (&u2HwRevTlvInfoLen);
    u2HwRevTlvLen = (UINT2) (LLDP_TLV_HEADER_LEN + u2HwRevTlvInfoLen);

    /* Check whether the linear buffer can accomodate Hardware Revision tlv */
    if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                    u2HwRevTlvLen) == OSIX_TRUE)
    {
        pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
        pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
        LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | LLDP_MED_INV_MGMT_TRC,
                  "LldpMedAddHwRevisionTlv: Max Pdu size exceeded!!!..\n");
        return;
    }
    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_ORG_SPEC_TLV,
                          u2HwRevTlvInfoLen, &u2TlvHeader);

    /* Put tlv header */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* Copy LLDP-MED OUI into linear buffer */
    MEMCPY (pu1Buf, gau1LldpMedOUI, LLDP_MAX_LEN_OUI);
    /* Increment buffer pointer */
    LLDP_INCR_BUF_PTR (pu1Buf, (UINT2) LLDP_MAX_LEN_OUI);
    /* Put Hardware revision TLV  subtype in linear buffer */
    LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_MED_HW_REVISION_SUBTYPE);
    /* Copy Hardware Revision into linear buffer */
    MEMCPY (pu1Buf,
            gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.au1MedLocHardwareRev,
            LLDP_STRLEN (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.
                         au1MedLocHardwareRev, LLDP_MED_HW_REV_LEN));
    /* set tlv length as next offset */
    *pu2LocOffset = u2HwRevTlvLen;
    LLDP_TRC_ARG1 (LLDP_MED_INV_MGMT_TRC,
                   "\rLldpMedAddHwRevisionTlv: Added Hardware Revision TLV "
                   "to preformed buffer for port"
                   "%d successfully\r\n", pPortInfo->u4LocPortNum);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedAddFwRevisionTlv
 *
 *    DESCRIPTION      : This function adds LLDP MED Firmware Revision TLV to 
 *                       the linear buffer
 *
 *    INPUT            : pPortInfo    - Pointer to port information structure
 *                       pu1BasePtr   - Pointer to base address of linear buffer
 *                       pu1Buf       - Pointer to linear buffer in which 
 *                                      Firmware Revision tlv has to be addded
 *                       pu2LocOffset - Pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf       - Pointer to linear buffer in which
 *                                      Firmware Revision tlv is addded
 *                       pu2LocOffset - Pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
PUBLIC VOID
LldpMedAddFwRevisionTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                         UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    UINT2               u2TlvHeader = 0;
    UINT2               u2FwRevTlvInfoLen = 0;
    UINT2               u2FwRevTlvLen = 0;

    *pu2LocOffset = 0;
    LLDP_TRC_ARG1 (LLDP_MED_INV_MGMT_TRC,
                   "\rLldpMedAddFwRevisionTlv: Adding Firmware Revision TLV to the"
                   " preformed buffer on port %d\r\n", pPortInfo->u4LocPortNum);

    LLDP_MED_GET_FW_REV_TLV_INFO_LEN (&u2FwRevTlvInfoLen);
    u2FwRevTlvLen = (UINT2) (LLDP_TLV_HEADER_LEN + u2FwRevTlvInfoLen);

    /* Check whether the linear buffer can accomodate Firmware Revision tlv */
    if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                    u2FwRevTlvLen) == OSIX_TRUE)
    {
        pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
        pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
        LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | LLDP_MED_INV_MGMT_TRC,
                  "LldpMedAddFwRevisionTlv: Max Pdu size exceeded!!!..\n");
        return;
    }
    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_ORG_SPEC_TLV,
                          u2FwRevTlvInfoLen, &u2TlvHeader);

    /* Put tlv header */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* Copy LLDP-MED OUI into linear buffer */
    MEMCPY (pu1Buf, gau1LldpMedOUI, LLDP_MAX_LEN_OUI);
    /* Increment buffer pointer */
    LLDP_INCR_BUF_PTR (pu1Buf, (UINT2) LLDP_MAX_LEN_OUI);
    /* Put Firmware revision TLV  subtype in linear buffer */
    LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_MED_FW_REVISION_SUBTYPE);
    /* Copy Firmware Revision into linear buffer */
    MEMCPY (pu1Buf,
            gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.au1MedLocFirmwareRev,
            LLDP_STRLEN (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.
                         au1MedLocFirmwareRev, LLDP_MED_FW_REV_LEN));
    /* Set tlv length as next offset */
    *pu2LocOffset = u2FwRevTlvLen;
    LLDP_TRC_ARG1 (LLDP_MED_INV_MGMT_TRC,
                   "\rLldpMedAddFwRevisionTlv: Added Firmware Revision TLV "
                   "to preformed buffer for port"
                   "%d successfully\r\n", pPortInfo->u4LocPortNum);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedAddSwRevisionTlv
 *
 *    DESCRIPTION      : This function adds LLDP MED Software Revision TLV to
 *                       the linear buffer
 *
 *    INPUT            : pPortInfo    - Pointer to port information structure
 *                       pu1BasePtr   - Pointer to base address of linear buffer
 *                       pu1Buf       - Pointer to linear buffer in which
 *                                      Software Revision tlv has to be addded
 *                       pu2LocOffset - Pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf       - Pointer to linear buffer in which 
 *                                      Software Revision tlv is addded
 *                       pu2LocOffset - Pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
PUBLIC VOID
LldpMedAddSwRevisionTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                         UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    UINT2               u2TlvHeader = 0;
    UINT2               u2SwRevTlvInfoLen = 0;
    UINT2               u2SwRevTlvLen = 0;

    *pu2LocOffset = 0;
    LLDP_TRC_ARG1 (LLDP_MED_INV_MGMT_TRC,
                   "\rLldpMedAddSwRevisionTlv: Adding Software Revision TLV to the"
                   " preformed buffer on port %d\r\n", pPortInfo->u4LocPortNum);

    LLDP_MED_GET_SW_REV_TLV_INFO_LEN (&u2SwRevTlvInfoLen);
    u2SwRevTlvLen = (UINT2) (LLDP_TLV_HEADER_LEN + u2SwRevTlvInfoLen);

    /* Check whether the linear buffer can accomodate Software Revision tlv */
    if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                    u2SwRevTlvLen) == OSIX_TRUE)
    {
        pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
        pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
        LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | LLDP_MED_INV_MGMT_TRC,
                  "LldpMedAddSwRevisionTlv: Max Pdu size exceeded!!!..\n");
        return;
    }
    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_ORG_SPEC_TLV,
                          u2SwRevTlvInfoLen, &u2TlvHeader);

    /* Put tlv header */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* Copy LLDP-MED OUI into linear buffer */
    MEMCPY (pu1Buf, gau1LldpMedOUI, LLDP_MAX_LEN_OUI);
    /* Increment buffer pointer */
    LLDP_INCR_BUF_PTR (pu1Buf, (UINT2) LLDP_MAX_LEN_OUI);
    /* Put Software revision TLV  subtype in linear buffer */
    LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_MED_SW_REVISION_SUBTYPE);

    /* Copy Software Revision into linear buffer */
    MEMCPY (pu1Buf,
            gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.au1MedLocSoftwareRev,
            LLDP_STRLEN (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.
                         au1MedLocSoftwareRev, LLDP_MED_SW_REV_LEN));
    /* Set tlv length as next offset */
    *pu2LocOffset = u2SwRevTlvLen;
    LLDP_TRC_ARG1 (LLDP_MED_INV_MGMT_TRC,
                   "\rLldpMedAddSwRevisionTlv: Added Software Revision TLV "
                   "to preformed buffer for port"
                   "%d successfully\r\n", pPortInfo->u4LocPortNum);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedAddSerialNumberTlv
 *
 *    DESCRIPTION      : This function adds LLDP MED Serial Number TLV to the
 *                       linear buffer
 *
 *    INPUT            : pPortInfo    - Pointer to port information structure
 *                       pu1BasePtr   - Pointer to base address of linear buffer
 *                       pu1Buf       - Pointer to linear buffer in which Serial
 *                                      Number tlv has to be addded
 *                       pu2LocOffset - Pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf       - Pointer to linear buffer in which Serial
 *                                      Number tlv is addded
 *                       pu2LocOffset - Pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
PUBLIC VOID
LldpMedAddSerialNumberTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                           UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    UINT2               u2TlvHeader = 0;
    UINT2               u2SerNumTlvInfoLen = 0;
    UINT2               u2SerNumTlvLen = 0;

    *pu2LocOffset = 0;
    LLDP_TRC_ARG1 (LLDP_MED_INV_MGMT_TRC,
                   "\rLldpMedAddSerialNumberTlv: Adding Serial Number TLV to the "
                   "preformed buffer on port %d\r\n", pPortInfo->u4LocPortNum);

    LLDP_MED_GET_SER_NUM_TLV_INFO_LEN (&u2SerNumTlvInfoLen);
    u2SerNumTlvLen = (UINT2) (LLDP_TLV_HEADER_LEN + u2SerNumTlvInfoLen);

    /* Check whether the linear buffer can accomodate Serial Number tlv */
    if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                    u2SerNumTlvLen) == OSIX_TRUE)
    {
        pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
        pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
        LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | LLDP_MED_INV_MGMT_TRC,
                  "LldpMedAddSerialNumberTlv: Max Pdu size exceeded!!!..\n");
        return;
    }
    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_ORG_SPEC_TLV,
                          u2SerNumTlvInfoLen, &u2TlvHeader);

    /* Put tlv header */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* Copy LLDP-MED OUI into linear buffer */
    MEMCPY (pu1Buf, gau1LldpMedOUI, LLDP_MAX_LEN_OUI);
    /* Increment buffer pointer */
    LLDP_INCR_BUF_PTR (pu1Buf, (UINT2) LLDP_MAX_LEN_OUI);
    /* Put Serial Number TLV  subtype in linear buffer */
    LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_MED_SERIAL_NUM_SUBTYPE);
    /* Copy Serial Number into linear buffer */
    MEMCPY (pu1Buf,
            gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.au1MedLocSerialNum,
            LLDP_STRLEN (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.
                         au1MedLocSerialNum, LLDP_MED_SER_NUM_LEN));
    /* Set tlv length as next offset */
    *pu2LocOffset = u2SerNumTlvLen;
    LLDP_TRC_ARG1 (LLDP_MED_INV_MGMT_TRC,
                   "\rLldpMedAddSerialNumberTlv: Added Serial Number TLV "
                   "to preformed buffer for port"
                   "%d successfully\r\n", pPortInfo->u4LocPortNum);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedAddMfgNameTlv
 *
 *    DESCRIPTION      : This function adds LLDP MED Manufacturer Name TLV to
 *                       the linear buffer
 *
 *    INPUT            : pPortInfo    - Pointer to port information structure
 *                       pu1BasePtr   - Pointer to base address of linear buffer
 *                       pu1Buf       - Pointer to linear buffer in which 
 *                                      Manufacturer Name tlv has to be addded
 *                       pu2LocOffset - Pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf       - Pointer to linear buffer in which 
 *                                      Manufacturer Name tlv is addded
 *                       pu2LocOffset - Pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
PUBLIC VOID
LldpMedAddMfgNameTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                      UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    UINT2               u2TlvHeader = 0;
    UINT2               u2MfgNameTlvInfoLen = 0;
    UINT2               u2MfgNameTlvLen = 0;

    *pu2LocOffset = 0;
    LLDP_TRC_ARG1 (LLDP_MED_INV_MGMT_TRC,
                   "\rLldpMedAddMfgNameTlv: Adding Manufacturer Name TLV to the"
                   "preformed buffer on port %d\r\n", pPortInfo->u4LocPortNum);

    LLDP_MED_GET_MFG_NAME_TLV_INFO_LEN (&u2MfgNameTlvInfoLen);
    u2MfgNameTlvLen = (UINT2) (LLDP_TLV_HEADER_LEN + u2MfgNameTlvInfoLen);

    /* Check whether the linear buffer can accomodate Manufacturer Name tlv */
    if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                    u2MfgNameTlvLen) == OSIX_TRUE)
    {
        pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
        pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
        LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | LLDP_MED_INV_MGMT_TRC,
                  "LldpMedAddMfgNameTlv: Max Pdu size exceeded!!!..\n");
        return;
    }
    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_ORG_SPEC_TLV,
                          u2MfgNameTlvInfoLen, &u2TlvHeader);

    /* Put tlv header into linear buffer */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* Copy LLDP-MED OUI into linear buffer */
    MEMCPY (pu1Buf, gau1LldpMedOUI, LLDP_MAX_LEN_OUI);
    /* Increment buffer pointer */
    LLDP_INCR_BUF_PTR (pu1Buf, (UINT2) LLDP_MAX_LEN_OUI);
    /* Put Manufacturer Name TLV  subtype in linear buffer */
    LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_MED_MFG_NAME_SUBTYPE);
    /* Copy Manufacturer Name into linear buffer */
    MEMCPY (pu1Buf,
            gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.au1MedLocMfgName,
            LLDP_STRLEN (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.
                         au1MedLocMfgName, LLDP_MED_MFG_NAME_LEN));
    /* Set tlv length as next offset */
    *pu2LocOffset = u2MfgNameTlvLen;
    LLDP_TRC_ARG1 (LLDP_MED_INV_MGMT_TRC,
                   "\rLldpMedAddMfgNameTlv: Added Manufacturer Name TLV "
                   "to preformed buffer for port"
                   "%d successfully\r\n", pPortInfo->u4LocPortNum);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedAddModelNameTlv
 *
 *    DESCRIPTION      : This function adds LLDP MED Model Name TLV to the
 *                       linear buffer
 *
 *    INPUT            : pPortInfo    - Pointer to port information structure
 *                       pu1BasePtr   - Pointer to base address of linear buffer
 *                       pu1Buf       - Pointer to linear buffer in which Model
 *                                      Name tlv has to be addded
 *                       pu2LocOffset - Pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf       - Poitner to linear buffer in which Model
 *                                      Name tlv is addded
 *                       pu2LocOffset - Pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
PUBLIC VOID
LldpMedAddModelNameTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                        UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    UINT2               u2TlvHeader = 0;
    UINT2               u2ModelNameTlvInfoLen = 0;
    UINT2               u2ModelNameTlvLen = 0;

    *pu2LocOffset = 0;
    LLDP_TRC_ARG1 (LLDP_MED_INV_MGMT_TRC,
                   "\rLldpMedAddModelNameTlv: Adding Model Name TLV to the "
                   "preformed buffer on port %d\r\n", pPortInfo->u4LocPortNum);

    LLDP_MED_GET_MODEL_NAME_TLV_INFO_LEN (&u2ModelNameTlvInfoLen);
    u2ModelNameTlvLen = (UINT2) (LLDP_TLV_HEADER_LEN + u2ModelNameTlvInfoLen);

    /* Check whether the linear buffer can accomodate Model Name tlv */
    if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                    u2ModelNameTlvLen) == OSIX_TRUE)
    {
        pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
        pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
        LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | LLDP_MED_INV_MGMT_TRC,
                  "LldpMedAddModelNameTlv: Max Pdu size exceeded!!!..\n");
        return;
    }
    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_ORG_SPEC_TLV,
                          u2ModelNameTlvInfoLen, &u2TlvHeader);

    /* Put tlv header */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* Copy LLDP-MED OUI into linear buffer */
    MEMCPY (pu1Buf, gau1LldpMedOUI, LLDP_MAX_LEN_OUI);
    /* Increment buffer pointer */
    LLDP_INCR_BUF_PTR (pu1Buf, (UINT2) LLDP_MAX_LEN_OUI);
    /* Put Model Name TLV  subtype in linear buffer */
    LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_MED_MODEL_NAME_SUBTYPE);
    /* Copy Model Name into linear buffer */
    MEMCPY (pu1Buf,
            gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.au1MedLocModelName,
            LLDP_STRLEN (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.
                         au1MedLocModelName, LLDP_MED_MODEL_NAME_LEN));
    /* Set tlv length as next offset */
    *pu2LocOffset = u2ModelNameTlvLen;
    LLDP_TRC_ARG1 (LLDP_MED_INV_MGMT_TRC,
                   "\rLldpMedAddModelNameTlv: Added Model Name TLV "
                   "to preformed buffer for port"
                   "%d successfully\r\n", pPortInfo->u4LocPortNum);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedAddAssetIdTlv
 *
 *    DESCRIPTION      : This function adds LLDP MED Asset ID TLV to the
 *                       linear buffer
 *
 *    INPUT            : pPortInfo    - Pointer to port information structure
 *                       pu1BasePtr   - Pointer to base address of linear buffer
 *                       pu1Buf       - Pointer to linear buffer in which Asset 
 *                                      ID tlv has to be addded
 *                       pu2LocOffset - Pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf       - Pointer to linear buffer in which Asset 
 *                                      ID tlv is addded
 *                       pu2LocOffset - Pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
PUBLIC VOID
LldpMedAddAssetIdTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                      UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    UINT2               u2TlvHeader = 0;
    UINT2               u2AssetIdTlvInfoLen = 0;
    UINT2               u2AssetIdTlvLen = 0;

    *pu2LocOffset = 0;
    LLDP_TRC_ARG1 (LLDP_MED_INV_MGMT_TRC,
                   "\rLldpMedAddAssetIdTlv: Adding Asset ID TLV to the"
                   "preformed buffer on port %d\r\n", pPortInfo->u4LocPortNum);

    LLDP_MED_GET_ASSET_ID_TLV_INFO_LEN (&u2AssetIdTlvInfoLen);
    u2AssetIdTlvLen = (UINT2) (LLDP_TLV_HEADER_LEN + u2AssetIdTlvInfoLen);

    /* Check whether the linear buffer can accomodate Asset ID tlv */
    if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                    u2AssetIdTlvLen) == OSIX_TRUE)
    {
        pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
        pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
        LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | LLDP_MED_INV_MGMT_TRC,
                  "LldpMedAddAssetIdTlv: Max Pdu size exceeded!!!..\n");
        return;
    }
    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_ORG_SPEC_TLV,
                          u2AssetIdTlvInfoLen, &u2TlvHeader);

    /* Put tlv header */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* Copy LLDP-MED OUI into linear buffer */
    MEMCPY (pu1Buf, gau1LldpMedOUI, LLDP_MAX_LEN_OUI);
    /* Increment buffer pointer */
    LLDP_INCR_BUF_PTR (pu1Buf, (UINT2) LLDP_MAX_LEN_OUI);
    /* Put Asset Id TLV  subtype in linear buffer */
    LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_MED_ASSET_ID_SUBTYPE);
    /* Copy Asset ID into linear buffer */
    MEMCPY (pu1Buf,
            gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.au1MedLocAssetId,
            LLDP_STRLEN (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.
                         au1MedLocAssetId, LLDP_MED_ASSET_ID_LEN));
    /* Set tlv length as next offset */
    *pu2LocOffset = u2AssetIdTlvLen;
    LLDP_TRC_ARG1 (LLDP_MED_INV_MGMT_TRC,
                   "\rLldpMedAddAssetIdTlv: Added Model Name TLV "
                   "to preformed buffer for port"
                   "%d successfully\r\n", pPortInfo->u4LocPortNum);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedUtlHandlePolicyChg
 *
 *    DESCRIPTION      : Reconstruct the buffer for all the agents associated  
 *                       on port where local policy information is changed
 *                       
 *    INPUT            : i4PortIndex - Interface Index
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC VOID
LldpMedUtlHandlePolicyChg (INT4 i4PortIndex)
{
    tLldpLocPortInfo   *pLocPortInfo = NULL;

    pLocPortInfo = (tLldpLocPortInfo *)
        RBTreeGetFirst (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree);

    while (pLocPortInfo != NULL)
    {
        if (pLocPortInfo->i4IfIndex == i4PortIndex)
        {
            if (pLocPortInfo->LocMedCapInfo.u2MedLocCapTxEnable &
                LLDP_MED_NETWORK_POLICY_TLV)
            {
                if (LldpTxUtlHandleLocPortInfoChg (pLocPortInfo) !=
                    OSIX_SUCCESS)
                {
                    LLDP_TRC (ALL_FAILURE_TRC | LLDP_MED_NW_POL_TRC,
                              "LldpMedUtlHandlePolicyChg:"
                              "LldpTxUtlHandleLocPortInfoChg failed\r\n");
                }
            }
        }
        pLocPortInfo = (tLldpLocPortInfo *) RBTreeGetNext
            (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
             (tRBElem *) pLocPortInfo, LldpAgentInfoUtlRBCmpInfo);
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedAddLocIdTlv
 *
 *    DESCRIPTION      : This function adds LLDP MED Location Identification TLV
 *                       to the linear buffer
 *
 *    INPUT            : pPortInfo    - Pointer to port information structure
 *                       pu1BasePtr   - Pointer to base address of linear buffer
 *
 *    OUTPUT           : pu1Buf       - Pointer to linear buffer in which LLDP
 *                                      Location Identification tlv is added
 *                       pu2LocOffset - Pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : NONE
 *
 ****************************************************************************/

PUBLIC VOID
LldpMedAddLocIdTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                    UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    tLldpMedLocLocationInfo LocIdInfo;
    tLldpMedLocLocationInfo *pLocIdInfo = NULL;
    tLldpRemoteNode    *pCurrRemoteNode = NULL;
    tLldpRemoteNode    *pNextRemoteNode = NULL;
    UINT2               u2TlvHeader = 0;
    UINT2               u2LocIdTlvInfoLen = 0;
    UINT2               u2LocIdTlvLen = 0;
    UINT1              *pu1InitialPtr = pu1Buf;
    BOOL1               bLocIdTlv = 0;

    *pu2LocOffset = 0;
    MEMSET (&LocIdInfo, 0, sizeof (tLldpMedLocLocationInfo));
    LLDP_TRC_ARG1 (LLDP_MED_LOC_ID_TRC,
                   "\rLldpMedAddLocIdTlv: Adding LLDP-MED Location Identification TLV to "
                   "the preformed buffer on port %d\r\n",
                   pPortInfo->u4LocPortNum);

    if ((pCurrRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_CRITICAL_TRC |
                  LLDP_MED_LOC_ID_TRC,
                  "\tMemory Allocation Failure for New pCurrRemoteNode \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return;
    }
    MEMSET (pCurrRemoteNode, 0, sizeof (tLldpRemoteNode));

    pCurrRemoteNode->i4RemLocalPortNum = pPortInfo->i4IfIndex;
    pNextRemoteNode = (tLldpRemoteNode *) RBTreeGetNext
        (gLldpGlobalInfo.RemSysDataRBTree, (tRBElem *) pCurrRemoteNode, NULL);
    while ((pNextRemoteNode != NULL) &&
           (pNextRemoteNode->i4RemLocalPortNum == pPortInfo->i4IfIndex))
    {
        /* Check for any remote device is Class III device
           If so send Location Tlv to all Remote Nodes */
        if (pNextRemoteNode->RemMedCapableInfo.u1MedRemDeviceClass
            == LLDP_MED_CLASS_3_DEVICE)
        {
            bLocIdTlv = LLDP_MED_TRUE;
            break;
        }
        pCurrRemoteNode->u4RemLastUpdateTime =
            pNextRemoteNode->u4RemLastUpdateTime;
        pCurrRemoteNode->i4RemLocalPortNum = pNextRemoteNode->i4RemLocalPortNum;
        pCurrRemoteNode->i4RemIndex = pNextRemoteNode->i4RemIndex;
        pCurrRemoteNode->u4DestAddrTblIndex =
            pNextRemoteNode->u4DestAddrTblIndex;

        pNextRemoteNode = (tLldpRemoteNode *) RBTreeGetNext
            (gLldpGlobalInfo.RemSysDataRBTree, (tRBElem *) pCurrRemoteNode,
             NULL);
    }
    if (pCurrRemoteNode != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                            (UINT1 *) pCurrRemoteNode);
    }
    if (bLocIdTlv == LLDP_MED_TRUE)
    {

        /* Add the Information of Location Identification Tlv for
         * particular port */

        /* Get the first Index of Location Identification Table */
        LocIdInfo.u4IfIndex = (UINT4) pPortInfo->i4IfIndex;

        pLocIdInfo =
            (tLldpMedLocLocationInfo *) RBTreeGetNext (gLldpGlobalInfo.
                                                       LldpMedLocLocationInfoRBTree,
                                                       (tRBElem *) & LocIdInfo,
                                                       NULL);

        while ((pLocIdInfo != NULL) &&
               (pLocIdInfo->u4IfIndex == (UINT4) pPortInfo->i4IfIndex))
        {

            u2LocIdTlvInfoLen = 0;
            u2LocIdTlvLen = 0;

            if (pLocIdInfo->i4RowStatus != ACTIVE)
            {
                LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                          LLDP_MED_LOC_ID_TRC,
                          "LldpMedAddLocIdTlv: Row Status is not active!!!..\n");
                pLocIdInfo =
                    (tLldpMedLocLocationInfo *) RBTreeGetNext (gLldpGlobalInfo.
                                                               LldpMedLocLocationInfoRBTree,
                                                               (tRBElem *)
                                                               pLocIdInfo,
                                                               NULL);

                continue;

            }
            if (pLocIdInfo->u1LocationDataFormat == LLDP_MED_COORDINATE_LOC)
            {
                u2LocIdTlvInfoLen =
                    (UINT2) ((LLDP_MED_MAX_COORDINATE_LOC_LENGTH +
                              LLDP_MED_LOC_DATA_FORMAT_LEN + LLDP_MAX_LEN_OUI +
                              LLDP_TLV_SUBTYPE_LEN));

            }
            else if (pLocIdInfo->u1LocationDataFormat == LLDP_MED_CIVIC_LOC)
            {
                u2LocIdTlvInfoLen =
                    (UINT2) (((pLocIdInfo->au1LocationID[0] + 1) +
                              LLDP_MED_LOC_DATA_FORMAT_LEN + LLDP_MAX_LEN_OUI +
                              LLDP_TLV_SUBTYPE_LEN));
            }
            else
            {
                LLDP_MED_GET_LOC_ID_INFO_LEN (&u2LocIdTlvInfoLen, pLocIdInfo);
            }
            u2LocIdTlvLen = (UINT2) (LLDP_TLV_HEADER_LEN + u2LocIdTlvInfoLen);

            /* Check whether the linear buffer can accomodate LLDP MED Location
               Identification tlv */
            if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                            u2LocIdTlvLen) == OSIX_TRUE)
            {
                pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
                pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
                LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                          LLDP_MED_LOC_ID_TRC,
                          "LldpMedAddLocIdTlv: Max Pdu size exceeded!!!..\n");
                return;
            }

            LLDP_FORM_TLV_HEADER ((UINT2) LLDP_ORG_SPEC_TLV,
                                  u2LocIdTlvInfoLen, &u2TlvHeader);

            /* Put tlv header */
            LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
            /* Copy LLDP-MED OUI into linear buffer */
            MEMCPY (pu1Buf, gau1LldpMedOUI, LLDP_MAX_LEN_OUI);
            /* Increment buffer pointer */
            LLDP_INCR_BUF_PTR (pu1Buf, (UINT2) LLDP_MAX_LEN_OUI);
            /* Put Location Identification TLV subtype in linear buffer */
            LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_MED_LOCATION_ID_SUBTYPE);

            /* Put Location Data format in linear buffer */

            /* There is some deviation observed between standard MIB and LLDP-MED 
             * draft version LLDP-MED Draft 07-R1.
             *
             * As per standard MIB:
             * LocationSubtype ::= TEXTUAL-CONVENTION
             *     SYNTAX      INTEGER  {
             *                    unknown(1),
             *               coordinateBased(2),
             *               civicAddress(3),
             *               elin(4)
             *               }
             *
             * As per standard document:
             *   Location data 
             *   format type         value 
             *   --------------------------
             *   0                   Invalid 
             *   1                   Coordinate-based LCI  
             *   2                   Civic Address LCI  
             *   3                   ECS ELIN  
             *   4 - 255             Reserved for future expansion
             *
             * To support both MIB and standard, the below covertion takes place.
             * */

            if (pLocIdInfo->u1LocationDataFormat == LLDP_MED_COORDINATE_LOC)
            {
                LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_MED_COORDINATE);
            }
            else if (pLocIdInfo->u1LocationDataFormat == LLDP_MED_CIVIC_LOC)
            {
                LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_MED_CIVIC);
            }
            else if (pLocIdInfo->u1LocationDataFormat == LLDP_MED_ELIN_LOC)
            {
                LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_MED_ELIN);
            }
            /* LLDP MED Location Identification info */
            if (pLocIdInfo->u1LocationDataFormat == LLDP_MED_COORDINATE_LOC)
            {
                MEMCPY (pu1Buf, pLocIdInfo->au1LocationID,
                        LLDP_MED_MAX_COORDINATE_LOC_LENGTH);
                LLDP_INCR_BUF_PTR (pu1Buf, LLDP_MED_MAX_COORDINATE_LOC_LENGTH);
            }
            else if (pLocIdInfo->u1LocationDataFormat == LLDP_MED_CIVIC_LOC)
            {
                MEMCPY (pu1Buf, pLocIdInfo->au1LocationID,
                        (pLocIdInfo->au1LocationID[0] + 1));
                LLDP_INCR_BUF_PTR (pu1Buf, (pLocIdInfo->au1LocationID[0] + 1));
            }
            else
            {
                MEMCPY (pu1Buf, pLocIdInfo->au1LocationID,
                        STRLEN (pLocIdInfo->au1LocationID));

                /* Increment buffer pointer */
                LLDP_INCR_BUF_PTR (pu1Buf, STRLEN (pLocIdInfo->au1LocationID));
            }
            pLocIdInfo = (tLldpMedLocLocationInfo *) RBTreeGetNext
                (gLldpGlobalInfo.
                 LldpMedLocLocationInfoRBTree, (tRBElem *) pLocIdInfo, NULL);

        }

        *pu2LocOffset = (UINT2) (pu1Buf - pu1InitialPtr);
        LLDP_TRC_ARG1 (LLDP_MED_LOC_ID_TRC,
                       "\rLldpMedAddLocIdTlv: Added Location Identification TLV "
                       "to preformed buffer for port"
                       "%d successfully\r\n", pPortInfo->u4LocPortNum);
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedAddPowMDITlv
 *
 *    DESCRIPTION      : This function adds LLDP-MED Extended Power Via MDI TLV
 *                       to the linear buffer
 *
 *    INPUT            : pPortInfo    - Pointer to port information structure
 *                       pu1BasePtr   - Pointer to base address of linear buffer
 *
 *    OUTPUT           : pu1Buf       - Pointer to linear buffer in which LLDP
 *                                      MED extended power via MDI tlv are added
 *                       pu2LocOffset - Pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : NONE
 *
 ****************************************************************************/

PUBLIC VOID
LldpMedAddPowMDITlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                     UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    tLldpLocPortTable   PortTable;
    tLldpLocPortTable  *pPortTable = NULL;
    UINT2               u2TlvHeader = 0;
    UINT2               u2PowerAv = 0;
    UINT1              *pu1InitialPtr = pu1Buf;
    UINT1               u1PowerInfo = 0;

    *pu2LocOffset = 0;
    MEMSET (&PortTable, 0, sizeof (tLldpLocPortTable));
    LLDP_TRC_ARG1 (LLDP_MED_EX_POW_TRC,
                   "\rLldpMedAddPowMDITlv: Adding LLDP-MED Extended Power-via-MDI TLV to"
                   "the preformed buffer on port %d\r\n",
                   pPortInfo->u4LocPortNum);

    /* Add all the Power-via-MDI Tlv Information
     * for the particular port */

    /* Get the Index of Port Table */
    PortTable.i4IfIndex = pPortInfo->i4IfIndex;
    pPortTable = (tLldpLocPortTable *) RBTreeGet
        (gLldpGlobalInfo.LldpPortInfoRBTree, &PortTable);

    if (pPortTable != NULL)
    {
        /* Check whether the linear buffer can accomodate LLDP MED Extended
           Power-Via-MDI tlv */
        if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                        LLDP_MED_POWER_TLV_LEN) == OSIX_TRUE)
        {
            pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
            pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
            LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | LLDP_MED_EX_POW_TRC,
                      "LldpMedAddPowMDITlv: Max Pdu size exceeded!!!..\n");
            return;
        }

        LLDP_FORM_TLV_HEADER ((UINT2) LLDP_ORG_SPEC_TLV,
                              (UINT2) LLDP_MED_POWER_TLV_INFO_LEN,
                              &u2TlvHeader);
        /* Put tlv header */
        LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
        /* Copy LLDP-MED OUI into linear buffer */
        MEMCPY (pu1Buf, gau1LldpMedOUI, LLDP_MAX_LEN_OUI);
        /* Increment buffer pointer */
        LLDP_INCR_BUF_PTR (pu1Buf, (UINT2) LLDP_MAX_LEN_OUI);
        /* Put Extended Power MDI TLV subtype in linear buffer */
        LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_MED_EXT_PW_MDI_SUBTYPE);

        /* LLDP MED Power Info */

        /* Add Power Device Type */
        u1PowerInfo = gLldpGlobalInfo.LocSysInfo.u1MedLocPoEDeviceType;
        u1PowerInfo = u1PowerInfo << LLDP_MED_POWER_TYPE_SHIFT_BIT;
        /* Add Power Source */
        u1PowerInfo |= gLldpGlobalInfo.LocSysInfo.u1MedLocPSEPowerSource <<
            LLDP_MED_POWER_SRC_SHIFT_BIT;
        /* Add Power Priority */
        u1PowerInfo |= pPortTable->MedPSEPowerInfo.u1PsePowerPriority;
        /* Add Power Info */
        LLDP_PUT_1BYTE (pu1Buf, u1PowerInfo);

        /* Add Power Value */
        u2PowerAv = pPortTable->MedPSEPowerInfo.u2PsePowerAv;
        LLDP_PUT_2BYTE (pu1Buf, u2PowerAv);

        *pu2LocOffset = (UINT2) (pu1Buf - pu1InitialPtr);
        LLDP_TRC_ARG1 (LLDP_MED_EX_POW_TRC,
                       "\rLldpMedAddPowMDITlv: Added Extended Power-Via-MDI TLV "
                       "to preformed buffer for port"
                       "%d successfully\r\n", pPortInfo->u4LocPortNum);
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedUtlHandleLocationChg
 *
 *    DESCRIPTION      : Reconstruct the buffer for all the agents associated
 *                       on port where location information is changed
 *
 *    INPUT            : i4PortIndex - Interface Index
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC VOID
LldpMedUtlHandleLocationChg (INT4 i4PortIndex)
{
    tLldpLocPortInfo   *pLocPortInfo = NULL;

    pLocPortInfo = (tLldpLocPortInfo *)
        RBTreeGetFirst (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree);

    while (pLocPortInfo != NULL)
    {
        if (pLocPortInfo->i4IfIndex == i4PortIndex)
        {
            if (pLocPortInfo->LocMedCapInfo.u2MedLocCapTxEnable &
                LLDP_MED_LOCATION_ID_TLV)
            {
                if (LldpTxUtlHandleLocPortInfoChg (pLocPortInfo) !=
                    OSIX_SUCCESS)
                {
                    LLDP_TRC (ALL_FAILURE_TRC | LLDP_MED_LOC_ID_TRC,
                              "LldpMedUtlHandleLocationChg:"
                              "LldpTxUtlHandleLocPortInfoChg failed\r\n");
                    return;
                }
            }
        }
        pLocPortInfo = (tLldpLocPortInfo *) RBTreeGetNext
            (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
             (tRBElem *) pLocPortInfo, LldpAgentInfoUtlRBCmpInfo);
    }
    return;
}

#endif
