/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldtlv.c,v 1.44 2017/10/11 13:42:02 siva Exp $
 *
 * Description: This File contains all the basic management set TLV related
 *              Parsing, Validation and remote MIB updation routines.
 *****************************************************************************/
#ifndef _LLDP_TLV_C_
#define _LLDP_TLV_C_
#include "lldinc.h"

/* TLV Decoding routines */
PRIVATE INT4 LldpTlvDecodeChassisIdInfo PROTO ((UINT1 *pu1TlvInfo,
                                                UINT2 u2TlvInfoLength,
                                                tLldpRemoteNode * pRemoteNode));
PRIVATE INT4 LldpTlvDecodePortIdInfo PROTO ((UINT1 *pu1TlvInfo,
                                             UINT2 u2TlvInfoLength,
                                             tLldpRemoteNode * pRemoteNode));
PRIVATE VOID LldpTlvDecodeTtlInfo PROTO ((UINT1 *pu1TlvInfo,
                                          UINT2 u2TlvInfoLength,
                                          tLldpRemoteNode * pRemoteNode));
PRIVATE VOID LldpTlvDecodePortDescInfo PROTO ((UINT1 *pu1TlvInfo,
                                               UINT2 u2TlvInfoLength,
                                               tLldpRemoteNode * pRemoteNode));
PRIVATE INT4 LldpTlvDecodeSysNameInfo PROTO ((UINT1 *pu1TlvInfo,
                                              UINT2 u2TlvLength,
                                              tLldpRemoteNode * pRemoteNode));
PRIVATE INT4 LldpTlvDecodeSysDescInfo PROTO ((UINT1 *pu1TlvInfo,
                                              UINT2 u2TlvLength,
                                              tLldpRemoteNode * pRemoteNode));
PRIVATE INT4 LldpTlvDecodeSysCapaInfo PROTO ((UINT1 *pu1TlvInfo,
                                              UINT2 u2TlvLength,
                                              tLldpRemoteNode * pRemoteNode));
PRIVATE INT4
    LldpTlvDecodeUnknownTlvInfo PROTO ((UINT1 *pu1TlvInfo,
                                        UINT2 u2TlvLength,
                                        tLldpRemUnknownTLVTable *
                                        pUnknownTlvNode));
PRIVATE VOID LldpTlvDecodeOrgDefInfo PROTO ((UINT1 *pu1TlvInfo,
                                             UINT2 u2TlvInfoLen,
                                             tLldpRemOrgDefInfoTable *
                                             pRemOrgDefNode));

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvProcChassisIdTlv
 *
 *    DESCRIPTION      : This function Processes the Remote ChassisId TLV.
 *                       Decode the Information from the TLV Information field
 *                       and then Validate the information. 
 *
 *    INPUT            : pu1Tlv - Buffer Points to the TLV 
 *                       u2TlvInfoLength - Length of the Information Field
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTlvProcChassisIdTlv (UINT1 *pu1Tlv, UINT2 u2TlvInfoLength)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT1              *pu1TlvInfo = NULL;
    UINT2               u2ActualTlvInfoLen = 0;
    UINT2               u2ChassisIdLen = 0;

    /* Allocate Memory for Remote Node */
    if ((pRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_MANDATORY_TLV_TRC,
                  "LldpTlvProcChassisIdTlv: Failed to Allocate Memory "
                  "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }

    LLDP_TRC (LLDP_MANDATORY_TLV_TRC,
              "LldpTlvProcChassisIdTlv: Processing Chassis ID TLV :\r\n");

    MEMSET (pRemoteNode, 0, sizeof (tLldpRemoteNode));
    /* make pu1TlvInfo to point to TlvInfo field where 
     * TLVinfo = Chassis id Subtype + chassis id info*/
    pu1TlvInfo = pu1Tlv + LLDP_TLV_HEADER_LEN;

    /* The TLV Info Length (2-256 (ChassisId Subtype + ChassisId) )for
     * Chassis ID TLV is verified while getting MSAP id from PDU
     * (LldpRxGetMSAPIdFromPdu). Hence no need to verify here. */

    /* get the chassis id info */
    /* Decode the Tlv Information */
    if (LldpTlvDecodeChassisIdInfo (pu1TlvInfo, u2TlvInfoLength, pRemoteNode)
        == OSIX_FAILURE)
    {
        /* Discard LLDPDU */
        /* Release the memory allocated for Remote Node */
        MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                            (UINT1 *) pRemoteNode);
        return OSIX_FAILURE;
    }

    LldpUtilGetChassisIdLen (pRemoteNode->i4RemChassisIdSubtype,
                             pRemoteNode->au1RemChassisId, &u2ChassisIdLen);
    u2ActualTlvInfoLen = (UINT2) (u2ChassisIdLen + LLDP_TLV_SUBTYPE_LEN);
    /* if u2TlvInfoLength > Actual TLV information string length 
     * [std IEEE 802.1AB-2005, clause 10.3.2.1 - (b)] */
    /* the extra octets can be ignored and
     * next tlv location = CurrenTlvLocation + 
     * (u2TlvInfoLength + LLDP_TLV_HEADER_LEN)*/

    /* if u2TlvInfoLength < Actual TLV information string length 
     * [std IEEE 802.1AB-2005, clause 10.3.2.1 - (c)] */
    if (u2TlvInfoLength < u2ActualTlvInfoLen)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC | ALL_FAILURE_TRC,
                  "\tChassis ID is Too Big\r\n");
        /* Discard LLDPDU */
        /* Release the memory allocated for Remote Node */
        MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                            (UINT1 *) pRemoteNode);
        return OSIX_FAILURE;
    }

    LLDP_TRC (LLDP_MANDATORY_TLV_TRC, "\tValidated Chassis ID TLV\r\n");
    /* Release the memory allocated for Remote Node */
    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId, (UINT1 *) pRemoteNode);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvProcPortIdTlv 
 *
 *    DESCRIPTION      : This function Process the Remote PortId TLV.
 *                       Decode the Information from the TLV Information field
 *                       and then Validate the information. 
 *
 *    INPUT            : pu1Tlv - Buffer Points to the TLV 
 *                       u2TlvInfoLength - Length of the Information Field
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTlvProcPortIdTlv (UINT1 *pu1Tlv, UINT2 u2TlvInfoLength)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT1              *pu1TlvInfo = NULL;
    UINT2               u2ActualTlvInfoLen = 0;
    UINT2               u2PortIdLen = 0;

    /* Allocate Memory for Remote Node */
    if ((pRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_MANDATORY_TLV_TRC,
                  "LldpTlvProcPortIdTlv: Failed to Allocate Memory "
                  "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }

    LLDP_TRC (LLDP_MANDATORY_TLV_TRC,
              "LldpTlvProcPortIdTlv: Processing Port ID TLV :\r\n");

    MEMSET (pRemoteNode, 0, sizeof (tLldpRemoteNode));
    /* make pu1TlvInfo to point to TlvInfo field where 
     * TLVinfo = Port id Subtype + port id info*/
    pu1TlvInfo = pu1Tlv + LLDP_TLV_HEADER_LEN;

    /* The TLV Info Length ( 2-256 (PortId Subtype + PortId) ) for
     * Port ID TLV is verified while getting MSAP id from PDU
     * (LldpRxGetMSAPIdFromPdu). Hence no need to verify here. */

    /* get the port id info */
    /* Decode the Tlv Information */
    if (LldpTlvDecodePortIdInfo (pu1TlvInfo, u2TlvInfoLength, pRemoteNode)
        == OSIX_FAILURE)
    {
        /* Discard LLDPDU */
        /* Release the memory allocated for Remote Node */
        MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                            (UINT1 *) pRemoteNode);
        return OSIX_FAILURE;
    }

    LldpUtilGetPortIdLen (pRemoteNode->i4RemPortIdSubtype,
                          pRemoteNode->au1RemPortId, &u2PortIdLen);
    u2ActualTlvInfoLen = (UINT2) (u2PortIdLen + LLDP_TLV_SUBTYPE_LEN);
    /* if u2TlvInfoLength > Actual TLV information string length 
     * [std IEEE 802.1AB-2005, clause 10.3.2.1 - (b)] */
    /* the extra octets can be ignored and
     * next tlv location = CurrenTlvLocation + 
     * (u2TlvInfoLength + LLDP_TLV_HEADER_LEN)*/

    /* if u2TlvInfoLength < Actual TLV information string length 
     * [std IEEE 802.1AB-2005, clause 10.3.2.1 - (c)] */
    if (u2TlvInfoLength < u2ActualTlvInfoLen)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC | ALL_FAILURE_TRC,
                  "\tPort ID is Too Big\r\n");
        /* Discard LLDPDU */
        /* Release the memory allocated for Remote Node */
        MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                            (UINT1 *) pRemoteNode);
        return OSIX_FAILURE;
    }

    LLDP_TRC (LLDP_MANDATORY_TLV_TRC, "\tValidated Port Id TLV\r\n");
    /* Release the memory allocated for Remote Node */
    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId, (UINT1 *) pRemoteNode);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvProcTtlTlv 
 *
 *    DESCRIPTION      : This function Process the Remote TTL TLV.
 *                       Decode the Information from the TLV Information field
 *                       and then Validate the information. 
 *
 *    INPUT            : pu1Tlv - Buffer Points to the TLV 
 *                       u2TlvInfoLength - Length of the Information Field
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTlvProcTtlTlv (UINT1 *pu1Tlv, UINT2 u2TlvInfoLength)
{
    UNUSED_PARAM (pu1Tlv);
    LLDP_TRC (LLDP_MANDATORY_TLV_TRC,
              "LldpTlvProcTtlTlv: Processing TTL TLV :\r\n");

    /* TLV Validation */
    /* 1. TLV information string length validation - [std IEEE 802.1AB-2005,
     *    clause 10.3.2 - (c) 2 ] */
    /* 2 < u2TlvInfoLength */
    if (u2TlvInfoLength < LLDP_TTL_TLV_INFO_LEN)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC | ALL_FAILURE_TRC,
                  "\tTTL is Too Big\r\n");
        /* Discard LLDPDU */
        return OSIX_FAILURE;
    }

    LLDP_TRC (LLDP_MANDATORY_TLV_TRC, "\tValidated Time To Live TLV\r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvProcPortDescTlv 
 *
 *    DESCRIPTION      : This function Process the Remote Portdesc TLV.
 *                       Decode the Information from the TLV Information field
 *                       and then Validate the information. 
 *
 *    INPUT            : pLocPortInfo - Port Info Structure
 *                       pu1Tlv - Buffer Points to the TLV 
 *                       u2TlvInfoLength - Length of the Information Field
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTlvProcPortDescTlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv,
                        UINT2 u2TlvInfoLength)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT1              *pu1TlvInfo = NULL;

    /* Allocate Memory for Remote Node */
    if ((pRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_PORT_DESC_TRC,
                  "LldpTlvProcPortDescTlv: Failed to Allocate Memory "
                  "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }

    LLDP_TRC (LLDP_PORT_DESC_TRC, "LldpTlvProcPortDescTlv: "
              "Processing Port Description TLV :\r\n");

    /* TLV information string validation. 
     * u2TlvInfoLength > 255 */
    if (u2TlvInfoLength > LLDP_MAX_LEN_PORTDESC)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC | ALL_FAILURE_TRC,
                  "\tPort Description is Too Big\r\n");
        /*As per IEEE 802.1AB-2009, subclause 9.2.7.7.2 (d), if the TLV
         * informatin string length is greater than the maximum permitted
         * lenghth, then the TLV is discarded */
        if (gLldpGlobalInfo.i4Version == LLDP_VERSION_09)
        {
            /* Release the memory allocated for Remote Node */
            MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                                (UINT1 *) pRemoteNode);
            LLDP_RX_DISCARD_TLV (pu1Tlv, u2TlvInfoLength);
            LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
            if (OSIX_FALSE == pLocPortInfo->bstatsFramesInerrorsTotal)
            {
                LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
                pLocPortInfo->bstatsFramesInerrorsTotal = OSIX_TRUE;
            }
            return OSIX_SUCCESS;
        }
    }

    MEMSET (pRemoteNode, 0, sizeof (tLldpRemoteNode));
    /* The received PDU should have only one process description TLV,
     * check whether this TLV has already been received/processed, 
     * if already received/processed dont process this and retrun success */
    if (LLDP_IS_SET_TLV_RCVD_BMP (pLocPortInfo, LLDP_TLV_RCVD_PORT_DESCR_BMP))
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_PORT_DESC_TRC,
                  "\tLLDPDU is contains more than 1 port desc TLV. "
                  "So discarding the Current TLV\r\n");
        LLDP_RX_DISCARD_TLV (pu1Tlv, u2TlvInfoLength);
        LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
        if (OSIX_FALSE == pLocPortInfo->bstatsFramesInerrorsTotal)
        {
            LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
            pLocPortInfo->bstatsFramesInerrorsTotal = OSIX_TRUE;
        }
        /* Release the memory allocated for Remote Node */
        MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                            (UINT1 *) pRemoteNode);
        return OSIX_SUCCESS;
    }
    else
    {
        LLDP_SET_TLV_RCVD_BMP (pLocPortInfo, LLDP_TLV_RCVD_PORT_DESCR_BMP);
    }

    /* make pu1TlvInfo to point to TlvInfo field where 
     * TLVinfo = Port id Subtype + port id info*/
    pu1TlvInfo = pu1Tlv + LLDP_TLV_HEADER_LEN;

    /* TLV Validation */

    /* if u2TlvInfoLength > Actual TLV information string length 
     * [std IEEE 802.1AB-2005, clause 10.3.2.1 - (b)] */
    /* the extra octets can be ignored by getting the system description
     * with the lu2TlvInfoLength length and
     * next tlv location = CurrenTlvLocation + 
     * (u2TlvInfoLength + LLDP_TLV_HEADER_LEN)*/

    /* Decode the Tlv Information */
    LldpTlvDecodePortDescInfo (pu1TlvInfo, u2TlvInfoLength, pRemoteNode);

    LLDP_TRC (LLDP_PORT_DESC_TRC, "\tValidated Port Description TLV\r\n");
    /* Release the memory allocated for Remote Node */
    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId, (UINT1 *) pRemoteNode);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvProcEndOfPduTlv 
 *
 *    DESCRIPTION      : This function Process the End Of LLDPDU  TLV.
 *
 *    INPUT            : pLocPortInfo - Port Info Structure
 *                       pu1Tlv - Buffer Points to the TLV 
 *                       u2TlvInfoLength - Length of the Information Field
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTlvProcEndOfPduTlv (UINT2 u2TlvInfoLength)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (u2TlvInfoLength != 0)
    {
        /* Discard PDU */
        LLDP_TRC (ALL_FAILURE_TRC,
                  "\tEND of TLV should not contain any payload. Discard the "
                  "LLDPDU.\r\n");
        i4RetVal = OSIX_FAILURE;
    }

    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvProcSysNameTlv
 *
 *    DESCRIPTION      : This function Process the Remote System Name TLV.
 *                       Decode the Information from the TLV Information field
 *                       and then Validate the information. Also notify 
 *                       if any protocol misconfiguration errors like duplicate
 *                       system name.
 *
 *    INPUT            : pLocPortInfo - Port Info Structure
 *                       pu1Tlv - Buffer Points to the TLV 
 *                       u2TlvLength - Length of the Information Field
 *
 *                       NOTE: LLDPDU will be discarded if this function
 *                       returns Failure.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS - If TLV passed in validation
 *                       OSIX_FAILURE - If TLV is failed in validation.
 *
 ****************************************************************************/
INT4
LldpTlvProcSysNameTlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv,
                       UINT2 u2TlvLength)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT1              *pu1TlvInfo = NULL;

    /* Create Memory for Remote Node */
    if ((pRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_SYS_NAME_TRC,
                  "LldpTlvProcSysNameTlv: Failed to Allocate Memory "
                  "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }

    MEMSET (pRemoteNode, 0, sizeof (tLldpRemoteNode));

    LLDP_TRC (LLDP_SYS_NAME_TRC,
              "LldpTlvProcSysNameTlv: Processing System Name TLV :\r\n");

    /* TLV information string validation.
     * u2TlvLength > 255 */
    if (u2TlvLength > LLDP_MAX_LEN_SYSNAME)
    {
        LLDP_TRC (LLDP_SYS_NAME_TRC | ALL_FAILURE_TRC,
                  "\tSystem Name is Too Big\r\n");
    }
    if (LLDP_IS_SET_TLV_RCVD_BMP (pLocPortInfo, LLDP_TLV_RCVD_SYS_NAME_BMP))
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_SYS_NAME_TRC,
                  "\tLLDPDU is contains more than 1 System Name TLV. "
                  "So discarding the Current TLV\r\n");
        LLDP_RX_DISCARD_TLV (pu1Tlv, u2TlvLength);
        LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
        if (OSIX_FALSE == pLocPortInfo->bstatsFramesInerrorsTotal)
        {
            LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
            pLocPortInfo->bstatsFramesInerrorsTotal = OSIX_TRUE;
        }

        /* Release the memory allocated for Remote Node */
        MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                            (UINT1 *) pRemoteNode);
        return OSIX_SUCCESS;
    }
    else
    {
        LLDP_SET_TLV_RCVD_BMP (pLocPortInfo, LLDP_TLV_RCVD_SYS_NAME_BMP);
    }

    /* make pu1TlvInfo to point to TlvInfo field where
     * TLVinfo = Port id Subtype + port id info*/
    pu1TlvInfo = pu1Tlv + LLDP_TLV_HEADER_LEN;

    /* TLV Validation */
    /* if u2TlvInfoLength > Actual TLV information string length
     * [std IEEE 802.1AB-2005, clause 10.3.2.1 - (b)] */
    /* the extra octets can be ignored by getting the system description
     * with the lu2TlvInfoLength length and
     * next tlv location = CurrenTlvLocation +
     * (u2TlvInfoLength + LLDP_TLV_HEADER_LEN)*/

    /* Decode the Tlv Information */
    LldpTlvDecodeSysNameInfo (pu1TlvInfo, u2TlvLength, pRemoteNode);

    LLDP_TRC (LLDP_SYS_NAME_TRC, "\tValidated System Name TLV\r\n");

    /* Release the memory allocated for Remote Node */
    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId, (UINT1 *) pRemoteNode);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvProcSysDescTlv
 *
 *    DESCRIPTION      : This function Process the Remote System Description
 *                       TLV. Decode the Information from the TLV Information 
 *                       field  and then Validate the information. Also notify 
 *                       if any protocol misconfiguration errors like duplicate
 *                       system description.
 *
 *    INPUT            : pLocPortInfo - Port Info Structure
 *                       pu1Tlv - Buffer Points to the TLV 
 *                       u2TlvLength - Length of the Information Field
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpTlvProcSysDescTlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv,
                       UINT2 u2TlvLength)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT1              *pu1TlvInfo = NULL;

    LLDP_TRC (LLDP_SYS_DESC_TLV, "LldpTlvProcSysDescTlv: "
              "Processing System Description TLV :\r\n");

    /* Create Memory for Remote Node */
    if ((pRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_SYS_DESC_TLV,
                  "LldpTlvProcSysDescTlv: Failed to Allocate Memory "
                  "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }

    MEMSET (pRemoteNode, 0, sizeof (tLldpRemoteNode));

    /* TLV information string validation.
     * u2TlvLength > 255 */
    if (u2TlvLength > LLDP_MAX_LEN_SYSDESC)
    {
        LLDP_TRC (LLDP_SYS_DESC_TRC | ALL_FAILURE_TRC,
                  "\tSystem Description is Too Big\r\n");
    }
    if (LLDP_IS_SET_TLV_RCVD_BMP (pLocPortInfo, LLDP_TLV_RCVD_SYS_DESCR_BMP))
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_SYS_DESC_TLV,
                  "\tLLDPDU is contains more than 1 System Description TLV."
                  " So discarding the Current TLV\r\n");
        LLDP_RX_DISCARD_TLV (pu1Tlv, u2TlvLength);
        LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
        if (OSIX_FALSE == pLocPortInfo->bstatsFramesInerrorsTotal)
        {
            LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
            pLocPortInfo->bstatsFramesInerrorsTotal = OSIX_TRUE;
        }

        /* Release the memory allocated for Remote Node */
        MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                            (UINT1 *) pRemoteNode);

        return OSIX_SUCCESS;
    }
    else
    {
        LLDP_SET_TLV_RCVD_BMP (pLocPortInfo, LLDP_TLV_RCVD_SYS_DESCR_BMP);
    }

    /* make pu1TlvInfo to point to TlvInfo field where
     * TLVinfo = Port id Subtype + port id info*/
    pu1TlvInfo = pu1Tlv + LLDP_TLV_HEADER_LEN;

    /* TLV Validation */

    /* if u2TlvInfoLength > Actual TLV information string length
     * [std IEEE 802.1AB-2005, clause 10.3.2.1 - (b)] */
    /* the extra octets can be ignored by getting the system description
     * with the lu2TlvInfoLength length and
     * next tlv location = CurrenTlvLocation +
     * (u2TlvInfoLength + LLDP_TLV_HEADER_LEN)*/

    /* Decode the Tlv Information */
    LldpTlvDecodeSysDescInfo (pu1TlvInfo, u2TlvLength, pRemoteNode);

    LLDP_TRC (LLDP_SYS_DESC_TLV, "\tValidated System Description TLV\r\n");

    /* Release the memory allocated for Remote Node */
    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId, (UINT1 *) pRemoteNode);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvProcSysCapaTlv
 *
 *    DESCRIPTION      : This function Process the Remote System capabilities
 *                       TLV. Decode the Information from the TLV Information 
 *                       field  and then Validate the information. Also notify 
 *                       if any protocol misconfiguration errors.
 *
 *    INPUT            : pLocPortInfo - Port Info Structure
 *                       pu1Tlv - Buffer Points to the TLV 
 *                       u2TlvLength - Length of the Information Field
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpTlvProcSysCapaTlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv,
                       UINT2 u2TlvLength)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT1              *pu1TlvInfo = NULL;
    UINT2               u2BitNumber = 0;
    BOOL1               bResult = OSIX_FALSE;

    /* Allocate Memory for Remote Node */
    if ((pRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_SYS_CAPAB_TRC,
                  "LldpTlvProcSysCapaTlv: Failed to Allocate Memory "
                  "for remote node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }

    MEMSET (pRemoteNode, 0, sizeof (tLldpRemoteNode));

    if (u2TlvLength > LLDP_SYS_CAPAB_TLV_INFO_LEN)
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_SYS_DESC_TLV,
                  "\tSyscapabilities TLV Error. Enable Capabiity bit is set"
                  " but corresponding Supported Capability bit is "
                  "not set\r\n");
        /* TLV Error : Discard the TLV not the LLDPDU */
        LLDP_RX_DISCARD_TLV (pu1Tlv, u2TlvLength);
        LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
        /* Release the memory allocated for Remote Node */
        MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                            (UINT1 *) pRemoteNode);
        return OSIX_SUCCESS;
    }

    /* validate the length based on 
     * [std IEEE 802.1AB-2005, clause 10.3.2.1-(c)] */
    if (u2TlvLength < LLDP_SYS_CAPAB_TLV_INFO_LEN)
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_SYS_DESC_TLV,
                  "\tMalformed LLDPDU. System Capabilities TLV information "
                  "string length value is less than the minimum required "
                  "length.Discard the entire PDU\r\n");
        LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
        /* Release the memory allocated for Remote Node */
        MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                            (UINT1 *) pRemoteNode);
        return OSIX_FAILURE;
    }

    LLDP_TRC (LLDP_SYS_CAPAB_TRC, "LldpTlvProcSysCapaTlv: "
              "Processing System Capabilities TLV :\r\n");
    if (LLDP_IS_SET_TLV_RCVD_BMP (pLocPortInfo, LLDP_TLV_RCVD_SYS_CAPA_BMP))
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_SYS_DESC_TLV,
                  "\tLLDPDU is contains more than 1 System Capabilities "
                  "TLV. So discarding the Current TLV\r\n");
        LLDP_RX_DISCARD_TLV (pu1Tlv, u2TlvLength);
        LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
        if (OSIX_FALSE == pLocPortInfo->bstatsFramesInerrorsTotal)
        {
            LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
            pLocPortInfo->bstatsFramesInerrorsTotal = OSIX_TRUE;
        }
        /* Release the memory allocated for Remote Node */
        MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                            (UINT1 *) pRemoteNode);
        return OSIX_SUCCESS;
    }
    else
    {
        LLDP_SET_TLV_RCVD_BMP (pLocPortInfo, LLDP_TLV_RCVD_SYS_CAPA_BMP);
    }

    pu1TlvInfo = pu1Tlv + LLDP_TLV_HEADER_LEN;
    /* Decode the Tlv Information */
    LldpTlvDecodeSysCapaInfo (pu1TlvInfo, u2TlvLength, pRemoteNode);
    /* Capabilities should be enabled only if the capability support bit 
     * is set */
    for (u2BitNumber = 1; u2BitNumber <
         (ISS_SYS_CAPABILITIES_LEN * BITS_PER_BYTE); u2BitNumber++)
    {
        OSIX_BITLIST_IS_BIT_SET (pRemoteNode->au1RemSysCapEnabled, u2BitNumber,
                                 ISS_SYS_CAPABILITIES_LEN, bResult);
        if (bResult == OSIX_TRUE)
        {
            /* check if the capability supported bit is set */
            OSIX_BITLIST_IS_BIT_SET (pRemoteNode->au1RemSysCapSupported,
                                     u2BitNumber,
                                     ISS_SYS_CAPABILITIES_LEN, bResult);
            if (bResult == OSIX_FALSE)
            {
                LLDP_TRC (ALL_FAILURE_TRC |
                          LLDP_SYS_DESC_TLV,
                          "\tSyscapabilities TLV Error. Enable Capabiity bit "
                          "is set but corresponding Supported Capability bit "
                          "is not set\r\n");
                /* TLV Error : Discard the TLV not the LLDPDU */
                LLDP_RX_DISCARD_TLV (pu1Tlv, u2TlvLength);
                LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
                if (OSIX_FALSE == pLocPortInfo->bstatsFramesInerrorsTotal)
                {
                    LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
                    pLocPortInfo->bstatsFramesInerrorsTotal = OSIX_TRUE;
                }
                /* Release the memory allocated for Remote Node */
                MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                                    (UINT1 *) pRemoteNode);
                return OSIX_SUCCESS;
            }
        }
    }
    LLDP_TRC (LLDP_SYS_CAPAB_TRC,
              "\tValidated Processing System Capabilities TLV\r\n");
    /* Release the memory allocated for Remote Node */
    MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId, (UINT1 *) pRemoteNode);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvProcManAddrTlv
 *
 *    DESCRIPTION      : This function Process the Remote System Management 
 *                       Address TLV.
 *                       Decode the Information from the TLV Information 
 *                       field  and then Validate the information. Also notify 
 *                       if any protocol misconfiguration errors.
 *
 *    INPUT            : pLocPortInfo - Port Info Structure
 *                       pu1Tlv - Buffer Points to the TLV 
 *                       u2TlvLength - Length of the Information Field
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpTlvProcManAddrTlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv,
                       UINT2 u2TlvLength)
{
    tLldpRemManAddressTable ManAddrNode;
    UINT1              *pu1TlvInfo = NULL;
    UINT1              *pu1TmpTlvInfo = NULL;
    UINT1               u1ManAddrStrLength = 0;    /*(Subtype + Address) */
    UINT2               u2DecodedLen = 0;
    BOOL1               bResult = OSIX_FALSE;

    MEMSET (&ManAddrNode, 0, sizeof (tLldpRemManAddressTable));

    LLDP_TRC (LLDP_MAN_ADDR_TRC, "LldpTlvProcManAddrTlv: "
              "Processing Management Address TLV :\r\n");

    pu1TlvInfo = pu1Tlv + LLDP_TLV_HEADER_LEN;
    pu1TmpTlvInfo = pu1TlvInfo;

    OSIX_BITLIST_IS_BIT_SET (pLocPortInfo->au1DupTlvChkFlag,
                             (UINT2) LLDP_DUP_MGMT_ADDR_TLV,
                             (INT4) LLDP_MAX_DUP_TLV_ARRAY_SIZE, bResult);

    if ((u2TlvLength < LLDP_MIN_MAN_ADDR_INFO_LEN)
        || (u2TlvLength > LLDP_MAX_MAN_ADDR_INFO_LEN))
    {
        if (pLocPortInfo->u1RxFrameType != LLDP_RX_FRAME_REFRESH ||
            bResult == OSIX_FALSE)
        {
            LLDP_TRC (ALL_FAILURE_TRC | LLDP_MAN_ADDR_TRC,
                      "Discarding LLDPDU as obtained Management Address TLV"
                      "length is not within the valid range (9-167) \r\n");
            /* Discard LLDPDU */
            LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
        }
        return OSIX_FAILURE;
    }

    LLDP_LBUF_GET_1_BYTE (pu1TmpTlvInfo, 0, u1ManAddrStrLength);

    if ((u1ManAddrStrLength > LLDP_MAX_LEN_MAN_ADDR + LLDP_SUBTYPE_LEN)
        || (u1ManAddrStrLength < LLDP_MIN_LEN_MAN_ADDR + LLDP_SUBTYPE_LEN))
    {
        if (pLocPortInfo->u1RxFrameType != LLDP_RX_FRAME_REFRESH ||
            bResult == OSIX_FALSE)
        {
            /*Discard TLV */
            LLDP_RX_DISCARD_TLV (pu1Tlv, u2TlvLength);
            LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
            if (OSIX_FALSE == pLocPortInfo->bstatsFramesInerrorsTotal)
            {
                LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
                pLocPortInfo->bstatsFramesInerrorsTotal = OSIX_TRUE;
            }
        }
        return OSIX_SUCCESS;
    }

    /* Decode the Tlv Information */
    if (LldpTlvDecodeManAddrInfo (pu1TlvInfo, u2TlvLength,
                                  &ManAddrNode, &u2DecodedLen) == OSIX_FAILURE)
    {
        if (pLocPortInfo->u1RxFrameType != LLDP_RX_FRAME_REFRESH ||
            bResult == OSIX_FALSE)
        {
            /* If OID string length is greater than 128, then discard the TLV 
             *   * Refer 802.1AB-2009 section 9.2.7.7.2 */
            LLDP_RX_DISCARD_TLV (pu1Tlv, u2TlvLength);
            LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
            if (OSIX_FALSE == pLocPortInfo->bstatsFramesInerrorsTotal)
            {
                LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
                pLocPortInfo->bstatsFramesInerrorsTotal = OSIX_TRUE;
            }
        }
        return OSIX_SUCCESS;
    }

    /* validate the length [std IEEE 802.1AB-2005, clause 10.3.2.1-(c)] */
    if (u2DecodedLen > u2TlvLength)
    {
        if (pLocPortInfo->u1RxFrameType != LLDP_RX_FRAME_REFRESH ||
            bResult == OSIX_FALSE)
        {
            LLDP_TRC (ALL_FAILURE_TRC | LLDP_MAN_ADDR_TRC,
                      "LldpTlvProcManAddrTlv: "
                      "Actual Length of Management Address TLV information is "
                      "greater than the length mentioned in the lenth field.\r\n");
            LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
        }
        return OSIX_FAILURE;
    }
    if ((ManAddrNode.i4RemManAddrIfSubtype != LLDP_MAN_ADDR_IF_SUB_UNKNOWN) &&
        (ManAddrNode.i4RemManAddrIfSubtype != LLDP_MAN_ADDR_IF_SUB_IFINDEX) &&
        (ManAddrNode.i4RemManAddrIfSubtype != LLDP_MAN_ADDR_IF_SUB_SYSPORTNUM))
    {
        if (pLocPortInfo->u1RxFrameType != LLDP_RX_FRAME_REFRESH ||
            bResult == OSIX_FALSE)
        {
            LLDP_TRC (ALL_FAILURE_TRC | LLDP_MAN_ADDR_TRC,
                      "\tMan Addr TLV Error. Unsupported If Numberibng Subtype is "
                      "advertised. Discarding the TLV\r\n");
            /* If subtype out of range */
            LLDP_RX_DISCARD_TLV (pu1Tlv, u2TlvLength);
            LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
            if (OSIX_FALSE == pLocPortInfo->bstatsFramesInerrorsTotal)
            {
                LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
                pLocPortInfo->bstatsFramesInerrorsTotal = OSIX_TRUE;
            }
        }
        return OSIX_SUCCESS;
    }
    LLDP_TRC (LLDP_MAN_ADDR_TRC, "\tValidated Management Address TLV\r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvProcOrgSpecificTlv
 *
 *    DESCRIPTION      : This function processes the Remote System 
 *                       Oganizationally Specific TLV.
 *                       Decode the Information from the TLV Information 
 *                       field if required and then validate the information.
 *
 *    INPUT            : pLocPortInfo - Port Info Structure
 *                       pu1Tlv - Buffer Pointing to the TLV 
 *                       u2TlvLength - Length of the Information Field
 *                       pu2MedSupTlvs - Contains the list of TLVs supported by
 *                                       the remote agent
 *
 *    OUTPUT           : pu1RxOUI - Received OUI
 *                       pu1TlvSubType - Subtype in received TLV
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE (If the return value is
 *                       Failure then the LLDPDU should be discarded)
 *
 ****************************************************************************/
INT4
LldpTlvProcOrgSpecificTlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv,
                           UINT2 u2TlvLength, UINT1 *pu1RxOUI,
                           UINT1 *pu1TlvSubType, UINT2 *pu2MedSupTlvs)
{
    UINT1              *pu1TlvInfo = NULL;
    UINT4               u4Offset = 0;
    INT4                i4RetVal = OSIX_FAILURE;
    INT4                i4AdminStatus = 0;
    BOOL1               bTlvDiscardFlag = OSIX_FALSE;

    /* Move the pointer to TLV info */
    pu1TlvInfo = pu1Tlv + LLDP_TLV_HEADER_LEN;

    /* Get the OUI */
    LLDP_LBUF_GET_STRING (pu1TlvInfo, pu1RxOUI, u4Offset, LLDP_MAX_LEN_OUI);

    /* Get the Tlv Subtype */
    LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, u4Offset, *pu1TlvSubType);

    /* Check if the TLV is 802.1 or 802.3 or any other organizationally
     * specific TLV*/
    if (MEMCMP (pu1RxOUI, gau1LldpDot1OUI, LLDP_MAX_LEN_OUI) == 0)
    {
        /* 802.1 Organizationally specific TLV */
        i4RetVal = LldpDot1TlvProcOrgSpecTlv (pLocPortInfo, pu1TlvInfo,
                                              u2TlvLength, *pu1TlvSubType,
                                              &bTlvDiscardFlag);
    }
    else if (MEMCMP (pu1RxOUI, gau1LldpDot3OUI, LLDP_MAX_LEN_OUI) == 0)
    {
        /* 802.3 Organizationally specific TLV */
        i4RetVal = LldpDot3TlvProcOrgSpecTlv (pLocPortInfo, pu1TlvInfo,
                                              *pu1TlvSubType, u2TlvLength,
                                              &bTlvDiscardFlag);
    }
    else if (MEMCMP (pu1RxOUI, gau1LldpMedOUI, LLDP_MAX_LEN_OUI) == 0)
    {
        /* LLDP-MED Organizationally specific TLV */
        LldpMedGetAdminStatus (pLocPortInfo->i4IfIndex, &i4AdminStatus);
        if ((pLocPortInfo->PortConfigTable.i4AdminStatus ==
             LLDP_ADM_STAT_TX_AND_RX) && (i4AdminStatus == LLDP_ENABLED))
        {

            i4RetVal = LldpMedProcOrgSpecTlv (pLocPortInfo, pu1TlvInfo,
                                              *pu1TlvSubType, u2TlvLength,
                                              &bTlvDiscardFlag, pu2MedSupTlvs);
        }
        else
        {
            i4RetVal = OSIX_SUCCESS;
            LLDP_RX_DISCARD_TLV (pu1Tlv, u2TlvLength);
            LLDP_MED_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo, *pu1TlvSubType);

        }
    }
    else
    {
        if ((u2TlvLength - (LLDP_MAX_LEN_OUI + LLDP_TLV_SUBTYPE_LEN)) >=
            LLDP_MAX_LEN_ORGDEF_INFO)
        {
            LLDP_TRC (ALL_FAILURE_TRC,
                      "\tUnrecognised Org spec. TLV Length is Too Big\r\n");
        }

        /* Unrecognized Organizationally specific TLV. So the content of the
         * TLV need to be stored, hence returning success */
        LLDP_INCR_CNTR_RX_TLVS_UNRECOGNIZED (pLocPortInfo);
        LLDP_TRC (CONTROL_PLANE_TRC, "Received Unrecognized Organizationally"
                  " Specific TLV!!!\n");
        i4RetVal = OSIX_SUCCESS;
    }
    if (bTlvDiscardFlag == OSIX_TRUE)
    {
        LLDP_RX_DISCARD_TLV (pu1Tlv, u2TlvLength);
        if (MEMCMP (pu1RxOUI, gau1LldpMedOUI, LLDP_MAX_LEN_OUI) == 0)
        {
            LLDP_MED_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo, *pu1TlvSubType);
        }
        else
        {
            LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
        }
        if (OSIX_FALSE == pLocPortInfo->bstatsFramesInerrorsTotal)
        {
            LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
            pLocPortInfo->bstatsFramesInerrorsTotal = OSIX_TRUE;
        }
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvProcUnknownTlv
 *
 *    DESCRIPTION      : This function Process the Remote System Unknown
 *                       Type TLV information.
 *                       Decode the Information from the TLV Information 
 *                       field  and then Validate the information. Also notify 
 *                       if any protocol misconfiguration errors.
 *
 *    INPUT            : pLocPortInfo - Port Info Structure
 *                       pu1Tlv - Buffer Points to the TLV 
 *                       u2TlvLength - Length of the Information Field
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpTlvProcUnknownTlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv,
                       UINT2 u2TlvLength)
{
    if (u2TlvLength >= LLDP_MAX_LEN_UNKNOWN_TLV_INFO)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "\tUnknown TLV Length is Too Big\r\n");
        return OSIX_FAILURE;
    }

    LLDP_INCR_CNTR_RX_TLVS_UNRECOGNIZED (pLocPortInfo);

    UNUSED_PARAM (pu1Tlv);
    return OSIX_SUCCESS;
}

/*--------------------------------------------------------------------------*/
/*                       TLV Decode Routines                                */
/*--------------------------------------------------------------------------*/

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvDecodeChassisIdInfo
 *
 *    DESCRIPTION      : This routine Decode the Chassi ID TLV information
 *                       field. Length and Type field of the TLV is already 
 *                       decoded. After decoding the information is updated in
 *                       the structure passed by the caller.
 *
 *    INPUT            : pu1TlvInfo - Buffer points to the TLV Info Field
 *                       u2TlvLength - Length of the TLV Information buffer
 *                      
 *    OUTPUT           : pRemoteNode - Decoded Value of TLV Information is 
 *                                     updated in this structure passed by the
 *                                     caller.
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *
 ****************************************************************************/
PRIVATE INT4
LldpTlvDecodeChassisIdInfo (UINT1 *pu1TlvInfo, UINT2 u2TlvInfoLength,
                            tLldpRemoteNode * pRemoteNode)
{
    UINT1               u1ChassisNwAddrSubtype = 0;
    UINT1              *pu1TmpTlvInfo;
    UINT2               u2ChassisIdLen = 0;

    LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, 0, pRemoteNode->i4RemChassisIdSubtype);
    u2ChassisIdLen = (UINT2) (u2TlvInfoLength - LLDP_TLV_SUBTYPE_LEN);

    if (pRemoteNode->i4RemChassisIdSubtype == LLDP_CHASS_ID_SUB_NW_ADDR)
    {
        pu1TmpTlvInfo = pu1TlvInfo;
        LLDP_LBUF_GET_1_BYTE (pu1TmpTlvInfo, 0, u1ChassisNwAddrSubtype);
        u2ChassisIdLen = (UINT2) (u2ChassisIdLen - LLDP_TLV_SUBTYPE_LEN);
        if (u1ChassisNwAddrSubtype == IPVX_ADDR_FMLY_IPV4)
        {
            if (u2ChassisIdLen != IPVX_IPV4_ADDR_LEN)
            {
                /*Discard LLDPDU */
                return OSIX_FAILURE;
            }
        }
        else if (u1ChassisNwAddrSubtype == IPVX_ADDR_FMLY_IPV6)
        {
            if (u2ChassisIdLen != IPVX_IPV6_ADDR_LEN)
            {
                /*Discard LLDPDU */
                return OSIX_FAILURE;
            }
        }
        u2ChassisIdLen++;
    }
    LLDP_LBUF_GET_STRING (pu1TlvInfo, pRemoteNode->au1RemChassisId, 0,
                          MEM_MAX_BYTES (u2ChassisIdLen,
                                         LLDP_MAX_LEN_CHASSISID));
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvDecodePortIdInfo
 *
 *    DESCRIPTION      : This routine Decode the PORT ID TLV information
 *                       field. Length and Type field of the TLV is already 
 *                       decoded. After decoding the information is updated in
 *                       the structure passed by the caller.
 *
 *    INPUT            : pu1TlvInfo - Buffer points to the TLV Info Field
 *                       u2TlvLength - Length of the TLV Information buffer
 *                      
 *    OUTPUT           : pRemoteNode - Decoded Value of TLV Information is 
 *                                     updated in this structure passed by the
 *                                     caller.
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
LldpTlvDecodePortIdInfo (UINT1 *pu1TlvInfo, UINT2 u2TlvInfoLength,
                         tLldpRemoteNode * pRemoteNode)
{

    UINT1               u1PortNwAddrSubtype = 0;
    UINT1              *pu1TmpTlvInfo;
    UINT2               u2PortIdInfoLen =
        (UINT2) (u2TlvInfoLength - LLDP_TLV_SUBTYPE_LEN);

    LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, 0, pRemoteNode->i4RemPortIdSubtype);

    if (pRemoteNode->i4RemPortIdSubtype == LLDP_PORT_ID_SUB_NW_ADDR)
    {
        pu1TmpTlvInfo = pu1TlvInfo;
        LLDP_LBUF_GET_1_BYTE (pu1TmpTlvInfo, 0, u1PortNwAddrSubtype);
        u2PortIdInfoLen = (UINT2) (u2PortIdInfoLen - LLDP_TLV_SUBTYPE_LEN);
        if (u1PortNwAddrSubtype == IPVX_ADDR_FMLY_IPV4)
        {
            if (u2PortIdInfoLen != IPVX_IPV4_ADDR_LEN)
            {
                /*Discard LLDPDU */
                return OSIX_FAILURE;
            }
        }
        else if (u1PortNwAddrSubtype == IPVX_ADDR_FMLY_IPV6)
        {
            if (u2PortIdInfoLen != IPVX_IPV6_ADDR_LEN)
            {
                /*Discard LLDPDU */
                return OSIX_FAILURE;
            }
        }
        u2PortIdInfoLen++;
    }
    LLDP_LBUF_GET_STRING (pu1TlvInfo, pRemoteNode->au1RemPortId, 0,
                          MEM_MAX_BYTES (u2PortIdInfoLen, LLDP_MAX_LEN_PORTID));
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvDecodeTtlInfo
 *
 *    DESCRIPTION      : This routine Decode the TTL TLV information
 *                       field. Length and Type field of the TLV is already 
 *                       decoded. After decoding the information is updated in
 *                       the structure passed by the caller.
 *
 *    INPUT            : pu1TlvInfo - Buffer points to the TLV Info Field
 *                       u2TlvLength - Length of the TLV Information buffer
 *                      
 *    OUTPUT           : pRemoteNode - Decoded Value of TLV Information is 
 *                                     updated in this structure passed by the
 *                                     caller.
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
PRIVATE VOID
LldpTlvDecodeTtlInfo (UINT1 *pu1TlvInfo, UINT2 u2TlvInfoLength,
                      tLldpRemoteNode * pRemoteNode)
{
    UNUSED_PARAM (u2TlvInfoLength);

    LLDP_LBUF_GET_2_BYTE (pu1TlvInfo, 0, pRemoteNode->u4RxTTL);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvDecodePortDescInfo
 *
 *    DESCRIPTION      : This routine Decode the Port description TLV 
 *                       information field. Length and Type field of the TLV is
 *                       already decoded. After decoding the information is 
 *                       updated in the structure passed by the caller.
 *
 *    INPUT            : pu1TlvInfo - Buffer points to the TLV Info Field
 *                       u2TlvLength - Length of the TLV Information buffer
 *                      
 *    OUTPUT           : pRemoteNode - Decoded Value of TLV Information is 
 *                                     updated in this structure passed by the
 *                                     caller.
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
PRIVATE VOID
LldpTlvDecodePortDescInfo (UINT1 *pu1TlvInfo, UINT2 u2TlvInfoLength,
                           tLldpRemoteNode * pRemoteNode)
{
    LLDP_LBUF_GET_STRING (pu1TlvInfo, pRemoteNode->au1RemPortDesc, 0,
                          MEM_MAX_BYTES (u2TlvInfoLength,
                                         LLDP_MAX_LEN_PORTDESC));

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvDecodeSysNameInfo
 *
 *    DESCRIPTION      : This routine Decode the System Name TLV information
 *                       field. Length and Type field of the TLV is already 
 *                       decoded. After decoding the information is updated in
 *                       the structure passed by the caller.
 *
 *    INPUT            : pu1TlvInfo - Buffer points to the TLV Info Field
 *                       u2TlvLength - Length of the TLV Information buffer
 *                      
 *    OUTPUT           : pRemoteNode - Decoded Value of TLV Information is 
 *                                     updated in this structure passed by the
 *                                     caller.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
LldpTlvDecodeSysNameInfo (UINT1 *pu1TlvInfo, UINT2 u2TlvLength,
                          tLldpRemoteNode * pRemoteNode)
{
    LLDP_LBUF_GET_STRING (pu1TlvInfo, &(pRemoteNode->au1RemSysName[0]), 0,
                          MEM_MAX_BYTES (u2TlvLength, LLDP_MAX_LEN_SYSNAME));
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvDecodeSysDescInfo
 *
 *    DESCRIPTION      : This routine Decode the System Description
 *                       TLV information field.
 *                       Length and Type field of the TLV is already 
 *                       decoded. After decoding the information is updated in
 *                       the structure passed by the caller.
 *
 *    INPUT            : pu1TlvInfo - Buffer points to the TLV Info Field
 *                       u2TlvLength - Length of the TLV Information buffer
 *                      
 *    OUTPUT           : pRemoteNode - Decoded Value of TLV Information is 
 *                                     updated in this structure passed by the
 *                                     caller.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
LldpTlvDecodeSysDescInfo (UINT1 *pu1TlvInfo, UINT2 u2TlvLength,
                          tLldpRemoteNode * pRemoteNode)
{
    LLDP_LBUF_GET_STRING (pu1TlvInfo, &(pRemoteNode->au1RemSysDesc[0]), 0,
                          MEM_MAX_BYTES (u2TlvLength, LLDP_MAX_LEN_SYSDESC));
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvDecodeSysCapaInfo
 *
 *    DESCRIPTION      : This routine Decode the System Capabilities
 *                       TLV information field.
 *                       After decoding the information is updated in
 *                       the structure passed by the caller.
 *                       This function will extract the System Capability
 *                       supported and capability enabled information.
 *
 *    INPUT            : pu1TlvInfo - Buffer points to the TLV Info Field
 *                       u2TlvLength - Length of the TLV Information buffer
 *                      
 *    OUTPUT           : pRemoteNode - Decoded Value of TLV Information is 
 *                                     updated in this structure passed by the
 *                                     caller.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
LldpTlvDecodeSysCapaInfo (UINT1 *pu1TlvInfo, UINT2 u2TlvLength,
                          tLldpRemoteNode * pRemoteNode)
{
    tLldpOctetStringType InOctetStr;
    tLldpOctetStringType OutOctetStr;
    UINT1               au1SysCap[ISS_SYS_CAPABILITIES_LEN];

    /* Get the system capabilities supported and convert it to pkt string
     * format */
    InOctetStr.pu1_OctetList = &au1SysCap[0];
    InOctetStr.i4_Length = ISS_SYS_CAPABILITIES_LEN;
    OutOctetStr.pu1_OctetList = &pRemoteNode->au1RemSysCapSupported[0];
    OutOctetStr.i4_Length = ISS_SYS_CAPABILITIES_LEN;

    MEMSET (InOctetStr.pu1_OctetList, 0, InOctetStr.i4_Length);
    MEMSET (OutOctetStr.pu1_OctetList, 0, OutOctetStr.i4_Length);
    /* Get the Supported capabilities from the buffer */
    LLDP_LBUF_GET_STRING (pu1TlvInfo, InOctetStr.pu1_OctetList,
                          0, InOctetStr.i4_Length);
    /* Convert the Bit list to pkt string as per 
     * Std IEEE802.1AB-2005 section 9.1 */
    LldpReverseOctetString (&InOctetStr, &OutOctetStr);

    /* Get the system capabilities enabled and convert it to pkt string
     * format */
    InOctetStr.pu1_OctetList = &au1SysCap[0];
    InOctetStr.i4_Length = ISS_SYS_CAPABILITIES_LEN;
    OutOctetStr.pu1_OctetList = &pRemoteNode->au1RemSysCapEnabled[0];
    OutOctetStr.i4_Length = ISS_SYS_CAPABILITIES_LEN;

    MEMSET (InOctetStr.pu1_OctetList, 0, InOctetStr.i4_Length);
    MEMSET (OutOctetStr.pu1_OctetList, 0, OutOctetStr.i4_Length);
    /* Get the Enabled capabilities from the buffer */
    LLDP_LBUF_GET_STRING (pu1TlvInfo, InOctetStr.pu1_OctetList,
                          0, InOctetStr.i4_Length);
    /* Convert the Bit list to pkt string as per 
     * Std IEEE802.1AB-2005 section 9.1 */
    LldpReverseOctetString (&InOctetStr, &OutOctetStr);
    UNUSED_PARAM (u2TlvLength);
    return OSIX_SUCCESS;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvDecodeManAddrInfo 
 *
 *    DESCRIPTION      : This routine Decode the Management Address
 *                       TLV information field.
 *                       After decoding the information is updated in
 *                       the structure passed by the caller.
 *
 *    INPUT            : pu1TlvInfo - Buffer points to the TLV Info Field
 *                       u2TlvLength - Length of the TLV Information buffer
 *                       u2DecodedLen - Length of the Decoded Value. its
 *                       return the actual value of the information field.
 *                      
 *    OUTPUT           : pRemoteNode - Decoded Value of TLV Information is 
 *                                     updated in this structure passed by the
 *                                     caller.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTlvDecodeManAddrInfo (UINT1 *pu1TlvInfo, UINT2 u2TlvLength,
                          tLldpRemManAddressTable * pManAddrNode,
                          UINT2 *pu2DecodedLen)
{
    UINT1               au1ManAddrOid[LLDP_MAX_LEN_MAN_OID + 1];
    /* au1ManAddrOid array is used to store the ASN.1 encoded OID string (i.e.
     * OID Length + OID List) so array length is LLDP_MAX_LEN_MAN_OID + 1 octet
     * for the OID length field */
    UINT1               u1ManAddrStrLength = 0;    /*(Subtype + Address) */
    UINT1               u1OidLength = 0;

    *pu2DecodedLen = 0;
    MEMSET (au1ManAddrOid, 0, LLDP_MAX_LEN_MAN_OID + 1);

    LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, 0, u1ManAddrStrLength);
    *pu2DecodedLen += 1;
    LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, 0, pManAddrNode->i4RemManAddrSubtype);
    *pu2DecodedLen += 1;
    LLDP_LBUF_GET_STRING (pu1TlvInfo, &(pManAddrNode->au1RemManAddr[0]),
                          0,
                          MEM_MAX_BYTES ((u1ManAddrStrLength -
                                          LLDP_SUBTYPE_LEN),
                                         LLDP_MAX_LEN_MAN_ADDR));
    *pu2DecodedLen += (u1ManAddrStrLength - LLDP_SUBTYPE_LEN);
    LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, 0, pManAddrNode->i4RemManAddrIfSubtype);
    *pu2DecodedLen += 1;

    if (LLDP_MAN_ADDR_IF_SUB_UNKNOWN == pManAddrNode->i4RemManAddrIfSubtype)
    {
        pManAddrNode->i4RemManAddrIfId = 0;
        pu1TlvInfo = pu1TlvInfo + 4;
        *pu2DecodedLen += 4;
    }
    else
    {
        LLDP_LBUF_GET_4_BYTE (pu1TlvInfo, 0, pManAddrNode->i4RemManAddrIfId);
        *pu2DecodedLen += 4;
    }

    LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, 0, u1OidLength);
    au1ManAddrOid[0] = u1OidLength;
    *pu2DecodedLen += 1;

    if (u1OidLength != 0)
    {
        if (u1OidLength > LLDP_MAX_LEN_MAN_OID)
        {
            LLDP_TRC (ALL_FAILURE_TRC | LLDP_MAN_ADDR_TRC,
                      "LldpTlvDecodeManAddrInfo: OID Length = %d, and it is "
                      "grater than the allowed OID length.\r\n");
            return OSIX_FAILURE;
        }
        LLDP_LBUF_GET_STRING (pu1TlvInfo, &au1ManAddrOid[1], 0, u1OidLength);
        *pu2DecodedLen += u1OidLength;

        /* Decode the OID in UINT4 Oid format and store in the remote 
         * database */
        if (LldpUtilDecodeManAddrOid (&au1ManAddrOid[0],
                                      &(pManAddrNode->RemManAddrOID))
            != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC | LLDP_MAN_ADDR_TRC,
                      "LldpTlvDecodeManAddrInfo: "
                      "Failed to decode the OID.\r\n");
        }
    }

    UNUSED_PARAM (u2TlvLength);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvDecodeUnknownTlvInfo
 *
 *    DESCRIPTION      : This routine Decode the Uonknown Type
 *                       TLV information field.
 *                       After decoding the information is updated in
 *                       the structure passed by the caller.
 *
 *    INPUT            : pu1TlvInfo - Buffer points to the TLV Info Field
 *                       u2TlvLength - Length of the TLV Information buffer
 *                      
 *    OUTPUT           : pRemoteNode - Decoded Value of TLV Information is 
 *                                     updated in this structure passed by the
 *                                     caller.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
LldpTlvDecodeUnknownTlvInfo (UINT1 *pu1TlvInfo, UINT2 u2TlvLength,
                             tLldpRemUnknownTLVTable * pUnknownTlvNode)
{
    LLDP_LBUF_GET_STRING (pu1TlvInfo,
                          &(pUnknownTlvNode->au1RemUnknownTLVInfo[0]),
                          0, MEM_MAX_BYTES
                          (u2TlvLength, LLDP_MAX_LEN_UNKNOWN_TLV_INFO));
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvDecodeOrgDefInfo
 *
 *    DESCRIPTION      : This routine decodes the organizationally specific
 *                       unrecognized TLV information string. After decoding
 *                       the information is updated in the structure passed by
 *                       the caller.
 *
 *    INPUT            : pu1TlvInfo     - Points to the subtype information 
 *                                        field in the TLV
 *                       u2TlvInfoLen     - TLV Information Length
 *                      
 *    OUTPUT           : pRemOrgDefNode - Decoded Value of TLV Information is 
 *                                        updated in this structure passed by
 *                                        the caller.
 *
 *    RETURNS          : None 
 *
 ****************************************************************************/
PRIVATE VOID
LldpTlvDecodeOrgDefInfo (UINT1 *pu1TlvInfo, UINT2 u2TlvInfoLen,
                         tLldpRemOrgDefInfoTable * pRemOrgDefNode)
{
    pRemOrgDefNode->u2RemOrgDefInfoLen =
        (UINT2) (LLDP_GET_ORG_DEF_INFO_STR_LEN (u2TlvInfoLen));
    LLDP_LBUF_GET_STRING (pu1TlvInfo, pRemOrgDefNode->au1RemOrgDefInfo,
                          0, MEM_MAX_BYTES (pRemOrgDefNode->u2RemOrgDefInfoLen,
                                            LLDP_MAX_LEN_ORGDEF_INFO));
    return;
}

/*--------------------------------------------------------------------------*/
/*                       TLV Update Routines                                */
/*--------------------------------------------------------------------------*/

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvUpdtRemChassisIdInfo 
 *
 *    DESCRIPTION      : This routine Decode the chassis id  TLV information
 *
 *    INPUT            : pu1Tlv- Buffer points to the TLV
 *                       u2TlvInfoLength - Length of the TLV Information buffer
 *                       pLocPortInfo - Local Port Info structure where the 
 *                                      TLV is received
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTlvUpdtRemChassisIdInfo (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv,
                             UINT2 u2TlvInfoLength)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT1              *pu1TlvInfo = NULL;

    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;

    pu1TlvInfo = pu1Tlv + LLDP_TLV_HEADER_LEN;
    LldpTlvDecodeChassisIdInfo (pu1TlvInfo, u2TlvInfoLength, pRemoteNode);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvUpdtRemPortIdInfo 
 *
 *    DESCRIPTION      : This routine Decode the port id TLV information
 *
 *    INPUT            : pu1Tlv- Buffer points to the TLV
 *                       u2TlvInfoLength - Length of the TLV Information buffer
 *                       pLocPortInfo - Local Port Info structure where the 
 *                                      TLV is received
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTlvUpdtRemPortIdInfo (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv,
                          UINT2 u2TlvInfoLength)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT1              *pu1TlvInfo = NULL;

    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;

    pu1TlvInfo = pu1Tlv + LLDP_TLV_HEADER_LEN;
    LldpTlvDecodePortIdInfo (pu1TlvInfo, u2TlvInfoLength, pRemoteNode);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvUpdtRemTtlInfo 
 *
 *    DESCRIPTION      : This routine Decode the TTL TLV information
 *
 *    INPUT            : pu1Tlv- Buffer points to the TLV
 *                       u2TlvInfoLength - Length of the TLV Information buffer
 *                       pLocPortInfo - Local Port Info structure where the 
 *                                      TLV is received
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTlvUpdtRemTtlInfo (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv,
                       UINT2 u2TlvInfoLength)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT1              *pu1TlvInfo = NULL;

    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;

    pu1TlvInfo = pu1Tlv + LLDP_TLV_HEADER_LEN;
    LldpTlvDecodeTtlInfo (pu1TlvInfo, u2TlvInfoLength, pRemoteNode);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvUpdtRemPortDescInfo 
 *
 *    DESCRIPTION      : This routine Decode the port desc TLV information
 *
 *    INPUT            : pu1Tlv- Buffer points to the TLV
 *                       u2TlvInfoLength - Length of the TLV Information buffer
 *                       pLocPortInfo - Local Port Info structure where the 
 *                                      TLV is received
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTlvUpdtRemPortDescInfo (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv,
                            UINT2 u2TlvInfoLength)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT1              *pu1TlvInfo = NULL;

    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;

    pu1TlvInfo = pu1Tlv + LLDP_TLV_HEADER_LEN;
    LldpTlvDecodePortDescInfo (pu1TlvInfo, u2TlvInfoLength, pRemoteNode);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvUpdtRemSysNameInfo
 *
 *    DESCRIPTION      : This routine Decode the System Name TLV information
 *                       field. Length and Type field of the TLV is already 
 *                       decoded. After decoding, the information is updated
 *                       in the Remote Node.
 *
 *    INPUT            : pu1Tlv- Buffer points to the TLV
 *                       u2TlvLength - Length of the TLV Information buffer
 *                       pLocPortInfo - Local Port Info structure where the 
 *                                      TLV is received
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpTlvUpdtRemSysNameInfo (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv,
                           UINT2 u2TlvLength)
{
    tLldMisConfigTrap   MisConfigInfo;
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT1              *pu1TlvInfo = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT2               u2ChassisIdLen = 0;
    UINT2               u2PortIdLen = 0;

    MEMSET (&MisConfigInfo, 0, sizeof (tLldMisConfigTrap));

    LLDP_TRC (LLDP_SYS_NAME_TRC,
              "LldpTlvUpdtRemSysNameInfo: "
              "Updating System Name Information :\r\n");

    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;
    if (pRemoteNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_SYS_NAME_TRC,
                  "\tNo Remote Node found to update the System Name "
                  " information.\r\n");
        return OSIX_FAILURE;
    }
    LLDP_TRC_ARG2 (LLDP_SYS_NAME_TRC,
                   "\tRemote - ChassisId: %s, PortId: %s\r\n",
                   pRemoteNode->au1RemChassisId, pRemoteNode->au1RemPortId);

    /* Decode the Tlv Information */
    /* This object is present is the Remote Node structure itself, so pass the
     * Remote Node pointer to the Decode Routine for updating the value */
    pu1TlvInfo = pu1Tlv + LLDP_TLV_HEADER_LEN;
    LldpTlvDecodeSysNameInfo (pu1TlvInfo, u2TlvLength, pRemoteNode);
    LLDP_TRC_ARG1 (LLDP_SYS_NAME_TRC,
                   "Decoded Value -\n\tSystem Name: %s\r\n",
                   pRemoteNode->au1RemSysName);

    /* Misconfig checkup */
    i4RetVal = STRNCMP (pRemoteNode->au1RemSysName,
                        gLldpGlobalInfo.LocSysInfo.au1LocSysName,
                        LLDP_MAX_LEN_SYSNAME);
    if (i4RetVal == 0)
    {
        /* Get the length of Chassis Id */
        LldpUtilGetChassisIdLen (pRemoteNode->i4RemChassisIdSubtype,
                                 pRemoteNode->au1RemChassisId, &u2ChassisIdLen);
        /* Get the length of port Id */
        LldpUtilGetPortIdLen (pRemoteNode->i4RemPortIdSubtype,
                              pRemoteNode->au1RemPortId, &u2PortIdLen);

        /* Send Fault Notification */
        MisConfigInfo.i4RemChassisIdSubtype =
            pRemoteNode->i4RemChassisIdSubtype;
        MisConfigInfo.i4RemPortIdSubtype = pRemoteNode->i4RemPortIdSubtype;
        MisConfigInfo.u4RemTimeMark = pRemoteNode->u4RemLastUpdateTime;
        MisConfigInfo.i4RemIndex = pRemoteNode->i4RemIndex;
        MisConfigInfo.i4RemLocalPortNum = pRemoteNode->i4RemLocalPortNum;
        MEMCPY (&MisConfigInfo.au1RemChassisId,
                pRemoteNode->au1RemChassisId,
                MEM_MAX_BYTES (u2ChassisIdLen, LLDP_MAX_LEN_CHASSISID));
        MEMCPY (&MisConfigInfo.au1PortId,
                pRemoteNode->au1RemPortId,
                MEM_MAX_BYTES (u2PortIdLen, LLDP_MAX_LEN_PORTID));
        /* Coverity Fix */
        STRNCPY (&MisConfigInfo.DupSysName,
                 pRemoteNode->au1RemSysName,
                 MEM_MAX_BYTES (STRLEN (pRemoteNode->au1RemSysName),
                                LLDP_MAX_LEN_SYSNAME));
        LldpSendNotification ((VOID *) &MisConfigInfo, LLDP_DUP_SYSTEM_NAME);
    }
    /* As the object is updated in Remote Node Structure , It will be inserted
     * in the Remote Table at the end of all updation */
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvUpdtRemSysDescInfo
 *
 *    DESCRIPTION      : This routine Decode the System Description TLV 
 *                       information field. 
 *                       After decoding the information it is 
 *                       updated in Remote Node. 
 *
 *    INPUT            : pu1Tlv- Buffer points to the TLV
 *                       u2TlvLength - Length of the TLV Information buffer
 *                       pLocPortInfo - Local Port Info structure where the 
 *                                      TLV is received
 *                      
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpTlvUpdtRemSysDescInfo (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv,
                           UINT2 u2TlvLength)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT1              *pu1TlvInfo = NULL;

    LLDP_TRC (LLDP_SYS_DESC_TLV,
              "LldpTlvUpdtRemSysDescInfo: "
              "Updating System Description TLV :\r\n");

    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;
    if (pRemoteNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_SYS_DESC_TLV,
                  "\tNo Remote Node found to update the System Name "
                  " information.\r\n");
        return OSIX_FAILURE;
    }
    LLDP_TRC_ARG2 (LLDP_SYS_DESC_TLV,
                   "\tRemote - ChassisId: %s, PortId: %s\r\n",
                   pRemoteNode->au1RemChassisId, pRemoteNode->au1RemPortId);

    pu1TlvInfo = pu1Tlv + LLDP_TLV_HEADER_LEN;
    LldpTlvDecodeSysDescInfo (pu1TlvInfo, u2TlvLength, pRemoteNode);
    LLDP_TRC_ARG1 (LLDP_SYS_DESC_TLV,
                   "Decoded Value -\n\tSystem Description: %s\r\n",
                   pRemoteNode->au1RemSysDesc);

    /* No Need to check for duplication of system description. */

    /* As the object is updated in Remote Node Structure , It will be inserted
     * in the Remote Table at the end of all updation */
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvUpdtRemSysCapaInfo
 *
 *    DESCRIPTION      : This routine Decode the System Capability TLV 
 *                       information fields. After decoding the information 
 *                       it updates the system capability supported and system 
 *                       capability enabled information in Remote Node.
 *
 *    INPUT            : pu1Tlv- Buffer points to the TLV
 *                       u2TlvLength - Length of the TLV Information buffer
 *                       pLocPortInfo - Local Port Info structure where the 
 *                                      TLV is received
 *                      
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpTlvUpdtRemSysCapaInfo (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv,
                           UINT2 u2TlvLength)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT1              *pu1TlvInfo = NULL;

    LLDP_TRC (LLDP_SYS_CAPAB_TRC,
              "(LldpTlvUpdtRemSysCapaInfo)"
              " - Updating System Capabilities TLV :\r\n");
    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;
    if (pRemoteNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_SYS_CAPAB_TRC,
                  "\tNo Remote Node found to update the System Capabilities "
                  " information.\r\n");
        return OSIX_FAILURE;
    }
    LLDP_TRC_ARG2 (LLDP_SYS_CAPAB_TRC,
                   "\tRemote - ChassisId: %s, PortId: %s\r\n",
                   pRemoteNode->au1RemChassisId, pRemoteNode->au1RemPortId);

    pu1TlvInfo = pu1Tlv + LLDP_TLV_HEADER_LEN;
    LldpTlvDecodeSysCapaInfo (pu1TlvInfo, u2TlvLength, pRemoteNode);
    LLDP_TRC_ARG2 (LLDP_SYS_DESC_TLV,
                   "\tDecoded Values -\n\tSystem Capabilities Supported: 0x%x"
                   "\n\tSystem Capabilities Enabled: 0x%x\r\n",
                   pRemoteNode->au1RemSysCapSupported,
                   pRemoteNode->au1RemSysCapEnabled);

    /* Misconfig Checkup is not needed in this case */

    /* As the object is updated in Remote Node Structure , It will be inserted
     * in the Remote Table at the end of all updation */
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvUpdtRemManAddrInfo
 *
 *    DESCRIPTION      : This routine Decode the Remote ManAddr TLV info
 *                       and store it in the RemManAddrRBTree. The operation
 *                       consist of the following steps -
 *                       1. If LLDPDU is a UPDATE PDU and this function is 
 *                       called while get the first ManAddr TLV then delete all
 *                       the existing ManAddr Nodes from the RemManAddrRBTree.
 *                       2. Allocate Memory for the New ManAddr Node.
 *                       3. Update the Common Remote Indices.
 *                       4. Call the Decode Routine to update the Newly created
 *                       ManAddr Node with proper information send by the TLV
 *                       5. Insert the Node to the RemManAddrRBTree
 *                       
 *
 *    INPUT            : pu1Tlv- Buffer points to the TLV
 *                       u2TlvLength - Length of the TLV Information buffer
 *                       pLocPortInfo - Local Port Info structure where the 
 *                                      TLV is received
 *                      
 *    OUTPUT           :  NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpTlvUpdtRemManAddrInfo (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv,
                           UINT2 u2TlvLength)
{
    tLldMisConfigTrap   MisConfigInfo;
    tLldpRemoteNode    *pRemoteNode = NULL;
    tLldpRemManAddressTable *pManAddrNode = NULL;
    tLldpLocManAddrTable *pLocManAddr = NULL;
    UINT1              *pu1TlvInfo = NULL;
    UINT2               u2DecodedLen = 0;
    UINT2               u2ChassisIdLen = 0;
    UINT2               u2PortIdLen = 0;

    MEMSET (&MisConfigInfo, 0, sizeof (tLldMisConfigTrap));

    LLDP_TRC (LLDP_MAN_ADDR_TRC,
              "LldpTlvUpdtRemManAddrInfo: Updating Management "
              "Address TLV :\r\n");

    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;
    if (pRemoteNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_MAN_ADDR_TRC,
                  "\tNo Remote Node found to update the Man Addr "
                  " information.\r\n");
        return OSIX_FAILURE;
    }
    LLDP_TRC_ARG2 (LLDP_MAN_ADDR_TRC,
                   "\tRemote - ChassisId: %s, PortId: %s\r\n",
                   pRemoteNode->au1RemChassisId, pRemoteNode->au1RemPortId);

    if ((pManAddrNode = (tLldpRemManAddressTable *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemManAddrPoolId))) == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_MAN_ADDR_TRC | LLDP_CRITICAL_TRC,
                  "\tMempool Allocation Failure for New RemManAddr Node"
                  "\r\n");
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "Mempool Allocation Failure for New RemManAddr Node"));
#endif
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }

    MEMSET (pManAddrNode, 0, sizeof (tLldpRemManAddressTable));

    /* Allocate Memory for LocManAddr */
    if ((pLocManAddr = (tLldpLocManAddrTable *)
         (MemAllocMemBlk (gLldpGlobalInfo.LocManAddrPoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_MAN_ADDR_TRC,
                  "LldpTlvUpdtRemManAddrInfo: Failed to Allocate Memory "
                  "for LocManAddr \r\n");
        MemReleaseMemBlock (gLldpGlobalInfo.RemManAddrPoolId,
                            (UINT1 *) pManAddrNode);
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }
    MEMSET (pLocManAddr, 0, sizeof (tLldpLocManAddrTable));

    /* Update the common Remote Indices */
    pManAddrNode->u4RemLastUpdateTime = pRemoteNode->u4RemLastUpdateTime;
    pManAddrNode->i4RemLocalPortNum = pRemoteNode->i4RemLocalPortNum;
    pManAddrNode->i4RemIndex = pRemoteNode->i4RemIndex;
    pManAddrNode->u4DestAddrTblIndex = pRemoteNode->u4DestAddrTblIndex;

    pu1TlvInfo = pu1Tlv + LLDP_TLV_HEADER_LEN;
    /* Call the Decode Routine */
    LldpTlvDecodeManAddrInfo (pu1TlvInfo, u2TlvLength,
                              pManAddrNode, &u2DecodedLen);
    LLDP_TRC_ARG5 (LLDP_MAN_ADDR_TRC,
                   "Decoded Values -\n\tManAddr Subtype: %d\n"
                   "\tMan Addr: %s\n\tIfId Subtype: %d\n"
                   "\tIf ID: %d\n\tOID: 0x%x\r\n",
                   pManAddrNode->i4RemManAddrSubtype,
                   pManAddrNode->au1RemManAddr,
                   pManAddrNode->i4RemManAddrIfSubtype,
                   pManAddrNode->i4RemManAddrIfId,
                   pManAddrNode->RemManAddrOID.au4_OidList);

    /* Insert the Node in the RemManAddrRBTree */
    if (RBTreeAdd (gLldpGlobalInfo.RemManAddrRBTree, (tRBElem *) pManAddrNode)
        != RB_SUCCESS)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.RemManAddrPoolId,
                            (UINT1 *) pManAddrNode);
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_MAN_ADDR_TRC,
                  "\tRBTree Addition failed for RemManAddrRBTree. Entry  "
                  " with same index already present in the RBTree. "
                  " So discarding the current ManAddr TLV.\r\n");
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_MAN_ADDR_TRC,
                  "\tMore than one similar ManAddr TLV is present "
                  " in this PDU.\r\n");
        LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
        if (OSIX_FALSE == pLocPortInfo->bstatsFramesInerrorsTotal)
        {
            LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
            pLocPortInfo->bstatsFramesInerrorsTotal = OSIX_TRUE;
        }
        /* Release the memory allocated for LocManAddr */
        MemReleaseMemBlock (gLldpGlobalInfo.LocManAddrPoolId,
                            (UINT1 *) pLocManAddr);
        return OSIX_SUCCESS;
    }
    /* Check for Misconfiguration */
    pLocManAddr->i4LocManAddrSubtype = pManAddrNode->i4RemManAddrSubtype;
    MEMCPY (pLocManAddr->au1LocManAddr, pManAddrNode->au1RemManAddr,
            LLDP_MAX_LEN_MAN_ADDR);

    if (NULL != (tLldpLocManAddrTable *) RBTreeGet
        (gLldpGlobalInfo.LocManAddrRBTree, (tRBElem *) pLocManAddr))
    {
        /* Get the length of Chassis Id */
        LldpUtilGetChassisIdLen (pRemoteNode->i4RemChassisIdSubtype,
                                 pRemoteNode->au1RemChassisId, &u2ChassisIdLen);

        /* Get the length of port Id */
        LldpUtilGetPortIdLen (pRemoteNode->i4RemPortIdSubtype,
                              pRemoteNode->au1RemPortId, &u2PortIdLen);

        /* Send Fault Notification */
        MisConfigInfo.i4RemChassisIdSubtype =
            pRemoteNode->i4RemChassisIdSubtype;
        MisConfigInfo.i4RemPortIdSubtype = pRemoteNode->i4RemPortIdSubtype;
        MisConfigInfo.u4RemTimeMark = pRemoteNode->u4RemLastUpdateTime;
        MisConfigInfo.i4RemIndex = pRemoteNode->i4RemIndex;
        MisConfigInfo.i4RemLocalPortNum = pRemoteNode->i4RemLocalPortNum;
        MEMCPY (&MisConfigInfo.au1RemChassisId[0], pRemoteNode->au1RemChassisId,
                MEM_MAX_BYTES (u2ChassisIdLen, LLDP_MAX_LEN_CHASSISID));
        MEMCPY (&MisConfigInfo.au1PortId[0], pRemoteNode->au1RemPortId,
                MEM_MAX_BYTES (u2PortIdLen, LLDP_MAX_LEN_PORTID));
        MEMCPY (&MisConfigInfo.DupManAddress, pManAddrNode->au1RemManAddr,
                LLDP_MAX_LEN_MAN_ADDR);
        MisConfigInfo.DupAddrSubType = pManAddrNode->i4RemManAddrSubtype;
        MisConfigInfo.DupManAddrIfId = pManAddrNode->i4RemManAddrIfId;

        LldpSendNotification ((VOID *) &MisConfigInfo, LLDP_DUP_MAN_ADDR);
    }
    /* Release the memory allocated for LocManAddr */
    MemReleaseMemBlock (gLldpGlobalInfo.LocManAddrPoolId,
                        (UINT1 *) pLocManAddr);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvUpdtRemOrgSpecificInfo
 *
 *    DESCRIPTION      : This routine decodes the Organizationally Specific
 *                       TLV information and store it in the Remotre DataBase. 
 *                       Notification will be sent if there is any protocol
 *                       mis-configuration.
 *
 *    INPUT            : pLocPortInfo - Local Port Info structure where the 
 *                                      TLV is received
 *                       pu1Tlv- Buffer points to the TLV
 *                       u2TlvLength - Length of the TLV Information buffer
 *                      
 *    OUTPUT           :  NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/

PUBLIC INT4
LldpTlvUpdtRemOrgSpecificInfo (tLldpLocPortInfo * pLocPortInfo,
                               UINT1 *pu1Tlv, UINT2 u2TlvLength)
{
    UINT1              *pu1TlvInfo = NULL;
    UINT4               u4Offset = 0;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT1               u1RxOUI[LLDP_MAX_LEN_OUI] = { 0 };
    UINT1               u1TlvSubType = 0;

    /* Get the TLV Info Filed */
    pu1TlvInfo = pu1Tlv + LLDP_TLV_HEADER_LEN;

    /* Get the OUI */
    LLDP_LBUF_GET_STRING (pu1TlvInfo, u1RxOUI, u4Offset, LLDP_MAX_LEN_OUI);

    /* Get the Tlv Subtype */
    LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, u4Offset, u1TlvSubType);

    /* Check if the TLV is 802.1 or 802.3 or any other organizationally
     * specific TLV*/
    if (MEMCMP (u1RxOUI, gau1LldpDot1OUI, LLDP_MAX_LEN_OUI) == 0)
    {
        /* 802.1 Organizationally specific TLV */
        i4RetVal = LldpDot1TlvUpdtOrgSpecTlv (pLocPortInfo, pu1TlvInfo,
                                              u2TlvLength, u1TlvSubType);
    }
    else if (MEMCMP (u1RxOUI, gau1LldpDot3OUI, LLDP_MAX_LEN_OUI) == 0)
    {
        /* 802.3 Organizationally specific TLV */
        i4RetVal = LldpDot3TlvUpdtOrgSpecTlv (pLocPortInfo, pu1TlvInfo,
                                              u2TlvLength, u1TlvSubType);
    }
    else if (MEMCMP (u1RxOUI, gau1LldpMedOUI, LLDP_MAX_LEN_OUI) == 0)
    {
        /* LLDP-MED Organizationally specific TLV */
        i4RetVal = LldpMedTlvUpdtOrgSpecTlv (pLocPortInfo, pu1TlvInfo,
                                             u2TlvLength, u1TlvSubType);

    }
    else
    {
        /* Unrecognized Organizationally specific TLV */
        i4RetVal = LldpTlvUpdtUnrecogOrgDefTlv (pLocPortInfo, pu1TlvInfo,
                                                u2TlvLength, u1RxOUI,
                                                u1TlvSubType);
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvUpdtUnrecogOrgDefTlv
 *
 *    DESCRIPTION      : This function updates the Oganizationally Specific
 *                       Unrecognized TLV in the received frame in the remote
 *                       database.
 *
 *    INPUT            : pLocPortInfo  - Port Info Structure
 *                       pu1TlvInfo    - Pointer to the TLV SubType Specific 
 *                                       Information 
 *                       u2TlvLength   - Length of the Information Field
 *                       pau1LldpOUI   - Pointer to the OUI
 *                       u1TlvSubType  - TLV SubType
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpTlvUpdtUnrecogOrgDefTlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1TlvInfo,
                             UINT2 u2TlvLength, UINT1 *pau1LldpOUI,
                             UINT1 u1TlvSubType)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    tLldpRemOrgDefInfoTable *pRemOrgDefNode = NULL;

    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;

    if ((pRemOrgDefNode =
         (tLldpRemOrgDefInfoTable *)
         MemAllocMemBlk (gLldpGlobalInfo.RemOrgDefInfoPoolId)) == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_CRITICAL_TRC,
                  "LldpTlvUpdtUnrecogOrgDefTlv : "
                  "Mempool Allocation Failure for New Unrecog Org"
                  " Spec TLV Node - But returning SUCCESS as it is not required"
                  " to store application TLVS in LLDP \r\n");
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpTlvUpdtUnrecogOrgDefTlv : Mempool Allocation"
                      "Failure for New Unrecog Org Spec TLV Node - But "
                      "returning SUCCESS as it is not required to store"
                      " application TLVS in LLDP"));
#endif
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        /* Application TLVs are not stored in LLDP module and they are 
         * posted to them so each application maintains them in their
         * respective modules. So even for memory allocation failures,
         * SUCCESS is returned. */
        return OSIX_SUCCESS;
    }

    MEMSET (pRemOrgDefNode, 0, sizeof (tLldpRemOrgDefInfoTable));
    /* Update the common Remote Indices */
    pRemOrgDefNode->u4RemLastUpdateTime = pRemoteNode->u4RemLastUpdateTime;
    pRemOrgDefNode->i4RemLocalPortNum = pRemoteNode->i4RemLocalPortNum;
    pRemOrgDefNode->i4RemIndex = pRemoteNode->i4RemIndex;
    /* Update the organizationally specific indices */
    pRemOrgDefNode->i4RemOrgDefInfoIndex = LLDP_GET_NEXT_FREE_REM_ORG_INDEX ();
    LLDP_UPDT_NEXT_FREE_REM_ORG_INDEX ();
    pRemOrgDefNode->i4RemOrgDefInfoSubtype = (INT4) u1TlvSubType;
    pRemOrgDefNode->u4DestAddrTblIndex = pRemoteNode->u4DestAddrTblIndex;
    MEMCPY (pRemOrgDefNode->au1RemOrgDefInfoOUI, pau1LldpOUI, LLDP_MAX_LEN_OUI);

    /* Call the Decode Routine */
    LldpTlvDecodeOrgDefInfo (pu1TlvInfo, u2TlvLength, pRemOrgDefNode);

    /* Insert the Node in the RemOrgDefInfoRBTree */
    if (RBTreeAdd (gLldpGlobalInfo.RemOrgDefInfoRBTree,
                   (tRBElem *) pRemOrgDefNode) != RB_SUCCESS)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.RemOrgDefInfoPoolId,
                            (UINT1 *) pRemOrgDefNode);
        LLDP_TRC (ALL_FAILURE_TRC,
                  "\tRBTree Addition failed for RemManAddrRBTree. Entry  "
                  " with same index already present in the RBTree. "
                  " So discarding the current Unrecognized TLV.\r\n");
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                       "\tReceived more than one similar Unrecognized TLV "
                       " on port %d.\r\n", pRemoteNode->i4RemLocalPortNum);
        LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
        if (OSIX_FALSE == pLocPortInfo->bstatsFramesInerrorsTotal)
        {
            LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
            pLocPortInfo->bstatsFramesInerrorsTotal = OSIX_TRUE;
        }
        return OSIX_SUCCESS;
    }
    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC, "Updated Unrecognized TLV for port %d\n",
                   pRemoteNode->i4RemLocalPortNum);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTlvUpdtRemUnknownTlvInfo
 *
 *    DESCRIPTION      : This routine updates the remote unknown TLV 
 *                       information.
 *                        
 *    INPUT            : pLocPortInfo - Local Port Info structure where the 
 *                                      TLV is received
 *                       pu1Tlv- Points to the TLV Subtype information field
 *                       u2TlvLength - Length of the TLV info field
 *                       
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpTlvUpdtRemUnknownTlvInfo (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv,
                              UINT2 u2TlvLength, UINT2 u2TlvType)
{
    tLldpRemUnknownTLVTable *pUnknownTlvNode = NULL;
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT1              *pu1TlvInfo = NULL;

    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;
    if (pRemoteNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  "\tNo Remote Node found to update the Unknown Tlv "
                  " information.\r\n");
        return OSIX_FAILURE;
    }

    if ((pUnknownTlvNode =
         (tLldpRemUnknownTLVTable *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemUnknownTLVPoolId))) == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_CRITICAL_TRC,
                  "\tMempool Allocation Failure for New RemUnknownTlv "
                  "Node\r\n");
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "Mempool Allocation Failure for New RemUnknownTlv Node"));
#endif
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }

    MEMSET (pUnknownTlvNode, 0, sizeof (tLldpRemUnknownTLVTable));

    /* Update the Unknown TLV Indices */
    pUnknownTlvNode->u4RemLastUpdateTime = pRemoteNode->u4RemLastUpdateTime;
    pUnknownTlvNode->i4RemLocalPortNum = pRemoteNode->i4RemLocalPortNum;
    pUnknownTlvNode->i4RemIndex = pRemoteNode->i4RemIndex;
    pUnknownTlvNode->i4RemUnknownTLVType = u2TlvType;
    pUnknownTlvNode->u4DestAddrTblIndex = pRemoteNode->u4DestAddrTblIndex;

    pu1TlvInfo = pu1Tlv + LLDP_TLV_HEADER_LEN;
    LldpTlvDecodeUnknownTlvInfo (pu1TlvInfo, u2TlvLength, pUnknownTlvNode);
    pUnknownTlvNode->u2RemUnknownTLVInfoLen = u2TlvLength;

    if (RBTreeAdd (gLldpGlobalInfo.RemUnknownTLVRBTree,
                   (tRBElem *) pUnknownTlvNode) != RB_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_CRITICAL_TRC,
                  "\tRBTree Addition failed for RemUnknownTLVRBTree" "\r\n");
        MemReleaseMemBlock (gLldpGlobalInfo.RemUnknownTLVPoolId,
                            (UINT1 *) pUnknownTlvNode);
        LLDP_TRC (ALL_FAILURE_TRC,
                  "\tRBTree Addition failed for RemManAddrRBTree. Entry  "
                  " with same index already present in the RBTree. "
                  " So discarding the current Unknown TLV.\r\n");
        LLDP_TRC (ALL_FAILURE_TRC,
                  "\tMore than one similar Unknown TLV is present "
                  " in this PDU.\r\n");
        LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
        if (OSIX_FALSE == pLocPortInfo->bstatsFramesInerrorsTotal)
        {
            LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
            pLocPortInfo->bstatsFramesInerrorsTotal = OSIX_TRUE;
        }
        return OSIX_SUCCESS;
    }
    return OSIX_SUCCESS;
}

/*************************************************************************/
/* Function Name : LldpReverseOctetString                               */
/* Description   : Reverse the Bit positions in pInOctetStr and store it */
/*                 in pOutOctetStr                                       */
/* Input(s)      : pInOctetStr - Input octetstring                       */
/* Output(s)     : pOutOctetStr - output octetstring                     */
/* Returns       : None                                                  */
/*************************************************************************/
PUBLIC VOID
LldpReverseOctetString (tLldpOctetStringType * pInOctetStr,
                        tLldpOctetStringType * pOutOctetStr)
{
    UINT4               u4Count = 0;
    UINT4               u4MaxBits = 0;
    UINT4               u4RevBitNum = 0;
    INT4                i4MaxBytes = 0;
    BOOL1               bResult = OSIX_FALSE;

    /* Example
     *  *      *             BIT Positions 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
     *   *           * pInOctetStr:              1 1 1 . . . . .               .  .  1
     *    *                * pOutOctetStr:             1 . . . . .     . .  .  .  .  1  1  1
     *     *                     */

    i4MaxBytes = pInOctetStr->i4_Length;
    pOutOctetStr->i4_Length = i4MaxBytes;

    u4MaxBits = (i4MaxBytes * BITS_PER_BYTE);

    for (u4Count = 1; u4Count <= u4MaxBits; u4Count++)
    {
        OSIX_BITLIST_IS_BIT_SET (pInOctetStr->pu1_OctetList, u4Count,
                                 i4MaxBytes, bResult);
        if (bResult == OSIX_TRUE)
        {
            u4RevBitNum = (u4MaxBits + 1) - u4Count;
            OSIX_BITLIST_SET_BIT (pOutOctetStr->pu1_OctetList, u4RevBitNum,
                                  i4MaxBytes);
        }
    }
    return;
}

#endif /* _LLDP_TLV_C_ */
/*--------------------------------------------------------------------------*/
/*                       End of the file  <filename.c>                      */
/*--------------------------------------------------------------------------*/
