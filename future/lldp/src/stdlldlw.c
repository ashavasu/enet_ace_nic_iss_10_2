/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: stdlldlw.c,v 1.41 2017/12/14 10:27:50 siva Exp $
*
* Description: Contains LLDP standard mib Low Level Routines
*********************************************************************/
#include "lr.h"
#include "fssnmp.h"
#include "lldinc.h"
#include "lldcli.h"
#include "slldv2cli.h"

extern UINT4        LldpConfigManAddrPortsTxEnable[11];
#ifdef ISS_WANTED
extern INT4         gi4MibResStatus;
#endif
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpMessageTxInterval
 Input       :  The Indices

                The Object 
                retValLldpMessageTxInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpMessageTxInterval (INT4 *pi4RetValLldpMessageTxInterval)
{
    LldpUtlGetMessageTxInterval (pi4RetValLldpMessageTxInterval);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpMessageTxHoldMultiplier
 Input       :  The Indices

                The Object 
                retValLldpMessageTxHoldMultiplier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpMessageTxHoldMultiplier (INT4 *pi4RetValLldpMessageTxHoldMultiplier)
{
    LldpUtlGetMessageTxHoldMultiplier (pi4RetValLldpMessageTxHoldMultiplier);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpReinitDelay
 Input       :  The Indices

                The Object 
                retValLldpReinitDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpReinitDelay (INT4 *pi4RetValLldpReinitDelay)
{
    LldpUtlGetReinitDelay (pi4RetValLldpReinitDelay);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpTxDelay
 Input       :  The Indices

                The Object 
                retValLldpTxDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpTxDelay (INT4 *pi4RetValLldpTxDelay)
{
    LldpUtlGetTxDelay (pi4RetValLldpTxDelay);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpNotificationInterval
 Input       :  The Indices

                The Object 
                retValLldpNotificationInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpNotificationInterval (INT4 *pi4RetValLldpNotificationInterval)
{
    LldpUtlGetNotificationInterval (pi4RetValLldpNotificationInterval);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpMessageTxInterval
 Input       :  The Indices

                The Object 
                setValLldpMessageTxInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpMessageTxInterval (INT4 i4SetValLldpMessageTxInterval)
{
    if (LldpUtlSetMessageTxInterval (i4SetValLldpMessageTxInterval) ==
        OSIX_SUCCESS)
    {
        /*Sending Trigger to MSR */
        LLDP_MSR_NOTIFY_SCALAR (LldpV2MessageTxInterval,
                                (sizeof (LldpV2MessageTxInterval) /
                                 sizeof (UINT4)),
                                (UINT4) i4SetValLldpMessageTxInterval);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetLldpMessageTxHoldMultiplier
 Input       :  The Indices

                The Object 
                setValLldpMessageTxHoldMultiplier
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpMessageTxHoldMultiplier (INT4 i4SetValLldpMessageTxHoldMultiplier)
{
    if (LldpUtlSetMessageTxHoldMultiplier (i4SetValLldpMessageTxHoldMultiplier)
        == OSIX_SUCCESS)
    {
        /*Sending Trigger to MSR */
        LLDP_MSR_NOTIFY_SCALAR (LldpV2MessageTxHoldMultiplier,
                                (sizeof (LldpV2MessageTxHoldMultiplier) /
                                 sizeof (UINT4)),
                                (UINT4) i4SetValLldpMessageTxHoldMultiplier);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetLldpReinitDelay
 Input       :  The Indices

                The Object 
                setValLldpReinitDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpReinitDelay (INT4 i4SetValLldpReinitDelay)
{
    if (LldpUtlSetReinitDelay (i4SetValLldpReinitDelay) == OSIX_SUCCESS)
    {
        /*Sending Trigger to MSR */
        LLDP_MSR_NOTIFY_SCALAR (LldpV2ReinitDelay,
                                (sizeof (LldpV2ReinitDelay) /
                                 sizeof (UINT4)),
                                (UINT4) i4SetValLldpReinitDelay);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetLldpTxDelay
 Input       :  The Indices

                The Object 
                setValLldpTxDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpTxDelay (INT4 i4SetValLldpTxDelay)
{
    LldpUtlSetTxDelay (i4SetValLldpTxDelay);

    /*Sending Trigger to MSR */
    LLDP_MSR_NOTIFY_SCALAR (LldpTxDelay,
                            (sizeof (LldpTxDelay) / sizeof (INT4)),
                            (UINT4) i4SetValLldpTxDelay);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetLldpNotificationInterval
 Input       :  The Indices

                The Object 
                setValLldpNotificationInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpNotificationInterval (INT4 i4SetValLldpNotificationInterval)
{
    if (LldpUtlSetNotificationInterval (i4SetValLldpNotificationInterval) ==
        OSIX_SUCCESS)
    {
        /*Sending Trigger to MSR */
        LLDP_MSR_NOTIFY_SCALAR (LldpV2NotificationInterval,
                                (sizeof (LldpV2NotificationInterval) /
                                 sizeof (UINT4)),
                                (UINT4) i4SetValLldpNotificationInterval);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpMessageTxInterval
 Input       :  The Indices

                The Object 
                testValLldpMessageTxInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpMessageTxInterval (UINT4 *pu4ErrorCode,
                                INT4 i4TestValLldpMessageTxInterval)
{
    if (LldpUtlTestMessageTxInterval
        (pu4ErrorCode, i4TestValLldpMessageTxInterval) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlTestMessageTxInterval returns Failure !!\r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2LldpMessageTxHoldMultiplier
 Input       :  The Indices

                The Object 
                testValLldpMessageTxHoldMultiplier
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpMessageTxHoldMultiplier (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValLldpMessageTxHoldMultiplier)
{
    if (LldpUtlTestMessageTxHoldMultiplier
        (pu4ErrorCode, i4TestValLldpMessageTxHoldMultiplier) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlTestMessageTxHoldMultiplier returns Failure!!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2LldpReinitDelay
 Input       :  The Indices

                The Object 
                testValLldpReinitDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpReinitDelay (UINT4 *pu4ErrorCode, INT4 i4TestValLldpReinitDelay)
{
    if (LldpUtlTestReinitDelay (pu4ErrorCode, i4TestValLldpReinitDelay) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlTestReinitDelay returns Failure!!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2LldpTxDelay
 Input       :  The Indices

                The Object 
                testValLldpTxDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpTxDelay (UINT4 *pu4ErrorCode, INT4 i4TestValLldpTxDelay)
{
    if (LldpUtlTestTxDelay (pu4ErrorCode, i4TestValLldpTxDelay) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlTestTxDelay returns Failure!!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2LldpNotificationInterval
 Input       :  The Indices

                The Object 
                testValLldpNotificationInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpNotificationInterval (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValLldpNotificationInterval)
{
    if (LldpUtlTestNotificationInterval
        (pu4ErrorCode, i4TestValLldpNotificationInterval) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlTestTxDelay returns Failure!!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpPortConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpPortConfigTable
 Input       :  The Indices
                LldpPortConfigPortNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpPortConfigTable (INT4 i4LldpPortConfigPortNum)
{
    if (LldpUtlValidateIndexInstanceLldpPortConfigTable
        (i4LldpPortConfigPortNum, gau1LldpMcastAddr) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlValidateIndexInstanceLldpPortConfigTable returns failure !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpPortConfigTable
 Input       :  The Indices
                LldpPortConfigPortNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpPortConfigTable (INT4 *pi4LldpPortConfigPortNum)
{
    if (LldpUtlGetFirstIndexLldpPortConfigTable (pi4LldpPortConfigPortNum) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpPortConfigTable returns failure !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpPortConfigTable
 Input       :  The Indices
                LldpPortConfigPortNum
                nextLldpPortConfigPortNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpPortConfigTable (INT4 i4LldpPortConfigPortNum,
                                    INT4 *pi4NextLldpPortConfigPortNum)
{
    if (LldpUtlGetNextIndexLldpPortConfigTable (i4LldpPortConfigPortNum,
                                                pi4NextLldpPortConfigPortNum) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpPortConfigTable returns failure !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpPortConfigAdminStatus
 Input       :  The Indices
                LldpPortConfigPortNum

                The Object 
                retValLldpPortConfigAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpPortConfigAdminStatus (INT4 i4LldpPortConfigPortNum,
                                 INT4 *pi4RetValLldpPortConfigAdminStatus)
{
    if (LldpUtlGetLldpPortConfigAdminStatus
        (i4LldpPortConfigPortNum, gau1LldpMcastAddr,
         pi4RetValLldpPortConfigAdminStatus) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Unable to get the u4LocPort  !!\r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpPortConfigNotificationEnable
 Input       :  The Indices
                LldpPortConfigPortNum

                The Object 
                retValLldpPortConfigNotificationEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpPortConfigNotificationEnable (INT4 i4LldpPortConfigPortNum,
                                        INT4
                                        *pi4RetValLldpPortConfigNotificationEnable)
{
    if (LldpUtlGetLldpPortConfigNotificationEnable
        (i4LldpPortConfigPortNum, gau1LldpMcastAddr,
         pi4RetValLldpPortConfigNotificationEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Unable to get the pi4RetValLldpPortConfigNotificationEnable"
                  "Invalid u4LocPort  !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpPortConfigTLVsTxEnable
 Input       :  The Indices
                LldpPortConfigPortNum

                The Object 
                retValLldpPortConfigTLVsTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpPortConfigTLVsTxEnable (INT4 i4LldpPortConfigPortNum,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValLldpPortConfigTLVsTxEnable)
{
    if (LldpUtlGetLldpPortConfigTLVsTxEnable
        (i4LldpPortConfigPortNum, gau1LldpMcastAddr,
         pRetValLldpPortConfigTLVsTxEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Unable to get the pRetValLldpPortConfigTLVsTxEnable"
                  "u4LocPort is Invalid !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpPortConfigAdminStatus
 Input       :  The Indices
                LldpPortConfigPortNum

                The Object 
                setValLldpPortConfigAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpPortConfigAdminStatus (INT4 i4LldpPortConfigPortNum,
                                 INT4 i4SetValLldpPortConfigAdminStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if (LldpUtlSetLldpPortConfigAdminStatus
        (i4LldpPortConfigPortNum, gau1LldpMcastAddr,
         i4SetValLldpPortConfigAdminStatus) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to Set nmhSetLldpPortConfigAdminStatus!!  \r\n");
        return SNMP_FAILURE;
    }

    /*Sending Trigger to MSR */
    LLDP_MSR_NOTIFY_PORT_CONFIG_TBL (LldpV2PortConfigAdminStatus,
                                     (sizeof (LldpV2PortConfigAdminStatus) /
                                      sizeof (UINT4)), &SnmpNotifyInfo);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", i4LldpPortConfigPortNum,
                      LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpPortConfigPortNum),
                      i4SetValLldpPortConfigAdminStatus));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetLldpPortConfigNotificationEnable
 Input       :  The Indices
                LldpPortConfigPortNum

                The Object 
                setValLldpPortConfigNotificationEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpPortConfigNotificationEnable (INT4 i4LldpPortConfigPortNum,
                                        INT4
                                        i4SetValLldpPortConfigNotificationEnable)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if (LldpUtlSetLldpPortConfigNotificationEnable
        (i4LldpPortConfigPortNum, gau1LldpMcastAddr,
         i4SetValLldpPortConfigNotificationEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to Set nmhSetLldpPortConfigNotificationEnable!!  \r\n");
        return SNMP_FAILURE;
    }

    /*Sending Trigger to MSR */
    LLDP_MSR_NOTIFY_PORT_CONFIG_TBL (LldpV2PortConfigNotificationEnable,
                                     (sizeof
                                      (LldpV2PortConfigNotificationEnable) /
                                      sizeof (UINT4)), &SnmpNotifyInfo);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", i4LldpPortConfigPortNum,
                      LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpPortConfigPortNum),
                      i4SetValLldpPortConfigNotificationEnable));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetLldpPortConfigTLVsTxEnable
 Input       :  The Indices
                LldpPortConfigPortNum

                The Object 
                setValLldpPortConfigTLVsTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpPortConfigTLVsTxEnable (INT4 i4LldpPortConfigPortNum,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValLldpPortConfigTLVsTxEnable)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if (LldpUtlSetLldpPortConfigTLVsTxEnable
        (i4LldpPortConfigPortNum, gau1LldpMcastAddr,
         pSetValLldpPortConfigTLVsTxEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD Unable to Set nmhSetLldpPortConfigNotificationEnable!!  \r\n");
        return SNMP_FAILURE;
    }

    /*Sending Trigger to MSR */
    LLDP_MSR_NOTIFY_PORT_CONFIG_TBL (LldpV2PortConfigTLVsTxEnable,
                                     (sizeof (LldpV2PortConfigTLVsTxEnable) /
                                      sizeof (UINT4)), &SnmpNotifyInfo);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %s", i4LldpPortConfigPortNum,
                      LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpPortConfigPortNum),
                      pSetValLldpPortConfigTLVsTxEnable));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpPortConfigAdminStatus
 Input       :  The Indices
                LldpPortConfigPortNum

                The Object 
                testValLldpPortConfigAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpPortConfigAdminStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4LldpPortConfigPortNum,
                                    INT4 i4TestValLldpPortConfigAdminStatus)
{
    if (LldpUtlTestLldpPortConfigAdminStatus
        (pu4ErrorCode, i4LldpPortConfigPortNum, gau1LldpMcastAddr,
         i4TestValLldpPortConfigAdminStatus) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlTestLldpPortConfigAdminStatus "
                  "returns FAILURE \r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2LldpPortConfigNotificationEnable
 Input       :  The Indices
                LldpPortConfigPortNum

                The Object 
                testValLldpPortConfigNotificationEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpPortConfigNotificationEnable (UINT4 *pu4ErrorCode,
                                           INT4 i4LldpPortConfigPortNum,
                                           INT4
                                           i4TestValLldpPortConfigNotificationEnable)
{
    if (LldpUtlTestLldpPortConfigNotificationEnable
        (pu4ErrorCode, i4LldpPortConfigPortNum, gau1LldpMcastAddr,
         i4TestValLldpPortConfigNotificationEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlTestLldpPortConfigNotificationEnable "
                  "returns FAILURE \r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2LldpPortConfigTLVsTxEnable
 Input       :  The Indices
                LldpPortConfigPortNum

                The Object 
                testValLldpPortConfigTLVsTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpPortConfigTLVsTxEnable (UINT4 *pu4ErrorCode,
                                     INT4 i4LldpPortConfigPortNum,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValLldpPortConfigTLVsTxEnable)
{
    if (LldpUtlTestLldpPortConfigTLVsTxEnable
        (pu4ErrorCode, i4LldpPortConfigPortNum, gau1LldpMcastAddr,
         pTestValLldpPortConfigTLVsTxEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlTestLldpPortConfigTLVsTxEnable "
                  "returns FAILURE \r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpLocManAddrTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpLocManAddrTable
 Input       :  The Indices
                LldpLocManAddrSubtype
                LldpLocManAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpLocManAddrTable (INT4 i4LldpLocManAddrSubtype,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pLldpLocManAddr)
{
    if (LldpUtlValidateIndexInstanceLldpLocManAddrTable
        (i4LldpLocManAddrSubtype, pLldpLocManAddr) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpUtlValidateIndexInstanceLldpLocManAddrTable returns NULL\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpLocManAddrTable
 Input       :  The Indices
                LldpLocManAddrSubtype
                LldpLocManAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpLocManAddrTable (INT4 *pi4LldpLocManAddrSubtype,
                                     tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr)
{
    if (LldpUtlGetFirstIndexLldpLocManAddrTable (pi4LldpLocManAddrSubtype,
                                                 pLldpLocManAddr) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpLocManAddrTable returns Null\r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpLocManAddrTable
 Input       :  The Indices
                LldpLocManAddrSubtype
                nextLldpLocManAddrSubtype
                LldpLocManAddr
                nextLldpLocManAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpLocManAddrTable (INT4 i4LldpLocManAddrSubtype,
                                    INT4 *pi4NextLldpLocManAddrSubtype,
                                    tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextLldpLocManAddr)
{
    if (LldpUtlGetNextIndexLldpLocManAddrTable
        (i4LldpLocManAddrSubtype, pi4NextLldpLocManAddrSubtype, pLldpLocManAddr,
         pNextLldpLocManAddr) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpLocManAddrTable returns Null\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpLocManAddrLen
 Input       :  The Indices
                LldpLocManAddrSubtype
                LldpLocManAddr

                The Object 
                retValLldpLocManAddrLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpLocManAddrLen (INT4 i4LldpLocManAddrSubtype,
                         tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr,
                         INT4 *pi4RetValLldpLocManAddrLen)
{
    if (LldpUtlGetLocManAddrLen (i4LldpLocManAddrSubtype, pLldpLocManAddr,
                                 pi4RetValLldpLocManAddrLen) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpUtlGetLocManAddrLen returns FAILURE \r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpLocManAddrIfSubtype
 Input       :  The Indices
                LldpLocManAddrSubtype
                LldpLocManAddr

                The Object 
                retValLldpLocManAddrIfSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpLocManAddrIfSubtype (INT4 i4LldpLocManAddrSubtype,
                               tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr,
                               INT4 *pi4RetValLldpLocManAddrIfSubtype)
{
    if (LldpUtlGetLocManAddrIfSubtype (i4LldpLocManAddrSubtype, pLldpLocManAddr,
                                       pi4RetValLldpLocManAddrIfSubtype) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpTxUtlGetLocManAddrNode returns NULL \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpLocManAddrIfId
 Input       :  The Indices
                LldpLocManAddrSubtype
                LldpLocManAddr

                The Object 
                retValLldpLocManAddrIfId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpLocManAddrIfId (INT4 i4LldpLocManAddrSubtype,
                          tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr,
                          INT4 *pi4RetValLldpLocManAddrIfId)
{
    if (LldpUtlGetLocManAddrIfId (i4LldpLocManAddrSubtype, pLldpLocManAddr,
                                  pi4RetValLldpLocManAddrIfId) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpTxUtlGetLocManAddrNode returns NULL \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpLocManAddrOID
 Input       :  The Indices
                LldpLocManAddrSubtype
                LldpLocManAddr

                The Object 
                retValLldpLocManAddrOID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpLocManAddrOID (INT4 i4LldpLocManAddrSubtype,
                         tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr,
                         tSNMP_OID_TYPE * pRetValLldpLocManAddrOID)
{
    if (LldpUtlGetLocManAddrIfOID (i4LldpLocManAddrSubtype, pLldpLocManAddr,
                                   pRetValLldpLocManAddrOID) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpTxUtlGetLocManAddrNode returns NULL \r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpConfigManAddrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpConfigManAddrTable
 Input       :  The Indices
                LldpLocManAddrSubtype
                LldpLocManAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpConfigManAddrTable (INT4 i4LldpLocManAddrSubtype,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pLldpLocManAddr)
{
    if (LldpUtlValidateIndexInstanceConfigManAddrTable
        (i4LldpLocManAddrSubtype, pLldpLocManAddr) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpUtlValidateIndexInstanceConfigManAddrTable returns NULL \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpConfigManAddrTable
 Input       :  The Indices
                LldpLocManAddrSubtype
                LldpLocManAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpConfigManAddrTable (INT4 *pi4LldpLocManAddrSubtype,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pLldpLocManAddr)
{
    if (LldpUtlGetFirstIndexLldpConfigManAddrTable
        (pi4LldpLocManAddrSubtype, pLldpLocManAddr) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpConfigManAddrTable returns NULL\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpConfigManAddrTable
 Input       :  The Indices
                LldpLocManAddrSubtype
                nextLldpLocManAddrSubtype
                LldpLocManAddr
                nextLldpLocManAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpConfigManAddrTable (INT4 i4LldpLocManAddrSubtype,
                                       INT4 *pi4NextLldpLocManAddrSubtype,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pLldpLocManAddr,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextLldpLocManAddr)
{
    if (LldpUtlGetNextIndexLldpConfigManAddrTable
        (i4LldpLocManAddrSubtype, pi4NextLldpLocManAddrSubtype, pLldpLocManAddr,
         pNextLldpLocManAddr) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpConfigManAddrTable returns Failure !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpConfigManAddrPortsTxEnable
 Input       :  The Indices
                LldpLocManAddrSubtype
                LldpLocManAddr

                The Object 
                retValLldpConfigManAddrPortsTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpConfigManAddrPortsTxEnable (INT4 i4LldpLocManAddrSubtype,
                                      tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValLldpConfigManAddrPortsTxEnable)
{
    if (LldpUtlGetConfigManAddrPortsTxEnable
        (i4LldpLocManAddrSubtype, pLldpLocManAddr,
         pRetValLldpConfigManAddrPortsTxEnable) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpTxUtlGetLocManAddrNode returns NULL \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpConfigManAddrPortsTxEnable
 Input       :  The Indices
                LldpLocManAddrSubtype
                LldpLocManAddr

                The Object 
                setValLldpConfigManAddrPortsTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpConfigManAddrPortsTxEnable (INT4 i4LldpLocManAddrSubtype,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pLldpLocManAddr,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSetValLldpConfigManAddrPortsTxEnable)
{
    tLldpLocManAddrTable *pLocManAddrNode = NULL;
    UINT1               au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR];
    tLldpLocPortInfo   *pPortInfo = NULL;
    BOOL1               bResult = OSIX_FALSE;
    INT4                i4IfIndex = 0;
    UINT4               u4DestMacIndex = 0;
    UINT4               u4PrevDestMacIndex = 0;
    UINT4               u4LocPortNum = 0;
    INT4                i4PrevIfIndex = 0;
    UINT4               u4SeqNum = LLDP_ZERO;
    tMacAddr            DstMac;
#ifdef ISS_WANTED
    UINT4               u4TempAddr = 0;
    tNetIpv4IfInfo      NetIp4IfInfo;
    tIp6Addr            TempIp6Addr;
    tNetIpv6AddrChange  NetIpv6AddrChange;
#endif
    tSnmpNotifyInfo     SnmpNotifyInfo;

    MEMSET (DstMac, 0, sizeof (tMacAddr));
    MEMSET (&au1LocManAddr[0], 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMCPY (&au1LocManAddr[0], pLldpLocManAddr->pu1_OctetList,
            pLldpLocManAddr->i4_Length);
#ifdef ISS_WANTED
    MEMSET (&NetIp4IfInfo, 0, sizeof (tNetIpv4IfInfo));
    MEMSET (&NetIpv6AddrChange, 0, sizeof (tNetIpv6AddrChange));
#endif
    MEMSET (&SnmpNotifyInfo, LLDP_ZERO, sizeof (tSnmpNotifyInfo));
#ifdef ISS_WANTED
    if (MIB_RESTORE_IN_PROGRESS == gi4MibResStatus)
    {
        if (i4LldpLocManAddrSubtype == IPVX_ADDR_FMLY_IPV4)
        {
            NetIp4IfInfo.u4IfIndex = (UINT4) i4IfIndex;
            MEMCPY (&u4TempAddr, au1LocManAddr, IPVX_IPV4_ADDR_LEN);
            NetIp4IfInfo.u4Addr = u4TempAddr;
            LldpTxUtlAddIpv4Addr (&NetIp4IfInfo);
        }
        else if (i4LldpLocManAddrSubtype == IPVX_ADDR_FMLY_IPV6)
        {
            NetIpv6AddrChange.u4Index = (UINT4) i4IfIndex;
            MEMCPY (&TempIp6Addr, au1LocManAddr, IPVX_IPV6_ADDR_LEN);
            (&NetIpv6AddrChange)->Ipv6AddrInfo.Ip6Addr = TempIp6Addr;
            LldpTxUtlAddIpv6Addr (&NetIpv6AddrChange);
        }
    }
#endif
    if (nmhGetFirstIndexLldpV2PortConfigTable (&i4IfIndex, &u4DestMacIndex)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    do
    {
        nmhGetLldpV2DestMacAddress (u4DestMacIndex, &DstMac);
        if (LldpUtlGetLocalPort (i4IfIndex, DstMac, &u4LocPortNum) ==
            OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
        pPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPortNum);
        if (pPortInfo == NULL)
            return OSIX_FAILURE;

        OSIX_BITLIST_IS_BIT_SET (pSetValLldpConfigManAddrPortsTxEnable->
                                 pu1_OctetList, (UINT2) pPortInfo->u4LocPortNum,
                                 (INT4) pSetValLldpConfigManAddrPortsTxEnable->
                                 i4_Length, bResult);
        if (bResult == OSIX_TRUE)
        {

            pPortInfo->u1ManAddrTlvTxEnabled = LLDP_TRUE;
        }
        else
        {
            pPortInfo->u1ManAddrTlvTxEnabled = LLDP_FALSE;
        }

        i4PrevIfIndex = i4IfIndex;
        u4PrevDestMacIndex = u4DestMacIndex;
    }
    while (nmhGetNextIndexLldpV2PortConfigTable
           (i4PrevIfIndex, &i4IfIndex, u4PrevDestMacIndex,
            &u4DestMacIndex) == SNMP_SUCCESS);

    pLocManAddrNode = (tLldpLocManAddrTable *)
        LldpTxUtlGetLocManAddrNode (i4LldpLocManAddrSubtype, &au1LocManAddr[0]);

    if (pLocManAddrNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpTxUtlGetLocManAddrNode returns NULL \r\n");
        return SNMP_FAILURE;
    }

    MEMCPY (pLocManAddrNode->au1LocManAddrPortsTxEnable,
            pSetValLldpConfigManAddrPortsTxEnable->pu1_OctetList,
            pSetValLldpConfigManAddrPortsTxEnable->i4_Length);

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, LldpConfigManAddrPortsTxEnable,
                          u4SeqNum,
                          FALSE, LldpLock, LldpUnLock, 2, SNMP_SUCCESS);
    SnmpNotifyInfo.u4OidLen =
        ((sizeof (LldpConfigManAddrPortsTxEnable)) / sizeof (UINT4));
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %s", i4LldpLocManAddrSubtype,
                      pLldpLocManAddr, pSetValLldpConfigManAddrPortsTxEnable));

    LldpTxUtlHandleLocSysInfoChg ();

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpConfigManAddrPortsTxEnable
 Input       :  The Indices
                LldpLocManAddrSubtype
                LldpLocManAddr

                The Object 
                testValLldpConfigManAddrPortsTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpConfigManAddrPortsTxEnable (UINT4 *pu4ErrorCode,
                                         INT4 i4LldpLocManAddrSubtype,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pLldpLocManAddr,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pTestValLldpConfigManAddrPortsTxEnable)
{
    UNUSED_PARAM (i4LldpLocManAddrSubtype);
    UNUSED_PARAM (pLldpLocManAddr);

    if (LldpUtlTestConfigManAddrPortsTxEnable (pu4ErrorCode,
                                               pTestValLldpConfigManAddrPortsTxEnable)
        == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_CRITICAL_TRC, " SNMPSTD: "
                  "LldpUtlTestConfigManAddrPortsTxEnable returns FAILURE \r\n");
        CLI_SET_ERR (LLDP_CLI_ERR_INVALID_MAN_ADDR_LEN);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpStatsRemTablesLastChangeTime
 Input       :  The Indices

                The Object 
                retValLldpStatsRemTablesLastChangeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpStatsRemTablesLastChangeTime (UINT4
                                        *pu4RetValLldpStatsRemTablesLastChangeTime)
{
    LldpUtlGetStatsRemTablesLastChangeTime
        (pu4RetValLldpStatsRemTablesLastChangeTime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpStatsRemTablesInserts
 Input       :  The Indices

                The Object 
                retValLldpStatsRemTablesInserts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpStatsRemTablesInserts (UINT4 *pu4RetValLldpStatsRemTablesInserts)
{
    LldpUtlGetStatsRemTablesInserts (pu4RetValLldpStatsRemTablesInserts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpStatsRemTablesDeletes
 Input       :  The Indices

                The Object 
                retValLldpStatsRemTablesDeletes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpStatsRemTablesDeletes (UINT4 *pu4RetValLldpStatsRemTablesDeletes)
{
    LldpUtlGetStatsRemTablesDeletes (pu4RetValLldpStatsRemTablesDeletes);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpStatsRemTablesDrops
 Input       :  The Indices

                The Object 
                retValLldpStatsRemTablesDrops
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpStatsRemTablesDrops (UINT4 *pu4RetValLldpStatsRemTablesDrops)
{
    LldpUtlGetStatsRemTablesDrops (pu4RetValLldpStatsRemTablesDrops);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpStatsRemTablesAgeouts
 Input       :  The Indices

                The Object 
                retValLldpStatsRemTablesAgeouts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpStatsRemTablesAgeouts (UINT4 *pu4RetValLldpStatsRemTablesAgeouts)
{
    LldpUtlGetStatsRemTablesAgeouts (pu4RetValLldpStatsRemTablesAgeouts);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpStatsTxPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpStatsTxPortTable
 Input       :  The Indices
                LldpStatsTxPortNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpStatsTxPortTable (INT4 i4LldpStatsTxPortNum)
{
    if (LldpUtlValidateIndexInstanceLldpStatsTxPortTable
        (i4LldpStatsTxPortNum, gau1LldpMcastAddr) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpUtlValidateIndexInstanceLldpStatsTxPortTable  returns FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpStatsTxPortTable
 Input       :  The Indices
                LldpStatsTxPortNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpStatsTxPortTable (INT4 *pi4LldpStatsTxPortNum)
{
    if (LldpUtlGetFirstIndexLldpStatsTxPortTable (pi4LldpStatsTxPortNum) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpStatsTxPortTable returns Failure !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpStatsTxPortTable
 Input       :  The Indices
                LldpStatsTxPortNum
                nextLldpStatsTxPortNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpStatsTxPortTable (INT4 i4LldpStatsTxPortNum,
                                     INT4 *pi4NextLldpStatsTxPortNum)
{
    if (LldpUtlGetNextLldpStatsTxPortTable
        (i4LldpStatsTxPortNum, pi4NextLldpStatsTxPortNum))
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextLldpStatsTxPortTable returns Failure !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpStatsTxPortFramesTotal
 Input       :  The Indices
                LldpStatsTxPortNum

                The Object 
                retValLldpStatsTxPortFramesTotal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpStatsTxPortFramesTotal (INT4 i4LldpStatsTxPortNum,
                                  UINT4 *pu4RetValLldpStatsTxPortFramesTotal)
{
    if (LldpUtlGetStatsTxPortFramesTotal
        (i4LldpStatsTxPortNum, pu4RetValLldpStatsTxPortFramesTotal,
         gau1LldpMcastAddr) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetStatsTxPortFramesTotal is failed \r\n");
        return SNMP_FAILURE;

    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpStatsRxPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpStatsRxPortTable
 Input       :  The Indices
                LldpStatsRxPortNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpStatsRxPortTable (INT4 i4LldpStatsRxPortNum)
{
    if (LldpUtlValidateIndexInstanceLldpStatsRxPortTable
        (i4LldpStatsRxPortNum, gau1LldpMcastAddr) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpUtlValidateIndexInstanceLldpStatsRxPortTable returns FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpStatsRxPortTable
 Input       :  The Indices
                LldpStatsRxPortNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpStatsRxPortTable (INT4 *pi4LldpStatsRxPortNum)
{
    if (LldpUtlGetFirstIndexStatsRxPortTable (pi4LldpStatsRxPortNum) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  " LldpUtlGetFirstIndexStatsRxPortTable returns FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpStatsRxPortTable
 Input       :  The Indices
                LldpStatsRxPortNum
                nextLldpStatsRxPortNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpStatsRxPortTable (INT4 i4LldpStatsRxPortNum,
                                     INT4 *pi4NextLldpStatsRxPortNum)
{
    if (LldpUtlGetNextLldpStatsTxPortTable
        (i4LldpStatsRxPortNum, pi4NextLldpStatsRxPortNum) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  " LldpUtlGetNextLldpStatsTxPortTable returns FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpStatsRxPortFramesDiscardedTotal
 Input       :  The Indices
                LldpStatsRxPortNum

                The Object 
                retValLldpStatsRxPortFramesDiscardedTotal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpStatsRxPortFramesDiscardedTotal (INT4 i4LldpStatsRxPortNum,
                                           UINT4
                                           *pu4RetValLldpStatsRxPortFramesDiscardedTotal)
{
    if (LldpUtlGetStatsRxPortFramesDiscardedTotal
        (i4LldpStatsRxPortNum, gau1LldpMcastAddr,
         pu4RetValLldpStatsRxPortFramesDiscardedTotal) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: "
                  "LldpUtlGetStatsRxPortFramesDiscardedTotal returns FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpStatsRxPortFramesErrors
 Input       :  The Indices
                LldpStatsRxPortNum

                The Object 
                retValLldpStatsRxPortFramesErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpStatsRxPortFramesErrors (INT4 i4LldpStatsRxPortNum,
                                   UINT4 *pu4RetValLldpStatsRxPortFramesErrors)
{
    if (LldpUtlGetStatsRxPortFramesErrors
        (i4LldpStatsRxPortNum, gau1LldpMcastAddr,
         pu4RetValLldpStatsRxPortFramesErrors) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetStatsRxPortFramesErrors is failed \r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpStatsRxPortFramesTotal
 Input       :  The Indices
                LldpStatsRxPortNum

                The Object 
                retValLldpStatsRxPortFramesTotal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpStatsRxPortFramesTotal (INT4 i4LldpStatsRxPortNum,
                                  UINT4 *pu4RetValLldpStatsRxPortFramesTotal)
{
    if (LldpUtlGetStatsRxPortFramesTotal
        (i4LldpStatsRxPortNum, gau1LldpMcastAddr,
         pu4RetValLldpStatsRxPortFramesTotal) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetStatsRxPortFramesTotal is failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpStatsRxPortTLVsDiscardedTotal
 Input       :  The Indices
                LldpStatsRxPortNum

                The Object 
                retValLldpStatsRxPortTLVsDiscardedTotal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpStatsRxPortTLVsDiscardedTotal (INT4 i4LldpStatsRxPortNum,
                                         UINT4
                                         *pu4RetValLldpStatsRxPortTLVsDiscardedTotal)
{
    if (LldpUtlGetStatsRxPortTLVsDiscardedTotal
        (i4LldpStatsRxPortNum, gau1LldpMcastAddr,
         pu4RetValLldpStatsRxPortTLVsDiscardedTotal) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetStatsRxPortFramesTotal is failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpStatsRxPortTLVsUnrecognizedTotal
 Input       :  The Indices
                LldpStatsRxPortNum

                The Object 
                retValLldpStatsRxPortTLVsUnrecognizedTotal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpStatsRxPortTLVsUnrecognizedTotal (INT4 i4LldpStatsRxPortNum,
                                            UINT4
                                            *pu4RetValLldpStatsRxPortTLVsUnrecognizedTotal)
{
    if (LldpUtlGetStatsRxPortTLVsUnrecognizedTotal
        (i4LldpStatsRxPortNum, gau1LldpMcastAddr,
         pu4RetValLldpStatsRxPortTLVsUnrecognizedTotal) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetStatsRxPortTLVsUnrecognizedTotal is failed \r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpStatsRxPortAgeoutsTotal
 Input       :  The Indices
                LldpStatsRxPortNum

                The Object 
                retValLldpStatsRxPortAgeoutsTotal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpStatsRxPortAgeoutsTotal (INT4 i4LldpStatsRxPortNum,
                                   UINT4 *pu4RetValLldpStatsRxPortAgeoutsTotal)
{
    if (LldpUtlGetStatsRxPortAgeoutsTotal
        (i4LldpStatsRxPortNum, gau1LldpMcastAddr,
         pu4RetValLldpStatsRxPortAgeoutsTotal) == OSIX_FAILURE)

    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetStatsRxPortAgeoutsTotal is failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpLocChassisIdSubtype
 Input       :  The Indices

                The Object 
                retValLldpLocChassisIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpLocChassisIdSubtype (INT4 *pi4RetValLldpLocChassisIdSubtype)
{
    LldpUtlGetLocChassisIdSubtype (pi4RetValLldpLocChassisIdSubtype);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpLocChassisId
 Input       :  The Indices

                The Object 
                retValLldpLocChassisId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpLocChassisId (tSNMP_OCTET_STRING_TYPE * pRetValLldpLocChassisId)
{
    if (LldpUtlGetLocChassisId (pRetValLldpLocChassisId) == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetLocChassisId is failed \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpLocSysName
 Input       :  The Indices

                The Object 
                retValLldpLocSysName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpLocSysName (tSNMP_OCTET_STRING_TYPE * pRetValLldpLocSysName)
{
    LldpUtlLocSysName (pRetValLldpLocSysName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpLocSysDesc
 Input       :  The Indices

                The Object 
                retValLldpLocSysDesc
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpLocSysDesc (tSNMP_OCTET_STRING_TYPE * pRetValLldpLocSysDesc)
{
    LldpUtlLocSysDesc (pRetValLldpLocSysDesc);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpLocSysCapSupported
 Input       :  The Indices

                The Object 
                retValLldpLocSysCapSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpLocSysCapSupported (tSNMP_OCTET_STRING_TYPE *
                              pRetValLldpLocSysCapSupported)
{
    LldpUtlLocSysCapSupported (pRetValLldpLocSysCapSupported);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpLocSysCapEnabled
 Input       :  The Indices

                The Object 
                retValLldpLocSysCapEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpLocSysCapEnabled (tSNMP_OCTET_STRING_TYPE *
                            pRetValLldpLocSysCapEnabled)
{
    LldpUtlLocSysCapEnabled (pRetValLldpLocSysCapEnabled);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpLocPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpLocPortTable
 Input       :  The Indices
                LldpLocPortNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpLocPortTable (INT4 i4LldpLocPortNum)
{
    if (LldpUtlValidateIndexInstanceLldpLocPortTable (i4LldpLocPortNum)
        == OSIX_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " SNMPSTD: LldpTxUtlValidatePortIndex"
                  " returns FAILURE \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpLocPortTable
 Input       :  The Indices
                LldpLocPortNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpLocPortTable (INT4 *pi4LldpLocPortNum)
{
    if (LldpUtlGetFirstIndexLldpLocPortTable (pi4LldpLocPortNum) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpLocPortTable returns failure!!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpLocPortTable
 Input       :  The Indices
                LldpLocPortNum
                nextLldpLocPortNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpLocPortTable (INT4 i4LldpLocPortNum,
                                 INT4 *pi4NextLldpLocPortNum)
{
    if (LldpUtlGetNextIndexLldpLocPortTable
        (i4LldpLocPortNum, pi4NextLldpLocPortNum) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpLocPortTable returns failure!!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpLocPortIdSubtype
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValLldpLocPortIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpLocPortIdSubtype (INT4 i4LldpLocPortNum,
                            INT4 *pi4RetValLldpLocPortIdSubtype)
{
    if (LldpUtlGetLocPortIdSubtype
        (i4LldpLocPortNum, pi4RetValLldpLocPortIdSubtype) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpLocPortId
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValLldpLocPortId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpLocPortId (INT4 i4LldpLocPortNum,
                     tSNMP_OCTET_STRING_TYPE * pRetValLldpLocPortId)
{
    if (LldpUtlLocPortId (i4LldpLocPortNum, pRetValLldpLocPortId) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpLocPortDesc
 Input       :  The Indices
                LldpLocPortNum

                The Object 
                retValLldpLocPortDesc
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpLocPortDesc (INT4 i4LldpLocPortNum,
                       tSNMP_OCTET_STRING_TYPE * pRetValLldpLocPortDesc)
{
    if (LldpUtlGetLocPortDesc (i4LldpLocPortNum, pRetValLldpLocPortDesc) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LLDP should be started before accessing its "
                  "MIB objects !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpRemTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpRemTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpRemTable (UINT4 u4LldpRemTimeMark,
                                      INT4 i4LldpRemLocalPortNum,
                                      INT4 i4LldpRemIndex)
{
    if (LldpUtlValidateIndexInstanceLldpRemTable (u4LldpRemTimeMark,
                                                  i4LldpRemLocalPortNum,
                                                  i4LldpRemIndex,
                                                  LLDP_V1_DEST_MAC_ADDR_INDEX
                                                  (i4LldpRemLocalPortNum)) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpRemTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpRemTable (UINT4 *pu4LldpRemTimeMark,
                              INT4 *pi4LldpRemLocalPortNum,
                              INT4 *pi4LldpRemIndex)
{
    if (LldpUtlGetFirstIndexLldpRemTable
        (pu4LldpRemTimeMark, pi4LldpRemLocalPortNum,
         pi4LldpRemIndex) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpRemTable returns failure!!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpRemTable
 Input       :  The Indices
                LldpRemTimeMark
                nextLldpRemTimeMark
                LldpRemLocalPortNum
                nextLldpRemLocalPortNum
                LldpRemIndex
                nextLldpRemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpRemTable (UINT4 u4LldpRemTimeMark,
                             UINT4 *pu4NextLldpRemTimeMark,
                             INT4 i4LldpRemLocalPortNum,
                             INT4 *pi4NextLldpRemLocalPortNum,
                             INT4 i4LldpRemIndex, INT4 *pi4NextLldpRemIndex)
{
    UINT4               u4NextDestIndex = 0;

    if (LldpUtlGetNextIndexLldpRemTable (u4LldpRemTimeMark,
                                         pu4NextLldpRemTimeMark,
                                         i4LldpRemLocalPortNum,
                                         pi4NextLldpRemLocalPortNum,
                                         LLDP_V1_DEST_MAC_ADDR_INDEX
                                         (i4LldpRemLocalPortNum),
                                         &u4NextDestIndex, i4LldpRemIndex,
                                         pi4NextLldpRemIndex) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexLldpRemTable returns failure!!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpRemChassisIdSubtype
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpRemChassisIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpRemChassisIdSubtype (UINT4 u4LldpRemTimeMark,
                               INT4 i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                               INT4 *pi4RetValLldpRemChassisIdSubtype)
{
    if (LldpUtlGetRemChassisIdSubtype
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         pi4RetValLldpRemChassisIdSubtype,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Function LldpUtlGetRemChassisIdSubtype return FAILURE !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpRemChassisId

 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpRemChassisId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpRemChassisId (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                        INT4 i4LldpRemIndex,
                        tSNMP_OCTET_STRING_TYPE * pRetValLldpRemChassisId)
{
    if (LldpUtlGetRemChassisId
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         pRetValLldpRemChassisId,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Function LldpUtlGetRemChassisId return FAILURE !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpRemPortIdSubtype
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpRemPortIdSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpRemPortIdSubtype (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                            INT4 i4LldpRemIndex,
                            INT4 *pi4RetValLldpRemPortIdSubtype)
{
    if (LldpUtlGetRemPortIdSubtype
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         pi4RetValLldpRemPortIdSubtype,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Function LldpUtlGetRemPortIdSubtype return FAILURE !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpRemPortId
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpRemPortId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpRemPortId (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                     INT4 i4LldpRemIndex,
                     tSNMP_OCTET_STRING_TYPE * pRetValLldpRemPortId)
{
    if (LldpUtlGetRemPortId
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         pRetValLldpRemPortId,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Function LldpUtlGetRemPortId return FAILURE !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpRemPortDesc
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpRemPortDesc
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpRemPortDesc (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                       INT4 i4LldpRemIndex,
                       tSNMP_OCTET_STRING_TYPE * pRetValLldpRemPortDesc)
{
    if (LldpUtlGetRemPortDesc
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         pRetValLldpRemPortDesc,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Function LldpUtlGetRemPortId return FAILURE !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpRemSysName
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpRemSysName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpRemSysName (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                      INT4 i4LldpRemIndex,
                      tSNMP_OCTET_STRING_TYPE * pRetValLldpRemSysName)
{
    if (LldpUtlGetRemSysName
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         pRetValLldpRemSysName,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Function LldpUtlGetRemSysName return FAILURE !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpRemSysDesc
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpRemSysDesc
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpRemSysDesc (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                      INT4 i4LldpRemIndex,
                      tSNMP_OCTET_STRING_TYPE * pRetValLldpRemSysDesc)
{
    if (LldpUtlGetRemSysDesc
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         pRetValLldpRemSysDesc,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Function LldpUtlGetRemSysDesc return FAILURE !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpRemSysCapSupported
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpRemSysCapSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpRemSysCapSupported (UINT4 u4LldpRemTimeMark,
                              INT4 i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValLldpRemSysCapSupported)
{
    if (LldpUtlGetRemSysCapSupported
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         pRetValLldpRemSysCapSupported,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Function LldpUtlGetRemSysCapSupported return FAILURE !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpRemSysCapEnabled
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex

                The Object 
                retValLldpRemSysCapEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpRemSysCapEnabled (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                            INT4 i4LldpRemIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValLldpRemSysCapEnabled)
{
    if (LldpUtlGetRemSysCapEnabled
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         pRetValLldpRemSysCapEnabled,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Function LldpUtlGetRemSysCapSupported return FAILURE !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpRemManAddrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpRemManAddrTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpRemManAddrSubtype
                LldpRemManAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpRemManAddrTable (UINT4 u4LldpRemTimeMark,
                                             INT4 i4LldpRemLocalPortNum,
                                             INT4 i4LldpRemIndex,
                                             INT4 i4LldpRemManAddrSubtype,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pLldpRemManAddr)
{
    if (LldpUtlValidateIndexInstanceLldpRemManAddrTable (u4LldpRemTimeMark,
                                                         i4LldpRemLocalPortNum,
                                                         i4LldpRemIndex,
                                                         i4LldpRemManAddrSubtype,
                                                         pLldpRemManAddr,
                                                         LLDP_V1_DEST_MAC_ADDR_INDEX
                                                         (i4LldpRemLocalPortNum))
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Function LldpUtlValidateIndexInstanceLldpRemManAddrTable  return FAILURE !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpRemManAddrTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpRemManAddrSubtype
                LldpRemManAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpRemManAddrTable (UINT4 *pu4LldpRemTimeMark,
                                     INT4 *pi4LldpRemLocalPortNum,
                                     INT4 *pi4LldpRemIndex,
                                     INT4 *pi4LldpRemManAddrSubtype,
                                     tSNMP_OCTET_STRING_TYPE * pLldpRemManAddr)
{
    if (LldpUtlGetFirstIndexLldpRemManAddrTable (pu4LldpRemTimeMark,
                                                 pi4LldpRemLocalPortNum,
                                                 pi4LldpRemIndex,
                                                 pi4LldpRemManAddrSubtype,
                                                 pLldpRemManAddr) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Function LldpUtlGetFirstIndexLldpRemManAddrTable return FAILURE !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpRemManAddrTable
 Input       :  The Indices
                LldpRemTimeMark
                nextLldpRemTimeMark
                LldpRemLocalPortNum
                nextLldpRemLocalPortNum
                LldpRemIndex
                nextLldpRemIndex
                LldpRemManAddrSubtype
                nextLldpRemManAddrSubtype
                LldpRemManAddr
                nextLldpRemManAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpRemManAddrTable (UINT4 u4LldpRemTimeMark,
                                    UINT4 *pu4NextLldpRemTimeMark,
                                    INT4 i4LldpRemLocalPortNum,
                                    INT4 *pi4NextLldpRemLocalPortNum,
                                    INT4 i4LldpRemIndex,
                                    INT4 *pi4NextLldpRemIndex,
                                    INT4 i4LldpRemManAddrSubtype,
                                    INT4 *pi4NextLldpRemManAddrSubtype,
                                    tSNMP_OCTET_STRING_TYPE * pLldpRemManAddr,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextLldpRemManAddr)
{
    UINT4               u4DestIndex = 0;

    if (LldpUtlGetNextIndexRemManAddrTable (u4LldpRemTimeMark,
                                            pu4NextLldpRemTimeMark,
                                            i4LldpRemLocalPortNum,
                                            pi4NextLldpRemLocalPortNum,
                                            LLDP_V1_DEST_MAC_ADDR_INDEX
                                            (i4LldpRemLocalPortNum),
                                            &u4DestIndex, i4LldpRemIndex,
                                            pi4NextLldpRemIndex,
                                            i4LldpRemManAddrSubtype,
                                            pi4NextLldpRemManAddrSubtype,
                                            pLldpRemManAddr,
                                            pNextLldpRemManAddr) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Function LldpUtlGetNextIndexRemManAddrTable return FAILURE !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpRemManAddrIfSubtype
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpRemManAddrSubtype
                LldpRemManAddr

                The Object 
                retValLldpRemManAddrIfSubtype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpRemManAddrIfSubtype (UINT4 u4LldpRemTimeMark,
                               INT4 i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                               INT4 i4LldpRemManAddrSubtype,
                               tSNMP_OCTET_STRING_TYPE * pLldpRemManAddr,
                               INT4 *pi4RetValLldpRemManAddrIfSubtype)
{
    if (LldpUtlGetRemManAddrIfSubtype
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         i4LldpRemManAddrSubtype, pLldpRemManAddr,
         pi4RetValLldpRemManAddrIfSubtype,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Function LldpUtlGetRemSysCapSupported return FAILURE !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpRemManAddrIfId
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpRemManAddrSubtype
                LldpRemManAddr

                The Object 
                retValLldpRemManAddrIfId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpRemManAddrIfId (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                          INT4 i4LldpRemIndex, INT4 i4LldpRemManAddrSubtype,
                          tSNMP_OCTET_STRING_TYPE * pLldpRemManAddr,
                          INT4 *pi4RetValLldpRemManAddrIfId)
{
    if (LldpUtlGetRemManAddrIfId
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         i4LldpRemManAddrSubtype, pLldpRemManAddr, pi4RetValLldpRemManAddrIfId,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)

    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Function LldpUtlGetRemSysCapSupported return FAILURE !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpRemManAddrOID
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpRemManAddrSubtype
                LldpRemManAddr

                The Object 
                retValLldpRemManAddrOID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpRemManAddrOID (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                         INT4 i4LldpRemIndex, INT4 i4LldpRemManAddrSubtype,
                         tSNMP_OCTET_STRING_TYPE * pLldpRemManAddr,
                         tSNMP_OID_TYPE * pRetValLldpRemManAddrOID)
{
    if (LldpUtlGetRemManAddrOID
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         i4LldpRemManAddrSubtype, pLldpRemManAddr, pRetValLldpRemManAddrOID,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Function LldpUtlGetRemSysCapSupported return FAILURE !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpRemUnknownTLVTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpRemUnknownTLVTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpRemUnknownTLVType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpRemUnknownTLVTable (UINT4 u4LldpRemTimeMark,
                                                INT4 i4LldpRemLocalPortNum,
                                                INT4 i4LldpRemIndex,
                                                INT4 i4LldpRemUnknownTLVType)
{
    if (LldpUtlValidateIndexInstanceRemUnknownTLVTable (u4LldpRemTimeMark,
                                                        i4LldpRemLocalPortNum,
                                                        i4LldpRemIndex,
                                                        i4LldpRemUnknownTLVType,
                                                        LLDP_V1_DEST_MAC_ADDR_INDEX
                                                        (i4LldpRemLocalPortNum))
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Function LldpUtlValidateIndexInstanceRemUnknownTLVTable  return FAILURE !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpRemUnknownTLVTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpRemUnknownTLVType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpRemUnknownTLVTable (UINT4 *pu4LldpRemTimeMark,
                                        INT4 *pi4LldpRemLocalPortNum,
                                        INT4 *pi4LldpRemIndex,
                                        INT4 *pi4LldpRemUnknownTLVType)
{
    if (LldpUtlGetFirstIndexLldpRemUnknownTLVTable
        (pu4LldpRemTimeMark, pi4LldpRemLocalPortNum, pi4LldpRemIndex,
         pi4LldpRemUnknownTLVType) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpRemUnknownTLVTable returns Failure!!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpRemUnknownTLVTable
 Input       :  The Indices
                LldpRemTimeMark
                nextLldpRemTimeMark
                LldpRemLocalPortNum
                nextLldpRemLocalPortNum
                LldpRemIndex
                nextLldpRemIndex
                LldpRemUnknownTLVType
                nextLldpRemUnknownTLVType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpRemUnknownTLVTable (UINT4 u4LldpRemTimeMark,
                                       UINT4 *pu4NextLldpRemTimeMark,
                                       INT4 i4LldpRemLocalPortNum,
                                       INT4 *pi4NextLldpRemLocalPortNum,
                                       INT4 i4LldpRemIndex,
                                       INT4 *pi4NextLldpRemIndex,
                                       INT4 i4LldpRemUnknownTLVType,
                                       INT4 *pi4NextLldpRemUnknownTLVType)
{
    UINT4               u4NextDestIndex = 0;

    if (LldpUtlGetNextIndexRemUnknownTLVTable
        (u4LldpRemTimeMark, pu4NextLldpRemTimeMark, i4LldpRemLocalPortNum,
         pi4NextLldpRemLocalPortNum,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum), &u4NextDestIndex,
         i4LldpRemIndex, pi4NextLldpRemIndex, i4LldpRemUnknownTLVType,
         pi4NextLldpRemUnknownTLVType) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexRemUnknownTLVTable returns failure!!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpRemUnknownTLVInfo
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpRemUnknownTLVType

                The Object 
                retValLldpRemUnknownTLVInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpRemUnknownTLVInfo (UINT4 u4LldpRemTimeMark,
                             INT4 i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                             INT4 i4LldpRemUnknownTLVType,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValLldpRemUnknownTLVInfo)
{
    if (LldpUtlGetRemUnknownTLVInfo
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         i4LldpRemUnknownTLVType, pRetValLldpRemUnknownTLVInfo,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Function LldpUtlGetRemUnknownTLVInfo return FAILURE !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpRemOrgDefInfoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpRemOrgDefInfoTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpRemOrgDefInfoOUI
                LldpRemOrgDefInfoSubtype
                LldpRemOrgDefInfoIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpRemOrgDefInfoTable (UINT4 u4LldpRemTimeMark,
                                                INT4 i4LldpRemLocalPortNum,
                                                INT4 i4LldpRemIndex,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pLldpRemOrgDefInfoOUI,
                                                INT4 i4LldpRemOrgDefInfoSubtype,
                                                INT4 i4LldpRemOrgDefInfoIndex)
{
    if (LldpUtlValidateIndexInstanceRemOrgDefInfoTable (u4LldpRemTimeMark,
                                                        i4LldpRemLocalPortNum,
                                                        i4LldpRemIndex,
                                                        pLldpRemOrgDefInfoOUI,
                                                        i4LldpRemOrgDefInfoSubtype,
                                                        i4LldpRemOrgDefInfoIndex,
                                                        LLDP_V1_DEST_MAC_ADDR_INDEX
                                                        (i4LldpRemLocalPortNum))
        == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Function LldpUtlValidateIndexInstanceRemOrgDefInfoTable return FAILURE !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpRemOrgDefInfoTable
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpRemOrgDefInfoOUI
                LldpRemOrgDefInfoSubtype
                LldpRemOrgDefInfoIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpRemOrgDefInfoTable (UINT4 *pu4LldpRemTimeMark,
                                        INT4 *pi4LldpRemLocalPortNum,
                                        INT4 *pi4LldpRemIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pLldpRemOrgDefInfoOUI,
                                        INT4 *pi4LldpRemOrgDefInfoSubtype,
                                        INT4 *pi4LldpRemOrgDefInfoIndex)
{
    if (LldpUtlGetFirstIndexLldpRemOrgDefInfoTable
        (pu4LldpRemTimeMark, pi4LldpRemLocalPortNum, pi4LldpRemIndex,
         pLldpRemOrgDefInfoOUI, pi4LldpRemOrgDefInfoSubtype,
         pi4LldpRemOrgDefInfoIndex) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetFirstIndexLldpRemOrgDefInfoTable returns Failure!!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpRemOrgDefInfoTable
 Input       :  The Indices
                LldpRemTimeMark
                nextLldpRemTimeMark
                LldpRemLocalPortNum
                nextLldpRemLocalPortNum
                LldpRemIndex
                nextLldpRemIndex
                LldpRemOrgDefInfoOUI
                nextLldpRemOrgDefInfoOUI
                LldpRemOrgDefInfoSubtype
                nextLldpRemOrgDefInfoSubtype
                LldpRemOrgDefInfoIndex
                nextLldpRemOrgDefInfoIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpRemOrgDefInfoTable (UINT4 u4LldpRemTimeMark,
                                       UINT4 *pu4NextLldpRemTimeMark,
                                       INT4 i4LldpRemLocalPortNum,
                                       INT4 *pi4NextLldpRemLocalPortNum,
                                       INT4 i4LldpRemIndex,
                                       INT4 *pi4NextLldpRemIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pLldpRemOrgDefInfoOUI,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextLldpRemOrgDefInfoOUI,
                                       INT4 i4LldpRemOrgDefInfoSubtype,
                                       INT4 *pi4NextLldpRemOrgDefInfoSubtype,
                                       INT4 i4LldpRemOrgDefInfoIndex,
                                       INT4 *pi4NextLldpRemOrgDefInfoIndex)
{
    UINT4               u4NextDestIndex = 0;

    if (LldpUtlGetNextIndexRemOrgDefInfoTable (u4LldpRemTimeMark,
                                               pu4NextLldpRemTimeMark,
                                               i4LldpRemLocalPortNum,
                                               pi4NextLldpRemLocalPortNum,
                                               LLDP_V1_DEST_MAC_ADDR_INDEX
                                               (i4LldpRemLocalPortNum),
                                               &u4NextDestIndex, i4LldpRemIndex,
                                               pi4NextLldpRemIndex,
                                               pLldpRemOrgDefInfoOUI,
                                               pNextLldpRemOrgDefInfoOUI,
                                               i4LldpRemOrgDefInfoSubtype,
                                               pi4NextLldpRemOrgDefInfoSubtype,
                                               i4LldpRemOrgDefInfoIndex,
                                               pi4NextLldpRemOrgDefInfoIndex) ==
        OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: LldpUtlGetNextIndexRemOrgDefInfoTable returns Failure!!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpRemOrgDefInfo
 Input       :  The Indices
                LldpRemTimeMark
                LldpRemLocalPortNum
                LldpRemIndex
                LldpRemOrgDefInfoOUI
                LldpRemOrgDefInfoSubtype
                LldpRemOrgDefInfoIndex

                The Object 
                retValLldpRemOrgDefInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpRemOrgDefInfo (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                         INT4 i4LldpRemIndex,
                         tSNMP_OCTET_STRING_TYPE * pLldpRemOrgDefInfoOUI,
                         INT4 i4LldpRemOrgDefInfoSubtype,
                         INT4 i4LldpRemOrgDefInfoIndex,
                         tSNMP_OCTET_STRING_TYPE * pRetValLldpRemOrgDefInfo)
{
    if (LldpUtlGetRemOrgDefInfo
        (u4LldpRemTimeMark, i4LldpRemLocalPortNum, i4LldpRemIndex,
         pLldpRemOrgDefInfoOUI, i4LldpRemOrgDefInfoSubtype,
         i4LldpRemOrgDefInfoIndex, pRetValLldpRemOrgDefInfo,
         LLDP_V1_DEST_MAC_ADDR_INDEX (i4LldpRemLocalPortNum)) == OSIX_FAILURE)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  " SNMPSTD: Function LldpUtlGetRemSysCapSupported return FAILURE !!\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpMessageTxInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpMessageTxInterval (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2LldpMessageTxHoldMultiplier
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpMessageTxHoldMultiplier (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2LldpReinitDelay
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpReinitDelay (UINT4 *pu4ErrorCode,
                         tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2LldpTxDelay
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpTxDelay (UINT4 *pu4ErrorCode,
                     tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2LldpNotificationInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpNotificationInterval (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpPortConfigTable
 Input       :  The Indices
                LldpPortConfigPortNum
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpPortConfigTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2LldpConfigManAddrTable
 Input       :  The Indices
                LldpLocManAddrSubtype
                LldpLocManAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpConfigManAddrTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
