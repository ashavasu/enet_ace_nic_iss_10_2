/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldo1tlv.c,v 1.27 2017/10/11 13:42:02 siva Exp $
 *
 * Description: This File contains all the LLDP 802.1 TLV related Parsing,
 *              Validation and remote MIB updation routines.
 *             
 *****************************************************************************/
#ifndef _LLDP_DOT1TLV_C_
#define _LLDP_DOT1TLV_C_
#include "lldinc.h"

/**************************************************************************
 *      --------------------------------------------------
 *      | OUI |SUBTYPE | Tlv SubType Specific Information
 *      --------------------------------------------------
 *                     ^pu1TlvInfo
 *************************************************************************/
/****************************************************************************
 *
 *    FUNCTION NAME    : LldpDot1TlvProcOrgSpecTlv
 *
 *    DESCRIPTION      : This function processes the IEEE 802.1  
 *                       Oganizationally Specific TLV in the received frame.
 *                       Decode the Information from the TLV Information 
 *                       field if required and then Validate the information.
 *
 *    INPUT            : pLocPortInfo - Pointer to the port info structure
 *                       pu1Tlv - Pointer to the TLV SubType Specific 
 *                                Information 
 *                       u2TlvLength - Length of the Information Field
 *                       u1TlvSubType - TLV SubType
 *
 *   OUTPUT           :  pbTlvDiscardFlag - Determines whether to discard the 
 *                                          TLV or not
 *
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpDot1TlvProcOrgSpecTlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1TlvInfo,
                           UINT2 u2TlvLength, UINT1 u1TlvSubType,
                           BOOL1 * pbTlvDiscardFlag)
{
    INT4                i4RetVal = OSIX_FAILURE;
    UINT4               u4DupTlvCount = 0;
    UINT1               u1AggStatus = 0;
    UINT1               u1RevAggStatus = 0;
    BOOL1               bResult = OSIX_FALSE;
    tSNMP_OCTET_STRING_TYPE InOctetStr;
    tSNMP_OCTET_STRING_TYPE OutOctetStr;

    if (LLDP_IS_UNRECOG_ORGDEF_TLV (u1TlvSubType) == OSIX_TRUE)
    {
        if ((u2TlvLength - (LLDP_MAX_LEN_OUI + LLDP_TLV_SUBTYPE_LEN)) >=
            LLDP_MAX_LEN_ORGDEF_INFO)
        {
            LLDP_TRC (ALL_FAILURE_TRC,
                      "\tUnrecognised Org spec. TLV Length is Too Big\r\n");
            return OSIX_FAILURE;
        }

        /* Unrecognized subtype, statsTLVsUnrecognizedTotal counter shall
         * be incremented and the TLV is assumed to be validated */
        if ((u1TlvSubType < 9) || (u1TlvSubType > 15))
        {
            LLDP_INCR_CNTR_RX_TLVS_UNRECOGNIZED (pLocPortInfo);
            LLDP_TRC (CONTROL_PLANE_TRC, "Received Dot1 Unrecognized TLV!!!\n");
        }
        return OSIX_SUCCESS;
    }

    switch (u1TlvSubType)
    {
        case LLDP_PORT_VLAN_ID_TLV:
            if (u2TlvLength < LLDP_PVID_TLV_INFO_LEN)
            {
                LLDP_TRC (LLDP_PORT_VLAN_TRC, "Discarding"
                          " LLDPDU as obtained PVID TLV length is less than"
                          " actual len\n");
                /* Return failure as the location of the next TLV is 
                 * indeterminate */
                return OSIX_FAILURE;
            }

            if (LLDP_IS_SET_TLV_RCVD_BMP (pLocPortInfo,
                                          LLDP_TLV_RCVD_PVID_BMP) == OSIX_TRUE)
            {
                /* PDU should not contain more than one PVID TLV. Hence discard 
                 * the TLV */
                LLDP_TRC (LLDP_PORT_VLAN_TRC, "Received"
                          " more than one PVID TLV!!\n");
                *pbTlvDiscardFlag = OSIX_TRUE;
                return OSIX_SUCCESS;
            }

            LLDP_SET_TLV_RCVD_BMP (pLocPortInfo, LLDP_TLV_RCVD_PVID_BMP);
            LLDP_TRC (LLDP_PORT_VLAN_TRC, "Validated PORT_VLAN_ID_TLV\n");
            i4RetVal = OSIX_SUCCESS;
            break;
        case LLDP_PROTO_VLAN_ID_TLV:
            i4RetVal = LldpDot1TlvProcProtoVlanIdTlv (pu1TlvInfo, u2TlvLength,
                                                      pbTlvDiscardFlag);
            break;
        case LLDP_VLAN_NAME_TLV:
            i4RetVal = LldpDot1TlvProcVlanNameTlv (pu1TlvInfo, u2TlvLength);
            /*If the received frame is a REFRESH frame, then we have to check
             * whether the received vlan name, vlan id combination is already
             * existing for the given remote node; if exists, then we have to
             * discard that TLV and increment the error counters; If the received
             * frame is a NEW frame, then the above check is done at the time of
             * remote table updation*/
            if (OSIX_SUCCESS == i4RetVal)
            {
                OSIX_BITLIST_IS_BIT_SET (pLocPortInfo->au1DupTlvChkFlag,
                                         (UINT2) LLDP_DUP_VLAN_NAME_TLV,
                                         (INT4) LLDP_MAX_DUP_TLV_ARRAY_SIZE,
                                         bResult);
                if ((LLDP_RX_FRAME_REFRESH == pLocPortInfo->u1RxFrameType) &&
                    (OSIX_FALSE == bResult))
                {
                    if (OSIX_SUCCESS == (LldpUtlGetDupVlanNameCount
                                         (pLocPortInfo->pu1RxLldpdu,
                                          pLocPortInfo->u2RxPduLen,
                                          &u4DupTlvCount)))
                    {
                        while (u4DupTlvCount > 0)
                        {
                            LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
                            if (OSIX_FALSE ==
                                pLocPortInfo->bstatsFramesInerrorsTotal)
                            {
                                LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
                                pLocPortInfo->bstatsFramesInerrorsTotal =
                                    OSIX_TRUE;
                            }
                            u4DupTlvCount--;
                        }
                        OSIX_BITLIST_SET_BIT (pLocPortInfo->au1DupTlvChkFlag,
                                              (UINT2) LLDP_DUP_VLAN_NAME_TLV,
                                              (INT4)
                                              LLDP_MAX_DUP_TLV_ARRAY_SIZE);
                    }
                }
            }
            else
            {
                *pbTlvDiscardFlag = OSIX_TRUE;
                return OSIX_SUCCESS;
            }

            break;
        case LLDP_PROTO_ID_TLV:
            /* TODO: Not supported in this release. Hence discard the tlv */
            i4RetVal = OSIX_SUCCESS;
            break;
        case LLDP_VID_USAGE_DIGEST_TLV:
            i4RetVal =
                LldpDot1TlvProcVidUsageDigestTlv (pu1TlvInfo, u2TlvLength);
            if (OSIX_SUCCESS == i4RetVal)
            {
                OSIX_BITLIST_IS_BIT_SET (pLocPortInfo->au1DupTlvChkFlag,
                                         (UINT2) LLDP_DUP_VID_DIGEST_TLV,
                                         (INT4) LLDP_MAX_DUP_TLV_ARRAY_SIZE,
                                         bResult);

                if (OSIX_FALSE == bResult)
                {
                    if (OSIX_SUCCESS == (LldpUtlGetVidUsageDigestTlvCnt
                                         (pLocPortInfo->pu1RxLldpdu,
                                          pLocPortInfo->u2RxPduLen,
                                          &u4DupTlvCount)))
                    {
                        /* In an LLDPDU if more than one VID Usage Digest TLV
                         * is present, then only the last TLV will be updated
                         * in the remote data base; So incrementing the TLV
                         * discarded count for (n-1) times if there are 'n'
                         * number of TLVs of such type.*/
                        while ((u4DupTlvCount - 1) > (UINT4) 0)
                        {
                            if (OSIX_FALSE ==
                                pLocPortInfo->bstatsFramesInerrorsTotal)
                            {
                                LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
                                pLocPortInfo->bstatsFramesInerrorsTotal =
                                    OSIX_TRUE;
                            }
                            LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
                            u4DupTlvCount--;
                        }
                    }
                    OSIX_BITLIST_SET_BIT (pLocPortInfo->au1DupTlvChkFlag,
                                          (UINT2) LLDP_DUP_VID_DIGEST_TLV,
                                          (INT4) LLDP_MAX_DUP_TLV_ARRAY_SIZE);
                }
            }
            break;
        case LLDP_MGMT_VID_TLV:
            i4RetVal = LldpDot1TlvProcMgmtVidTlv (pu1TlvInfo, u2TlvLength);
            if (OSIX_SUCCESS == i4RetVal)
            {
                OSIX_BITLIST_IS_BIT_SET (pLocPortInfo->au1DupTlvChkFlag,
                                         (UINT2) LLDP_DUP_MGMT_VID_TLV,
                                         (INT4) LLDP_MAX_DUP_TLV_ARRAY_SIZE,
                                         bResult);

                if (OSIX_FALSE == bResult)
                {
                    if (OSIX_SUCCESS == (LldpUtlGetMgmtVidTlvCnt
                                         (pLocPortInfo->pu1RxLldpdu,
                                          pLocPortInfo->u2RxPduLen,
                                          &u4DupTlvCount)))
                    {
                        /* In an LLDPDU if more than one VID Usage Digest TLV
                         * is present, then only the last TLV will be updated
                         * in the remote data base; So incrementing the TLV
                         * discarded count for (n-1) times if there is 'n' 
                         * number of TLVs of such type.*/
                        while ((u4DupTlvCount - 1) > (UINT4) 0)
                        {
                            if (OSIX_FALSE ==
                                pLocPortInfo->bstatsFramesInerrorsTotal)
                            {
                                LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
                                pLocPortInfo->bstatsFramesInerrorsTotal =
                                    OSIX_TRUE;
                            }
                            LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
                            u4DupTlvCount--;
                        }
                    }
                    OSIX_BITLIST_SET_BIT (pLocPortInfo->au1DupTlvChkFlag,
                                          (UINT2) LLDP_DUP_MGMT_VID_TLV,
                                          (INT4) LLDP_MAX_DUP_TLV_ARRAY_SIZE);
                }
            }
            break;
        case LLDP_V2_LINK_AGG_TLV:
            if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
            {
                *pbTlvDiscardFlag = OSIX_TRUE;
                i4RetVal = OSIX_SUCCESS;
                break;
            }
            if (u2TlvLength < LLDP_LINK_AGG_TLV_INFO_LEN)
            {
                LLDP_TRC (LLDP_LAGG_TRC, "Discarding"
                          " LLDPDU as obtained Link Agg TLV length is less than"
                          " actual len\n");
                /* Return failure as the location of the next TLV is 
                 * indeterminate */
                return OSIX_FAILURE;
            }
            if (LLDP_IS_SET_TLV_RCVD_BMP (pLocPortInfo,
                                          LLDP_TLV_RCVD_LINK_AGG_BMP)
                == OSIX_TRUE)
            {
                /* PDU should not contain more than one Link Agg TLV. Hence 
                 * discard the TLV */
                LLDP_TRC (LLDP_LAGG_TRC, "Received"
                          " more than one Link Agg TLV!!\n");
                *pbTlvDiscardFlag = OSIX_TRUE;
                return OSIX_SUCCESS;
            }

            /* If the received TLV indicates that link aggregation capability
             * is not supported but the status is enabled, then the TLV msut be
             * discarded */
            LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, 0, u1AggStatus);

            /* Decode the Tlv Information */
            InOctetStr.pu1_OctetList = &u1AggStatus;
            InOctetStr.i4_Length = sizeof (UINT1);
            OutOctetStr.pu1_OctetList = &u1RevAggStatus;
            OutOctetStr.i4_Length = sizeof (UINT1);

            /* Convert the Bit list to pkt string as per 
             * Std IEEE802.1AB-2005 section 9.1 */
            SNMP_ReverseOctetString (&InOctetStr, &OutOctetStr);

            if (!(OutOctetStr.pu1_OctetList[0] & LLDP_AGG_CAP_BMAP) &&
                (OutOctetStr.pu1_OctetList[0] & LLDP_AGG_STATUS_BMAP))
            {
                *pbTlvDiscardFlag = OSIX_TRUE;
                LLDP_TRC (LLDP_LAGG_TRC, "Discarding"
                          " Link Agg TLV since aggregation capability is not"
                          " supported and status is enabled!!\n");
            }

            LLDP_SET_TLV_RCVD_BMP (pLocPortInfo, LLDP_TLV_RCVD_LINK_AGG_BMP);
            LLDP_TRC (LLDP_LINK_AGG_TLV, "Validated" " LINK_AGG_TLV\n");
            i4RetVal = OSIX_SUCCESS;
            break;
            /* if the tlvsubtype doesn't match any of the above cases
             * then the TLV is considered as unrecognized TLV and the same
             * is handled above(LLDP_IS_UNRECOG_ORGDEF_TLV). Hence default
             * case is not required */
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpDot1TlvProcProtoVlanIdTlv
 *
 *    DESCRIPTION      : This function processes the Port and Protocol Vlan Id
 *                       Tlv in the received frame.
 *                       Decode the Information from the TLV Information 
 *                       field  and then validate the information. Set the 
 *                       flag *pbTlvDiscardFlag to true if the TLV is to be 
 *                       discarded.
 *
 *    INPUT            : pu1Tlv - Pointer to the TLV SubType Specific 
 *                                Information 
 *                       u2TlvLength - Length of the Information Field
 * 
 *    OUTPUT           : pbTlvDiscardFlag - Determines whether to discard the
 *                                          TLV or not
 *
 *    RETURNS          : OSIX_SUCCESS / 
 *                       OSIX_FAILURE - if the PDU is to be discarded
 *
 ****************************************************************************/

INT4
LldpDot1TlvProcProtoVlanIdTlv (UINT1 *pu1TlvInfo, UINT2 u2TlvLength,
                               BOOL1 * pbTlvDiscardFlag)
{
    tLldpxdot1RemProtoVlanInfo ProtoVlanInfo;

    MEMSET (&ProtoVlanInfo, 0, sizeof (tLldpxdot1RemProtoVlanInfo));

    /* Length validation */
    if (u2TlvLength < LLDP_PPVID_TLV_INFO_LEN)
    {
        LLDP_TRC (LLDP_PPVLAN_TRC, "Discarding"
                  " LLDPDU as obtained PPVID TLV length is less than"
                  " actual len\n");
        /* Return failure as the location of the next TLV is indeterminate */
        return OSIX_FAILURE;
    }

    /* Decode the Tlv Information */
    LldpDot1TlvDecodeProtoVlanInfo (pu1TlvInfo, &ProtoVlanInfo);

    if (((ProtoVlanInfo.u1ProtoVlanSupported == LLDP_FALSE) &&
         (ProtoVlanInfo.u1ProtoVlanEnabled == LLDP_TRUE)) ||
        (ProtoVlanInfo.u2ProtoVlanId > LLDP_MAX_VLAN_ID))

    {
        /* TLV Error : Discard the TLV not the LLDPDU. Return success as the 
         * PDU  should not be discarded */
        *pbTlvDiscardFlag = OSIX_TRUE;
        LLDP_TRC (LLDP_PPVLAN_TRC, "Discarding PPVID_TLV\n");
    }
    LLDP_TRC (LLDP_PPVLAN_TRC, "Validated PPVID_TLV\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpDot1TlvProcVlanNameTlv
 *
 *    DESCRIPTION      : This function processes the Vlan Name Tlv in the
 *                       received frame.
 *                       Decode the Information from the TLV Information 
 *                       field  and then Validate the information.
 *
 *    INPUT            : pu1TlvInfo - Pointer to the TLV SubType Specific 
 *                                    Information 
 *                       u2TlvLength - Length of the Information Field
 * 
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / 
 *                       OSIX_FAILURE - if the PDU is to be discarded
 *
 ****************************************************************************/
INT4
LldpDot1TlvProcVlanNameTlv (UINT1 *pu1TlvInfo, UINT2 u2TlvLength)
{
    tLldpxdot1RemVlanNameInfo VlanNameInfo;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT1              *pu1TmpTlvInfo = NULL;
    UINT1               u1VlanNameLen = 0;
    UINT2               u2TlvInfoLen = 0;

    MEMSET (&VlanNameInfo, 0, sizeof (tLldpxdot1RemVlanNameInfo));
    pu1TmpTlvInfo = pu1TlvInfo;

    /* Length validation */
    if ((u2TlvLength < LLDP_MIN_VLAN_NAME_INFO_LEN) ||
        (u2TlvLength > LLDP_MAX_VLAN_NAME_INFO_LEN))
    {
        LLDP_TRC (LLDP_VLAN_NAME_TRC, "Discarding"
                  " LLDPDU as obtained Vlan Name TLV length is not within"
                  " the valid range(8-39) \n");
        /* Return failure as the location of the next TLV is indeterminate */
        /* Discard LLDPDU */
        return OSIX_FAILURE;
    }
    LLDP_LBUF_GET_2_BYTE (pu1TmpTlvInfo, 0, VlanNameInfo.u2VlanId);
    LLDP_LBUF_GET_1_BYTE (pu1TmpTlvInfo, 0, u1VlanNameLen);

    if (u1VlanNameLen > VLAN_STATIC_MAX_NAME_LEN)
    {
        /* Discard LLDPDU */
        return OSIX_FAILURE;
    }
    /* Decode the Tlv Information */
    LldpDot1TlvDecodeVlanNameInfo (pu1TlvInfo, &VlanNameInfo);

    u2TlvInfoLen = (UINT2) ((LLDP_MAX_LEN_OUI + LLDP_TLV_SUBTYPE_LEN +
                             LLDP_TLV_VLAN_ID_LEN +
                             LLDP_TLV_VLAN_NAME_LEN_FEILD +
                             LLDP_STRLEN (VlanNameInfo.au1VlanName,
                                          VLAN_STATIC_MAX_NAME_LEN)));

    /* Length validation */
    if (u2TlvLength < u2TlvInfoLen)
    {
        LLDP_TRC (LLDP_VLAN_NAME_TRC, "Discarding"
                  " LLDPDU as obtained Vlan Name TLV length is less than"
                  " actual len\n");

        /* Return failure as the location of the next TLV is indeterminate */
        i4RetVal = OSIX_FAILURE;
    }
    LLDP_TRC (LLDP_PORT_VLAN_TRC, "Validated VLAN_NAME_TLV\n");
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpDot1TlvProcVidUsageDigestTlv
 *
 *    DESCRIPTION      : This function processes the Vid Usage Digest Tlv in the
 *                       received frame.
 *                       Decode the Information from the TLV Information 
 *                       field  and then Validate the information.
 *
 *    INPUT            : pu1TlvInfo - Pointer to the TLV SubType Specific 
 *                                    Information 
 *                       u2TlvLength - Length of the Information Field
 * 
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / 
 *                       OSIX_FAILURE - if the PDU is to be discarded
 *
 ****************************************************************************/
INT4
LldpDot1TlvProcVidUsageDigestTlv (UINT1 *pu1TlvInfo, UINT2 u2TlvLength)
{
    UNUSED_PARAM (pu1TlvInfo);

    /* Length validation */
    if (u2TlvLength != LLDP_VID_USAGE_DIGEST_STRLEN)
    {
        LLDP_TRC (LLDP_VLAN_NAME_TRC, "Discarding"
                  " LLDPDU as obtained VidUsageDigest TLV length is not valid\r\n");
        /* Return failure as the location of the next TLV is indeterminate */
        /* Discard LLDPDU */
        return OSIX_FAILURE;
    }

    LLDP_TRC (LLDP_PORT_VLAN_TRC, "Validated VID_USAGE_DIGEST_TLV\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpDot1TlvProcMgmtVidTlv
 *
 *    DESCRIPTION      : This function processes the Mgmt Vid Tlv in the
 *                       received frame.
 *                       Decode the Information from the TLV Information 
 *                       field  and then Validate the information.
 *
 *    INPUT            : pu1TlvInfo - Pointer to the TLV SubType Specific 
 *                                    Information 
 *                       u2TlvLength - Length of the Information Field
 * 
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / 
 *                       OSIX_FAILURE - if the PDU is to be discarded
 *
 ****************************************************************************/
INT4
LldpDot1TlvProcMgmtVidTlv (UINT1 *pu1TlvInfo, UINT2 u2TlvLength)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT1              *pu1TmpTlvInfo = NULL;
    UINT2               u2MgmtVid = 0;

    pu1TmpTlvInfo = pu1TlvInfo;

    /* Length validation */
    if (u2TlvLength != LLDP_MGMT_VID_STRLEN)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "Discarding"
                  " LLDPDU as obtained MgmtVid TLV length is not valid\r\n");
        /* Return failure as the location of the next TLV is indeterminate */
        /* Discard LLDPDU */
        return OSIX_FAILURE;
    }
    LLDP_LBUF_GET_2_BYTE (pu1TmpTlvInfo, 0, u2MgmtVid);

    if ((u2MgmtVid > LLDP_MAX_VLAN_ID))
    {
        LLDP_TRC (ALL_FAILURE_TRC, "Discarding"
                  " LLDPDU as obtained MgmtVid is not valid range \r\n");

        return OSIX_FAILURE;

    }

    LLDP_TRC (LLDP_PORT_VLAN_TRC, "Validated MGMT_VID_TLV\n");
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpDot1TlvProcProtocolIdTlv
 *
 *    DESCRIPTION      : This function processes the Protocol Id Tlv in the
 *                       received frame.
 *                       Decode the Information from the TLV Information 
 *                       field  and then Validate the information. Also notify 
 *                       if there is any protocol misconfiguration errors.
 *
 *    INPUT            : pLocPortInfo - Port Info Structure
 *                       pu1Tlv - Pointer to the TLV SubType Specific 
 *                       Information 
 *                       u2TlvLength - Length of the Information Field
 *                       u1TlvSubType - TLV SubType
 *                       pbTlvDiscardFlag - Determines whether to discard the 
 *                                          TLV or not

 * 
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / 
 *                       OSIX_FAILURE - if the PDU is to be discarded
 *
 ****************************************************************************/
INT4
LldpDot1TlvProcProtocolIdTlv (tLldpLocPortInfo * pLocPortInfo,
                              UINT1 *pu1TlvInfo, UINT2 u2TlvLength)
{
    /* TODO: not supported for current release */
    UNUSED_PARAM (pLocPortInfo);
    UNUSED_PARAM (pu1TlvInfo);
    UNUSED_PARAM (u2TlvLength);
    return OSIX_SUCCESS;
}

/*--------------------------------------------------------------------------*/
/*                       TLV Decode Routines                                */
/*--------------------------------------------------------------------------*/
/****************************************************************************
 *
 *    FUNCTION NAME    : LldpDot1TlvDecodeProtoVlanInfo 
 *
 *    DESCRIPTION      : This routine decodes the Port and Protocol Vlan ID 
 *                       TLV information. After decoding the information is
 *                       updated in the structure passed by the caller.
 *
 *    INPUT            : pu1TlvInfo - Points to the flag Field in the TLV
 *                      
 *    OUTPUT           : pProtoVlanInfo - Decoded Value of TLV Information is 
 *                                     updated in this structure passed by the
 *                                     caller.
 *
 *    RETURNS          : None 
 *
 ****************************************************************************/
VOID
LldpDot1TlvDecodeProtoVlanInfo (UINT1 *pu1TlvInfo,
                                tLldpxdot1RemProtoVlanInfo * pProtoVlanInfo)
{
    UINT1               u1ProtoVlanFlag = 0;

    LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, 0, u1ProtoVlanFlag);
    /* Bit 1 represents Protocol Vlan support and bit 2 represents Protocol
     * Vlan status */
    if (u1ProtoVlanFlag & LLDP_PROTO_VLAN_SUPP_BMAP)
    {
        pProtoVlanInfo->u1ProtoVlanSupported = LLDP_TRUE;
    }
    else
    {
        pProtoVlanInfo->u1ProtoVlanSupported = LLDP_FALSE;
    }

    if (u1ProtoVlanFlag & LLDP_PROTO_VLAN_STAT_BMAP)
    {
        pProtoVlanInfo->u1ProtoVlanEnabled = LLDP_TRUE;
    }
    else
    {
        pProtoVlanInfo->u1ProtoVlanEnabled = LLDP_FALSE;
    }

    LLDP_LBUF_GET_2_BYTE (pu1TlvInfo, 0, pProtoVlanInfo->u2ProtoVlanId);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpDot1TlvDecodeVlanNameInfo 
 *
 *    DESCRIPTION      : This routine decodes the Vlan Name TLV information.
 *                       After decoding the information is updated in the 
 *                       structure passed by the caller.
 *
 *    INPUT            : pu1TlvInfo - Points to the flag Field in the TLV
 *                      
 *    OUTPUT           : pVlanNameInfo - Decoded Value of TLV Information is 
 *                                     updated in this structure passed by the
 *                                     caller.
 *
 *    RETURNS          : None 
 *
 ****************************************************************************/
VOID
LldpDot1TlvDecodeVlanNameInfo (UINT1 *pu1TlvInfo,
                               tLldpxdot1RemVlanNameInfo * pVlanNameInfo)
{

    LLDP_LBUF_GET_2_BYTE (pu1TlvInfo, 0, pVlanNameInfo->u2VlanId);
    LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, 0, pVlanNameInfo->u1VlanNameLen);
    LLDP_LBUF_GET_STRING (pu1TlvInfo, pVlanNameInfo->au1VlanName, 0,
                          MEM_MAX_BYTES (pVlanNameInfo->u1VlanNameLen,
                                         VLAN_STATIC_MAX_NAME_LEN));
}

/*--------------------------------------------------------------------------*/
/*                       TLV Update Routines                                */
/*--------------------------------------------------------------------------*/

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpDot1TlvUpdtOrgSpecTlv
 *
 *    DESCRIPTION      : This function updates the IEEE 802.1  
 *                       Oganizationally Specific TLV in the received frame.
 *
 *    INPUT            : pLocPortInfo - Port Info Structure
 *                       pu1TlvInfo - Pointer to the TLV SubType Specific 
 *                       Information 
 *                       u2TlvLength - Length of the Information Field
 *                       u1TlvSubType - TLV SubType
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpDot1TlvUpdtOrgSpecTlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1TlvInfo,
                           UINT2 u2TlvLength, UINT1 u1TlvSubType)
{
    INT4                i4RetVal = OSIX_FAILURE;
    tLldpRemoteNode    *pRemoteNode = NULL;
    if (LLDP_IS_UNRECOG_ORGDEF_TLV (u1TlvSubType) == OSIX_TRUE)
    {
        i4RetVal = LldpTlvUpdtUnrecogOrgDefTlv (pLocPortInfo, pu1TlvInfo,
                                                u2TlvLength,
                                                gau1LldpDot1OUI, u1TlvSubType);
        return i4RetVal;
    }

    switch (u1TlvSubType)
    {
        case LLDP_PORT_VLAN_ID_TLV:
            i4RetVal = LldpDot1TlvUpdtPortVlanIdTlv (pLocPortInfo, pu1TlvInfo);
            break;
        case LLDP_PROTO_VLAN_ID_TLV:
            i4RetVal = LldpDot1TlvUpdtProtoVlanIdTlv (pLocPortInfo, pu1TlvInfo);
            break;
        case LLDP_VLAN_NAME_TLV:
            i4RetVal = LldpDot1TlvUpdtVlanNameTlv (pLocPortInfo, pu1TlvInfo);
            break;
        case LLDP_PROTO_ID_TLV:
            /* TODO: Protocol Id is not supported in this release */
            i4RetVal = OSIX_SUCCESS;
            break;
        case LLDP_VID_USAGE_DIGEST_TLV:
            i4RetVal =
                LldpDot1TlvUpdtVidUsageDigestTlv (pLocPortInfo, pu1TlvInfo);
            break;
        case LLDP_MGMT_VID_TLV:
            i4RetVal = LldpDot1TlvUpdtMgmtVidTlv (pLocPortInfo, pu1TlvInfo);
            break;
        case LLDP_V2_LINK_AGG_TLV:
            if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
            {
                break;
            }
            /* Get the Remote Node */
            pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;
            pRemoteNode->bRcvdLinkAggTlv = OSIX_TRUE;
            if (pRemoteNode->pDot3RemPortInfo == NULL)
            {
                /* Dot3 Specific TLVs received for the first time. Hence allocate
                 * memory*/
                if ((pRemoteNode->pDot3RemPortInfo =
                     (tLldpxdot3RemPortInfo *)
                     (MemAllocMemBlk (gLldpGlobalInfo.RemDot3PortInfoPoolId)))
                    == NULL)
                {
                    LLDP_TRC (ALL_FAILURE_TRC | LLDP_CRITICAL_TRC,
                              "LldpDot3TlvUpdtOrgSpecTlv: Mempool Allocation "
                              "Failure for New RemManAddr Node\r\n");
#ifndef FM_WANTED
                    SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL,
                                  gLldpGlobalInfo.u4SysLogId,
                                  "LldpDot3TlvUpdtOrgSpecTlv: Mempool Allocation"
                                  "Failure for New RemManAddr Node"));
#endif
                    LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
                    return OSIX_FAILURE;
                }
                MEMSET (pRemoteNode->pDot3RemPortInfo, 0,
                        sizeof (tLldpxdot3RemPortInfo));
            }
            i4RetVal = LldpDot3TlvUpdtLinkAggTlv (pLocPortInfo,
                                                  pRemoteNode->pDot3RemPortInfo,
                                                  pu1TlvInfo);
            break;
            /* if the tlvsubtype doesn't match any of the above cases
             * then the TLV is considered as unrecognized TLV and the same
             * is handled above(LLDP_IS_UNRECOG_ORGDEF_TLV). Hence default
             * case is not required */
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpDot1TlvUpdtPortVlanIdTlv
 *
 *    DESCRIPTION      : This routine decodes the Port Vlan ID TLV information.
 *                       After decoding, the information is updated in
 *                       the Remote Node. Also send a misconfiguration
 *                       notification if the received PVID does not match with
 *                       the received port's PVID.
 *                        
 *
 *    INPUT            : pLocPortInfo - Local Port Info structure where the 
 *                                      TLV is received
 *                       pu1Tlv- Points to the TLV Subtype information field
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpDot1TlvUpdtPortVlanIdTlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    tLldMisConfigTrap   MisConfigInfo;
    UINT2               u2ChassisIdLen = 0;
    UINT2               u2PortIdLen = 0;

    MEMSET (&MisConfigInfo, 0, sizeof (tLldMisConfigTrap));
    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;
    pRemoteNode->bRcvdPVidTlv = OSIX_TRUE;

    /* Decode the Tlv Information */
    /* This object is present in the Remote Node structure itself, so pass the
     * Remote Node pointer to the Decode Routine for updating the value */
    LLDP_LBUF_GET_2_BYTE (pu1Tlv, 0, pRemoteNode->u2PVId);

    LLDP_TRC_ARG2 (LLDP_PORT_VLAN_TRC, "Updating Port Vlan "
                   " ID TLV on port %d with the value: %d\n",
                   pLocPortInfo->u4LocPortNum, pRemoteNode->u2PVId);

    /* If the sending system does not know the PVID or does not support PVID 
     * operation, zero will be sent as the PVID value.*/
    if (pRemoteNode->u2PVId != pLocPortInfo->u2PVid)
    {
        /* Get the length of Chassis Id */
        LldpUtilGetChassisIdLen (pRemoteNode->i4RemChassisIdSubtype,
                                 pRemoteNode->au1RemChassisId, &u2ChassisIdLen);

        /* Get the length of port Id */
        LldpUtilGetPortIdLen (pRemoteNode->i4RemPortIdSubtype,
                              pRemoteNode->au1RemPortId, &u2PortIdLen);

        MisConfigInfo.i4RemChassisIdSubtype =
            pRemoteNode->i4RemChassisIdSubtype;
        MisConfigInfo.i4RemPortIdSubtype = pRemoteNode->i4RemPortIdSubtype;
        MisConfigInfo.u4RemTimeMark = pRemoteNode->u4RemLastUpdateTime;
        MisConfigInfo.i4RemLocalPortNum = pRemoteNode->i4RemLocalPortNum;
        MisConfigInfo.i4RemIndex = pRemoteNode->i4RemIndex;
        MEMCPY (&MisConfigInfo.au1RemChassisId[0], pRemoteNode->au1RemChassisId,
                MEM_MAX_BYTES (u2ChassisIdLen, LLDP_MAX_LEN_CHASSISID));
        MEMCPY (&MisConfigInfo.au1PortId[0], pRemoteNode->au1RemPortId,
                MEM_MAX_BYTES (u2PortIdLen, LLDP_MAX_LEN_PORTID));
        MisConfigInfo.MisPortVlanId = pRemoteNode->u2PVId;
        /* Send Misconfiguration Notification */
        LldpSendNotification ((VOID *) &MisConfigInfo,
                              LLDP_MIS_CONFIG_PORT_VID);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpDot1TlvUpdtProtoVlanIdTlv
 *
 *    DESCRIPTION      : This routine decodes the Protocol Vlan ID TLV 
 *                       information. After decoding, the information
 *                       is updated in the Remote Node. Also notify
 *                       if there is any protocol misconfiguration errors.
 *                        
 *
 *    INPUT            : pu1Tlv- Points to the TLV Subtype information field
 *                       pLocPortInfo - Local Port Info structure where the 
 *                                      TLV is received
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpDot1TlvUpdtProtoVlanIdTlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    tLldpxdot1RemProtoVlanInfo *pProtoVlanNode;
    UINT1               u1ProtoVlanFlag = 0;

    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;

    if ((pProtoVlanNode =
         (tLldpxdot1RemProtoVlanInfo *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemProtoVlanPoolId))) == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_CRITICAL_TRC,
                  "LldpDot1TlvUpdtProtoVlanIdTlv; Mempool Allocation "
                  " Failure for New Protocol Vlan Node\r\n");
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpDot1TlvUpdtProtoVlanIdTlv; Mempool Allocation"
                      "Failure for New Protocol Vlan Node"));
#endif
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }

    MEMSET (pProtoVlanNode, 0, sizeof (tLldpxdot1RemProtoVlanInfo));
    /* Update the common Remote Indices */
    pProtoVlanNode->u4RemLastUpdateTime = pRemoteNode->u4RemLastUpdateTime;
    pProtoVlanNode->i4RemLocalPortNum = pRemoteNode->i4RemLocalPortNum;
    pProtoVlanNode->i4RemIndex = pRemoteNode->i4RemIndex;
    pProtoVlanNode->u4DestAddrTblIndex = pRemoteNode->u4DestAddrTblIndex;

    /* Call the Decode Routine */
    LldpDot1TlvDecodeProtoVlanInfo (pu1Tlv, pProtoVlanNode);

    u1ProtoVlanFlag = (pProtoVlanNode->u1ProtoVlanSupported |
                       pProtoVlanNode->u1ProtoVlanEnabled);

    /* Insert the Node in the RemProtoVlanRBTree */
    if (RBTreeAdd (gLldpGlobalInfo.RemProtoVlanRBTree,
                   (tRBElem *) pProtoVlanNode) != RB_SUCCESS)
    {
        /* The node with given index may be existing in the RBTree. So
         * log that condition as the standard says that if more than one 
         * protocol vlan TLV is defined for a port then the PPVID value 
         * shall be different from any other PPVID
         */
        LLDP_TRC_ARG2 (ALL_FAILURE_TRC, "LLDPDU received on port %d "
                       "contains multiple Port And Protocol VLAN ID TLVs"
                       " with the same PPVID value %d\n",
                       pRemoteNode->i4RemLocalPortNum,
                       pProtoVlanNode->u2ProtoVlanId);
        MemReleaseMemBlock (gLldpGlobalInfo.RemProtoVlanPoolId,
                            (UINT1 *) pProtoVlanNode);
        LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
        if (OSIX_FALSE == pLocPortInfo->bstatsFramesInerrorsTotal)
        {
            LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
            pLocPortInfo->bstatsFramesInerrorsTotal = OSIX_TRUE;
        }
        return OSIX_SUCCESS;
    }

    LLDP_TRC_ARG3 (LLDP_PORT_VLAN_TRC, "Updating Port and"
                   " Protocol Vlan ID TLV on port %d with the the following"
                   " value\n Port and Protocol capbility/status %d\n PPVID%d\n",
                   pLocPortInfo->u4LocPortNum, u1ProtoVlanFlag,
                   pProtoVlanNode->u2ProtoVlanId);

    /* Send notification if there is any misconfiguration */
    LldpRxChkMisConfigProtoVlanId (pLocPortInfo, pProtoVlanNode);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpDot1TlvUpdtVlanNameTlv
 *
 *    DESCRIPTION      : This routine decodes the Vlan Name TLV 
 *                       information. After decoding, the information
 *                       is updated in the Remote Node. Also notify
 *                       if there is any protocol misconfiguration errors.
 *                        
 *
 *    INPUT            : pu1Tlv- Points to the TLV Subtype information field
 *                       pLocPortInfo - Local Port Info structure where the 
 *                                      TLV is received
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpDot1TlvUpdtVlanNameTlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    tLldpxdot1RemVlanNameInfo *pVlanNameNode = NULL;

    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;

    if ((pVlanNameNode =
         (tLldpxdot1RemVlanNameInfo *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemVlanNameInfoPoolId))) == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_CRITICAL_TRC,
                  "LldpDot1TlvUpdtVlanNameTlv: "
                  "Mempool Allocation Failure for New Vlan Name Node\r\n");
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpDot1TlvUpdtVlanNameTlv: Mempool Allocation Failure"
                      "for New Vlan Name Node"));
#endif
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }

    MEMSET (pVlanNameNode, 0, sizeof (tLldpxdot1RemVlanNameInfo));
    /* Update the common Remote Indices */
    pVlanNameNode->u4RemLastUpdateTime = pRemoteNode->u4RemLastUpdateTime;
    pVlanNameNode->i4RemLocalPortNum = pRemoteNode->i4RemLocalPortNum;
    pVlanNameNode->i4RemIndex = pRemoteNode->i4RemIndex;
    pVlanNameNode->u4DestAddrTblIndex = pRemoteNode->u4DestAddrTblIndex;

    /* Call the Decode Routine */
    LldpDot1TlvDecodeVlanNameInfo (pu1Tlv, pVlanNameNode);

    /* Insert the Node in the RemProtoVlanRBTree */
    if (RBTreeAdd (gLldpGlobalInfo.RemVlanNameInfoRBTree,
                   (tRBElem *) pVlanNameNode) != RB_SUCCESS)
    {
        /* The node with given index may be existing in the RBTree. So
         * log that condition as the standard says that if more than one 
         * Vlan Name TLV is defined for a port then the Vlan ID and the
         * associated Vlan name combination shall be different from any other 
         * combination defined for the port */
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LLDPDU received on port %d "
                       "contains multiple VLAN Name TLVs with the same Vlan"
                       " id and name combination\n",
                       pRemoteNode->i4RemLocalPortNum);
        MemReleaseMemBlock (gLldpGlobalInfo.RemVlanNameInfoPoolId,
                            (UINT1 *) pVlanNameNode);
        LLDP_INCR_CNTR_RX_TLVS_DISCARDED (pLocPortInfo);
        if (OSIX_FALSE == pLocPortInfo->bstatsFramesInerrorsTotal)
        {
            LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pLocPortInfo);
            pLocPortInfo->bstatsFramesInerrorsTotal = OSIX_TRUE;
        }
        return OSIX_SUCCESS;
    }

    LLDP_TRC_ARG3 (LLDP_PORT_VLAN_TRC, "Updating Vlan Name"
                   " TLV on port %d with the the following"
                   " values\n Vlan Id: %d\nVlan name: %s\n",
                   pLocPortInfo->u4LocPortNum, pVlanNameNode->u2VlanId,
                   pVlanNameNode->au1VlanName);

    LldpRxChkMisConfigVlanName (pLocPortInfo, pVlanNameNode);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpDot1TlvUpdtVidUsageDigestTlv
 *
 *    DESCRIPTION      : This routine decodes the VidUsageDigest TLV 
 *                       information. After decoding, the information
 *                       is updated in the Remote Node. Also notify
 *                       if there is any protocol misconfiguration errors.
 *                        
 *
 *    INPUT            : pu1Tlv- Points to the TLV Subtype information field
 *                       pLocPortInfo - Local Port Info structure where the 
 *                                      TLV is received
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpDot1TlvUpdtVidUsageDigestTlv (tLldpLocPortInfo * pLocPortInfo,
                                  UINT1 *pu1Tlv)
{
    tLldpRemoteNode    *pRemoteNode = NULL;

    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;

    LLDP_LBUF_GET_4_BYTE (pu1Tlv, 0, pRemoteNode->u4RemVidUsageDigest);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpDot1TlvUpdtMgmtVidTlv
 *
 *    DESCRIPTION      : This routine decodes the MgmtVid TLV 
 *                       information. After decoding, the information
 *                       is updated in the Remote Node. Also notify
 *                       if there is any protocol misconfiguration errors.
 *                        
 *
 *    INPUT            : pu1Tlv- Points to the TLV Subtype information field
 *                       pLocPortInfo - Local Port Info structure where the 
 *                                      TLV is received
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpDot1TlvUpdtMgmtVidTlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv)
{
    tLldpRemoteNode    *pRemoteNode = NULL;

    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;

    LLDP_LBUF_GET_2_BYTE (pu1Tlv, 0, pRemoteNode->u2RemMgmtVid);
    return OSIX_SUCCESS;
}
#endif /* _LLDP_DOT1TLV_C_ */
/*--------------------------------------------------------------------------*/
/*                       End of the file  lldo1tlv.c                      */
/*--------------------------------------------------------------------------*/
