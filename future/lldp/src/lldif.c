/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldif.c,v 1.45 2017/12/14 10:30:25 siva Exp $
 *
 * Description: This file contains procedures for 
 *              - creating/deleting interface structures 
 *              - handling interface up/down, interface parameter change 
 *                notifications from lower layer.  
 *********************************************************************/
#include "lldinc.h"

/* static functio prototypes */
PRIVATE INT4        PROTO (LldpIfUpdateIfInfo (tLldpLocPortInfo *));
PRIVATE INT4        PROTO (LldpIfUpdateIfAlias (tLldpLocPortInfo *));
PRIVATE INT4        PROTO (LldpIfDot3PowerInfoInit (tLldpLocPortInfo *));

#ifdef L2RED_WANTED
PRIVATE VOID        PROTO (LldpIfHandleOperDown (tLldpLocPortInfo * pPortInfo));
#endif

PRIVATE tCfaIfInfo  gCfaIfInfo;
/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpIfCreate
 *                                                                          
 *    DESCRIPTION      : This function allocates memory for the new port entry,
 *                       initializes the entry and adds it to the port table. 
 *
 *    INPUT            : u4PortIndex - Port number to be created
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
LldpIfCreate (UINT4 u4PortIndex)
{
    tLldpLocPortInfo   *pPortEntry = NULL;
    tLldpLocPortTable  *pPortInfo = NULL;
    tLldpLocPortTable   PortInfo;
    tLldpv2AgentToLocPort *pAgentMappingEntry = NULL;
    UINT1               au1DstMac[MAC_ADDR_LEN] =
        { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x0E };

    /* LldpIfCreate is called in 
     * LldpIfCreateAllPorts: u4PortIndex validation is done at calling place
     * interface creation:  u4PortIndex Validation is done before message 
     *                      posting. 
     * So u4PortIndex validation is not required here. */

    /* After posting the evet to LLDP, the event is queued in the queue, 
     * when the event is in the queue, if LLDP is shutdown the oper status 
     * change should be given to higher layers, that is the reason system 
     * control is checked in LldpApiNotifyIfOperStatusChg and LldpIfCreate */
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (INIT_SHUT_TRC, "LldpIfCreate: LLDP module is shut down\n");
        return OSIX_FAILURE;
    }

    /* LLDP_IS_PORT_ID_VALID returns OSIX_FALSE
     * if port is invalid */
    if (LLDP_IS_PORT_ID_VALID (u4PortIndex) == OSIX_FALSE)
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpIfCreate port is invalid"
                       "%lu \r\n", u4PortIndex);
        return OSIX_FAILURE;
    }
    if (LLDP_IS_PORT_CHANNEL (u4PortIndex) == OSIX_TRUE)
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpIfCreate port is a port channel"
                       "lu \r\n", u4PortIndex);
        return OSIX_FAILURE;
    }
    MEMSET (&PortInfo, 0, sizeof (PortInfo));
    PortInfo.i4IfIndex = (INT4) u4PortIndex;
    pPortInfo = RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &PortInfo);
    if (pPortInfo != NULL)
    {
        LLDP_TRC (INIT_SHUT_TRC, "LldpIfCreate: "
                  "Port with same index already exists. "
                  "Another entry cannot be created\r\n");
        return OSIX_SUCCESS;
    }
    if ((pPortInfo =
         (tLldpLocPortTable *) MemAllocMemBlk (gLldpGlobalInfo.
                                               LocPortTablePoolId)) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC, "LldpIfCreate: Alloc mem block "
                  "FAILED!!!\r\n");
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpIfCreate: Alloc mem block for port FAILED !!!"));
#endif
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }

    MEMSET (pPortInfo, 0, sizeof (tLldpLocPortTable));
    pPortInfo->i4IfIndex = PortInfo.i4IfIndex;
    MEMCPY (pPortInfo->au1DstMac, au1DstMac, MAC_ADDR_LEN);
    pPortInfo->i4MedAdminStatus = LLDP_DISABLED;
    if (RBTreeAdd (gLldpGlobalInfo.LldpPortInfoRBTree, pPortInfo) != RB_SUCCESS)
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                (UINT1 *) pPortInfo) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "port table entry Memory release failed!!. \r\n");
        }
        return OSIX_FAILURE;
    }

    if ((pAgentMappingEntry =
         (tLldpv2AgentToLocPort *) MemAllocMemBlk (gLldpGlobalInfo.
                                                   LldpAgentToLocPortMappingPoolId))
        == NULL)
    {
        if (RBTreeRemove (gLldpGlobalInfo.LldpPortInfoRBTree, pPortInfo) !=
            RB_SUCCESS)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "RBTreeRemove failed for port table entry !!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                (UINT1 *) pPortInfo) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "port table entry Memory release failed!!. \r\n");
        }
        LLDP_TRC (LLDP_CRITICAL_TRC, "LldpIfCreate: Alloc mem block "
                  "FAILED for tLldpv2AgentToLocPort!!!\r\n");
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpIfCreate: Alloc mem block for lldp agent mapping FAILED !!!"));
#endif
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;

    }
    MEMSET (pAgentMappingEntry, 0, sizeof (tLldpv2AgentToLocPort));
    pAgentMappingEntry->i4IfIndex = pPortInfo->i4IfIndex;
    MEMCPY (pAgentMappingEntry->Lldpv2DestMacAddress, au1DstMac, MAC_ADDR_LEN);
    pAgentMappingEntry->u4LldpLocPort = (UINT4) pPortInfo->i4IfIndex;
    pAgentMappingEntry->u4DstMacAddrTblIndex = 1;

    pAgentMappingEntry->u1RowStatus = LLDP_ACTIVE;
    if (RBTreeAdd (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                   pAgentMappingEntry) != RB_SUCCESS)
    {
        if (RBTreeRemove (gLldpGlobalInfo.LldpPortInfoRBTree, pPortInfo) !=
            RB_SUCCESS)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "RBTreeRemove failed for port table entry !!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                (UINT1 *) pPortInfo) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "port table entry Memory release failed!!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId,
                                (UINT1 *) pAgentMappingEntry) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "agentmapping entry Memory release failed!!. \r\n");
        }
        LLDP_TRC (LLDP_CRITICAL_TRC, "LldpIfCreate: RBTreeAdd  "
                  "FAILED for tLldpv2AgentToLocPort!!!\r\n");
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpIfCreate: RBTree add for lldp agent mapping FAILED !!!"));
#endif
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }
    if (RBTreeAdd (gLldpGlobalInfo.Lldpv2AgentMapTblRBTree,
                   pAgentMappingEntry) != RB_SUCCESS)
    {
        if (RBTreeRemove (gLldpGlobalInfo.LldpPortInfoRBTree, pPortInfo) !=
            RB_SUCCESS)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "RBTreeRemove failed for port table entry !!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                (UINT1 *) pPortInfo) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "port table entry Memory release failed!!. \r\n");
        }
        if (RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                          pAgentMappingEntry) != RB_SUCCESS)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "RBTreeRemove failed for agentToLocport table entry !!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId,
                                (UINT1 *) pAgentMappingEntry) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "agentmapping entry Memory release failed!!. \r\n");
        }
        LLDP_TRC (LLDP_CRITICAL_TRC, "LldpIfCreate: RBTreeAdd  "
                  "FAILED for tLldpv2AgentToLocPort!!!\r\n");
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpIfCreate: RBTree add for lldp agent mapping FAILED !!!"));
#endif
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }
    pPortEntry = LLDP_GET_LOC_PORT_INFO (pAgentMappingEntry->u4LldpLocPort);
    /* pPortEntry is not NULL, port with same index already exists */
    if (pPortEntry != NULL)
    {
        LLDP_TRC (INIT_SHUT_TRC, "LldpIfCreate: "
                  "Port with same index already exists. "
                  "Another entry cannot be created\r\n");
        return OSIX_SUCCESS;
    }

    /* Allocate memory for the new port entry */
    if ((pPortEntry =
         (tLldpLocPortInfo *) MemAllocMemBlk (gLldpGlobalInfo.
                                              LocPortInfoPoolId)) == NULL)
    {
        if (RBTreeRemove (gLldpGlobalInfo.LldpPortInfoRBTree, pPortInfo) !=
            RB_SUCCESS)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "RBTreeRemove failed for port table entry !!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                (UINT1 *) pPortInfo) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "port table entry Memory release failed!!. \r\n");
        }
        if (RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                          pAgentMappingEntry) != RB_SUCCESS)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "RBTreeRemove failed for agentToLocport table entry !!. \r\n");
        }
        if (RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentMapTblRBTree,
                          pAgentMappingEntry) != RB_SUCCESS)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "RBTreeRemove failed for agentToLocport table entry !!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId,
                                (UINT1 *) pAgentMappingEntry) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "agentmapping entry Memory release failed!!. \r\n");
        }
        LLDP_TRC (LLDP_CRITICAL_TRC, "LldpIfCreate: Alloc mem block "
                  "FAILED!!!\r\n");
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpIfCreate: Alloc mem block FAILED !!!"));
#endif
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }

    MEMSET (pPortEntry, 0, sizeof (tLldpLocPortInfo));

    /* Update the interface specific paramaters obtained from lower layer. */
    pPortEntry->u4LocPortNum = pAgentMappingEntry->u4LldpLocPort;
    pPortEntry->i4IfIndex = pPortInfo->i4IfIndex;
    pPortEntry->u4DstMacAddrTblIndex = 1;
    MEMCPY (pPortEntry->PortConfigTable.u1DstMac, au1DstMac, MAC_ADDR_LEN);
    /* Populate port related info */
    if (LldpIfInit (pPortEntry) != OSIX_SUCCESS)
    {
        LLDP_TRC (INIT_SHUT_TRC, "LldpIfCreate: LldpIfInit FAILED" "!!!\r\n");

        /* Release Memory allocated for holding PDU Info */
        if (pPortEntry->pPreFormedLldpdu != NULL)
        {
            MemReleaseMemBlock (gLldpGlobalInfo.LldpPduInfoPoolId,
                                pPortEntry->pPreFormedLldpdu);
            pPortEntry->pPreFormedLldpdu = NULL;
            pPortEntry->u4PreFormedLldpduLen = 0;
        }

        /* delete the port inforamtion and release the memory */
        if (LldpTxUtlClearPortInfo (pPortEntry) != OSIX_SUCCESS)
        {
            LLDP_TRC (INIT_SHUT_TRC, "LldpIfCreate: "
                      "LldpTxUtlClearPortInfo returns FAILURE!!!\r\n");
        }
        /* release the memory allocated for port info structure */
        if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortInfoPoolId,
                                (UINT1 *) pPortEntry) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "port info Memory release failed!!. \r\n");
        }
        if (RBTreeRemove (gLldpGlobalInfo.LldpPortInfoRBTree, pPortInfo) !=
            RB_SUCCESS)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "RBTreeRemove failed for port table entry !!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                (UINT1 *) pPortInfo) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "port table entry Memory release failed!!. \r\n");
        }
        if (RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                          pAgentMappingEntry) != RB_SUCCESS)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "RBTreeRemove failed for agentToLocport table entry !!. \r\n");
        }
        if (RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentMapTblRBTree,
                          pAgentMappingEntry) != RB_SUCCESS)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "RBTreeRemove failed for agentToLocport table entry !!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId,
                                (UINT1 *) pAgentMappingEntry) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "agentmapping entry Memory release failed!!. \r\n");
        }
        return OSIX_FAILURE;
    }
    if (LldpIfUpdateIfDesc (pPortEntry) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpIfCreate: LldpIfUpdateIfDesc"
                  "returns FAILURE\r\n");

        /* Release Memory allocated for holding PDU Info */
        if (pPortEntry->pPreFormedLldpdu != NULL)
        {
            MemReleaseMemBlock (gLldpGlobalInfo.LldpPduInfoPoolId,
                                pPortEntry->pPreFormedLldpdu);
            pPortEntry->pPreFormedLldpdu = NULL;
            pPortEntry->u4PreFormedLldpduLen = 0;
        }

        /* delete port related information and releas memory */
        if (LldpTxUtlClearPortInfo (pPortEntry) != OSIX_SUCCESS)
        {
            LLDP_TRC (INIT_SHUT_TRC, "LldpIfCreate: "
                      "LldpIfUpdateIfDesc returns FAILURE!!!\r\n");
        }
        /* release the memory allocated for port info structure */
        if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortInfoPoolId,
                                (UINT1 *) pPortEntry) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "port info Memory release failed!!. \r\n");
        }
        if (RBTreeRemove (gLldpGlobalInfo.LldpPortInfoRBTree, pPortInfo) !=
            RB_SUCCESS)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "RBTreeRemove failed for port table entry !!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                (UINT1 *) pPortInfo) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "port table entry Memory release failed!!. \r\n");
        }
        if (RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                          pAgentMappingEntry) != RB_SUCCESS)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "RBTreeRemove failed for agentToLocport table entry !!. \r\n");
        }
        if (RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentMapTblRBTree,
                          pAgentMappingEntry) != RB_SUCCESS)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "RBTreeRemove failed for agentToLocport table entry !!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId,
                                (UINT1 *) pAgentMappingEntry) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "agentmapping entry Memory release failed!!. \r\n");
        }
        return OSIX_FAILURE;
    }

    /* Add the local port entry to the local port table ie agent entry */
    if (LldpIfAddToPortTable (pPortEntry) != OSIX_SUCCESS)
    {
        LLDP_TRC (INIT_SHUT_TRC, "LldpIfCreate: LldpIfInit FAILED!!!\r\n");

        /* Release Memory allocated for holding PDU Info */
        if (pPortEntry->pPreFormedLldpdu != NULL)
        {
            MemReleaseMemBlock (gLldpGlobalInfo.LldpPduInfoPoolId,
                                pPortEntry->pPreFormedLldpdu);
            pPortEntry->pPreFormedLldpdu = NULL;
            pPortEntry->u4PreFormedLldpduLen = 0;
        }

        /* delete port related information and releas memory */
        if (LldpTxUtlClearPortInfo (pPortEntry) != OSIX_SUCCESS)
        {
            LLDP_TRC (INIT_SHUT_TRC, "LldpIfCreate: "
                      "LldpTxUtlClearPortInfo returns FAILURE!!!\r\n");
        }
        if (RBTreeRemove (gLldpGlobalInfo.LldpPortInfoRBTree, pPortInfo) !=
            RB_SUCCESS)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "RBTreeRemove failed for port table entry !!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                (UINT1 *) pPortInfo) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "port table entry Memory release failed!!. \r\n");
        }
        if (RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                          pAgentMappingEntry) != RB_SUCCESS)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "RBTreeRemove failed for agentToLocport table entry !!. \r\n");
        }
        if (RBTreeRemove (gLldpGlobalInfo.Lldpv2AgentMapTblRBTree,
                          pAgentMappingEntry) != RB_SUCCESS)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "RBTreeRemove failed for agentToLocport table entry !!. \r\n");
        }
        if (MemReleaseMemBlock (gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId,
                                (UINT1 *) pAgentMappingEntry) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "agentmapping entry Memory release failed!!. \r\n");
        }
        /* release the memory allocated for port info structure */
        if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortInfoPoolId,
                                (UINT1 *) pPortEntry) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "port info Memory release failed!!. \r\n");
        }
        return OSIX_FAILURE;
    }

    /* While starting LLDP module, indication has to be sent regarding
     * the interface that are made operationally up. This leads to the
     * transition of Rx state machine from wait for operational state to
     * initialise state */

    if ((LldpPortGetIfOperStatus ((UINT4) pPortEntry->i4IfIndex,
                                  &pPortEntry->u1OperStatus)
         == OSIX_SUCCESS) && (pPortEntry->u1OperStatus == CFA_IF_UP))
    {
        LldpRxSemRun (pPortEntry, LLDP_RX_EV_PORT_OPER_UP);
        if (gLldpGlobalInfo.u1ModuleStatus == LLDP_ENABLED)
        {
            LldpTxTmrSmMachine (pPortEntry, LLDP_TX_TMR_EV_PORT_OPER_UP);
            LldpTxSmMachine (pPortEntry, LLDP_TX_EV_PORT_OPER_UP);
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpIfDelete
 *                                                                          
 *    DESCRIPTION      : This function releases memory for the port entry,
 *                       and removes it from the port table. 
 *
 *    INPUT            : pPortEntry - Port Entry to be deleted
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None. 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
LldpIfDelete (tLldpLocPortInfo * pPortEntry)
{
    /* After posting the evet to LLDP, the event is queued in the queue, 
     * when the event is in the queue, if LLDP is shutdown the oper status 
     * change should be given to higher layers, that is the reason system 
     * control is checked in LldpApiNotifyIfOperStatusChg and LldpIfDelete */
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (INIT_SHUT_TRC, "LldpIfDelete: LLDP mdule is shut down\n");
        return;
    }

    /* Delete the port entry from the port table */
    if (pPortEntry != NULL)
    {
        LldpUtilDeletePortInfo (pPortEntry);
    }

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpIfOperChg
 *                                                                          
 *    DESCRIPTION      : This function is called when the oper status of the 
 *                       interface changes.  
 *
 *    INPUT            : pPortInfo - Pointer to port info struct.
 *                       u1OperStatus - Oper status of the interface   
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
LldpIfOperChg (tLldpLocPortInfo * pPortInfo, UINT1 u1OperStatus)
{
    INT4                i4LldpEnabled = LLDP_DISABLED;
    UINT1               u1Event = 0;
    tLldpLocPortInfo   *pLocalPortInfo = pPortInfo;

    /* After posting the evet to LLDP, the event is queued in the queue, 
     * when the event is in the queue, if LLDP is shutdown the oper status 
     * change should be given to higher layers, that is the reason system 
     * control is checked in LldpApiNotifyIfOperStatusChg and LldpIfOperChg */
    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (INIT_SHUT_TRC, "LLDP module is shut down\r\n");
        return OSIX_FAILURE;
    }

    if (pPortInfo == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpIfOperChg: pPortInfo NULL"
                  " pointer received\r\n");
        return OSIX_FAILURE;
    }

    if (pPortInfo->u1OperStatus == u1OperStatus)
    {
        LLDP_TRC (INIT_SHUT_TRC, "No change in oper status of the"
                  " interface\r\n");
        return OSIX_SUCCESS;
    }

    /* Operation status of the interface is modified. 
     * Update the new oper status in Lldp portInfo. */
    pPortInfo->u1OperStatus = u1OperStatus;

#ifdef L2RED_WANTED
    /* Port oper status is changed, so sync up the 
     * oper change with STANDBY node */
    LldpRedSendSyncUpMsg ((UINT1) LLDP_RED_OPER_STATUS_CHG_MSG,
                          (VOID *) pPortInfo);
#endif

    i4LldpEnabled = pPortInfo->PortConfigTable.i4AdminStatus;

    if (pPortInfo->u1OperStatus == CFA_IF_UP)
    {
        if (LLdpLaIsPortInIcclIf ((UINT4) pPortInfo->i4IfIndex) == OSIX_SUCCESS)
        {
            return OSIX_SUCCESS;
        }

        LLDP_TRC_ARG1 (CONTROL_PLANE_TRC, "LldpIfOperChg: "
                       " OperStatus chg rcvd for interface %lu - UP\r\n",
                       pPortInfo->u4LocPortNum);

        /* oper status is changed to OPER_UP, so update interface related 
         * information in LLDP port information structure */
        if (LldpIfUpdateIfInfo (pPortInfo) != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC, "LldpIfOperChg: LldpIfUpdateIfInfo "
                      "returns FAILURE\r\n");
            return OSIX_FAILURE;
        }
        if (LldpIfUpdateIfAlias (pPortInfo) != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC, "LldpIfOperChg: LldpIfUpdateIfAlias"
                      "returns FAILURE\r\n");
            return OSIX_FAILURE;
        }
        /* Call TX SEM */
        /* whenever oper up event comes check whether LLDP admin status for 
         * any of the following values before invoking TX SEM */
        if (gLldpGlobalInfo.u1ModuleStatus == LLDP_ENABLED)
        {
            if ((i4LldpEnabled == LLDP_ADM_STAT_TX_ONLY) ||
                (i4LldpEnabled == LLDP_ADM_STAT_TX_AND_RX))
            {
                u1Event = LLDP_TX_EV_PORT_OPER_UP;
                LldpTxSmMachine (pPortInfo, u1Event);
                LldpTxTmrSmMachine (pPortInfo, u1Event);
                if ((pPortInfo->bSomethingChangedLocal == OSIX_TRUE) &&
                    (gLldpGlobalInfo.u1ModuleStatus == LLDP_ENABLED))
                {
                    LldpTxTmrSmMachine (pPortInfo, LLDP_TX_TMR_EV_LOCAL_CHANGE);
                }
            }
        }
        /* Call RX SEM */
        /* whenever oper change(oper up/oper down) event comes, 
         * No need to check LLDP admin status, just invoke RX SEM */
        u1Event = LLDP_RX_EV_PORT_OPER_UP;
        LldpRxSemRun (pPortInfo, u1Event);
    }
    else if (pPortInfo->u1OperStatus == CFA_IF_DOWN)
    {
        LLDP_TRC_ARG1 (CONTROL_PLANE_TRC, "LldpIfOperChg: OperStatus "
                       "change  rcvd for interface %lu - DOWN\r\n",
                       pPortInfo->u4LocPortNum);
        /* Delete all the Remote Node associated with current port. This function
         * takes care of deleting rxinfo age timer if running */
        while ((pLocalPortInfo != NULL) &&
               (pLocalPortInfo->i4IfIndex == pPortInfo->i4IfIndex))
        {
            LldpRxUtlDelRemInfoForPort (pLocalPortInfo);
            pLocalPortInfo =
                (tLldpLocPortInfo *) RBTreeGetNext (gLldpGlobalInfo.
                                                    LldpLocPortAgentInfoRBTree,
                                                    (tRBElem *) pLocalPortInfo,
                                                    LldpAgentInfoUtlRBCmpInfo);
        }

#ifdef L2RED_WANTED
        /* If the node is STANDBY and current TXSEM state is IDLE, then CFA 
         * port oper status is retreived. If the CFA port oper status is UP, 
         * then port down indication is given to lower layers and TXSEM 
         * transits to INIT state. Else, TXSEM transits to INIT state directly
         */
        if ((LLDP_RED_NODE_STATUS () == RM_STANDBY) &&
            (pPortInfo->i4TxSemCurrentState == LLDP_TX_IDLE))
        {
            LldpIfHandleOperDown (pPortInfo);
            return OSIX_SUCCESS;
        }
#endif
        /* but if the event is oper down, no need to check admin status 
         * just invoke TX SEM */
        u1Event = LLDP_TX_EV_PORT_OPER_DOWN;
        /* Setting the number of neighbors to 0 during
           the Port Oper Down */
        pPortInfo->u2NoOfNeighbors = 0;
        LldpTxSmMachine (pPortInfo, u1Event);
        LldpTxTmrSmMachine (pPortInfo, u1Event);
        u1Event = LLDP_RX_EV_PORT_OPER_DOWN;
        LldpRxSemRun (pPortInfo, u1Event);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpIfUpdateIfInfo 
 *                                                                          
 *    DESCRIPTION      : This function updates the port strucutre with
 *                       interface related information. 
 *                       While udpating local system information set
 *                       somethingChangedLocal flag to OSIX_TRUE, but no need to
 *                       call sem machine(as we do in other places wherever we
 *                       set somethingChangedLocal to OSIX_TRUE,since this
 *                       function is called when OperStatusChange(SEM will be 
 *                       started on receiving OperStatus change(UP) event).
 *
 *    INPUT            : pPortInfo - Lldp Port specific information
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
/* called when port oper changes(UP) */
PRIVATE INT4
LldpIfUpdateIfInfo (tLldpLocPortInfo * pPortInfo)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT2               u2CurrAutoNegAdvtCap = 0;
    UINT2               u2NewAutoNegAdvtCap = 0;
    UINT1               u1CurrAutoNegSupport = 0;
    UINT1               u1CurrAutoNegStatus = 0;

    MEMSET (&gCfaIfInfo, 0, sizeof (tCfaIfInfo));

    /* update tCfaIfInfo */
    if (LldpPortGetIfInfo ((UINT4) pPortInfo->i4IfIndex, &gCfaIfInfo) !=
        OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpIfUpdateIfInfo: LldpIfUpdateIfInfo"
                  "returns FAILURE\r\n");
        i4RetVal = OSIX_FAILURE;
    }

    /* OperMauType */
    /* change in data so update in local system information */
    if (pPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo.u2OperMauType
        != gCfaIfInfo.u2OperMauType)
    {
        pPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo.u2OperMauType =
            gCfaIfInfo.u2OperMauType;
        pPortInfo->bSomethingChangedLocal = OSIX_TRUE;
    }
    /* Auto Negotiation advertisment capability */
    u2CurrAutoNegAdvtCap =
        pPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo.u2AutoNegAdvtCap;
    u2NewAutoNegAdvtCap = gCfaIfInfo.u2AdvtCapability;
    /* change in data so update in local system information */
    if (u2CurrAutoNegAdvtCap != u2NewAutoNegAdvtCap)
    {
        pPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo.u2AutoNegAdvtCap =
            u2NewAutoNegAdvtCap;
        pPortInfo->bSomethingChangedLocal = OSIX_TRUE;
    }
    /* Auto Negotiation support */
    u1CurrAutoNegSupport =
        pPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo.u1AutoNegSupport;
    /* change in data so update in local system information */
    if (u1CurrAutoNegSupport != gCfaIfInfo.u1AutoNegSupport)
    {
        if (gCfaIfInfo.u1AutoNegSupport == ENET_AUTO_SUPPORT)
        {
            pPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo.u1AutoNegSupport =
                LLDP_TRUE;
        }
        else if (gCfaIfInfo.u1AutoNegSupport == ENET_AUTO_NO_SUPPORT)
        {
            pPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo.u1AutoNegSupport =
                LLDP_FALSE;
        }
        pPortInfo->bSomethingChangedLocal = OSIX_TRUE;
    }
    /* Auto Negotiation status */
    u1CurrAutoNegStatus =
        pPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo.u1AutoNegEnabled;
    /* change in data so update in local system information */
    if (u1CurrAutoNegStatus != gCfaIfInfo.u1AutoNegStatus)
    {
        if (gCfaIfInfo.u1AutoNegStatus == CFA_ENABLED)
        {
            pPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo.u1AutoNegEnabled =
                LLDP_TRUE;
        }
        else if (gCfaIfInfo.u1AutoNegStatus == CFA_DISABLED)
        {
            pPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo.u1AutoNegEnabled =
                LLDP_FALSE;
        }
        pPortInfo->bSomethingChangedLocal = OSIX_TRUE;
    }
    /* change in data so update in local system information */
    if (pPortInfo->Dot3LocPortInfo.u2MaxFrameSize != gCfaIfInfo.u4IfMtu)
    {
        pPortInfo->Dot3LocPortInfo.u2MaxFrameSize = (UINT2) gCfaIfInfo.u4IfMtu;
        pPortInfo->bSomethingChangedLocal = OSIX_TRUE;
    }
    return (i4RetVal);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpIfUpdateIfAlias 
 *                                                                          
 *    DESCRIPTION      : This function updates interface alias (during port
 *                       oper status only if) portid subtype is IFALIAS, 
 *                       otherwise returns SUCCESS
 
 *    INPUT            : pPortInfo - Lldp Port specific information
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
/* called when port oper changes(UP) */
PRIVATE INT4
LldpIfUpdateIfAlias (tLldpLocPortInfo * pPortInfo)
{
    UINT1               au1NewIfAlias[CFA_MAX_PORT_NAME_LENGTH];
    UINT2               u2IfAliasLen = 0;
    tLldpLocPortTable  *pPortEntry = NULL;
    tLldpLocPortTable   PortEntry;

    MEMSET (&PortEntry, 0, sizeof (tLldpLocPortTable));
    PortEntry.i4IfIndex = pPortInfo->i4IfIndex;
    pPortEntry = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, (tRBElem *) & PortEntry);
    if (pPortEntry == NULL)        /*For Klocwork */
    {
        return OSIX_FAILURE;
    }

    MEMSET (au1NewIfAlias, 0, CFA_MAX_PORT_NAME_LENGTH);

    if ((LldpPortGetIfAlias ((UINT4) pPortInfo->i4IfIndex, au1NewIfAlias))
        != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpIfUpdateIfAlias: LldpPortGetIfAlias "
                  "returns FAILURE\r\n");
        return OSIX_FAILURE;
    }

    LldpUtilGetPortIdLen (LLDP_PORT_ID_SUB_IF_ALIAS, au1NewIfAlias,
                          &u2IfAliasLen);

    /* Initialize port id */
    MEMSET (pPortEntry->au1LocPortId, 0, LLDP_MAX_LEN_PORTID);

    if (u2IfAliasLen < LLDP_MAX_LEN_PORTID)
    {
        STRNCPY (pPortEntry->au1LocPortId, au1NewIfAlias, u2IfAliasLen);
    }
    pPortInfo->bSomethingChangedLocal = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpIfUpdateIfDesc 
 *                                                                          
 *    DESCRIPTION      : This function updates interface description 
 *                       (when port oper status becomes UP)
 
 *    INPUT            : pPortInfo - Lldp Port specific information
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
/* called when port oper changes(UP) */
PUBLIC INT4
LldpIfUpdateIfDesc (tLldpLocPortInfo * pPortInfo)
{
    tLldpLocPortTable  *pPortEntry = NULL;
    tLldpLocPortTable  *pTmpPortEntry;

    MEMSET (&gCfaIfInfo, 0, sizeof (tCfaIfInfo));

    if ((pTmpPortEntry = (tLldpLocPortTable *)
         MemAllocMemBlk (gLldpGlobalInfo.LocPortTablePoolId)) == NULL)
    {
        LLDP_TRC ((LLDP_CRITICAL_TRC | ALL_FAILURE_TRC),
                  "LldpTxUtlSetPortId: Failed to Allocate Memory "
                  "for LldpLocPortTable node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }
    MEMSET (pTmpPortEntry, 0, sizeof (tLldpLocPortTable));
    pTmpPortEntry->i4IfIndex = pPortInfo->i4IfIndex;
    pPortEntry = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree,
                   (tRBElem *) pTmpPortEntry);
    if (pPortEntry == NULL)        /*For Klocwork */
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                (UINT1 *) pTmpPortEntry) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "LldpTxUtlSetPortId: "
                      "Node info Memory release failed!!. \r\n");
        }
        return OSIX_FAILURE;
    }

    /* get new ifdesc */
    if (LldpPortGetIfInfo ((UINT4) pPortInfo->i4IfIndex, &gCfaIfInfo) !=
        OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpIfUpdateIfDesc: LldpPortGetIfInfo"
                  "returns FAILURE\r\n");
        if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                (UINT1 *) pTmpPortEntry) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "LldpTxUtlSetPortId: "
                      "Node info Memory release failed!!. \r\n");
        }
        return OSIX_FAILURE;
    }

    /* if change in data, update local system information */
    if (STRNCMP (pPortEntry->au1LocPortDesc,
                 gCfaIfInfo.au1Descr, LLDP_MAX_LEN_PORTDESC) != 0)
    {
        /* Initialize port id */
        MEMSET (pPortEntry->au1LocPortDesc, 0, LLDP_MAX_LEN_PORTDESC);
        STRNCPY (pPortEntry->au1LocPortDesc, gCfaIfInfo.au1Descr,
                 LLDP_MAX_LEN_PORTDESC);
        pPortInfo->bSomethingChangedLocal = OSIX_TRUE;
        LldpTxTmrSmMachine (pPortInfo, LLDP_TX_TMR_EV_LOCAL_CHANGE);
    }
    MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                        (UINT1 *) pTmpPortEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpIfInit 
 *                                                                          
 *    DESCRIPTION      : This function initializes the port strucutre with
 *                       default values. 
 *
 *    INPUT            : pPortInfo - Lldp Port specific information
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
LldpIfInit (tLldpLocPortInfo * pPortInfo)
{
    UINT1               au1IfAlias[CFA_MAX_PORT_NAME_LENGTH];
    UINT2               au2ConfProtoVlans[VLAN_MAX_VID_SET_ENTRIES_PER_PORT];
    UINT2               u2Index = 0;
    UINT1               u1NumVlan = 0;
    UINT2               u2AggId = 0;
    tLldpLocPortTable  *pPortEntry = NULL;
    tLldpLocPortTable  *pTmpPortEntry = NULL;

    if ((pTmpPortEntry = (tLldpLocPortTable *)
         MemAllocMemBlk (gLldpGlobalInfo.LocPortTablePoolId)) == NULL)
    {
        LLDP_TRC ((LLDP_CRITICAL_TRC | ALL_FAILURE_TRC),
                  "LldpTxUtlSetPortId: Failed to Allocate Memory "
                  "for LldpLocPortTable node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }
    MEMSET (pTmpPortEntry, 0, sizeof (tLldpLocPortTable));

    pTmpPortEntry->i4IfIndex = pPortInfo->i4IfIndex;
    pPortEntry = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree,
                   (tRBElem *) pTmpPortEntry);
    if (pPortEntry == NULL)        /*For Klocwork */
    {
        if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                (UINT1 *) pTmpPortEntry) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "LldpTxUtlSetPortId: "
                      "Node info Memory release failed!!. \r\n");
        }
        return OSIX_FAILURE;
    }

    MEMSET (&gCfaIfInfo, 0, sizeof (tCfaIfInfo));
    MEMSET (au1IfAlias, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au2ConfProtoVlans, 0, (sizeof (UINT2) *
                                   VLAN_MAX_VID_SET_ENTRIES_PER_PORT));

    /* it is local pointer, so no need to allocate memory for this */
    pPortInfo->pBackPtrRemoteNode = NULL;

    pPortInfo->pPreFormedLldpdu =
        (UINT1 *) MemAllocMemBlk (gLldpGlobalInfo.LldpPduInfoPoolId);
    if (pPortInfo->pPreFormedLldpdu == NULL)
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpIfInit: Memory Allocation failed "
                       "for PreFormedLldpdu for Port: %d\r\n",
                       pPortInfo->i4IfIndex);
        if (MemReleaseMemBlock
            (gLldpGlobalInfo.LocPortTablePoolId,
             (UINT1 *) pTmpPortEntry) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "LldpIfInit: " "PDU info Memory release failed!!. \r\n");
        }
        return OSIX_FAILURE;
    }
    pPortInfo->u4PreFormedLldpduLen = 0;
    /* pu1RxLldpdu - used to store the pointer of rcvd pdu. 
     * No need to allocate memory */
    pPortInfo->pu1RxLldpdu = NULL;
    pPortInfo->u2RxPduLen = 0;
    pPortInfo->bstatsFramesInerrorsTotal = OSIX_FALSE;
    pPortInfo->u4FastTxSysUpTime = 0;
    pPortInfo->i1FastTxRateLimit = 0;

    /* The flag "au1DupTlvChkFlag" is used to check the existence of duplicate
     * TLVs in the Refresh Frame; Currently Bit 1 is used for identifying the
     * duplicate VLAN Name TLV & Bit 2 is used for Management Address TLV.
     * Bit 3 to Bit 8 is currently unused; Reset all the Bits of the flag */
    MEMSET (pPortInfo->au1DupTlvChkFlag, 0,
            sizeof (pPortInfo->au1DupTlvChkFlag));

    /*pPortInfo->u4MsgInterval = gLldpGlobalInfo.SysConfigInfo.i4MsgTxInterval; */
    /* no need to start timers here, but all timers are created durin global
     * initialization */

    /* Initialize PortConfigTable */
    pPortInfo->PortConfigTable.i4AdminStatus = LLDP_ADM_STAT_TX_AND_RX;
    pPortInfo->PortConfigTable.u1TLVsTxEnable = 0;
    pPortInfo->PortConfigTable.u1NotificationEnable = LLDP_MIS_CFG_NOTIFICATION;
    pPortInfo->PortConfigTable.u1FsConfigNotificationType =
        LLDP_MIS_CFG_NOTIFICATION;
    /* LLDP-MED */
    pPortInfo->LocMedCapInfo.u2MedLocCapTxSupported =
        LLDP_MED_DEF_TLV_SUPPORTED;
    pPortInfo->bMedCapable = LLDP_MED_FALSE;
    pPortInfo->bMedFrameDiscarded = LLDP_MED_FALSE;
    /* Retrieve the Power Management TLV details */
    if (LldpMedPortGetPSEPowerInfo (pPortEntry) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpIfInit: Failure in getting"
                  "Power Info \r\n");
    }

    /* since MEMSET is done on port info no need to explicitly initialize 
     * StatsTxPortTable and StatsRxPortTable */

    /* By default Portd id subtype is IfAlias,so initialize port id subtype and 
     * port id */
    /* port id subtype */
    pPortEntry->i4LocPortIdSubtype = LLDP_PORT_ID_SUB_IF_ALIAS;
    /* port id */
    if ((LldpPortGetIfAlias (pPortInfo->i4IfIndex, au1IfAlias)) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpIfInit: LldpPortGetIfAlias"
                  "returns FAILURE\r\n");
        if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                (UINT1 *) pTmpPortEntry) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "LldpTxUtlSetPortId: "
                      "Node info Memory release failed!!. \r\n");
        }
        return OSIX_FAILURE;
    }
    MEMSET (pPortEntry->au1LocPortId, 0, LLDP_MAX_LEN_PORTID);
    STRNCPY (pPortEntry->au1LocPortId, au1IfAlias, LLDP_MAX_LEN_PORTID);

    /* populate interface related information */
    /* get IfInfo */
    if (LldpPortGetIfInfo ((UINT4) pPortInfo->i4IfIndex, &gCfaIfInfo) !=
        OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpIfInit: LldpPortGetIfInfo"
                  "returns FAILURE\r\n");
        if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                (UINT1 *) pTmpPortEntry) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "LldpTxUtlSetPortId: "
                      "Node info Memory release failed!!. \r\n");
        }
        return OSIX_FAILURE;
    }
    /* port description */
    MEMSET (pPortEntry->au1LocPortDesc, 0, LLDP_MAX_LEN_PORTDESC);
    STRNCPY (pPortEntry->au1LocPortDesc, gCfaIfInfo.au1Descr,
             LLDP_MAX_LEN_PORTDESC);
    /*Intialize the dot1ManVid digest */
    pPortEntry->u4LocMgmtVid = IssGetDefaultVlanIdFromNvRam ();

    /* Dot3AutoNegInfo */
    /* OperMauType */
    pPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo.u2OperMauType =
        gCfaIfInfo.u2OperMauType;
    /* Auto Negotiation advertisment capability */
    pPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo.u2AutoNegAdvtCap =
        gCfaIfInfo.u2AdvtCapability;
    /* Auto Negotiation support */
    if (gCfaIfInfo.u1AutoNegSupport == ENET_AUTO_SUPPORT)
    {
        pPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo.u1AutoNegSupport = LLDP_TRUE;
    }
    else
    {
        pPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo.u1AutoNegSupport =
            LLDP_FALSE;
    }
    /* Auto Negotiation status */
    if (gCfaIfInfo.u1AutoNegStatus == CFA_ENABLED)
    {
        pPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo.u1AutoNegEnabled = LLDP_TRUE;
    }
    else
    {
        pPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo.u1AutoNegEnabled =
            LLDP_FALSE;
    }

    /* Dot3PowerInfo */
    /* we are not supporting PowerInfo */
    LldpIfDot3PowerInfoInit (pPortInfo);

    /* u1AggStatus */
    if (LldpPortGetAggStatus ((UINT4) pPortInfo->i4IfIndex,
                              &(pPortInfo->Dot3LocPortInfo.u1AggStatus))
        != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpIfInit: LldpPortGetAggStatus"
                  "returns FAILURE\r\n");
        if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                (UINT1 *) pTmpPortEntry) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "LldpTxUtlSetPortId: "
                      "Node info Memory release failed!!. \r\n");
        }
        return OSIX_FAILURE;
    }
    /* u4AggPortId */
    pPortInfo->Dot3LocPortInfo.u4AggPortId = u2AggId;
    if (L2IwfIsPortInPortChannel ((UINT4) pPortInfo->i4IfIndex) ==
        L2IWF_SUCCESS)
    {
        if (L2IwfGetPortChannelForPort ((UINT4) pPortInfo->i4IfIndex, &u2AggId)
            == L2IWF_SUCCESS)
        {
            pPortInfo->Dot3LocPortInfo.u4AggPortId = u2AggId;
        }
    }
    /* Maximum frame size */
    pPortInfo->Dot3LocPortInfo.u2MaxFrameSize = (UINT2) gCfaIfInfo.u4IfMtu;
    /* u1TLVTxEnable */
    pPortInfo->Dot3LocPortInfo.u1TLVTxEnable = 0;
    pPortInfo->u1ProtoVlanEnabled = LLDP_FALSE;
    pPortInfo->u1ManAddrTlvTxEnabled = LLDP_FALSE;
    pPortInfo->u1RefreshNotify = LLDP_FALSE;
    pPortInfo->u2NoOfNeighbors = 0;
    /* Initialize LocProtoVlanDllList */
    TMO_DLL_Init (&(pPortInfo->LocProtoVlanDllList));
    /* If the port is a member of the port-channel then the ports will not be 
     * visible to VLAN module.*/
    if (L2IwfIsPortInPortChannel (pPortInfo->i4IfIndex) != L2IWF_SUCCESS)
    {                            /* ProtoVlanEnabled */
        if (LldpPortGetProtoVlanStatus ((UINT4) pPortInfo->i4IfIndex,
                                        &(pPortInfo->u1ProtoVlanEnabled))
            != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC, "LldpIfInit: LldpPortGetProtoVlanStatus"
                      "returns FAILURE\r\n");
            if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                    (UINT1 *) pTmpPortEntry) == MEM_FAILURE)
            {
                LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "LldpTxUtlSetPortId: "
                          "Node info Memory release failed!!. \r\n");
            }
            return OSIX_FAILURE;
        }

        /* populate proto vlan info in the DLL */
        if (LldpPortGetProtoVlan ((UINT4) pPortInfo->i4IfIndex,
                                  &au2ConfProtoVlans[0],
                                  &u1NumVlan) != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC, "LldpIfInit: LldpPortGetProtoVlan"
                      "returns FAILURE\r\n");
            return OSIX_FAILURE;
        }
    }
    /* If the system does not know the PPVID, then the PPVID value need to be 
     * set as 0*/
    if (u1NumVlan == 0)
    {
        if (LldpTxUtlAddProtoVlanEntry (pPortInfo, 0) != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC, "LldpIfInit: LldpTxUtlAddProtoVlanEntry"
                      "returns FAILURE\r\n");
            if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                    (UINT1 *) pTmpPortEntry) == MEM_FAILURE)
            {
                LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "LldpIfInit: "
                          "Node info Memory release failed!!. \r\n");
            }
            return OSIX_FAILURE;
        }
    }
    else
    {                            /* add the vlan info to DLL */
        for (u2Index = 0; ((u2Index < u1NumVlan) &&
                           (u2Index < VLAN_MAX_VID_SET_ENTRIES_PER_PORT));
             u2Index++)
        {
            if (LldpTxUtlAddProtoVlanEntry
                (pPortInfo, au2ConfProtoVlans[u2Index]) != OSIX_SUCCESS)
            {
                LLDP_TRC (ALL_FAILURE_TRC,
                          "LldpIfInit: LldpTxUtlAddProtoVlanEntry"
                          "returns FAILURE\r\n");
                if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                        (UINT1 *) pTmpPortEntry) == MEM_FAILURE)
                {
                    LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                              "LldpIfInit: "
                              "Node info Memory release failed!!. \r\n");
                }
                return OSIX_FAILURE;
            }
        }
    }
    pPortInfo->i4TxSemCurrentState = LLDP_TX_INIT;
    pPortInfo->i4RxSemCurrentState = LLDP_RX_WAIT_PORT_OPERATIONAL;
    pPortInfo->i4TxTimerSemCurrentState = LLDP_TX_TMR_INIT;
    pPortInfo->i4RxTtl = LLDP_INVALID_VALUE;

    /* initialize PVID */
    if (LldpPortGetPortVlanId
        ((UINT4) pPortInfo->i4IfIndex, &(pPortInfo->u2PVid)) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpIfInit: LldpPortGetPortVlanId"
                  "returns FAILURE\r\n");
        if (MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                (UINT1 *) pTmpPortEntry) == MEM_FAILURE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "LldpIfInit: " "Node info Memory release failed!!. \r\n");
        }
        return OSIX_FAILURE;
    }
    /* PortVlanTxEnable */
    pPortInfo->u1PortVlanTxEnable = LLDP_FALSE;

    pPortInfo->TxDelayTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;
    pPortInfo->TtrTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;
    pPortInfo->ShutWhileTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;
    pPortInfo->bMaxPduSizeExceeded = OSIX_FALSE;
    pPortInfo->bSomethingChangedLocal = OSIX_FALSE;
    /* this flag indicates that the msg interval timer is started 
     * with jitterd msg interval (i.e. for the first time when starting 
     * meg interval timer we should use jittered msg interval) */
    pPortInfo->bMsgIntvalJittered = OSIX_FALSE;
    pPortInfo->u1RxFrameType = LLDP_RX_FRAME_NEW;
    pPortInfo->u1OperStatus = CFA_IF_DOWN;

    if (LldpUtlDisableAllLldpXdot1ConfigVlanNameTxEnable
        (pPortInfo->i4IfIndex, LLDP_FALSE) != OSIX_SUCCESS)
    {
    }

    /* construct the preformed buffer */
    if (LldpTxUtlConstructPreformedBuf (pPortInfo, LLDP_INFO_FRAME)
        != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpIfInit: LldpTxUtlConstructPreformedBuf"
                  "returns FAILURE\r\n");
        MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                            (UINT1 *) pTmpPortEntry);
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                        (UINT1 *) pTmpPortEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpIfDot3PowerInfoInit 
 *                                                                          
 *    DESCRIPTION      : This function initializes the local port 
 *                       IEEE802.3 organizationally specific power 
 *                       information strucutre 
 *
 *    INPUT            : pPortInfo - Lldp Port specific information
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
LldpIfDot3PowerInfoInit (tLldpLocPortInfo * pPortInfo)
{
    /* TODO Not supported in the current release */
    UNUSED_PARAM (pPortInfo);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpIfAddToPortTable
 *                                                                          
 *    DESCRIPTION      : This function adds a port into the global struct.
 *                       It is called whenever a new port is created.   
 *
 *    INPUT            : pPortInfo - Pointer to port information struct.
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
LldpIfAddToPortTable (tLldpLocPortInfo * pPortInfo)
{
    UINT4               u4PortIndex = 0;

    u4PortIndex = pPortInfo->u4LocPortNum;
    if (LLDP_GET_LOC_PORT_INFO (u4PortIndex) != NULL)
    {
        LLDP_TRC_ARG1 (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC, "Port Entry already"
                       " exists for port %d\r\n", u4PortIndex);
        return OSIX_FAILURE;
    }

    if (RBTreeAdd (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree, pPortInfo) !=
        RB_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpIfCreateAllPorts
 *                                                                          
 *    DESCRIPTION      : This function creates all valid ports in LLDP module. 
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
LldpIfCreateAllPorts (VOID)
{
    UINT2               u2PortIndex = 0;
    UINT2               u2PrevPort = 0;

    while (LldpPortGetNextValidPort (u2PrevPort, &u2PortIndex) == OSIX_SUCCESS)
    {
        if (LldpIfCreate ((UINT4) u2PortIndex) == OSIX_FAILURE)
        {
            LLDP_TRC_ARG1 (INIT_SHUT_TRC, "LldpIfCreateAllPorts: Port %lu"
                           "Entry creation FAILED\r\n", u2PortIndex);
        }
        else
        {
            LLDP_TRC_ARG1 (INIT_SHUT_TRC, "LldpIfCreateAllPorts: Port %lu "
                           "Entry created\r\n", u2PortIndex);
        }

        u2PrevPort = u2PortIndex;
        u2PortIndex = 0;
    }
    return;
}

/****************************************************************************
*                                                                          
*    FUNCTION NAME    : LldpIfHandleOperDown 
*                                                                          
*    DESCRIPTION      : This function handles the port oper down event in
*                       STANDBY node
*
*    INPUT            : pPortInfo - Pointer to port info struct.
*                                                                          
*    OUTPUT           : TXSEM transits to INIT state
*                       RXSEM transits to WAIT_FOR_OPERUP state
*                                                                          
*    RETURNS          : NONE 
*                                                                          
****************************************************************************/
#ifdef L2RED_WANTED
PRIVATE VOID
LldpIfHandleOperDown (tLldpLocPortInfo * pPortInfo)
{
    UINT1               u1CfaIfOperStatus = CFA_IF_DOWN;
    UINT1               u1Event = 0;

    /* Since port number is already validated while creating the port info
     * structure, LldpPortGetIfOperStatus will never return failure
     * in this scenario */
    LldpPortGetIfOperStatus ((UINT4) pPortInfo->i4IfIndex, &u1CfaIfOperStatus);

    if (u1CfaIfOperStatus == CFA_IF_UP)
    {
        LldpPortDownProcessedIndication ((UINT4) pPortInfo->i4IfIndex);
    }
    /* TXSEM has to transit to INIT state. So, post re-init delay timer expiry
     * event to TXSEM with current state as SHUT_FRAME */
    pPortInfo->i4TxSemCurrentState = LLDP_TX_SHUT_FRAME;
    u1Event = (UINT1) LLDP_TX_EV_REINIT_DELAY;
    LldpTxSmMachine (pPortInfo, u1Event);
    /* RXSEM: move to wait for port operational state */
    u1Event = (UINT1) LLDP_RX_EV_PORT_OPER_DOWN;
    LldpRxSemRun (pPortInfo, u1Event);
    return;
}
#endif

/*-----------------------------------------------------------------------*/
/*                       End of the file  lldpif.c                       */
/*-----------------------------------------------------------------------*/
