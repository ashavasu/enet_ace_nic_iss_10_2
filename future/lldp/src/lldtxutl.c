/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldtxutl.c,v 1.80 2017/12/16 11:48:18 siva Exp $
 *
 * Description: This file contains utility routines that is used by
 *              LLDP transmit module.
 *****************************************************************************/
#ifndef _LLDTXUTL_C_
#define _LLDTXUTL_C_

#include "lldinc.h"

UINT1               gau1Lldpdu[LLDP_MAX_PDU_SIZE];

PRIVATE INT4 LldpHandlePduSizeExceeded PROTO ((tLldpLocPortInfo * pPortInfo,
                                               UINT1 *pu1BasePtr,
                                               UINT1 *pu1BuffPtr,
                                               UINT2 u2OptionalTlvsByteCount));
PRIVATE VOID LldpTxUtlAddSysMacAddr PROTO ((tLldpLocPortInfo * pPortInfo,
                                            UINT1 *pu1Buf,
                                            UINT2 *pu2LocOffset));

PRIVATE INT4 LldpTxUtlAddPduHeader PROTO ((tLldpLocPortInfo * pPortInfo));

PRIVATE tLldpLocManAddrTable TmpLocManNode;

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddIpv4Addr 
 *
 *    DESCRIPTION      : This function adds IPV4 management address 
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None  
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddIpv4Addr (tNetIpv4IfInfo * pNetIp4IfInfo)
{
    tLldpLocManAddrTable *pManAddrNode = NULL;
    UINT4               u4CfaIfIndex = 0;
    UINT4               u4TmpAddr = 0;
    UINT1               au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR] = { 0 };
    UINT1               u1IsNodePresent = LLDP_TRUE;
    INT4                i4IfIndex = 0;
    UINT4               u4DestMacIndex = 0;
    UINT4               u4LocPortNum = 0;
    INT4                i4PrevIfIndex = 0;
    UINT4               u4PrevDestMacIndex = 0;
    tMacAddr            DstMac;
    tLldpLocPortInfo   *pPortInfo = NULL;
#ifdef ISS_WANTED
    tCfaIfInfo          CfaIfInfo;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
#endif

    MEMSET (au1LocManAddr, 0, LLDP_MAX_LEN_MAN_ADDR);
    u4TmpAddr = (pNetIp4IfInfo->u4Addr);
    MEMCPY (au1LocManAddr, &u4TmpAddr, sizeof (UINT4));
    if (LldpPortGetIfIndexFromPort (pNetIp4IfInfo->u4IfIndex, &u4CfaIfIndex)
        != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpTxUtlAddIpv4Addr: "
                  "LldpPortGetIfIndexFromPort failed\r\n");
        return OSIX_FAILURE;
    }

    /* If the node is already available, update the fields and oper status */
    pManAddrNode = (tLldpLocManAddrTable *)
        LldpTxUtlGetLocManAddrNode (IPVX_ADDR_FMLY_IPV4, &au1LocManAddr[0]);

    if (pManAddrNode == NULL)
    {
        u1IsNodePresent = LLDP_FALSE;
        if ((pManAddrNode = (tLldpLocManAddrTable *)
             (MemAllocMemBlk (gLldpGlobalInfo.LocManAddrPoolId))) == NULL)
        {
            LLDP_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC | LLDP_CRITICAL_TRC,
                      "LldpTxUtlAddIpv4Addr : Failed to allocate memory for "
                      "Local ManAddr Node\r\n");
#ifndef FM_WANTED
            SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                          "LldpTxUtlAddIpv4Addr : Failed to allocate memory for"
                          "Local ManAddr Node"));
#endif
            LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
            return OSIX_FAILURE;
        }

        MEMSET (pManAddrNode, 0, sizeof (tLldpLocManAddrTable));
    }

    MEMCPY (pManAddrNode->au1LocManAddr, &u4TmpAddr, sizeof (UINT4));

    pManAddrNode->i4LocManAddrSubtype = IPVX_ADDR_FMLY_IPV4;
    /* ManAddrLen = Addr subtype len + ManAddr Len */
    pManAddrNode->i4LocManAddrLen = LLDP_SUBTYPE_LEN + sizeof (UINT4);
    pManAddrNode->i4LocManAddrIfSubtype = LLDP_MAN_ADDR_IF_SUB_IFINDEX;
    pManAddrNode->i4LocManAddrIfId = (INT4) u4CfaIfIndex;
    pManAddrNode->u4L3IfIndex = pNetIp4IfInfo->u4IfIndex;
    pManAddrNode->u1OperStatus = IPIF_OPER_ENABLE;
    /* Fetch the OID for the ifindex */
    if (LldpPortGetOidforIfIndex (&pManAddrNode->LocManAddrOID) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "LldpTxUtlAddIpv4Addr : Unable to Populate the OID for "
                  "IfIndex\r\n");
    }
    if (nmhGetFirstIndexLldpV2PortConfigTable (&i4IfIndex, &u4DestMacIndex)
        != SNMP_FAILURE)
    {
        do
        {
            MEMSET (DstMac, 0, sizeof (tMacAddr));
            nmhGetLldpV2DestMacAddress (u4DestMacIndex, &DstMac);
            if (LldpUtlGetLocalPort (i4IfIndex, DstMac, &u4LocPortNum) !=
                OSIX_FAILURE)
            {
#ifdef ISS_WANTED
                if (LldpPortGetIfInfo ((UINT4) pManAddrNode->i4LocManAddrIfId,
                                       &CfaIfInfo) != OSIX_FAILURE)
                {
                    if (L2IwfMiIsVlanMemberPort (L2IWF_DEFAULT_CONTEXT,
                                                 (tVlanId) CfaIfInfo.u2VlanId,
                                                 (UINT4) i4IfIndex) ==
                        OSIX_TRUE)
                    {
#endif
                        pPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPortNum);
                        if (pPortInfo != NULL)
                        {
                            if (pPortInfo->u1ManAddrTlvTxEnabled == LLDP_TRUE)
                            {
                                OSIX_BITLIST_SET_BIT (pManAddrNode->
                                                      au1LocManAddrPortsTxEnable,
                                                      (UINT2) pPortInfo->
                                                      u4LocPortNum,
                                                      LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
                            }
                            else
                            {
                                OSIX_BITLIST_RESET_BIT (pManAddrNode->
                                                        au1LocManAddrPortsTxEnable,
                                                        (UINT2) pPortInfo->
                                                        u4LocPortNum,
                                                        LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
                            }
                        }
#ifdef ISS_WANTED
                    }
                }
#endif
            }

            i4PrevIfIndex = i4IfIndex;
            u4PrevDestMacIndex = u4DestMacIndex;
        }
        while (nmhGetNextIndexLldpV2PortConfigTable
               (i4PrevIfIndex, &i4IfIndex, u4PrevDestMacIndex,
                &u4DestMacIndex) == SNMP_SUCCESS);
    }

    if (u1IsNodePresent == LLDP_FALSE)
    {
        /* Add the Entry in the Local ManAddr Table */
        if (RBTreeAdd
            (gLldpGlobalInfo.LocManAddrRBTree,
             (tRBElem *) pManAddrNode) != RB_SUCCESS)
        {
            MemReleaseMemBlock (gLldpGlobalInfo.LocManAddrPoolId,
                                (UINT1 *) pManAddrNode);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddIpv6Addr 
 *
 *    DESCRIPTION      : This function adds IPV6 management address 
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None  
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddIpv6Addr (tNetIpv6AddrChange * pNetIpv6AddrChange)
{
    tLldpLocManAddrTable *pManAddrNode = NULL;
    UINT4               u4CfaIfIndex = 0;
    UINT1               au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR] = { 0 };
    UINT1               u1IsNodePresent = LLDP_TRUE;
    INT4                i4IfIndex = 0;
    UINT4               u4DestMacIndex = 0;
    UINT4               u4LocPortNum = 0;
    INT4                i4PrevIfIndex = 0;
    UINT4               u4PrevDestMacIndex = 0;
    tMacAddr            DstMac;
    tLldpLocPortInfo   *pPortInfo = NULL;
#ifdef ISS_WANTED
    tCfaIfInfo          CfaIfInfo;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
#endif

    MEMSET (DstMac, 0, sizeof (tMacAddr));

    u4CfaIfIndex = pNetIpv6AddrChange->u4Index;
    MEMSET (au1LocManAddr, 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMCPY (au1LocManAddr, &pNetIpv6AddrChange->Ipv6AddrInfo.Ip6Addr,
            sizeof (tIp6Addr));
    pManAddrNode =
        (tLldpLocManAddrTable *)
        LldpTxUtlGetLocManAddrNode (IPVX_ADDR_FMLY_IPV6, &au1LocManAddr[0]);

    if (pManAddrNode == NULL)
    {
        u1IsNodePresent = LLDP_FALSE;
        /* Add to Man Addr */
        if ((pManAddrNode = (tLldpLocManAddrTable *)
             (MemAllocMemBlk (gLldpGlobalInfo.LocManAddrPoolId))) == NULL)
        {
            LLDP_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC | LLDP_CRITICAL_TRC,
                      "(LldpTxUtlAddIpv6Addr) - Failed to allocate memory for "
                      "Local ManAddr Node\r\n");
#ifndef FM_WANTED
            SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                          "(LldpTxUtlAddIpv6Addr) - Failed to allocate memory for"
                          "Local ManAddr Node"));
#endif
            LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
            return OSIX_FAILURE;
        }
        MEMSET (pManAddrNode, 0, sizeof (tLldpLocManAddrTable));
    }

    MEMCPY (pManAddrNode->au1LocManAddr,
            &pNetIpv6AddrChange->Ipv6AddrInfo.Ip6Addr, sizeof (tIp6Addr));
    pManAddrNode->i4LocManAddrSubtype = IPVX_ADDR_FMLY_IPV6;
    pManAddrNode->i4LocManAddrLen = sizeof (INT4) + IPVX_IPV6_ADDR_LEN;
    /* Addr subtype len + ManAddr Len */
    pManAddrNode->i4LocManAddrIfSubtype = LLDP_MAN_ADDR_IF_SUB_IFINDEX;
    pManAddrNode->i4LocManAddrIfId = (INT4) u4CfaIfIndex;
    pManAddrNode->u1OperStatus = IPIF_OPER_ENABLE;

    /* Fetch the OID for the ifindex */
    if (LldpPortGetOidforIfIndex (&pManAddrNode->LocManAddrOID) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "LldpTxUtlAddIpv6Addr : Unable to Populate the OID for "
                  "IfIndex\r\n");
    }
    if (nmhGetFirstIndexLldpV2PortConfigTable (&i4IfIndex, &u4DestMacIndex)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    do
    {
        MEMSET (DstMac, 0, sizeof (tMacAddr));
        nmhGetLldpV2DestMacAddress (u4DestMacIndex, &DstMac);
        if (LldpUtlGetLocalPort (i4IfIndex, DstMac, &u4LocPortNum) !=
            OSIX_FAILURE)
        {
#ifdef ISS_WANTED
            if (LldpPortGetIfInfo ((UINT4) pManAddrNode->i4LocManAddrIfId,
                                   &CfaIfInfo) != OSIX_FAILURE)
            {
                if (L2IwfMiIsVlanMemberPort (L2IWF_DEFAULT_CONTEXT,
                                             (tVlanId) CfaIfInfo.u2VlanId,
                                             (UINT4) i4IfIndex) == OSIX_TRUE)
                {
#endif
                    pPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPortNum);
                    if (pPortInfo != NULL)
                    {
                        if (pPortInfo->u1ManAddrTlvTxEnabled == LLDP_TRUE)
                        {
                            OSIX_BITLIST_SET_BIT (pManAddrNode->
                                                  au1LocManAddrPortsTxEnable,
                                                  (UINT2) pPortInfo->
                                                  u4LocPortNum,
                                                  LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
                        }
                        else
                        {
                            OSIX_BITLIST_RESET_BIT (pManAddrNode->
                                                    au1LocManAddrPortsTxEnable,
                                                    (UINT2) pPortInfo->
                                                    u4LocPortNum,
                                                    LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
                        }
                    }
#ifdef ISS_WANTED
                }
            }
#endif
        }

        i4PrevIfIndex = i4IfIndex;
        u4PrevDestMacIndex = u4DestMacIndex;
    }
    while (nmhGetNextIndexLldpV2PortConfigTable
           (i4PrevIfIndex, &i4IfIndex, u4PrevDestMacIndex,
            &u4DestMacIndex) == SNMP_SUCCESS);

    if (u1IsNodePresent == LLDP_FALSE)
    {
        /* Add the Entry in the Local ManAddr Table */
        if (RBTreeAdd
            (gLldpGlobalInfo.LocManAddrRBTree,
             (tRBElem *) pManAddrNode) != RB_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC | LLDP_CRITICAL_TRC,
                      "(LldpTxUtlAddIpv6Addr) - Failed to Add the Node to "
                      "LocManAddrRBTree\r\n");
            MemReleaseMemBlock (gLldpGlobalInfo.LocManAddrPoolId,
                                (UINT1 *) pManAddrNode);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlDelManAddrNode
 *
 *    DESCRIPTION      : This function deletes the management address node
 *                       from the LocManAddrRBTree.
 *                       RBNodeFree Function is called which is
 *                       responsible for releasing the memory allocated to 
 *                       that node. 
 *
 *    INPUT            : i4ManAddrSubtype - management address subtype
 *                       pu1ManAddr - management address
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlDelManAddrNode (INT4 i4ManAddrSubtype, UINT1 *pu1ManAddr)
{
    tLldpLocManAddrTable *pManAddrNode = NULL;

    /* get management address node from management address RBTree */
    pManAddrNode = LldpTxUtlGetLocManAddrNode (i4ManAddrSubtype, pu1ManAddr);

    if (pManAddrNode == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpTxUtlDelManAddrNode: "
                  "LldpTxUtlGetLocManAddrNode returns FAILURE\r\n");
        return OSIX_FAILURE;
    }

    /* remove management address node from management address RBTree */
    RBTreeRem (gLldpGlobalInfo.LocManAddrRBTree, (tRBElem *) pManAddrNode);

    /* release the memory */
    MemReleaseMemBlock (gLldpGlobalInfo.LocManAddrPoolId,
                        (UINT1 *) pManAddrNode);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 * Function     : LldpTxUtlSetChassisId
 * 
 * Description  : This function sets the chassis id based on the chassis id 
 *                subtype passed. 
 *                
 * Input        : i4NewChassisIdSubType - chassis id subtype
 *                
 * Output       : None
 * 
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE
 * 
*****************************************************************************/
PUBLIC INT4
LldpTxUtlSetChassisId (INT4 i4ChassisIdSubType)
{
    tCfaIfInfo          CfaIfInfo;
    UINT1               au1IfAlias[CFA_MAX_PORT_NAME_LENGTH];
    UINT1              *pu1TmpChassisId;
    BOOL1               bLocalSysInfoChanged = OSIX_FALSE;
    tMacAddr            BaseMacAddr;
    UINT4               u4DefIfIndex = 0;
    UINT4               u4Ipv4Addr = 0;
    UINT2               u2ChassIdLen = 0;

    MEMSET (au1IfAlias, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    switch (i4ChassisIdSubType)
    {
        case LLDP_CHASS_ID_SUB_IF_ALIAS:
            LldpPortGetDefaultRouterIfIndex (&u4DefIfIndex);
            if (LldpPortGetIfAlias (u4DefIfIndex, au1IfAlias) != OSIX_SUCCESS)
            {
                LLDP_TRC (ALL_FAILURE_TRC, "LldpTxUtlSetChassisId: "
                          "LldpPortGetIfAlias returns FAILURE.\r\n");
                return OSIX_FAILURE;
            }
            /* update chassis id in the local information port structure */
            MEMSET (gLldpGlobalInfo.LocSysInfo.au1LocChassisId, 0,
                    LLDP_MAX_LEN_CHASSISID);
            /* get chassis id length */
            LldpUtilGetChassisIdLen (LLDP_CHASS_ID_SUB_IF_ALIAS,
                                     au1IfAlias, &u2ChassIdLen);

            MEMCPY (gLldpGlobalInfo.LocSysInfo.au1LocChassisId,
                    au1IfAlias, MEM_MAX_BYTES (u2ChassIdLen,
                                               CFA_MAX_PORT_NAME_LENGTH));
            bLocalSysInfoChanged = OSIX_TRUE;
            break;
        case LLDP_CHASS_ID_SUB_MAC_ADDR:
            LldpPortGetSysMacAddr (BaseMacAddr);
            /* update chassis id in the local information port structure */
            MEMSET (gLldpGlobalInfo.LocSysInfo.au1LocChassisId, 0,
                    LLDP_MAX_LEN_CHASSISID);
            /* get chassis id length */
            LldpUtilGetChassisIdLen (LLDP_CHASS_ID_SUB_MAC_ADDR,
                                     BaseMacAddr, &u2ChassIdLen);

            MEMCPY (gLldpGlobalInfo.LocSysInfo.au1LocChassisId,
                    BaseMacAddr, MEM_MAX_BYTES (u2ChassIdLen,
                                                sizeof (tMacAddr)));
            bLocalSysInfoChanged = OSIX_TRUE;
            break;
        case LLDP_CHASS_ID_SUB_NW_ADDR:
            LldpPortGetDefaultRouterIfIndex (&u4DefIfIndex);
            if (LldpPortGetIpv4Addr (u4DefIfIndex, &u4Ipv4Addr) != OSIX_SUCCESS)
            {
                LLDP_TRC (ALL_FAILURE_TRC, "LldpTxUtlSetChassisId: "
                          "LldpPortGetIpv4Addr returns FAILURE.\r\n");
                return OSIX_FAILURE;
            }
            /* update chassis id */
            MEMSET (gLldpGlobalInfo.LocSysInfo.au1LocChassisId, 0,
                    LLDP_MAX_LEN_CHASSISID);

            pu1TmpChassisId = &gLldpGlobalInfo.LocSysInfo.au1LocChassisId[0];
            LLDP_PUT_1BYTE (pu1TmpChassisId, (UINT1) IPVX_ADDR_FMLY_IPV4);
            PTR_ASSIGN4 (&gLldpGlobalInfo.LocSysInfo.au1LocChassisId[1],
                         u4Ipv4Addr);
            bLocalSysInfoChanged = OSIX_TRUE;
            break;
        case LLDP_CHASS_ID_SUB_IF_NAME:
            LldpPortGetDefaultRouterIfIndex (&u4DefIfIndex);
            if (LldpPortGetIfInfo (u4DefIfIndex, &CfaIfInfo) != OSIX_SUCCESS)
            {
                LLDP_TRC (ALL_FAILURE_TRC, "LldpTxUtlSetChassisId: "
                          "LldpPortGetIfInfo returns FAILURE.\r\n");
                return OSIX_FAILURE;
            }
            /* update chassis id */
            MEMSET (gLldpGlobalInfo.LocSysInfo.au1LocChassisId, 0,
                    LLDP_MAX_LEN_CHASSISID);
            /* get chassis id length */
            LldpUtilGetChassisIdLen (LLDP_CHASS_ID_SUB_IF_NAME,
                                     CfaIfInfo.au1IfName, &u2ChassIdLen);

            MEMCPY (gLldpGlobalInfo.LocSysInfo.au1LocChassisId,
                    CfaIfInfo.au1IfName,
                    MEM_MAX_BYTES (u2ChassIdLen, CFA_MAX_PORT_NAME_LENGTH));
            bLocalSysInfoChanged = OSIX_TRUE;
            break;
        default:
            LLDP_TRC (ALL_FAILURE_TRC, "LldpTxUtlSetChassisId: "
                      "default case.\r\n");
            break;
    }

    /* no change in local system info */
    if (bLocalSysInfoChanged == OSIX_FALSE)
    {
        return OSIX_SUCCESS;
    }
    /* change in local sys info */
    LldpTxUtlHandleLocSysInfoChg ();

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 * Function     : LldpTxUtlHandleLocSysInfoChg 
 * 
 * Description  : This function handles local system information change.
 *                Gets the port entry, if txdelay timer is expired for that 
 *                port reconstrcuts preformed buff and transmits the pdu.
 *                else sets somethingchangedLocal to OSIX_TRUE. This processes 
 *                is repeated for all ports.
 *
 * Input        : None               
 *
 * Output       : None
 * 
 * Returns      : None
 * 
 *****************************************************************************/
PUBLIC VOID
LldpTxUtlHandleLocSysInfoChg (VOID)
{
    UINT4               u4PortIndex = 0;
    tLldpLocPortInfo    PortInfo;
    tLldpLocPortInfo   *pPortInfo = NULL;

    MEMSET (&PortInfo, 0, sizeof (tLldpLocPortInfo));

    pPortInfo = (tLldpLocPortInfo *)
        RBTreeGetFirst (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree);
    while (pPortInfo != NULL)

    {
        /* get port entry */
        if (LldpTxUtlHandleLocPortInfoChg (pPortInfo) != OSIX_SUCCESS)
        {
            LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpTxUtlHandleLocSysInfoChg: "
                           "LldpTxUtlHandleLocPortInfoChg returns FAILURE "
                           "for port %d.\r\n", u4PortIndex);
        }
        PortInfo.u4LocPortNum = pPortInfo->u4LocPortNum;
        pPortInfo = (tLldpLocPortInfo *)
            RBTreeGetNext (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
                           &PortInfo, LldpAgentInfoUtlRBCmpInfo);

    }
    /* notify other modules about the change in chassis-id */
    LldpNotifyLocSysInfoChg (LLDP_UPDATE_CHASSISID,
                             0,
                             gLldpGlobalInfo.LocSysInfo.i4LocChassisIdSubtype,
                             gLldpGlobalInfo.LocSysInfo.au1LocChassisId);
    return;
}

/*****************************************************************************
 *
 * Function     : LldpTxUtlHandleLocPortInfoChg 
 *
 * Description  : This function handles local port information change.
 *                If txdelay timer is expired for that
 *                port reconstrcuts preformed buff and transmits the pdu.
 *                else sets somethingchangedLocal to OSIX_TRUE.
 *
 * Input        : pPortInfo - pointer to port information structure             
 *
 * Output       : None
 *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
LldpTxUtlHandleLocPortInfoChg (tLldpLocPortInfo * pPortInfo)
{
    tLldpLocPortTable  *pPortEntry = NULL;
    tLldpLocPortTable   PortEntry;

    MEMSET (&PortEntry, 0, sizeof (tLldpLocPortTable));
    PortEntry.i4IfIndex = pPortInfo->i4IfIndex;
    pPortEntry = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, (tRBElem *) & PortEntry);
    if (pPortEntry == NULL)        /*For Klocwork */
    {
        return OSIX_FAILURE;
    }

    /* change in local port information, check whether txdelay
     * timer expired, if yes construct the pdu and transmit the
     * frame, else set something changed local to true and return
     * success, so that when txdelay expires something changed local
     * will be handled */
    if ((pPortInfo->TxDelayTmrNode.u1Status != LLDP_TMR_EXPIRED) &&
        (gLldpGlobalInfo.i4Version == LLDP_VERSION_05))
    {
        pPortInfo->bSomethingChangedLocal = OSIX_TRUE;
        return OSIX_SUCCESS;
    }

    /* If the node is Standby node and if any local data change occurs 
     * then set the bSomethingChangedLocal flag to TRUE(which is done above), 
     * and return. So, that when the Standby node becomes active 
     * the local data change will be transmitted from the new Active node */
#ifdef L2RED_WANTED
    if (LLDP_RED_NODE_STATUS () == RM_STANDBY)
    {
        return OSIX_SUCCESS;
    }
#endif

    /* construct the preformed buffer */
    if (LldpTxUtlConstructPreformedBuf (pPortInfo, LLDP_INFO_FRAME)
        != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpTxUtlHandleLocPortInfoChg: "
                  "Construction of preformed buffer Failed.\r\n");
        return OSIX_FAILURE;
    }
    /* invoke state machine to transmit the frame */
    LldpTxTmrSmMachine (pPortInfo, LLDP_TX_TMR_EV_LOCAL_CHANGE);
    /* notify other modules about the change in port-id */
    LldpNotifyLocSysInfoChg (LLDP_UPDATE_PORTID,
                             pPortInfo->u4LocPortNum,
                             pPortEntry->i4LocPortIdSubtype,
                             pPortEntry->au1LocPortId);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 * Function     : LldpTxUtlSetPortId
 * 
 * Description  : This function sets the port id based on the port id subtype 
 *                passed. 
 *                
 * Input        : u4Value            :  value to be jittered
 *                u4JitterPcent      :  jitter percent
 *                
 * Output       : None
 * 
 * Returns      : Modified value after jittering
 * 
 *****************************************************************************/
PUBLIC INT4
LldpTxUtlSetPortId (UINT4 u4LocPortNum, INT4 i4PortIdSubType)
{
    tLldpLocPortInfo   *pPortInfo = NULL;
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4AgentCktId = 0;
    UINT1               au1IfAlias[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1SrcMacAddr[MAC_ADDR_LEN];
    UINT1              *pu1TmpPortId;
    BOOL1               bLocalPortInfoChanged = OSIX_FALSE;
    UINT4               u4Ipv4Addr = 0;
    UINT2               u2PortIdLen = 0;
    tLldpLocPortTable  *pPortEntry = NULL;
    tLldpLocPortTable  *pTmpPortEntry = NULL;
    tLldpv2AgentToLocPort AgentToLocPort;
    tLldpv2AgentToLocPort *pAgentToLocPort = NULL;

    MEMSET (&AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    MEMSET (au1IfAlias, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1SrcMacAddr, 0, MAC_ADDR_LEN);

    if ((pTmpPortEntry = (tLldpLocPortTable *)
         MemAllocMemBlk (gLldpGlobalInfo.LocPortTablePoolId)) == NULL)
    {
        LLDP_TRC ((LLDP_CRITICAL_TRC | ALL_FAILURE_TRC),
                  "LldpTxUtlSetPortId: Failed to Allocate Memory "
                  "for LldpLocPortTable node \r\n");
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }
    MEMSET (pTmpPortEntry, 0, sizeof (tLldpLocPortTable));

    pTmpPortEntry->i4IfIndex = (INT4) u4LocPortNum;
    pPortEntry = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree,
                   (tRBElem *) pTmpPortEntry);
    if (pPortEntry == NULL)        /*For Klocwork */
    {
        MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                            (UINT1 *) pTmpPortEntry);
        return OSIX_FAILURE;
    }

    if ((i4PortIdSubType == LLDP_PORT_ID_SUB_MAC_ADDR) ||
        (i4PortIdSubType == LLDP_PORT_ID_SUB_IF_NAME))
    {
        if (LldpPortGetIfInfo (u4LocPortNum, &CfaIfInfo) != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC, "LldpTxUtlSetPortId: "
                      "LldpPortGetIfInfo Failed.\r\n");
            MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                (UINT1 *) pTmpPortEntry);
            return OSIX_FAILURE;
        }
    }

    switch (i4PortIdSubType)
    {
        case LLDP_PORT_ID_SUB_IF_ALIAS:
            if (LldpPortGetIfAlias (u4LocPortNum, au1IfAlias) != OSIX_SUCCESS)
            {
                LLDP_TRC (ALL_FAILURE_TRC, "LldpTxUtlSetPortId: "
                          "LldpPortGetIfAlias Failed.\r\n");
                MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                    (UINT1 *) pTmpPortEntry);
                return OSIX_FAILURE;
            }
            /* update port id in the local information port structure */
            MEMSET (pPortEntry->au1LocPortId, 0, LLDP_MAX_LEN_PORTID);
            /* get port id length */
            LldpUtilGetPortIdLen (LLDP_PORT_ID_SUB_IF_ALIAS, au1IfAlias,
                                  &u2PortIdLen);
            MEMCPY (pPortEntry->au1LocPortId, au1IfAlias,
                    MEM_MAX_BYTES (u2PortIdLen, CFA_MAX_PORT_NAME_LENGTH));
            bLocalPortInfoChanged = OSIX_TRUE;
            break;
        case LLDP_PORT_ID_SUB_MAC_ADDR:
            /* update port id in the local information port structure */
            MEMSET (pPortEntry->au1LocPortId, 0, LLDP_MAX_LEN_PORTID);
            /* get port id length */
            LldpUtilGetPortIdLen (LLDP_PORT_ID_SUB_MAC_ADDR,
                                  CfaIfInfo.au1MacAddr, &u2PortIdLen);

            MEMCPY (pPortEntry->au1LocPortId,
                    CfaIfInfo.au1MacAddr,
                    MEM_MAX_BYTES (u2PortIdLen, CFA_ENET_ADDR_LEN));
            bLocalPortInfoChanged = OSIX_TRUE;
            break;
        case LLDP_PORT_ID_SUB_NW_ADDR:
            if (LldpPortGetIpv4Addr (u4LocPortNum, &u4Ipv4Addr) != OSIX_SUCCESS)
            {
                LLDP_TRC (ALL_FAILURE_TRC, "LldpTxUtlSetPortId: "
                          "LldpPortGetIpv4Addr returns FAILURE.\r\n");
                MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                    (UINT1 *) pTmpPortEntry);
                return OSIX_FAILURE;
            }
            /* update port id */
            MEMSET (pPortEntry->au1LocPortId, 0, LLDP_MAX_LEN_PORTID);
            /* get port id length */
            LldpUtilGetPortIdLen (LLDP_PORT_ID_SUB_NW_ADDR, NULL, &u2PortIdLen);
            pu1TmpPortId = &pPortEntry->au1LocPortId[0];
            LLDP_PUT_1BYTE (pu1TmpPortId, (UINT1) IPVX_ADDR_FMLY_IPV4);
            PTR_ASSIGN4 (&pPortEntry->au1LocPortId[1], u4Ipv4Addr);
            bLocalPortInfoChanged = OSIX_TRUE;
            break;
        case LLDP_PORT_ID_SUB_IF_NAME:
            /* update port id in the local information port structure */
            MEMSET (pPortEntry->au1LocPortId, 0, LLDP_MAX_LEN_PORTID);
            /* get port id length */
            LldpUtilGetPortIdLen (LLDP_PORT_ID_SUB_IF_NAME,
                                  CfaIfInfo.au1IfName, &u2PortIdLen);

            MEMCPY (pPortEntry->au1LocPortId,
                    CfaIfInfo.au1IfName,
                    MEM_MAX_BYTES (u2PortIdLen, CFA_MAX_PORT_NAME_LENGTH));
            bLocalPortInfoChanged = OSIX_TRUE;
            break;
        case LLDP_PORT_ID_SUB_AGENT_CKT_ID:
            if (LldpPortGetAgentCktId (u4LocPortNum,
                                       &u4AgentCktId) != OSIX_SUCCESS)
            {
                LLDP_TRC (ALL_FAILURE_TRC, "LldpTxUtlSetPortId: "
                          "LldpPortGetAgentCktId Failed.\r\n");
                MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                                    (UINT1 *) pTmpPortEntry);
                return OSIX_FAILURE;
            }
            /* update port id in the local information port structure */
            MEMSET (pPortEntry->au1LocPortId, 0, LLDP_MAX_LEN_PORTID);
            /* get port id length */
            LldpUtilGetPortIdLen (LLDP_PORT_ID_SUB_AGENT_CKT_ID,
                                  NULL, &u2PortIdLen);

            MEMCPY (pPortEntry->au1LocPortId,
                    (UINT1 *) &u4AgentCktId,
                    MEM_MAX_BYTES (u2PortIdLen, sizeof (UINT4)));
            bLocalPortInfoChanged = OSIX_TRUE;
            break;

        default:
            LLDP_TRC (ALL_FAILURE_TRC, "LldpTxUtlSetPortId: "
                      "default case.\r\n");
            break;
    }
    /* change in local port information */
    if (bLocalPortInfoChanged == OSIX_TRUE)
    {
        AgentToLocPort.i4IfIndex = (INT4) u4LocPortNum;

        pAgentToLocPort = (tLldpv2AgentToLocPort *)
            RBTreeGetNext (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                           &AgentToLocPort, LldpAgentToLocPortUtlRBCmpInfo);
        while ((pAgentToLocPort != NULL) &&
               (pAgentToLocPort->i4IfIndex == (INT4) u4LocPortNum))
        {
            pPortInfo = LLDP_GET_LOC_PORT_INFO (pAgentToLocPort->u4LldpLocPort);
            if (pPortInfo != NULL)
            {
                if (LldpTxUtlHandleLocPortInfoChg (pPortInfo) != OSIX_SUCCESS)
                {
                    LLDP_TRC (ALL_FAILURE_TRC, "LldpTxUtlSetPortId: "
                              "LldpTxUtlHandleLocPortInfoChg returned failed.\r\n");
                }
            }
            AgentToLocPort.i4IfIndex = pAgentToLocPort->i4IfIndex;
            MEMCPY (AgentToLocPort.Lldpv2DestMacAddress,
                    pAgentToLocPort->Lldpv2DestMacAddress, MAC_ADDR_LEN);
            pAgentToLocPort = (tLldpv2AgentToLocPort *)
                RBTreeGetNext (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                               &AgentToLocPort, LldpAgentToLocPortUtlRBCmpInfo);

        }
    }
    MemReleaseMemBlock (gLldpGlobalInfo.LocPortTablePoolId,
                        (UINT1 *) pTmpPortEntry);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 * Function     : LldpTxUtlJitter
 * 
 * Description  : Determines the jitter value based on the jitter percent
 *                and adds it to the value to be jittered.                   
 *                
 * Input        : u4Value            :  value to be jittered
 *                u4JitterPcent      :  jitter percent
 *                
 * Output       : None
 * 
 * Returns      : Modified value after jittering
 * 
*****************************************************************************/
PUBLIC UINT4
LldpTxUtlJitter (UINT4 u4Value, UINT4 u4JitterPcent)
{
    UINT4               u4Temp;
    static UINT4        su4Next = 1;

    su4Next = su4Next * 1103515245 + 12345;
    u4Temp =
        (((100
           - u4JitterPcent) +
          ((su4Next / 65536) % u4JitterPcent)) * (u4Value)) / 100;
    if (u4Temp == 0)
    {
        u4Temp = 1;
    }
    return (u4Temp);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlGetLocPortEntry
 *
 *    DESCRIPTION      : This function returns the pointer to the port entry
 *                       from the global structure.
 *
 *    INPUT            : u4PortIndex - Index of the port for which the 
 *                                     entry is required.
 *                       ppPortEntry - Address of pointer to port entry.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

PUBLIC INT4
LldpTxUtlGetLocPortEntry (UINT4 u4PortIndex, tLldpLocPortInfo ** ppLocPortEntry)
{
    INT4                i4RetVal = OSIX_FAILURE;
    UINT4               u4DestIndex = 0;
    UINT4               u4LocPortNum = 0;

    *ppLocPortEntry = NULL;

    if (LLDP_IS_PORT_ID_VALID (u4PortIndex) == OSIX_TRUE)
    {
        u4DestIndex = LLDP_V1_DEST_MAC_ADDR_INDEX ((INT4) u4PortIndex);
        u4LocPortNum =
            LldpGetLocalPortFromIfIndexDestMacIfIndex (u4PortIndex,
                                                       u4DestIndex);
        *ppLocPortEntry = LLDP_GET_LOC_PORT_INFO (u4LocPortNum);
        if (*ppLocPortEntry != NULL)
        {
            i4RetVal = OSIX_SUCCESS;
        }
    }
    return (i4RetVal);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlValidatePortIndex
 *
 *    DESCRIPTION      : This function validates the port index 
 *
 *    INPUT            : i4LldpPortConfigPortNum ( PortIndex) 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/

PUBLIC INT4
LldpTxUtlValidatePortIndex (UINT4 u4LldpPortConfigPortNum)
{
    tLldpLocPortInfo   *pLldpLocPortInfo;

    if (LldpTxUtlGetLocPortEntry (u4LldpPortConfigPortNum,
                                  &pLldpLocPortInfo) == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlGetProtoVlanEntry
 *
 *    DESCRIPTION      : This function gets the protocol vlan entry for the
 *                       given port number and vlan id.
 *
 *    INPUT            : VlanId - VLAN ID to be matched
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : Pointer to the protocol vlan entry / NULL  
 *
 ****************************************************************************/
PUBLIC tLldpxdot1LocProtoVlanInfo *
LldpTxUtlGetProtoVlanEntry (UINT4 u4IfIndex, tVlanId ProtoVlanId)
{
    tLldpLocPortInfo   *pLldpLocPortInfo;
    tLldpxdot1LocProtoVlanInfo *pProtoVlanEntry = NULL;
    UINT4               u4DestMacAddrIndex = 0;
    UINT4               u4LocPortNum = 0;

    u4DestMacAddrIndex = LLDP_V1_DEST_MAC_ADDR_INDEX ((INT4) u4IfIndex);
    u4LocPortNum =
        LldpGetLocalPortFromIfIndexDestMacIfIndex
        (u4IfIndex, u4DestMacAddrIndex);

    pLldpLocPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPortNum);

    if ((pLldpLocPortInfo != NULL) &&
        (LLDP_IS_VLAN_ID_VALID (ProtoVlanId) == OSIX_TRUE))
    {
        TMO_DLL_Scan (&(pLldpLocPortInfo->LocProtoVlanDllList),
                      pProtoVlanEntry, tLldpxdot1LocProtoVlanInfo *)
        {
            if (pProtoVlanEntry->u2ProtoVlanId == ProtoVlanId)
            {
                break;
            }
        }
    }
    /* In case entry not present NULL will be returned */
    return pProtoVlanEntry;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlRBCmpLocManAddr
 *
 *    DESCRIPTION      : RBTree compare function for LocManAddrRBTree.
 *
 *    INPUT            : pRBElem1 - Pointer to the LocManAddrRBNode node1
 *                       pRBElem2 - Pointer to the LocManAddrRBNode node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : LLDP_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       LLDP_RB_LESS    - if node pRBElem1's key is less than
 *                                         node pRBElem2's key.
 *                       LLDP_RB_GREATER - if node pRBElem1's key is greater
 *                                         than node pRBElem2's key.
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlRBCmpLocManAddr (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tLldpLocManAddrTable *pLocEntry1 = (tLldpLocManAddrTable *) pRBElem1;
    tLldpLocManAddrTable *pLocEntry2 = (tLldpLocManAddrTable *) pRBElem2;
    INT4                i4MemcmpRetVal = LLDP_INVALID_VALUE;

    /* Compare the First Index - Management address subtype */
    if (pLocEntry1->i4LocManAddrSubtype > pLocEntry2->i4LocManAddrSubtype)
    {
        return LLDP_RB_GREATER;
    }
    else if (pLocEntry1->i4LocManAddrSubtype < pLocEntry2->i4LocManAddrSubtype)
    {
        return LLDP_RB_LESS;
    }

    /* First Indicess are equal so
     * Compare the Second Index - Management address */
    i4MemcmpRetVal = MEMCMP (pLocEntry1->au1LocManAddr,
                             pLocEntry2->au1LocManAddr, LLDP_MAX_LEN_MAN_ADDR);
    if (i4MemcmpRetVal > 0)
    {
        return LLDP_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return LLDP_RB_LESS;
    }
    else
    {
        return LLDP_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlConstructPreformedBuf
 *
 *    DESCRIPTION      : Constructs the LLDPDU preformed buffer.
 *
 *    INPUT            : pPortInfo - porinter to port information structure
 *                       u1FrameType - type of the frame to be constructed
 *    
 *    OUTPUT           : pPortInfo - porinter to port information structure
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlConstructPreformedBuf (tLldpLocPortInfo * pPortInfo, UINT1 u1FrameType)
{
    UINT1              *pu1NextTlv = NULL;    /* this pointer points to the 
                                               memory address where next tlv 
                                               has to be added */

    UINT1              *pu1BasePtr = NULL;    /* this pointer always
                                               point to the starting 
                                               address of the linear
                                               buffer */

    UINT2               u2NextOffset = 0;    /* offset in bytes by which the
                                               linear buffer is moved in the
                                               a function (called from this 
                                               function) */

    UINT4               u4AddedTlvsByteCount = 0;    /* number of bytes to be
                                                       copied to the CRU buffer */

    UINT2               u2OptionalTlvsByteCount = 0;    /* total byte occupied by
                                                           optional tlvs */

    if (pPortInfo->pPreFormedLldpdu == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_CRITICAL_TRC,
                  "LldpTxUtlConstructPreformedBuf: "
                  "pPreFormedLldpdu linear buffer in Local PortInfo Table is NULL\r\n");
        return OSIX_FAILURE;
    }

    /* clear linear buffer */
    MEMSET (pPortInfo->pPreFormedLldpdu, 0, LLDP_MAX_PDU_SIZE);
    MEMSET (gau1Lldpdu, 0, LLDP_MAX_PDU_SIZE);
    pPortInfo->u4PreFormedLldpduLen = 0;
    pu1NextTlv = gau1Lldpdu;
    pu1BasePtr = gau1Lldpdu;

    if (LldpTxUtlAddPduHeader (pPortInfo) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC | LLDP_CRITICAL_TRC,
                  "LldpTxUtlConstructPreformedBuf "
                  "LldpTxUtlAddPduHeader Failed \r\n");
        return OSIX_FAILURE;
    }

    if (LldpTxUtlAddMandatoryTlvs (pPortInfo, pu1NextTlv, &u2NextOffset,
                                   u1FrameType) != OSIX_SUCCESS)
    {
        MEMSET (pPortInfo->pPreFormedLldpdu, 0, LLDP_MAX_PDU_SIZE);
        pPortInfo->u4PreFormedLldpduLen = 0;
        LLDP_TRC (ALL_FAILURE_TRC,
                  "LldpTxUtlConstructPreformedBuf: Adding mandatory"
                  " TLVs Failed \r\n");
        return OSIX_FAILURE;
    }
    /* move buffer pointer to point to next to ttl tlv */
    LLDP_INCR_BUF_PTR (pu1NextTlv, u2NextOffset);

    /* if frametype is shutdown frame, add endof lldpdu tlv and copy linear 
     * buffer to CRU buffer */
    if (u1FrameType == LLDP_SHUTDOWN_FRAME)
    {
        LldpTxUtlAddEndOfLLdpduTlv (pu1NextTlv, &u2NextOffset);

        /* move buffer pointer to point to end of pdu */
        LLDP_INCR_BUF_PTR (pu1NextTlv, u2NextOffset);
        /* Copy the preformed linear buffer to CRU buffer */
        LLDP_GET_LINEAR_BUFF_BYTE_COUNT (pu1BasePtr, pu1NextTlv,
                                         &u4AddedTlvsByteCount);

        MEMCPY (&(pPortInfo->pPreFormedLldpdu[pPortInfo->u4PreFormedLldpduLen]),
                gau1Lldpdu, u4AddedTlvsByteCount);
        pPortInfo->u4PreFormedLldpduLen += u4AddedTlvsByteCount;
        return OSIX_SUCCESS;
    }
    LldpTxUtlAddBasicOptionalTlvs (pPortInfo, pu1BasePtr, pu1NextTlv,
                                   &u2NextOffset);
    /* move buffer pointer points to end of management address tlv,
       where DOT1 organizationally specific tlvs have to be added */

    LLDP_INCR_BUF_PTR (pu1NextTlv,
                       LLDP_MAX_BYTES_MINUS_ONE (u2NextOffset,
                                                 LLDP_MAX_PDU_SIZE));
    u2OptionalTlvsByteCount += u2NextOffset;

    /* if pdu size exceeds max allowed pdu size */
    if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
    {
        if (LldpHandlePduSizeExceeded (pPortInfo, pu1BasePtr, pu1NextTlv,
                                       u2OptionalTlvsByteCount) != OSIX_SUCCESS)
        {
            MEMSET (pPortInfo->pPreFormedLldpdu, 0, LLDP_MAX_PDU_SIZE);
            pPortInfo->u4PreFormedLldpduLen = 0;
            LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpTxUtlConstructPreformedBuf: "
                           "Handling max pdu size exceeded Failed on port %d\r\n",
                           pPortInfo->i4IfIndex);
            return OSIX_FAILURE;
        }
        return OSIX_SUCCESS;
    }
    /* now buffer pointer points to end of management address tlv */
    /* add IEEE 802.1 Organizationally Specific TLVs */
    LldpTxUtlAddDot1OrgSpecTlvs (pPortInfo, pu1BasePtr, pu1NextTlv,
                                 &u2NextOffset);
    /* move buffer pointer points to end of Dot1 Tlvs,
       where DOT3 organizationally specific tlvs have to be added */
    LLDP_INCR_BUF_PTR (pu1NextTlv,
                       LLDP_MAX_BYTES_MINUS_ONE (u2NextOffset,
                                                 LLDP_MAX_PDU_SIZE));
    u2OptionalTlvsByteCount += u2NextOffset;

    /* if pdu size exceeds max allowed pdu size */
    if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
    {
        if (LldpHandlePduSizeExceeded (pPortInfo, pu1BasePtr, pu1NextTlv,
                                       u2OptionalTlvsByteCount) != OSIX_SUCCESS)
        {
            MEMSET (pPortInfo->pPreFormedLldpdu, 0, LLDP_MAX_PDU_SIZE);
            pPortInfo->u4PreFormedLldpduLen = 0;
            LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpTxUtlConstructPreformedBuf: "
                           "Handling max pdu size exceeded Failed on port %d\r\n",
                           pPortInfo->i4IfIndex);
            return OSIX_FAILURE;
        }
        return OSIX_SUCCESS;
    }
    /* now buffer pointer points to end of Dot1 Tlvs */
    /* add IEEE 802.3 Organizationally Specific TLVs */
    LldpTxUtlAddDot3OrgSpecTlvs (pPortInfo, pu1BasePtr, pu1NextTlv,
                                 &u2NextOffset);
    /* move buffer pointer points to end of Dot3 Tlvs */
    LLDP_INCR_BUF_PTR (pu1NextTlv, u2NextOffset);
    u2OptionalTlvsByteCount += u2NextOffset;
    /* if pdu size exceeds max allowed pdu size */
    if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
    {
        if (LldpHandlePduSizeExceeded (pPortInfo, pu1BasePtr, pu1NextTlv,
                                       u2OptionalTlvsByteCount) != OSIX_SUCCESS)
        {
            MEMSET (pPortInfo->pPreFormedLldpdu, 0, LLDP_MAX_PDU_SIZE);
            pPortInfo->u4PreFormedLldpduLen = 0;
            LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpTxUtlConstructPreformedBuf: "
                           "Handling max pdu size exceeded Failed on port %d\r\n",
                           pPortInfo->i4IfIndex);
            return OSIX_FAILURE;
        }
        return OSIX_SUCCESS;
    }

    /* now buffer pointer points to end of Dot3 Tlvs */

    /* Add all the application registered TLVs */
    LldpTxUtlAddAppSpecTlvs (pPortInfo, pu1BasePtr, pu1NextTlv,
                             &u2NextOffset, u1FrameType);

    /* move buffer pointer points to end of application Tlvs */
    LLDP_INCR_BUF_PTR (pu1NextTlv, u2NextOffset);
    u2OptionalTlvsByteCount += u2NextOffset;
    /* if pdu size exceeds max allowed pdu size */
    if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
    {
        if (LldpHandlePduSizeExceeded (pPortInfo, pu1BasePtr, pu1NextTlv,
                                       u2OptionalTlvsByteCount) != OSIX_SUCCESS)
        {
            MEMSET (pPortInfo->pPreFormedLldpdu, 0, LLDP_MAX_PDU_SIZE);
            pPortInfo->u4PreFormedLldpduLen = 0;
            LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpTxUtlConstructPreformedBuf: "
                           "Handling max pdu size exceeded Failed on port %d\r\n",
                           pPortInfo->i4IfIndex);
            return OSIX_FAILURE;
        }
        return OSIX_SUCCESS;
    }
    /* LLDP-MED TLVs has to be advertised only when LLDP-MED packet is 
     * received from End point device. bMedCapable flag will set to 
     * TRUE once we receive the Media Capability TLV from the end point 
     * device. So always check whether MED Tlvs has to be included for 
     * this agent or not. */
    if (pPortInfo->bMedCapable == LLDP_MED_TRUE)
    {
        LldpMedAddMedOrgSpecTlvs (pPortInfo, pu1BasePtr, pu1NextTlv,
                                  &u2NextOffset);
        /* Move buffer pointer points to end of LLDP-MED Tlvs */
        LLDP_INCR_BUF_PTR (pu1NextTlv, u2NextOffset);
        u2OptionalTlvsByteCount += u2NextOffset;
        /* If pdu size exceeds max allowed pdu size */
        if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
        {
            if (LldpHandlePduSizeExceeded (pPortInfo, pu1BasePtr, pu1NextTlv,
                                           u2OptionalTlvsByteCount) !=
                OSIX_SUCCESS)
            {
                MEMSET (pPortInfo->pPreFormedLldpdu, 0, LLDP_MAX_PDU_SIZE);
                pPortInfo->u4PreFormedLldpduLen = 0;
                LLDP_TRC_ARG1 (ALL_FAILURE_TRC,
                               "LldpTxUtlConstructPreformedBuf: "
                               "Handling max pdu size exceeded Failed on port %d\r\n",
                               pPortInfo->i4IfIndex);
            }
            return OSIX_SUCCESS;
        }
    }
    /* If all tlvs are added successfully, then add the endof lldpdu tlv */
    /* Now buffer pointer points to end of LLDP-MED Tlvs */
    LldpTxUtlAddEndOfLLdpduTlv (pu1NextTlv, &u2NextOffset);
    /* Move buffer pointer to point to end of lldppdu tlv(ie end of pdu) */
    LLDP_INCR_BUF_PTR (pu1NextTlv, u2NextOffset);
    /* Copy the preformed linear buffer to CRU buffer */
    LLDP_GET_LINEAR_BUFF_BYTE_COUNT (pu1BasePtr, pu1NextTlv,
                                     &u4AddedTlvsByteCount);
    MEMCPY (&(pPortInfo->pPreFormedLldpdu[pPortInfo->u4PreFormedLldpduLen]),
            gau1Lldpdu, u4AddedTlvsByteCount);
    pPortInfo->u4PreFormedLldpduLen += u4AddedTlvsByteCount;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddPduHeader
 *
 *    DESCRIPTION      : This function adds LLDPDU header to the constructed
 *                       LLDPDU
 *
 *    INPUT            : pPortInfo - pointer to port information structure
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
LldpTxUtlAddPduHeader (tLldpLocPortInfo * pPortInfo)
{
    tCfaIfInfo          CfaIfInfo;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    if (LldpPortGetIfInfo (pPortInfo->i4IfIndex, &CfaIfInfo) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpTxUtlAddPduHeader: "
                  "LldpPortGetIfInfo returns FAILURE\n");
        return OSIX_FAILURE;
    }

    if (CfaIfInfo.u1IfType == CFA_ENET)
    {
        if (LldpTxPrependEnetv2Header (pPortInfo) != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC,
                      "LldpTxUtlAddPduHeader: LldpTxPrependEnetv2Header "
                      "returned Failure\n");
            return OSIX_FAILURE;
        }
    }
    else if ((CfaIfInfo.u1IfType == CFA_TOKENRING) ||
             (CfaIfInfo.u1IfType == CFA_FDDI))
    {
        if (LldpTxPrependSnapHeader (pPortInfo) != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC,
                      "LldpTxUtlAddPduHeader: LldpTxPrependSnapHeader returned "
                      "Failure\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpHandlePduSizeExceeded 
 *
 *    DESCRIPTION      : This function Handles max pdu size exceeded scenario
 *
 *    INPUT            : pPortInfo - pointer to the port information structure
 *                       pu1BasePtr - pointer to the base address of linear 
 *                                    buffer, in which the pdu has to be filled
 *                                    in.
 *                       pu1BuffPtr - pointer to the linear buffer, in which 
 *                                    the pdu has to be filled in.
 *                       u2OptionalTlvsByteCount - count in bytes of which 
 *                                                 optional tlvs bytes are 
 *                                                 added to the linear buffer
 *                       
 *    OUTPUT           : pu1BuffPtr - pointer to linear buffer in which the pdu
 *                                    is filled in
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
LldpHandlePduSizeExceeded (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                           UINT1 *pu1BuffPtr, UINT2 u2OptionalTlvsByteCount)
{
    UINT2               u2NextOffset = 0;
    UINT4               u4AddedTlvsByteCount = 0;
    UINT2               u2PortIdLen = 0;
    tLldpExceedFrameSize ExceedFrameSize;
    tLldpLocPortTable  *pPortEntry = NULL;
    tLldpLocPortTable   PortEntry;

    MEMSET (&PortEntry, 0, sizeof (tLldpLocPortTable));
    PortEntry.i4IfIndex = pPortInfo->i4IfIndex;
    pPortEntry = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, (tRBElem *) & PortEntry);
    if (pPortEntry == NULL)        /*For Klocwork */
    {
        return OSIX_FAILURE;
    }

    MEMSET (&ExceedFrameSize, 0, sizeof (tLldpExceedFrameSize));

    /* reset flag */
    pPortInfo->bMaxPduSizeExceeded = OSIX_FALSE;

    /* copy the preformed linear buffer to CRU buffer */
    /* get the number of byte to be copied to the linear buffer */
    LLDP_GET_LINEAR_BUFF_BYTE_COUNT (pu1BasePtr, pu1BuffPtr,
                                     &u4AddedTlvsByteCount);

    MEMCPY (&(pPortInfo->pPreFormedLldpdu[pPortInfo->u4PreFormedLldpduLen]),
            gau1Lldpdu, u4AddedTlvsByteCount);
    pPortInfo->u4PreFormedLldpduLen += u4AddedTlvsByteCount;
    /* add end of lldpdu tlv */
    /* local buffer pointer remains unchanged, so directly pass the 
     * pointer to add endof lldpdu tlv */
    pu1BuffPtr -= u2OptionalTlvsByteCount;
    LldpTxUtlAddEndOfLLdpduTlv (pu1BuffPtr, &u2NextOffset);
    /* move buffer pointer to end of pdu */
    LLDP_INCR_BUF_PTR (pu1BuffPtr, u2NextOffset);

    /* Since pdu size exceeded the max allowed size, only mandatory TLVs 
     * are added to the preformed buffer */
    /* copy local port id */
    if (LldpUtilGetPortIdLen (pPortEntry->i4LocPortIdSubtype,
                              pPortEntry->au1LocPortId,
                              &u2PortIdLen) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpHandlePduSizeExceeded: "
                  "LldpUtilGetPortIdLen returns FAILURE\r\n");
        return OSIX_FAILURE;
    }
    MEMCPY (ExceedFrameSize.au1LocPortId,
            pPortEntry->au1LocPortId,
            MEM_MAX_BYTES (u2PortIdLen, LLDP_MAX_LEN_PORTID));
    /* local port num */
    ExceedFrameSize.u4LocPortNum = pPortInfo->u4LocPortNum;
    ExceedFrameSize.i4LocPortIdSubtype = pPortEntry->i4LocPortIdSubtype;
    /* send trap to NMS */
    LldpSendNotification ((VOID *) &ExceedFrameSize, LLDP_EXCEED_FRAME_SIZE);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    :LldpTxUtlAddMandatoryTlvs 
 *
 *    DESCRIPTION      : Adds mandatory tlvs(chassis id, port id, ttl) to the 
 *                       linear buffer
 *
 *    INPUT            : pPortInfo - pointer to the port information structure
 *                       pu1Buf - pointer to linear buffer, in which the pdu 
 *                                has to be filled in.
 *                       pu2NextOffset - offset in bytes by which the linear 
 *                                       buffer is moved in this function
 *                       u1FrameType - type of the frame to be constructed
 *                       
 *    OUTPUT           : pu1Buf - pointer to linear buffer in which the pdu is 
 *                                filled in
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddMandatoryTlvs (tLldpLocPortInfo * pPortInfo, UINT1 *pu1Buf,
                           UINT2 *pu2NextOffset, UINT1 u1FrameType)
{
    UINT2               u2LocOffset = 0;    /* number of bytes occupied by 
                                               mandatory tlvs  */
    UINT1              *pu1InitialPtr = pu1Buf;

    *pu2NextOffset = 0;
    /* add chassis id tlv add chassis id tlv  */
    if (LldpTxUtlAddChassisIdTlv (pu1Buf, &u2LocOffset) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  "LldpTxUtlAddMandatoryTlvs: "
                  "LldpTxUtlAddChassisIdTlv returns FAILURE \r\n");
        return OSIX_FAILURE;
    }

    /* move buffer pointer to point to end of chassis id tlv */
    LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);
    if (LldpTxUtlAddPortIdTlv (pu1Buf, &u2LocOffset, pPortInfo) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  "LldpTxUtlAddMandatoryTlvs: "
                  "LldpTxUtlAddPortIdTlv returns FAILURE \r\n");
        return OSIX_FAILURE;
    }
    /* move buffer pointer to point to end of port id tlv */
    LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);
    LldpTxUtlAddTtlTlv (pu1Buf, &u2LocOffset, u1FrameType);
    /* move ptr to end of ttl tlv */
    LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);

    /* next offset is (start addr - current addr) of buffer ptr */
    *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddChassisIdTlv 
 *
 *    DESCRIPTION      : This function adds chassis id tlv to the linear buffer
 *
 *    INPUT            : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv has to be addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by 
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv is addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddChassisIdTlv (UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    UINT2               u2TlvHeader = 0;
    UINT2               u2ChassIdTlvInfoLen = 0;
    UINT2               u2ChassIdTlvLen = 0;
    UINT2               u2ChassIdLen = 0;

    LLDP_TRC (LLDP_MANDATORY_TLV_TRC,
              "LldpTxUtlAddChassisIdTlv: Adding Chassis ID TLV to "
              "preformed buffer... \r\n");
    *pu2LocOffset = 0;
    if (LldpUtilGetChassisIdLen
        (gLldpGlobalInfo.LocSysInfo.i4LocChassisIdSubtype,
         gLldpGlobalInfo.LocSysInfo.au1LocChassisId,
         &u2ChassIdLen) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  "LldpTxUtlAddPortIdTlv:"
                  "LldpUtilGetPortIdLen returns FAILURE\r\n");
        return OSIX_FAILURE;
    }
    u2ChassIdTlvInfoLen = (UINT2) (u2ChassIdLen + LLDP_TLV_SUBTYPE_LEN);
    u2ChassIdTlvLen = (UINT2) (LLDP_TLV_HEADER_LEN + u2ChassIdTlvInfoLen);
    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_CHASSIS_ID_TLV, u2ChassIdTlvInfoLen,
                          &u2TlvHeader);
    /* put tlv header into linear buffert */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* put chassis id subtype in linear buffer */
    LLDP_PUT_1BYTE (pu1Buf,
                    (UINT1) gLldpGlobalInfo.LocSysInfo.i4LocChassisIdSubtype);
    /* copy chassis id into linear buffer */
    MEMCPY (pu1Buf, gLldpGlobalInfo.LocSysInfo.au1LocChassisId,
            MEM_MAX_BYTES ((u2ChassIdTlvInfoLen - LLDP_TLV_SUBTYPE_LEN),
                           LLDP_MAX_LEN_CHASSISID));
    /* store the chassis id tlv len as next offset */
    *pu2LocOffset = u2ChassIdTlvLen;
    LLDP_TRC (LLDP_MANDATORY_TLV_TRC,
              "LldpTxUtlAddChassisIdTlv: Added Chassis ID TLV to "
              "preformed buffer successfully \r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddPortIdTlv 
 *
 *    DESCRIPTION      : This function adds port id tlv to the linear buffer
 *
 *    INPUT            : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv has to be addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by 
 *                                      which the linear pointer is moved in
 *                                      this function
 *                       pPortInfo - pointer to port information structure
 *
 *    OUTPUT           : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv is addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddPortIdTlv (UINT1 *pu1Buf, UINT2 *pu2LocOffset,
                       tLldpLocPortInfo * pPortInfo)
{
    UINT2               u2TlvHeader = 0;
    UINT2               u2PortIdTlvInfoLen = 0;
    UINT2               u2PortIdTlvLen = 0;
    UINT2               u2PortIdLen = 0;
    tLldpLocPortTable  *pPortEntry = NULL;
    tLldpLocPortTable   PortEntry;

    MEMSET (&PortEntry, 0, sizeof (tLldpLocPortTable));
    PortEntry.i4IfIndex = pPortInfo->i4IfIndex;
    pPortEntry = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, (tRBElem *) & PortEntry);
    if (pPortEntry == NULL)        /*For Klocwork */
    {
        return OSIX_FAILURE;
    }

    LLDP_TRC_ARG1 (LLDP_MANDATORY_TLV_TRC,
                   "LldpTxUtlAddPortIdTlv: Adding Port ID TLV to preformed "
                   "buffer for port %d...\r\n", pPortInfo->u4LocPortNum);
    *pu2LocOffset = 0;
    if (LldpUtilGetPortIdLen (pPortEntry->i4LocPortIdSubtype,
                              pPortEntry->au1LocPortId,
                              &u2PortIdLen) != OSIX_SUCCESS)
    {
        LLDP_TRC (LLDP_MANDATORY_TLV_TRC, "LldpTxUtlAddPortIdTlv:"
                  "LldpUtilGetPortIdLen returns FAILURE\r\n");
        return OSIX_FAILURE;
    }

    u2PortIdTlvInfoLen = (UINT2) (LLDP_TLV_SUBTYPE_LEN + u2PortIdLen);
    u2PortIdTlvLen = (UINT2) (LLDP_TLV_HEADER_LEN + u2PortIdTlvInfoLen);
    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_PORT_ID_TLV, u2PortIdTlvInfoLen,
                          &u2TlvHeader);
    /* put tlv header in linear buffer */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* put port id subtype in linear buffer */
    LLDP_PUT_1BYTE (pu1Buf, (UINT1) pPortEntry->i4LocPortIdSubtype);
    /* copy port id into linear buffer */
    MEMCPY (pu1Buf, pPortEntry->au1LocPortId,
            MEM_MAX_BYTES ((u2PortIdTlvInfoLen - LLDP_TLV_SUBTYPE_LEN),
                           LLDP_MAX_LEN_PORTID));

    /* store the port id tlv len as next offset */
    *pu2LocOffset = u2PortIdTlvLen;
    LLDP_TRC_ARG1 (LLDP_MANDATORY_TLV_TRC,
                   "LldpTxUtlAddPortIdTlv: Added Port ID TLV to preformed "
                   "buffer for port %d successfully\r\n",
                   pPortInfo->u4LocPortNum);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddTtlTlv 
 *
 *    DESCRIPTION      : This function adds ttl id tlv to the linear buffer
 *
 *    INPUT            : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv has to be addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by 
 *                                      which the linear pointer is moved in
 *                                      this function
 *                       u1FrameType - type of the frame to be constructed
 *
 *    OUTPUT           : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv is addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC VOID
LldpTxUtlAddTtlTlv (UINT1 *pu1Buf, UINT2 *pu2LocOffset, UINT1 u1FrameType)
{
    UINT2               u2Ttl = 0;
    UINT2               u2TlvHeader = 0;

    LLDP_TRC (LLDP_MANDATORY_TLV_TRC,
              "LldpTxUtlAddTtlTlv: Adding TTL TLV to preformed "
              "buffer... \r\n");
    *pu2LocOffset = 0;
    /* if frametype is information frame */
    if (u1FrameType == LLDP_INFO_FRAME)
    {
        /* LLDP_GET_TIME_TO_LIVE returns TTL
         * TTL = MsgInterval * HoldTimerMultipiler 
         * LLDP_GET_TIME_TO_LIVE returns TTL if (TTL <= 65535)
         * else LLDP_GET_TIME_TO_LIVE returns 65535  */
        u2Ttl = (UINT2) (LLDP_GET_TIME_TO_LIVE ());
    }
    /* else if frame type is shutdown frame 
     * TTL is 0, and it already initialized with 0 */
    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_TIME_TO_LIVE_TLV,
                          (UINT2) LLDP_TTL_TLV_INFO_LEN, &u2TlvHeader);
    /* put tlv header in linear buffer */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* put time to live in linear buffer */
    LLDP_PUT_2BYTE (pu1Buf, u2Ttl);
    /* store the ttl tlv len as next offset */
    *pu2LocOffset = LLDP_TTL_TLV_LEN;
    LLDP_TRC (LLDP_MANDATORY_TLV_TRC,
              "LldpTxUtlAddTtlTlv: Added TTL TLV to preformed "
              "buffer successfully \r\n");
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddBasicOptionalTlvs
 *
 *    DESCRIPTION      : This function adds basic optional tlvs(port desc, 
 *                       sys name, sys desc, sys capabilities and management 
 *                       address) to the linear buffer if they are enabled for 
 *                       transmision
 *
 *    INPUT            : pPortInfo - poitner to the port entry for which the 
 *                       CRU buffer is formed
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddBasicOptionalTlvs (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                               UINT1 *pu1Buf, UINT2 *pu2NextOffset)
{
    UINT2               u2LocOffset = 0;    /* number of bytes occupied by
                                               mandatory tlvs  */
    UINT1              *pu1InitialPtr = pu1Buf;    /* store the initial value 
                                                   of buffer pointer */

    *pu2NextOffset = 0;
    /* check if port description tlv is enabled, if true then add the tlv */
    if (pPortInfo->PortConfigTable.u1TLVsTxEnable & LLDP_PORT_DESC_TLV_ENABLED)
    {
        LldpTxUtlAddPortDescTlv (pPortInfo, pu1BasePtr, pu1Buf, &u2LocOffset);
        if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC,
                      "LldpTxUtlAddBasicOptionalTlvs: Max Pdu size "
                      "exceeded while adding PortDesc TLV \r\n");
            /* set buffer pointer to point next to ttl tlv */
            *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
            return OSIX_SUCCESS;
        }

    }

    /* check if system name tlv is enabled, if true then add the tlv */
    if (pPortInfo->PortConfigTable.u1TLVsTxEnable & LLDP_SYS_NAME_TLV_ENABLED)
    {
        /* move buffer pointer to point to end of port description tlv */
        LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);
        LldpTxUtlAddSysNameTlv (pPortInfo, pu1BasePtr, pu1Buf, &u2LocOffset);
        if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC,
                      "LldpTxUtlAddBasicOptionalTlvs: Max Pdu size "
                      "exceeded while adding SysName TLV \r\n");
            /* set buffer pointer to point next to ttl tlv */
            *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
            return OSIX_SUCCESS;
        }
    }

    /* check if system description tlv is enabled, if true then add the tlv */
    if (pPortInfo->PortConfigTable.u1TLVsTxEnable & LLDP_SYS_DESC_TLV_ENABLED)
    {
        /* move buffer pointer to point to end of system name tlv */
        LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);
        LldpTxUtlAddSysDescTlv (pPortInfo, pu1BasePtr, pu1Buf, &u2LocOffset);
        if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC,
                      "LldpTxUtlAddBasicOptionalTlvs: Max Pdu size "
                      "exceeded while adding SysDesc TLV \r\n");
            /* set buffer pointer to point next to ttl tlv */
            *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
            return OSIX_SUCCESS;
        }
    }

    /* check if system capabilities tlv is enabled, if true then add the tlv */
    if (pPortInfo->PortConfigTable.u1TLVsTxEnable & LLDP_SYS_CAPAB_TLV_ENABLED)
    {
        /* move buffer pointer to point to end of system description tlv */
        LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);
        LldpTxUtlAddSysCapabTlv (pPortInfo, pu1BasePtr, pu1Buf, &u2LocOffset);
        if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC,
                      "LldpTxUtlAddBasicOptionalTlvs: Max Pdu size "
                      "exceeded while adding SysCapab TLV \r\n");
            /* set buffer pointer to point next to ttl tlv */
            *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
            return OSIX_SUCCESS;
        }
    }

    /* move buffer pointer to point to end of system capabilities tlv */
    LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);
    LldpTxUtlAddManAddrTlv (pPortInfo, pu1BasePtr, pu1Buf, &u2LocOffset);
    /* It is not required  to check 
     * if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
     * because the operation needs to be done 
     * if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE) or
     * in normal return case is same (pls refer other places where
     * if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE) is checked)
     * after returning from here, the called function does the appropriate 
     * action if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE).
     * after several review this decision is taken, so pls go through the flow
     * before making any changes here */

    /* move buffer pointer to popint to end of ManAddrTlv */
    LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);
    *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddPortDescTlv 
 *
 *    DESCRIPTION      : This function adds port desc tlv to the linear buffer
 *
 *    INPUT            : pPortInfo - pointer to port information structure
 *                       pu1BasePtr - pointer to base address of linear buffer
 *                       pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv has to be addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by 
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv is addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddPortDescTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                         UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    UINT2               u2TlvHeader = 0;
    UINT2               u2PortDescTlvInfoLen = 0;
    UINT2               u2PortDescTlvLen = 0;
    tLldpLocPortTable  *pPortEntry = NULL;
    tLldpLocPortTable   PortEntry;

    MEMSET (&PortEntry, 0, sizeof (tLldpLocPortTable));
    PortEntry.i4IfIndex = pPortInfo->i4IfIndex;
    pPortEntry = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, (tRBElem *) & PortEntry);
    if (pPortEntry == NULL)        /*For Klocwork */
    {
        return OSIX_FAILURE;
    }

    LLDP_TRC_ARG1 (LLDP_PORT_DESC_TRC,
                   "LldpTxUtlAddPortDescTlv: Adding Port description TLV to "
                   "preformed buffer for port %d...\r\n",
                   pPortInfo->u4LocPortNum);
    *pu2LocOffset = 0;
    LLDP_GET_PORT_DESC_TLV_INFO_LEN (&u2PortDescTlvInfoLen, pPortEntry);
    u2PortDescTlvLen = (UINT2) (LLDP_TLV_HEADER_LEN + u2PortDescTlvInfoLen);
    /* check whether space is available to accomodate port desc tlv */
    if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf, u2PortDescTlvLen) ==
        OSIX_TRUE)
    {
        pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpTxUtlAddPortDescTlv: --- Max Pdu size exceeded ---\r\n");
        pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
        return OSIX_SUCCESS;
    }
    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_PORT_DESC_TLV, u2PortDescTlvInfoLen,
                          &u2TlvHeader);
    /* put tlv header in linear buffer */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* copy port desc into linear buffer */
    /* port desc len is same as u2PortDescTlvInfoLen */
    MEMCPY (pu1Buf, pPortEntry->au1LocPortDesc, u2PortDescTlvInfoLen);
    /* set tlv length as next offset */
    *pu2LocOffset = u2PortDescTlvLen;
    LLDP_TRC_ARG1 (LLDP_PORT_DESC_TRC,
                   "LldpTxUtlAddPortDescTlv: Added Port description TLV "
                   "to preformed buffer for port %d successfully\r\n",
                   pPortInfo->u4LocPortNum);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddSysNameTlv 
 *
 *    DESCRIPTION      : This function adds system name tlv to the linear buffer
 *
 *    INPUT            : pPortInfo - pointer to port information structure
 *                       pu1BasePtr - pointer to base address of linear buffer
 *                       pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv has to be addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by 
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv is addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddSysNameTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                        UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    UINT2               u2SysNameTlvInfoLen = 0;
    UINT2               u2SysNameTlvLen = 0;
    UINT2               u2TlvHeader = 0;

    LLDP_TRC_ARG1 (LLDP_SYS_NAME_TRC,
                   "LldpTxUtlAddSysNameTlv: Adding System name TLV to "
                   "preformed buffer for port %d...\r\n",
                   pPortInfo->u4LocPortNum);
    *pu2LocOffset = 0;
    LLDP_GET_SYS_NAME_TLV_INFO_LEN (&u2SysNameTlvInfoLen);
    u2SysNameTlvLen = (UINT2) (LLDP_TLV_HEADER_LEN + u2SysNameTlvInfoLen);
    /* check whether space is available to accomodate sys name tlv */
    if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf, u2SysNameTlvLen) ==
        OSIX_TRUE)
    {
        pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpTxUtlAddSysNameTlv: --- Max Pdu size exceeded ---\r\n");
        pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
        return OSIX_SUCCESS;
    }
    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_SYS_NAME_TLV, u2SysNameTlvInfoLen,
                          &u2TlvHeader);
    /* put tlv header in linear buffer */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* copy system name into linear buffer */
    /* sys name len is same as u2SysNameTlvInfoLen */
    MEMCPY (pu1Buf, gLldpGlobalInfo.LocSysInfo.au1LocSysName,
            u2SysNameTlvInfoLen);
    /* set tlv length as next offset */
    *pu2LocOffset = u2SysNameTlvLen;
    LLDP_TRC_ARG1 (LLDP_SYS_NAME_TRC,
                   "LldpTxUtlAddSysNameTlv: Added System name TLV to "
                   "preformed buffer for port %d successfully\r\n",
                   pPortInfo->u4LocPortNum);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddSysDescTlv 
 *
 *    DESCRIPTION      : This function adds system description tlv to the 
 *                       linear buffer
 *
 *    INPUT            : pPortInfo - pointer to port information structure
 *                       pu1BasePtr - pointer to base address of linear buffer
 *                       pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv has to be addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by 
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv is addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddSysDescTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                        UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    UINT2               u2SysDescTlvInfoLen = 0;
    UINT2               u2SysDescTlvLen = 0;
    UINT2               u2TlvHeader = 0;

    LLDP_TRC_ARG1 (LLDP_SYS_DESC_TRC,
                   "LldpTxUtlAddSysDescTlv: Adding System description TLV to "
                   "preformed buffer for port %d...\r\n",
                   pPortInfo->u4LocPortNum);
    *pu2LocOffset = 0;
    LLDP_GET_SYS_DESC_TLV_INFO_LEN (&u2SysDescTlvInfoLen);
    u2SysDescTlvLen = (UINT2) (LLDP_TLV_HEADER_LEN + u2SysDescTlvInfoLen);
    /* check whether space is available to accomodate sys desc tlv */
    if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf, u2SysDescTlvLen) ==
        OSIX_TRUE)
    {
        pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpTxUtlAddSysDescTlv: --- Max Pdu size exceeded ---\r\n");
        pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
        return OSIX_SUCCESS;
    }
    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_SYS_DESC_TLV, u2SysDescTlvInfoLen,
                          &u2TlvHeader);
    /* put tlv header in linear buffer */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* copy system name into linear buffer */
    /* sys desc len is same as u2SysDescTlvInfoLen */
    MEMCPY (pu1Buf, gLldpGlobalInfo.LocSysInfo.au1LocSysDesc,
            u2SysDescTlvInfoLen);
    /* set tlv length as next offset */
    *pu2LocOffset = u2SysDescTlvLen;
    LLDP_TRC_ARG1 (LLDP_SYS_DESC_TRC,
                   "LldpTxUtlAddSysDescTlv: Added System description TLV to "
                   "preformed buffer for port %d successfully\r\n",
                   pPortInfo->u4LocPortNum);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddSysCapabTlv 
 *
 *    DESCRIPTION      : This function adds system capabilities tlv to the 
 *                       linear buffer
 *
 *    INPUT            : pPortInfo - pointer to port information structure
 *                       pu1BasePtr - pointer to base address of linear buffer
 *                       pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv has to be addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by 
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv is addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddSysCapabTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                         UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    tLldpOctetStringType InOctetStr;
    tLldpOctetStringType OutOctetStr;
    UINT2               u2TlvHeader = 0;
    UINT1               au1SysCap[ISS_SYS_CAPABILITIES_LEN];

    LLDP_TRC_ARG1 (LLDP_SYS_CAPAB_TRC,
                   "LldpTxUtlAddSysCapabTlv: Adding System capabilities TLV to "
                   "preformed buffer for port %d...\r\n",
                   pPortInfo->u4LocPortNum);
    *pu2LocOffset = 0;
    /* check whether space is available to accomodate sys capab tlv */
    if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                    (UINT2) LLDP_SYS_CAPAB_TLV_LEN) ==
        OSIX_TRUE)
    {
        pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
        pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpTxUtlAddSysCapabTlv: --- Max Pdu size exceeded ---\r\n");
        return OSIX_SUCCESS;
    }
    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_SYS_CAPAB_TLV,
                          LLDP_SYS_CAPAB_TLV_INFO_LEN, &u2TlvHeader);
    /* put tlv header in linear buffer */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* Get the system capabilities supported and convert it to pkt string
     * format */
    InOctetStr.pu1_OctetList =
        &gLldpGlobalInfo.LocSysInfo.au1LocSysCapSupported[0];
    InOctetStr.i4_Length = ISS_SYS_CAPABILITIES_LEN;

    OutOctetStr.pu1_OctetList = &au1SysCap[0];
    OutOctetStr.i4_Length = ISS_SYS_CAPABILITIES_LEN;
    MEMSET (OutOctetStr.pu1_OctetList, 0, OutOctetStr.i4_Length);

    /* Convert the Bit list to pkt string as per 
     * Std IEEE802.1AB-2005 section 9.1 */
    LldpReverseOctetString (&InOctetStr, &OutOctetStr);
    /* put system capabilities supported into linear buffer */
    MEMCPY (pu1Buf, OutOctetStr.pu1_OctetList, OutOctetStr.i4_Length);
    pu1Buf += OutOctetStr.i4_Length;

    /* Get the system capabilities supported and convert it to pkt string
     * format */
    InOctetStr.pu1_OctetList =
        &gLldpGlobalInfo.LocSysInfo.au1LocSysCapEnabled[0];
    InOctetStr.i4_Length = ISS_SYS_CAPABILITIES_LEN;

    OutOctetStr.pu1_OctetList = &au1SysCap[0];
    OutOctetStr.i4_Length = ISS_SYS_CAPABILITIES_LEN;
    MEMSET (OutOctetStr.pu1_OctetList, 0, OutOctetStr.i4_Length);

    /* Convert the Bit list to pkt string as per 
     * Std IEEE802.1AB-2005 section 9.1 */
    LldpReverseOctetString (&InOctetStr, &OutOctetStr);
    /* put system capabilities enabled into linear buffer */
    MEMCPY (pu1Buf, OutOctetStr.pu1_OctetList, OutOctetStr.i4_Length);
    pu1Buf += OutOctetStr.i4_Length;

    *pu2LocOffset = (UINT2) LLDP_SYS_CAPAB_TLV_LEN;
    LLDP_TRC_ARG1 (LLDP_SYS_CAPAB_TRC,
                   "LldpTxUtlAddSysCapabTlv: Added System capabilities TLV to "
                   "preformed buffer for port %d successfully\r\n",
                   pPortInfo->u4LocPortNum);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddManAddrTlv 
 *
 *    DESCRIPTION      : This function adds management address tlv to the 
 *                       linear buffer
 *
 *    INPUT            : pPortInfo - pointer to port information structure
 *                       pu1BasePtr - pointer to base address of linear buffer
 *                       pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv has to be addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by 
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv is addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddManAddrTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                        UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    UINT2               u2ManAddrTlvInfoLen = 0;
    UINT2               u2ManAddrTlvLen = 0;
    UINT2               u2TlvHeader = 0;
    UINT4               u4ManAddrIfId = 0;
    UINT1               u1ManAddrStrLen = 0;    /* ManAddr Subtype 
                                                   + ManAddr Length */
    UINT1               au1ManAddrOidStr[LLDP_MAX_LEN_MAN_OID + 1];
    UINT1               au1ZeroAddr[IPVX_MAX_INET_ADDR_LEN] = { 0 };
    UINT1               u1ManAddrOidStrLength = 0;    /* Default Value - 
                                                       Donot change this 
                                                       value */

    tLldpLocManAddrTable *pManAddr = NULL;    /* stores management address node */
    BOOL1               bResult = OSIX_FALSE;    /* flag that indicates whether 
                                                   management address tlv is
                                                   enabled on the port or not */
    UINT1              *pu1InitialPtr = pu1Buf;    /* store the initial value 
                                                   of buffer pointer */

    *pu2LocOffset = 0;
    MEMSET (au1ManAddrOidStr, 0, LLDP_MAX_LEN_MAN_OID + 1);

    /* Get the first managemet address node from RBTree */
    pManAddr = (tLldpLocManAddrTable *) RBTreeGetFirst
        (gLldpGlobalInfo.LocManAddrRBTree);
    /* if no management address is configured, ie if no management address
     * entry present in the RBTree, check whether management address TLV is
     * enabled for this port, if yes construct management address TLV with 
     * system mac address, else return success */
    if (pManAddr == NULL)
    {
        if (pPortInfo->u1ManAddrTlvTxEnabled == LLDP_FALSE)
        {
            return OSIX_SUCCESS;
        }
        LldpTxUtlAddSysMacAddr (pPortInfo, pu1Buf, pu2LocOffset);
        LLDP_INCR_BUF_PTR (pu1Buf, *pu2LocOffset);
        return OSIX_SUCCESS;
    }

    do
    {
        /* check whether this management address is enabled for transmission 
           and its Operational Status is enabled */
        OSIX_BITLIST_IS_BIT_SET (pManAddr->au1LocManAddrPortsTxEnable,
                                 (UINT2) pPortInfo->u4LocPortNum,
                                 (INT4)
                                 sizeof (pManAddr->au1LocManAddrPortsTxEnable),
                                 bResult);
        if ((bResult == OSIX_FALSE) ||
            (pManAddr->u1OperStatus != IPIF_OPER_ENABLE))
        {
            continue;
        }
        LLDP_TRC_ARG1 (LLDP_MAN_ADDR_TRC,
                       "LldpTxUtlAddManAddrTlv: Adding Managementaddress TLV "
                       "to preformed buffer for port %d...\r\n",
                       pPortInfo->u4LocPortNum);
        u1ManAddrStrLen = (UINT1) pManAddr->i4LocManAddrLen;
        /* Here u1ManAddrStrLen contains the ManAddr Subtype Len + ManAddr 
         * Len */
        /* Get the OID String Length */
        if (LldpUtilEncodeManAddrOid (&(pManAddr->LocManAddrOID),
                                      &au1ManAddrOidStr[0]) == OSIX_SUCCESS)
        {
            u1ManAddrOidStrLength = au1ManAddrOidStr[0];
        }

        LLDP_GET_MAN_ADDR_TLV_INFO_LEN (&u2ManAddrTlvInfoLen, u1ManAddrStrLen,
                                        u1ManAddrOidStrLength);

        u2ManAddrTlvLen = (UINT2) (LLDP_TLV_HEADER_LEN + u2ManAddrTlvInfoLen);
        /* check whether space is available to accomodate man addr tlv */
        if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf, u2ManAddrTlvLen)
            == OSIX_TRUE)
        {
            pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
            pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
            *pu2LocOffset = (UINT2) (pu1Buf - pu1InitialPtr);
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "LldpTxUtlAddManAddrTlv: Max Pdu size exceeded\r\n");
            return OSIX_SUCCESS;
        }

        LLDP_FORM_TLV_HEADER ((UINT2) LLDP_MAN_ADDR_TLV, u2ManAddrTlvInfoLen,
                              &u2TlvHeader);
        /* put tlv header in linear buffer */
        LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
        /* put management address str len in linear buffer */
        LLDP_PUT_1BYTE (pu1Buf, u1ManAddrStrLen);
        /* put management address subtype in linear buffer */
        LLDP_PUT_1BYTE (pu1Buf, (UINT1) pManAddr->i4LocManAddrSubtype);
        if ((MEMCMP (gLldpGlobalInfo.MgmtAddr.au1Addr, au1ZeroAddr,
                     gLldpGlobalInfo.MgmtAddr.u1AddrLen)) != 0 &&
            (gLldpGlobalInfo.i4TagStatus == LLDP_TAG_ENABLE))
        {
            /* Configured Management IP address is not 0.0.0.0 and tagged
             * LLDP feature is enabled in the SYSTEM */
            MEMCPY (pu1Buf, gLldpGlobalInfo.MgmtAddr.au1Addr,
                    (pManAddr->i4LocManAddrLen - LLDP_TLV_SUBTYPE_LEN));
        }
        else
        {
            MEMCPY (pu1Buf, pManAddr->au1LocManAddr,
                    (pManAddr->i4LocManAddrLen - LLDP_TLV_SUBTYPE_LEN));
        }
        LLDP_INCR_BUF_PTR (pu1Buf, (UINT2)
                           (pManAddr->i4LocManAddrLen - LLDP_TLV_SUBTYPE_LEN));
        /* put management address interface number subtype  in linear buffer */
        LLDP_PUT_1BYTE (pu1Buf, (UINT1) pManAddr->i4LocManAddrIfSubtype);
        /* put management address interface number in linear buffer */
        u4ManAddrIfId = (UINT4) pManAddr->i4LocManAddrIfId;
        LLDP_PUT_4BYTE (pu1Buf, u4ManAddrIfId);
        /* put management address OID string length in linear buffer */
        if (u1ManAddrOidStrLength == 0)
        {
            LLDP_PUT_1BYTE (pu1Buf, u1ManAddrOidStrLength);
        }
        else
        {
            /* Copy the entire OID String Length */
            LLDP_PUT_1BYTE (pu1Buf, u1ManAddrOidStrLength);

            /* Copy the entire OID String */
            if (u1ManAddrOidStrLength < (LLDP_MAX_LEN_MAN_OID + 1))
            {
                MEMCPY (pu1Buf, &au1ManAddrOidStr[1], u1ManAddrOidStrLength);
                LLDP_INCR_BUF_PTR (pu1Buf, u1ManAddrOidStrLength);
            }
        }
        LLDP_TRC_ARG1 (LLDP_MAN_ADDR_TRC,
                       "LldpTxUtlAddManAddrTlv: Added Managementaddress TLV to "
                       "preformed buffer for port %d successfully \r\n",
                       pPortInfo->u4LocPortNum);
    }
    while ((pManAddr = (tLldpLocManAddrTable *)
            RBTreeGetNext (gLldpGlobalInfo.LocManAddrRBTree,
                           (tRBElem *) pManAddr, NULL)) != NULL);

    /* set next offset as (current value - initial value) of
     * buffer pointer */
    *pu2LocOffset = (UINT2) (pu1Buf - pu1InitialPtr);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddSysMacAddr 
 *
 *    DESCRIPTION      : This function adds system mac address to management 
 *                       address tlv
 *
 *    INPUT            : pPortInfo - pointer to port information structure
 *                       pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv has to be addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by 
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv is addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
LldpTxUtlAddSysMacAddr (tLldpLocPortInfo * pPortInfo, UINT1 *pu1Buf,
                        UINT2 *pu2LocOffset)
{
    tMacAddr            SwitchMacAddr;
    UINT2               u2ManAddrTlvInfoLen = 0;
    UINT2               u2ManAddrTlvLen = 0;
    UINT1               u1ManAddrStrLen = 0;
    UINT2               u2TlvHeader = 0;
    UINT4               u4ManAddrIfId = 0;

    LLDP_TRC_ARG1 (LLDP_MAN_ADDR_TRC,
                   "LldpTxUtlAddSysMacAddr: No management address entry "
                   "present in LocManAddrRBTree. So,adding system mac address "
                   "as management address in ManAddr TLV for port %d... "
                   " \r\n", pPortInfo->u4LocPortNum);
    *pu2LocOffset = 0;
    MEMSET (SwitchMacAddr, 0, sizeof (tMacAddr));
    /* get sys mac address */
    LldpPortGetSysMacAddr (SwitchMacAddr);
    u1ManAddrStrLen = (UINT1) (LLDP_TLV_SUBTYPE_LEN + MAC_ADDR_LEN);
    /* szieof (man addr strlen) + man addr strlen + 
     * sizeof (interface numbering subtype) + sizeof (interface number) +
     * sizeof (OID str len) */
    u2ManAddrTlvInfoLen = (UINT2) (sizeof (u1ManAddrStrLen) + u1ManAddrStrLen +
                                   sizeof (UINT1) + sizeof (UINT4) +
                                   sizeof (UINT1));
    u2ManAddrTlvLen = (UINT2) (LLDP_TLV_HEADER_LEN + u2ManAddrTlvInfoLen);

    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_MAN_ADDR_TLV, u2ManAddrTlvInfoLen,
                          &u2TlvHeader);
    /* put tlv header in linear buffer */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* put management address str length in linear buffer */
    LLDP_PUT_1BYTE (pu1Buf, u1ManAddrStrLen);
    /* put management address subtype in linear buffer */
    LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_MAN_ADDR_SUB_TYPE_OTHER);
    /* cpoy sys mac addr as management address into linear buffer */
    MEMCPY (pu1Buf, &SwitchMacAddr, MAC_ADDR_LEN);
    LLDP_INCR_BUF_PTR (pu1Buf, (UINT2) MAC_ADDR_LEN);
    /* put management address interface number subtype  in linear buffer */
    LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_MAN_ADDR_IF_SUB_UNKNOWN);
    /* put management address interface number(0) in linear buffer */
    u4ManAddrIfId = 0;
    LLDP_PUT_4BYTE (pu1Buf, u4ManAddrIfId);
    /* put management address OID string length(0) in linear buffer */
    LLDP_PUT_1BYTE (pu1Buf, 0);
    LLDP_TRC_ARG1 (LLDP_MAN_ADDR_TRC,
                   "LldpTxUtlAddSysMacAddr: Added system mac address as"
                   "Managementaddress TLV to preformed buffer for port %d "
                   "successfully \r\n", pPortInfo->u4LocPortNum);
    *pu2LocOffset = u2ManAddrTlvLen;

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddDot1OrgSpecTlvs
 *
 *    DESCRIPTION      : Adds IEE 802.1 organizationally specific optional
 *                       tlvs(port vlan id, port vlan name, port and protocol 
 *                       vlan id and protocol identity) to the linear buffer
 *
 *    INPUT            : pPortInfo - pointer to port information structure
 *                       pu1BasePtr - pointer to base address of linear buffer
 *                       pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv has to be addded
 *                       pu2NextOffset - pointer to offset(offset in bytes by
 *                                       which the linear pointer is moved in
 *                                       this function)
 *    OUTPUT           : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv is addded
 *                       pu2NextOffset - pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function)
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddDot1OrgSpecTlvs (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                             UINT1 *pu1Buf, UINT2 *pu2NextOffset)
{
    UINT1              *pu1InitialPtr = pu1Buf;    /* store the memory address
                                                   where the buffer pointer
                                                   points to */

    UINT2               u2LocOffset = 0;    /* offset in bytes of which the 
                                               linear buffer is filled in 
                                               the called function */
    tLldpLocPortTable  *pPortTable = NULL;
    tLldpLocPortTable   PortTable;

    MEMSET (&PortTable, 0, sizeof (tLldpLocPortTable));
    *pu2NextOffset = 0;

    /* add port vlan id tlv */
    if (pPortInfo->u1PortVlanTxEnable == LLDP_TRUE)
    {
        LldpTxUtlAddPortVlanIdTlv (pPortInfo, pu1BasePtr, pu1Buf, &u2LocOffset);
        if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC,
                      "\r(LldpTxUtlAddDot1OrgSpecTlvs) Max Pdu size exceeded "
                      "while adding PortVlanId TLV \r\n");
            *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
            return OSIX_SUCCESS;
        }
    }

    /* move buffer pointer to point to end of PortVlanIdTlv */
    LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);
    LldpTxUtlAddProtoVlanIdTlv (pPortInfo, pu1BasePtr, pu1Buf, &u2LocOffset);

    /* if max pdu size exceeded return to calling function, this scenario is
     * taken care in that function(ConstructpreformedPdu) */
    if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC,
                  "\r(LldpTxUtlAddDot1OrgSpecTlvs) Max Pdu size exceeded "
                  "while adding ProtoVlanId TLV \r\n");
        *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
        return OSIX_SUCCESS;
    }

    /* move buffer pointer to point to end of ProtoVlanIdTlv */
    LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);
    /* add vlan name tlv */
    LldpTxUtlAddPortVlanNameTlv (pPortInfo, pu1BasePtr, pu1Buf, &u2LocOffset);

    /* if max pdu size exceeded return to calling function, this scenario is 
     * taken care in that function(ConstructpreformedPdu) */
    if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC,
                  "\r(LldpTxUtlAddDot1OrgSpecTlvs) Max Pdu size "
                  "exceeded while adding PortVlanName TLV \r\n");
        *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
        return OSIX_SUCCESS;
    }
    /* move buffer pointer to point to end of VlanNameTlv */
    LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);
    /* add proto id tlv */
    LldpTxUtlAddProtoIdTlv (pPortInfo, pu1BasePtr, pu1Buf, &u2LocOffset);
    /* if max pdu size exceeded return to calling function, this scenario is 
     * taken care in that function(ConstructpreformedPdu) */
    if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC,
                  "\r(LldpTxUtlAddDot1OrgSpecTlvs) Max Pdu size exceeded "
                  "while adding ProtoId TLV \r\n");
        *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
        return OSIX_SUCCESS;
    }
    /* move buffer pointer to point to end of ProtoIdTlv */
    LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);

    PortTable.i4IfIndex = pPortInfo->i4IfIndex;
    pPortTable = (tLldpLocPortTable *) RBTreeGet
        (gLldpGlobalInfo.LldpPortInfoRBTree, &PortTable);
    if (pPortTable == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC,
                  "\rtLldpLocPortTable entry not found\r\n ");
        return OSIX_FAILURE;
    }
    /* VID usage digest TLV */
    if ((pPortTable->u1VidUsageDigestTlvTxEnabled == OSIX_TRUE) &&
        (gLldpGlobalInfo.i4Version == LLDP_VERSION_09))
    {
        /* add Vid usage Digest agg tlv */
        LldpTxUtlAddVidUsageDigestTlv (pPortInfo, pu1BasePtr, pu1Buf,
                                       &u2LocOffset);
        if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC,
                      "\r(LldpTxUtlAddDot1OrgSpecTlvs) Max Pdu size "
                      "exceeded while adding VidUsageDigest TLV \r\n");
            *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
            return OSIX_SUCCESS;
        }
    }
    /* move buffer pointer to point to end of VidUsageDigest Tlv */
    LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);

    /* Management VID TLV */
    if ((pPortTable->u1ManVidTlvTxEnabled == OSIX_TRUE) &&
        (gLldpGlobalInfo.i4Version == LLDP_VERSION_09))
    {
        LldpTxUtlAddMgmtVidTlv (pPortInfo, pu1BasePtr, pu1Buf, &u2LocOffset);
        if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC,
                      "\r(LldpTxUtlAddDot1OrgSpecTlvs) Max Pdu size "
                      "exceeded while adding MgmtVid TLV \r\n");
            *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
            return OSIX_SUCCESS;
        }
    }
    /* move buffer pointer to point to end of MgmtVid Tlv */
    LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);
    u2LocOffset = 0;
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_09)
    {
        /* Link aggregation TLV */
        if (pPortInfo->Dot3LocPortInfo.
            u1TLVTxEnable & LLDP_LINK_AGG_TLV_ENABLED)
        {
            /* add link agg tlv */
            LldpTxUtlAddLinkAggTlv (pPortInfo, pu1BasePtr, pu1Buf,
                                    &u2LocOffset);
            if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
            {
                LLDP_TRC (LLDP_CRITICAL_TRC,
                          "\r(LldpTxUtlAddDot1OrgSpecTlvs) Max Pdu size "
                          "exceeded while adding LinkAgg TLV \r\n");
                *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
                return OSIX_SUCCESS;
            }
        }
    }
    /* move buffer pointer to point to end of LinkAgg tlv */
    LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);
    *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddPortVlanIdTlv 
 *
 *    DESCRIPTION      : This function adds port vlan tlv to the 
 *                       linear buffer
 *
 *    INPUT            : pPortInfo - pointer to port information structure
 *                       pu1BasePtr - pointer to base address of linear buffer
 *                       pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv has to be addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by 
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv is addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddPortVlanIdTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                           UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    UINT2               u2TlvHeader = 0;
    UINT2               u2PVId = 0;

    *pu2LocOffset = 0;
    LLDP_TRC_ARG1 (LLDP_PORT_VLAN_TRC,
                   "\r(LldpTxUtlAddPortVlanIdTlv) Adding PortVlanId "
                   "TLV to preformed buffer for port %d"
                   "\r\n", pPortInfo->u4LocPortNum);
    /* check the linear buffer can accomodate port vlan id tlv */
    if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                    LLDP_PVID_TLV_LEN) == OSIX_TRUE)
    {
        pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
        pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpTxUtlAddPortVlanIdTlv: Max Pdu size exceeded" "!!!..\n");
        return OSIX_SUCCESS;
    }
    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_ORG_SPEC_TLV,
                          (UINT2) LLDP_PVID_TLV_INFO_LEN, &u2TlvHeader);
    /* put tlv header in linear buffer */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* copy 802.1 OUI into linear buffer */
    MEMCPY (pu1Buf, gau1LldpDot1OUI, LLDP_MAX_LEN_OUI);
    LLDP_INCR_BUF_PTR (pu1Buf, (UINT2) LLDP_MAX_LEN_OUI);
    /* put Dot1 subtype in linear buffer */
    LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_PORT_VLAN_ID_TLV);
    /* put PVID in linear buffer */
    u2PVId = pPortInfo->u2PVid;
    LLDP_PUT_2BYTE (pu1Buf, u2PVId);
    /* set next pointer as port vlan id len */
    *pu2LocOffset = LLDP_PVID_TLV_LEN;
    LLDP_TRC_ARG1 (LLDP_PORT_VLAN_TRC,
                   "\r(LldpTxUtlAddPortVlanIdTlv) Added PortVlanId TLV to "
                   "preformed buffer for port %d successfully\r\n",
                   pPortInfo->u4LocPortNum);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddProtoVlanIdTlv 
 *
 *    DESCRIPTION      : This function adds proto vlan id tlv to the 
 *                       linear buffer
 *
 *    INPUT            : pPortInfo - pointer to port information structure
 *                       pu1BasePtr - pointer to base address of linear buffer
 *                       pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv has to be addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by 
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv is addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddProtoVlanIdTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                            UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    tTMO_DLL_NODE      *pProtoVlanNode = NULL;
    tLldpxdot1LocProtoVlanInfo *pProtoVlanInfo = NULL;
    UINT2               u2TlvHeader = 0;
    UINT2               u2ProtoVlanId = 0;
    UINT1               u1Flag = 0;
    UINT1              *pu1InitialPtr = pu1Buf;

    *pu2LocOffset = 0;
    LLDP_TRC_ARG1 (LLDP_PPVLAN_TRC,
                   "\r(LldpTxUtlAddProtoVlanIdTlv) Adding ProtoVlanId TLV "
                   "to preformed buffer for port %d\r\n",
                   pPortInfo->u4LocPortNum);
    /* Scan the ProtoVlanInfo DLL */
    TMO_DLL_Scan (&(pPortInfo->LocProtoVlanDllList),
                  pProtoVlanNode, tTMO_DLL_NODE *)
    {
        pProtoVlanInfo = (tLldpxdot1LocProtoVlanInfo *) pProtoVlanNode;

        if (pProtoVlanInfo->u1ProtoVlanTxEnabled == LLDP_FALSE)
        {
            continue;
        }
        /* check the linear buffer can accomodate proto vlan id tlv */
        if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                        LLDP_PPVID_TLV_LEN) == OSIX_TRUE)
        {
            pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
            pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
            *pu2LocOffset = (UINT2) (pu1Buf - pu1InitialPtr);
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "LldpTxUtlAddProtoVlanIdTlv: Max Pdu size exceeded"
                      "!!!..\n");
            return OSIX_SUCCESS;
        }
        LLDP_FORM_TLV_HEADER ((UINT2) LLDP_ORG_SPEC_TLV,
                              (UINT2) LLDP_PPVID_TLV_INFO_LEN, &u2TlvHeader);
        /* put tlv header into linear buffer */
        LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
        /* copy 802.1 OUI into linear buffer */
        MEMCPY (pu1Buf, gau1LldpDot1OUI, LLDP_MAX_LEN_OUI);
        LLDP_INCR_BUF_PTR (pu1Buf, LLDP_MAX_LEN_OUI);
        /* put Dot1 subtype in linear buffer */
        LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_PROTO_VLAN_ID_TLV);
        if (gLldpGlobalInfo.u1ProtoVlanSupported == LLDP_TRUE)
        {
            u1Flag = (u1Flag | LLDP_PROTOVLAN_SUPPORTED);
        }
        if (pPortInfo->u1ProtoVlanEnabled == LLDP_TRUE)
        {
            u1Flag = (u1Flag | LLDP_PROTOVLAN_ENABLED);
        }
        LLDP_PUT_1BYTE (pu1Buf, u1Flag)
            /* put PPVID in linear buffer */
            u2ProtoVlanId = pProtoVlanInfo->u2ProtoVlanId;
        LLDP_PUT_2BYTE (pu1Buf, u2ProtoVlanId);
    }

    *pu2LocOffset = (UINT2) (pu1Buf - pu1InitialPtr);
    LLDP_TRC_ARG1 (LLDP_PPVLAN_TRC,
                   "\r(LldpTxUtlAddProtoVlanIdTlv) Added ProtoVlanId TLV "
                   "to preformed buffer for port %d successfully\r\n",
                   pPortInfo->u4LocPortNum);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddPortVlanNameTlv 
 *
 *    DESCRIPTION      : This function adds proto vlan id tlv to the 
 *                       linear buffer
 *
 *    INPUT            : pPortInfo - pointer to port information structure
 *                       pu1BasePtr - pointer to base address of linear buffer
 *                       pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv has to be addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by 
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv is addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddPortVlanNameTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                             UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    UINT2               u2VlanNameCount = 0;
    UINT2               u2TlvHeader = 0;
    UINT2               u2VlanNameTlvInfoLen = 0;
    UINT2               u2VlanNameTlvLen = 0;
    UINT2               u2Count = 0;
    UINT2               u2VlanNameLen = 0;
    UINT1              *pu1InitialPtr = pu1Buf;

    MEMSET (gVlanNameInfo, 0, (sizeof (tVlanNameInfo) * VLAN_DEV_MAX_NUM_VLAN));

    *pu2LocOffset = 0;
    LLDP_TRC_ARG1 (LLDP_VLAN_NAME_TRC,
                   "\r(LldpTxUtlAddPortVlanNameTlv) Adding PortVlanName "
                   "TLV to preformed buffer for port %d\r\n",
                   pPortInfo->u4LocPortNum);
    LldpVlndbGetAllVlanName ((UINT4) pPortInfo->i4IfIndex, &gVlanNameInfo[0],
                             &u2VlanNameCount);

    for (u2Count = 0; ((u2Count < u2VlanNameCount) &&
                       (u2Count < VLAN_DEV_MAX_NUM_VLAN)); u2Count++)
    {
        LLDP_GET_VLAN_NAME_TLV_INFO_LEN (&u2VlanNameTlvInfoLen,
                                         gVlanNameInfo[u2Count]);
        u2VlanNameTlvLen = (UINT2) (LLDP_TLV_HEADER_LEN + u2VlanNameTlvInfoLen);
        /* check the linear buffer can accomodate proto vlan id tlv */
        if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                        u2VlanNameTlvLen) == OSIX_TRUE)
        {
            pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
            pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
            *pu2LocOffset = (UINT2) (pu1Buf - pu1InitialPtr);
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "LldpTxUtlAddPortVlanNameTlv: Max Pdu size exceeded"
                      "!!!..\n");
            return OSIX_SUCCESS;
        }
        LLDP_FORM_TLV_HEADER ((UINT2) LLDP_ORG_SPEC_TLV, u2VlanNameTlvInfoLen,
                              &u2TlvHeader);
        /* put tlv header into linear buffer */
        LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
        /* copy proto 802.1 OUI into linear buffer */
        MEMCPY (pu1Buf, gau1LldpDot1OUI, LLDP_MAX_LEN_OUI);
        LLDP_INCR_BUF_PTR (pu1Buf, (UINT2) LLDP_MAX_LEN_OUI);
        /* put Dot1 subtype in linear buffer */
        LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_VLAN_NAME_TLV);
        /* Vlan Id into linear buffer */
        LLDP_PUT_2BYTE (pu1Buf, gVlanNameInfo[u2Count].u2VlanId);
        /* put vlan name length into linear buffer */
        u2VlanNameLen = (UINT2) LLDP_STRLEN (gVlanNameInfo[u2Count].au1VlanName,
                                             VLAN_STATIC_MAX_NAME_LEN);
        LLDP_PUT_1BYTE (pu1Buf, (UINT1) u2VlanNameLen);
        /* put vlan name into linear buffer */
        MEMCPY (pu1Buf, gVlanNameInfo[u2Count].au1VlanName,
                MEM_MAX_BYTES (u2VlanNameLen, VLAN_STATIC_MAX_NAME_LEN));
        LLDP_INCR_BUF_PTR (pu1Buf, u2VlanNameLen);
    }

    *pu2LocOffset = (UINT2) (pu1Buf - pu1InitialPtr);
    LLDP_TRC_ARG1 (LLDP_VLAN_NAME_TRC,
                   "\r(LldpTxUtlAddPortVlanNameTlv) Added PortVlanName "
                   "TLV to preformed buffer for port %d successfully\r\n",
                   pPortInfo->u4LocPortNum);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddProtoIdTlv 
 *
 *    DESCRIPTION      : This function adds proto id tlv to the 
 *                       linear buffer
 *
 *    INPUT            : pPortInfo - pointer to port information structure
 *                       pu1BasePtr - pointer to base address of linear buffer
 *                       pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv has to be addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by 
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv is addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddProtoIdTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                        UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    UINT2               u2ProtoIdTlvInfoLen = 0;
    UINT2               u2ProtoIdTlvLen = 0;
    UINT2               u2TlvHeader = 0;
    UINT2               u2ProtoIdLen = 0;
    UINT1              *pu1InitialPtr = pu1Buf;

    tLldpxdot1LocProtoIdInfo *pProtoId = NULL;
    tLldpxdot1LocProtoIdInfo nextProtoId;

    *pu2LocOffset = 0;
    LLDP_TRC_ARG1 (LLDP_PROTO_ID_TRC,
                   "\r(LldpTxUtlAddPortVlanNameTlv) Adding ProtoId TLV "
                   "to preformed buffer for port"
                   "%d\r\n", pPortInfo->u4LocPortNum);
    /* Get the proto id  node from RBTree */
    pProtoId =
        (tLldpxdot1LocProtoIdInfo *) RBTreeGetFirst (gLldpGlobalInfo.
                                                     LocProtoIdInfoRBTree);
    /* if no proto id entry present in the RBTree, then return SUCCESS */
    if (pProtoId == NULL)
    {
        LLDP_TRC_ARG1 (LLDP_PROTO_ID_TRC,
                       "\r(LldpTxUtlAddPortVlanNameTlv) no proto id "
                       "entry present in the RBTree"
                       "%d\r\n", pPortInfo->u4LocPortNum);
        return OSIX_SUCCESS;
    }

    do
    {
        MEMSET (&nextProtoId, 0, sizeof (tLldpxdot1LocProtoIdInfo));
        MEMCPY (&nextProtoId, pProtoId, sizeof (tLldpxdot1LocProtoIdInfo));

        LLDP_GET_PROTO_ID_TLV_LEN (&u2ProtoIdTlvLen, pProtoId);
        u2ProtoIdTlvInfoLen = (UINT2) (u2ProtoIdTlvLen - LLDP_TLV_HEADER_LEN);
        /* check the linear buffer can accomodate proto vlan id tlv */
        if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                        u2ProtoIdTlvLen) == OSIX_TRUE)
        {
            pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
            pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
            *pu2LocOffset = (UINT2) (pu1Buf - pu1InitialPtr);
            LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "LldpTxUtlAddProtoIdTlv: Max Pdu size exceeded"
                      "!!!..\n");
            return OSIX_SUCCESS;
        }
        LLDP_FORM_TLV_HEADER ((UINT2) LLDP_ORG_SPEC_TLV, u2ProtoIdTlvInfoLen,
                              &u2TlvHeader);
        /* put tlv header in linear buffer */
        LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
        /* copy 802.1 OUI into linear buffer */
        MEMCPY (pu1Buf, gau1LldpDot1OUI, LLDP_MAX_LEN_OUI);
        LLDP_INCR_BUF_PTR (pu1Buf, LLDP_MAX_LEN_OUI);
        /* put Dot1 subtype in linear buffer */
        LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_PROTO_ID_TLV);
        /* put proto id length into linear buffer */
        u2ProtoIdLen = (UINT2) LLDP_STRLEN (nextProtoId.au1ProtocolId,
                                            LLDP_MAX_LEN_PROTOID);
        LLDP_PUT_1BYTE (pu1Buf, (UINT1) u2ProtoIdLen);
        /* put proto identity into linear buffer */
        MEMCPY (pu1Buf, nextProtoId.au1ProtocolId, u2ProtoIdLen);
        LLDP_INCR_BUF_PTR (pu1Buf, u2ProtoIdLen);

        /* get next proto id entry */
        pProtoId = (tLldpxdot1LocProtoIdInfo *) RBTreeGetNext
            (gLldpGlobalInfo.LocProtoIdInfoRBTree, (tRBElem *) & nextProtoId,
             NULL);
    }
    while (pProtoId != NULL);

    *pu2LocOffset = (UINT2) (pu1Buf - pu1InitialPtr);
    LLDP_TRC_ARG1 (LLDP_PROTO_ID_TRC,
                   "\r(LldpTxUtlAddPortVlanNameTlv) Added ProtoId TLV to "
                   "preformed buffer for port %d successfully\r\n",
                   pPortInfo->u4LocPortNum);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddVidUsageDigestTlv 
 *
 *    DESCRIPTION      : This function adds VID Usage Digest tlv to the 
 *                       linear buffer
 *
 *    INPUT            : pPortInfo - pointer to port information structure
 *                       pu1BasePtr - pointer to base address of linear buffer
 *                       pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv has to be addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by 
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv is addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddVidUsageDigestTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                               UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    tLldpLocPortTable  *pLldpLocPortTable;    /* Pointer to the port spec. structure
                                               on which the agent resides */
    tLldpLocPortInfo   *pLocalPortInfo = NULL;
    tLldpv2AgentToLocPort *pLldpAgentToLocPort = NULL;
    tLldpv2AgentToLocPort LldpAgentToLocPort;
    tLldpLocPortTable   LldpLocPortTable;
    UINT4               u4Val = 0;
    UINT2               u2VidTlvInfoLen = 0;
    UINT2               u2VidTlvLen = 0;
    UINT2               u2TlvHeader = 0;
    UINT1              *pu1InitialPtr = pu1Buf;

    MEMSET (&LldpLocPortTable, 0, sizeof (tLldpLocPortTable));
    MEMSET (&LldpAgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));

    *pu2LocOffset = 0;
    LldpLocPortTable.i4IfIndex = pPortInfo->i4IfIndex;
    pLldpLocPortTable = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpLocPortTable);
    if (pLldpLocPortTable == NULL)    /* For Klocwork */
    {
        *pu2LocOffset = (UINT2) (pu1Buf - pu1InitialPtr);
        return OSIX_FAILURE;
    }
    u4Val = pPortInfo->u4LocVidUsageDigest;
    LldpAgentToLocPort.i4IfIndex = pPortInfo->i4IfIndex;
    MEMCPY (LldpAgentToLocPort.Lldpv2DestMacAddress, gau1LldpMcastAddr,
            MAC_ADDR_LEN);

    pLldpAgentToLocPort = (tLldpv2AgentToLocPort *)
        RBTreeGet (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                   &LldpAgentToLocPort);

    if ((pLldpAgentToLocPort != NULL) &&
        (pLldpAgentToLocPort->u4DstMacAddrTblIndex !=
         pPortInfo->u4DstMacAddrTblIndex))
    {
        pLocalPortInfo =
            LLDP_GET_LOC_PORT_INFO (pLldpAgentToLocPort->u4LldpLocPort);
        if (pLocalPortInfo != NULL)
        {
            u4Val = pLocalPortInfo->u4LocVidUsageDigest;
        }
    }
    LLDP_TRC_ARG1 (LLDP_PROTO_ID_TRC,
                   "\r(LldpTxUtlAddVidUsageDigestTlv) Adding VID Usgae Digest TLV "
                   "to preformed buffer for port"
                   "%d\r\n", pPortInfo->u4LocPortNum);

    LLDP_GET_VID_USAGE_DIGEST_TLV_LEN (&u2VidTlvLen);
    u2VidTlvInfoLen = (UINT2) (u2VidTlvLen - LLDP_TLV_HEADER_LEN);
    /* check the linear buffer can accomodate proto vlan id tlv */
    if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                    u2VidTlvLen) == OSIX_TRUE)
    {
        pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
        pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
        *pu2LocOffset = (UINT2) (pu1Buf - pu1InitialPtr);
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpTxUtlAddVidUsageDigestTlv: Max Pdu size exceeded"
                  "!!!..\n");
        return OSIX_SUCCESS;
    }
    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_ORG_SPEC_TLV, u2VidTlvInfoLen,
                          &u2TlvHeader);
    /* put tlv header in linear buffer */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* copy 802.1 OUI into linear buffer */
    MEMCPY (pu1Buf, gau1LldpDot1OUI, LLDP_MAX_LEN_OUI);
    LLDP_INCR_BUF_PTR (pu1Buf, LLDP_MAX_LEN_OUI);
    /* put Dot1 subtype in linear buffer */
    LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_VID_USAGE_DIGEST_TLV);
    /* put proto identity into linear buffer */
    LLDP_PUT_4BYTE (pu1Buf, u4Val);

    *pu2LocOffset = (UINT2) (pu1Buf - pu1InitialPtr);
    LLDP_TRC_ARG1 (LLDP_PROTO_ID_TRC,
                   "\r(LldpTxUtlAddPortVlanNameTlv) Added ProtoId TLV to "
                   "preformed buffer for port %d successfully\r\n",
                   pPortInfo->u4LocPortNum);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddMgmtVidTlv
 *
 *    DESCRIPTION      : This function adds Mgmt VID tlv to the 
 *                       linear buffer
 *
 *    INPUT            : pPortInfo - pointer to port information structure
 *                       pu1BasePtr - pointer to base address of linear buffer
 *                       pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv has to be addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by 
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv is addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddMgmtVidTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                        UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    tLldpLocPortTable  *pLldpLocPortTable;    /* Pointer to the port spec. structure
                                               on which the agent resides */
    tLldpLocPortTable   LldpLocPortTable;
    UINT2               u2TempManVid = 0;
    UINT2               u2MgmtVidTlvInfoLen = 0;
    UINT2               u2MgmtVidTlvLen = 0;
    UINT2               u2TlvHeader = 0;
    UINT1              *pu1InitialPtr = pu1Buf;

    MEMSET (&LldpLocPortTable, 0, sizeof (tLldpLocPortTable));
    LldpLocPortTable.i4IfIndex = pPortInfo->i4IfIndex;
    pLldpLocPortTable = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpLocPortTable);
    if (pLldpLocPortTable == NULL)    /* For Klocwork */
    {
        *pu2LocOffset = (UINT2) (pu1Buf - pu1InitialPtr);
        return OSIX_FAILURE;
    }
    *pu2LocOffset = 0;
    LLDP_TRC_ARG1 (LLDP_PROTO_ID_TRC,
                   "\r(LldpTxUtlAddMgmtVidTlv) Adding VID Usgae Digest TLV "
                   "to preformed buffer for port"
                   "%d\r\n", pPortInfo->u4LocPortNum);

    LLDP_GET_MGMT_VID_TLV_LEN (&u2MgmtVidTlvLen);
    u2MgmtVidTlvInfoLen = (UINT2) (u2MgmtVidTlvLen - LLDP_TLV_HEADER_LEN);
    /* check the linear buffer can accomodate proto vlan id tlv */
    if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                    u2MgmtVidTlvLen) == OSIX_TRUE)
    {
        pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
        pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
        *pu2LocOffset = (UINT2) (pu1Buf - pu1InitialPtr);
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpTxUtlAddMgmtVidTlv: Max Pdu size exceeded" "!!!..\n");
        return OSIX_SUCCESS;
    }

    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_ORG_SPEC_TLV, u2MgmtVidTlvInfoLen,
                          &u2TlvHeader);
    /* put tlv header in linear buffer */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* copy 802.1 OUI into linear buffer */
    MEMCPY (pu1Buf, gau1LldpDot1OUI, LLDP_MAX_LEN_OUI);
    LLDP_INCR_BUF_PTR (pu1Buf, LLDP_MAX_LEN_OUI);
    /* put Dot1 subtype in linear buffer */
    LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_MGMT_VID_TLV);
    /* put proto identity into linear buffer */
    u2TempManVid = (UINT2) pLldpLocPortTable->u4LocMgmtVid;
    LLDP_PUT_2BYTE (pu1Buf, u2TempManVid);

    *pu2LocOffset = (UINT2) (pu1Buf - pu1InitialPtr);
    LLDP_TRC_ARG1 (LLDP_PROTO_ID_TRC,
                   "\r(LldpTxUtlAddMgmtVidTlv) Added ProtoId TLV to "
                   "preformed buffer for port %d successfully\r\n",
                   pPortInfo->u4LocPortNum);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddDot3OrgSpecTlvs
 *
 *    DESCRIPTION      : Adds IEE 802.3 organizationally specific optional
 *                       tlvs(MAC/PHY config/status, power via mdi, 
 *                       Link aggregation, Max frame size) to the linear buffer
 *
 *    INPUT            : pPortInfo - poitner to the port entry for which the
 *                       CRU buffer is formed
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddDot3OrgSpecTlvs (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                             UINT1 *pu1Buf, UINT2 *pu2NextOffset)
{
    UINT1              *pu1InitialPtr = pu1Buf;    /* store the memory address where the
                                                   buffer pointer points to */
    UINT2               u2LocOffset = 0;    /* offset in bytes of which the linear buffer is
                                               filled with dot3 tlvs */

    *pu2NextOffset = 0;
    /* check if MAC_PHY tlv is enabled, if true then add the tlv.
     * MAC PHY TLV is mandatory to be included in LLDP-MED LLDPDU. Hence add the
     * tlv when Med Capability is enabled */
    if ((pPortInfo->Dot3LocPortInfo.u1TLVTxEnable & LLDP_MAC_PHY_TLV_ENABLED) ||
        ((pPortInfo->bMedCapable == LLDP_MED_TRUE) &&
         (pPortInfo->LocMedCapInfo.
          u2MedLocCapTxEnable & LLDP_MED_CAPABILITY_TLV)))
    {
        /* add mac/phy config/status tlv */
        LldpTxUtlAddMacPhyTlv (pPortInfo, pu1BasePtr, pu1Buf, &u2LocOffset);
        if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC,
                      "\r(LldpTxUtlAddDot3OrgSpecTlvs) Max Pdu size "
                      "exceeded while adding MacPhy TLV \r\n");
            *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
            return OSIX_SUCCESS;
        }
    }

    if (pPortInfo->Dot3LocPortInfo.u1TLVTxEnable & LLDP_PWR_VIA_MDI_TLV_ENABLED)
    {
        /* move buffer pointer to point to end of mac/phy tlv */
        LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);
        /* add power via mdi tlv */
        LldpTxUtlAddPwrViaMdiTlv (pPortInfo, pu1BasePtr, pu1Buf, &u2LocOffset);
        if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC,
                      "\r(LldpTxUtlAddDot3OrgSpecTlvs) Max Pdu size exceeded "
                      "while adding PwrViaMdi TLV \r\n");
            *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
            return OSIX_SUCCESS;
        }
    }

    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
    {
        /* Link aggregation TLV */
        if (pPortInfo->Dot3LocPortInfo.
            u1TLVTxEnable & LLDP_LINK_AGG_TLV_ENABLED)
        {
            /* move buffer pointer to point to end of LinkAgg tlv */
            LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);
            /* add link agg tlv */
            LldpTxUtlAddLinkAggTlv (pPortInfo, pu1BasePtr, pu1Buf,
                                    &u2LocOffset);
            if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
            {
                LLDP_TRC (LLDP_CRITICAL_TRC,
                          "\r(LldpTxUtlAddDot1OrgSpecTlvs) Max Pdu size "
                          "exceeded while adding LinkAgg TLV \r\n");
                *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
                return OSIX_SUCCESS;
            }
        }
    }
    if (pPortInfo->Dot3LocPortInfo.u1TLVTxEnable &
        LLDP_MAX_FRAME_SIZE_TLV_ENABLED)
    {
        /* move buffer pointer to point to end of Link Aggregation tlv */
        LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);
        /* add max frame size tlv */
        LldpTxUtlAddMaxFrameSizeTlv (pPortInfo, pu1BasePtr, pu1Buf,
                                     &u2LocOffset);
        if (pPortInfo->bMaxPduSizeExceeded == OSIX_TRUE)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC,
                      "\r(LldpTxUtlAddDot3OrgSpecTlvs) Max Pdu size "
                      "exceeded while adding MaxFrameSize TLV \r\n");
            *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
            return OSIX_SUCCESS;
        }
    }
    /* move to end of pdu */
    LLDP_INCR_BUF_PTR (pu1Buf, u2LocOffset);
    *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddAppSpecTlvs
 *
 *    DESCRIPTION      : Adds the application specific optional
 *                       tlvs to the linear buffer
 *
 *    INPUT            : pPortInfo - pointer to port information structure
 *                       pu1BasePtr - pointer to base address of linear buffer
 *                       pu1Buf - pointer to linear buffer in which chassis id
 *                                tlv has to be addded
 *                       pu2NextOffset - pointer to offset(offset in bytes by 
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv is addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddAppSpecTlvs (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                         UINT1 *pu1Buf, UINT2 *pu2NextOffset, UINT1 u1FrameType)
{
    UINT1              *pu1InitialPtr = pu1Buf;    /* store the memory address where the
                                                   buffer pointer points to */
    tLldpAppInfo        TmpLldpAppNode;
    tLldpAppInfo       *pLldpAppNode = NULL;
    /*Making array as static to prevent StackSize warning */
    static UINT1        u1localBuf[5 * LLDP_MAX_APP_TLV_LEN];
    UINT1              *pu1localBuf = NULL;
    UINT2               u2TlvMaxLen = sizeof (u1localBuf);
    UINT2              *pu2TlvMaxLen = &u2TlvMaxLen;

    MEMSET (u1localBuf, 0, 5 * LLDP_MAX_APP_TLV_LEN);
    pu1localBuf = u1localBuf;
    *pu2NextOffset = 0;

    /* 1. Get the applications registered in pPortInfo->u4LocPortNum
     * 2. If TX status of application TLV is enabled then copy the TLV 
     *    into LLDP PDU.
     * 3. Proceed to the Next application, Repeat till all the registered
     *    applications are traversed.
     */

    MEMSET (&TmpLldpAppNode, 0, sizeof (tLldpAppInfo));
    /* Get the first application register on this port by specifying
     * only port number as part of the index of application RB Tree */
    TmpLldpAppNode.u4PortId = (UINT4) pPortInfo->i4IfIndex;

    pLldpAppNode = (tLldpAppInfo *) RBTreeGetNext (gLldpGlobalInfo.AppRBTree,
                                                   (tRBElem *) & TmpLldpAppNode,
                                                   NULL);

    if (pLldpAppNode == NULL)
    {
        /* No application is registered, so return */
        return OSIX_SUCCESS;
    }

    do
    {

        /* Ensure only applications registered on the current local port
         * only is processed. If the port number changes it implies all 
         * the registered applications are processed.*/

        if (pLldpAppNode->u4PortId != (UINT4) pPortInfo->i4IfIndex)
        {
            LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                           "\r(LldpTxUtlAddAppSpecTlvs) All application TLV "
                           " nodes processed for port"
                           "%d\r\n", pPortInfo->u4LocPortNum);
            break;
        }
        if ((pLldpAppNode->bAppTlvTxStatus == OSIX_TRUE) &&
            (pLldpAppNode->u4LldpInstSelector ==
             pPortInfo->u4DstMacAddrTblIndex))
        {
            /* Application TLV needs to added to the preformed buffer */
            /* check the linear buffer can accomodate this tlv */
            if (pLldpAppNode->u1TakeTlvFrmApp == OSIX_TRUE)
            {
                /*Taken from application */
                pLldpAppNode->pAppCallBackTakeTlvFrmAppFn (pu1localBuf,
                                                           pu2TlvMaxLen,
                                                           (UINT4) pPortInfo->
                                                           i4IfIndex,
                                                           u1FrameType);
            }
            else
            {
                /*Taken from node */
                MEMCPY (pu1localBuf, pLldpAppNode->pu1TxAppTlv,
                        pLldpAppNode->u2TxAppTlvLen);
                u2TlvMaxLen = pLldpAppNode->u2TxAppTlvLen;
            }
            if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                            u2TlvMaxLen) == OSIX_TRUE)
            {
                pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
                pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
                LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "LldpTxUtlAddAppSpecTlvs: Max Pdu size "
                          "exceeded!!!..\n");
                return OSIX_SUCCESS;
            }

            /* Copy the application TLV at the end of the linear buffer */
            MEMCPY (pu1Buf, pu1localBuf, u2TlvMaxLen);

            /* Move the pointer to end of the linear buffer */
            LLDP_INCR_BUF_PTR (pu1Buf, u2TlvMaxLen);
        }

        /* Get the next application in the port */
        pLldpAppNode = (tLldpAppInfo *) RBTreeGetNext
            (gLldpGlobalInfo.AppRBTree, (tRBElem *) pLldpAppNode, NULL);
    }
    while (pLldpAppNode != NULL);

    /* Increment the offset accordingly */
    *pu2NextOffset = (UINT2) (pu1Buf - pu1InitialPtr);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddMacPhyTlv 
 *
 *    DESCRIPTION      : This function adds Mac/Phy tlv to the 
 *                       linear buffer
 *
 *    INPUT            : pPortInfo - pointer to port information structure
 *                       pu1BasePtr - pointer to base address of linear buffer
 *                       pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv has to be addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by 
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv is addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddMacPhyTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                       UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    tLldpOctetStringType InOctetStr;
    tLldpOctetStringType OutOctetStr;
    UINT1               au1InAutoNegCap[LLDP_MAX_LEN_PHY_MEDIA_CAP];
    UINT1               au1OutAutoNegCap[LLDP_MAX_LEN_PHY_MEDIA_CAP];
    UINT2               u2TlvHeader = 0;
    UINT2               u2OperMauType = 0;
    UINT2               u2AutoNegAdvtCap = 0;
    UINT1               u1Flag = 0;

    MEMSET (&InOctetStr, 0, sizeof (tLldpOctetStringType));
    MEMSET (&OutOctetStr, 0, sizeof (tLldpOctetStringType));
    MEMSET (&au1InAutoNegCap[0], 0, LLDP_MAX_LEN_PHY_MEDIA_CAP);
    MEMSET (&au1OutAutoNegCap[0], 0, LLDP_MAX_LEN_PHY_MEDIA_CAP);

    InOctetStr.pu1_OctetList = &au1InAutoNegCap[0];
    InOctetStr.i4_Length = LLDP_MAX_LEN_PHY_MEDIA_CAP;
    OutOctetStr.pu1_OctetList = &au1OutAutoNegCap[0];
    OutOctetStr.i4_Length = LLDP_MAX_LEN_PHY_MEDIA_CAP;

    *pu2LocOffset = 0;
    LLDP_TRC_ARG1 (LLDP_MAC_PHY_TRC,
                   "\r(LldpTxUtlAddMacPhyTlv) Adding MacPhy TLV to "
                   "preformed buffer for port %d\r\n", pPortInfo->u4LocPortNum);
    /* check the linear buffer can accomodate mac/phy tlv */
    if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                    LLDP_MAC_PHY_TLV_LEN) == OSIX_TRUE)
    {
        pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
        pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpTxUtlAddMacPhyTlv: Max Pdu size exceeded!!!..\n");
        return OSIX_SUCCESS;
    }
    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_ORG_SPEC_TLV,
                          (UINT2) LLDP_MAC_PHY_TLV_INFO_LEN, &u2TlvHeader);
    /* put tlv header into linear buffer */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* copy 802.3 OUI into linear buffer */
    MEMCPY (pu1Buf, gau1LldpDot3OUI, LLDP_MAX_LEN_OUI);
    /* increment buffer pointer */
    LLDP_INCR_BUF_PTR (pu1Buf, (UINT2) LLDP_MAX_LEN_OUI)
        /* put Dot3 subtype in linear buffer */
        LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_MAC_PHY_CONFIG_STATUS_TLV);
    /* put auto-nego support/status */
    if (pPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo.u1AutoNegSupport ==
        LLDP_TRUE)
    {
        u1Flag = (u1Flag | LLDP_AUTONEG_SUPPORTED);
    }
    if (pPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo.u1AutoNegEnabled ==
        LLDP_TRUE)
    {
        u1Flag = (u1Flag | LLDP_AUTONEG_ENABLED);
    }
    LLDP_PUT_1BYTE (pu1Buf, u1Flag);

    /* put auto neg advertised capabilities */
    u2AutoNegAdvtCap =
        pPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo.u2AutoNegAdvtCap;
    PTR_ASSIGN2 (InOctetStr.pu1_OctetList, u2AutoNegAdvtCap);
    InOctetStr.i4_Length = sizeof (UINT2);

    /* Convert the Bit list to pkt string as per 
     * Std IEEE802.1AB-2005 section 9.1 */
    LldpReverseOctetString (&InOctetStr, &OutOctetStr);
    MEMCPY (pu1Buf, OutOctetStr.pu1_OctetList, OutOctetStr.i4_Length);
    pu1Buf += OutOctetStr.i4_Length;

    /* put oper mau type  - 2octets */
    u2OperMauType = (pPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo.u2OperMauType);
    LLDP_PUT_2BYTE (pu1Buf, u2OperMauType);
    *pu2LocOffset = LLDP_MAC_PHY_TLV_LEN;
    LLDP_TRC_ARG1 (LLDP_MAC_PHY_TRC,
                   "\r(LldpTxUtlAddMacPhyTlv) Added MacPhy TLV to "
                   "preformed buffer for port %d successfully\r\n",
                   pPortInfo->u4LocPortNum);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddPwrViaMdiTlv 
 *
 *    DESCRIPTION      : This function adds power via MDI tlv to the 
 *                       linear buffer
 *
 *    INPUT            : pPortInfo - pointer to port information structure
 *                       pu1BasePtr - pointer to base address of linear buffer
 *                       pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv has to be addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by 
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv is addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddPwrViaMdiTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                          UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{

    UINT2               u2TlvHeader = 0;

    *pu2LocOffset = 0;
    LLDP_TRC_ARG1 (LLDP_PWR_MDI_TRC,
                   "\r(LldpTxUtlAddPwrViaMdiTlv) Adding PwrViaMdi TLV to "
                   "preformed buffer for port %d\r\n", pPortInfo->u4LocPortNum);
    /* check the linear buffer can accomodate proto vlan id tlv */
    if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                    LLDP_PWR_VIA_MDI_TLV_LEN) == OSIX_TRUE)
    {
        pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
        pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpTxUtlAddPwrViaMdiTlv: Max Pdu size exceeded!!!..\n");
        return OSIX_SUCCESS;
    }
    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_ORG_SPEC_TLV,
                          (UINT2) LLDP_PWR_VIA_MDI_TLV_INFO_LEN, &u2TlvHeader);
    /* put tlv header into linear buffer */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* copy 802.3 OUI into linear buffer */
    MEMCPY (pu1Buf, gau1LldpDot3OUI, LLDP_MAX_LEN_OUI);
    /* increment buffer pointer */
    LLDP_INCR_BUF_PTR (pu1Buf, (UINT2) LLDP_MAX_LEN_OUI);
    /* put Dot3 subtype in linear buffer */
    LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_POWER_VIA_MDI_TLV);
    /* power info */
    LLDP_PUT_1BYTE (pu1Buf,
                    pPortInfo->Dot3LocPortInfo.Dot3PowerInfo.u1PowerInfo);
    LLDP_PUT_1BYTE (pu1Buf,
                    pPortInfo->Dot3LocPortInfo.Dot3PowerInfo.u1PowPairs);
    LLDP_PUT_1BYTE (pu1Buf,
                    pPortInfo->Dot3LocPortInfo.Dot3PowerInfo.u1PowClass);

    *pu2LocOffset = LLDP_PWR_VIA_MDI_TLV_LEN;
    LLDP_TRC_ARG1 (LLDP_PWR_MDI_TRC,
                   "\r(LldpTxUtlAddPwrViaMdiTlv) Added PwrViaMdi TLV "
                   "to preformed buffer for port"
                   "%d successfully\r\n", pPortInfo->u4LocPortNum);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddLinkAggTlv 
 *
 *    DESCRIPTION      : This function adds link agg tlv to the 
 *                       linear buffer
 *
 *    INPUT            : pPortInfo - pointer to port information structure
 *                       pu1BasePtr - pointer to base address of linear buffer
 *                       pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv has to be addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by 
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv is addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddLinkAggTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                        UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    tLldpOctetStringType InOctetStr;
    tLldpOctetStringType OutOctetStr;

    UINT4               u4AggPortId = 0;
    UINT2               u2TlvHeader = 0;
    UINT1               u1AggStatus = 0;

    *pu2LocOffset = 0;
    LLDP_TRC_ARG1 (LLDP_LAGG_TRC,
                   "\r(LldpTxUtlAddLinkAggTlv) Adding LinkAgg TLV to "
                   "preformed buffer for port %d\r\n", pPortInfo->u4LocPortNum);
    /* check the linear buffer can accomodate Link Aggregation tlv */
    if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                    LLDP_LINK_AGG_TLV_LEN) == OSIX_TRUE)
    {
        pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
        pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpTxUtlAddLinkAggTlv: Max Pdu size exceeded!!!..\n");
        return OSIX_SUCCESS;
    }
    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_ORG_SPEC_TLV,
                          (UINT2) LLDP_LINK_AGG_TLV_INFO_LEN, &u2TlvHeader);
    /* put tlv header into linear buffer */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* put 802.3 into linear buffer */
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
    {
        /*MEMCPY (pu1Buf, gau1LldpDot3OUI, LLDP_MAX_LEN_OUI); */
        MEMCPY (pu1Buf, gau1LldpDot3OUI, LLDP_MAX_LEN_OUI);
    }
    else
    {
        MEMCPY (pu1Buf, gau1LldpDot1OUI, LLDP_MAX_LEN_OUI);
    }
    /* increment buffer pointer */
    LLDP_INCR_BUF_PTR (pu1Buf, (UINT2) LLDP_MAX_LEN_OUI);
    /* put Dot3 subtype in linear buffer */
    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
    {
        LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_LINK_AGG_TLV);
    }
    else
    {
        LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_V2_LINK_AGG_TLV);
    }
    /* In LLDPDUs the bits are numbered from 0 to 7 where 0 is the low-order 
     * bit. But in the database it is stored in the opposite way for the SNMP
     * session. Hence converting it to the LLDPDU bit ordering convention
     */
    InOctetStr.pu1_OctetList = &pPortInfo->Dot3LocPortInfo.u1AggStatus;
    InOctetStr.i4_Length = sizeof (UINT1);

    OutOctetStr.pu1_OctetList = &u1AggStatus;
    OutOctetStr.i4_Length = sizeof (UINT1);

    MEMSET (OutOctetStr.pu1_OctetList, 0, OutOctetStr.i4_Length);

    /* Convert the Bit list to pkt string as per 
     * Std IEEE802.1AB-2005 section 9.1 */
    LldpReverseOctetString (&InOctetStr, &OutOctetStr);
    /* put system capabilities supported into linear buffer */
    MEMCPY (pu1Buf, OutOctetStr.pu1_OctetList, OutOctetStr.i4_Length);
    pu1Buf += OutOctetStr.i4_Length;

    /* Link Agg PortId */
    u4AggPortId = pPortInfo->Dot3LocPortInfo.u4AggPortId;
    LLDP_PUT_4BYTE (pu1Buf, u4AggPortId);
    *pu2LocOffset = LLDP_LINK_AGG_TLV_LEN;
    LLDP_TRC_ARG1 (LLDP_LAGG_TRC,
                   "\r(LldpTxUtlAddLinkAggTlv) Added LinkAgg TLV to "
                   "preformed buffer for port %d successfully\r\n",
                   pPortInfo->u4LocPortNum);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddMaxFrameSizeTlv 
 *
 *    DESCRIPTION      : This function adds max frame size tlv to the 
 *                       linear buffer
 *
 *    INPUT            : pPortInfo - pointer to port information structure
 *                       pu1BasePtr - pointer to base address of linear buffer
 *                       pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv has to be addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by 
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv is addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlAddMaxFrameSizeTlv (tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                             UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    UINT2               u2TlvHeader = 0;
    UINT2               u2MaxFrameSize = 0;

    *pu2LocOffset = 0;
    LLDP_TRC_ARG1 (LLDP_MAX_FRAME_TRC,
                   "\r(LldpTxUtlAddMaxFrameSizeTlv) Adding MaxFrameSize "
                   "TLV to preformed buffer for port %d\r\n",
                   pPortInfo->u4LocPortNum);
    /* check the linear buffer can accomodate Max Frame size tlv */
    if (LLDP_MAX_PDU_SIZE_EXCEEDED (pu1BasePtr, pu1Buf,
                                    LLDP_MAX_FRAME_SIZE_TLV_LEN) == OSIX_TRUE)
    {
        pPortInfo->bMaxPduSizeExceeded = OSIX_TRUE;
        pPortInfo->StatsTxPortTable.u4LldpPDULengthErrors++;
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpTxUtlAddMaxFrameSizeTlv: Max Pdu size exceeded!!!..\n");
        return OSIX_SUCCESS;
    }
    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_ORG_SPEC_TLV,
                          (UINT2) LLDP_MAX_FRAME_SIZE_TLV_INFO_LEN,
                          &u2TlvHeader);
    /* put tlv header into linear buffer */
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    /* put 802.3 into linear buffer */
    MEMCPY (pu1Buf, gau1LldpDot3OUI, LLDP_MAX_LEN_OUI);
    /* increment buffer pointer */
    LLDP_INCR_BUF_PTR (pu1Buf, (UINT2) LLDP_MAX_LEN_OUI);
    /* put Dot3 subtype in linear buffer */
    LLDP_PUT_1BYTE (pu1Buf, (UINT1) LLDP_MAX_FRAME_SIZE_TLV);
    u2MaxFrameSize = pPortInfo->Dot3LocPortInfo.u2MaxFrameSize;
    LLDP_PUT_2BYTE (pu1Buf, u2MaxFrameSize);
    *pu2LocOffset = LLDP_MAX_FRAME_SIZE_TLV_LEN;
    LLDP_TRC_ARG1 (LLDP_MAX_FRAME_TRC,
                   "\r(LldpTxUtlAddMaxFrameSizeTlv) Added MaxFrameSize "
                   "TLV to preformed buffer for port %d successfully\r\n",
                   pPortInfo->u4LocPortNum);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddEndOfLLdpduTlv 
 *
 *    DESCRIPTION      : This function adds end of lldpdu tlv to the 
 *                       linear buffer
 *
 *    INPUT            : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv has to be addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by 
 *                                      which the linear pointer is moved in
 *                                      this function
 *
 *    OUTPUT           : pu1Buf - poitner to linear buffer in which chassis id
 *                                tlv is addded
 *                       pu2LocOffset - pointer to offset(offset in bytes by
 *                                      which the linear pointer is moved in
 *                                      this function
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC VOID
LldpTxUtlAddEndOfLLdpduTlv (UINT1 *pu1Buf, UINT2 *pu2LocOffset)
{
    UINT2               u2TlvHeader = 0;

    *pu2LocOffset = 0;
    /* Pdu size will never exceed max allowed pdu size while adding 
     * end of lldpdu tlv, because whenever we add any other tlv
     * we are checkin the buffer space whether the buffer can accomodate
     * that particular tlv + end of lldpdu tlv */
    LLDP_FORM_TLV_HEADER ((UINT2) LLDP_END_OF_LLDPDU_TLV,
                          (UINT2) LLDP_END_OF_LLDPDU_TLV_INFO_LEN,
                          &u2TlvHeader);
    LLDP_PUT_2BYTE (pu1Buf, u2TlvHeader);
    *pu2LocOffset = LLDP_END_OF_LLDPDU_TLV_LEN;
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlRBCmpLocProtoIdInfo
 *
 *    DESCRIPTION      : RBTree compare function for LocProtoIdInfoRBTree.
 *
 *    INPUT            : pRBElem1 - Pointer to the LocProtoIdRBNode node1
 *                       pRBElem2 - Pointer to the LocProtoIdRBNode node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : LLDP_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       LLDP_RB_LESS    - if node pRBElem1's key is less than
 *                                         node pRBElem2's key.
 *                       LLDP_RB_GREATER - if node pRBElem1's key is greater
 *                                         than node pRBElem2's key.
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlRBCmpLocProtoIdInfo (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tLldpxdot1LocProtoIdInfo *pLocEntry1 =
        (tLldpxdot1LocProtoIdInfo *) pRBElem1;
    tLldpxdot1LocProtoIdInfo *pLocEntry2 =
        (tLldpxdot1LocProtoIdInfo *) pRBElem2;

    /* Compare the First Index - Management address subtype */
    if (pLocEntry1->u4IfIndex > pLocEntry2->u4IfIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pLocEntry1->u4IfIndex < pLocEntry2->u4IfIndex)
    {
        return LLDP_RB_LESS;
    }

    /* First Index is same so compare the Second Index */
    if (pLocEntry1->u4ProtocolIndex > pLocEntry2->u4ProtocolIndex)
    {
        return LLDP_RB_GREATER;
    }
    else if (pLocEntry1->u4ProtocolIndex < pLocEntry2->u4ProtocolIndex)
    {
        return LLDP_RB_LESS;
    }
    else
    {
        return LLDP_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlAddProtoVlanEntry
 *
 *    DESCRIPTION      : This function adds an entry to port and protocol 
 *                       vlan DLL maintained for the port. Memory allocated
 *                       for the nodes in the List need to be released at the
 *                       time of deleting the node from the List.
 *                       
 *    INPUT            : pLldpLocPortInfo - Pointer to port info structure
 *                       u2VlanId         - Protocol Vlan Id
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/

PUBLIC INT4
LldpTxUtlAddProtoVlanEntry (tLldpLocPortInfo * pLldpLocPortInfo, UINT2 u2VlanId)
{
    tLldpxdot1LocProtoVlanInfo *pCurProtoVlanNode = NULL;
    tLldpxdot1LocProtoVlanInfo *pNewProtoVlanNode = NULL;
    tTMO_DLL_NODE      *pNode = NULL;
    tTMO_DLL_NODE      *pCurNode = NULL;
    tTMO_DLL           *pDll = NULL;
    BOOL1               bAddedToList = LLDP_FALSE;

    pDll = &(pLldpLocPortInfo->LocProtoVlanDllList);
    if ((pNewProtoVlanNode =
         (tLldpxdot1LocProtoVlanInfo *) MemAllocMemBlk
         (gLldpGlobalInfo.LocProtoVlanInfoPoolId)) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpTxUtlAddProtoVlanEntry: Memory allocation failed!!..\n");
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpTxUtlAddProtoVlanEntry: Memory allocation failed!!.."));
#endif
        LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
        return OSIX_FAILURE;
    }

    MEMSET (pNewProtoVlanNode, 0, sizeof (tLldpxdot1LocProtoVlanInfo));
    pNode = &pNewProtoVlanNode->NextLocProtoVlanNode;
    TMO_DLL_Init_Node (pNode);

    pNewProtoVlanNode->u2ProtoVlanId = u2VlanId;
    pNewProtoVlanNode->u1ProtoVlanTxEnabled = LLDP_FALSE;

    /* The List is empty */
    if (TMO_DLL_Count (pDll) == 0)
    {
        TMO_DLL_Add (pDll, pNode);
        return OSIX_SUCCESS;
    }

    /* List contains protocol vlans. Add the new entry in sorted order for
     * SNMP walk */
    TMO_DLL_Scan (pDll, pCurNode, tTMO_DLL_NODE *)
    {
        pCurProtoVlanNode = (tLldpxdot1LocProtoVlanInfo *) pCurNode;
        /* Replace this node with the new protocol vlan id */
        if (pCurProtoVlanNode->u2ProtoVlanId == 0)
        {
            TMO_DLL_Replace (pDll, pCurNode, pNode);
            MemReleaseMemBlock (gLldpGlobalInfo.LocProtoVlanInfoPoolId,
                                (UINT1 *) pCurNode);
            bAddedToList = LLDP_TRUE;
            break;
        }

        if ((TMO_DLL_Next (pDll, pCurNode) == NULL) &&
            (pCurProtoVlanNode->u2ProtoVlanId < u2VlanId))
        {
            TMO_DLL_Add (pDll, pNode);
            bAddedToList = LLDP_TRUE;
            break;
        }
        else if (pCurProtoVlanNode->u2ProtoVlanId > u2VlanId)
        {
            TMO_DLL_Insert (pDll, pCurNode->pPrev, pNode);
            bAddedToList = LLDP_TRUE;
            break;
        }
        if (pCurProtoVlanNode->u2ProtoVlanId == u2VlanId)
        {
            /* An entry with the same v-lan is already present */
            MemReleaseMemBlock (gLldpGlobalInfo.LocProtoVlanInfoPoolId,
                                (UINT1 *) pNewProtoVlanNode);
            bAddedToList = LLDP_TRUE;
            break;
        }
    }

    if (bAddedToList == LLDP_FALSE)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.LocProtoVlanInfoPoolId,
                            (UINT1 *) pNewProtoVlanNode);

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlUpdateProtoVlanEntry
 *
 *    DESCRIPTION      : This function either deletes or modifies an entry in
 *                       port and protocol vlan DLL maintained fot the port.
 *                       
 *    INPUT            : pLldpLocPortInfo - Pointer to port info structure
 *                       u2VlanId         - Protocol Vlan Id to be added/deleted
 *                                          to/from the LLDP data structure
 *                       u2OldVlanId      - Non-zero only if u4ActionFlag = 
 *                                          LLDP_UPDATE i.e, if for the same
 *                                          protocol group the protocol vlan 
 *                                          id is replaced with another 
 *                                          protocol vlan id.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/

PUBLIC INT4
LldpTxUtlUpdateProtoVlanEntry (tLldpLocPortInfo * pLldpLocPortInfo,
                               UINT2 u2VlanId, UINT2 u2OldVlanId,
                               UINT4 u4ActionFlag)
{
    tLldpxdot1LocProtoVlanInfo *pCurProtoVlanNode = NULL;
    UINT2               u2DelVlanId;
    tTMO_DLL           *pDll = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT1               u1AddFlag = OSIX_FALSE;

    pDll = &(pLldpLocPortInfo->LocProtoVlanDllList);

    if (u4ActionFlag == LLDP_UPDATE)
    {
        /* In case of updation the old vlan id will be deleted and the 
         * new id will be inserted again so that the sorted order is maintained.
         */
        u2DelVlanId = u2OldVlanId;
    }
    else
    {
        u2DelVlanId = u2VlanId;
    }

    TMO_DLL_Scan (pDll, pCurProtoVlanNode, tLldpxdot1LocProtoVlanInfo *)
    {
        if (pCurProtoVlanNode->u2ProtoVlanId == u2DelVlanId)
        {
            TMO_DLL_Delete (pDll, &pCurProtoVlanNode->NextLocProtoVlanNode);
            MemReleaseMemBlock (gLldpGlobalInfo.LocProtoVlanInfoPoolId,
                                (UINT1 *) pCurProtoVlanNode);
            i4RetVal = OSIX_SUCCESS;
            break;
        }
    }

    if ((u4ActionFlag == LLDP_DELETE) && (TMO_DLL_Count (pDll) == 0))
    {
        /* As the port is not enabled with any PPVID, add a node in the
         * dll with PPVID value as 0 (IEEE 802.1AB section F.3.2)*/
        u2VlanId = 0;
        u1AddFlag = OSIX_TRUE;
    }

    if ((u4ActionFlag == LLDP_UPDATE) || (u1AddFlag == OSIX_TRUE))
    {
        if (LldpTxUtlAddProtoVlanEntry (pLldpLocPortInfo, u2VlanId)
            == OSIX_SUCCESS)
        {
            i4RetVal = OSIX_SUCCESS;
        }
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlGetLocManAddrNode
 *
 *    DESCRIPTION      : This fiunction fetch the Local Management Address
 *                       information node from LocManAddrRBTree
 *                       and return the pointer of the Node. This function
 *                       is useful for SNMP low level interface operations.
 *
 *    INPUT            : i4LldpLocManAddrSubtype - Management Address subtype
 *                       pu1LocManAddr           - Management Address
 *
 *    OUTPUT           : pManAddrNode - pointer to management address node
 *
 *    RETURNS          : pointer to management address node
 *
 ****************************************************************************/
PUBLIC tLldpLocManAddrTable *
LldpTxUtlGetLocManAddrNode (INT4 i4LldpLocManAddrSubtype, UINT1 *pu1LocManAddr)
{
    tLldpLocManAddrTable *pManAddrNode = NULL;
    INT4                i4AddrLen = LLDP_INVALID_VALUE;

    MEMSET (&TmpLocManNode, 0, sizeof (tLldpLocManAddrTable));

    if (i4LldpLocManAddrSubtype == IPVX_ADDR_FMLY_IPV4)
    {
        i4AddrLen = IPVX_IPV4_ADDR_LEN;
    }
    else if (i4LldpLocManAddrSubtype == IPVX_ADDR_FMLY_IPV6)
    {
        i4AddrLen = IPVX_IPV6_ADDR_LEN;
    }
    else                        /* For Unknown Types */
    {
        i4AddrLen = LLDP_MAX_LEN_MAN_ADDR;
    }

    /* Validate the Index */
    if (LldpTxUtlValidateManAddrTblIndices (i4LldpLocManAddrSubtype,
                                            i4AddrLen) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpTxUtlGetLocManAddrNode"
                  "LldpTxUtlValidateManAddrTblIndices failed!!..\n");
        return NULL;
    }

    TmpLocManNode.i4LocManAddrSubtype = i4LldpLocManAddrSubtype;
    MEMSET (TmpLocManNode.au1LocManAddr, 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMCPY (TmpLocManNode.au1LocManAddr, pu1LocManAddr, i4AddrLen);

    /* Fetch the Node from the RBTree */
    pManAddrNode = (tLldpLocManAddrTable *) RBTreeGet
        (gLldpGlobalInfo.LocManAddrRBTree, (tRBElem *) & TmpLocManNode);

    return pManAddrNode;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlValidateManAddrTblIndices
 *
 *    DESCRIPTION      : This routine validate the Indices of the Local
 *                       Management Address Table.
 *
 *    INPUT            : i4LldpLocManAddrSubtype - man addr subtype
 *                       i4ManAddrLen - man addr len 
 *    OUTPUT           :
 *
 *    RETURNS          :
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlValidateManAddrTblIndices (INT4 i4LldpLocManAddrSubtype,
                                    INT4 i4ManAddrLen)
{
    /* Validate the Man Addr Specific Indices */
    if ((i4LldpLocManAddrSubtype < LLDP_MIN_MAN_ADDR_SUBTYPE) ||
        (i4LldpLocManAddrSubtype > LLDP_MAX_MAN_ADDR_SUBTYPE))
    {
        return OSIX_FAILURE;
    }
    if ((i4ManAddrLen > LLDP_MAX_LEN_MAN_ADDR) ||
        (i4ManAddrLen < LLDP_MIN_LEN_MAN_ADDR))
    {
        return OSIX_FAILURE;
    }

    /* All indices are validated */
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlGetNextManAddr
 *
 *    DESCRIPTION      : Get the Next Management Address Entry for this
 *                       particular Neighbor from LocManAddrRBTree.
 *
 *
 *    INPUT            : pCurrManAddrNode - Current ManAddr Node, whoes Next
 *                                         Node Need to be returned
 *                       ppNextManAddrNode - Pointer to next node                  
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : Pointer To the Next Mnagement Addr Node, or NULL if
 *                       End of ManAddr for that neighbor.
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlGetNextManAddr (tLldpLocManAddrTable * pCurrManAddrNode,
                         tLldpLocManAddrTable ** ppNextManAddrNode)
{
    /* Get the Next Node from LocManAddrRBTree */
    *ppNextManAddrNode = RBTreeGetNext (gLldpGlobalInfo.LocManAddrRBTree,
                                        (tRBElem *) pCurrManAddrNode, NULL);

    if (*ppNextManAddrNode != NULL)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpTxUtlRBFreeLocManAddr
 *
 *    DESCRIPTION      : This function releases the memory associated with
 *                       LocManAddr
 *
 *    INPUT            : pRBElem - pointer to RBElement of which memory should
 *                                 be released 
 *    OUTPUT           :
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlRBFreeLocManAddr (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.LocManAddrPoolId,
                            (UINT1 *) pRBElem);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpTxUtlRBFreeLocProtoId 
 *
 *    DESCRIPTION      : This function releases the memory associated with 
 *                       ProtoIdInfo.
 *
 *    INPUT            : pRBElem - pointer to RBElement of which memory should
 *                                 be released
 *
 *    OUTPUT           :
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlRBFreeLocProtoId (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (gLldpGlobalInfo.LocProtoIdInfoPoolId,
                            (UINT1 *) pRBElem);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlDeleteQMsg 
 *
 *    DESCRIPTION      : Function that deletes Application message in Queue
 *
 *    INPUT            : QId -    Queue Id to be deleted
 *                       PoolId - Mem Pool Id associated with the queue
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC VOID
LldpTxUtlDeleteQMsg (tOsixQId QId, tMemPoolId PoolId)
{
    tLldpQMsg          *pQueue = NULL;

    if (QId == 0)
    {
        return;
    }
    if (PoolId == 0)
    {
        return;
    }
    /* Dequeue all messages */
    while (OsixQueRecv (QId, (UINT1 *) &pQueue,
                        OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
    {
        if (pQueue == NULL)
        {
            continue;
        }

        MemReleaseMemBlock (PoolId, (UINT1 *) pQueue);
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlClearPortInfo
 *
 *    DESCRIPTION      : This function is used to remove the port info in the
 *                       global structure. This is called whenever the port
 *                       is deleted.
 *
 *    INPUT            : pPortEntry - Pointer to port information struct
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlClearPortInfo (tLldpLocPortInfo * pPortEntry)
{
    pPortEntry->pBackPtrRemoteNode = NULL;
    pPortEntry->pu1RxLldpdu = NULL;
    pPortEntry->u2RxPduLen = 0;
    /* delete the entires in ProtoVlanDLL and release the memory */
    if (LldpTxUtlDelProtoVlanDLL (pPortEntry) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC,
                  "LldpTxUtlClearPortInfo: LldpTxUtlDelProtoVlanDLL "
                  " returns FAILURE!!..\n");
        return OSIX_FAILURE;
    }
    /* memory allocated for port information structure is released in TxSem 
     * LLDP_TX_INIT state (refer LldpTxSmStateLldpInit) */
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlDelProtoVlanDLL
 *
 *    DESCRIPTION      : Function called for deleting the entries from Proto
 *                       Vlan DLL.
 *
 *    INPUT            : pPortEntry - pointer to port inforamtion structure
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxUtlDelProtoVlanDLL (tLldpLocPortInfo * pPortEntry)
{
    tLldpxdot1LocProtoVlanInfo *pProtoVlanNode = NULL;
    tTMO_DLL           *pDll = NULL;
    tTMO_DLL_NODE      *pNode = NULL;

    pDll = &(pPortEntry->LocProtoVlanDllList);
    /* The List is empty */
    if (TMO_DLL_Count (pDll) != 0)
    {
        pNode = NULL;
        /* get first node */
        pProtoVlanNode = (tLldpxdot1LocProtoVlanInfo *)
            TMO_DLL_Next (pDll, pNode);
        /* delete all the nodes */
        while (pProtoVlanNode != NULL)
        {
            /* delete the node */
            TMO_DLL_Delete (pDll, &pProtoVlanNode->NextLocProtoVlanNode);
            /* release the memory */
            MemReleaseMemBlock (gLldpGlobalInfo.LocProtoVlanInfoPoolId,
                                (UINT1 *) pProtoVlanNode);
            /* reset the pointer */
            pProtoVlanNode = NULL;
            /* get next entry */
            pProtoVlanNode = (tLldpxdot1LocProtoVlanInfo *)
                TMO_DLL_Next (pDll, pNode);
        }
    }
    /* Initialize LocProtoVlanDllList */
    TMO_DLL_Init (&(pPortEntry->LocProtoVlanDllList));
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxUtlValidateInterfaceIndex
 *
 *    DESCRIPTION      : This function validates the port index 
 *
 *    INPUT            : i4LldpPortConfigPortNum ( PortIndex) 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/

PUBLIC INT4
LldpTxUtlValidateInterfaceIndex (INT4 i4LldpPortConfigPortNum)
{
    tLldpLocPortTable  *pLldpLocPortEntry = NULL;
    tLldpLocPortTable   LldpLocPortEntry;

    MEMSET (&LldpLocPortEntry, 0, sizeof (tLldpLocPortTable));

    if (LLDP_IS_PORT_ID_VALID ((UINT4) i4LldpPortConfigPortNum) == OSIX_FALSE)
    {
        return OSIX_FAILURE;
    }
    LldpLocPortEntry.i4IfIndex = i4LldpPortConfigPortNum;
    pLldpLocPortEntry = (tLldpLocPortTable *)
        RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &LldpLocPortEntry);
    if (pLldpLocPortEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

#endif /* _LLDTXUTL_C_ */

/*--------------------------------------------------------------------------*/
/*                       End of the file  <lldtxutl.c>                      */
/*--------------------------------------------------------------------------*/
