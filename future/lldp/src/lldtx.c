/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldtx.c,v 1.18 2017/12/14 10:30:27 siva Exp $
 *
 * Description: This file contains LLDP transmit related functions
 *****************************************************************************/
#ifndef _LLDP_TX_C_
#define _LLDP_TX_C_

#include "lldinc.h"
static UINT1        au1EntireBuf[LLDP_MAX_FRAME_SIZE];
PRIVATE tCfaIfInfo  gCfaStagTxIfInfo;
static UINT1        au1UpdtdEntireBuf[LLDP_MAX_FRAME_SIZE];

/****************************************************************************
 *    FUNCTION NAME    : LldpSendStagPdu
 *
 *    DESCRIPTION      : This function send tagged (s-tag) LLD-PDU on the 
 *                       interface.
 *
 *    INPUT            : pPortInfo   - pointer to port information structure 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
LldpSendStagPdu (tLldpLocPortInfo * pPortInfo)
{
    /* LOGIC : Frame the required DMAC, SMAC, SVLAN, ETHTYPE in the linear 
     * buffer and copy the linear buffer each and every time to the Duplicated
     * cru buf which has been duplicated via 'CRU_BUF_Duplicate_BufChain' from
     * the OffSet Zero (Starting Pointer) */

    UINT2               u2Tag;
    tLldpLocPortTable   PortEntry;
    tLldpLocPortTable  *pPortEntry = NULL;
    tCRU_BUF_CHAIN_HEADER *pDupPktToSend = NULL;
    UINT1               au1PduHeader[LLDP_MAX_LEN_STAG_ENETV2_HEADER] = { 0 };
    UINT4               u4FrameSize = 0;
    UINT1              *pu1PduHeader;

    INT4                i4SbpCount = 0;
    UINT1               au1SvlanEnetV2EtherType[VLAN_TYPE_OR_LEN_SIZE]
        = { 0x88, 0xa8 };
    UINT1               au1LldpEnetV2EtherType[LLDP_MAX_LEN_ENETV2_ENCAPTYPE]
        = { 0x88, 0xCC };
    UINT1               au1CEEOUI[LLDP_MAX_LEN_OUI] = { 0x00, 0x1b, 0x21 };
    UINT1              *pu1Tlv = NULL;
    UINT1              *pu1TlvInfo = NULL;
    UINT1              *pu1UpdtdTlv = NULL;
    UINT4               u4Offset;
    UINT1               u1TlvSubType;
    UINT1               u1IsMoveTlvLen;
    UINT2               u2TlvType = 0;
    UINT2               u2TlvLength = 0;
    UINT2               u2TotalTlvLengthMoved = 0;
    UINT2               u2TlvStrtOffset = 0;
    UINT2               u2TlvEndOffset = 0;
    UINT2               u2Count = 0;
    UINT1               au1RxOUI[LLDP_MAX_LEN_OUI] = { 0 };
    UINT1              *pu1RxOUI = NULL;

    MEMSET (&PortEntry, 0, sizeof (PortEntry));
    MEMSET (au1EntireBuf, 0, LLDP_MAX_FRAME_SIZE);
    PortEntry.i4IfIndex = pPortInfo->i4IfIndex;
    pPortEntry = RBTreeGet (gLldpGlobalInfo.LldpPortInfoRBTree, &PortEntry);

    if (pPortEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    if ((MEMCMP (gau1LldpMcastAddr, pPortInfo->PortConfigTable.u1DstMac,
                 MAC_ADDR_LEN)) != 0)
    {
        /* Tagged lldp packet needs to be sent on Native LLD-PDU 
         * and not on the LLDP Applications */
        LLDP_TRC (CONTROL_PLANE_TRC,
                  "LldpSendStagPdu:Mac Address is not LLDP Global Mac Address!!!\r\n");
        return OSIX_FAILURE;
    }
    MEMCPY (au1EntireBuf, pPortInfo->pPreFormedLldpdu,
            pPortInfo->u4PreFormedLldpduLen);
    u4FrameSize = pPortInfo->u4PreFormedLldpduLen;

    /* The pu1Tlv pointer now marks to the LLD-PDU .Moving the pointer 
     * to after DEST MAC, SOURCE MAC and LLDP ETHTYPE */
    pu1Tlv = (au1EntireBuf + CFA_ENET_V2_HEADER_SIZE);
    LLDP_GET_TLV_TYPE_LENGTH (pu1Tlv, &u2TlvType, &u2TlvLength);
    /* Deletion of DCBX TLV if present before sending taggedLLDP -Start */
    while (u2TlvType != LLDP_END_OF_LLDPDU_TLV)
    {
        u1IsMoveTlvLen = OSIX_TRUE;
        if (u2TlvType == LLDP_ORG_SPEC_TLV)
        {

            pu1TlvInfo = pu1Tlv + LLDP_TLV_HEADER_LEN;
            u4Offset = 0;
            /* Retreiving the OUI specfic to the Org.Specific TLV */
            pu1RxOUI = au1RxOUI;
            LLDP_LBUF_GET_STRING (pu1TlvInfo, pu1RxOUI, u4Offset,
                                  LLDP_MAX_LEN_OUI);

            if ((MEMCMP (pu1RxOUI, gau1LldpDot1OUI, LLDP_MAX_LEN_OUI) == 0) ||
                (MEMCMP (pu1RxOUI, au1CEEOUI, LLDP_MAX_LEN_OUI) == 0))
            {
                u2TlvStrtOffset = (UINT2) (CFA_ENET_V2_HEADER_SIZE +
                                           u2TotalTlvLengthMoved);
                u2TlvEndOffset = (UINT2) (CFA_ENET_V2_HEADER_SIZE +
                                          u2TotalTlvLengthMoved + u2TlvLength +
                                          LLDP_TLV_HEADER_LEN);

                LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, u4Offset, u1TlvSubType);
                switch (u1TlvSubType)
                {
                        /* Baseline DCBX TLV */
                    case 2:
                        /* Congestion notification TLV */
                    case 8:
                        /* IEEE ETS Configuration TLV */
                    case 9:
                        /* ETS Recommendation TLV */
                    case 10:
                        /* Priority Based Flow control TLV */
                    case 11:
                        /* Priority Based Flow Control TLV */
                    case 12:
                        /* Application Protocol */
                        /* Assigning Memory to the pointer */
                        pu1UpdtdTlv = au1UpdtdEntireBuf;
                        MEMSET (pu1UpdtdTlv, 0, LLDP_MAX_FRAME_SIZE);
                        /* Copy the buffer to temp pointer till DCBX TLV */
                        MEMCPY (pu1UpdtdTlv, au1EntireBuf, u2TlvStrtOffset);
                        /* Increment the pointer to the begining of DCBX tlv */
                        pu1UpdtdTlv = pu1UpdtdTlv + u2TlvStrtOffset;
                        /* Increment au1EntireBuf till end of DCBX and copy the remaining bytes
                           to pointer */
                        MEMCPY (pu1UpdtdTlv, au1EntireBuf + u2TlvEndOffset,
                                (u4FrameSize - u2TlvEndOffset));
                        /* Update the framesize value */
                        u4FrameSize =
                            u4FrameSize - u2TlvLength - LLDP_TLV_HEADER_LEN;
                        /* Copy the buffer to entire buffer */
                        MEMCPY (au1EntireBuf, au1UpdtdEntireBuf, u4FrameSize);
                        u1IsMoveTlvLen = OSIX_FALSE;
                        break;
                    default:
                        break;
                        /* Dont do Anything */

                }
            }
        }
        if (u1IsMoveTlvLen == OSIX_TRUE)
        {
            pu1Tlv = pu1Tlv + LLDP_TLV_HEADER_LEN + u2TlvLength;
            u2TotalTlvLengthMoved =
                (UINT2) (u2TotalTlvLengthMoved + u2TlvLength +
                         LLDP_TLV_HEADER_LEN);
        }
        LLDP_GET_TLV_TYPE_LENGTH (pu1Tlv, &u2TlvType, &u2TlvLength);
    }
    /* Deletion of DCBX TLV if present before sending taggedLLDP -END */
    u4FrameSize = u4FrameSize + VLAN_TAG_PID_LEN;

    u2Count = 0;
    while (i4SbpCount < VLAN_EVB_MAX_SBP_PER_UAP)
    {
        LLDP_TRC_ARG3 (CONTROL_PLANE_TRC,
                       "LldpSendStagPdu:Port:%d SbpCount=%d Svid=%d!!!\r\n",
                       pPortInfo->i4IfIndex, i4SbpCount,
                       pPortEntry->au2SVlanIdArray[i4SbpCount]);

        if (pPortEntry->au2SVlanIdArray[i4SbpCount] != 0)
        {
            if ((pDupPktToSend =
                 CRU_BUF_Allocate_MsgBufChain (u4FrameSize, 0)) == NULL)
            {
                LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "LldpSendStagPdu:CRU_BUF_Allocate_MsgBufChain Failed!!!\r\n");
                return OSIX_FAILURE;
            }

            MEMSET (pDupPktToSend->ModuleData.au1ModuleInfo, 0,
                    CRU_BUF_NAME_LEN);
            CRU_BUF_UPDATE_MODULE_INFO (pDupPktToSend, "LldpSend");
            if (CRU_BUF_Copy_OverBufChain (pDupPktToSend,
                                           &(au1EntireBuf
                                             [LLDP_MAX_LEN_ENETV2_HEADER]),
                                           (LLDP_MAX_LEN_ENETV2_HEADER + 4),
                                           (u4FrameSize -
                                            LLDP_MAX_LEN_ENETV2_HEADER)) ==
                CFA_FAILURE)
            {
                LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "LldpSendStagPdu:CRU_BUF_Copy_OverBufChain Failed"
                          "during Copying the LLDP TLV's to the PDU!!!\r\n");
                CRU_BUF_Release_MsgBufChain (pDupPktToSend, FALSE);
                return OSIX_FAILURE;
            }

            MEMSET (&gCfaStagTxIfInfo, 0, sizeof (tCfaIfInfo));
            pu1PduHeader = au1PduHeader;

            /* 1. Destination multi cast address */
            MEMCPY (pu1PduHeader, pPortInfo->PortConfigTable.u1DstMac,
                    MAC_ADDR_LEN);
            LLDP_INCR_BUF_PTR (pu1PduHeader, (UINT2) MAC_ADDR_LEN);
            /* 2. Source MAC address */
            if (LldpPortGetIfInfo
                ((UINT4) pPortInfo->i4IfIndex,
                 &gCfaStagTxIfInfo) != OSIX_SUCCESS)
            {
                LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "LldpSendStagPdu: LldpPortGetIfInfo returned "
                          "Failure\r\n");
                CRU_BUF_Release_MsgBufChain (pDupPktToSend, FALSE);
                return OSIX_FAILURE;
            }
            MEMCPY (pu1PduHeader, gCfaStagTxIfInfo.au1MacAddr,
                    CFA_ENET_ADDR_LEN);
            LLDP_INCR_BUF_PTR (pu1PduHeader, CFA_ENET_ADDR_LEN);
            /*3. SVLAN EtherType */
            MEMCPY (pu1PduHeader, au1SvlanEnetV2EtherType,
                    VLAN_TYPE_OR_LEN_SIZE);
            LLDP_INCR_BUF_PTR (pu1PduHeader, VLAN_TYPE_OR_LEN_SIZE);
            u2Tag = 0;            /* VLAN Tag Priority */
            u2Tag = (UINT2) (u2Tag << VLAN_TAG_PRIORITY_SHIFT);
            u2Tag = (UINT2) (u2Tag |
                             (pPortEntry->au2SVlanIdArray[i4SbpCount]
                              & VLAN_ID_MASK));
            u2Tag = (UINT2) (OSIX_HTONS (u2Tag));
            /* 4. SVLAN PRIORITY and ID */
            MEMCPY (pu1PduHeader, (UINT1 *) &(u2Tag), VLAN_TAG_SIZE);
            LLDP_INCR_BUF_PTR (pu1PduHeader, VLAN_TAG_SIZE);

            /* 5. Ethertype */
            MEMCPY (pu1PduHeader, au1LldpEnetV2EtherType,
                    LLDP_MAX_LEN_ENETV2_ENCAPTYPE);
            LLDP_INCR_BUF_PTR (pu1PduHeader, LLDP_MAX_LEN_ENETV2_ENCAPTYPE);

            if (CRU_BUF_Copy_OverBufChain (pDupPktToSend, au1PduHeader, 0,
                                           LLDP_MAX_LEN_STAG_ENETV2_HEADER) ==
                CFA_FAILURE)
            {
                LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "LldpSendStagPdu:CRU_BUF_Copy_OverBufChain Failed"
                          "while sending S-tag packets!!!\r\n");
                CRU_BUF_Release_MsgBufChain (pDupPktToSend, FALSE);
                return OSIX_FAILURE;
            }

            if (LldpPortHandleOutgoingPkt
                (pDupPktToSend, (UINT2) pPortInfo->i4IfIndex,
                 u4FrameSize) != OSIX_SUCCESS)
            {
                LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "LldpSendStagPdu: LldpPortHandleOutgoingPkt returned "
                          "Failure!!!\r\n");

                CRU_BUF_Release_MsgBufChain (pDupPktToSend, FALSE);
                return OSIX_FAILURE;
            }
            LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                           "LldpSendStagPdu: Incrementing the tagged LLDP counters"
                           "for UAP Port:%d!!!\r\n", pPortInfo->i4IfIndex);
            LLDP_TAG_INCR_CNTR_TX_FRAMES (pPortInfo);
            u2Count++;
        }
        i4SbpCount++;
    }
    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC,
                   "LldpSendStagPdu:Count = %d!!!\r\n", u2Count);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpTxFrame
 *
 *    DESCRIPTION      : This function prepends the ethernet header based on 
 *                       interface type and forwards the LLDP frame to the 
 *                       L2IWF to transmit it out on the specified interface.
 *
 *    INPUT            : pPortInfo   - pointer to port information structure 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpTxFrame (tLldpLocPortInfo * pPortInfo)
{
    UINT4               u4FrameSize = 0;
    tCfaIfInfo          CfaIfInfo;
    tCRU_BUF_CHAIN_HEADER *pPktToSend = NULL;
    /*  HITLESS RESTART */
    UINT4               u4MsgInterval = 0;
    UINT4               u4JitteredMsgInterval = 0;
    INT4                i4RetVal = OSIX_FAILURE;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    if (pPortInfo->pPreFormedLldpdu == NULL)
    {
        LLDP_TRC_ARG2 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                       "LldpTxFrame: Preformed buffer is not constructed "
                       "for port: %d Dest Mac Index: %d\n\n",
                       pPortInfo->i4IfIndex, pPortInfo->u4DstMacAddrTblIndex);
        return OSIX_FAILURE;
    }

    u4FrameSize = pPortInfo->u4PreFormedLldpduLen;
    pPktToSend = CRU_BUF_Allocate_MsgBufChain (u4FrameSize, 0);
    if (pPktToSend == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpTxFrame: CRU_BUF_Allocate_MsgBufChain Failed!!!\r\n");
        return OSIX_FAILURE;
    }

    if (CRU_BUF_Copy_OverBufChain (pPktToSend, pPortInfo->pPreFormedLldpdu, 0,
                                   u4FrameSize) == CFA_FAILURE)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpTxFrame: CRU_BUF_Copy_OverBufChain Failed\r\n");
        CRU_BUF_Release_MsgBufChain (pPktToSend, FALSE);
        return OSIX_FAILURE;
    }

    /* pPktToSend will be released by CFA/L2Iwf in success and failure case */

    /* HITLESS RESTART  */
    if (LLDP_HR_STDY_ST_REQ_RCVD () == OSIX_TRUE)
    {
        u4MsgInterval = (UINT4) gLldpGlobalInfo.SysConfigInfo.i4MsgTxInterval;

        /* staggering implementation */
        /* when starting timer for the first time we should use jitter and 
         * we should use jittered msg interval. Timer API TmrStartTimer is 
         * used for setting interval in ticks(not seconds), so that the expiry of 
         * the timer will be disatributed across interval */
        if (pPortInfo->bMsgIntvalJittered == OSIX_FALSE)
        {
            pPortInfo->bMsgIntvalJittered = OSIX_TRUE;
            /* get jitterd message interval */
            u4JitteredMsgInterval = LldpTxUtlJitter ((u4MsgInterval *
                                                      SYS_NUM_OF_TIME_UNITS_IN_A_SEC),
                                                     (UINT4)
                                                     LLDP_JITTER_PERCENT);
            /* now u4MsgInterval = jittered message interval in STUPS */
            /* get the remaining stups(stups in the range 1 to 99 will not be 
             * converted in sec, because 1sec = 100 stups) and convert the 
             * remaining stups to milli sec */
            /* convert u4MsgInterval to seconds */
            u4MsgInterval = u4JitteredMsgInterval /
                SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
        }

        if (LldpRedHRSendStdyStPkt (pPktToSend, u4FrameSize,
                                    (UINT2) pPortInfo->u4LocPortNum,
                                    u4MsgInterval) == OSIX_FAILURE)
        {
            LLDP_TRC (DATA_PATH_TRC, "LLDP HR: Sending steady state packet to "
                      "RM failed \r\n");
        }
        /* Check the packet is LLDP-MED packet or LLDP packet and increment the 
         * counters accordingly */
        if ((pPortInfo->bMedCapable == LLDP_MED_TRUE) &&
            ((pPortInfo->LocMedCapInfo.
              u2MedLocCapTxEnable & LLDP_MED_CAPABILITY_TLV) ==
             LLDP_MED_CAPABILITY_TLV))
        {
            LLDP_MED_INCR_CNTR_TX_FRAMES (pPortInfo);
        }
        else
        {
            LLDP_INCR_CNTR_TX_FRAMES (pPortInfo);
        }
        return OSIX_SUCCESS;
    }
    if (LldpPortHandleOutgoingPkt (pPktToSend, (UINT2) pPortInfo->i4IfIndex,
                                   u4FrameSize) != OSIX_SUCCESS)
    {
        LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "LldpTxFrame: LldpPortHandleOutgoingPkt returned "
                  "Failure!!!\r\n");
        return OSIX_FAILURE;
    }
    i4RetVal = LldpPortGetIfInfo ((UINT4) pPortInfo->i4IfIndex, &(CfaIfInfo));
    if ((i4RetVal == OSIX_SUCCESS) &&
        (gLldpGlobalInfo.i4TagStatus == LLDP_TAG_ENABLE) &&
        (CfaIfInfo.u1BrgPortType == CFA_UPLINK_ACCESS_PORT))
    {
        /* Util To send Tagged LLDP */
        LLDP_TRC_ARG1 (CONTROL_PLANE_TRC, "Invoking LldpSendStagPdu from"
                       "LldpTxFrame to send out tagged Lldp Packets on UAP"
                       "port:%u\r\n", pPortInfo->i4IfIndex);
        LldpSendStagPdu (pPortInfo);

    }
    /* Check the packet is LLDP-MED packet or LLDP packet and increment the 
     * counters accordingly */
    if ((pPortInfo->bMedCapable == LLDP_MED_TRUE) &&
        ((pPortInfo->LocMedCapInfo.
          u2MedLocCapTxEnable & LLDP_MED_CAPABILITY_TLV) ==
         LLDP_MED_CAPABILITY_TLV))
    {
        LLDP_MED_INCR_CNTR_TX_FRAMES (pPortInfo);
    }
    else
    {
        LLDP_INCR_CNTR_TX_FRAMES (pPortInfo);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxPrependEnetv2Header
 *
 *    DESCRIPTION      : This function Prepends Enetv2 header to the LLDPDU
 *                       To be uniform with other L2 modules, Ethernet header 
 *                       is prepended by LLDP module itself.
 *
 *    INPUT            : pPortInfo - pointer to port info structure
 *
 *    OUTPUT           : pPortInfo - pointer to port info structure
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpTxPrependEnetv2Header (tLldpLocPortInfo * pPortInfo)
{
    UINT1               au1PduHeader[LLDP_MAX_LEN_ENETV2_HEADER];
    UINT1              *pu1PduHeader = au1PduHeader;
    tCfaIfInfo          CfaIfInfo;
    UINT1               au1LldpEnetV2EtherType[LLDP_MAX_LEN_ENETV2_ENCAPTYPE]
        = { 0x88, 0xCC };
    UINT1               au1LldpMcastAddr[MAC_ADDR_LEN] = { 0 };

    MEMSET (pu1PduHeader, 0, LLDP_MAX_LEN_ENETV2_HEADER);
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    MEMCPY (au1LldpMcastAddr, pPortInfo->PortConfigTable.u1DstMac,
            MAC_ADDR_LEN);

    /* Form LLDP header */
    /* 1. Destination multi cast address */
    MEMCPY (pu1PduHeader, au1LldpMcastAddr, MAC_ADDR_LEN);
    LLDP_INCR_BUF_PTR (pu1PduHeader, (UINT2) MAC_ADDR_LEN);
    /* 2. Source MAC address */
    if (LldpPortGetIfInfo ((UINT4) pPortInfo->i4IfIndex, &CfaIfInfo)
        != OSIX_SUCCESS)
    {
        LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "LldpTxPrependEnetv2Header: LldpPortGetIfInfo returned "
                  "Failure\r\n");
        return OSIX_FAILURE;
    }
    if (pPortInfo->pPreFormedLldpdu == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMCPY (pu1PduHeader, CfaIfInfo.au1MacAddr, CFA_ENET_ADDR_LEN);
    LLDP_INCR_BUF_PTR (pu1PduHeader, CFA_ENET_ADDR_LEN);
    /* 3. Ethertype */
    MEMCPY (pu1PduHeader, au1LldpEnetV2EtherType,
            LLDP_MAX_LEN_ENETV2_ENCAPTYPE);
    LLDP_INCR_BUF_PTR (pu1PduHeader, LLDP_MAX_LEN_ENETV2_ENCAPTYPE);

    /* prepend the header to the preformed buffer */
    MEMCPY (pPortInfo->pPreFormedLldpdu, au1PduHeader,
            LLDP_MAX_LEN_ENETV2_HEADER);
    pPortInfo->u4PreFormedLldpduLen =
        (UINT4) (pPortInfo->u4PreFormedLldpduLen + LLDP_MAX_LEN_ENETV2_HEADER);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpTxPrependSnapHeader
 *
 *    DESCRIPTION      : This function Prepends Snap header to the LLDPDU
 *                       To be uniform with other L2 modules, Ethernet header
 *                       is prepended by LLDP module itself.
 *
 *    INPUT            : pPortInfo - pointer to port info structure
 *
 *    OUTPUT           : pPortInfo - pointer to port info structure
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpTxPrependSnapHeader (tLldpLocPortInfo * pPortInfo)
{
    UINT1               au1PduHeader[LLDP_MAX_LEN_SNAP_HEADER];
    UINT1              *pu1PduHeader = au1PduHeader;
    tCfaIfInfo          CfaIfInfo;
    UINT1               au1LldpSnapEtherType[LLDP_MAX_LEN_SNAP_ENCAPTYPE] =
        { 0xAA, 0xAA, 0x03, 0x00, 0x00, 0x00, 0x88, 0xCC };
    UINT1               au1LldpMcastAddr[MAC_ADDR_LEN] =
        { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x0E };

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    MEMSET (pu1PduHeader, 0, LLDP_MAX_LEN_SNAP_HEADER);

    /* Form LLDP header */
    /* 1. Destination multi cast address */
    MEMCPY (pu1PduHeader, au1LldpMcastAddr, MAC_ADDR_LEN);
    LLDP_INCR_BUF_PTR (pu1PduHeader, (UINT2) MAC_ADDR_LEN);
    /* 2. Source MAC address */
    if (LldpPortGetIfInfo ((UINT4) pPortInfo->i4IfIndex, &CfaIfInfo)
        != OSIX_SUCCESS)
    {
        LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "LldpTxPrependSnapHeader: LldpPortGetIfInfo returned "
                  "Failure\r\n");
        return OSIX_FAILURE;
    }
    if (pPortInfo->pPreFormedLldpdu == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMCPY (pu1PduHeader, CfaIfInfo.au1MacAddr, CFA_ENET_ADDR_LEN);
    LLDP_INCR_BUF_PTR (pu1PduHeader, CFA_ENET_ADDR_LEN);
    /* 3. Ethertype */
    MEMCPY (pu1PduHeader, au1LldpSnapEtherType, LLDP_MAX_LEN_SNAP_ENCAPTYPE);
    LLDP_INCR_BUF_PTR (pu1PduHeader, LLDP_MAX_LEN_SNAP_ENCAPTYPE);

    /* prepend the header to the preformed buffer */
    MEMCPY (pPortInfo->pPreFormedLldpdu, au1PduHeader,
            LLDP_MAX_LEN_SNAP_HEADER);
    pPortInfo->u4PreFormedLldpduLen =
        (UINT4) (pPortInfo->u4PreFormedLldpduLen + LLDP_MAX_LEN_SNAP_HEADER);
    return OSIX_SUCCESS;
}

#endif /* _LLDP_TX_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  lldtx.c                        */
/*-----------------------------------------------------------------------*/
