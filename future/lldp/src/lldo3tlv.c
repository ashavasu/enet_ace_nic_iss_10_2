/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldo3tlv.c,v 1.21 2017/10/11 13:42:02 siva Exp $
 *
 * Description: This File contains all the LLDP 802.3 TLV related Parsing,
 *              Validation and remote MIB updation routines.
 *             
 *****************************************************************************/
#ifndef _LLDP_DOT3TLV_C_
#define _LLDP_DOT3TLV_C_
#include "lldinc.h"

/**************************************************************************
 *      --------------------------------------------------
 *      | OUI |SUBTYPE | Tlv SubType Specific Information
 *      --------------------------------------------------
 *                     ^pu1TlvInfo
 *************************************************************************/

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpDot3TlvProcOrgSpecTlv
 *
 *    DESCRIPTION      : This function processes the IEEE 802.3  
 *                       Oganizationally Specific TLV in the received frame.
 *                       Decode the Information from the TLV Information 
 *                       field  and then Validate the information.
 *
 *    INPUT            : pLocPortInfo - Pointer to the port info structure
 *                       u1TlvSubType - TLV SubType
 *                       u2TlvLength - Length of the Information Field
 *
 *    OUTPUT           : pbTlvDiscardFlag - Determines whether to discard the 
 *                                          TLV or not
 *
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpDot3TlvProcOrgSpecTlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1TlvInfo,
                           UINT1 u1TlvSubType, UINT2 u2TlvLength,
                           BOOL1 * pbTlvDiscardFlag)
{
    INT4                i4RetVal = OSIX_FAILURE;
    UINT1               u1AutoNegInfo = 0;
    UINT1               u1AggStatus = 0;
    UINT1               u1RevAggStatus = 0;
    tLldpOctetStringType InOctetStr;
    tLldpOctetStringType OutOctetStr;
    if (LLDP_IS_UNRECOG_ORGDEF_TLV (u1TlvSubType) == OSIX_TRUE)
    {
        if ((u2TlvLength - (LLDP_MAX_LEN_OUI + LLDP_TLV_SUBTYPE_LEN)) >=
            LLDP_MAX_LEN_ORGDEF_INFO)
        {
            LLDP_TRC (ALL_FAILURE_TRC,
                      "\tUnrecognised Org spec. TLV Length is Too Big\r\n");
            return OSIX_FAILURE;
        }

        /* Unrecognized subtype, statsTLVsUnrecognizedTotal counter shall
         * be incremented and the TLV is assumed to be validated */
        LLDP_INCR_CNTR_RX_TLVS_UNRECOGNIZED (pLocPortInfo);
        LLDP_TRC (CONTROL_PLANE_TRC, "Received Dot3 Unrecognized TLV!!!\n");
        return OSIX_SUCCESS;
    }

    switch (u1TlvSubType)
    {
        case LLDP_MAC_PHY_CONFIG_STATUS_TLV:
            if (u2TlvLength < LLDP_MAC_PHY_TLV_INFO_LEN)
            {
                LLDP_TRC (LLDP_MAC_PHY_TRC, "Discarding"
                          " LLDPDU as obtained MAC/PHY TLV length is less than"
                          " actual len\n");
                /* Return failure as the location of the next TLV is 
                 * indeterminate */
                return OSIX_FAILURE;
            }
            if (LLDP_IS_SET_TLV_RCVD_BMP (pLocPortInfo,
                                          LLDP_TLV_RCVD_MAC_PHY_BMP)
                == OSIX_TRUE)
            {
                /* PDU should not contain more than one MAC/PHY TLV. Hence 
                 * discard the TLV */
                LLDP_TRC (LLDP_MAC_PHY_TRC, "Received"
                          " more than one MAC/PHY TLV!!\n");
                *pbTlvDiscardFlag = OSIX_TRUE;
                return OSIX_SUCCESS;
            }

            /* If the received TLV indicates that auto-negotiation is not
             * supported but the status is enabled, then the TLV msut be
             * discarded */
            LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, 0, u1AutoNegInfo);

            /* Bit 0 represents the auto-neg support and 
             * bit 1 represents the auto-neg status */
            if (!(u1AutoNegInfo & LLDP_AUTO_NEG_SUPP_BMAP) &&
                (u1AutoNegInfo & LLDP_AUTO_NEG_STAT_BMAP))
            {
                *pbTlvDiscardFlag = OSIX_TRUE;
                LLDP_TRC (LLDP_MAC_PHY_TRC,
                          "Discarding MAC_PHY_TLV as auto-neg "
                          " capability is not supported but status"
                          " is enabled\n");
            }

            LLDP_SET_TLV_RCVD_BMP (pLocPortInfo, LLDP_TLV_RCVD_MAC_PHY_BMP);
            LLDP_TRC (LLDP_MAC_PHY_TRC, "Validated" " MAC_PHY_TLV\n");
            i4RetVal = OSIX_SUCCESS;
            break;

        case LLDP_POWER_VIA_MDI_TLV:
            /* TODO: not supported in the current release. Hence discard the 
             * tlv
             */
            i4RetVal = OSIX_SUCCESS;
            break;
        case LLDP_LINK_AGG_TLV:
            if (gLldpGlobalInfo.i4Version == LLDP_VERSION_09)
            {
                *pbTlvDiscardFlag = OSIX_TRUE;
                i4RetVal = OSIX_SUCCESS;
                break;
            }
            if (u2TlvLength < LLDP_LINK_AGG_TLV_INFO_LEN)
            {
                LLDP_TRC (LLDP_LAGG_TRC, "Discarding"
                          " LLDPDU as obtained Link Agg TLV length is less than"
                          " actual len\n");
                /* Return failure as the location of the next TLV is 
                 * indeterminate */
                return OSIX_FAILURE;
            }
            if (LLDP_IS_SET_TLV_RCVD_BMP (pLocPortInfo,
                                          LLDP_TLV_RCVD_LINK_AGG_BMP)
                == OSIX_TRUE)
            {
                /* PDU should not contain more than one Link Agg TLV. Hence 
                 * discard the TLV */
                LLDP_TRC (LLDP_LAGG_TRC, "Received"
                          " more than one Link Agg TLV!!\n");
                *pbTlvDiscardFlag = OSIX_TRUE;
                return OSIX_SUCCESS;
            }

            /* If the received TLV indicates that link aggregation capability
             * is not supported but the status is enabled, then the TLV msut be
             * discarded */
            LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, 0, u1AggStatus);

            /* Decode the Tlv Information */
            InOctetStr.pu1_OctetList = &u1AggStatus;
            InOctetStr.i4_Length = sizeof (UINT1);
            OutOctetStr.pu1_OctetList = &u1RevAggStatus;
            OutOctetStr.i4_Length = sizeof (UINT1);

            /* Convert the Bit list to pkt string as per 
             * Std IEEE802.1AB-2005 section 9.1 */
            LldpReverseOctetString (&InOctetStr, &OutOctetStr);

            if (!(OutOctetStr.pu1_OctetList[0] & LLDP_AGG_CAP_BMAP) &&
                (OutOctetStr.pu1_OctetList[0] & LLDP_AGG_STATUS_BMAP))
            {
                *pbTlvDiscardFlag = OSIX_TRUE;
                LLDP_TRC (LLDP_LAGG_TRC, "Discarding"
                          " Link Agg TLV since aggregation capability is not"
                          " supported and status is enabled!!\n");
            }

            LLDP_SET_TLV_RCVD_BMP (pLocPortInfo, LLDP_TLV_RCVD_LINK_AGG_BMP);
            LLDP_TRC (LLDP_LINK_AGG_TLV, "Validated" " LINK_AGG_TLV\n");
            i4RetVal = OSIX_SUCCESS;
            break;
        case LLDP_MAX_FRAME_SIZE_TLV:
            if (u2TlvLength < LLDP_MAX_FRAME_SIZE_TLV_INFO_LEN)
            {
                LLDP_TRC (LLDP_MAX_FRAME_TRC, "Discarding"
                          " LLDPDU as obtained Max Frame Size TLV length is"
                          " less than actual len\n");
                /* Return failure as the location of the next TLV is
                 * indeterminate */
                return OSIX_FAILURE;
            }

            if (LLDP_IS_SET_TLV_RCVD_BMP (pLocPortInfo,
                                          LLDP_TLV_RCVD_MAX_FRAME_BMP)
                == OSIX_TRUE)
            {
                /* PDU should not contain more than one Max Frame Size TLV.
                 * Hence discard the TLV */
                LLDP_TRC (LLDP_MAC_PHY_TRC, "Received"
                          " more than one Max Frame Size TLV!!\n");
                *pbTlvDiscardFlag = OSIX_TRUE;
                return OSIX_SUCCESS;
            }

            LLDP_SET_TLV_RCVD_BMP (pLocPortInfo, LLDP_TLV_RCVD_MAX_FRAME_BMP);
            LLDP_TRC (LLDP_MAX_FRAME_SIZE_TLV, "Validated"
                      " MAX_FRAME_SIZE_TLV\n");
            i4RetVal = OSIX_SUCCESS;
            break;
            /* if the tlvsubtype doesn't match any of the above cases
             * then the TLV is considered as unrecognized TLV and the same
             * is handled above(LLDP_IS_UNRECOG_ORGDEF_TLV). Hence default 
             * case is not required */
    }
    return i4RetVal;
}

/*--------------------------------------------------------------------------*/
/*                       TLV Decode Routines                                */
/*--------------------------------------------------------------------------*/
/****************************************************************************
 *
 *    FUNCTION NAME    : LldpDot3TlvDecodeMacPhyInfo
 *
 *    DESCRIPTION      : This routine decodes the MAC/PHY Configuration/Status 
 *                       TLV information. After decoding the information is
 *                       updated in the structure passed by the caller.
 *
 *    INPUT            : pu1TlvInfo   - Points to the flag Field in the TLV
 *                      
 *    OUTPUT           : pAutoNegInfo - Decoded Value of TLV Information is 
 *                                      updated in this structure passed by
 *                                      the caller.
 *
 *    RETURNS          : None 
 *
 ****************************************************************************/
VOID
LldpDot3TlvDecodeMacPhyInfo (UINT1 *pu1TlvInfo,
                             tLldpxdot3AutoNegInfo * pAutoNegInfo)
{
    UINT1               u1AutoNegInfo = 0;
    tSNMP_OCTET_STRING_TYPE InOctetStr;
    tSNMP_OCTET_STRING_TYPE OutOctetStr;
    UINT1               au1InAutoNegAdvtCap[LLDP_MAX_LEN_PHY_MEDIA_CAP];
    UINT1               au1OutAutoNegAdvtCap[LLDP_MAX_LEN_PHY_MEDIA_CAP];

    MEMSET (&InOctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&OutOctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1InAutoNegAdvtCap[0], 0, LLDP_MAX_LEN_PHY_MEDIA_CAP);
    MEMSET (&au1OutAutoNegAdvtCap[0], 0, LLDP_MAX_LEN_PHY_MEDIA_CAP);

    InOctetStr.pu1_OctetList = &au1InAutoNegAdvtCap[0];
    InOctetStr.i4_Length = LLDP_MAX_LEN_PHY_MEDIA_CAP;
    OutOctetStr.pu1_OctetList = &au1OutAutoNegAdvtCap[0];
    OutOctetStr.i4_Length = LLDP_MAX_LEN_PHY_MEDIA_CAP;

    LLDP_LBUF_GET_1_BYTE (pu1TlvInfo, 0, u1AutoNegInfo);

    /* Bit 0 represents the auto-neg support and 
     * bit 1 represents the auto-neg status */
    if (u1AutoNegInfo & LLDP_AUTO_NEG_SUPP_BMAP)
    {
        pAutoNegInfo->u1AutoNegSupport = LLDP_TRUE;
    }
    else
    {
        pAutoNegInfo->u1AutoNegSupport = LLDP_FALSE;
    }

    if (u1AutoNegInfo & LLDP_AUTO_NEG_STAT_BMAP)
    {
        pAutoNegInfo->u1AutoNegEnabled = LLDP_TRUE;
    }
    else
    {
        pAutoNegInfo->u1AutoNegEnabled = LLDP_FALSE;
    }

    /* Decode the AutoNeg Advertised capability bits */
    LLDP_LBUF_GET_STRING (pu1TlvInfo, InOctetStr.pu1_OctetList, 0,
                          InOctetStr.i4_Length);
    /* Convert the Bit list to pkt string as per 
     * Std IEEE802.1AB-2005 section 9.1 */
    SNMP_ReverseOctetString (&InOctetStr, &OutOctetStr);
    PTR_FETCH2 (pAutoNegInfo->u2AutoNegAdvtCap, OutOctetStr.pu1_OctetList);

    LLDP_LBUF_GET_2_BYTE (pu1TlvInfo, 0, pAutoNegInfo->u2OperMauType);
}

/*--------------------------------------------------------------------------*/
/*                       TLV Update Routines                                */
/*--------------------------------------------------------------------------*/

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpDot3TlvUpdtOrgSpecTlv
 *
 *    DESCRIPTION      : This function updates the IEEE 802.3  
 *                       Oganizationally Specific TLV in the received frame.
 *
 *    INPUT            : pLocPortInfo - Port Info Structure
 *                       pu1TlvInfo - Pointer to the TLV SubType Specific 
 *                       Information 
 *                       u2TlvLength - Length of the Information Field
 *                       u1TlvSubType - TLV SubType
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpDot3TlvUpdtOrgSpecTlv (tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1TlvInfo,
                           UINT2 u2TlvLength, UINT1 u1TlvSubType)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;
    if (LLDP_IS_UNRECOG_ORGDEF_TLV (u1TlvSubType) == OSIX_TRUE)
    {
        i4RetVal = LldpTlvUpdtUnrecogOrgDefTlv (pLocPortInfo, pu1TlvInfo,
                                                u2TlvLength,
                                                gau1LldpDot3OUI, u1TlvSubType);
        LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "LldpRxUpdtDot3OrgSpecTlv: Received unknown Subtype\r\n");
        return i4RetVal;
    }

    if (pRemoteNode->pDot3RemPortInfo == NULL)
    {
        /* Dot3 Specific TLVs received for the first time. Hence allocate
         * memory*/
        if ((pRemoteNode->pDot3RemPortInfo =
             (tLldpxdot3RemPortInfo *)
             (MemAllocMemBlk (gLldpGlobalInfo.RemDot3PortInfoPoolId))) == NULL)
        {
            LLDP_TRC (ALL_FAILURE_TRC | LLDP_CRITICAL_TRC,
                      "LldpDot3TlvUpdtOrgSpecTlv: Mempool Allocation "
                      "Failure for New RemManAddr Node\r\n");
#ifndef FM_WANTED
            SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                          "LldpDot3TlvUpdtOrgSpecTlv: Mempool Allocation"
                          "Failure for New RemManAddr Node"));
#endif
            LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
            return OSIX_FAILURE;
        }
        MEMSET (pRemoteNode->pDot3RemPortInfo, 0,
                sizeof (tLldpxdot3RemPortInfo));
    }

    switch (u1TlvSubType)
    {
        case LLDP_MAC_PHY_CONFIG_STATUS_TLV:
            i4RetVal = LldpDot3TlvUpdtMacPhyTlv (pLocPortInfo,
                                                 pRemoteNode->pDot3RemPortInfo,
                                                 pu1TlvInfo);
            break;

        case LLDP_POWER_VIA_MDI_TLV:
            /*TODO: Not supported in current release */
            i4RetVal = OSIX_SUCCESS;
            break;
        case LLDP_LINK_AGG_TLV:
            if (gLldpGlobalInfo.i4Version == LLDP_VERSION_09)
            {
                i4RetVal = OSIX_SUCCESS;
                break;
            }
            i4RetVal = LldpDot3TlvUpdtLinkAggTlv (pLocPortInfo,
                                                  pRemoteNode->pDot3RemPortInfo,
                                                  pu1TlvInfo);
            break;
        case LLDP_MAX_FRAME_SIZE_TLV:
            i4RetVal =
                LldpDot3TlvUpdtMaxFrameSizeTlv (pLocPortInfo,
                                                pRemoteNode->pDot3RemPortInfo,
                                                pu1TlvInfo);
            break;
            /* if the tlvsubtype doesn't match any of the above cases
             * then the TLV is considered as unrecognized TLV and the same
             * is handled above(LLDP_IS_UNRECOG_ORGDEF_TLV). Hence default 
             * case is not required */
    }

    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpDot3TlvUpdtMacPhyTlv
 *
 *    DESCRIPTION      : This routine updates the MAC/PHY Configuration/Status
 *                       TLV information in the Remote Node.Also send the 
 *                       notification if there is any protocol
 *                       misconfiguration errors.
 *                        
 *    INPUT            : pLocPortInfo - Local Port Info structure where the 
 *                                      TLV is received
 *                       pDot3RemPortInfo - Pointer to Dot3 Specific Info 
 *                                          structure
 *                       pu1Tlv- Points to the TLV Subtype information field
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpDot3TlvUpdtMacPhyTlv (tLldpLocPortInfo * pLocPortInfo,
                          tLldpxdot3RemPortInfo * pDot3RemPortInfo,
                          UINT1 *pu1Tlv)
{
    tLldpxdot3AutoNegInfo *pLocAutoNegInfo = NULL;
    tLldpxdot3AutoNegInfo *pRemAutoNegInfo = NULL;
    tLldpRemoteNode    *pRemoteNode = NULL;
    tLldMisConfigTrap   MisConfigInfo;
    UINT2               u2ChassisIdLen = 0;
    UINT2               u2PortIdLen = 0;

    MEMSET (&MisConfigInfo, 0, sizeof (tLldMisConfigTrap));

    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;
    pRemoteNode->bRcvdMacPhyTlv = OSIX_TRUE;

    pRemAutoNegInfo = &pDot3RemPortInfo->RemDot3AutoNegInfo;
    /* Decode the Tlv Information */
    LldpDot3TlvDecodeMacPhyInfo (pu1Tlv, pRemAutoNegInfo);
    /* As the object is updated in Remote Node Structure , It will be inserted
     * in the Remote Table at the end of all updation */

    LLDP_TRC_ARG5 (LLDP_MAC_PHY_TRC, "Updating MAC/PHY"
                   " Configuration/Status TLV on port %d"
                   "with the following information \nAuto-Neg Support: %d\n"
                   "Auto-Neg Status: %d\n Advrt Capability: %ld\n"
                   "Oper Mau Type: %d\n", pLocPortInfo->u4LocPortNum,
                   pRemAutoNegInfo->u1AutoNegSupport,
                   pRemAutoNegInfo->u1AutoNegEnabled,
                   pRemAutoNegInfo->u2AutoNegAdvtCap,
                   pRemAutoNegInfo->u2OperMauType);

    /* Checking for misconfigurations */
    pLocAutoNegInfo = &pLocPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo;

    if (pLocAutoNegInfo->u2OperMauType != pRemAutoNegInfo->u2OperMauType)
    {
        /* Get the length of Chassis Id */
        LldpUtilGetChassisIdLen (pRemoteNode->i4RemChassisIdSubtype,
                                 pRemoteNode->au1RemChassisId, &u2ChassisIdLen);

        /* Get the length of port Id */
        LldpUtilGetPortIdLen (pRemoteNode->i4RemPortIdSubtype,
                              pRemoteNode->au1RemPortId, &u2PortIdLen);

        MisConfigInfo.i4RemChassisIdSubtype =
            pRemoteNode->i4RemChassisIdSubtype;
        MisConfigInfo.i4RemPortIdSubtype = pRemoteNode->i4RemPortIdSubtype;
        MisConfigInfo.u4RemTimeMark = pRemoteNode->u4RemLastUpdateTime;
        MisConfigInfo.i4RemLocalPortNum = pRemoteNode->i4RemLocalPortNum;
        MisConfigInfo.i4RemIndex = pRemoteNode->i4RemIndex;
        MEMCPY (&MisConfigInfo.au1RemChassisId,
                pRemoteNode->au1RemChassisId,
                MEM_MAX_BYTES (u2ChassisIdLen, LLDP_MAX_LEN_CHASSISID));
        MEMCPY (&MisConfigInfo.au1PortId,
                pRemoteNode->au1RemPortId,
                MEM_MAX_BYTES (u2PortIdLen, LLDP_MAX_LEN_PORTID));
        MisConfigInfo.MisOperMauType = pRemAutoNegInfo->u2OperMauType;
        /* Send Misconfiguration Notification */
        LldpSendNotification ((VOID *) &MisConfigInfo,
                              LLDP_MIS_CONFIG_OPER_MAU_TYPE);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpDot3TlvUpdtLinkAggTlv
 *
 *    DESCRIPTION      : This routine updates the Link Aggregation
 *                       TLV information in the Remote Node.Also send the 
 *                       notification if there is any protocol
 *                       misconfiguration errors.
 *
 *    INPUT            : pLocPortInfo - Local Port Info structure where the 
 *                                      TLV is received
 *                       pDot3RemPortInfo - Pointer to Dot3 Specific Info 
 *                                          structure
 *                       pu1Tlv- Points to the TLV Subtype information field

 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpDot3TlvUpdtLinkAggTlv (tLldpLocPortInfo * pLocPortInfo,
                           tLldpxdot3RemPortInfo * pDot3RemPortInfo,
                           UINT1 *pu1Tlv)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    tLldMisConfigTrap   MisConfigInfo;
    tSNMP_OCTET_STRING_TYPE InOctetStr;
    tSNMP_OCTET_STRING_TYPE OutOctetStr;
    UINT1               u1AggStatus = 0;
    UINT2               u2ChassisIdLen = 0;
    UINT2               u2PortIdLen = 0;

    MEMSET (&MisConfigInfo, 0, sizeof (tLldMisConfigTrap));

    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;
    pRemoteNode->bRcvdLinkAggTlv = OSIX_TRUE;

    /* Decode the Tlv Information */
    InOctetStr.pu1_OctetList = &u1AggStatus;
    InOctetStr.i4_Length = sizeof (UINT1);
    OutOctetStr.pu1_OctetList = &(pRemoteNode->pDot3RemPortInfo->u1AggStatus);
    OutOctetStr.i4_Length = sizeof (UINT1);

    MEMSET (InOctetStr.pu1_OctetList, 0, InOctetStr.i4_Length);
    MEMSET (OutOctetStr.pu1_OctetList, 0, OutOctetStr.i4_Length);
    LLDP_LBUF_GET_1_BYTE (pu1Tlv, 0, u1AggStatus);
    /* Convert the Bit list to pkt string as per 
     * Std IEEE802.1AB-2005 section 9.1 */
    SNMP_ReverseOctetString (&InOctetStr, &OutOctetStr);

    LLDP_LBUF_GET_4_BYTE (pu1Tlv, 0, pDot3RemPortInfo->u4AggPortId);

    LLDP_TRC_ARG3 (LLDP_LAGG_TRC, "Updating Link"
                   " Aggregation TLV on port %d with the following information"
                   "\nAggregation Capability/Status: %d\n"
                   "Aggregated Port ID: %d\n", pLocPortInfo->u4LocPortNum,
                   pDot3RemPortInfo->u1AggStatus,
                   pDot3RemPortInfo->u4AggPortId);

    /* As the object is updated in Remote Node Structure , It will be inserted
     * in the Remote Table at the end of all updation */
    /* Checking for misconfigurations */
    if (pDot3RemPortInfo->u1AggStatus != LLDP_LOC_PORT_AGG_INFO (pLocPortInfo))
    {
        /* Get the length of Chassis Id */
        LldpUtilGetChassisIdLen (pRemoteNode->i4RemChassisIdSubtype,
                                 pRemoteNode->au1RemChassisId, &u2ChassisIdLen);

        /* Get the length of port Id */
        LldpUtilGetPortIdLen (pRemoteNode->i4RemPortIdSubtype,
                              pRemoteNode->au1RemPortId, &u2PortIdLen);

        MisConfigInfo.i4RemPortIdSubtype = pRemoteNode->i4RemPortIdSubtype;
        MisConfigInfo.i4RemChassisIdSubtype =
            pRemoteNode->i4RemChassisIdSubtype;
        MisConfigInfo.u4RemTimeMark = pRemoteNode->u4RemLastUpdateTime;
        MisConfigInfo.i4RemLocalPortNum = pRemoteNode->i4RemLocalPortNum;
        MisConfigInfo.i4RemIndex = pRemoteNode->i4RemIndex;
        MEMCPY (&MisConfigInfo.au1RemChassisId,
                pRemoteNode->au1RemChassisId,
                MEM_MAX_BYTES (u2ChassisIdLen, LLDP_MAX_LEN_CHASSISID));
        MEMCPY (&MisConfigInfo.au1PortId,
                pRemoteNode->au1RemPortId,
                MEM_MAX_BYTES (u2PortIdLen, LLDP_MAX_LEN_PORTID));
        MisConfigInfo.MisLinkAggInfo = pDot3RemPortInfo->u1AggStatus;
        /* Send Misconfiguration Notification */
        LldpSendNotification ((VOID *) &MisConfigInfo,
                              LLDP_MIS_CONFIG_LINK_AGG_INFO);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpDot3TlvUpdtMaxFrameSizeTlv
 *
 *    DESCRIPTION      : This routine updates the Maximum Frame Size
 *                       TLV information in the Remote Node.Also send the 
 *                       notification if there is any protocol
 *                       misconfiguration errors.
 *                        
 *    INPUT            : pLocPortInfo - Local Port Info structure where the 
 *                                      TLV is received
 *                       pDot3RemPortInfo - Pointer to Dot3 Specific Info 
 *                                          structure
 *                       pu1Tlv- Points to the TLV Subtype information field
 *                       
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpDot3TlvUpdtMaxFrameSizeTlv (tLldpLocPortInfo * pLocPortInfo,
                                tLldpxdot3RemPortInfo * pDot3RemPortInfo,
                                UINT1 *pu1Tlv)
{
    tLldMisConfigTrap   MisConfigInfo;
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT2               u2ChassisIdLen = 0;
    UINT2               u2PortIdLen = 0;

    MEMSET (&MisConfigInfo, 0, sizeof (tLldMisConfigTrap));

    /* Get the Remote Node */
    pRemoteNode = pLocPortInfo->pBackPtrRemoteNode;
    pRemoteNode->bRcvdMaxFrameTlv = OSIX_TRUE;

    /* Decode the Tlv Information */
    LLDP_LBUF_GET_2_BYTE (pu1Tlv, 0, pDot3RemPortInfo->u2MaxFrameSize);
    /* As the object is updated in Remote Node Structure , It will be inserted
     * in the Remote Table at the end of all updation */
    LLDP_TRC_ARG2 (LLDP_MAX_FRAME_TRC, "Updating Maximum"
                   " Frame Size TLV on port %d with the following information"
                   "\nMaximum Frame Size: %d\n", pLocPortInfo->u4LocPortNum,
                   pDot3RemPortInfo->u2MaxFrameSize);

    /* Checking for misconfigurations */
    if (pDot3RemPortInfo->u2MaxFrameSize !=
        pLocPortInfo->Dot3LocPortInfo.u2MaxFrameSize)
    {
        /* Get the length of Chassis Id */
        LldpUtilGetChassisIdLen (pRemoteNode->i4RemChassisIdSubtype,
                                 pRemoteNode->au1RemChassisId, &u2ChassisIdLen);

        /* Get the length of port Id */
        LldpUtilGetPortIdLen (pRemoteNode->i4RemPortIdSubtype,
                              pRemoteNode->au1RemPortId, &u2PortIdLen);

        MisConfigInfo.i4RemPortIdSubtype = pRemoteNode->i4RemPortIdSubtype;
        MisConfigInfo.i4RemChassisIdSubtype =
            pRemoteNode->i4RemChassisIdSubtype;
        MisConfigInfo.u4RemTimeMark = pRemoteNode->u4RemLastUpdateTime;
        MisConfigInfo.i4RemLocalPortNum = pRemoteNode->i4RemLocalPortNum;
        MisConfigInfo.i4RemIndex = pRemoteNode->i4RemIndex;
        MEMCPY (&MisConfigInfo.au1RemChassisId,
                pRemoteNode->au1RemChassisId, MEM_MAX_BYTES (u2ChassisIdLen,
                                                             LLDP_MAX_LEN_CHASSISID));
        MEMCPY (&MisConfigInfo.au1PortId,
                pRemoteNode->au1RemPortId, MEM_MAX_BYTES (u2PortIdLen,
                                                          LLDP_MAX_LEN_PORTID));
        MisConfigInfo.MisMaxFrmSize = pDot3RemPortInfo->u2MaxFrameSize;
        /* Send Miscofiguration Notification */
        LldpSendNotification ((VOID *) &MisConfigInfo,
                              LLDP_MIS_CONFIG_MAX_FRAME_SIZE);
    }
    return OSIX_SUCCESS;
}

#endif /* _LLDP_DOT3TLV_C_ */
/*--------------------------------------------------------------------------*/
/*                       End of the file  lldo3tlv.c                      */
/*--------------------------------------------------------------------------*/
