/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved                    *
 *                                                                         *
 * $Id: lldrdsb.c,v 1.2 2013/03/28 11:45:11 siva Exp $                     *
 *                                                                         *
 * Description: This file contains the utility functions for LLDP stubs for*
 *              redundancy.                                                *
 ***************************************************************************/

#include "lldinc.h"

/* HITLESS RESTART */
/*****************************************************************************/
/* Function Name      : LldpRedHRProcStdyStPktReq                            */
/*                                                                           */
/* Description        : This function triggers the steady state packet from  */
/*                      LLDP to the RM by setting the periodic timeout value */
/*                      as zero                                              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LldpRedHRProcStdyStPktReq (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name      : LldpRedHRSendStdyStPkt                               */
/*                                                                           */
/* Description        : This function sends steady state packet to RM.       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT1
LldpRedHRSendStdyStPkt (tCRU_BUF_CHAIN_HEADER * pu1LinBuf, UINT4 u4PktLen,
                        UINT2 u2Port, UINT4 u4TimeOut)
{
    if (gLldpUtVariable == 2)
    {
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (pu1LinBuf);
    UNUSED_PARAM (u4PktLen);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u4TimeOut);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LldpRedHRSendStdyStTailMsg                           */
/*                                                                           */
/* Description        : This function is called when all the steady state    */
/*                      pkts were sent to the RM. It sends steady state tail */
/*                      message to RM module.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT1
LldpRedHRSendStdyStTailMsg (VOID)
{
    return OSIX_SUCCESS;
}

/******************************************************************************/
/*  Function           : LldpRedGetHRFlag                                     */
/*  Input(s)           : None                                                 */
/*  Output(s)          : None                                                 */
/*  Returns            : Hitless restart flag value.                          */
/*  Action             : This API returns the hitless restart flag value.     */
/******************************************************************************/
UINT1
LldpRedGetHRFlag (VOID)
{
    return OSIX_SUCCESS;
}
