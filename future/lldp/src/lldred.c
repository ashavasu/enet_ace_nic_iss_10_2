/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: lldred.c,v 1.38 2017/10/11 13:42:02 siva Exp $
 *
 * Description: This file contains LLDP High Availability handling
 *              routines.
 *********************************************************************/
#ifndef _LLDRED_C_
#define _LLDRED_C_

#include "lldinc.h"
UINT1               gau1LldPdu[LLDP_MAX_PDU_SIZE];

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedRcvPktFromRm 
 *
 *    DESCRIPTION      : This API constructs a message containing the
 *                       given RM event and RM message. And posts it to the
 *                       LLDP queue
 *
 *    INPUT            : u1Event   - Event type given by RM module 
 *                       pData     - RM Message to enqueue
 *                       u2DataLen - Length of the message
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
PUBLIC INT4
LldpRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tLldpQMsg          *pQMsg = NULL;

    /* If the received event is not any of the following, then just return
     * without processing the event */
    if ((u1Event != RM_MESSAGE) &&
        (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) &&
        (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) &&
        (u1Event != L2_INITIATE_BULK_UPDATES) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE) &&
        (u1Event != RM_COMPLETE_SYNC_UP) && (u1Event != L2_COMPLETE_SYNC_UP))
    {
        LLDP_TRC_ARG1 (LLDP_RED_TRC,
                       "LldpRedRcvPktFromRm: Received Invalid event:%d \r\n",
                       u1Event);
        return OSIX_FAILURE;
    }
    /* pData(message pointer is valid only if the event is 
     * RM_MESSAGE/RM_STANDBY_UP/RM_STANDBY_DOWN. If the recevied
     * event is any of the above mentioned event then validate the  
     * message pointer and message length */
    if ((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP)
        || (u1Event == RM_STANDBY_DOWN))
    {
        if (pData == NULL)
        {
            LLDP_TRC (LLDP_RED_TRC,
                      "LldpRedRcvPktFromRm: Received message with "
                      "message pointer as NULL\r\n");
            return OSIX_FAILURE;
        }
        if (u1Event == RM_MESSAGE)
        {
            if ((u2DataLen < LLDP_RED_MSG_HDR_SIZE) ||
                (u2DataLen > LLDP_RED_MAX_MSG_SIZE))
            {
                RM_FREE (pData);
                LLDP_TRC (LLDP_RED_TRC,
                          "LldpRedRcvPktFromRm: Received invalid message\r\n");
                return OSIX_FAILURE;
            }
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            if (u2DataLen != RM_NODE_COUNT_MSG_SIZE)
            {
                LldpPortRelRmMsgMem ((UINT1 *) pData);
                LLDP_TRC (LLDP_RED_TRC,
                          "LldpRedRcvPktFromRm: Received invalid message\r\n");
                return OSIX_FAILURE;
            }
        }
    }

    /* Allocate Interface message buffer from the pool. If the 
     * memory allocation failed then release the memory allocated 
     * for the message pointer(if any) */
    if ((pQMsg = (tLldpQMsg *) (MemAllocMemBlk
                                (gLldpGlobalInfo.QMsgPoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_RED_TRC,
                  "LldpRedRcvPktFromRm: Memory allocation failed for RM "
                  "message\r\n");
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpRedRcvPktFromRm: Memory allocation failed for RM"
                      "message"));
#endif
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            LldpPortRelRmMsgMem ((UINT1 *) pData);
        }
        return OSIX_FAILURE;
    }

    MEMSET (pQMsg, 0, sizeof (tLldpQMsg));

    pQMsg->u4MsgType = LLDP_RM_MSG;

    pQMsg->LldpRmMsg.u1Event = u1Event;
    pQMsg->LldpRmMsg.pData = pData;
    pQMsg->LldpRmMsg.u2DataLen = u2DataLen;

    if (LldpQueEnqAppMsg (pQMsg) == OSIX_FAILURE)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_RED_TRC,
                  "LldpRedRcvPktFromRm: Enqueuing RmMessage to LLDP message "
                  "queue failed\r\n");

        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            LldpPortRelRmMsgMem ((UINT1 *) pData);
        }
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedProcessRmMsg 
 *
 *    DESCRIPTION      : This function processes the received RM event
 *
 *    INPUT            : pRmCtrlMsg - Pointer to the structure containing RM
 *                                    buffer and the RM event
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
PUBLIC VOID
LldpRedProcessRmMsg (tLldpRmCtrlMsg * pRmCtrlMsg)
{
    tRmNodeInfo        *pRmNode = NULL;
    tRmProtoAck         ProtoAck;
    UINT4               u4SeqNum = 0;

    /* No need to NULL check for pRmCtrlMsg here , since it is already done in 
     * LldpRedRcvPktFromRm */

    switch (pRmCtrlMsg->u1Event)
    {
        case RM_MESSAGE:

            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM (pRmCtrlMsg->pData, &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR (pRmCtrlMsg->pData, pRmCtrlMsg->u2DataLen);

            ProtoAck.u4AppId = RM_LLDP_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            LLDP_TRC (LLDP_RED_TRC, " Received Valid message\r\n");
            LldpRedProcSyncUpMsg (pRmCtrlMsg);
            if (gLldpRedGlobalInfo.bGlobShutWhileRcvdTimeProcessed
                == OSIX_FALSE)
            {
                /* The Changes were introduced to avoid duplicate memory release 
                 * during LLDP_RED_TMR_EXP_MSG event handling,  which is already 
                 * released by LLPD - Shut in Active - Standby Scenario */

                RM_FREE (pRmCtrlMsg->pData);
            }
            /* Sending ACK to RM */
            RmApiSendProtoAckToRM (&ProtoAck);
            break;

        case GO_ACTIVE:
            LLDP_TRC (LLDP_RED_TRC, " Received GO_ACTIVE event\r\n");

            if (LLDP_RED_NODE_STATUS () == RM_ACTIVE)
            {
                break;
            }
            LldpRedHandleGoActive ();
            if (LLDP_BULK_UPD_REQ_RECD () == OSIX_TRUE)
            {
                LLDP_BULK_UPD_REQ_RECD () = OSIX_FALSE;
                LldpRedInitBulkUpdateFlags ();
                LLDP_RED_BULK_UPD_STATUS () = LLDP_BULK_UPD_IN_PROGRESS;
                LldpRedProcBulkUpdReq ();
            }
            break;

        case GO_STANDBY:
            LLDP_TRC (LLDP_RED_TRC, " Received GO_STANDBY event\r\n");
            if (LLDP_RED_NODE_STATUS () == RM_STANDBY)
            {
                break;
            }
            LldpRedHandleGoStandby ();
            LLDP_BULK_UPD_REQ_RECD () = OSIX_FALSE;
            break;

        case RM_INIT_HW_AUDIT:
            /* HW Audit not implemented for LLDP */
            break;

        case RM_STANDBY_UP:
            LLDP_TRC (LLDP_RED_TRC, " Received Standby UP event\r\n");
            pRmNode = (tRmNodeInfo *) pRmCtrlMsg->pData;
            /* update Standby node count */
            gLldpRedGlobalInfo.u1NumPeersUp = pRmNode->u1NumStandby;
            LldpPortRelRmMsgMem ((UINT1 *) pRmNode);
            LldpRedHandleStandByUpEvent ();
            break;

        case RM_STANDBY_DOWN:
            LLDP_TRC (LLDP_RED_TRC, " Recevied Standby DOWN event\r\n");
            pRmNode = (tRmNodeInfo *) pRmCtrlMsg->pData;
            /* update Standby node count */
            gLldpRedGlobalInfo.u1NumPeersUp = pRmNode->u1NumStandby;
            LldpPortRelRmMsgMem ((UINT1 *) pRmNode);
            break;

        case L2_INITIATE_BULK_UPDATES:
            LLDP_TRC (LLDP_RED_TRC, " Recevied L2_INITIATE_BULK_UPDATES  "
                      "event\r\n");
            LldpRedSendBulkUpdReq ();
            break;

        case RM_CONFIG_RESTORE_COMPLETE:
            LLDP_TRC (LLDP_RED_TRC, " Recevied config restore complete "
                      "event\r\n");
            if (LLDP_RED_NODE_STATUS () == RM_INIT)
            {
                if (LldpPortGetRmNodeState () == (UINT1) RM_STANDBY)
                {
                    LldpRedMakeNodeStandbyFromIdle ();
                }
            }
            break;
        case RM_COMPLETE_SYNC_UP:
#if !defined (CFA_WANTED) && !defined (PNAC_WANTED) && !defined (LA_WANTED) \
            && !defined (RSTP_WANTED) && !defined (MSTP_WANTED) \
                && !defined (VLAN_WANTED) && !defined (ECFM_WANTED) \
                && !defined (ELMI_WANTED) && !defined (IGS_WANTED) \
                && !defined (MLDS_WANTED)
            LldpRedTrigHigherLayer (L2_COMPLETE_SYNC_UP);
#endif
            break;
        case L2_COMPLETE_SYNC_UP:
            LldpRedTrigHigherLayer (L2_COMPLETE_SYNC_UP);
            break;
        default:
            LLDP_TRC (LLDP_RED_TRC, " Received invalid event\r\n");
            break;
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedHandleStandByUpEvent 
 *
 *    DESCRIPTION      : This function handles the Standby up event. 
 *                       It processes the bulk update requset event if 
 *                       the bulk update request is received before
 *                       receiving Standby up event.
 *                       
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedHandleStandByUpEvent (VOID)
{
    if (LLDP_BULK_UPD_REQ_RECD () == OSIX_TRUE)
    {
#if !defined (CFA_WANTED) && !defined (PNAC_WANTED) && !defined (LA_WANTED) \
        && !defined (RSTP_WANTED) && !defined (MSTP_WANTED) \
            && !defined (VLAN_WANTED) && !defined (ECFM_WANTED) \
            && !defined (ELMI_WANTED) && !defined (IGS_WANTED) \
            && !defined (MLDS_WANTED)
        LLDP_BULK_UPD_REQ_RECD () = OSIX_FALSE;
        /* Bulk request msg is recieved before RM_STANDBY_UP event.
         * So we are sending bulk updates now. */

        LldpRedInitBulkUpdateFlags ();
        LLDP_RED_BULK_UPD_STATUS () = LLDP_BULK_UPD_IN_PROGRESS;
        LldpRedProcBulkUpdReq ();
#endif
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedSendBulkUpdReq 
 *
 *    DESCRIPTION      : This function sends bulk update request message to RM
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedSendBulkUpdReq (VOID)
{
    tRmMsg             *pRmMsg = NULL;
    UINT1               u1MsgType = 0;
    UINT2               u2MsgLen = 0;
    UINT4               u4Offset = 0;

    if (LLDP_RED_NODE_STATUS () != RM_STANDBY)
    {
        LLDP_TRC (LLDP_RED_TRC, "LldpRedSendBulkUpdReq: Trying to send "
                  "bulk req when LLDP node status is not STANDBY\r\n");
        return;
    }

    /* bulk request message type and length */
    u1MsgType = (UINT1) LLDP_RED_BULK_UPDT_REQ_MSG;
    u2MsgLen = (UINT2) LLDP_RED_BULK_UPD_REQ_MSG_LEN;

    if (LldpRedAllocMemForRmMsg (u1MsgType, u2MsgLen, &pRmMsg) != OSIX_SUCCESS)
    {
        return;
    }
    /* form bulk update request message */
    LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
    LLDP_RED_PUT_2_BYTE (pRmMsg, u4Offset, u2MsgLen);
    /* Send bulk update request message to RM */
    LldpRedSendMsgToRm (u1MsgType, u2MsgLen, pRmMsg);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedMakeNodeStandbyFromIdle
 *
 *    DESCRIPTION      : This function makes node standby from idle state
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
LldpRedMakeNodeStandbyFromIdle (VOID)
{
    LLDP_RED_NODE_STATUS () = RM_STANDBY;

    gLldpRedGlobalInfo.u1NumPeersUp = 0;

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (LLDP_RED_TRC,
                  "LldpRedMakeNodeStandbyFromIdle: LLDP Node made "
                  "standby and LLDP is not started\r\n");
        return;
    }

    if (LLDP_MODULE_STATUS () == LLDP_ENABLED)
    {
        LldpModuleEnable ();
        LLDP_TRC (LLDP_RED_TRC, "LldpRedMakeNodeStandbyFromIdle: Enabled "
                  "module\r\n");
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedTrigHigherLayer 
 *
 *    DESCRIPTION      : This function sends L2_SYNC_UP_COMPLETED event to
 *                       RM.
 *
 *    INPUT            : u1TrigType - Trigger type(L2_COMPLETE_SYNC_UP)
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedTrigHigherLayer (UINT1 u1TrigType)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    if ((LLDP_RED_NODE_STATUS () == RM_ACTIVE) &&
        (u1TrigType == L2_COMPLETE_SYNC_UP))
    {
        /* send L2_SYNC_UP_COMPLETED event to RM */
        ProtoEvt.u4Event = (UINT4) L2_SYNC_UP_COMPLETED;
        ProtoEvt.u4AppId = RM_LLDP_APP_ID;
        ProtoEvt.u4Error = RM_NONE;
        if (LldpPortSendEventToRm (&ProtoEvt) != OSIX_SUCCESS)
        {
            LLDP_TRC (LLDP_RED_TRC,
                      "LldpRedTrigHigherLayer: Sending L2_SYNC_UP_COMPLETED"
                      "event to RM failed\r\n");
        }

    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedHandleGoActive 
 *
 *    DESCRIPTION      : This function handles the GO_ACTIVE event from RM
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
LldpRedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    /* update the standby node count */
    gLldpRedGlobalInfo.u1NumPeersUp = LldpPortGetStandbyNodeCount ();

    if (LLDP_RED_NODE_STATUS () == RM_ACTIVE)
    {
        return;
    }

    else if (LLDP_RED_NODE_STATUS () == RM_INIT)
    {
        LldpRedMakeNodeActiveFromIdle ();
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }

    else if (LLDP_RED_NODE_STATUS () == RM_STANDBY)
    {
        LldpRedMakeNodeActiveFromStandby ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }

    /* send go active complete event to RM */
    ProtoEvt.u4AppId = RM_LLDP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;
    if (LldpPortSendEventToRm (&ProtoEvt) != OSIX_SUCCESS)
    {
        LLDP_TRC (LLDP_RED_TRC,
                  "LldpRedHandleGoActive: Sending Go Active complete "
                  "event to RM failed\r\n");
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedMakeNodeActiveFromStandby
 *
 *    DESCRIPTION      : This function makes node Active from Standby state
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
LldpRedMakeNodeActiveFromStandby (VOID)
{
    LLDP_TRC (LLDP_RED_TRC,
              "LldpRedMakeNodeActiveFromStandby: Made node Active from "
              "Standby state\r\n");

    /* update the node status and standby node count */
    LLDP_RED_NODE_STATUS () = RM_ACTIVE;
    gLldpRedGlobalInfo.u1NumPeersUp = LldpPortGetStandbyNodeCount ();

    /* In Active node, the global shut down while timer is started
     * when module is disabled or LLDP system control is shutdown.
     * So, timer start event would have been received by Standby node
     * and expected expiry time would have been calculated and
     * stored in timer sync up database(even if LLDP module is in disabled
     * state). And before global shutdown while timer 
     * expiry if force switchover is initated then the remaining time
     * for which the global shutdown while timer need to be run has to
     * be calculated and applied(even if LLDP module status is disabled) */

    /* verify whether global shutdown while timer start sync up is received or
     * not, if no global shutdown while timer start sync up is 
     * received(u4GlobShutWhileExpTime == 0), then no need to start the 
     * timer */
    if (gLldpRedGlobalInfo.u4GlobShutWhileExpTime != 0)
    {
        LldpRedApplyGlobShutWhileTmrSyncUp ();
    }

    if (LLDP_MODULE_STATUS () == LLDP_DISABLED)
    {
        return;
    }

    /* verify whether notification interval timer start sync up is received or
     * not, if no notification interval timer start sync up is 
     * received(u4NotifIntTmrExpTime == 0), then no need to start the 
     * timer */
    if (gLldpRedGlobalInfo.u4NotifIntTmrExpTime != 0)
    {
        LldpRedApplyNotifIntTmrSyncUp ();
    }

    /* for this timer the verification(whether timer start sync up is 
     * received or not) is done inside the function */
    LldpRedApplyRxInfoAgeTmrSyncUp ();

    /* If the tail message is not received then the remote node information
     * will not be complete. Hence delete all the remote node information*/
    if (LLDP_RED_TAIL_MSG_RCVD () == OSIX_FALSE)
    {
        LldpRxUtlDrainRemSysTables ();
    }

    /* In order to prevent the remote entries(present in peer node)
     * corresponding to this node getting aged out, transmit LLDPDU from 
     * new Active node if the LLDP transmit sem state is IDLE */
    LldpRedTransmitLldpdu ();
    /* read remaining time sync up database and
     * apply timer values and start timers */

    /* reset the timer sync up info in the redundancy global information
     * structure */
    gLldpRedGlobalInfo.u4GlobShutWhileExpTime = 0;
    gLldpRedGlobalInfo.u4NotifIntTmrExpTime = 0;
    gLldpRedGlobalInfo.u4GlobShutWhileRcvdTime = 0;
    gLldpRedGlobalInfo.u4NotifIntTmrRcvdTime = 0;
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedApplyGlobShutWhileTmrSyncUp
 *
 *    DESCRIPTION      : This function applies the global shutdown while
 *                       timer sync up information on global shutdown while 
 *                       timer
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
LldpRedApplyGlobShutWhileTmrSyncUp (VOID)
{
    UINT4               u4GlobShutWhileTmrRemTime = 0;
    UINT1               u1TimerId = 0;

    /* calculate remaining time for which the timer has to be started
     * Remaining time = Expected expiry time - current system time */
    u4GlobShutWhileTmrRemTime = (gLldpRedGlobalInfo.u4GlobShutWhileExpTime -
                                 UtlGetTimeSinceEpoch ());

    /* The remaining time will be greater than the received time, if the 
     * switchover occured after the duration for which the timer is started
     * in active node. 
     * In this case there is a possibility that the timer would have 
     * expired in previous Active node, and in previous Active node 
     * whenever global shut while timer expires the expires the timer expiry 
     * event synced with Standby node(while processing the sync up, the 
     * expected expiry time is reset to zero(0)), the remaining time will 
     * never be greater than the received time. */

    /* Remaining time will be zero, if the switchover occurs when the 
     * global shut while timer is about to expire */
    if (u4GlobShutWhileTmrRemTime == 0)
    {
        /* Global shut while timer has expired, so call the expiry call back
         * function(LldpTmrGlobalShutWhileTmrExp) */
        u1TimerId = (UINT1) LLDP_TX_TMR_GLOBAL_SHUT_WHILE;
        (*(gLldpGlobalInfo.aTmrDesc[u1TimerId].TmrExpFn)) (NULL);
        return;
    }

    /* if Remaining time is less than or equal to the received time interval 
     * then start the timer */
    else if (u4GlobShutWhileTmrRemTime <=
             gLldpRedGlobalInfo.u4GlobShutWhileRcvdTime)
    {
        if (TmrStart (gLldpGlobalInfo.TmrListId,
                      &(gLldpGlobalInfo.GlobalShutWhileTmr),
                      LLDP_TX_TMR_GLOBAL_SHUT_WHILE,
                      u4GlobShutWhileTmrRemTime, 0) != TMR_SUCCESS)
        {
            LLDP_TRC (LLDP_RED_TRC | LLDP_CRITICAL_TRC,
                      "LldpRedApplyGlobShutWhileTmrSyncUp: Starting "
                      "global ShutWhileTmr failed\r\n");
            return;
        }
        LLDP_GLOB_SHUT_WHILE_TMR_STATUS () = LLDP_TMR_RUNNING;
        LLDP_TRC (LLDP_RED_TRC,
                  "LldpRedApplyGlobShutWhileTmrSyncUp: Started global "
                  "ShutWhile timer successfully\r\n");
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedApplyNotifIntTmrSyncUp
 *
 *    DESCRIPTION      : This function applies the notification interval 
 *                       timer sync up information on notification 
 *                       interval timer
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
LldpRedApplyNotifIntTmrSyncUp (VOID)
{
    UINT4               u4NotifIntTmrRemTime = 0;
    UINT1               u1TimerId = 0;
    BOOL1               bNotifEnableFlag = OSIX_FALSE;
    tLldpLocPortInfo   *pPortInfo = NULL;
    tLldpv2AgentToLocPort LldpAgentPortInfo;
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;

    /* Scan through all the valid ports and if notification is disabled
     * on all the ports, to verify that whether notification is enabled or 
     * disabled */

    MEMSET (&LldpAgentPortInfo, 0, sizeof (tLldpv2AgentToLocPort));

    while ((pLldpAgentPortInfo = LldpGetNextConfigPortMapNode
            (&LldpAgentPortInfo)) != NULL)
    {
        MEMCPY (&LldpAgentPortInfo, pLldpAgentPortInfo,
                sizeof (tLldpv2AgentToLocPort));

        pPortInfo = LLDP_GET_LOC_PORT_INFO (pLldpAgentPortInfo->u4LldpLocPort);
        if (pPortInfo == NULL)
        {
            LLDP_TRC_ARG1 (LLDP_RED_TRC,
                           "LldpRedApplyNotifIntTmrSyncUp: Getting local port "
                           "information failed for port:%d\r\n",
                           pLldpAgentPortInfo->u4LldpLocPort);
            return;
        }

        if (pPortInfo->PortConfigTable.u1NotificationEnable == LLDP_TRUE)
        {
            bNotifEnableFlag = OSIX_TRUE;
            break;
        }
    }

    /* if notification is disabled then no need to start the notification 
     * interval timer just return */
    if (bNotifEnableFlag == OSIX_FALSE)
    {
        return;
    }

    /* calculate remaining time for which the timer has to be started
     * Remaining time = Expected expiry time - current system time */
    u4NotifIntTmrRemTime = (gLldpRedGlobalInfo.u4NotifIntTmrExpTime -
                            UtlGetTimeSinceEpoch ());
    /* The remaining time will be greater than the received time, if the 
     * switchover occured after the duration for which the timer is started
     * in active node. 
     * In this case there is a possibility that the timer would have 
     * expired/stopped in previous Active node, and in previous Active node 
     * whenever notification interval timer expires the
     * notification is sent(if any remote table change occured). So, no
     * need to call timer expiry function in new Active node after the 
     * switch over, if the timer has already expired/stopped in previous 
     * Active node */

    /* Remaining time will be zero, if the switchover occurs when the 
     * notification interval timer is about to expire */
    if (u4NotifIntTmrRemTime == 0)
    {
        /* notificatiion interval timer has expired, so call the 
         * expiry call back function(LldpTmrNotifIntervalTmrExp) */
        u1TimerId = (UINT1) LLDP_NOTIF_INT_TIMER;
        (*(gLldpGlobalInfo.aTmrDesc[u1TimerId].TmrExpFn)) (NULL);
        return;
    }
    /* if Remaining time is less than or equal to the received time interval 
     * then start the timer */
    else if (u4NotifIntTmrRemTime <= gLldpRedGlobalInfo.u4NotifIntTmrRcvdTime)
    {
        if (TmrStart (gLldpGlobalInfo.TmrListId,
                      &(gLldpGlobalInfo.NotifIntervalTmr),
                      LLDP_NOTIF_INT_TIMER,
                      u4NotifIntTmrRemTime, 0) != TMR_SUCCESS)
        {
            LLDP_TRC (LLDP_RED_TRC | LLDP_CRITICAL_TRC,
                      "LldpRedApplyNotifIntTmrSyncUp: Starting "
                      "notification interval timer failed\r\n");
            return;
        }
        LLDP_NOTIF_INTVAL_TMR_STATUS () = LLDP_TMR_RUNNING;
        LLDP_TRC (LLDP_RED_TRC,
                  "LldpRedApplyNotifIntTmrSyncUp: Started notification "
                  "interval timer successfully\r\n");
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedApplyRxInfoAgeTmrSyncUp
 *
 *    DESCRIPTION      : This function applies the rxinfo age out
 *                       timer sync up information on rxinfo age out timer
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
LldpRedApplyRxInfoAgeTmrSyncUp (VOID)
{
    tLldpRemoteNode    *pFirstRemoteNode = NULL;
    tLldpRemoteNode    *pNextRemoteNode = NULL;

    pFirstRemoteNode = (tLldpRemoteNode *) RBTreeGetFirst
        (gLldpGlobalInfo.RemSysDataRBTree);

    /* No remote entry present in remote table for this port */
    if (pFirstRemoteNode == NULL)
    {
        return;
    }

    LldpRedApplyTmrSyncUpOnRemNode (pFirstRemoteNode);

    while (LldpRxUtlGetNextRemoteNode (pFirstRemoteNode, &pNextRemoteNode)
           == OSIX_SUCCESS)
    {
        LldpRedApplyTmrSyncUpOnRemNode (pNextRemoteNode);
        /* get the next node pointer */
        pFirstRemoteNode = pNextRemoteNode;
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedApplyTmrSyncUpOnRemNode
 *
 *    DESCRIPTION      : This function applies the rxinfo age out
 *                       timer sync up information for the given remote node
 *
 *    INPUT            : pRemoteNode - pointer to remote node information
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
LldpRedApplyTmrSyncUpOnRemNode (tLldpRemoteNode * pRemoteNode)
{
    UINT4               u4RxInfoAgeTmrRemTime = 0;
    UINT1               u1TimerId = 0;

    /* if no rxinfo age out timer start sync up is received,
     * then no need to start the timer */
    if (pRemoteNode->u4RxInfoAgeTmrExpTime == 0)
    {
        return;
    }

    /* The remaining time will be greater than the received time, if the 
     * switchover occured after the duration for which the timer is started
     * in active node. 
     * In this case there is a possibility that the timer would have 
     * expired in previous Active node, and in previous Active node 
     * whenever rxinfo age timer expires the expires the timer expiry 
     * event synced with Standby node as remote table age out sync up.
     * And the corresponfing remote entry is deleted whenever age out
     * sync up is received. So, the the remaining time will never be greater 
     * than the received time. */

    /* calculate remaining time for which the timer has to be started
     * Remaining time = Expected expiry time - current system time */
    u4RxInfoAgeTmrRemTime = (pRemoteNode->u4RxInfoAgeTmrExpTime -
                             UtlGetTimeSinceEpoch ());

    /* Remaining time will be zero, if the switchover occurs when the 
     * rxinfo age out timer is about to expire */
    if (u4RxInfoAgeTmrRemTime == 0)
    {
        /* rxinfo age out timer has expired, so call the 
         * expiry call back function(LldpTmrRxInfoAgeTmrExp) */
        u1TimerId = (UINT1) LLDP_RX_TMR_RX_INFO_TTL;
        (*(gLldpGlobalInfo.aTmrDesc[u1TimerId].TmrExpFn)) (pRemoteNode);
        return;
    }

    /* if Remaining time is less than or equal to the received time interval 
     * then start the timer */
    else if (u4RxInfoAgeTmrRemTime <= pRemoteNode->u4RxTTL)
    {
        if (TmrStart (gLldpGlobalInfo.TmrListId, &(pRemoteNode->RxInfoAgeTmr),
                      LLDP_RX_TMR_RX_INFO_TTL,
                      u4RxInfoAgeTmrRemTime, 0) != TMR_SUCCESS)
        {
            LLDP_TRC (LLDP_RED_TRC | LLDP_CRITICAL_TRC,
                      "LldpRedApplyTmrSyncUpOnRemNode: Starting rxinfo "
                      "ageout timer failed\r\n");
            return;
        }
        pRemoteNode->RxInfoAgeTmrNode.u1Status = LLDP_TMR_RUNNING;
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedTransmitLldpdu
 *
 *    DESCRIPTION      : This function constructs and transmits LLDPDUs on all 
 *                       available ports, if the current transmit sem state of 
 *                       the port is IDLE
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
LldpRedTransmitLldpdu (VOID)
{
    tLldpLocPortInfo   *pPortInfo = NULL;
    tLldpv2AgentToLocPort LldpAgentPortInfo;
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;

    MEMSET (&LldpAgentPortInfo, 0, sizeof (tLldpv2AgentToLocPort));

    while ((pLldpAgentPortInfo = LldpGetNextConfigPortMapNode
            (&LldpAgentPortInfo)) != NULL)
    {
        MEMCPY (&LldpAgentPortInfo, pLldpAgentPortInfo,
                sizeof (tLldpv2AgentToLocPort));
        pPortInfo = LLDP_GET_LOC_PORT_INFO (pLldpAgentPortInfo->u4LldpLocPort);
        if (pPortInfo == NULL)
        {
            LLDP_TRC_ARG1 (LLDP_RED_TRC,
                           "LldpRedTransmitLldpdu: Getting local port "
                           "information failed for port:%d\r\n",
                           pLldpAgentPortInfo->u4LldpLocPort);
            return;
        }

        if ((pPortInfo->i4TxSemCurrentState != LLDP_TX_IDLE) &&
            (pPortInfo->i4TxTimerSemCurrentState != LLDP_TX_IDLE))
        {
            continue;
        }

        /* if any local change has occured then re-construct the 
         * preformed buffer */
        if (pPortInfo->bSomethingChangedLocal == OSIX_TRUE)
        {
            if (LldpTxUtlConstructPreformedBuf (pPortInfo, LLDP_INFO_FRAME)
                != OSIX_SUCCESS)
            {
                LLDP_TRC_ARG1 (LLDP_RED_TRC,
                               "LldpRedTransmitLldpdu: constructing preformed "
                               "buffer failed for port: %d\r\n",
                               pPortInfo->u4LocPortNum);
                continue;
            }
            pPortInfo->bSomethingChangedLocal = OSIX_FALSE;
        }
        LldpTxSmMachine (pPortInfo, LLDP_TX_EV_TX_NOW);
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedMakeNodeActiveFromIdle
 *
 *    DESCRIPTION      : This function makes node Active from Idle state 
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
LldpRedMakeNodeActiveFromIdle (VOID)
{
    LLDP_TRC (LLDP_RED_TRC,
              "LldpRedMakeNodeActiveFromIdle: Made node Active from IDLE "
              "state\r\n");

    /* update the node status */
    LLDP_RED_NODE_STATUS () = RM_ACTIVE;
    /* store the system time at which active node boots up
     * for the first time */
    if (LLDP_RED_ACTIVE_NODE_UP_SYSTIME () == 0)
    {
        /* Get current linux system time and store it in global structure */
        LLDP_RED_ACTIVE_NODE_UP_SYSTIME () = UtlGetTimeSinceEpoch ();
    }

    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        LLDP_TRC (LLDP_RED_TRC,
                  "LldpRedMakeNodeActiveFromIdle: Lldp Node Made node "
                  "Active When Lldp System control is in Shutdown "
                  "state\r\n");
        return;
    }
    /*
     * Configurations are restored before GO_ACTIVE/GO_STANDBY event is
     * received by protocol. So, during config restoration if module 
     * status has been set to LLDP_ENABLED, then enable LLDP module now.
     */
    if (LLDP_MODULE_STATUS () == LLDP_ENABLED)
    {
        LldpModuleEnable ();
        LLDP_TRC (LLDP_RED_TRC,
                  " LldpRedMakeNodeActiveFromIdle: Enabled LLDP " "module\r\n");
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedHandleStandby
 *
 *    DESCRIPTION      : This function handles the GO_STANDBY event from RM
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
LldpRedHandleGoStandby (VOID)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    LLDP_RED_TAIL_MSG_RCVD () = OSIX_FALSE;

    if ((LLDP_RED_NODE_STATUS () == RM_STANDBY) ||
        (LLDP_RED_NODE_STATUS () == RM_INIT))
    {
        /* Incase of current node state is RM_INIT, Discard this 
         * event, Node will become standby on RM_CONFIG_RESTORE_COMPLETE 
         * */
        return;
    }

    else if (LLDP_RED_NODE_STATUS () == RM_ACTIVE)
    {
        LldpRedMakeNodeStandbyFromActive ();
    }

    /* Intimate RM about the completion of GO_STANDBY process */
    ProtoEvt.u4AppId = RM_LLDP_APP_ID;
    ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
    ProtoEvt.u4Error = RM_NONE;
    if (LldpPortSendEventToRm (&ProtoEvt) != OSIX_SUCCESS)
    {
        LLDP_TRC (LLDP_RED_TRC, "LldpRedHandleGoStandby: Sending Go "
                  "Standby completion event to RM failed\r\n");
        return;

    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedMakeNodeStandbyFromActive
 *
 *    DESCRIPTION      : This function makes node Standby from Active state
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
LldpRedMakeNodeStandbyFromActive (VOID)
{
    tLldpLocPortInfo   *pPortInfo = NULL;
    tLldpv2AgentToLocPort LldpAgentPortInfo;
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;

    /* update node status */
    LLDP_RED_NODE_STATUS () = RM_STANDBY;
    /* initialize standby node count */
    gLldpRedGlobalInfo.u1NumPeersUp = 0;
    LLDP_RED_BULK_UPD_STATUS () = LLDP_BULK_UPD_NOT_STARTED;

    if (LLDP_MODULE_STATUS () == LLDP_ENABLED)
    {
        /* Disable the module inorder to flush the dynamic information */
        LldpModuleDisable ();
        LldpModuleEnable ();
    }
    else if (LLDP_SYSTEM_CONTROL () == LLDP_SHUTDOWN_INPROGRESS)
    {
        /* We need to forcefully stop the Global Shutwhile Timer */
        if (TmrStopTimer (gLldpGlobalInfo.TmrListId,
                          &(gLldpGlobalInfo.GlobalShutWhileTmr.TimerNode))
            != TMR_SUCCESS)
        {
            LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "LldpRedMakeNodeStandbyFromActive: Failed to stop the "
                      "Global ShutWhile Timer\r\n");
        }
        LLDP_GLOB_SHUT_WHILE_TMR_STATUS () = LLDP_TMR_NOT_RUNNING;
        /* Timer Expiry Handelr function need not to be called from here. 
         * It will be called by LldpRedProcTmrExpSyncUp() after getting the
         * Tmr Exp sync up message from current Active node */
    }

    /* Make the timer's status as not running  and reset the traffic counters */
    LLDP_NOTIF_INTVAL_TMR_STATUS () = LLDP_TMR_NOT_RUNNING;

    MEMSET (&LldpAgentPortInfo, 0, sizeof (tLldpv2AgentToLocPort));

    while ((pLldpAgentPortInfo = LldpGetNextConfigPortMapNode
            (&LldpAgentPortInfo)) != NULL)
    {
        MEMCPY (&LldpAgentPortInfo, pLldpAgentPortInfo,
                sizeof (tLldpv2AgentToLocPort));

        pPortInfo = LLDP_GET_LOC_PORT_INFO (pLldpAgentPortInfo->u4LldpLocPort);
        if (pPortInfo != NULL)
        {
            pPortInfo->TxDelayTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;
            pPortInfo->TtrTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;
            pPortInfo->ShutWhileTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;
            LLDP_RESET_ALL_COUNTERS (pPortInfo);
        }
    }

    LLDP_TRC (LLDP_RED_TRC, "LldpRedMakeNodeStandbyFromActive: Active to "
              "Standby transition is successful\r\n");
    /* bulk request to Active node will be triggered on getting 
     * L2_INITIATE_BULK_UPDATES event from RM */
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedSendSyncUpMsg 
 *
 *    DESCRIPTION      : This function contstructs and sends the dynamic sync 
 *                       up message to RM
 *
 *    INPUT            : u1MsgType - Message type
 *                       SyncUpMsg - pointer to sync up information
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
PUBLIC VOID
LldpRedSendSyncUpMsg (UINT1 u1MsgType, VOID *pSyncUpMsg)
{
    tRmMsg             *pRmMsg = NULL;
    tRmMsg             *pBasePtr = NULL;
    UINT2               u2MsgLen = 0;
    UINT4               u4Offset = 0;
    INT4                i4RetVal = OSIX_SUCCESS;

    if (LLDP_RED_NODE_STATUS () != RM_ACTIVE)
    {
        /* Only ACTIVE node can send sync up messages */
        LLDP_TRC (LLDP_RED_TRC,
                  "LldpRedSendSyncUpMsg: Sync up message can't be"
                  " sent from STANDBY node\r\n");
        return;
    }

    /* If no standby node is up then no need send sync up message,
     * just return */
    if (gLldpRedGlobalInfo.u1NumPeersUp == 0)
    {
        /* When there are no standby nodes, no need to send sync up message */
        LLDP_TRC (LLDP_RED_TRC,
                  "LldpRedSendSyncUpMsg: No need to send sync up message,"
                  " if STANDBY node is down\r\n");
        return;
    }
    /* Message can't be NULL */
    if (pSyncUpMsg == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_RED_TRC,
                  "LldpRedSendSyncUpMsg:Message pointer is NULL\r\n");
        return;
    }

    /* get message length */
    LldpRedGetMsgLen (u1MsgType, &u2MsgLen, pSyncUpMsg);

    /* allocate memory for the sync up message */
    if (LldpRedAllocMemForRmMsg (u1MsgType, u2MsgLen, &pRmMsg) != OSIX_SUCCESS)
    {
        return;
    }

    /*  store the initial value of RM message buffer pointer */
    pBasePtr = pRmMsg;

    /* reserve space for RM message header by
     * setting u4Offset to LLDP_RED_MSG_HDR_SIZE */
    u4Offset = LLDP_RED_MSG_HDR_SIZE;

    /* Form the message */
    switch (u1MsgType)
    {
        case LLDP_RED_OPER_STATUS_CHG_MSG:
            LldpRedFormOperChgSyncUpMsg (pRmMsg,
                                         (tLldpLocPortInfo *) pSyncUpMsg,
                                         &u4Offset);
            break;
        case LLDP_RED_TMR_START_MSG:
            i4RetVal = LldpRedFormTmrStartSyncUpMsg (pRmMsg,
                                                     (tLldpRedTmrSyncUpInfo *)
                                                     pSyncUpMsg, &u4Offset);
            break;
        case LLDP_RED_TMR_EXP_MSG:
            LldpRedFormTmrExpSyncUpMsg (pRmMsg,
                                        (tLldpRedTmrSyncUpInfo *) pSyncUpMsg,
                                        &u4Offset);
            break;
        case LLDP_RED_REM_TAB_INSERT_MSG:
        case LLDP_RED_REM_TAB_UPDATE_MSG:
        case LLDP_RED_REM_TAB_DELETE_MSG:
        case LLDP_RED_REM_TAB_AGEOUT_MSG:
            i4RetVal = LldpRedFormRemTabChgSyncUpMsg (pRmMsg, pSyncUpMsg,
                                                      u1MsgType, &u4Offset);
            break;
        default:
            LLDP_TRC (LLDP_RED_TRC,
                      "LldpRedSendSyncUpMsg:Invalid message type\r\n");
            break;
    }

    if (i4RetVal == OSIX_FAILURE)
    {
        LLDP_TRC (LLDP_RED_TRC,
                  "LldpRedSendSyncUpMsg: Dynamic sync up message "
                  "formation failed\r\n");
        /* release the memory allocated for dynamic sync up message */
        RM_FREE (pRmMsg);
        return;
    }
    /* update the message length with actual number of
     * of bytes added in the RM message buffer */
    u2MsgLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pRmMsg);
    /* Add message header(msg type and msg len) in 
     * starting location of RM message buffer */
    pRmMsg = pBasePtr;
    LldpRedPutMsgHdrInRmMsg (pRmMsg, u1MsgType, u2MsgLen);
    /* Send update message to RM */
    LldpRedSendMsgToRm (u1MsgType, u2MsgLen, pBasePtr);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedGetMsgLen 
 *
 *    DESCRIPTION      : This function returns the message length based on 
 *                       message type
 *
 *    INPUT            : u1MsgType - Message type
 *                       pu2MsgLen - pointer to message length
 *                       pSyncUpInfo - pointer to sync up info
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
LldpRedGetMsgLen (UINT1 u1MsgType, UINT2 *pu2MsgLen, VOID *pSyncUpInfo)
{
    tLldpRedTmrSyncUpInfo *pTmrSyncUpInfo = NULL;
    tLldpRedRemTabChgInfo *pRemTabChgInfo = NULL;
    tLldpLocPortInfo   *pPortInfo = NULL;

    switch (u1MsgType)
    {
        case LLDP_RED_OPER_STATUS_CHG_MSG:
            *pu2MsgLen = (UINT2) LLDP_RED_OPER_STATUS_CHG_MSG_LEN;
            break;

        case LLDP_RED_TMR_START_MSG:
            pTmrSyncUpInfo = (tLldpRedTmrSyncUpInfo *) pSyncUpInfo;
            if ((pTmrSyncUpInfo->u1TmrType == LLDP_TX_TMR_GLOBAL_SHUT_WHILE) ||
                (pTmrSyncUpInfo->u1TmrType == LLDP_NOTIF_INT_TIMER))
            {
                *pu2MsgLen = (UINT2) (LLDP_RED_MSG_HDR_SIZE +
                                      sizeof (pTmrSyncUpInfo->u4TmrInterval) +
                                      sizeof (pTmrSyncUpInfo->u1TmrType));
            }
            else if (pTmrSyncUpInfo->u1TmrType == LLDP_RX_TMR_RX_INFO_TTL)
            {
                *pu2MsgLen = (UINT2) (LLDP_RED_MSG_HDR_SIZE +
                                      sizeof (pTmrSyncUpInfo->u1TmrType) +
                                      sizeof (tLldpRedMsapRBIndex));
            }
            break;

        case LLDP_RED_TMR_EXP_MSG:
            *pu2MsgLen = (UINT2) LLDP_RED_TMR_EXP_MSG_LEN;
            break;

        case LLDP_RED_REM_TAB_INSERT_MSG:
        case LLDP_RED_REM_TAB_UPDATE_MSG:
            pRemTabChgInfo = (tLldpRedRemTabChgInfo *) pSyncUpInfo;
            pPortInfo = pRemTabChgInfo->pPortInfo;
            /* LLDP_RED_MSG_HDR_SIZE + sizeof (i4RemIndex) +
             * sizeof (Ifindex)+ sizeof (MAC) + LLDPDU */
            *pu2MsgLen = (UINT2) (LLDP_RED_MSG_HDR_SIZE +
                                  sizeof (pRemTabChgInfo->i4RemIndex) +
                                  sizeof (pRemTabChgInfo->pPortInfo->
                                          i4IfIndex) +
                                  sizeof (tMacAddr) + pPortInfo->u2RxPduLen);
            break;

        case LLDP_RED_REM_TAB_DELETE_MSG:
        case LLDP_RED_REM_TAB_AGEOUT_MSG:
            *pu2MsgLen = (UINT2) LLDP_RED_REM_TAB_DELETE_MSG_LEN;
            break;

        default:
            LLDP_TRC (LLDP_RED_TRC, "LldpRedGetMsgLen: Invalid message "
                      "type\r\n");
            break;
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedPutMsgHdrInRmMsg
 *
 *    DESCRIPTION      : This function adds RM msg header(message type and
 *                       message length) in the message
 *
 *    INPUT            : pRmMsgBuf - pointer to message buffer
 *                       u1MsgType - message type
 *                       u2MsgLen - message length
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
LldpRedPutMsgHdrInRmMsg (tRmMsg * pRmMsgBuf, UINT1 u1MsgType, UINT2 u2MsgLen)
{
    UINT4               u4Offset = 0;

    /* RM message header */
    /* --------------------
     * | MsgType | MsgLen |
     * --------------------*/

    /* MsgType */
    LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, u1MsgType);
    /* MsgLen */
    LLDP_RED_PUT_2_BYTE (pRmMsgBuf, u4Offset, u2MsgLen);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedFormRemTabChgSyncUpMsg 
 *
 *    DESCRIPTION      : This function forms the remote table change dynamic 
 *                       sync up message
 *
 *    INPUT            : pRmMsgBuf   - pointer to message buffer
 *                       pSyncUpInfo - pointer to remote table change sync up
 *                                     information
 *                       u1MsgType   - message type
 *                     : pu4Offset - pointer to number of bytes added in RM
 *                                   message buffer in calling function
 *
 *    OUTPUT           : pu4Offset - pointer to number of bytes added in RM
 *                                   message buffer in this function
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
LldpRedFormRemTabChgSyncUpMsg (tRmMsg * pRmMsgBuf, VOID *pSyncUpInfo,
                               UINT1 u1MsgType, UINT4 *pu4Offset)
{
    UINT4               u4Offset = 0;
    INT4                i4RetVal = OSIX_SUCCESS;

    /* get the actual offset */
    u4Offset = *pu4Offset;

    if ((u1MsgType == LLDP_RED_REM_TAB_INSERT_MSG) ||
        (u1MsgType == LLDP_RED_REM_TAB_UPDATE_MSG))
    {
        /* remote table insert/update sync up message format */
        /* ---------------------------------------------------------
         * | MsgHdr | Remote Index | IfIndex | MAC address | LLDPDU |
         * ---------------------------------------------------------*/
        i4RetVal = LldpRedPutLldpduInRmMsg (pRmMsgBuf,
                                            (tLldpRedRemTabChgInfo *)
                                            pSyncUpInfo, &u4Offset);
    }
    else if ((u1MsgType == LLDP_RED_REM_TAB_DELETE_MSG) ||
             (u1MsgType == LLDP_RED_REM_TAB_AGEOUT_MSG))
    {
        /* remote table delete/ageout sync up message format */
        /* ------------------------------
         * | MsgHdr | MSAP RBTree Index |
         * -----------------------------*/
        LldpRedPutMsapRBIndexInRmMsg (pRmMsgBuf,
                                      (tLldpRedMsapRBIndex *) pSyncUpInfo,
                                      &u4Offset);
    }

    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedPutLldpduInRmMsg 
 *
 *    DESCRIPTION      : This function puts the LLDPDU and port number 
 *                       on which the LLDPDU is receivd in RM message
 *
 *    INPUT            : pRmMsgBuf - pointer to RM message buffer
 *                       pRemTabChgpInfo - pointer to remote table 
 *                                              change information structure
 *                     : pu4Offset - pointer to number of bytes added in RM
 *                                   message buffer in this function
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
LldpRedPutLldpduInRmMsg (tRmMsg * pRmMsgBuf,
                         tLldpRedRemTabChgInfo * pRemTabChgInfo,
                         UINT4 *pu4Offset)
{
    UINT4               u4Offset = 0;
    INT4                i4RetVal = OSIX_SUCCESS;
    tLldpLocPortInfo   *pPortInfo = NULL;
    tMacAddr            MacAddr;

    u4Offset = *pu4Offset;

    /* Remote index */
    LLDP_RED_PUT_4_BYTE (pRmMsgBuf, u4Offset, pRemTabChgInfo->i4RemIndex);
    /* received port number and local port number are same, so add the
     * local port number in the RM message */
    pPortInfo = pRemTabChgInfo->pPortInfo;
    /* IfIndex and Mac Address will send in place of local port number */
    LLDP_RED_PUT_4_BYTE (pRmMsgBuf, u4Offset, (UINT4) pPortInfo->i4IfIndex);

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    LldpGetMacFromLldpLocalPort (pPortInfo->u4LocPortNum, &MacAddr);
    LLDP_RED_PUT_6_BYTE (pRmMsgBuf, u4Offset, &MacAddr);
    /* Copy the LLDPDU to the RM data buffer */
    if (CRU_BUF_Copy_OverBufChain (pRmMsgBuf,
                                   pPortInfo->pu1RxLldpdu, u4Offset,
                                   (UINT4) pPortInfo->u2RxPduLen) ==
        CRU_FAILURE)
    {
        LLDP_TRC (LLDP_RED_TRC, "LldpRedPutLldpduInRmMsg: Copying LLDPDU to "
                  "RM data buffer failed\r\n");
        i4RetVal = OSIX_FAILURE;
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedFormTmrExpSyncUpMsg 
 *
 *    DESCRIPTION      : This function forms the timer expiry sync up 
 *                       message
 *
 *    INPUT            : pRmMsgBuf - pointer to message buffer
 *                       TmrSyncUpInfo - pointer to timer sync up information
 *                     : pu4Offset - pointer to number of bytes added in RM
 *                                   message buffer in calling function
 *
 *    OUTPUT           : pu4Offset - pointer to number of bytes added in RM
 *                                   message buffer in this function
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedFormTmrExpSyncUpMsg (tRmMsg * pRmMsgBuf,
                            tLldpRedTmrSyncUpInfo * pTmrSyncUpInfo,
                            UINT4 *pu4Offset)
{
    UINT4               u4Offset = 0;

    /* timer expiry sync up data */
    /* -----------------------
     * | MsgHdr | Timer type |
     * -----------------------*/

    /* get the actual offset */
    u4Offset = *pu4Offset;
    /* Timer type */
    LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, pTmrSyncUpInfo->u1TmrType);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedFormTmrStartSyncUpMsg 
 *
 *    DESCRIPTION      : This function forms the timer start sync up 
 *                       message
 *
 *    INPUT            : pRmMsgBuf - pointer to message buffer
 *                       pTmrSyncUpInfo - pointer to timer sync up information
 *                     : pu4Offset - pointer to number of bytes added in RM
 *                                   message buffer in calling function
 *
 *    OUTPUT           : NONE 
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
LldpRedFormTmrStartSyncUpMsg (tRmMsg * pRmMsgBuf,
                              tLldpRedTmrSyncUpInfo * pTmrSyncUpInfo,
                              UINT4 *pu4Offset)
{
    UINT4               u4Offset = 0;

    /* timer start sync up data */
    /* -----------------------------------------------------------
     * | MsgHdr | Timer type | Time interval | MSAP RBTree Index |
     * -----------------------------------------------------------
     * Note: 
     * - Time Interval is valid only if timer type is NotifIntval/GlobShutWhile
     * - MSAP RBTree Index is valid only if timer type is RxInfoAge */

    /* get the actual offset */
    u4Offset = *pu4Offset;
    /* Timer type */
    LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, pTmrSyncUpInfo->u1TmrType);

    /* Put timer interval only if the timer type is NotifIntval/GlobShutWhile */
    if ((pTmrSyncUpInfo->u1TmrType == LLDP_NOTIF_INT_TIMER) ||
        (pTmrSyncUpInfo->u1TmrType == LLDP_TX_TMR_GLOBAL_SHUT_WHILE))
    {
        /* Time interval */
        LLDP_RED_PUT_4_BYTE (pRmMsgBuf, u4Offset,
                             pTmrSyncUpInfo->u4TmrInterval);
    }

    /* Put MSAP RBTree Index in Rm Msg only if timer type is rxinfo age timer */
    if (pTmrSyncUpInfo->u1TmrType == LLDP_RX_TMR_RX_INFO_TTL)
    {
        if (pTmrSyncUpInfo->pMsapRBIndex == NULL)
        {
            LLDP_TRC (LLDP_RED_TRC,
                      "LldpRedFormTmrStartSyncUpMsg: "
                      "Msap RBIndex is NULL\r\n");
            return OSIX_FAILURE;
        }

        /* put MSAP RBTree Index */
        LldpRedPutMsapRBIndexInRmMsg (pRmMsgBuf, pTmrSyncUpInfo->pMsapRBIndex,
                                      &u4Offset);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedFormOperChgSyncUpMsg 
 *
 *    DESCRIPTION      : This function forms the port oper change sync up 
 *                       message
 *
 *    INPUT            : pRmMsgBuf - pointer to message buffer
 *                       pPortInfo - pointer to port info structure
 *                     : pu4Offset - pointer to number of bytes added in RM
 *                                   message buffer in calling function
 *
 *    OUTPUT           : NONE 
 *
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
LldpRedFormOperChgSyncUpMsg (tRmMsg * pRmMsgBuf, tLldpLocPortInfo * pPortInfo,
                             UINT4 *pu4Offset)
{
    tMacAddr            MacAddr;
    UINT4               u4Offset = 0;

    /* port oper chg sync up data */
    /* -----------------------------------------------
     * | MsgHdr | IfIndex |Mac address|  OperStatus  |
     * ----------------------------------------------*/

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    if (LldpGetMacFromLldpLocalPort (pPortInfo->u4LocPortNum,
                                     &MacAddr) != OSIX_SUCCESS)
    {
        return;
    }
    /* get the actual offset */
    u4Offset = *pu4Offset;
    /* IfIndex */
    LLDP_RED_PUT_4_BYTE (pRmMsgBuf, u4Offset, pPortInfo->i4IfIndex);
    /*MAC address */
    LLDP_RED_PUT_6_BYTE (pRmMsgBuf, u4Offset, &MacAddr);
    /* OperStatus */
    LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, pPortInfo->u1OperStatus);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedPutMsapRBIndexInRmMsg 
 *
 *    DESCRIPTION      : This function puts MSAP RBTree Index in RM message
 *
 *    INPUT            : pRmMsgBuf - pointer to message buffer
 *                       pMsapRBIndex - pointer to MSAP RBTree Index
 *                     : pu4Offset - pointer to number of bytes added in RM
 *                                   message buffer in this function
 *    OUTPUT           : NONE                               
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
VOID
LldpRedPutMsapRBIndexInRmMsg (tRmMsg * pRmMsgBuf,
                              tLldpRedMsapRBIndex * pMsapRBIndex,
                              UINT4 *pu4Offset)
{
    tMacAddr            MacAddr;
    UINT4               u4Offset = 0;
    UINT2               u2ChassIdLen = 0;
    UINT2               u2PortIdLen = 0;

    /* MSAP RBTree Index */
    /* --------------------------------------------------------------------------------------
     * |Rcvd PortNum |MAC address |Chassis Id subtype |Chassis Id |Port Id Subtype |Port Id |
     * --------------------------------------------------------------------------------------*/

    /* get the actual offset */
    u4Offset = *pu4Offset;
    /* received port number (i4RemLocalPortNum) */
    LLDP_RED_PUT_4_BYTE (pRmMsgBuf, u4Offset,
                         (UINT4) pMsapRBIndex->i4RemLocalPortNum);

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    LldpGetMacAddrFromDestAddrTblIndex (pMsapRBIndex->u4DestAddrTblIndex,
                                        &MacAddr);
    LLDP_RED_PUT_6_BYTE (pRmMsgBuf, u4Offset, &MacAddr);

    /* chassis id subtype */
    LLDP_RED_PUT_4_BYTE (pRmMsgBuf, u4Offset,
                         (UINT4) pMsapRBIndex->i4RemChassisIdSubtype);

    /* chassis id length */
    if (LldpUtilGetChassisIdLen (pMsapRBIndex->i4RemChassisIdSubtype,
                                 &(pMsapRBIndex->au1RemChassisId[0]),
                                 &u2ChassIdLen) != OSIX_SUCCESS)
    {
        LLDP_TRC (LLDP_RED_TRC, "LldpRedPutMsapRBIndexInRmMsg: "
                  "LldpUtilGetChassisIdLen returns FAILURE.\r\n");
        return;
    }
    LLDP_RED_PUT_2_BYTE (pRmMsgBuf, u4Offset, u2ChassIdLen);
    /* chassis id */
    LLDP_RED_PUT_N_BYTE (pRmMsgBuf, &(pMsapRBIndex->au1RemChassisId[0]),
                         u4Offset, u2ChassIdLen);
    /* port id subtype */
    LLDP_RED_PUT_4_BYTE (pRmMsgBuf, u4Offset,
                         (UINT4) pMsapRBIndex->i4RemPortIdSubtype);
    /* port id len */
    if (LldpUtilGetPortIdLen (pMsapRBIndex->i4RemPortIdSubtype,
                              &(pMsapRBIndex->au1RemPortId[0]),
                              &u2PortIdLen) != OSIX_SUCCESS)
    {
        LLDP_TRC (LLDP_RED_TRC, "LldpRedPutMsapRBIndexInRmMsg: "
                  "LldpUtilGetPortIdLen returns FAILURE.\r\n");
        return;
    }
    LLDP_RED_PUT_2_BYTE (pRmMsgBuf, u4Offset, u2PortIdLen);
    /* port id */
    LLDP_RED_PUT_N_BYTE (pRmMsgBuf, &(pMsapRBIndex->au1RemPortId[0]), u4Offset,
                         u2PortIdLen);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedProcSyncUpMsg 
 *
 *    DESCRIPTION      : This function is invoked whenever LLDP module
 *                       receives a dynamic sync up/bulk update/bulk request
 *                       message.
 *
 *    INPUT            : pRmCtrlMsg - RM control message 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedProcSyncUpMsg (tLldpRmCtrlMsg * pRmCtrlMsg)
{
    tRmMsg             *pRmMsg = NULL;
    UINT4               u4Offset = 0;
    UINT1               u1MsgType = 0;
    UINT2               u2MsgLen = 0;
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    /* get RM message pointer */
    pRmMsg = pRmCtrlMsg->pData;
    /* MsgType */
    LLDP_RED_GET_1_BYTE (pRmMsg, u4Offset, u1MsgType);
    /* MsgLen */
    LLDP_RED_GET_2_BYTE (pRmMsg, u4Offset, u2MsgLen);

    /* validate message lentgh */
    if (u2MsgLen != pRmCtrlMsg->u2DataLen)
    {
        /* message type length present in message and
         * data length present in RMCtrl message doesn't match */
        LLDP_TRC (LLDP_RED_TRC, "LldpRedProcSyncUpMsg: Invalid message "
                  "length\r\n");
        return;
    }

    /* Active node can process only BULK_REQ. So, if the node status is
     * Active and message type is not BULK_REQ then return */
    if (LLDP_RED_NODE_STATUS () == RM_ACTIVE)
    {
        if (u1MsgType != LLDP_RED_BULK_UPDT_REQ_MSG &&
            u1MsgType != LLDP_HR_STDY_ST_PKT_REQ)
        {
            LLDP_TRC (LLDP_RED_TRC,
                      "LldpRedProcSyncUpMsg:Active node can't process sync up "
                      " messages\r\n");
            return;
        }
    }
    else if (LLDP_RED_NODE_STATUS () == RM_INIT)
    {
        LLDP_TRC (LLDP_RED_TRC,
                  "LldpRedProcSyncUpMsg:Received sync up "
                  "msg from RM when LLDP node is in INIT state\r\n");
        return;
    }

    /* Process the message */
    switch (u1MsgType)
    {
        case LLDP_RED_BULK_UPDATE_MSG:
            LldpRedProcBulkUpdMsg (pRmMsg, u2MsgLen);
            break;

        case LLDP_RED_BULK_UPDT_TAIL_MSG:
            LLDP_RED_TAIL_MSG_RCVD () = OSIX_TRUE;
            ProtoEvt.u4AppId = RM_LLDP_APP_ID;
            ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
            ProtoEvt.u4Error = RM_NONE;
            if (LldpPortSendEventToRm (&ProtoEvt) != OSIX_SUCCESS)
            {
                LLDP_TRC (LLDP_RED_TRC | LLDP_CRITICAL_TRC,
                          "LldpRedProcSyncUpMsg: Sending Bulk update "
                          "complete event" " to RM failed\r\n");
            }
            break;

        case LLDP_RED_OPER_STATUS_CHG_MSG:
            LldpRedProcOperChgSyncUp (pRmMsg, u2MsgLen);
            break;

        case LLDP_RED_TMR_START_MSG:
            LldpRedProcTmrStartSyncUp (pRmMsg, u2MsgLen);
            break;

        case LLDP_RED_TMR_EXP_MSG:
            LldpRedProcTmrExpSyncUp (pRmMsg, u2MsgLen);
            break;

        case LLDP_RED_REM_TAB_INSERT_MSG:
            LldpRedProcRemTabInsertSyncUp (pRmMsg, u2MsgLen);
            break;

        case LLDP_RED_REM_TAB_UPDATE_MSG:
            LldpRedProcRemTabUpdateSyncUp (pRmMsg, u2MsgLen);
            break;

        case LLDP_RED_REM_TAB_DELETE_MSG:
        case LLDP_RED_REM_TAB_AGEOUT_MSG:
            LldpRedProcRemTabDeleteSyncUp (pRmMsg, u1MsgType);
            break;

        case LLDP_RED_BULK_UPDT_REQ_MSG:
            LLDP_TRC (LLDP_RED_TRC, " Received bulk update request\r\n");
            if (LLDP_RED_IS_STANDBY_UP () == OSIX_FALSE)
            {
                /* This is a special case, where bulk request msg from
                 * standby is coming before RM_STANDBY_UP. So no need to
                 * process the bulk request now. Bulk updates will be send
                 * on RM_STANDBY_UP event.
                 */
                LLDP_BULK_UPD_REQ_RECD () = OSIX_TRUE;
                break;
            }
            LLDP_BULK_UPD_REQ_RECD () = OSIX_FALSE;
            /* On recieving LLDP_BULK_UPD_REQ_MSG, Bulk updation process 
             * should be started. */
            LldpRedInitBulkUpdateFlags ();
            LLDP_RED_BULK_UPD_STATUS () = LLDP_BULK_UPD_IN_PROGRESS;
            LldpRedProcBulkUpdReq ();
            break;

            /* HITLESS RESTART */
        case LLDP_HR_STDY_ST_PKT_REQ:
            LLDP_TRC (LLDP_RED_TRC, "Received Steady State Pkt Request msg\n");
            LldpRedHRProcStdyStPktReq ();
            break;

        default:
            LLDP_TRC (LLDP_RED_TRC,
                      "LldpRedProcSyncUpMsg:Invalid message type\r\n");
            break;
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedProcBulkUpdMsg
 *
 *    DESCRIPTION      : This function processes the bulk update message 
 *                       received from RM
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *                       u2MsgLen - bulk update message length
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedProcBulkUpdMsg (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    UINT1               u1BulkUpdType = 0;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT4               u4Offset = 0;

    if (u2MsgLen > LLDP_RED_MAX_BULK_UPD_SIZE)
    {
        LLDP_TRC_ARG1 (LLDP_RED_TRC,
                       "LldpRedProcBulkUpdMsg: Bulk update message"
                       " length: %d, is greater than"
                       " max bulk update size, hence ignored the bulk update"
                       " message\r\n", u2MsgLen);
        return;
    }

    u4Offset = LLDP_RED_MSG_HDR_SIZE;
    LLDP_RED_GET_1_BYTE (pRmMsg, u4Offset, u1BulkUpdType);
    /* move to next memory location where bulk update information is
     * present */
    LLDP_CRU_BUF_MOVE_VALID_OFFSET (pRmMsg, u4Offset);
    LLDP_TRC (LLDP_RED_TRC, "Processing the bulk update message\n");

    switch (u1BulkUpdType)
    {
        case LLDP_RED_GLOBINFO_BULK_UPD_MSG:
            LldpRedProcGlobBulkUpd (pRmMsg);
            break;
        case LLDP_RED_PORTINFO_BULK_UPD_MSG:
            i4RetVal = LldpRedProcPortInfoBulkUpd (pRmMsg);
            break;
        case LLDP_RED_MANADDR_BULK_UPD_MSG:
            i4RetVal = LldpRedProcManAddrBulkUpd (pRmMsg);
            break;
        case LLDP_RED_PROTOVLAN_BULK_UPD_MSG:
            i4RetVal = LldpRedProcProtoVlanBulkUpd (pRmMsg, u2MsgLen);
            break;
        case LLDP_RED_VLANNAME_BULK_UPD_MSG:
            i4RetVal = LldpRedProcVlanNameBulkUpd (pRmMsg);
            break;
        case LLDP_RED_UNKNOWN_TLV_BULK_UPD_MSG:
            i4RetVal = LldpRedProcUnknownTlvBulkUpd (pRmMsg);
            break;
        case LLDP_RED_ORGDEF_INFO_BULK_UPD_MSG:
            i4RetVal = LldpRedProcOrgDefInfoBulkUpd (pRmMsg);
            break;
        case LLDP_RED_REM_NODE_BULK_UPD_MSG:
            i4RetVal = LldpRedProcRemNodeBulkUpd (pRmMsg);
            break;
        case LLDP_RED_MED_NWPOL_BULK_UPD_MSG:
            i4RetVal = LldpRedProcMedNwPolRemNodeBulkUpd (pRmMsg);
            break;
        case LLDP_RED_MED_LOCATION_BULK_UPD_MSG:
            i4RetVal = LldpRedProcMedLocationRemNodeBulkUpd (pRmMsg);
            break;
        default:
            LLDP_TRC_ARG1 (LLDP_RED_TRC,
                           "LldpRedProcBulkUpdMsg: Invalid bulk update "
                           "type:%d\r\n", u1BulkUpdType);
            break;
    }
    if (i4RetVal != OSIX_SUCCESS)
    {
        LLDP_TRC_ARG1 (LLDP_RED_TRC | LLDP_CRITICAL_TRC,
                       "LldpRedProcBulkUpdMsg: Bulk update"
                       " processing(bulk update"
                       " type:%d) failed\r\n", u1BulkUpdType);
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedProcPortInfoBulkUpd
 *
 *    DESCRIPTION      : This function processes the port specific information
 *                       bulk update message received from RM
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
LldpRedProcPortInfoBulkUpd (tRmMsg * pRmMsg)
{
    tLldpLocPortInfo   *pPortInfo = NULL;
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;
    tMacAddr            MacAddr;
    UINT4               u4IfIndex = 0;
    UINT4               u4Offset = 0;
    UINT4               u4AgeoutsTotal = 0;
    UINT2               u2PortCount = 0;
    UINT1               u1OperStatus = 0;

    LLDP_TRC (LLDP_RED_TRC, "Received port info bulk update message\n");
    LLDP_RED_GET_2_BYTE (pRmMsg, u4Offset, u2PortCount);

    while (u2PortCount > 0)
    {
        LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, u4IfIndex);
        LLDP_RED_GET_6_BYTE (pRmMsg, u4Offset, &MacAddr);

        pLldpAgentPortInfo = LldpGetPortMapTableNode (u4IfIndex, &MacAddr);

        if (pLldpAgentPortInfo == NULL)
        {
            LLDP_TRC_ARG1 (LLDP_RED_TRC | LLDP_CRITICAL_TRC,
                           "LldpRedProcPortInfoBulkUpd: Agent to Local port Mapping not Present"
                           " for ifindex  %d", u4IfIndex);
            return OSIX_FAILURE;
        }
        LLDP_RED_GET_1_BYTE (pRmMsg, u4Offset, u1OperStatus);

        pPortInfo = LLDP_GET_LOC_PORT_INFO (pLldpAgentPortInfo->u4LldpLocPort);
        if (pPortInfo == NULL)
        {
            LLDP_TRC_ARG1 (LLDP_RED_TRC | LLDP_CRITICAL_TRC,
                           "LldpRedProcPortInfoBulkUpd: Port entry not created"
                           " for port %d", pLldpAgentPortInfo->u4LldpLocPort);
            return OSIX_FAILURE;
        }

        if (LldpIfOperChg (pPortInfo, u1OperStatus) != OSIX_SUCCESS)
        {
            LLDP_TRC_ARG1 (LLDP_RED_TRC, " Processing oper status change"
                           " failed for port:%d\r\n",
                           pLldpAgentPortInfo->u4LldpLocPort);
            return OSIX_FAILURE;
        }

        LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, u4AgeoutsTotal);
        pPortInfo->StatsRxPortTable.u4AgeoutsTotal = u4AgeoutsTotal;
        u2PortCount--;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedProcManAddrBulkUpd
 *
 *    DESCRIPTION      : This function processes the remote management address
 *                       information bulk update message received from RM. 
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *
 *    OUTPUT           : NONE 
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
LldpRedProcManAddrBulkUpd (tRmMsg * pRmMsg)
{
    tLldpRemManAddressTable *pManAddrNode = NULL;
    UINT4               u4Offset = 0;
    UINT1               u1ManAddCnt = 0;

    LLDP_TRC (LLDP_RED_TRC, "Received management address bulk update"
              " message\n");
    LLDP_RED_GET_1_BYTE (pRmMsg, u4Offset, u1ManAddCnt);

    while (u1ManAddCnt > 0)
    {
        /* allocate memory for new management address node */
        if ((pManAddrNode = (tLldpRemManAddressTable *)
             (MemAllocMemBlk (gLldpGlobalInfo.RemManAddrPoolId))) == NULL)
        {
            LLDP_TRC (LLDP_RED_TRC | LLDP_CRITICAL_TRC,
                      "LldpRedProcManAddrBulkUpd: Mempool Allocation Failure"
                      " for management addr node\r\n");
#ifndef FM_WANTED
            SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                          "LldpRedProcManAddrBulkUpd: Mempool Allocation Failure"
                          "for management addr node"));
#endif
            return OSIX_FAILURE;
        }
        MEMSET (pManAddrNode, 0, sizeof (tLldpRemManAddressTable));
        LldpRedGetManAddrInfo (pManAddrNode, pRmMsg, &u4Offset);

        if (RBTreeAdd (gLldpGlobalInfo.RemManAddrRBTree,
                       (tRBElem *) pManAddrNode) != RB_SUCCESS)
        {
            MemReleaseMemBlock (gLldpGlobalInfo.RemManAddrPoolId,
                                (UINT1 *) pManAddrNode);
            LLDP_TRC (LLDP_RED_TRC, "LldpRedProcManAddrBulkUpd:"
                      "RBTree Addition failed for RemManAddrRBTree."
                      "Entry with same index already present in the "
                      "RBTree\r\n");
            pManAddrNode = NULL;
        }
        u1ManAddCnt--;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedProcProtoVlanBulkUpd
 *
 *    DESCRIPTION      : This function processes the remote protocol vlan
 *                       information bulk update message received from RM. 
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *                       u2MsgLen - Total length of the message
 *
 *    OUTPUT           : NONE 
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
LldpRedProcProtoVlanBulkUpd (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    tLldpxdot1RemProtoVlanInfo *pProtoVlanNode = NULL;
    UINT4               u4Offset = 0;
    UINT4               u4ProtoVlanMsgLen = 0;

    LLDP_TRC (LLDP_RED_TRC, "Received protocol vlan bulk update message\n");

    u4ProtoVlanMsgLen = (UINT4) (u2MsgLen - LLDP_RED_BULK_UPD_MSG_HDR_SIZE);

    while (u4ProtoVlanMsgLen >= LLDP_RED_PROTO_VLAN_MSG_LEN)
    {
        if ((pProtoVlanNode = (tLldpxdot1RemProtoVlanInfo *)
             (MemAllocMemBlk (gLldpGlobalInfo.RemProtoVlanPoolId))) == NULL)
        {
            LLDP_TRC (LLDP_RED_TRC | LLDP_CRITICAL_TRC,
                      "LldpRedProcProtoVlanBulkUpd: Mempool Allocation Failure "
                      "for proto vlan node\r\n");
#ifndef FM_WANTED
            SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                          "LldpRedProcProtoVlanBulkUpd: Mempool Allocation"
                          "Failure for proto vlan node"));
#endif
            return OSIX_FAILURE;
        }
        MEMSET (pProtoVlanNode, 0, sizeof (tLldpxdot1RemProtoVlanInfo));
        LldpRedGetProtoVlanInfo (pProtoVlanNode, pRmMsg, &u4Offset);
        if (RBTreeAdd (gLldpGlobalInfo.RemProtoVlanRBTree,
                       (tRBElem *) pProtoVlanNode) != RB_SUCCESS)
        {
            MemReleaseMemBlock (gLldpGlobalInfo.RemProtoVlanPoolId,
                                (UINT1 *) pProtoVlanNode);
            LLDP_TRC (LLDP_RED_TRC, "LldpRedProcProtoVlanBulkUpd: "
                      "RBTree Addition failed for RemProtoVlanRBTree."
                      " Entry with same index already present in the "
                      " RBTree\r\n");
            pProtoVlanNode = NULL;
        }

        u4ProtoVlanMsgLen = u4ProtoVlanMsgLen - LLDP_RED_PROTO_VLAN_MSG_LEN;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedProcVlanNameBulkUpd
 *
 *    DESCRIPTION      : This function processes the remote vlan name
 *                       information bulk update message received from RM. 
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *
 *    OUTPUT           : NONE 
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
LldpRedProcVlanNameBulkUpd (tRmMsg * pRmMsg)
{
    tLldpxdot1RemVlanNameInfo *pVlanNameNode = NULL;
    UINT4               u4Offset = 0;
    UINT1               u1VlanCount = 0;

    LLDP_TRC (LLDP_RED_TRC, "Received vlan name bulk update message\n");

    LLDP_RED_GET_1_BYTE (pRmMsg, u4Offset, u1VlanCount);
    while (u1VlanCount > 0)
    {
        if ((pVlanNameNode = (tLldpxdot1RemVlanNameInfo *)
             (MemAllocMemBlk (gLldpGlobalInfo.RemVlanNameInfoPoolId))) == NULL)
        {
            LLDP_TRC (LLDP_RED_TRC | LLDP_CRITICAL_TRC,
                      "LldpRedProcVlanNameBulkUpd: Mempool Allocation Failure"
                      " for vlan name node\r\n");
#ifndef FM_WANTED
            SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                          "LldpRedProcVlanNameBulkUpd: Mempool Allocation Failure"
                          "for vlan name node"));
#endif

            return OSIX_FAILURE;
        }
        MEMSET (pVlanNameNode, 0, sizeof (tLldpxdot1RemVlanNameInfo));

        LldpRedGetVlanNameInfo (pVlanNameNode, pRmMsg, &u4Offset);

        if (RBTreeAdd (gLldpGlobalInfo.RemVlanNameInfoRBTree,
                       (tRBElem *) pVlanNameNode) != RB_SUCCESS)
        {
            MemReleaseMemBlock (gLldpGlobalInfo.RemVlanNameInfoPoolId,
                                (UINT1 *) pVlanNameNode);
            LLDP_TRC (LLDP_RED_TRC,
                      " RBTree Addition failed for RemVlanNameInfoRBTree."
                      " Entry with same index already present in the"
                      " RBTree\r\n");
            pVlanNameNode = NULL;
        }
        u1VlanCount--;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedProcUnknownTlvBulkUpd
 *
 *    DESCRIPTION      : This function processes the remote unknown tlv
 *                       information bulk update message received from RM. 
 *
 *    INPUT            : pRmMsg   - pointer to RM message buffer
 *
 *    OUTPUT           : NONE 
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
LldpRedProcUnknownTlvBulkUpd (tRmMsg * pRmMsg)
{
    tLldpRemUnknownTLVTable *pUnknownTlvNode = NULL;
    UINT4               u4Offset = 0;
    UINT1               u1TlvCount = 0;

    LLDP_TRC (LLDP_RED_TRC, "Received unknown tlv bulk update message\n");
    LLDP_RED_GET_1_BYTE (pRmMsg, u4Offset, u1TlvCount);

    while (u1TlvCount > 0)
    {
        if ((pUnknownTlvNode = (tLldpRemUnknownTLVTable *)
             (MemAllocMemBlk (gLldpGlobalInfo.RemUnknownTLVPoolId))) == NULL)
        {
            LLDP_TRC (LLDP_RED_TRC | LLDP_CRITICAL_TRC,
                      "LldpRedProcUnknownTlvBulkUpd: Mempool Allocation Failure"
                      " for unknown tlv node\r\n");
#ifndef FM_WANTED
            SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                          "LldpRedProcUnknownTlvBulkUpd: Mempool Allocation"
                          "Failure for unknown tlv node"));
#endif
            return OSIX_FAILURE;
        }
        MEMSET (pUnknownTlvNode, 0, sizeof (tLldpRemUnknownTLVTable));

        LldpRedGetUnknownTlvInfo (pUnknownTlvNode, pRmMsg, &u4Offset);
        /* Insert the unknown tlv in the RemUnknownTLVRBTree */
        if (RBTreeAdd (gLldpGlobalInfo.RemUnknownTLVRBTree,
                       (tRBElem *) pUnknownTlvNode) != RB_SUCCESS)
        {
            MemReleaseMemBlock (gLldpGlobalInfo.RemUnknownTLVPoolId,
                                (UINT1 *) pUnknownTlvNode);
            LLDP_TRC (LLDP_RED_TRC, "LldpRedProcUnknownTlvBulkUpd: "
                      "RBTree Addition failed for RemUnknownTLVRBTree. "
                      "Entry with same index already present in the "
                      "RBTree\r\n");
            pUnknownTlvNode = NULL;
        }
        u1TlvCount--;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedProcOrgDefInfoBulkUpd
 *
 *    DESCRIPTION      : This function processes the remote organizationally
 *                       defined information bulk update message received 
 *                       from RM. 
 *
 *    INPUT            : pRmMsg   - pointer to RM message buffer
 *
 *    OUTPUT           : NONE 
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
LldpRedProcOrgDefInfoBulkUpd (tRmMsg * pRmMsg)
{
    tLldpRemOrgDefInfoTable *pRemOrgDefInfoNode = NULL;
    UINT4               u4Offset = 0;
    UINT1               u1TlvCount = 0;

    LLDP_TRC (LLDP_RED_TRC, "Received org defined tlv bulk update message");
    LLDP_RED_GET_1_BYTE (pRmMsg, u4Offset, u1TlvCount);

    while (u1TlvCount > 0)
    {
        if ((pRemOrgDefInfoNode = (tLldpRemOrgDefInfoTable *)
             (MemAllocMemBlk (gLldpGlobalInfo.RemOrgDefInfoPoolId))) == NULL)
        {
            LLDP_TRC (LLDP_RED_TRC | LLDP_CRITICAL_TRC,
                      "LldpRedProcOrgDefInfoBulkUpd: Mempool Allocation Failure"
                      " for org.def info node\r\n");
#ifndef FM_WANTED
            SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                          "LldpRedProcOrgDefInfoBulkUpd: Mempool Allocation"
                          "Failure for org.def info node"));
#endif
            return OSIX_FAILURE;
        }
        MEMSET (pRemOrgDefInfoNode, 0, sizeof (tLldpRemOrgDefInfoTable));

        LldpRedGetOrgDefInfo (pRemOrgDefInfoNode, pRmMsg, &u4Offset);
        if (RBTreeAdd (gLldpGlobalInfo.RemOrgDefInfoRBTree,
                       (tRBElem *) pRemOrgDefInfoNode) != RB_SUCCESS)
        {
            MemReleaseMemBlock (gLldpGlobalInfo.RemOrgDefInfoPoolId,
                                (UINT1 *) pRemOrgDefInfoNode);
            LLDP_TRC (LLDP_RED_TRC, "LldpRedProcOrgDefInfoBulkUpd: "
                      "RBTree Addition failed for RemOrgDefInfoRBTree. "
                      "Entry with same index already present in the "
                      "RBTree\r\n");
            pRemOrgDefInfoNode = NULL;
        }
        u1TlvCount--;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedGetDot3TlvInfo
 *
 *    DESCRIPTION      : This function retreives the dot3 tlv information
 *                       present in RM message buffer and stores it in the
 *                       given remote node pointer.
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *
 *    OUTPUT           : pRemoteNode - pointer to remote node information
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
LldpRedGetDot3TlvInfo (tLldpRemoteNode * pRemoteNode, tRmMsg * pRmMsg,
                       UINT4 *pu4Offset)
{
    tLldpxdot3RemPortInfo *pDot3RemPortInfo = NULL;
    tLldpxdot3AutoNegInfo *pAutoNegInfo = NULL;
    UINT1               u1Dot3TlvsFlag = 0;
    UINT1               u1AutoNegFlag = 0;

    /* ************** Dot3 org spec info ******************* */
    /* Mac Phy TLV */
    /* Power Via MDI TLV - Not supported */
    /* Link Aggregartion TLV */
    /* Max Frame Size TLV */
    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1Dot3TlvsFlag);

    if (u1Dot3TlvsFlag == 0)
    {
        /* No Dot3 information is present */
        return OSIX_SUCCESS;
    }

    /* Dot3Tlv Information is present in the RM message. So allocate
     * memory to store it.*/
    if ((pRemoteNode->pDot3RemPortInfo = (tLldpxdot3RemPortInfo *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemDot3PortInfoPoolId))) == NULL)
    {
        LLDP_TRC (LLDP_RED_TRC | LLDP_CRITICAL_TRC, "LldpRedGetDot3TlvInfo:"
                  " Mempool Allocation Failure\r\n");
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpRedGetDot3TlvInfo: Mempool Allocation Failure"));
#endif
        return OSIX_FAILURE;
    }
    MEMSET (pRemoteNode->pDot3RemPortInfo, 0, sizeof (tLldpxdot3RemPortInfo));

    pDot3RemPortInfo = pRemoteNode->pDot3RemPortInfo;

    if (u1Dot3TlvsFlag & LLDP_RED_MAC_PHY_TLV_BMAP)
    {
        pRemoteNode->bRcvdMacPhyTlv = OSIX_TRUE;
        pAutoNegInfo = &pDot3RemPortInfo->RemDot3AutoNegInfo;
        LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1AutoNegFlag);
        if (u1AutoNegFlag & LLDP_AUTONEG_SUPPORTED)
        {
            pAutoNegInfo->u1AutoNegSupport = LLDP_TRUE;
        }
        else
        {
            pAutoNegInfo->u1AutoNegSupport = LLDP_FALSE;
        }

        if (u1AutoNegFlag & LLDP_AUTONEG_ENABLED)
        {
            pAutoNegInfo->u1AutoNegEnabled = LLDP_TRUE;
        }
        else
        {
            pAutoNegInfo->u1AutoNegEnabled = LLDP_FALSE;
        }

        LLDP_RED_GET_2_BYTE (pRmMsg, *pu4Offset,
                             pAutoNegInfo->u2AutoNegAdvtCap);
        LLDP_RED_GET_2_BYTE (pRmMsg, *pu4Offset, pAutoNegInfo->u2OperMauType);
    }

    if (u1Dot3TlvsFlag & LLDP_RED_LINK_AGG_TLV_BMAP)
    {
        pRemoteNode->bRcvdLinkAggTlv = OSIX_TRUE;
        LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, pDot3RemPortInfo->u1AggStatus);
        LLDP_RED_GET_4_BYTE (pRmMsg, *pu4Offset, pDot3RemPortInfo->u4AggPortId);
    }

    if (u1Dot3TlvsFlag & LLDP_RED_MAX_FRAME_SIZE_TLV_BMAP)
    {
        pRemoteNode->bRcvdMaxFrameTlv = OSIX_TRUE;
        LLDP_RED_GET_2_BYTE (pRmMsg, *pu4Offset,
                             pDot3RemPortInfo->u2MaxFrameSize);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedGetBasicOptTlvInfo
 *
 *    DESCRIPTION      : This function retreives the basic tlv information
 *                       present in RM message buffer and stores it in the
 *                       given remote node pointer
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *
 *    OUTPUT           : pRemoteNode - pointer to remote node information
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedGetBasicOptTlvInfo (tLldpRemoteNode * pRemoteNode, tRmMsg * pRmMsg,
                           UINT4 *pu4Offset)
{
    UINT1               u1PortDescLen = 0;
    UINT1               u1SysNameLen = 0;
    UINT1               u1SysDescLen = 0;
    UINT1               u1SysCapabLen = 0;

    /* Port Description TLV */
    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1PortDescLen);
    if (u1PortDescLen != 0)
    {
        LLDP_RED_GET_N_BYTE (pRmMsg, &(pRemoteNode->au1RemPortDesc[0]),
                             *pu4Offset, (UINT4) u1PortDescLen);
    }

    /* System Name TLV */
    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1SysNameLen);
    if (u1SysNameLen != 0)
    {
        LLDP_RED_GET_N_BYTE (pRmMsg, &(pRemoteNode->au1RemSysName[0]),
                             *pu4Offset, (UINT4) u1SysNameLen);
    }

    /* System Description TLV */
    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1SysDescLen);
    if (u1SysDescLen != 0)
    {
        LLDP_RED_GET_N_BYTE (pRmMsg, &(pRemoteNode->au1RemSysDesc[0]),
                             *pu4Offset, (UINT4) u1SysDescLen);
    }

    /* System Capabilities TLV */
    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1SysCapabLen);

    if (u1SysCapabLen != 0)
    {
        LLDP_RED_GET_N_BYTE (pRmMsg, &(pRemoteNode->au1RemSysCapSupported[0]),
                             *pu4Offset, ISS_SYS_CAPABILITIES_LEN);
        LLDP_RED_GET_N_BYTE (pRmMsg, &(pRemoteNode->au1RemSysCapEnabled[0]),
                             *pu4Offset, ISS_SYS_CAPABILITIES_LEN);
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedProcRemNodeBulkUpd
 *
 *    DESCRIPTION      : This function processes the remote node information
 *                       present in the RM message buffer.
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *
 *    OUTPUT           : NONE 
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
LldpRedProcRemNodeBulkUpd (tRmMsg * pRmMsg)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT4               u4LldpLocPort = 0;
    tLldpLocPortInfo   *pPortInfo = NULL;

    LLDP_TRC (LLDP_RED_TRC, "LldpRedProcRemNodeBulkUpd: Received "
              "Remote Node Bulk Update\n");

    /* Create Memory for Remote Node */
    if ((pRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_RED_TRC,
                  "LldpRedProcRemNodeBulkUpd: Failed to Allocate Memory "
                  "for remote node \r\n");
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpRedProcRemNodeBulkUpd: Failed to Allocate Memory"
                      "for remote node"));
#endif
        return OSIX_FAILURE;
    }
    MEMSET (pRemoteNode, 0, sizeof (tLldpRemoteNode));

    LldpRedGetRemNodeInfo (pRemoteNode, pRmMsg);
    /* Check for the local port info */

    u4LldpLocPort =
        LldpGetLocalPortFromIfIndexDestMacIfIndex ((UINT4) pRemoteNode->
                                                   i4RemLocalPortNum,
                                                   pRemoteNode->
                                                   u4DestAddrTblIndex);
    if (u4LldpLocPort == 0)
    {
        LLDP_TRC_ARG1 (LLDP_RED_TRC,
                       "LldpRedProcRemNodeBulkUpd: Agent to Local port mapping is "
                       "not present for port: %d\r\n",
                       pRemoteNode->i4RemLocalPortNum);
        /* Release the memory allocated for Remote Node */
        MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                            (UINT1 *) pRemoteNode);
        return OSIX_SUCCESS;
    }

    pPortInfo = LLDP_GET_LOC_PORT_INFO (u4LldpLocPort);
    if (pPortInfo == NULL)
    {
        LLDP_TRC_ARG1 (LLDP_RED_TRC,
                       "LldpRedProcRemNodeBulkUpd: Local PortInfo is not present for "
                       "port : %d \r\n", pRemoteNode->i4RemLocalPortNum);
        /* Release the memory allocated for Remote Node */
        MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                            (UINT1 *) pRemoteNode);
        return OSIX_SUCCESS;
    }

    LldpRxUpdateRemoteLink (pRemoteNode);

    /* Set rxinfo ageout timer status as not running in standby node */
    pRemoteNode->RxInfoAgeTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;

    if (LldpRxUtlAddRemoteNode (pRemoteNode) == OSIX_FAILURE)
    {
        /* Release the memory allocated for Remote Node */
        MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                            (UINT1 *) pRemoteNode);
        pRemoteNode = NULL;
        LLDP_TRC (LLDP_RED_TRC,
                  "LldpRedProcRemNodeBulkUpd: Failed to add remote node to "
                  "RBTree\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedGetRemNodeInfo
 *
 *    DESCRIPTION      : This function retreives the remote node information
 *                       present in RM message buffer and stores it in the
 *                       given remote node pointer
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *
 *    OUTPUT           : pRemoteNode - pointer to remote node information
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
INT4
LldpRedGetRemNodeInfo (tLldpRemoteNode * pRemoteNode, tRmMsg * pRmMsg)
{
    UINT4               u4Offset = 0;
    UINT4               u4RemLocalPortNum = 0;
    UINT4               u4RemIndex = 0;
    UINT4               u4DestAddrTblIndex = 0;
    UINT4               u4RemVidUsageDigest = 0;
    UINT4               u4RxInfoAgeRemTime = 0;
    UINT2               u2RemMgmtVid = 0;
    UINT1               u1ChassIdLen = 0;
    UINT1               u1PortIdLen = 0;
    UINT1               u1RemChassisIdSubtype = 0;
    UINT1               u1RemPortIdSubtype = 0;
    UINT1               u1PduMsgDigestLen = 0;
    tMacAddr            MacAddr;

    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, pRemoteNode->u4RemLastUpdateTime);

    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, u4RemLocalPortNum);
    pRemoteNode->i4RemLocalPortNum = (INT4) u4RemLocalPortNum;

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    LLDP_RED_GET_6_BYTE (pRmMsg, u4Offset, &MacAddr);
    LldpGetDestAddrTblIndexFromMacAddr (&MacAddr, &u4DestAddrTblIndex);
    pRemoteNode->u4DestAddrTblIndex = u4DestAddrTblIndex;

    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, u4RemIndex);
    pRemoteNode->i4RemIndex = (INT4) u4RemIndex;

    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, u4RemVidUsageDigest);
    pRemoteNode->u4RemVidUsageDigest = u4RemVidUsageDigest;

    LLDP_RED_GET_2_BYTE (pRmMsg, u4Offset, u2RemMgmtVid);
    pRemoteNode->u2RemMgmtVid = u2RemMgmtVid;

    LLDP_RED_GET_1_BYTE (pRmMsg, u4Offset, u1RemChassisIdSubtype);
    pRemoteNode->i4RemChassisIdSubtype = (INT4) u1RemChassisIdSubtype;

    LLDP_RED_GET_1_BYTE (pRmMsg, u4Offset, u1ChassIdLen);

    LLDP_RED_GET_N_BYTE (pRmMsg, &(pRemoteNode->au1RemChassisId[0]),
                         u4Offset, (UINT4) u1ChassIdLen);

    LLDP_RED_GET_1_BYTE (pRmMsg, u4Offset, u1RemPortIdSubtype);
    pRemoteNode->i4RemPortIdSubtype = (INT4) u1RemPortIdSubtype;

    LLDP_RED_GET_1_BYTE (pRmMsg, u4Offset, u1PortIdLen);

    LLDP_RED_GET_N_BYTE (pRmMsg, &(pRemoteNode->au1RemPortId[0]), u4Offset,
                         (UINT4) u1PortIdLen);

    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, pRemoteNode->u4RxTTL);

    LldpRedGetBasicOptTlvInfo (pRemoteNode, pRmMsg, &u4Offset);

    LLDP_RED_GET_1_BYTE (pRmMsg, u4Offset, pRemoteNode->bRcvdPVidTlv);

    if (pRemoteNode->bRcvdPVidTlv == OSIX_TRUE)
    {
        LLDP_RED_GET_2_BYTE (pRmMsg, u4Offset, pRemoteNode->u2PVId);
    }

    if (LldpRedGetDot3TlvInfo (pRemoteNode, pRmMsg, &u4Offset) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, u4RxInfoAgeRemTime);

    if (u4RxInfoAgeRemTime != 0)
    {
        /* update rxinfo age timer expected expiry time */
        pRemoteNode->u4RxInfoAgeTmrExpTime = (UINT4) (u4RxInfoAgeRemTime +
                                                      UtlGetTimeSinceEpoch ());
    }

    LldpRedGetMedTlvInfoBulkUpdMsg (pRmMsg, &u4Offset, pRemoteNode);

    LldpRedGetInvTlvInfoBulkUpMsg (pRmMsg, &u4Offset, pRemoteNode);

    LldpRedGetPowerTlvInfoBulkUpdMsg (pRmMsg, &u4Offset, pRemoteNode);

    /* PduMsgDigest */
    LLDP_RED_GET_1_BYTE (pRmMsg, u4Offset, u1PduMsgDigestLen);

    if (u1PduMsgDigestLen != 0)
    {
        LLDP_RED_GET_N_BYTE (pRmMsg, &(pRemoteNode->au1PduMsgDigest[0]),
                             u4Offset, (UINT4) u1PduMsgDigestLen);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedGetMsapRBIndexFromRmMsg 
 *
 *    DESCRIPTION      : This function gets MSAP RBTree Index from RM message
 *
 *    INPUT            : pMsapRBIndex - pointer to MSAP RBTree Index
 *                       pRmMsgBuf - pointer to message buffer
 *
 *    OUTPUT           : pu4Offset - pointer to number of bytes added in RM
 *                                   message buffer in this function
 *
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
LldpRedGetMsapRBIndexFromRmMsg (tLldpRedMsapRBIndex * pMsapRBIndex,
                                tRmMsg * pRmMsg, UINT4 *pu4Offset)
{
    tMacAddr            MacAddr;
    UINT4               u4Offset = 0;
    UINT2               u2ChassIdLen = 0;
    UINT4               u4DestAddrTblIndex = 0;
    UINT2               u2PortIdLen = 0;

    u4Offset = *pu4Offset;
    /* received port number (i4RemLocalPortNum) */
    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, pMsapRBIndex->i4RemLocalPortNum);

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    LLDP_RED_GET_6_BYTE (pRmMsg, u4Offset, &MacAddr);
    LldpGetDestAddrTblIndexFromMacAddr (&MacAddr, &u4DestAddrTblIndex);
    pMsapRBIndex->u4DestAddrTblIndex = u4DestAddrTblIndex;
    /* chassis id subtype */
    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, pMsapRBIndex->i4RemChassisIdSubtype);
    /* chassis id len */
    LLDP_RED_GET_2_BYTE (pRmMsg, u4Offset, u2ChassIdLen)
        /* chassis id */
        LLDP_RED_GET_N_BYTE (pRmMsg, &(pMsapRBIndex->au1RemChassisId[0]),
                             u4Offset, u2ChassIdLen);

    /* port id subtype */
    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, pMsapRBIndex->i4RemPortIdSubtype);
    /* port id len */
    LLDP_RED_GET_2_BYTE (pRmMsg, u4Offset, u2PortIdLen);
    /* port id */
    LLDP_RED_GET_N_BYTE (pRmMsg, &(pMsapRBIndex->au1RemPortId[0]), u4Offset,
                         u2PortIdLen);

    /* update offset */
    *pu4Offset = u4Offset;
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedProcTmrStartSyncUp 
 *
 *    DESCRIPTION      : This function processes the timer start sync up 
 *                       message
 *
 *    INPUT            : pRmMsg   - Sync up message 
 *                       u2MsgLen - Message length
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedProcTmrStartSyncUp (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    UINT4               u4Offset = 0;
    UINT1               u1TmrType = 0;
    UINT4               u4TmrInterval = 0;
    UINT4               u4ExpectExpTime = 0;

    UNUSED_PARAM (u2MsgLen);

    /* Message header is already read from RM message buffer.
     * So set the set the offset with LLDP_RED_MSG_HDR_SIZE */
    u4Offset = LLDP_RED_MSG_HDR_SIZE;
    /* Get timer type */
    LLDP_RED_GET_1_BYTE (pRmMsg, u4Offset, u1TmrType);
    /* if timer type is NotifIntval/GlobShutwhile get the time interval
     * from RM message */
    if ((u1TmrType == LLDP_TX_TMR_GLOBAL_SHUT_WHILE) ||
        (u1TmrType == LLDP_NOTIF_INT_TIMER))
    {
        /* Get timer interval */
        LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, u4TmrInterval);
        /* calculate expected exp time */
        u4ExpectExpTime = (UtlGetTimeSinceEpoch () + u4TmrInterval);
    }

    /* update expected expiry time based on the timer type */
    switch (u1TmrType)
    {
        case LLDP_TX_TMR_GLOBAL_SHUT_WHILE:
            gLldpRedGlobalInfo.u4GlobShutWhileExpTime = u4ExpectExpTime;
            gLldpRedGlobalInfo.u4GlobShutWhileRcvdTime = u4TmrInterval;
            gLldpRedGlobalInfo.bGlobShutWhileTmrSyncUpRvcd = OSIX_TRUE;
            break;
        case LLDP_NOTIF_INT_TIMER:
            gLldpRedGlobalInfo.u4NotifIntTmrExpTime = u4ExpectExpTime;
            gLldpRedGlobalInfo.u4NotifIntTmrRcvdTime = u4TmrInterval;
            break;
        case LLDP_RX_TMR_RX_INFO_TTL:
            LldpRedUpdRxInfoAgeExpTime (pRmMsg, &u4Offset);
            break;
        default:
            LLDP_TRC (LLDP_RED_TRC,
                      "LldpRedProcTmrStartSyncUp: Invalid timer " "type\r\n");
            break;
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedUpdRxInfoAgeExpTime 
 *
 *    DESCRIPTION      : This function stores rx info age out timer's 
 *                       expected expiry time
 *
 *    INPUT            : pRmMsg - Sync up message 
 *                     : pu4Offset - pointer to number of bytes added in RM
 *                                   message buffer in this function
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
LldpRedUpdRxInfoAgeExpTime (tRmMsg * pRmMsg, UINT4 *pu4Offset)
{
    UINT4               u4Offset = 0;
    UINT4               u4ExpectExpTime = 0;
    UINT4               u4LldpLocPort = 0;
    tLldpRemoteNode    *pRemoteNode = NULL;
    tLldpRedMsapRBIndex MsapRBIndex;
    tLldpLocPortInfo   *pPortInfo = NULL;

    MEMSET (&MsapRBIndex, 0, sizeof (tLldpRedMsapRBIndex));

    u4Offset = *pu4Offset;

    /* Get MSAP RBTree Index */
    LldpRedGetMsapRBIndexFromRmMsg (&MsapRBIndex, pRmMsg, &u4Offset);

    u4LldpLocPort =
        LldpGetLocalPortFromIfIndexDestMacIfIndex ((UINT4) MsapRBIndex.
                                                   i4RemLocalPortNum,
                                                   MsapRBIndex.
                                                   u4DestAddrTblIndex);
    if (u4LldpLocPort == 0)
    {
        LLDP_TRC_ARG1 (LLDP_RED_TRC,
                       "LldpRedUpdRxInfoAgeExpTime: Local PortInfo is not present for "
                       "port : %d \r\n", MsapRBIndex.i4RemLocalPortNum);
        return OSIX_SUCCESS;
    }

    pPortInfo = LLDP_GET_LOC_PORT_INFO (u4LldpLocPort);
    /* Rxinfo age out timer start sync is received(i.e, Refresh frame sync up 
     * is recieved), so increment frames received counter */
    if (pPortInfo == NULL)
    {
        LLDP_TRC_ARG1 (LLDP_RED_TRC,
                       "LldpRedUpdRxInfoAgeExpTime: Local PortInfo is not present for "
                       "port : %d \r\n", MsapRBIndex.i4RemLocalPortNum);
        return OSIX_SUCCESS;
    }
    else
    {
        /* Update Received Frame Statistics */
        /* If the received packet contains MED TLVs, Increment LLDP-MED receive
         * counter else increment LLDP PDU received counter */
        if (pPortInfo->bMedCapable == LLDP_MED_TRUE)
        {
            LLDP_MED_INCR_CNTR_RX_FRAMES (pPortInfo);
        }
        else
        {
            LLDP_INCR_CNTR_RX_FRAMES (pPortInfo);
        }
    }

    /* Find the Remote Node by MSAP RBTree Index in RemMSAPRBTree */
    pRemoteNode = LldpRxUtlGetRemoteNodeByMSAP (u4LldpLocPort,
                                                MsapRBIndex.
                                                i4RemChassisIdSubtype,
                                                &(MsapRBIndex.
                                                  au1RemChassisId[0]),
                                                MsapRBIndex.i4RemPortIdSubtype,
                                                &(MsapRBIndex.au1RemPortId[0]));

    if (pRemoteNode == NULL)
    {
        /* RxInfoAgeOutTimer start sync up received for a remote entry which 
         * doesn't present in the remote table. ie.remote table insert/update 
         * of this remote entry might have been missed */
        LLDP_TRC_ARG6 (LLDP_CRITICAL_TRC | LLDP_RED_TRC,
                       "RxInfoAgeOutTimer start sync up "
                       "message received for a remote entry which doesn't "
                       "present in the remote table. "
                       "And whose MSAP RBTree Index:%d:%u:%d:%s:%d:%s\r\n",
                       MsapRBIndex.i4RemLocalPortNum,
                       MsapRBIndex.u4DestAddrTblIndex,
                       MsapRBIndex.i4RemChassisIdSubtype,
                       &(MsapRBIndex.au1RemChassisId[0]),
                       MsapRBIndex.i4RemPortIdSubtype,
                       &(MsapRBIndex.au1RemPortId[0]));
        return OSIX_FAILURE;
    }

    u4ExpectExpTime = (UtlGetTimeSinceEpoch () + pRemoteNode->u4RxTTL);
    pRemoteNode->u4RxInfoAgeTmrExpTime = u4ExpectExpTime;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedProcTmrExpSyncUp 
 *
 *    DESCRIPTION      : This function processes the timer expiry sync up 
 *                       message
 *
 *    INPUT            : pRmMsg - Sync up message 
 *                       u2MsgLen - Message length
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedProcTmrExpSyncUp (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    UINT1               u1TmrType = 0;
    UINT1               u1TimerId = 0;
    UINT4               u4Offset = 0;

    if (u2MsgLen != LLDP_RED_TMR_EXP_MSG_LEN)
    {
        LLDP_TRC (LLDP_RED_TRC,
                  "LldpRedProcTmrExpSyncUp: Message with invalid "
                  "length\r\n");
        return;
    }

    /* Message header is already read from RM message buffer.
     * So set the set the offset with LLDP_RED_MSG_HDR_SIZE */
    u4Offset = LLDP_RED_MSG_HDR_SIZE;
    /* Get timer type */
    LLDP_RED_GET_1_BYTE (pRmMsg, u4Offset, u1TmrType);

    if (u1TmrType != LLDP_TX_TMR_GLOBAL_SHUT_WHILE)
    {
        LLDP_TRC (LLDP_RED_TRC,
                  "LldpRedProcTmrExpSyncUp:Timer expiry sync up message "
                  "received with invalid timer type \r\n");
        return;
    }

    /* Global shutdown while timer is started in Active node whenever
     * module is disabled or system control status is shutdown. In both
     * the cases running timers are stopped in Active node, and global
     * shutdown while timer is started. So, In Standby node clear the timer
     * sync up information whenever global shutdown while timer expiry event
     * is received */
    gLldpRedGlobalInfo.u4GlobShutWhileExpTime = 0;
    gLldpRedGlobalInfo.u4GlobShutWhileRcvdTime = 0;
    gLldpRedGlobalInfo.u4NotifIntTmrExpTime = 0;
    gLldpRedGlobalInfo.u4NotifIntTmrRcvdTime = 0;
    gLldpRedGlobalInfo.bGlobShutWhileTmrSyncUpRvcd = OSIX_FALSE;
    gLldpRedGlobalInfo.bGlobShutWhileRcvdTimeProcessed = OSIX_TRUE;

    /* Global shut while timer has expired, so reset the expected expiry time
     * stored in timer sync up database and call the expiry call back 
     * function(LldpTmrGlobalShutWhileTmrExp) */
    u1TimerId = (UINT1) LLDP_TX_TMR_GLOBAL_SHUT_WHILE;
    (*(gLldpGlobalInfo.aTmrDesc[u1TimerId].TmrExpFn)) (NULL);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedGetManAddrInfo
 *
 *    DESCRIPTION      : This function gets/retreives the remote management 
 *                       address information present in the RM message buffer
 *                       and stores it in the given remote management address
 *                       information node pointer.
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *                       pu4Offset - pointer to the next location from where 
 *                                   the information needs to be read
 *
 *    OUTPUT           : pManAddrNode - pointer to remote management address
 *                                      information
 *                       pu4Offset
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedGetManAddrInfo (tLldpRemManAddressTable * pManAddrNode, tRmMsg * pRmMsg,
                       UINT4 *pu4Offset)
{
    UINT4               u4RemLocalPortNum = 0;
    UINT4               u4DestAddrTblIndex = 0;
    UINT4               u4RemIndex = 0;
    UINT4               u4ManAddrIfId = 0;
    UINT1               u1ManAddSubType = 0;
    UINT1               u1ManAddrLen = 0;
    UINT1               u1ManAddrIfSubtype = 0;
    UINT1               u1ManAddrOIDLen = 0;
    UINT1               au1ManAddrOid[LLDP_MAX_LEN_MAN_OID + 1];
    tMacAddr            MacAddr;

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    MEMSET (au1ManAddrOid, 0, LLDP_MAX_LEN_MAN_OID + 1);

    LLDP_RED_GET_4_BYTE (pRmMsg, *pu4Offset, pManAddrNode->u4RemLastUpdateTime);
    LLDP_RED_GET_4_BYTE (pRmMsg, *pu4Offset, u4RemLocalPortNum);
    pManAddrNode->i4RemLocalPortNum = (INT4) u4RemLocalPortNum;
    LLDP_RED_GET_6_BYTE (pRmMsg, *pu4Offset, &MacAddr);

    LldpGetDestAddrTblIndexFromMacAddr (&MacAddr, &u4DestAddrTblIndex);
    pManAddrNode->u4DestAddrTblIndex = u4DestAddrTblIndex;

    LLDP_RED_GET_4_BYTE (pRmMsg, *pu4Offset, u4RemIndex);
    pManAddrNode->i4RemIndex = (INT4) u4RemIndex;
    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1ManAddSubType);
    pManAddrNode->i4RemManAddrSubtype = (INT4) u1ManAddSubType;
    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1ManAddrLen);
    LLDP_RED_GET_N_BYTE (pRmMsg, &(pManAddrNode->au1RemManAddr[0]),
                         *pu4Offset, u1ManAddrLen);
    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1ManAddrIfSubtype);
    pManAddrNode->i4RemManAddrIfSubtype = (INT4) u1ManAddrIfSubtype;
    LLDP_RED_GET_4_BYTE (pRmMsg, *pu4Offset, u4ManAddrIfId);
    pManAddrNode->i4RemManAddrIfId = (INT4) u4ManAddrIfId;
    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1ManAddrOIDLen);
    au1ManAddrOid[0] = u1ManAddrOIDLen;
    if (u1ManAddrOIDLen != 0)
    {
        LLDP_RED_GET_N_BYTE (pRmMsg, &au1ManAddrOid[1], *pu4Offset,
                             u1ManAddrOIDLen);
        /* Decode the OID in UINT4 Oid format and store in the remote
         * database */
        if (LldpUtilDecodeManAddrOid (&au1ManAddrOid[0],
                                      (&pManAddrNode->RemManAddrOID))
            != OSIX_SUCCESS)
        {
            LLDP_TRC (LLDP_RED_TRC, "LldpRedGetManAddrInfo: "
                      "Failed to decode the OID.\r\n");
        }
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedGetProtoVlanInfo
 *
 *    DESCRIPTION      : This function gets/retreives the remote protocol
 *                       vlan information present in the RM message buffer
 *                       and stores it in the given remote protocol vlan
 *                       information node pointer.
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *                       pu4Offset - pointer to the location from where the 
 *                                   information has to be read
 *
 *    OUTPUT           : pProtoVlanNode - pointer to remote protocol vlan
 *                                        information
 *                       pu4Offset   
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedGetProtoVlanInfo (tLldpxdot1RemProtoVlanInfo * pProtoVlanNode,
                         tRmMsg * pRmMsg, UINT4 *pu4Offset)
{
    UINT4               u4RemLocalPortNum = 0;
    UINT4               u4RemIndex = 0;
    UINT4               u4DestAddrTblIndex = 0;
    UINT1               u1ProtoVlanFlag = 0;
    tMacAddr            MacAddr;

    LLDP_RED_GET_4_BYTE (pRmMsg, *pu4Offset,
                         pProtoVlanNode->u4RemLastUpdateTime);
    LLDP_RED_GET_4_BYTE (pRmMsg, *pu4Offset, u4RemLocalPortNum);
    pProtoVlanNode->i4RemLocalPortNum = (INT4) u4RemLocalPortNum;

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    LLDP_RED_GET_6_BYTE (pRmMsg, *pu4Offset, &MacAddr);

    LldpGetDestAddrTblIndexFromMacAddr (&MacAddr, &u4DestAddrTblIndex);
    pProtoVlanNode->u4DestAddrTblIndex = u4DestAddrTblIndex;

    LLDP_RED_GET_4_BYTE (pRmMsg, *pu4Offset, u4RemIndex);
    pProtoVlanNode->i4RemIndex = (INT4) u4RemIndex;

    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1ProtoVlanFlag);

    if (u1ProtoVlanFlag & LLDP_PROTO_VLAN_SUPP_BMAP)
    {
        pProtoVlanNode->u1ProtoVlanSupported = LLDP_TRUE;
    }
    else
    {
        pProtoVlanNode->u1ProtoVlanSupported = LLDP_FALSE;
    }

    if (u1ProtoVlanFlag & LLDP_PROTO_VLAN_STAT_BMAP)
    {
        pProtoVlanNode->u1ProtoVlanEnabled = LLDP_TRUE;
    }
    else
    {
        pProtoVlanNode->u1ProtoVlanEnabled = LLDP_FALSE;
    }
    LLDP_RED_GET_2_BYTE (pRmMsg, *pu4Offset, pProtoVlanNode->u2ProtoVlanId);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedGetVlanNameInfo
 *
 *    DESCRIPTION      : This function gets/retreives the remote vlan name
 *                       information present in the RM message buffer
 *                       and stores it in the given remote vlan name
 *                       information node pointer.
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *                       pu4Offset - pointer to the next location from 
 *                                   where the information needs to be read
 *
 *    OUTPUT           : pVlanNameNode - pointer to remote vlan name
 *                                      information
 *                       pu4Offset   
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedGetVlanNameInfo (tLldpxdot1RemVlanNameInfo * pVlanNameNode,
                        tRmMsg * pRmMsgBuf, UINT4 *pu4Offset)
{
    UINT4               u4RemLocalPortNum = 0;
    UINT4               u4RemIndex = 0;
    UINT4               u4DestAddrTblIndex = 0;
    tMacAddr            MacAddr;

    LLDP_RED_GET_4_BYTE (pRmMsgBuf, *pu4Offset,
                         pVlanNameNode->u4RemLastUpdateTime);
    LLDP_RED_GET_4_BYTE (pRmMsgBuf, *pu4Offset, u4RemLocalPortNum);
    pVlanNameNode->i4RemLocalPortNum = (INT4) u4RemLocalPortNum;

    LLDP_RED_GET_6_BYTE (pRmMsgBuf, *pu4Offset, &MacAddr);

    LldpGetDestAddrTblIndexFromMacAddr (&MacAddr, &u4DestAddrTblIndex);
    pVlanNameNode->u4DestAddrTblIndex = u4DestAddrTblIndex;

    LLDP_RED_GET_4_BYTE (pRmMsgBuf, *pu4Offset, u4RemIndex);
    pVlanNameNode->i4RemIndex = (INT4) u4RemIndex;

    LLDP_RED_GET_2_BYTE (pRmMsgBuf, *pu4Offset, pVlanNameNode->u2VlanId);
    LLDP_RED_GET_1_BYTE (pRmMsgBuf, *pu4Offset, pVlanNameNode->u1VlanNameLen);
    LLDP_RED_GET_N_BYTE (pRmMsgBuf, &(pVlanNameNode->au1VlanName[0]),
                         *pu4Offset, pVlanNameNode->u1VlanNameLen);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedGetUnknownTlvInfo
 *
 *    DESCRIPTION      : This function gets/retreives the remote unknown tlv
 *                       information present in the RM message buffer
 *                       and stores it in the given remote unknown tlv
 *                       information node pointer.
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *                       pu4Offset - pointer to the next location from where
 *                                   the information needs to be read
 *
 *    OUTPUT           : pUnknownTlvNode - pointer to remote unknown tlv
 *                                         information
 *                       pu4Offset
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedGetUnknownTlvInfo (tLldpRemUnknownTLVTable * pUnknownTlvNode,
                          tRmMsg * pRmMsgBuf, UINT4 *pu4Offset)
{
    UINT4               u4RemLocalPortNum = 0;
    UINT4               u4RemIndex = 0;
    UINT4               u4DestAddrTblIndex = 0;
    UINT1               u1RemUnknownTLVType = 0;
    tMacAddr            MacAddr;

    LLDP_RED_GET_4_BYTE (pRmMsgBuf, *pu4Offset,
                         pUnknownTlvNode->u4RemLastUpdateTime);
    LLDP_RED_GET_4_BYTE (pRmMsgBuf, *pu4Offset, u4RemLocalPortNum);
    pUnknownTlvNode->i4RemLocalPortNum = (INT4) u4RemLocalPortNum;

    LLDP_RED_GET_6_BYTE (pRmMsgBuf, *pu4Offset, &MacAddr);

    LldpGetDestAddrTblIndexFromMacAddr (&MacAddr, &u4DestAddrTblIndex);
    pUnknownTlvNode->u4DestAddrTblIndex = u4DestAddrTblIndex;

    LLDP_RED_GET_4_BYTE (pRmMsgBuf, *pu4Offset, u4RemIndex);
    pUnknownTlvNode->i4RemIndex = (INT4) u4RemIndex;

    LLDP_RED_GET_1_BYTE (pRmMsgBuf, *pu4Offset, u1RemUnknownTLVType);
    pUnknownTlvNode->i4RemUnknownTLVType = (INT4) u1RemUnknownTLVType;
    LLDP_RED_GET_2_BYTE (pRmMsgBuf, *pu4Offset,
                         pUnknownTlvNode->u2RemUnknownTLVInfoLen);
    LLDP_RED_GET_N_BYTE (pRmMsgBuf,
                         &(pUnknownTlvNode->au1RemUnknownTLVInfo[0]),
                         *pu4Offset, pUnknownTlvNode->u2RemUnknownTLVInfoLen);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedGetOrgDefInfo
 *
 *    DESCRIPTION      : This function gets/retreives the remote 
 *                       organizatioanlly defined information present in the 
 *                       RM message buffer and stores it in the given remote 
 *                       organizatioanlly defined information node pointer.
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *                       pu4Offset - pointer to the next location from where
 *                                   the information needs to be read
 *
 *    OUTPUT           : pOrgDefInfoNode - pointer to remote organizationally
 *                                         defined information
 *                       pu4Offset 
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedGetOrgDefInfo (tLldpRemOrgDefInfoTable * pOrgDefInfoNode,
                      tRmMsg * pRmMsgBuf, UINT4 *pu4Offset)
{
    tMacAddr            MacAddr;
    UINT4               u4RemLocalPortNum = 0;
    UINT4               u4RemIndex = 0;
    UINT4               u4DestAddrTblIndex = 0;
    UINT4               u4RemOrgDefInfoIndex = 0;
    UINT1               u1RemOrgDefInfoSubtype = 0;

    LLDP_RED_GET_4_BYTE (pRmMsgBuf, *pu4Offset,
                         pOrgDefInfoNode->u4RemLastUpdateTime);
    LLDP_RED_GET_4_BYTE (pRmMsgBuf, *pu4Offset, u4RemLocalPortNum);
    pOrgDefInfoNode->i4RemLocalPortNum = (INT4) u4RemLocalPortNum;

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    LLDP_RED_GET_6_BYTE (pRmMsgBuf, *pu4Offset, &MacAddr);

    LldpGetDestAddrTblIndexFromMacAddr (&MacAddr, &u4DestAddrTblIndex);
    pOrgDefInfoNode->u4DestAddrTblIndex = u4DestAddrTblIndex;

    LLDP_RED_GET_4_BYTE (pRmMsgBuf, *pu4Offset, u4RemIndex);
    pOrgDefInfoNode->i4RemIndex = (INT4) u4RemIndex;

    LLDP_RED_GET_N_BYTE (pRmMsgBuf,
                         &(pOrgDefInfoNode->au1RemOrgDefInfoOUI[0]),
                         *pu4Offset, LLDP_MAX_LEN_OUI);

    LLDP_RED_GET_1_BYTE (pRmMsgBuf, *pu4Offset, u1RemOrgDefInfoSubtype);
    pOrgDefInfoNode->i4RemOrgDefInfoSubtype = (INT4) u1RemOrgDefInfoSubtype;
    LLDP_RED_GET_4_BYTE (pRmMsgBuf, *pu4Offset, u4RemOrgDefInfoIndex);
    pOrgDefInfoNode->i4RemOrgDefInfoIndex = (INT4) u4RemOrgDefInfoIndex;
    LLDP_RED_GET_2_BYTE (pRmMsgBuf, *pu4Offset,
                         pOrgDefInfoNode->u2RemOrgDefInfoLen);
    LLDP_RED_GET_N_BYTE (pRmMsgBuf,
                         &(pOrgDefInfoNode->au1RemOrgDefInfo[0]),
                         *pu4Offset, pOrgDefInfoNode->u2RemOrgDefInfoLen);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedProcRemTabInsertSyncUp 
 *
 *    DESCRIPTION      : This function processes the remote table insert 
 *                       dynamic sync up message
 *
 *    INPUT            : pRmMsg   - Sync up message 
 *                       u2MsgLen - dynamic sync up message length
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedProcRemTabInsertSyncUp (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4Offset = 0;
    UINT4               u4RemIndex = 0;
    tLldpLocPortInfo   *pPortInfo = NULL;
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;
    tMacAddr            MacAddr;

    /* remote table insert/update sync up message format */
    /* --------------------------------------------------------
     * | MsgHdr | Remote Index | ifIndex |Mac Address | LLDPDU |
     * --------------------------------------------------------*/
    MEMSET (gau1LldPdu, 0, LLDP_MAX_PDU_SIZE);

    /* Message header is already read from RM message buffer.
     * So set the set the offset with LLDP_RED_MSG_HDR_SIZE */
    u4Offset = LLDP_RED_MSG_HDR_SIZE;

    /* get the remote index */
    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, u4RemIndex);

    /* get ifIndex number */
    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, u4IfIndex);
    /* get Mac address */
    LLDP_RED_GET_6_BYTE (pRmMsg, u4Offset, &MacAddr);

    pLldpAgentPortInfo = LldpGetPortMapTableNode (u4IfIndex, &MacAddr);

    if (pLldpAgentPortInfo == NULL)
    {
        return;
    }

    pPortInfo = LLDP_GET_LOC_PORT_INFO (pLldpAgentPortInfo->u4LldpLocPort);
    if (pPortInfo == NULL)
    {
        LLDP_TRC_ARG1 (LLDP_RED_TRC,
                       "LldpRedProcRemTabInsertSyncUp: Getting local port "
                       "information failed for port:%d\r\n",
                       pLldpAgentPortInfo->u4LldpLocPort);
        return;
    }
    /* get LLDPDU Length */
    pPortInfo->u2RxPduLen = (UINT2) (u2MsgLen -
                                     LLDP_RED_MSG_HDR_SIZE -
                                     sizeof (pLldpAgentPortInfo->
                                             i4IfIndex) -
                                     sizeof (tMacAddr) - sizeof (u4RemIndex));
    /* assign memory for pu1RxLldpdu */
    pPortInfo->pu1RxLldpdu = gau1LldPdu;
    /* get LLDPDU from RM message */
    LLDP_RED_GET_N_BYTE (pRmMsg, pPortInfo->pu1RxLldpdu, u4Offset,
                         pPortInfo->u2RxPduLen);

    /* update frame type */
    pPortInfo->u1RxFrameType = LLDP_RX_FRAME_NEW;
    LldpRedAddRemNode (pPortInfo, (INT4) u4RemIndex);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedAddRemNode 
 *
 *    DESCRIPTION      : This function stores tlvs information present in the
 *                       received LLDPDU in the remote node and 
 *                       adds the remote node to the remote table.
 *
 *    INPUT            : pPortInfo - pointer to local port info structure 
 *                       i4RemIndex - Remote index
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedAddRemNode (tLldpLocPortInfo * pPortInfo, INT4 i4RemIndex)
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    UINT1               au1PduMsgDigest[16];

    MEMSET (au1PduMsgDigest, 0, 16);

    /* increment frames received counter */
    LLDP_INCR_CNTR_RX_FRAMES (pPortInfo);

    /* Allocate memory for new remote node */
    if ((pRemoteNode = (tLldpRemoteNode *)
         (MemAllocMemBlk (gLldpGlobalInfo.RemNodePoolId))) == NULL)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_RED_TRC,
                  "LldpRedAddRemNode: Failed to Allocate Memory for "
                  "new Remote Node\r\n");
#ifndef FM_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                      "LldpRedAddRemNode: Failed to Allocate Memory for"
                      "new Remote Node"));
#endif
        return;
    }
    MEMSET (pRemoteNode, 0, sizeof (tLldpRemoteNode));
    /* Update the Remote system data RBTree Indices */
    pRemoteNode->u4RemLastUpdateTime = LLDP_SYS_UP_TIME ();
    pRemoteNode->i4RemLocalPortNum = (INT4) pPortInfo->i4IfIndex;
    pRemoteNode->u4DestAddrTblIndex = pPortInfo->u4DstMacAddrTblIndex;
    pRemoteNode->i4RemIndex = i4RemIndex;
    /* Calculate Message Digest of the received LLDPDU */
    LldpPortCalculatePduMsgDigest (pPortInfo->pu1RxLldpdu,
                                   pPortInfo->u2RxPduLen, &au1PduMsgDigest[0]);
    /* Store the new message digest */
    MEMCPY (&pRemoteNode->au1PduMsgDigest[0], &au1PduMsgDigest[0], 16);
    /* reset the TLVs Received flags, these flags are update while
     * updating remote database */
    pRemoteNode->bRcvdPVidTlv = OSIX_FALSE;
    pRemoteNode->bRcvdMacPhyTlv = OSIX_FALSE;
    pRemoteNode->bRcvdLinkAggTlv = OSIX_FALSE;
    pRemoteNode->bRcvdMaxFrameTlv = OSIX_FALSE;
    /* Store the RemoteNode pointer for future reference */
    pPortInfo->pBackPtrRemoteNode = pRemoteNode;
    /* update the remote database with the sync up information received */
    LldpRedUpdateRemTab (pPortInfo);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedUpdateRemTab
 *
 *    DESCRIPTION      : This function stores tlvs information present in the
 *                       received LLDPDU in the remote node. 
 *
 *    INPUT            : pPortInfo - pointer to local port info structure 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedUpdateRemTab (tLldpLocPortInfo * pPortInfo)
{
    tLldpRemoteNode    *pRemoteNode = NULL;

    pRemoteNode = pPortInfo->pBackPtrRemoteNode;
    /* Update the Remote System Information Tables */
    if (LldpRxUpdateRemDataBase (pPortInfo) == OSIX_SUCCESS)
    {
        /* update rxinfo age timer expected expiry time */
        pRemoteNode->u4RxInfoAgeTmrExpTime = (pRemoteNode->u4RxTTL +
                                              UtlGetTimeSinceEpoch ());

        if (LldpRxUtlAddRemoteNode (pRemoteNode) == OSIX_FAILURE)
        {
            /* Release the memory allocated for Remote Node */
            MemReleaseMemBlock (gLldpGlobalInfo.RemNodePoolId,
                                (UINT1 *) pRemoteNode);
            pRemoteNode = NULL;
            LLDP_TRC (LLDP_RED_TRC,
                      "LldpRedUpdateRemTab: Failed to add remote node to "
                      "RBTree\r\n");
            return;
        }
        LLDP_REMOTE_STAT_LAST_CHNG_TIME = LLDP_SYS_UP_TIME ();

        if (pPortInfo->u1RxFrameType == LLDP_RX_FRAME_NEW)
        {
            /* Update the New free remote index */
            LLDP_UPDT_NEXT_FREE_REM_INDEX ();
            /* Update counters */
            LLDP_REMOTE_STAT_TABLES_INSERTS++;
        }
        else if (pPortInfo->u1RxFrameType == LLDP_RX_FRAME_UPDATE)
        {
            /* Update counters */
            LLDP_REMOTE_STAT_TABLES_UPDATES++;
        }
        /* Set the flag bSendRemTblNotif, remote table change notification will
         * be sent only if this flag is true */
        if ((pPortInfo->PortConfigTable.u1NotificationEnable == LLDP_TRUE) &&
            ((pPortInfo->PortConfigTable.u1FsConfigNotificationType ==
              LLDP_REMOTE_CHG_NOTIFICATION) ||
             (pPortInfo->PortConfigTable.u1FsConfigNotificationType ==
              LLDP_REMOTE_CHG_MIS_CFG_NOTIFICATION)))
        {
            gLldpGlobalInfo.bSendRemTblNotif = OSIX_TRUE;
        }
    }

    else                        /* Updation Process Failed */
    {
        LLDP_TRC_ARG1 (LLDP_RED_TRC,
                       "LldpRedUpdateRemTab: Failed to update the remote "
                       "database for port:%d\r\n", pPortInfo->u4LocPortNum);
        /* Delete the Remote Node */
        LldpRxUtlDelRemoteNode (pRemoteNode);
        LLDP_REMOTE_STAT_TABLES_DROPS++;
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedProcRemTabUpdateSyncUp 
 *
 *    DESCRIPTION      : This function processes the remote table update 
 *                       sync up message
 *
 *    INPUT            : pRmMsg - Sync up message 
 *                       u2MsgLen - Message length
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedProcRemTabUpdateSyncUp (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    UINT4               u4RemIndex = 0;
    UINT4               u4Offset = 0;
    UINT4               u4RemLocalPortNum = 0;
    UINT4               u4DestAddrTblIndex = 0;
    tLldpLocPortInfo   *pPortInfo = NULL;
    tLldpRemoteNode    *pRemoteNode = NULL;
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;
    tLldpRedMsapRBIndex MsapRBIndex;
    tMacAddr            MacAddr;

    /* remote table insert/update sync up message format */
    /* -------------------------------------------------
     * | MsgHdr | Remote Index | Rcvd PortNum | LLDPDU |
     * ------------------------------------------------*/
    MEMSET (&MsapRBIndex, 0, sizeof (tLldpRedMsapRBIndex));
    MEMSET (gau1LldPdu, 0, LLDP_MAX_PDU_SIZE);

    /* Message header is already read from RM message buffer.
     * So set the set the offset with LLDP_RED_MSG_HDR_SIZE */
    u4Offset = LLDP_RED_MSG_HDR_SIZE;
    /* get the remote index */
    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, u4RemIndex);
    /* get received/local port number */
    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, u4RemLocalPortNum);

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    LLDP_RED_GET_6_BYTE (pRmMsg, u4Offset, &MacAddr);
    if (LldpGetDestAddrTblIndexFromMacAddr (&MacAddr, &u4DestAddrTblIndex)
        != OSIX_SUCCESS)
    {
        return;
    }
    pLldpAgentPortInfo = LldpGetPortMapTableNode (u4RemLocalPortNum, &MacAddr);
    if (pLldpAgentPortInfo == NULL)
    {
        return;
    }

    pPortInfo = LLDP_GET_LOC_PORT_INFO (pLldpAgentPortInfo->u4LldpLocPort);
    if (pPortInfo == NULL)
    {
        LLDP_TRC_ARG1 (LLDP_RED_TRC,
                       "LldpRedProcRemTabUpdateSyncUp: Getting local port "
                       "information failed for port:%d\r\n", u4RemLocalPortNum);
        return;
    }
    /* get LLDPDU Length */
    pPortInfo->u2RxPduLen = (UINT2) (u2MsgLen -
                                     LLDP_RED_MSG_HDR_SIZE -
                                     sizeof (u4RemLocalPortNum) -
                                     sizeof (tMacAddr) - sizeof (u4RemIndex));
    /* assign memory for pu1RxLldpdu */
    pPortInfo->pu1RxLldpdu = gau1LldPdu;
    /* get LLDPDU from RM message */
    LLDP_RED_GET_N_BYTE (pRmMsg, pPortInfo->pu1RxLldpdu, u4Offset,
                         pPortInfo->u2RxPduLen);

    /* Get the MSAP ID from received LLDPDU */
    if (LldpRxGetMSAPIdFromPdu (pPortInfo->pu1RxLldpdu,
                                &(MsapRBIndex.i4RemChassisIdSubtype),
                                &(MsapRBIndex.au1RemChassisId[0]),
                                &(MsapRBIndex.i4RemPortIdSubtype),
                                &(MsapRBIndex.au1RemPortId[0])) != OSIX_SUCCESS)
    {
        LLDP_TRC_ARG1 (LLDP_RED_TRC,
                       "LldpRedProcRemTabUpdateSyncUp:Get MSAPId From LLDPDU "
                       "is Failed for port:%d\r\n", u4RemLocalPortNum);
        /* Bad Frame */
        LLDP_INCR_CNTR_RX_FRAMES_ERRORS (pPortInfo);
        LLDP_INCR_CNTR_RX_FRAMES_DISCARDED (pPortInfo);
        /* transit to LLDP_RX_WAIT_FOR_FRAME state by setting current rx sem 
         * state as LLDP_RX_LLDP_INITIALIZE and posting 
         * LLDP_RX_EV_RXMOD_ADMIN_UP event to rxsem machine */
        pPortInfo->i4RxSemCurrentState = LLDP_RX_LLDP_INITIALIZE;
        LldpRxSemRun (pPortInfo, LLDP_RX_EV_RXMOD_ADMIN_UP);
        return;
    }

    /* Find the Remote Node by MSAP RBTree Index in RemMSAPRBTree */
    MsapRBIndex.i4RemLocalPortNum = (INT4) u4RemLocalPortNum;
    MsapRBIndex.u4DestAddrTblIndex = u4DestAddrTblIndex;
    pRemoteNode =
        LldpRxUtlGetRemoteNodeByMSAP (pLldpAgentPortInfo->u4LldpLocPort,
                                      MsapRBIndex.i4RemChassisIdSubtype,
                                      &(MsapRBIndex.au1RemChassisId[0]),
                                      MsapRBIndex.i4RemPortIdSubtype,
                                      &(MsapRBIndex.au1RemPortId[0]));
    if (pRemoteNode == NULL)
    {
        LLDP_TRC (LLDP_RED_TRC | LLDP_CRITICAL_TRC,
                  "LldpRedProcRemTabUpdateSyncUp: Received remote node "
                  "update sync up message for a remote node which is not "
                  "present in the remote table\r\n");
        return;
    }
    /* delete the existing entry */
    LldpRxUtlDelRemoteNode (pRemoteNode);
    /* update frame type */
    pPortInfo->u1RxFrameType = LLDP_RX_FRAME_UPDATE;
    /* add the received remote node information received in the sync up msg */
    LldpRedAddRemNode (pPortInfo, (INT4) u4RemIndex);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedDelRemNode
 *
 *    DESCRIPTION      : This function processes the remote table ageout
 *                       sync up message
 *
 *    INPUT            : pMsapRBIndex - pointer to MSAP RBtree index 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
INT4
LldpRedDelRemNode (tLldpRedMsapRBIndex * pMsapRBIndex)
{
    UINT4               u4LldpLocPort = 0;
    tLldpRemoteNode    *pRemoteNode = NULL;
    tLldpLocPortInfo   *pPortInfo = NULL;

    u4LldpLocPort =
        LldpGetLocalPortFromIfIndexDestMacIfIndex ((UINT4) pMsapRBIndex->
                                                   i4RemLocalPortNum,
                                                   pMsapRBIndex->
                                                   u4DestAddrTblIndex);

    /* Find the Remote Node by MSAP ID in RemMSAPRBTree */
    pRemoteNode = LldpRxUtlGetRemoteNodeByMSAP (u4LldpLocPort,
                                                pMsapRBIndex->
                                                i4RemChassisIdSubtype,
                                                &(pMsapRBIndex->
                                                  au1RemChassisId[0]),
                                                pMsapRBIndex->
                                                i4RemPortIdSubtype,
                                                &(pMsapRBIndex->
                                                  au1RemPortId[0]));
    if (pRemoteNode == NULL)
    {
        /* Remote entry delete sync up message received for a remote entry
         * which doesn't present in the remote table. ie.remote table
         * insert/update of this remote entry might have been missed */
        LLDP_TRC_ARG6 (LLDP_CRITICAL_TRC | LLDP_RED_TRC,
                       "Remote entry delete sync up message received for a "
                       "remote entry which doesn't present in the remote "
                       "table. And whose MSAP ID:%d:%u:%d:%s:%d:%s\r\n",
                       pMsapRBIndex->i4RemLocalPortNum,
                       pMsapRBIndex->u4DestAddrTblIndex,
                       pMsapRBIndex->i4RemChassisIdSubtype,
                       &(pMsapRBIndex->au1RemChassisId[0]),
                       pMsapRBIndex->i4RemPortIdSubtype,
                       &(pMsapRBIndex->au1RemPortId[0]));
        return OSIX_FAILURE;
    }
    LldpRxUtlDelRemoteNode (pRemoteNode);

    pPortInfo = LLDP_GET_LOC_PORT_INFO (u4LldpLocPort);
    if (pPortInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    /* Send Remtoe table change notification */
    if ((pPortInfo->PortConfigTable.u1NotificationEnable == LLDP_TRUE) &&
        ((pPortInfo->PortConfigTable.u1FsConfigNotificationType ==
          LLDP_REMOTE_CHG_NOTIFICATION) ||
         (pPortInfo->PortConfigTable.u1FsConfigNotificationType ==
          LLDP_REMOTE_CHG_MIS_CFG_NOTIFICATION)))
    {
        gLldpGlobalInfo.bSendRemTblNotif = OSIX_TRUE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedProcRemTabDeleteSyncUp 
 *
 *    DESCRIPTION      : This function processes the remote table delete 
 *                       sync up message
 *
 *    INPUT            : pRmMsg    - Sync up message 
 *                       u1MsgType - Message type
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedProcRemTabDeleteSyncUp (tRmMsg * pRmMsg, UINT1 u1MsgType)
{
    tLldpRedMsapRBIndex MsapRBIndex;
    tLldpLocPortInfo   *pPortInfo = NULL;
    UINT4               u4Offset = 0;
    UINT4               u4LldpLocPort = 0;

    MEMSET (&MsapRBIndex, 0, sizeof (tLldpRedMsapRBIndex));

    /* Message header is already read from RM message buffer.
     * So set the set the offset with LLDP_RED_MSG_HDR_SIZE */
    u4Offset = LLDP_RED_MSG_HDR_SIZE;

    LldpRedGetMsapRBIndexFromRmMsg (&MsapRBIndex, pRmMsg, &u4Offset);

    if (LldpRedDelRemNode (&MsapRBIndex) != OSIX_SUCCESS)
    {
        return;
    }

    u4LldpLocPort =
        LldpGetLocalPortFromIfIndexDestMacIfIndex ((UINT4) MsapRBIndex.
                                                   i4RemLocalPortNum,
                                                   MsapRBIndex.
                                                   u4DestAddrTblIndex);

    pPortInfo = LLDP_GET_LOC_PORT_INFO (u4LldpLocPort);
    if (pPortInfo == NULL)
    {
        return;
    }

    /* remote delete sync up is received in STANDBY node, whenever ACTIVE node 
     * receives shutdown and deletes the remote entry. so increment frames
     * received counter in STANDBY node only if the message type is
     * remote table delete */
    if (u1MsgType == LLDP_RED_REM_TAB_DELETE_MSG)
    {
        LLDP_INCR_CNTR_RX_FRAMES (pPortInfo);
    }

    /* LldpRedProcRemTabDeleteSyncUp is invoked for both remote table delete or
     * remote table ageout. So, increment the age out count if the 
     * received sync up message type is remote table ageout */
    else if (u1MsgType == LLDP_RED_REM_TAB_AGEOUT_MSG)
    {
        LLDP_REMOTE_STAT_TABLES_AGEOUTS++;
        LLDP_INCR_CNTR_RX_AGEOUTS (pPortInfo);
    }
    LLDP_REMOTE_STAT_TABLES_DELETES++;
    LLDP_REMOTE_STAT_LAST_CHNG_TIME = LLDP_SYS_UP_TIME ();
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedProcGlobBulkUpd
 *
 *    DESCRIPTION      : This function processes the global sync up information
 *                       received
 *
 *    INPUT            : pRmMsg - Sync up message
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedProcGlobBulkUpd (tRmMsg * pRmMsg)
{
    tLldpRedGlobBulkUpdInfo GlobBulkUpdInfo;
    UINT4               u4Offset = 0;
    UINT4               u4StandByNodeCurSysTime = 0;
    UINT4               u4ActivNodeCurSysTime = 0;
    UINT4               u4TimeDiff = 0;

    LLDP_TRC (LLDP_RED_TRC, "LldpRedProcGlobBulkUpd:Received global info "
              "sync up message from RM\r\n");

/* global bulk update information */
/*-----------------------------------------------------------------------------
 *| MsgHdr | ActivNodeCurSysTime | ActivUpSysTime | NotifTmrStartVal |
  | NotifIntRemTimeRemTime | RemStats|
 *----------------------------------------------------------------------------*/
    /* RemStats contains,
     * Remote table last change time
     * Remote table inserts
     * Remote table updates 
     * Remote table deletes
     * Remote table ageouts */

    /* get the global sync up info im RM message */
    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, GlobBulkUpdInfo.u4ActiveUpSysTime);
    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset,
                         GlobBulkUpdInfo.u4ActivNodeCurSysTime);
    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, GlobBulkUpdInfo.u4NotifTmrStartVal);
    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, GlobBulkUpdInfo.u4NotifIntRemTime);
    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, GlobBulkUpdInfo.u4RemTabLastChgTime);
    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, GlobBulkUpdInfo.u4RemTabInserts);
    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, GlobBulkUpdInfo.u4RemTabUpdates);
    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, GlobBulkUpdInfo.u4RemTabDeletes);
    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, GlobBulkUpdInfo.u4RemTabAgeOuts);

    /* Get the difference between the current system time in Standby node and 
     * the active node current system time(received in golbal bulk update 
     * message). If the difference is greater than the allowed difference, then
     * log critical trace */
    u4ActivNodeCurSysTime = GlobBulkUpdInfo.u4ActivNodeCurSysTime;
    u4StandByNodeCurSysTime = UtlGetTimeSinceEpoch ();

    /* get the time difference */
    if (u4ActivNodeCurSysTime >= u4StandByNodeCurSysTime)
    {
        u4TimeDiff = u4ActivNodeCurSysTime - u4StandByNodeCurSysTime;
    }
    else
    {
        u4TimeDiff = u4StandByNodeCurSysTime - u4ActivNodeCurSysTime;
    }

    if (u4TimeDiff > LLDP_RED_SYSTIME_ALLOWED_VARIANCE)
    {
        /* The following trace is used in test case which validates the
         * current system time of active node and standby node */
        LLDP_TRC (LLDP_RED_TRC | CONTROL_PLANE_TRC,
                  "Current system time doesn't match...\r\n");

        LLDP_TRC_ARG3 (LLDP_RED_TRC | CONTROL_PLANE_TRC,
                       "LldpRedProcGlobBulkUpd: Diffrence between "
                       " Active node system time:%d and"
                       " Standby node system time:%d"
                       " is more than the allowed value:%d."
                       " Configure system time of Active and"
                       " Standby nodes to same time\r\n",
                       u4ActivNodeCurSysTime,
                       u4StandByNodeCurSysTime,
                       LLDP_RED_SYSTIME_ALLOWED_VARIANCE);
    }
    /* update active up system time */
    LLDP_RED_ACTIVE_NODE_UP_SYSTIME () = GlobBulkUpdInfo.u4ActiveUpSysTime;

    /* u4NotifIntRemTime will be zero if timer is not running in the ACTIVE 
     * node, so no need to update expected expiry time if remaining time is 0 */

    if (GlobBulkUpdInfo.u4NotifIntRemTime != 0)
    {
        /* get the time interval with which the notification timer started
         * in active node */
        gLldpRedGlobalInfo.u4NotifIntTmrRcvdTime =
            GlobBulkUpdInfo.u4NotifTmrStartVal;
        /* update the otification interval timer expiry time */
        gLldpRedGlobalInfo.u4NotifIntTmrExpTime =
            (GlobBulkUpdInfo.u4NotifIntRemTime + UtlGetTimeSinceEpoch ());
    }

    /* update remote table statistics */
    LLDP_REMOTE_STAT_LAST_CHNG_TIME = GlobBulkUpdInfo.u4RemTabLastChgTime;
    LLDP_REMOTE_STAT_TABLES_INSERTS = GlobBulkUpdInfo.u4RemTabInserts;
    LLDP_REMOTE_STAT_TABLES_UPDATES = GlobBulkUpdInfo.u4RemTabUpdates;
    LLDP_REMOTE_STAT_TABLES_DELETES = GlobBulkUpdInfo.u4RemTabDeletes;
    LLDP_REMOTE_STAT_TABLES_AGEOUTS = GlobBulkUpdInfo.u4RemTabAgeOuts;

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedProcOperChgSyncUp
 *
 *    DESCRIPTION      : This function processes the Oper status information
 *                       received for a particular port
 *
 *    INPUT            : pRmMsg - Sync up message 
 *                       u2MsgLen - Message length
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedProcOperChgSyncUp (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    tLldpLocPortInfo   *pPortInfo = NULL;
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;
    tMacAddr            MacAddr;
    UINT1               u1OperStatus = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4Offset = 0;

    LLDP_TRC (LLDP_RED_TRC,
              "LldpRedProcOperChgSyncUp:Received port oper status change "
              "sync up message from RM\r\n");

    /* LLDP sync data - read the portnum, Oper status */
    /* -----------------------------------------------------------
     * | MsgType | MsgLen | ifIndex | MAC address |  Operational |
     * |         |        |         |             |  Status      |
     * -----------------------------------------------------------*/

    /* Message header is already read from RM message buffer.
     * So set the set the offset with LLDP_RED_MSG_HDR_SIZE */
    u4Offset = LLDP_RED_MSG_HDR_SIZE;
    /* validate message length */
    if (u2MsgLen != LLDP_RED_OPER_STATUS_CHG_MSG_LEN)
    {
        LLDP_TRC (LLDP_RED_TRC,
                  "LldpRedProcOperChgSyncUp: Message with invalid length\r\n");
        return;
    }
    /* Get IfIndex number  */
    LLDP_RED_GET_4_BYTE (pRmMsg, u4Offset, u4IfIndex);
    /*Get Mac Address */
    LLDP_RED_GET_6_BYTE (pRmMsg, u4Offset, &MacAddr);

    /* Get port oper status */
    LLDP_RED_GET_1_BYTE (pRmMsg, u4Offset, u1OperStatus);

    pLldpAgentPortInfo = LldpGetPortMapTableNode (u4IfIndex, &MacAddr);
    if (pLldpAgentPortInfo == NULL)
    {
        return;
    }

    pPortInfo = LLDP_GET_LOC_PORT_INFO (pLldpAgentPortInfo->u4LldpLocPort);
    if (pPortInfo == NULL)
    {
        LLDP_TRC_ARG1 (LLDP_RED_TRC,
                       "LldpRedProcOperChgSyncUp: Getting local port "
                       "information failed for port:%d\r\n",
                       pLldpAgentPortInfo->u4LldpLocPort);
        return;
    }

    if (LldpIfOperChg (pPortInfo, u1OperStatus) != OSIX_SUCCESS)
    {
        LLDP_TRC_ARG1 (LLDP_RED_TRC,
                       "LldpRedProcOperChgSyncUp: Processing oper status "
                       "change failed for port:%d\r\n",
                       pLldpAgentPortInfo->u4LldpLocPort);
        return;
    }

    return;
}

/*****************************************************************************
 * 
 * Common routines  
 *
 * ***************************************************************************/

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedInitRedGlobalInfo 
 *
 *    DESCRIPTION      : This function initializes LLDP-RED global information
 *                       with default value
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
PUBLIC VOID
LldpRedInitRedGlobalInfo (VOID)
{
    MEMSET (&(gLldpRedGlobalInfo), 0, sizeof (tLldpRedGlobalInfo));
    gLldpRedGlobalInfo.bBulkUpdNextPort = OSIX_FALSE;
    LLDP_RED_BULK_UPD_STATUS () = LLDP_BULK_UPD_NOT_STARTED;
    LLDP_RED_NODE_STATUS () = (UINT1) LldpPortGetRmNodeState ();
    /* update the standby node count */
    if (LLDP_RED_NODE_STATUS () == RM_ACTIVE)
    {
        gLldpRedGlobalInfo.u1NumPeersUp = LldpPortGetStandbyNodeCount ();
    }
    /* HITLESS RESTART */
    LLDP_HR_STDY_ST_REQ_RCVD () = OSIX_FALSE;
    gLldpRedGlobalInfo.bGlobShutWhileRcvdTimeProcessed = OSIX_FALSE;
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedFillMsapRBIndex 
 *
 *    DESCRIPTION      : This function retreives MSAP RBTress indices 
 *                       from remote node and fills it in the
 *                       in the given MsapRBIndex structure pointer
 *
 *    INPUT            : pMsapRBIndex - pointer to MSAP RBTree Index
 *                       pRemoteNode - pointer to remote node
 *
 *    OUTPUT           : pMsapRBIndex - pointer to MSAP RBTree Index
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
PUBLIC VOID
LldpRedFillMsapRBIndex (tLldpRedMsapRBIndex * pMsapRBIndex,
                        tLldpRemoteNode * pRemoteNode)
{
    UINT2               u2ChassIdLen = 0;
    UINT2               u2PortIdLen = 0;

    /* received port number */
    pMsapRBIndex->i4RemLocalPortNum = pRemoteNode->i4RemLocalPortNum;
    pMsapRBIndex->u4DestAddrTblIndex = pRemoteNode->u4DestAddrTblIndex;
    /* chassis id subtype */
    pMsapRBIndex->i4RemChassisIdSubtype = pRemoteNode->i4RemChassisIdSubtype;
    /* chassis id length is already validated and added in the remote node
     * during remote entry addition to the remote table, So 
     * LldpUtilGetChassisIdLen will never return failure in this scenario */
    LldpUtilGetChassisIdLen (pRemoteNode->i4RemChassisIdSubtype,
                             &(pRemoteNode->au1RemChassisId[0]), &u2ChassIdLen);
    /* chassis id */
    MEMCPY (&(pMsapRBIndex->au1RemChassisId[0]),
            &(pRemoteNode->au1RemChassisId[0]),
            MEM_MAX_BYTES (u2ChassIdLen, LLDP_MAX_LEN_CHASSISID));
    /* port id subtype */
    pMsapRBIndex->i4RemPortIdSubtype = pRemoteNode->i4RemPortIdSubtype;
    /* port id length is already validated and added in the remote node
     * during remote entry addition to the remote table, So 
     * LldpUtilGetPortIdLen will never return failure in this scenario */
    LldpUtilGetPortIdLen (pRemoteNode->i4RemPortIdSubtype,
                          &(pRemoteNode->au1RemPortId[0]), &u2PortIdLen);
    /* port id */
    MEMCPY (&(pMsapRBIndex->au1RemPortId[0]), &(pRemoteNode->au1RemPortId[0]),
            MEM_MAX_BYTES (u2PortIdLen, LLDP_MAX_LEN_PORTID));
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedProcBulkUpdReq
 *
 *    DESCRIPTION      : This function processes the bulk update request.
 *                       Bulk update sync up is splitted into the following:
 *                       1. Global information bulk update
 *                       2. Port specific information bulk update
 *                       3. Neighbor information bulk update: This is again 
 *                          splitted into the following:
 *                          a. Management address RBtree syncup
 *                          b. Protocol vlan Rbtree syncup
 *                          c. Vlan name Rbtree syncup
 *                          d. Unknown tlv Rbtree syncup
 *                          e. Unrecognized Organization tlv RBtree syncup
 *                          d. Remote Node RBtree syncup
 *                          The bulk syncup of each RBtree will be triggered
 *                          only when the information in the above RBtree
 *                          is completely synced with the standby.
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
PUBLIC VOID
LldpRedProcBulkUpdReq (VOID)
{
    if (LLDP_RED_NODE_STATUS () != RM_ACTIVE)
    {
        /* Only Active node can process bulk request */
        LLDP_TRC (LLDP_RED_TRC, "Rcvd Bulk Req when lldp node state is not "
                  " Active\r\n");
        return;
    }

    /* Bulk Update Message */
    /* --------------------------------------------------------------
     * | Msg Type | Len | Bulk Update Type | Bulk Update Information |
     * --------------------------------------------------------------*/

    LLDP_TRC (LLDP_RED_TRC, "Handling the Bulk Update Request\r\n");

    /* This case occurs when bulk update request is received when LLDP is in
     * the shutdown state*/
    if (LLDP_SYSTEM_CONTROL () != LLDP_START)
    {
        /* Global bulk update message needs to be sent as this message contains
         * the System time at which Active node was booted and also the current
         * system time of the Active node. This info will be send only in the
         * bulk update message and this is required when the module is enabled.
         */
        if (gLldpRedGlobalInfo.bGlobBulkUpdSent == OSIX_FALSE)
        {
            LldpRedSendGlobBulkUpd ();
        }
        LldpRedSendBulkUpdTailMsg ();
        return;
    }

    if (gLldpRedGlobalInfo.bGlobBulkUpdSent == OSIX_FALSE)
    {
        LldpRedSendGlobBulkUpd ();
        gLldpRedGlobalInfo.bGlobBulkUpdSent = OSIX_TRUE;
    }

    if (gLldpRedGlobalInfo.bBulkUpdNextPort != OSIX_TRUE)
    {
        LldpRedSendPortInfoBulkUpd ();
    }

    LldpRedSendRemInfoBulkUpd ();
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedSendRemInfoBulkUpd
 *
 *    DESCRIPTION      : This function sends the neighbor information to the 
 *                       standby node.
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
VOID
LldpRedSendRemInfoBulkUpd (VOID)
{
    /* This flag is used to indicate whether the trigger for next bulk update
     * has been given or not*/
    BOOL1               bBulkUpdEvtSent = OSIX_FALSE;

    if (LLDP_RED_MAN_ADR_BULK_UPD_STATUS () != OSIX_TRUE)
    {
        LldpRedSendManAddrTabInfo (&bBulkUpdEvtSent);
    }

    if (bBulkUpdEvtSent == OSIX_TRUE)
    {
        return;
    }

    if (LLDP_RED_PROTO_VLAN_BULK_UPD_STATUS () != OSIX_TRUE)
    {
        LldpRedSendProtoVlanTabInfo (&bBulkUpdEvtSent);
    }

    if (bBulkUpdEvtSent == OSIX_TRUE)
    {
        return;
    }

    if (LLDP_RED_VLAN_NAME_BULK_UPD_STATUS () != OSIX_TRUE)
    {
        LldpRedSendVlanNameTabInfo (&bBulkUpdEvtSent);
    }

    if (bBulkUpdEvtSent == OSIX_TRUE)
    {
        return;
    }

    if (LLDP_RED_UNK_TLV_BULK_UPD_STATUS () != OSIX_TRUE)
    {
        LldpRedSendUnknownTlvInfo (&bBulkUpdEvtSent);
    }

    if (bBulkUpdEvtSent == OSIX_TRUE)
    {
        return;
    }
    if (LLDP_RED_UNREC_ORG_BULK_UPD_STATUS () != OSIX_TRUE)
    {
        LldpRedSendOrgDefTabInfo (&bBulkUpdEvtSent);
    }

    if (bBulkUpdEvtSent == OSIX_TRUE)
    {
        return;
    }

    if (LLDP_RED_MED_NW_POL_BULK_UPD_STATUS () != OSIX_TRUE)
    {
        /* Bulk Update for LLDP-MED network policy remote nodes */
        LldpRedSendMedNwPolicyInfo ();
    }

    if (LLDP_RED_MED_LOCATION_BULK_UPD_STATUS () != OSIX_TRUE)
    {
        /* Bulk Update for LLDP-MED Location remote nodes */
        LldpRedSendMedLocationInfo ();
    }

    /* Bulk Update for LLDP remote nodes */
    LldpRedSendRemNodeInfo ();

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedSendRemNodeInfo
 *
 *    DESCRIPTION      : This function retreives a remote node information
 *                       and constructs remote node bulk update message and
 *                       sends the bulk update message to RM. 
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
VOID
LldpRedSendRemNodeInfo ()
{
    tLldpRemoteNode    *pRemoteNode = NULL;
    tLldpRemoteNode    *pNextRemoteNode = NULL;
    tRmMsg             *pRmMsgBuf = NULL;
    UINT4               u4Offset = 0;
    UINT4               u4MsgLenOffset = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2BulkUpdCount = 0;
    UINT2               u2MaxBulkUpdSize = LLDP_RED_MAX_BULK_UPD_SIZE;
    UINT1               u1MsgType = RM_BULK_UPDATE_MSG;
    UINT1               u1BulkUpdType = 0;

    if (LLDP_RED_BULK_UPD_STATUS () == LLDP_BULK_UPD_COMPLETED)
    {
        LldpRedSendBulkUpdTailMsg ();
        LLDP_TRC (LLDP_RED_TRC, "LldpRedSendRemNodeInfo: Completed Bulk "
                  "Request Handling!!!\n");
        return;
    }

    if (LLDP_RED_NEXT_REMOTE_NODE () == NULL)
    {
        LLDP_RED_NEXT_REMOTE_NODE () = (tLldpRemoteNode *)
            RBTreeGetFirst (gLldpGlobalInfo.RemSysDataRBTree);

        if (LLDP_RED_NEXT_REMOTE_NODE () == NULL)
        {
            LldpRedSendBulkUpdTailMsg ();
            LLDP_TRC (LLDP_RED_TRC, "LldpRedSendRemNodeInfo: Completed Bulk "
                      "Request Handling\n");
            return;
        }
    }

    pNextRemoteNode = LLDP_RED_NEXT_REMOTE_NODE ();
    u1BulkUpdType = LLDP_RED_REM_NODE_BULK_UPD_MSG;
    u2BulkUpdCount = LLDP_RED_REMOTE_NODE_CNT_PER_BULK_UPD;

    while ((pNextRemoteNode != NULL) && (u2BulkUpdCount > 0))
    {
        pRemoteNode = pNextRemoteNode;
        /* In this approach only 1 remote node info is sent in a single
         * message. This can be optimised by checking whether the message 
         * can accomadate another base node information or not. For this
         * get the size of the base remote node*/
        if (LldpRedAllocMemForRmMsg (u1MsgType, u2MaxBulkUpdSize,
                                     &pRmMsgBuf) != OSIX_SUCCESS)
        {
            return;
        }

        LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, u1MsgType);
        u4MsgLenOffset = u4Offset;
        LLDP_RED_PUT_2_BYTE (pRmMsgBuf, u4Offset, u2MsgLen);
        LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, u1BulkUpdType);

        LldpRedFormRemNodeBulkUpdMsg (pRmMsgBuf, &u4Offset, pRemoteNode);
        /* Fill the actual message length */
        u2MsgLen = (UINT2) u4Offset;
        LLDP_RED_PUT_2_BYTE (pRmMsgBuf, u4MsgLenOffset, u2MsgLen);

        LldpRedSendMsgToRm (u1MsgType, u2MsgLen, pRmMsgBuf);

        pNextRemoteNode = NULL;
        LldpRxUtlGetNextRemoteNode (pRemoteNode, &pNextRemoteNode);
        u4Offset = 0;
        u2BulkUpdCount--;
    }

    LLDP_RED_NEXT_REMOTE_NODE () = pNextRemoteNode;

    /* All the information in the Remote Node RBTree have been synced
     * with the standby. As there are no more information to be synced, send 
     * the tail message to standby node and indicate the completion of bulk 
     * request handling to RMGR in Active node. Also set the bulk update request
     * handling completion status in LLDP;
     */
    if (LLDP_RED_NEXT_REMOTE_NODE () == NULL)
    {
        LldpRedSendBulkUpdTailMsg ();
        LLDP_TRC (LLDP_RED_TRC, "LldpRedSendRemNodeInfo: Completed Bulk "
                  "Request Handling!!!\n");

    }
    else
    {
        LldpRedTrigNextBulkUpd ();
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedFormRemNodeBulkUpdMsg
 *
 *    DESCRIPTION      : This function constructs the remote node information
 *                       bulk update message by retreiving all remote node 
 *                       information present in the given node
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *                       pu4Offset - pointer to the next location from where
 *                                   information needs to be filled
 *                       pRemoteNode - pointer to remote node 
 *
 *    OUTPUT           : pRmMsg, pu4Offset
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
VOID
LldpRedFormRemNodeBulkUpdMsg (tRmMsg * pRmMsg, UINT4 *pu4Offset,
                              tLldpRemoteNode * pRemoteNode)
{
    tMacAddr            MacAddr;
    UINT4               u4RxInfoAgeRemTime = 0;
    UINT2               u2ChassIdLen = 0;
    UINT2               u2PortIdLen = 0;
    UINT1               u1PduMsgDigestLen = 0;

    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset, pRemoteNode->u4RemLastUpdateTime);
    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         (UINT4) pRemoteNode->i4RemLocalPortNum);

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    LldpGetMacAddrFromDestAddrTblIndex (pRemoteNode->u4DestAddrTblIndex,
                                        &MacAddr);
    LLDP_RED_PUT_6_BYTE (pRmMsg, *pu4Offset, &MacAddr);

    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset, (UINT4) pRemoteNode->i4RemIndex);

    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset, pRemoteNode->u4RemVidUsageDigest);
    LLDP_RED_PUT_2_BYTE (pRmMsg, *pu4Offset, pRemoteNode->u2RemMgmtVid);

    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset,
                         (UINT1) pRemoteNode->i4RemChassisIdSubtype);
    LldpUtilGetChassisIdLen (pRemoteNode->i4RemChassisIdSubtype,
                             pRemoteNode->au1RemChassisId, &u2ChassIdLen);
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, (UINT1) u2ChassIdLen);
    LLDP_RED_PUT_N_BYTE (pRmMsg, &(pRemoteNode->au1RemChassisId[0]),
                         *pu4Offset, (UINT4) u2ChassIdLen);
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset,
                         (UINT1) pRemoteNode->i4RemPortIdSubtype);
    LldpUtilGetPortIdLen (pRemoteNode->i4RemPortIdSubtype,
                          pRemoteNode->au1RemPortId, &u2PortIdLen);
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, (UINT1) u2PortIdLen);
    LLDP_RED_PUT_N_BYTE (pRmMsg, &(pRemoteNode->au1RemPortId[0]), *pu4Offset,
                         (UINT4) u2PortIdLen);
    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset, pRemoteNode->u4RxTTL);

    LldpRedAddBasicTlvInfo (pRmMsg, pu4Offset, pRemoteNode);

    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, pRemoteNode->bRcvdPVidTlv);

    if (pRemoteNode->bRcvdPVidTlv == OSIX_TRUE)
    {
        LLDP_RED_PUT_2_BYTE (pRmMsg, *pu4Offset, pRemoteNode->u2PVId);
    }

    LldpRedAddDot3TlvInfo (pRmMsg, pu4Offset, pRemoteNode);

    if (pRemoteNode->RxInfoAgeTmrNode.u1Status == LLDP_TMR_RUNNING)
    {
        TmrGetRemainingTime (gLldpGlobalInfo.TmrListId,
                             &(pRemoteNode->RxInfoAgeTmr.TimerNode),
                             &u4RxInfoAgeRemTime);
        u4RxInfoAgeRemTime = (u4RxInfoAgeRemTime /
                              SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
    }

    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset, u4RxInfoAgeRemTime);

    /* LLDP-MED related information - Capability */
    LldpRedAddMedTlvInfoBulkUpdMsg (pRmMsg, pu4Offset, pRemoteNode);

    /* Inventory information like, AssetId, Hw Revision, FirmwareRevision, Mfg
     * Name, Model Name and SW Revision */
    LldpRedAddInvTlvInfoBulkUpdMsg (pRmMsg, pu4Offset, pRemoteNode);
    LldpRedAddPowerTlvInfoBulkUpdMsg (pRmMsg, pu4Offset, pRemoteNode);

    u1PduMsgDigestLen = (UINT1) LLDP_STRLEN (&(pRemoteNode->au1PduMsgDigest[0]),
                                             16);
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1PduMsgDigestLen);

    if (u1PduMsgDigestLen != 0)
    {
        LLDP_RED_PUT_N_BYTE (pRmMsg, &(pRemoteNode->au1PduMsgDigest[0]),
                             *pu4Offset, u1PduMsgDigestLen);
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedAddDot3TlvInfo
 *
 *    DESCRIPTION      : This function fills the  dot3 tlv information present
 *                       in the given remote node in the RM message
 *
 *    INPUT            : pRmMsg    - pointer to RM message buffer
 *                       pu4Offset - pointer to the next location from where
 *                                   information needs to be filled
 *                       pRemoteNode - pointer to remote node 
 *
 *    OUTPUT           : pRmMsg, pu4Offset
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
VOID
LldpRedAddDot3TlvInfo (tRmMsg * pRmMsg, UINT4 *pu4Offset,
                       tLldpRemoteNode * pRemoteNode)
{
    tLldpxdot3RemPortInfo *pDot3RemPortInfo = NULL;
    UINT4               u4Dot3TlvOffset = 0;
    UINT2               u2AutoNegAdvtCap = 0;
    UINT1               u1Dot3TlvsFlag = 0;
    UINT1               u1AutoNegFlag = 0;

    u4Dot3TlvOffset = *pu4Offset;
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1Dot3TlvsFlag);

    if (pRemoteNode->pDot3RemPortInfo == NULL)
    {
        return;
    }

    pDot3RemPortInfo = pRemoteNode->pDot3RemPortInfo;

    if (pRemoteNode->bRcvdMacPhyTlv == OSIX_TRUE)
    {
        u1Dot3TlvsFlag = u1Dot3TlvsFlag | LLDP_RED_MAC_PHY_TLV_BMAP;

        if (pDot3RemPortInfo->RemDot3AutoNegInfo.u1AutoNegSupport == LLDP_TRUE)
        {
            u1AutoNegFlag = u1AutoNegFlag | LLDP_AUTONEG_SUPPORTED;
        }
        if (pDot3RemPortInfo->RemDot3AutoNegInfo.u1AutoNegEnabled == LLDP_TRUE)
        {
            u1AutoNegFlag = u1AutoNegFlag | LLDP_AUTONEG_ENABLED;
        }

        LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1AutoNegFlag);
        u2AutoNegAdvtCap =
            pDot3RemPortInfo->RemDot3AutoNegInfo.u2AutoNegAdvtCap;

        LLDP_RED_PUT_2_BYTE (pRmMsg, *pu4Offset, u2AutoNegAdvtCap);
        LLDP_RED_PUT_2_BYTE (pRmMsg, *pu4Offset,
                             pDot3RemPortInfo->RemDot3AutoNegInfo.
                             u2OperMauType);
    }

    if (pRemoteNode->bRcvdLinkAggTlv == OSIX_TRUE)
    {
        u1Dot3TlvsFlag = u1Dot3TlvsFlag | LLDP_RED_LINK_AGG_TLV_BMAP;
        LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, pDot3RemPortInfo->u1AggStatus);
        LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset, pDot3RemPortInfo->u4AggPortId);

    }
    if (pRemoteNode->bRcvdMaxFrameTlv == OSIX_TRUE)
    {
        u1Dot3TlvsFlag = u1Dot3TlvsFlag | LLDP_RED_MAX_FRAME_SIZE_TLV_BMAP;
        LLDP_RED_PUT_2_BYTE (pRmMsg, *pu4Offset,
                             pDot3RemPortInfo->u2MaxFrameSize);
    }

    /* Fill the type of TLVs that will be contained in this RM message */
    LLDP_RED_PUT_1_BYTE (pRmMsg, u4Dot3TlvOffset, u1Dot3TlvsFlag);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedSendManAddrTabInfo
 *
 *    DESCRIPTION      : This function sends the remote management address 
 *                       table information bulk update to RM 
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedSendManAddrTabInfo (BOOL1 * pbBulkUpdEvtSent)
{
    tLldpRemManAddressTable *pManAddrNode = NULL;
    tLldpRemManAddressTable *pNextManAddrNode = NULL;
    tRmMsg             *pRmMsgBuf = NULL;
    UINT4               u4Offset = 0;
    UINT4               u4MsgLenOffset = 0;
    UINT4               u4ManAddrMsgLen = 0;
    UINT4               u4CountOffset = 0;
    UINT4               u4ManAddrOIDLen = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2BulkUpdCount = 0;
    UINT4               u4MaxBulkUpdSize = LLDP_RED_MAX_BULK_UPD_SIZE;
    UINT1               u1MsgType = RM_BULK_UPDATE_MSG;
    UINT1               u1BulkUpdType = LLDP_RED_MANADDR_BULK_UPD_MSG;
    UINT1               u1ManAddrCount = 0;

    if (LLDP_RED_NEXT_MAN_ADDR_NODE () == NULL)
    {
        LLDP_RED_NEXT_MAN_ADDR_NODE () = (tLldpRemManAddressTable *)
            RBTreeGetFirst (gLldpGlobalInfo.RemManAddrRBTree);

        if (LLDP_RED_NEXT_MAN_ADDR_NODE () == NULL)
        {
            /* No entry is present in the management address RBtree. Set the
             * status as TRUE and proceed with the bulk update sync up of the
             * next RBtree*/
            LLDP_RED_MAN_ADR_BULK_UPD_STATUS () = OSIX_TRUE;
            LLDP_TRC (LLDP_RED_TRC, "LldpRedSendManAddrTabInfo: No entry is "
                      "present in the Remote Management Address RBTree\n");
            return;
        }
    }

    pNextManAddrNode = LLDP_RED_NEXT_MAN_ADDR_NODE ();
    u2BulkUpdCount = LLDP_RED_MAN_ADDR_CNT_PER_BULK_UPD;

    if (LldpRedAllocMemForRmMsg (u1MsgType, (UINT2) u4MaxBulkUpdSize,
                                 &pRmMsgBuf) != OSIX_SUCCESS)
    {
        return;
    }

    LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, u1MsgType);
    u4MsgLenOffset = u4Offset;
    /* Fill the message length as 0. Actual length will be filled before sending
     * message to RM
     */
    LLDP_RED_PUT_2_BYTE (pRmMsgBuf, u4Offset, u2MsgLen);
    LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, u1BulkUpdType);
    u4CountOffset = u4Offset;
    LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, u1ManAddrCount);

    while ((pNextManAddrNode != NULL) && (u2BulkUpdCount > 0))
    {
        pManAddrNode = pNextManAddrNode;
        u4ManAddrOIDLen = pManAddrNode->RemManAddrOID.u4_Length;
        u4ManAddrMsgLen = LLDP_RED_GET_MAN_ADDR_MSG_LEN
            (LldpUtilGetManAddrLen (pManAddrNode->i4RemManAddrSubtype),
             u4ManAddrOIDLen);
        if ((u4MaxBulkUpdSize - u4Offset) < u4ManAddrMsgLen)
        {
            u2MsgLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pRmMsgBuf);
            /* Fill the actual message length */
            LLDP_RED_PUT_2_BYTE (pRmMsgBuf, u4MsgLenOffset, u2MsgLen);
            LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4CountOffset, u1ManAddrCount);
            LldpRedSendMsgToRm (u1MsgType, u2MsgLen, pRmMsgBuf);
            u4Offset = 0;
            u1ManAddrCount = 0;
            if (LldpRedAllocMemForRmMsg (u1MsgType, (UINT2) u4MaxBulkUpdSize,
                                         &pRmMsgBuf) != OSIX_SUCCESS)
            {
                return;
            }

            LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, u1MsgType);
            u4MsgLenOffset = u4Offset;
            LLDP_RED_PUT_2_BYTE (pRmMsgBuf, u4Offset, u2MsgLen);
            LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, u1BulkUpdType);
            u4CountOffset = u4Offset;
            LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, u1ManAddrCount);
        }

        LldpRedFormManAddrBulkUpdMsg (pRmMsgBuf, &u4Offset, pManAddrNode);
        u1ManAddrCount++;
        if (LldpRxUtlGetNextManAddr (pManAddrNode, &pNextManAddrNode)
            != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC, "LldpRedSendManAddrTabInfo:"
                      "LldpRxUtlGetNextManAddr\r\n");
        }
        u2BulkUpdCount--;
    }

    /* Atleast 1 management address node information will be present 
     * in the message*/
    u2MsgLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pRmMsgBuf);
    /* Fill the actual message length */
    LLDP_RED_PUT_2_BYTE (pRmMsgBuf, u4MsgLenOffset, u2MsgLen);
    LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4CountOffset, u1ManAddrCount);
    LldpRedSendMsgToRm (u1MsgType, u2MsgLen, pRmMsgBuf);

    LLDP_RED_NEXT_MAN_ADDR_NODE () = pNextManAddrNode;

    /* All the information in the remote Management Address have been synced
     * with the standby. Hence set the management address bulk update status 
     * as TRUE and trigger for the next sub bulk update.
     */
    if (LLDP_RED_NEXT_MAN_ADDR_NODE () == NULL)
    {
        /* This approach will result in triggering the sub bulk update if there is
         * only 1 management address node. Can be avoided by maintaining a global
         * variable indicating the max. no: of RM message that can be sent in a single
         * bulk req.event. If the no: of messages sent does not reach the max count then
         * LldpRedTrigNextBulkUpd function shoould not be called and the pbBulkUpdEvtSent
         * variable should not be set. This will result in the processing of the next
         * table*/
        LLDP_RED_MAN_ADR_BULK_UPD_STATUS () = OSIX_TRUE;
    }
    LldpRedTrigNextBulkUpd ();
    *pbBulkUpdEvtSent = OSIX_TRUE;
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedSendProtoVlanTabInfo 
 *
 *    DESCRIPTION      : This function sends the remote protocol vlan 
 *                       table information bulk update to RM 
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedSendProtoVlanTabInfo (BOOL1 * pbBulkUpdEvtSent)
{
    tLldpxdot1RemProtoVlanInfo *pProtoVlanNode = NULL;
    tLldpxdot1RemProtoVlanInfo *pNextProtoVlanNode = NULL;
    tRmMsg             *pRmMsg = NULL;
    UINT4               u4Offset = 0;
    UINT4               u4MsgLenOffset = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2BulkUpdCount = 0;
    UINT2               u2MaxBulkUpdSize = LLDP_RED_MAX_BULK_UPD_SIZE;
    UINT1               u1MsgType = RM_BULK_UPDATE_MSG;
    UINT1               u1BulkUpdType = 0;

    if (LLDP_RED_NEXT_PROTO_VLAN_NODE () == NULL)
    {
        LLDP_RED_NEXT_PROTO_VLAN_NODE () = (tLldpxdot1RemProtoVlanInfo *)
            RBTreeGetFirst (gLldpGlobalInfo.RemProtoVlanRBTree);

        if (LLDP_RED_NEXT_PROTO_VLAN_NODE () == NULL)
        {
            LLDP_TRC (LLDP_RED_TRC, "LldpRedSendProtoVlanTabInfo: No entry is "
                      "present in the Protocol Vlan RBTree\n");
            LLDP_RED_PROTO_VLAN_BULK_UPD_STATUS () = OSIX_TRUE;
            return;
        }
    }

    pNextProtoVlanNode = LLDP_RED_NEXT_PROTO_VLAN_NODE ();
    u1BulkUpdType = LLDP_RED_PROTOVLAN_BULK_UPD_MSG;
    u2BulkUpdCount = LLDP_RED_PROTO_VLAN_CNT_PER_BULK_UPD;

    if (LldpRedAllocMemForRmMsg (u1MsgType, u2MaxBulkUpdSize,
                                 &pRmMsg) != OSIX_SUCCESS)
    {
        return;
    }

    LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
    u4MsgLenOffset = u4Offset;
    /* Fill the message length as 0. Actual length will be filled before sending
     * message to RM
     */
    LLDP_RED_PUT_2_BYTE (pRmMsg, u4Offset, u2MsgLen);
    LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1BulkUpdType);

    while ((pNextProtoVlanNode != NULL) && (u2BulkUpdCount > 0))
    {
        pProtoVlanNode = pNextProtoVlanNode;
        if ((u2MaxBulkUpdSize - u4Offset) < LLDP_RED_PROTO_VLAN_MSG_LEN)
        {
            u2MsgLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pRmMsg);
            /* Fill the actual message length */
            LLDP_RED_PUT_2_BYTE (pRmMsg, u4MsgLenOffset, u2MsgLen);
            LldpRedSendMsgToRm (u1MsgType, u2MsgLen, pRmMsg);
            u4Offset = 0;

            if (LldpRedAllocMemForRmMsg (u1MsgType, u2MaxBulkUpdSize,
                                         &pRmMsg) != OSIX_SUCCESS)
            {
                return;
            }

            LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
            u4MsgLenOffset = u4Offset;
            LLDP_RED_PUT_2_BYTE (pRmMsg, u4Offset, u2MsgLen);
            LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1BulkUpdType);
        }

        LldpRedFormProtoVlanBulkUpdMsg (pRmMsg, &u4Offset, pProtoVlanNode);
        LldpRxUtlGetNextProtoVlan (pProtoVlanNode, &pNextProtoVlanNode);
        u2BulkUpdCount--;
    }

    /* Atleast 1 port and protocol node information will be present 
     * in the message*/
    u2MsgLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pRmMsg);
    /* Fill the actual message length */
    LLDP_RED_PUT_2_BYTE (pRmMsg, u4MsgLenOffset, u2MsgLen);
    LldpRedSendMsgToRm (u1MsgType, u2MsgLen, pRmMsg);

    LLDP_RED_NEXT_PROTO_VLAN_NODE () = pNextProtoVlanNode;

    /* All the information in the remote Protocol Vlan RBTree have been synced
     * with the standby. Hence set the protocol vlan bulk update status 
     * as TRUE and trigger for the next sub bulk update.
     */
    if (LLDP_RED_NEXT_PROTO_VLAN_NODE () == NULL)
    {
        LLDP_RED_PROTO_VLAN_BULK_UPD_STATUS () = OSIX_TRUE;
    }
    LldpRedTrigNextBulkUpd ();
    *pbBulkUpdEvtSent = OSIX_TRUE;
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedSendVlanNameTabInfo
 *
 *    DESCRIPTION      : This function sends the remote vlan name
 *                       table information bulk update to RM 
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedSendVlanNameTabInfo (BOOL1 * pbBulkUpdEvtSent)
{
    tLldpxdot1RemVlanNameInfo *pVlanNameNode = NULL;
    tLldpxdot1RemVlanNameInfo *pNextVlanNameNode = NULL;
    tRmMsg             *pRmMsg = NULL;
    UINT4               u4Offset = 0;
    UINT4               u4MsgLenOffset = 0;
    UINT4               u4CountOffset = 0;
    UINT4               u4VlanNameMsgLen = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2BulkUpdCount = 0;
    UINT2               u2MaxBulkUpdSize = LLDP_RED_MAX_BULK_UPD_SIZE;
    UINT1               u1VlanCount = 0;
    UINT1               u1MsgType = RM_BULK_UPDATE_MSG;
    UINT1               u1BulkUpdType = 0;

    if (LLDP_RED_NEXT_VLAN_NAME_NODE () == NULL)
    {
        LLDP_RED_NEXT_VLAN_NAME_NODE () = (tLldpxdot1RemVlanNameInfo *)
            RBTreeGetFirst (gLldpGlobalInfo.RemVlanNameInfoRBTree);

        if (LLDP_RED_NEXT_VLAN_NAME_NODE () == NULL)
        {
            LLDP_TRC (LLDP_RED_TRC, "LldpRedSendVlanNameTabInfo: No entry is "
                      "present in the Vlan Name RBTree\n");
            LLDP_RED_VLAN_NAME_BULK_UPD_STATUS () = OSIX_TRUE;
            return;
        }
    }

    pNextVlanNameNode = LLDP_RED_NEXT_VLAN_NAME_NODE ();
    u1BulkUpdType = LLDP_RED_VLANNAME_BULK_UPD_MSG;
    u2BulkUpdCount = LLDP_RED_VLAN_NAME_CNT_PER_BULK_UPD;

    if (LldpRedAllocMemForRmMsg (u1MsgType, u2MaxBulkUpdSize,
                                 &pRmMsg) != OSIX_SUCCESS)
    {
        return;
    }

    LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
    u4MsgLenOffset = u4Offset;
    /* Fill the message length as 0. Actual length will be filled before sending
     * message to RM
     */
    LLDP_RED_PUT_2_BYTE (pRmMsg, u4Offset, u2MsgLen);
    LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1BulkUpdType);
    u4CountOffset = u4Offset;
    LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1VlanCount);

    while ((pNextVlanNameNode != NULL) && (u2BulkUpdCount > 0))
    {
        pVlanNameNode = pNextVlanNameNode;
        u4VlanNameMsgLen =
            LLDP_RED_GET_VLAN_NAME_MSG_LEN (pVlanNameNode->u1VlanNameLen);
        if ((u2MaxBulkUpdSize - u4Offset) < u4VlanNameMsgLen)
        {
            u2MsgLen = (UINT2) u4Offset;
            /* Fill the actual message length */
            LLDP_RED_PUT_2_BYTE (pRmMsg, u4MsgLenOffset, u2MsgLen);
            LLDP_RED_PUT_1_BYTE (pRmMsg, u4CountOffset, u1VlanCount);
            LldpRedSendMsgToRm (u1MsgType, u2MsgLen, pRmMsg);
            u4Offset = 0;
            u1VlanCount = 0;
            if (LldpRedAllocMemForRmMsg (u1MsgType, u2MaxBulkUpdSize,
                                         &pRmMsg) != OSIX_SUCCESS)
            {
                return;
            }

            LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
            u4MsgLenOffset = u4Offset;
            LLDP_RED_PUT_2_BYTE (pRmMsg, u4Offset, u2MsgLen);
            LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1BulkUpdType);
            u4CountOffset = u4Offset;
            LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1VlanCount);
        }

        LldpRedFormVlanNameBulkUpdMsg (pRmMsg, &u4Offset, pVlanNameNode);
        u1VlanCount++;
        LldpRxUtlGetNextVlanName (pVlanNameNode, &pNextVlanNameNode);
        u2BulkUpdCount--;
    }

    /* Atleast 1 vlan name node information will be present in the message */
    /* Offset contains the length of the entire message */
    LLDP_RED_PUT_2_BYTE (pRmMsg, u4MsgLenOffset, (UINT2) u4Offset);
    LLDP_RED_PUT_1_BYTE (pRmMsg, u4CountOffset, u1VlanCount);
    LldpRedSendMsgToRm (u1MsgType, (UINT2) u4Offset, pRmMsg);

    LLDP_RED_NEXT_VLAN_NAME_NODE () = pNextVlanNameNode;

    /* All the information in the remote Vlan Name RBTree have been synced
     * with the standby. Hence set the vlan name bulk update status 
     * as TRUE and trigger for the next sub bulk update.
     */
    if (LLDP_RED_NEXT_VLAN_NAME_NODE () == NULL)
    {
        LLDP_RED_VLAN_NAME_BULK_UPD_STATUS () = OSIX_TRUE;
    }

    LldpRedTrigNextBulkUpd ();
    *pbBulkUpdEvtSent = OSIX_TRUE;
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedSendUnknownTlvInfo
 *
 *    DESCRIPTION      : This function sends the remote unknown tlv
 *                       table information bulk update to RM 
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedSendUnknownTlvInfo (BOOL1 * pbBulkUpdEvtSent)
{
    tLldpRemUnknownTLVTable *pUnknownTLVNode = NULL;
    tLldpRemUnknownTLVTable *pNextUnknownTLVNode = NULL;
    tRmMsg             *pRmMsg = NULL;
    UINT4               u4Offset = 0;
    UINT4               u4MsgLenOffset = 0;
    UINT4               u4UnkTlvMsgLen = 0;
    UINT4               u4CountOffset = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2BulkUpdCount = 0;
    UINT2               u2MaxBulkUpdSize = LLDP_RED_MAX_BULK_UPD_SIZE;
    UINT1               u1MsgType = RM_BULK_UPDATE_MSG;
    UINT1               u1BulkUpdType = 0;
    UINT1               u1TlvInfoCount = 0;

    if (LLDP_RED_NEXT_UNK_TLV_NODE () == NULL)
    {
        LLDP_RED_NEXT_UNK_TLV_NODE () = (tLldpRemUnknownTLVTable *)
            RBTreeGetFirst (gLldpGlobalInfo.RemUnknownTLVRBTree);

        if (LLDP_RED_NEXT_UNK_TLV_NODE () == NULL)
        {
            LLDP_RED_UNK_TLV_BULK_UPD_STATUS () = OSIX_TRUE;
            LLDP_TRC (LLDP_RED_TRC, "LldpRedSendUnknownTlvInfo: No entry is "
                      "present in the UnknownTlv RBTree\n");
            return;
        }
    }

    pNextUnknownTLVNode = LLDP_RED_NEXT_UNK_TLV_NODE ();
    u1BulkUpdType = LLDP_RED_UNKNOWN_TLV_BULK_UPD_MSG;
    u2BulkUpdCount = LLDP_RED_UNK_TLV_CNT_PER_BULK_UPD;

    if (LldpRedAllocMemForRmMsg (u1MsgType, u2MaxBulkUpdSize,
                                 &pRmMsg) != OSIX_SUCCESS)
    {
        return;
    }

    LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
    u4MsgLenOffset = u4Offset;
    /* Fill the message length as 0. Actual length will be filled before sending
     * message to RM
     */
    LLDP_RED_PUT_2_BYTE (pRmMsg, u4Offset, u2MsgLen);
    LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1BulkUpdType);
    u4CountOffset = u4Offset;
    LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1TlvInfoCount);

    while ((pNextUnknownTLVNode != NULL) && (u2BulkUpdCount > 0))
    {
        pUnknownTLVNode = pNextUnknownTLVNode;
        u4UnkTlvMsgLen =
            LLDP_RED_GET_UNK_TLV_MSG_LEN
            (pUnknownTLVNode->u2RemUnknownTLVInfoLen);

        if ((u2MaxBulkUpdSize - u4Offset) < u4UnkTlvMsgLen)
        {
            u2MsgLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pRmMsg);
            /* Fill the actual message length */
            LLDP_RED_PUT_2_BYTE (pRmMsg, u4MsgLenOffset, u2MsgLen);
            LLDP_RED_PUT_1_BYTE (pRmMsg, u4CountOffset, u1TlvInfoCount);
            LldpRedSendMsgToRm (u1MsgType, u2MsgLen, pRmMsg);
            u1TlvInfoCount = 0;
            u4Offset = 0;
            if (LldpRedAllocMemForRmMsg (u1MsgType, u2MaxBulkUpdSize,
                                         &pRmMsg) != OSIX_SUCCESS)
            {
                return;
            }

            LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
            u4MsgLenOffset = u4Offset;
            LLDP_RED_PUT_2_BYTE (pRmMsg, u4Offset, u2MsgLen);
            LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1BulkUpdType);
            u4CountOffset = u4Offset;
            LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1TlvInfoCount);
        }

        LldpRedFormUnknownTlvBulkUpdMsg (pRmMsg, &u4Offset, pUnknownTLVNode);
        u1TlvInfoCount++;
        if (LldpRxUtlGetNextUnknownTLV (pUnknownTLVNode, &pNextUnknownTLVNode)
            != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC, "LldpRedSendUnknownTlvInfo:"
                      "LldpRxUtlGetNextUnknownTLV\r\n");
        }
        u2BulkUpdCount--;
    }

    /* Atleast 1 port and protocol node information will be present 
     * in the message*/
    u2MsgLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pRmMsg);
    /* Fill the actual message length */
    LLDP_RED_PUT_2_BYTE (pRmMsg, u4MsgLenOffset, u2MsgLen);
    LLDP_RED_PUT_1_BYTE (pRmMsg, u4CountOffset, u1TlvInfoCount);
    LldpRedSendMsgToRm (u1MsgType, u2MsgLen, pRmMsg);
    LLDP_RED_NEXT_UNK_TLV_NODE () = pNextUnknownTLVNode;

    /* All the information in the remote Unknown TLV RBTree have been synced
     * with the standby. Hence set the  Unknown TLV bulk update status 
     * as TRUE and trigger for the next sub bulk update.
     */
    if (LLDP_RED_NEXT_UNK_TLV_NODE () == NULL)
    {
        LLDP_RED_UNK_TLV_BULK_UPD_STATUS () = OSIX_TRUE;
    }

    LldpRedTrigNextBulkUpd ();
    *pbBulkUpdEvtSent = OSIX_TRUE;
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedSendOrgDefTabInfo
 *
 *    DESCRIPTION      : This function sends the remote unrecognized 
 *                       organizationally defined information table bulk update to RM 
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedSendOrgDefTabInfo (BOOL1 * pbBulkUpdEvtSent)
{
    tLldpRemOrgDefInfoTable *pOrgDefInfoNode = NULL;
    tLldpRemOrgDefInfoTable *pNextOrgDefInfoNode = NULL;
    tRmMsg             *pRmMsg = NULL;
    UINT4               u4Offset = 0;
    UINT4               u4MsgLenOffset = 0;
    UINT4               u4TlvCountOffset = 0;
    UINT4               u4OrgTlvLen = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2BulkUpdCount = 0;
    UINT2               u2MaxBulkUpdSize = LLDP_RED_MAX_BULK_UPD_SIZE;
    UINT1               u1MsgType = RM_BULK_UPDATE_MSG;
    UINT1               u1BulkUpdType = 0;
    UINT1               u1TlvCount = 0;

    if (LLDP_RED_NEXT_UNREC_ORG_NODE () == NULL)
    {
        LLDP_RED_NEXT_UNREC_ORG_NODE () = (tLldpRemOrgDefInfoTable *)
            RBTreeGetFirst (gLldpGlobalInfo.RemOrgDefInfoRBTree);

        if (LLDP_RED_NEXT_UNREC_ORG_NODE () == NULL)
        {
            LLDP_TRC (LLDP_RED_TRC, "LldpRedSendOrgDefTabInfo: No entry is "
                      "present in the unrecognized organizationally"
                      "defined TLV RBTree\n");
            LLDP_RED_UNREC_ORG_BULK_UPD_STATUS () = OSIX_TRUE;
            return;
        }
    }

    pNextOrgDefInfoNode = LLDP_RED_NEXT_UNREC_ORG_NODE ();
    u1BulkUpdType = LLDP_RED_ORGDEF_INFO_BULK_UPD_MSG;
    u2BulkUpdCount = LLDP_RED_UNREC_ORG_TLV_CNT_PER_BULK_UPD;

    if (LldpRedAllocMemForRmMsg (u1MsgType, u2MaxBulkUpdSize,
                                 &pRmMsg) != OSIX_SUCCESS)
    {
        return;
    }

    LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
    u4MsgLenOffset = u4Offset;
    /* Fill the message length as 0. Actual length will be filled before sending
     * message to RM
     */
    LLDP_RED_PUT_2_BYTE (pRmMsg, u4Offset, u2MsgLen);
    LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1BulkUpdType);
    u4TlvCountOffset = u4Offset;
    LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1TlvCount);

    while ((pNextOrgDefInfoNode != NULL) && (u2BulkUpdCount > 0))
    {
        pOrgDefInfoNode = pNextOrgDefInfoNode;
        u4OrgTlvLen =
            LLDP_RED_GET_UNREC_ORG_TLV_MSG_LEN (pOrgDefInfoNode->
                                                u2RemOrgDefInfoLen);
        if ((u2MaxBulkUpdSize - u4Offset) < u4OrgTlvLen)
        {
            u2MsgLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pRmMsg);
            /* Fill the actual message length */
            LLDP_RED_PUT_2_BYTE (pRmMsg, u4MsgLenOffset, u2MsgLen);
            LLDP_RED_PUT_1_BYTE (pRmMsg, u4TlvCountOffset, u1TlvCount);
            LldpRedSendMsgToRm (u1MsgType, u2MsgLen, pRmMsg);
            u4Offset = 0;
            u1TlvCount = 0;
            if (LldpRedAllocMemForRmMsg (u1MsgType, u2MaxBulkUpdSize,
                                         &pRmMsg) != OSIX_SUCCESS)
            {
                return;
            }

            LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
            u4MsgLenOffset = u4Offset;
            LLDP_RED_PUT_2_BYTE (pRmMsg, u4Offset, u2MsgLen);
            LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1BulkUpdType);
            u4TlvCountOffset = u4Offset;
            LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1TlvCount);
        }

        LldpRedFormOrgDefInfoBulkUpdMsg (pRmMsg, &u4Offset, pOrgDefInfoNode);
        u1TlvCount++;
        if (LldpRxUtlGetNextOrgDefInfo (pOrgDefInfoNode, &pNextOrgDefInfoNode)
            != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC, "LldpRedSendOrgDefTabInfo:"
                      "LldpRxUtlGetNextOrgDefInfo\r\n");
        }
        u2BulkUpdCount--;
    }

    /* Atleast 1 port and protocol node information will be present 
     * in the message*/
    u2MsgLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pRmMsg);
    /* Fill the actual message length */
    LLDP_RED_PUT_2_BYTE (pRmMsg, u4MsgLenOffset, u2MsgLen);
    LLDP_RED_PUT_1_BYTE (pRmMsg, u4TlvCountOffset, u1TlvCount);
    LldpRedSendMsgToRm (u1MsgType, u2MsgLen, pRmMsg);

    LLDP_RED_NEXT_UNREC_ORG_NODE () = pNextOrgDefInfoNode;

    /* All the information in the remote Unrecognized Org defined TLV RBTree
     * have been synced with the standby. Hence set the bulk update status 
     * as TRUE and trigger for the next sub bulk update.
     */
    if (LLDP_RED_NEXT_UNREC_ORG_NODE () == NULL)
    {
        LLDP_RED_UNREC_ORG_BULK_UPD_STATUS () = OSIX_TRUE;
    }

    LldpRedTrigNextBulkUpd ();
    *pbBulkUpdEvtSent = OSIX_TRUE;
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedFormManAddrBulkUpdMsg 
 *
 *    DESCRIPTION      : This function constructs the remote management
 *                       address bulk update message by retreiving all 
 *                       management address information present in the given
 *                       remote management address node 
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *                       pu4Offset - pointer to the next location from where
 *                                   information needs to be filled
 *                       pRemManAddrNode - pointer to management address node
 *                                         whose information needs to be filled
 *                                         in the RM message
 *
 *    OUTPUT           : pRmMsg, pu4Offset
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedFormManAddrBulkUpdMsg (tRmMsg * pRmMsg, UINT4 *pu4Offset,
                              tLldpRemManAddressTable * pRemManAddrNode)
{
    UINT1               u1ManAddrSubtype;
    UINT1               u1ManAddrIfSubtype;
    UINT1               u1OIDLen = 0;
    UINT1               u1ManAddrLen = 0;
    UINT1               au1ManAddrOidStr[LLDP_MAX_LEN_MAN_OID + 1];
    tMacAddr            MacAddr;

    MEMSET (au1ManAddrOidStr, 0, LLDP_MAX_LEN_MAN_OID + 1);
    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         pRemManAddrNode->u4RemLastUpdateTime);
    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         (UINT4) pRemManAddrNode->i4RemLocalPortNum);

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    LldpGetMacAddrFromDestAddrTblIndex (pRemManAddrNode->u4DestAddrTblIndex,
                                        &MacAddr);
    LLDP_RED_PUT_6_BYTE (pRmMsg, *pu4Offset, &MacAddr);

    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         (UINT4) pRemManAddrNode->i4RemIndex);
    u1ManAddrSubtype = (UINT1) pRemManAddrNode->i4RemManAddrSubtype;
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1ManAddrSubtype);
    u1ManAddrLen = (UINT1) LldpUtilGetManAddrLen
        (pRemManAddrNode->i4RemManAddrSubtype);
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1ManAddrLen);
    LLDP_RED_PUT_N_BYTE (pRmMsg, &(pRemManAddrNode->au1RemManAddr[0]),
                         *pu4Offset, (UINT4) u1ManAddrLen);
    u1ManAddrIfSubtype = (UINT1) pRemManAddrNode->i4RemManAddrIfSubtype;
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1ManAddrIfSubtype);
    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         (UINT4) pRemManAddrNode->i4RemManAddrIfId);
    if (LldpUtilEncodeManAddrOid (&(pRemManAddrNode->RemManAddrOID),
                                  &au1ManAddrOidStr[0]) == OSIX_SUCCESS)
    {
        u1OIDLen = au1ManAddrOidStr[0];
    }
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1OIDLen);
    if (u1OIDLen != 0)
    {
        LLDP_RED_PUT_N_BYTE (pRmMsg, &au1ManAddrOidStr[1], *pu4Offset,
                             (UINT4) u1OIDLen);
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedFormProtoVlanBulkUpdMsg
 *
 *    DESCRIPTION      : This function constructs the remote protocol
 *                       vlan bulk update message by retreiving all 
 *                       protocol vlan information present in 
 *                       given remote protocol vlan node
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *                       pu4Offset - pointer to the next location from where
 *                                   information needs to be filled
 *                       pProtoVlanNode - pointer to protocol vlan node 
 *
 *    OUTPUT           : pRmMsg, pu4Offset
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedFormProtoVlanBulkUpdMsg (tRmMsg * pRmMsg, UINT4 *pu4Offset,
                                tLldpxdot1RemProtoVlanInfo * pProtoVlanNode)
{
    tMacAddr            MacAddr;
    UINT1               u1ProtoVlanFlag = 0;

    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         pProtoVlanNode->u4RemLastUpdateTime);
    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         (UINT4) pProtoVlanNode->i4RemLocalPortNum);

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    LldpGetMacAddrFromDestAddrTblIndex (pProtoVlanNode->u4DestAddrTblIndex,
                                        &MacAddr);
    LLDP_RED_PUT_6_BYTE (pRmMsg, *pu4Offset, &MacAddr);

    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         (UINT4) pProtoVlanNode->i4RemIndex);

    if (pProtoVlanNode->u1ProtoVlanSupported == LLDP_TRUE)
    {
        u1ProtoVlanFlag = (u1ProtoVlanFlag | LLDP_PROTOVLAN_SUPPORTED);
    }
    if (pProtoVlanNode->u1ProtoVlanEnabled == LLDP_TRUE)
    {
        u1ProtoVlanFlag = (u1ProtoVlanFlag | LLDP_PROTOVLAN_ENABLED);
    }

    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1ProtoVlanFlag);
    LLDP_RED_PUT_2_BYTE (pRmMsg, *pu4Offset, pProtoVlanNode->u2ProtoVlanId);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedFormVlanNameBulkUpdMsg
 *
 *    DESCRIPTION      : This function constructs the remote vlan
 *                       name bulk update message by retreiving all 
 *                       vlan name information present in the given
 *                       remote vlan name node
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *                       pu4Offset - pointer to the next location from where
 *                                   information needs to be filled
 *                       pVlanNameNode - pointer to vlan name node
 *
 *    OUTPUT           : pRmMsg, pu4Offset
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedFormVlanNameBulkUpdMsg (tRmMsg * pRmMsg, UINT4 *pu4Offset,
                               tLldpxdot1RemVlanNameInfo * pVlanNameNode)
{
    tMacAddr            MacAddr;

    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         pVlanNameNode->u4RemLastUpdateTime);
    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         (UINT4) pVlanNameNode->i4RemLocalPortNum);

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    LldpGetMacAddrFromDestAddrTblIndex (pVlanNameNode->u4DestAddrTblIndex,
                                        &MacAddr);
    LLDP_RED_PUT_6_BYTE (pRmMsg, *pu4Offset, &MacAddr);

    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset, (UINT4) pVlanNameNode->i4RemIndex);
    LLDP_RED_PUT_2_BYTE (pRmMsg, *pu4Offset, pVlanNameNode->u2VlanId);
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, pVlanNameNode->u1VlanNameLen);
    LLDP_RED_PUT_N_BYTE (pRmMsg, &(pVlanNameNode->au1VlanName[0]),
                         *pu4Offset, (UINT4) pVlanNameNode->u1VlanNameLen);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedFormUnknownTlvBulkUpdMsg 
 *
 *    DESCRIPTION      : This function constructs the remote unknown tlv
 *                       information bulk update message by retreiving all 
 *                       unknown tlv information present in the given
 *                       remote unknown tlv node 
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *                       pu4Offset - pointer to the next location from where
 *                                   information needs to be filled
 *                       pUnknownTlvNode - pointer to the remote unknown tlv
 *                                         node
 *
 *    OUTPUT           : pRmMsg, pu4Offset
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedFormUnknownTlvBulkUpdMsg (tRmMsg * pRmMsg, UINT4 *pu4Offset,
                                 tLldpRemUnknownTLVTable * pUnknownTlvNode)
{
    tMacAddr            MacAddr;

    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         pUnknownTlvNode->u4RemLastUpdateTime);
    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         (UINT4) pUnknownTlvNode->i4RemLocalPortNum);

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    LldpGetMacAddrFromDestAddrTblIndex (pUnknownTlvNode->u4DestAddrTblIndex,
                                        &MacAddr);
    LLDP_RED_PUT_6_BYTE (pRmMsg, *pu4Offset, &MacAddr);

    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         (UINT4) pUnknownTlvNode->i4RemIndex);
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset,
                         (UINT1) pUnknownTlvNode->i4RemUnknownTLVType);
    LLDP_RED_PUT_2_BYTE (pRmMsg, *pu4Offset,
                         pUnknownTlvNode->u2RemUnknownTLVInfoLen);
    LLDP_RED_PUT_N_BYTE (pRmMsg,
                         &(pUnknownTlvNode->au1RemUnknownTLVInfo[0]),
                         *pu4Offset, pUnknownTlvNode->u2RemUnknownTLVInfoLen);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedFormOrgDefInfoBulkUpdMsg
 *
 *    DESCRIPTION      : This function constructs the remote 
 *                       organizationally defined information bulk update 
 *                       message by retreiving all organizationally defined 
 *                       information present in the given remote
 *                       organizationally defined node
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *                       pu4Offset - pointer to the next location from where
 *                                   information needs to be filled
 *                       pOrgDefInfoNode - pointer to the organizationally
 *                                         defined node 
 *
 *    OUTPUT           : pRmMsg, pu4Offset
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedFormOrgDefInfoBulkUpdMsg (tRmMsg * pRmMsg, UINT4 *pu4Offset,
                                 tLldpRemOrgDefInfoTable * pOrgDefInfoNode)
{
    UINT1               u1OrgDefInfoSubtype = 0;
    tMacAddr            MacAddr;

    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         pOrgDefInfoNode->u4RemLastUpdateTime);
    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         (UINT4) pOrgDefInfoNode->i4RemLocalPortNum);

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    LldpGetMacAddrFromDestAddrTblIndex (pOrgDefInfoNode->u4DestAddrTblIndex,
                                        &MacAddr);
    LLDP_RED_PUT_6_BYTE (pRmMsg, *pu4Offset, &MacAddr);

    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         (UINT4) pOrgDefInfoNode->i4RemIndex);
    LLDP_RED_PUT_N_BYTE (pRmMsg,
                         &(pOrgDefInfoNode->au1RemOrgDefInfoOUI[0]),
                         *pu4Offset, LLDP_MAX_LEN_OUI);
    u1OrgDefInfoSubtype = (UINT1) pOrgDefInfoNode->i4RemOrgDefInfoSubtype;
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1OrgDefInfoSubtype);
    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         (UINT4) pOrgDefInfoNode->i4RemOrgDefInfoIndex);
    LLDP_RED_PUT_2_BYTE (pRmMsg, *pu4Offset,
                         pOrgDefInfoNode->u2RemOrgDefInfoLen);
    LLDP_RED_PUT_N_BYTE (pRmMsg,
                         &(pOrgDefInfoNode->au1RemOrgDefInfo[0]),
                         *pu4Offset,
                         (UINT4) pOrgDefInfoNode->u2RemOrgDefInfoLen);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedFormPortInfoBulkUpd
 *
 *    DESCRIPTION      : This function fills the oper status and the age out
 *                       total count for the specific port into the RM message
 *
 *    INPUT            : pRmMsg    - pointer to RM message buffer
 *                       pu4Offset - pointer to the next location from where
 *                                   information needs to be filled
 *                       *pu1CountFlag - indicates whether the port count needs
 *                                       to be incremented
 *                       pPortInfo - pointer to port specific information 
 *                                   structure
 *
 *    OUTPUT           : pRmMsg, pu4Offset, pu1CountFlag 
*    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedFormPortInfoBulkUpd (tRmMsg * pRmMsg, UINT4 *pu4Offset,
                            UINT1 *pu1CountFlag, tLldpLocPortInfo * pPortInfo)
{
    tMacAddr            MacAddr;

    if (LldpGetMacFromLldpLocalPort (pPortInfo->u4LocPortNum, &MacAddr) !=
        OSIX_SUCCESS)
    {
        return;
    }

    if ((pPortInfo->u1OperStatus == CFA_IF_UP) ||
        (pPortInfo->StatsRxPortTable.u4AgeoutsTotal != 0))
    {
        LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset, pPortInfo->i4IfIndex);
        LLDP_RED_PUT_6_BYTE (pRmMsg, *pu4Offset, &MacAddr);
        LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, pPortInfo->u1OperStatus);
        LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                             pPortInfo->StatsRxPortTable.u4AgeoutsTotal);
        *pu1CountFlag = OSIX_TRUE;
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedAllocMemForRmMsg
 *
 *    DESCRIPTION      : This function allocates memory for dynamic sync up/
 *                       bulk update message
 *
 *    INPUT            : u1MsgType - Message type
 *                       u2MsgLen  - Message length
 *
 *    OUTPUT           : ppRmMsg - pointer to allocate memory 
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
LldpRedAllocMemForRmMsg (UINT1 u1MsgType, UINT2 u2MsgLen, tRmMsg ** ppRmMsg)
{
    tRmMsg             *pRmMsg = NULL;
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    *ppRmMsg = NULL;

    if ((pRmMsg = RM_ALLOC_TX_BUF ((UINT4) u2MsgLen)) == NULL)
    {
        if ((u1MsgType == LLDP_RED_BULK_UPDT_REQ_MSG) ||
            (u1MsgType == LLDP_RED_BULK_UPDATE_MSG) ||
            (u1MsgType == LLDP_RED_BULK_UPDT_TAIL_MSG))
        {
            /* Send bulk update failure event to RM */
            ProtoEvt.u4AppId = RM_LLDP_APP_ID;
            ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
            ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
            if (LldpPortSendEventToRm (&ProtoEvt) != OSIX_SUCCESS)
            {
                LLDP_TRC (LLDP_RED_TRC,
                          "LldpRedAllocMemForRmMsg: Sending failure event to"
                          " RM failed\r\n");
            }
        }
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_RED_TRC,
                  "LldpRedAllocMemForRmMsg:Memory allocation failed\r\n");
        return OSIX_FAILURE;
    }

    *ppRmMsg = pRmMsg;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedSendBulkUpdTailMsg
 *
 *    DESCRIPTION      : This function sends bulk update tail message to RM
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pRmMsg = NULL;
    UINT4               u4Offset = 0;
    UINT2               u2MsgLen = (UINT2) LLDP_RED_MSG_HDR_SIZE;
    UINT1               u1MsgType = (UINT1) RM_BULK_UPDT_TAIL_MSG;

    if (LldpRedAllocMemForRmMsg (u1MsgType, u2MsgLen, &pRmMsg) != OSIX_SUCCESS)
    {
        return;
    }

    /* update bulk update status in RMGR */
    LldpPortSetBulkUpdateStatus ();
    /* update bulk update status in LLDP */
    LLDP_RED_BULK_UPD_STATUS () = LLDP_BULK_UPD_COMPLETED;

    LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
    LLDP_RED_PUT_2_BYTE (pRmMsg, u4Offset, u2MsgLen);
    LldpRedSendMsgToRm (u1MsgType, u2MsgLen, pRmMsg);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedTrigNextBulkUpd
 *
 *    DESCRIPTION      : This function triggers next bulk update message
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedTrigNextBulkUpd (VOID)
{
    /* Send an event to start the next sub bulk update */
    OsixEvtSend (gLldpGlobalInfo.TaskId, LLDP_RED_SUB_BULK_UPD_EVENT);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedSendPortInfoBulkUpd
 *
 *    DESCRIPTION      : This function sends port specific bulk update message
 *                       to the Standby node. 
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedSendPortInfoBulkUpd (VOID)
{
    tRmMsg             *pRmMsg = NULL;
    tLldpLocPortInfo   *pPortInfo = NULL;
    tLldpv2AgentToLocPort LldpAgentPortInfo;
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;
    UINT4               u4Offset = 0;
    UINT4               u4MsgLenOffset = 0;
    UINT4               u4PortCountOffset = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2BufSize = 0;
    UINT1               u1CountFlag = OSIX_FALSE;
    UINT2               u2PortCount = 0;
    UINT1               u1MsgType = 0;
    UINT1               u1BulkUpdType = 0;

    /* port info bulk update data */
    /* -------------------------------------------------------------------------
     * | MsgHdr | Port Count | ifIndex | MAC address |OperStatus | AgeOutTotal |
     * -------------------------------------------------------------------------*/

    u1MsgType = RM_BULK_UPDATE_MSG;
    u1BulkUpdType = LLDP_RED_PORTINFO_BULK_UPD_MSG;
    u2BufSize = LLDP_RED_MAX_BULK_UPD_SIZE;

    MEMSET (&LldpAgentPortInfo, 0, sizeof (tLldpv2AgentToLocPort));

    while ((pLldpAgentPortInfo =
            LldpGetNextConfigPortMapNode (&LldpAgentPortInfo)) != NULL)
    {

        MEMCPY (&LldpAgentPortInfo, pLldpAgentPortInfo,
                sizeof (tLldpv2AgentToLocPort));
        pPortInfo = LLDP_GET_LOC_PORT_INFO (pLldpAgentPortInfo->u4LldpLocPort);
        if (pPortInfo == NULL)
        {
            LLDP_TRC_ARG1 (LLDP_RED_TRC,
                           "LldpRedSendPortInfoBulkUpd: Getting local port "
                           "information failed for port:%d\r\n",
                           pLldpAgentPortInfo->u4LldpLocPort);
            if (pRmMsg != NULL)
            {
                RM_FREE (pRmMsg);
            }
            return;
        }

        if (pRmMsg == NULL)
        {
            if (LldpRedAllocMemForRmMsg (u1MsgType, u2BufSize, &pRmMsg)
                != OSIX_SUCCESS)
            {
                return;
            }

            LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
            u4MsgLenOffset = u4Offset;

            /* Actual message length and port count will be filled depending
             * on the number of port information. Now 0 will be filled.
             */
            LLDP_RED_PUT_2_BYTE (pRmMsg, u4Offset, u2MsgLen);
            LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1BulkUpdType);
            u4PortCountOffset = u4Offset;
            LLDP_RED_PUT_2_BYTE (pRmMsg, u4Offset, u2PortCount);
        }

        if ((u2BufSize - u4Offset) < LLDP_RED_PORT_INFO_MSG_LEN)
        {
            break;
        }

        LldpRedFormPortInfoBulkUpd (pRmMsg, &u4Offset, &u1CountFlag, pPortInfo);

        if (u1CountFlag == OSIX_TRUE)
        {
            u1CountFlag = OSIX_FALSE;
            u2PortCount++;
        }
    }
    gLldpRedGlobalInfo.bBulkUpdNextPort = OSIX_TRUE;

    /* Send the message to RM only if atleast 1 port information is filled in 
     * the message*/
    if ((pRmMsg != NULL) && (u2PortCount != 0))
    {
        /* Fill the actual message length and the port count */
        u2MsgLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pRmMsg);
        LLDP_RED_PUT_2_BYTE (pRmMsg, u4MsgLenOffset, u2MsgLen);
        LLDP_RED_PUT_2_BYTE (pRmMsg, u4PortCountOffset, u2PortCount);
        LldpRedSendMsgToRm (u1MsgType, u2MsgLen, pRmMsg);
    }
    else if (pRmMsg != NULL)
    {
        RM_FREE (pRmMsg);
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedSendGlobBulkUpd
 *
 *    DESCRIPTION      : This function sends global information
 *                       bulk update message to the standby node.
 *
 *                       Global information includes the following,
 *                       u4ActiveUpSysTime     - System time at which Active 
 *                                               node booted up
 *                       u4NotifTmrStartVal    - Time interval with which 
 *                                               notification timer started
 *                                               in active node
 *                       u4ActivNodeCurSysTime - Active node's current system
 *                                               time
 *                       u4NotifIntRemTime     - Notification interval timer's
 *                                               remaining time(if timer is 
 *                                               running)
 *                       u4RemTabLastChgTime   - Remote table's last change time
 *                       u4RemTabInserts       - Remote table inserts count
 *                       u4RemTabUpdates       - Remote table updates count
 *                       u4RemTabAgeOuts       - Remote table ageout count
 *                       u4RemTabDeletes       - Remote table delete count
 *                       
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedSendGlobBulkUpd (VOID)
{
    UINT1               u1MsgType = 0;
    UINT1               u1BulkUpdtType = 0;
    UINT2               u2MsgLen = 0;
    UINT4               u4Offset = 0;
    tRmMsg             *pRmMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    tLldpRedGlobBulkUpdInfo GlobBulkUpdInfo;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    MEMSET (&GlobBulkUpdInfo, 0, sizeof (tLldpRedGlobBulkUpdInfo));

    LldpRedFormGlobBulkUpdInfo (&GlobBulkUpdInfo);

    /* Alloate memory for message */
    if ((pRmMsg = RM_ALLOC_TX_BUF (LLDP_RED_GLOBINFO_BULK_UPD_MSG_LEN)) == NULL)
    {
        /* Send bulk update failure event to RM */
        ProtoEvt.u4AppId = RM_LLDP_APP_ID;
        ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        if (LldpPortSendEventToRm (&ProtoEvt) != OSIX_SUCCESS)
        {
            LLDP_TRC (LLDP_RED_TRC, "LldpRedSendGlobBulkUpd: Sending bulk "
                      "update failure event to RM failed\r\n");
        }
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_RED_TRC, "LldpRedSendGlobBulkUpd: "
                  "Memory allocation failed for bulk update msg\r\n");
        return;
    }

    u1MsgType = (UINT1) RM_BULK_UPDATE_MSG;
    u2MsgLen = (UINT2) LLDP_RED_GLOBINFO_BULK_UPD_MSG_LEN;
    u1BulkUpdtType = (UINT1) LLDP_RED_GLOBINFO_BULK_UPD_MSG;

    LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
    LLDP_RED_PUT_2_BYTE (pRmMsg, u4Offset, u2MsgLen);
    LLDP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1BulkUpdtType);
    /* put the global sync up info im RM message */
    LLDP_RED_PUT_4_BYTE (pRmMsg, u4Offset, GlobBulkUpdInfo.u4ActiveUpSysTime);
    LLDP_RED_PUT_4_BYTE (pRmMsg, u4Offset,
                         GlobBulkUpdInfo.u4ActivNodeCurSysTime);
    LLDP_RED_PUT_4_BYTE (pRmMsg, u4Offset, GlobBulkUpdInfo.u4NotifTmrStartVal);
    LLDP_RED_PUT_4_BYTE (pRmMsg, u4Offset, GlobBulkUpdInfo.u4NotifIntRemTime);
    LLDP_RED_PUT_4_BYTE (pRmMsg, u4Offset, GlobBulkUpdInfo.u4RemTabLastChgTime);
    LLDP_RED_PUT_4_BYTE (pRmMsg, u4Offset, GlobBulkUpdInfo.u4RemTabInserts);
    LLDP_RED_PUT_4_BYTE (pRmMsg, u4Offset, GlobBulkUpdInfo.u4RemTabUpdates);
    LLDP_RED_PUT_4_BYTE (pRmMsg, u4Offset, GlobBulkUpdInfo.u4RemTabDeletes);
    LLDP_RED_PUT_4_BYTE (pRmMsg, u4Offset, GlobBulkUpdInfo.u4RemTabAgeOuts);
    LldpRedSendMsgToRm (u1MsgType, u2MsgLen, pRmMsg);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedSendMsgToRm
 *
 *    DESCRIPTION      : This function sends the given protocol(LLDP) message 
 *                       to RM
 *    
 *    INPUT            : u1MsgType - Message type
 *                       u2MsgLen - Message length
 *                       pRmMsg - Pointer to message
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedSendMsgToRm (UINT1 u1MsgType, UINT2 u2MsgLen, tRmMsg * pRmMsg)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    if (LldpPortEnqMsgToRm (pRmMsg, u2MsgLen) != OSIX_SUCCESS)
    {
        /* if msg type is,
         * bulk update request or
         * bulk update or
         * bulk update tail 
         * then notify about send to fail to RM */
        if ((u1MsgType == LLDP_RED_BULK_UPDT_REQ_MSG) ||
            (u1MsgType == LLDP_RED_BULK_UPDATE_MSG) ||
            (u1MsgType == LLDP_RED_BULK_UPDT_TAIL_MSG))
        {
            /* Send bulk update failure event to RM */
            ProtoEvt.u4AppId = RM_LLDP_APP_ID;
            ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            if (LldpPortSendEventToRm (&ProtoEvt) != OSIX_SUCCESS)
            {
                LLDP_TRC (LLDP_RED_TRC,
                          "LldpRedSendMsgToRm: Sending bulk update failure "
                          "event to RM failed\r\n");
            }
            LLDP_TRC (LLDP_RED_TRC,
                      "LldpRedSendMsgToRm: Sending bulk update msg/sync up msg"
                      "to RM failed\r\n");
        }
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedFormGlobBulkUpdInfo
 *
 *    DESCRIPTION      : This function forms global information bulk update 
 *                       message
 *                       
 *    INPUT            : NONE
 *    
 *    OUTPUT           : pGlobBulkUpdInfo - pointer to global bulk update 
 *                                          information structure
 *
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
LldpRedFormGlobBulkUpdInfo (tLldpRedGlobBulkUpdInfo * pGlobBulkUpdInfo)
{
    UINT4               u4NotifIntRemTime = 0;
    /* put system time at which active node is up */
    pGlobBulkUpdInfo->u4ActiveUpSysTime = LLDP_RED_ACTIVE_NODE_UP_SYSTIME ();
    /* get active node current system time */
    pGlobBulkUpdInfo->u4ActivNodeCurSysTime = UtlGetTimeSinceEpoch ();
    /* Get notification interval timer remaining time */
    if (LLDP_NOTIF_INTVAL_TMR_STATUS () == LLDP_TMR_RUNNING)
    {
        /* get the time interval with which notification timer started in 
         * active node */
        pGlobBulkUpdInfo->u4NotifTmrStartVal =
            gLldpGlobalInfo.u4NotifTmrStartVal;
        /* get the remaining time */
        TmrGetRemainingTime (gLldpGlobalInfo.TmrListId,
                             &(gLldpGlobalInfo.NotifIntervalTmr.TimerNode),
                             &u4NotifIntRemTime);
        pGlobBulkUpdInfo->u4NotifIntRemTime = (u4NotifIntRemTime /
                                               SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
    }
    /* put the following counter values in RM message.These counters are
     * accessed by NMS for polling remote table changes. So, these counters
     * need to be synced with STANDBY node in bulk update. 
     */
    pGlobBulkUpdInfo->u4RemTabLastChgTime = LLDP_REMOTE_STAT_LAST_CHNG_TIME;
    pGlobBulkUpdInfo->u4RemTabInserts = LLDP_REMOTE_STAT_TABLES_INSERTS;
    pGlobBulkUpdInfo->u4RemTabUpdates = LLDP_REMOTE_STAT_TABLES_UPDATES;
    pGlobBulkUpdInfo->u4RemTabDeletes = LLDP_REMOTE_STAT_TABLES_DELETES;
    pGlobBulkUpdInfo->u4RemTabAgeOuts = LLDP_REMOTE_STAT_TABLES_AGEOUTS;
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedAddBasicTlvInfo
 *
 *    DESCRIPTION      : This function fills the basic tlv information present
 *                       in the given remote node in the given RM message
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *                       pu4Offset - pointer to the next location from where
 *                                   information needs to be filled
 *                       pRemoteNode - pointer to remote node 
 *
 *    OUTPUT           : pRmMsg, pu4Offset
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
VOID
LldpRedAddBasicTlvInfo (tRmMsg * pRmMsg, UINT4 *pu4Offset,
                        tLldpRemoteNode * pRemoteNode)
{
    UINT1               u1PortDescLen = 0;
    UINT1               u1SysNameLen = 0;
    UINT1               u1SysDescLen = 0;
    UINT1               u1SysCapabLen = 0;

    /* Port Description TLV */
    u1PortDescLen = (UINT1) LLDP_STRLEN (&(pRemoteNode->au1RemPortDesc[0]),
                                         LLDP_MAX_LEN_PORTDESC);
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1PortDescLen);

    if (u1PortDescLen != 0)
    {
        LLDP_RED_PUT_N_BYTE (pRmMsg, &(pRemoteNode->au1RemPortDesc[0]),
                             *pu4Offset, (UINT4) u1PortDescLen);
    }

    /* System Name TLV */
    u1SysNameLen = (UINT1) LLDP_STRLEN (&(pRemoteNode->au1RemSysName[0]),
                                        LLDP_MAX_LEN_SYSNAME);
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1SysNameLen);

    if (u1SysNameLen != 0)
    {
        LLDP_RED_PUT_N_BYTE (pRmMsg, &(pRemoteNode->au1RemSysName[0]),
                             *pu4Offset, (UINT4) u1SysNameLen);
    }

    /* System Description TLV */
    u1SysDescLen = (UINT1) LLDP_STRLEN (&(pRemoteNode->au1RemSysDesc[0]),
                                        LLDP_MAX_LEN_SYSDESC);
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1SysDescLen);

    if (u1SysDescLen != 0)
    {
        LLDP_RED_PUT_N_BYTE (pRmMsg, &(pRemoteNode->au1RemSysDesc[0]),
                             *pu4Offset, (UINT4) u1SysDescLen);
    }

    /* System Capabilities TLV */
    /* if system capability tlv is received then any of the 
     * of system capability info byte will be not zero(0) */
    if ((pRemoteNode->au1RemSysCapSupported[0] != 0) ||
        (pRemoteNode->au1RemSysCapSupported[1] != 0) ||
        (pRemoteNode->au1RemSysCapEnabled[0] != 0) ||
        (pRemoteNode->au1RemSysCapEnabled[1] != 0))
    {
        /* u1SysCapabLen = sizeof (SysCapSupported) + sizeof (SysCapEnabled) */
        u1SysCapabLen = ISS_SYS_CAPABILITIES_LEN + ISS_SYS_CAPABILITIES_LEN;
    }

    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1SysCapabLen);
    if (u1SysCapabLen != 0)
    {
        LLDP_RED_PUT_N_BYTE (pRmMsg, &(pRemoteNode->au1RemSysCapSupported[0]),
                             *pu4Offset, ISS_SYS_CAPABILITIES_LEN);
        LLDP_RED_PUT_N_BYTE (pRmMsg, &(pRemoteNode->au1RemSysCapEnabled[0]),
                             *pu4Offset, ISS_SYS_CAPABILITIES_LEN);
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedInitBulkUpdateFlags
 *
 *    DESCRIPTION      : This function forms the basic tlv information
 *                       bulk update message
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *                       pRemoteNode - pointer to remote node 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
VOID
LldpRedInitBulkUpdateFlags ()
{
    gLldpRedGlobalInfo.bBulkUpdNextPort = OSIX_FALSE;
    gLldpRedGlobalInfo.bGlobBulkUpdSent = OSIX_FALSE;
    LLDP_RED_MAN_ADR_BULK_UPD_STATUS () = OSIX_FALSE;
    LLDP_RED_PROTO_VLAN_BULK_UPD_STATUS () = OSIX_FALSE;
    LLDP_RED_VLAN_NAME_BULK_UPD_STATUS () = OSIX_FALSE;
    LLDP_RED_UNK_TLV_BULK_UPD_STATUS () = OSIX_FALSE;
    LLDP_RED_UNREC_ORG_BULK_UPD_STATUS () = OSIX_FALSE;
    LLDP_RED_MED_NW_POL_BULK_UPD_STATUS () = OSIX_FALSE;
    LLDP_RED_MED_LOCATION_BULK_UPD_STATUS () = OSIX_FALSE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedUpdNxtRemNodeBlkUpdPtr
 *
 *    DESCRIPTION      : This function is used to update the BulkUpdNexNode
 *                       whenever a neighbor information is deleted.
 *
 *    INPUT            : pRemoteNode - pointer to remote node 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
VOID
LldpRedUpdNxtRemNodeBlkUpdPtr (tLldpRemoteNode * pRemoteNode)
{
    tLldpRemoteNode    *pTmpRemoteNode = NULL;

    if (LLDP_RED_NODE_STATUS () != RM_ACTIVE)
    {
        return;
    }
    LldpRedUpdNxtManAddrBlkUpdPtr (pRemoteNode);
    LldpRedUpdNxtProtoVlanBlkUpdPtr (pRemoteNode);
    LldpRedUpdNxtVlanNameBlkUpdPtr (pRemoteNode);
    LldpRedUpdNxtUnknownTLVBlkUpdPtr (pRemoteNode);
    LldpRedUpdNxtOrgDefInfoBlkUpdPtr (pRemoteNode);
    LldpRedUpdNxtMedNwPolBlkUpdPtr (pRemoteNode);
    LldpRedUpdNxtMedLocationBlkUpdPtr (pRemoteNode);

    if (LLDP_RED_NEXT_REMOTE_NODE () != NULL)
    {
        if (LLDP_IS_NODE_FOR_NEIGHBOR (pRemoteNode,
                                       LLDP_RED_NEXT_REMOTE_NODE ())
            == OSIX_TRUE)
        {
            if (LldpRxUtlGetNextRemoteNode (pRemoteNode, &pTmpRemoteNode)
                == OSIX_FAILURE)
            {
                LLDP_RED_BULK_UPD_STATUS () = LLDP_BULK_UPD_COMPLETED;
            }
            LLDP_RED_NEXT_REMOTE_NODE () = pTmpRemoteNode;
        }
    }

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedUpdNxtOrgDefInfoBlkUpdPtr
 *
 *    DESCRIPTION      : This function is used to update the next Unrecognized
 *                       Org TLV node whenever a neighbor information is deleted
 *
 *    INPUT            : pRemoteNode - pointer to remote node 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
VOID
LldpRedUpdNxtOrgDefInfoBlkUpdPtr (tLldpRemoteNode * pRemoteNode)
{
    tLldpRemOrgDefInfoTable *pNextOrgTLVNode;
    tLldpRemOrgDefInfoTable *pTmpOrgTLVNode;

    pTmpOrgTLVNode = LLDP_RED_NEXT_UNREC_ORG_NODE ();

    if (pTmpOrgTLVNode != NULL)
    {
        if (LLDP_IS_NODE_FOR_NEIGHBOR (pRemoteNode, pTmpOrgTLVNode)
            == OSIX_TRUE)
        {
            if (LldpRxUtlGetNextOrgDefInfo (pTmpOrgTLVNode,
                                            &pNextOrgTLVNode) == OSIX_FAILURE)
            {
                LLDP_RED_NEXT_UNREC_ORG_NODE () = NULL;
                LLDP_RED_UNREC_ORG_BULK_UPD_STATUS () = OSIX_TRUE;
                return;
            }

            while (LLDP_IS_NODE_FOR_NEIGHBOR (pRemoteNode, pNextOrgTLVNode)
                   == OSIX_TRUE)
            {
                pTmpOrgTLVNode = pNextOrgTLVNode;
                if (LldpRxUtlGetNextOrgDefInfo (pTmpOrgTLVNode,
                                                &pNextOrgTLVNode)
                    == OSIX_FAILURE)
                {
                    LLDP_RED_NEXT_UNREC_ORG_NODE () = NULL;
                    LLDP_RED_UNREC_ORG_BULK_UPD_STATUS () = OSIX_TRUE;
                    return;
                }
            }
            LLDP_RED_NEXT_UNREC_ORG_NODE () = pNextOrgTLVNode;
        }
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedUpdNxtUnknownTLVBlkUpdPtr
 *
 *    DESCRIPTION      : This function is used to update the next Unknown
 *                       TLV node whenever a neighbor information is deleted.
 *
 *    INPUT            : pRemoteNode - pointer to remote node 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
VOID
LldpRedUpdNxtUnknownTLVBlkUpdPtr (tLldpRemoteNode * pRemoteNode)
{
    tLldpRemUnknownTLVTable *pNextUnkTLVNode;
    tLldpRemUnknownTLVTable *pTmpUnkTLVNode;

    pTmpUnkTLVNode = LLDP_RED_NEXT_UNK_TLV_NODE ();

    if (pTmpUnkTLVNode != NULL)
    {
        if (LLDP_IS_NODE_FOR_NEIGHBOR (pRemoteNode, pTmpUnkTLVNode)
            == OSIX_TRUE)
        {
            if (LldpRxUtlGetNextUnknownTLV (pTmpUnkTLVNode,
                                            &pNextUnkTLVNode) == OSIX_FAILURE)
            {
                LLDP_RED_NEXT_UNK_TLV_NODE () = NULL;
                LLDP_RED_UNK_TLV_BULK_UPD_STATUS () = OSIX_TRUE;
                return;
            }

            while (LLDP_IS_NODE_FOR_NEIGHBOR (pRemoteNode, pNextUnkTLVNode)
                   == OSIX_TRUE)
            {
                pTmpUnkTLVNode = pNextUnkTLVNode;
                if (LldpRxUtlGetNextUnknownTLV (pTmpUnkTLVNode,
                                                &pNextUnkTLVNode)
                    == OSIX_FAILURE)
                {
                    LLDP_RED_NEXT_UNK_TLV_NODE () = NULL;
                    LLDP_RED_UNK_TLV_BULK_UPD_STATUS () = OSIX_TRUE;
                    return;
                }
            }
            LLDP_RED_NEXT_UNK_TLV_NODE () = pNextUnkTLVNode;
        }
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedUpdNxtVlanNameBlkUpdPtr
 *
 *    DESCRIPTION      : This function is used to update the next vlan
 *                       name node whenever a neighbor information is deleted.
 *
 *    INPUT            : pRemoteNode - pointer to remote node 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
VOID
LldpRedUpdNxtVlanNameBlkUpdPtr (tLldpRemoteNode * pRemoteNode)
{
    tLldpxdot1RemVlanNameInfo *pNextVlanNameNode;
    tLldpxdot1RemVlanNameInfo *pTmpVlanNameNode;

    pTmpVlanNameNode = LLDP_RED_NEXT_VLAN_NAME_NODE ();

    if (pTmpVlanNameNode != NULL)
    {
        if (LLDP_IS_NODE_FOR_NEIGHBOR (pRemoteNode, pTmpVlanNameNode)
            == OSIX_TRUE)
        {
            if (LldpRxUtlGetNextVlanName (pTmpVlanNameNode,
                                          &pNextVlanNameNode) == OSIX_FAILURE)
            {
                LLDP_RED_NEXT_VLAN_NAME_NODE () = NULL;
                LLDP_RED_VLAN_NAME_BULK_UPD_STATUS () = OSIX_TRUE;
                return;
            }

            while (LLDP_IS_NODE_FOR_NEIGHBOR (pRemoteNode, pNextVlanNameNode)
                   == OSIX_TRUE)
            {
                pTmpVlanNameNode = pNextVlanNameNode;
                if (LldpRxUtlGetNextVlanName (pTmpVlanNameNode,
                                              &pNextVlanNameNode)
                    == OSIX_FAILURE)
                {
                    LLDP_RED_NEXT_VLAN_NAME_NODE () = NULL;
                    LLDP_RED_VLAN_NAME_BULK_UPD_STATUS () = OSIX_TRUE;
                    return;
                }
            }
            LLDP_RED_NEXT_VLAN_NAME_NODE () = pNextVlanNameNode;
        }
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedUpdNxtProtoVlanBlkUpdPtr
 *
 *    DESCRIPTION      : This function is used to update the next protocol
 *                       vlan node whenever a neighbor information is deleted.
 *
 *    INPUT            : pRemoteNode - pointer to remote node 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
VOID
LldpRedUpdNxtProtoVlanBlkUpdPtr (tLldpRemoteNode * pRemoteNode)
{
    tLldpxdot1RemProtoVlanInfo *pNextProtoVlanNode;
    tLldpxdot1RemProtoVlanInfo *pTmpProtoVlanNode;

    pTmpProtoVlanNode = LLDP_RED_NEXT_PROTO_VLAN_NODE ();

    if (pTmpProtoVlanNode != NULL)
    {
        if (LLDP_IS_NODE_FOR_NEIGHBOR (pRemoteNode, pTmpProtoVlanNode)
            == OSIX_TRUE)
        {
            if (LldpRxUtlGetNextProtoVlan (pTmpProtoVlanNode,
                                           &pNextProtoVlanNode) == OSIX_FAILURE)
            {
                LLDP_RED_NEXT_PROTO_VLAN_NODE () = NULL;
                LLDP_RED_PROTO_VLAN_BULK_UPD_STATUS () = OSIX_TRUE;
                return;
            }

            while (LLDP_IS_NODE_FOR_NEIGHBOR (pRemoteNode, pNextProtoVlanNode)
                   == OSIX_TRUE)
            {
                pTmpProtoVlanNode = pNextProtoVlanNode;
                if (LldpRxUtlGetNextProtoVlan (pTmpProtoVlanNode,
                                               &pNextProtoVlanNode)
                    == OSIX_FAILURE)
                {
                    LLDP_RED_NEXT_PROTO_VLAN_NODE () = NULL;
                    LLDP_RED_PROTO_VLAN_BULK_UPD_STATUS () = OSIX_TRUE;
                    return;
                }
            }
            LLDP_RED_NEXT_PROTO_VLAN_NODE () = pNextProtoVlanNode;
        }
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpRedUpdNxtManAddrBlkUpdPtr
 *
 *    DESCRIPTION      : This function is used to update the next management
 *                       address node whenever a neighbor information is 
 *                       deleted.
 *
 *    INPUT            : pRemoteNode - pointer to remote node 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
VOID
LldpRedUpdNxtManAddrBlkUpdPtr (tLldpRemoteNode * pRemoteNode)
{
    tLldpRemManAddressTable *pTmpManAddNode;
    tLldpRemManAddressTable *pNextManAddNode;

    pTmpManAddNode = LLDP_RED_NEXT_MAN_ADDR_NODE ();
    if (pTmpManAddNode != NULL)
    {
        if (LLDP_IS_NODE_FOR_NEIGHBOR (pRemoteNode, pTmpManAddNode)
            == OSIX_TRUE)
        {
            if (LldpRxUtlGetNextManAddr (pTmpManAddNode, &pNextManAddNode) ==
                OSIX_FAILURE)
            {
                LLDP_RED_NEXT_MAN_ADDR_NODE () = NULL;
                LLDP_RED_MAN_ADR_BULK_UPD_STATUS () = OSIX_TRUE;
                return;
            }
            while (LLDP_IS_NODE_FOR_NEIGHBOR (pRemoteNode, pNextManAddNode)
                   == OSIX_TRUE)
            {
                pTmpManAddNode = pNextManAddNode;
                if (LldpRxUtlGetNextManAddr (pTmpManAddNode, &pNextManAddNode)
                    == OSIX_FAILURE)
                {
                    LLDP_RED_NEXT_MAN_ADDR_NODE () = NULL;
                    LLDP_RED_MAN_ADR_BULK_UPD_STATUS () = OSIX_TRUE;
                    return;
                }
            }
            LLDP_RED_NEXT_MAN_ADDR_NODE () = pNextManAddNode;
        }
    }
    return;
}

/* HITLESS RESTART */
/*****************************************************************************/
/* Function Name      : LldpRedHRProcStdyStPktReq                            */
/*                                                                           */
/* Description        : This function triggers the steady state packet from  */
/*                      LLDP to the RM by setting the periodic timeout value */
/*                      as zero                                              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LldpRedHRProcStdyStPktReq (VOID)
{
    tLldpLocPortInfo   *pLocPortEntry = NULL;
    tLldpv2AgentToLocPort LldpAgentPortInfo;
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;
    UINT1               u1TailFlag = 0;

    if (LLDP_MODULE_STATUS () == LLDP_DISABLED)
    {
        /* Module is disabled. So peroidic timers will not be running.
         *          * TO move next moduls, send tail msg here */
        LLDP_TRC (LLDP_RED_TRC, "LLDP Module is disabled; sending steady"
                  " state tail msg.\n");
        LldpRedHRSendStdyStTailMsg ();
        return;
    }

    if (LLDP_RED_NODE_STATUS () != RM_ACTIVE)
    {
        LLDP_TRC (LLDP_RED_TRC,
                  "LldpRedHRProcStdyStPktReq: LLDP Node is not active; "
                  " Processing of steady state pkt request failed.\r\n");
        return;
    }
    if (LLDP_HR_STATUS () == LLDP_HR_STATUS_DISABLE)
    {
        LLDP_TRC (LLDP_RED_TRC, "LLDP: Hitless restart is not enabled."
                  " Processing of steady state pkt request failed.\n");
        return;
    }

    /* This flag is set to indicate the LLDP timer expiry handler that steady 
     * state request had received from RM */

    LLDP_HR_STDY_ST_REQ_RCVD () = OSIX_TRUE;

    MEMSET (&LldpAgentPortInfo, 0, sizeof (tLldpv2AgentToLocPort));

    while ((pLldpAgentPortInfo =
            LldpGetNextConfigPortMapNode (&LldpAgentPortInfo)) != NULL)
    {

        MEMCPY (&LldpAgentPortInfo, pLldpAgentPortInfo,
                sizeof (tLldpv2AgentToLocPort));
        pLocPortEntry =
            LLDP_GET_LOC_PORT_INFO (pLldpAgentPortInfo->u4LldpLocPort);
        if (pLocPortEntry == NULL)
        {
            LLDP_TRC_ARG1 (LLDP_RED_TRC,
                           "LldpRedHRProcStdyStPktReq: Getting local port "
                           "information failed for port:%d\r\n",
                           pLldpAgentPortInfo->u4LldpLocPort);
            return;
        }

        if (pLocPortEntry->TtrTmrNode.u1Status == LLDP_TMR_RUNNING)
        {
            if (TmrStopTimer (gLldpGlobalInfo.TmrListId,
                              &(pLocPortEntry->TtrTmr.TimerNode)) !=
                TMR_SUCCESS)
            {
                LLDP_TRC_ARG1 (OS_RESOURCE_TRC | ALL_FAILURE_TRC
                               | CONTROL_PLANE_TRC,
                               "TX-SEM[%d]: In LldpTxSmStateShutFrame "
                               "TmrStopTimer(TtrTmr) returns FAILURE!!!\r\n",
                               pLocPortEntry->u4LocPortNum);
                continue;
            }
            /* set the transmit interval timer status as NOT_RUNNING */
            pLocPortEntry->TtrTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;

            if (TmrStart (gLldpGlobalInfo.TmrListId, &(pLocPortEntry->TtrTmr),
                          LLDP_TX_TMR_TTR, 0, 0) != TMR_SUCCESS)
            {
                LLDP_TRC_ARG1 (OS_RESOURCE_TRC | ALL_FAILURE_TRC |
                               CONTROL_PLANE_TRC,
                               "TX-SEM[%d]: TtrTmr Start Timer FAILED !!!\r\n",
                               pLocPortEntry->u4LocPortNum);
                continue;
            }
            /* set the transmit interval timer(TtrTmr) status as
             * RUNNING */
            pLocPortEntry->TtrTmrNode.u1Status = LLDP_TMR_RUNNING;
            if (u1TailFlag == 0)
            {
                u1TailFlag = LLDP_HR_STDY_ST_PKT_TAIL;
            }

        }
    }
    /* This steady state tail msg sending is done in case of LLDP periodic
     * timers are not running. */
    if (u1TailFlag != LLDP_HR_STDY_ST_PKT_TAIL)
    {
        LldpRedHRSendStdyStTailMsg ();

        /* This flag is set to indicate the LLDP timer expiry handler that steady
         * state request had received from RM */
        LLDP_HR_STDY_ST_REQ_RCVD () = OSIX_FALSE;
    }

    return;
}

/*****************************************************************************/
/* Function Name      : LldpRedHRSendStdyStPkt                               */
/*                                                                           */
/* Description        : This function sends steady state packet to RM.       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT1
LldpRedHRSendStdyStPkt (tCRU_BUF_CHAIN_HEADER * pu1LinBuf, UINT4 u4PktLen,
                        UINT2 u2Port, UINT4 u4TimeOut)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = 0;
    UINT4               u4MsgLen = 0;
    UINT4               u4BufSize = 0;

    if (LLDP_RED_NODE_STATUS () != RM_ACTIVE)
    {
        /*Only the Active node needs to send the SyncUp message */
        LLDP_TRC (LLDP_RED_TRC,
                  "LldpRedHRSendStdyStPkt: LLDP Node is in standby "
                  "state. So Steady state packet is not sent \r\n");
        return OSIX_SUCCESS;
    }
    if (LLDP_HR_STATUS () == LLDP_HR_STATUS_DISABLE)
    {
        LLDP_TRC (LLDP_RED_TRC, "LLDP: Hitless restart is not enabled."
                  " Sending of steady state pkt failed.\n");
        return OSIX_SUCCESS;
    }
    /* Forming  steady state packet

       <- 24B --><---- 1B -------><-- 2B -->< 2B -><-- 4B -->< 124 B >
       _______________________________________________________________________
       |        |                         |         |      |         |        |
       | RM Hdr | LLDP_HR_STDY_ST_PKT_MSG | Msg Len | PORT | TimeOut | Buffer |
       |________|_________________________|_________|______|_________|________|

       * The RM Hdr shall be included by RM.
     */

    /* If the packet length is less than Ethernet packet's minimum size,
     * the packet will be discarded by the peer. So increasing the packet length
     * if it is lesser 
     */
    if (u4PktLen < LLDP_ENET_MIN_PDU_SIZE)
    {
        u4PktLen = LLDP_ENET_MIN_PDU_SIZE;
    }

    u4BufSize = LLDP_RED_MSG_TYPE_SIZE + LLDP_RED_MSG_LEN_SIZE + sizeof (UINT2)    /* for port */
        + sizeof (UINT4)        /* Timeout */
        + u4PktLen;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u4BufSize)) == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "Rm alloc failed\n");
        return (OSIX_FAILURE);
    }

    /* Fill the message type. */

    u4MsgLen = u4PktLen + sizeof (UINT4) + sizeof (UINT2);

    LLDP_RED_PUT_1_BYTE (pMsg, u4Offset, LLDP_HR_STDY_ST_PKT_MSG);
    LLDP_RED_PUT_2_BYTE (pMsg, u4Offset, (UINT2) u4MsgLen);
    LLDP_RED_PUT_2_BYTE (pMsg, u4Offset, u2Port);
    LLDP_RED_PUT_4_BYTE (pMsg, u4Offset, u4TimeOut);

    CRU_BUF_Concat_MsgBufChains (pMsg, pu1LinBuf);

    /* Fill the port information to be synced up. */

    LldpRedSendMsgToRm (LLDP_HR_STDY_ST_PKT_MSG, (UINT2) u4BufSize, pMsg);
    LLDP_TRC (LLDP_RED_TRC,
              "LldpRedHRSendStdyStPkt: LLDP Steady state packet "
              "is sent to RM \r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LldpRedHRSendStdyStTailMsg                           */
/*                                                                           */
/* Description        : This function is called when all the steady state    */
/*                      pkts were sent to the RM. It sends steady state tail */
/*                      message to RM module.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT1
LldpRedHRSendStdyStTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = LLDP_RM_OFFSET;
    UINT2               u2BufSize = 0;

    if (LLDP_RED_NODE_STATUS () != RM_ACTIVE)
    {
        LLDP_TRC (LLDP_RED_TRC, "LLDP: Node is not active. Steady state "
                  "tail msg is not sent to RM.\n");
        return OSIX_SUCCESS;
    }
    if (LLDP_HR_STATUS () == LLDP_HR_STATUS_DISABLE)
    {
        LLDP_TRC (LLDP_RED_TRC, "LLDP: Hitless restart is not enabled. "
                  "Steady state tail msg is not sent to RM.\n");
        return OSIX_SUCCESS;
    }

    u2BufSize = LLDP_RED_MSG_TYPE_SIZE + LLDP_RED_MSG_LEN_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "Rm alloc failed\n");
        return (OSIX_FAILURE);
    }

    /* Form a steady state tail message.

     *         <--------1 Byte--------><---2 Byte--->
     ________ __________________________ _____________
     |        |                          |             |
     | RM Hdr | LLDP_HR_STDY_ST_PKT_TAIL | Msg Length  |
     |________|__________________________|_____________|

     * The RM Hdr shall be included by RM.
     */

    /* Fill the message type. */
    LLDP_RED_PUT_1_BYTE (pMsg, u4Offset, LLDP_HR_STDY_ST_PKT_TAIL);
    LLDP_RED_PUT_2_BYTE (pMsg, u4Offset, u2BufSize);

    LLDP_TRC (LLDP_RED_TRC, "LLDP: sending steady state tail msg to RM.\n");
    LldpRedSendMsgToRm (LLDP_HR_STDY_ST_PKT_TAIL, u2BufSize, pMsg);

    return OSIX_SUCCESS;
}

/***************************************************************************/
/* Function           : LldpRedGetHRFlag                                   */
/* Input(s)           : None                                               */
/* Output(s)          : None                                               */
/* Returns            : Hitless restart flag value.                        */
/* Action             : This API returns the hitless restart flag value.   */
/***************************************************************************/
UINT1
LldpRedGetHRFlag (VOID)
{
    UINT1               u1HRFlag = 0;

    u1HRFlag = RmGetHRFlag ();

    return (u1HRFlag);
}

/***************** LLDP-MED Related functionality *************************/

/****************************************************************************
 *  FUNCTION NAME    : LldpRedSendMedNwPolicyInfo 
 *
 *  DESCRIPTION      : This function retreives MED Network Poicy remote 
 *                     node information and sends bulk update message to RM.
 *
 *  INPUT            : NONE
 *
 *  OUTPUT           : NONE
 *
 *  RETURNS          : NONE 
 *
 ****************************************************************************/
VOID
LldpRedSendMedNwPolicyInfo ()
{
    tLldpMedRemNwPolicyInfo *pMedNwPolRemNode = NULL;
    tLldpMedRemNwPolicyInfo *pMedNextNwPolRemNode = NULL;
    tRmMsg             *pRmMsgBuf = NULL;
    UINT4               u4Offset = 0;
    UINT4               u4MsgLenOffset = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2BulkUpdCount = 0;
    UINT2               u2MaxBulkUpdSize = 0;
    UINT1               u1MsgType = 0;
    UINT1               u1BulkUpdType = 0;
    UINT1               u1TlvCount = 0;
    UINT4               u4TlvCountOffset = 0;

    LLDP_TRC (LLDP_RED_TRC, "LldpRedSendMedNwPolicyInfo: Send bulk "
              "update message for MED network policy info to RM.\n");

    /* Copy next node to local pointer and process further. */
    pMedNextNwPolRemNode = LLDP_RED_NEXT_MED_REM_NW_POL_NODE ();

    /* Verify if next remote node is available, if not get the first node
     * and send the bulk update message */
    if (pMedNextNwPolRemNode == NULL)
    {
        pMedNextNwPolRemNode = (tLldpMedRemNwPolicyInfo *)
            RBTreeGetFirst (gLldpGlobalInfo.LldpMedRemNwPolicyInfoRBTree);

        if (pMedNextNwPolRemNode == NULL)
        {
            LLDP_TRC (LLDP_RED_TRC, "LldpRedSendMedNwPolicyInfo: No nodes - "
                      "Completed Bulk update Request Handling\n");
            LLDP_RED_MED_NW_POL_BULK_UPD_STATUS () = OSIX_TRUE;
            return;
        }
    }

    u1BulkUpdType = LLDP_RED_MED_NWPOL_BULK_UPD_MSG;
    u2BulkUpdCount = LLDP_RED_MED_NWPOL_REM_NODE_CNT_PER_BULK_UPD;
    u2MaxBulkUpdSize = LLDP_RED_MAX_BULK_UPD_SIZE;
    u1MsgType = RM_BULK_UPDATE_MSG;

    if (LldpRedAllocMemForRmMsg (u1MsgType, u2MaxBulkUpdSize,
                                 &pRmMsgBuf) != OSIX_SUCCESS)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_RED_TRC,
                  "LldpRedSendMedNwPolicyInfo: Failed to allocate memory for "
                  "RM message.\n");
        return;
    }

    /* Fill the Message Header to be sent to RM module */
    LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, u1MsgType);

    /* Store the offset, such that actual length will be filled once
     * all the required TLV information is filled in the while loop. */
    u4MsgLenOffset = u4Offset;
    LLDP_RED_PUT_2_BYTE (pRmMsgBuf, u4Offset, u2MsgLen);

    /* Type LLDP_RED_MED_NWPOL_BULK_UPD_MSG */
    LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, u1BulkUpdType);

    /* Store the offset, to fill the TLV count at the end of while loop */
    u4TlvCountOffset = u4Offset;
    LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, u1TlvCount);

    while ((pMedNextNwPolRemNode != NULL) && (u2BulkUpdCount > 0))
    {
        /* Store the current node, which is used to get the next node */
        pMedNwPolRemNode = pMedNextNwPolRemNode;

        if ((u2MaxBulkUpdSize - u4Offset) < LLDP_MED_NW_POLICY_INFO_LEN)
        {
            u2MsgLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pRmMsgBuf);

            /* Fill the actual message length */
            LLDP_RED_PUT_2_BYTE (pRmMsgBuf, u4MsgLenOffset, u2MsgLen);

            /* Send Message to RM */
            LldpRedSendMsgToRm (u1MsgType, u2MsgLen, pRmMsgBuf);

            u4Offset = 0;

            if (LldpRedAllocMemForRmMsg (u1MsgType, u2MaxBulkUpdSize,
                                         &pRmMsgBuf) != OSIX_SUCCESS)
            {
                LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_RED_TRC,
                          "LldpRedSendMedNwPolicyInfo: Failed to allocate memory"
                          "for RM message.\n");
                return;
            }

            LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, u1MsgType);
            u4MsgLenOffset = u4Offset;
            LLDP_RED_PUT_2_BYTE (pRmMsgBuf, u4Offset, u2MsgLen);
            LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, u1BulkUpdType);
        }

        /* Construct the MED Network policy remote node information 
         * within the bulk update message */
        LldpRedFormMedNwPolBulkUpdMsg (pRmMsgBuf, &u4Offset, pMedNwPolRemNode);

        /* Increment the TLV count */
        u1TlvCount++;

        /* Reset the next pointer to null and get the next remote node */
        pMedNextNwPolRemNode = NULL;
        pMedNextNwPolRemNode = (tLldpMedRemNwPolicyInfo *) RBTreeGetNext
            (gLldpGlobalInfo.LldpMedRemNwPolicyInfoRBTree,
             (tRBElem *) pMedNwPolRemNode, NULL);

        /* Decrement the no of BulkUpd messages count to be sent */
        u2BulkUpdCount--;
    }

    u2MsgLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pRmMsgBuf);
    LLDP_RED_PUT_2_BYTE (pRmMsgBuf, u4MsgLenOffset, u2MsgLen);

    LLDP_TRC_ARG3 (LLDP_RED_TRC, "%s: Message Length :%d TLV count: %d\n",
                   __FUNCTION__, u2MsgLen, (UINT4) u1TlvCount);

    /* Fill the TLV count at the offset we stored earlier. Using this
     * count, we parse the no of TLVs. */
    LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4TlvCountOffset, u1TlvCount);

    /* Send the information to RM module */
    LldpRedSendMsgToRm (u1MsgType, u2MsgLen, pRmMsgBuf);

    /* Store the next remote node in the global structure variable
     * (gLldpRedGlobalInfo.pNextMedRemNwPolicynode) */
    LLDP_RED_NEXT_MED_REM_NW_POL_NODE () = pMedNextNwPolRemNode;

    if (LLDP_RED_NEXT_MED_REM_NW_POL_NODE () == NULL)
    {
        LLDP_RED_MED_NW_POL_BULK_UPD_STATUS () = OSIX_TRUE;
        LLDP_TRC (LLDP_RED_TRC, "LldpRedSendMedNwPolicyInfo: Completed Bulk "
                  "Request Handling!!!\n");
    }
    else
    {

        LLDP_TRC (LLDP_RED_TRC, "LldpRedSendMedNwPolicyInfo: Sending an event "
                  "to start the next sub bulk update !!!\n");
        /* Send an event to start the next sub bulk update for the pending 
         * information */
        LldpRedTrigNextBulkUpd ();
    }

    LLDP_TRC (LLDP_RED_TRC, "LldpRedSendMedNwPolicyInfo: Bulk "
              "update message for MED network policy sent to RM. Exiting\n");
    return;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpRedFormMedNwPolBulkUpdMsg
 *
 *    DESCRIPTION      : This function constructs the MED network policy remote 
 *                       node information bulk update message by retreiving
 *                       information present in the given node
 *
 *    INPUT            : pRmMsg      - pointer to RM message buffer
 *                       pu4Offset   - pointer to the next location from where
 *                                     information needs to be filled
 *                       pMedNwPolRemNode - pointer to remote node
 *
 *    OUTPUT           : pRmMsg, pu4Offset
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
VOID
LldpRedFormMedNwPolBulkUpdMsg (tRmMsg * pRmMsg, UINT4 *pu4Offset,
                               tLldpMedRemNwPolicyInfo * pMedNwPolRemNode)
{
    UINT4               u4PolicyInfo = 0;

    LLDP_TRC (LLDP_RED_TRC, "LldpRedFormMedNwPolBulkUpdMsg: Fill"
              "the RM message.\n");
    /* Last updated time */
    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         pMedNwPolRemNode->u4RemLastUpdateTime);
    /* Remote Local port number */
    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         (UINT4) pMedNwPolRemNode->i4RemLocalPortNum);
    /* Remote Index */
    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         (UINT4) pMedNwPolRemNode->i4RemIndex);
    /* Destination Address Table Index */
    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         pMedNwPolRemNode->u4RemDestAddrTblIndex);

    /* Attributes for Network Policy Information field
       ------------------------------------------------------
       | Application | U | T | X | Vlan   | L2      | DSCP  |
       |   Type      |   |   |   | ID     | Priority| VALUE |
       ------------------------------------------------------
       1 Octet        3 Bits      12-bits  3-bits    6-bits  
       <------------------  4 Octets -----------------------> */

    /* Application Type - Voice/Guest Voice/Soft phone Voice/Video Conferencing/
     *  Stream Video. */
    u4PolicyInfo = pMedNwPolRemNode->u1RemPolicyAppType;
    u4PolicyInfo = u4PolicyInfo << LLDP_MED_APP_TYPE_SHIFT_BIT;
    /* Policy unknown bit value */
    u4PolicyInfo |= pMedNwPolRemNode->bRemPolicyUnKnown <<
        LLDP_MED_UNKNOWN_SHIFT_BIT;
    /* Policy Tagged bit value */
    u4PolicyInfo |= pMedNwPolRemNode->bRemPolicyTagged <<
        LLDP_MED_TAGGED_SHIFT_BIT;
    /* Vlan Id value */
    u4PolicyInfo |= pMedNwPolRemNode->u2RemPolicyVlanId <<
        LLDP_MED_VLAN_ID_SHIFT_BIT;
    /* Priority value */
    u4PolicyInfo |= pMedNwPolRemNode->u1RemPolicyPriority <<
        LLDP_MED_PRIORITY_SHIFT_BIT;
    /* Dscp value */
    u4PolicyInfo |= pMedNwPolRemNode->u1RemPolicyDscp;

    LLDP_TRC_ARG5 (LLDP_RED_TRC,
                   "LldpRedFormMedNwPolBulkUpdMsg: "
                   "RemLocalPortNum[%d]\nRemIndex[%d]\nRemLastUpdateTime[%d]\n"
                   "DestAddrTblIndex[%d]\nPolicyAppType[%d]\r\n",
                   pMedNwPolRemNode->i4RemLocalPortNum,
                   pMedNwPolRemNode->i4RemIndex,
                   pMedNwPolRemNode->u4RemLastUpdateTime,
                   pMedNwPolRemNode->u4RemDestAddrTblIndex,
                   pMedNwPolRemNode->u1RemPolicyAppType);

    LLDP_TRC_ARG5 (LLDP_RED_TRC, "UnKnownFlag[%d]\n"
                   "Tagged[%d]\nVlanId[%d]\nPriority[%d]\nDscp[%d]\n",
                   pMedNwPolRemNode->bRemPolicyUnKnown,
                   pMedNwPolRemNode->bRemPolicyTagged,
                   pMedNwPolRemNode->u2RemPolicyVlanId,
                   pMedNwPolRemNode->u1RemPolicyPriority,
                   pMedNwPolRemNode->u1RemPolicyDscp);

    /* put network policy info */
    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset, u4PolicyInfo);

    LLDP_TRC (LLDP_RED_TRC, "LldpRedFormMedNwPolBulkUpdMsg: Completed"
              "forming the RM message.\n");
    return;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpRedProcMedNwPolRemNodeBulkUpd 
 *
 *    DESCRIPTION      : This function processes the MED network policy remote 
 *                       node information present in the RM message buffer.
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *
 *    OUTPUT           : NONE 
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
LldpRedProcMedNwPolRemNodeBulkUpd (tRmMsg * pRmMsg)
{
    tLldpMedRemNwPolicyInfo *pMedNwPolRemNode = NULL;
    UINT1               u1TlvCount = 0;
    UINT4               u4Offset = 0;

    LLDP_TRC (LLDP_RED_TRC, "LldpRedProcMedNwPolRemNodeBulkUpd: "
              "Received MED Network Policy Remote " "Node Bulk Update\n");

    LLDP_RED_GET_1_BYTE (pRmMsg, u4Offset, u1TlvCount);
    LLDP_TRC_ARG1 (LLDP_RED_TRC, "Received TLV count %d\n", u1TlvCount);

    while (u1TlvCount > 0)
    {
        /* Create Memory for Remote Node */
        if ((pMedNwPolRemNode = (tLldpMedRemNwPolicyInfo *)
             (MemAllocMemBlk (gLldpGlobalInfo.LldpMedRemNwPolicyPoolId))) ==
            NULL)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_RED_TRC,
                      "LldpRedProcMedNwPolRemNodeBulkUpd: Failed to Allocate Memory "
                      "for remote node \r\n");
#ifndef FM_WANTED
            SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                          "LldpRedProcMedNwPolRemNodeBulkUpd: Failed to Allocate Memory"
                          "for remote node"));
#endif
            return OSIX_FAILURE;
        }

        MEMSET (pMedNwPolRemNode, 0, sizeof (tLldpMedRemNwPolicyInfo));

        /* Fill the Remote node with the required information */
        LldpRedGetMedNwPolRemNodeInfo (pMedNwPolRemNode, &u4Offset, pRmMsg);

        LLDP_TRC_ARG5 (LLDP_RED_TRC,
                       "Add remote node to RBTree -\nRemLocalPortNum[%d]\n"
                       "i4RemIndex[%d]\nRemLastUpdateTime[%d]\n"
                       "DestAddrTblIndex[%d]\nPolicyAppType[%d]\r\n",
                       pMedNwPolRemNode->i4RemLocalPortNum,
                       pMedNwPolRemNode->i4RemIndex,
                       pMedNwPolRemNode->u4RemLastUpdateTime,
                       pMedNwPolRemNode->u4RemDestAddrTblIndex,
                       pMedNwPolRemNode->u1RemPolicyAppType);

        LLDP_TRC_ARG5 (LLDP_RED_TRC, "UnKnownFlag[%d]\n"
                       "Tagged[%d]\nVlanId[%d]\nPriority[%d]\nDscp[%d]\n",
                       pMedNwPolRemNode->bRemPolicyUnKnown,
                       pMedNwPolRemNode->bRemPolicyTagged,
                       pMedNwPolRemNode->u2RemPolicyVlanId,
                       pMedNwPolRemNode->u1RemPolicyPriority,
                       pMedNwPolRemNode->u1RemPolicyDscp);

        /* Insert the Node in LldpMedRemNwPolicyInfoRBTree */
        if (RBTreeAdd (gLldpGlobalInfo.LldpMedRemNwPolicyInfoRBTree,
                       (tRBElem *) pMedNwPolRemNode) != RB_SUCCESS)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_RED_TRC,
                      "LldpRedProcMedNwPolRemNodeBulkUpd: Failed to add remote "
                      "node to RBTree\r\n");
            MemReleaseMemBlock (gLldpGlobalInfo.LldpMedRemNwPolicyPoolId,
                                (UINT1 *) pMedNwPolRemNode);
            pMedNwPolRemNode = NULL;
        }

        u1TlvCount--;
        LLDP_TRC_ARG1 (LLDP_RED_TRC, "Tlv count %d\n", u1TlvCount);
    }

    LLDP_TRC (LLDP_RED_TRC, "LldpRedProcMedNwPolRemNodeBulkUpd: Successfully "
              "processed MED Network Policy Remote Node Bulk Update.\n");

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME    : LldpRedGetMedNwPolRemNodeInfo
 *
 * DESCRIPTION      : This function retreives the remote node information
 *                    present in RM message buffer and stores it in the
 *                    given remote node pointer
 *
 * INPUT            : pRmMsg - pointer to RM message buffer
 *
 * OUTPUT           : pMedNwPolRemNode - pointer to remote node information
 * 
 * RETURNS          : NONE
 ****************************************************************************/
VOID
LldpRedGetMedNwPolRemNodeInfo (tLldpMedRemNwPolicyInfo * pMedNwPolRemNode, UINT4
                               *pu4Offset, tRmMsg * pRmMsg)
{
    UINT4               u4RemDestAddrTblIndex = 0;
    UINT4               u4RemLocalPortNo = 0;
    UINT4               u4PolicyInfo = 0;

    LLDP_TRC (LLDP_RED_TRC, "LldpRedGetMedNwPolRemNodeInfo: Get the MED "
              "Network policy remote node information "
              "from Bulk update message\n");

    /* Last Update time */
    LLDP_RED_GET_4_BYTE (pRmMsg, *pu4Offset,
                         pMedNwPolRemNode->u4RemLastUpdateTime);

    /* Remote local port number */
    LLDP_RED_GET_4_BYTE (pRmMsg, *pu4Offset, u4RemLocalPortNo);
    pMedNwPolRemNode->i4RemLocalPortNum = (INT4) u4RemLocalPortNo;

    /* Remote Index */
    LLDP_RED_GET_4_BYTE (pRmMsg, *pu4Offset, pMedNwPolRemNode->i4RemIndex);

    /* Destination Address table Index */
    LLDP_RED_GET_4_BYTE (pRmMsg, *pu4Offset, u4RemDestAddrTblIndex);
    pMedNwPolRemNode->u4RemDestAddrTblIndex = u4RemDestAddrTblIndex;

    /* Get the 4-byte value to local variable */
    LLDP_RED_GET_4_BYTE (pRmMsg, *pu4Offset, u4PolicyInfo);

    /* Decode the attributes from Network Policy Information field
       ------------------------------------------------------
       | Application | U | T | X | Vlan   | L2      | DSCP  |
       |   Type      |   |   |   | ID     | Priority| VALUE |
       ------------------------------------------------------
       1 Octet        3 Bits      12-bits  3-bits    6-bits  
       <------------------  4 Octets -----------------------> */

    /* Get first octet for Application type value */
    pMedNwPolRemNode->u1RemPolicyAppType |= (u4PolicyInfo &
                                             LLDP_MED_APP_TYPE_BIT_MASK) >>
        LLDP_MED_APP_TYPE_SHIFT_BIT;
    /* Get next 1 bit value for Unknown flag */
    pMedNwPolRemNode->bRemPolicyUnKnown = LLDP_MED_GET_POLICY_BIT (u4PolicyInfo,
                                                                   LLDP_MED_UNKNOWN_SHIFT_BIT);

    if (pMedNwPolRemNode->bRemPolicyUnKnown != LLDP_MED_UNKNOWN_FLAG)
    {
        /* Get next 1 bit value for Tagged flag */
        pMedNwPolRemNode->bRemPolicyTagged |=
            LLDP_MED_GET_POLICY_BIT (u4PolicyInfo, LLDP_MED_TAGGED_SHIFT_BIT);
        /* Get next 12-bits value for Vlan ID */
        pMedNwPolRemNode->u2RemPolicyVlanId |= (u4PolicyInfo &
                                                LLDP_MED_VLAN_ID_BIT_MASK) >>
            LLDP_MED_VLAN_ID_SHIFT_BIT;
        /* Get next 3-bits value for Policy Priority */
        pMedNwPolRemNode->u1RemPolicyPriority |= (u4PolicyInfo &
                                                  LLDP_MED_PRIORITY_BIT_MASK) >>
            LLDP_MED_PRIORITY_SHIFT_BIT;
        /* Get the last 6-bits value for Dscp */
        pMedNwPolRemNode->u1RemPolicyDscp |= (u4PolicyInfo &
                                              LLDP_MED_DSCP_BIT_MASK);
    }

    LLDP_TRC_ARG5 (LLDP_RED_TRC,
                   "Received TLV info -\nRemLocalPortNum[%d]\n"
                   "i4RemIndex[%d]\nRemLastUpdateTime[%d]\n"
                   "DestAddrTblIndex[%d]\nPolicyAppType[%d]\r\n",
                   pMedNwPolRemNode->i4RemLocalPortNum,
                   pMedNwPolRemNode->i4RemIndex,
                   pMedNwPolRemNode->u4RemLastUpdateTime,
                   pMedNwPolRemNode->u4RemDestAddrTblIndex,
                   pMedNwPolRemNode->u1RemPolicyAppType);

    LLDP_TRC_ARG5 (LLDP_RED_TRC, "UnKnownFlag[%d]\n"
                   "Tagged[%d]\nVlanId[%d]\nPriority[%d]\nDscp[%d]\n",
                   pMedNwPolRemNode->bRemPolicyUnKnown,
                   pMedNwPolRemNode->bRemPolicyTagged,
                   pMedNwPolRemNode->u2RemPolicyVlanId,
                   pMedNwPolRemNode->u1RemPolicyPriority,
                   pMedNwPolRemNode->u1RemPolicyDscp);

    LLDP_TRC (LLDP_RED_TRC, "LldpRedGetMedNwPolRemNodeInfo: Remote node "
              "information fetched exiting.\n");
    return;
}

/*************************************************************************
 * FUNCTION NAME    : LldpRedUpdNxtMedNwPolBlkUpdPtr
 *
 * DESCRIPTION      : This function is used to update the next MED network 
 *                    policy node whenever a neighbor information is deleted
 *
 * INPUT            : pRemoteNode - pointer to remote node 
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : NONE 
 *************************************************************************/
VOID
LldpRedUpdNxtMedNwPolBlkUpdPtr (tLldpRemoteNode * pRemoteNode)
{
    tLldpMedRemNwPolicyInfo *pMedNwPolRemNode = NULL;
    tLldpMedRemNwPolicyInfo *pNextMedNwPolRemNode = NULL;

    LLDP_TRC (LLDP_RED_TRC, "LldpRedUpdNxtMedNwPolBlkUpdPtr: "
              "Function entry.\n");

    pMedNwPolRemNode = LLDP_RED_NEXT_MED_REM_NW_POL_NODE ();

    if (pMedNwPolRemNode == NULL)
    {
        LLDP_TRC (LLDP_RED_TRC, "LldpRedUpdNxtMedNwPolBlkUpdPtr: "
                  "No node present.\n");
        return;
    }

    while ((LLDP_IS_NODE_FOR_NEIGHBOR (pRemoteNode, pMedNwPolRemNode) ==
            OSIX_TRUE) && (pRemoteNode->u4DestAddrTblIndex
                           == pMedNwPolRemNode->u4RemDestAddrTblIndex))
    {
        pNextMedNwPolRemNode = (tLldpMedRemNwPolicyInfo *)
            RBTreeGetNext (gLldpGlobalInfo.LldpMedLocNwPolicyInfoRBTree,
                           (tRBElem *) pMedNwPolRemNode, NULL);
        if (pNextMedNwPolRemNode == NULL)
        {
            LLDP_RED_NEXT_MED_REM_NW_POL_NODE () = NULL;
            LLDP_RED_MED_NW_POL_BULK_UPD_STATUS () = OSIX_TRUE;
            return;
        }
        pMedNwPolRemNode = pNextMedNwPolRemNode;
    }
    LLDP_RED_NEXT_MED_REM_NW_POL_NODE () = pNextMedNwPolRemNode;

    LLDP_TRC (LLDP_RED_TRC, "LldpRedUpdNxtMedNwPolBlkUpdPtr: "
              "Function exit.\n");
    return;
}

/*************************************************************************
 * FUNCTION NAME    : LldpRedAddMedTlvInfoBulkUpdMsg 
 *
 * DESCRIPTION      : This function is used to constrcut Med Capability Tlv 
 *                    information to be send in bulk update message
 *
 * INPUT            : pRmMsg      - pointer to message buffer
 *                    pu4Offset   - offset
 *                    pRemoteNode - pointer to remote node 
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : NONE 
 *************************************************************************/
VOID
LldpRedAddMedTlvInfoBulkUpdMsg (tRmMsg * pRmMsg, UINT4 *pu4Offset,
                                tLldpRemoteNode * pRemoteNode)
{
    UINT2               u2CapSupported =
        pRemoteNode->RemMedCapableInfo.u2MedRemCapSupported;
    UINT2               u2CapEnable =
        pRemoteNode->RemMedCapableInfo.u2MedRemCapEnable;
    UINT1               u1DeviceClass =
        pRemoteNode->RemMedCapableInfo.u1MedRemDeviceClass;

    LLDP_TRC_ARG3 (LLDP_RED_TRC, "LldpRedAddMedTlvInfoBulkUpdMsg: "
                   "CapSupported %d, CapEnable = %d, DeviceClass = %d\n",
                   u2CapSupported, u2CapEnable, u1DeviceClass);

    LLDP_RED_PUT_2_BYTE (pRmMsg, *pu4Offset, u2CapSupported);
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1DeviceClass);
    LLDP_RED_PUT_2_BYTE (pRmMsg, *pu4Offset, u2CapEnable);

    LLDP_TRC (LLDP_RED_TRC, "LldpRedAddMedTlvInfoBulkUpdMsg: "
              "Function exit.\n");
    return;
}

/*************************************************************************
 * FUNCTION NAME    : LldpRedGetMedTlvInfoBulkUpdMsg 
 *
 * DESCRIPTION      : This function is used to extract Med Capability Tlv 
 *                    information from bulk update message
 *
 * INPUT            : pRmMsg      - pointer to message buffer
 *                    pu4Offset   - offset
 *                    pRemoteNode - pointer to remote node 
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : NONE 
 *************************************************************************/
VOID
LldpRedGetMedTlvInfoBulkUpdMsg (tRmMsg * pRmMsg, UINT4 *pu4Offset,
                                tLldpRemoteNode * pRemoteNode)
{
    UINT2               u2CapSupported = 0;
    UINT2               u2CapEnable = 0;
    UINT1               u1DeviceClass = 0;

    LLDP_TRC (LLDP_RED_TRC, "LldpRedGetMedTlvInfoBulkUpdMsg: Func Entry \n");

    LLDP_RED_GET_2_BYTE (pRmMsg, *pu4Offset, u2CapSupported);
    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1DeviceClass);
    LLDP_RED_GET_2_BYTE (pRmMsg, *pu4Offset, u2CapEnable);

    pRemoteNode->RemMedCapableInfo.u2MedRemCapSupported = u2CapSupported;
    pRemoteNode->RemMedCapableInfo.u1MedRemDeviceClass = u1DeviceClass;
    pRemoteNode->RemMedCapableInfo.u2MedRemCapEnable = u2CapEnable;

    LLDP_TRC_ARG3 (LLDP_RED_TRC, "LldpRedGetMedTlvInfoBulkUpdMsg: "
                   "CapSupported %d, CapEnable = %d, DeviceClass = %d Func exit.\n",
                   u2CapSupported, u2CapEnable, u1DeviceClass);
    return;
}

/*************************************************************************
 * FUNCTION NAME    : LldpRedAddInvTlvInfoBulkUpdMsg 
 *
 * DESCRIPTION      : This function is used to construct Inventory TLV 
 *                    information to be sent in Bulk update message.
 *
 * INPUT            : pRmMsg      - pointer to message buffer
 *                    pu4Offset   - offset
 *                    pRemoteNode - pointer to remote node 
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : NONE 
 *************************************************************************/
VOID
LldpRedAddInvTlvInfoBulkUpdMsg (tRmMsg * pRmMsg, UINT4 *pu4Offset,
                                tLldpRemoteNode * pRemoteNode)
{
    UINT1               u1Length = 0;

    LLDP_TRC (LLDP_RED_TRC, "LldpRedAddInvTlvInfoBulkUpdMsg: Func Entry \n");

    u1Length = LLDP_STRLEN (pRemoteNode->RemInventoryInfo.au1MedRemHardwareRev,
                            LLDP_MED_HW_REV_LEN);

    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1Length);
    LLDP_RED_PUT_N_BYTE (pRmMsg,
                         &(pRemoteNode->RemInventoryInfo.
                           au1MedRemHardwareRev[0]), *pu4Offset,
                         (UINT4) u1Length);

    LLDP_TRC_ARG1 (LLDP_RED_TRC, "Remote Hardware Revision: %s\n",
                   pRemoteNode->RemInventoryInfo.au1MedRemHardwareRev);

    u1Length = LLDP_STRLEN (pRemoteNode->RemInventoryInfo.au1MedRemSoftwareRev,
                            LLDP_MED_SW_REV_LEN);
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1Length);
    LLDP_RED_PUT_N_BYTE (pRmMsg,
                         &(pRemoteNode->RemInventoryInfo.
                           au1MedRemSoftwareRev[0]), *pu4Offset,
                         (UINT4) u1Length);

    LLDP_TRC_ARG1 (LLDP_RED_TRC, "Remote Software Revision: %s\n",
                   pRemoteNode->RemInventoryInfo.au1MedRemSoftwareRev);

    u1Length = LLDP_STRLEN (pRemoteNode->RemInventoryInfo.au1MedRemFirmwareRev,
                            LLDP_MED_FW_REV_LEN);
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1Length);
    LLDP_RED_PUT_N_BYTE (pRmMsg,
                         &(pRemoteNode->RemInventoryInfo.
                           au1MedRemFirmwareRev[0]), *pu4Offset,
                         (UINT4) u1Length);

    LLDP_TRC_ARG1 (LLDP_RED_TRC, "Remote Firmware Revision: %s\n",
                   pRemoteNode->RemInventoryInfo.au1MedRemFirmwareRev);

    u1Length = LLDP_STRLEN (pRemoteNode->RemInventoryInfo.au1MedRemSerialNum,
                            LLDP_MED_SER_NUM_LEN);
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1Length);
    LLDP_RED_PUT_N_BYTE (pRmMsg,
                         &(pRemoteNode->RemInventoryInfo.au1MedRemSerialNum[0]),
                         *pu4Offset, (UINT4) u1Length);

    LLDP_TRC_ARG1 (LLDP_RED_TRC, "Remote Serial Number: %s\n",
                   pRemoteNode->RemInventoryInfo.au1MedRemSerialNum);

    u1Length = LLDP_STRLEN (pRemoteNode->RemInventoryInfo.au1MedRemMfgName,
                            LLDP_MED_MFG_NAME_LEN);
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1Length);
    LLDP_RED_PUT_N_BYTE (pRmMsg,
                         &(pRemoteNode->RemInventoryInfo.au1MedRemMfgName[0]),
                         *pu4Offset, (UINT4) u1Length);

    LLDP_TRC_ARG1 (LLDP_RED_TRC, "Remote Mfg Name: %s\n",
                   pRemoteNode->RemInventoryInfo.au1MedRemMfgName);

    u1Length = LLDP_STRLEN (pRemoteNode->RemInventoryInfo.au1MedRemModelName,
                            LLDP_MED_MODEL_NAME_LEN);
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1Length);
    LLDP_RED_PUT_N_BYTE (pRmMsg,
                         &(pRemoteNode->RemInventoryInfo.au1MedRemModelName[0]),
                         *pu4Offset, (UINT4) u1Length);

    LLDP_TRC_ARG1 (LLDP_RED_TRC, "Remote Model Name: %s\n",
                   pRemoteNode->RemInventoryInfo.au1MedRemModelName);

    u1Length = LLDP_STRLEN (pRemoteNode->RemInventoryInfo.au1MedRemAssetId,
                            LLDP_MED_ASSET_ID_LEN);
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1Length);
    LLDP_RED_PUT_N_BYTE (pRmMsg,
                         &(pRemoteNode->RemInventoryInfo.au1MedRemAssetId[0]),
                         *pu4Offset, (UINT4) u1Length);

    LLDP_TRC_ARG1 (LLDP_RED_TRC, "Remote AssetId: %s\n",
                   pRemoteNode->RemInventoryInfo.au1MedRemAssetId);

    LLDP_TRC (LLDP_RED_TRC, "LldpRedAddInvTlvInfoBulkUpdMsg: Func Exit \n");
    return;
}

/*************************************************************************
 * FUNCTION NAME    : LldpRedGetInvTlvInfoBulkUpMsg
 *
 * DESCRIPTION      : This function is used to extract the inventory TLV 
 *                    information from the bulk update message
 *
 * INPUT            : pRmMsg      - pointer to message buffer 
 *                    pu4Offset   - offset
 *                    pRemoteNode - pointer to remote node 
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : NONE 
 *************************************************************************/
VOID
LldpRedGetInvTlvInfoBulkUpMsg (tRmMsg * pRmMsg, UINT4 *pu4Offset,
                               tLldpRemoteNode * pRemoteNode)
{

    UINT1               u1Length = 0;

    LLDP_TRC (LLDP_RED_TRC, "LldpRedGetInvTlvInfoBulkUpMsg: Func Entry\n");

    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1Length);
    LLDP_RED_GET_N_BYTE (pRmMsg,
                         &(pRemoteNode->RemInventoryInfo.
                           au1MedRemHardwareRev[0]), *pu4Offset,
                         (UINT4) u1Length);

    LLDP_TRC_ARG1 (LLDP_RED_TRC, "Remote Hardware Revision: %s\n",
                   pRemoteNode->RemInventoryInfo.au1MedRemHardwareRev);

    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1Length);
    LLDP_RED_GET_N_BYTE (pRmMsg,
                         &(pRemoteNode->RemInventoryInfo.
                           au1MedRemSoftwareRev[0]), *pu4Offset,
                         (UINT4) u1Length);

    LLDP_TRC_ARG1 (LLDP_RED_TRC, "Remote Software Revision: %s\n",
                   pRemoteNode->RemInventoryInfo.au1MedRemSoftwareRev);

    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1Length);
    LLDP_RED_GET_N_BYTE (pRmMsg,
                         &(pRemoteNode->RemInventoryInfo.
                           au1MedRemFirmwareRev[0]), *pu4Offset,
                         (UINT4) u1Length);

    LLDP_TRC_ARG1 (LLDP_RED_TRC, "Remote Firmware Revision: %s\n",
                   pRemoteNode->RemInventoryInfo.au1MedRemFirmwareRev);

    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1Length);
    LLDP_RED_GET_N_BYTE (pRmMsg,
                         &(pRemoteNode->RemInventoryInfo.au1MedRemSerialNum[0]),
                         *pu4Offset, (UINT4) u1Length);

    LLDP_TRC_ARG1 (LLDP_RED_TRC, "Remote Serial Number: %s\n",
                   pRemoteNode->RemInventoryInfo.au1MedRemSerialNum);

    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1Length);
    LLDP_RED_GET_N_BYTE (pRmMsg,
                         &(pRemoteNode->RemInventoryInfo.au1MedRemMfgName[0]),
                         *pu4Offset, (UINT4) u1Length);

    LLDP_TRC_ARG1 (LLDP_RED_TRC, "Remote Mfg Name: %s\n",
                   pRemoteNode->RemInventoryInfo.au1MedRemMfgName);

    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1Length);
    LLDP_RED_GET_N_BYTE (pRmMsg,
                         &(pRemoteNode->RemInventoryInfo.au1MedRemModelName[0]),
                         *pu4Offset, (UINT4) u1Length);

    LLDP_TRC_ARG1 (LLDP_RED_TRC, "Remote Model Name: %s\n",
                   pRemoteNode->RemInventoryInfo.au1MedRemModelName);

    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1Length);
    LLDP_RED_GET_N_BYTE (pRmMsg,
                         &(pRemoteNode->RemInventoryInfo.au1MedRemAssetId[0]),
                         *pu4Offset, (UINT4) u1Length);

    LLDP_TRC_ARG1 (LLDP_RED_TRC, "Remote AssetId: %s\n",
                   pRemoteNode->RemInventoryInfo.au1MedRemAssetId);

    LLDP_TRC (LLDP_RED_TRC, "LldpRedGetInvTlvInfoBulkUpMsg: Func Exit\n");
    return;
}

/****************************************************************************
 *  FUNCTION NAME    : LldpRedSendMedLocationInfo 
 *
 *  DESCRIPTION      : This function retreives MED Location remote 
 *                     node information and sends bulk update message to RM.
 *
 *  INPUT            : NONE
 *
 *  OUTPUT           : NONE
 *
 *  RETURNS          : NONE 
 *
 ****************************************************************************/
VOID
LldpRedSendMedLocationInfo ()
{
    tLldpMedRemLocationInfo *pMedLocRemNode = NULL;
    tLldpMedRemLocationInfo *pMedNextLocRemNode = NULL;
    tRmMsg             *pRmMsgBuf = NULL;
    UINT4               u4Offset = 0;
    UINT4               u4MsgLenOffset = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2BulkUpdCount = 0;
    UINT2               u2MaxBulkUpdSize = 0;
    UINT1               u1MsgType = 0;
    UINT1               u1BulkUpdType = 0;
    UINT1               u1TlvCount = 0;
    UINT4               u4TlvCountOffset = 0;

    LLDP_TRC (LLDP_RED_TRC, "LldpRedSendMedLocationInfo: Send bulk "
              "update message for MED Location info to RM.\n");

    /* Copy next node to local pointer and process further. */
    pMedNextLocRemNode = LLDP_RED_NEXT_MED_REM_LOC_NODE ();

    /* Verify if next remote node is available, if not get the first node
     * and send the bulk update message */
    if (pMedNextLocRemNode == NULL)
    {
        pMedNextLocRemNode = (tLldpMedRemLocationInfo *)
            RBTreeGetFirst (gLldpGlobalInfo.LldpMedRemLocationInfoRBTree);

        if (pMedNextLocRemNode == NULL)
        {
            LLDP_TRC (LLDP_RED_TRC, "LldpRedSendMedLocationInfo: No nodes - "
                      "Completed Bulk update Request Handling\n");
            LLDP_RED_MED_LOCATION_BULK_UPD_STATUS () = OSIX_TRUE;
            return;
        }
    }

    u1BulkUpdType = LLDP_RED_MED_LOCATION_BULK_UPD_MSG;
    u2BulkUpdCount = LLDP_RED_MED_LOC_REM_NODE_CNT_PER_BULK_UPD;
    u2MaxBulkUpdSize = LLDP_RED_MAX_BULK_UPD_SIZE;
    u1MsgType = RM_BULK_UPDATE_MSG;

    if (LldpRedAllocMemForRmMsg (u1MsgType, u2MaxBulkUpdSize,
                                 &pRmMsgBuf) != OSIX_SUCCESS)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_RED_TRC,
                  "LldpRedSendMedLocationInfo: Failed to allocate memory for "
                  "RM message.\n");
        return;
    }

    /* Fill the Message Header to be sent to RM module */
    LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, u1MsgType);

    /* Store the offset, such that actual length will be filled once
     * all the required TLV information is filled in the while loop. */
    u4MsgLenOffset = u4Offset;
    LLDP_RED_PUT_2_BYTE (pRmMsgBuf, u4Offset, u2MsgLen);

    /* Type LLDP_RED_MED_LOCATION_BULK_UPD_MSG */
    LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, u1BulkUpdType);

    /* Store the offset, to fill the TLV count at the end of while loop */
    u4TlvCountOffset = u4Offset;
    LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, u1TlvCount);

    while ((pMedNextLocRemNode != NULL) && (u2BulkUpdCount > 0))
    {
        /* Store the current node, which is used to get the next node */
        pMedLocRemNode = pMedNextLocRemNode;

        if ((u2MaxBulkUpdSize - u4Offset) < LLDP_MED_LOCATION_ID_TLV_LEN)
        {
            u2MsgLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pRmMsgBuf);

            /* Fill the actual message length */
            LLDP_RED_PUT_2_BYTE (pRmMsgBuf, u4MsgLenOffset, u2MsgLen);

            /* Send Message to RM */
            LldpRedSendMsgToRm (u1MsgType, u2MsgLen, pRmMsgBuf);

            u4Offset = 0;

            if (LldpRedAllocMemForRmMsg (u1MsgType, u2MaxBulkUpdSize,
                                         &pRmMsgBuf) != OSIX_SUCCESS)
            {
                LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_RED_TRC,
                          "LldpRedSendMedLocationInfo: Failed to allocate memory"
                          "for RM message.\n");
                return;
            }

            LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, u1MsgType);
            u4MsgLenOffset = u4Offset;
            LLDP_RED_PUT_2_BYTE (pRmMsgBuf, u4Offset, u2MsgLen);
            LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, u1BulkUpdType);
        }

        /* Construct the MED Location remote node information 
         * within the bulk update message */
        LldpRedFormMedLocBulkUpdMsg (pRmMsgBuf, &u4Offset, pMedLocRemNode);

        /* Increment the TLV count */
        u1TlvCount++;

        /* Reset the next pointer to null and get the next remote node */
        pMedNextLocRemNode = NULL;
        pMedNextLocRemNode = (tLldpMedRemLocationInfo *) RBTreeGetNext
            (gLldpGlobalInfo.LldpMedRemLocationInfoRBTree,
             (tRBElem *) pMedLocRemNode, NULL);

        /* Decrement the no of BulkUpd messages count to be sent */
        u2BulkUpdCount--;
    }

    u2MsgLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pRmMsgBuf);
    LLDP_RED_PUT_2_BYTE (pRmMsgBuf, u4MsgLenOffset, u2MsgLen);

    LLDP_TRC_ARG3 (LLDP_RED_TRC, "%s: Message Length :%d TLV count: %d\n",
                   __FUNCTION__, u2MsgLen, (UINT4) u1TlvCount);

    /* Fill the TLV count at the offset we stored earlier. Using this
     * count, we parse the no of TLVs. */
    LLDP_RED_PUT_1_BYTE (pRmMsgBuf, u4TlvCountOffset, u1TlvCount);

    /* Send the information to RM module */
    LldpRedSendMsgToRm (u1MsgType, u2MsgLen, pRmMsgBuf);

    /* Store the next remote node in the global structure variable
     * (gLldpRedGlobalInfo.pNextMedRemLocationnode) */
    LLDP_RED_NEXT_MED_REM_LOC_NODE () = pMedNextLocRemNode;

    if (LLDP_RED_NEXT_MED_REM_LOC_NODE () == NULL)
    {
        LLDP_RED_MED_LOCATION_BULK_UPD_STATUS () = OSIX_TRUE;
        LLDP_TRC (LLDP_RED_TRC, "LldpRedSendMedLocationInfo: Completed Bulk "
                  "Request Handling!!!\n");
    }
    else
    {

        LLDP_TRC (LLDP_RED_TRC, "LldpRedSendMedLocationInfo: Sending an event "
                  "to start the next sub bulk update !!!\n");
        /* Send an event to start the next sub bulk update for the pending 
         * information */
        LldpRedTrigNextBulkUpd ();
    }

    LLDP_TRC (LLDP_RED_TRC, "LldpRedSendMedLocationInfo: Bulk "
              "update message for MED Location Information sent to RM. Exiting\n");
    return;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpRedFormMedLocBulkUpdMsg
 *
 *    DESCRIPTION      : This function constructs the MED Location TLV remote 
 *                       node information bulk update message by retreiving
 *                       information present in the given node
 *
 *    INPUT            : pRmMsg      - pointer to RM message buffer
 *                       pu4Offset   - pointer to the next location from where
 *                                     information needs to be filled
 *                       pMedLocRemNode - pointer to remote node
 *
 *    OUTPUT           : pRmMsg, pu4Offset
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
VOID
LldpRedFormMedLocBulkUpdMsg (tRmMsg * pRmMsg, UINT4 *pu4Offset,
                             tLldpMedRemLocationInfo * pMedLocRemNode)
{
    UINT1               u1Length = 0;

    LLDP_TRC (LLDP_RED_TRC, "LldpRedFormMedLocBulkUpdMsg: Fill"
              "the RM message.\n");
    /* Last updated time */
    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         pMedLocRemNode->u4RemLastUpdateTime);
    /* Remote Local port number */
    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         (UINT4) pMedLocRemNode->i4RemLocalPortNum);
    /* Remote Index */
    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         (UINT4) pMedLocRemNode->i4RemIndex);
    /* Destination Address Table Index */
    LLDP_RED_PUT_4_BYTE (pRmMsg, *pu4Offset,
                         pMedLocRemNode->u4RemDestAddrTblIndex);

    /* Attributes for Location Identification Information field
       -------------------------------
       | Location Data | Location ID |
       |  Format       |             | 
       ------------------------------
       1 Octet          Variable size       
     */
    /* Location Data format */
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset,
                         pMedLocRemNode->u1RemLocationSubType);
    /* Location ID information */
    if (pMedLocRemNode->u1RemLocationSubType == LLDP_MED_COORDINATE_LOC)
    {
        u1Length = LLDP_STRLEN (pMedLocRemNode->au1RemLocationID,
                                LLDP_MED_MAX_COORDINATE_LOC_LENGTH);
    }
    else if (pMedLocRemNode->u1RemLocationSubType == LLDP_MED_CIVIC_LOC)
    {
        u1Length = LLDP_STRLEN (pMedLocRemNode->au1RemLocationID,
                                LLDP_MED_MAX_LOC_LENGTH);
    }
    else if (pMedLocRemNode->u1RemLocationSubType == LLDP_MED_ELIN_LOC)
    {
        u1Length = LLDP_STRLEN (pMedLocRemNode->au1RemLocationID,
                                LLDP_MED_MAX_ELIN_LOC_LENGTH);
    }
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1Length);
    LLDP_RED_PUT_N_BYTE (pRmMsg, &(pMedLocRemNode->au1RemLocationID[0]),
                         *pu4Offset, u1Length);

    LLDP_TRC_ARG5 (LLDP_RED_TRC,
                   "LldpRedFormMedLocBulkUpdMsg: "
                   "RemLocalPortNum[%d]\nRemIndex[%d]\nRemLastUpdateTime[%d]\n"
                   "DestAddrTblIndex[%d]\nLocationDataFormat[%d]\r\n",
                   pMedLocRemNode->i4RemLocalPortNum,
                   pMedLocRemNode->i4RemIndex,
                   pMedLocRemNode->u4RemLastUpdateTime,
                   pMedLocRemNode->u4RemDestAddrTblIndex,
                   pMedLocRemNode->u1RemLocationSubType);

    LLDP_TRC (LLDP_RED_TRC, "LldpRedFormMedLocBulkUpdMsg: Completed"
              "forming the RM message.\n");
    return;
}

/****************************************************************************
 *    FUNCTION NAME    : LldpRedProcMedLocationRemNodeBulkUpd 
 *
 *    DESCRIPTION      : This function processes the MED Location remote 
 *                       node information present in the RM message buffer.
 *
 *    INPUT            : pRmMsg - pointer to RM message buffer
 *
 *    OUTPUT           : NONE 
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
LldpRedProcMedLocationRemNodeBulkUpd (tRmMsg * pRmMsg)
{
    tLldpMedRemLocationInfo *pMedLocRemNode = NULL;
    UINT1               u1TlvCount = 0;
    UINT4               u4Offset = 0;

    LLDP_TRC (LLDP_RED_TRC, "LldpRedProcMedLocationRemNodeBulkUpd: "
              "Received MED Location Remote " "Node Bulk Update\n");

    LLDP_RED_GET_1_BYTE (pRmMsg, u4Offset, u1TlvCount);
    LLDP_TRC_ARG1 (LLDP_RED_TRC, "Received TLV count %d\n", u1TlvCount);

    while (u1TlvCount > 0)
    {
        /* Create Memory for Remote Node */
        if ((pMedLocRemNode = (tLldpMedRemLocationInfo *)
             (MemAllocMemBlk (gLldpGlobalInfo.LldpMedRemLocationPoolId))) ==
            NULL)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_RED_TRC,
                      "LldpRedProcMedLocationRemNodeBulkUpd: Failed to Allocate Memory "
                      "for remote node \r\n");
#ifndef FM_WANTED
            SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gLldpGlobalInfo.u4SysLogId,
                          "LldpRedProcMedLocationRemNodeBulkUpd: Failed to Allocate Memory"
                          "for remote node"));
#endif
            return OSIX_FAILURE;
        }

        MEMSET (pMedLocRemNode, 0, sizeof (tLldpMedRemLocationInfo));

        /* Fill the Remote node with the required information */
        LldpRedGetMedLocationRemNodeInfo (pMedLocRemNode, &u4Offset, pRmMsg);

        LLDP_TRC_ARG5 (LLDP_RED_TRC,
                       "Add remote node to RBTree -\nRemLocalPortNum[%d]\n"
                       "i4RemIndex[%d]\nRemLastUpdateTime[%d]\n"
                       "DestAddrTblIndex[%d]\nLocationSubType[%d]\r\n",
                       pMedLocRemNode->i4RemLocalPortNum,
                       pMedLocRemNode->i4RemIndex,
                       pMedLocRemNode->u4RemLastUpdateTime,
                       pMedLocRemNode->u4RemDestAddrTblIndex,
                       pMedLocRemNode->u1RemLocationSubType);

        /* Insert the Node in LldpMedRemLocationInfoRBTree */
        if (RBTreeAdd (gLldpGlobalInfo.LldpMedRemLocationInfoRBTree,
                       (tRBElem *) pMedLocRemNode) != RB_SUCCESS)
        {
            LLDP_TRC (LLDP_CRITICAL_TRC | LLDP_RED_TRC,
                      "LldpRedProcMedLocationRemNodeBulkUpd: Failed to add remote "
                      "node to RBTree\r\n");
            MemReleaseMemBlock (gLldpGlobalInfo.LldpMedRemLocationPoolId,
                                (UINT1 *) pMedLocRemNode);
            pMedLocRemNode = NULL;
        }

        u1TlvCount--;
        LLDP_TRC_ARG1 (LLDP_RED_TRC, "Tlv count %d\n", u1TlvCount);
    }

    LLDP_TRC (LLDP_RED_TRC,
              "LldpRedProcMedLocationRemNodeBulkUpd: Successfully "
              "processed MED Location Identification Remote Node Bulk Update.\n");

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME    : LldpRedGetMedLocationRemNodeInfo 
 *
 * DESCRIPTION      : This function retreives the remote node information
 *                    present in RM message buffer and stores it in the
 *                    given remote node pointer
 *
 * INPUT            : pRmMsg - pointer to RM message buffer
 *
 * OUTPUT           : pMedLocRemNode - pointer to remote node information
 * 
 * RETURNS          : NONE
 ****************************************************************************/
VOID
LldpRedGetMedLocationRemNodeInfo (tLldpMedRemLocationInfo * pMedLocRemNode,
                                  UINT4 *pu4Offset, tRmMsg * pRmMsg)
{
    UINT4               u4RemDestAddrTblIndex = 0;
    UINT4               u4RemLocalPortNo = 0;
    UINT1               u1Length = 0;
    UINT1               u1LocData = 0;

    LLDP_TRC (LLDP_RED_TRC, "LldpRedGetMedLocationRemNodeInfo: Get the MED "
              "Network policy remote node information "
              "from Bulk update message\n");

    /* Last Update time */
    LLDP_RED_GET_4_BYTE (pRmMsg, *pu4Offset,
                         pMedLocRemNode->u4RemLastUpdateTime);

    /* Remote local port number */
    LLDP_RED_GET_4_BYTE (pRmMsg, *pu4Offset, u4RemLocalPortNo);
    pMedLocRemNode->i4RemLocalPortNum = (INT4) u4RemLocalPortNo;

    /* Remote Index */
    LLDP_RED_GET_4_BYTE (pRmMsg, *pu4Offset, pMedLocRemNode->i4RemIndex);

    /* Destination Address table Index */
    LLDP_RED_GET_4_BYTE (pRmMsg, *pu4Offset, u4RemDestAddrTblIndex);
    pMedLocRemNode->u4RemDestAddrTblIndex = u4RemDestAddrTblIndex;

    /* Decode the attributes from Location Identification Information field
       --------------------------------------
       | Location Data Format | Location Id |
       --------------------------------------
       1 Octet                 Variable size
     */

    /* Get 1st Octet for Location Data format */
    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1LocData);
    pMedLocRemNode->u1RemLocationSubType = u1LocData;

    /* Get Location Identification Value */
    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1Length);
    LLDP_RED_GET_N_BYTE (pRmMsg, &(pMedLocRemNode->au1RemLocationID[0]),
                         *pu4Offset, (UINT4) u1Length);

    LLDP_TRC_ARG5 (LLDP_RED_TRC,
                   "Received TLV info -\nRemLocalPortNum[%d]\n"
                   "i4RemIndex[%d]\nRemLastUpdateTime[%d]\n"
                   "DestAddrTblIndex[%d]\nLocationSubType[%d]\r\n",
                   pMedLocRemNode->i4RemLocalPortNum,
                   pMedLocRemNode->i4RemIndex,
                   pMedLocRemNode->u4RemLastUpdateTime,
                   pMedLocRemNode->u4RemDestAddrTblIndex,
                   pMedLocRemNode->u1RemLocationSubType);

    LLDP_TRC (LLDP_RED_TRC, "LldpRedGetMedLocationRemNodeInfo: Remote node "
              "information fetched exiting.\n");
    return;
}

/*************************************************************************
 * FUNCTION NAME    : LldpRedUpdNxtMedLocationBlkUpdPtr 
 *
 * DESCRIPTION      : This function is used to update the next MED location 
 *                    identification node whenever a neighbor information is deleted
 *
 * INPUT            : pRemoteNode - pointer to remote node 
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : NONE 
 *************************************************************************/
VOID
LldpRedUpdNxtMedLocationBlkUpdPtr (tLldpRemoteNode * pRemoteNode)
{
    tLldpMedRemLocationInfo *pMedLocRemNode = NULL;
    tLldpMedRemLocationInfo *pNextMedLocRemNode = NULL;

    LLDP_TRC (LLDP_RED_TRC, "LldpRedUpdNxtMedLocationBlkUpdPtr: "
              "Function entry.\n");

    pMedLocRemNode = LLDP_RED_NEXT_MED_REM_LOC_NODE ();

    if (pMedLocRemNode == NULL)
    {
        LLDP_TRC (LLDP_RED_TRC, "LldpRedUpdNxtMedLocationBlkUpdPtr: "
                  "No node present.\n");
        return;
    }

    while ((LLDP_IS_NODE_FOR_NEIGHBOR (pRemoteNode, pMedLocRemNode) ==
            OSIX_TRUE) && (pRemoteNode->u4DestAddrTblIndex
                           == pMedLocRemNode->u4RemDestAddrTblIndex))
    {
        /* Getting Next Index of Remote Location Table */
        pNextMedLocRemNode = (tLldpMedRemLocationInfo *)
            RBTreeGetNext (gLldpGlobalInfo.LldpMedRemLocationInfoRBTree,
                           (tRBElem *) pMedLocRemNode, NULL);

        if (pNextMedLocRemNode == NULL)
        {
            LLDP_RED_NEXT_MED_REM_LOC_NODE () = NULL;
            LLDP_RED_MED_LOCATION_BULK_UPD_STATUS () = OSIX_TRUE;
            return;
        }
        pMedLocRemNode = pNextMedLocRemNode;
    }
    LLDP_RED_NEXT_MED_REM_LOC_NODE () = pNextMedLocRemNode;

    LLDP_TRC (LLDP_RED_TRC, "LldpRedUpdNxtMedLocationBlkUpdPtr: "
              "Function exit.\n");
    return;
}

/*************************************************************************
 * FUNCTION NAME    : LldpRedAddPowerTlvInfoBulkUpdMsg 
 *
 * DESCRIPTION      : This function is used to constrcut Power Tlv 
 *                    information to be send in bulk update message
 *
 * INPUT            : pRmMsg      - pointer to message buffer
 *                    pu4Offset   - offset
 *                    pRemoteNode - pointer to remote node 
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : NONE 
 *************************************************************************/
VOID
LldpRedAddPowerTlvInfoBulkUpdMsg (tRmMsg * pRmMsg, UINT4 *pu4Offset,
                                  tLldpRemoteNode * pRemoteNode)
{
    UINT1               u1DeviceType =
        pRemoteNode->RemMedPowerMDIInfo.u1PoEDeviceType;
    UINT1               u1PowerSource =
        pRemoteNode->RemMedPowerMDIInfo.u1PowerSource;
    UINT1               u1PowerPriority =
        pRemoteNode->RemMedPowerMDIInfo.u1PowerPriority;
    UINT2               u2PowerValue =
        pRemoteNode->RemMedPowerMDIInfo.u2PowerValue;

    LLDP_TRC_ARG3 (LLDP_RED_TRC, "LldpRedAddPowerTlvInfoBulkUpdMsg: "
                   "DeviceType = %d, PowerSource = %d, PowerPriority = %d\n",
                   u1DeviceType, u1PowerSource, u1PowerPriority);
    LLDP_TRC_ARG1 (LLDP_RED_TRC, "LldpRedAddPowerTlvInfoBulkUpdMsg: "
                   "Power Value = %d\n", u2PowerValue);

    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1DeviceType);
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1PowerSource);
    LLDP_RED_PUT_1_BYTE (pRmMsg, *pu4Offset, u1PowerPriority);
    LLDP_RED_PUT_2_BYTE (pRmMsg, *pu4Offset, u2PowerValue);

    LLDP_TRC (LLDP_RED_TRC, "LldpRedAddPowerTlvInfoBulkUpdMsg: "
              "Function exit.\n");
    return;
}

/*************************************************************************
 * FUNCTION NAME    : LldpRedGetPowerTlvInfoBulkUpdMsg 
 *
 * DESCRIPTION      : This function is used to extract Extended Power Tlv 
 *                    information from bulk update message
 *
 * INPUT            : pRmMsg      - pointer to message buffer
 *                    pu4Offset   - offset
 *                    pRemoteNode - pointer to remote node 
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : NONE 
 *************************************************************************/
VOID
LldpRedGetPowerTlvInfoBulkUpdMsg (tRmMsg * pRmMsg, UINT4 *pu4Offset,
                                  tLldpRemoteNode * pRemoteNode)
{
    UINT1               u1DeviceType = 0;
    UINT1               u1PowerSource = 0;
    UINT1               u1PowerPriority = 0;
    UINT2               u2PowerValue = 0;

    LLDP_TRC (LLDP_RED_TRC, "LldpRedGetPowerTlvInfoBulkUpdMsg: Func Entry \n");

    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1DeviceType);
    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1PowerSource);
    LLDP_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1PowerPriority);
    LLDP_RED_GET_2_BYTE (pRmMsg, *pu4Offset, u2PowerValue);

    pRemoteNode->RemMedPowerMDIInfo.u1PoEDeviceType = u1DeviceType;
    pRemoteNode->RemMedPowerMDIInfo.u1PowerSource = u1PowerSource;
    pRemoteNode->RemMedPowerMDIInfo.u1PowerPriority = u1PowerPriority;
    pRemoteNode->RemMedPowerMDIInfo.u2PowerValue = u2PowerValue;

    LLDP_TRC_ARG3 (LLDP_RED_TRC, "LldpRedGetPowerTlvInfoBulkUpdMsg: "
                   "DeviceType = %d, PowerSource = %d, PowerPriority = %d\n",
                   u1DeviceType, u1PowerSource, u1PowerPriority);
    LLDP_TRC_ARG1 (LLDP_RED_TRC, "LldpRedGetPowerTlvInfoBulkUpdMsg: "
                   "Power Value = %d\n", u2PowerValue);
    return;
}
#endif /* _LLDRED_C_ */
/*---------------------------------------------------------------------------*/
/*                        End of the file  <lldred.c>                        */
/*---------------------------------------------------------------------------*/
