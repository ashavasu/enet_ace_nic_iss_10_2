/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldtmr.c,v 1.33 2017/12/16 11:48:18 siva Exp $
 *
 * Description: This file contains LLDP timer related functions.  
 *********************************************************************/
#include "lldinc.h"
/* Proto types of the functions private to this file only */
PRIVATE VOID LldpTmrInitTmrDesc PROTO ((VOID));
PRIVATE VOID LldpTmrTxDelayTmrExp PROTO ((VOID *pArg));
PRIVATE VOID LldpTmrTtrTmrExp PROTO ((VOID *pArg));
PRIVATE VOID LldpTmrShutWhileTmrExp PROTO ((VOID *pArg));
PRIVATE VOID LldpTmrRxInfoAgeTmrExp PROTO ((VOID *pArg));
PRIVATE VOID LldpTmrNotifIntervalTmrExp PROTO ((VOID *pArg));
PRIVATE VOID LldpTmrGlobalShutWhileTmrExp PROTO ((VOID *pArg));

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpTmrInit 
 *                                                                          
 *    DESCRIPTION      : This function creates a timer list for all the timers
 *                       in LLDP module.  
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
LldpTmrInit (VOID)
{
    if (TmrCreateTimerList (LLDP_TASK_NAME, LLDP_TMR_EXPIRY_EVENT, NULL,
                            &(gLldpGlobalInfo.TmrListId)) == TMR_FAILURE)
    {
        LLDP_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC | CONTROL_PLANE_TRC),
                  "LldpTmrInit: Timer list creation FAILED !!!\r\n");
        return OSIX_FAILURE;
    }

    LldpTmrInitTmrDesc ();

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpTmrInitTmrDesc 
 *                                                                          
 *    DESCRIPTION      : This function intializes the timer desc for all 
 *                       the timers in LLDP module.  
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
LldpTmrInitTmrDesc (VOID)
{
    gLldpGlobalInfo.aTmrDesc[LLDP_TX_TMR_SHUT_WHILE].i2Offset =
        (INT2) LLDP_OFFSET (tLldpLocPortInfo, ShutWhileTmr);
    gLldpGlobalInfo.aTmrDesc[LLDP_TX_TMR_SHUT_WHILE].TmrExpFn =
        LldpTmrShutWhileTmrExp;

    gLldpGlobalInfo.aTmrDesc[LLDP_TX_TMR_TX_DELAY].i2Offset =
        (INT2) LLDP_OFFSET (tLldpLocPortInfo, TxDelayTmr);
    gLldpGlobalInfo.aTmrDesc[LLDP_TX_TMR_TX_DELAY].TmrExpFn =
        LldpTmrTxDelayTmrExp;

    gLldpGlobalInfo.aTmrDesc[LLDP_TX_TMR_TTR].i2Offset =
        (INT2) LLDP_OFFSET (tLldpLocPortInfo, TtrTmr);
    gLldpGlobalInfo.aTmrDesc[LLDP_TX_TMR_TTR].TmrExpFn = LldpTmrTtrTmrExp;

    gLldpGlobalInfo.aTmrDesc[LLDP_RX_TMR_RX_INFO_TTL].i2Offset =
        (INT2) LLDP_OFFSET (tLldpRemoteNode, RxInfoAgeTmr);
    gLldpGlobalInfo.aTmrDesc[LLDP_RX_TMR_RX_INFO_TTL].TmrExpFn =
        LldpTmrRxInfoAgeTmrExp;

    gLldpGlobalInfo.aTmrDesc[LLDP_NOTIF_INT_TIMER].i2Offset =
        (INT2) LLDP_INVALID_VALUE;
    gLldpGlobalInfo.aTmrDesc[LLDP_NOTIF_INT_TIMER].TmrExpFn =
        LldpTmrNotifIntervalTmrExp;

    gLldpGlobalInfo.aTmrDesc[LLDP_TX_TMR_GLOBAL_SHUT_WHILE].i2Offset =
        (INT2) LLDP_INVALID_VALUE;
    gLldpGlobalInfo.aTmrDesc[LLDP_TX_TMR_GLOBAL_SHUT_WHILE].TmrExpFn =
        LldpTmrGlobalShutWhileTmrExp;

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpTmrDeInit 
 *                                                                          
 *    DESCRIPTION      : This function de-initializes the timer list.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
LldpTmrDeInit (VOID)
{
    if (TmrDeleteTimerList (gLldpGlobalInfo.TmrListId) == TMR_FAILURE)
    {
        LLDP_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC | CONTROL_PLANE_TRC),
                  "LldpTmrDeInit: Timer list deletion FAILED !!!\r\n");
        return OSIX_FAILURE;
    }

    LLDP_TRC ((INIT_SHUT_TRC | CONTROL_PLANE_TRC),
              "LldpTmrDeInit: Timer list deletion successful !!!\r\n");

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpTmrExpHandler 
 *                                                                          
 *    DESCRIPTION      : This function is called whenever a timer expiry 
 *                       message is received by LLDP task. Different timer
 *                       expiry handlers are called based on the timer type. 
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
LldpTmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;
    INT2                i2Offset;
    /* HITLESS RESTART */
    UINT1               u1HRTmrFlag = 0;

    /* Timer expiry event is handled even if LLDP module is disabled.
     * Because RxInfoAgeTmr timer is allowed to run even if the module is
     * disabled, so that the remote entries will age out if no refresh frame 
     * is received within the recevied TTL period. Hence Timer (RxInfoAgeTmr 
     * timer) expiry event is handled even if LLDP module is disabled */

    LLDP_TRC (CONTROL_PLANE_TRC, "LldpTimerExpHandler: Timer expiry to be"
              " handled\r\n");

    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gLldpGlobalInfo.TmrListId)) != NULL)
    {
        LLDP_TRC_ARG1 (CONTROL_PLANE_TRC, "Timer to be processed %x\r\n",
                       pExpiredTimers);

        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;
        if (u1TimerId < LLDP_MAX_TIMER_TYPES)
        {
            i2Offset = gLldpGlobalInfo.aTmrDesc[u1TimerId].i2Offset;

            LLDP_TRC_ARG2 (CONTROL_PLANE_TRC, "TimerId: %d\tOffset: %d\r\n",
                           u1TimerId, i2Offset);

            if (i2Offset == LLDP_INVALID_VALUE)
            {
                /* The timer function does not take any parameter. */
                (*(gLldpGlobalInfo.aTmrDesc[u1TimerId].TmrExpFn)) (NULL);
            }
            else
            {
                (*(gLldpGlobalInfo.aTmrDesc[u1TimerId].TmrExpFn))
                    ((UINT1 *) pExpiredTimers - i2Offset);
            }

            /* HITLESS RESTART */
            if ((u1TimerId == LLDP_TX_TMR_TTR) &&
                (LLDP_HR_STDY_ST_REQ_RCVD () == OSIX_TRUE) &&
                (u1HRTmrFlag == 0))
            {
                /* Periodic timer has been made to expire after processing
                 * steady state pkt request from RM. This flag indicates
                 * to send steady tail msg after sending all the steady
                 * state packets of LLDP to RM. */
                u1HRTmrFlag = 1;
            }
        }
    }                            /* End of while */

    /* HITLESS RESTART */
    if (u1HRTmrFlag == 1)
    {
        /* sending steady state tail msg to RM */
        LldpRedHRSendStdyStTailMsg ();
        LLDP_HR_STDY_ST_REQ_RCVD () = OSIX_FALSE;
    }
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpTmrTxDelayTmrExp
 *                                                                          
 *    DESCRIPTION      : Function that handles TxDelay/Delay while timer of
 *                       LLDP Tx Module.
 *                       This timer indicates the delay(in seconds) that
 *                       LLDP Tx module introduces between successive frame 
 *                       transmissions.
 *
 *    INPUT            : pArg - pointer to the port specific struct whose 
 *                       timer has expired. 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
LldpTmrTxDelayTmrExp (VOID *pArg)
{
    tLldpLocPortInfo   *pPortInfo = NULL;

    LLDP_TRC (CONTROL_PLANE_TRC, "LLDP TxDelay timer expired\r\n");
    pPortInfo = (tLldpLocPortInfo *) pArg;

    /* check either something changed local flag or message interval timer 
     * expired flag is set to TRUE, if something changed local flag is set to 
     * TRUE, update frame transmission can be initiated or if message interval 
     * timer expired flag is set to TRUE, refresh frame transmission can be 
     * initiated. If none of the above two condition 
     * satisfy just set txdelay timer status as expired and return. 
     * txdelay timer expiry will be handled if some thing changed local 
     * event comes after txdelay timer expiry event */
    pPortInfo->TxDelayTmrNode.u1Status = LLDP_TMR_EXPIRED;
    pPortInfo->bTxTick = OSIX_TRUE;
    LldpTxTmrSmMachine (pPortInfo, LLDP_TX_TMR_EV_TX_DELAY);
    if (pPortInfo->bSomethingChangedLocal == OSIX_FALSE)
    {
        return;
    }

    /* something changed local is OSIX_TRUE, 
     * 1. set something changed local to OSIX_FALSE
     * 2. stop message interval timer
     * 3. reconstruct LLDPDU
     * 4. initiate update frame transmission */
    if (pPortInfo->bSomethingChangedLocal == OSIX_TRUE)
    {
        if (TmrStopTimer (gLldpGlobalInfo.TmrListId,
                          &(pPortInfo->TtrTmr.TimerNode)) != TMR_SUCCESS)
        {
            return;
        }
        /* set the transmit interval timer(TtrTmr) status as
         * NOT_RUNNING */
        pPortInfo->TtrTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;
        /* reconstruct preformed buffer with info(update) frame */
        if (LldpTxUtlConstructPreformedBuf (pPortInfo, LLDP_INFO_FRAME)
            != OSIX_SUCCESS)
        {
            LLDP_TRC (CONTROL_PLANE_TRC, "LldpTmrTxDelayTmrExp: "
                      "LldpTxUtlConstructPreformedBuf failed\r\n");
            return;
        }
        /* call state machine */
        pPortInfo->bSomethingChangedLocal = OSIX_FALSE;
        LldpTxTmrSmMachine (pPortInfo, LLDP_TX_TMR_EV_LOCAL_CHANGE);
    }

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpTmrTtrTmrExp
 *                                                                          
 *    DESCRIPTION      : Function that handles TTR timer/Message Interval Timer
 *                       of LLDP Tx Module.
 *                       This timer indicates the number of seconds remaining 
 *                       until next refresh frame transmission can occur.
 *
 *    INPUT            : pArg - pointer to the port specific struct whose 
 *                       timer has expired. 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
LldpTmrTtrTmrExp (VOID *pArg)
{
    tLldpLocPortInfo   *pPortInfo = NULL;
    LLDP_TRC (CONTROL_PLANE_TRC, "LLDP MsgInterval timer expired\r\n");
    pPortInfo = (tLldpLocPortInfo *) pArg;

    /* set the transmit interval timer(TtrTmr) status as
     * EXPIRED */
    pPortInfo->TtrTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;
    /* As per IEEE802.1AB standard, Message interval should be greater than or
     * equal to 4 * TxDelay OR
     * TxDelay should be less than or eqaul to (Message interval/4). Hence
     * whenver Ttr timer(message interval timer) expires, TxDelay timer
     * might have been already expired, so no need to check here that whether 
     * TxDelay timer expired or not */
    /* Call state event machine(Transit to LLDP_TX_INFO_FRAME state) */
    LldpTxTmrSmMachine (pPortInfo, LLDP_TX_TMR_EV_MSG_INTVAL);
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpTmrShutWhileTmrExp
 *                                                                          
 *    DESCRIPTION      : Function that handles Shutdown while/Reinit Timer of
 *                       LLDP Tx Module.
 *                       This timer indicates the number of seconds remaining 
 *                       until LLDP re-initialization can occur.
 *
 *    INPUT            : pArg - pointer to the port specific struct whose 
 *                       timer has expired. 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
LldpTmrShutWhileTmrExp (VOID *pArg)
{
    tLldpLocPortInfo   *pPortInfo = NULL;

    LLDP_TRC (CONTROL_PLANE_TRC, "LLDP ShutWhile timer expired\r\n");
    pPortInfo = (tLldpLocPortInfo *) pArg;

    /* set the shutdown while timer status as EXPIRED */
    pPortInfo->ShutWhileTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;

    /* Call state event machine(Transit to LLDP_TX_INIT state) */
    LldpTxSmMachine (pPortInfo, LLDP_TX_EV_REINIT_DELAY);

    /* This timer will be started only when the port status
     * is changed. The shutdown frame packet will be sent if
     * the port is operationally down. In this case, indication 
     * must be sent to lower layers regarding port disable */
    if (pPortInfo->u1OperStatus == CFA_IF_DOWN)
    {
        LldpPortDownProcessedIndication (pPortInfo->i4IfIndex);
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpTmrRxInfoAgeTmrExp
 *                                                                          
 *    DESCRIPTION      : Function is called when info age out Timer expired.
 *                       This timer indicates the number of seconds remaining 
 *                       until the remote entry assocaited with this timer 
 *                       can expire.
 *
 *    INPUT            : pArg - pointer to the Remote Node struct whose 
 *                       timer has expired. 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
LldpTmrRxInfoAgeTmrExp (VOID *pArg)
{
    tLldpLocPortInfo   *pPortInfo = NULL;
    tLldpRemoteNode    *pRemoteNode = NULL;
    tLldpv2DestAddrTbl *pLldpv2DestAddrIndex = NULL;
    tLldpv2DestAddrTbl  Lldpv2DestAddrIndex;
    INT4                i4RecvdPortNum = LLDP_INVALID_VALUE;

    MEMSET (&Lldpv2DestAddrIndex, 0, sizeof (tLldpv2DestAddrTbl));
    pRemoteNode = (tLldpRemoteNode *) pArg;
    pRemoteNode->RxInfoAgeTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;
    Lldpv2DestAddrIndex.u4LlldpV2DestAddrTblIndex =
        pRemoteNode->u4DestAddrTblIndex;
    pLldpv2DestAddrIndex =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & Lldpv2DestAddrIndex);
    if (pLldpv2DestAddrIndex == NULL)
    {
        return;
    }
    /* when ISSU Maintenance Mode is in Progress restart the Lldp 
     * RX Info Age Timer */
    if (IssuGetMaintModeOperation () == OSIX_TRUE)
    {
        if (LldpRxStartInfoAgeTmr (pRemoteNode) != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC,
                      "ISSU Maintenace Mode:"
                      "Failed to Start the Info Age out Timer\r\n");
            return;
        }
        LLDP_TRC (CONTROL_PLANE_TRC,
                  "ISSU MM: Info Age out Timer restarted \r\n");
        return;
    }
    if (LldpUtlGetLocalPort (pRemoteNode->i4RemLocalPortNum,
                             pLldpv2DestAddrIndex->Lldpv2DestMacAddress,
                             (UINT4 *) &i4RecvdPortNum) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpTmrRxInfoAgeTmrExp:"
                  "LldpUtlGetLocalPort\r\n");
    }
    if (i4RecvdPortNum == LLDP_INVALID_VALUE)
    {
        return;
    }

    pPortInfo = LLDP_GET_LOC_PORT_INFO (i4RecvdPortNum);

    pPortInfo->pBackPtrRemoteNode = pRemoteNode;
    /* Call the Rx State Event Machine */
    LldpRxSemRun (pPortInfo, LLDP_RX_EV_INFO_AGEOUT);
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpTmrNotifIntervalTmrExp
 *                                                                          
 *    DESCRIPTION      : This function sends the notification and restarts the
 *                       Notification Interval Timer if the remote table has 
 *                       been updated while timer was running.
 *
 *    INPUT            : pArg
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
LldpTmrNotifIntervalTmrExp (VOID *pArg)
{
    UINT4               u4SysUpTime = 0;
    UNUSED_PARAM (pArg);

    LLDP_TRC (CONTROL_PLANE_TRC, "LLDP notification interval timer "
              "expired\r\n");
    LLDP_NOTIF_INTVAL_TMR_STATUS () = LLDP_TMR_NOT_RUNNING;
    u4SysUpTime = LLDP_SYS_UP_TIME ();

    /* Start the timer only if the remote table has been changed while the timer
     * was running and if the notification has been enabled on atleast one port.
     */
    /*
     * Consider the following scenario: Remote table change notification has
     * been enabled only on port 1. So when a neigbhor is learnt on this port
     * the trap will be sent and the timer will be started. Now a neignhor is 
     * learnt on port 2. In this case LLDP_REMOTE_STAT_LAST_CHNG_TIME will be
     * updated and when the timer expires, trap will again be generated.
     * To avoid this whenever the remote table change occurs bSendRemTblNotif
     * will be set to true and LldpSendNotification will be called only if
     * notification is enabled and the type is LLDP_REMOTE_CHG_NOTIFICATION or 
     * LLDP_REMOTE_CHG_MIS_CFG_NOTIFICATION. bSendRemTblNotif will be set to
     * false once the trap is sent. Now in the above mentioned scenario as 
     * bSendRemTblNotif is not set to true, on timer expiry the trap will not
     * be sent.
     */
    if (((((u4SysUpTime - LLDP_REMOTE_STAT_LAST_CHNG_TIME) /
           SYS_TIME_TICKS_IN_A_SEC) <= (UINT4) LLDP_NOTIF_INTERVAL ()) &&
         (gLldpGlobalInfo.bNotificationEnable == OSIX_TRUE) &&
         (gLldpGlobalInfo.bSendRemTblNotif == OSIX_TRUE)) ||
        (gLldpGlobalInfo.bSendTopChgNotif == OSIX_TRUE))
    {
        /* If LLDP-MED topology change boolean value is set as TRUE, reset the value
         * to FALSE since the remote node information is not available here and send 
         * the LLDP Remote change notification instead */

        gLldpGlobalInfo.bSendTopChgNotif = OSIX_FALSE;
        /* Send Remote table change notification */
        LldpSendNotification (NULL, LLDP_REM_TABLE_CHG);
    }
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpRxStartInfoAgeTmr
 *                                                                          
 *    DESCRIPTION      : This function starts the Info Ageout timer for a 
 *                       Remote Node.                     
 *
 *    INPUT            : pRemoteNode - Remote Node Pointer from which the 
 *                                     Info Age Timer Need to be started.
 *                      
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
LldpRxStartInfoAgeTmr (tLldpRemoteNode * pRemoteNode)
{
    UINT4               u4Duration = 0;

    if (pRemoteNode->u4RxTTL != 0)
    {
        if (IssuGetMaintModeOperation () == OSIX_TRUE)
        {
            u4Duration = pRemoteNode->u4RxTTL + ISSU_TIMER_VALUE;
        }
        else
        {
            u4Duration = pRemoteNode->u4RxTTL;
        }

        if (TmrStart (gLldpGlobalInfo.TmrListId,
                      &(pRemoteNode->RxInfoAgeTmr),
                      LLDP_RX_TMR_RX_INFO_TTL, u4Duration, 0) != TMR_FAILURE)
        {
            pRemoteNode->RxInfoAgeTmrNode.u1Status = LLDP_TMR_RUNNING;
            return OSIX_SUCCESS;
        }
        else
        {
            LLDP_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "LldpRxStartInfoAgeTmr: Unable to Start "
                      "the Info Ageout Timer\r\n");
        }
    }
    else
    {
        LLDP_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "LldpRxStartInfoAgeTmr: Cannot start the Ageout "
                  "Timer with RxTTL 0\r\n");
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpRxStopInfoAgeTmr
 *                                                                          
 *    DESCRIPTION      : This function stops the Info Ageout timer for a 
 *                       Remote Node.                     
 *
 *    INPUT            : pRemoteNode - Remote Node Pointer from which the 
 *                                     Info Age Timer Need to be stopped.
 *                      
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
LldpRxStopInfoAgeTmr (tLldpRemoteNode * pRemoteNode)
{
    if (TmrStop (gLldpGlobalInfo.TmrListId, &(pRemoteNode->RxInfoAgeTmr))
        != TMR_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "\r(LldpRxStopInfoAgeTmr) Unable to Stop the Info "
                  "Ageout Timer\r\n");
    }
    else
    {
        pRemoteNode->RxInfoAgeTmrNode.u1Status = LLDP_TMR_NOT_RUNNING;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpRxReStartInfoAgeTmr
 *                                                                          
 *    DESCRIPTION      : This function re-starts the Info Ageout timer for a 
 *                       Remote Node.                     
 *
 *    INPUT            : pRemoteNode - Remote Node Pointer from which the 
 *                                     Info Age Timer Need to be restarted.
 *                      
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
LldpRxReStartInfoAgeTmr (tLldpRemoteNode * pRemoteNode)
{
    if (pRemoteNode->u4RxTTL != 0)
    {
        if (TmrRestart (gLldpGlobalInfo.TmrListId, &(pRemoteNode->RxInfoAgeTmr),
                        LLDP_RX_TMR_RX_INFO_TTL, (UINT4) pRemoteNode->u4RxTTL,
                        0) != TMR_SUCCESS)

        {
            /* Trace Msg */
            LLDP_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "Failed to Restart the Info Age Timer.\r\n");
            return OSIX_FAILURE;
        }
        else
        {
            /* Trace Msg */
            LLDP_TRC (CONTROL_PLANE_TRC,
                      "Info Age Timer Re-Started Successfully\r\n");
            pRemoteNode->RxInfoAgeTmrNode.u1Status = LLDP_TMR_RUNNING;
            return OSIX_SUCCESS;
        }
    }
    else
    {
        LLDP_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC, "RxTTL is Invalid\r\n");
        return OSIX_FAILURE;
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpTmrGlobalShutWhileTmrExp
 *                                                                          
 *    DESCRIPTION      : This function is called when LLDP module is shut down
 *                       or disabled. The function clears all the port info
 *                       and move it to the initialise state if module is
 *                       disabled and releases the memory if the module is
 *                       shutdown.
 *
 *    INPUT            : pArg
 *                      
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
LldpTmrGlobalShutWhileTmrExp (VOID *pArg)
{
    tLldpLocPortInfo   *pPortEntry = NULL;
    tLldpLocPortInfo    PortEntry;

#ifdef L2RED_WANTED
    tLldpRedTmrSyncUpInfo TmrSyncUpInfo;

    MEMSET (&TmrSyncUpInfo, 0, sizeof (tLldpRedTmrSyncUpInfo));

    /* Fill global shutwhile timer start sync up information */
    TmrSyncUpInfo.u1TmrType = (UINT1) LLDP_TX_TMR_GLOBAL_SHUT_WHILE;
    TmrSyncUpInfo.u4TmrInterval = 0;
    TmrSyncUpInfo.pMsapRBIndex = NULL;
    /* Send global shutwhile timer start sync up information */
    LldpRedSendSyncUpMsg ((UINT1) LLDP_RED_TMR_EXP_MSG,
                          (VOID *) &TmrSyncUpInfo);
#endif

    UNUSED_PARAM (pArg);

    LLDP_TRC (CONTROL_PLANE_TRC, "LLDP Global ShutWhile timer expired\r\n");

    /* While shutting down or disabling LLDP module, the global timer is
     * started instead of starting timer for each port. The port entry
     * should be cleared for all ports. In case of module shutdown, port
     * related info should be released here simce it is being accessed.
     * The timer list is also deleted here as the global timer was started
     * while shutting the module. */

    pPortEntry = (tLldpLocPortInfo *)
        RBTreeGetFirst (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree);
    while (pPortEntry != NULL)
    {
        MEMSET (&PortEntry, 0, sizeof (tLldpLocPortInfo));
        PortEntry.u4LocPortNum = pPortEntry->u4LocPortNum;

        if (LLDP_SYSTEM_CONTROL () == LLDP_SHUTDOWN_INPROGRESS)
        {
            /* Release Memory allocated for holding PDU Info */
            if (pPortEntry->pPreFormedLldpdu != NULL)
            {
                if (MemReleaseMemBlock (gLldpGlobalInfo.LldpPduInfoPoolId,
                                        (UINT1 *) (pPortEntry->
                                                   pPreFormedLldpdu)) ==
                    MEM_FAILURE)
                {
                    LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                              "LldpTmrGlobalShutWhileTmrExp "
                              "PDU info Memory release failed!!.. "
                              "LLDP shutdown is unsuccessful\r\n");
                    pPortEntry->pPreFormedLldpdu = NULL;
                    pPortEntry->u4PreFormedLldpduLen = 0;
                }
            }

            /* release the memory allocated for port info structure */
            RBTreeRem (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree, pPortEntry);
            if (MemReleaseMemBlock
                (gLldpGlobalInfo.LocPortInfoPoolId,
                 (UINT1 *) pPortEntry) == MEM_FAILURE)
            {
                LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "LldpTmrGlobalShutWhileTmrExp "
                          "port info Memory release failed!!.. "
                          "LLDP shutdown is unsuccessful\r\n");
                return;

            }
            /* reset the pointer corresponding to this port in the global
             * information strcture to NULL */
            pPortEntry = NULL;
        }
        else
        {
            /* Module is disabled. So the state of ports must be changed.
             * Call state event machine(Transit to LLDP_TX_INIT state) */
            LldpTxSmMachine (pPortEntry, LLDP_TX_EV_REINIT_DELAY);
            LldpTxTmrSmMachine (pPortEntry, LLDP_TX_EV_PORT_OPER_DOWN);

        }
        pPortEntry = (tLldpLocPortInfo *)
            RBTreeGetNext (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
                           &PortEntry, LldpAgentInfoUtlRBCmpInfo);

    }

    if (LLDP_SYSTEM_CONTROL () == LLDP_SHUTDOWN_INPROGRESS)
    {
        /* PDU Info mempool */
        if (gLldpGlobalInfo.LldpPduInfoPoolId != 0)
        {
            if (MemDeleteMemPool (gLldpGlobalInfo.LldpPduInfoPoolId) ==
                MEM_FAILURE)
            {
                LLDP_TRC (OS_RESOURCE_TRC, "LldpTmrGlobalShutWhileTmrExp: "
                          "PDU Info MemPool Delete FAILED\r\n");
                LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "LldpTmrGlobalShutWhileTmrExp "
                          "LLDP shutdown is unsuccessful\r\n");
            }
            gLldpGlobalInfo.LldpPduInfoPoolId = 0;
        }
        /* Local Port Info mempool */
        if (gLldpGlobalInfo.LocPortInfoPoolId != 0)
        {
            if (MemDeleteMemPool (gLldpGlobalInfo.LocPortInfoPoolId) ==
                MEM_FAILURE)
            {
                LLDP_TRC (OS_RESOURCE_TRC, "LldpTmrGlobalShutWhileTmrExp: "
                          "Local Port Info MemPool Delete FAILED\r\n");
                LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "LldpTmrGlobalShutWhileTmrExp "
                          "LLDP shutdown is unsuccessful\r\n");
                return;
            }
            gLldpGlobalInfo.LldpLocPortAgentInfoRBTree = NULL;
            gLldpGlobalInfo.LocPortInfoPoolId = 0;
        }
        if (gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId != 0)
        {
            RBTreeDestroy (gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree,
                           LldpUtlRBFreeAgentToLocPortTable, 0);
            gLldpGlobalInfo.Lldpv2AgentToLocPortMapTblRBTree = NULL;
            RBTreeDestroy (gLldpGlobalInfo.Lldpv2AgentMapTblRBTree,
                           LldpUtlRBFreeAgentMapTable, 0);
            gLldpGlobalInfo.Lldpv2AgentMapTblRBTree = NULL;
            gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId = 0;
        }

        if (gLldpGlobalInfo.LldpDestMacAddrPoolId != 0)
        {
            RBTreeDestroy (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree,
                           LldpUtlRBFreeDestAddrTable, 0);
            gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree = NULL;
            gLldpGlobalInfo.LldpDestMacAddrPoolId = 0;
        }

        if (gLldpGlobalInfo.LocPortTablePoolId != 0)
        {
            RBTreeDestroy (gLldpGlobalInfo.LldpPortInfoRBTree,
                           LldpUtlRBFreePortTable, 0);
            gLldpGlobalInfo.LldpPortInfoRBTree = NULL;
            gLldpGlobalInfo.LocPortTablePoolId = 0;
        }
        /* DeInit Timer */
        if (gLldpGlobalInfo.TmrListId != 0)
        {
            if (LldpTmrDeInit () == OSIX_FAILURE)
            {
                LLDP_TRC (OS_RESOURCE_TRC, "LldpTmrGlobalShutWhileTmrExp: "
                          "Timer Deinit FAILED\r\n");
                LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "LldpTmrGlobalShutWhileTmrExp "
                          "LLDP shutdown is unsuccessful\r\n");
                return;
            }
        }

        if (LldpPortDeRegisterWithRM () != OSIX_SUCCESS)
        {
            LLDP_TRC (ALL_FAILURE_TRC,
                      "LldpTmrGlobalShutWhileTmrExp: DeRegistration with RM "
                      "FAILED!!!\r\n");
        }

        /* Delete all the messages in queue */
        LldpTxUtlDeleteQMsg (gLldpGlobalInfo.MsgQId,
                             gLldpGlobalInfo.QMsgPoolId);
        LldpTxUtlDeleteQMsg (gLldpGlobalInfo.RxPduQId,
                             gLldpGlobalInfo.RxPduQPoolId);
        LldpMainMemClear ();
        LLDP_SYSTEM_CONTROL () = LLDP_SHUTDOWN;
    }

    LLDP_GLOB_SHUT_WHILE_TMR_STATUS () = LLDP_TMR_NOT_RUNNING;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  lldtmr.c                       */
/*-----------------------------------------------------------------------*/
