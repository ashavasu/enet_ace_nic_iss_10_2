/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: stdlldpmedx.h,v 1.1 2015/09/09 13:22:01 siva Exp $
 *
 * Description: This file contains the OIDs of stdlldpmed mib
 *********************************************************************/
/*
 * Automatically generated by FuturePostmosy
 * Do not Edit!!!
 */

#ifndef _STDLLDPMED_H
#define _STDLLDPMED_H

/* SNMP-MIB translation table. */

static struct MIB_OID std_lldp_med_orig_mib_oid_table[] = {
{"ccitt",        "0"},
{"iso",        "1"},
{"lldpExtensions",        "1.0.8802.1.1.2.1.5"},
{"lldpXMedMIB",        "1.0.8802.1.1.2.1.5.4795"},
{"lldpXMedNotifications",        "1.0.8802.1.1.2.1.5.4795.0"},
{"lldpXMedObjects",        "1.0.8802.1.1.2.1.5.4795.1"},
{"lldpXMedConfig",        "1.0.8802.1.1.2.1.5.4795.1.1"},
{"lldpXMedLocDeviceClass",        "1.0.8802.1.1.2.1.5.4795.1.1.1"},
{"lldpXMedPortConfigTable",        "1.0.8802.1.1.2.1.5.4795.1.1.2"},
{"lldpXMedPortConfigEntry",        "1.0.8802.1.1.2.1.5.4795.1.1.2.1"},
{"lldpXMedPortCapSupported",        "1.0.8802.1.1.2.1.5.4795.1.1.2.1.1"},
{"lldpXMedPortConfigTLVsTxEnable",        "1.0.8802.1.1.2.1.5.4795.1.1.2.1.2"},
{"lldpXMedPortConfigNotifEnable",        "1.0.8802.1.1.2.1.5.4795.1.1.2.1.3"},
{"lldpXMedFastStartRepeatCount",        "1.0.8802.1.1.2.1.5.4795.1.1.3"},
{"lldpXMedLocalData",        "1.0.8802.1.1.2.1.5.4795.1.2"},
{"lldpXMedLocMediaPolicyTable",        "1.0.8802.1.1.2.1.5.4795.1.2.1"},
{"lldpXMedLocMediaPolicyEntry",        "1.0.8802.1.1.2.1.5.4795.1.2.1.1"},
{"lldpXMedLocMediaPolicyAppType",        "1.0.8802.1.1.2.1.5.4795.1.2.1.1.1"},
{"lldpXMedLocMediaPolicyVlanID",        "1.0.8802.1.1.2.1.5.4795.1.2.1.1.2"},
{"lldpXMedLocMediaPolicyPriority",        "1.0.8802.1.1.2.1.5.4795.1.2.1.1.3"},
{"lldpXMedLocMediaPolicyDscp",        "1.0.8802.1.1.2.1.5.4795.1.2.1.1.4"},
{"lldpXMedLocMediaPolicyUnknown",        "1.0.8802.1.1.2.1.5.4795.1.2.1.1.5"},
{"lldpXMedLocMediaPolicyTagged",        "1.0.8802.1.1.2.1.5.4795.1.2.1.1.6"},
{"lldpXMedLocHardwareRev",        "1.0.8802.1.1.2.1.5.4795.1.2.2"},
{"lldpXMedLocFirmwareRev",        "1.0.8802.1.1.2.1.5.4795.1.2.3"},
{"lldpXMedLocSoftwareRev",        "1.0.8802.1.1.2.1.5.4795.1.2.4"},
{"lldpXMedLocSerialNum",        "1.0.8802.1.1.2.1.5.4795.1.2.5"},
{"lldpXMedLocMfgName",        "1.0.8802.1.1.2.1.5.4795.1.2.6"},
{"lldpXMedLocModelName",        "1.0.8802.1.1.2.1.5.4795.1.2.7"},
{"lldpXMedLocAssetID",        "1.0.8802.1.1.2.1.5.4795.1.2.8"},
{"lldpXMedLocLocationTable",        "1.0.8802.1.1.2.1.5.4795.1.2.9"},
{"lldpXMedLocLocationEntry",        "1.0.8802.1.1.2.1.5.4795.1.2.9.1"},
{"lldpXMedLocLocationSubtype",        "1.0.8802.1.1.2.1.5.4795.1.2.9.1.1"},
{"lldpXMedLocLocationInfo",        "1.0.8802.1.1.2.1.5.4795.1.2.9.1.2"},
{"lldpXMedLocXPoEDeviceType",        "1.0.8802.1.1.2.1.5.4795.1.2.10"},
{"lldpXMedLocXPoEPSEPortTable",        "1.0.8802.1.1.2.1.5.4795.1.2.11"},
{"lldpXMedLocXPoEPSEPortEntry",        "1.0.8802.1.1.2.1.5.4795.1.2.11.1"},
{"lldpXMedLocXPoEPSEPortPowerAv",        "1.0.8802.1.1.2.1.5.4795.1.2.11.1.1"},
{"lldpXMedLocXPoEPSEPortPDPriority",        "1.0.8802.1.1.2.1.5.4795.1.2.11.1.2"},
{"lldpXMedLocXPoEPSEPowerSource",        "1.0.8802.1.1.2.1.5.4795.1.2.12"},
{"lldpXMedLocXPoEPDPowerReq",        "1.0.8802.1.1.2.1.5.4795.1.2.13"},
{"lldpXMedLocXPoEPDPowerSource",        "1.0.8802.1.1.2.1.5.4795.1.2.14"},
{"lldpXMedLocXPoEPDPowerPriority",        "1.0.8802.1.1.2.1.5.4795.1.2.15"},
{"lldpXMedRemoteData",        "1.0.8802.1.1.2.1.5.4795.1.3"},
{"lldpXMedRemCapabilitiesTable",        "1.0.8802.1.1.2.1.5.4795.1.3.1"},
{"lldpXMedRemCapabilitiesEntry",        "1.0.8802.1.1.2.1.5.4795.1.3.1.1"},
{"lldpXMedRemCapSupported",        "1.0.8802.1.1.2.1.5.4795.1.3.1.1.1"},
{"lldpXMedRemCapCurrent",        "1.0.8802.1.1.2.1.5.4795.1.3.1.1.2"},
{"lldpXMedRemDeviceClass",        "1.0.8802.1.1.2.1.5.4795.1.3.1.1.3"},
{"lldpXMedRemMediaPolicyTable",        "1.0.8802.1.1.2.1.5.4795.1.3.2"},
{"lldpXMedRemMediaPolicyEntry",        "1.0.8802.1.1.2.1.5.4795.1.3.2.1"},
{"lldpXMedRemMediaPolicyAppType",        "1.0.8802.1.1.2.1.5.4795.1.3.2.1.1"},
{"lldpXMedRemMediaPolicyVlanID",        "1.0.8802.1.1.2.1.5.4795.1.3.2.1.2"},
{"lldpXMedRemMediaPolicyPriority",        "1.0.8802.1.1.2.1.5.4795.1.3.2.1.3"},
{"lldpXMedRemMediaPolicyDscp",        "1.0.8802.1.1.2.1.5.4795.1.3.2.1.4"},
{"lldpXMedRemMediaPolicyUnknown",        "1.0.8802.1.1.2.1.5.4795.1.3.2.1.5"},
{"lldpXMedRemMediaPolicyTagged",        "1.0.8802.1.1.2.1.5.4795.1.3.2.1.6"},
{"lldpXMedRemInventoryTable",        "1.0.8802.1.1.2.1.5.4795.1.3.3"},
{"lldpXMedRemInventoryEntry",        "1.0.8802.1.1.2.1.5.4795.1.3.3.1"},
{"lldpXMedRemHardwareRev",        "1.0.8802.1.1.2.1.5.4795.1.3.3.1.1"},
{"lldpXMedRemFirmwareRev",        "1.0.8802.1.1.2.1.5.4795.1.3.3.1.2"},
{"lldpXMedRemSoftwareRev",        "1.0.8802.1.1.2.1.5.4795.1.3.3.1.3"},
{"lldpXMedRemSerialNum",        "1.0.8802.1.1.2.1.5.4795.1.3.3.1.4"},
{"lldpXMedRemMfgName",        "1.0.8802.1.1.2.1.5.4795.1.3.3.1.5"},
{"lldpXMedRemModelName",        "1.0.8802.1.1.2.1.5.4795.1.3.3.1.6"},
{"lldpXMedRemAssetID",        "1.0.8802.1.1.2.1.5.4795.1.3.3.1.7"},
{"lldpXMedRemLocationTable",        "1.0.8802.1.1.2.1.5.4795.1.3.4"},
{"lldpXMedRemLocationEntry",        "1.0.8802.1.1.2.1.5.4795.1.3.4.1"},
{"lldpXMedRemLocationSubtype",        "1.0.8802.1.1.2.1.5.4795.1.3.4.1.1"},
{"lldpXMedRemLocationInfo",        "1.0.8802.1.1.2.1.5.4795.1.3.4.1.2"},
{"lldpXMedRemXPoETable",        "1.0.8802.1.1.2.1.5.4795.1.3.5"},
{"lldpXMedRemXPoEEntry",        "1.0.8802.1.1.2.1.5.4795.1.3.5.1"},
{"lldpXMedRemXPoEDeviceType",        "1.0.8802.1.1.2.1.5.4795.1.3.5.1.1"},
{"lldpXMedRemXPoEPSETable",        "1.0.8802.1.1.2.1.5.4795.1.3.6"},
{"lldpXMedRemXPoEPSEEntry",        "1.0.8802.1.1.2.1.5.4795.1.3.6.1"},
{"lldpXMedRemXPoEPSEPowerAv",        "1.0.8802.1.1.2.1.5.4795.1.3.6.1.1"},
{"lldpXMedRemXPoEPSEPowerSource",        "1.0.8802.1.1.2.1.5.4795.1.3.6.1.2"},
{"lldpXMedRemXPoEPSEPowerPriority",        "1.0.8802.1.1.2.1.5.4795.1.3.6.1.3"},
{"lldpXMedRemXPoEPDTable",        "1.0.8802.1.1.2.1.5.4795.1.3.7"},
{"lldpXMedRemXPoEPDEntry",        "1.0.8802.1.1.2.1.5.4795.1.3.7.1"},
{"lldpXMedRemXPoEPDPowerReq",        "1.0.8802.1.1.2.1.5.4795.1.3.7.1.1"},
{"lldpXMedRemXPoEPDPowerSource",        "1.0.8802.1.1.2.1.5.4795.1.3.7.1.2"},
{"lldpXMedRemXPoEPDPowerPriority",        "1.0.8802.1.1.2.1.5.4795.1.3.7.1.3"},
{"lldpXMedConformance",        "1.0.8802.1.1.2.1.5.4795.2"},
{"lldpXMedCompliances",        "1.0.8802.1.1.2.1.5.4795.2.1"},
{"lldpXMedGroups",        "1.0.8802.1.1.2.1.5.4795.2.2"},
{"lldpV2Xdot1MIB",        "1.0.8802.1.1.2.1.5.40000"},
{"org",        "1.3"},
{"dod",        "1.3.6"},
{"internet",        "1.3.6.1"},
{"directory",        "1.3.6.1.1"},
{"mgmt",        "1.3.6.1.2"},
{"mib-2",        "1.3.6.1.2.1"},
{"ip",        "1.3.6.1.2.1.4"},
{"transmission",        "1.3.6.1.2.1.10"},
{"mplsStdMIB",        "1.3.6.1.2.1.10.166"},
{"rmon",        "1.3.6.1.2.1.16"},
{"statistics",        "1.3.6.1.2.1.16.1"},
{"etherStatsHighCapacityEntry",        "1.3.6.1.2.1.16.1.7.1"},
{"history",        "1.3.6.1.2.1.16.2"},
{"hosts",        "1.3.6.1.2.1.16.4"},
{"hostTopN",        "1.3.6.1.2.1.16.5"},
{"matrix",        "1.3.6.1.2.1.16.6"},
{"filter",        "1.3.6.1.2.1.16.7"},
{"capture",        "1.3.6.1.2.1.16.8"},
{"tokenRing",        "1.3.6.1.2.1.16.10"},
{"protocolDist",        "1.3.6.1.2.1.16.12"},
{"nlHost",        "1.3.6.1.2.1.16.14"},
{"nlMatrix",        "1.3.6.1.2.1.16.15"},
{"alHost",        "1.3.6.1.2.1.16.16"},
{"alMatrix",        "1.3.6.1.2.1.16.17"},
{"usrHistory",        "1.3.6.1.2.1.16.18"},
{"probeConfig",        "1.3.6.1.2.1.16.19"},
{"rmonConformance",        "1.3.6.1.2.1.16.20"},
{"dot1dBridge",        "1.3.6.1.2.1.17"},
{"dot1dStp",        "1.3.6.1.2.1.17.2"},
{"dot1dTp",        "1.3.6.1.2.1.17.4"},
{"vrrpOperEntry",        "1.3.6.1.2.1.18.1.3.1"},
{"dns",        "1.3.6.1.2.1.32"},
{"experimental",        "1.3.6.1.3"},
{"private",        "1.3.6.1.4"},
{"enterprises",        "1.3.6.1.4.1"},
{"ARICENT-MPLS-TP-MIB",        "1.3.6.1.4.1.2076.13.4"},
{"issExt",        "1.3.6.1.4.1.2076.81.8"},
{"fsDot1dBridge",        "1.3.6.1.4.1.2076.116"},
{"fsDot1dStp",        "1.3.6.1.4.1.2076.116.2"},
{"fsDot1dTp",        "1.3.6.1.4.1.2076.116.4"},
{"security",        "1.3.6.1.5"},
{"snmpV2",        "1.3.6.1.6"},
{"snmpDomains",        "1.3.6.1.6.1"},
{"snmpProxys",        "1.3.6.1.6.2"},
{"snmpModules",        "1.3.6.1.6.3"},
{"snmpTraps",        "1.3.6.1.6.3.1.1.5"},
{"snmpAuthProtocols",        "1.3.6.1.6.3.10.1.1"},
{"snmpPrivProtocols",        "1.3.6.1.6.3.10.1.2"},
{"ieee802dot1mibs",        "1.3.111.2.802.1.1"},
{"joint-iso-ccitt",        "2"},
{0,0}
};

#endif /* _STDLLDPMED_H */
