/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: fslldwr.h,v 1.13 2016/07/12 12:40:24 siva Exp $
 *
 * Description: Wrapper header file generated for fslldp.mib 
 *********************************************************************/
#ifndef _FSLLDPWR_H
#define _FSLLDPWR_H

VOID RegisterFSLLDP(VOID);

VOID UnRegisterFSLLDP(VOID);
INT4 FsLldpSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpTraceInputGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpTraceLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpTagStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpConfiguredMgmtIpv4AddressGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpConfiguredMgmtIpv6AddressGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpTraceInputSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpTraceLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpTagStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpConfiguredMgmtIpv4AddressSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpConfiguredMgmtIpv6AddressSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpTraceInputTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpTraceLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpTagStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpConfiguredMgmtIpv4AddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpConfiguredMgmtIpv6AddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpSystemControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsLldpModuleStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsLldpTraceInputDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsLldpTraceLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsLldpTagStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsLldpConfiguredMgmtIpv4AddressDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsLldpConfiguredMgmtIpv6AddressDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsLldpLocChassisIdSubtypeGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpLocChassisIdGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpLocChassisIdSubtypeSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpLocChassisIdSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpLocChassisIdSubtypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpLocChassisIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpLocChassisIdSubtypeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsLldpLocChassisIdDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsLldpLocPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsLldpLocPortIdSubtypeGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpLocPortIdGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpPortConfigNotificationTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpLocPortDstMacGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpLocPortIdSubtypeSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpLocPortIdSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpPortConfigNotificationTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpLocPortDstMacSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpLocPortIdSubtypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpLocPortIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpPortConfigNotificationTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpLocPortDstMacTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpMedAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpLocPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsLldpManAddrConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsLldpManAddrConfigOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMemAllocFailureGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpInputQOverFlowsGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpStatsRemTablesUpdatesGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpClearStatsGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpClearStatsSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpClearStatsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpClearStatsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Fslldpv2VersionGet(tSnmpIndex *, tRetVal *);
INT4 Fslldpv2VersionSet(tSnmpIndex *, tRetVal *);
INT4 Fslldpv2VersionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fslldpv2VersionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFslldpv2ConfigPortMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 Fslldpv2ConfigPortMapNumGet(tSnmpIndex *, tRetVal *);
INT4 Fslldpv2ConfigPortRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Fslldpv2ConfigPortRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Fslldpv2ConfigPortRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fslldpv2ConfigPortMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFslldpV2DestAddressTable(tSnmpIndex *, tSnmpIndex *);
INT4 FslldpV2DestMacAddressGet(tSnmpIndex *, tRetVal *);
INT4 Fslldpv2DestRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FslldpV2DestMacAddressSet(tSnmpIndex *, tRetVal *);
INT4 Fslldpv2DestRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FslldpV2DestMacAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fslldpv2DestRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FslldpV2DestAddressTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsLldpStatsTaggedTxPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsLldpStatsTaggedTxPortFramesTotalGet(tSnmpIndex *, tRetVal *);
#endif /* _FSLLDPWR_H */
