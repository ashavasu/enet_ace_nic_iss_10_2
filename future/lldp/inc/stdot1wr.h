/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: stdot1wr.h,v 1.4 2008/08/20 15:13:21 iss Exp $
 *
 * Description: Wrapper header file for stdot1lldp mib  
 *********************************************************************/
#ifndef _STDOT1WR_H
#define _STDOT1WR_H
INT4 GetNextIndexLldpXdot1ConfigPortVlanTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSTDOT1(VOID);

VOID UnRegisterSTDOT1(VOID);
INT4 LldpXdot1ConfigPortVlanTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1ConfigPortVlanTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1ConfigPortVlanTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXdot1ConfigPortVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexLldpXdot1LocVlanNameTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1LocVlanNameGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1ConfigVlanNameTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1ConfigVlanNameTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1ConfigVlanNameTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1ConfigVlanNameTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXdot1ConfigVlanNameTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexLldpXdot1LocProtoVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1LocProtoVlanSupportedGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1LocProtoVlanEnabledGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1ConfigProtoVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1ConfigProtoVlanTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1ConfigProtoVlanTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1ConfigProtoVlanTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXdot1ConfigProtoVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexLldpXdot1LocProtocolTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1LocProtocolIdGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1ConfigProtocolTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1ConfigProtocolTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1ConfigProtocolTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1ConfigProtocolTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXdot1ConfigProtocolTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexLldpXdot1LocTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1LocPortVlanIdGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1RemTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1RemPortVlanIdGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1RemProtoVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1RemProtoVlanSupportedGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1RemProtoVlanEnabledGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1RemVlanNameTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1RemVlanNameGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1RemProtocolTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1RemProtocolIdGet(tSnmpIndex *, tRetVal *);
#endif /* _STDOT1WR_H */
