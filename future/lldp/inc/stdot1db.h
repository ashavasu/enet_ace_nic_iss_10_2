/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdot1db.h,v 1.4 2008/08/20 15:13:21 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDOT1DB_H
#define _STDOT1DB_H

UINT1 LldpXdot1ConfigPortVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXdot1LocVlanNameTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 LldpXdot1ConfigVlanNameTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 LldpXdot1LocProtoVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXdot1ConfigProtoVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXdot1LocProtocolTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXdot1ConfigProtocolTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXdot1LocTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXdot1RemTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXdot1RemProtoVlanTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXdot1RemVlanNameTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 LldpXdot1RemProtocolTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 stdot1 [] ={1,0,8802,1,1,2,1,5,32962};
tSNMP_OID_TYPE stdot1OID = {9, stdot1};


UINT4 LldpXdot1ConfigPortVlanTxEnable [ ] ={1,0,8802,1,1,2,1,5,32962,1,1,1,1,1};
UINT4 LldpXdot1LocVlanId [ ] ={1,0,8802,1,1,2,1,5,32962,1,2,3,1,1};
UINT4 LldpXdot1LocVlanName [ ] ={1,0,8802,1,1,2,1,5,32962,1,2,3,1,2};
UINT4 LldpXdot1ConfigVlanNameTxEnable [ ] ={1,0,8802,1,1,2,1,5,32962,1,1,2,1,1};
UINT4 LldpXdot1LocProtoVlanId [ ] ={1,0,8802,1,1,2,1,5,32962,1,2,2,1,1};
UINT4 LldpXdot1LocProtoVlanSupported [ ] ={1,0,8802,1,1,2,1,5,32962,1,2,2,1,2};
UINT4 LldpXdot1LocProtoVlanEnabled [ ] ={1,0,8802,1,1,2,1,5,32962,1,2,2,1,3};
UINT4 LldpXdot1ConfigProtoVlanTxEnable [ ] ={1,0,8802,1,1,2,1,5,32962,1,1,3,1,1};
UINT4 LldpXdot1LocProtocolIndex [ ] ={1,0,8802,1,1,2,1,5,32962,1,2,4,1,1};
UINT4 LldpXdot1LocProtocolId [ ] ={1,0,8802,1,1,2,1,5,32962,1,2,4,1,2};
UINT4 LldpXdot1ConfigProtocolTxEnable [ ] ={1,0,8802,1,1,2,1,5,32962,1,1,4,1,1};
UINT4 LldpXdot1LocPortVlanId [ ] ={1,0,8802,1,1,2,1,5,32962,1,2,1,1,1};
UINT4 LldpXdot1RemPortVlanId [ ] ={1,0,8802,1,1,2,1,5,32962,1,3,1,1,1};
UINT4 LldpXdot1RemProtoVlanId [ ] ={1,0,8802,1,1,2,1,5,32962,1,3,2,1,1};
UINT4 LldpXdot1RemProtoVlanSupported [ ] ={1,0,8802,1,1,2,1,5,32962,1,3,2,1,2};
UINT4 LldpXdot1RemProtoVlanEnabled [ ] ={1,0,8802,1,1,2,1,5,32962,1,3,2,1,3};
UINT4 LldpXdot1RemVlanId [ ] ={1,0,8802,1,1,2,1,5,32962,1,3,3,1,1};
UINT4 LldpXdot1RemVlanName [ ] ={1,0,8802,1,1,2,1,5,32962,1,3,3,1,2};
UINT4 LldpXdot1RemProtocolIndex [ ] ={1,0,8802,1,1,2,1,5,32962,1,3,4,1,1};
UINT4 LldpXdot1RemProtocolId [ ] ={1,0,8802,1,1,2,1,5,32962,1,3,4,1,2};


tMbDbEntry stdot1MibEntry[]= {

{{14,LldpXdot1ConfigPortVlanTxEnable}, GetNextIndexLldpXdot1ConfigPortVlanTable, LldpXdot1ConfigPortVlanTxEnableGet, LldpXdot1ConfigPortVlanTxEnableSet, LldpXdot1ConfigPortVlanTxEnableTest, LldpXdot1ConfigPortVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpXdot1ConfigPortVlanTableINDEX, 1, 0, 0, "2"},

{{14,LldpXdot1ConfigVlanNameTxEnable}, GetNextIndexLldpXdot1ConfigVlanNameTable, LldpXdot1ConfigVlanNameTxEnableGet, LldpXdot1ConfigVlanNameTxEnableSet, LldpXdot1ConfigVlanNameTxEnableTest, LldpXdot1ConfigVlanNameTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpXdot1ConfigVlanNameTableINDEX, 2, 0, 0, "2"},

{{14,LldpXdot1ConfigProtoVlanTxEnable}, GetNextIndexLldpXdot1ConfigProtoVlanTable, LldpXdot1ConfigProtoVlanTxEnableGet, LldpXdot1ConfigProtoVlanTxEnableSet, LldpXdot1ConfigProtoVlanTxEnableTest, LldpXdot1ConfigProtoVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpXdot1ConfigProtoVlanTableINDEX, 2, 0, 0, "2"},

{{14,LldpXdot1ConfigProtocolTxEnable}, GetNextIndexLldpXdot1ConfigProtocolTable, LldpXdot1ConfigProtocolTxEnableGet, LldpXdot1ConfigProtocolTxEnableSet, LldpXdot1ConfigProtocolTxEnableTest, LldpXdot1ConfigProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpXdot1ConfigProtocolTableINDEX, 2, 0, 0, "2"},

{{14,LldpXdot1LocPortVlanId}, GetNextIndexLldpXdot1LocTable, LldpXdot1LocPortVlanIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpXdot1LocTableINDEX, 1, 0, 0, NULL},

{{14,LldpXdot1LocProtoVlanId}, GetNextIndexLldpXdot1LocProtoVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, LldpXdot1LocProtoVlanTableINDEX, 2, 0, 0, NULL},

{{14,LldpXdot1LocProtoVlanSupported}, GetNextIndexLldpXdot1LocProtoVlanTable, LldpXdot1LocProtoVlanSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot1LocProtoVlanTableINDEX, 2, 0, 0, NULL},

{{14,LldpXdot1LocProtoVlanEnabled}, GetNextIndexLldpXdot1LocProtoVlanTable, LldpXdot1LocProtoVlanEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot1LocProtoVlanTableINDEX, 2, 0, 0, NULL},

{{14,LldpXdot1LocVlanId}, GetNextIndexLldpXdot1LocVlanNameTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, LldpXdot1LocVlanNameTableINDEX, 2, 0, 0, NULL},

{{14,LldpXdot1LocVlanName}, GetNextIndexLldpXdot1LocVlanNameTable, LldpXdot1LocVlanNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpXdot1LocVlanNameTableINDEX, 2, 0, 0, NULL},

{{14,LldpXdot1LocProtocolIndex}, GetNextIndexLldpXdot1LocProtocolTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, LldpXdot1LocProtocolTableINDEX, 2, 0, 0, NULL},

{{14,LldpXdot1LocProtocolId}, GetNextIndexLldpXdot1LocProtocolTable, LldpXdot1LocProtocolIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpXdot1LocProtocolTableINDEX, 2, 0, 0, NULL},

{{14,LldpXdot1RemPortVlanId}, GetNextIndexLldpXdot1RemTable, LldpXdot1RemPortVlanIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpXdot1RemTableINDEX, 3, 0, 0, NULL},

{{14,LldpXdot1RemProtoVlanId}, GetNextIndexLldpXdot1RemProtoVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, LldpXdot1RemProtoVlanTableINDEX, 4, 0, 0, NULL},

{{14,LldpXdot1RemProtoVlanSupported}, GetNextIndexLldpXdot1RemProtoVlanTable, LldpXdot1RemProtoVlanSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot1RemProtoVlanTableINDEX, 4, 0, 0, NULL},

{{14,LldpXdot1RemProtoVlanEnabled}, GetNextIndexLldpXdot1RemProtoVlanTable, LldpXdot1RemProtoVlanEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot1RemProtoVlanTableINDEX, 4, 0, 0, NULL},

{{14,LldpXdot1RemVlanId}, GetNextIndexLldpXdot1RemVlanNameTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, LldpXdot1RemVlanNameTableINDEX, 4, 0, 0, NULL},

{{14,LldpXdot1RemVlanName}, GetNextIndexLldpXdot1RemVlanNameTable, LldpXdot1RemVlanNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpXdot1RemVlanNameTableINDEX, 4, 0, 0, NULL},

{{14,LldpXdot1RemProtocolIndex}, GetNextIndexLldpXdot1RemProtocolTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, LldpXdot1RemProtocolTableINDEX, 4, 0, 0, NULL},

{{14,LldpXdot1RemProtocolId}, GetNextIndexLldpXdot1RemProtocolTable, LldpXdot1RemProtocolIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpXdot1RemProtocolTableINDEX, 4, 0, 0, NULL},
};
tMibData stdot1Entry = { 20, stdot1MibEntry };
#endif /* _STDOT1DB_H */

