/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldmacs.h,v 1.31 2016/07/12 12:40:24 siva Exp $
 *
 * Description: This file contains macro definitions used in
 *              LLDP module
 **********************************************************************/

#ifndef _LLDMACS_H_
#define _LLDMACS_H_

#define LLDP_STRLEN(pu1String, i4MaxLen) \
  ((STRLEN (pu1String) >= i4MaxLen) ? i4MaxLen : STRLEN (pu1String))
#define  LLDP_GET_LOC_PORT_INFO(u4PortNum) LldpGetLocPortInfo (u4PortNum)

#define  LLDP_LOC_PORT_AUTO_NEG_INFO(pLldpLocPortInfo) \
    pLldpLocPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo

#define  LLDP_LOC_PORT_AGG_INFO(pLldpLocPortInfo) \
    pLldpLocPortInfo->Dot3LocPortInfo.u1AggStatus

#define  LLDP_REM_PORT_AGG_INFO(pDot3RemPortInfo) \
    pDot3RemPortInfo->u1AggStatus

#define  LLDP_REM_PORT_AUTO_NEG_INFO(pDot3RemPortInfo) \
    pDot3RemPortInfo->RemDot3AutoNegInfo

#define LLDP_IS_PORT_ID_VALID(u4IfIndex) \
    (((u4IfIndex >= LLDP_MIN_PORTS) && \
      (u4IfIndex <= LLDP_MAX_PORTS)) ? OSIX_TRUE : OSIX_FALSE)

#define LLDP_IS_PORT_CHANNEL(u4Port) \
    (((u4Port > BRG_MAX_PHY_PORTS) && (u4Port <= BRG_MAX_PHY_PLUS_LOG_PORTS)) \
     ? OSIX_TRUE : OSIX_FALSE)

/* 0 will be used if the system does not know the PVID or PPVID.
 * Hence VLAN_IS_VLAN_ID_VALID is not used for validating the
 * vlan id*/
#define LLDP_IS_VLAN_ID_VALID(VlanId) \
    (((VlanId > VLAN_MAX_VLAN_ID)) ? OSIX_FALSE : OSIX_TRUE)

#define LLDP_IS_REM_INDEX_VALID(i4RemIndex) \
    ((i4RemIndex >= LLDP_MIN_REM_INDEX) ? OSIX_TRUE : OSIX_FALSE)

#define LLDP_IS_DEST_INDEX_VALID(u4DestIndex) \
    ((u4DestIndex >= LLDP_MAX_DEST_INDEX) ? OSIX_FALSE : OSIX_TRUE)
#define LLDP_IS_PROTO_ID_VALID(u4ProtoIndex) \
    (((u4ProtoIndex >= LLDP_MIN_PROTO_INDEX) && \
      (u4ProtoIndex <= LLDP_MAX_PROTO_INDEX)) ? OSIX_TRUE : OSIX_FALSE)

#define LLDP_IS_REM_INDICES_VALID(i4LocalPortNum, u4DestIndex, i4RemIndex) \
    ((LLDP_IS_PORT_ID_VALID(i4LocalPortNum) == OSIX_TRUE) && \
  (LLDP_IS_DEST_INDEX_VALID (u4DestIndex) == OSIX_TRUE) && \
     (LLDP_IS_REM_INDEX_VALID(i4RemIndex) == OSIX_TRUE) ?\
     OSIX_TRUE : OSIX_FALSE )
    
#define  LLDP_OFFSET(x,y)   FSAP_OFFSETOF(x,y)

#define  LLDP_SYSTEM_CONTROL()          gLldpGlobalInfo.u1LldpSystemControl
#define  LLDP_MODULE_STATUS()           gLldpGlobalInfo.u1ModuleStatus  

#define LLDP_MAX_MEM_CMP_LEN(u4Len1, u4Len2) \
    (u4Len1 > u4Len2) ? u4Len1 : u4Len2

#define LLDP_GET_TIME_TO_LIVE() \
    (gLldpGlobalInfo.SysConfigInfo.i4MsgTxInterval * \
     gLldpGlobalInfo.SysConfigInfo.i4MsgTxHoldMultiplier) <= 65535 ? \
        (gLldpGlobalInfo.SysConfigInfo.i4MsgTxInterval * \
         gLldpGlobalInfo.SysConfigInfo.i4MsgTxHoldMultiplier) : 65535 \

  #define  LLDP_NOTIF_INTERVAL() \
    gLldpGlobalInfo.SysConfigInfo.i4NotificationInterval
  
#define  LLDP_IS_STARTED() \
   (LLDP_SYSTEM_CONTROL() == LLDP_START)

#define  LLDP_IS_SHUTDOWN() \
   ((LLDP_SYSTEM_CONTROL() == LLDP_SHUTDOWN) || \
    (LLDP_SYSTEM_CONTROL() == LLDP_SHUTDOWN_INPROGRESS))

/* Extract the TLV Type and TLV Info Length from the given TLV */
    /* ----------------------------------------------------------------------
     * How the macro LLDP_GET_TLV_TYPE_LENGTH works
     * The type and length of a TLV is present in the first 2 bytes of TLV
     * u2TlvHdr = 0x0207
     *             i.e.    00000010 00000111
     * So   Length(9bit) =        0 00000111
     * and  Type(7bit)   = 0000001
     * For Length pad 7bits with 0 in the left-hand side
     * For Type use 9 bit right shifting.
     * So after decoding it comes as:
     *      Length       = 00000000 00000111 (i.e. 0x0007)
     *      Type         = 00000000 00000001 (i.e. 0x0001)
     * ----------------------------------------------------------------------*/
#define LLDP_GET_TLV_TYPE_LENGTH(pu1Tlv, pu2TlvType, pu2TlvLength) \
{\
    UINT2 u2TlvHdr = 0;                           \
    MEMCPY (&u2TlvHdr, pu1Tlv, 2);                \
    u2TlvHdr = OSIX_NTOHS (u2TlvHdr);             \
    *pu2TlvLength = (UINT2)(u2TlvHdr & 0x01FF);   \
    *pu2TlvType = (UINT2)(u2TlvHdr >> 9);         \
}

/* TLV Discard related Macros - 
 * During Frame Processing if one TLV needs to be discarded then Mark it as 
 * discard by using LLDP_RX_DISCARD_TLV() , similarly during updation check 
 * whether the current TLV is marked as discard by using 
 * LLDP_RX_IS_SET_DISCARD_TLV. If it is marked as discard, skip that TLV */

/* Mark the TLV as Discard - Set the entire info field with NULL */
#define LLDP_RX_DISCARD_TLV(pu1Tlv, u2TlvLen) \
    MEMSET(pu1Tlv + LLDP_TLV_HEADER_LEN, 0, u2TlvLen)\
/* Check if the current TLV is marked as Discard */
#define LLDP_RX_IS_SET_DISCARD_TLV(pu1Tlv, u2TlvLen, bResult) \
{\
    UINT1 au1TlvInfo[LLDP_MAX_LEN_TLV_VALUE_FIELD];\
    MEMSET (&au1TlvInfo[0], 0, LLDP_MAX_LEN_TLV_VALUE_FIELD);\
    if (MEMCMP (pu1Tlv + LLDP_TLV_HEADER_LEN, &au1TlvInfo[0], u2TlvLen) == 0)\
    {\
        bResult = OSIX_TRUE;\
    }\
    else\
    {\
        bResult = OSIX_FALSE;\
    }\
}
#define LLDP_SET_TLV_RCVD_BMP(pLocPortInfo, u2Bmp) \
    pLocPortInfo->u2TlvRcvdBmp = (pLocPortInfo->u2TlvRcvdBmp | u2Bmp)\

#define LLDP_IS_SET_TLV_RCVD_BMP(pLocPortInfo, u2Bmp) \
((pLocPortInfo->u2TlvRcvdBmp & u2Bmp)?OSIX_TRUE : OSIX_FALSE)

#define LLDP_LBUF_GET_1_BYTE(pBuf, u4Offset, u1Value) \
{\
    u1Value = pBuf[u4Offset]; \
    pBuf += 1;\
}
#define LLDP_LBUF_GET_2_BYTE(pBuf, u4Offset, u2Value) \
{\
    UINT2 u2TemVal = 0; \
    MEMCPY (&u2TemVal, pBuf + u4Offset, 2);\
    u2Value = (UINT2) (OSIX_NTOHS(u2TemVal));\
    pBuf += 2;\
}
#define LLDP_LBUF_GET_4_BYTE(pBuf, u4Offset, u4Value) \
{\
    UINT4 u4TmpVal = 0; \
    MEMCPY (&u4TmpVal, pBuf + u4Offset, 4);\
    u4Value = OSIX_NTOHL (u4TmpVal);\
    pBuf += 4;\
}
#define LLDP_LBUF_GET_STRING(pBuf, pu1_StringMemArea, u4Offset, u2_StringLength) \
{\
    MEMCPY (pu1_StringMemArea, pBuf + u4Offset, u2_StringLength);\
    pBuf += (UINT1)u2_StringLength;\
}
                                                                                
#define LLDP_GET_NEXT_FREE_REM_INDEX() (gLldpGlobalInfo.i4NextFreeRemIndex)
#define LLDP_UPDT_NEXT_FREE_REM_INDEX() (gLldpGlobalInfo.i4NextFreeRemIndex++)

#define LLDP_GET_NEXT_FREE_REM_ORG_INDEX()\
    (gLldpGlobalInfo.i4NextFreeRemOrgDeInfoIndex)

#define LLDP_UPDT_NEXT_FREE_REM_ORG_INDEX() \
    (gLldpGlobalInfo.i4NextFreeRemOrgDeInfoIndex++)

#define LLDP_IS_NODE_FOR_NEIGHBOR(pRemoteNode, pCurrNode) \
(((pRemoteNode->u4RemLastUpdateTime == pCurrNode->u4RemLastUpdateTime) &&\
 (pRemoteNode->i4RemLocalPortNum ==  pCurrNode->i4RemLocalPortNum ) &&\
 (pRemoteNode->i4RemIndex == pCurrNode->i4RemIndex)) ? OSIX_TRUE : OSIX_FALSE)


#define LLDP_GET_ORG_DEF_INFO_STR_LEN(u2TlvInfoLen)\
    (u2TlvInfoLen - (LLDP_MAX_LEN_OUI + LLDP_TLV_SUBTYPE_LEN ))

/******************************************************************************/
/* LLDP Tx Module Related Macros used to assign fields to buffer              */
/******************************************************************************/
#define LLDP_PUT_1BYTE(pu1PktBuf, u1Val)    \
    *pu1PktBuf = u1Val;                     \
    pu1PktBuf += 1;

#define LLDP_PUT_2BYTE(pu1PktBuf, u2Val)    \
    u2Val = (UINT2)OSIX_HTONS(u2Val);       \
    MEMCPY (pu1PktBuf, &u2Val, 2);          \
    pu1PktBuf += 2;

#define LLDP_PUT_4BYTE(pu1PktBuf, u4Val)    \
    u4Val = OSIX_HTONL(u4Val);              \
    MEMCPY (pu1PktBuf, &u4Val, 4);          \
    pu1PktBuf += 4;

#define LLDP_PUT_MAC_ADDR(pu1PktBuf, Value) \
    MEMCPY (pu1PktBuf, &Value, 6);          \
    pu1PktBuf += 6;

#define LLDP_INCR_BUF_PTR(putBuffPtr, u2Value) \
    putBuffPtr = putBuffPtr + u2Value;

/* max pdu size exceeded is TRUE 
 * if (Buffer + (TlvLength + End of Pdu Tlv Length) > Max Pdu Size */
#define LLDP_MAX_PDU_SIZE_EXCEEDED(pu1InitialPtr, pu1BuffPtr, TlvLength) \
    ((((pu1BuffPtr + TlvLength + LLDP_END_OF_LLDPDU_TLV_LEN) - \
       pu1InitialPtr) > LLDP_MAX_PDU_SIZE)? OSIX_TRUE : OSIX_FALSE)

    
#define LLDP_GET_PROTO_VLAN_CAPAB_AND_STATUS(pPortInfo, pu1Flag)             \
    *pu1Flag = (*pu1Flag | pPortInfo->bProtoVlanEnabled);                   \
    *pu1Flag = (*pu1Flag | gLldpGlobalInfo.bProtoVlanSupported);

#define LLDP_GET_AUTO_NEG_SUPPORT_AND_STATUS(pPortInfo, pu1Flag)             \
    *pu1Flag = (*pu1Flag |                                                   \
                pPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo.u1AutoNegSupport); \
    *pu1Flag = (*pu1Flag |                                                   \
                pPortInfo->Dot3LocPortInfo.Dot3AutoNegInfo.u1AutoNegEnabled);

#define LLDP_GET_PORT_DESC_TLV_INFO_LEN(pu2Len, pPortInfo)                   \
    *pu2Len = (UINT2) LLDP_STRLEN (pPortInfo->au1LocPortDesc, \
                                   LLDP_MAX_LEN_PORTDESC)
                       
#define LLDP_GET_SYS_NAME_TLV_INFO_LEN(pu2Len)                               \
    *pu2Len = (UINT2) LLDP_STRLEN (gLldpGlobalInfo.LocSysInfo.au1LocSysName, \
                                   LLDP_MAX_LEN_SYSNAME)

#define LLDP_GET_SYS_DESC_TLV_INFO_LEN(pu2Len)                               \
    *pu2Len = (UINT2) LLDP_STRLEN (gLldpGlobalInfo.LocSysInfo.au1LocSysDesc, \
                                   LLDP_MAX_LEN_SYSDESC)

#define LLDP_GET_SYS_CAPAB_TLV_INFO_LEN(pu2Len)                              \
    *pu2Len = sizeof (UINT2) + sizeof (UINT2)

#define LLDP_GET_MAN_ADDR_TLV_INFO_LEN(pu2Len, u1ManAddrStrLen, \
                                       u1ManAddrOidStrLength)    \
    *pu2Len =  (UINT2)(  sizeof (UINT1) /* ManAddr Str Length Field */        \
                       + u1ManAddrStrLen /* ManAddr Subtype + ManAddr Length*/\
                       + LLDP_TLV_SUBTYPE_LEN /* IfSubtype  */                \
                       + sizeof (INT4)        /* IfId */                      \
                       + sizeof (UINT1) +  u1ManAddrOidStrLength              \
                       /* OID String(Oid Length + Oid list) */                \
                       ) 

#define LLDP_GET_VLAN_NAME_TLV_INFO_LEN(pu2Len, VlanNameInfo)                \
    *pu2Len = (UINT2)( LLDP_MAX_LEN_OUI +  LLDP_TLV_SUBTYPE_LEN +            \
                       sizeof (VlanNameInfo.u2VlanId) +                    \
                       sizeof (UINT1) +                                      \
                       LLDP_STRLEN (VlanNameInfo.au1VlanName, VLAN_STATIC_MAX_NAME_LEN))

#define LLDP_GET_PROTO_ID_TLV_LEN(pu2Len, pProtoId)                          \
    *pu2Len = (UINT2)(LLDP_TLV_HEADER_LEN + LLDP_MAX_LEN_OUI +               \
            LLDP_TLV_SUBTYPE_LEN + sizeof (UINT1) +                          \
            LLDP_STRLEN (pProtoId->au1ProtocolId, LLDP_MAX_LEN_PROTOID))

#define LLDP_GET_VID_USAGE_DIGEST_TLV_LEN(pu2Len)                          \
    *pu2Len = (UINT2)(LLDP_TLV_HEADER_LEN + LLDP_MAX_LEN_OUI +               \
            LLDP_TLV_SUBTYPE_LEN + LLDP_VID_USAGE_DIGEST_LEN)

#define LLDP_GET_MGMT_VID_TLV_LEN(pu2Len)                          \
    *pu2Len = (UINT2)(LLDP_TLV_HEADER_LEN + LLDP_MAX_LEN_OUI +               \
            LLDP_TLV_SUBTYPE_LEN + LLDP_MGMT_VID_LEN)


/* LLDP-MED */

#define LLDP_MED_GET_LOC_ID_INFO_LEN(pu2Len, pLocIdInfo)                    \
    *pu2Len = (UINT2) (LLDP_STRLEN                                          \
            (pLocIdInfo->au1LocationID,                                     \
             LLDP_MED_MAX_LOC_LENGTH) + LLDP_MED_LOC_DATA_FORMAT_LEN +          \
             LLDP_MAX_LEN_OUI + LLDP_TLV_SUBTYPE_LEN)                       \

#define LLDP_MED_GET_HW_REV_TLV_INFO_LEN(pu2Len)                            \
    *pu2Len = (UINT2) (LLDP_STRLEN                                          \
            (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.au1MedLocHardwareRev,    \
             LLDP_MED_HW_REV_LEN) + LLDP_MAX_LEN_OUI + LLDP_TLV_SUBTYPE_LEN)

#define LLDP_MED_GET_FW_REV_TLV_INFO_LEN(pu2Len)                            \
    *pu2Len = (UINT2) (LLDP_STRLEN                                          \
            (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.au1MedLocFirmwareRev,    \
             LLDP_MED_FW_REV_LEN) + LLDP_MAX_LEN_OUI + LLDP_TLV_SUBTYPE_LEN)

#define LLDP_MED_GET_SW_REV_TLV_INFO_LEN(pu2Len)                            \
    *pu2Len = (UINT2) (LLDP_STRLEN                                          \
            (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.au1MedLocSoftwareRev,    \
             LLDP_MED_SW_REV_LEN) + LLDP_MAX_LEN_OUI + LLDP_TLV_SUBTYPE_LEN)

#define LLDP_MED_GET_SER_NUM_TLV_INFO_LEN(pu2Len)                           \
    *pu2Len = (UINT2) (LLDP_STRLEN                                          \
            (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.au1MedLocSerialNum,      \
             LLDP_MED_SER_NUM_LEN) + LLDP_MAX_LEN_OUI + LLDP_TLV_SUBTYPE_LEN)

#define LLDP_MED_GET_MFG_NAME_TLV_INFO_LEN(pu2Len)                          \
    *pu2Len = (UINT2) (LLDP_STRLEN                                          \
            (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.au1MedLocMfgName,        \
             LLDP_MED_MFG_NAME_LEN) + LLDP_MAX_LEN_OUI + LLDP_TLV_SUBTYPE_LEN)

#define LLDP_MED_GET_MODEL_NAME_TLV_INFO_LEN(pu2Len)                        \
    *pu2Len = (UINT2) (LLDP_STRLEN                                          \
            (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.au1MedLocModelName,      \
             LLDP_MED_MODEL_NAME_LEN) + LLDP_MAX_LEN_OUI + LLDP_TLV_SUBTYPE_LEN)

#define LLDP_MED_GET_ASSET_ID_TLV_INFO_LEN(pu2Len)                          \
    *pu2Len = (UINT2) (LLDP_STRLEN                                          \
            (gLldpGlobalInfo.LocSysInfo.LocInventoryInfo.au1MedLocAssetId,        \
             LLDP_MED_ASSET_ID_LEN) + LLDP_MAX_LEN_OUI + LLDP_TLV_SUBTYPE_LEN)

#define LLDP_MED_SET_TLV_RCVD_BMP(pLocPortInfo, u2Bmp)                      \
    pLocPortInfo->u2MedTlvRcvdBmp = (pLocPortInfo->u2MedTlvRcvdBmp | u2Bmp) 

#define LLDP_MED_IS_TLV_RCVD_BMP_SET(pLocPortInfo, u2Bmp)                   \
((pLocPortInfo->u2MedTlvRcvdBmp & u2Bmp)?OSIX_TRUE : OSIX_FALSE)

#define LLDP_MED_IS_APP_TYPE_BMP_SET(pLocPortInfo, u2Bmp)                   \
((pLocPortInfo->u2PolicyBmp & u2Bmp)?OSIX_TRUE : OSIX_FALSE)

#define LLDP_MED_SET_APP_TYPE_BMP(pLocPortInfo, u2Bmp)                      \
    pLocPortInfo->u2PolicyBmp = (pLocPortInfo->u2PolicyBmp | u2Bmp)         

#define LLDP_MED_GET_POLICY_BIT(u4PolicyInfo, u2Offset)                     \
    ((u4PolicyInfo & (1 << u2Offset)) ? LLDP_MED_TRUE : LLDP_MED_FALSE) 
/* MACROs defined to increment LLDP-MED trasmit and receive counters */
#define LLDP_MED_INCR_CNTR_TX_FRAMES(pLocPortInfo) \
        pLocPortInfo->MedStatsInfo.u4TxMedFramesTotal++ 

#define LLDP_MED_INCR_CNTR_RX_FRAMES(pLocPortInfo) \
    pLocPortInfo->MedStatsInfo.u4RxMedFramesTotal++; \

#define LLDP_MED_INCR_CNTR_RX_FRAMES_DISCARDED(pLocPortInfo) \
    pLocPortInfo->MedStatsInfo.u4RxMedFramesDiscardedTotal++

#define LLDP_MED_INCR_CNTR_RX_TLVS_DISCARDED(pLocPortInfo, u1TlvSubType) \
{\
    pLocPortInfo->MedStatsInfo.u4RxMedTLVsDiscardedTotal++; \
    if (u1TlvSubType == LLDP_MED_CAPABILITY_SUBTYPE) \
    { \
        pLocPortInfo->MedStatsInfo.u4RxMedCapTLVsDiscarded++; \
    } \
    else if (u1TlvSubType == LLDP_MED_NW_POLICY_SUBTYPE) \
    { \
        pLocPortInfo->MedStatsInfo.u4RxMedPolicyTLVsDiscarded++; \
    } \
    else if ((u1TlvSubType >= LLDP_MED_HW_REVISION_SUBTYPE) && \
            (u1TlvSubType <= LLDP_MED_ASSET_ID_SUBTYPE)) \
    { \
        pLocPortInfo->MedStatsInfo.u4RxMedInventoryTLVsDiscarded++; \
    } \
    else if (u1TlvSubType == LLDP_MED_LOCATION_ID_SUBTYPE) \
    { \
        pLocPortInfo->MedStatsInfo.u4RxMedLocationTLVsDiscarded++; \
    } \
    else if (u1TlvSubType == LLDP_MED_EXT_PW_MDI_SUBTYPE) \
    { \
        pLocPortInfo->MedStatsInfo.u4RxMedExPowerMDITLVsDiscarded++; \
    } \
}

#define LLDP_GET_LINEAR_BUFF_BYTE_COUNT(pu1BasePtr, pu1BuffPtr, pu4ByteCount) \
            *pu4ByteCount = pu1BuffPtr - pu1BasePtr

/* macro for forming tlv header */
/* assumed bit sequence of tlv header is 
 * 0  0  0  0     0  0  0  0     0  0  0  0     0  0  0  0
 * 15 14 13 12    11 10 9  8     7  6  5  4     3  2  1  0
 * |    7bits           |            9bits               |
 *      type                         length              */
#define LLDP_FORM_TLV_HEADER(u2Type, u2Len, pu2Header)                      \
    *pu2Header = u2Type << (UINT2)LLDP_TLV_LEN_MAX_BITS;                    \
    *pu2Header = *pu2Header | u2Len;
/* --------------------------------------------------------------------------
   How the LLDP_FORM_TLV_HEADER macro works:
       0000 0000 0000 0101 - u2Type     = 5
       0000 0000 0110 0100 - u2Len      = 100(0x64)
       0000 0000 0000 0000 - *pu2Header = 0(initial value)
   step1:
       *pu2Header = u2Type << LLDP_TLV_LEN_MAX_BITS;
       = 0000 0000 0000 0101 << 9bits;
       value in header after executing step1:           
       *pu2Header = 0000 1010 0000 0000

   step2:
       *pu2Header = *pu2Header | u2Len;
       = 0000 1010 0000 0000 | 0000 0000 0110 0100;
       value in header after executing step1:
       0000 1010 0110 0100

   Result:
       0000 1010 0110 0100 - 2660(0x0a64)
   ------------------------------------------------------------------------- */
#ifdef L2RED_WANTED
/* LLDP_SYS_UP_TIME () (UtlGetTimeSinceEpoch () - \
 *                      LLDP_RED_ACTIVE_NODE_UP_SYSTIME())
 *
 * This macro returns the difference between current system time and
 * system time at which active node booted up for the first time. And this
 * difference is equal to the ISS EXE up time of active node. This macro is 
 * used by both active and standby nodes to calculate the remote table change
 * time whenever a remote entry is learnt/synced.
 * 
 * Example,
 * Active node EXE booted up at system time 1000(and at this point of time
 * ISS EXE up time is 0).
 * 
 * Standby node booted up at system time 1100(and at this point of time
 * active node ISS EXE up time is 100, and Standby node ISS EXE up time is 0).
 *
 * Switchover occurs at system time 1200(and at this point of time active node 
 * ISS EXE up time is 200, and Standby node ISS EXE up time is 100).
 *
 * New active node(which was standby before switchover) learns a remote entry
 * at system time 1300, so it calculates the remote table change time as 
 * follows,
 *
 * Current system time - Active node up time = 1300 - 1000 = 300. */

#define  LLDP_SYS_UP_TIME() ((UtlGetTimeSinceEpoch () - \
                             LLDP_RED_ACTIVE_NODE_UP_SYSTIME()) \
                             * SYS_TIME_TICKS_IN_A_SEC)
#else
#define  LLDP_SYS_UP_TIME()   (OsixGetSysUpTime() * SYS_TIME_TICKS_IN_A_SEC)
#endif

#define LLDP_CONVERT_TO_TICKS (pu4Interval)     \
     *pu4Interval = *pu4Interval * SYS_NUM_OF_TIME_UNITS_IN_A_SEC

#define LLDP_GET_TLV_SUB_TYPE (pu1TlvInfo, pu2SubType) \
    MEMCPY (pu2SubType, pu1TlvInfo, LLDP_TLV_SUBTYPE_LEN)


/* Counter update macros - these are all per port basis counters */
#define LLDP_INCR_CNTR_TX_FRAMES(pLocPortInfo) \
    pLocPortInfo->StatsTxPortTable.u4TxFramesTotal++
#define LLDP_TAG_INCR_CNTR_TX_FRAMES(pLocPortInfo) \
    pLocPortInfo->StatsTxPortTable.u4TxTaggedFramesTotal++

#define LLDP_INCR_CNTR_RX_FRAMES_ERRORS(pLocPortInfo) \
pLocPortInfo->StatsRxPortTable.u4FramesErrors++

#define LLDP_INCR_CNTR_RX_FRAMES(pLocPortInfo) \
pLocPortInfo->StatsRxPortTable.u4RxFramesTotal++;

#define LLDP_INCR_CNTR_RX_TLVS_DISCARDED(pLocPortInfo) \
pLocPortInfo->StatsRxPortTable.u4TLVsDiscardedTotal++

#define LLDP_INCR_CNTR_RX_TLVS_UNRECOGNIZED(pLocPortInfo) \
pLocPortInfo->StatsRxPortTable.u4TLVsUnrecognizedTotal++

#define LLDP_INCR_CNTR_RX_AGEOUTS(pLocPortInfo) \
pLocPortInfo->StatsRxPortTable.u4AgeoutsTotal++

#define LLDP_INCR_CNTR_RX_FRAMES_DISCARDED(pLocPortInfo) \
    pLocPortInfo->StatsRxPortTable.i4FramesDiscardedTotal++

#define LLDP_RESET_CNTR_TX_FRAMES(pLocPortInfo) \
{\
    pLocPortInfo->StatsTxPortTable.u4TxFramesTotal = 0; \
    pLocPortInfo->MedStatsInfo.u4TxMedFramesTotal = 0;\
}

/* Reset the counters Only per port basis counters */
#define LLDP_RESET_ALL_COUNTERS(pLocPortInfo) \
{\
    pLocPortInfo->StatsRxPortTable.u4FramesErrors  = 0;\
    pLocPortInfo->StatsRxPortTable.u4RxFramesTotal = 0;\
    pLocPortInfo->StatsRxPortTable.u4TLVsDiscardedTotal = 0;\
    pLocPortInfo->StatsRxPortTable.u4TLVsUnrecognizedTotal = 0;\
    pLocPortInfo->StatsRxPortTable.u4AgeoutsTotal = 0;\
    pLocPortInfo->StatsRxPortTable.i4FramesDiscardedTotal = 0;\
    pLocPortInfo->MedStatsInfo.u4RxMedFramesTotal = 0; \
    pLocPortInfo->MedStatsInfo.u4RxMedFramesDiscardedTotal = 0; \
    pLocPortInfo->MedStatsInfo.u4RxMedTLVsDiscardedTotal = 0; \
    pLocPortInfo->MedStatsInfo.u4RxMedCapTLVsDiscarded = 0; \
    pLocPortInfo->MedStatsInfo.u4RxMedPolicyTLVsDiscarded = 0; \
    pLocPortInfo->MedStatsInfo.u4RxMedInventoryTLVsDiscarded = 0; \
    pLocPortInfo->MedStatsInfo.u4RxMedLocationTLVsDiscarded = 0; \
    pLocPortInfo->MedStatsInfo.u4RxMedExPowerMDITLVsDiscarded = 0; \
}

/* Rx Statistic macros - Global statistics */
#define LLDP_REMOTE_STAT_TABLES_INSERTS \
gLldpGlobalInfo.StatsRemTabInfo.u4Inserts/* New Neighbor learnt. */

#define LLDP_REMOTE_STAT_TABLES_DELETES \
gLldpGlobalInfo.StatsRemTabInfo.u4Deletes /* Existing Neighbor information 
                                             is completely removed */

#define LLDP_REMOTE_STAT_TABLES_DROPS \
gLldpGlobalInfo.StatsRemTabInfo.u4Drops /* Not able to store the Remote 
                                           Information Due to insufficient 
                                           Space -too many neighbor situation */

#define LLDP_REMOTE_STAT_TABLES_AGEOUTS \
gLldpGlobalInfo.StatsRemTabInfo.u4AgeOuts /* Information Aged out */

#define LLDP_REMOTE_STAT_TABLES_UPDATES \
gLldpGlobalInfo.StatsRemTabInfo.u4Updates /* Remote table updates */

#define LLDP_REMOTE_STAT_LAST_CHNG_TIME \
gLldpGlobalInfo.StatsRemTabInfo.u4LastChgTime
/* SysUpTime is stored whenever 
 * LLDP_REMOTE_STAT_TABLES_INSERTS,
 * LLDP_REMOTE_STAT_TABLES_DELETES,
 * LLDP_REMOTE_STAT_TABLES_DROPS and 
 * LLDP_REMOTE_STAT_TABLES_AGEOUTS
 * LLDP_REMOTE_STAT_TABLES_UPDATES
 * are updated.
 */

#define LLDP_IS_UNRECOG_ORGDEF_TLV(u1OrgTlvSubtype) \
((gLldpGlobalInfo.i4Version == LLDP_VERSION_05) ? ((((u1OrgTlvSubtype == 0) || \
 (u1OrgTlvSubtype >= 5)) ? OSIX_TRUE : OSIX_FALSE )) : \
 (((u1OrgTlvSubtype == 0) || (u1OrgTlvSubtype >= 8)) ? OSIX_TRUE : OSIX_FALSE )) 

#define LLDP_IS_UNKNOWN_TLV(u2Tlvtype) \
(((u2TlvType >= 9) && (u2TlvType <= 126)) ? OSIX_TRUE : OSIX_FALSE)

/* LLDP error counters */
/* Total Memory Allocation Failures */ 
#define LLDP_ERR_CNTR_MEM_ALLOC_FAIL gLldpGlobalInfo.i4MemAllocFailure
/* Total Input Queue Overflows (same counter is used for both the queues RxPduQ
 * and AppMsgQ */
#define LLDP_ERR_CNTR_INPUT_Q_OVERFLOW gLldpGlobalInfo.i4InputQOverFlows

#define LLDP_NOTIF_INTVAL_TMR_STATUS() \
    gLldpGlobalInfo.NotifIntervalTmrNode.u1Status

#define LLDP_GLOB_SHUT_WHILE_TMR_STATUS() \
    gLldpGlobalInfo.GlobalShutWhileTmrNode.u1Status

/* This macro is used inorder to check and access array.
 * Array index can not be sizeof of array
 * */
#define   LLDP_MAX_BYTES_MINUS_ONE(u4NoBytes,MAX)   \
          (((u4NoBytes) < MAX) ? (u4NoBytes) : (MAX - 1))
    
#define   LLDP_CHK_NULL_PTR_RET(ptr,RetVal)                   \
          if(ptr == NULL)                                     \
          {                                                   \
              return RetVal;                                  \
          }

/* HITLESS RESTART */
#define LLDP_HR_STATUS()               LldpRedGetHRFlag()
#define LLDP_HR_STATUS_DISABLE         RM_HR_STATUS_DISABLE
#define LLDP_HR_STDY_ST_REQ_RCVD()     gLldpGlobalInfo.u1StdyStReqRcvd
#define LLDP_V1_DEST_MAC_ADDR_INDEX(i4IfIndex) \
                                  LldpV1DestMacAddrIndex (i4IfIndex)
#ifdef DCBX_WANTED
#define             CEE_TLV_PRESENT                     0
#define             IEEE_TLV_PRESENT                    1
#define             CEE_IEEE_TLV_BOTH_PRESENT           (1<<CEE_TLV_PRESENT | 1<<IEEE_TLV_PRESENT)
#endif
#endif /* _LLDMACS_H_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  lldmacs.h                      */
/*-----------------------------------------------------------------------*/

