/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldtxsm.h,v 1.4 2013/03/28 11:45:03 siva Exp $
 *
 * Description: Data structure used by LLDP Tx module State
 *              Event machine.
 *********************************************************************/
#ifndef _LLD_TXSM_H_
#define _LLD_TXSM_H_

PRIVATE VOID LldpTxSmEventIgnore            PROTO ((tLldpLocPortInfo *));
PRIVATE VOID LldpTxSmStateLldpInit          PROTO ((tLldpLocPortInfo *));
PRIVATE VOID LldpTxSmStateIdle              PROTO ((tLldpLocPortInfo *));
PRIVATE VOID LldpTxSmStateShutFrame         PROTO ((tLldpLocPortInfo *));
PRIVATE VOID LldpTxSmStateInfoFrame         PROTO ((tLldpLocPortInfo *));
PRIVATE VOID LldpTxTmrSmEventIgnore         PROTO ((tLldpLocPortInfo *));
PRIVATE VOID LldpTxTmrSmStateIdle           PROTO ((tLldpLocPortInfo *));
PRIVATE VOID LldpTxTmrSmStateLldpInit       PROTO ((tLldpLocPortInfo *));
PRIVATE VOID LldpTxTmrSmEventTmrExpiry      PROTO ((tLldpLocPortInfo *));
PRIVATE VOID LldpTxTmrSmEventTmrTxTick      PROTO ((tLldpLocPortInfo *));
PRIVATE VOID LldpTxTmrSmEventTmrSignalTx    PROTO ((tLldpLocPortInfo *));
PRIVATE VOID LldpTxTmrSmEventTxFastStart    PROTO ((tLldpLocPortInfo *));

/* Index of the Tx Sem Function pointers */
enum {
    A0  = 0,
    A1  = 1,
    A2  = 2,
    A3  = 3,
    A4  = 4,
    LLDP_MAX_TXSEM_FN_PTRS  = 5
};

/* LLDP TX SEM function pointers */
void (*gaLldpTxActionProc[LLDP_MAX_TXSEM_FN_PTRS]) (tLldpLocPortInfo *) =
{                                    /* STATES : */
   /* A0 */ LldpTxSmEventIgnore,     /* Do Noting */
   /* A1 */ LldpTxSmStateLldpInit,   /* LLDP_TX_LLDP_INIT */
   /* A2 */ LldpTxSmStateIdle,       /* LLDP_TX_IDLE */
   /* A3 */ LldpTxSmStateShutFrame,  /* LLDP_TX_SHUT_FRAME */
   /* A4 */ LldpTxSmStateInfoFrame,  /* LLDP_TX_INFO_FRAME */
};

/* LLDP Tx State Event Table */
const UINT1 gau1LldpTxSem[LLDP_TX_MAX_EVENTS][LLDP_TX_MAX_STATES] =
{
/*                               STATES:
 * _____________________________________________________
 *                  |      Tx   |  Tx    | Tx   | Tx    |
 *                  |      Lldp |  Idle  | Shut | Info  |
 *     EVENTS       |      Init |        | Frame| Frame |
 * _____________________________________________________*/
/* PORT_OPER_DOWN   |*/  { A0,     A3,     A0,    A0 },
/* PORT_OPER_UP     |*/  { A2,     A0,     A0,    A0 },
/* TXMOD_ADMIN_UP   |*/  { A2,     A0,     A0,    A0 },
/* TXMOD_ADMIN_DOWN |*/  { A0,     A3,     A0,    A0 },
/* REINIT_DELAY     |*/  { A0,     A0,     A1,    A0 },
/* TX_NOW           |*/  { A0,     A4,     A0,    A0 },
/* --------------------------------------------------------------------------*/
};

enum {
    LLDPV2_B0  = 0,
    LLDPV2_B1  = 1,
    LLDPV2_B2  = 2,
    LLDPV2_B3  = 3,
    LLDPV2_B4  = 4,
    LLDPV2_B5  = 5,
    LLDPV2_B6  = 6,
    LLDP_MAX_TXTMRSEM_FN_PTRS  = 7
};
/* LLDP Tx Timer State Event Table */
const UINT1 gau1LldpTxTmrSem[LLDP_TX_TMR_MAX_EVENTS][LLDP_TX_TMR_MAX_STATES] =
{
/*                               STATES:
 * ___________________________________________________________________________
 *                  |      TxTmr|  TxTmr | TxTmr  | TxTmr   | TxTmr  | TxTmr  |
 *                  |      Lldp |  Idle  | Expiry | TxTick  | Signal | TxFast |
 *     EVENTS       |      Init |        |        |         | Tx     | Start  |
 * ___________________________________________________________________________*/
/* PORT_OPER_DOWN   |*/  { LLDPV2_B0,     LLDPV2_B1,     LLDPV2_B1,       LLDPV2_B1,      LLDPV2_B1,      LLDPV2_B1},
/* PORT_OPER_UP     |*/  { LLDPV2_B2,     LLDPV2_B0,     LLDPV2_B0,       LLDPV2_B0,      LLDPV2_B0,      LLDPV2_B0},
/* TXMOD_ADMIN_UP   |*/  { LLDPV2_B2,     LLDPV2_B0,     LLDPV2_B0,       LLDPV2_B0,      LLDPV2_B0,      LLDPV2_B0},
/* TXMOD_ADMIN_DOWN |*/  { LLDPV2_B0,     LLDPV2_B1,     LLDPV2_B1,       LLDPV2_B1,      LLDPV2_B1,      LLDPV2_B1},
/* TX_DELAY         |*/  { LLDPV2_B0,     LLDPV2_B4,     LLDPV2_B4,       LLDPV2_B0,      LLDPV2_B4,      LLDPV2_B4},
/* MSG_INTVAL       |*/  { LLDPV2_B0,     LLDPV2_B3,     LLDPV2_B0,       LLDPV2_B0,      LLDPV2_B3,      LLDPV2_B3},
/* LOCAL_CHANGE     |*/  { LLDPV2_B0,     LLDPV2_B5,     LLDPV2_B5,       LLDPV2_B5,      LLDPV2_B0,      LLDPV2_B5},
/* NEIGHBOR_DETECTED|*/  { LLDPV2_B0,     LLDPV2_B6,     LLDPV2_B0,       LLDPV2_B6,      LLDPV2_B6,      LLDPV2_B0},
/* --------------------------------------------------------------------------*/
};
/* LLDP TX Timer SEM function pointers */
void (*gaLldpTxTmrActionProc[LLDP_MAX_TXTMRSEM_FN_PTRS]) (tLldpLocPortInfo *) =
{                                    /* STATES : */
   /* LLDPV2_B0 */ LldpTxTmrSmEventIgnore,     /* Do Noting */
   /* LLDPV2_B1 */ LldpTxTmrSmStateLldpInit,   /* LLDP_TX_TMR_LLDP_INIT */
   /* LLDPV2_B2 */ LldpTxTmrSmStateIdle,       /* LLDP_TX_TMR_IDLE */
   /* LLDPV2_B3 */ LldpTxTmrSmEventTmrExpiry,  /* LLDP_TX_TMR_EXPIRY */
   /* LLDPV2_B4 */ LldpTxTmrSmEventTmrTxTick,     /* LLDP_TX_TMR_TX_TICK */
   /* LLDPV2_B5 */ LldpTxTmrSmEventTmrSignalTx,   /* LLDP_TX_TMR_SIGNAL_TX */
   /* LLDPV2_B6 */ LldpTxTmrSmEventTxFastStart,/* LLDP_TX_TMR_TXFAST_START */
};
const CHR1 *gau1LldpTxStateStr[LLDP_TX_MAX_STATES] = {
    "LLDP_TX_LLDP_INIT",
    "LLDP_TX_IDLE",
    "LLDP_TX_SHUT_FRAME",   
    "LLDP_TX_INFO_FRAME",    
};

const CHR1 *gau1LldpTxEvntStr[LLDP_TX_MAX_EVENTS] = {
    "PORT_OPER_DOWN",
    "PORT_OPER_UP",
    "TXMOD_ADMIN_UP",    
    "TXMOD_ADMIN_DOWN",  
    "LLDP_TX_EV_REINIT_DELAY",
    "LLDP_TX_EV_TX_NOW",
};

const CHR1 *gau1LldpTxTmrStateStr[LLDP_TX_TMR_MAX_STATES] = {
    "LLDP_TX_TMR_LLDP_INIT",
    "LLDP_TX_TMR_IDLE",
    "LLDP_TX_TMR_EXPIRY",   
    "LLDP_TX_TMR_TX_TICK",    
    "LLDP_TX_TMR_SIGNAL_TX",    
    "LLDP_TX_TMR_TX_FAST_START",
};

const CHR1 *gau1LldpTxTmrEvntStr[LLDP_TX_TMR_MAX_EVENTS] = {
    "PORT_OPER_DOWN",
    "PORT_OPER_UP",
    "TXMOD_ADMIN_UP",    
    "TXMOD_ADMIN_DOWN",  
    "LLDP_TX_TMR_EV_TX_DELAY",
    "LLDP_TX_TMR_EV_MSG_INTVAL",
    "LLDP_TX_TMR_EV_LOCAL_CHANGE",
    "LLDP_TX_TMR_EV_NEIGHBOR_DETECTED",
};
#endif /* _LLD_TXSM_H_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  lldtxsm.h                      */
/*-----------------------------------------------------------------------*/
