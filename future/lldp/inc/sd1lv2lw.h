/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: sd1lv2lw.h,v 1.2 2013/03/28 11:45:03 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for LldpV2Xdot1ConfigPortVlanTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1ConfigPortVlanTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1ConfigPortVlanTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1ConfigPortVlanTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1ConfigPortVlanTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1ConfigPortVlanTxEnable ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpV2Xdot1ConfigPortVlanTxEnable ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpV2Xdot1ConfigPortVlanTxEnable ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpV2Xdot1ConfigPortVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpV2Xdot1ConfigVlanNameTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1ConfigVlanNameTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1ConfigVlanNameTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1ConfigVlanNameTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1ConfigVlanNameTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1ConfigVlanNameTxEnable ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpV2Xdot1ConfigVlanNameTxEnable ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpV2Xdot1ConfigVlanNameTxEnable ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpV2Xdot1ConfigVlanNameTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpV2Xdot1ConfigProtoVlanTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1ConfigProtoVlanTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1ConfigProtoVlanTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1ConfigProtoVlanTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1ConfigProtoVlanTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1ConfigProtoVlanTxEnable ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpV2Xdot1ConfigProtoVlanTxEnable ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpV2Xdot1ConfigProtoVlanTxEnable ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpV2Xdot1ConfigProtoVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpV2Xdot1ConfigProtocolTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1ConfigProtocolTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1ConfigProtocolTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1ConfigProtocolTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1ConfigProtocolTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1ConfigProtocolTxEnable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpV2Xdot1ConfigProtocolTxEnable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpV2Xdot1ConfigProtocolTxEnable ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpV2Xdot1ConfigProtocolTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpV2Xdot1ConfigVidUsageDigestTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1ConfigVidUsageDigestTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1ConfigVidUsageDigestTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1ConfigVidUsageDigestTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1ConfigVidUsageDigestTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1ConfigVidUsageDigestTxEnable ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpV2Xdot1ConfigVidUsageDigestTxEnable ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpV2Xdot1ConfigVidUsageDigestTxEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpV2Xdot1ConfigVidUsageDigestTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpV2Xdot1ConfigManVidTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1ConfigManVidTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1ConfigManVidTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1ConfigManVidTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1ConfigManVidTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1ConfigManVidTxEnable ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpV2Xdot1ConfigManVidTxEnable ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpV2Xdot1ConfigManVidTxEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpV2Xdot1ConfigManVidTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpV2Xdot1LocTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1LocTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1LocTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1LocTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1LocTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1LocPortVlanId ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpV2Xdot1LocProtoVlanTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1LocProtoVlanTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1LocProtoVlanTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1LocProtoVlanTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1LocProtoVlanTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1LocProtoVlanSupported ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetLldpV2Xdot1LocProtoVlanEnabled ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for LldpV2Xdot1LocVlanNameTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1LocVlanNameTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1LocVlanNameTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1LocVlanNameTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1LocVlanNameTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1LocVlanName ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for LldpV2Xdot1LocProtocolTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1LocProtocolTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1LocProtocolTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1LocProtocolTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1LocProtocolTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1LocProtocolId ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for LldpV2Xdot1LocVidUsageDigestTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1LocVidUsageDigestTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1LocVidUsageDigestTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1LocVidUsageDigestTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1LocVidUsageDigestTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1LocVidUsageDigest ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpV2Xdot1LocManVidTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1LocManVidTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1LocManVidTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1LocManVidTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1LocManVidTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1LocManVid ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpV2Xdot1LocLinkAggTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1LocLinkAggTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1LocLinkAggTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1LocLinkAggTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1LocLinkAggTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1LocLinkAggStatus ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpV2Xdot1LocLinkAggPortId ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpV2Xdot1RemTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1RemTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1RemTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1RemTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1RemTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1RemPortVlanId ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpV2Xdot1RemProtoVlanTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1RemProtoVlanTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1RemProtoVlanTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1RemProtoVlanTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1RemProtoVlanTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1RemProtoVlanSupported ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetLldpV2Xdot1RemProtoVlanEnabled ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for LldpV2Xdot1RemVlanNameTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1RemVlanNameTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1RemVlanNameTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1RemVlanNameTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1RemVlanNameTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1RemVlanName ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for LldpV2Xdot1RemProtocolTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1RemProtocolTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1RemProtocolTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1RemProtocolTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1RemProtocolTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1RemProtocolId ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for LldpV2Xdot1RemVidUsageDigestTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1RemVidUsageDigestTable ARG_LIST((UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1RemVidUsageDigestTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1RemVidUsageDigestTable ARG_LIST((UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1RemVidUsageDigestTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1RemVidUsageDigest ARG_LIST((UINT4  , INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpV2Xdot1RemManVidTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1RemManVidTable ARG_LIST((UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1RemManVidTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1RemManVidTable ARG_LIST((UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1RemManVidTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1RemManVid ARG_LIST((UINT4  , INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpV2Xdot1RemLinkAggTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1RemLinkAggTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1RemLinkAggTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1RemLinkAggTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1RemLinkAggTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1RemLinkAggStatus ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpV2Xdot1RemLinkAggPortId ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,UINT4 *));
