/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: llddefn.h,v 1.44 2017/12/14 10:30:24 siva Exp $
 * 
 * Description: This file contains macros definitions for constants
 *              used in LLDP.
 *********************************************************************/
#ifndef _LLDDEFN_H
#define _LLDDEFN_H

#define LLDP_TASK_NAME                ((const UINT1 *)"LLDP")
#define LLDP_MSG_QUEUE_NAME           "MSGQ"
#define LLDP_RXPDU_QUEUE_NAME         "PDUQ"
#define LLDP_PROTO_SEM                "LLSM"

#if (defined (ISS_WANTED) || defined (RM_WANTED)) && defined (SNMP_2_WANTED)

#define LLDP_MSR_NOTIFY_SCALAR(pu4Oid, u4Len, u4SetVal) LldpUtilIndMsrForFsScalars (pu4Oid, u4Len, u4SetVal)
#else
#define LLDP_MSR_NOTIFY_SCALAR(pu4Oid, u4Len, u4SetVal)\
        {\
            UNUSED_PARAM (pu4Oid);\
            UNUSED_PARAM(u4Len);\
            UNUSED_PARAM(u4SetVal);\
        }
#endif
#if (defined (ISS_WANTED) || defined (RM_WANTED)) && defined (SNMP_2_WANTED)

#define LLDP_MSR_NOTIFY_PORT_CONFIG_TBL(pu4Oid, u4Len, i4SetVal) LldpUtilUptInfoForStdPortConfigTbl (pu4Oid, u4Len, i4SetVal)
#else
#define LLDP_MSR_NOTIFY_PORT_CONFIG_TBL(pu4Oid, u4Len, i4SetVal)\
        {\
            UNUSED_PARAM (pu4Oid);\
            UNUSED_PARAM(u4Len);\
            UNUSED_PARAM(i4SetVal);\
        }
#endif
#if (defined (ISS_WANTED) || defined (RM_WANTED)) && defined (SNMP_2_WANTED)

#define LLDP_MSR_NOTIFY_DOT1_CONFIG_PROTO_VLAN_TBL(pu4Oid, u4Len, i4PortNum, i4Vlan, i4SetVal) \
        LldpUtilIndMsrForDot1ConfigProtoVlanTbl (pu4Oid, u4Len, i4PortNum, i4Vlan, i4SetVal)
#else
#define LLDP_MSR_NOTIFY_DOT1_CONFIG_PROTO_VLAN_TBL(pu4Oid, u4Len, i4PortNum, i4Vlan, i4SetVal)\
        {\
            UNUSED_PARAM (pu4Oid);\
            UNUSED_PARAM(u4Len);\
            UNUSED_PARAM(i4PortNum);\
            UNUSED_PARAM(i4Vlan);\
            UNUSED_PARAM(i4SetVal);\
        }
#endif
#if (defined (ISS_WANTED) || defined (RM_WANTED)) && defined (SNMP_2_WANTED)
#define LLDP_MSR_NOTIFY_DOT1_CONFIG_VLAN_TBL(pu4Oid, u4Len, i4PortNum, i4Vlan, i4SetVal) \
                    LldpUtilIndMsrForDot1ConfigVlanNameTbl (pu4Oid, u4Len, i4PortNum, i4Vlan, i4SetVal)
#else
#define LLDP_MSR_NOTIFY_DOT1_CONFIG_VLAN_TBL(pu4Oid, u4Len, i4PortNum, i4Vlan, i4SetVal)\
        {\
            UNUSED_PARAM (pu4Oid);\
            UNUSED_PARAM(u4Len);\
            UNUSED_PARAM(i4PortNum);\
            UNUSED_PARAM(i4Vlan);\
            UNUSED_PARAM(i4SetVal);\
        }
#endif
#if (defined (ISS_WANTED) || defined (RM_WANTED)) && defined (SNMP_2_WANTED)
#define LLDP_MSR_NOTIFY_DOT3_PORT_CONFIG_TBL(pu4Oid, u4Len, i4PortNum, i4SetVal) \
                   LldpUtilIndMsrForDot3PortConfigTbl (pu4Oid, u4Len, i4PortNum, i4SetVal)
#else
#define LLDP_MSR_NOTIFY_DOT3_PORT_CONFIG_TBL(pu4Oid, u4Len, i4PortNum, i4SetVal)\
        {\
            UNUSED_PARAM (pu4Oid);\
            UNUSED_PARAM(u4Len);\
            UNUSED_PARAM(i4PortNum);\
            UNUSED_PARAM(i4SetVal);\
        }
#endif
#if (defined (ISS_WANTED) || defined (RM_WANTED)) && defined (SNMP_2_WANTED)
#define LLDP_MSR_NOTIFY_DOT1_CONFIG_PORT_VLAN_TBL(pu4Oid, u4Len, i4PortNum, i4SetVal) \
                     LldpUtilIndMsrForDot1ConfigPortVlanTbl (pu4Oid, u4Len, i4PortNum, i4SetVal)
#else
#define LLDP_MSR_NOTIFY_DOT1_CONFIG_PORT_VLAN_TBL(pu4Oid, u4Len, i4PortNum, i4SetVal)\
        {\
            UNUSED_PARAM (pu4Oid);\
            UNUSED_PARAM(u4Len);\
            UNUSED_PARAM(i4PortNum);\
            UNUSED_PARAM(i4SetVal);\
        }
#endif
#define LLDP_ZERO       0
#define LLDP_TWO        2
#define LLDP_FOUR       4
#define LLDP_SEVEN  7
/* The following macro is used to fix the size of the array which isused to store
 * the Management Address TLV details  during parsing of Refresh frame. */
#define LLDP_DUP_MGMT_TLV_CNT  15
/* Events that will be received by LLDP Task */
#define LLDP_TMR_EXPIRY_EVENT         0x01 /* Bit 0 */
#define LLDP_QMSG_EVENT               0x02 /* Bit 1 */
#define LLDP_RXPDU_EVENT              0x04 /* Bit 2 */
#define LLDP_RED_SUB_BULK_UPD_EVENT   0x08 /* Bit 3 */
#define LLDP_ALL_EVENTS               (LLDP_TMR_EXPIRY_EVENT | LLDP_QMSG_EVENT \
                                       | LLDP_RXPDU_EVENT |\
                                       LLDP_RED_SUB_BULK_UPD_EVENT)    
/* Maximum number of interfaces maintained in LLDP */

#define LLDP_MIN_REM_INDEX            1
#define LLDP_MAX_REM_INDEX            2147483647
#define LLDP_MIN_PROTO_INDEX          1
#define LLDP_MAX_PROTO_INDEX          2147483647

#define LLDP_MAX_VLAN_ID              4094 /* VLAN_DEV_MAX_VLAN_ID is not used 
                                            * as it is defined as 4093 in
                                            * bcmchassis.h
                                            */
#define LLDP_SUBTYPE_LEN              1
/* Only the first four bits are used to represent the transmission state 
 * of the TLVs.*/
#define LLDP_DOT3TLV_BMAP             0xF0
#define LLDP_AGG_CAP_BMAP             0x80
#define LLDP_AGG_STATUS_BMAP          0x40
#define LLDP_RESET_AGG_CAP            0x00
#define LLDP_RESET_AGG_STATUS_BMAP    0x80
/* These macros are used while decoding the LLDPDU */
#define LLDP_AUTO_NEG_SUPP_BMAP       0x01
#define LLDP_AUTO_NEG_STAT_BMAP       0x02
#define LLDP_PROTO_VLAN_SUPP_BMAP     0x02
#define LLDP_PROTO_VLAN_STAT_BMAP     0x04


/* Each byte(8bits) represents 8 ports, hence the following macro 
 * represents (maximum number of ports supported)/8 */
#define LLDP_MAN_ADDR_TX_EN_MAX_BYTES  MAX_LLDP_LOC_PORT_TABLE 


/* Invalid value */
#define LLDP_INVALID_VALUE            -1

#define LLDP_IS_AGG_CAP              0x80
#define LLDP_IS_AGG_PORT_ACTIVE      0x40

/*LLDP ROW STATUS VALUES */
#define   LLDP_ACTIVE                      ACTIVE
#define   LLDP_NOT_IN_SERVICE              NOT_IN_SERVICE
#define   LLDP_NOT_READY                   NOT_READY
#define   LLDP_CREATE_AND_GO               CREATE_AND_GO
#define   LLDP_CREATE_AND_WAIT             CREATE_AND_WAIT
#define   LLDP_DESTROY                     DESTROY

/****************************************************************************/
/* LLDP packet related constants                                            */
/****************************************************************************/

#define LLDP_MAX_FRAME_SIZE                 1520
#define LLDP_MIN_PDU_SIZE                   28 /* size of all mandatory TLVs
                                                  with minimum no. of bytes */
#define LLDP_MIN_FRAME_SIZE                 CFA_ENET_V2_HEADER_SIZE + \
                                            LLDP_MIN_PDU_SIZE 

#define LLDP_ENET_DEST_SRC_ADDR_SIZE        12
#define LLDP_SNAP_ENCODED_PDU_OFFSET        LLDP_ENET_DEST_SRC_ADDR_SIZE +  \
                                            LLDP_MAX_LEN_SNAP_ENCAPTYPE

#define LLDP_MAX_LEN_MCAST_ADDR             6
#define LLDP_MAX_LEN_ENETV2_ENCAPTYPE       2
#define LLDP_MAX_LEN_SNAP_ENCAPTYPE         8
#define LLDP_MAX_LEN_ENETV2_HEADER          14
#define LLDP_MAX_LEN_STAG_ENETV2_HEADER     18
#define LLDP_MAX_LEN_SNAP_HEADER            20 
#define LLDP_DEST_MCAST_MAC_ADDR     { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x0E }
/****************************************************************************/
/*    LLDP RB Tree related constants                                        */
/****************************************************************************/
#define LLDP_RB_GREATER                     1
#define LLDP_RB_EQUAL                       0
#define LLDP_RB_LESS                        -1

/****************************************************************************/
/*    lldtxutl.c related constants                                          */
/****************************************************************************/
/* constant specifying the percentage of jitter */
#define  LLDP_JITTER_PERCENT                25
                                            
#define LLDP_SHUTDOWN_FRAME                 1
#define LLDP_INFO_FRAME                     2

#define LLDP_TLV_VLAN_ID_LEN                2
#define LLDP_TLV_VLAN_NAME_LEN_FEILD        1
                                            
#define LLDP_TLV_SUBTYPE_LEN                1
#define LLDP_TLV_HEADER_LEN                 2
                                            
#define LLDP_TLV_HEADER_TLVTYPE             1
#define LLDP_TLV_HEADER_TLVLEN              2
 
#define LLDP_TLV_TYPE_MAX_BITS              7
#define LLDP_TLV_LEN_MAX_BITS               9

#define LLDP_DOT1_TLV                       1
#define LLDP_DOT3_TLV                       2

#define LLDP_VID_USAGE_DIGEST_LEN           4 
#define LLDP_VID_USAGE_DIGEST_STRLEN        8
#define LLDP_MGMT_VID_LEN                   2
#define LLDP_MGMT_VID_STRLEN                6

#define LLDP_END_OF_LLDPDU_TLV_INFO_LEN     0
#define LLDP_END_OF_LLDPDU_TLV_LEN          \
 LLDP_END_OF_LLDPDU_TLV_INFO_LEN + LLDP_TLV_HEADER_LEN
 
#define LLDP_TTL_TLV_INFO_LEN               2
#define LLDP_TTL_TLV_LEN                    \
 LLDP_TTL_TLV_INFO_LEN + LLDP_TLV_HEADER_LEN
 
#define LLDP_SYS_CAPAB_TLV_INFO_LEN         4
#define LLDP_SYS_CAPAB_TLV_LEN              \
 LLDP_SYS_CAPAB_TLV_INFO_LEN + LLDP_TLV_HEADER_LEN
 
#define LLDP_MIN_MAN_ADDR_INFO_LEN          9
#define LLDP_MAX_MAN_ADDR_INFO_LEN          167 
 
#define LLDP_PVID_TLV_INFO_LEN              6
#define LLDP_PVID_TLV_LEN                   \
 LLDP_PVID_TLV_INFO_LEN + LLDP_TLV_HEADER_LEN

#define LLDP_PPVID_TLV_INFO_LEN             7
#define LLDP_PPVID_TLV_LEN                  \
 LLDP_PPVID_TLV_INFO_LEN + LLDP_TLV_HEADER_LEN

#define LLDP_MAX_VLAN_NAME_INFO_LEN         39
#define LLDP_MIN_VLAN_NAME_INFO_LEN          7
 
#define LLDP_MAC_PHY_TLV_INFO_LEN           9
#define LLDP_MAC_PHY_TLV_LEN                \
 LLDP_MAC_PHY_TLV_INFO_LEN + LLDP_TLV_HEADER_LEN

#define LLDP_PWR_VIA_MDI_TLV_INFO_LEN       7
#define LLDP_PWR_VIA_MDI_TLV_LEN            \
 LLDP_PWR_VIA_MDI_TLV_INFO_LEN + LLDP_TLV_HEADER_LEN

#define LLDP_LINK_AGG_TLV_INFO_LEN          9
#define LLDP_LINK_AGG_TLV_LEN               \
 LLDP_LINK_AGG_TLV_INFO_LEN + LLDP_TLV_HEADER_LEN

#define LLDP_MAX_FRAME_SIZE_TLV_INFO_LEN    6
#define LLDP_MAX_FRAME_SIZE_TLV_LEN         \
 LLDP_MAX_FRAME_SIZE_TLV_INFO_LEN + LLDP_TLV_HEADER_LEN

#define LLDP_ORG_DEF_UNRECOG_TLV_INFO_LEN   511
#define LLDP_ORG_DEF_UNRECOG_TLV_LEN        \
 LLDP_ORG_DEF_UNRECOG_TLV_INFO_LEN + LLDP_TLV_HEADER_LEN

/* LLDP-MED related MACROs definition */

#define LLDP_MED_TLV_ADD                   1
#define LLDP_MED_TLV_DEL                   0
#define LLDP_MED_ADD_POLICY                1
#define LLDP_MED_DEL_POLICY                0

#define LLDP_MED_VLAN_TAGGED               1
#define LLDP_MED_VLAN_UNTAGGED             0

#define LLDP_MED_UNKNOWN_FLAG              1

#define LLDP_MED_MIN_APP_TYPE              1
#define LLDP_MED_MAX_APP_TYPE              8

#define LLDP_MED_MIN_POW_TYPE              0x00
#define LLDP_MED_MAX_POW_TYPE              0x03

#define LLDP_MED_MIN_POW_SRC               0x00
#define LLDP_MED_MAX_POW_SRC               0x03

#define LLDP_MED_MAX_POW_PRI               0x03

/* LLDP-MED Power Device Types */
#define LLDP_MED_PSE_DEVICE                0x00
#define LLDP_MED_PD_DEVICE                 0x01
#define LLDP_MED_DEFAULT_DEVICE            LLDP_MED_PSE_DEVICE
#define LLDP_MED_POWER_VALUE               1000

/* LLDP-MED PSE Power Source types */
#define LLDP_MED_PSE_UNKNOWN               0x00
#define LLDP_MED_PSE_PRIMARY               0x01
#define LLDP_MED_PSE_BACK_UP               0x02
#define LLDP_MED_PSE_RESERVED              0x03
#define LLDP_MED_DEFAULT_PSE_SRC           LLDP_MED_PSE_PRIMARY

/* LLDP-MED PD Power Source types */
#define LLDP_MED_PD_UNKNOWN                0x00
#define LLDP_MED_PD_PSE                    0x01
#define LLDP_MED_PD_LOCAL                  0x02
#define LLDP_MED_PD_PSE_LOCAL              0x03

/* LLDP MED Priority Types */
#define LLDP_MED_PRI_UNKNOWN               0x0000 /* Unknown Priority */
#define LLDP_MED_PRI_CRITICAL              0x0001 /* Critical Priority */
#define LLDP_MED_PRI_HIGH                  0x0002 /* High Priority */
#define LLDP_MED_PRI_LOW                   0x0003 /* Low Priority */

#define LLDP_MED_MAX_POWER_VAL             1023

/* TLV length = Header Length + Info Length (In Octets) */
#define LLDP_MED_CAPABILITY_TLV_INFO_LEN    7
#define LLDP_MED_CAPABILITY_TLV_LEN      \
  LLDP_MED_CAPABILITY_TLV_INFO_LEN + LLDP_TLV_HEADER_LEN

#define LLDP_MED_NW_POLICY_INFO_LEN         8
#define LLDP_MED_NW_POLICY_TLV_LEN      \
    LLDP_MED_NW_POLICY_INFO_LEN + LLDP_TLV_HEADER_LEN

#define LLDP_MED_POWER_TLV_INFO_LEN         7
#define LLDP_MED_POWER_TLV_LEN          \
    LLDP_MED_POWER_TLV_INFO_LEN + LLDP_TLV_HEADER_LEN

/* Maximum length of Inventory TLV */
#define LLDP_MED_INVENTORY_TLV_LEN          36

/* Maximum length of Location Identification TLV */
#define LLDP_MED_LOCATION_ID_TLV_LEN        261

#define LLDP_MED_MIN_ELIN_TLV_LENGTH        15
#define LLDP_MED_MAX_ELIN_TLV_LENGTH        30

#define LLDP_MED_COORDINATE_TLV_LENGTH      21

#define LLDP_MED_MIN_CIVIC_TLV_LENGTH       11

/* Range of LLDP-MED TLV Subtypes */
#define LLDP_MED_MIN_SUB_TYPE               1
#define LLDP_MED_MAX_SUB_TYPE               11

/* Macros defined for range validations */
#define LLDP_MED_MAX_LEN_TLV_TXENABLE       2
#define LLDP_MED_MAX_LEN_APP_TYPE           1
#define LLDP_MIN_VLAN_ID                    1
#define LLDP_MED_MIN_PRIORITY               0
#define LLDP_MED_MAX_PRIORITY               7
#define LLDP_MED_MIN_DSCP                   0
#define LLDP_MED_MAX_DSCP                   63

/* Bit values used for LLDP_MED_IS_TLV_RCVD_BMP_SET to 
 * verify the duplicate TLVs present in PDU */
#define LLDP_MED_RECV_MED_CAP_TLV           0x0001
#define LLDP_MED_RECV_INVENTORY_TLV         0x0002
#define LLDP_MED_RECV_COORDINATE_LOC_TLV    0x0004
#define LLDP_MED_RECV_EXT_PW_PDI_TLV        0x0008
#define LLDP_MED_RECV_HW_REV_TLV            0x0010
#define LLDP_MED_RECV_FW_REV_TLV            0x0020
#define LLDP_MED_RECV_SW_REV_TLV            0x0040
#define LLDP_MED_RECV_SER_NUM_TLV           0x0080
#define LLDP_MED_RECV_MFG_NAME_TLV          0x0100
#define LLDP_MED_RECV_MODEL_NAME_TLV        0x0200
#define LLDP_MED_RECV_ASSET_ID_TLV          0x0400
#define LLDP_MED_RECV_CIVIC_LOC_TLV         0x0800
#define LLDP_MED_RECV_ELIN_LOC_TLV          0x1000
#define LLDP_MED_INVENTORY_SET_BMP          0x07F0

/* Bit values used for LLDP_MED_IS_APP_TYPE_BMP_SET to
 * verify Duplicate Network Policy TLVs in PDU */
#define LLDP_MED_NW_POL_VOICE               0x0001
#define LLDP_MED_NW_POL_VOICE_SIG           0x0002
#define LLDP_MED_NW_POL_GUEST_VOICE         0x0004
#define LLDP_MED_NW_POL_GUEST_VOICE_SIG     0x0008
#define LLDP_MED_NW_POL_SOFT_PHN_VOICE      0x0010
#define LLDP_MED_NW_POL_VIDEO_CONF          0x0020
#define LLDP_MED_NW_POL_STREAM_VIDEO        0x0040
#define LLDP_MED_NW_POL_VIDEO_SIG           0x0080

/* Bit Mask to decode all fields from Network Policy
 * Information of size 4 Octets */
#define LLDP_MED_APP_TYPE_BIT_MASK          0xFF000000
#define LLDP_MED_UNKNOWN_BIT_MASK           0x00800000
#define LLDP_MED_TAGGED_BIT_MASK            0x00400000
#define LLDP_MED_VLAN_ID_BIT_MASK           0x001FFE00
#define LLDP_MED_PRIORITY_BIT_MASK          0x000001C0
#define LLDP_MED_DSCP_BIT_MASK              0x0000003F
#define LLDP_MED_APP_TYPE_SHIFT_BIT         24
#define LLDP_MED_UNKNOWN_SHIFT_BIT          23
#define LLDP_MED_TAGGED_SHIFT_BIT           22
#define LLDP_MED_VLAN_ID_SHIFT_BIT          9
#define LLDP_MED_PRIORITY_SHIFT_BIT         6

/* Bit shifts for Power Tlv */
#define LLDP_MED_POWER_TYPE_BIT_MASK        0xC0
#define LLDP_MED_POWER_SRC_BIT_MASK         0x30
#define LLDP_MED_POWER_PRI_BIT_MASK         0x0F
#define LLDP_MED_POWER_TYPE_SHIFT_BIT       6
#define LLDP_MED_POWER_SRC_SHIFT_BIT        4


/* Clear Counters */
#define LLDP_MED_SET_CLEAR_COUNTER          1
#define LLDP_MED_NO_SET_CLEAR_COUNTER       0

#define LLDP_SET_CLEAR_COUNTER              1
#define LLDP_NO_SET_CLEAR_COUNTER           0

/* lldtx.c related definitions */
#define LLDP_MAX_ENETV2_HEADER_LEN          \
 LLDP_MAX_LEN_MCAST_ADDR + MAC_ADDR_LEN +   \
 LLDP_MAX_LEN_ENETV2_ENCAPTYPE

#define LLDP_MAX_SNAP_HEADER_LEN            \
 LLDP_MAX_LEN_MCAST_ADDR + MAC_ADDR_LEN +   \
 LLDP_MAX_LEN_SNAP_ENCAPTYPE

#define LLDP_MIN_MAN_ADDR_SUBTYPE           LLDP_MAN_ADDR_IF_SUB_UNKNOWN
#define LLDP_MAX_MAN_ADDR_SUBTYPE           LLDP_MAN_ADDR_IF_SUB_SYSPORTNUM
#define LLDP_MIN_LEN_MAN_ADDR               1
#define LLDP_MIN_UNKNOWN_TLV_TYPE           8 /*Changing Min Unknown TLV type as 9 from 8, As 8 is a SUB_TYPE for ETS TC supported TLV */
#define LLDP_MAX_UNKNOWN_TLV_TYPE           126
#define LLDP_MIN_LEN_OUI                    3
#define LLDP_MIN_LEN_ORG_DEF_INFO_SUBTYPE   1
#define LLDP_MAX_LEN_ORG_DEF_INFO_SUBTYPE   255
#define LLDP_MIN_LEN_ORG_DEF_INFO_INDEX     1
#define LLDP_MAX_LEN_ORG_DEF_INFO_INDEX     2147483647
/* The following Bitmask are used to Flag the bit which
 * describe whether the TLV is already received */
#define LLDP_TLV_RCVD_CASSIS_ID_BMP         0x0001
#define LLDP_TLV_RCVD_PORT_ID_BMP           0x0002
#define LLDP_TLV_RCVD_TTL_BMP               0x0004
#define LLDP_TLV_RCVD_PORT_DESCR_BMP        0x0008
#define LLDP_TLV_RCVD_SYS_NAME_BMP          0x0010
#define LLDP_TLV_RCVD_SYS_DESCR_BMP         0x0020
#define LLDP_TLV_RCVD_SYS_CAPA_BMP          0x0040
#define LLDP_TLV_RCVD_PVID_BMP              0x0080
#define LLDP_TLV_RCVD_PPVID_BMP             0x0100
#define LLDP_TLV_RCVD_VLAN_NAME_BMP         0x0200
#define LLDP_TLV_RCVD_PROTO_ID_BMP          0x0400
#define LLDP_TLV_RCVD_MAC_PHY_BMP           0x0800
#define LLDP_TLV_RCVD_POE_BMP               0x1000
#define LLDP_TLV_RCVD_LINK_AGG_BMP          0x2000
#define LLDP_TLV_RCVD_MAX_FRAME_BMP         0x4000
                                            
/* lldtxutl.c related constants */          
#define LLDP_PROTOVLAN_ENABLED              0x04
#define LLDP_PROTOVLAN_SUPPORTED            0x02
                                            
#define LLDP_AUTONEG_SUPPORTED              0x01
#define LLDP_AUTONEG_ENABLED                0x02
/* stdlldlw.c related constants */          
/* nmh related constants */                 
#define LLDP_MIN_MSG_INTERVAL               5
#define LLDP_MAX_MSG_INTERVAL               32768
#define LLDP_MIN_HOLD_COUNT                 2
#define LLDP_MAX_HOLD_COUNT                 10
#define LLDP_MIN_REINIT_DELAY               1
#define LLDP_MAX_REINIT_DELAY               10
#define LLDP_MIN_TXDELAY                    1
#define LLDP_MAX_TXDELAY                    8192
#define LLDP_MIN_NOTIFY_INTVAL              5
#define LLDP_MAX_NOTIFY_INTVAL              3600
#define LLDP_MIN_BASIC_TLV_TXENABLE         0
#define LLDP_MAX_BASIC_TLV_TXENABLE         0xF0/* 0xF0 - 1111 0000 */
#define LLDP_MAX_LEN_BASIC_TLV_TXENABLE     1
#define LLDP_MAX_DEST_ADDR_TBL_INDEX        FsLLDPSizingParams[MAX_LLDP_DEST_MAC_ADDR_TABLE_SIZING_ID].u4PreAllocatedUnits
#define LLDP_MAX_NEIGHBOR_COUNT             FsLLDPSizingParams[MAX_LLDP_REMOTE_NODES_SIZING_ID].u4PreAllocatedUnits
#define LLDP_CLI_MAX_MAC_STRING_SIZE        21

#define LLDP_MAX_VLAN_NAME_INFO             4094

#define LLDP_DEFAULT_CREDIT_MAX             1
#define LLDPV2_DEFAULT_CREDIT_MAX           5
#define LLDP_MIN_CREDIT_MAX                 1
#define LLDP_MAX_CREDIT_MAX                 10

#define LLDP_DEFAULT_FAST_TX                30
#define LLDPV2_DEFAULT_MESSAGE_FAST_TX      1
#define LLDP_MIN_MESSAGE_FAST_TX            1
#define LLDP_MAX_MESSAGE_FAST_TX            3600

#define LLDP_DEFAULT_TX_FAST_INIT           1
#define LLDPV2_DEFAULT_TX_FAST_INIT         4
#define LLDPV2_DEFAULT_MED_FAST_REPEAT_COUNT    3
#define LLDP_MIN_TX_FAST_INIT               1
#define LLDP_MAX_TX_FAST_INIT               8
#define LLDP_MIN_MED_FAST_REPEAT_COUNT      1
#define LLDP_MAX_MED_FAST_REPEAT_COUNT      10


/* Q depth is increased to 60 to handle the packets from L2 modules.
 * Since the Q message was 50, some of the packets are geeting dropped 
 * Receive PDU Q depth is increased to handle packets from 256 ports */
#define LLDP_MAX_MSGQ_DEPTH                 60 /* Nominal value */
#define LLDP_MAX_RXPDUQ_DEPTH               60 /* Nominal value */  

#define LLDP_TRC_MIN_SIZE                   1
#define LLDP_MIN_LEN_CHASSISID              1
#define LLDP_MIN_LEN_PORTID                 1
#define LLDP_MAX_LEN_PHY_MEDIA_CAP          2
#define LLDP_MAX_LEN_TLV_VALUE_FIELD        512 /* Maximum possible length for
                                                   TLV Value Field */
#define LLDP_VID_MAP_LEN                    4096

#define LLDP_TMR_RUNNING                    1
#define LLDP_TMR_NOT_RUNNING                2
#define LLDP_TMR_EXPIRED                    3

#define LLDP_BULK_UPD_NOT_STARTED           0
#define LLDP_BULK_UPD_COMPLETED             1
#define LLDP_BULK_UPD_IN_PROGRESS           2



/* Do not change the MEMBLK_COUNT directly. Change the value 
 * of the macro that is mapped to MEMBLK_COUNT */

/* LocPortInfoPoolId */
#define LLDP_LOC_PORT_INFO_MEMBLK_SIZE      sizeof(tLldpLocPortInfo)
#define LLDP_LOC_PORT_INFO_MEMBLK_COUNT     LLDP_MAX_PORTS

/* QMsgPoolId */
#define LLDP_QMSG_MEMBLK_SIZE               sizeof(tLldpQMsg)
#define LLDP_QMSG_MEMBLK_COUNT              LLDP_MAX_MSGQ_DEPTH

/* RxPduQPoolId */
#define LLDP_RXPDU_Q_MEMBLK_SIZE            sizeof(tLldpRxPduQMsg) 
#define LLDP_RXPDU_Q_MEMBLK_COUNT           LLDP_MAX_RXPDUQ_DEPTH

/* RemNodePoolId */
#define LLDP_REM_NODE_MEMBLK_SIZE           sizeof(tLldpRemoteNode)
#define LLDP_REM_NODE_MEMBLK_COUNT          LLDP_MAX_NEIGHBORS

/* LocManAddrPoolId */
#define LLDP_LOC_MAN_ADDR_MEMBLK_SIZE       sizeof(tLldpLocManAddrTable)
#define LLDP_LOC_MAN_ADDR_MEMBLK_COUNT      LLDP_MAX_LOC_MAN_ADDR

/* RemManAddrPoolId */
#define LLDP_REM_MAN_ADDR_MEMBLK_SIZE       sizeof(tLldpRemManAddressTable)
#define LLDP_REM_MAN_ADDR_MEMBLK_COUNT      LLDP_MAX_REM_MAN_ADDR

/* RemUnknownTLVPoolId */
#define LLDP_REM_UKNOWN_TLV_MEMBLK_SIZE     sizeof(tLldpRemUnknownTLVTable)
#define LLDP_REM_UKNOWN_TLV_MEMBLK_COUNT    LLDP_MAX_REM_UNKNOWN_TLV

/* RemOrgDefInfoPoolId */
#define LLDP_REM_ORG_DEF_INFO_MEMBLK_SIZE   sizeof(tLldpRemOrgDefInfoTable)
#define LLDP_REM_ORG_DEF_INFO_MEMBLK_COUNT  LLDP_MAX_REM_ORG_DEF_INFO_TLV

/* LocProtoVlanInfoPoolId */
#define LLDP_LOC_PROTO_VLAN_MEMBLK_SIZE     sizeof(tLldpxdot1LocProtoVlanInfo)
#define LLDP_LOC_PROTO_VLAN_MEMBLK_COUNT    LLDP_MAX_LOC_PPVID

/* LocProtoIdInfoPoolId */    
#define LLDP_LOC_PROTOID_MEMBLK_SIZE        sizeof(tLldpxdot1LocProtoIdInfo)
#define LLDP_LOC_PROTOID_MEMBLK_COUNT       LLDP_MAX_LOC_PROTOID
    
/* RemProtoVlanPoolId */
#define LLDP_REM_PROTO_VLAN_MEMBLK_SIZE     sizeof(tLldpxdot1RemProtoVlanInfo)
#define LLDP_REM_PROTO_VLAN_MEMBLK_COUNT    LLDP_MAX_REM_PPVID

/* RemVlanNameInfoPoolId */
#define LLDP_REM_VLAN_NAME_MEMBLK_SIZE      sizeof(tLldpxdot1RemVlanNameInfo)
#define LLDP_REM_VLAN_NAME_MEMBLK_COUNT     LLDP_MAX_REM_VLAN

/* RemProtocolPoolId */
#define LLDP_REM_PROTOID_MEMBLK_SIZE        sizeof(tLldpxdot1RemProtoIdInfo)
#define LLDP_REM_PROTOID_MEMBLK_COUNT       LLDP_MAX_REM_PROTOID

/* RemDot3PortInfoPoolId */
#define LLDP_REM_DOT3_MEMBLK_SIZE           sizeof (tLldpxdot3RemPortInfo)
#define LLDP_REM_DOT3_MEMBLK_COUNT          LLDP_MAX_NEIGHBORS

/* ApplTblPoolId */
#define LLDP_APPL_TBL_MEMBLK_SIZE           sizeof (tLldpAppInfo)
#define LLDP_APPL_TBL_MEMBLK_COUNT          (LLDP_MAX_PORTS * LLDP_MAX_APPL * 0.5)
                                            /* Applications will run on 50% 
                                             * of the ports in the system. This
                                             * parameter needs to be tuned 
                                             * based on the need. */ 

/* AppTlvPoolId - This pool is used in the following 2 scenarios 
 * 1. Application TLVs posted to LLDP queue. Maximum Q-depth can be used.
 * 2. Application TLVs in LLDP's application info table */
#define LLDP_APPL_TLV_MEMBLK_SIZE           LLDP_MAX_APP_TLV_LEN
#define LLDP_APPL_TLV_MEMBLK_COUNT          (LLDP_APPL_TBL_MEMBLK_COUNT + LLDP_MAX_MSGQ_DEPTH)

/****************************************************************************/
/*    LLDP TX Sem related  constants                                        */
/****************************************************************************/
/* LLDP Tx State Event Machine STATES */
enum
{
    LLDP_TX_INIT       = 1,
    LLDP_TX_IDLE       = 2,
    LLDP_TX_SHUT_FRAME = 3,
    LLDP_TX_INFO_FRAME = 4,
    LLDP_TX_MAX_STATES = 5
};

/* LLDP Tx State Event Machine EVENTS */
enum
{
    LLDP_TX_EV_PORT_OPER_DOWN     = 0, /* port oper status = DOWN */
    LLDP_TX_EV_PORT_OPER_UP       = 1, /* port oper status = UP */
    LLDP_TX_EV_TXMOD_ADMIN_UP     = 2, /* adminStatus = TxOnly or TxRx */
    LLDP_TX_EV_TXMOD_ADMIN_DOWN   = 3, /* adminStaus = Disable or RxOnly */
    LLDP_TX_EV_REINIT_DELAY       = 4, /* ReInitDelay(defval=2sec) Timer
                                          Expiry Event */
    LLDP_TX_EV_TX_NOW             = 5,
   /* LLDP_TX_EV_TX_DELAY           = 5,*/ /* TxDelay(defval=2sec) Timer Expiry
                                          Event */
    /*LLDP_TX_EV_MSG_INTVAL         = 6,*/ /* MsgInterval(defval=30sec) Timer
                                          Expiry Event */
    /*LLDP_TX_EV_LOCAL_CHANGE       = 7,*/ /* SomeThingChangedLocal status */
    LLDP_TX_MAX_EVENTS            = 6,
};

/* LLDP Tx Timer State Event Machine STATES */
enum
{
    LLDP_TX_TMR_INIT          = 1,
    LLDP_TX_TMR_IDLE          = 2,
    LLDP_TX_TMR_EXPIRY        = 3,
    LLDP_TX_TMR_TX_TICK       = 4,
    LLDP_TX_TMR_SIGNAL_TX     = 5,
    LLDP_TX_TMR_TX_FAST_START = 6,
    LLDP_TX_TMR_MAX_STATES    = 7
};

/* LLDP Tx Timer State Event Machine EVENTS */
enum
{
    LLDP_TX_TMR_EV_PORT_OPER_DOWN     = 0, /* port oper status = DOWN */
    LLDP_TX_TMR_EV_PORT_OPER_UP       = 1, /* port oper status = UP */
    LLDP_TX_TMR_EV_TXMOD_ADMIN_UP     = 2, /* adminStatus = TxOnly or TxRx */
    LLDP_TX_TMR_EV_TXMOD_ADMIN_DOWN   = 3, /* adminStaus = Disable or RxOnly */
    LLDP_TX_TMR_EV_TX_DELAY           = 4, /* TxDelay(defval=1sec) Timer Expiry
                                          Event */
    LLDP_TX_TMR_EV_MSG_INTVAL         = 5, /* MsgInterval(defval=30sec) Timer
                                          Expiry Event */
    LLDP_TX_TMR_EV_LOCAL_CHANGE       = 6, /* SomeThingChangedLocal status */
    LLDP_TX_TMR_EV_NEIGHBOR_DETECTED  = 7, /* new neighbor detected status */
    LLDP_TX_TMR_MAX_EVENTS            = 8
};
/****************************************************************************/
/*    LLDP RX Sem related  constants                                        */
/****************************************************************************/
/* LLDP Rx State Event Machine EVENTS */
enum
{
    LLDP_RX_EV_PORT_OPER_DOWN     = 0, /* port oper status = DOWN */
    LLDP_RX_EV_INFO_AGEOUT        = 1, /* rxInfoAge timer expires */
    LLDP_RX_EV_PORT_OPER_UP       = 2, /* port oper status = UP */
    LLDP_RX_EV_RXMOD_ADMIN_UP     = 3, /* adimStatus = RxOnly or TxRx */
    LLDP_RX_EV_RXMOD_ADMIN_DOWN   = 4, /* adminStaus = Disable or TxOnly */
    LLDP_RX_EV_FRAME_RECEIVED     = 5, /* While received a LLDPU */
    LLDP_RX_MAX_EVENTS            = 6
};

/* LLDP Rx State Event Machine STATES */
enum
{
    LLDP_RX_WAIT_PORT_OPERATIONAL = 0,
    LLDP_RX_DELETE_AGED_INFO      = 1,
    LLDP_RX_LLDP_INITIALIZE       = 2,
    LLDP_RX_WAIT_FOR_FRAME        = 3,
    LLDP_RX_FRAME_RECEIVED        = 4,
    LLDP_RX_DELETE_INFO           = 5,
    LLDP_RX_UPDATE_INFO           = 6,
    LLDP_RX_MAX_STATES            = 7
};

/* Frame Type Classification - Used diring Rx LLDPDU Processing */
 enum
{
    LLDP_RX_FRAME_NEW             = 1,
    LLDP_RX_FRAME_UPDATE          = 2,
    LLDP_RX_FRAME_REFRESH         = 3,
    LLDP_RX_FRAME_SHUTDOWN        = 4
};

/* Update information types - used to notify other modules when local system
 * information is changed*/
 enum
{
   LLDP_UPDATE_CHASSISID          = 1, /* notify other modules about the 
                                        * change in chassis-id
                                        */
   LLDP_UPDATE_PORTID             = 2,  /* notify other modules about the 
                                        * change in port-id
                                        */
   LLDP_MAX_SYSTEM_ID             = 3  /* Max */ 
};

/* Following constants are used to check the existence of Duplicate TLVs 
 * (Duplicate VLAN Name TLV, Management Address TLV) in an LLDPDU */
enum
{
    LLDP_DUP_VLAN_NAME_TLV        = 1, /* Duplicate VLAN Name TLV */
    LLDP_DUP_MGMT_ADDR_TLV        = 2, /* Duplicate Management Address TLV */
    LLDP_DUP_VID_DIGEST_TLV       = 3, /* Vid Usage Digest TLV */
    LLDP_DUP_MGMT_VID_TLV         = 4  /* Management VID TLV */
};


#endif /* _LLDDEFN_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  llddefn.h                      */
/*-----------------------------------------------------------------------*/
