/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldtrap.h,v 1.10 2015/09/09 13:22:37 siva Exp $
 *
 * Description: This file contains trap data structures defined for
 *              LLDP module.
 *********************************************************************/

#ifndef _LLDP_TRAP_H
#define _LLDP_TRAP_H

#define LLDP_OBJECT_NAME_LEN 256

#define LLDP_SNMPV2_TRAP_OID_LEN 11


enum
{
    LLDP_REM_TABLE_CHG = 1, /*Remote Table Change*/
    LLDP_EXCEED_FRAME_SIZE, /*Exceeds Frame Size*/
    LLDP_DUP_CHASIS_ID , /* Duplicate Chassis Id*/
    LLDP_DUP_SYSTEM_NAME, /*Duplicate System Name*/
    LLDP_DUP_MAN_ADDR, /*Duplicate Management Address*/
    LLDP_MIS_CONFIG_PORT_VID, /*MisConfigured Port VLANID*/
    LLDP_MIS_CONFIG_PPVID, /*MisConfigured Port and Protocol VLANID*/
    LLDP_MIS_CONFIG_VLAN_NAME, /* MisConfigured VlanName*/
    LLDP_MIS_CONFIG_PROTO_IDENTITY_CONFIG,
    LLDP_MIS_CONFIG_LINK_AGG_INFO,
    LLDP_MIS_CONFIG_POWER_VIA_MDI,
    LLDP_MIS_CONFIG_MAX_FRAME_SIZE,
    LLDP_MIS_CONFIG_OPER_MAU_TYPE,
    LLDP_MED_POLICY_MISMATCH, /*Network Policy Mismatch*/
    LLDP_MAX_CONFIG_INFO
};

typedef struct LldpDupConManAddr
{
    INT4  i4RemManAddrSubtype;                  /*Management Addr Sub Type*/
    INT4  i4RemManAddrIfId;                     /* Management address IfId */
    UINT1 au1DupManAddr[LLDP_MAX_LEN_MAN_ADDR]; /* Management Address*/
    UINT1 au1Pad[1];
}tLldpDupManAddr;

typedef struct LldpMisConfVlanName {
    UINT2 u2RemVlanId;                            /* Remote Vlan Id */
    UINT1 au1VlanName[VLAN_STATIC_MAX_NAME_LEN];  /* Vlan Name */
    UINT1 au1Pad[2];
}tLldpMisConfVlanName;

typedef struct LldpMisConfProtoIdentity {
    UINT4 u4RemProtocolIndex;                   /* Protocol Index  */
    UINT1 au1ProtocolId [LLDP_MAX_LEN_PROTOID]; /* Protocol Identity */
    UINT1 au1Pad[1];
}tLldpMisConfProtoIdentity;
typedef struct LldpExceedFrameSize {
    UINT4 u4LocPortNum;
    INT4  i4LocPortIdSubtype;
    UINT1 au1LocPortId[LLDP_MAX_LEN_PORTID];   /* Port Id */
    UINT1 au1Pad[1];
}tLldpExceedFrameSize;
typedef struct LldpMisConfPpvid{
    UINT2 u2Ppvid;
    BOOL1 b1ProtoVlanSupported;
    UINT1 au1Pad[1];
}tLldpMisConfPpvid;

typedef struct LldMisConfigTrap
{
    UINT4 u4RemTimeMark;  /* Time Filter */
    INT4  i4RemIndex;         /* Remote Index */ 
    INT4  i4RemLocalPortNum;  /* Received Local Port Num */
    INT4  i4RemChassisIdSubtype;
    INT4  i4RemPortIdSubtype;
    UINT1 au1RemChassisId[LLDP_MAX_LEN_CHASSISID];  /* Chasis Id */
    UINT1 au1PortId[LLDP_MAX_LEN_PORTID];        /* Port Id   */
    UINT1 au1Pad[2];
    union {
        tLldpDupManAddr DupManAddr;           /* Mis Configured
                                               * management 
                                               * address*/
        tLldpMisConfProtoIdentity MisConfProtoID ;    /* Mis Configured
                                                       * Protocol 
                                                       * Identity */
        tLldpMisConfVlanName MisConfVlanName;         /* MisConfigured
                                                       * Vlan Name */
        tLldpMisConfPpvid    MisConfPpVid;
        UINT2 u2PortVlanId;                           /* Vlan Id */
        UINT2 u2MaxFrameSize;                         /* Mismatched
                                                       * maximum
                                                       * framesize */
        UINT2  u2OperMauType;                 


        UINT1 au1DupSysName[LLDP_MAX_LEN_SYSNAME];    /* Dulpicate
                                                       * System Name */
        UINT1 u1PowClass;                             /* Power via
                                                       * MDI*/
        UINT1 u1AggStatus;                            /* Link Agg Info*/

        UINT1 u1AppType;                              /* Application
                                                         Type */
        UINT1 u1DeviceClass;                          /* Device
                                                         Class */

    }unMisConfParam;

#define DupSysName  unMisConfParam.au1DupSysName
#define DupManAddress     unMisConfParam.DupManAddr.au1DupManAddr
#define DupAddrSubType unMisConfParam.DupManAddr.i4RemManAddrSubtype
#define DupManAddrIfId    unMisConfParam.DupManAddr.i4RemManAddrIfId
#define MisProtoID        unMisConfParam.MisConfProtoID.au1ProtocolId
#define MisProtoIndex     unMisConfParam.MisConfProtoID.u4RemProtocolIndex 
#define MisVlanName       unMisConfParam.MisConfVlanName.au1VlanName
#define MisVlanId         unMisConfParam.MisConfVlanName.u2RemVlanId
#define MisPortVlanId     unMisConfParam.u2PortVlanId
#define MisPpvid          unMisConfParam.MisConfPpVid.u2Ppvid
#define MisVlanSupport    unMisConfParam.MisConfPpVid.b1ProtoVlanSupported
#define MisMaxFrmSize     unMisConfParam.u2MaxFrameSize
#define MisPowViaMDI      unMisConfParam.u1PowClass
#define MisLinkAggInfo    unMisConfParam.u1AggStatus
#define MisOperMauType    unMisConfParam.u2OperMauType
#define MisAppType        unMisConfParam.u1AppType
#define MisDeviceClass    unMisConfParam.u1DeviceClass

}tLldMisConfigTrap;
INT4 
LldpGetObjIndexOIDs (tLldMisConfigTrap *pMisConfTrap, 
                     tSNMP_VAR_BIND **ppStartVb,
                     tSNMP_VAR_BIND **ppVbList);
VOID LldpSendNotification (VOID *, UINT1 );
INT4 LldpParseSubIdNew (UINT1 **ppu1TempPtr, UINT4 *pu4Value);
tSNMP_OID_TYPE     *
LldpMakeObjIdFrmString (INT1 *pi1TextStr, UINT1 *pu1TableName);

#endif

