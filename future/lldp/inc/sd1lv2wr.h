/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: sd1lv2wr.h,v 1.2 2013/03/28 11:45:03 siva Exp $
 *
 * Description: Proto types for Low Level  Routines
 **********************************************************************/

#ifndef _SD1LV2WR_H
#define _SD1LV2WR_H
INT4 GetNextIndexLldpV2Xdot1ConfigPortVlanTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSD1LV2(VOID);

VOID UnRegisterSD1LV2(VOID);
INT4 LldpV2Xdot1ConfigPortVlanTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot1ConfigPortVlanTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot1ConfigPortVlanTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot1ConfigPortVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexLldpV2Xdot1ConfigVlanNameTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1ConfigVlanNameTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot1ConfigVlanNameTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot1ConfigVlanNameTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot1ConfigVlanNameTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexLldpV2Xdot1ConfigProtoVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1ConfigProtoVlanTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot1ConfigProtoVlanTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot1ConfigProtoVlanTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot1ConfigProtoVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexLldpV2Xdot1ConfigProtocolTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1ConfigProtocolTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot1ConfigProtocolTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot1ConfigProtocolTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot1ConfigProtocolTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexLldpV2Xdot1ConfigVidUsageDigestTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1ConfigVidUsageDigestTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot1ConfigVidUsageDigestTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot1ConfigVidUsageDigestTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot1ConfigVidUsageDigestTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexLldpV2Xdot1ConfigManVidTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1ConfigManVidTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot1ConfigManVidTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot1ConfigManVidTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot1ConfigManVidTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexLldpV2Xdot1LocTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1LocPortVlanIdGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2Xdot1LocProtoVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1LocProtoVlanSupportedGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot1LocProtoVlanEnabledGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2Xdot1LocVlanNameTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1LocVlanNameGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2Xdot1LocProtocolTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1LocProtocolIdGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2Xdot1LocVidUsageDigestTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1LocVidUsageDigestGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2Xdot1LocManVidTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1LocManVidGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2Xdot1LocLinkAggTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1LocLinkAggStatusGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot1LocLinkAggPortIdGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2Xdot1RemTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1RemPortVlanIdGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2Xdot1RemProtoVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1RemProtoVlanSupportedGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot1RemProtoVlanEnabledGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2Xdot1RemVlanNameTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1RemVlanNameGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2Xdot1RemProtocolTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1RemProtocolIdGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2Xdot1RemVidUsageDigestTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1RemVidUsageDigestGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2Xdot1RemManVidTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1RemManVidGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2Xdot1RemLinkAggTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1RemLinkAggStatusGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot1RemLinkAggPortIdGet(tSnmpIndex *, tRetVal *);
#endif /* _SD1LV2WR_H */
