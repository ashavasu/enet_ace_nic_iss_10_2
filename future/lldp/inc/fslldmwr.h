/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fslldmwr.h,v 1.2 2016/01/28 11:09:06 siva Exp $
*
* Description: LLDP-MED Proto Types for Wrapper Functions
*********************************************************************/

#ifndef _FSLLDMWR_H
#define _FSLLDMWR_H
INT4 GetNextIndexFsLldpMedPortConfigTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSLLDM(VOID);

VOID UnRegisterFSLLDM(VOID);
INT4 FsLldpMedPortCapSupportedGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedPortConfigTLVsTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedPortConfigTLVsTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedPortConfigTLVsTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpMedPortConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsLldpMedLocMediaPolicyTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsLldpMedLocMediaPolicyVlanIDGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedLocMediaPolicyPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedLocMediaPolicyDscpGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedLocMediaPolicyUnknownGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedLocMediaPolicyTaggedGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedLocMediaPolicyRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedLocMediaPolicyVlanIDSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedLocMediaPolicyPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedLocMediaPolicyDscpSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedLocMediaPolicyUnknownSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedLocMediaPolicyTaggedSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedLocMediaPolicyRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedLocMediaPolicyVlanIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpMedLocMediaPolicyPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpMedLocMediaPolicyDscpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpMedLocMediaPolicyUnknownTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpMedLocMediaPolicyTaggedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpMedLocMediaPolicyRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpMedLocMediaPolicyTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsLldpMedLocLocationTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsLldpMedLocLocationRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedLocLocationRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedLocLocationRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpMedLocLocationTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsLldpXMedRemCapabilitiesTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsLldpXMedRemCapSupportedGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpXMedRemCapCurrentGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpXMedRemDeviceClassGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsLldpXMedRemMediaPolicyTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsLldpXMedRemMediaPolicyVlanIDGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpXMedRemMediaPolicyPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpXMedRemMediaPolicyDscpGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpXMedRemMediaPolicyUnknownGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpXMedRemMediaPolicyTaggedGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsLldpXMedRemInventoryTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsLldpXMedRemHardwareRevGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpXMedRemFirmwareRevGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpXMedRemSoftwareRevGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpXMedRemSerialNumGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpXMedRemMfgNameGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpXMedRemModelNameGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpXMedRemAssetIDGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsLldpMedStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsLldpMedStatsTxFramesTotalGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedStatsRxFramesTotalGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedStatsRxFramesDiscardedTotalGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedStatsRxTLVsDiscardedTotalGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedStatsRxCapTLVsDiscardedGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedStatsRxPolicyTLVsDiscardedGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedStatsRxInventoryTLVsDiscardedGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedStatsRxLocationTLVsDiscardedGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedStatsRxExPowerMDITLVsDiscardedGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedStatsRxCapTLVsDiscardedReasonGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedStatsRxPolicyDiscardedReasonGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedStatsRxInventoryDiscardedReasonGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedStatsRxLocationDiscardedReasonGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedStatsRxExPowerDiscardedReasonGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedClearStatsGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedClearStatsSet(tSnmpIndex *, tRetVal *);
INT4 FsLldpMedClearStatsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsLldpMedClearStatsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsLldpXMedRemLocationTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsLldpXMedRemLocationInfoGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsLldpXMedRemXPoEPDTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsLldpXMedRemXPoEDeviceTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpXMedRemXPoEPDPowerReqGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpXMedRemXPoEPDPowerSourceGet(tSnmpIndex *, tRetVal *);
INT4 FsLldpXMedRemXPoEPDPowerPriorityGet(tSnmpIndex *, tRetVal *);
#endif /* _FSLLDMWR_H */
