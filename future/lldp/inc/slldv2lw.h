/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: slldv2lw.h,v 1.2 2013/03/28 11:45:03 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2MessageTxInterval ARG_LIST((UINT4 *));

INT1
nmhGetLldpV2MessageTxHoldMultiplier ARG_LIST((UINT4 *));

INT1
nmhGetLldpV2ReinitDelay ARG_LIST((UINT4 *));

INT1
nmhGetLldpV2NotificationInterval ARG_LIST((UINT4 *));

INT1
nmhGetLldpV2TxCreditMax ARG_LIST((UINT4 *));

INT1
nmhGetLldpV2MessageFastTx ARG_LIST((UINT4 *));

INT1
nmhGetLldpV2TxFastInit ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpV2MessageTxInterval ARG_LIST((UINT4 ));

INT1
nmhSetLldpV2MessageTxHoldMultiplier ARG_LIST((UINT4 ));

INT1
nmhSetLldpV2ReinitDelay ARG_LIST((UINT4 ));

INT1
nmhSetLldpV2NotificationInterval ARG_LIST((UINT4 ));

INT1
nmhSetLldpV2TxCreditMax ARG_LIST((UINT4 ));

INT1
nmhSetLldpV2MessageFastTx ARG_LIST((UINT4 ));

INT1
nmhSetLldpV2TxFastInit ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpV2MessageTxInterval ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2LldpV2MessageTxHoldMultiplier ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2LldpV2ReinitDelay ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2LldpV2NotificationInterval ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2LldpV2TxCreditMax ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2LldpV2MessageFastTx ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2LldpV2TxFastInit ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpV2MessageTxInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2LldpV2MessageTxHoldMultiplier ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2LldpV2ReinitDelay ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2LldpV2NotificationInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2LldpV2TxCreditMax ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2LldpV2MessageFastTx ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2LldpV2TxFastInit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpV2PortConfigTable. */
INT1
nmhValidateIndexInstanceLldpV2PortConfigTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2PortConfigTable  */

INT1
nmhGetFirstIndexLldpV2PortConfigTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2PortConfigTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2PortConfigAdminStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetLldpV2PortConfigNotificationEnable ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetLldpV2PortConfigTLVsTxEnable ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpV2PortConfigAdminStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetLldpV2PortConfigNotificationEnable ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetLldpV2PortConfigTLVsTxEnable ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpV2PortConfigAdminStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2LldpV2PortConfigNotificationEnable ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2LldpV2PortConfigTLVsTxEnable ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpV2PortConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpV2DestAddressTable. */
INT1
nmhValidateIndexInstanceLldpV2DestAddressTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2DestAddressTable  */

INT1
nmhGetFirstIndexLldpV2DestAddressTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2DestAddressTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2DestMacAddress ARG_LIST((UINT4 ,tMacAddr * ));

/* Proto Validate Index Instance for LldpV2ManAddrConfigTxPortsTable. */
INT1
nmhValidateIndexInstanceLldpV2ManAddrConfigTxPortsTable ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for LldpV2ManAddrConfigTxPortsTable  */

INT1
nmhGetFirstIndexLldpV2ManAddrConfigTxPortsTable ARG_LIST((INT4 * , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2ManAddrConfigTxPortsTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2ManAddrConfigTxEnable ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetLldpV2ManAddrConfigRowStatus ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpV2ManAddrConfigTxEnable ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetLldpV2ManAddrConfigRowStatus ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpV2ManAddrConfigTxEnable ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2LldpV2ManAddrConfigRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpV2ManAddrConfigTxPortsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2StatsRemTablesLastChangeTime ARG_LIST((UINT4 *));

INT1
nmhGetLldpV2StatsRemTablesInserts ARG_LIST((UINT4 *));

INT1
nmhGetLldpV2StatsRemTablesDeletes ARG_LIST((UINT4 *));

INT1
nmhGetLldpV2StatsRemTablesDrops ARG_LIST((UINT4 *));

INT1
nmhGetLldpV2StatsRemTablesAgeouts ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for LldpV2StatsTxPortTable. */
INT1
nmhValidateIndexInstanceLldpV2StatsTxPortTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2StatsTxPortTable  */

INT1
nmhGetFirstIndexLldpV2StatsTxPortTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2StatsTxPortTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2StatsTxPortFramesTotal ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetLldpV2StatsTxLLDPDULengthErrors ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpV2StatsRxPortTable. */
INT1
nmhValidateIndexInstanceLldpV2StatsRxPortTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2StatsRxPortTable  */

INT1
nmhGetFirstIndexLldpV2StatsRxPortTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2StatsRxPortTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2StatsRxPortFramesDiscardedTotal ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetLldpV2StatsRxPortFramesErrors ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetLldpV2StatsRxPortFramesTotal ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetLldpV2StatsRxPortTLVsDiscardedTotal ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetLldpV2StatsRxPortTLVsUnrecognizedTotal ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetLldpV2StatsRxPortAgeoutsTotal ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2LocChassisIdSubtype ARG_LIST((INT4 *));

INT1
nmhGetLldpV2LocChassisId ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpV2LocSysName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpV2LocSysDesc ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpV2LocSysCapSupported ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpV2LocSysCapEnabled ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for LldpV2LocPortTable. */
INT1
nmhValidateIndexInstanceLldpV2LocPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2LocPortTable  */

INT1
nmhGetFirstIndexLldpV2LocPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2LocPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2LocPortIdSubtype ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpV2LocPortId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpV2LocPortDesc ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for LldpV2LocManAddrTable. */
INT1
nmhValidateIndexInstanceLldpV2LocManAddrTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for LldpV2LocManAddrTable  */

INT1
nmhGetFirstIndexLldpV2LocManAddrTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2LocManAddrTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2LocManAddrLen ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetLldpV2LocManAddrIfSubtype ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetLldpV2LocManAddrIfId ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetLldpV2LocManAddrOID ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OID_TYPE * ));

/* Proto Validate Index Instance for LldpV2RemTable. */
INT1
nmhValidateIndexInstanceLldpV2RemTable ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2RemTable  */

INT1
nmhGetFirstIndexLldpV2RemTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2RemTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2RemChassisIdSubtype ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetLldpV2RemChassisId ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpV2RemPortIdSubtype ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetLldpV2RemPortId ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpV2RemPortDesc ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpV2RemSysName ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpV2RemSysDesc ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpV2RemSysCapSupported ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpV2RemSysCapEnabled ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpV2RemRemoteChanges ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetLldpV2RemTooManyNeighbors ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for LldpV2RemManAddrTable. */
INT1
nmhValidateIndexInstanceLldpV2RemManAddrTable ARG_LIST((UINT4  , INT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for LldpV2RemManAddrTable  */

INT1
nmhGetFirstIndexLldpV2RemManAddrTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2RemManAddrTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2RemManAddrIfSubtype ARG_LIST((UINT4  , INT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetLldpV2RemManAddrIfId ARG_LIST((UINT4  , INT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetLldpV2RemManAddrOID ARG_LIST((UINT4  , INT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OID_TYPE * ));

/* Proto Validate Index Instance for LldpV2RemUnknownTLVTable. */
INT1
nmhValidateIndexInstanceLldpV2RemUnknownTLVTable ARG_LIST((UINT4  , INT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2RemUnknownTLVTable  */

INT1
nmhGetFirstIndexLldpV2RemUnknownTLVTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2RemUnknownTLVTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2RemUnknownTLVInfo ARG_LIST((UINT4  , INT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for LldpV2RemOrgDefInfoTable. */
INT1
nmhValidateIndexInstanceLldpV2RemOrgDefInfoTable ARG_LIST((UINT4  , INT4  , UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2RemOrgDefInfoTable  */

INT1
nmhGetFirstIndexLldpV2RemOrgDefInfoTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , UINT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2RemOrgDefInfoTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2RemOrgDefInfo ARG_LIST((UINT4  , INT4  , UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
