/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: stdot3wr.h,v 1.4 2008/08/20 15:13:21 iss Exp $
 *
 * Description: Wrapper header file for stdot3lldp mib  
 *********************************************************************/
#ifndef _STDOT3WR_H
#define _STDOT3WR_H
INT4 GetNextIndexLldpXdot3PortConfigTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSTDOT3(VOID);

VOID UnRegisterSTDOT3(VOID);
INT4 LldpXdot3PortConfigTLVsTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot3PortConfigTLVsTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot3PortConfigTLVsTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXdot3PortConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexLldpXdot3LocPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot3LocPortAutoNegSupportedGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot3LocPortAutoNegEnabledGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot3LocPortAutoNegAdvertisedCapGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot3LocPortOperMauTypeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot3LocPowerTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot3LocPowerPortClassGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot3LocPowerMDISupportedGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot3LocPowerMDIEnabledGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot3LocPowerPairControlableGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot3LocPowerPairsGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot3LocPowerClassGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot3LocLinkAggTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot3LocLinkAggStatusGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot3LocLinkAggPortIdGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot3LocMaxFrameSizeTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot3LocMaxFrameSizeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot3RemPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot3RemPortAutoNegSupportedGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot3RemPortAutoNegEnabledGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot3RemPortAutoNegAdvertisedCapGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot3RemPortOperMauTypeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot3RemPowerTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot3RemPowerPortClassGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot3RemPowerMDISupportedGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot3RemPowerMDIEnabledGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot3RemPowerPairControlableGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot3RemPowerPairsGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot3RemPowerClassGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot3RemLinkAggTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot3RemLinkAggStatusGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot3RemLinkAggPortIdGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot3RemMaxFrameSizeTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot3RemMaxFrameSizeGet(tSnmpIndex *, tRetVal *);
#endif /* _STDOT3WR_H */
