/*********************************************************************
 * copyright (C) 2007 Aricent Inc. All Rights Reserved.
 *
 * $Id: lldsz.h,v 1.9 2017/12/14 10:30:25 siva Exp $
 *
 * Description : This file contains prototypes of mempool 
 *               creation/deletion functions in LLDP module.
 *********************************************************************/
enum {
    MAX_LLDP_AGENT_TO_LOC_PORT_TABLE_SIZING_ID,
    MAX_LLDP_APPL_TLV_COUNT_SIZING_ID,
    MAX_LLDP_APP_INFO_SIZING_ID,
    MAX_LLDP_ARRAY_SIZE_INFO_SIZING_ID,
    MAX_LLDP_DEST_MAC_ADDR_TABLE_SIZING_ID,
    MAX_LLDP_LEN_MAN_OID_SIZING_ID,
    MAX_LLDP_LOCAL_MGMT_ADDR_TABLE_SIZING_ID,
    MAX_LLDP_LOCAL_PROTO_IDENTIFY_INFO_SIZING_ID,
    MAX_LLDP_LOCAL_PROTO_VLAN_INFO_SIZING_ID,
    MAX_LLDP_LOC_PORT_TABLE_SIZING_ID,
    MAX_LLDP_MED_LOC_LOCATION_TABLE_SIZING_ID,
    MAX_LLDP_MED_LOC_NW_POLICY_TABLE_SIZING_ID,
    MAX_LLDP_MED_REM_LOCATION_TABLE_SIZING_ID,
    MAX_LLDP_MED_REM_NW_POLICY_TABLE_SIZING_ID,
    MAX_LLDP_NEIGH_INFO_SIZING_ID,
    MAX_LLDP_PDU_COUNT_SIZING_ID,
    MAX_LLDP_PORT_TABLE_SIZING_ID,
    MAX_LLDP_Q_MESG_SIZING_ID,
    MAX_LLDP_REMOTE_MGMT_ADDR_TABLE_SIZING_ID,
    MAX_LLDP_REMOTE_NODES_SIZING_ID,
    MAX_LLDP_REMOTE_ORG_DEF_INFO_TABLE_SIZING_ID,
    MAX_LLDP_REMOTE_PORT_INFO_SIZING_ID,
    MAX_LLDP_REMOTE_PROTO_IDENTIFY_INFO_SIZING_ID,
    MAX_LLDP_REMOTE_PROTO_VLAN_INFO_SIZING_ID,
    MAX_LLDP_REMOTE_UNKNOWN_TLV_TABLE_SIZING_ID,
    MAX_LLDP_REMOTE_VLAN_NAME_INFO_SIZING_ID,
    MAX_LLDP_RX_PDU_Q_MESG_SIZING_ID,
    MAX_LLDP_VLAN_NAME_TLV_COUNTER_SIZING_ID,
    LLDP_MAX_SIZING_ID
};


#ifdef  _LLDPSZ_C
tMemPoolId LLDPMemPoolIds[ LLDP_MAX_SIZING_ID];
INT4  LldpSizingMemCreateMemPools(VOID);
VOID  LldpSizingMemDeleteMemPools(VOID);
INT4  LldpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _LLDPSZ_C  */
extern tMemPoolId LLDPMemPoolIds[ ];
extern INT4  LldpSizingMemCreateMemPools(VOID);
extern VOID  LldpSizingMemDeleteMemPools(VOID);
#endif /*  _LLDPSZ_C  */


#ifdef  _LLDPSZ_C
tFsModSizingParams FsLLDPSizingParams [] = {
{ "tLldpv2AgentToLocPort", "MAX_LLDP_AGENT_TO_LOC_PORT_TABLE", sizeof(tLldpv2AgentToLocPort),MAX_LLDP_AGENT_TO_LOC_PORT_TABLE, MAX_LLDP_AGENT_TO_LOC_PORT_TABLE,0 },
{ "UINT1[512]", "MAX_LLDP_APPL_TLV_COUNT", sizeof(UINT1[512]),MAX_LLDP_APPL_TLV_COUNT, MAX_LLDP_APPL_TLV_COUNT,0 },
{ "tLldpAppInfo", "MAX_LLDP_APP_INFO", sizeof(tLldpAppInfo),MAX_LLDP_APP_INFO, MAX_LLDP_APP_INFO,0 },
{ "UINT1[256]", "MAX_LLDP_ARRAY_SIZE_INFO", sizeof(UINT1[256]),MAX_LLDP_ARRAY_SIZE_INFO, MAX_LLDP_ARRAY_SIZE_INFO,0 },
{ "tLldpv2DestAddrTbl", "MAX_LLDP_DEST_MAC_ADDR_TABLE", sizeof(tLldpv2DestAddrTbl),MAX_LLDP_DEST_MAC_ADDR_TABLE, MAX_LLDP_DEST_MAC_ADDR_TABLE,0 },
{ "CHR1[LLDP_MAX_LEN_MAN_OID*2]", "MAX_LLDP_LEN_MAN_OID", sizeof(CHR1[LLDP_MAX_LEN_MAN_OID*2]),MAX_LLDP_LEN_MAN_OID, MAX_LLDP_LEN_MAN_OID,0 },
{ "tLldpLocManAddrTable", "MAX_LLDP_LOCAL_MGMT_ADDR_TABLE", sizeof(tLldpLocManAddrTable),MAX_LLDP_LOCAL_MGMT_ADDR_TABLE, MAX_LLDP_LOCAL_MGMT_ADDR_TABLE,0 },
{ "tLldpxdot1LocProtoIdInfo", "MAX_LLDP_LOCAL_PROTO_IDENTIFY_INFO", sizeof(tLldpxdot1LocProtoIdInfo),MAX_LLDP_LOCAL_PROTO_IDENTIFY_INFO, MAX_LLDP_LOCAL_PROTO_IDENTIFY_INFO,0 },
{ "tLldpxdot1LocProtoVlanInfo", "MAX_LLDP_LOCAL_PROTO_VLAN_INFO", sizeof(tLldpxdot1LocProtoVlanInfo),MAX_LLDP_LOCAL_PROTO_VLAN_INFO, MAX_LLDP_LOCAL_PROTO_VLAN_INFO,0 },
{ "tLldpLocPortInfo", "MAX_LLDP_LOC_PORT_TABLE", sizeof(tLldpLocPortInfo),MAX_LLDP_LOC_PORT_TABLE, MAX_LLDP_LOC_PORT_TABLE,0 },
{ "tLldpMedLocLocationInfo", "MAX_LLDP_MED_LOC_LOCATION_TABLE", sizeof(tLldpMedLocLocationInfo),MAX_LLDP_MED_LOC_LOCATION_TABLE, MAX_LLDP_MED_LOC_LOCATION_TABLE,0 },
{ "tLldpMedLocNwPolicyInfo", "MAX_LLDP_MED_LOC_NW_POLICY_TABLE", sizeof(tLldpMedLocNwPolicyInfo),MAX_LLDP_MED_LOC_NW_POLICY_TABLE, MAX_LLDP_MED_LOC_NW_POLICY_TABLE,0 },
{ "tLldpMedRemLocationInfo", "MAX_LLDP_MED_REM_LOCATION_TABLE", sizeof(tLldpMedRemLocationInfo),MAX_LLDP_MED_REM_LOCATION_TABLE, MAX_LLDP_MED_REM_LOCATION_TABLE,0 },
{ "tLldpMedRemNwPolicyInfo", "MAX_LLDP_MED_REM_NW_POLICY_TABLE", sizeof(tLldpMedRemNwPolicyInfo),MAX_LLDP_MED_REM_NW_POLICY_TABLE, MAX_LLDP_MED_REM_NW_POLICY_TABLE,0 },
{ "tLldCliSizingNeighInfo", "MAX_LLDP_NEIGH_INFO", sizeof(tLldCliSizingNeighInfo),MAX_LLDP_NEIGH_INFO, MAX_LLDP_NEIGH_INFO,0 },
{ "UINT1[LLDP_MAX_PDU_SIZE]", "MAX_LLDP_PDU_COUNT", sizeof(UINT1[LLDP_MAX_PDU_SIZE]),MAX_LLDP_PDU_COUNT, MAX_LLDP_PDU_COUNT,0 },
{ "tLldpLocPortTable", "MAX_LLDP_PORT_TABLE", sizeof(tLldpLocPortTable),MAX_LLDP_PORT_TABLE, MAX_LLDP_PORT_TABLE,0 },
{ "tLldpQMsg", "MAX_LLDP_Q_MESG", sizeof(tLldpQMsg),MAX_LLDP_Q_MESG, MAX_LLDP_Q_MESG,0 },
{ "tLldpRemManAddressTable", "MAX_LLDP_REMOTE_MGMT_ADDR_TABLE", sizeof(tLldpRemManAddressTable),MAX_LLDP_REMOTE_MGMT_ADDR_TABLE, MAX_LLDP_REMOTE_MGMT_ADDR_TABLE,0 },
{ "tLldpRemoteNode", "MAX_LLDP_REMOTE_NODES", sizeof(tLldpRemoteNode),MAX_LLDP_REMOTE_NODES, MAX_LLDP_REMOTE_NODES,0 },
{ "tLldpRemOrgDefInfoTable", "MAX_LLDP_REMOTE_ORG_DEF_INFO_TABLE", sizeof(tLldpRemOrgDefInfoTable),MAX_LLDP_REMOTE_ORG_DEF_INFO_TABLE, MAX_LLDP_REMOTE_ORG_DEF_INFO_TABLE,0 },
{ "tLldpxdot3RemPortInfo", "MAX_LLDP_REMOTE_PORT_INFO", sizeof(tLldpxdot3RemPortInfo),MAX_LLDP_REMOTE_PORT_INFO, MAX_LLDP_REMOTE_PORT_INFO,0 },
{ "tLldpxdot1RemProtoIdInfo", "MAX_LLDP_REMOTE_PROTO_IDENTIFY_INFO", sizeof(tLldpxdot1RemProtoIdInfo),MAX_LLDP_REMOTE_PROTO_IDENTIFY_INFO, MAX_LLDP_REMOTE_PROTO_IDENTIFY_INFO,0 },
{ "tLldpxdot1RemProtoVlanInfo", "MAX_LLDP_REMOTE_PROTO_VLAN_INFO", sizeof(tLldpxdot1RemProtoVlanInfo),MAX_LLDP_REMOTE_PROTO_VLAN_INFO, MAX_LLDP_REMOTE_PROTO_VLAN_INFO,0 },
{ "tLldpRemUnknownTLVTable", "MAX_LLDP_REMOTE_UNKNOWN_TLV_TABLE", sizeof(tLldpRemUnknownTLVTable),MAX_LLDP_REMOTE_UNKNOWN_TLV_TABLE, MAX_LLDP_REMOTE_UNKNOWN_TLV_TABLE,0 },
{ "tLldpxdot1RemVlanNameInfo", "MAX_LLDP_REMOTE_VLAN_NAME_INFO", sizeof(tLldpxdot1RemVlanNameInfo),MAX_LLDP_REMOTE_VLAN_NAME_INFO, MAX_LLDP_REMOTE_VLAN_NAME_INFO,0 },
{ "tLldpRxPduQMsg", "MAX_LLDP_RX_PDU_Q_MESG", sizeof(tLldpRxPduQMsg),MAX_LLDP_RX_PDU_Q_MESG, MAX_LLDP_RX_PDU_Q_MESG,0 },
{ "tLldpVlanNameTlvCounter", "MAX_LLDP_VLAN_NAME_TLV_COUNTER", sizeof(tLldpVlanNameTlvCounter),MAX_LLDP_VLAN_NAME_TLV_COUNTER, MAX_LLDP_VLAN_NAME_TLV_COUNTER,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _LLDPSZ_C  */
extern tFsModSizingParams FsLLDPSizingParams [];
#endif /*  _LLDPSZ_C  */


