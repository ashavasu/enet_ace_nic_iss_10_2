/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: sd3lv2wr.h,v 1.2 2013/03/28 11:45:03 siva Exp $
 * *
 * * Description: Protocol Mib Data base
 * *********************************************************************/
#ifndef _SD3LV2WR_H
#define _SD3LV2WR_H
INT4 GetNextIndexLldpV2Xdot3PortConfigTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSD3LV2(VOID);

VOID UnRegisterSD3LV2(VOID);
INT4 LldpV2Xdot3PortConfigTLVsTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot3PortConfigTLVsTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot3PortConfigTLVsTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot3PortConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexLldpV2Xdot3LocPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot3LocPortAutoNegSupportedGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot3LocPortAutoNegEnabledGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot3LocPortAutoNegAdvertisedCapGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot3LocPortOperMauTypeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2Xdot3LocPowerTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot3LocPowerPortClassGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot3LocPowerMDISupportedGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot3LocPowerMDIEnabledGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot3LocPowerPairControlableGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot3LocPowerPairsGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot3LocPowerClassGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2Xdot3LocMaxFrameSizeTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot3LocMaxFrameSizeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2Xdot3RemPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot3RemPortAutoNegSupportedGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot3RemPortAutoNegEnabledGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot3RemPortAutoNegAdvertisedCapGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot3RemPortOperMauTypeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2Xdot3RemPowerTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot3RemPowerPortClassGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot3RemPowerMDISupportedGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot3RemPowerMDIEnabledGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot3RemPowerPairControlableGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot3RemPowerPairsGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2Xdot3RemPowerClassGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2Xdot3RemMaxFrameSizeTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot3RemMaxFrameSizeGet(tSnmpIndex *, tRetVal *);
#endif /* _SD3LV2WR_H */
