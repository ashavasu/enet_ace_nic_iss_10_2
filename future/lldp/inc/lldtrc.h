/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lldtrc.h,v 1.8 2016/06/20 10:00:28 siva Exp $
 *
 * Description: This file contains procedures and definitions 
 *             used for debugging.
 *******************************************************************/
#ifndef _LLDP_TRC_H_
#define _LLDP_TRC_H_

#ifdef TRACE_WANTED

#define  LLDP_TRC_FLAG  gLldpGlobalInfo.u4TraceOption
#define  LLDP_NAME      "[LLDP] "


#define LLDP_PKT_DUMP(TraceType, pBuf, Length, Str)                           \
        MOD_PKT_DUMP(LLDP_TRC_FLAG, TraceType, LLDP_NAME, pBuf, Length, Str)

#define LLDP_TRC(TraceType, Str)                                              \
        MOD_TRC(LLDP_TRC_FLAG, TraceType, LLDP_NAME, Str)

#define LLDP_TRC_ARG1(TraceType, Str, Arg1)                                   \
        MOD_TRC_ARG1(LLDP_TRC_FLAG, TraceType, LLDP_NAME, Str, Arg1)

#define LLDP_TRC_ARG2(TraceType, Str, Arg1, Arg2)                             \
        MOD_TRC_ARG2(LLDP_TRC_FLAG, TraceType, LLDP_NAME, Str, Arg1, Arg2)

#define LLDP_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)                       \
        MOD_TRC_ARG3(LLDP_TRC_FLAG, TraceType, LLDP_NAME, Str, Arg1, Arg2,  \
                     Arg3)

#define LLDP_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4)                 \
        MOD_TRC_ARG4(LLDP_TRC_FLAG, TraceType, LLDP_NAME, Str, Arg1, Arg2,  \
                     Arg3,Arg4)

#define LLDP_TRC_ARG5(TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5)           \
        MOD_TRC_ARG5(LLDP_TRC_FLAG, TraceType, LLDP_NAME, Str, Arg1, Arg2,  \
                     Arg3, Arg4, Arg5)

#define LLDP_TRC_ARG6(TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5,Arg6)           \
        MOD_TRC_ARG6(LLDP_TRC_FLAG, TraceType, LLDP_NAME, Str, Arg1, Arg2,  \
                     Arg3, Arg4, Arg5,Arg6)

#define LLDP_TRC_ARG7(TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5,Arg6,Arg7)   \
        MOD_TRC_ARG7(LLDP_TRC_FLAG, TraceType, LLDP_NAME, Str, Arg1, Arg2,  \
                     Arg3, Arg4, Arg5,Arg6,Arg7)               
        

#else  /* TRACE_WANTED */
   #define LLDP_PKT_DUMP(TraceType, pBuf, Length, Str)
   #define LLDP_TRC_ARG1(TraceType, Str, Arg1)\
{ \
  UNUSED_PARAM (Arg1); \
}
   #define LLDP_TRC_ARG2(TraceType, Str, Arg1, Arg2)\
{ \
  UNUSED_PARAM (Arg1); \
  UNUSED_PARAM (Arg2); \
}
   #define LLDP_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)\
{ \
  UNUSED_PARAM (Arg1); \
  UNUSED_PARAM (Arg2); \
  UNUSED_PARAM (Arg3); \
}
   #define LLDP_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4)\
{ \
  UNUSED_PARAM (Arg1); \
  UNUSED_PARAM (Arg2); \
  UNUSED_PARAM (Arg3); \
  UNUSED_PARAM (Arg4); \
}
   #define LLDP_TRC_ARG5(TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5) \
{ \
  UNUSED_PARAM (Arg1); \
  UNUSED_PARAM (Arg2); \
  UNUSED_PARAM (Arg3); \
  UNUSED_PARAM (Arg4); \
  UNUSED_PARAM (Arg5); \
}
   #define LLDP_TRC_ARG6(TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) \
{ \
  UNUSED_PARAM (Arg1); \
  UNUSED_PARAM (Arg2); \
  UNUSED_PARAM (Arg3); \
  UNUSED_PARAM (Arg4); \
  UNUSED_PARAM (Arg5); \
  UNUSED_PARAM (Arg6); \
}
   #define LLDP_TRC_ARG7(TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7) \
{ \
  UNUSED_PARAM (Arg1); \
  UNUSED_PARAM (Arg2); \
  UNUSED_PARAM (Arg3); \
  UNUSED_PARAM (Arg4); \
  UNUSED_PARAM (Arg5); \
  UNUSED_PARAM (Arg6); \
  UNUSED_PARAM (Arg7); \
}

#endif /* TRACE_WANTED */


#endif /* _LLDP_TRC_H_ */
/*---------------------------------------------------------------------------*/
/*                       End of the file  lldtrc.h                           */
/*---------------------------------------------------------------------------*/
