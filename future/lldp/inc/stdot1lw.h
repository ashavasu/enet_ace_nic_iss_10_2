/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdot1lw.h,v 1.3 2008/08/20 15:13:21 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for LldpXdot1ConfigPortVlanTable. */
INT1
nmhValidateIndexInstanceLldpXdot1ConfigPortVlanTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1ConfigPortVlanTable  */

INT1
nmhGetFirstIndexLldpXdot1ConfigPortVlanTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1ConfigPortVlanTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1ConfigPortVlanTxEnable ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXdot1ConfigPortVlanTxEnable ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXdot1ConfigPortVlanTxEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXdot1ConfigPortVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpXdot1LocVlanNameTable. */
INT1
nmhValidateIndexInstanceLldpXdot1LocVlanNameTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1LocVlanNameTable  */

INT1
nmhGetFirstIndexLldpXdot1LocVlanNameTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1LocVlanNameTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1LocVlanName ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for LldpXdot1ConfigVlanNameTable. */
INT1
nmhValidateIndexInstanceLldpXdot1ConfigVlanNameTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1ConfigVlanNameTable  */

INT1
nmhGetFirstIndexLldpXdot1ConfigVlanNameTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1ConfigVlanNameTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1ConfigVlanNameTxEnable ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXdot1ConfigVlanNameTxEnable ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXdot1ConfigVlanNameTxEnable ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXdot1ConfigVlanNameTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpXdot1LocProtoVlanTable. */
INT1
nmhValidateIndexInstanceLldpXdot1LocProtoVlanTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1LocProtoVlanTable  */

INT1
nmhGetFirstIndexLldpXdot1LocProtoVlanTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1LocProtoVlanTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1LocProtoVlanSupported ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetLldpXdot1LocProtoVlanEnabled ARG_LIST((INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for LldpXdot1ConfigProtoVlanTable. */
INT1
nmhValidateIndexInstanceLldpXdot1ConfigProtoVlanTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1ConfigProtoVlanTable  */

INT1
nmhGetFirstIndexLldpXdot1ConfigProtoVlanTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1ConfigProtoVlanTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1ConfigProtoVlanTxEnable ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXdot1ConfigProtoVlanTxEnable ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXdot1ConfigProtoVlanTxEnable ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXdot1ConfigProtoVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpXdot1LocProtocolTable. */
INT1
nmhValidateIndexInstanceLldpXdot1LocProtocolTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1LocProtocolTable  */

INT1
nmhGetFirstIndexLldpXdot1LocProtocolTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1LocProtocolTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1LocProtocolId ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for LldpXdot1ConfigProtocolTable. */
INT1
nmhValidateIndexInstanceLldpXdot1ConfigProtocolTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1ConfigProtocolTable  */

INT1
nmhGetFirstIndexLldpXdot1ConfigProtocolTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1ConfigProtocolTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1ConfigProtocolTxEnable ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXdot1ConfigProtocolTxEnable ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXdot1ConfigProtocolTxEnable ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXdot1ConfigProtocolTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpXdot1LocTable. */
INT1
nmhValidateIndexInstanceLldpXdot1LocTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1LocTable  */

INT1
nmhGetFirstIndexLldpXdot1LocTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1LocTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1LocPortVlanId ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for LldpXdot1RemTable. */
INT1
nmhValidateIndexInstanceLldpXdot1RemTable ARG_LIST((UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1RemTable  */

INT1
nmhGetFirstIndexLldpXdot1RemTable ARG_LIST((UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1RemTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1RemPortVlanId ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for LldpXdot1RemProtoVlanTable. */
INT1
nmhValidateIndexInstanceLldpXdot1RemProtoVlanTable ARG_LIST((UINT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1RemProtoVlanTable  */

INT1
nmhGetFirstIndexLldpXdot1RemProtoVlanTable ARG_LIST((UINT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1RemProtoVlanTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1RemProtoVlanSupported ARG_LIST((UINT4  , INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetLldpXdot1RemProtoVlanEnabled ARG_LIST((UINT4  , INT4  , INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for LldpXdot1RemVlanNameTable. */
INT1
nmhValidateIndexInstanceLldpXdot1RemVlanNameTable ARG_LIST((UINT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1RemVlanNameTable  */

INT1
nmhGetFirstIndexLldpXdot1RemVlanNameTable ARG_LIST((UINT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1RemVlanNameTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1RemVlanName ARG_LIST((UINT4  , INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for LldpXdot1RemProtocolTable. */
INT1
nmhValidateIndexInstanceLldpXdot1RemProtocolTable ARG_LIST((UINT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1RemProtocolTable  */

INT1
nmhGetFirstIndexLldpXdot1RemProtocolTable ARG_LIST((UINT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1RemProtocolTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1RemProtocolId ARG_LIST((UINT4  , INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
