/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldglob.h,v 1.8 2015/09/09 13:19:29 siva Exp $
 *
 * Description: 
 *             
 *****************************************************************************/
#ifndef _LLDGLOB_H_
#define _LLDGLOB_H_ 

tLldpGlobalInfo     gLldpGlobalInfo;
tVlanNameInfo       gVlanNameInfo [VLAN_DEV_MAX_NUM_VLAN];
UINT1               gu1TxAll = OSIX_FALSE;
UINT1               gau1LldpDot1OUI[LLDP_MAX_LEN_OUI] = { 0x00, 0x80, 0xC2 };
UINT1               gau1LldpDot3OUI[LLDP_MAX_LEN_OUI] = { 0x00, 0x12, 0x0F };
UINT1               gau1LldpMedOUI[LLDP_MAX_LEN_OUI] = {0x00, 0x12, 0xBB};
UINT1               gau1LldpMcastAddr[MAC_ADDR_LEN] =
        { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x0E };
UINT1               gau1LldpNullMacAddr[MAC_ADDR_LEN] =
        { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
UINT4               gu4LldpAgentNumber = 1;
/* By default, the backward compatibility is set as false, 
 * if the user wants the old algorithm, he has to change it accordingly*/
UINT4               gu4LldpASNBackwardCompatibility = OSIX_FALSE; 
UINT4               gu4LldpDestMacAddrTblIndex = 1;
#ifdef L2RED_WANTED
tLldpRedGlobalInfo  gLldpRedGlobalInfo;
#endif
UINT4               gLldpUtVariable = 0;
#endif /* _LLDGLOB_H_ */
/*--------------------------------------------------------------------------*/
/*                       End of the file  <filename.c>                      */
/*--------------------------------------------------------------------------*/

