/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: sd3lv2lw.h,v 1.2 2013/03/28 11:45:03 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for LldpV2Xdot3PortConfigTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot3PortConfigTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot3PortConfigTable  */

INT1
nmhGetFirstIndexLldpV2Xdot3PortConfigTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot3PortConfigTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot3PortConfigTLVsTxEnable ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpV2Xdot3PortConfigTLVsTxEnable ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpV2Xdot3PortConfigTLVsTxEnable ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpV2Xdot3PortConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpV2Xdot3LocPortTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot3LocPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot3LocPortTable  */

INT1
nmhGetFirstIndexLldpV2Xdot3LocPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot3LocPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot3LocPortAutoNegSupported ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpV2Xdot3LocPortAutoNegEnabled ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpV2Xdot3LocPortAutoNegAdvertisedCap ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpV2Xdot3LocPortOperMauType ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpV2Xdot3LocPowerTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot3LocPowerTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot3LocPowerTable  */

INT1
nmhGetFirstIndexLldpV2Xdot3LocPowerTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot3LocPowerTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot3LocPowerPortClass ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpV2Xdot3LocPowerMDISupported ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpV2Xdot3LocPowerMDIEnabled ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpV2Xdot3LocPowerPairControlable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpV2Xdot3LocPowerPairs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetLldpV2Xdot3LocPowerClass ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpV2Xdot3LocMaxFrameSizeTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot3LocMaxFrameSizeTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot3LocMaxFrameSizeTable  */

INT1
nmhGetFirstIndexLldpV2Xdot3LocMaxFrameSizeTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot3LocMaxFrameSizeTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot3LocMaxFrameSize ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpV2Xdot3RemPortTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot3RemPortTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot3RemPortTable  */

INT1
nmhGetFirstIndexLldpV2Xdot3RemPortTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot3RemPortTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot3RemPortAutoNegSupported ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetLldpV2Xdot3RemPortAutoNegEnabled ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetLldpV2Xdot3RemPortAutoNegAdvertisedCap ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpV2Xdot3RemPortOperMauType ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpV2Xdot3RemPowerTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot3RemPowerTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot3RemPowerTable  */

INT1
nmhGetFirstIndexLldpV2Xdot3RemPowerTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot3RemPowerTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot3RemPowerPortClass ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetLldpV2Xdot3RemPowerMDISupported ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetLldpV2Xdot3RemPowerMDIEnabled ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetLldpV2Xdot3RemPowerPairControlable ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetLldpV2Xdot3RemPowerPairs ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetLldpV2Xdot3RemPowerClass ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpV2Xdot3RemMaxFrameSizeTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot3RemMaxFrameSizeTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot3RemMaxFrameSizeTable  */

INT1
nmhGetFirstIndexLldpV2Xdot3RemMaxFrameSizeTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot3RemMaxFrameSizeTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot3RemMaxFrameSize ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,UINT4 *));
