/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsllddb.h,v 1.12 2016/07/12 12:40:23 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSLLDPDB_H
#define _FSLLDPDB_H

UINT1 FsLldpLocPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsLldpManAddrConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fslldpv2ConfigPortMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FslldpV2DestAddressTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsLldpStatsTaggedTxPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fslldp [] ={1,3,6,1,4,1,2076,158};
tSNMP_OID_TYPE fslldpOID = {8, fslldp};


UINT4 FsLldpSystemControl [ ] ={1,3,6,1,4,1,2076,158,1,1};
UINT4 FsLldpModuleStatus [ ] ={1,3,6,1,4,1,2076,158,1,2};
UINT4 FsLldpTraceInput [ ] ={1,3,6,1,4,1,2076,158,1,3};
UINT4 FsLldpTraceOption [ ] ={1,3,6,1,4,1,2076,158,1,4};
UINT4 FsLldpTraceLevel [ ] ={1,3,6,1,4,1,2076,158,1,5};
UINT4 FsLldpTagStatus [ ] ={1,3,6,1,4,1,2076,158,1,6};
UINT4 FsLldpConfiguredMgmtIpv4Address [ ] ={1,3,6,1,4,1,2076,158,1,7};
UINT4 FsLldpConfiguredMgmtIpv6Address [ ] ={1,3,6,1,4,1,2076,158,1,8};
UINT4 FsLldpLocChassisIdSubtype [ ] ={1,3,6,1,4,1,2076,158,2,1};
UINT4 FsLldpLocChassisId [ ] ={1,3,6,1,4,1,2076,158,2,2};
UINT4 FsLldpLocPortIdSubtype [ ] ={1,3,6,1,4,1,2076,158,2,3,1,1};
UINT4 FsLldpLocPortId [ ] ={1,3,6,1,4,1,2076,158,2,3,1,2};
UINT4 FsLldpPortConfigNotificationType [ ] ={1,3,6,1,4,1,2076,158,2,3,1,3};
UINT4 FsLldpLocPortDstMac [ ] ={1,3,6,1,4,1,2076,158,2,3,1,4};
UINT4 FsLldpMedAdminStatus [ ] ={1,3,6,1,4,1,2076,158,2,3,1,5};
UINT4 FsLldpManAddrConfigOperStatus [ ] ={1,3,6,1,4,1,2076,158,2,4,1,1};
UINT4 FsLldpMemAllocFailure [ ] ={1,3,6,1,4,1,2076,158,3,1};
UINT4 FsLldpInputQOverFlows [ ] ={1,3,6,1,4,1,2076,158,3,2};
UINT4 FsLldpStatsRemTablesUpdates [ ] ={1,3,6,1,4,1,2076,158,3,3};
UINT4 FsLldpClearStats [ ] ={1,3,6,1,4,1,2076,158,3,4};
UINT4 Fslldpv2Version [ ] ={1,3,6,1,4,1,2076,158,5,1};
UINT4 Fslldpv2ConfigPortMapIfIndex [ ] ={1,3,6,1,4,1,2076,158,5,2,1,1};
UINT4 Fslldpv2ConfigPortMapDestMacAddress [ ] ={1,3,6,1,4,1,2076,158,5,2,1,2};
UINT4 Fslldpv2ConfigPortMapNum [ ] ={1,3,6,1,4,1,2076,158,5,2,1,3};
UINT4 Fslldpv2ConfigPortRowStatus [ ] ={1,3,6,1,4,1,2076,158,5,2,1,4};
UINT4 FslldpV2AddressTableIndex [ ] ={1,3,6,1,4,1,2076,158,5,3,1,1};
UINT4 FslldpV2DestMacAddress [ ] ={1,3,6,1,4,1,2076,158,5,3,1,2};
UINT4 Fslldpv2DestRowStatus [ ] ={1,3,6,1,4,1,2076,158,5,3,1,3};
UINT4 FsLldpStatsTaggedTxPortFramesTotal [ ] ={1,3,6,1,4,1,2076,158,5,4,1,1};




tMbDbEntry fslldpMibEntry[]= {

{{10,FsLldpSystemControl}, NULL, FsLldpSystemControlGet, FsLldpSystemControlSet, FsLldpSystemControlTest, FsLldpSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsLldpModuleStatus}, NULL, FsLldpModuleStatusGet, FsLldpModuleStatusSet, FsLldpModuleStatusTest, FsLldpModuleStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsLldpTraceInput}, NULL, FsLldpTraceInputGet, FsLldpTraceInputSet, FsLldpTraceInputTest, FsLldpTraceInputDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "critical"},

{{10,FsLldpTraceOption}, NULL, FsLldpTraceOptionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, "8192"},

{{10,FsLldpTraceLevel}, NULL, FsLldpTraceLevelGet, FsLldpTraceLevelSet, FsLldpTraceLevelTest, FsLldpTraceLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "8192"},

{{10,FsLldpTagStatus}, NULL, FsLldpTagStatusGet, FsLldpTagStatusSet, FsLldpTagStatusTest, FsLldpTagStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsLldpConfiguredMgmtIpv4Address}, NULL, FsLldpConfiguredMgmtIpv4AddressGet, FsLldpConfiguredMgmtIpv4AddressSet, FsLldpConfiguredMgmtIpv4AddressTest, FsLldpConfiguredMgmtIpv4AddressDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsLldpConfiguredMgmtIpv6Address}, NULL, FsLldpConfiguredMgmtIpv6AddressGet, FsLldpConfiguredMgmtIpv6AddressSet, FsLldpConfiguredMgmtIpv6AddressTest, FsLldpConfiguredMgmtIpv6AddressDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsLldpLocChassisIdSubtype}, NULL, FsLldpLocChassisIdSubtypeGet, FsLldpLocChassisIdSubtypeSet, FsLldpLocChassisIdSubtypeTest, FsLldpLocChassisIdSubtypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "4"},

{{10,FsLldpLocChassisId}, NULL, FsLldpLocChassisIdGet, FsLldpLocChassisIdSet, FsLldpLocChassisIdTest, FsLldpLocChassisIdDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsLldpLocPortIdSubtype}, GetNextIndexFsLldpLocPortTable, FsLldpLocPortIdSubtypeGet, FsLldpLocPortIdSubtypeSet, FsLldpLocPortIdSubtypeTest, FsLldpLocPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLldpLocPortTableINDEX, 1, 0, 0, "1"},

{{12,FsLldpLocPortId}, GetNextIndexFsLldpLocPortTable, FsLldpLocPortIdGet, FsLldpLocPortIdSet, FsLldpLocPortIdTest, FsLldpLocPortTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsLldpLocPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLldpPortConfigNotificationType}, GetNextIndexFsLldpLocPortTable, FsLldpPortConfigNotificationTypeGet, FsLldpPortConfigNotificationTypeSet, FsLldpPortConfigNotificationTypeTest, FsLldpLocPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLldpLocPortTableINDEX, 1, 0, 0, "2"},

{{12,FsLldpLocPortDstMac}, GetNextIndexFsLldpLocPortTable, FsLldpLocPortDstMacGet, FsLldpLocPortDstMacSet, FsLldpLocPortDstMacTest, FsLldpLocPortTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsLldpLocPortTableINDEX, 1, 0, 0, "0180C200000E"},

{{12,FsLldpMedAdminStatus}, GetNextIndexFsLldpLocPortTable, FsLldpMedAdminStatusGet, FsLldpMedAdminStatusSet, FsLldpMedAdminStatusTest, FsLldpLocPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLldpLocPortTableINDEX, 1, 0, 0, "2"},

{{12,FsLldpManAddrConfigOperStatus}, GetNextIndexFsLldpManAddrConfigTable, FsLldpManAddrConfigOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLldpManAddrConfigTableINDEX, 2, 0, 0, NULL},

{{10,FsLldpMemAllocFailure}, NULL, FsLldpMemAllocFailureGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsLldpInputQOverFlows}, NULL, FsLldpInputQOverFlowsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsLldpStatsRemTablesUpdates}, NULL, FsLldpStatsRemTablesUpdatesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsLldpClearStats}, NULL, FsLldpClearStatsGet, FsLldpClearStatsSet, FsLldpClearStatsTest, FsLldpClearStatsDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,Fslldpv2Version}, NULL, Fslldpv2VersionGet, Fslldpv2VersionSet, Fslldpv2VersionTest, Fslldpv2VersionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{12,Fslldpv2ConfigPortMapIfIndex}, GetNextIndexFslldpv2ConfigPortMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fslldpv2ConfigPortMapTableINDEX, 2, 0, 0, NULL},

{{12,Fslldpv2ConfigPortMapDestMacAddress}, GetNextIndexFslldpv2ConfigPortMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, Fslldpv2ConfigPortMapTableINDEX, 2, 0, 0, NULL},

{{12,Fslldpv2ConfigPortMapNum}, GetNextIndexFslldpv2ConfigPortMapTable, Fslldpv2ConfigPortMapNumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fslldpv2ConfigPortMapTableINDEX, 2, 0, 0, NULL},

{{12,Fslldpv2ConfigPortRowStatus}, GetNextIndexFslldpv2ConfigPortMapTable, Fslldpv2ConfigPortRowStatusGet, Fslldpv2ConfigPortRowStatusSet, Fslldpv2ConfigPortRowStatusTest, Fslldpv2ConfigPortMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fslldpv2ConfigPortMapTableINDEX, 2, 0, 1, NULL},

{{12,FslldpV2AddressTableIndex}, GetNextIndexFslldpV2DestAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FslldpV2DestAddressTableINDEX, 1, 0, 0, NULL},

{{12,FslldpV2DestMacAddress}, GetNextIndexFslldpV2DestAddressTable, FslldpV2DestMacAddressGet, FslldpV2DestMacAddressSet, FslldpV2DestMacAddressTest, FslldpV2DestAddressTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FslldpV2DestAddressTableINDEX, 1, 0, 0, NULL},

{{12,Fslldpv2DestRowStatus}, GetNextIndexFslldpV2DestAddressTable, Fslldpv2DestRowStatusGet, Fslldpv2DestRowStatusSet, Fslldpv2DestRowStatusTest, FslldpV2DestAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FslldpV2DestAddressTableINDEX, 1, 0, 1, NULL},

{{12,FsLldpStatsTaggedTxPortFramesTotal}, GetNextIndexFsLldpStatsTaggedTxPortTable, FsLldpStatsTaggedTxPortFramesTotalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLldpStatsTaggedTxPortTableINDEX, 2, 0, 0, NULL},
};
tMibData fslldpEntry = { 29, fslldpMibEntry };

#endif /* _FSLLDPDB_H */

