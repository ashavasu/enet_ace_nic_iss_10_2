/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lldtdfs.h,v 1.46 2016/11/01 12:17:02 siva Exp $
 *
 * Description: This file contains data structures defined for
 *              LLDP module.
 *********************************************************************/
#ifndef _LLDP_TDFS_H
#define _LLDP_TDFS_H

#include <lldcli.h>

typedef tLldpCliNeighborInfo tLldCliSizingNeighInfo;

/* Message types received by LLDP module */
enum
{
    LLDP_CREATE_PORT_MSG         = 0, /* Port creation */
    LLDP_DELETE_PORT_MSG         = 1, /* Port deletion */
    LLDP_MAP_PORT_MSG            = 2, /* Map Port*/
    LLDP_UNMAP_PORT_MSG          = 3, /* UnMap Port*/
    LLDP_OPER_STATUS_CHG_MSG     = 4, /* Oper status change */
    LLDP_PORT_IFALIAS_MSG        = 5, /* IfAlias change */
    LLDP_PORT_AGENT_CKTID_MSG    = 6, /* Agent CircuitID change */
    LLDP_SYS_NAME_CHG_MSG        = 7, /* System Name Change */
    LLDP_IP4_MAN_ADDR_CHG_MSG    = 8, /* IPv4 MgmtAddr Change*/
    LLDP_IP6_MAN_ADDR_CHG_MSG    = 9, /* IPv6 MgmtAddr Change*/
    LLDP_PORT_VLAN_ID_MSG        = 10,/* PVID */
    LLDP_PROT_VLAN_ID_MSG        = 11,/* Protocol vlan id */
    LLDP_PROT_VLAN_STATUS_MSG    = 12,/* Prot vlan enabled / disabled*/
    LLDP_VLAN_INFO_CHG_FOR_PORT  = 13,/* Vlan info change for a single port*/
    LLDP_VLAN_INFO_CHG_MSG       = 14,/* Vlan info change*/
    LLDP_PROT_IDENT_MSG          = 15,/* Protocols running on a port*/
    LLDP_POW_MDI_STATUS          = 16,
    LLDP_POW_PAIRS               = 17,
    LLDP_PORT_AGG_CAP_MSG        = 18,/* To indicate the change in aggregation
                                         capability of the port */
    LLDP_RESET_AGG_CAP_MSG       = 19,
    LLDP_PORT_AGG_STATUS_MSG     = 20,
    LLDP_RM_MSG                  = 21,
    LLDP_APPL_PORT_REGISTER      = 22,
    LLDP_APPL_PORT_DEREGISTER    = 23,
    LLDP_APPL_PORT_UPDATES       = 24,
    LLDP_SYS_DESC_CHG_MSG        = 25,
    LLDP_SET_DST_MAC             = 27,
    LLDP_PORT_DESC_CHG_MSG  = 28,
    LLDP_SVID_ADD                = 29,
    LLDP_SVID_DELETE             = 30,
    LLDP_MAX_MSG_TYPE            = 31
};

/* Action to be performed on the received message */
enum
{
    LLDP_ADD     = 1,
    LLDP_DELETE  = 2,
    LLDP_UPDATE  = 3
};

/* LLDP TLV Type */
enum
{
    LLDP_END_OF_LLDPDU_TLV = 0,  /* End Of LLDPDU TLV */
    LLDP_CHASSIS_ID_TLV    = 1,  /* Chassis ID TLV */
    LLDP_PORT_ID_TLV       = 2,  /* Port ID TLV */
    LLDP_TIME_TO_LIVE_TLV  = 3,  /* Time To Live TLV */ 
    LLDP_PORT_DESC_TLV     = 4,  /* Port Description TLV */
    LLDP_SYS_NAME_TLV      = 5,  /* System Name TLV */
    LLDP_SYS_DESC_TLV      = 6,  /* System Description TLV */
    LLDP_SYS_CAPAB_TLV     = 7,  /* System Capabilities TLV */
    LLDP_MAN_ADDR_TLV      = 8,  /* Management Address TLV */
    LLDP_ORG_SPEC_TLV      = 127 /* Organizationally Specific TLV */
};

/* LLDP DOT3 ORG SPEC TLV SubTypes */
enum
{
    LLDP_MAC_PHY_CONFIG_STATUS_TLV = 1,
    LLDP_POWER_VIA_MDI_TLV         = 2,
    /*LLDP_LINK_AGG_TLV              = 3,*/
    LLDP_LINK_AGG_TLV              = 3,
    LLDP_MAX_FRAME_SIZE_TLV        = 4
};

/* LLDP Man Address Interface Sub Type */
enum
{
    LLDP_MAN_ADDR_IF_SUB_UNKNOWN    = 1, /* Unknown */
    LLDP_MAN_ADDR_IF_SUB_IFINDEX    = 2, /* IfIndex */
    LLDP_MAN_ADDR_IF_SUB_SYSPORTNUM = 3, /* SystemPortNumber */
};

enum
{
   LLDP_MED_CAPABILITY_SUBTYPE   = 1, /* MED Capability TLV subtype */
   LLDP_MED_NW_POLICY_SUBTYPE    = 2, /* Network Policy TLV Subtype */
   LLDP_MED_LOCATION_ID_SUBTYPE  = 3, /* Location Identification TLV subtype */
   LLDP_MED_EXT_PW_MDI_SUBTYPE   = 4, /* Extended Power Via MDI TLV subtype */
   LLDP_MED_HW_REVISION_SUBTYPE  = 5, /* Inventory Hardwar Revision TLV subtype */
   LLDP_MED_FW_REVISION_SUBTYPE  = 6, /* Inventory Firmware Revision TLV subtype */
   LLDP_MED_SW_REVISION_SUBTYPE  = 7, /* Inventory Software Revision TLV subtype */
   LLDP_MED_SERIAL_NUM_SUBTYPE   = 8, /* Inventory Serial Number TLV subtype */
   LLDP_MED_MFG_NAME_SUBTYPE     = 9, /* Inventory Manufacturer Nmae TLV subtype */
   LLDP_MED_MODEL_NAME_SUBTYPE   = 10, /* Inventory Model Name TLV subtype */
   LLDP_MED_ASSET_ID_SUBTYPE     = 11  /* Inventory Asset Id TLV subtype */
};

/* LLDP MED Location Identification TLV location subtype as per standard ANSI/TIA-1057 */
enum
{
    LLDP_MED_COORDINATE                  = 1, /* Co-ordinate Location Subtype */
    LLDP_MED_CIVIC                       = 2, /* Civic Location Subtype */
    LLDP_MED_ELIN                        = 3  /* Elin Location Subtype */
};


/* LLDP MED Extended Power-Via-MDI TLV Device Type as per standard mib */
enum
{
    LLDP_MED_UNKNOWN                     = 1, /* Unknown device type */
    LLDP_MED_PSE                         = 2, /* PSE device */
    LLDP_MED_PD                          = 3  /* PD device */
};

/* LLDP MED Extended Power-Via-MDI TLV Power Source types for PSE Device as per standard mib */
enum
{
    LLDP_MED_PSE_UNKNOWN_MIB             = 1, /* Unknown Power Source */  
    LLDP_MED_PSE_PRIMARY_MIB             = 2, /* Primary Power Source */
    LLDP_MED_PSE_BACK_UP_MIB             = 3  /* Backup Power Source */
};

/* LLDP MED Extended Power-Via-MDI TLV Power Source types for PD Device as per standard mib */
enum
{
    LLDP_MED_PD_UNKNOWN_MIB              = 1, /* Unknown Power Source */
    LLDP_MED_PD_PSE_MIB                  = 2, /* PSE Power Source */
    LLDP_MED_PD_LOCAL_MIB                = 3, /* Local Power Source */
    LLDP_MED_PD_PSE_LOCAL_MIB            = 4  /* PSE and Local Power Source */
};

/* LLDP MED Extended Power-Via-MDI TLV Power Priority types as per standard mib */
enum
{
    LLDP_MED_PRI_UNKNOWN_MIB             = 1, /* Unknown Power Priority */
    LLDP_MED_PRI_CRITICAL_MIB            = 2, /* Priority Power Priority */
    LLDP_MED_PRI_HIGH_MIB                = 3, /* High Power Priority */
    LLDP_MED_PRI_LOW_MIB                 = 4  /* Low Power Priority */
};


/* as per ianaAddressFamilyNumbers sys mac address falls into
 *  * category other(0), ipV4(1), ipV4(2), all802(6)*/
#define LLDP_MAN_ADDR_SUB_TYPE_OTHER    6


/*------------------------------------------------------------------------
 *           Message Q Structures
 *-----------------------------------------------------------------------*/
/* MsgQ structure - For VlanInfo */
typedef struct VlanId
{
    UINT2     u2VlanId;
    UINT2     u2OldVlanId; /* Useful when the protocol vlan id configured for 
                            * given a port and protocol group is replaced with 
                            * another protocol vlan id
                            */
}tLldpQVlanId;

/* MsgQ structure - For ManAddr */
typedef struct
{
    tNetIpv4IfInfo  NetIpIfInfo;
    UINT4           u4BitMap;
}tLldpQIp4ManAddress;


/* Data structure to store the application info in LLDP */
typedef struct _LldpAppInfo
{
    tRBNodeEmbd AppRBNode;          /* RB Node */
    tLldpAppId  LldpAppId;          /* Unique application ID */
    VOID        (*pAppCallBackFn) (tLldpAppTlv *); 
                                    /* Applicationn Callback function */
    UINT4       u4PortId;           /* Port in which the application needs
                                     * to be registered */
    UINT4       u4LldpInstSelector;
                                    /* Destination MAC address to be used
                                     * in LLDP PDUs carrying this application
                                     * TLV. TODO: This is not useful now 
                                     * as LLDP-V2 is not supported */
    UINT1       *pu1TxAppTlv;       /* Application TLV buffer to be sent */
    UINT1       *pu1RxAppTlv;       /* Application TLV buffer received in the last 
                                       LLDP PDU */
    UINT1       (*pAppCallBackTakeTlvFrmAppFn) (UINT1 *pu1AppendTlv, UINT2 *u2TlvMaxLen,
                UINT4 u4LocPortNum, UINT1 u1FrameType); 
                                    /* Call back fn to be called for the application 
                                       to append its TLV.s to the Tx.ed LLDP PDU*/
    UINT2       u2TxAppTlvLen;      /* Tx TLV Length */
    UINT2       u2RxAppTlvLen;      /* Rx TLV Length */
    BOOL1       bAppTlvTxStatus;
                                    /* Flag to indicate whether the pu1TxAppTlv
                                     * needs to be present in the LLDP PDUs */
    BOOL1       bIsTlvRecvd;        /* Flag to indicate whether last received
                                     * LLDP PDU contained this application's TLV.
                                     * This flag will be set/reset during LLDP 
                                     * PDU parsing in LLDP module.*/
    UINT1       u1AppTlvRxStatus;   /* Flag to indicate whether the given
                                     * application TLV Rx should be indicated to the
                                     * application or not. The value must be OSIX_TRUE 
                                     * to indicate application's TLV Rx. To Disable the 
                                     * indication of application TLV Rx set value as
                                     * OSIX_FALSE */
    UINT1       u1AppPDURecv;       /* To be turned on when app wants the whole recvd LLDP PDU*/
    UINT1       u1TakeTlvFrmApp;    /* To be turned on when the application itself can append its tlv
                                     * to the LLDP PDU to be transmitted  */
    UINT1       u1AppRefreshFrameNotify;      /* To be turned on if the refresh frame also needs
                                               * to be notified to the registered application  */
    UINT1       u1AppNeighborExistenceNotify; /* To be turned on when app needs to know whenever an info 
                                               * from existing neighb our arrives at the switch*/
    UINT1       au1Padding[1];
}tLldpAppInfo;

/* Data structures used for Messaging/Interface with RM  */
#ifdef L2RED_WANTED
typedef struct _LldpRmCtrlMsg {
    tRmMsg           *pData;     /* RM message pointer */
    UINT2             u2DataLen; /* Length of RM message */
    UINT1             u1Event;   /* RM event */
    UINT1             au1Pad[1];
} tLldpRmCtrlMsg;
#endif /* L2RED_WANTED */

/* The structure of message passed from other modules to LLDP. */
typedef struct
{
    UINT4 u4MsgType;    /* Message type */
    UINT4 u4MsgSubType; /* Message sub type
                         * (Add/ Delete/ Update) */  
    UINT4 u4Port;
    UINT4 u4IfIndex;
    union
    {
#ifdef L2RED_WANTED
        tLldpRmCtrlMsg                    LldpRmCtrlMsg; /* Control Msg from RM 
                                                            Module */
#endif /* L2RED_WANTED */

        /* PortId subtype related variables */
        tLldpQVlanId                      VlanId;
        tLldpQIp4ManAddress               Ip4ManAddrQMsg; 
        tNetIpv6AddrChange                Ip6ManAddrQMsg;
        tPortList                         PortList; /* This variable is used by 
                                                     * LA and VLAN. In LA when
                                                     * port-channel is removed 
                                                     * it will notify LLDP with
                                                     * the list of ports whose
                                                     * aggregation capability 
                                                     * need to be resetted.
                                                     * In VLAN, when the name
                                                     * associated with a vlan 
                                                     * changes, it will notify 
                                                     * LLDP with the list of 
                                                     * ports mapped to that 
                                                     * vlan so that the 
                                                     * pre-formed buffer can be
                                                     * reconstructed with the 
                                                     * new value.
                                                     */
        tLldpAppPortMsg                   LldpAppPortMsg; /* Application posted
                                                             messsages */
        tLldpAgent                        LldpAgent;
        tLldpUpdateSvidOnUap              LldpUpdSvidOnUap;
        UINT4                             u4AgentCircuitId;
        UINT1                             au1IfAlias[CFA_MAX_PORT_NAME_LENGTH];
        UINT1                             au1LocSysName[LLDP_MAX_LEN_SYSNAME + 1];
        UINT1                             au1LocSysDesc[LLDP_MAX_LEN_SYSDESC + 1];
        UINT1      au1PortDesc[LLDP_MAX_LEN_PORTDESC + 1];  
        UINT1                             u1ProtVlanStatus;
        UINT1                             u1ProtocolStatus;
        UINT1                             u1AggCapability;
        UINT1                             u1AggStatus;
        UINT1                             u1PowPairs;
        UINT1                             u1OperStatus;   /* Used for link
                                                            status updation 
                                                            from CFA */
        BOOL1                             bPowMDIStatus; /* MDI power enabled or
                                                          * disbled */
    }unMsgParam;
    
#define IfAlias unMsgParam.au1IfAlias
#define AgentCircuitId unMsgParam.u4AgentCircuitId
#define NewVlanId unMsgParam.VlanId.u2VlanId
#define OldVlanId unMsgParam.VlanId.u2OldVlanId
#define ProtVlanStatus unMsgParam.u1ProtVlanStatus
#define AggCapability unMsgParam.u1AggCapability
#define AggStatus unMsgParam.u1AggStatus
#define PortList unMsgParam.PortList
#define OperStatus unMsgParam.u1OperStatus
#define SystemName unMsgParam.au1LocSysName
#define SystemDesc unMsgParam.au1LocSysDesc
#define PortDescConfig unMsgParam.au1PortDesc
#define Ip4ManAddr   unMsgParam.Ip4ManAddrQMsg
#define Ip6ManAddr   unMsgParam.Ip6ManAddrQMsg
#define AppPortMsg   unMsgParam.LldpAppPortMsg
#define Agent        unMsgParam.LldpAgent
#define UpdSvidOnUap unMsgParam.LldpUpdSvidOnUap

#ifdef L2RED_WANTED
#define LldpRmMsg    unMsgParam.LldpRmCtrlMsg
#endif /* L2RED_WANTED */

}tLldpQMsg;

/* This structure is used to post the LLDPDU to the LLDP RxPDU queue */
typedef struct
{
    tCRU_BUF_CHAIN_HEADER  *pLldpPdu; /* Received LLDPDU */
    UINT4                  u4Port;    /* Received Port Number */
}tLldpRxPduQMsg;

/*--------------------END: MessageQ--------------------------------------- */


/* LLDP Local Management Address  */
typedef struct LldpLocManAddrTable
{
    tRBNodeEmbd      LocManAddrRBNode;     /* RB Node */
    tLldpManAddrOid  LocManAddrOID;        /* Management Address OID */
    UINT4            u4L3IfIndex;          /* Layer 3 If Index for the Ip Addr*/
    INT4             i4LocManAddrSubtype;  /* INDEX: Management Address Subtype*/
    INT4             i4LocManAddrLen;      /* Management Address Length */
    INT4             i4LocManAddrIfSubtype;/* Management Address Interface 
                                              Subtype*/
    INT4             i4LocManAddrIfId;     /* Management Address Interface Id */
    UINT1            au1LocManAddr[LLDP_MAX_LEN_MAN_ADDR + 1]; /* INDEX: Management 
                                                            address value */
    UINT1            au1LocManAddrPortsTxEnable[LLDP_MAN_ADDR_TX_EN_MAX_BYTES];
                     /* port list of 4096 bits, ie, a bit for each port */
    UINT1            au1LocManAddrRowstats[LLDP_MAN_ADDR_TX_EN_MAX_BYTES];
                      /* port list to indicate row status */
    UINT1            u1OperStatus;         /* To specify the operational status of 
                                              management address */
    UINT1             au1Pad[3];
} tLldpLocManAddrTable;

/* ----- IEEE 802.1 Org Specific Info ----- */
/* Local Dot1 Oraganizationally specific info*/

typedef struct Lldpxdot1LocProtoVlanInfo
{
    tTMO_DLL_NODE     NextLocProtoVlanNode;   /* Points to the next Node */ 
    UINT2             u2ProtoVlanId;          /* INDEX : Protocol Vlan id*/
    UINT1             u1ProtoVlanTxEnabled;
    UINT1             au1Pad[1];
}tLldpxdot1LocProtoVlanInfo;

typedef struct Lldpxdot1LocProtoIdInfo
{
    tRBNodeEmbd         NextLocProtoIdRBNode; /* Points to the next RBNode */
    UINT4               u4IfIndex;            /* INDEX : Port Number */
    UINT4               u4ProtocolIndex;      /* INDEX : Protocol Index */
    UINT1               au1ProtocolId [LLDP_MAX_LEN_PROTOID + 1]; /* Protocol Id */
    UINT1               u1ProtoTxEnable;
    UINT1               au1Pad[3];
}tLldpxdot1LocProtoIdInfo;


/* This structure is used while getting the Remote RBNode on which Dot3Tlv
 * is received*/ 
typedef struct 
{
    tLldpRemoteNode *pRemNode;
    UINT1            u1Dot3TlvType;
    UINT1            au1Pad[3];
}tLldpRemNodeInfo;

typedef struct LldpOctetStringType{
        UINT1  *pu1_OctetList;
        INT4   i4_Length;
} tLldpOctetStringType;

/* The following structure is used to check the presence of duplicate VLAN Name 
 * TLV during parsing stage itself */
typedef struct
{
 UINT1 au1VlanNameTlvCount[LLDP_MAX_VLAN_ID]; /* The index of the array represents
             the VLAN ID.*/
}tLldpVlanNameTlvCounter;
#endif /* _LLDP_TDFS_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  lldptdfs.h                     */
/*-----------------------------------------------------------------------*/
