/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdllddb.h,v 1.4 2008/08/20 15:13:21 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDLLDDB_H
#define _STDLLDDB_H

UINT1 LldpPortConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpConfigManAddrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 LldpStatsTxPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpStatsRxPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpLocPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpLocManAddrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 LldpRemTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpRemManAddrTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 LldpRemUnknownTLVTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpRemOrgDefInfoTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,3 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 stdlld [] ={1,0,8802,1,1,2};
tSNMP_OID_TYPE stdlldOID = {6, stdlld};


UINT4 LldpMessageTxInterval [ ] ={1,0,8802,1,1,2,1,1,1};
UINT4 LldpMessageTxHoldMultiplier [ ] ={1,0,8802,1,1,2,1,1,2};
UINT4 LldpReinitDelay [ ] ={1,0,8802,1,1,2,1,1,3};
UINT4 LldpTxDelay [ ] ={1,0,8802,1,1,2,1,1,4};
UINT4 LldpNotificationInterval [ ] ={1,0,8802,1,1,2,1,1,5};
UINT4 LldpPortConfigPortNum [ ] ={1,0,8802,1,1,2,1,1,6,1,1};
UINT4 LldpPortConfigAdminStatus [ ] ={1,0,8802,1,1,2,1,1,6,1,2};
UINT4 LldpPortConfigNotificationEnable [ ] ={1,0,8802,1,1,2,1,1,6,1,3};
UINT4 LldpPortConfigTLVsTxEnable [ ] ={1,0,8802,1,1,2,1,1,6,1,4};
UINT4 LldpConfigManAddrPortsTxEnable [ ] ={1,0,8802,1,1,2,1,1,7,1,1};
UINT4 LldpStatsRemTablesLastChangeTime [ ] ={1,0,8802,1,1,2,1,2,1};
UINT4 LldpStatsRemTablesInserts [ ] ={1,0,8802,1,1,2,1,2,2};
UINT4 LldpStatsRemTablesDeletes [ ] ={1,0,8802,1,1,2,1,2,3};
UINT4 LldpStatsRemTablesDrops [ ] ={1,0,8802,1,1,2,1,2,4};
UINT4 LldpStatsRemTablesAgeouts [ ] ={1,0,8802,1,1,2,1,2,5};
UINT4 LldpStatsTxPortNum [ ] ={1,0,8802,1,1,2,1,2,6,1,1};
UINT4 LldpStatsTxPortFramesTotal [ ] ={1,0,8802,1,1,2,1,2,6,1,2};
UINT4 LldpStatsRxPortNum [ ] ={1,0,8802,1,1,2,1,2,7,1,1};
UINT4 LldpStatsRxPortFramesDiscardedTotal [ ] ={1,0,8802,1,1,2,1,2,7,1,2};
UINT4 LldpStatsRxPortFramesErrors [ ] ={1,0,8802,1,1,2,1,2,7,1,3};
UINT4 LldpStatsRxPortFramesTotal [ ] ={1,0,8802,1,1,2,1,2,7,1,4};
UINT4 LldpStatsRxPortTLVsDiscardedTotal [ ] ={1,0,8802,1,1,2,1,2,7,1,5};
UINT4 LldpStatsRxPortTLVsUnrecognizedTotal [ ] ={1,0,8802,1,1,2,1,2,7,1,6};
UINT4 LldpStatsRxPortAgeoutsTotal [ ] ={1,0,8802,1,1,2,1,2,7,1,7};
UINT4 LldpLocChassisIdSubtype [ ] ={1,0,8802,1,1,2,1,3,1};
UINT4 LldpLocChassisId [ ] ={1,0,8802,1,1,2,1,3,2};
UINT4 LldpLocSysName [ ] ={1,0,8802,1,1,2,1,3,3};
UINT4 LldpLocSysDesc [ ] ={1,0,8802,1,1,2,1,3,4};
UINT4 LldpLocSysCapSupported [ ] ={1,0,8802,1,1,2,1,3,5};
UINT4 LldpLocSysCapEnabled [ ] ={1,0,8802,1,1,2,1,3,6};
UINT4 LldpLocPortNum [ ] ={1,0,8802,1,1,2,1,3,7,1,1};
UINT4 LldpLocPortIdSubtype [ ] ={1,0,8802,1,1,2,1,3,7,1,2};
UINT4 LldpLocPortId [ ] ={1,0,8802,1,1,2,1,3,7,1,3};
UINT4 LldpLocPortDesc [ ] ={1,0,8802,1,1,2,1,3,7,1,4};
UINT4 LldpLocManAddrSubtype [ ] ={1,0,8802,1,1,2,1,3,8,1,1};
UINT4 LldpLocManAddr [ ] ={1,0,8802,1,1,2,1,3,8,1,2};
UINT4 LldpLocManAddrLen [ ] ={1,0,8802,1,1,2,1,3,8,1,3};
UINT4 LldpLocManAddrIfSubtype [ ] ={1,0,8802,1,1,2,1,3,8,1,4};
UINT4 LldpLocManAddrIfId [ ] ={1,0,8802,1,1,2,1,3,8,1,5};
UINT4 LldpLocManAddrOID [ ] ={1,0,8802,1,1,2,1,3,8,1,6};
UINT4 LldpRemTimeMark [ ] ={1,0,8802,1,1,2,1,4,1,1,1};
UINT4 LldpRemLocalPortNum [ ] ={1,0,8802,1,1,2,1,4,1,1,2};
UINT4 LldpRemIndex [ ] ={1,0,8802,1,1,2,1,4,1,1,3};
UINT4 LldpRemChassisIdSubtype [ ] ={1,0,8802,1,1,2,1,4,1,1,4};
UINT4 LldpRemChassisId [ ] ={1,0,8802,1,1,2,1,4,1,1,5};
UINT4 LldpRemPortIdSubtype [ ] ={1,0,8802,1,1,2,1,4,1,1,6};
UINT4 LldpRemPortId [ ] ={1,0,8802,1,1,2,1,4,1,1,7};
UINT4 LldpRemPortDesc [ ] ={1,0,8802,1,1,2,1,4,1,1,8};
UINT4 LldpRemSysName [ ] ={1,0,8802,1,1,2,1,4,1,1,9};
UINT4 LldpRemSysDesc [ ] ={1,0,8802,1,1,2,1,4,1,1,10};
UINT4 LldpRemSysCapSupported [ ] ={1,0,8802,1,1,2,1,4,1,1,11};
UINT4 LldpRemSysCapEnabled [ ] ={1,0,8802,1,1,2,1,4,1,1,12};
UINT4 LldpRemManAddrSubtype [ ] ={1,0,8802,1,1,2,1,4,2,1,1};
UINT4 LldpRemManAddr [ ] ={1,0,8802,1,1,2,1,4,2,1,2};
UINT4 LldpRemManAddrIfSubtype [ ] ={1,0,8802,1,1,2,1,4,2,1,3};
UINT4 LldpRemManAddrIfId [ ] ={1,0,8802,1,1,2,1,4,2,1,4};
UINT4 LldpRemManAddrOID [ ] ={1,0,8802,1,1,2,1,4,2,1,5};
UINT4 LldpRemUnknownTLVType [ ] ={1,0,8802,1,1,2,1,4,3,1,1};
UINT4 LldpRemUnknownTLVInfo [ ] ={1,0,8802,1,1,2,1,4,3,1,2};
UINT4 LldpRemOrgDefInfoOUI [ ] ={1,0,8802,1,1,2,1,4,4,1,1};
UINT4 LldpRemOrgDefInfoSubtype [ ] ={1,0,8802,1,1,2,1,4,4,1,2};
UINT4 LldpRemOrgDefInfoIndex [ ] ={1,0,8802,1,1,2,1,4,4,1,3};
UINT4 LldpRemOrgDefInfo [ ] ={1,0,8802,1,1,2,1,4,4,1,4};


tMbDbEntry stdlldMibEntry[]= {

{{9,LldpMessageTxInterval}, NULL, LldpMessageTxIntervalGet, LldpMessageTxIntervalSet, LldpMessageTxIntervalTest, LldpMessageTxIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "30"},

{{9,LldpMessageTxHoldMultiplier}, NULL, LldpMessageTxHoldMultiplierGet, LldpMessageTxHoldMultiplierSet, LldpMessageTxHoldMultiplierTest, LldpMessageTxHoldMultiplierDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "4"},

{{9,LldpReinitDelay}, NULL, LldpReinitDelayGet, LldpReinitDelaySet, LldpReinitDelayTest, LldpReinitDelayDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{9,LldpTxDelay}, NULL, LldpTxDelayGet, LldpTxDelaySet, LldpTxDelayTest, LldpTxDelayDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{9,LldpNotificationInterval}, NULL, LldpNotificationIntervalGet, LldpNotificationIntervalSet, LldpNotificationIntervalTest, LldpNotificationIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{11,LldpPortConfigPortNum}, GetNextIndexLldpPortConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, LldpPortConfigTableINDEX, 1, 0, 0, NULL},

{{11,LldpPortConfigAdminStatus}, GetNextIndexLldpPortConfigTable, LldpPortConfigAdminStatusGet, LldpPortConfigAdminStatusSet, LldpPortConfigAdminStatusTest, LldpPortConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpPortConfigTableINDEX, 1, 0, 0, "3"},

{{11,LldpPortConfigNotificationEnable}, GetNextIndexLldpPortConfigTable, LldpPortConfigNotificationEnableGet, LldpPortConfigNotificationEnableSet, LldpPortConfigNotificationEnableTest, LldpPortConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpPortConfigTableINDEX, 1, 0, 0, "2"},

{{11,LldpPortConfigTLVsTxEnable}, GetNextIndexLldpPortConfigTable, LldpPortConfigTLVsTxEnableGet, LldpPortConfigTLVsTxEnableSet, LldpPortConfigTLVsTxEnableTest, LldpPortConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, LldpPortConfigTableINDEX, 1, 0, 0, NULL},

{{11,LldpConfigManAddrPortsTxEnable}, GetNextIndexLldpConfigManAddrTable, LldpConfigManAddrPortsTxEnableGet, LldpConfigManAddrPortsTxEnableSet, LldpConfigManAddrPortsTxEnableTest, LldpConfigManAddrTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, LldpConfigManAddrTableINDEX, 2, 0, 0, "0"},

{{9,LldpStatsRemTablesLastChangeTime}, NULL, LldpStatsRemTablesLastChangeTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,LldpStatsRemTablesInserts}, NULL, LldpStatsRemTablesInsertsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,LldpStatsRemTablesDeletes}, NULL, LldpStatsRemTablesDeletesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,LldpStatsRemTablesDrops}, NULL, LldpStatsRemTablesDropsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,LldpStatsRemTablesAgeouts}, NULL, LldpStatsRemTablesAgeoutsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,LldpStatsTxPortNum}, GetNextIndexLldpStatsTxPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, LldpStatsTxPortTableINDEX, 1, 0, 0, NULL},

{{11,LldpStatsTxPortFramesTotal}, GetNextIndexLldpStatsTxPortTable, LldpStatsTxPortFramesTotalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, LldpStatsTxPortTableINDEX, 1, 0, 0, NULL},

{{11,LldpStatsRxPortNum}, GetNextIndexLldpStatsRxPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, LldpStatsRxPortTableINDEX, 1, 0, 0, NULL},

{{11,LldpStatsRxPortFramesDiscardedTotal}, GetNextIndexLldpStatsRxPortTable, LldpStatsRxPortFramesDiscardedTotalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, LldpStatsRxPortTableINDEX, 1, 0, 0, NULL},

{{11,LldpStatsRxPortFramesErrors}, GetNextIndexLldpStatsRxPortTable, LldpStatsRxPortFramesErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, LldpStatsRxPortTableINDEX, 1, 0, 0, NULL},

{{11,LldpStatsRxPortFramesTotal}, GetNextIndexLldpStatsRxPortTable, LldpStatsRxPortFramesTotalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, LldpStatsRxPortTableINDEX, 1, 0, 0, NULL},

{{11,LldpStatsRxPortTLVsDiscardedTotal}, GetNextIndexLldpStatsRxPortTable, LldpStatsRxPortTLVsDiscardedTotalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, LldpStatsRxPortTableINDEX, 1, 0, 0, NULL},

{{11,LldpStatsRxPortTLVsUnrecognizedTotal}, GetNextIndexLldpStatsRxPortTable, LldpStatsRxPortTLVsUnrecognizedTotalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, LldpStatsRxPortTableINDEX, 1, 0, 0, NULL},

{{11,LldpStatsRxPortAgeoutsTotal}, GetNextIndexLldpStatsRxPortTable, LldpStatsRxPortAgeoutsTotalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, LldpStatsRxPortTableINDEX, 1, 0, 0, NULL},

{{9,LldpLocChassisIdSubtype}, NULL, LldpLocChassisIdSubtypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,LldpLocChassisId}, NULL, LldpLocChassisIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,LldpLocSysName}, NULL, LldpLocSysNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,LldpLocSysDesc}, NULL, LldpLocSysDescGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,LldpLocSysCapSupported}, NULL, LldpLocSysCapSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,LldpLocSysCapEnabled}, NULL, LldpLocSysCapEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,LldpLocPortNum}, GetNextIndexLldpLocPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, LldpLocPortTableINDEX, 1, 0, 0, NULL},

{{11,LldpLocPortIdSubtype}, GetNextIndexLldpLocPortTable, LldpLocPortIdSubtypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpLocPortTableINDEX, 1, 0, 0, NULL},

{{11,LldpLocPortId}, GetNextIndexLldpLocPortTable, LldpLocPortIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpLocPortTableINDEX, 1, 0, 0, NULL},

{{11,LldpLocPortDesc}, GetNextIndexLldpLocPortTable, LldpLocPortDescGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpLocPortTableINDEX, 1, 0, 0, NULL},

{{11,LldpLocManAddrSubtype}, GetNextIndexLldpLocManAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, LldpLocManAddrTableINDEX, 2, 0, 0, NULL},

{{11,LldpLocManAddr}, GetNextIndexLldpLocManAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, LldpLocManAddrTableINDEX, 2, 0, 0, NULL},

{{11,LldpLocManAddrLen}, GetNextIndexLldpLocManAddrTable, LldpLocManAddrLenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpLocManAddrTableINDEX, 2, 0, 0, NULL},

{{11,LldpLocManAddrIfSubtype}, GetNextIndexLldpLocManAddrTable, LldpLocManAddrIfSubtypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpLocManAddrTableINDEX, 2, 0, 0, NULL},

{{11,LldpLocManAddrIfId}, GetNextIndexLldpLocManAddrTable, LldpLocManAddrIfIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpLocManAddrTableINDEX, 2, 0, 0, NULL},

{{11,LldpLocManAddrOID}, GetNextIndexLldpLocManAddrTable, LldpLocManAddrOIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, LldpLocManAddrTableINDEX, 2, 0, 0, NULL},

{{11,LldpRemTimeMark}, GetNextIndexLldpRemTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpRemTableINDEX, 3, 0, 0, NULL},

{{11,LldpRemLocalPortNum}, GetNextIndexLldpRemTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, LldpRemTableINDEX, 3, 0, 0, NULL},

{{11,LldpRemIndex}, GetNextIndexLldpRemTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, LldpRemTableINDEX, 3, 0, 0, NULL},

{{11,LldpRemChassisIdSubtype}, GetNextIndexLldpRemTable, LldpRemChassisIdSubtypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpRemTableINDEX, 3, 0, 0, NULL},

{{11,LldpRemChassisId}, GetNextIndexLldpRemTable, LldpRemChassisIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpRemTableINDEX, 3, 0, 0, NULL},

{{11,LldpRemPortIdSubtype}, GetNextIndexLldpRemTable, LldpRemPortIdSubtypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpRemTableINDEX, 3, 0, 0, NULL},

{{11,LldpRemPortId}, GetNextIndexLldpRemTable, LldpRemPortIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpRemTableINDEX, 3, 0, 0, NULL},

{{11,LldpRemPortDesc}, GetNextIndexLldpRemTable, LldpRemPortDescGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpRemTableINDEX, 3, 0, 0, NULL},

{{11,LldpRemSysName}, GetNextIndexLldpRemTable, LldpRemSysNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpRemTableINDEX, 3, 0, 0, NULL},

{{11,LldpRemSysDesc}, GetNextIndexLldpRemTable, LldpRemSysDescGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpRemTableINDEX, 3, 0, 0, NULL},

{{11,LldpRemSysCapSupported}, GetNextIndexLldpRemTable, LldpRemSysCapSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpRemTableINDEX, 3, 0, 0, NULL},

{{11,LldpRemSysCapEnabled}, GetNextIndexLldpRemTable, LldpRemSysCapEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpRemTableINDEX, 3, 0, 0, NULL},

{{11,LldpRemManAddrSubtype}, GetNextIndexLldpRemManAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, LldpRemManAddrTableINDEX, 5, 0, 0, NULL},

{{11,LldpRemManAddr}, GetNextIndexLldpRemManAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, LldpRemManAddrTableINDEX, 5, 0, 0, NULL},

{{11,LldpRemManAddrIfSubtype}, GetNextIndexLldpRemManAddrTable, LldpRemManAddrIfSubtypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpRemManAddrTableINDEX, 5, 0, 0, NULL},

{{11,LldpRemManAddrIfId}, GetNextIndexLldpRemManAddrTable, LldpRemManAddrIfIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpRemManAddrTableINDEX, 5, 0, 0, NULL},

{{11,LldpRemManAddrOID}, GetNextIndexLldpRemManAddrTable, LldpRemManAddrOIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, LldpRemManAddrTableINDEX, 5, 0, 0, NULL},

{{11,LldpRemUnknownTLVType}, GetNextIndexLldpRemUnknownTLVTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, LldpRemUnknownTLVTableINDEX, 4, 0, 0, NULL},

{{11,LldpRemUnknownTLVInfo}, GetNextIndexLldpRemUnknownTLVTable, LldpRemUnknownTLVInfoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpRemUnknownTLVTableINDEX, 4, 0, 0, NULL},

{{11,LldpRemOrgDefInfoOUI}, GetNextIndexLldpRemOrgDefInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, LldpRemOrgDefInfoTableINDEX, 6, 0, 0, NULL},

{{11,LldpRemOrgDefInfoSubtype}, GetNextIndexLldpRemOrgDefInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, LldpRemOrgDefInfoTableINDEX, 6, 0, 0, NULL},

{{11,LldpRemOrgDefInfoIndex}, GetNextIndexLldpRemOrgDefInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, LldpRemOrgDefInfoTableINDEX, 6, 0, 0, NULL},

{{11,LldpRemOrgDefInfo}, GetNextIndexLldpRemOrgDefInfoTable, LldpRemOrgDefInfoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpRemOrgDefInfoTableINDEX, 6, 0, 0, NULL},
};
tMibData stdlldEntry = { 63, stdlldMibEntry };
#endif /* _STDLLDDB_H */

