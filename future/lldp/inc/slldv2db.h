/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: slldv2db.h,v 1.2 2013/03/28 11:45:03 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _SLLDV2DB_H
#define _SLLDV2DB_H

UINT1 LldpV2PortConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpV2DestAddressTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpV2ManAddrConfigTxPortsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 LldpV2StatsTxPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpV2StatsRxPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpV2LocPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 LldpV2LocManAddrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 LldpV2RemTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpV2RemManAddrTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 LldpV2RemUnknownTLVTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpV2RemOrgDefInfoTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,3 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 slldv2 [] ={1,3,111,2,802,1,1,13};
tSNMP_OID_TYPE slldv2OID = {8, slldv2};


UINT4 LldpV2MessageTxInterval [ ] ={1,3,111,2,802,1,1,13,1,1,1};
UINT4 LldpV2MessageTxHoldMultiplier [ ] ={1,3,111,2,802,1,1,13,1,1,2};
UINT4 LldpV2ReinitDelay [ ] ={1,3,111,2,802,1,1,13,1,1,3};
UINT4 LldpV2NotificationInterval [ ] ={1,3,111,2,802,1,1,13,1,1,4};
UINT4 LldpV2TxCreditMax [ ] ={1,3,111,2,802,1,1,13,1,1,5};
UINT4 LldpV2MessageFastTx [ ] ={1,3,111,2,802,1,1,13,1,1,6};
UINT4 LldpV2TxFastInit [ ] ={1,3,111,2,802,1,1,13,1,1,7};
UINT4 LldpV2PortConfigIfIndex [ ] ={1,3,111,2,802,1,1,13,1,1,8,1,1};
UINT4 LldpV2PortConfigDestAddressIndex [ ] ={1,3,111,2,802,1,1,13,1,1,8,1,2};
UINT4 LldpV2PortConfigAdminStatus [ ] ={1,3,111,2,802,1,1,13,1,1,8,1,3};
UINT4 LldpV2PortConfigNotificationEnable [ ] ={1,3,111,2,802,1,1,13,1,1,8,1,4};
UINT4 LldpV2PortConfigTLVsTxEnable [ ] ={1,3,111,2,802,1,1,13,1,1,8,1,5};
UINT4 LldpV2AddressTableIndex [ ] ={1,3,111,2,802,1,1,13,1,1,9,1,1};
UINT4 LldpV2DestMacAddress [ ] ={1,3,111,2,802,1,1,13,1,1,9,1,2};
UINT4 LldpV2ManAddrConfigIfIndex [ ] ={1,3,111,2,802,1,1,13,1,1,10,1,1};
UINT4 LldpV2ManAddrConfigDestAddressIndex [ ] ={1,3,111,2,802,1,1,13,1,1,10,1,2};
UINT4 LldpV2ManAddrConfigLocManAddrSubtype [ ] ={1,3,111,2,802,1,1,13,1,1,10,1,3};
UINT4 LldpV2ManAddrConfigLocManAddr [ ] ={1,3,111,2,802,1,1,13,1,1,10,1,4};
UINT4 LldpV2ManAddrConfigTxEnable [ ] ={1,3,111,2,802,1,1,13,1,1,10,1,5};
UINT4 LldpV2ManAddrConfigRowStatus [ ] ={1,3,111,2,802,1,1,13,1,1,10,1,6};
UINT4 LldpV2StatsRemTablesLastChangeTime [ ] ={1,3,111,2,802,1,1,13,1,2,1};
UINT4 LldpV2StatsRemTablesInserts [ ] ={1,3,111,2,802,1,1,13,1,2,2};
UINT4 LldpV2StatsRemTablesDeletes [ ] ={1,3,111,2,802,1,1,13,1,2,3};
UINT4 LldpV2StatsRemTablesDrops [ ] ={1,3,111,2,802,1,1,13,1,2,4};
UINT4 LldpV2StatsRemTablesAgeouts [ ] ={1,3,111,2,802,1,1,13,1,2,5};
UINT4 LldpV2StatsTxIfIndex [ ] ={1,3,111,2,802,1,1,13,1,2,6,1,1};
UINT4 LldpV2StatsTxDestMACAddress [ ] ={1,3,111,2,802,1,1,13,1,2,6,1,2};
UINT4 LldpV2StatsTxPortFramesTotal [ ] ={1,3,111,2,802,1,1,13,1,2,6,1,3};
UINT4 LldpV2StatsTxLLDPDULengthErrors [ ] ={1,3,111,2,802,1,1,13,1,2,6,1,4};
UINT4 LldpV2StatsRxDestIfIndex [ ] ={1,3,111,2,802,1,1,13,1,2,7,1,1};
UINT4 LldpV2StatsRxDestMACAddress [ ] ={1,3,111,2,802,1,1,13,1,2,7,1,2};
UINT4 LldpV2StatsRxPortFramesDiscardedTotal [ ] ={1,3,111,2,802,1,1,13,1,2,7,1,3};
UINT4 LldpV2StatsRxPortFramesErrors [ ] ={1,3,111,2,802,1,1,13,1,2,7,1,4};
UINT4 LldpV2StatsRxPortFramesTotal [ ] ={1,3,111,2,802,1,1,13,1,2,7,1,5};
UINT4 LldpV2StatsRxPortTLVsDiscardedTotal [ ] ={1,3,111,2,802,1,1,13,1,2,7,1,6};
UINT4 LldpV2StatsRxPortTLVsUnrecognizedTotal [ ] ={1,3,111,2,802,1,1,13,1,2,7,1,7};
UINT4 LldpV2StatsRxPortAgeoutsTotal [ ] ={1,3,111,2,802,1,1,13,1,2,7,1,8};
UINT4 LldpV2LocChassisIdSubtype [ ] ={1,3,111,2,802,1,1,13,1,3,1};
UINT4 LldpV2LocChassisId [ ] ={1,3,111,2,802,1,1,13,1,3,2};
UINT4 LldpV2LocSysName [ ] ={1,3,111,2,802,1,1,13,1,3,3};
UINT4 LldpV2LocSysDesc [ ] ={1,3,111,2,802,1,1,13,1,3,4};
UINT4 LldpV2LocSysCapSupported [ ] ={1,3,111,2,802,1,1,13,1,3,5};
UINT4 LldpV2LocSysCapEnabled [ ] ={1,3,111,2,802,1,1,13,1,3,6};
UINT4 LldpV2LocPortIfIndex [ ] ={1,3,111,2,802,1,1,13,1,3,7,1,1};
UINT4 LldpV2LocPortIdSubtype [ ] ={1,3,111,2,802,1,1,13,1,3,7,1,2};
UINT4 LldpV2LocPortId [ ] ={1,3,111,2,802,1,1,13,1,3,7,1,3};
UINT4 LldpV2LocPortDesc [ ] ={1,3,111,2,802,1,1,13,1,3,7,1,4};
UINT4 LldpV2LocManAddrSubtype [ ] ={1,3,111,2,802,1,1,13,1,3,8,1,1};
UINT4 LldpV2LocManAddr [ ] ={1,3,111,2,802,1,1,13,1,3,8,1,2};
UINT4 LldpV2LocManAddrLen [ ] ={1,3,111,2,802,1,1,13,1,3,8,1,3};
UINT4 LldpV2LocManAddrIfSubtype [ ] ={1,3,111,2,802,1,1,13,1,3,8,1,4};
UINT4 LldpV2LocManAddrIfId [ ] ={1,3,111,2,802,1,1,13,1,3,8,1,5};
UINT4 LldpV2LocManAddrOID [ ] ={1,3,111,2,802,1,1,13,1,3,8,1,6};
UINT4 LldpV2RemTimeMark [ ] ={1,3,111,2,802,1,1,13,1,4,1,1,1};
UINT4 LldpV2RemLocalIfIndex [ ] ={1,3,111,2,802,1,1,13,1,4,1,1,2};
UINT4 LldpV2RemLocalDestMACAddress [ ] ={1,3,111,2,802,1,1,13,1,4,1,1,3};
UINT4 LldpV2RemIndex [ ] ={1,3,111,2,802,1,1,13,1,4,1,1,4};
UINT4 LldpV2RemChassisIdSubtype [ ] ={1,3,111,2,802,1,1,13,1,4,1,1,5};
UINT4 LldpV2RemChassisId [ ] ={1,3,111,2,802,1,1,13,1,4,1,1,6};
UINT4 LldpV2RemPortIdSubtype [ ] ={1,3,111,2,802,1,1,13,1,4,1,1,7};
UINT4 LldpV2RemPortId [ ] ={1,3,111,2,802,1,1,13,1,4,1,1,8};
UINT4 LldpV2RemPortDesc [ ] ={1,3,111,2,802,1,1,13,1,4,1,1,9};
UINT4 LldpV2RemSysName [ ] ={1,3,111,2,802,1,1,13,1,4,1,1,10};
UINT4 LldpV2RemSysDesc [ ] ={1,3,111,2,802,1,1,13,1,4,1,1,11};
UINT4 LldpV2RemSysCapSupported [ ] ={1,3,111,2,802,1,1,13,1,4,1,1,12};
UINT4 LldpV2RemSysCapEnabled [ ] ={1,3,111,2,802,1,1,13,1,4,1,1,13};
UINT4 LldpV2RemRemoteChanges [ ] ={1,3,111,2,802,1,1,13,1,4,1,1,14};
UINT4 LldpV2RemTooManyNeighbors [ ] ={1,3,111,2,802,1,1,13,1,4,1,1,15};
UINT4 LldpV2RemManAddrSubtype [ ] ={1,3,111,2,802,1,1,13,1,4,2,1,1};
UINT4 LldpV2RemManAddr [ ] ={1,3,111,2,802,1,1,13,1,4,2,1,2};
UINT4 LldpV2RemManAddrIfSubtype [ ] ={1,3,111,2,802,1,1,13,1,4,2,1,3};
UINT4 LldpV2RemManAddrIfId [ ] ={1,3,111,2,802,1,1,13,1,4,2,1,4};
UINT4 LldpV2RemManAddrOID [ ] ={1,3,111,2,802,1,1,13,1,4,2,1,5};
UINT4 LldpV2RemUnknownTLVType [ ] ={1,3,111,2,802,1,1,13,1,4,3,1,1};
UINT4 LldpV2RemUnknownTLVInfo [ ] ={1,3,111,2,802,1,1,13,1,4,3,1,2};
UINT4 LldpV2RemOrgDefInfoOUI [ ] ={1,3,111,2,802,1,1,13,1,4,4,1,1};
UINT4 LldpV2RemOrgDefInfoSubtype [ ] ={1,3,111,2,802,1,1,13,1,4,4,1,2};
UINT4 LldpV2RemOrgDefInfoIndex [ ] ={1,3,111,2,802,1,1,13,1,4,4,1,3};
UINT4 LldpV2RemOrgDefInfo [ ] ={1,3,111,2,802,1,1,13,1,4,4,1,4};




tMbDbEntry slldv2MibEntry[]= {

{{11,LldpV2MessageTxInterval}, NULL, LldpV2MessageTxIntervalGet, LldpV2MessageTxIntervalSet, LldpV2MessageTxIntervalTest, LldpV2MessageTxIntervalDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "30"},

{{11,LldpV2MessageTxHoldMultiplier}, NULL, LldpV2MessageTxHoldMultiplierGet, LldpV2MessageTxHoldMultiplierSet, LldpV2MessageTxHoldMultiplierTest, LldpV2MessageTxHoldMultiplierDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "4"},

{{11,LldpV2ReinitDelay}, NULL, LldpV2ReinitDelayGet, LldpV2ReinitDelaySet, LldpV2ReinitDelayTest, LldpV2ReinitDelayDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,LldpV2NotificationInterval}, NULL, LldpV2NotificationIntervalGet, LldpV2NotificationIntervalSet, LldpV2NotificationIntervalTest, LldpV2NotificationIntervalDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "30"},

{{11,LldpV2TxCreditMax}, NULL, LldpV2TxCreditMaxGet, LldpV2TxCreditMaxSet, LldpV2TxCreditMaxTest, LldpV2TxCreditMaxDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{11,LldpV2MessageFastTx}, NULL, LldpV2MessageFastTxGet, LldpV2MessageFastTxSet, LldpV2MessageFastTxTest, LldpV2MessageFastTxDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,LldpV2TxFastInit}, NULL, LldpV2TxFastInitGet, LldpV2TxFastInitSet, LldpV2TxFastInitTest, LldpV2TxFastInitDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "4"},

{{13,LldpV2PortConfigIfIndex}, GetNextIndexLldpV2PortConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, LldpV2PortConfigTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2PortConfigDestAddressIndex}, GetNextIndexLldpV2PortConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpV2PortConfigTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2PortConfigAdminStatus}, GetNextIndexLldpV2PortConfigTable, LldpV2PortConfigAdminStatusGet, LldpV2PortConfigAdminStatusSet, LldpV2PortConfigAdminStatusTest, LldpV2PortConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpV2PortConfigTableINDEX, 2, 0, 0, "3"},

{{13,LldpV2PortConfigNotificationEnable}, GetNextIndexLldpV2PortConfigTable, LldpV2PortConfigNotificationEnableGet, LldpV2PortConfigNotificationEnableSet, LldpV2PortConfigNotificationEnableTest, LldpV2PortConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpV2PortConfigTableINDEX, 2, 0, 0, "2"},

{{13,LldpV2PortConfigTLVsTxEnable}, GetNextIndexLldpV2PortConfigTable, LldpV2PortConfigTLVsTxEnableGet, LldpV2PortConfigTLVsTxEnableSet, LldpV2PortConfigTLVsTxEnableTest, LldpV2PortConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, LldpV2PortConfigTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2AddressTableIndex}, GetNextIndexLldpV2DestAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpV2DestAddressTableINDEX, 1, 0, 0, NULL},

{{13,LldpV2DestMacAddress}, GetNextIndexLldpV2DestAddressTable, LldpV2DestMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, LldpV2DestAddressTableINDEX, 1, 0, 0, NULL},

{{13,LldpV2ManAddrConfigIfIndex}, GetNextIndexLldpV2ManAddrConfigTxPortsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, LldpV2ManAddrConfigTxPortsTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2ManAddrConfigDestAddressIndex}, GetNextIndexLldpV2ManAddrConfigTxPortsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpV2ManAddrConfigTxPortsTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2ManAddrConfigLocManAddrSubtype}, GetNextIndexLldpV2ManAddrConfigTxPortsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, LldpV2ManAddrConfigTxPortsTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2ManAddrConfigLocManAddr}, GetNextIndexLldpV2ManAddrConfigTxPortsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, LldpV2ManAddrConfigTxPortsTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2ManAddrConfigTxEnable}, GetNextIndexLldpV2ManAddrConfigTxPortsTable, LldpV2ManAddrConfigTxEnableGet, LldpV2ManAddrConfigTxEnableSet, LldpV2ManAddrConfigTxEnableTest, LldpV2ManAddrConfigTxPortsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpV2ManAddrConfigTxPortsTableINDEX, 4, 0, 0, "2"},

{{13,LldpV2ManAddrConfigRowStatus}, GetNextIndexLldpV2ManAddrConfigTxPortsTable, LldpV2ManAddrConfigRowStatusGet, LldpV2ManAddrConfigRowStatusSet, LldpV2ManAddrConfigRowStatusTest, LldpV2ManAddrConfigTxPortsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpV2ManAddrConfigTxPortsTableINDEX, 4, 0, 1, NULL},

{{11,LldpV2StatsRemTablesLastChangeTime}, NULL, LldpV2StatsRemTablesLastChangeTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,LldpV2StatsRemTablesInserts}, NULL, LldpV2StatsRemTablesInsertsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,LldpV2StatsRemTablesDeletes}, NULL, LldpV2StatsRemTablesDeletesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,LldpV2StatsRemTablesDrops}, NULL, LldpV2StatsRemTablesDropsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,LldpV2StatsRemTablesAgeouts}, NULL, LldpV2StatsRemTablesAgeoutsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,LldpV2StatsTxIfIndex}, GetNextIndexLldpV2StatsTxPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, LldpV2StatsTxPortTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2StatsTxDestMACAddress}, GetNextIndexLldpV2StatsTxPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpV2StatsTxPortTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2StatsTxPortFramesTotal}, GetNextIndexLldpV2StatsTxPortTable, LldpV2StatsTxPortFramesTotalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, LldpV2StatsTxPortTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2StatsTxLLDPDULengthErrors}, GetNextIndexLldpV2StatsTxPortTable, LldpV2StatsTxLLDPDULengthErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, LldpV2StatsTxPortTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2StatsRxDestIfIndex}, GetNextIndexLldpV2StatsRxPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, LldpV2StatsRxPortTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2StatsRxDestMACAddress}, GetNextIndexLldpV2StatsRxPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpV2StatsRxPortTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2StatsRxPortFramesDiscardedTotal}, GetNextIndexLldpV2StatsRxPortTable, LldpV2StatsRxPortFramesDiscardedTotalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, LldpV2StatsRxPortTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2StatsRxPortFramesErrors}, GetNextIndexLldpV2StatsRxPortTable, LldpV2StatsRxPortFramesErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, LldpV2StatsRxPortTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2StatsRxPortFramesTotal}, GetNextIndexLldpV2StatsRxPortTable, LldpV2StatsRxPortFramesTotalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, LldpV2StatsRxPortTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2StatsRxPortTLVsDiscardedTotal}, GetNextIndexLldpV2StatsRxPortTable, LldpV2StatsRxPortTLVsDiscardedTotalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, LldpV2StatsRxPortTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2StatsRxPortTLVsUnrecognizedTotal}, GetNextIndexLldpV2StatsRxPortTable, LldpV2StatsRxPortTLVsUnrecognizedTotalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, LldpV2StatsRxPortTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2StatsRxPortAgeoutsTotal}, GetNextIndexLldpV2StatsRxPortTable, LldpV2StatsRxPortAgeoutsTotalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, LldpV2StatsRxPortTableINDEX, 2, 0, 0, NULL},

{{11,LldpV2LocChassisIdSubtype}, NULL, LldpV2LocChassisIdSubtypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,LldpV2LocChassisId}, NULL, LldpV2LocChassisIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,LldpV2LocSysName}, NULL, LldpV2LocSysNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,LldpV2LocSysDesc}, NULL, LldpV2LocSysDescGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,LldpV2LocSysCapSupported}, NULL, LldpV2LocSysCapSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,LldpV2LocSysCapEnabled}, NULL, LldpV2LocSysCapEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,LldpV2LocPortIfIndex}, GetNextIndexLldpV2LocPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, LldpV2LocPortTableINDEX, 1, 0, 0, NULL},

{{13,LldpV2LocPortIdSubtype}, GetNextIndexLldpV2LocPortTable, LldpV2LocPortIdSubtypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2LocPortTableINDEX, 1, 0, 0, NULL},

{{13,LldpV2LocPortId}, GetNextIndexLldpV2LocPortTable, LldpV2LocPortIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2LocPortTableINDEX, 1, 0, 0, NULL},

{{13,LldpV2LocPortDesc}, GetNextIndexLldpV2LocPortTable, LldpV2LocPortDescGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2LocPortTableINDEX, 1, 0, 0, NULL},

{{13,LldpV2LocManAddrSubtype}, GetNextIndexLldpV2LocManAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, LldpV2LocManAddrTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2LocManAddr}, GetNextIndexLldpV2LocManAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, LldpV2LocManAddrTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2LocManAddrLen}, GetNextIndexLldpV2LocManAddrTable, LldpV2LocManAddrLenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpV2LocManAddrTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2LocManAddrIfSubtype}, GetNextIndexLldpV2LocManAddrTable, LldpV2LocManAddrIfSubtypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2LocManAddrTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2LocManAddrIfId}, GetNextIndexLldpV2LocManAddrTable, LldpV2LocManAddrIfIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpV2LocManAddrTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2LocManAddrOID}, GetNextIndexLldpV2LocManAddrTable, LldpV2LocManAddrOIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, LldpV2LocManAddrTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2RemTimeMark}, GetNextIndexLldpV2RemTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpV2RemTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2RemLocalIfIndex}, GetNextIndexLldpV2RemTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, LldpV2RemTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2RemLocalDestMACAddress}, GetNextIndexLldpV2RemTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpV2RemTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2RemIndex}, GetNextIndexLldpV2RemTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpV2RemTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2RemChassisIdSubtype}, GetNextIndexLldpV2RemTable, LldpV2RemChassisIdSubtypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2RemTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2RemChassisId}, GetNextIndexLldpV2RemTable, LldpV2RemChassisIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2RemTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2RemPortIdSubtype}, GetNextIndexLldpV2RemTable, LldpV2RemPortIdSubtypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2RemTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2RemPortId}, GetNextIndexLldpV2RemTable, LldpV2RemPortIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2RemTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2RemPortDesc}, GetNextIndexLldpV2RemTable, LldpV2RemPortDescGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2RemTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2RemSysName}, GetNextIndexLldpV2RemTable, LldpV2RemSysNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2RemTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2RemSysDesc}, GetNextIndexLldpV2RemTable, LldpV2RemSysDescGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2RemTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2RemSysCapSupported}, GetNextIndexLldpV2RemTable, LldpV2RemSysCapSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2RemTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2RemSysCapEnabled}, GetNextIndexLldpV2RemTable, LldpV2RemSysCapEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2RemTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2RemRemoteChanges}, GetNextIndexLldpV2RemTable, LldpV2RemRemoteChangesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2RemTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2RemTooManyNeighbors}, GetNextIndexLldpV2RemTable, LldpV2RemTooManyNeighborsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2RemTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2RemManAddrSubtype}, GetNextIndexLldpV2RemManAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, LldpV2RemManAddrTableINDEX, 6, 0, 0, NULL},

{{13,LldpV2RemManAddr}, GetNextIndexLldpV2RemManAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, LldpV2RemManAddrTableINDEX, 6, 0, 0, NULL},

{{13,LldpV2RemManAddrIfSubtype}, GetNextIndexLldpV2RemManAddrTable, LldpV2RemManAddrIfSubtypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2RemManAddrTableINDEX, 6, 0, 0, NULL},

{{13,LldpV2RemManAddrIfId}, GetNextIndexLldpV2RemManAddrTable, LldpV2RemManAddrIfIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpV2RemManAddrTableINDEX, 6, 0, 0, NULL},

{{13,LldpV2RemManAddrOID}, GetNextIndexLldpV2RemManAddrTable, LldpV2RemManAddrOIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, LldpV2RemManAddrTableINDEX, 6, 0, 0, NULL},

{{13,LldpV2RemUnknownTLVType}, GetNextIndexLldpV2RemUnknownTLVTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpV2RemUnknownTLVTableINDEX, 5, 0, 0, NULL},

{{13,LldpV2RemUnknownTLVInfo}, GetNextIndexLldpV2RemUnknownTLVTable, LldpV2RemUnknownTLVInfoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2RemUnknownTLVTableINDEX, 5, 0, 0, NULL},

{{13,LldpV2RemOrgDefInfoOUI}, GetNextIndexLldpV2RemOrgDefInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, LldpV2RemOrgDefInfoTableINDEX, 7, 0, 0, NULL},

{{13,LldpV2RemOrgDefInfoSubtype}, GetNextIndexLldpV2RemOrgDefInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpV2RemOrgDefInfoTableINDEX, 7, 0, 0, NULL},

{{13,LldpV2RemOrgDefInfoIndex}, GetNextIndexLldpV2RemOrgDefInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpV2RemOrgDefInfoTableINDEX, 7, 0, 0, NULL},

{{13,LldpV2RemOrgDefInfo}, GetNextIndexLldpV2RemOrgDefInfoTable, LldpV2RemOrgDefInfoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2RemOrgDefInfoTableINDEX, 7, 0, 0, NULL},
};
tMibData slldv2Entry = { 79, slldv2MibEntry };

#endif /* _SLLDV2DB_H */

