/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: stdlldwr.h,v 1.5 2008/08/20 15:13:21 iss Exp $
 *
 * Description: Wrapper header file for stdllp mib  
 *********************************************************************/
#ifndef _STDLLDWR_H
#define _STDLLDWR_H

VOID RegisterSTDLLDP(VOID);

VOID UnRegisterSTDLLD(VOID);
INT4 LldpMessageTxIntervalGet(tSnmpIndex *, tRetVal *);
INT4 LldpMessageTxHoldMultiplierGet(tSnmpIndex *, tRetVal *);
INT4 LldpReinitDelayGet(tSnmpIndex *, tRetVal *);
INT4 LldpTxDelayGet(tSnmpIndex *, tRetVal *);
INT4 LldpNotificationIntervalGet(tSnmpIndex *, tRetVal *);
INT4 LldpMessageTxIntervalSet(tSnmpIndex *, tRetVal *);
INT4 LldpMessageTxHoldMultiplierSet(tSnmpIndex *, tRetVal *);
INT4 LldpReinitDelaySet(tSnmpIndex *, tRetVal *);
INT4 LldpTxDelaySet(tSnmpIndex *, tRetVal *);
INT4 LldpNotificationIntervalSet(tSnmpIndex *, tRetVal *);
INT4 LldpMessageTxIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpMessageTxHoldMultiplierTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpReinitDelayTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpTxDelayTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpNotificationIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpMessageTxIntervalDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 LldpMessageTxHoldMultiplierDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 LldpReinitDelayDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 LldpTxDelayDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 LldpNotificationIntervalDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);





INT4 GetNextIndexLldpPortConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpPortConfigAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 LldpPortConfigNotificationEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpPortConfigTLVsTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpPortConfigAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 LldpPortConfigNotificationEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpPortConfigTLVsTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpPortConfigAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpPortConfigNotificationEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpPortConfigTLVsTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpPortConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexLldpConfigManAddrTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpConfigManAddrPortsTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpConfigManAddrPortsTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpConfigManAddrPortsTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpConfigManAddrTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 LldpStatsRemTablesLastChangeTimeGet(tSnmpIndex *, tRetVal *);
INT4 LldpStatsRemTablesInsertsGet(tSnmpIndex *, tRetVal *);
INT4 LldpStatsRemTablesDeletesGet(tSnmpIndex *, tRetVal *);
INT4 LldpStatsRemTablesDropsGet(tSnmpIndex *, tRetVal *);
INT4 LldpStatsRemTablesAgeoutsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpStatsTxPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpStatsTxPortFramesTotalGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpStatsRxPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpStatsRxPortFramesDiscardedTotalGet(tSnmpIndex *, tRetVal *);
INT4 LldpStatsRxPortFramesErrorsGet(tSnmpIndex *, tRetVal *);
INT4 LldpStatsRxPortFramesTotalGet(tSnmpIndex *, tRetVal *);
INT4 LldpStatsRxPortTLVsDiscardedTotalGet(tSnmpIndex *, tRetVal *);
INT4 LldpStatsRxPortTLVsUnrecognizedTotalGet(tSnmpIndex *, tRetVal *);
INT4 LldpStatsRxPortAgeoutsTotalGet(tSnmpIndex *, tRetVal *);
INT4 LldpLocChassisIdSubtypeGet(tSnmpIndex *, tRetVal *);
INT4 LldpLocChassisIdGet(tSnmpIndex *, tRetVal *);
INT4 LldpLocSysNameGet(tSnmpIndex *, tRetVal *);
INT4 LldpLocSysDescGet(tSnmpIndex *, tRetVal *);
INT4 LldpLocSysCapSupportedGet(tSnmpIndex *, tRetVal *);
INT4 LldpLocSysCapEnabledGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpLocPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpLocPortIdSubtypeGet(tSnmpIndex *, tRetVal *);
INT4 LldpLocPortIdGet(tSnmpIndex *, tRetVal *);
INT4 LldpLocPortDescGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpLocManAddrTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpLocManAddrLenGet(tSnmpIndex *, tRetVal *);
INT4 LldpLocManAddrIfSubtypeGet(tSnmpIndex *, tRetVal *);
INT4 LldpLocManAddrIfIdGet(tSnmpIndex *, tRetVal *);
INT4 LldpLocManAddrOIDGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpRemTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpRemChassisIdSubtypeGet(tSnmpIndex *, tRetVal *);
INT4 LldpRemChassisIdGet(tSnmpIndex *, tRetVal *);
INT4 LldpRemPortIdSubtypeGet(tSnmpIndex *, tRetVal *);
INT4 LldpRemPortIdGet(tSnmpIndex *, tRetVal *);
INT4 LldpRemPortDescGet(tSnmpIndex *, tRetVal *);
INT4 LldpRemSysNameGet(tSnmpIndex *, tRetVal *);
INT4 LldpRemSysDescGet(tSnmpIndex *, tRetVal *);
INT4 LldpRemSysCapSupportedGet(tSnmpIndex *, tRetVal *);
INT4 LldpRemSysCapEnabledGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpRemManAddrTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpRemManAddrIfSubtypeGet(tSnmpIndex *, tRetVal *);
INT4 LldpRemManAddrIfIdGet(tSnmpIndex *, tRetVal *);
INT4 LldpRemManAddrOIDGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpRemUnknownTLVTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpRemUnknownTLVInfoGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpRemOrgDefInfoTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpRemOrgDefInfoGet(tSnmpIndex *, tRetVal *);
#endif /* _STDLLDWR_H */
