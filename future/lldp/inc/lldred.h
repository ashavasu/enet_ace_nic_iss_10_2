/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lldred.h,v 1.12 2016/01/28 11:09:06 siva Exp $
 *
 * Description: This file contains the definitions and data structures
 *              and prototypes to support High Availability for LLDP module.   
 *
 ******************************************************************/
#ifndef _LLDRED_H_
#define _LLDRED_H_

/* Data Structures */
typedef struct LldpRedGlobalInfo
{
    /* This pointer is used by STANDBY node
     * to store memory address of remote node
     * whose bulk update is in progress */
    tLldpRemoteNode              *pNextRemoteNode;
    /* Used to hold next MED remote network policy node for 
     * which Bulk update has to be sent. */ 
    tLldpMedRemNwPolicyInfo        *pNextMedRemNwPolicynode; 
    /* Used to hold next MED remote Location node for 
     * which Bulk update has to be sent. */ 
    tLldpMedRemLocationInfo        *pNextMedRemLocationnode;
    tLldpRemManAddressTable      *pNextRemManAddNode; /* Next Man Addr for 
                                                       * which Bulk update has 
                                                       * to be sent.*/ 
    tLldpRemUnknownTLVTable      *pNextRemUnknownTLVNode;
    tLldpRemOrgDefInfoTable      *pNextRemOrgTLVNode;  
    tLldpxdot1RemVlanNameInfo    *pNextRemVlanNameNode; 
    tLldpxdot1RemProtoVlanInfo   *pNextRemProtoVlanNode;
    UINT4                        u4BulkUpdNextPort; /* Next port for which Bulk
                                                     * update has to be
                                                     * generated
                                                     */
    /* Timer sync up information received from Active node */
    UINT4                        u4GlobShutWhileExpTime;
    UINT4                        u4NotifIntTmrExpTime;
    UINT4                        u4GlobShutWhileRcvdTime;
    UINT4                        u4NotifIntTmrRcvdTime;
    UINT1                        u1NumPeersUp; /* Indicates number of standby 
                                                * nodes that are up.
                                                */
    UINT1                        u1LldpNodeStatus; /* Node status(RM_INIT/
                                                    * RM_ACTIVE/ RM_STANDBY).
                                                    */
    UINT1                        u1BulkUpdStatus; /* This will be set to 
                                                   * OSIX_TRUE when bulk update
                                                   * request handling has been
                                                   * completed.
                                                   */
    BOOL1                        bGlobBulkUpdSent; /* This flag is set to
                                                    * OSIX_TRUE after sending
                                                    * the global information 
                                                    * bulk update message.
                                                    */
    BOOL1                        bManAddrBulkUpdStatus; 
                                               /* This flag will be set to TRUE
                                                * after all nodes in the remote
                                                * management address RBTree has
                                                * been synced with the Standby.
                                                */ 
    BOOL1                        bProtoVlanBulkUpdStatus;
    BOOL1                        bVlanNameBulkUpdStatus;
    BOOL1                        bUnkTlvBulkUpdStatus;
    BOOL1                        bOrgDefInfoBulkUpdStatus;
    BOOL1                        bMedNwPolicyBulkUpdStatus;
    BOOL1                        bMedLocationBulkUpdStatus;
    BOOL1                        bBulkUpdReqRcvd; /* This flag will be set to
                                                   * OSIX_TRUE if bulk request
                                                   * is received before getting
                                                   * the RM_STANDBY_UP event.
                                                   */
    BOOL1                        bTailMsgRcvd;
    /* This flag will be set to OSIX_TRUE whenever gloabal shutdown while
     * timer start sync up msg received, and reset to OSIX_FALSE whenever
     * global shutdown while timer expiry sync up msg is received.
     * This flag is set to OSIX_FALSE by default */
    BOOL1                        bGlobShutWhileTmrSyncUpRvcd;
    BOOL1                        bBulkUpdNextPort; /* Next port for which Bulk
                                                     * update has to be
                                                     * generated
                                                     */
    /* This flag bGlobShutWhileRcvdTimeProcessed is to indicate 
       GloblShutWhileRcvd Time Sync-up message is Active to Standby 
       State Scenario.  This is used for skiping the releasing of memory, 
       in higher layer of LLDP L2 Red Message processing. Since the memory 
       is already released in the GlobalShutWhileRcvd Time processing 
      (During shutting down the LLDP module). */
    BOOL1                        bGlobShutWhileRcvdTimeProcessed;
 
}tLldpRedGlobalInfo;

/* This structure contains MSAP RBTree(MSAP remote table) 
 * indices */
typedef struct LldpRedMsapRBIndex
{
    INT4  i4RemChassisIdSubtype; /* Remote chassis id subtype */
    INT4  i4RemPortIdSubtype;    /* Remote port id subtype */
    INT4  i4RemLocalPortNum;     /* Port number on which the remote entry 
                                    is learnt */
    UINT4 u4DestAddrTblIndex;
    UINT1 au1RemChassisId[LLDP_MAX_LEN_CHASSISID]; /* Remote chassis id */
    UINT1 au1RemPortId[LLDP_MAX_LEN_PORTID];       /* Remote port id */ 
    UINT1 au1Pad[2];
}tLldpRedMsapRBIndex;

typedef struct LldpRedTmrSyncUpInfo
{
    tLldpRedMsapRBIndex *pMsapRBIndex;  /* MSAP RBTree indeices */
    UINT4               u4TmrInterval;  /* Time interval */
    UINT1               u1TmrType;      /* Timer type */
    UINT1               au1Pad[3];
}tLldpRedTmrSyncUpInfo;

typedef struct LldpRedRemTabChgInfo
{
    tLldpLocPortInfo *pPortInfo;
    INT4             i4RemIndex;
}tLldpRedRemTabChgInfo;

typedef struct LldpRedGlobBulkUpdInfo
{
    UINT4 u4ActiveUpSysTime;     /* System time at which Active node bootedup 
                                    for the first time */
    UINT4 u4ActivNodeCurSysTime; /* Active node's current system time */
    UINT4 u4NotifTmrStartVal;    /* Time interval with which the notifcation
                                    timer is started in active node */
    UINT4 u4NotifIntRemTime;     /* Notification interval timer's remaining 
                                    time */  
    UINT4 u4RemTabLastChgTime;   /* Remote table last change time */
    UINT4 u4RemTabInserts;       /* Remote table insert count */
    UINT4 u4RemTabUpdates;       /* Remote table update count */
    UINT4 u4RemTabDeletes;       /* Remote table delete count */
    UINT4 u4RemTabAgeOuts;       /* Remote table age out count */
}tLldpRedGlobBulkUpdInfo;

/* Message types */
enum {
    
    LLDP_RED_BULK_UPDT_REQ_MSG   = 1,
    LLDP_RED_BULK_UPDATE_MSG,
    LLDP_RED_BULK_UPDT_TAIL_MSG,
    LLDP_RED_OPER_STATUS_CHG_MSG,
    LLDP_RED_TMR_START_MSG,
    LLDP_RED_TMR_EXP_MSG,
    LLDP_RED_REM_TAB_INSERT_MSG,
    LLDP_RED_REM_TAB_UPDATE_MSG,
    LLDP_RED_REM_TAB_DELETE_MSG,
    LLDP_RED_REM_TAB_AGEOUT_MSG,
    LLDP_MAX_RED_MSG_TYPES
};

/* bulk update sub types */
enum {
    LLDP_RED_GLOBINFO_BULK_UPD_MSG     = 1,
    LLDP_RED_PORTINFO_BULK_UPD_MSG     = 2,
    LLDP_RED_REM_NODE_BULK_UPD_MSG     = 3,
    LLDP_RED_MANADDR_BULK_UPD_MSG      = 4,
    LLDP_RED_PROTOVLAN_BULK_UPD_MSG    = 5,
    LLDP_RED_VLANNAME_BULK_UPD_MSG     = 6,
    LLDP_RED_UNKNOWN_TLV_BULK_UPD_MSG  = 7,
    LLDP_RED_ORGDEF_INFO_BULK_UPD_MSG  = 8,
    LLDP_RED_MED_NWPOL_BULK_UPD_MSG    = 9,
    LLDP_RED_MED_LOCATION_BULK_UPD_MSG = 10,
    LLDP_MAX_RED_UPD_SUB_TYPES         = 11
};

#define LLDP_RED_SYSTIME_ALLOWED_VARIANCE 1


/* bulk update message lengths */
#define LLDP_RED_MAX_BULK_UPD_SIZE 1500

#define LLDP_RED_MAN_ADDR_CNT_PER_BULK_UPD           50
#define LLDP_RED_PROTO_VLAN_CNT_PER_BULK_UPD         50
#define LLDP_RED_VLAN_NAME_CNT_PER_BULK_UPD          50
#define LLDP_RED_UNK_TLV_CNT_PER_BULK_UPD            25
#define LLDP_RED_UNREC_ORG_TLV_CNT_PER_BULK_UPD      25
#define LLDP_RED_REMOTE_NODE_CNT_PER_BULK_UPD        10
#define LLDP_RED_MED_NWPOL_REM_NODE_CNT_PER_BULK_UPD 10
#define LLDP_RED_MED_LOC_REM_NODE_CNT_PER_BULK_UPD   10

/* Bulk update Type Message Length */
#define LLDP_RED_GLOBINFO_BULK_UPD_MSG_LEN (LLDP_RED_BULK_UPD_MSG_HDR_SIZE + \
                                            sizeof (tLldpRedGlobBulkUpdInfo))

#define LLDP_RED_PORT_INFO_MSG_LEN (LLDP_RED_PORT_NUM_SIZE + \
                                   LLDP_RED_MAC_ADDR_SIZE +\
                                   LLDP_RED_PORT_OPER_STATUS_SIZE + \
                                   LLDP_RED_PORT_AGEOUT_TOTAL_SIZE)

#define LLDP_RED_GET_MAN_ADDR_MSG_LEN(u2Len, u4ManAddrOIDLen)\
                                   (LLDP_RED_REM_INDEX_SIZE + \
                                   LLDP_RED_MAC_ADDR_SIZE +\
                                   LLDP_RED_MAN_ADDR_SUBTYPE_SIZE +\
                                   LLDP_RED_MAN_ADDR_LEN_FIELD_SIZE + \
                                   u2Len + \
                                   LLDP_RED_MAN_ADDR_INTF_SUBTYPE_SIZE + \
                                   LLDP_RED_INTF_ID_SIZE + \
                                   LLDP_RED_OID_LEN_FIELD_SIZE + \
                                   u4ManAddrOIDLen)

#define LLDP_RED_PROTO_VLAN_MSG_LEN  (LLDP_RED_REM_INDEX_SIZE + \
                                     LLDP_RED_MAC_ADDR_SIZE +\
                                     LLDP_RED_PROTO_VLAN_STAT_SIZE + \
                                     LLDP_RED_PROTO_VLAN_ID_SIZE)

#define LLDP_RED_GET_VLAN_NAME_MSG_LEN(u1VlanNameLen)\
                                     (LLDP_RED_REM_INDEX_SIZE + \
                                     LLDP_RED_VLAN_ID_SIZE + \
                                     LLDP_RED_VLAN_NAME_LEN_SIZE + \
                                     u1VlanNameLen)

#define LLDP_RED_GET_UNK_TLV_MSG_LEN(u2Len)\
                                  (LLDP_RED_REM_INDEX_SIZE + \
                                  LLDP_RED_MAC_ADDR_SIZE +\
                                  LLDP_RED_UNK_TLV_TYPE_SIZE + \
                                  LLDP_RED_UNK_TLV_INFO_LEN_FIELD_SIZE + \
                                  u2Len) 
                                  
#define LLDP_RED_GET_UNREC_ORG_TLV_MSG_LEN(u2Len)\
                                        (LLDP_RED_REM_INDEX_SIZE + \
                                        LLDP_MAX_LEN_OUI + \
                                        LLDP_RED_MAC_ADDR_SIZE +\
                                        LLDP_RED_ORG_SUBTYPE_SIZE + \
                                        LLDP_RED_ORG_DEF_INFO_INDEX_SIZE + \
                                        LLDP_RED_ORG_DEF_INFO_LEN_SIZE +\
                                        u2Len)

/* Size of each field in the Message */
#define LLDP_RED_PORT_COUNT_SIZE 2
#define LLDP_RED_PORT_NUM_SIZE 4
#define LLDP_RED_PORT_OPER_STATUS_SIZE 1
#define LLDP_RED_PORT_AGEOUT_TOTAL_SIZE 4                                  
#define LLDP_RED_MAN_ADDR_SUBTYPE_SIZE 1
#define LLDP_RED_REM_INDEX_SIZE  12 /* Timemark + Local Port Number + Remote Index*/
#define LLDP_RED_MAN_ADDR_LEN_FIELD_SIZE  1
#define LLDP_RED_MAN_ADDR_FIELD_SIZE  LLDP_MAX_LEN_MAN_ADDR
#define LLDP_RED_MAN_ADDR_INTF_SUBTYPE_SIZE 1
#define LLDP_RED_INTF_ID_SIZE 4
#define LLDP_RED_OID_LEN_FIELD_SIZE 1
#define LLDP_RED_PROTO_VLAN_STAT_SIZE 1
#define LLDP_RED_PROTO_VLAN_ID_SIZE  2
#define LLDP_RED_VLAN_ID_SIZE 2
#define LLDP_RED_VLAN_NAME_LEN_SIZE 1                                            
#define LLDP_RED_VLAN_NAME_SIZE  VLAN_STATIC_MAX_NAME_LEN
#define LLDP_RED_UNK_TLV_TYPE_SIZE 1
#define LLDP_RED_UNK_TLV_INFO_LEN_FIELD_SIZE 2
#define LLDP_RED_ORG_SUBTYPE_SIZE 1
#define LLDP_RED_ORG_DEF_INFO_INDEX_SIZE 4
#define LLDP_RED_ORG_DEF_INFO_LEN_SIZE 2                                  
#define LLDP_RED_MAC_ADDR_SIZE 6
/* Macros used to indicate whether the following TLVs are present in the
 * RM message */                                       
#define LLDP_RED_MAC_PHY_TLV_BMAP        0x01
#define LLDP_RED_POW_VIA_MDI_TLV_BMAP    0x02
#define LLDP_RED_LINK_AGG_TLV_BMAP       0x04
#define LLDP_RED_MAX_FRAME_SIZE_TLV_BMAP 0x08

/* HITLESS RESTART */
#define LLDP_HR_STDY_ST_PKT_REQ        RM_HR_STDY_ST_PKT_REQ
#define LLDP_HR_STDY_ST_PKT_MSG        RM_HR_STDY_ST_PKT_MSG
#define LLDP_HR_STDY_ST_PKT_TAIL       RM_HR_STDY_ST_PKT_TAIL

#define LLDP_RM_OFFSET                 0  /* Offset in the RM buffer from where
                                         * protocols can start writing. */
#define LLDP_ENET_MIN_PDU_SIZE         60 /* Minimum Packet Size for ethernet 
                                           * packets */

/* change the max msg size while changing 
 * RemoteTableInsert/RemoteTableUpdate sync up message */
/* LLDP_RED_MSG_HDR_SIZE + sizeof(i4RemIndex) + sizeof(u4PortNum) + RM_HDR_LENGTH
 * LLDP_MAX_PDU_SIZE
 * 3 + 4 + 4 + 24 + 1500 +  */
#define LLDP_RED_MAX_MSG_SIZE 1535
    
#define LLDP_RED_MSG_TYPE_SIZE 1
#define LLDP_RED_MSG_LEN_SIZE  2
#define LLDP_RED_BULK_UPDT_MSG_TYPE_SIZE 1


#define LLDP_RED_MSG_HDR_SIZE       (LLDP_RED_MSG_TYPE_SIZE +\
                                    LLDP_RED_MSG_LEN_SIZE)
#define LLDP_RED_BULK_UPD_MSG_HDR_SIZE     (LLDP_RED_MSG_HDR_SIZE + \
                                            LLDP_RED_BULK_UPDT_MSG_TYPE_SIZE)

/* Message Length */

/* LLDP_RED_MSG_HDR_SIZE + sizeof (u1OperStatus) + sizeof(u4PortNum) */
#define LLDP_RED_OPER_STATUS_CHG_MSG_LEN       14  
/* LLDP_RED_MSG_HDR_SIZE + sizeof (u1TmrType) */
#define LLDP_RED_TMR_EXP_MSG_LEN        4

#define LLDP_RED_REM_TAB_DELETE_MSG_LEN (LLDP_RED_MSG_HDR_SIZE + \
                                         sizeof (tLldpRedMsapRBIndex))

#define LLDP_RED_BULK_UPD_MSG_LEN       LLDP_RED_MSG_HDR_SIZE
#define LLDP_RED_BULK_UPD_TAIL_MSG_LEN  LLDP_RED_MSG_HDR_SIZE
#define LLDP_RED_BULK_UPD_REQ_MSG_LEN       LLDP_RED_MSG_HDR_SIZE

    
#define LLDP_RED_PUT_1_BYTE(pMsg, u4Offset, u1MesgType) \
{\
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset, u1MesgType); \
    u4Offset += 1; \
}

#define LLDP_RED_PUT_2_BYTE(pMsg, u4Offset, u2MesgType) \
{\
    RM_DATA_ASSIGN_2_BYTE (pMsg, u4Offset, u2MesgType); \
    u4Offset += 2; \
}

#define LLDP_RED_PUT_4_BYTE(pMsg, u4Offset, u4MesgType) \
{\
    RM_DATA_ASSIGN_4_BYTE (pMsg, u4Offset, u4MesgType); \
    u4Offset += 4; \
}

#define LLDP_RED_PUT_6_BYTE(pdest, u4Offset, psrc) \
{\
    RM_COPY_TO_OFFSET(pdest, psrc, u4Offset, 6); \
    u4Offset +=6; \
}

#define LLDP_RED_PUT_N_BYTE(pdest, psrc, u4Offset, u4Size) \
{\
    RM_COPY_TO_OFFSET(pdest, psrc, u4Offset, u4Size); \
    u4Offset +=u4Size; \
}

#define LLDP_RED_GET_1_BYTE(pMsg, u4Offset, u1MesgType) \
{\
    RM_GET_DATA_1_BYTE (pMsg, u4Offset, u1MesgType); \
    u4Offset += 1; \
}

#define LLDP_RED_GET_2_BYTE(pMsg, u4Offset, u2MesgType) \
{\
    RM_GET_DATA_2_BYTE (pMsg, u4Offset, u2MesgType); \
    u4Offset += 2; \
}

#define LLDP_RED_GET_4_BYTE(pMsg, u4Offset, u4MesgType) \
{\
    RM_GET_DATA_4_BYTE (pMsg, u4Offset, u4MesgType); \
    u4Offset += 4; \
}

#define LLDP_RED_GET_6_BYTE(psrc, u4Offset, pdest) \
{\
    RM_GET_DATA_N_BYTE (psrc, pdest, u4Offset, 6); \
    u4Offset += 6; \
}

#define LLDP_RED_GET_N_BYTE(psrc, pdest, u4Offset, u4Size) \
{\
    RM_GET_DATA_N_BYTE (psrc, pdest, u4Offset, u4Size); \
    u4Offset +=u4Size; \
}

#define LLDP_CRU_BUF_MOVE_VALID_OFFSET(pBuf, Len) \
{ \
    CRU_BUF_Move_ValidOffset (pBuf, Len);\
}


#define LLDP_RED_NODE_STATUS() \
    gLldpRedGlobalInfo.u1LldpNodeStatus
    
#define LLDP_RED_BULK_UPD_STATUS() \
    gLldpRedGlobalInfo.u1BulkUpdStatus
    
#define LLDP_RED_NEXT_MAN_ADDR_NODE() \
    gLldpRedGlobalInfo.pNextRemManAddNode
    
#define LLDP_RED_NEXT_PROTO_VLAN_NODE() \
    gLldpRedGlobalInfo.pNextRemProtoVlanNode

#define LLDP_RED_NEXT_VLAN_NAME_NODE() \
    gLldpRedGlobalInfo.pNextRemVlanNameNode

#define LLDP_RED_NEXT_UNK_TLV_NODE() \
     gLldpRedGlobalInfo.pNextRemUnknownTLVNode

#define LLDP_RED_NEXT_UNREC_ORG_NODE() \
     gLldpRedGlobalInfo.pNextRemOrgTLVNode
    
#define LLDP_RED_NEXT_REMOTE_NODE() \
     gLldpRedGlobalInfo.pNextRemoteNode

#define LLDP_RED_NEXT_MED_REM_NW_POL_NODE() \
     gLldpRedGlobalInfo.pNextMedRemNwPolicynode
     
#define LLDP_RED_NEXT_MED_REM_LOC_NODE() \
     gLldpRedGlobalInfo.pNextMedRemLocationnode

#define LLDP_RED_MAN_ADR_BULK_UPD_STATUS() \
    gLldpRedGlobalInfo.bManAddrBulkUpdStatus

#define LLDP_RED_PROTO_VLAN_BULK_UPD_STATUS() \
    gLldpRedGlobalInfo.bProtoVlanBulkUpdStatus

#define LLDP_RED_VLAN_NAME_BULK_UPD_STATUS() \
    gLldpRedGlobalInfo.bVlanNameBulkUpdStatus

#define  LLDP_RED_UNK_TLV_BULK_UPD_STATUS() \
    gLldpRedGlobalInfo.bUnkTlvBulkUpdStatus
 
#define LLDP_RED_UNREC_ORG_BULK_UPD_STATUS() \
    gLldpRedGlobalInfo.bOrgDefInfoBulkUpdStatus

#define LLDP_RED_MED_NW_POL_BULK_UPD_STATUS() \
    gLldpRedGlobalInfo.bMedNwPolicyBulkUpdStatus

#define LLDP_RED_MED_LOCATION_BULK_UPD_STATUS() \
    gLldpRedGlobalInfo.bMedLocationBulkUpdStatus

#define LLDP_RED_TAIL_MSG_RCVD() \
    gLldpRedGlobalInfo.bTailMsgRcvd

#define LLDP_RED_IS_STANDBY_UP() \
    ((gLldpRedGlobalInfo.u1NumPeersUp > 0) ? OSIX_TRUE : OSIX_FALSE)
    
#define LLDP_BULK_UPD_REQ_RECD()  gLldpRedGlobalInfo.bBulkUpdReqRcvd

#define LLDP_RED_BULK_SPLIT_MSG_SIZE  1500 

#define LLDP_RED_ACTIVE_NODE_UP_SYSTIME() \
    gLldpGlobalInfo.u4FirstActiveUpTime

/* Function Prototypes */
PUBLIC VOID LldpRedProcessRmMsg       PROTO ((tLldpRmCtrlMsg * pMsg));

PUBLIC VOID LldpRedSendSyncUpMsg      PROTO ((UINT1 u1MsgType, 
                                              VOID *pSyncUpMsg));


PUBLIC VOID LldpRedInitRedGlobalInfo  PROTO ((VOID));


PUBLIC VOID LldpRedFillMsapRBIndex    PROTO ((tLldpRedMsapRBIndex *pMsapRBIndex,
                                              tLldpRemoteNode *pRemoteNode));

/* Private functions */
 VOID
LldpRedMakeNodeStandbyFromActive PROTO ((VOID));
    
 VOID
LldpRedApplyNotifIntTmrSyncUp PROTO ((VOID));

 VOID
LldpRedApplyGlobShutWhileTmrSyncUp PROTO ((VOID));
    
 VOID
LldpRedApplyRxInfoAgeTmrSyncUp PROTO ((VOID));
    
 VOID
LldpRedApplyTmrSyncUpOnRemNode PROTO ((tLldpRemoteNode *pRemoteNode));
    
 VOID
LldpRedMakeNodeActiveFromStandby PROTO ((VOID));
    
 VOID
LldpRedTransmitLldpdu PROTO ((VOID));
    
 VOID
LldpRedMakeNodeActiveFromIdle PROTO ((VOID));
    
 VOID
LldpRedFormPortInfoBulkUpd PROTO ((tRmMsg *pRmMsg, UINT4 *pu4Offset, 
                                   UINT1 *pu1CountFlag,
                                   tLldpLocPortInfo *pPortInfo));

 VOID
LldpRedFormRemNodeBulkUpdMsg PROTO ((tRmMsg *pRmMsg, UINT4 *pu4Offset, 
                                     tLldpRemoteNode *pRemoteNode));
    
 INT4
LldpRedProcPortInfoBulkUpd PROTO ((tRmMsg *pRmMsg));
    
 INT4
LldpRedGetDot3TlvInfo PROTO ((tLldpRemoteNode *pRemoteNode, tRmMsg *pRmMsg,
                              UINT4 *pu4Offset));
    
 INT4
LldpRedProcOrgDefInfoBulkUpd PROTO ((tRmMsg *pRmMsg));

 VOID
LldpRedGetOrgDefInfo PROTO ((tLldpRemOrgDefInfoTable *pOrgDefInfoNode,
                             tRmMsg *pRmMsgBuf, UINT4 *pu4Offset));

 INT4
LldpRedProcUnknownTlvBulkUpd PROTO ((tRmMsg *pRmMsg));

 VOID
LldpRedGetUnknownTlvInfo PROTO ((tLldpRemUnknownTLVTable *pUnknownTlvNode,
                                 tRmMsg *pRmMsgBuf, UINT4 *pu4Offset));

 INT4
LldpRedProcVlanNameBulkUpd PROTO ((tRmMsg *pRmMsg));

 VOID
LldpRedGetVlanNameInfo PROTO ((tLldpxdot1RemVlanNameInfo *pVlanNameNode,
                               tRmMsg *pRmMsgBuf, UINT4 *pu4Offset));

 INT4
LldpRedProcProtoVlanBulkUpd PROTO ((tRmMsg *pRmMsg, UINT2 u2MsgLen));

 VOID
LldpRedGetProtoVlanInfo PROTO ((tLldpxdot1RemProtoVlanInfo *pProtoVlanNode,
                                tRmMsg *pRmMsg, UINT4 *pu4Offset));

 INT4
LldpRedProcManAddrBulkUpd PROTO ((tRmMsg *pRmMsg));


 VOID
LldpRedGetManAddrInfo PROTO ((tLldpRemManAddressTable *pManAddrNode,
                              tRmMsg *pRmMsgBuf, UINT4 *pu4Offset));
 VOID
LldpRedGetBasicOptTlvInfo PROTO ((tLldpRemoteNode *pRemoteNode,
                                  tRmMsg *pRmMsg, UINT4 *pu4Offset));
   
 INT4
LldpRedProcRemNodeBulkUpd PROTO ((tRmMsg *pRmMsg));
    
 INT4
LldpRedGetRemNodeInfo PROTO ((tLldpRemoteNode *pRemoteNode, tRmMsg *pRmMsg));
    

 VOID 
LldpRedProcBulkUpdMsg PROTO ((tRmMsg *pRmMsg, UINT2 u2MsgLen));

 VOID
LldpRedSendBulkUpdTailMsg PROTO ((VOID));
    
VOID
LldpRedFormOrgDefInfoBulkUpdMsg PROTO ((tRmMsg *pRmMsg, UINT4 *pu4Offset, 
                                        tLldpRemOrgDefInfoTable 
                                        *pOrgDefInfoNode));

 VOID
LldpRedFormUnknownTlvBulkUpdMsg PROTO ((tRmMsg *pRmMsg, UINT4 *pu4Offset,
                                        tLldpRemUnknownTLVTable 
                                        *pUnknownTlvNode));

 VOID
LldpRedFormVlanNameBulkUpdMsg PROTO ((tRmMsg *pRmMsg, UINT4 *pu4Offset,
                                      tLldpxdot1RemVlanNameInfo
                                      *pVlanNameNode));
    
 VOID
LldpRedFormProtoVlanBulkUpdMsg PROTO ((tRmMsg *pRmMsg, UINT4 *pu4Offset,
                                       tLldpxdot1RemProtoVlanInfo
                                       *pProtoVlanNode));
VOID
LldpRedInitBulkUpdateFlags PROTO ((VOID));
    
 VOID
LldpRedSendOrgDefTabInfo PROTO ((BOOL1 *pbBulkUpdEvtSent));
    
 VOID
LldpRedSendUnknownTlvInfo PROTO ((BOOL1 *pbBulkUpdEvtSent));
    
 VOID
LldpRedAddDot3TlvInfo  PROTO ((tRmMsg *pRmMsg, UINT4 *pu4Offset, 
                               tLldpRemoteNode *pRemoteNode));
    
 VOID
LldpRedSendVlanNameTabInfo PROTO ((BOOL1 *pbBulkUpdEvtSent));
    
 VOID
LldpRedSendProtoVlanTabInfo PROTO ((BOOL1 *pbBulkUpdEvtSent));

VOID
LldpRedFormManAddrBulkUpdMsg PROTO ((tRmMsg *pRmMsg, UINT4 *pu4Offset,
                              tLldpRemManAddressTable *pRemManAddrNode));
    
 VOID
 LldpRedSendManAddrTabInfo PROTO ((BOOL1 *pbBulkUpdEvtSent));
    
    
 VOID
LldpRedSendRemNodeInfo PROTO ((VOID));

  VOID
LldpRedSendRemInfoBulkUpd PROTO ((VOID));
    
 INT4
LldpRedAllocMemForRmMsg PROTO ((UINT1 u1MsgType, UINT2 u2MsgLen, 
                                tRmMsg **ppRmMsg));

 VOID
LldpRedTrigNextBulkUpd PROTO ((VOID));

 VOID 
LldpRedSendMsgToRm PROTO ((UINT1 u1MsgType, UINT2 u2MsgLen, tRmMsg *pBasePtr));

 VOID
LldpRedSendPortInfoBulkUpd    PROTO ((VOID));

 VOID
LldpRedSendGlobBulkUpd        PROTO ((VOID));

 VOID
LldpRedMakeNodeStandbyFromIdle PROTO ((VOID));
    
VOID 
LldpRedFormGlobBulkUpdInfo PROTO ((tLldpRedGlobBulkUpdInfo *pGlobBulkUpdInfo));
    
 VOID
LldpRedUpdateRemTab           PROTO ((tLldpLocPortInfo *pPortInfo));

 VOID
LldpRedAddRemNode             PROTO ((tLldpLocPortInfo *pPortInfo, 
                                      INT4 i4RemIndex));
    


 INT4
LldpRedPutLldpduInRmMsg       PROTO ((tRmMsg * pRmMsgBuf, 
                                      tLldpRedRemTabChgInfo *pRemTabChgInfo,
                                      UINT4 *pu4Offset));

 VOID
LldpRedTrigHigherLayer        PROTO ((UINT1 u1TrigType));
    
 VOID
LldpRedSendBulkUpdReq            PROTO ((VOID));

 VOID 
LldpRedGetMsgLen              PROTO ((UINT1 u1MsgType, UINT2 *pu2MsgLen, 
                                      VOID *pSyncUpInfo));

 VOID 
LldpRedProcSyncUpMsg          PROTO ((tLldpRmCtrlMsg * pRmCtrlMsg)); 

 VOID 
LldpRedFormOperChgSyncUpMsg   PROTO ((tRmMsg * pRmMsgBuf,
                                      tLldpLocPortInfo * pPortInfo,
                                      UINT4 *pu4Offset));

 VOID 
LldpRedProcOperChgSyncUp      PROTO ((tRmMsg * pRmMsg, UINT2 u2MsgLen));

 VOID 
LldpRedProcTmrStartSyncUp     PROTO ((tRmMsg * pRmMsg, UINT2 u2MsgLen));

 VOID 
LldpRedProcTmrExpSyncUp       PROTO ((tRmMsg * pRmMsg, UINT2 u2MsgLen));

 VOID 
LldpRedProcRemTabInsertSyncUp PROTO ((tRmMsg * pRmMsg, UINT2 u2MsgLen));

 VOID 
LldpRedProcRemTabUpdateSyncUp PROTO ((tRmMsg * pRmMsg, UINT2 u2MsgLen));

 VOID 
LldpRedProcRemTabDeleteSyncUp PROTO ((tRmMsg * pRmMsg, 
                                      UINT1 u1MsgType));

 VOID
LldpRedProcGlobBulkUpd     PROTO ((tRmMsg * pRmMsg));
    
 INT4 
LldpRedUpdRxInfoAgeExpTime    PROTO ((tRmMsg * pRmMsg, UINT4 *pu4Offset));

 INT4 
LldpRedDelRemNode             PROTO ((tLldpRedMsapRBIndex * pMsapRBIndex));

 VOID 
LldpRedHandleGoActive         PROTO ((VOID));

 VOID 
LldpRedHandleGoStandby        PROTO ((VOID));

 INT4
LldpRedFormRemTabChgSyncUpMsg PROTO ((tRmMsg * pRmMsgBuf, VOID * pSyncUpInfo,
                                      UINT1 u1MsgType, UINT4 *pu4Offset));

VOID
LldpRedFormTmrExpSyncUpMsg   PROTO ((tRmMsg * pRmMsgBuf,
                                     tLldpRedTmrSyncUpInfo * pTmrSyncUpInfo,
                                     UINT4 *pu4Offset));

 INT4
LldpRedFormTmrStartSyncUpMsg PROTO ((tRmMsg * pRmMsgBuf, 
                                     tLldpRedTmrSyncUpInfo * pTmrSyncUpInfo,
                                     UINT4 *pu4Offset));

 VOID
LldpRedPutMsgHdrInRmMsg    PROTO ((tRmMsg * pRmMsgBuf, UINT1 u1MsgType, 
                                   UINT2 u2MsgLen));


 VOID 
LldpRedAddBasicTlvInfo PROTO ((tRmMsg *pRmMsg, UINT4 *pu4Offset, 
                                       tLldpRemoteNode * pRemoteNode));


 VOID 
LldpRedPutMsapRBIndexInRmMsg      PROTO ((tRmMsg * pRmMsgBuf, 
                                          tLldpRedMsapRBIndex * pMsapRBIndex, 
                                          UINT4 *pu4Offset));

 VOID 
LldpRedGetMsapRBIndexFromRmMsg   PROTO ((tLldpRedMsapRBIndex * pMsapRBIndex,
                                         tRmMsg * pRmMsg, UINT4 *pu4Offset));

VOID
LldpRedHandleStandByUpEvent PROTO ((VOID));


VOID
LldpRedUpdNxtRemNodeBlkUpdPtr PROTO ((tLldpRemoteNode * pRemoteNode));

VOID
LldpRedUpdNxtManAddrBlkUpdPtr PROTO ((tLldpRemoteNode * pRemoteNode));

VOID
LldpRedUpdNxtProtoVlanBlkUpdPtr PROTO ((tLldpRemoteNode * pRemoteNode));

VOID
LldpRedUpdNxtVlanNameBlkUpdPtr PROTO ((tLldpRemoteNode * pRemoteNode));

VOID
LldpRedUpdNxtUnknownTLVBlkUpdPtr PROTO ((tLldpRemoteNode * pRemoteNode));

VOID
LldpRedUpdNxtOrgDefInfoBlkUpdPtr PROTO ((tLldpRemoteNode * pRemoteNode));

VOID
LldpRedUpdNxtMedNwPolBlkUpdPtr PROTO ((tLldpRemoteNode * pRemoteNode));

VOID LldpRedFormMedNwPolBulkUpdMsg PROTO ((tRmMsg * pRmMsg, UINT4 *pu4Offset,
            tLldpMedRemNwPolicyInfo *pMedRemNode));

VOID
LldpRedGetMedNwPolRemNodeInfo PROTO ((tLldpMedRemNwPolicyInfo* pMedRemoteNode,
            UINT4 *pu4Offset, tRmMsg * pRmMsg));

INT4
LldpRedProcMedNwPolRemNodeBulkUpd PROTO ((tRmMsg * pRmMsg));

VOID LldpRedSendMedNwPolicyInfo PROTO ((VOID));

VOID 
LldpRedAddMedTlvInfoBulkUpdMsg PROTO ((tRmMsg *pRmMsg, UINT4 *u4Offset, tLldpRemoteNode *pRemoteNode));
VOID 
LldpRedGetMedTlvInfoBulkUpdMsg PROTO ((tRmMsg *pRmMsg, UINT4 *pu4Offset, tLldpRemoteNode *pRemoteNode));

VOID     
LldpRedAddInvTlvInfoBulkUpdMsg PROTO ((tRmMsg * pRmMsg, UINT4 *pu4Offset,
        tLldpRemoteNode *pRemoteNode));

VOID     
LldpRedGetInvTlvInfoBulkUpMsg PROTO ((tRmMsg *pRmMsg, UINT4 *pu4Offset, tLldpRemoteNode
        *pRemoteNode));

VOID LldpRedFormMedLocBulkUpdMsg PROTO ((tRmMsg * pRmMsg, UINT4 *pu4Offset,
            tLldpMedRemLocationInfo *pMedLocRemNode));

VOID LldpRedSendMedLocationInfo PROTO ((VOID));

INT4
LldpRedProcMedLocationRemNodeBulkUpd PROTO ((tRmMsg * pRmMsg));

VOID
LldpRedGetMedLocationRemNodeInfo PROTO ((tLldpMedRemLocationInfo* pMedLocRemNode, UINT4
        *pu4Offset, tRmMsg * pRmMsg));
VOID
LldpRedUpdNxtMedLocationBlkUpdPtr PROTO ((tLldpRemoteNode * pRemoteNode));

VOID
LldpRedAddPowerTlvInfoBulkUpdMsg PROTO ((tRmMsg *pRmMsg, UINT4 *pu4Offset,
        tLldpRemoteNode *pRemoteNode));

VOID
LldpRedGetPowerTlvInfoBulkUpdMsg PROTO ((tRmMsg * pRmMsg, UINT4 *pu4Offset,
        tLldpRemoteNode * pRemoteNode));


#endif /* _LLDRED_H_ */
/*---------------------------------------------------------------------------*/
/*                        End of the file  <lldred.h>                        */
/*---------------------------------------------------------------------------*/
