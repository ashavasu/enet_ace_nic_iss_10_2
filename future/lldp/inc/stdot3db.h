/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdot3db.h,v 1.4 2008/08/20 15:13:21 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDOT3DB_H
#define _STDOT3DB_H

UINT1 LldpXdot3PortConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXdot3LocPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXdot3LocPowerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXdot3LocLinkAggTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXdot3LocMaxFrameSizeTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXdot3RemPortTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXdot3RemPowerTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXdot3RemLinkAggTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXdot3RemMaxFrameSizeTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 stdot3 [] ={1,0,8802,1,1,2,1,5,4623};
tSNMP_OID_TYPE stdot3OID = {9, stdot3};


UINT4 LldpXdot3PortConfigTLVsTxEnable [ ] ={1,0,8802,1,1,2,1,5,4623,1,1,1,1,1};
UINT4 LldpXdot3LocPortAutoNegSupported [ ] ={1,0,8802,1,1,2,1,5,4623,1,2,1,1,1};
UINT4 LldpXdot3LocPortAutoNegEnabled [ ] ={1,0,8802,1,1,2,1,5,4623,1,2,1,1,2};
UINT4 LldpXdot3LocPortAutoNegAdvertisedCap [ ] ={1,0,8802,1,1,2,1,5,4623,1,2,1,1,3};
UINT4 LldpXdot3LocPortOperMauType [ ] ={1,0,8802,1,1,2,1,5,4623,1,2,1,1,4};
UINT4 LldpXdot3LocPowerPortClass [ ] ={1,0,8802,1,1,2,1,5,4623,1,2,2,1,1};
UINT4 LldpXdot3LocPowerMDISupported [ ] ={1,0,8802,1,1,2,1,5,4623,1,2,2,1,2};
UINT4 LldpXdot3LocPowerMDIEnabled [ ] ={1,0,8802,1,1,2,1,5,4623,1,2,2,1,3};
UINT4 LldpXdot3LocPowerPairControlable [ ] ={1,0,8802,1,1,2,1,5,4623,1,2,2,1,4};
UINT4 LldpXdot3LocPowerPairs [ ] ={1,0,8802,1,1,2,1,5,4623,1,2,2,1,5};
UINT4 LldpXdot3LocPowerClass [ ] ={1,0,8802,1,1,2,1,5,4623,1,2,2,1,6};
UINT4 LldpXdot3LocLinkAggStatus [ ] ={1,0,8802,1,1,2,1,5,4623,1,2,3,1,1};
UINT4 LldpXdot3LocLinkAggPortId [ ] ={1,0,8802,1,1,2,1,5,4623,1,2,3,1,2};
UINT4 LldpXdot3LocMaxFrameSize [ ] ={1,0,8802,1,1,2,1,5,4623,1,2,4,1,1};
UINT4 LldpXdot3RemPortAutoNegSupported [ ] ={1,0,8802,1,1,2,1,5,4623,1,3,1,1,1};
UINT4 LldpXdot3RemPortAutoNegEnabled [ ] ={1,0,8802,1,1,2,1,5,4623,1,3,1,1,2};
UINT4 LldpXdot3RemPortAutoNegAdvertisedCap [ ] ={1,0,8802,1,1,2,1,5,4623,1,3,1,1,3};
UINT4 LldpXdot3RemPortOperMauType [ ] ={1,0,8802,1,1,2,1,5,4623,1,3,1,1,4};
UINT4 LldpXdot3RemPowerPortClass [ ] ={1,0,8802,1,1,2,1,5,4623,1,3,2,1,1};
UINT4 LldpXdot3RemPowerMDISupported [ ] ={1,0,8802,1,1,2,1,5,4623,1,3,2,1,2};
UINT4 LldpXdot3RemPowerMDIEnabled [ ] ={1,0,8802,1,1,2,1,5,4623,1,3,2,1,3};
UINT4 LldpXdot3RemPowerPairControlable [ ] ={1,0,8802,1,1,2,1,5,4623,1,3,2,1,4};
UINT4 LldpXdot3RemPowerPairs [ ] ={1,0,8802,1,1,2,1,5,4623,1,3,2,1,5};
UINT4 LldpXdot3RemPowerClass [ ] ={1,0,8802,1,1,2,1,5,4623,1,3,2,1,6};
UINT4 LldpXdot3RemLinkAggStatus [ ] ={1,0,8802,1,1,2,1,5,4623,1,3,3,1,1};
UINT4 LldpXdot3RemLinkAggPortId [ ] ={1,0,8802,1,1,2,1,5,4623,1,3,3,1,2};
UINT4 LldpXdot3RemMaxFrameSize [ ] ={1,0,8802,1,1,2,1,5,4623,1,3,4,1,1};


tMbDbEntry stdot3MibEntry[]= {

{{14,LldpXdot3PortConfigTLVsTxEnable}, GetNextIndexLldpXdot3PortConfigTable, LldpXdot3PortConfigTLVsTxEnableGet, LldpXdot3PortConfigTLVsTxEnableSet, LldpXdot3PortConfigTLVsTxEnableTest, LldpXdot3PortConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, LldpXdot3PortConfigTableINDEX, 1, 0, 0, NULL},

{{14,LldpXdot3LocPortAutoNegSupported}, GetNextIndexLldpXdot3LocPortTable, LldpXdot3LocPortAutoNegSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot3LocPortTableINDEX, 1, 0, 0, NULL},

{{14,LldpXdot3LocPortAutoNegEnabled}, GetNextIndexLldpXdot3LocPortTable, LldpXdot3LocPortAutoNegEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot3LocPortTableINDEX, 1, 0, 0, NULL},

{{14,LldpXdot3LocPortAutoNegAdvertisedCap}, GetNextIndexLldpXdot3LocPortTable, LldpXdot3LocPortAutoNegAdvertisedCapGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpXdot3LocPortTableINDEX, 1, 0, 0, NULL},

{{14,LldpXdot3LocPortOperMauType}, GetNextIndexLldpXdot3LocPortTable, LldpXdot3LocPortOperMauTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpXdot3LocPortTableINDEX, 1, 0, 0, NULL},

{{14,LldpXdot3LocPowerPortClass}, GetNextIndexLldpXdot3LocPowerTable, LldpXdot3LocPowerPortClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot3LocPowerTableINDEX, 1, 0, 0, NULL},

{{14,LldpXdot3LocPowerMDISupported}, GetNextIndexLldpXdot3LocPowerTable, LldpXdot3LocPowerMDISupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot3LocPowerTableINDEX, 1, 0, 0, NULL},

{{14,LldpXdot3LocPowerMDIEnabled}, GetNextIndexLldpXdot3LocPowerTable, LldpXdot3LocPowerMDIEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot3LocPowerTableINDEX, 1, 0, 0, NULL},

{{14,LldpXdot3LocPowerPairControlable}, GetNextIndexLldpXdot3LocPowerTable, LldpXdot3LocPowerPairControlableGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot3LocPowerTableINDEX, 1, 0, 0, NULL},

{{14,LldpXdot3LocPowerPairs}, GetNextIndexLldpXdot3LocPowerTable, LldpXdot3LocPowerPairsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpXdot3LocPowerTableINDEX, 1, 0, 0, NULL},

{{14,LldpXdot3LocPowerClass}, GetNextIndexLldpXdot3LocPowerTable, LldpXdot3LocPowerClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpXdot3LocPowerTableINDEX, 1, 0, 0, NULL},

{{14,LldpXdot3LocLinkAggStatus}, GetNextIndexLldpXdot3LocLinkAggTable, LldpXdot3LocLinkAggStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpXdot3LocLinkAggTableINDEX, 1, 0, 0, NULL},

{{14,LldpXdot3LocLinkAggPortId}, GetNextIndexLldpXdot3LocLinkAggTable, LldpXdot3LocLinkAggPortIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpXdot3LocLinkAggTableINDEX, 1, 0, 0, NULL},

{{14,LldpXdot3LocMaxFrameSize}, GetNextIndexLldpXdot3LocMaxFrameSizeTable, LldpXdot3LocMaxFrameSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpXdot3LocMaxFrameSizeTableINDEX, 1, 0, 0, NULL},

{{14,LldpXdot3RemPortAutoNegSupported}, GetNextIndexLldpXdot3RemPortTable, LldpXdot3RemPortAutoNegSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot3RemPortTableINDEX, 3, 0, 0, NULL},

{{14,LldpXdot3RemPortAutoNegEnabled}, GetNextIndexLldpXdot3RemPortTable, LldpXdot3RemPortAutoNegEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot3RemPortTableINDEX, 3, 0, 0, NULL},

{{14,LldpXdot3RemPortAutoNegAdvertisedCap}, GetNextIndexLldpXdot3RemPortTable, LldpXdot3RemPortAutoNegAdvertisedCapGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpXdot3RemPortTableINDEX, 3, 0, 0, NULL},

{{14,LldpXdot3RemPortOperMauType}, GetNextIndexLldpXdot3RemPortTable, LldpXdot3RemPortOperMauTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpXdot3RemPortTableINDEX, 3, 0, 0, NULL},

{{14,LldpXdot3RemPowerPortClass}, GetNextIndexLldpXdot3RemPowerTable, LldpXdot3RemPowerPortClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot3RemPowerTableINDEX, 3, 0, 0, NULL},

{{14,LldpXdot3RemPowerMDISupported}, GetNextIndexLldpXdot3RemPowerTable, LldpXdot3RemPowerMDISupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot3RemPowerTableINDEX, 3, 0, 0, NULL},

{{14,LldpXdot3RemPowerMDIEnabled}, GetNextIndexLldpXdot3RemPowerTable, LldpXdot3RemPowerMDIEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot3RemPowerTableINDEX, 3, 0, 0, NULL},

{{14,LldpXdot3RemPowerPairControlable}, GetNextIndexLldpXdot3RemPowerTable, LldpXdot3RemPowerPairControlableGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot3RemPowerTableINDEX, 3, 0, 0, NULL},

{{14,LldpXdot3RemPowerPairs}, GetNextIndexLldpXdot3RemPowerTable, LldpXdot3RemPowerPairsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpXdot3RemPowerTableINDEX, 3, 0, 0, NULL},

{{14,LldpXdot3RemPowerClass}, GetNextIndexLldpXdot3RemPowerTable, LldpXdot3RemPowerClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpXdot3RemPowerTableINDEX, 3, 0, 0, NULL},

{{14,LldpXdot3RemLinkAggStatus}, GetNextIndexLldpXdot3RemLinkAggTable, LldpXdot3RemLinkAggStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpXdot3RemLinkAggTableINDEX, 3, 0, 0, NULL},

{{14,LldpXdot3RemLinkAggPortId}, GetNextIndexLldpXdot3RemLinkAggTable, LldpXdot3RemLinkAggPortIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpXdot3RemLinkAggTableINDEX, 3, 0, 0, NULL},

{{14,LldpXdot3RemMaxFrameSize}, GetNextIndexLldpXdot3RemMaxFrameSizeTable, LldpXdot3RemMaxFrameSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpXdot3RemMaxFrameSizeTableINDEX, 3, 0, 0, NULL},
};
tMibData stdot3Entry = { 27, stdot3MibEntry };
#endif /* _STDOT3DB_H */

