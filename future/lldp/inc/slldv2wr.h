/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: slldv2wr.h,v 1.2 2013/03/28 11:45:03 siva Exp $
 * *
 * * Description: Protocol Mib Data base
 * *********************************************************************/

#ifndef _SLLDV2WR_H
#define _SLLDV2WR_H

VOID RegisterSLLDV2(VOID);

VOID UnRegisterSLLDV2(VOID);
INT4 LldpV2MessageTxIntervalGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2MessageTxHoldMultiplierGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2ReinitDelayGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2NotificationIntervalGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2TxCreditMaxGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2MessageFastTxGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2TxFastInitGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2MessageTxIntervalSet(tSnmpIndex *, tRetVal *);
INT4 LldpV2MessageTxHoldMultiplierSet(tSnmpIndex *, tRetVal *);
INT4 LldpV2ReinitDelaySet(tSnmpIndex *, tRetVal *);
INT4 LldpV2NotificationIntervalSet(tSnmpIndex *, tRetVal *);
INT4 LldpV2TxCreditMaxSet(tSnmpIndex *, tRetVal *);
INT4 LldpV2MessageFastTxSet(tSnmpIndex *, tRetVal *);
INT4 LldpV2TxFastInitSet(tSnmpIndex *, tRetVal *);
INT4 LldpV2MessageTxIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpV2MessageTxHoldMultiplierTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpV2ReinitDelayTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpV2NotificationIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpV2TxCreditMaxTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpV2MessageFastTxTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpV2TxFastInitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpV2MessageTxIntervalDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 LldpV2MessageTxHoldMultiplierDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 LldpV2ReinitDelayDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 LldpV2NotificationIntervalDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 LldpV2TxCreditMaxDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 LldpV2MessageFastTxDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 LldpV2TxFastInitDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexLldpV2PortConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2PortConfigAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2PortConfigNotificationEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2PortConfigTLVsTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2PortConfigAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 LldpV2PortConfigNotificationEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpV2PortConfigTLVsTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpV2PortConfigAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpV2PortConfigNotificationEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpV2PortConfigTLVsTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpV2PortConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexLldpV2DestAddressTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2DestMacAddressGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2ManAddrConfigTxPortsTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2ManAddrConfigTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2ManAddrConfigRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2ManAddrConfigTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpV2ManAddrConfigRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 LldpV2ManAddrConfigTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpV2ManAddrConfigRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpV2ManAddrConfigTxPortsTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 LldpV2StatsRemTablesLastChangeTimeGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2StatsRemTablesInsertsGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2StatsRemTablesDeletesGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2StatsRemTablesDropsGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2StatsRemTablesAgeoutsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2StatsTxPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2StatsTxPortFramesTotalGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2StatsTxLLDPDULengthErrorsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2StatsRxPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2StatsRxPortFramesDiscardedTotalGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2StatsRxPortFramesErrorsGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2StatsRxPortFramesTotalGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2StatsRxPortTLVsDiscardedTotalGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2StatsRxPortTLVsUnrecognizedTotalGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2StatsRxPortAgeoutsTotalGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2LocChassisIdSubtypeGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2LocChassisIdGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2LocSysNameGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2LocSysDescGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2LocSysCapSupportedGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2LocSysCapEnabledGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2LocPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2LocPortIdSubtypeGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2LocPortIdGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2LocPortDescGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2LocManAddrTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2LocManAddrLenGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2LocManAddrIfSubtypeGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2LocManAddrIfIdGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2LocManAddrOIDGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2RemTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2RemChassisIdSubtypeGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2RemChassisIdGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2RemPortIdSubtypeGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2RemPortIdGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2RemPortDescGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2RemSysNameGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2RemSysDescGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2RemSysCapSupportedGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2RemSysCapEnabledGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2RemRemoteChangesGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2RemTooManyNeighborsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2RemManAddrTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2RemManAddrIfSubtypeGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2RemManAddrIfIdGet(tSnmpIndex *, tRetVal *);
INT4 LldpV2RemManAddrOIDGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2RemUnknownTLVTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2RemUnknownTLVInfoGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2RemOrgDefInfoTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2RemOrgDefInfoGet(tSnmpIndex *, tRetVal *);
#endif /* _SLLDV2WR_H */
