/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fslldlw.h,v 1.11 2016/07/12 12:40:24 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLldpSystemControl ARG_LIST((INT4 *));

INT1
nmhGetFsLldpModuleStatus ARG_LIST((INT4 *));

INT1
nmhGetFsLldpTraceInput ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLldpTraceOption ARG_LIST((INT4 *));

INT1
nmhGetFsLldpTraceLevel ARG_LIST((INT4 *));

INT1
nmhGetFsLldpTagStatus ARG_LIST((INT4 *));

INT1
nmhGetFsLldpConfiguredMgmtIpv4Address ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLldpConfiguredMgmtIpv6Address ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsLldpSystemControl ARG_LIST((INT4 ));

INT1
nmhSetFsLldpModuleStatus ARG_LIST((INT4 ));

INT1
nmhSetFsLldpTraceInput ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsLldpTraceLevel ARG_LIST((INT4 ));

INT1
nmhSetFsLldpTagStatus ARG_LIST((INT4 ));

INT1
nmhSetFsLldpConfiguredMgmtIpv4Address ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsLldpConfiguredMgmtIpv6Address ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsLldpSystemControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsLldpModuleStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsLldpTraceInput ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsLldpTraceLevel ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsLldpTagStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsLldpConfiguredMgmtIpv4Address ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsLldpConfiguredMgmtIpv6Address ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsLldpSystemControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLldpModuleStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLldpTraceInput ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLldpTraceLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLldpTagStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLldpConfiguredMgmtIpv4Address ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLldpConfiguredMgmtIpv6Address ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLldpLocChassisIdSubtype ARG_LIST((INT4 *));

INT1
nmhGetFsLldpLocChassisId ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsLldpLocChassisIdSubtype ARG_LIST((INT4 ));

INT1
nmhSetFsLldpLocChassisId ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsLldpLocChassisIdSubtype ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsLldpLocChassisId ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsLldpLocChassisIdSubtype ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLldpLocChassisId ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsLldpLocPortTable. */
INT1
nmhValidateIndexInstanceFsLldpLocPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsLldpLocPortTable  */

INT1
nmhGetFirstIndexFsLldpLocPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLldpLocPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLldpLocPortIdSubtype ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLldpLocPortId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLldpPortConfigNotificationType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLldpLocPortDstMac ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsLldpMedAdminStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsLldpLocPortIdSubtype ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsLldpLocPortId ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsLldpPortConfigNotificationType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsLldpLocPortDstMac ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetFsLldpMedAdminStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsLldpLocPortIdSubtype ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsLldpLocPortId ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsLldpPortConfigNotificationType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsLldpLocPortDstMac ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2FsLldpMedAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsLldpLocPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsLldpManAddrConfigTable. */
INT1
nmhValidateIndexInstanceFsLldpManAddrConfigTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsLldpManAddrConfigTable  */

INT1
nmhGetFirstIndexFsLldpManAddrConfigTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLldpManAddrConfigTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLldpManAddrConfigOperStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLldpMemAllocFailure ARG_LIST((INT4 *));

INT1
nmhGetFsLldpInputQOverFlows ARG_LIST((INT4 *));

INT1
nmhGetFsLldpStatsRemTablesUpdates ARG_LIST((UINT4 *));

INT1
nmhGetFsLldpClearStats ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsLldpClearStats ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsLldpClearStats ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsLldpClearStats ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFslldpv2Version ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFslldpv2Version ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fslldpv2Version ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fslldpv2Version ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fslldpv2ConfigPortMapTable. */
INT1
nmhValidateIndexInstanceFslldpv2ConfigPortMapTable ARG_LIST((INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for Fslldpv2ConfigPortMapTable  */

INT1
nmhGetFirstIndexFslldpv2ConfigPortMapTable ARG_LIST((INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFslldpv2ConfigPortMapTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFslldpv2ConfigPortMapNum ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFslldpv2ConfigPortRowStatus ARG_LIST((INT4  , tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFslldpv2ConfigPortRowStatus ARG_LIST((INT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fslldpv2ConfigPortRowStatus ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fslldpv2ConfigPortMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FslldpV2DestAddressTable. */
INT1
nmhValidateIndexInstanceFslldpV2DestAddressTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FslldpV2DestAddressTable  */

INT1
nmhGetFirstIndexFslldpV2DestAddressTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFslldpV2DestAddressTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFslldpV2DestMacAddress ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetFslldpv2DestRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFslldpV2DestMacAddress ARG_LIST((UINT4  ,tMacAddr ));

INT1
nmhSetFslldpv2DestRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FslldpV2DestMacAddress ARG_LIST((UINT4 *  ,UINT4  ,tMacAddr ));

INT1
nmhTestv2Fslldpv2DestRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FslldpV2DestAddressTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsLldpStatsTaggedTxPortTable. */
INT1
nmhValidateIndexInstanceFsLldpStatsTaggedTxPortTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsLldpStatsTaggedTxPortTable  */

INT1
nmhGetFirstIndexFsLldpStatsTaggedTxPortTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLldpStatsTaggedTxPortTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLldpStatsTaggedTxPortFramesTotal ARG_LIST((INT4  , UINT4 ,UINT4 *));
