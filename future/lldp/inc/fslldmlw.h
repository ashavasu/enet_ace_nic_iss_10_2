/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fslldmlw.h,v 1.2 2016/01/28 11:09:06 siva Exp $
*
* Description: LLDP-MED Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsLldpMedPortConfigTable. */
INT1
nmhValidateIndexInstanceFsLldpMedPortConfigTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsLldpMedPortConfigTable  */

INT1
nmhGetFirstIndexFsLldpMedPortConfigTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLldpMedPortConfigTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLldpMedPortCapSupported ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLldpMedPortConfigTLVsTxEnable ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsLldpMedPortConfigTLVsTxEnable ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsLldpMedPortConfigTLVsTxEnable ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsLldpMedPortConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsLldpMedLocMediaPolicyTable. */
INT1
nmhValidateIndexInstanceFsLldpMedLocMediaPolicyTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsLldpMedLocMediaPolicyTable  */

INT1
nmhGetFirstIndexFsLldpMedLocMediaPolicyTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLldpMedLocMediaPolicyTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLldpMedLocMediaPolicyVlanID ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsLldpMedLocMediaPolicyPriority ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsLldpMedLocMediaPolicyDscp ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsLldpMedLocMediaPolicyUnknown ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsLldpMedLocMediaPolicyTagged ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsLldpMedLocMediaPolicyRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsLldpMedLocMediaPolicyVlanID ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsLldpMedLocMediaPolicyPriority ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsLldpMedLocMediaPolicyDscp ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsLldpMedLocMediaPolicyUnknown ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsLldpMedLocMediaPolicyTagged ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsLldpMedLocMediaPolicyRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsLldpMedLocMediaPolicyVlanID ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsLldpMedLocMediaPolicyPriority ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsLldpMedLocMediaPolicyDscp ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsLldpMedLocMediaPolicyUnknown ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsLldpMedLocMediaPolicyTagged ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsLldpMedLocMediaPolicyRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsLldpMedLocMediaPolicyTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsLldpMedLocLocationTable. */
INT1
nmhValidateIndexInstanceFsLldpMedLocLocationTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsLldpMedLocLocationTable  */

INT1
nmhGetFirstIndexFsLldpMedLocLocationTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLldpMedLocLocationTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLldpMedLocLocationRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsLldpMedLocLocationRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsLldpMedLocLocationRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsLldpMedLocLocationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsLldpXMedRemCapabilitiesTable. */
INT1
nmhValidateIndexInstanceFsLldpXMedRemCapabilitiesTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsLldpXMedRemCapabilitiesTable  */

INT1
nmhGetFirstIndexFsLldpXMedRemCapabilitiesTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLldpXMedRemCapabilitiesTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLldpXMedRemCapSupported ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLldpXMedRemCapCurrent ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLldpXMedRemDeviceClass ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsLldpXMedRemMediaPolicyTable. */
INT1
nmhValidateIndexInstanceFsLldpXMedRemMediaPolicyTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsLldpXMedRemMediaPolicyTable  */

INT1
nmhGetFirstIndexFsLldpXMedRemMediaPolicyTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLldpXMedRemMediaPolicyTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLldpXMedRemMediaPolicyVlanID ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsLldpXMedRemMediaPolicyPriority ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsLldpXMedRemMediaPolicyDscp ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsLldpXMedRemMediaPolicyUnknown ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsLldpXMedRemMediaPolicyTagged ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Validate Index Instance for FsLldpXMedRemInventoryTable. */
INT1
nmhValidateIndexInstanceFsLldpXMedRemInventoryTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsLldpXMedRemInventoryTable  */

INT1
nmhGetFirstIndexFsLldpXMedRemInventoryTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLldpXMedRemInventoryTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLldpXMedRemHardwareRev ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLldpXMedRemFirmwareRev ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLldpXMedRemSoftwareRev ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLldpXMedRemSerialNum ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLldpXMedRemMfgName ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLldpXMedRemModelName ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLldpXMedRemAssetID ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsLldpMedStatsTable. */
INT1
nmhValidateIndexInstanceFsLldpMedStatsTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsLldpMedStatsTable  */

INT1
nmhGetFirstIndexFsLldpMedStatsTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLldpMedStatsTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLldpMedStatsTxFramesTotal ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLldpMedStatsRxFramesTotal ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLldpMedStatsRxFramesDiscardedTotal ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLldpMedStatsRxTLVsDiscardedTotal ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLldpMedStatsRxCapTLVsDiscarded ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLldpMedStatsRxPolicyTLVsDiscarded ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLldpMedStatsRxInventoryTLVsDiscarded ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLldpMedStatsRxLocationTLVsDiscarded ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLldpMedStatsRxExPowerMDITLVsDiscarded ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLldpMedStatsRxCapTLVsDiscardedReason ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLldpMedStatsRxPolicyDiscardedReason ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLldpMedStatsRxInventoryDiscardedReason ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLldpMedStatsRxLocationDiscardedReason ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLldpMedStatsRxExPowerDiscardedReason ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLldpMedClearStats ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsLldpMedClearStats ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsLldpMedClearStats ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsLldpMedClearStats ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsLldpXMedRemLocationTable. */
INT1
nmhValidateIndexInstanceFsLldpXMedRemLocationTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsLldpXMedRemLocationTable  */

INT1
nmhGetFirstIndexFsLldpXMedRemLocationTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLldpXMedRemLocationTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLldpXMedRemLocationInfo ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsLldpXMedRemXPoEPDTable. */
INT1
nmhValidateIndexInstanceFsLldpXMedRemXPoEPDTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsLldpXMedRemXPoEPDTable  */

INT1
nmhGetFirstIndexFsLldpXMedRemXPoEPDTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLldpXMedRemXPoEPDTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLldpXMedRemXPoEDeviceType ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsLldpXMedRemXPoEPDPowerReq ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsLldpXMedRemXPoEPDPowerSource ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsLldpXMedRemXPoEPDPowerPriority ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));
