
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lldprot.h,v 1.65 2017/12/14 10:30:25 siva Exp $
 *
 * Description: This file contains prototypes for functions
 *              defined in LLDP.
 **********************************************************************/
#ifndef _LLDPROTO_H
#define _LLDPROTO_H

/*--------------------------------------------------------------------------*/
/*                       lldmain.c                                          */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 LldpMainInitGlobalInfo       PROTO ((VOID));
PUBLIC VOID LldpMainMemClear             PROTO ((VOID));
PUBLIC VOID LldpMainAssignMempoolIds     PROTO ((VOID));
/*--------------------------------------------------------------------------*/
/*                       lldmod.c                                           */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 LldpModuleStart              PROTO ((VOID));
PUBLIC VOID LldpModuleShutDown           PROTO ((VOID));
PUBLIC VOID LldpModuleEnable             PROTO ((VOID));
PUBLIC VOID LldpModuleDisable            PROTO ((VOID));

/*--------------------------------------------------------------------------*/
/*                       lldrx.c                                            */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 LldpRxComparePduMsgDigest    PROTO ((UINT1 *pu1RecvDigest, 
                                                 UINT1 *pu1ExDigest));
PUBLIC INT4 LldpRxUpdateRemoteLink       PROTO ((tLldpRemoteNode *pRemoteNode));
PUBLIC INT4 LldpRxGetMSAPIdFromPdu       PROTO ((UINT1 *pu1Lldpdu, 
                                                 INT4 *pi4ChassIdSubtype,
                                                 UINT1 *pu1ChassisId,
                                                 INT4 *pi4PortIdSubtype,
                                                 UINT1 *pu1PortId));
PUBLIC INT4 LldpRxProcessTlv             PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT2 u2TlvType,
                                                 UINT2 u2TlvLength,
                                                 UINT1 *pu1Tlv,
                                                 UINT2 *pu2MedSupTlvs));
PUBLIC INT4 LldpRxProcessFrame           PROTO ((tLldpLocPortInfo *
                                                 pLocPortInfo));
PUBLIC INT4 LldpRxUpdateRemDataBase      PROTO ((tLldpLocPortInfo *
                                                 pLocPortInfo));
PUBLIC INT4 LldpRxUpdtTlvInfo            PROTO ((tLldpLocPortInfo *
                                                 pLocPortInfo, 
                                                 UINT2 u2TlvType, 
                                                 UINT2 u2TlvLength,
                                                 UINT1 *pu1Tlv));
PUBLIC INT4 LldpRxProcessRefreshFrame    PROTO ((tLldpLocPortInfo *
                                                 pLocPortInfo));
PUBLIC VOID 
LldpRxChkMisConfigProtoVlanId            PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 tLldpxdot1RemProtoVlanInfo *
                                                 pProtoVlanNode));
PUBLIC VOID LldpRxChkMisConfigVlanName   PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 tLldpxdot1RemVlanNameInfo *
                                                 pVlanNameNode));
PUBLIC VOID LldpRxChkMisConfigChassId    PROTO ((tLldpRemoteNode *pRemoteNode));

PUBLIC VOID
LldpRxPostTlvsToApplications PROTO ((tLldpLocPortInfo * pLocPortInfo));
PUBLIC VOID
LldpRxFrameNotificationToApplns PROTO ((tLldpLocPortInfo *pLocPortInfo));
PUBLIC VOID
LldpRxProcessWholePdu PROTO ((tLldpLocPortInfo * pLocPortInfo));
/*--------------------------------------------------------------------------*/
/*                       lldport.c                                          */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 
LldpPortValidatePortIdSubType            PROTO ((INT4 i4PortIdSubType));
    
PUBLIC INT4 LldpPortGetIfIndexFromPort   PROTO ((UINT4 u4Port,
                                                 UINT4 *pu4IfIndex));
PUBLIC INT4 LldpPortHandleOutgoingPkt    PROTO ((tCRU_BUF_CHAIN_HEADER *
                                                 pPktToSend,
                                                 UINT4 u4LocPortNum,
                                                 UINT4 u4FrameSize));
PUBLIC INT4 LldpPortGetNextValidPort     PROTO ((UINT2 u2PrevPort, 
                                                 UINT2 *pu2PortIndex));
PUBLIC VOID LldpPortNotifyFaults         PROTO ((tSNMP_VAR_BIND *pTrapMsg,
                                                 UINT1 *pu1Msg, 
                                                 UINT4 u4ModuleId));
PUBLIC INT4 LldpPortValidateIfIndex      PROTO ((UINT4 u4PortIndex));
PUBLIC INT4 LldpPortGetProtoVlanStatus   PROTO ((UINT4 u4LocPortNum, 
                                                 UINT1 *pu1VlanStatus));
PUBLIC INT4 LldpPortGetPortVlanId        PROTO ((UINT4 u4LocPortNum,
                                                 UINT2 *pu2PVid));
PUBLIC INT4 LldpPortGetProtoVlan         PROTO ((UINT4 u4LocPortNum, 
                                                 UINT2 *pu2ConfProtoVlans,
                                                 UINT1 *pu1NumVlan));
PUBLIC INT4 LldpPortGetSysName           PROTO ((UINT1 *pu1RetValSysName));
PUBLIC INT4 LldpPortGetSysDescr          PROTO ((UINT1 *pu1RetValSysDescr));
PUBLIC VOID LldpPortGetSysCapaSupported  PROTO ((UINT1 *pu1SysCapaSupported));
PUBLIC VOID LldpPortGetSysCapaEnabled    PROTO ((UINT1 *pu1SysCapaEnabled));
PUBLIC INT4 LldpPortGetIpv4Addr          PROTO ((UINT4 u4IfIndex,
                                                 UINT4 *pu4Ipv4Addr));
PUBLIC VOID LldpPortGetSysMacAddr        PROTO ((tMacAddr BaseMacAddr));
PUBLIC INT4 LldpPortGetIfAlias           PROTO ((UINT4 u4IfIndex,
                                                 UINT1 *pu1IfAlias));
PUBLIC INT4 LldpPortGetIfOperStatus      PROTO ((UINT4 u4LocPortNum, 
                                                 UINT1 *pu1OperStatus));
PUBLIC VOID LldpPortGetProtoVlanCapab    PROTO ((UINT1 *pu1Flag));
PUBLIC INT4 LldpPortGetAggStatus         PROTO ((UINT4 u4LocPortNum, 
                                                 UINT1 *pu1AggStatus));
PUBLIC VOID 
LldpPortGetDefaultRouterIfIndex          PROTO ((UINT4 *pu4DefIfIndex));
PUBLIC INT4 
LldpPortCalculatePduMsgDigest            PROTO ((UINT1 *, UINT2, UINT1 *));
PUBLIC INT4 LldpPortGetIfInfo            PROTO ((UINT4 u4IfIndex, 
                                                 tCfaIfInfo *pCfaIfInfo));
PUBLIC INT4 LldpPortGetAgentCktId        PROTO ((UINT4 u4PortNum, 
                                                 UINT4 * pu4RetValAgentCktId));
PUBLIC INT4 LldpPortGetOidforIfIndex     PROTO ((tLldpManAddrOid *pRetValOidStr));
PUBLIC INT4 LldpPortRegisterWithIp       PROTO ((VOID));
PUBLIC INT4 LldpPortDeRegisterWithIp     PROTO ((VOID));
PUBLIC INT4
LldpPortDownProcessedIndication          PROTO ((UINT4 u4IfIndex));
PUBLIC INT4 LldpPortDeRegisterWithRM     PROTO ((VOID));
PUBLIC VOID
LldpPortSendReRegToApp PROTO ((VOID));
PUBLIC VOID
LldpPortGetPortsForPortChannel PROTO ((UINT2 u2AggId, UINT2 *pu2MemberPorts,
                                       UINT2 *pu2NumPorts));
PUBLIC INT4
LldpPortIsPortInPortChannel PROTO ((UINT4 u4IfIndex));
PUBLIC VOID
LldpPortGetPortChannelForPort PROTO ((tLldpLocPortInfo * pPortInfo, 
                                      UINT4 *pu4AggId));
INT4
LLdpLaIsPortInIcclIf (UINT4 u4IfIndex);

/*LLDP-MED*/

PUBLIC INT4 LldpMedPortGetInventoryInfo  PROTO ((tLldpMedLocInventoryInfo 
                                                *pInventoryInfo));

PUBLIC INT4 LldpMedPortGetPSEPowerInfo   PROTO ((tLldpLocPortTable
                                                *pPortEntry));

PUBLIC INT4 LldpMedPortGetPoEPowerInfo   PROTO ((UINT1 *pu1PSEPowerSource,
                                                UINT1 *pu1PoEDeviceType));
/*--------------------------------------------------------------------------*/
/*                       lldred.c                                           */
/*--------------------------------------------------------------------------*/

PUBLIC VOID
LldpRedProcBulkUpdReq                    PROTO ((VOID));

PUBLIC INT4 LldpPortRegisterWithRM       PROTO ((VOID));
    
PUBLIC INT4 LldpPortRelRmMsgMem          PROTO ((UINT1 *pu1Block));

PUBLIC INT4 LldpPortEnqMsgToRm           PROTO ((tRmMsg * pRmMsg, 
                                                 UINT2 u2DataLen));

PUBLIC UINT1 LldpPortGetStandbyNodeCount PROTO ((VOID));

PUBLIC VOID LldpPortSetBulkUpdateStatus  PROTO ((VOID));
PUBLIC UINT4 LldpPortGetRmNodeState PROTO ((VOID));
PUBLIC INT4 LldpPortSendEventToRm PROTO ((tRmProtoEvt *pEvt));
    
PUBLIC VOID LldpNotifyLocSysInfoChg      PROTO ((UINT1, UINT4, INT4,UINT1 *));
/* HITLESS RESTART */
VOID LldpRedHRProcStdyStPktReq  PROTO ((VOID));
INT1 LldpRedHRSendStdyStTailMsg PROTO ((VOID));
INT1
LldpRedHRSendStdyStPkt (tCRU_BUF_CHAIN_HEADER *pu1LinBuf, UINT4 u4PktLen, 
        UINT2 u2Port, UINT4 u4TimeOut);
UINT1
LldpRedGetHRFlag (VOID);

/*--------------------------------------------------------------------------*/
/*                       lldvlndb.c                                         */
/*--------------------------------------------------------------------------*/
PUBLIC INT4
LldpVlndbValidateVlanNameTable           PROTO ((UINT4 u4IfIndex,
                                                 UINT2 VlanId));
PUBLIC INT4
LldpVlndbGetFirstVlanNameTblInd          PROTO ((INT4 *pi4IfIndex,
                                                 INT4 *pi4VlanId));
PUBLIC INT4
LldpVlndbGetNextVlanNameTblInd           PROTO ((INT4 i4LldpLocPortNum,
                                                 INT4 *pi4NextLldpLocPortNum,
                                                 INT4 i4LldpXdot1LocVlanId,
                                                 INT4 *pi4NextLldpXdot1LocVlanId));
PUBLIC INT4 LldpVlndbGetVlanName         PROTO ((UINT4 u4LldpLocPortNum, 
                                                 UINT2 u2LldpXdot1LocVlanId, 
                                                 UINT1 *pu1VlanName));
PUBLIC INT4 LldpVlndbGetVlanNameTxStatus PROTO ((UINT4 u4LldpLocPortNum, 
                                                 UINT2 u2LldpXdot1LocVlanId, 
                                                 UINT1 *pu1VlanNameTxStatus));
PUBLIC INT4 LldpVlndbGetAllVlanName      PROTO ((UINT4 u4PortNum, 
                                                 tVlanNameInfo *pVlanNameInfo,
                                                 UINT2 *pu2VlanNameCount));
PUBLIC INT4 LldpVlndbSetVlanNameTxStatus PROTO ((UINT4 u4LldpLocPortNum, 
                                                 UINT2 u2LldpXdot1LocVlanId,
                                                 UINT1 u1VlanNameTxStatus,
                                                 UINT1 *pu1ConfTxEnable));
PUBLIC INT4
LldpVlndbValidateVlanNameEntry           PROTO ((UINT4 u4LldpLocPortNum, 
                                                 UINT2 u2LldpXdot1LocVlanId));
/*--------------------------------------------------------------------------*/
/*                       lldutil.c                                          */
/*--------------------------------------------------------------------------*/
PUBLIC VOID LldpUtilClearStats           PROTO ((tLldpLocPortInfo *pPortInfo));
PUBLIC VOID LldpUtilPktFree            PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf));
PUBLIC VOID LldpUtilCreateRBTree         PROTO ((VOID));
PUBLIC VOID LldpUtilDeleteRBTree         PROTO ((VOID));
PUBLIC INT4 LldpUtilGetPortIdLen         PROTO ((INT4 i4Subtype, 
                                                 UINT1 *pu1PortId, 
                                                 UINT2 *pu2Length));
PUBLIC INT4 LldpUtilGetChassisIdLen      PROTO ((INT4 i4Subtype, 
                                                 UINT1 *pu1ChassId, 
                                                 UINT2 *pu2Length));
PUBLIC UINT2 LldpUtilGetManAddrLen       PROTO ((INT4 i4ManAddrSubtype));
PUBLIC UINT4 LldpUtilGetTraceOptionValue PROTO ((UINT1 *pu1TraceInput, 
                                                 INT4 i4Tracelen));
PUBLIC UINT4 LldpUtilGetTraceInputValue  PROTO ((UINT1 *pu1TraceInput,
                                                 UINT4 u4TracVal));
PUBLIC INT4 LldpUtilEncodeManAddrOid     PROTO ((tLldpManAddrOid *pInOidStr,
                                                 UINT1 *pu1OutOidStr));
PUBLIC INT4 LldpUtilDecodeManAddrOid     PROTO ((UINT1 *pu1InOidStr, 
                                                 tLldpManAddrOid *pOutOidStr));
PUBLIC VOID LldpUtilDeletePortInfo       PROTO ((tLldpLocPortInfo *pPortInfo));

PUBLIC VOID LldpUtilSetTraceOption       PROTO ((UINT1 *pu1Token, 
                                                 UINT4 *pu4TraceOption,
                                                 UINT1 *pu1TlvTokenPresent));
PUBLIC VOID LldpUtilSplitStrToTokens     PROTO ((UINT1 *pu1InputStr, 
                                                 INT4 i4Strlen, 
                                                 UINT1 u1Delimiter, 
                                                 UINT2 u2MaxToken,
                                                 UINT1 *apu1Token[], 
                                                 UINT1 *pu1TokenCount));
PUBLIC VOID
LldpUtlSendAppAgeout PROTO ((tLldpLocPortInfo *pPortInfo));
PUBLIC INT4 LldpAppUtlRBCmpInfo PROTO ((tRBElem * pElem1, tRBElem * pElem2));

PUBLIC INT4
LldpAgentToLocPortUtlRBCmpInfo PROTO ((tRBElem * pElem1, tRBElem * pElem2));
PUBLIC INT4
LldpAgentToLocPortIndexUtlRBCmpInfo PROTO ((tRBElem * pElem1, tRBElem * pElem2));
PUBLIC INT4
LldpDstMacAddrUtlRBCmpInfo PROTO ((tRBElem * pElem1, tRBElem * pElem2));
PUBLIC INT4
LldpPortInfoUtlRBCmpInfo PROTO ((tRBElem * pElem1, tRBElem * pElem2));
PUBLIC INT4
LldpAgentInfoUtlRBCmpInfo PROTO ((tRBElem * pElem1, tRBElem * pElem2));
/* LLDP-MED */
PUBLIC INT4
LldpMedLocNwPolicyInfoRBCmpInfo          PROTO ((tRBElem * pElem1, 
                                                 tRBElem * pElem2));
PUBLIC INT4
LldpMedRemNwPolicyInfoRBCmpInfo          PROTO ((tRBElem * pElem1, 
                                                 tRBElem * pElem2));
PUBLIC INT4
LldpMedLocLocationInfoRBCmpInfo          PROTO ((tRBElem * pElem1,
                                                 tRBElem * pElem2));
PUBLIC INT4
LldpMedRemLocationInfoRBCmpInfo          PROTO ((tRBElem * pElem1,
                                                 tRBElem * pElem2));                                                 
INT4
LldpUtlGetLocalPort (INT4 i4IfIndex, tMacAddr DestMacAddr, UINT4 *pu4LocalPort);
INT4
LldpUtlGetLldpPortConfigAdminStatus (INT4 i4LldpPortConfigPortNum, tMacAddr DestMacAddr,
                                     INT4 *pi4PortConfigAdminStatus);
INT4
LldpUtlGetLldpPortConfigNotificationEnable (INT4 i4LldpPortConfigPortNum, tMacAddr DestMacAddr,
                                            INT4 *pi4PortConfigNotificationEnable);
INT4
LldpUtlGetLldpPortConfigTLVsTxEnable (INT4 i4LldpPortConfigPortNum, tMacAddr DestMacAddr,
                                      tSNMP_OCTET_STRING_TYPE *pRetValLldpPortConfigTLVsTxEnable);
INT4 
LldpUtlSetLldpPortConfigAdminStatus (INT4 i4LldpPortConfigPortNum, tMacAddr DestMacAddr,
                                          INT4 i4SetValLldpPortConfigAdminStatus);
INT4
LldpUtlSetLldpPortConfigNotificationEnable (INT4 i4LldpPortConfigPortNum, tMacAddr DestMacAddr,
                                                 INT4 i4SetValLldpPortConfigNotificationEnable);

INT4
LldpUtlSetLldpPortConfigTLVsTxEnable (INT4 i4LldpPortConfigPortNum, tMacAddr DestMacAddr,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSetValLldpPortConfigTLVsTxEnable);

INT4
LldpUtlTestLldpPortConfigAdminStatus (UINT4 *pu4ErrorCode, INT4 i4LldpPortConfigPortNum,
                                      tMacAddr DestMacAddr,
                                      INT4 i4TestValLldpPortConfigAdminStatus);

INT4
LldpUtlTestLldpPortConfigNotificationEnable (UINT4 *pu4ErrorCode, INT4 i4LldpPortConfigPortNum,
                                            tMacAddr DestMacAddr,
                                            INT4 i4TestValLldpPortConfigNotificationEnable);

INT4
LldpUtlTestLldpPortConfigTLVsTxEnable (UINT4 *pu4ErrorCode, INT4 i4LldpPortConfigPortNum,
                                       tMacAddr DestMacAddr,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pTestValLldpPortConfigTLVsTxEnable);
INT4
LldpUtlValidateIndexInstanceLldpLocManAddrTable(INT4 i4LldpLocManAddrSubtype,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pLldpLocManAddr);
INT4
LldpUtlGetFirstIndexLldpLocManAddrTable(INT4 *pi4LldpLocManAddrSubtype,
                                        tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr);
INT4
LldpUtlGetNextIndexLldpLocManAddrTable(INT4 i4LldpLocManAddrSubtype,
                                       INT4 *pi4NextLldpLocManAddrSubtype,
                                       tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextLldpLocManAddr);

INT4
LldpUtlGetLocManAddrLen (INT4 i4LldpLocManAddrSubtype,
                         tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr,
                         INT4 *pi4RetValLldpLocManAddrLen);

INT4
LldpUtlGetLocManAddrIfSubtype (INT4 i4LldpLocManAddrSubtype,
                               tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr,
                               INT4 *pi4RetValLldpLocManAddrIfSubtype);
INT4
LldpUtlGetLocManAddrIfId (INT4 i4LldpLocManAddrSubtype,
                               tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr,
                               INT4 *pi4RetValLldpLocManAddrIfId);
INT4
LldpUtlGetLocManAddrIfOID (INT4 i4LldpLocManAddrSubtype,
                           tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr,
                           tSNMP_OID_TYPE * pRetValLldpLocManAddrOID);
INT4
LldpUtlGetConfigManAddrPortsTxEnable (INT4 i4LldpLocManAddrSubtype,
                                      tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr,
                                      tSNMP_OCTET_STRING_TYPE * pRetValLldpConfigManAddrPortsTxEnable);
INT4
LldpUtlSetConfigManAddrPortsTxEnable (INT4 i4LldpLocManAddrSubtype,
                                      tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr,
                                      tSNMP_OCTET_STRING_TYPE * pSetValLldpConfigManAddrPortsTxEnable);
INT4
LldpUtlGetStatsRemTablesLastChangeTime(UINT4
                                        *pu4RetValLldpStatsRemTablesLastChangeTime);
INT4
LldpUtlTestConfigManAddrPortsTxEnable (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE * pSetValLldpConfigManAddrPortsTxEnable);
INT4
LldpUtlGetStatsRemTablesInserts (UINT4 *pu4RetValLldpStatsRemTablesInserts);

INT4
LldpUtlGetStatsRemTablesDeletes (UINT4 *pu4RetValLldpStatsRemTablesDeletes);
INT4
LldpUtlGetStatsRemTablesDrops (UINT4 *pu4RetValLldpStatsRemTablesDrops);
INT4
LldpUtlGetStatsRemTablesAgeouts (UINT4 *pu4RetValLldpStatsRemTablesAgeouts);
INT4
LldpUtlGetStatsTxPortFramesTotal (INT4 i4LldpPortConfigPortNum, UINT4 *pu4RetValLldpStatsTxPortFramesTotal,
                                  tMacAddr DestMacAddr);
INT4
LldpUtlGetStatsRxPortFramesDiscardedTotal (INT4 i4LldpStatsRxPortNum, tMacAddr DestMacAddr,
                                           UINT4
                                           *pu4RetValLldpStatsRxPortFramesDiscardedTotal);
INT4
LldpUtlGetStatsRxPortFramesErrors(INT4 i4LldpStatsRxPortNum, tMacAddr DestMacAddr,
                                  UINT4 *pu4RetValLldpStatsRxPortFramesErrors);
INT4
LldpUtlGetStatsRxPortFramesTotal (INT4 i4LldpStatsRxPortNum, tMacAddr DestMacAddr,
                                  UINT4 *pu4RetValLldpStatsRxPortFramesTotal);
INT4
LldpUtlGetStatsRxPortTLVsDiscardedTotal (INT4 i4LldpStatsRxPortNum, tMacAddr DestMacAddr,
                                         UINT4
                                         *pu4RetValLldpStatsRxPortTLVsDiscardedTotal);
INT4
LldpUtlGetStatsRxPortTLVsUnrecognizedTotal (INT4 i4LldpStatsRxPortNum, tMacAddr DestMacAddr,
                                            UINT4
                                            *pu4RetValLldpStatsRxPortTLVsUnrecognizedTotal);
INT4
LldpUtlGetStatsRxPortAgeoutsTotal (INT4 i4LldpStatsRxPortNum, tMacAddr DestMacAddr,
                                            UINT4 *pu4RetValLldpStatsRxPortAgeoutsTotal);
INT4
LldpUtlGetLocChassisIdSubtype (INT4 *pi4RetValLldpLocChassisIdSubtype);
INT4
LldpUtlGetLocChassisId (tSNMP_OCTET_STRING_TYPE * pRetValLldpLocChassisId);
INT4
LldpUtlLocSysName (tSNMP_OCTET_STRING_TYPE * pRetValLldpLocSysName);
INT4
LldpUtlLocSysDesc (tSNMP_OCTET_STRING_TYPE * pRetValLldpLocSysDesc);
INT4
LldpUtlLocSysCapSupported (tSNMP_OCTET_STRING_TYPE *
                              pRetValLldpLocSysCapSupported);
INT4
LldpUtlLocSysCapEnabled (tSNMP_OCTET_STRING_TYPE *
                            pRetValLldpLocSysCapEnabled);
INT4
LldpUtlGetLocPortIdSubtype (INT4 i4LldpLocPortNum,
                      INT4 *pi4RetValLldpLocPortIdSubtype);
INT4
LldpUtlLocPortId (INT4 i4LldpLocPortNum,
                  tSNMP_OCTET_STRING_TYPE * pRetValLldpLocPortId);
INT4
LldpUtlGetLocPortDesc (INT4 i4LldpLocPortNum,
                       tSNMP_OCTET_STRING_TYPE * pRetValLldpLocPortDesc);
INT4
LldpUtlGetRemChassisIdSubtype (UINT4 u4LldpRemTimeMark,
                               INT4 i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                               INT4 *pi4RetValLldpRemChassisIdSubtype,
                               UINT4 DestMacAddr);
INT4 
LldpUtlGetRemChassisId(UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                            INT4 i4LldpRemIndex,
                            tSNMP_OCTET_STRING_TYPE * pRetValLldpRemChassisId,
                            UINT4 DestMacAddr);
INT4
LldpUtlGetRemPortIdSubtype (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                            INT4 i4LldpRemIndex,
                            INT4 *pi4RetValLldpRemPortIdSubtype, UINT4 u4DestMacAddr);
INT4
LldpUtlGetRemPortId(UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                    INT4 i4LldpRemIndex,
                    tSNMP_OCTET_STRING_TYPE * pRetValLldpRemPortId, UINT4 u4DestMacAddr);
INT4
LldpUtlGetRemPortDesc(UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                       INT4 i4LldpRemIndex,
                       tSNMP_OCTET_STRING_TYPE * pRetValLldpRemPortDesc, UINT4 u4DestMacAddr);
INT4
LldpUtlGetRemSysName (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                      INT4 i4LldpRemIndex,
                      tSNMP_OCTET_STRING_TYPE * pRetValLldpRemSysName, UINT4 u4DestMacAddr);
INT4
LldpUtlGetRemSysDesc (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                      INT4 i4LldpRemIndex,
                      tSNMP_OCTET_STRING_TYPE * pRetValLldpRemSysDesc, UINT4 u4DestMacAddr);
INT4
LldpUtlGetRemSysCapSupported (UINT4 u4LldpRemTimeMark,
                              INT4 i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValLldpRemSysCapSupported, UINT4 u4DestMacAddr);
INT4
LldpUtlGetRemSysCapEnabled (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                            INT4 i4LldpRemIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValLldpRemSysCapEnabled, UINT4 u4DestMacAddr);
INT4
LldpUtlGetRemManAddrIfSubtype (UINT4 u4LldpRemTimeMark,
                               INT4 i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                               INT4 i4LldpRemManAddrSubtype,
                               tSNMP_OCTET_STRING_TYPE * pLldpRemManAddr,
                               INT4 *pi4RetValLldpRemManAddrIfSubtype, UINT4 u4DestMacAddr);
INT4
LldpUtlGetRemManAddrIfId (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                          INT4 i4LldpRemIndex, INT4 i4LldpRemManAddrSubtype,
                          tSNMP_OCTET_STRING_TYPE * pLldpRemManAddr,
                          INT4 *pi4RetValLldpRemManAddrIfId, UINT4 u4DestMacAddr);
INT4
LldpUtlGetRemManAddrOID (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                         INT4 i4LldpRemIndex, INT4 i4LldpRemManAddrSubtype,
                         tSNMP_OCTET_STRING_TYPE * pLldpRemManAddr,
                         tSNMP_OID_TYPE * pRetValLldpRemManAddrOID, UINT4 u4DestMacAddr);
INT4
LldpUtlGetRemUnknownTLVInfo(UINT4 u4LldpRemTimeMark,
                             INT4 i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
                             INT4 i4LldpRemUnknownTLVType,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValLldpRemUnknownTLVInfo, UINT4 u4DestMacAddr);
INT4
LldpUtlGetRemOrgDefInfo (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                         INT4 i4LldpRemIndex,
                         tSNMP_OCTET_STRING_TYPE * pLldpRemOrgDefInfoOUI,
                         INT4 i4LldpRemOrgDefInfoSubtype,
                         INT4 i4LldpRemOrgDefInfoIndex,
                         tSNMP_OCTET_STRING_TYPE * pRetValLldpRemOrgDefInfo,
                         UINT4 u4DestMacAddr);
INT4
LldpUtlGetMessageTxInterval(INT4 *pi4RetValLldpMessageTxInterval);
INT4 
LldpUtlGetRemRemoteChanges (UINT4 u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum,
                            INT4 i4LldpRemIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pi4RetValLldpV2RemRemoteChanges, UINT4 u4DestMacAddr);
UINT4 LldpV1DestMacAddrIndex PROTO ((INT4 i4IfIndex));

UINT4 LldpV1LocPortNum PROTO ((INT4 i4IfIndex));
UINT4 
LldpGetLocalPortFromIfIndexDestMacIfIndex (UINT4 u4IfIndex,
                                                UINT4 u4DstMacAddrTblIndex);
INT4 
LldpGetMacFromLldpLocalPort (UINT4 u4LldpLocPort,tMacAddr * pMacAddr);
tLldpv2AgentToLocPort * 
LldpGetNextConfigPortMapNode (tLldpv2AgentToLocPort  *pLldpAgentPortEntry);
tLldpv2AgentToLocPort * 
LldpGetPortMapTableNode (UINT4 u4IfIndex, tMacAddr *pMacAddr);
INT4 
LldpGetDestAddrTblIndexFromMacAddr (tMacAddr * pMacAddr , UINT4 *pu4DstMacAddrTblIndex);
INT4 
LldpGetMacAddrFromDestAddrTblIndex (UINT4 u4DstMacAddrTblIndex, tMacAddr * pMacAddr);

VOID
LldpUtilIndMsrForFsScalars (UINT4 *pu4ObjectId, UINT4 u4OidLen, UINT4 u4SetVal);
VOID
LldpUtilUptInfoForStdPortConfigTbl (UINT4 *pu4ObjectId, UINT4 u4OidLen,
                             tSnmpNotifyInfo *pSnmpNotifyInfo);
VOID
LldpUtilIndMsrForStdManAddrTxTbl (INT4 i4LldpLocManAddrSubtype,
                                  tSNMP_OCTET_STRING_TYPE *pLldpLocManAddr,
                                  tSNMP_OCTET_STRING_TYPE *pSetValLldpConfigManAddrPortsTxEnable);
VOID
LldpUtilIndMsrForDot1ConfigPortVlanTbl (UINT4 *pu4ObjectId, UINT4 u4OidLen,
                                          INT4 i4Index, INT4 i4SetVal);
VOID
LldpUtilIndMsrForDot1ConfigVlanNameTbl (UINT4 *pu4ObjectId, UINT4 u4OidLen,
                                          INT4 i4Index, INT4 VlanId, INT4 i4SetVal);
VOID
LldpUtilIndMsrForDot1ConfigProtoVlanTbl (UINT4 *pu4ObjectId, UINT4 u4OidLen,
                                          INT4 i4Index, INT4 VlanId, INT4 i4SetVal);
VOID
LldpUtilIndMsrForDot3PortConfigTbl (UINT4 *pu4ObjectId, UINT4 u4OidLen,
                                          INT4 i4Index, tSNMP_OCTET_STRING_TYPE *pSetVal);
INT4
LldpUtlGetMessageTxHoldMultiplier (INT4 *pi4RetValLldpMessageTxHoldMultiplier);
INT4
LldpUtlGetReinitDelay (INT4 *pi4RetValLldpReinitDelay);
INT4
LldpUtlGetTxDelay(INT4 *pi4RetValLldpTxDelay);
INT4
LldpUtlGetNotificationInterval(INT4 *pi4RetValLldpNotificationInterval);
INT4
LldpUtlSetMessageTxInterval(INT4 i4SetValLldpMessageTxInterval);
INT4
LldpUtlSetMessageTxHoldMultiplier (INT4 i4SetValLldpMessageTxHoldMultiplier);
INT4
LldpUtlSetReinitDelay(INT4 i4SetValLldpReinitDelay);
INT4
LldpUtlSetTxDelay (INT4 i4SetValLldpTxDelay);
INT4
LldpUtlSetNotificationInterval(INT4 i4SetValLldpNotificationInterval);
INT4
LldpUtlTestMessageTxInterval (UINT4 *pu4ErrorCode,
                              INT4 i4TestValLldpMessageTxInterval);
INT4
LldpUtlTestMessageTxHoldMultiplier (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValLldpMessageTxHoldMultiplier);
INT4
LldpUtlTestReinitDelay (UINT4 *pu4ErrorCode, INT4 i4TestValLldpReinitDelay);
INT4
LldpUtlTestTxDelay(UINT4 *pu4ErrorCode, INT4 i4TestValLldpTxDelay);
INT4
LldpUtlTestNotificationInterval(UINT4 *pu4ErrorCode,
                                INT4 i4TestValLldpNotificationInterval);
INT4
LldpUtlValidateIndexInstanceLldpPortConfigTable(INT4 i4LldpPortConfigPortNum, tMacAddr DestMacAddr);
INT4
LldpUtlGetFirstIndexLldpPortConfigTable (INT4 *pi4LldpPortConfigPortNum);
INT4
LldpUtlGetNextIndexLldpPortConfigTable (INT4 i4LldpPortConfigPortNum,
                                        INT4 *pi4NextLldpPortConfigPortNum);
INT4
LldpUtlValidateIndexInstanceConfigManAddrTable(INT4 i4LldpLocManAddrSubtype,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pLldpLocManAddr);
INT4
LldpUtlGetFirstIndexLldpConfigManAddrTable (INT4 *pi4LldpLocManAddrSubtype,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pLldpLocManAddr);
INT4
LldpUtlGetNextIndexLldpConfigManAddrTable (INT4 i4LldpLocManAddrSubtype,
                                           INT4 *pi4NextLldpLocManAddrSubtype,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pLldpLocManAddr,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pNextLldpLocManAddr);
INT4
LldpUtlValidateIndexInstanceLldpStatsTxPortTable (INT4 i4LldpStatsTxPortNum,
                                                  tMacAddr DestMacAddr);
INT4
LldpUtlGetFirstIndexLldpStatsTxPortTable (INT4 *pi4LldpStatsTxPortNum);
INT4
LldpUtlGetNextLldpStatsTxPortTable (INT4 i4LldpStatsTxPortNum,
                                    INT4 *pi4NextLldpStatsTxPortNum);
INT4
LldpUtlValidateIndexInstanceLldpStatsRxPortTable(INT4 i4LldpStatsRxPortNum, tMacAddr DestMacAddr);
INT4
LldpUtlGetFirstIndexStatsRxPortTable (INT4 *pi4LldpStatsRxPortNum);
INT4
LldpUtlGetNextLldpStatsRxPortTable (INT4 i4LldpStatsRxPortNum,
                                    INT4 *pi4NextLldpStatsRxPortNum);
INT4
LldpUtlValidateIndexInstanceLldpLocPortTable(INT4 i4LldpLocPortNum);
INT4
LldpUtlGetFirstIndexLldpLocPortTable (INT4 *pi4LldpLocPortNum);
INT4
LldpUtlGetNextIndexLldpLocPortTable (INT4 i4LldpLocPortNum,
                                     INT4 *pi4NextLldpLocPortNum);
INT4
LldpUtlValidateIndexInstanceLldpRemTable (UINT4 u4LldpRemTimeMark,
                                          INT4 i4LldpRemLocalPortNum,
                                          INT4 i4LldpRemIndex,
                                          UINT4 u4DestMacAddr);
INT4
LldpUtlGetFirstIndexLldpRemTable (UINT4 *pu4LldpRemTimeMark,
                                  INT4 *pi4LldpRemLocalPortNum,
                                  INT4 *pi4LldpRemIndex);
INT4
LldpUtlGetNextIndexLldpRemTable (UINT4 u4LldpRemTimeMark,
                                 UINT4 *pu4NextLldpRemTimeMark,
                                 INT4 i4LldpRemLocalPortNum,
         INT4 *pi4NextLldpRemLocalPortNum,
         UINT4 u4DestIndex, UINT4 *pu4NextDestIndex,
                                 INT4 i4LldpRemIndex, INT4 *pi4NextLldpRemIndex);
INT4
LldpUtlValidateIndexInstanceLldpRemManAddrTable (UINT4 u4LldpRemTimeMark,
                                                 INT4 i4LldpRemLocalPortNum,
                                                 INT4 i4LldpRemIndex,
                                                 INT4 i4LldpRemManAddrSubtype,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pLldpRemManAddr,
                                                 UINT4 u4DestMacAddr);
INT4
LldpUtlGetFirstIndexLldpRemManAddrTable(UINT4 *pu4LldpRemTimeMark,
                                        INT4 *pi4LldpRemLocalPortNum,
                                        INT4 *pi4LldpRemIndex,
                                        INT4 *pi4LldpRemManAddrSubtype,
                                        tSNMP_OCTET_STRING_TYPE * pLldpRemManAddr);
INT4
LldpUtlGetNextIndexRemManAddrTable (UINT4 u4LldpRemTimeMark,
                                    UINT4 *pu4NextLldpRemTimeMark,
                                    INT4 i4LldpRemLocalPortNum,
                                    INT4 *pi4NextLldpRemLocalPortNum,
                                    UINT4 u4DestIndex, UINT4 *pNextDestIndex,
         INT4 i4LldpRemIndex,
                                    INT4 *pi4NextLldpRemIndex,
                                    INT4 i4LldpRemManAddrSubtype,
                                    INT4 *pi4NextLldpRemManAddrSubtype,
                                    tSNMP_OCTET_STRING_TYPE * pLldpRemManAddr,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextLldpRemManAddr);
INT4
LldpUtlValidateIndexInstanceRemUnknownTLVTable (UINT4 u4LldpRemTimeMark,
                                                INT4 i4LldpRemLocalPortNum,
                                                INT4 i4LldpRemIndex,
                                                INT4 i4LldpRemUnknownTLVType,
                                                UINT4 u4DestMacAddr);
INT4
LldpUtlGetFirstIndexLldpRemUnknownTLVTable(UINT4 *pu4LldpRemTimeMark,
                                           INT4 *pi4LldpRemLocalPortNum,
                                           INT4 *pi4LldpRemIndex,
                                           INT4 *pi4LldpRemUnknownTLVType);
INT4
LldpUtlGetNextIndexRemUnknownTLVTable(UINT4 u4LldpRemTimeMark,
                                      UINT4 *pu4NextLldpRemTimeMark,
                                      INT4 i4LldpRemLocalPortNum,
                                      INT4 *pi4NextLldpRemLocalPortNum,
           UINT4 u4DestIndex,
           UINT4 *pu4DestIndex,
                                      INT4 i4LldpRemIndex,
                                      INT4 *pi4NextLldpRemIndex,
                                      INT4 i4LldpRemUnknownTLVType,
                                      INT4 *pi4NextLldpRemUnknownTLVType);
INT4
LldpUtlValidateIndexInstanceRemOrgDefInfoTable(UINT4 u4LldpRemTimeMark,
                                               INT4 i4LldpRemLocalPortNum,
                                               INT4 i4LldpRemIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pLldpRemOrgDefInfoOUI,
                                               INT4 i4LldpRemOrgDefInfoSubtype,
                                               INT4 i4LldpRemOrgDefInfoIndex,
                                               UINT4 u4DestMacAddr);
INT4
LldpUtlGetFirstIndexLldpRemOrgDefInfoTable(UINT4 *pu4LldpRemTimeMark,
                                           INT4 *pi4LldpRemLocalPortNum,
                                           INT4 *pi4LldpRemIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pLldpRemOrgDefInfoOUI,
                                           INT4 *pi4LldpRemOrgDefInfoSubtype,
                                           INT4 *pi4LldpRemOrgDefInfoIndex);
INT4
LldpUtlGetNextIndexRemOrgDefInfoTable(UINT4 u4LldpRemTimeMark,
                                       UINT4 *pu4NextLldpRemTimeMark,
                                       INT4 i4LldpRemLocalPortNum,
                                       INT4 *pi4NextLldpRemLocalPortNum,
                                       UINT4 u4DestINdex,
            UINT4 *pu4NextDestIndex,
                                       INT4 i4LldpRemIndex,
                                       INT4 *pi4NextLldpRemIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pLldpRemOrgDefInfoOUI,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextLldpRemOrgDefInfoOUI,
                                       INT4 i4LldpRemOrgDefInfoSubtype,
                                       INT4 *pi4NextLldpRemOrgDefInfoSubtype,
                                       INT4 i4LldpRemOrgDefInfoIndex,
                                       INT4 *pi4NextLldpRemOrgDefInfoIndex);

INT4
LldpUtlValidateIndexInstanceLldpXdot1ConfigPortVlanTable(INT4 i4LldpPortConfigPortNum, 
                                                         tMacAddr DestMacAddr);

INT4
LldpUtlGetFirstIndexLldpXdot1ConfigPortVlanTable(INT4 *pi4LldpPortConfigPortNum);

INT4
LldpUtlGetNextIndexLldpXdot1ConfigPortVlanTable(INT4 i4LldpPortConfigPortNum,
                                                INT4 *pi4NextLldpPortConfigPortNum);

INT4
LldpUtlGetLldpXdot1ConfigPortVlanTxEnable (INT4 i4LldpPortConfigPortNum, tMacAddr DestMacAddr,
                                           INT4 *pi4RetValLldpXdot1ConfigPortVlanTxEnable);

INT4
LldpUtlSetLldpXdot1ConfigPortVlanTxEnable (INT4 i4LldpPortConfigPortNum, tMacAddr DestMacAddr,
                                           INT4 i4SetValLldpXdot1ConfigPortVlanTxEnable);

INT4
LldpUtlTestLldpXdot1ConfigPortVlanTxEnable(UINT4 *pu4ErrorCode,INT4 i4LldpPortConfigPortNum,
                                           INT4 i4TestValLldpXdot1ConfigPortVlanTxEnable,
                                           tMacAddr DestMacAddr);

INT4
LldpUtlValidateIndexInstanceLldpXdot1LocVlanNameTable(INT4 i4LldpLocPortNum,
                                                      INT4 i4LldpXdot1LocVlanId);

INT4
LldpUtlGetFirstIndexLldpXdot1LocVlanNameTable(INT4 *pi4LldpLocPortNum,
                                              INT4 *pi4LldpXdot1LocVlanId);

INT4
LldpUtlGetNextIndexLldpXdot1LocVlanNameTable (INT4 i4LldpLocPortNum,
                                              INT4 *pi4NextLldpLocPortNum,
                                              INT4 i4LldpXdot1LocVlanId,
                                              INT4 *pi4NextLldpXdot1LocVlanId);

INT4
LldpUtlGetLldpXdot1LocVlanName (INT4 i4LldpLocPortNum,
                                INT4 i4LldpXdot1LocVlanId,
                                tSNMP_OCTET_STRING_TYPE * pRetValLldpXdot1LocVlanName);

INT4
LldpUtlValidateIndexInstanceLldpXdot1ConfigVlanNameTable (INT4 i4LldpLocPortNum,
                                                          INT4 i4LldpXdot1LocVlanId);

INT4
LldpUtlGetFirstIndexLldpXdot1ConfigVlanNameTable(INT4 *pi4LldpLocPortNum,
                                                 INT4 *pi4LldpXdot1LocVlanId);

INT4
LldpUtlGetNextIndexLldpXdot1ConfigVlanNameTable(INT4 i4LldpLocPortNum,
                                                INT4 *pi4NextLldpLocPortNum,
                                                INT4 i4LldpXdot1LocVlanId,
                                                INT4 *pi4NextLldpXdot1LocVlanId);

INT4
LldpUtlGetLldpXdot1ConfigVlanNameTxEnable (INT4 i4LldpLocPortNum,
                                           INT4 i4LldpXdot1LocVlanId,
                                           INT4 *pi4RetValLldpXdot1ConfigVlanNameTxEnable);

INT4
LldpUtlSetLldpXdot1ConfigVlanNameTxEnable(INT4 i4LldpLocPortNum,                                                                                            INT4 i4LldpXdot1LocVlanId,                                                                                        INT4 i4SetValLldpXdot1ConfigVlanNameTxEnable);

INT4
LldpUtlDisableAllLldpXdot1ConfigVlanNameTxEnable (INT4 i4IfIndex,
     INT4 i4SetValLldpXdot1ConfigVlanNameTxEnable);

INT4
LldpUtlTestLldpXdot1ConfigVlanNameTxEnable (UINT4 *pu4ErrorCode,
                                            INT4 i4LldpLocPortNum,
                                            INT4 i4LldpXdot1LocVlanId,
                                            INT4 i4TestValLldpXdot1ConfigVlanNameTxEnable);

INT4
LldpUtlValidateIndexInstanceLldpXdot1LocProtoVlanTable (INT4 i4LldpLocPortNum,
                                                        INT4 i4LldpXdot1LocProtoVlanId);

INT4
LldpUtlGetFirstIndexLldpXdot1LocProtoVlanTable (INT4 *pi4LldpLocPortNum,
                                             INT4 *pi4LldpXdot1LocProtoVlanId);

INT4
LldpUtlGetNextIndexLldpXdot1LocProtoVlanTable (INT4 i4LldpLocPortNum,
                                            INT4 *pi4NextLldpLocPortNum,
                                            INT4 i4LldpXdot1LocProtoVlanId,
                                            INT4 *pi4NextLldpXdot1LocProtoVlanId);

INT4
LldpUtlGetLldpXdot1LocProtoVlanSupported (INT4 i4LldpLocPortNum,
                                       INT4 i4LldpXdot1LocProtoVlanId,
                                       INT4 *pi4RetValLldpXdot1LocProtoVlanSupported);
INT4
LldpUtlGetLldpXdot1LocProtoVlanEnabled (INT4 i4LldpLocPortNum,
                                     INT4 i4LldpXdot1LocProtoVlanId,
                                     INT4 *pi4RetValLldpXdot1LocProtoVlanEnabled);

INT4
LldpUtlValidateIndexInstanceLldpXdot1ConfigProtoVlanTable(INT4 i4LldpLocPortNum,
                                                          INT4 i4LldpXdot1LocProtoVlanId);

INT4
LldpUtlGetFirstIndexLldpXdot1ConfigProtoVlanTable (INT4 *pi4LldpLocPortNum,
                                                   INT4 *pi4LldpXdot1LocProtoVlanId);

INT4
LldpUtlGetNextIndexLldpXdot1ConfigProtoVlanTable (INT4 i4LldpLocPortNum,
                                                  INT4 *pi4NextLldpLocPortNum,
                                                  INT4 i4LldpXdot1LocProtoVlanId,
                                                  INT4 *pi4NextLldpXdot1LocProtoVlanId);

INT4
LldpUtlGetLldpXdot1ConfigProtoVlanTxEnable (INT4 i4LldpLocPortNum,
                                            INT4 i4LldpXdot1LocProtoVlanId,
                                            INT4 *pi4RetValLldpXdot1ConfigProtoVlanTxEnable);

INT4
LldpUtlSetLldpXdot1ConfigProtoVlanTxEnable (INT4 i4LldpLocPortNum,
                                            INT4 i4LldpXdot1LocProtoVlanId,
                                            INT4 i4SetValLldpXdot1ConfigProtoVlanTxEnable);

INT4
LldpUtlTestv2LldpXdot1ConfigProtoVlanTxEnable (UINT4 *pu4ErrorCode,
                                               INT4 i4LldpLocPortNum,
                                               INT4 i4LldpXdot1LocProtoVlanId,
                                               INT4 i4TestValLldpXdot1ConfigProtoVlanTxEnable);

INT4
LldpUtlValidateIndexInstanceLldpXdot1LocTable (INT4 i4LldpLocPortNum);

INT4
LldpUtlGetFirstIndexLldpXdot1LocTable (INT4 *pi4LldpLocPortNum);

INT4
LldpUtlGetNextIndexLldpXdot1LocTable (INT4 i4LldpLocPortNum,
                                      INT4 *pi4NextLldpLocPortNum);

INT4
LldpUtlGetLldpXdot1LocPortVlanId (INT4 i4LldpLocPortNum,
                                  INT4 *pi4RetValLldpXdot1LocPortVlanId);

INT4
LldpUtlValidateIndexInstanceLldpXdot1RemTable (UINT4 u4LldpRemTimeMark,
                                               INT4 i4LldpRemLocalPortNum,
                                               INT4 i4LldpRemIndex,
                                      UINT4 u4DestMacAddr);

INT4
LldpUtlGetFirstIndexLldpXdot1RemTable (UINT4 *pu4LldpRemTimeMark,
                                       INT4 *pi4LldpRemLocalPortNum,
                                       INT4 *pi4LldpRemIndex);

INT4
LldpUtlGetNextIndexLldpXdot1RemTable (UINT4 u4LldpRemTimeMark,
                                      UINT4 *pu4NextLldpRemTimeMark,
                                      INT4 i4LldpRemLocalPortNum,
                                      INT4 *pi4NextLldpRemLocalPortNum,
                                      INT4 i4LldpRemIndex, INT4 *pi4NextLldpRemIndex,
                                      UINT4 u4DestMacAddr);

INT4
LldpUtlGetLldpXdot1RemPortVlanId (UINT4 u4LldpRemTimeMark,
                                  INT4 i4LldpRemLocalPortNum,
                                  INT4 i4LldpRemIndex,
                                  INT4 *pi4RetValLldpXdot1RemPortVlanId,
                                  UINT4 u4DestMacAddr);

INT4
LldpUtlValidateIndexInstanceLldpXdot1RemProtoVlanTable (UINT4 u4LldpRemTimeMark,
                                                        INT4 i4LldpRemLocalPortNum,
                                                        INT4 i4LldpRemIndex,
                                                        INT4 i4LldpXdot1RemProtoVlanId,
                                                        UINT4 u4DestMacAddr);

INT4
LldpUtlGetFirstIndexLldpXdot1RemProtoVlanTable (UINT4 *pu4LldpRemTimeMark,
                                                INT4 *pi4LldpRemLocalPortNum,
                                                INT4 *pi4LldpRemIndex,
                                                INT4 *pi4LldpXdot1RemProtoVlanId);

INT4
LldpUtlGetNextIndexLldpXdot1RemProtoVlanTable (UINT4 u4LldpRemTimeMark,
                                               UINT4 *pu4NextLldpRemTimeMark,
                                               INT4 i4LldpRemLocalPortNum,
                                               INT4 *pi4NextLldpRemLocalPortNum,
                                               INT4 i4LldpRemIndex,
                                               INT4 *pi4NextLldpRemIndex,
                                               INT4 i4LldpXdot1RemProtoVlanId,
                                               INT4 *pi4NextLldpXdot1RemProtoVlanId,
                                               UINT4 u4DestMacAddr);

INT4
LldpUtlGetLldpXdot1RemProtoVlanSupported (UINT4 u4LldpRemTimeMark,
                                          INT4 i4LldpRemLocalPortNum,
                                          INT4 i4LldpRemIndex,
                                          INT4 i4LldpXdot1RemProtoVlanId,
                                          INT4 *pi4RetValLldpXdot1RemProtoVlanSupported,
                                          UINT4 u4DestMacAddr);

INT4
LldpUtlGetLldpXdot1RemProtoVlanEnabled (UINT4 u4LldpRemTimeMark,
                                        INT4 i4LldpRemLocalPortNum,
                                        INT4 i4LldpRemIndex,
                                        INT4 i4LldpXdot1RemProtoVlanId,
                                        INT4 *pi4RetValLldpXdot1RemProtoVlanEnabled,
                                        UINT4 u4DestMacAddr);

INT4
LldpUtlValidateIndexInstanceLldpXdot1RemVlanNameTable (UINT4 u4LldpRemTimeMark,
                                                       INT4 i4LldpRemLocalPortNum,
                                                       INT4 i4LldpRemIndex,
                                                       INT4 i4LldpXdot1RemVlanId,
                                                       UINT4 u4DestMacAddr);

INT4
LldpUtlGetFirstIndexLldpXdot1RemVlanNameTable (UINT4 *pu4LldpRemTimeMark,
                                               INT4 *pi4LldpRemLocalPortNum,
                                               INT4 *pi4LldpRemIndex,
                                               INT4 *pi4LldpXdot1RemVlanId);

INT4
LldpUtlGetNextIndexLldpXdot1RemVlanNameTable (UINT4 u4LldpRemTimeMark,
                                              UINT4 *pu4NextLldpRemTimeMark,
                                              INT4 i4LldpRemLocalPortNum,
                                              INT4 *pi4NextLldpRemLocalPortNum,
                                              INT4 i4LldpRemIndex,
                                              INT4 *pi4NextLldpRemIndex,
                                              INT4 i4LldpXdot1RemVlanId,
                                              INT4 *pi4NextLldpXdot1RemVlanId,
                                              UINT4 u4DestMacAddr);

INT4
LldpUtlGetLldpXdot1RemVlanName (UINT4 u4LldpRemTimeMark,
                                INT4 i4LldpRemLocalPortNum,
                                INT4 i4LldpRemIndex,
                                INT4 i4LldpXdot1RemVlanId,
                                tSNMP_OCTET_STRING_TYPE
                                * pRetValLldpXdot1RemVlanName,
                                UINT4 u4DestMacAddr);

INT4
LldpUtlValidateIndexInstanceLldpXdot1RemProtocolTable (UINT4 u4LldpRemTimeMark,
                                                       INT4 i4LldpRemLocalPortNum,
                                                       INT4 i4LldpRemIndex,
                                                       INT4 i4LldpXdot1RemProtocolIndex,
                                                       UINT4 u4DestMacAddr);

INT4
LldpUtlGetFirstIndexLldpXdot1RemProtocolTable (UINT4 *pu4LldpRemTimeMark,
                                               INT4 *pi4LldpRemLocalPortNum,
                                               INT4 *pi4LldpRemIndex,
                                               INT4 *pi4LldpXdot1RemProtocolIndex);

INT4
LldpUtlGetNextIndexLldpXdot1RemProtocolTable (UINT4 u4LldpRemTimeMark,
                                              UINT4 *pu4NextLldpRemTimeMark,
                                              INT4 i4LldpRemLocalPortNum,
                                              INT4 *pi4NextLldpRemLocalPortNum,
                                              INT4 i4LldpRemIndex,
                                              INT4 *pi4NextLldpRemIndex,
                                              INT4 i4LldpXdot1RemProtocolIndex,
                                              INT4 *pi4NextLldpXdot1RemProtocolIndex,
                                                       UINT4 u4DestMacAddr);

INT4
LldpUtlGetLldpXdot1RemProtocolId (UINT4 u4LldpRemTimeMark,
                                  INT4 i4LldpRemLocalPortNum,
                                  INT4 i4LldpRemIndex,
                                  INT4 i4LldpXdot1RemProtocolIndex,
                                  tSNMP_OCTET_STRING_TYPE
                                  * pRetValLldpXdot1RemProtocolId,
                                                       UINT4 u4DestMacAddr);

INT4
LldpUtlValidateIndexInstanceLldpXdot3PortConfigTable(INT4 i4LldpPortConfigPortNum,
                                                     tMacAddr DestMacAddr);

INT4
LldpUtlGetFirstIndexLldpXdot3PortConfigTable (INT4 *pi4LldpPortConfigPortNum);

INT4
LldpUtlGetNextIndexLldpXdot3PortConfigTable (INT4 i4LldpPortConfigPortNum,
                                             INT4 *pi4NextLldpPortConfigPortNum);

INT4
LldpUtlGetLldpXdot3PortConfigTLVsTxEnable (INT4 i4LldpPortConfigPortNum,
                                           tSNMP_OCTET_STRING_TYPE
                                           * pRetValLldpXdot3PortConfigTLVsTxEnable,
                                           tMacAddr DestMacAddr);

INT4
LldpUtlSetLldpXdot3PortConfigTLVsTxEnable (INT4 i4LldpPortConfigPortNum,
                                           tSNMP_OCTET_STRING_TYPE
                                           * pSetValLldpXdot3PortConfigTLVsTxEnable,
                                           tMacAddr DestMacAddr);

INT4
LldpUtlTestLldpXdot3PortConfigTLVsTxEnable (UINT4 *pu4ErrorCode,
                                            INT4 i4LldpPortConfigPortNum,
                                            tSNMP_OCTET_STRING_TYPE
                                            * pTestValLldpXdot3PortConfigTLVsTxEnable,
                                            tMacAddr DestMacAddr);

INT4
LldpUtlValidateIndexInstanceLldpXdot3LocPortTable (INT4 i4LldpLocPortNum);

INT4
LldpUtlGetFirstIndexLldpXdot3LocPortTable (INT4 *pi4LldpLocPortNum);

INT4
LldpUtlGetNextIndexLldpXdot3LocPortTable (INT4 i4LldpLocPortNum,
                                          INT4 *pi4NextLldpLocPortNum);

INT4
LldpUtlGetLldpXdot3LocPortAutoNegSupported (INT4 i4LldpLocPortNum,
                                            INT4 *pi4RetValLldpXdot3LocPortAutoNegSupported);

INT4
LldpUtlGetLldpXdot3LocPortAutoNegEnabled (INT4 i4LldpLocPortNum,
                                          INT4 *pi4RetValLldpXdot3LocPortAutoNegEnabled);

INT4
LldpUtlGetLldpXdot3LocPortAutoNegAdvertisedCap (INT4 i4LldpLocPortNum,
                                                tSNMP_OCTET_STRING_TYPE
                                                * pRetValLldpXdot3LocPortAutoNegAdvertisedCap);

INT4
LldpUtlGetLldpXdot3LocPortOperMauType (INT4 i4LldpLocPortNum,
                                       INT4 *pi4RetValLldpXdot3LocPortOperMauType);

INT4
LldpUtlValidateIndexInstanceLldpXdot3LocLinkAggTable (INT4 i4LldpLocPortNum);

INT4
LldpUtlGetFirstIndexLldpXdot3LocLinkAggTable (INT4 *pi4LldpLocPortNum);

INT4
LldpUtlGetNextIndexLldpXdot3LocLinkAggTable (INT4 i4LldpLocPortNum,
                                             INT4 *pi4NextLldpLocPortNum);

INT4
LldpUtlGetLldpXdot3LocLinkAggStatus (INT4 i4LldpLocPortNum,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pRetValLldpXdot3LocLinkAggStatus);

INT4
LldpUtlGetLldpXdot3LocLinkAggPortId (INT4 i4LldpLocPortNum,
                                     INT4 *pi4RetValLldpXdot3LocLinkAggPortId);

INT4
LldpUtlValidateIndexInstanceLldpXdot3LocMaxFrameSizeTable (INT4 i4LldpLocPortNum);

INT4
LldpUtlGetFirstIndexLldpXdot3LocMaxFrameSizeTable (INT4 *pi4LldpLocPortNum);

INT4
LldpUtlGetNextIndexLldpXdot3LocMaxFrameSizeTable(INT4 i4LldpLocPortNum,
                                                 INT4 *pi4NextLldpLocPortNum);

INT4
LldpUtlGetLldpXdot3LocMaxFrameSize (INT4 i4LldpLocPortNum,
                                    INT4 *pi4RetValLldpXdot3LocMaxFrameSize);

INT4
LldpUtlValidateIndexInstanceLldpXdot3RemPortTable (UINT4 u4LldpRemTimeMark,
                                                   INT4 i4LldpRemLocalPortNum,
                                                   INT4 i4LldpRemIndex,
                                                   UINT4 u4DestMacAddr);

INT4
LldpUtlGetFirstIndexLldpXdot3RemPortTable (UINT4 *pu4LldpRemTimeMark,
                                           INT4 *pi4LldpRemLocalPortNum,
                                           INT4 *pi4LldpRemIndex);

INT4
LldpUtlGetNextIndexLldpXdot3RemPortTable(UINT4 u4LldpRemTimeMark,
                                         UINT4 *pu4NextLldpRemTimeMark,
                                         INT4 i4LldpRemLocalPortNum,
                                         INT4 *pi4NextLldpRemLocalPortNum,
                                         INT4 i4LldpRemIndex,
                                         INT4 *pi4NextLldpRemIndex,
                                         tMacAddr DestMacAddr);

INT4
LldpUtlGetLldpXdot3RemPortAutoNegSupported (UINT4 u4LldpRemTimeMark,
                                            INT4 i4LldpRemLocalPortNum,
                                            INT4 i4LldpRemIndex,
                                            INT4 *pi4RetValLldpXdot3RemPortAutoNegSupported,
                                            UINT4 u4DestMacAddr);

INT4
LldpUtlGetLldpXdot3RemPortAutoNegEnabled (UINT4 u4LldpRemTimeMark,
                                          INT4 i4LldpRemLocalPortNum,
                                          INT4 i4LldpRemIndex,
                                          INT4 *pi4RetValLldpXdot3RemPortAutoNegEnabled,
                                          UINT4 u4DestMacAddr);

INT4
LldpUtlGetLldpXdot3RemPortAutoNegAdvertisedCap(UINT4 u4LldpRemTimeMark,
                                               INT4 i4LldpRemLocalPortNum,
                                               INT4 i4LldpRemIndex,
                                               tSNMP_OCTET_STRING_TYPE
                                               * pRetValLldpXdot3RemPortAutoNegAdvertisedCap,
                                               UINT4 u4DestMacAddr);
INT4
LldpUtlGetLldpXdot3RemPortOperMauType (UINT4 u4LldpRemTimeMark,
                                       INT4 i4LldpRemLocalPortNum,
                                       INT4 i4LldpRemIndex,
                                       INT4 *pi4RetValLldpXdot3RemPortOperMauType,
                                       UINT4 u4DestMacAddr);

INT4
LldpUtlValidateIndexInstanceLldpXdot3RemPowerTable (UINT4 u4LldpRemTimeMark,
                                                    INT4 i4LldpRemLocalPortNum,
                                                    INT4 i4LldpRemIndex,
                                                    UINT4 u4DestMacAddr);

INT4
LldpUtlValidateIndexInstanceLldpXdot3RemLinkAggTable (UINT4 u4LldpRemTimeMark,
                                                      INT4 i4LldpRemLocalPortNum,
                                                      INT4 i4LldpRemIndex,
                                                      UINT4 u4DestMacAddr);

INT4
LldpUtlGetFirstIndexLldpXdot3RemLinkAggTable (UINT4 *pu4LldpRemTimeMark,
                                              INT4 *pi4LldpRemLocalPortNum,
                                              INT4 *pi4LldpRemIndex);

INT4
LldpUtlGetNextIndexLldpXdot3RemLinkAggTable (UINT4 u4LldpRemTimeMark,
                                             UINT4 *pu4NextLldpRemTimeMark,
                                             INT4 i4LldpRemLocalPortNum,
                                             INT4 *pi4NextLldpRemLocalPortNum,
                                             INT4 i4LldpRemIndex,
                                             INT4 *pi4NextLldpRemIndex,
                                             tMacAddr DestMacAddr);

INT4
LldpUtlGetLldpXdot3RemLinkAggStatus (UINT4 u4LldpRemTimeMark,
                                     INT4 i4LldpRemLocalPortNum,
                                     INT4 i4LldpRemIndex,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pRetValLldpXdot3RemLinkAggStatus,
                                     UINT4 u4DestMacAddr);

INT4
LldpUtlGetLldpXdot3RemLinkAggPortId(UINT4 u4LldpRemTimeMark,
                                    INT4 i4LldpRemLocalPortNum,
                                    INT4 i4LldpRemIndex,
                                    INT4 *pi4RetValLldpXdot3RemLinkAggPortId,
                                    UINT4 u4DestMacAddr);

INT4 LldpUtlValidateIndexInstanceLldpXdot3RemMaxFrameSizeTable(UINT4
        u4LldpRemTimeMark, INT4 i4LldpRemLocalPortNum, INT4 i4LldpRemIndex,
        UINT4 u4DestMacAddr);

INT4
LldpUtlGetFirstIndexLldpXdot3RemMaxFrameSizeTable (UINT4 *pu4LldpRemTimeMark,
                                                   INT4 *pi4LldpRemLocalPortNum,
                                                   INT4 *pi4LldpRemIndex);

INT4
LldpUtlGetNextIndexLldpXdot3RemMaxFrameSizeTable (UINT4 u4LldpRemTimeMark,
                                                  UINT4 *pu4NextLldpRemTimeMark,
                                                  INT4 i4LldpRemLocalPortNum,
                                                  INT4 *pi4NextLldpRemLocalPortNum,
                                                  INT4 i4LldpRemIndex,
                                                  INT4 *pi4NextLldpRemIndex,
                                                  tMacAddr DestMacAddr);

INT4
LldpUtlGetLldpXdot3RemMaxFrameSize (UINT4 u4LldpRemTimeMark,
                                    INT4 i4LldpRemLocalPortNum,
                                    INT4 i4LldpRemIndex,
                                    INT4 *pi4RetValLldpXdot3RemMaxFrameSize,
                                    UINT4 u4DestMacAddr);

INT4
LldpUtlGetDestMacTblIndex (tMacAddr DestMacAddr, UINT4 *pu4DestMacIndex);

INT4
LldpDeleteDestMacAddrEntry (INT4 i4IfIndex, tMacAddr DestMacAddr);

INT4
LldpCreateDestMacAddressEnty (INT4 i4IfIndex, tMacAddr DestMacAddr);

INT4
LldpUtilGetDestMacAddrTblIndex (UINT4 u4LocPortNum, UINT4 *pu4DestMacAddrTblIndex);

PUBLIC VOID
LldpUtilCreateDestMacAddrTable (VOID);
PUBLIC UINT1 LldpUtlCheckDestMacAddrIndexUsage PROTO ((UINT4 u4Index));

PUBLIC INT4
LldpUtlGetDupVlanNameCount (UINT1 *pu1Lldpdu, UINT2 u2LldpduLen, 
                                                UINT4 *pu4DupVlanNameCount);

PUBLIC INT4
LldpUtlGetDupMgmtAddrCount (tLldpLocPortInfo *pLocPortInfo,INT4 *pi4DupMgmtAddrCount);

PUBLIC INT4
LldpUtlGetVidUsageDigestTlvCnt (UINT1 *pu1Lldpdu, UINT2 u2LldpduLen,
                                                UINT4 *pu4VidDigestTlvCount);

PUBLIC INT4
LldpUtlGetMgmtVidTlvCnt (UINT1 *pu1Lldpdu, UINT2 u2LldpduLen,
                                                   UINT4 *pu4MgmtVidTlvCount);

PUBLIC INT4
LldpTxUtlValidateInterfaceIndex PROTO ((INT4 i4LldpPortConfigPortNum));

PUBLIC INT4 
LldpMedUtlValidateIndexPortConfTbl             PROTO ((INT4 
        i4LldpV2PortConfigIfIndex, 
        tMacAddr DestMacAddr));

PUBLIC INT4 
LldpMedUtlGetFirstIndexPortConfTbl             PROTO ((INT4
        *pi4LldpV2PortConfigIfIndex, UINT4
        *pu4LldpV2PortConfigDestAddressIndex));

PUBLIC INT4 
LldpMedUtlGetNextIndexPortConfTbl              PROTO ((INT4
        i4LldpV2PortConfigIfIndex, INT4 *pi4NextLldpV2PortConfigIfIndex,
        UINT4 u4LldpV2PortConfigDestAddressIndex, UINT4 *pu4NextLldpV2PortConfigDestAddressIndex ));


PUBLIC INT4 
LldpMedUtlGetLocPortCapSupported               PROTO ((INT4 
        i4LldpV2PortConfigIfIndex,
        tMacAddr DestMacAddr, tSNMP_OCTET_STRING_TYPE *
        pRetValFsLldpXMedPortCapSupported));


PUBLIC INT4 
LldpMedUtlGetLldpPortConfigTLVsTxEnable        PROTO ((INT4
        i4LldpV2PortConfigIfIndex, tMacAddr DestMacAddr,
        tSNMP_OCTET_STRING_TYPE * pRetValFsLldpXMedPortConfigTLVsTxEnable));


PUBLIC INT4 
LldpMedUtlSetPortConfigTLVsTxEnable            PROTO ((INT4
        i4LldpV2PortConfigIfIndex, tMacAddr DestMacAddr,
        tSNMP_OCTET_STRING_TYPE * pSetValFsLldpXMedPortConfigTLVsTxEnable));

PUBLIC INT4 
LldpMedUtlTestPortConfigTLVsTxEnable           PROTO ((UINT4 
        *pu4ErrorCode, INT4
        i4LldpV2PortConfigIfIndex, tMacAddr DestMacAddr,
        tSNMP_OCTET_STRING_TYPE * pTestValFsLldpXMedPortConfigTLVsTxEnable));


PUBLIC INT4 
LldpMedUtlValIndexLocNwPolTbl                  PROTO ((INT4
        i4LldpV2LocPortIfIndex, tSNMP_OCTET_STRING_TYPE
        *pFsLldpMedLocMediaPolicyAppType));


PUBLIC INT4 
LldpMedUtlGetFirstIndexLocNwPolTbl             PROTO ((INT4
        *pi4LldpV2LocPortIfIndex, tSNMP_OCTET_STRING_TYPE *
        pFsLldpMedLocMediaPolicyAppType));

PUBLIC INT4 
LldpMedUtlGetNextIndexLocNwPolTbl              PROTO ((INT4
        i4LldpV2LocPortIfIndex, INT4 *pi4NextLldpV2LocPortIfIndex,
        tSNMP_OCTET_STRING_TYPE *pFsLldpMedLocMediaPolicyAppType,
        tSNMP_OCTET_STRING_TYPE *pNextFsLldpMedLocMediaPolicyAppType));


PUBLIC INT4 
LldpMedUtlGetLocNwPolVlanID                    PROTO ((INT4 
        i4LldpV2LocPortIfIndex,
        tSNMP_OCTET_STRING_TYPE *pFsLldpMedLocMediaPolicyAppType, INT4
        *pi4RetValFsLldpMedLocMediaPolicyVlanID));

PUBLIC INT4 
LldpMedUtlGetLocNwPolPriority                  PROTO ((INT4 
        i4LldpV2LocPortIfIndex,
        tSNMP_OCTET_STRING_TYPE *pFsLldpMedLocMediaPolicyAppType, INT4 *
        pi4RetValFsLldpMedLocMediaPolicyPriority));

PUBLIC INT4 
LldpMedUtlGetLocNwPolDscp                      PROTO ((INT4 
        i4LldpV2LocPortIfIndex,
        tSNMP_OCTET_STRING_TYPE *pFsLldpMedLocMediaPolicyAppType, INT4 *
        pi4RetValFsLldpMedLocMediaPolicyDscp));

PUBLIC INT4
LldpMedUtlGetLocNwPolUnknown                   PROTO ((INT4
        i4LldpV2LocPortIfIndex,
        tSNMP_OCTET_STRING_TYPE *pFsLldpMedLocMediaPolicyAppType, INT4
        *pi4RetValFsLldpMedLocMediaPolicyUnknown));

PUBLIC INT4 
LldpMedUtlGetLocNwPolTagged                    PROTO ((INT4 
        i4LldpV2LocPortIfIndex,
        tSNMP_OCTET_STRING_TYPE *pFsLldpMedLocMediaPolicyAppType, INT4
        *pi4RetValFsLldpMedLocMediaPolicyTagged));


PUBLIC INT4 
LldpMedUtlGetLocNwPolRowStatus                 PROTO ((INT4 
        i4LldpV2LocPortIfIndex,
        tSNMP_OCTET_STRING_TYPE *pFsLldpMedLocMediaPolicyAppType, INT4
        *pi4RetValFsLldpMedLocMediaPolicyRowStatus));

INT4 
LldpMedUtlValIndexRemCapInvTbl                 PROTO ((UINT4
            u4LldpV2RemTimeMark, INT4 i4LldpV2RemLocalIfIndex, UINT4
            u4LldpV2RemLocalDestMACAddress, INT4 i4LldpV2RemIndex));

INT4 
LldpMedUtlGetFirstIndexRemCapTbl               PROTO ((UINT4
            *pu4LldpV2RemTimeMark, INT4 *pi4LldpV2RemLocalIfIndex, UINT4
            *pu4LldpV2RemLocalDestMACAddress, INT4 *pi4LldpV2RemIndex));

INT4 
LldpMedUtlGetNextIndexRemCapTbl                PROTO ((UINT4
            u4LldpV2RemTimeMark, UINT4 *pu4NextLldpV2RemTimeMark, INT4
            i4LldpV2RemLocalIfIndex, INT4 *pi4NextLldpV2RemLocalIfIndex, UINT4
            u4LldpV2RemLocalDestMACAddress, UINT4
            *pu4NextLldpV2RemLocalDestMACAddress, INT4 i4LldpV2RemIndex, INT4
            *pi4NextLldpV2RemIndex));

INT4 
LldpMedUtlGetRemCapSupported                   PROTO ((UINT4 
            u4LldpV2RemTimeMark, INT4
            i4LldpV2RemLocalIfIndex, UINT4 u4LldpV2RemLocalDestMACAddress, INT4
            i4LldpV2RemIndex, tSNMP_OCTET_STRING_TYPE *
            pRetValFsLldpXMedRemCapSupported));


INT4 
LldpMedUtlGetRemCapCurrent                     PROTO ((UINT4 
            u4LldpV2RemTimeMark, INT4
            i4LldpV2RemLocalIfIndex, UINT4 u4LldpV2RemLocalDestMACAddress, INT4
            i4LldpV2RemIndex, tSNMP_OCTET_STRING_TYPE *
            pRetValFsLldpXMedRemCapCurrent));


INT4 
LldpMedUtlGetRemDeviceClass                    PROTO ((UINT4 
            u4LldpV2RemTimeMark, INT4
            i4LldpV2RemLocalIfIndex, UINT4 u4LldpV2RemLocalDestMACAddress, INT4
            i4LldpV2RemIndex, INT4 *pi4RetValFsLldpXMedRemDeviceClass));

INT4 
LldpMedUtlGetFirstIndexRemInvTbl               PROTO ((UINT4
            *pu4LldpV2RemTimeMark, INT4 *pi4LldpV2RemLocalIfIndex, UINT4
            *pu4LldpV2RemLocalDestMACAddress, INT4 *pi4LldpV2RemIndex));

INT4 
LldpMedUtlGetNextIndexRemInvTbl                PROTO ((UINT4 
            u4LldpV2RemTimeMark,
            UINT4 *pu4NextLldpV2RemTimeMark, INT4 i4LldpV2RemLocalIfIndex, INT4
            *pi4NextLldpV2RemLocalIfIndex, UINT4
            u4LldpV2RemLocalDestMACAddress, UINT4
            *pu4NextLldpV2RemLocalDestMACAddress, INT4 i4LldpV2RemIndex, INT4
            *pi4NextLldpV2RemIndex));

INT4 
LldpMedUtlGetRemHardwareRev                    PROTO ((UINT4 
            u4LldpV2RemTimeMark, INT4
            i4LldpV2RemLocalIfIndex, UINT4 u4LldpV2RemLocalDestMACAddress, INT4
            i4LldpV2RemIndex, tSNMP_OCTET_STRING_TYPE *
            pRetValFsLldpXMedRemHardwareRev));

INT4 
LldpMedUtlGetRemFirmwareRev                    PROTO ((UINT4 
            u4LldpV2RemTimeMark, INT4
            i4LldpV2RemLocalIfIndex, UINT4 u4LldpV2RemLocalDestMACAddress, INT4
            i4LldpV2RemIndex, tSNMP_OCTET_STRING_TYPE *
            pRetValFsLldpXMedRemFirmwareRev));


INT4 
LldpMedUtlGetRemSoftwareRev                    PROTO ((UINT4 
            u4LldpV2RemTimeMark, INT4
            i4LldpV2RemLocalIfIndex, UINT4 u4LldpV2RemLocalDestMACAddress, INT4
            i4LldpV2RemIndex, tSNMP_OCTET_STRING_TYPE
            *pRetValFsLldpXMedRemSoftwareRev));


INT4 
LldpMedUtlGetRemSerialNum                      PROTO ((UINT4 
            u4LldpV2RemTimeMark, INT4
            i4LldpV2RemLocalIfIndex, UINT4 u4LldpV2RemLocalDestMACAddress, INT4
            i4LldpV2RemIndex, tSNMP_OCTET_STRING_TYPE *
            pRetValFsLldpXMedRemSerialNum));

INT4 
LldpMedUtlGetRemMfgName                        PROTO ((UINT4 
            u4LldpV2RemTimeMark, INT4
            i4LldpV2RemLocalIfIndex, UINT4 u4LldpV2RemLocalDestMACAddress, INT4
            i4LldpV2RemIndex, tSNMP_OCTET_STRING_TYPE *
            pRetValFsLldpXMedRemMfgName));

INT4 
LldpMedUtlGetRemModelName                      PROTO ((UINT4 
            u4LldpV2RemTimeMark, INT4
            i4LldpV2RemLocalIfIndex, UINT4 u4LldpV2RemLocalDestMACAddress, INT4
            i4LldpV2RemIndex, tSNMP_OCTET_STRING_TYPE *
            pRetValFsLldpXMedRemModelName));


INT4 
LldpMedUtlGetRemAssetID                        PROTO ((UINT4 
            u4LldpV2RemTimeMark, INT4
            i4LldpV2RemLocalIfIndex, UINT4 u4LldpV2RemLocalDestMACAddress, INT4
            i4LldpV2RemIndex, tSNMP_OCTET_STRING_TYPE *
            pRetValFsLldpXMedRemAssetID));

INT4 
LldpMedUtlValIndexRemNwPolTbl                  PROTO ((UINT4
            u4LldpV2RemTimeMark, INT4  i4LldpV2RemLocalIfIndex, UINT4
            u4LldpV2RemLocalDestMACAddress, INT4  i4LldpV2RemIndex,
            tSNMP_OCTET_STRING_TYPE * pFsLldpXMedRemMediaPolicyAppType));

INT4
LldpMedUtlGetFirstIndexRemNwPolTbl             PROTO ((UINT4 
            *pu4LldpV2RemTimeMark,
            INT4 *pi4LldpV2RemLocalIfIndex, UINT4
            *pu4LldpV2RemLocalDestMACAddress, INT4 *pi4LldpV2RemIndex,
            tSNMP_OCTET_STRING_TYPE *pFsLldpXMedRemMediaPolicyAppType));

INT4 
LldpMedUtlGetNextIndexRemNwPolTbl              PROTO ((UINT4
            u4LldpV2RemTimeMark, UINT4 *pu4NextLldpV2RemTimeMark, INT4
            i4LldpV2RemLocalIfIndex, INT4 *pi4NextLldpV2RemLocalIfIndex, UINT4
            u4LldpV2RemLocalDestMACAddress, UINT4
            *pu4NextLldpV2RemLocalDestMACAddress, INT4 i4LldpV2RemIndex, INT4
            *pi4NextLldpV2RemIndex, tSNMP_OCTET_STRING_TYPE
            *pFsLldpXMedRemMediaPolicyAppType, tSNMP_OCTET_STRING_TYPE
            *pNextFsLldpXMedRemMediaPolicyAppType));

INT4
LldpMedUtlGetRemNwPolVlanID                    PROTO ((UINT4 
            u4LldpV2RemTimeMark, INT4
            i4LldpV2RemLocalIfIndex, UINT4 u4LldpV2RemLocalDestMACAddress, INT4
            i4LldpV2RemIndex, tSNMP_OCTET_STRING_TYPE
            *pFsLldpXMedRemMediaPolicyAppType, INT4
            *pi4RetValFsLldpXMedRemMediaPolicyVlanID));

INT4
LldpMedUtlGetRemNwPolPriority                  PROTO ((UINT4 
            u4LldpV2RemTimeMark, INT4
            i4LldpV2RemLocalIfIndex, UINT4 u4LldpV2RemLocalDestMACAddress, INT4
            i4LldpV2RemIndex, tSNMP_OCTET_STRING_TYPE
            *pFsLldpXMedRemMediaPolicyAppType, INT4
            *pi4RetValFsLldpXMedRemMediaPolicyPriority));

INT4
LldpMedUtlGetRemNwPolDscp                      PROTO ((UINT4 
            u4LldpV2RemTimeMark, INT4
            i4LldpV2RemLocalIfIndex, UINT4 u4LldpV2RemLocalDestMACAddress, INT4
            i4LldpV2RemIndex, tSNMP_OCTET_STRING_TYPE
            *pFsLldpXMedRemMediaPolicyAppType, INT4
            *pi4RetValFsLldpXMedRemMediaPolicyDscp));

INT4
LldpMedUtlGetRemNwPolTagged                    PROTO ((UINT4 
            u4LldpV2RemTimeMark, INT4
            i4LldpV2RemLocalIfIndex, UINT4 u4LldpV2RemLocalDestMACAddress, INT4
            i4LldpV2RemIndex, tSNMP_OCTET_STRING_TYPE
            *pFsLldpXMedRemMediaPolicyAppType, INT4
            *pi4RetValFsLldpXMedRemMediaPolicyTagged));

INT4 
LldpMedUtlGetRemNwPolUnknown                   PROTO ((UINT4 
            u4LldpV2RemTimeMark, INT4
            i4LldpV2RemLocalIfIndex, UINT4 u4LldpV2RemLocalDestMACAddress, INT4
            i4LldpV2RemIndex, tSNMP_OCTET_STRING_TYPE
            *pFsLldpXMedRemMediaPolicyAppType, INT4
            *pi4RetValFsLldpXMedRemMediaPolicyUnknown));

INT4
LldpMedUtlValIndexRemLocationTbl
                            (UINT4 u4LldpV2RemTimeMark,
                             INT4  i4LldpV2RemLocalIfIndex,
                             UINT4  u4LldpV2RemLocalDestMACAddress,
                             INT4  i4LldpV2RemIndex,
                             INT4  i4LldpXMedRemLocationSubtype);

INT4
LldpMedUtlGetFirstIndexRemLocationTbl
                                (UINT4 *pu4LldpV2RemTimeMark,
                                 INT4 *pi4LldpV2RemLocalIfIndex,
                                 UINT4 *pu4LldpV2RemLocalDestMACAddress,
                                 INT4 *pi4LldpV2RemIndex,
                                 INT4 *pi4LldpXMedRemLocationSubtype);

INT4
LldpMedUtlGetNextIndexRemLocationTbl
                            (UINT4 u4LldpV2RemTimeMark,
                             UINT4 *pu4NextLldpV2RemTimeMark,
                             INT4 i4LldpV2RemLocalIfIndex,
                             INT4 *pi4NextLldpV2RemLocalIfIndex,
                             UINT4 u4LldpV2RemLocalDestMACAddress,
                             UINT4 *pu4NextLldpV2RemLocalDestMACAddress,
                             INT4 i4LldpV2RemIndex,
                             INT4 *pi4NextLldpV2RemIndex,
                             INT4 i4LldpXMedRemLocationSubtype,
                             INT4 *pi4NextLldpXMedRemLocationSubtype);

INT4
LldpMedUtlGetRemLocationInfo(UINT4 u4LldpV2RemTimeMark,
                            INT4  i4LldpV2RemLocalIfIndex,
                            UINT4 u4LldpV2RemLocalDestMACAddress,
                            INT4  i4LldpV2RemIndex,
                            INT4  i4LldpXMedRemLocationSubtype,
                            tSNMP_OCTET_STRING_TYPE
                            *pRetValLldpXMedRemLocationInfo);

INT4
LldpMedUtlGetRemPoEDeviceType(UINT4 u4LldpV2RemTimeMark,
                             INT4  i4LldpV2RemLocalIfIndex,
                             UINT4 u4LldpV2RemLocalDestMACAddress,
                             INT4 i4LldpV2RemIndex,
                             INT4 *pi4RetValLldpXMedRemXPoEDeviceType);

INT4
LldpMedUtlGetRemPoEPDPowerReq(UINT4 u4LldpV2RemTimeMark,
                             INT4  i4LldpV2RemLocalIfIndex,
                             UINT4 u4LldpV2RemLocalDestMACAddress,
                             INT4 i4LldpV2RemIndex,
                             UINT4 *pu4RetValLldpXMedRemXPoEPDPowerReq);

INT4
LldpMedUtlGetRemPoEPDPowerSource(UINT4 u4LldpV2RemTimeMark,
                             INT4  i4LldpV2RemLocalIfIndex,
                             UINT4 u4LldpV2RemLocalDestMACAddress,
                             INT4 i4LldpV2RemIndex,
                             INT4 *pi4RetValLldpXMedRemXPoEPDPowerSource);

INT4
LldpMedUtlGetRemPoEPDPowerPriority(UINT4 u4LldpV2RemTimeMark,
                             INT4  i4LldpV2RemLocalIfIndex,
                             UINT4 u4LldpV2RemLocalDestMACAddress,
                             INT4 i4LldpV2RemIndex,
                             INT4 *pi4RetValLldpXMedRemXPoEPDPowerPriority);

PUBLIC VOID 
LldpMedGetAdminStatus PROTO ((INT4 i4LldpV2PortConfigIfIndex, INT4 *pi4AdminStatus));

PUBLIC VOID
LldpMedHandleAdminStatusChng PROTO ((INT4 i4LldpLocPortNum, INT4 i4MedAdminStatus));

PUBLIC VOID
LldpMedUtilDelRemMedTlvsInfo PROTO((INT4 i4LldpLocPortNum));

PUBLIC BOOL1
LldpIsDcbxApplicationRegistered PROTO ((UINT4 u4IfIndex));

PUBLIC BOOL1
LldpIsMultipleAgentsOnInterface PROTO ((INT4 i4IfIndex));
/*--------------------------------------------------------------------------*/
/*                       lldque.c                                           */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 LldpQueEnqAppMsg             PROTO ((tLldpQMsg *pMsg));
PUBLIC INT4 LldpQueEnqRxPduMsg           PROTO ((tLldpRxPduQMsg *pMsg));
PUBLIC VOID LldpQueAppMsgHandler         PROTO ((VOID));
PUBLIC VOID LldpQueRxPduMsgHandler       PROTO ((VOID));
INT4 LldpQueHandleIfAlias PROTO ((tLldpLocPortInfo * pLocPortEntry,
                                          UINT4 u4IfIndex, UINT1 *pu1IfAlias));
VOID LldpQueUpdtIfAlias PROTO ((UINT1 *pu1Dest, UINT1 *pu1Src,
                                        INT4 i4MaxLen));
INT4
    LldpQueHandleAgentCktId PROTO ((tLldpLocPortInfo * pLocPortEntry,
                                    UINT4 u4AgentCircuitId));
INT4 LldpQueHandleSysName PROTO ((UINT1 *));
INT4 LldpQueHandleSysDesc PROTO ((UINT1 *));
INT4 LldpQueHandlePortDesc PROTO ((UINT1 *,UINT4 ));
INT4
    LldpQueHandleIp4ManAddr PROTO ((tLldpQIp4ManAddress * pIp4ManAddrQMsg));
INT4
    LldpQueHandleIp6ManAddr PROTO ((tNetIpv6AddrChange * pIp6ManAddrQMsg));
INT4 LldpQueHandlePVid PROTO ((tLldpLocPortInfo * pLocPortEntry,
                                       UINT2 u2PVId));
INT4
    LldpQueHandleProtoVlanStatus PROTO ((UINT4 u4IfIndex,
                                         UINT1 u1ProtVlanStatus));
INT4
    LldpQueHandleProtoVlanId PROTO ((UINT4 u4IfIndex, UINT2 u2VlanId,
                                     UINT2 u2OldVlanId, UINT4 u4ActionFlag));
INT4
    LldpQueHandleVlanInfoChgForPort PROTO ((tLldpLocPortInfo * pLocPortEntry));
INT4 LldpQueHandleVlanInfoChg PROTO ((tPortList EggressPortList));
INT4
    LldpQueHandlePortAggCapability PROTO ((tLldpLocPortInfo * pLocPortEntry,
                                           UINT1 u1AggCap));
INT4 LldpQueHandleResetAggCapOnPorts PROTO ((tPortList AggConfPorts));
INT4
    LldpQueHandlePortAggStatus PROTO ((tLldpLocPortInfo * pLocPortEntry,
                                       UINT1 u1AggStatus));
VOID
    LldpQueHandleIncomingFrame PROTO ((tLldpLocPortInfo * pLocPortEntry,
                                       tCRU_BUF_CHAIN_HEADER * pBuf));
INT4
    LldpQueHandleApplPortRequest PROTO ((UINT4 u4PortId,
                                         tLldpAppPortMsg * pLldpAppPortMsg,
                                         UINT1 u1Request));
INT4
LldpQueHandleDstMac PROTO ((tLldpAgent *pLldpAgent));

/*--------------------------------------------------------------------------*/
/*                       lldtxutl.c                                         */
/*--------------------------------------------------------------------------*/
PUBLIC INT4
LldpTxUtlHandleLocPortInfoChg            PROTO ((tLldpLocPortInfo * pPortInfo));
PUBLIC VOID 
LldpTxUtlHandleLocSysInfoChg             PROTO ((VOID));
PUBLIC UINT4 LldpTxUtlJitter             PROTO ((UINT4 u4Value, 
                                                 UINT4 u4JitterPcent));
PUBLIC INT4 LldpTxUtlDelManAddrNode      PROTO ((INT4 i4ManAddrSubtype, 
                                                 UINT1 *pu1ManAddr));
PUBLIC INT4 LldpTxUtlSetChassisId        PROTO ((INT4 i4NewChassisIdSubType));
PUBLIC INT4 LldpTxUtlSetPortId           PROTO ((UINT4 u4LocPortNum, 
                                                 INT4 i4PortIdSubType));
PUBLIC INT4 LldpTxUtlGetNextManAddr      PROTO ((tLldpLocManAddrTable * 
                                                 pCurrManAddrNode,
                                                 tLldpLocManAddrTable ** 
                                                 ppNextManAddrNode));
PUBLIC INT4
LldpTxUtlValidateManAddrTblIndices       PROTO ((INT4 i4LldpLocManAddrSubtype,
                                                 INT4 i4ManAddrLen));
PUBLIC tLldpLocManAddrTable * 
LldpTxUtlGetLocManAddrNode               PROTO ((INT4 i4LldpLocManAddrSubtype,
                                                 UINT1 * pu1LocManAddr));
PUBLIC INT4 LldpTxUtlGetLocPortEntry     PROTO ((UINT4 u4PortIndex, 
                                                 tLldpLocPortInfo ** 
                                                 ppLocPortEntry));
PUBLIC INT4 LldpTxUtlRBCmpLocManAddr     PROTO ((tRBElem * pRBElem1, 
                                                 tRBElem * pRBElem2));
PUBLIC INT4 
LldpTxUtlConstructPreformedBuf           PROTO ((tLldpLocPortInfo *pPortInfo,
                                                 UINT1 u1FrameType));
/* Mandatory TLVs */
PUBLIC INT4 LldpTxUtlAddMandatoryTlvs    PROTO ((tLldpLocPortInfo *pPortInfo, 
                                                 UINT1 *pu1Buf, 
                                                 UINT2 *pu2NextOffset, 
                                                 UINT1 u1FrameType));
PUBLIC INT4 LldpTxUtlAddChassisIdTlv     PROTO ((UINT1 *pu1Buf, 
                                                 UINT2 *pu2LocOffset));
PUBLIC INT4 LldpTxUtlAddPortIdTlv        PROTO ((UINT1 *pu1Buf, 
                                                 UINT2 *pu2LocOffset,
                                                 tLldpLocPortInfo *pPortInfo));
PUBLIC VOID LldpTxUtlAddTtlTlv           PROTO ((UINT1 *pu1Buf, 
                                                 UINT2 *pu2LocOffset, 
                                                 UINT1 u1FrameType));
/* Basic Optional TLVs */
PUBLIC INT4
LldpTxUtlAddBasicOptionalTlvs            PROTO ((tLldpLocPortInfo *pPortInfo,
                                                 UINT1 *pu1BasePtr, 
                                                 UINT1 *pu1Buf, 
                                                 UINT2 *pu2NextOffset));
PUBLIC INT4 LldpTxUtlAddPortDescTlv      PROTO ((tLldpLocPortInfo * pPortInfo, 
                                                 UINT1 *pu1BasePtr,
                                                 UINT1 *pu1Buf, 
                                                 UINT2 *pu2LocOffset));
PUBLIC INT4 LldpTxUtlAddSysNameTlv       PROTO ((tLldpLocPortInfo * pPortInfo,
                                                 UINT1 *pu1BasePtr,
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2LocOffset)); 
PUBLIC INT4 LldpTxUtlAddSysDescTlv       PROTO ((tLldpLocPortInfo * pPortInfo,
                                                 UINT1 *pu1BasePtr,
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2LocOffset));
PUBLIC INT4 LldpTxUtlAddSysCapabTlv      PROTO ((tLldpLocPortInfo * pPortInfo,
                                                 UINT1 *pu1BasePtr, 
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2LocOffset));
PUBLIC INT4 LldpTxUtlAddManAddrTlv       PROTO ((tLldpLocPortInfo *pPortInfo, 
                                                 UINT1 *pu1BasePtr,
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2LocOffset));
/* DOT1 TLVs */
PUBLIC INT4 LldpTxUtlAddDot1OrgSpecTlvs  PROTO ((tLldpLocPortInfo * pPortInfo,
                                                 UINT1 *pu1BasePtr, 
                                                 UINT1 *pu1Buf, 
                                                 UINT2 *pu2NextOffset));
PUBLIC INT4 LldpTxUtlAddPortVlanIdTlv    PROTO ((tLldpLocPortInfo *pPortInfo,
                                                 UINT1 *pu1BasePtr,
                                                 UINT1 *pu1Buf, 
                                                 UINT2 *pu2LocOffset));
PUBLIC INT4 LldpTxUtlAddProtoVlanIdTlv   PROTO ((tLldpLocPortInfo * pPortInfo,
                                                 UINT1 *pu1BasePtr, 
                                                 UINT1 *pu1Buf, 
                                                 UINT2 *pu2LocOffset));
PUBLIC INT4 LldpTxUtlAddPortVlanNameTlv  PROTO ((tLldpLocPortInfo *pPortInfo,
                                                 UINT1 *pu1BasePtr,
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2LocOffset));
PUBLIC INT4 LldpTxUtlAddProtoIdTlv       PROTO ((tLldpLocPortInfo *pPortInfo,
                                                 UINT1 *pu1BasePtr,
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2LocOffset));
/* DOT3 TLVs */
PUBLIC INT4 LldpTxUtlAddDot3OrgSpecTlvs  PROTO ((tLldpLocPortInfo *pPortInfo, 
                                                 UINT1 *pu1BasePtr, 
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2NextOffset));
PUBLIC INT4 LldpTxUtlAddMacPhyTlv        PROTO ((tLldpLocPortInfo *pPortInfo,
                                                 UINT1 *pu1BasePtr, 
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2LocOffset));
PUBLIC INT4 LldpTxUtlAddPwrViaMdiTlv     PROTO ((tLldpLocPortInfo * pPortInfo,
                                                 UINT1 *pu1BasePtr,
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2LocOffset));
PUBLIC INT4 LldpTxUtlAddLinkAggTlv       PROTO ((tLldpLocPortInfo * pPortInfo,
                                                 UINT1 *pu1BasePtr,
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2LocOffset));
PUBLIC INT4 LldpTxUtlAddMaxFrameSizeTlv  PROTO ((tLldpLocPortInfo * pPortInfo,
                                                 UINT1 *pu1BasePtr,
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2LocOffset));
PUBLIC VOID LldpTxUtlAddEndOfLLdpduTlv   PROTO ((UINT1 *pu1Buf, 
                                                 UINT2 *pu2LocOffset));
PUBLIC INT4 LldpTxUtlClearPortInfo       PROTO ((tLldpLocPortInfo *pPortEntry));
PUBLIC INT4 LldpTxUtlDelProtoVlanDLL     PROTO ((tLldpLocPortInfo *pPortEntry));
PUBLIC VOID LldpTxUtlDeleteQMsg          PROTO ((tOsixQId QId, 
                                                 tMemPoolId PoolId));
PUBLIC INT4 LldpTxUtlRBFreeLocManAddr    PROTO ((tRBElem *pRBElem, 
                                                 UINT4 u4Arg));
PUBLIC INT4 LldpTxUtlRBFreeLocProtoId    PROTO ((tRBElem *pRBElem,
                                                 UINT4 u4Arg));
PUBLIC INT4 LldpTxUtlAddIpv6Addr         PROTO ((tNetIpv6AddrChange *
                                                 pNetIpv6AddrChange));
PUBLIC INT4 LldpTxUtlAddIpv4Addr         PROTO ((tNetIpv4IfInfo *
                                                 pNetIp4IfInfo));
PUBLIC INT4 LldpTxUtlValidatePortIndex   PROTO ((UINT4 u4LldpPortConfigPortNum));
PUBLIC INT4 LldpTxUtlRBCmpLocProtoIdInfo PROTO ((tRBElem *pRBElem1,
                                                 tRBElem *pRBElem2));
PUBLIC tLldpxdot1LocProtoVlanInfo *
LldpTxUtlGetProtoVlanEntry               PROTO ((UINT4 u4IfIndex,
                                                 tVlanId ProtoVlanId));
PUBLIC INT4 
LldpTxUtlAddProtoVlanEntry               PROTO ((tLldpLocPortInfo *
                                                 pLldpLocPortInfo,
                                                 UINT2 u2VlanId));
PUBLIC INT4 
LldpTxUtlUpdateProtoVlanEntry            PROTO ((tLldpLocPortInfo *
                                                 pLldpLocPortInfo,
                                                 UINT2 u2VlanId, 
                                                 UINT2 u2OldVlanId,
                                                 UINT4 u4ActionFlag));

PUBLIC VOID LldpTxUtlAddCredit PROTO ((tLldpLocPortInfo * pPortInfo));
PUBLIC INT4
LldpTxUtlAddMgmtVidTlv PROTO ((tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                        UINT1 *pu1Buf, UINT2 *pu2LocOffset));
PUBLIC INT4
LldpTxUtlAddVidUsageDigestTlv PROTO ((tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                        UINT1 *pu1Buf, UINT2 *pu2LocOffset));


/*--------------------------------------------------------------------------*/
/*                       lldmtlv.c                                         */
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/*                    LLDP-MED Specific TLV                                 */
/*--------------------------------------------------------------------------*/
PUBLIC VOID LldpMedAddMedOrgSpecTlvs   PROTO ((tLldpLocPortInfo *pPortInfo,
                                                 UINT1 *pu1BasePtr,
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2NextOffset));

PUBLIC VOID LldpMedAddMedCapTlv        PROTO ((tLldpLocPortInfo *pPortInfo,
                                                 UINT1 *pu1BasePtr,
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2LocOffset));

PUBLIC VOID LldpMedAddNwPolicyTlv      PROTO ((tLldpLocPortInfo *pPortInfo,
                                                 UINT1 *pu1BasePtr,
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2LocOffset));  

PUBLIC VOID LldpMedAddLocIdTlv         PROTO ((tLldpLocPortInfo *pPortInfo,
                                                 UINT1 *pu1BasePtr,
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2LocOffset));

PUBLIC VOID LldpMedAddPowMDITlv        PROTO ((tLldpLocPortInfo *pPortInfo,
                                                 UINT1 *pu1BasePtr,
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2LocOffset));

PUBLIC VOID LldpMedAddHwRevisionTlv    PROTO ((tLldpLocPortInfo *pPortInfo,
                                                 UINT1 *pu1BasePtr,
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2LocOffset));

PUBLIC VOID LldpMedAddFwRevisionTlv    PROTO ((tLldpLocPortInfo *pPortInfo,
                                                 UINT1 *pu1BasePtr,
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2LocOffset));

PUBLIC VOID LldpMedAddSwRevisionTlv    PROTO ((tLldpLocPortInfo *pPortInfo,
                                                 UINT1 *pu1BasePtr,
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2LocOffset));

PUBLIC VOID LldpMedAddSerialNumberTlv  PROTO ((tLldpLocPortInfo *pPortInfo,
                                                 UINT1 *pu1BasePtr,
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2LocOffset));

PUBLIC VOID LldpMedAddMfgNameTlv       PROTO ((tLldpLocPortInfo *pPortInfo,
                                                 UINT1 *pu1BasePtr,
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2LocOffset));

PUBLIC VOID LldpMedAddModelNameTlv     PROTO ((tLldpLocPortInfo *pPortInfo,
                                                 UINT1 *pu1BasePtr,
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2LocOffset));

PUBLIC VOID LldpMedAddAssetIdTlv       PROTO ((tLldpLocPortInfo *pPortInfo,
                                                 UINT1 *pu1BasePtr,
                                                 UINT1 *pu1Buf,
                                                 UINT2 *pu2LocOffset));

PUBLIC VOID LldpMedUtlHandlePolicyChg     PROTO ((INT4 i4PortIndex));

PUBLIC VOID LldpMedUtlHandleLocationChg   PROTO ((INT4 i4PortIndex));


/*--------------------------------------------------------------------------*/
/*                       lldrxutl.c                                         */
/*--------------------------------------------------------------------------*/
PUBLIC VOID LldpRxUtlDrainRemSysTables   PROTO ((VOID));
PUBLIC VOID LldpRxUtlDelRemInfoForPort   PROTO ((tLldpLocPortInfo *
                                                 pLocPortInfo));
PUBLIC INT4 LldpRxUtlDelRemVlanNameInfo  PROTO ((tLldpRemoteNode *pRemoteNode));
PUBLIC INT4 LldpRxUtlDelRemProtoVlan     PROTO ((tLldpRemoteNode *pRemoteNode));
PUBLIC INT4 LldpRxUtlDelRemProtoId       PROTO ((tLldpRemoteNode *pRemoteNode));
PUBLIC INT4 LldpRxUtlDelRemManAddr       PROTO ((tLldpRemoteNode *pRemoteNode));
PUBLIC INT4 LldpRxUtlDelRemOrgDefInfo    PROTO ((tLldpRemoteNode *pRemoteNode));
PUBLIC INT4 LldpRxUtlDelRemUnknownTLV    PROTO ((tLldpRemoteNode *pRemoteNode));
/* RBTree Compare Functions */
PUBLIC INT4 LldpRxUtlRBCmpSysData        PROTO ((tRBElem *e1, tRBElem *e2));
PUBLIC INT4 LldpRxUtlRBCmpMSAP           PROTO ((tRBElem *e1, tRBElem *e2));
PUBLIC INT4 LldpRxUtlRBCmpManAddr        PROTO ((tRBElem *e1, tRBElem *e2));
PUBLIC INT4 LldpRxUtlRBCmpOrgDefInfo     PROTO ((tRBElem *e1, tRBElem *e2));
PUBLIC INT4 LldpRxUtlRBCmpUnknownTLV     PROTO ((tRBElem *e1, tRBElem *e2));
PUBLIC INT4 LldpRxUtlRBCmpVlanNameInfo   PROTO ((tRBElem *e1, tRBElem *e2));
PUBLIC INT4 LldpRxUtlRBCmpProtoVlanInfo  PROTO ((tRBElem *e1, tRBElem *e2));
PUBLIC INT4 LldpRxUtlRBCmpProtoIdInfo    PROTO ((tRBElem *e1, tRBElem *e2));
/* RBTree Free Functions */
PUBLIC INT4 LldpRxUtlRBFreeSysData       PROTO ((tRBElem *e1, UINT4 u4Arg));
PUBLIC INT4 LldpRxUtlRBFreeMSAP          PROTO ((tRBElem *e1, UINT4 u4Arg));
PUBLIC INT4 LldpRxUtlRBFreeManAddr       PROTO ((tRBElem *e1, UINT4 u4Arg));
PUBLIC INT4 LldpRxUtlRBFreeOrgDefInfo    PROTO ((tRBElem *e1, UINT4 u4Arg));
PUBLIC INT4 LldpRxUtlRBFreeUnknownTLV    PROTO ((tRBElem *e1, UINT4 u4Arg));
PUBLIC INT4 LldpRxUtlRBFreeVlanNameInfo  PROTO ((tRBElem *e1, UINT4 u4Arg));
PUBLIC INT4 LldpRxUtlRBFreeProtoVlanInfo PROTO ((tRBElem *e1, UINT4 u4Arg));
PUBLIC INT4 LldpRxUtlRBFreeProtoIdInfo   PROTO ((tRBElem *e1, UINT4 u4Arg));
PUBLIC INT4 LldpRxUtlRBCmpV2SysData (tRBElem * e1, tRBElem * e2);
/* RBTree Walk Action Function */
PUBLIC INT4 
LldpRxUtlWalkFnRemNodeForPort            PROTO ((tRBElem *pRBElem,
                                                 eRBVisit visit, 
                                                 UINT4 u4Level, void *pArg, 
                                                 void *pOut));
PUBLIC INT4 LldpRxUtlWalkFnManAddr       PROTO ((tRBElem *pRBElem, 
                                                 eRBVisit visit,
                                                 UINT4 u4Level, void *pArg,
                                                 void *pOut));
PUBLIC INT4 LldpRxUtlWalkFnUnknownTlv    PROTO ((tRBElem *pRBElem, 
                                                 eRBVisit visit,
                                                 UINT4 u4Level, void *pArg,
                                                 void *pOut));
PUBLIC INT4 LldpRxUtlWalkFnOrgDefInfo    PROTO ((tRBElem *pRBElem, 
                                                 eRBVisit visit,
                                                 UINT4 u4Level, void *pArg,
                                                 void *pOut));
PUBLIC INT4 LldpRxUtlWalkFnProtoVlan     PROTO ((tRBElem *pRBElem,
                                                 eRBVisit visit,
                                                 UINT4 u4Level, void *pArg,
                                                 void *pOut));
PUBLIC INT4 LldpRxUtlWalkFnVlanName      PROTO ((tRBElem *pRBElem,
                                                 eRBVisit visit, 
                                                 UINT4 u4Level, void *pArg,
                                                 void *pOut));
PUBLIC INT4 LldpRxUtlWalkFnProtocolId    PROTO ((tRBElem *pRBElem, 
                                                 eRBVisit visit,
                                                 UINT4 u4Level, void *pArg, 
                                                 void *pOut));
PUBLIC INT4 LldpRxUtlWalkFnRemTable      PROTO ((tRBElem *pRBElem, 
                                                 eRBVisit visit,
                                                 UINT4 u4Level, void *pArg,
                                                 void *pOut));
/* Operation on Remote Node */
/*---- GetFirst Routines for RBNodes (Neighbor Specific) */
PUBLIC tLldpRemManAddressTable * 
LldpRxUtlGetFirstManAddr                 PROTO ((tLldpRemoteNode *pRemoteNode));
PUBLIC tLldpRemUnknownTLVTable *
LldpRxUtlGetFirstUnknownTLV              PROTO ((tLldpRemoteNode *pRemoteNode));
PUBLIC tLldpRemOrgDefInfoTable *
LldpRxUtlGetFirstOrgDefInfo              PROTO ((tLldpRemoteNode *pRemoteNode));
PUBLIC tLldpxdot1RemProtoVlanInfo *
LldpRxUtlGetFirstProtoVlan               PROTO ((tLldpRemoteNode *pRemoteNode));
PUBLIC tLldpxdot1RemVlanNameInfo *
LldpRxUtlGetFirstVlanName                PROTO ((tLldpRemoteNode *pRemoteNode));
PUBLIC tLldpxdot1RemProtoIdInfo *
LldpRxUtlGetFirstProtoId                 PROTO ((tLldpRemoteNode *pRemoteNode));
PUBLIC INT4
LldpRxUtlGetFirstDot3RemTableInd         PROTO ((UINT4 *pu4TimeMark,
                                                 INT4 *pi4LocalPortNum, 
                                                 INT4 *pi4RemIndex, 
                                                 UINT1 u1TlvType));
PUBLIC INT4
LldpRxUtlGetNextDot3RemTableInd          PROTO ((UINT4 u4TimeMark,
                                                 UINT4 *pu4NextTimeMark,
                                                 INT4 i4LocalPortNum,
                                                 INT4 *pi4NextLocalPortNum,
                                                 INT4 i4RemIndex,
                                                 INT4 *pi4NextRemIndex, 
                                                 UINT1 u1TlvType));
PUBLIC INT4 LldpRxUtlAddRemoteNode       PROTO ((tLldpRemoteNode *pRemoteNode));
PUBLIC VOID LldpRxUtlDelRemoteNode       PROTO ((tLldpRemoteNode *pRemoteNode));
PUBLIC tLldpRemoteNode *
LldpRxUtlGetRemoteNodeByMSAP             PROTO ((INT4 i4RecvdPortNum,
                                                 INT4 i4ChassisIdSubtype,
                                                 UINT1 *pu1ChassisId, 
                                                 INT4 i4PortIdSubtype,
                                                 UINT1 *pu1PortId));
/* Indices Validation Routines */
PUBLIC INT4 
LldpRxUtlValidateVlanNameIndices         PROTO ((UINT4 u4TimeMark, 
                                                 INT4 i4LocalPortNum, 
             UINT4 u4DestInd,
                                                 INT4 i4RemIndex, 
                                                 UINT2 u2VlanId));
PUBLIC INT4
LldpRxUtlValidateRemTableIndices         PROTO ((UINT4 u4TimeMark, 
                                                 INT4 i4LocalPortNum, 
             UINT4 u4DestInd,
                                                 INT4 i4RemIndex));
PUBLIC INT4
LldpRxUtlValidateDot3RemTableInd         PROTO ((UINT4 u4TimeMark, 
                                                 INT4 i4LocalPortNum, 
             UINT4 u4DestInd,
             INT4 i4RemIndex));
PUBLIC INT4
LldpRxUtlValidateProtVlanIndices         PROTO ((UINT4 u4TimeMark, 
                                                 INT4 i4LocalPortNum, 
                                                 UINT4 u4DestIndex,
             INT4 i4RemIndex, 
                                                 UINT2 u2ProtoVlanId));
PUBLIC INT4
LldpRxUtlValidateProtoIdIndices          PROTO ((UINT4 u4TimeMark, 
                                                 INT4 i4LocalPortNum, 
                                                 UINT4 u4DestIndex,
                                                 INT4 i4RemIndex,
                                                 UINT4 u4ProtocolIndex));
PUBLIC INT4
LldpRxUtlValidateManAddrTblIndices       PROTO ((UINT4 u4LldpRemTimeMark, 
                                                 INT4 i4LldpRemLocalPortNum,
                                                 UINT4 u4DestIndex,
                                                 INT4 i4LldpRemIndex, 
                                                 INT4 i4LldpRemManAddrSubtype,
                                                 INT4 i4ManAddrLen));
PUBLIC INT4
LldpRxUtlValidateUnknownTlvInfoTblIndices PROTO ((UINT4 u4LldpRemTimeMark,
                                                  INT4 i4LldpRemLocalPortNum,
                                                 UINT4 u4DestIndex,
                                                  INT4 i4LldpRemIndex,
                                                  INT4 
                                                  i4LldpRemUnknownTLVType));
PUBLIC INT4
LldpRxUtlValidateOrgDefInfoTblIndices    PROTO ((UINT4 u4LldpRemTimeMark,
                                                 INT4 i4LldpRemLocalPortNum,
                                                 UINT4 u4DestIndex,
                                                 INT4 i4LldpRemIndex,
                                                 INT4 
                                                 i4LldpRemOrgDefInfoSubtype,
                                                 INT4 
                                                 i4LldpRemOrgDefInfoIndex));
/* GetNext Routines - For SNMP Low level operation */
PUBLIC INT4 LldpRxUtlGetNextRemoteNode   PROTO ((tLldpRemoteNode *
                                                 pCurrRemoteNode, 
                                                 tLldpRemoteNode **
                                                 ppNextRemoteNode));
PUBLIC INT4 LldpRxUtlGetNextMSAPRemoteNode PROTO ((tLldpRemoteNode *
                                                   pCurrRemoteNode,
                                                   tLldpRemoteNode **
                                                   ppNextRemoteNode));

PUBLIC INT4 LldpRxUtlGetNextProtoVlan    PROTO ((tLldpxdot1RemProtoVlanInfo *
                                                 pCurProtoVlanNode,
                                                 tLldpxdot1RemProtoVlanInfo **
                                                 ppNextProtoVlanNode));
PUBLIC INT4 LldpRxUtlGetNextProtoId      PROTO ((tLldpxdot1RemProtoIdInfo *
                                                 pCurProtoIdNode,
                                                 tLldpxdot1RemProtoIdInfo **
                                                 ppNextProtoIdNode));
PUBLIC INT4 LldpRxUtlGetNextManAddr      PROTO ((tLldpRemManAddressTable *
                                                 pCurrManAddrNode,
                                                 tLldpRemManAddressTable **
                                                 ppNextManAddrNode));
PUBLIC INT4 LldpRxUtlGetNextOrgDefInfo   PROTO ((tLldpRemOrgDefInfoTable *
                                                 pRemOrgDefInfoNode,
                                                 tLldpRemOrgDefInfoTable **
                                                 ppNextOrgDefInfoNode));
PUBLIC INT4 LldpRxUtlGetNextUnknownTLV   PROTO ((tLldpRemUnknownTLVTable *
                                                 pRemUnknownTLVNode,
                                                 tLldpRemUnknownTLVTable **
                                                 ppNextRemUnknownTLVNode));
PUBLIC INT4 LldpRxUtlGetNextVlanName     PROTO ((tLldpxdot1RemVlanNameInfo *
                                                 pCurVlanNameNode,
                                                 tLldpxdot1RemVlanNameInfo **
                                                 ppNextVlanNameNode));

/* Get Routines - For SNMP Low level operation */
PUBLIC tLldpRemoteNode * 
LldpRxUtlGetRemoteNode                   PROTO ((UINT4 u4TimeMark,
                                                 INT4 i4LocalPortNum, 
                                                UINT4 u4DestIndex,
             INT4 i4RemIndex));
PUBLIC tLldpRemManAddressTable *
LldpRxUtlGetRemManAddrNode               PROTO ((UINT4 u4LldpRemTimeMark,
                                                 INT4 i4LldpRemLocalPortNum,
             UINT4 u4DestIndex,
                                                 INT4 i4LldpRemIndex,
                                                 INT4 i4LldpRemManAddrSubtype,
                                                 UINT1 *pu1RemManAddr));
PUBLIC INT4 LldpRxUtlGetRemProtoVlanNode PROTO ((UINT4 u4TimeMark, 
                                                 INT4 i4LocalPortNum, 
             UINT4 u4DestIndex,
                                                 INT4 i4RemIndex, 
                                                 UINT2 u2ProtoVlanId,
                                                 tLldpxdot1RemProtoVlanInfo   
                                                 **ppProtoVlanNode));
PUBLIC INT4 LldpRxUtlGetRemVlanNameNode  PROTO ((UINT4 u4TimeMark,
                                                 INT4 i4LocalPortNum, 
                                                 UINT4 u4DestIndex,
             INT4 i4RemIndex, 
                                                 UINT2 u2VlanId,
                                                 tLldpxdot1RemVlanNameInfo 
                                                 **ppVlanNode));
PUBLIC INT4 LldpRxUtlGetRemProtoIdNode   PROTO ((UINT4 u4TimeMark, 
                                                 INT4 i4LocalPortNum, 
             UINT4 u4DestIndex,
                                                 INT4 i4RemIndex, 
                                                 UINT4 u4ProtocolIndex, 
                                                 tLldpxdot1RemProtoIdInfo
                                                 **ppProtoIdNode));
PUBLIC tLldpRemUnknownTLVTable *
LldpRxUtlGetRemUnknownTlvInfo            PROTO ((UINT4 u4LldpRemTimeMark,
                                                 INT4 i4LldpRemLocalPortNum, 
             UINT4 u4DestIndex,
                                                 INT4 i4LldpRemIndex,
                                                 INT4 i4LldpRemUnknownTLVType));
PUBLIC tLldpRemOrgDefInfoTable *
LldpRxUtlGetRemOrgDefInfo                PROTO ((UINT4 u4LldpRemTimeMark,
                                                 INT4 i4LldpRemLocalPortNum,
             UINT4 u4DestIndex,
                                                 INT4 i4LldpRemIndex,
                                                 UINT1 *pu1RemOrgDefInfoOUI,
                                                 INT4 
                                                 i4LldpRemOrgDefInfoSubtype,
                                                 INT4 
                                                 i4LldpRemOrgDefInfoIndex));
PUBLIC VOID
LldpMedRxUtlDelRemPolicyInfo             PROTO ((tLldpRemoteNode *pRemoteNode));


PUBLIC INT4
LldpTxUtlAddAppSpecTlvs PROTO ((tLldpLocPortInfo * pPortInfo, UINT1 *pu1BasePtr,
                                UINT1 *pu1Buf, UINT2 *pu2NextOffset, 
                                UINT1 u1FrameType));
PUBLIC VOID
LldpMedRxUtlDelRemLocationInfo            PROTO ((tLldpRemoteNode *pRemoteNode));

PUBLIC INT4
LldpRxUtlRBFreeLocationInfo             PROTO ((tRBElem *e1, UINT4 u4Arg));

PUBLIC INT4
LldpRxUtlRBFreeNwPolicyInfo PROTO ((tRBElem *e1, UINT4 u4Arg));
/*--------------------------------------------------------------------------*/
/*                       lldtlv.c                                           */
/*--------------------------------------------------------------------------*/
/* TLV Processing routines */
PUBLIC INT4 LldpTlvProcChassisIdTlv      PROTO ((UINT1 *pu1Tlv,
                                                 UINT2 u2TlvInfoLength));
PUBLIC INT4 LldpTlvProcPortIdTlv         PROTO ((UINT1 *pu1Tlv, 
                                                 UINT2 u2TlvInfoLength));
PUBLIC INT4 LldpTlvProcTtlTlv            PROTO ((UINT1 *pu1Tlv,
                                                 UINT2 u2TlvInfoLength));
PUBLIC INT4 LldpTlvProcPortDescTlv       PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1Tlv,
                                                 UINT2 u2TlvInfoLength));
PUBLIC INT4 LldpTlvProcEndOfPduTlv       PROTO ((UINT2 u2TlvInfoLength));
PUBLIC INT4 LldpTlvProcSysNameTlv        PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1Tlv,
                                                 UINT2 u2TlvLength));
PUBLIC INT4 LldpTlvProcSysDescTlv        PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1Tlv,
                                                 UINT2 u2TlvLength));
PUBLIC INT4 LldpTlvProcSysCapaTlv        PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1Tlv,
                                                 UINT2 u2TlvLength));
PUBLIC INT4 LldpTlvProcManAddrTlv        PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1Tlv,
                                                 UINT2 u2TlvLength));
PUBLIC INT4 LldpUtlTlvProcManAddrTlv     PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1Tlv,
                                                 UINT2 u2TlvLength));
PUBLIC INT4 LldpTlvDecodeManAddrInfo     PROTO ((UINT1 *pu1TlvInfo,
                                                 UINT2 u2TlvLength,
                                                 tLldpRemManAddressTable *
                                                 pManAddrNode,
                                                 UINT2 *pu2DecodedLen));
PUBLIC INT4 LldpTlvProcOrgSpecificTlv    PROTO ((tLldpLocPortInfo *
                                                 pLocPortInfo, UINT1 *pu1Tlv,
                                                 UINT2 u2TlvLength, 
                                                 UINT1 *pu1RxOUI,
                                                 UINT1 *pu1TlvSubType,
                                                 UINT2 *pu2MedSupTlvs));

PUBLIC INT4 LldpTlvProcUnknownTlv        PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1Tlv,
                                                 UINT2 u2TlvLength));
/* TLV specific remote database update routines */
PUBLIC INT4 LldpTlvUpdtRemChassisIdInfo  PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1Tlv,
                                                 UINT2 u2TlvInfoLength));

PUBLIC INT4 LldpTlvUpdtRemPortIdInfo     PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1Tlv,
                                                 UINT2 u2TlvInfoLength));
PUBLIC INT4 LldpTlvUpdtRemTtlInfo        PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1Tlv,
                                                 UINT2 u2TlvInfoLength));
PUBLIC INT4 LldpTlvUpdtRemPortDescInfo   PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1Tlv,
                                                 UINT2 u2TlvInfoLength));

PUBLIC INT4 LldpTlvUpdtRemSysNameInfo    PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1Tlv,
                                                 UINT2 u2TlvLength));
PUBLIC INT4 LldpTlvUpdtRemSysDescInfo    PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1Tlv,
                                                 UINT2 u2TlvLength));
PUBLIC INT4 LldpTlvUpdtRemSysCapaInfo    PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1Tlv,
                                                 UINT2 u2TlvLength));
PUBLIC INT4 LldpTlvUpdtRemManAddrInfo    PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1Tlv,
                                                 UINT2 u2TlvLength));
PUBLIC INT4 LldpTlvUpdtRemUnknownTlvInfo PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1Tlv,
                                                 UINT2 u2TlvLength,
                                                 UINT2 u2TlvType));
PUBLIC INT4 
LldpTlvUpdtRemOrgSpecificInfo            PROTO((tLldpLocPortInfo *pLocPortInfo,
                                                UINT1 *pu1Tlv,
                                                UINT2 u2TlvLength));
PUBLIC INT4 LldpTlvUpdtUnrecogOrgDefTlv  PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1TlvInfo, 
                                                 UINT2 u2TlvLength,
                                                 UINT1 *pau1LldpOUI, 
                                                 UINT1 u1TlvSubType));
PUBLIC VOID
LldpReverseOctetString PROTO ((tLldpOctetStringType *pInOctetStr,
                               tLldpOctetStringType *pOutOctetStr));

/*--------------------------------------------------------------------------*/
/*                       lldo1tlv.c                                         */
/*--------------------------------------------------------------------------*/
/* TLV Processing routines */
PUBLIC INT4 LldpDot1TlvProcOrgSpecTlv    PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1TlvInfo,
                                                 UINT2 u2TlvLength,
                                                 UINT1 u1TlvSubType,
                                                 BOOL1 *pbTlvDiscardFlag));
PUBLIC INT4 
LldpDot1TlvProcProtoVlanIdTlv            PROTO ((UINT1 *pu1TlvInfo,
                                                 UINT2 u2TlvLength,
                                                 BOOL1 *pbTlvDiscardFlag));
PUBLIC INT4 LldpDot1TlvProcVlanNameTlv   PROTO ((UINT1 *pu1TlvInfo, 
                                                 UINT2 u2TlvLength));
PUBLIC INT4 LldpDot1TlvProcProtocolIdTlv PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1TlvInfo,
                                                 UINT2 u2TlvLength));
/* TLV specific remote database update routines */
PUBLIC INT4 LldpDot1TlvUpdtOrgSpecTlv    PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1TlvInfo, 
                                                 UINT2 u2TlvLength,
                                                 UINT1 u1TlvSubType));
PUBLIC INT4 LldpDot1TlvUpdtPortVlanIdTlv PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1Tlv));
PUBLIC INT4 
LldpDot1TlvUpdtProtoVlanIdTlv            PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1Tlv));
PUBLIC INT4 LldpDot1TlvUpdtVlanNameTlv   PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1Tlv));
/* TLV Decoding routines */
PUBLIC VOID
LldpDot1TlvDecodeProtoVlanInfo           PROTO ((UINT1 *pu1TlvInfo,
                                                 tLldpxdot1RemProtoVlanInfo 
                                                 *pProtoVlanInfo));
PUBLIC VOID
LldpDot1TlvDecodeVlanNameInfo            PROTO ((UINT1 *pu1TlvInfo, 
                                                 tLldpxdot1RemVlanNameInfo 
                                                 *pVlanNameInfo));
INT4
LldpDot1TlvUpdtMgmtVidTlv PROTO ((tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv));
INT4
LldpDot1TlvUpdtVidUsageDigestTlv PROTO ((tLldpLocPortInfo * pLocPortInfo, UINT1 *pu1Tlv));
INT4
LldpDot1TlvProcMgmtVidTlv PROTO ((UINT1 *pu1TlvInfo, UINT2 u2TlvLength));
INT4
LldpDot1TlvProcVidUsageDigestTlv PROTO ((UINT1 *pu1TlvInfo, UINT2 u2TlvLength));

/*--------------------------------------------------------------------------*/
/*                       lldo3tlv.c                                         */
/*--------------------------------------------------------------------------*/
/* TLV Processing routines */
PUBLIC INT4 LldpDot3TlvProcOrgSpecTlv    PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1TlvInfo,
                                                 UINT1 u1TlvSubType,
                                                 UINT2 u2TlvLength,
                                                 BOOL1 *pbTlvDiscardFlag));
/* TLV specific remote database update routines */
PUBLIC INT4 LldpDot3TlvUpdtOrgSpecTlv    PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1TlvInfo,
                                                 UINT2 u2TlvLength,
                                                 UINT1 u1TlvSubType));
PUBLIC INT4 LldpDot3TlvUpdtMacPhyTlv     PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 tLldpxdot3RemPortInfo *
                                                 pDot3RemPortInfo, 
                                                 UINT1 *pu1Tlv));
PUBLIC INT4 LldpDot3TlvUpdtLinkAggTlv    PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 tLldpxdot3RemPortInfo *
                                                 pDot3RemPortInfo,
                                                 UINT1 *pu1Tlv));
PUBLIC INT4
LldpDot3TlvUpdtMaxFrameSizeTlv           PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 tLldpxdot3RemPortInfo *
                                                 pDot3RemPortInfo,
                                                 UINT1 *pu1Tlv));
/* TLV Decoding routines */
PUBLIC VOID LldpDot3TlvDecodeMacPhyInfo  PROTO ((UINT1 *pu1TlvInfo, 
                                                 tLldpxdot3AutoNegInfo *
                                                 pAutoNegInfo));

/*--------------------------------------------------------------------------*/
/*                       lldtx.c                                            */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 LldpTxFrame                  PROTO ((tLldpLocPortInfo * pPortInfo));
PUBLIC INT4 LldpTxPrependEnetv2Header    PROTO ((tLldpLocPortInfo *pPortInfo));
PUBLIC INT4 LldpTxPrependSnapHeader      PROTO ((tLldpLocPortInfo *pPortInfo));

/*--------------------------------------------------------------------------*/
/*                       lldtmr.c                                           */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 LldpTmrInit                  PROTO ((VOID));
PUBLIC INT4 LldpTmrDeInit                PROTO ((VOID));
PUBLIC VOID LldpTmrExpHandler            PROTO ((VOID));
PUBLIC INT4 LldpRxReStartInfoAgeTmr      PROTO ((tLldpRemoteNode *pRemoteNode));
PUBLIC INT4 LldpRxStartInfoAgeTmr        PROTO ((tLldpRemoteNode *pRemoteNode));
PUBLIC INT4 LldpRxStopInfoAgeTmr         PROTO ((tLldpRemoteNode *pRemoteNode));

/*--------------------------------------------------------------------------*/
/*                       lldtxsm.c                                          */
/*--------------------------------------------------------------------------*/
PUBLIC VOID LldpTxSmMachine              PROTO ((tLldpLocPortInfo *pPortInfo,
                                                 UINT1 u1Event));
PUBLIC VOID
LldpTxTmrSmMachine PROTO ((tLldpLocPortInfo * pPortInfo, UINT1 u1Event));

/*--------------------------------------------------------------------------*/
/*                       lldrxsm.c                                          */
/*--------------------------------------------------------------------------*/
PUBLIC VOID LldpRxSemRun                 PROTO ((tLldpLocPortInfo *
                                                 pLocPortInfo, INT4 i4Event));

/*--------------------------------------------------------------------------*/
/*                       lldif.c                                            */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 LldpIfCreate                 PROTO ((UINT4 u4PortIndex));
PUBLIC VOID LldpIfDelete                 PROTO ((tLldpLocPortInfo *pPortEntry));
PUBLIC INT4 LldpIfOperChg                PROTO ((tLldpLocPortInfo *pPortInfo, 
                                                 UINT1 u1OperStatus));
PUBLIC VOID LldpIfCreateAllPorts         PROTO ((VOID));
PUBLIC INT4        PROTO (LldpIfUpdateIfDesc (tLldpLocPortInfo *));
PUBLIC INT4        PROTO (LldpIfInit (tLldpLocPortInfo *));
PUBLIC INT4        PROTO (LldpIfAddToPortTable (tLldpLocPortInfo *));
PUBLIC INT4 LldpUtlCalcCRC32 PROTO ((UINT1 *pu1Input, UINT4 *pu4Result));
PUBLIC INT4 LldpUtlRBFreeAgentToLocPortTable PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
PUBLIC INT4 LldpUtlRBFreeAgentMapTable PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
PUBLIC INT4  LldpUtlRBFreePortTable PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
PUBLIC INT4  LldpUtlRBFreeDestAddrTable PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
PUBLIC INT4  LldpUtlRBFreeAgentTable PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
PUBLIC INT4 LldpUtilSetDstMac PROTO ((INT4 i4IfIndex, UINT1 *pu1MacAddr,
                  UINT1 u1RowStatus, UINT1 *pu1FatalError));
/*--------------------------------------------------------------------------*/
/*                       lldmed.c                                         */
/*--------------------------------------------------------------------------*/
/* LLDP-MED TLV Processing routines */

PUBLIC INT4 LldpMedProcOrgSpecTlv        PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1TlvInfo,
                                                 UINT1 u1TlvSubType,
                                                 UINT2 u2TlvLength,
                                                 BOOL1 *pbTlvDiscardFlag,
                                                 UINT2 *pu2MedSupTlvs));
/* LLDP-MED Remote Database updation routines*/
PUBLIC INT4 LldpMedTlvUpdtOrgSpecTlv     PROTO ((tLldpLocPortInfo * pLocPortInfo,
                                                 UINT1 *pu1TlvInfo,
                                                 UINT2 u2TlvLength,
                                                 UINT1 u1TlvSubType));

PUBLIC INT4 LldpMedDecodeCapTlv          PROTO ((tLldpRemoteNode *pRemoteNode,
                                                 UINT1 *pu1TlvInfo));

PUBLIC INT4 LldpMedDecodeNwPolicyTlv     PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1TlvInfo));

PUBLIC INT4 LldpMedDecodeHwRevTlv        PROTO ((tLldpRemoteNode *pRemoteNode,
                                                 UINT1 *pu1TlvInfo,
                                                 UINT2 u2TlvLength));

PUBLIC INT4 LldpMedDecodeSwRevTlv        PROTO ((tLldpRemoteNode *pRemoteNode,
                                                 UINT1 *pu1TlvInfo,
                                                 UINT2 u2TlvLength));

PUBLIC INT4 LldpMedDecodeFwRevTlv        PROTO ((tLldpRemoteNode *pRemoteNode,
                                                 UINT1 *pu1TlvInfo,
                                                 UINT2 u2TlvLength));

PUBLIC INT4 LldpMedDecodeSerialNumTlv    PROTO ((tLldpRemoteNode *pRemoteNode,
                                                 UINT1 *pu1TlvInfo,
                                                 UINT2 u2TlvLength));

PUBLIC INT4 LldpMedDecodeMfgNameTlv      PROTO ((tLldpRemoteNode *pRemoteNode,
                                                 UINT1 *pu1TlvInfo,
                                                 UINT2 u2TlvLength));

PUBLIC INT4 LldpMedDecodeModelNameTlv    PROTO ((tLldpRemoteNode *pRemoteNode,
                                                 UINT1 *pu1TlvInfo,
                                                 UINT2 u2TlvLength));

PUBLIC INT4 LldpMedDecodeAssetIdTlv      PROTO ((tLldpRemoteNode *pRemoteNode,
                                                 UINT1 *pu1TlvInfo,
                                                 UINT2 u2TlvLength));

PUBLIC INT4 LldpMedDecodeLocIdTlv        PROTO ((tLldpLocPortInfo *pLocPortInfo,
                                                 UINT1 *pu1TlvInfo,
                                                 UINT2 u2TlvLength));
PUBLIC VOID LldpUtlNotifyDcbxOnTxRxDisable PROTO ((INT4 , UINT4 ));

PUBLIC INT4 LldpMedDecodePowMDITlv       PROTO ((tLldpRemoteNode *pRemoteNode,
                                                 UINT1 *pu1TlvInfo));

INT4 LldpMedTlvProcNwPolicyTlv           PROTO ((tLldpLocPortInfo * pLocPortInfo,
                                                 UINT1 *pu1TlvInfo));

INT4 LldpMedTlvProcLocIdTlv              PROTO ((tLldpLocPortInfo * pLocPortInfo,
                                                 UINT1 *pu1TlvInfo,
                                                 UINT2 u2TlvLength));

INT4 LldpMedTlvProcPowMDITlv             PROTO ((tLldpLocPortInfo * pLocPortInfo,
                                                 UINT1 *pu1TlvInfo));
#ifdef DCBX_WANTED
PUBLIC VOID LldpUtlNotifyDcbxOnTxRxStatChg PROTO ((INT4, INT4));
#endif
INT4
 LldpRxUtlRBCmpV2ValidateSysData (tRBElem * e1, tRBElem * e2);

PUBLIC VOID LldpMedSendNotification      PROTO ((tLldpRemoteNode * pRemoteNode));

PUBLIC VOID LldpMedResetMedCapableFlag PROTO ((tLldpLocPortInfo *pLocPortInfo));
PUBLIC VOID LldpMedTriggerLocTlvChng PROTO ((tLldpLocPortInfo *pLocPortInfo));

VOID LldpUtlSvidDeleteOnUap (UINT4, UINT4);
VOID LldpUtlSvidUpdateOnUap (UINT4, UINT4);
PUBLIC INT4 LldpSendStagPdu PROTO ((tLldpLocPortInfo *));
INT4
LldpUtlGetStatsTxTaggedPortFramesTotal (INT4 i4LldpPortConfigPortNum,
                                        UINT4 *,tMacAddr );

INT4 
LldpUtlTestLldpCdcpTxStatus(VOID);
#endif /* _LLDPROTO_H */
/*--------------------------------------------------------------------------*/
/*                       End of the file  lldprot.h                         */
/*--------------------------------------------------------------------------*/
