/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: sdlldmdb.h,v 1.1 2015/09/09 13:30:09 siva Exp $
*
* Description: LLDP-MED Standard Mib Data base
*********************************************************************/
#ifndef _STDLLDPMDB_H
#define _STDLLDPMDB_H

UINT1 LldpXMedPortConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXMedLocMediaPolicyTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 LldpXMedLocLocationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 LldpXMedLocXPoEPSEPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXMedRemCapabilitiesTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXMedRemMediaPolicyTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 LldpXMedRemInventoryTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXMedRemLocationTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 LldpXMedRemXPoETableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXMedRemXPoEPSETableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXMedRemXPoEPDTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 stdlldpm [] ={1,0,8802,1,1,2,1,5,4795};
tSNMP_OID_TYPE stdlldpmOID = {9, stdlldpm};


UINT4 LldpXMedLocDeviceClass [ ] ={1,0,8802,1,1,2,1,5,4795,1,1,1};
UINT4 LldpXMedPortCapSupported [ ] ={1,0,8802,1,1,2,1,5,4795,1,1,2,1,1};
UINT4 LldpXMedPortConfigTLVsTxEnable [ ] ={1,0,8802,1,1,2,1,5,4795,1,1,2,1,2};
UINT4 LldpXMedPortConfigNotifEnable [ ] ={1,0,8802,1,1,2,1,5,4795,1,1,2,1,3};
UINT4 LldpXMedFastStartRepeatCount [ ] ={1,0,8802,1,1,2,1,5,4795,1,1,3};
UINT4 LldpXMedLocMediaPolicyAppType [ ] ={1,0,8802,1,1,2,1,5,4795,1,2,1,1,1};
UINT4 LldpXMedLocMediaPolicyVlanID [ ] ={1,0,8802,1,1,2,1,5,4795,1,2,1,1,2};
UINT4 LldpXMedLocMediaPolicyPriority [ ] ={1,0,8802,1,1,2,1,5,4795,1,2,1,1,3};
UINT4 LldpXMedLocMediaPolicyDscp [ ] ={1,0,8802,1,1,2,1,5,4795,1,2,1,1,4};
UINT4 LldpXMedLocMediaPolicyUnknown [ ] ={1,0,8802,1,1,2,1,5,4795,1,2,1,1,5};
UINT4 LldpXMedLocMediaPolicyTagged [ ] ={1,0,8802,1,1,2,1,5,4795,1,2,1,1,6};
UINT4 LldpXMedLocHardwareRev [ ] ={1,0,8802,1,1,2,1,5,4795,1,2,2};
UINT4 LldpXMedLocFirmwareRev [ ] ={1,0,8802,1,1,2,1,5,4795,1,2,3};
UINT4 LldpXMedLocSoftwareRev [ ] ={1,0,8802,1,1,2,1,5,4795,1,2,4};
UINT4 LldpXMedLocSerialNum [ ] ={1,0,8802,1,1,2,1,5,4795,1,2,5};
UINT4 LldpXMedLocMfgName [ ] ={1,0,8802,1,1,2,1,5,4795,1,2,6};
UINT4 LldpXMedLocModelName [ ] ={1,0,8802,1,1,2,1,5,4795,1,2,7};
UINT4 LldpXMedLocAssetID [ ] ={1,0,8802,1,1,2,1,5,4795,1,2,8};
UINT4 LldpXMedLocLocationSubtype [ ] ={1,0,8802,1,1,2,1,5,4795,1,2,9,1,1};
UINT4 LldpXMedLocLocationInfo [ ] ={1,0,8802,1,1,2,1,5,4795,1,2,9,1,2};
UINT4 LldpXMedLocXPoEDeviceType [ ] ={1,0,8802,1,1,2,1,5,4795,1,2,10};
UINT4 LldpXMedLocXPoEPSEPortPowerAv [ ] ={1,0,8802,1,1,2,1,5,4795,1,2,11,1,1};
UINT4 LldpXMedLocXPoEPSEPortPDPriority [ ] ={1,0,8802,1,1,2,1,5,4795,1,2,11,1,2};
UINT4 LldpXMedLocXPoEPSEPowerSource [ ] ={1,0,8802,1,1,2,1,5,4795,1,2,12};
UINT4 LldpXMedLocXPoEPDPowerReq [ ] ={1,0,8802,1,1,2,1,5,4795,1,2,13};
UINT4 LldpXMedLocXPoEPDPowerSource [ ] ={1,0,8802,1,1,2,1,5,4795,1,2,14};
UINT4 LldpXMedLocXPoEPDPowerPriority [ ] ={1,0,8802,1,1,2,1,5,4795,1,2,15};
UINT4 LldpXMedRemCapSupported [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,1,1,1};
UINT4 LldpXMedRemCapCurrent [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,1,1,2};
UINT4 LldpXMedRemDeviceClass [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,1,1,3};
UINT4 LldpXMedRemMediaPolicyAppType [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,2,1,1};
UINT4 LldpXMedRemMediaPolicyVlanID [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,2,1,2};
UINT4 LldpXMedRemMediaPolicyPriority [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,2,1,3};
UINT4 LldpXMedRemMediaPolicyDscp [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,2,1,4};
UINT4 LldpXMedRemMediaPolicyUnknown [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,2,1,5};
UINT4 LldpXMedRemMediaPolicyTagged [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,2,1,6};
UINT4 LldpXMedRemHardwareRev [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,3,1,1};
UINT4 LldpXMedRemFirmwareRev [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,3,1,2};
UINT4 LldpXMedRemSoftwareRev [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,3,1,3};
UINT4 LldpXMedRemSerialNum [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,3,1,4};
UINT4 LldpXMedRemMfgName [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,3,1,5};
UINT4 LldpXMedRemModelName [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,3,1,6};
UINT4 LldpXMedRemAssetID [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,3,1,7};
UINT4 LldpXMedRemLocationSubtype [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,4,1,1};
UINT4 LldpXMedRemLocationInfo [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,4,1,2};
UINT4 LldpXMedRemXPoEDeviceType [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,5,1,1};
UINT4 LldpXMedRemXPoEPSEPowerAv [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,6,1,1};
UINT4 LldpXMedRemXPoEPSEPowerSource [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,6,1,2};
UINT4 LldpXMedRemXPoEPSEPowerPriority [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,6,1,3};
UINT4 LldpXMedRemXPoEPDPowerReq [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,7,1,1};
UINT4 LldpXMedRemXPoEPDPowerSource [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,7,1,2};
UINT4 LldpXMedRemXPoEPDPowerPriority [ ] ={1,0,8802,1,1,2,1,5,4795,1,3,7,1,3};




tMbDbEntry stdlldpmMibEntry[]= {

{{12,LldpXMedLocDeviceClass}, NULL, LldpXMedLocDeviceClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{14,LldpXMedPortCapSupported}, GetNextIndexLldpXMedPortConfigTable, LldpXMedPortCapSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpXMedPortConfigTableINDEX, 1, 0, 0, NULL},

{{14,LldpXMedPortConfigTLVsTxEnable}, GetNextIndexLldpXMedPortConfigTable, LldpXMedPortConfigTLVsTxEnableGet, LldpXMedPortConfigTLVsTxEnableSet, LldpXMedPortConfigTLVsTxEnableTest, LldpXMedPortConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, LldpXMedPortConfigTableINDEX, 1, 0, 0, NULL},

{{14,LldpXMedPortConfigNotifEnable}, GetNextIndexLldpXMedPortConfigTable, LldpXMedPortConfigNotifEnableGet, LldpXMedPortConfigNotifEnableSet, LldpXMedPortConfigNotifEnableTest, LldpXMedPortConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpXMedPortConfigTableINDEX, 1, 0, 0, "2"},

{{12,LldpXMedFastStartRepeatCount}, NULL, LldpXMedFastStartRepeatCountGet, LldpXMedFastStartRepeatCountSet, LldpXMedFastStartRepeatCountTest, LldpXMedFastStartRepeatCountDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{14,LldpXMedLocMediaPolicyAppType}, GetNextIndexLldpXMedLocMediaPolicyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, LldpXMedLocMediaPolicyTableINDEX, 2, 0, 0, NULL},

{{14,LldpXMedLocMediaPolicyVlanID}, GetNextIndexLldpXMedLocMediaPolicyTable, LldpXMedLocMediaPolicyVlanIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpXMedLocMediaPolicyTableINDEX, 2, 0, 0, NULL},

{{14,LldpXMedLocMediaPolicyPriority}, GetNextIndexLldpXMedLocMediaPolicyTable, LldpXMedLocMediaPolicyPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpXMedLocMediaPolicyTableINDEX, 2, 0, 0, NULL},

{{14,LldpXMedLocMediaPolicyDscp}, GetNextIndexLldpXMedLocMediaPolicyTable, LldpXMedLocMediaPolicyDscpGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpXMedLocMediaPolicyTableINDEX, 2, 0, 0, NULL},

{{14,LldpXMedLocMediaPolicyUnknown}, GetNextIndexLldpXMedLocMediaPolicyTable, LldpXMedLocMediaPolicyUnknownGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXMedLocMediaPolicyTableINDEX, 2, 0, 0, NULL},

{{14,LldpXMedLocMediaPolicyTagged}, GetNextIndexLldpXMedLocMediaPolicyTable, LldpXMedLocMediaPolicyTaggedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXMedLocMediaPolicyTableINDEX, 2, 0, 0, NULL},

{{12,LldpXMedLocHardwareRev}, NULL, LldpXMedLocHardwareRevGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,LldpXMedLocFirmwareRev}, NULL, LldpXMedLocFirmwareRevGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,LldpXMedLocSoftwareRev}, NULL, LldpXMedLocSoftwareRevGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,LldpXMedLocSerialNum}, NULL, LldpXMedLocSerialNumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,LldpXMedLocMfgName}, NULL, LldpXMedLocMfgNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,LldpXMedLocModelName}, NULL, LldpXMedLocModelNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,LldpXMedLocAssetID}, NULL, LldpXMedLocAssetIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{14,LldpXMedLocLocationSubtype}, GetNextIndexLldpXMedLocLocationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, LldpXMedLocLocationTableINDEX, 2, 0, 0, NULL},

{{14,LldpXMedLocLocationInfo}, GetNextIndexLldpXMedLocLocationTable, LldpXMedLocLocationInfoGet, LldpXMedLocLocationInfoSet, LldpXMedLocLocationInfoTest, LldpXMedLocLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, LldpXMedLocLocationTableINDEX, 2, 0, 0, ""},

{{12,LldpXMedLocXPoEDeviceType}, NULL, LldpXMedLocXPoEDeviceTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{14,LldpXMedLocXPoEPSEPortPowerAv}, GetNextIndexLldpXMedLocXPoEPSEPortTable, LldpXMedLocXPoEPSEPortPowerAvGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, LldpXMedLocXPoEPSEPortTableINDEX, 1, 0, 0, NULL},

{{14,LldpXMedLocXPoEPSEPortPDPriority}, GetNextIndexLldpXMedLocXPoEPSEPortTable, LldpXMedLocXPoEPSEPortPDPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXMedLocXPoEPSEPortTableINDEX, 1, 0, 0, NULL},

{{12,LldpXMedLocXPoEPSEPowerSource}, NULL, LldpXMedLocXPoEPSEPowerSourceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,LldpXMedLocXPoEPDPowerReq}, NULL, LldpXMedLocXPoEPDPowerReqGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,LldpXMedLocXPoEPDPowerSource}, NULL, LldpXMedLocXPoEPDPowerSourceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,LldpXMedLocXPoEPDPowerPriority}, NULL, LldpXMedLocXPoEPDPowerPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{14,LldpXMedRemCapSupported}, GetNextIndexLldpXMedRemCapabilitiesTable, LldpXMedRemCapSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpXMedRemCapabilitiesTableINDEX, 3, 0, 0, NULL},

{{14,LldpXMedRemCapCurrent}, GetNextIndexLldpXMedRemCapabilitiesTable, LldpXMedRemCapCurrentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpXMedRemCapabilitiesTableINDEX, 3, 0, 0, NULL},

{{14,LldpXMedRemDeviceClass}, GetNextIndexLldpXMedRemCapabilitiesTable, LldpXMedRemDeviceClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXMedRemCapabilitiesTableINDEX, 3, 0, 0, NULL},

{{14,LldpXMedRemMediaPolicyAppType}, GetNextIndexLldpXMedRemMediaPolicyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, LldpXMedRemMediaPolicyTableINDEX, 4, 0, 0, NULL},

{{14,LldpXMedRemMediaPolicyVlanID}, GetNextIndexLldpXMedRemMediaPolicyTable, LldpXMedRemMediaPolicyVlanIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpXMedRemMediaPolicyTableINDEX, 4, 0, 0, NULL},

{{14,LldpXMedRemMediaPolicyPriority}, GetNextIndexLldpXMedRemMediaPolicyTable, LldpXMedRemMediaPolicyPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpXMedRemMediaPolicyTableINDEX, 4, 0, 0, NULL},

{{14,LldpXMedRemMediaPolicyDscp}, GetNextIndexLldpXMedRemMediaPolicyTable, LldpXMedRemMediaPolicyDscpGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LldpXMedRemMediaPolicyTableINDEX, 4, 0, 0, NULL},

{{14,LldpXMedRemMediaPolicyUnknown}, GetNextIndexLldpXMedRemMediaPolicyTable, LldpXMedRemMediaPolicyUnknownGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXMedRemMediaPolicyTableINDEX, 4, 0, 0, NULL},

{{14,LldpXMedRemMediaPolicyTagged}, GetNextIndexLldpXMedRemMediaPolicyTable, LldpXMedRemMediaPolicyTaggedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXMedRemMediaPolicyTableINDEX, 4, 0, 0, NULL},

{{14,LldpXMedRemHardwareRev}, GetNextIndexLldpXMedRemInventoryTable, LldpXMedRemHardwareRevGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpXMedRemInventoryTableINDEX, 3, 0, 0, NULL},

{{14,LldpXMedRemFirmwareRev}, GetNextIndexLldpXMedRemInventoryTable, LldpXMedRemFirmwareRevGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpXMedRemInventoryTableINDEX, 3, 0, 0, NULL},

{{14,LldpXMedRemSoftwareRev}, GetNextIndexLldpXMedRemInventoryTable, LldpXMedRemSoftwareRevGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpXMedRemInventoryTableINDEX, 3, 0, 0, NULL},

{{14,LldpXMedRemSerialNum}, GetNextIndexLldpXMedRemInventoryTable, LldpXMedRemSerialNumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpXMedRemInventoryTableINDEX, 3, 0, 0, NULL},

{{14,LldpXMedRemMfgName}, GetNextIndexLldpXMedRemInventoryTable, LldpXMedRemMfgNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpXMedRemInventoryTableINDEX, 3, 0, 0, NULL},

{{14,LldpXMedRemModelName}, GetNextIndexLldpXMedRemInventoryTable, LldpXMedRemModelNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpXMedRemInventoryTableINDEX, 3, 0, 0, NULL},

{{14,LldpXMedRemAssetID}, GetNextIndexLldpXMedRemInventoryTable, LldpXMedRemAssetIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpXMedRemInventoryTableINDEX, 3, 0, 0, NULL},

{{14,LldpXMedRemLocationSubtype}, GetNextIndexLldpXMedRemLocationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, LldpXMedRemLocationTableINDEX, 4, 0, 0, NULL},

{{14,LldpXMedRemLocationInfo}, GetNextIndexLldpXMedRemLocationTable, LldpXMedRemLocationInfoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpXMedRemLocationTableINDEX, 4, 0, 0, NULL},

{{14,LldpXMedRemXPoEDeviceType}, GetNextIndexLldpXMedRemXPoETable, LldpXMedRemXPoEDeviceTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXMedRemXPoETableINDEX, 3, 0, 0, NULL},

{{14,LldpXMedRemXPoEPSEPowerAv}, GetNextIndexLldpXMedRemXPoEPSETable, LldpXMedRemXPoEPSEPowerAvGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, LldpXMedRemXPoEPSETableINDEX, 3, 0, 0, NULL},

{{14,LldpXMedRemXPoEPSEPowerSource}, GetNextIndexLldpXMedRemXPoEPSETable, LldpXMedRemXPoEPSEPowerSourceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXMedRemXPoEPSETableINDEX, 3, 0, 0, NULL},

{{14,LldpXMedRemXPoEPSEPowerPriority}, GetNextIndexLldpXMedRemXPoEPSETable, LldpXMedRemXPoEPSEPowerPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXMedRemXPoEPSETableINDEX, 3, 0, 0, NULL},

{{14,LldpXMedRemXPoEPDPowerReq}, GetNextIndexLldpXMedRemXPoEPDTable, LldpXMedRemXPoEPDPowerReqGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, LldpXMedRemXPoEPDTableINDEX, 3, 0, 0, NULL},

{{14,LldpXMedRemXPoEPDPowerSource}, GetNextIndexLldpXMedRemXPoEPDTable, LldpXMedRemXPoEPDPowerSourceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXMedRemXPoEPDTableINDEX, 3, 0, 0, NULL},

{{14,LldpXMedRemXPoEPDPowerPriority}, GetNextIndexLldpXMedRemXPoEPDTable, LldpXMedRemXPoEPDPowerPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXMedRemXPoEPDTableINDEX, 3, 0, 0, NULL},
};
tMibData stdlldpmEntry = { 52, stdlldpmMibEntry };

#endif /* _STDLLDPMDB_H */

