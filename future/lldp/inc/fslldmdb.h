/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fslldmdb.h,v 1.2 2016/01/28 11:09:06 siva Exp $
*
* Description: LLDP-MED Protocol Mib Data base
*********************************************************************/
#ifndef _FSLLDMDB_H
#define _FSLLDMDB_H

UINT1 FsLldpMedPortConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsLldpMedLocMediaPolicyTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsLldpMedLocLocationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsLldpXMedRemCapabilitiesTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsLldpXMedRemMediaPolicyTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsLldpXMedRemInventoryTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsLldpMedStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsLldpXMedRemLocationTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsLldpXMedRemXPoEPDTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fslldm [] ={1,3,6,1,4,1,29601,2,101};
tSNMP_OID_TYPE fslldmOID = {9, fslldm};


UINT4 FsLldpMedPortCapSupported [ ] ={1,3,6,1,4,1,29601,2,101,1,1,1,1};
UINT4 FsLldpMedPortConfigTLVsTxEnable [ ] ={1,3,6,1,4,1,29601,2,101,1,1,1,2};
UINT4 FsLldpMedLocMediaPolicyAppType [ ] ={1,3,6,1,4,1,29601,2,101,1,2,1,1};
UINT4 FsLldpMedLocMediaPolicyVlanID [ ] ={1,3,6,1,4,1,29601,2,101,1,2,1,2};
UINT4 FsLldpMedLocMediaPolicyPriority [ ] ={1,3,6,1,4,1,29601,2,101,1,2,1,3};
UINT4 FsLldpMedLocMediaPolicyDscp [ ] ={1,3,6,1,4,1,29601,2,101,1,2,1,4};
UINT4 FsLldpMedLocMediaPolicyUnknown [ ] ={1,3,6,1,4,1,29601,2,101,1,2,1,5};
UINT4 FsLldpMedLocMediaPolicyTagged [ ] ={1,3,6,1,4,1,29601,2,101,1,2,1,6};
UINT4 FsLldpMedLocMediaPolicyRowStatus [ ] ={1,3,6,1,4,1,29601,2,101,1,2,1,7};
UINT4 FsLldpMedLocLocationRowStatus [ ] ={1,3,6,1,4,1,29601,2,101,1,3,1,1};
UINT4 FsLldpXMedRemCapSupported [ ] ={1,3,6,1,4,1,29601,2,101,2,1,1,1};
UINT4 FsLldpXMedRemCapCurrent [ ] ={1,3,6,1,4,1,29601,2,101,2,1,1,2};
UINT4 FsLldpXMedRemDeviceClass [ ] ={1,3,6,1,4,1,29601,2,101,2,1,1,3};
UINT4 FsLldpXMedRemMediaPolicyAppType [ ] ={1,3,6,1,4,1,29601,2,101,2,2,1,1};
UINT4 FsLldpXMedRemMediaPolicyVlanID [ ] ={1,3,6,1,4,1,29601,2,101,2,2,1,2};
UINT4 FsLldpXMedRemMediaPolicyPriority [ ] ={1,3,6,1,4,1,29601,2,101,2,2,1,3};
UINT4 FsLldpXMedRemMediaPolicyDscp [ ] ={1,3,6,1,4,1,29601,2,101,2,2,1,4};
UINT4 FsLldpXMedRemMediaPolicyUnknown [ ] ={1,3,6,1,4,1,29601,2,101,2,2,1,5};
UINT4 FsLldpXMedRemMediaPolicyTagged [ ] ={1,3,6,1,4,1,29601,2,101,2,2,1,6};
UINT4 FsLldpXMedRemHardwareRev [ ] ={1,3,6,1,4,1,29601,2,101,2,3,1,1};
UINT4 FsLldpXMedRemFirmwareRev [ ] ={1,3,6,1,4,1,29601,2,101,2,3,1,2};
UINT4 FsLldpXMedRemSoftwareRev [ ] ={1,3,6,1,4,1,29601,2,101,2,3,1,3};
UINT4 FsLldpXMedRemSerialNum [ ] ={1,3,6,1,4,1,29601,2,101,2,3,1,4};
UINT4 FsLldpXMedRemMfgName [ ] ={1,3,6,1,4,1,29601,2,101,2,3,1,5};
UINT4 FsLldpXMedRemModelName [ ] ={1,3,6,1,4,1,29601,2,101,2,3,1,6};
UINT4 FsLldpXMedRemAssetID [ ] ={1,3,6,1,4,1,29601,2,101,2,3,1,7};
UINT4 FsLldpMedStatsIfIndex [ ] ={1,3,6,1,4,1,29601,2,101,3,1,1,1};
UINT4 FsLldpMedStatsDestMACAddress [ ] ={1,3,6,1,4,1,29601,2,101,3,1,1,2};
UINT4 FsLldpMedStatsTxFramesTotal [ ] ={1,3,6,1,4,1,29601,2,101,3,1,1,3};
UINT4 FsLldpMedStatsRxFramesTotal [ ] ={1,3,6,1,4,1,29601,2,101,3,1,1,4};
UINT4 FsLldpMedStatsRxFramesDiscardedTotal [ ] ={1,3,6,1,4,1,29601,2,101,3,1,1,5};
UINT4 FsLldpMedStatsRxTLVsDiscardedTotal [ ] ={1,3,6,1,4,1,29601,2,101,3,1,1,6};
UINT4 FsLldpMedStatsRxCapTLVsDiscarded [ ] ={1,3,6,1,4,1,29601,2,101,3,1,1,7};
UINT4 FsLldpMedStatsRxPolicyTLVsDiscarded [ ] ={1,3,6,1,4,1,29601,2,101,3,1,1,8};
UINT4 FsLldpMedStatsRxInventoryTLVsDiscarded [ ] ={1,3,6,1,4,1,29601,2,101,3,1,1,9};
UINT4 FsLldpMedStatsRxLocationTLVsDiscarded [ ] ={1,3,6,1,4,1,29601,2,101,3,1,1,10};
UINT4 FsLldpMedStatsRxExPowerMDITLVsDiscarded [ ] ={1,3,6,1,4,1,29601,2,101,3,1,1,11};
UINT4 FsLldpMedStatsRxCapTLVsDiscardedReason [ ] ={1,3,6,1,4,1,29601,2,101,3,1,1,12};
UINT4 FsLldpMedStatsRxPolicyDiscardedReason [ ] ={1,3,6,1,4,1,29601,2,101,3,1,1,13};
UINT4 FsLldpMedStatsRxInventoryDiscardedReason [ ] ={1,3,6,1,4,1,29601,2,101,3,1,1,14};
UINT4 FsLldpMedStatsRxLocationDiscardedReason [ ] ={1,3,6,1,4,1,29601,2,101,3,1,1,15};
UINT4 FsLldpMedStatsRxExPowerDiscardedReason [ ] ={1,3,6,1,4,1,29601,2,101,3,1,1,16};
UINT4 FsLldpMedClearStats [ ] ={1,3,6,1,4,1,29601,2,101,3,2};
UINT4 FsLldpXMedMediaPolicyAppType [ ] ={1,3,6,1,4,1,29601,2,101,5,1,1};
UINT4 FsLldpXMedRemLocationInfo [ ] ={1,3,6,1,4,1,29601,2,101,2,4,1,1};
UINT4 FsLldpXMedRemXPoEDeviceType [ ] ={1,3,6,1,4,1,29601,2,101,2,5,1,1};
UINT4 FsLldpXMedRemXPoEPDPowerReq [ ] ={1,3,6,1,4,1,29601,2,101,2,5,1,2};
UINT4 FsLldpXMedRemXPoEPDPowerSource [ ] ={1,3,6,1,4,1,29601,2,101,2,5,1,3};
UINT4 FsLldpXMedRemXPoEPDPowerPriority [ ] ={1,3,6,1,4,1,29601,2,101,2,5,1,4};




tMbDbEntry fslldmMibEntry[]= {

{{13,FsLldpMedPortCapSupported}, GetNextIndexFsLldpMedPortConfigTable, FsLldpMedPortCapSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLldpMedPortConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedPortConfigTLVsTxEnable}, GetNextIndexFsLldpMedPortConfigTable, FsLldpMedPortConfigTLVsTxEnableGet, FsLldpMedPortConfigTLVsTxEnableSet, FsLldpMedPortConfigTLVsTxEnableTest, FsLldpMedPortConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsLldpMedPortConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedLocMediaPolicyAppType}, GetNextIndexFsLldpMedLocMediaPolicyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsLldpMedLocMediaPolicyTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedLocMediaPolicyVlanID}, GetNextIndexFsLldpMedLocMediaPolicyTable, FsLldpMedLocMediaPolicyVlanIDGet, FsLldpMedLocMediaPolicyVlanIDSet, FsLldpMedLocMediaPolicyVlanIDTest, FsLldpMedLocMediaPolicyTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsLldpMedLocMediaPolicyTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedLocMediaPolicyPriority}, GetNextIndexFsLldpMedLocMediaPolicyTable, FsLldpMedLocMediaPolicyPriorityGet, FsLldpMedLocMediaPolicyPrioritySet, FsLldpMedLocMediaPolicyPriorityTest, FsLldpMedLocMediaPolicyTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsLldpMedLocMediaPolicyTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedLocMediaPolicyDscp}, GetNextIndexFsLldpMedLocMediaPolicyTable, FsLldpMedLocMediaPolicyDscpGet, FsLldpMedLocMediaPolicyDscpSet, FsLldpMedLocMediaPolicyDscpTest, FsLldpMedLocMediaPolicyTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsLldpMedLocMediaPolicyTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedLocMediaPolicyUnknown}, GetNextIndexFsLldpMedLocMediaPolicyTable, FsLldpMedLocMediaPolicyUnknownGet, FsLldpMedLocMediaPolicyUnknownSet, FsLldpMedLocMediaPolicyUnknownTest, FsLldpMedLocMediaPolicyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLldpMedLocMediaPolicyTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedLocMediaPolicyTagged}, GetNextIndexFsLldpMedLocMediaPolicyTable, FsLldpMedLocMediaPolicyTaggedGet, FsLldpMedLocMediaPolicyTaggedSet, FsLldpMedLocMediaPolicyTaggedTest, FsLldpMedLocMediaPolicyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLldpMedLocMediaPolicyTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedLocMediaPolicyRowStatus}, GetNextIndexFsLldpMedLocMediaPolicyTable, FsLldpMedLocMediaPolicyRowStatusGet, FsLldpMedLocMediaPolicyRowStatusSet, FsLldpMedLocMediaPolicyRowStatusTest, FsLldpMedLocMediaPolicyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLldpMedLocMediaPolicyTableINDEX, 2, 0, 1, NULL},

{{13,FsLldpMedLocLocationRowStatus}, GetNextIndexFsLldpMedLocLocationTable, FsLldpMedLocLocationRowStatusGet, FsLldpMedLocLocationRowStatusSet, FsLldpMedLocLocationRowStatusTest, FsLldpMedLocLocationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLldpMedLocLocationTableINDEX, 2, 0, 1, NULL},

{{13,FsLldpXMedRemCapSupported}, GetNextIndexFsLldpXMedRemCapabilitiesTable, FsLldpXMedRemCapSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLldpXMedRemCapabilitiesTableINDEX, 4, 0, 0, NULL},

{{13,FsLldpXMedRemCapCurrent}, GetNextIndexFsLldpXMedRemCapabilitiesTable, FsLldpXMedRemCapCurrentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLldpXMedRemCapabilitiesTableINDEX, 4, 0, 0, NULL},

{{13,FsLldpXMedRemDeviceClass}, GetNextIndexFsLldpXMedRemCapabilitiesTable, FsLldpXMedRemDeviceClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLldpXMedRemCapabilitiesTableINDEX, 4, 0, 0, NULL},

{{13,FsLldpXMedRemMediaPolicyAppType}, GetNextIndexFsLldpXMedRemMediaPolicyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsLldpXMedRemMediaPolicyTableINDEX, 5, 0, 0, NULL},

{{13,FsLldpXMedRemMediaPolicyVlanID}, GetNextIndexFsLldpXMedRemMediaPolicyTable, FsLldpXMedRemMediaPolicyVlanIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsLldpXMedRemMediaPolicyTableINDEX, 5, 0, 0, NULL},

{{13,FsLldpXMedRemMediaPolicyPriority}, GetNextIndexFsLldpXMedRemMediaPolicyTable, FsLldpXMedRemMediaPolicyPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsLldpXMedRemMediaPolicyTableINDEX, 5, 0, 0, NULL},

{{13,FsLldpXMedRemMediaPolicyDscp}, GetNextIndexFsLldpXMedRemMediaPolicyTable, FsLldpXMedRemMediaPolicyDscpGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsLldpXMedRemMediaPolicyTableINDEX, 5, 0, 0, NULL},

{{13,FsLldpXMedRemMediaPolicyUnknown}, GetNextIndexFsLldpXMedRemMediaPolicyTable, FsLldpXMedRemMediaPolicyUnknownGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLldpXMedRemMediaPolicyTableINDEX, 5, 0, 0, NULL},

{{13,FsLldpXMedRemMediaPolicyTagged}, GetNextIndexFsLldpXMedRemMediaPolicyTable, FsLldpXMedRemMediaPolicyTaggedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLldpXMedRemMediaPolicyTableINDEX, 5, 0, 0, NULL},

{{13,FsLldpXMedRemHardwareRev}, GetNextIndexFsLldpXMedRemInventoryTable, FsLldpXMedRemHardwareRevGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLldpXMedRemInventoryTableINDEX, 4, 0, 0, NULL},

{{13,FsLldpXMedRemFirmwareRev}, GetNextIndexFsLldpXMedRemInventoryTable, FsLldpXMedRemFirmwareRevGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLldpXMedRemInventoryTableINDEX, 4, 0, 0, NULL},

{{13,FsLldpXMedRemSoftwareRev}, GetNextIndexFsLldpXMedRemInventoryTable, FsLldpXMedRemSoftwareRevGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLldpXMedRemInventoryTableINDEX, 4, 0, 0, NULL},

{{13,FsLldpXMedRemSerialNum}, GetNextIndexFsLldpXMedRemInventoryTable, FsLldpXMedRemSerialNumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLldpXMedRemInventoryTableINDEX, 4, 0, 0, NULL},

{{13,FsLldpXMedRemMfgName}, GetNextIndexFsLldpXMedRemInventoryTable, FsLldpXMedRemMfgNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLldpXMedRemInventoryTableINDEX, 4, 0, 0, NULL},

{{13,FsLldpXMedRemModelName}, GetNextIndexFsLldpXMedRemInventoryTable, FsLldpXMedRemModelNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLldpXMedRemInventoryTableINDEX, 4, 0, 0, NULL},

{{13,FsLldpXMedRemAssetID}, GetNextIndexFsLldpXMedRemInventoryTable, FsLldpXMedRemAssetIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLldpXMedRemInventoryTableINDEX, 4, 0, 0, NULL},

{{13,FsLldpXMedRemLocationInfo}, GetNextIndexFsLldpXMedRemLocationTable, FsLldpXMedRemLocationInfoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLldpXMedRemLocationTableINDEX, 5, 0, 0, NULL},

{{13,FsLldpXMedRemXPoEDeviceType}, GetNextIndexFsLldpXMedRemXPoEPDTable, FsLldpXMedRemXPoEDeviceTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLldpXMedRemXPoEPDTableINDEX, 4, 0, 0, NULL},

{{13,FsLldpXMedRemXPoEPDPowerReq}, GetNextIndexFsLldpXMedRemXPoEPDTable, FsLldpXMedRemXPoEPDPowerReqGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsLldpXMedRemXPoEPDTableINDEX, 4, 0, 0, NULL},

{{13,FsLldpXMedRemXPoEPDPowerSource}, GetNextIndexFsLldpXMedRemXPoEPDTable, FsLldpXMedRemXPoEPDPowerSourceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLldpXMedRemXPoEPDTableINDEX, 4, 0, 0, NULL},

{{13,FsLldpXMedRemXPoEPDPowerPriority}, GetNextIndexFsLldpXMedRemXPoEPDTable, FsLldpXMedRemXPoEPDPowerPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLldpXMedRemXPoEPDTableINDEX, 4, 0, 0, NULL},

{{13,FsLldpMedStatsIfIndex}, GetNextIndexFsLldpMedStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsLldpMedStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedStatsDestMACAddress}, GetNextIndexFsLldpMedStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsLldpMedStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedStatsTxFramesTotal}, GetNextIndexFsLldpMedStatsTable, FsLldpMedStatsTxFramesTotalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLldpMedStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedStatsRxFramesTotal}, GetNextIndexFsLldpMedStatsTable, FsLldpMedStatsRxFramesTotalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLldpMedStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedStatsRxFramesDiscardedTotal}, GetNextIndexFsLldpMedStatsTable, FsLldpMedStatsRxFramesDiscardedTotalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLldpMedStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedStatsRxTLVsDiscardedTotal}, GetNextIndexFsLldpMedStatsTable, FsLldpMedStatsRxTLVsDiscardedTotalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLldpMedStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedStatsRxCapTLVsDiscarded}, GetNextIndexFsLldpMedStatsTable, FsLldpMedStatsRxCapTLVsDiscardedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLldpMedStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedStatsRxPolicyTLVsDiscarded}, GetNextIndexFsLldpMedStatsTable, FsLldpMedStatsRxPolicyTLVsDiscardedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLldpMedStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedStatsRxInventoryTLVsDiscarded}, GetNextIndexFsLldpMedStatsTable, FsLldpMedStatsRxInventoryTLVsDiscardedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLldpMedStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedStatsRxLocationTLVsDiscarded}, GetNextIndexFsLldpMedStatsTable, FsLldpMedStatsRxLocationTLVsDiscardedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLldpMedStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedStatsRxExPowerMDITLVsDiscarded}, GetNextIndexFsLldpMedStatsTable, FsLldpMedStatsRxExPowerMDITLVsDiscardedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLldpMedStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedStatsRxCapTLVsDiscardedReason}, GetNextIndexFsLldpMedStatsTable, FsLldpMedStatsRxCapTLVsDiscardedReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLldpMedStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedStatsRxPolicyDiscardedReason}, GetNextIndexFsLldpMedStatsTable, FsLldpMedStatsRxPolicyDiscardedReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLldpMedStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedStatsRxInventoryDiscardedReason}, GetNextIndexFsLldpMedStatsTable, FsLldpMedStatsRxInventoryDiscardedReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLldpMedStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedStatsRxLocationDiscardedReason}, GetNextIndexFsLldpMedStatsTable, FsLldpMedStatsRxLocationDiscardedReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLldpMedStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsLldpMedStatsRxExPowerDiscardedReason}, GetNextIndexFsLldpMedStatsTable, FsLldpMedStatsRxExPowerDiscardedReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLldpMedStatsTableINDEX, 2, 0, 0, NULL},

{{11,FsLldpMedClearStats}, NULL, FsLldpMedClearStatsGet, FsLldpMedClearStatsSet, FsLldpMedClearStatsTest, FsLldpMedClearStatsDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{12,FsLldpXMedMediaPolicyAppType}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData fslldmEntry = { 49, fslldmMibEntry };

#endif /* _FSLLDMDB_H */

