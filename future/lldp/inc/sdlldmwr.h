/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: sdlldmwr.h,v 1.1 2015/09/09 13:30:09 siva Exp $
*
* Description: LLDP-MED Proto Types for Standard Wrapper Functions
*********************************************************************/

#ifndef _STDLLDPMWR_H
#define _STDLLDPMWR_H

VOID RegisterSTDLLDPM(VOID);

VOID UnRegisterSTDLLDPM(VOID);
INT4 LldpXMedLocDeviceClassGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXMedPortConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXMedPortCapSupportedGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedPortConfigTLVsTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedPortConfigNotifEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedPortConfigTLVsTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedPortConfigNotifEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedPortConfigTLVsTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXMedPortConfigNotifEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXMedPortConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 LldpXMedFastStartRepeatCountGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedFastStartRepeatCountSet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedFastStartRepeatCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXMedFastStartRepeatCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexLldpXMedLocMediaPolicyTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXMedLocMediaPolicyVlanIDGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedLocMediaPolicyPriorityGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedLocMediaPolicyDscpGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedLocMediaPolicyUnknownGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedLocMediaPolicyTaggedGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedLocHardwareRevGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedLocFirmwareRevGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedLocSoftwareRevGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedLocSerialNumGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedLocMfgNameGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedLocModelNameGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedLocAssetIDGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXMedLocLocationTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXMedLocLocationInfoGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedLocLocationInfoSet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedLocLocationInfoTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXMedLocLocationTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 LldpXMedLocXPoEDeviceTypeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXMedLocXPoEPSEPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXMedLocXPoEPSEPortPowerAvGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedLocXPoEPSEPortPDPriorityGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedLocXPoEPSEPowerSourceGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedLocXPoEPDPowerReqGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedLocXPoEPDPowerSourceGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedLocXPoEPDPowerPriorityGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXMedRemCapabilitiesTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXMedRemCapSupportedGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedRemCapCurrentGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedRemDeviceClassGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXMedRemMediaPolicyTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXMedRemMediaPolicyVlanIDGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedRemMediaPolicyPriorityGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedRemMediaPolicyDscpGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedRemMediaPolicyUnknownGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedRemMediaPolicyTaggedGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXMedRemInventoryTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXMedRemHardwareRevGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedRemFirmwareRevGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedRemSoftwareRevGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedRemSerialNumGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedRemMfgNameGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedRemModelNameGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedRemAssetIDGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXMedRemLocationTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXMedRemLocationInfoGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXMedRemXPoETable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXMedRemXPoEDeviceTypeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXMedRemXPoEPSETable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXMedRemXPoEPSEPowerAvGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedRemXPoEPSEPowerSourceGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedRemXPoEPSEPowerPriorityGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXMedRemXPoEPDTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXMedRemXPoEPDPowerReqGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedRemXPoEPDPowerSourceGet(tSnmpIndex *, tRetVal *);
INT4 LldpXMedRemXPoEPDPowerPriorityGet(tSnmpIndex *, tRetVal *);
#endif /* _STDLLDPMWR_H */
