/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: sdlldmlw.h,v 1.1 2015/09/09 13:30:09 siva Exp $
*
* Description: LLDP-MED Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXMedLocDeviceClass ARG_LIST((INT4 *));

/* Proto Validate Index Instance for LldpXMedPortConfigTable. */
INT1
nmhValidateIndexInstanceLldpXMedPortConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXMedPortConfigTable  */

INT1
nmhGetFirstIndexLldpXMedPortConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXMedPortConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXMedPortCapSupported ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpXMedPortConfigTLVsTxEnable ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpXMedPortConfigNotifEnable ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXMedPortConfigTLVsTxEnable ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetLldpXMedPortConfigNotifEnable ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXMedPortConfigTLVsTxEnable ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2LldpXMedPortConfigNotifEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXMedPortConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXMedFastStartRepeatCount ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXMedFastStartRepeatCount ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXMedFastStartRepeatCount ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXMedFastStartRepeatCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpXMedLocMediaPolicyTable. */
INT1
nmhValidateIndexInstanceLldpXMedLocMediaPolicyTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for LldpXMedLocMediaPolicyTable  */

INT1
nmhGetFirstIndexLldpXMedLocMediaPolicyTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXMedLocMediaPolicyTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXMedLocMediaPolicyVlanID ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetLldpXMedLocMediaPolicyPriority ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetLldpXMedLocMediaPolicyDscp ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetLldpXMedLocMediaPolicyUnknown ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetLldpXMedLocMediaPolicyTagged ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXMedLocHardwareRev ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpXMedLocFirmwareRev ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpXMedLocSoftwareRev ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpXMedLocSerialNum ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpXMedLocMfgName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpXMedLocModelName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpXMedLocAssetID ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for LldpXMedLocLocationTable. */
INT1
nmhValidateIndexInstanceLldpXMedLocLocationTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXMedLocLocationTable  */

INT1
nmhGetFirstIndexLldpXMedLocLocationTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXMedLocLocationTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXMedLocLocationInfo ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXMedLocLocationInfo ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXMedLocLocationInfo ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXMedLocLocationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXMedLocXPoEDeviceType ARG_LIST((INT4 *));

/* Proto Validate Index Instance for LldpXMedLocXPoEPSEPortTable. */
INT1
nmhValidateIndexInstanceLldpXMedLocXPoEPSEPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXMedLocXPoEPSEPortTable  */

INT1
nmhGetFirstIndexLldpXMedLocXPoEPSEPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXMedLocXPoEPSEPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXMedLocXPoEPSEPortPowerAv ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetLldpXMedLocXPoEPSEPortPDPriority ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXMedLocXPoEPSEPowerSource ARG_LIST((INT4 *));

INT1
nmhGetLldpXMedLocXPoEPDPowerReq ARG_LIST((UINT4 *));

INT1
nmhGetLldpXMedLocXPoEPDPowerSource ARG_LIST((INT4 *));

INT1
nmhGetLldpXMedLocXPoEPDPowerPriority ARG_LIST((INT4 *));

/* Proto Validate Index Instance for LldpXMedRemCapabilitiesTable. */
INT1
nmhValidateIndexInstanceLldpXMedRemCapabilitiesTable ARG_LIST((UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXMedRemCapabilitiesTable  */

INT1
nmhGetFirstIndexLldpXMedRemCapabilitiesTable ARG_LIST((UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXMedRemCapabilitiesTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXMedRemCapSupported ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpXMedRemCapCurrent ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpXMedRemDeviceClass ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for LldpXMedRemMediaPolicyTable. */
INT1
nmhValidateIndexInstanceLldpXMedRemMediaPolicyTable ARG_LIST((UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for LldpXMedRemMediaPolicyTable  */

INT1
nmhGetFirstIndexLldpXMedRemMediaPolicyTable ARG_LIST((UINT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXMedRemMediaPolicyTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXMedRemMediaPolicyVlanID ARG_LIST((UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetLldpXMedRemMediaPolicyPriority ARG_LIST((UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetLldpXMedRemMediaPolicyDscp ARG_LIST((UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetLldpXMedRemMediaPolicyUnknown ARG_LIST((UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetLldpXMedRemMediaPolicyTagged ARG_LIST((UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Validate Index Instance for LldpXMedRemInventoryTable. */
INT1
nmhValidateIndexInstanceLldpXMedRemInventoryTable ARG_LIST((UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXMedRemInventoryTable  */

INT1
nmhGetFirstIndexLldpXMedRemInventoryTable ARG_LIST((UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXMedRemInventoryTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXMedRemHardwareRev ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpXMedRemFirmwareRev ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpXMedRemSoftwareRev ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpXMedRemSerialNum ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpXMedRemMfgName ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpXMedRemModelName ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpXMedRemAssetID ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for LldpXMedRemLocationTable. */
INT1
nmhValidateIndexInstanceLldpXMedRemLocationTable ARG_LIST((UINT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXMedRemLocationTable  */

INT1
nmhGetFirstIndexLldpXMedRemLocationTable ARG_LIST((UINT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXMedRemLocationTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXMedRemLocationInfo ARG_LIST((UINT4  , INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for LldpXMedRemXPoETable. */
INT1
nmhValidateIndexInstanceLldpXMedRemXPoETable ARG_LIST((UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXMedRemXPoETable  */

INT1
nmhGetFirstIndexLldpXMedRemXPoETable ARG_LIST((UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXMedRemXPoETable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXMedRemXPoEDeviceType ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for LldpXMedRemXPoEPSETable. */
INT1
nmhValidateIndexInstanceLldpXMedRemXPoEPSETable ARG_LIST((UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXMedRemXPoEPSETable  */

INT1
nmhGetFirstIndexLldpXMedRemXPoEPSETable ARG_LIST((UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXMedRemXPoEPSETable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXMedRemXPoEPSEPowerAv ARG_LIST((UINT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetLldpXMedRemXPoEPSEPowerSource ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetLldpXMedRemXPoEPSEPowerPriority ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for LldpXMedRemXPoEPDTable. */
INT1
nmhValidateIndexInstanceLldpXMedRemXPoEPDTable ARG_LIST((UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXMedRemXPoEPDTable  */

INT1
nmhGetFirstIndexLldpXMedRemXPoEPDTable ARG_LIST((UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXMedRemXPoEPDTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXMedRemXPoEPDPowerReq ARG_LIST((UINT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetLldpXMedRemXPoEPDPowerSource ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetLldpXMedRemXPoEPDPowerPriority ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));
