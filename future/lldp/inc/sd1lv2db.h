/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: sd1lv2db.h,v 1.2 2013/03/28 11:45:03 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _SD1LV2DB_H
#define _SD1LV2DB_H

UINT1 LldpV2Xdot1ConfigPortVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpV2Xdot1ConfigVlanNameTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 LldpV2Xdot1ConfigProtoVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 LldpV2Xdot1ConfigProtocolTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 LldpV2Xdot1ConfigVidUsageDigestTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpV2Xdot1ConfigManVidTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpV2Xdot1LocTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpV2Xdot1LocProtoVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpV2Xdot1LocVlanNameTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 LldpV2Xdot1LocProtocolTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpV2Xdot1LocVidUsageDigestTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpV2Xdot1LocManVidTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpV2Xdot1LocLinkAggTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpV2Xdot1RemTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpV2Xdot1RemProtoVlanTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpV2Xdot1RemVlanNameTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 LldpV2Xdot1RemProtocolTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpV2Xdot1RemVidUsageDigestTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpV2Xdot1RemManVidTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpV2Xdot1RemLinkAggTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 sd1lv2 [] ={1,0,8802,1,13,1,5,32962};
tSNMP_OID_TYPE sd1lv2OID = {8, sd1lv2};


UINT4 LldpV2Xdot1ConfigPortVlanTxEnable [ ] ={1,0,8802,1,13,1,5,32962,1,1,1,1,1};
UINT4 LldpV2Xdot1ConfigVlanNameTxEnable [ ] ={1,0,8802,1,13,1,5,32962,1,1,2,1,1};
UINT4 LldpV2Xdot1ConfigProtoVlanTxEnable [ ] ={1,0,8802,1,13,1,5,32962,1,1,3,1,1};
UINT4 LldpV2Xdot1ConfigProtocolTxEnable [ ] ={1,0,8802,1,13,1,5,32962,1,1,4,1,1};
UINT4 LldpV2Xdot1ConfigVidUsageDigestTxEnable [ ] ={1,0,8802,1,13,1,5,32962,1,1,5,1,1};
UINT4 LldpV2Xdot1ConfigManVidTxEnable [ ] ={1,0,8802,1,13,1,5,32962,1,1,6,1,1};
UINT4 LldpV2Xdot1LocPortVlanId [ ] ={1,0,8802,1,13,1,5,32962,1,2,1,1,1};
UINT4 LldpV2Xdot1LocProtoVlanId [ ] ={1,0,8802,1,13,1,5,32962,1,2,2,1,1};
UINT4 LldpV2Xdot1LocProtoVlanSupported [ ] ={1,0,8802,1,13,1,5,32962,1,2,2,1,2};
UINT4 LldpV2Xdot1LocProtoVlanEnabled [ ] ={1,0,8802,1,13,1,5,32962,1,2,2,1,3};
UINT4 LldpV2Xdot1LocVlanId [ ] ={1,0,8802,1,13,1,5,32962,1,2,3,1,1};
UINT4 LldpV2Xdot1LocVlanName [ ] ={1,0,8802,1,13,1,5,32962,1,2,3,1,2};
UINT4 LldpV2Xdot1LocProtocolIndex [ ] ={1,0,8802,1,13,1,5,32962,1,2,4,1,1};
UINT4 LldpV2Xdot1LocProtocolId [ ] ={1,0,8802,1,13,1,5,32962,1,2,4,1,2};
UINT4 LldpV2Xdot1LocVidUsageDigest [ ] ={1,0,8802,1,13,1,5,32962,1,2,5,1,1};
UINT4 LldpV2Xdot1LocManVid [ ] ={1,0,8802,1,13,1,5,32962,1,2,6,1,1};
UINT4 LldpV2Xdot1LocLinkAggStatus [ ] ={1,0,8802,1,13,1,5,32962,1,2,7,1,1};
UINT4 LldpV2Xdot1LocLinkAggPortId [ ] ={1,0,8802,1,13,1,5,32962,1,2,7,1,2};
UINT4 LldpV2Xdot1RemPortVlanId [ ] ={1,0,8802,1,13,1,5,32962,1,3,1,1,1};
UINT4 LldpV2Xdot1RemProtoVlanId [ ] ={1,0,8802,1,13,1,5,32962,1,3,2,1,1};
UINT4 LldpV2Xdot1RemProtoVlanSupported [ ] ={1,0,8802,1,13,1,5,32962,1,3,2,1,2};
UINT4 LldpV2Xdot1RemProtoVlanEnabled [ ] ={1,0,8802,1,13,1,5,32962,1,3,2,1,3};
UINT4 LldpV2Xdot1RemVlanId [ ] ={1,0,8802,1,13,1,5,32962,1,3,3,1,1};
UINT4 LldpV2Xdot1RemVlanName [ ] ={1,0,8802,1,13,1,5,32962,1,3,3,1,2};
UINT4 LldpV2Xdot1RemProtocolIndex [ ] ={1,0,8802,1,13,1,5,32962,1,3,4,1,1};
UINT4 LldpV2Xdot1RemProtocolId [ ] ={1,0,8802,1,13,1,5,32962,1,3,4,1,2};
UINT4 LldpV2Xdot1RemVidUsageDigest [ ] ={1,0,8802,1,13,1,5,32962,1,3,5,1,1};
UINT4 LldpV2Xdot1RemManVid [ ] ={1,0,8802,1,13,1,5,32962,1,3,6,1,1};
UINT4 LldpV2Xdot1RemLinkAggStatus [ ] ={1,0,8802,1,13,1,5,32962,1,3,7,1,1};
UINT4 LldpV2Xdot1RemLinkAggPortId [ ] ={1,0,8802,1,13,1,5,32962,1,3,7,1,2};




tMbDbEntry sd1lv2MibEntry[]= {

{{13,LldpV2Xdot1ConfigPortVlanTxEnable}, GetNextIndexLldpV2Xdot1ConfigPortVlanTable, LldpV2Xdot1ConfigPortVlanTxEnableGet, LldpV2Xdot1ConfigPortVlanTxEnableSet, LldpV2Xdot1ConfigPortVlanTxEnableTest, LldpV2Xdot1ConfigPortVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpV2Xdot1ConfigPortVlanTableINDEX, 2, 0, 0, "2"},

{{13,LldpV2Xdot1ConfigVlanNameTxEnable}, GetNextIndexLldpV2Xdot1ConfigVlanNameTable, LldpV2Xdot1ConfigVlanNameTxEnableGet, LldpV2Xdot1ConfigVlanNameTxEnableSet, LldpV2Xdot1ConfigVlanNameTxEnableTest, LldpV2Xdot1ConfigVlanNameTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpV2Xdot1ConfigVlanNameTableINDEX, 2, 0, 0, "2"},

{{13,LldpV2Xdot1ConfigProtoVlanTxEnable}, GetNextIndexLldpV2Xdot1ConfigProtoVlanTable, LldpV2Xdot1ConfigProtoVlanTxEnableGet, LldpV2Xdot1ConfigProtoVlanTxEnableSet, LldpV2Xdot1ConfigProtoVlanTxEnableTest, LldpV2Xdot1ConfigProtoVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpV2Xdot1ConfigProtoVlanTableINDEX, 2, 0, 0, "2"},

{{13,LldpV2Xdot1ConfigProtocolTxEnable}, GetNextIndexLldpV2Xdot1ConfigProtocolTable, LldpV2Xdot1ConfigProtocolTxEnableGet, LldpV2Xdot1ConfigProtocolTxEnableSet, LldpV2Xdot1ConfigProtocolTxEnableTest, LldpV2Xdot1ConfigProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpV2Xdot1ConfigProtocolTableINDEX, 2, 0, 0, "2"},

{{13,LldpV2Xdot1ConfigVidUsageDigestTxEnable}, GetNextIndexLldpV2Xdot1ConfigVidUsageDigestTable, LldpV2Xdot1ConfigVidUsageDigestTxEnableGet, LldpV2Xdot1ConfigVidUsageDigestTxEnableSet, LldpV2Xdot1ConfigVidUsageDigestTxEnableTest, LldpV2Xdot1ConfigVidUsageDigestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpV2Xdot1ConfigVidUsageDigestTableINDEX, 1, 0, 0, "2"},

{{13,LldpV2Xdot1ConfigManVidTxEnable}, GetNextIndexLldpV2Xdot1ConfigManVidTable, LldpV2Xdot1ConfigManVidTxEnableGet, LldpV2Xdot1ConfigManVidTxEnableSet, LldpV2Xdot1ConfigManVidTxEnableTest, LldpV2Xdot1ConfigManVidTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpV2Xdot1ConfigManVidTableINDEX, 1, 0, 0, "2"},

{{13,LldpV2Xdot1LocPortVlanId}, GetNextIndexLldpV2Xdot1LocTable, LldpV2Xdot1LocPortVlanIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpV2Xdot1LocTableINDEX, 1, 0, 0, NULL},

{{13,LldpV2Xdot1LocProtoVlanId}, GetNextIndexLldpV2Xdot1LocProtoVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpV2Xdot1LocProtoVlanTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2Xdot1LocProtoVlanSupported}, GetNextIndexLldpV2Xdot1LocProtoVlanTable, LldpV2Xdot1LocProtoVlanSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2Xdot1LocProtoVlanTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2Xdot1LocProtoVlanEnabled}, GetNextIndexLldpV2Xdot1LocProtoVlanTable, LldpV2Xdot1LocProtoVlanEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2Xdot1LocProtoVlanTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2Xdot1LocVlanId}, GetNextIndexLldpV2Xdot1LocVlanNameTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, LldpV2Xdot1LocVlanNameTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2Xdot1LocVlanName}, GetNextIndexLldpV2Xdot1LocVlanNameTable, LldpV2Xdot1LocVlanNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2Xdot1LocVlanNameTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2Xdot1LocProtocolIndex}, GetNextIndexLldpV2Xdot1LocProtocolTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpV2Xdot1LocProtocolTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2Xdot1LocProtocolId}, GetNextIndexLldpV2Xdot1LocProtocolTable, LldpV2Xdot1LocProtocolIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2Xdot1LocProtocolTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2Xdot1LocVidUsageDigest}, GetNextIndexLldpV2Xdot1LocVidUsageDigestTable, LldpV2Xdot1LocVidUsageDigestGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpV2Xdot1LocVidUsageDigestTableINDEX, 1, 0, 0, NULL},

{{13,LldpV2Xdot1LocManVid}, GetNextIndexLldpV2Xdot1LocManVidTable, LldpV2Xdot1LocManVidGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpV2Xdot1LocManVidTableINDEX, 1, 0, 0, NULL},

{{13,LldpV2Xdot1LocLinkAggStatus}, GetNextIndexLldpV2Xdot1LocLinkAggTable, LldpV2Xdot1LocLinkAggStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2Xdot1LocLinkAggTableINDEX, 1, 0, 0, NULL},

{{13,LldpV2Xdot1LocLinkAggPortId}, GetNextIndexLldpV2Xdot1LocLinkAggTable, LldpV2Xdot1LocLinkAggPortIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpV2Xdot1LocLinkAggTableINDEX, 1, 0, 0, NULL},

{{13,LldpV2Xdot1RemPortVlanId}, GetNextIndexLldpV2Xdot1RemTable, LldpV2Xdot1RemPortVlanIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpV2Xdot1RemTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2Xdot1RemProtoVlanId}, GetNextIndexLldpV2Xdot1RemProtoVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpV2Xdot1RemProtoVlanTableINDEX, 5, 0, 0, NULL},

{{13,LldpV2Xdot1RemProtoVlanSupported}, GetNextIndexLldpV2Xdot1RemProtoVlanTable, LldpV2Xdot1RemProtoVlanSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2Xdot1RemProtoVlanTableINDEX, 5, 0, 0, NULL},

{{13,LldpV2Xdot1RemProtoVlanEnabled}, GetNextIndexLldpV2Xdot1RemProtoVlanTable, LldpV2Xdot1RemProtoVlanEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2Xdot1RemProtoVlanTableINDEX, 5, 0, 0, NULL},

{{13,LldpV2Xdot1RemVlanId}, GetNextIndexLldpV2Xdot1RemVlanNameTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, LldpV2Xdot1RemVlanNameTableINDEX, 5, 0, 0, NULL},

{{13,LldpV2Xdot1RemVlanName}, GetNextIndexLldpV2Xdot1RemVlanNameTable, LldpV2Xdot1RemVlanNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2Xdot1RemVlanNameTableINDEX, 5, 0, 0, NULL},

{{13,LldpV2Xdot1RemProtocolIndex}, GetNextIndexLldpV2Xdot1RemProtocolTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpV2Xdot1RemProtocolTableINDEX, 5, 0, 0, NULL},

{{13,LldpV2Xdot1RemProtocolId}, GetNextIndexLldpV2Xdot1RemProtocolTable, LldpV2Xdot1RemProtocolIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2Xdot1RemProtocolTableINDEX, 5, 0, 0, NULL},

{{13,LldpV2Xdot1RemVidUsageDigest}, GetNextIndexLldpV2Xdot1RemVidUsageDigestTable, LldpV2Xdot1RemVidUsageDigestGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpV2Xdot1RemVidUsageDigestTableINDEX, 3, 0, 0, NULL},

{{13,LldpV2Xdot1RemManVid}, GetNextIndexLldpV2Xdot1RemManVidTable, LldpV2Xdot1RemManVidGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpV2Xdot1RemManVidTableINDEX, 3, 0, 0, NULL},

{{13,LldpV2Xdot1RemLinkAggStatus}, GetNextIndexLldpV2Xdot1RemLinkAggTable, LldpV2Xdot1RemLinkAggStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2Xdot1RemLinkAggTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2Xdot1RemLinkAggPortId}, GetNextIndexLldpV2Xdot1RemLinkAggTable, LldpV2Xdot1RemLinkAggPortIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpV2Xdot1RemLinkAggTableINDEX, 4, 0, 0, NULL},
};
tMibData sd1lv2Entry = { 30, sd1lv2MibEntry };

#endif /* _SD1LV2DB_H */

