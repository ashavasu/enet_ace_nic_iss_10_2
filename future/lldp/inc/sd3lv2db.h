/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: sd3lv2db.h,v 1.2 2013/03/28 11:45:03 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _SD3LV2DB_H
#define _SD3LV2DB_H

UINT1 LldpV2Xdot3PortConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpV2Xdot3LocPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpV2Xdot3LocPowerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpV2Xdot3LocMaxFrameSizeTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpV2Xdot3RemPortTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpV2Xdot3RemPowerTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpV2Xdot3RemMaxFrameSizeTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 sd3lv2 [] ={1,0,8802,1,13,1,5,4623};
tSNMP_OID_TYPE sd3lv2OID = {8, sd3lv2};


UINT4 LldpV2Xdot3PortConfigTLVsTxEnable [ ] ={1,0,8802,1,13,1,5,4623,1,1,1,1,1};
UINT4 LldpV2Xdot3LocPortAutoNegSupported [ ] ={1,0,8802,1,13,1,5,4623,1,2,1,1,1};
UINT4 LldpV2Xdot3LocPortAutoNegEnabled [ ] ={1,0,8802,1,13,1,5,4623,1,2,1,1,2};
UINT4 LldpV2Xdot3LocPortAutoNegAdvertisedCap [ ] ={1,0,8802,1,13,1,5,4623,1,2,1,1,3};
UINT4 LldpV2Xdot3LocPortOperMauType [ ] ={1,0,8802,1,13,1,5,4623,1,2,1,1,4};
UINT4 LldpV2Xdot3LocPowerPortClass [ ] ={1,0,8802,1,13,1,5,4623,1,2,2,1,1};
UINT4 LldpV2Xdot3LocPowerMDISupported [ ] ={1,0,8802,1,13,1,5,4623,1,2,2,1,2};
UINT4 LldpV2Xdot3LocPowerMDIEnabled [ ] ={1,0,8802,1,13,1,5,4623,1,2,2,1,3};
UINT4 LldpV2Xdot3LocPowerPairControlable [ ] ={1,0,8802,1,13,1,5,4623,1,2,2,1,4};
UINT4 LldpV2Xdot3LocPowerPairs [ ] ={1,0,8802,1,13,1,5,4623,1,2,2,1,5};
UINT4 LldpV2Xdot3LocPowerClass [ ] ={1,0,8802,1,13,1,5,4623,1,2,2,1,6};
UINT4 LldpV2Xdot3LocMaxFrameSize [ ] ={1,0,8802,1,13,1,5,4623,1,2,3,1,1};
UINT4 LldpV2Xdot3RemPortAutoNegSupported [ ] ={1,0,8802,1,13,1,5,4623,1,3,1,1,1};
UINT4 LldpV2Xdot3RemPortAutoNegEnabled [ ] ={1,0,8802,1,13,1,5,4623,1,3,1,1,2};
UINT4 LldpV2Xdot3RemPortAutoNegAdvertisedCap [ ] ={1,0,8802,1,13,1,5,4623,1,3,1,1,3};
UINT4 LldpV2Xdot3RemPortOperMauType [ ] ={1,0,8802,1,13,1,5,4623,1,3,1,1,4};
UINT4 LldpV2Xdot3RemPowerPortClass [ ] ={1,0,8802,1,13,1,5,4623,1,3,2,1,1};
UINT4 LldpV2Xdot3RemPowerMDISupported [ ] ={1,0,8802,1,13,1,5,4623,1,3,2,1,2};
UINT4 LldpV2Xdot3RemPowerMDIEnabled [ ] ={1,0,8802,1,13,1,5,4623,1,3,2,1,3};
UINT4 LldpV2Xdot3RemPowerPairControlable [ ] ={1,0,8802,1,13,1,5,4623,1,3,2,1,4};
UINT4 LldpV2Xdot3RemPowerPairs [ ] ={1,0,8802,1,13,1,5,4623,1,3,2,1,5};
UINT4 LldpV2Xdot3RemPowerClass [ ] ={1,0,8802,1,13,1,5,4623,1,3,2,1,6};
UINT4 LldpV2Xdot3RemMaxFrameSize [ ] ={1,0,8802,1,13,1,5,4623,1,3,3,1,1};




tMbDbEntry sd3lv2MibEntry[]= {

{{13,LldpV2Xdot3PortConfigTLVsTxEnable}, GetNextIndexLldpV2Xdot3PortConfigTable, LldpV2Xdot3PortConfigTLVsTxEnableGet, LldpV2Xdot3PortConfigTLVsTxEnableSet, LldpV2Xdot3PortConfigTLVsTxEnableTest, LldpV2Xdot3PortConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, LldpV2Xdot3PortConfigTableINDEX, 2, 0, 0, NULL},

{{13,LldpV2Xdot3LocPortAutoNegSupported}, GetNextIndexLldpV2Xdot3LocPortTable, LldpV2Xdot3LocPortAutoNegSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2Xdot3LocPortTableINDEX, 1, 0, 0, NULL},

{{13,LldpV2Xdot3LocPortAutoNegEnabled}, GetNextIndexLldpV2Xdot3LocPortTable, LldpV2Xdot3LocPortAutoNegEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2Xdot3LocPortTableINDEX, 1, 0, 0, NULL},

{{13,LldpV2Xdot3LocPortAutoNegAdvertisedCap}, GetNextIndexLldpV2Xdot3LocPortTable, LldpV2Xdot3LocPortAutoNegAdvertisedCapGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2Xdot3LocPortTableINDEX, 1, 0, 0, NULL},

{{13,LldpV2Xdot3LocPortOperMauType}, GetNextIndexLldpV2Xdot3LocPortTable, LldpV2Xdot3LocPortOperMauTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpV2Xdot3LocPortTableINDEX, 1, 0, 0, NULL},

{{13,LldpV2Xdot3LocPowerPortClass}, GetNextIndexLldpV2Xdot3LocPowerTable, LldpV2Xdot3LocPowerPortClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2Xdot3LocPowerTableINDEX, 1, 0, 0, NULL},

{{13,LldpV2Xdot3LocPowerMDISupported}, GetNextIndexLldpV2Xdot3LocPowerTable, LldpV2Xdot3LocPowerMDISupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2Xdot3LocPowerTableINDEX, 1, 0, 0, NULL},

{{13,LldpV2Xdot3LocPowerMDIEnabled}, GetNextIndexLldpV2Xdot3LocPowerTable, LldpV2Xdot3LocPowerMDIEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2Xdot3LocPowerTableINDEX, 1, 0, 0, NULL},

{{13,LldpV2Xdot3LocPowerPairControlable}, GetNextIndexLldpV2Xdot3LocPowerTable, LldpV2Xdot3LocPowerPairControlableGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2Xdot3LocPowerTableINDEX, 1, 0, 0, NULL},

{{13,LldpV2Xdot3LocPowerPairs}, GetNextIndexLldpV2Xdot3LocPowerTable, LldpV2Xdot3LocPowerPairsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpV2Xdot3LocPowerTableINDEX, 1, 0, 0, NULL},

{{13,LldpV2Xdot3LocPowerClass}, GetNextIndexLldpV2Xdot3LocPowerTable, LldpV2Xdot3LocPowerClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpV2Xdot3LocPowerTableINDEX, 1, 0, 0, NULL},

{{13,LldpV2Xdot3LocMaxFrameSize}, GetNextIndexLldpV2Xdot3LocMaxFrameSizeTable, LldpV2Xdot3LocMaxFrameSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpV2Xdot3LocMaxFrameSizeTableINDEX, 1, 0, 0, NULL},

{{13,LldpV2Xdot3RemPortAutoNegSupported}, GetNextIndexLldpV2Xdot3RemPortTable, LldpV2Xdot3RemPortAutoNegSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2Xdot3RemPortTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2Xdot3RemPortAutoNegEnabled}, GetNextIndexLldpV2Xdot3RemPortTable, LldpV2Xdot3RemPortAutoNegEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2Xdot3RemPortTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2Xdot3RemPortAutoNegAdvertisedCap}, GetNextIndexLldpV2Xdot3RemPortTable, LldpV2Xdot3RemPortAutoNegAdvertisedCapGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2Xdot3RemPortTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2Xdot3RemPortOperMauType}, GetNextIndexLldpV2Xdot3RemPortTable, LldpV2Xdot3RemPortOperMauTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpV2Xdot3RemPortTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2Xdot3RemPowerPortClass}, GetNextIndexLldpV2Xdot3RemPowerTable, LldpV2Xdot3RemPowerPortClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2Xdot3RemPowerTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2Xdot3RemPowerMDISupported}, GetNextIndexLldpV2Xdot3RemPowerTable, LldpV2Xdot3RemPowerMDISupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2Xdot3RemPowerTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2Xdot3RemPowerMDIEnabled}, GetNextIndexLldpV2Xdot3RemPowerTable, LldpV2Xdot3RemPowerMDIEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2Xdot3RemPowerTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2Xdot3RemPowerPairControlable}, GetNextIndexLldpV2Xdot3RemPowerTable, LldpV2Xdot3RemPowerPairControlableGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpV2Xdot3RemPowerTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2Xdot3RemPowerPairs}, GetNextIndexLldpV2Xdot3RemPowerTable, LldpV2Xdot3RemPowerPairsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpV2Xdot3RemPowerTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2Xdot3RemPowerClass}, GetNextIndexLldpV2Xdot3RemPowerTable, LldpV2Xdot3RemPowerClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpV2Xdot3RemPowerTableINDEX, 4, 0, 0, NULL},

{{13,LldpV2Xdot3RemMaxFrameSize}, GetNextIndexLldpV2Xdot3RemMaxFrameSizeTable, LldpV2Xdot3RemMaxFrameSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpV2Xdot3RemMaxFrameSizeTableINDEX, 4, 0, 0, NULL},
};
tMibData sd3lv2Entry = { 23, sd3lv2MibEntry };

#endif /* _SD3LV2DB_H */

