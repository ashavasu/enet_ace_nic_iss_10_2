/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdot3lw.h,v 1.3 2008/08/20 15:13:21 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for LldpXdot3PortConfigTable. */
INT1
nmhValidateIndexInstanceLldpXdot3PortConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot3PortConfigTable  */

INT1
nmhGetFirstIndexLldpXdot3PortConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot3PortConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot3PortConfigTLVsTxEnable ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXdot3PortConfigTLVsTxEnable ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXdot3PortConfigTLVsTxEnable ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXdot3PortConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpXdot3LocPortTable. */
INT1
nmhValidateIndexInstanceLldpXdot3LocPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot3LocPortTable  */

INT1
nmhGetFirstIndexLldpXdot3LocPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot3LocPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot3LocPortAutoNegSupported ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpXdot3LocPortAutoNegEnabled ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpXdot3LocPortAutoNegAdvertisedCap ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpXdot3LocPortOperMauType ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for LldpXdot3LocPowerTable. */
INT1
nmhValidateIndexInstanceLldpXdot3LocPowerTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot3LocPowerTable  */

INT1
nmhGetFirstIndexLldpXdot3LocPowerTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot3LocPowerTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot3LocPowerPortClass ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpXdot3LocPowerMDISupported ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpXdot3LocPowerMDIEnabled ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpXdot3LocPowerPairControlable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpXdot3LocPowerPairs ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpXdot3LocPowerClass ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for LldpXdot3LocLinkAggTable. */
INT1
nmhValidateIndexInstanceLldpXdot3LocLinkAggTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot3LocLinkAggTable  */

INT1
nmhGetFirstIndexLldpXdot3LocLinkAggTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot3LocLinkAggTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot3LocLinkAggStatus ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpXdot3LocLinkAggPortId ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for LldpXdot3LocMaxFrameSizeTable. */
INT1
nmhValidateIndexInstanceLldpXdot3LocMaxFrameSizeTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot3LocMaxFrameSizeTable  */

INT1
nmhGetFirstIndexLldpXdot3LocMaxFrameSizeTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot3LocMaxFrameSizeTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot3LocMaxFrameSize ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for LldpXdot3RemPortTable. */
INT1
nmhValidateIndexInstanceLldpXdot3RemPortTable ARG_LIST((UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot3RemPortTable  */

INT1
nmhGetFirstIndexLldpXdot3RemPortTable ARG_LIST((UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot3RemPortTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot3RemPortAutoNegSupported ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetLldpXdot3RemPortAutoNegEnabled ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetLldpXdot3RemPortAutoNegAdvertisedCap ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpXdot3RemPortOperMauType ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for LldpXdot3RemPowerTable. */
INT1
nmhValidateIndexInstanceLldpXdot3RemPowerTable ARG_LIST((UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot3RemPowerTable  */

INT1
nmhGetFirstIndexLldpXdot3RemPowerTable ARG_LIST((UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot3RemPowerTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot3RemPowerPortClass ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetLldpXdot3RemPowerMDISupported ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetLldpXdot3RemPowerMDIEnabled ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetLldpXdot3RemPowerPairControlable ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetLldpXdot3RemPowerPairs ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetLldpXdot3RemPowerClass ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for LldpXdot3RemLinkAggTable. */
INT1
nmhValidateIndexInstanceLldpXdot3RemLinkAggTable ARG_LIST((UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot3RemLinkAggTable  */

INT1
nmhGetFirstIndexLldpXdot3RemLinkAggTable ARG_LIST((UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot3RemLinkAggTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot3RemLinkAggStatus ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpXdot3RemLinkAggPortId ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for LldpXdot3RemMaxFrameSizeTable. */
INT1
nmhValidateIndexInstanceLldpXdot3RemMaxFrameSizeTable ARG_LIST((UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot3RemMaxFrameSizeTable  */

INT1
nmhGetFirstIndexLldpXdot3RemMaxFrameSizeTable ARG_LIST((UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot3RemMaxFrameSizeTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot3RemMaxFrameSize ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));
