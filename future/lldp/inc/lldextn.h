/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldextn.h,v 1.8 2015/09/09 13:19:29 siva Exp $
 *
 * Description: This file contains extern variables declaration
 *              used in LLDP.
 *********************************************************************/
#ifndef _LLDEXTN_H
#define _LLDEXTN_H

PUBLIC tVlanNameInfo   gVlanNameInfo [VLAN_DEV_MAX_NUM_VLAN];
PUBLIC UINT1           gu1TxAll;
PUBLIC UINT1           gau1LldpDot1OUI[LLDP_MAX_LEN_OUI];
PUBLIC UINT1           gau1LldpDot3OUI[LLDP_MAX_LEN_OUI];
PUBLIC UINT1           gau1LldpMedOUI[LLDP_MAX_LEN_OUI];
PUBLIC UINT1           gau1LldpMcastAddr[MAC_ADDR_LEN];
PUBLIC UINT4           gu4LldpAgentNumber;
PUBLIC UINT4 gu4LldpDestMacAddrTblIndex;
PUBLIC UINT4 gLldpUtVariable;
PUBLIC UINT4 gu4LldpASNBackwardCompatibility;
#ifdef L2RED_WANTED
PUBLIC tLldpRedGlobalInfo     gLldpRedGlobalInfo;
#endif

#endif /* _LLDEXTN_H*/
/*-----------------------------------------------------------------------*/
/*                       End of the file  lldextn.h                      */
/*-----------------------------------------------------------------------*/
