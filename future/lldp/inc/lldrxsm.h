/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lldrxsm.h,v 1.3 2007/12/12 11:09:52 iss Exp $
 *
 * Description: Data structure used by LLDP Rx module State
 *              Event machine.
 *********************************************************************/
#ifndef _LLDP_RXSM_H_
#define _LLDP_RXSM_H_

#include "lldinc.h"

#define LLDP_RXSEM_SET_CURRENT_STATE(pPortInfo, i4State) \
{\
    pPortInfo->i4RxSemCurrentState = i4State; \
}

PRIVATE VOID LldpRxSemEventIgnore              PROTO ((tLldpLocPortInfo *));
PRIVATE VOID LldpRxSmStateWaitPortOperational  PROTO ((tLldpLocPortInfo *));
PRIVATE VOID LldpRxSmStateDelAgedInfo          PROTO ((tLldpLocPortInfo *));
PRIVATE VOID LldpRxSmStateRxLldpInit           PROTO ((tLldpLocPortInfo *));
PRIVATE VOID LldpRxSmStateRxWaitForFrame       PROTO ((tLldpLocPortInfo *));
PRIVATE VOID LldpRxSmStateRxFrame              PROTO ((tLldpLocPortInfo *));
PRIVATE VOID LldpRxSmStateDelInfo              PROTO ((tLldpLocPortInfo *));
PRIVATE VOID LldpRxSmStateUpdateInfo           PROTO ((tLldpLocPortInfo *));

/* Index of the Rx Sem Function pointers */
enum 
{
    A0  = 0,
    A1  = 1,
    A2  = 2,
    A3  = 3,
    A4  = 4,
    A5  = 5,
    A6  = 6,
    LLDP_MAX_RXSEM_FN_PTRS  = 7
};

/* LLDP RX SEM function pointers */
void (*gaLldpRxActionProc[LLDP_MAX_RXSEM_FN_PTRS]) (tLldpLocPortInfo *) =
{                                              /* STATES : */
   /* A0 */  LldpRxSemEventIgnore,           /* Do Noting */
   /* A1 */  LldpRxSmStateWaitPortOperational, /*LLDP_WAIT_PORT_OPERATIONAL */
   /* A2 */  LldpRxSmStateDelAgedInfo,         /*DELETE_AGED_INFO */
   /* A3 */  LldpRxSmStateRxLldpInit,          /*RX_LLDP_INITIALIZE */
   /* A4 */  LldpRxSmStateRxWaitForFrame,      /*RX_WAIT_FOR_FRAME */
   /* A5 */  LldpRxSmStateRxFrame,             /*RX_FRAME */
   /* A6 */  LldpRxSmStateDelInfo,             /*DELETE_INFO */
   /* NOTE: LldpRxSmStateUpdateInfo() function described the state UPDATE_INFO
    * but there is no explicit event to move into this state. Its a direct 
    * function call from LldpRxSmStateRxFrame */
};

/* LLDP Rx State Event Table */
const UINT1 gau1LldpRxSem[LLDP_RX_MAX_EVENTS][LLDP_RX_MAX_STATES] =
{
/*                               STATES:
 * __________________________________________________________________________
 *                  |       Wait | Delete | Rx  | Wait | Frame| Delete|Update|
 *                  |       Port | AgedOut| Init| For  | Rece-| Info  |Info  |
 *     EVENTS       |      OperUP| Info   |     | Frame| ived |       |      |
 * __________________________________________________________________________*/
/* PORT_OPER_DOWN   |*/  {  A0,    A1,     A1,    A1,     A1,    A1,     A1 },
/* INFO_AGEOUT      |*/  {  A2,    A0,     A0,    A6,     A0,    A0,     A0 },
/* PORT_OPER_UP     |*/  {  A3,    A0,     A0,    A0,     A0,    A0,     A0 },
/* RX_MOD_UP        |*/  {  A0,    A0,     A4,    A0,     A0,    A0,     A0 },
/* RX_MOD_DOWN      |*/  {  A0,    A0,     A0,    A3,     A0,    A0,     A0 },
/* FRAME_RECEIVED   |*/  {  A0,    A0,     A0,    A5,     A0,    A0,     A0 },
/* --------------------------------------------------------------------------*/
};

/* Useful for debug messages */
const CHR1 *gau1LldpRxStateStr[LLDP_RX_MAX_STATES] = 
{
    "WAIT_PORT_OPERATIONAL",
    "DELETE_AGED_INFO",
    "LLDP_INITIALIZE",   
    "WAIT_FOR_FRAME",    
    "FRAME_RECEIVED",     
    "DELETE_INFO",     
    "UPDATE_INFO",        
};

const CHR1 *gau1LldpRxEvntStr[LLDP_RX_MAX_EVENTS] = 
{
    "PORT_OPER_DOWN",
    "INFO_AGEOUT",
    "PORT_OPER_UP",
    "RXMOD_ADMIN_UP",    
    "RXMOD_ADMIN_DOWN",  
    "FRAME_RECEIVED",
};
#endif /* _LLDP_RXSM_H_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  lldprxsem.h                        */
/*-----------------------------------------------------------------------*/
