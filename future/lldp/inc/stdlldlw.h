/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdlldlw.h,v 1.3 2008/08/20 15:13:21 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpMessageTxInterval ARG_LIST((INT4 *));

INT1
nmhGetLldpMessageTxHoldMultiplier ARG_LIST((INT4 *));

INT1
nmhGetLldpReinitDelay ARG_LIST((INT4 *));

INT1
nmhGetLldpTxDelay ARG_LIST((INT4 *));

INT1
nmhGetLldpNotificationInterval ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpMessageTxInterval ARG_LIST((INT4 ));

INT1
nmhSetLldpMessageTxHoldMultiplier ARG_LIST((INT4 ));

INT1
nmhSetLldpReinitDelay ARG_LIST((INT4 ));

INT1
nmhSetLldpTxDelay ARG_LIST((INT4 ));

INT1
nmhSetLldpNotificationInterval ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpMessageTxInterval ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2LldpMessageTxHoldMultiplier ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2LldpReinitDelay ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2LldpTxDelay ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2LldpNotificationInterval ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpMessageTxInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2LldpMessageTxHoldMultiplier ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2LldpReinitDelay ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2LldpTxDelay ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2LldpNotificationInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpPortConfigTable. */
INT1
nmhValidateIndexInstanceLldpPortConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpPortConfigTable  */

INT1
nmhGetFirstIndexLldpPortConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpPortConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpPortConfigAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpPortConfigNotificationEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpPortConfigTLVsTxEnable ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpPortConfigAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetLldpPortConfigNotificationEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetLldpPortConfigTLVsTxEnable ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpPortConfigAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2LldpPortConfigNotificationEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2LldpPortConfigTLVsTxEnable ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpPortConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpConfigManAddrTable. */
INT1
nmhValidateIndexInstanceLldpConfigManAddrTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for LldpConfigManAddrTable  */

INT1
nmhGetFirstIndexLldpConfigManAddrTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpConfigManAddrTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpConfigManAddrPortsTxEnable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpConfigManAddrPortsTxEnable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpConfigManAddrPortsTxEnable ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpConfigManAddrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpStatsRemTablesLastChangeTime ARG_LIST((UINT4 *));

INT1
nmhGetLldpStatsRemTablesInserts ARG_LIST((UINT4 *));

INT1
nmhGetLldpStatsRemTablesDeletes ARG_LIST((UINT4 *));

INT1
nmhGetLldpStatsRemTablesDrops ARG_LIST((UINT4 *));

INT1
nmhGetLldpStatsRemTablesAgeouts ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for LldpStatsTxPortTable. */
INT1
nmhValidateIndexInstanceLldpStatsTxPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpStatsTxPortTable  */

INT1
nmhGetFirstIndexLldpStatsTxPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpStatsTxPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpStatsTxPortFramesTotal ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpStatsRxPortTable. */
INT1
nmhValidateIndexInstanceLldpStatsRxPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpStatsRxPortTable  */

INT1
nmhGetFirstIndexLldpStatsRxPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpStatsRxPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpStatsRxPortFramesDiscardedTotal ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetLldpStatsRxPortFramesErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetLldpStatsRxPortFramesTotal ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetLldpStatsRxPortTLVsDiscardedTotal ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetLldpStatsRxPortTLVsUnrecognizedTotal ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetLldpStatsRxPortAgeoutsTotal ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpLocChassisIdSubtype ARG_LIST((INT4 *));

INT1
nmhGetLldpLocChassisId ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpLocSysName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpLocSysDesc ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpLocSysCapSupported ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpLocSysCapEnabled ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for LldpLocPortTable. */
INT1
nmhValidateIndexInstanceLldpLocPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpLocPortTable  */

INT1
nmhGetFirstIndexLldpLocPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpLocPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpLocPortIdSubtype ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpLocPortId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpLocPortDesc ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for LldpLocManAddrTable. */
INT1
nmhValidateIndexInstanceLldpLocManAddrTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for LldpLocManAddrTable  */

INT1
nmhGetFirstIndexLldpLocManAddrTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpLocManAddrTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpLocManAddrLen ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetLldpLocManAddrIfSubtype ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetLldpLocManAddrIfId ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetLldpLocManAddrOID ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OID_TYPE * ));

/* Proto Validate Index Instance for LldpRemTable. */
INT1
nmhValidateIndexInstanceLldpRemTable ARG_LIST((UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpRemTable  */

INT1
nmhGetFirstIndexLldpRemTable ARG_LIST((UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpRemTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpRemChassisIdSubtype ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetLldpRemChassisId ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpRemPortIdSubtype ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetLldpRemPortId ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpRemPortDesc ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpRemSysName ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpRemSysDesc ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpRemSysCapSupported ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLldpRemSysCapEnabled ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for LldpRemManAddrTable. */
INT1
nmhValidateIndexInstanceLldpRemManAddrTable ARG_LIST((UINT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for LldpRemManAddrTable  */

INT1
nmhGetFirstIndexLldpRemManAddrTable ARG_LIST((UINT4 * , INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpRemManAddrTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpRemManAddrIfSubtype ARG_LIST((UINT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetLldpRemManAddrIfId ARG_LIST((UINT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetLldpRemManAddrOID ARG_LIST((UINT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OID_TYPE * ));

/* Proto Validate Index Instance for LldpRemUnknownTLVTable. */
INT1
nmhValidateIndexInstanceLldpRemUnknownTLVTable ARG_LIST((UINT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpRemUnknownTLVTable  */

INT1
nmhGetFirstIndexLldpRemUnknownTLVTable ARG_LIST((UINT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpRemUnknownTLVTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpRemUnknownTLVInfo ARG_LIST((UINT4  , INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for LldpRemOrgDefInfoTable. */
INT1
nmhValidateIndexInstanceLldpRemOrgDefInfoTable ARG_LIST((UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpRemOrgDefInfoTable  */

INT1
nmhGetFirstIndexLldpRemOrgDefInfoTable ARG_LIST((UINT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpRemOrgDefInfoTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpRemOrgDefInfo ARG_LIST((UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
