/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: icchsock.c,v 1.16.2.1 2018/03/16 13:43:47 siva Exp $
*
* Description: Inter chassis communication handler socket interface 
*              functions.
*********************************************************************/

#include "icchincs.h"
#include "fssocket.h"
#include <stdio.h>
#include <string.h>

#define ICCH_SYNC_TCP_SERVER_PORT 7217
#define ICCH_SYNC_TCP_MAX_CONN  5
#define ICCH_MAX_RECV_SEND_RETRIES 50

/******************************************************************************
 * Function           : IcchTcpSrvSockInit
 * Input(s)           : pi4SrvSockFd - Pointer to server socket fd
 * Output(s)          : TCP server socket descriptor
 * Returns            : ICCH_SUCCESS/ICCH_FAILURE
 * Action             : Routine to create a TCP server scoket, set socket options
 *                      for accepting the connections from peer.
 ******************************************************************************/
INT4
IcchTcpSrvSockInit (INT4 *pi4SrvSockFd)
{
    INT4                i4SockFd = ICCH_INV_SOCK_FD;
    INT4                i4Flags = -1;
    UINT4               u4RetVal = ICCH_SUCCESS;
    INT4                i4RetVal = 0;
    INT4                i4OptVal = TRUE;
    struct sockaddr_in  serv_addr;

    /* Check if the TCP server already started */
    /* Open the tcp socket */
    i4SockFd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4SockFd < 0)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                  "IcchTcpSrvSockInit: TCP server socket creation failure !!!\r\n");
        u4RetVal = ICCH_FAILURE;
    }
    /* Get current socket flags */
    if (u4RetVal != ICCH_FAILURE)
    {
        if ((i4Flags = fcntl (i4SockFd, F_GETFL, 0)) < 0)
        {
            ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                      "IcchTcpSrvSockInit: TCP server Fcntl "
                      "GET Failure !!!\r\n");
            IcchCloseSocket (i4SockFd);
            u4RetVal = ICCH_FAILURE;
        }
    }

    if (u4RetVal != ICCH_FAILURE)
    {
        /* Set the socket is non-blocking mode */
        i4Flags |= O_NONBLOCK;
        if (fcntl (i4SockFd, F_SETFL, i4Flags) < 0)
        {
            ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                      "IcchTcpSrvSockInit: TCP server Fcntl "
                      "SET Failure !!!\r\n");
            IcchCloseSocket (i4SockFd);
            u4RetVal = ICCH_FAILURE;
        }
    }

    if (u4RetVal != ICCH_FAILURE)
    {
        i4RetVal = setsockopt (i4SockFd, SOL_SOCKET, SO_REUSEADDR,
                               (INT4 *) &i4OptVal, sizeof (i4OptVal));
        if (i4RetVal < 0)
        {
            perror ("IcchTcpSrvSockInit: Unable to set SO_REUSEADDR options "
                    "for socket\r\n");
            IcchCloseSocket (i4SockFd);
            u4RetVal = ICCH_FAILURE;
        }
    }
    if (u4RetVal != ICCH_FAILURE)
    {
        MEMSET (&serv_addr, 0, sizeof (serv_addr));
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = OSIX_HTONL (ICCH_GET_SELF_NODE_ID ());
        serv_addr.sin_port = OSIX_HTONS (ICCH_SYNC_TCP_SERVER_PORT);

        i4RetVal = bind (i4SockFd, (struct sockaddr *) &serv_addr,
                         sizeof (serv_addr));
        if (i4RetVal < 0)
        {
            perror ("IcchTcpSrvSockInit: Unable to bind with TCP server "
                    "address and port for socket\r\n");
            IcchCloseSocket (i4SockFd);
            u4RetVal = ICCH_FAILURE;
        }
    }
    if (u4RetVal != ICCH_FAILURE)
    {
        i4RetVal = listen (i4SockFd, ICCH_SYNC_TCP_MAX_CONN);
        if (i4RetVal < 0)
        {
            perror ("IcchTcpSrvSockInit: Unable to listen for " "the socket\n");
            IcchCloseSocket (i4SockFd);
            u4RetVal = ICCH_FAILURE;
        }
    }
    if (u4RetVal == ICCH_FAILURE)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC, "IcchTcpSrvSockInit: "
                  "Server socket creation failed \r\n");
        return (ICCH_FAILURE);
    }
    *pi4SrvSockFd = i4SockFd;
    /* Add this SockFd to SELECT library */
    SelAddFd (i4SockFd, IcchTcpSrvSockCallBk);
    return (ICCH_SUCCESS);
}

/******************************************************************************
 * Function           : IcchTcpAcceptNewConnection
 * Input(s)           : i4SrvSockFd - Server socket fd
 *                      pi4SockFd - Pointer to client socket fd
 *                      pu4PeerAddr - Peer IP address
 * Output(s)          : New connection identifier
 * Returns            : ICCH_FAILURE/ICCH_SUCCESS
 * Action             : This routine accepts the
 *                      connection from the TCP peer.
 ******************************************************************************/
INT4
IcchTcpAcceptNewConnection (INT4 i4SrvSockFd, INT4 *pi4SockFd,
                            UINT4 *pu4PeerAddr)
{
    INT4                i4CliSockFd = ICCH_INV_SOCK_FD;
    INT4                i4Flags = 0;
#ifdef LNXIP4_WANTED
    INT4                i4precedence = 7;
#endif
    UINT4               u4IpAddr = 0;
    UINT4               u4CliLen = sizeof (struct sockaddr_in);
    UINT4               u4SndBuf = ICCH_MAX_FRAGMENT;
    UINT4               u4RcvBuf = ICCH_MAX_FRAGMENT;

    struct sockaddr_in  CliSockAddr;

    if (i4SrvSockFd == ICCH_INV_SOCK_FD)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                  "IcchTcpAcceptNewConnection: Invalid server socket id \r\n");
        return ICCH_FAILURE;
    }
    MEMSET (&CliSockAddr, 0, u4CliLen);

    i4CliSockFd =
        accept (i4SrvSockFd, (struct sockaddr *) &CliSockAddr,
                (socklen_t *) & u4CliLen);
    if (i4CliSockFd < 0)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                  "IcchTcpAcceptNewConnection: Connection accept "
                  "failed !!!\r\n");
        perror ("Accept error");
        return ICCH_FAILURE;
    }
    u4IpAddr = OSIX_NTOHL (CliSockAddr.sin_addr.s_addr);

    if ((i4Flags = fcntl (i4CliSockFd, F_GETFL, 0)) < 0)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                  "IcchTcpAcceptNewConnection: TCP server Fcntl "
                  "GET Failure !!!\r\n");
        IcchCloseSocket (i4CliSockFd);
        return ICCH_FAILURE;
    }

    /* Set the socket in non-blocking mode */
    i4Flags |= O_NONBLOCK;
    if (fcntl (i4CliSockFd, F_SETFL, i4Flags) < 0)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                  "IcchTcpAcceptNewConnection: TCP server Fcntl "
                  "SET Failure !!!\r\n");
        IcchCloseSocket (i4CliSockFd);
        return ICCH_FAILURE;
    }

    if (setsockopt (i4CliSockFd, SOL_SOCKET, SO_SNDBUF,
                    (VOID *) &u4SndBuf, sizeof (UINT4)) < 0)
    {
        IcchCloseSocket (i4CliSockFd);
        return ICCH_FAILURE;
    }

    if (setsockopt (i4CliSockFd, SOL_SOCKET, SO_RCVBUF,
                    (VOID *) &u4RcvBuf, sizeof (UINT4)) < 0)
    {
        IcchCloseSocket (i4CliSockFd);
        return ICCH_FAILURE;
    }

#ifdef LNXIP4_WANTED
    /* Priority Given to  Socket since they are very vital
     * This is done so as to ensure that the Packets are not lost *
     * when sent through ICCL link */
    if (setsockopt (i4CliSockFd, SOL_SOCKET, SO_PRIORITY,
                    (char *) &i4precedence, sizeof (i4precedence)) < 0)
    {
        IcchCloseSocket (i4CliSockFd);
        return ICCH_FAILURE;
    }
#endif

    if (SelAddFd (i4CliSockFd, IcchPktRcvd) == ICCH_FAILURE)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                  "IcchTcpAcceptNewConnection : "
                  "SelAddFd Failure while adding ICCH TCP socket\r\n");
        IcchCloseSocket (i4CliSockFd);
        return ICCH_FAILURE;
    }
    SelAddFd (i4SrvSockFd, IcchTcpSrvSockCallBk);
    *pi4SockFd = i4CliSockFd;
    *pu4PeerAddr = u4IpAddr;
    return ICCH_SUCCESS;
}

/******************************************************************************
 * Function           : IcchTcpSockConnect
 * Input(s)           : u4PeerAddr -> Peer address to be connected
 *                      pi4Connfd -> Pointer to the connection fd
 *                      pi4ConnState -> Pointer to the connection status
 * Output(s)          : None
 * Returns            : ICCH_SUCCESS/ICCH_FAILURE
 * Action             : Routine used for connecting the TCP peer
 ******************************************************************************/
INT4
IcchTcpSockConnect (UINT4 u4PeerAddr, INT4 *pi4Connfd, INT4 *pi4ConnState)
{
    struct sockaddr_in  serv_addr;
    UINT4               u4RetVal = ICCH_SUCCESS;
    UINT4               u4SndBuf = ICCH_MAX_FRAGMENT;
    UINT4               u4RcvBuf = ICCH_MAX_FRAGMENT;
    INT4                i4SockFd = 0;
    INT4                i4RetVal = 0;
#ifdef LNXIP4_WANTED
    INT4                i4precedence = 7;
#endif
    INT4                i4Flags = 0;

    if (*pi4Connfd == ICCH_INV_SOCK_FD)
    {
        ICCH_TRC (ICCH_NOTIF_TRC, "IcchTcpSockConnect:"
                  " New socket connection \r\n");

        /* Open the tcp socket */
        i4SockFd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
        if (i4SockFd < 0)
        {
            ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                      "IcchTcpSockConnect: TCP client socket "
                      "creation failure !!!\r\n");
            u4RetVal = ICCH_FAILURE;
        }
        /* Get current socket flags */
        if (u4RetVal != ICCH_FAILURE)
        {
            if ((i4Flags = fcntl (i4SockFd, F_GETFL, 0)) < 0)
            {
                ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                          "IcchTcpSockConnect: TCP client Fcntl "
                          "GET Failure !!!\r\n");
                IcchCloseSocket (i4SockFd);
                u4RetVal = ICCH_FAILURE;
            }
        }
        if (u4RetVal != ICCH_FAILURE)
        {
            /* Set the socket is non-blocking mode */
            i4Flags |= O_NONBLOCK;
            if (fcntl (i4SockFd, F_SETFL, i4Flags) < 0)
            {
                ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                          "IcchTcpSockConnect: TCP client Fcntl "
                          "SET Failure !!!\r\n");
                IcchCloseSocket (i4SockFd);
                u4RetVal = ICCH_FAILURE;
            }
        }
        if (u4RetVal != ICCH_FAILURE)
        {
            MEMSET (&serv_addr, 0, sizeof (serv_addr));
            serv_addr.sin_family = AF_INET;
            serv_addr.sin_addr.s_addr = OSIX_HTONL (ICCH_GET_SELF_NODE_ID ());
            serv_addr.sin_port = 0;

            i4RetVal = bind (i4SockFd, (struct sockaddr *) &serv_addr,
                             sizeof (serv_addr));
            if (i4RetVal < 0)
            {
                perror ("IcchTcpSockConnect: Unable to bind with TCP client "
                        "source address \r\n");
                IcchCloseSocket (i4SockFd);
                u4RetVal = ICCH_FAILURE;
            }
        }

    }
    else
    {
        ICCH_TRC (ICCH_NOTIF_TRC,
                  "IcchTcpSockConnect: Reconnection in progress");
        i4SockFd = *pi4Connfd;
    }

    if (u4RetVal != ICCH_FAILURE)
    {
        MEMSET ((char *) &serv_addr, 0, sizeof (serv_addr));
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = OSIX_HTONL (u4PeerAddr);
        serv_addr.sin_port = OSIX_HTONS (ICCH_SYNC_TCP_SERVER_PORT);

        if (connect (i4SockFd, (struct sockaddr *) &serv_addr,
                     sizeof (serv_addr)) < 0)
        {
            if ((errno == EINPROGRESS) || (errno == EALREADY))
            {
                *pi4Connfd = i4SockFd;
                *pi4ConnState = ICCH_SOCK_CONNECT_IN_PROGRESS;
                /* call back for writing pkt in this socket */
                SelAddWrFd (i4SockFd, IcchTcpWriteCallBackFn);
                ICCH_TRC (ICCH_NOTIF_TRC, "IcchTcpSockConnect: "
                          "Connection in progress");
                return ICCH_SUCCESS;
            }
            ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                      "IcchTcpSockConnect: TCP client socket connect failed\r\n");
            IcchCloseSocket (i4SockFd);
            return ICCH_FAILURE;
        }

        *pi4Connfd = i4SockFd;
        *pi4ConnState = ICCH_SOCK_CONNECTED;
        ICCH_TRC (ICCH_NOTIF_TRC, "IcchTcpSockConnect: "
                  "Connection is successful \r\n");

        if (setsockopt (i4SockFd, SOL_SOCKET, SO_SNDBUF,
                        (VOID *) &u4SndBuf, sizeof (UINT4)) < 0)
        {
            IcchCloseSocket (i4SockFd);
            return ICCH_FAILURE;
        }

        if (setsockopt (i4SockFd, SOL_SOCKET, SO_RCVBUF,
                        (VOID *) &u4RcvBuf, sizeof (UINT4)) < 0)
        {
            IcchCloseSocket (i4SockFd);
            return ICCH_FAILURE;
        }

#ifdef LNXIP4_WANTED
        /* Priority Given to  Socket since they are very vital
         * This is done so as to ensure that the Packets are not lost *
         * when sent through ICCL link */
        if (setsockopt (i4SockFd, SOL_SOCKET, SO_PRIORITY,
                        (char *) &i4precedence, sizeof (i4precedence)) < 0)
        {
            IcchCloseSocket (i4SockFd);
            return ICCH_FAILURE;
        }
#endif

        /* call back for receiving pkt in this socket */
        if (SelAddFd (i4SockFd, IcchPktRcvd) == ICCH_FAILURE)
        {
            ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                      "IcchTcpAcceptNewConnection :"
                      "SelAddFd Failure while adding TCP client socket\r\n");
        }
        *pi4Connfd = i4SockFd;
        *pi4ConnState = ICCH_SOCK_CONNECTED;
        ICCH_TRC (ICCH_NOTIF_TRC, "IcchTcpSockConnect: "
                  "Connection is successful \r\n");
        return ICCH_SUCCESS;
    }
    ICCH_TRC (ICCH_ALL_FAILURE_TRC, "IcchTcpSockConnect: FAILED \r\n");
    return ICCH_FAILURE;
}

/******************************************************************************
 * Function           : IcchCloseSocket
 * Input(s)           : i4SockFd - Socket descriptor.
 * Output(s)          : None.
 * Returns            : 0 on success & -1 on failure
 * Action             : Routine to close the socket created for ICCH.
 ******************************************************************************/
INT4
IcchCloseSocket (INT4 i4SockFd)
{
    INT4                i4RetVal = ICCH_SUCCESS;

    if (i4SockFd == ICCH_INV_SOCK_FD)
    {
        return (i4RetVal);
    }
    i4RetVal = close (i4SockFd);
    if (i4RetVal < 0)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC, "!!! socket close Failure !!!\r\n");
    }
    return (i4RetVal);
}

/******************************************************************************
 * Function           : IcchTcpSockSend
 * Input(s)           : i4ConnFd - Connection fd
 *                      pu1Data - Pointer to data to be send
 *                      u2PktLen - Packet length
 * Output(s)          : i4WrBytes - No. of bytes written
 * Returns            : ICCH_SUCCESS/ICCH_FAILURE
 * Action             : Routine to send ICCH protocol packets.
 ******************************************************************************/
UINT4
IcchTcpSockSend (INT4 i4ConnFd, UINT1 *pu1Data, UINT2 u2PktLen,
                 INT4 *pi4WrBytes)
{
    INT4                i4WrBytes = 0;
    static INT2         i2SendRetries = 0;

    /* Send the file information to request the file */
    i4WrBytes = send (i4ConnFd, pu1Data, u2PktLen, MSG_NOSIGNAL);
    if (i4WrBytes <= 0)
    {
        /* Other than EWOULDBLOCK OR EAGAIN cases,
         * critical error occured in the socket*/
        if ((errno == EWOULDBLOCK) || (errno == EAGAIN))
        {
            i2SendRetries++;
            if (i2SendRetries == ICCH_MAX_RECV_SEND_RETRIES)
            {
                /* Recovery Mechanism :
                 * If send() is continuously returning EWOULDBLOCK for
                 * around 10 times, socket can be closed.
                 * This is required for client (slave) when server (Master)
                 * is down unexpectedly*/
                *pi4WrBytes = 0;
                /* Resetting retries */
                i2SendRetries = 0;
                return ICCH_SUCCESS;
            }
            else
            {
                *pi4WrBytes = 0;
                return ICCH_SUCCESS;
            }
        }
        *pi4WrBytes = 0;
        if (errno == ECONNRESET)
        {
            ICCH_TRC (ICCH_ALL_FAILURE_TRC, "\n IcchTcpSockSend: "
                      "send failed may be due to peer down detection/corrupted packet "
                      "on socket at the peer\n\r");
        }
        else
        {
            ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                      "\n IcchTcpSockSend: ICCH sync message "
                      "send failed \r\n");
        }
        /* Resetting retries */
        i2SendRetries = 0;
        return ICCH_FAILURE;
    }
    *pi4WrBytes = i4WrBytes;
    /* Resetting retries */
    i2SendRetries = 0;
    return ICCH_SUCCESS;
}

/******************************************************************************
 * Function           : IcchTcpSockRcv
 * Input(s)           : i4ConnFd - Connection fd
 *                      pu1Data - Pointer to data to be send
 *                      u2PktLen - Packet length
 * Output(s)          : pi4RdBytes - No. of bytes received
 * Returns            : ICCH_SUCCESS/ICCH_FAILURE
 * Action             : Routine to send ICCH protocol packets.
 ******************************************************************************/
UINT4
IcchTcpSockRcv (INT4 i4ConnFd, UINT1 *pu1Data, UINT2 u2MsgLen, INT4 *pi4RdBytes)
{
    INT4                i4RdBytes = 0;
    static INT2         i2RecvRetries = 0;

    i4RdBytes = recv (i4ConnFd, pu1Data, u2MsgLen, MSG_NOSIGNAL);

    if (i4RdBytes <= 0)
    {
        /* Other than EWOULDBLOCK OR EAGAIN cases,
         * critical error occured in the socket*/
        if ((errno == EWOULDBLOCK) || (errno == EAGAIN))
        {
            i2RecvRetries++;
            if (i2RecvRetries == ICCH_MAX_RECV_SEND_RETRIES)
            {
                /* Recovery Mechanism :
                 * If recv() is continuously returning EWOULDBLOCK for 
                 * around 10 times, socket can be closed.
                 * This is required for client (slave) when server (Master)
                 * is down unexpectedly*/
                *pi4RdBytes = i4RdBytes;
                /* Resetting retries */
                i2RecvRetries = 0;
                return ICCH_SUCCESS;
            }
            else
            {
                *pi4RdBytes = i4RdBytes;
                return ICCH_SUCCESS;
            }
        }
        ICCH_TRC (ALL_FAILURE_TRC, "\n IcchTcpSockRcv:"
                  "ICCH sync message receive failed \r\n");
        /* Resetting retries */
        i2RecvRetries = 0;
        return ICCH_FAILURE;
    }

    *pi4RdBytes = i4RdBytes;
    /* Resetting retries */
    i2RecvRetries = 0;
    return ICCH_SUCCESS;
}

/******************************************************************************
 * Function           : IcchUtilCloseSockConn
 * Input(s)           : u1SsnIndex - Session index
 * Output(s)          : None.
 * Returns            : None
 ******************************************************************************/
VOID
IcchUtilCloseSockConn (UINT1 u1SsnIndex)
{
    tIcchTxBufNode     *pIcchTxBufNode = NULL;
    UINT1               u1SsnCnt = 0;

    if (ICCH_GET_SSN_INFO (u1SsnIndex).i4ConnFd == ICCH_INV_SOCK_FD)
    {
        /* Clear the ICCH Tx queue and release the memory allocated */
        ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                  "IcchUtilCloseSockConn: Invalid socket close \r\n");
        IcchClearSyncMsgQueue ();
        return;
    }
    if (ICCH_SLL_COUNT
        (&ICCH_SSN_ENTRY_TX_LIST (ICCH_GET_SSN_INFO (u1SsnIndex))) != 0)
    {
        pIcchTxBufNode = (tIcchTxBufNode *) TMO_SLL_First
            (&(ICCH_SSN_ENTRY_TX_LIST (ICCH_GET_SSN_INFO (u1SsnCnt))));

        while (pIcchTxBufNode)
        {
            TMO_SLL_Delete (&
                            (ICCH_SSN_ENTRY_TX_LIST
                             (ICCH_GET_SSN_INFO (u1SsnCnt))),
                            &pIcchTxBufNode->TxNode);
            MemReleaseMemBlock (gIcchInfo.IcchPktMsgPoolId,
                                (UINT1 *) pIcchTxBufNode->pu1Msg);
            if (MemReleaseMemBlock
                (gIcchInfo.IcchTxBufMemPoolId,
                 (UINT1 *) pIcchTxBufNode) != MEM_SUCCESS)
            {
                ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                          "IcchUtilCloseSockConn: Mem release from ctrl msg "
                          "pool failed\r\n");
            }
            pIcchTxBufNode = (tIcchTxBufNode *) TMO_SLL_First
                (&(ICCH_SSN_ENTRY_TX_LIST (ICCH_GET_SSN_INFO (u1SsnCnt))));
        }                        /* End of while */

        SelRemoveWrFd (ICCH_GET_SSN_INFO (u1SsnIndex).i4ConnFd);
    }

    SelRemoveFd (ICCH_GET_SSN_INFO (u1SsnIndex).i4ConnFd);
    IcchCloseSocket (ICCH_GET_SSN_INFO (u1SsnIndex).i4ConnFd);
    IcchRxBufClearRxBuffer ();
    IcchRxResetRxBuffering ();
    IcchClearSyncMsgQueue ();
    ICCH_GET_SSN_INFO (u1SsnIndex).i4ConnFd = ICCH_INV_SOCK_FD;
    ICCH_GET_SSN_INFO (u1SsnIndex).i1ConnStatus = ICCH_SOCK_NOT_CONNECTED;
    ICCH_TRC (ICCH_NOTIF_TRC, "IcchUtilCloseSockConn: socket close done \r\n");
    IcchTmrStopTimer (ICCH_SEQ_RECOV_TIMER);
    IcchTmrStopTimer (ICCH_ACK_RECOV_TIMER);
    ICCH_IS_RX_BUFF_PROCESSING_ALLOWED () = ICCH_TRUE;
    return;
}
