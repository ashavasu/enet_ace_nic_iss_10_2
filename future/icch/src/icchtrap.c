/*****************************************************************************/
/* $Id: icchtrap.c,v 1.8 2015/11/04 11:22:15 siva Exp $                  */
/* Copyright (C) 2015 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : icchtrap.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        :                                                */
/*    MODULE NAME           : ICCH                                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE : 12 Jan 2015                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains routines for ICCH module    */
/*                            to generate traps during state change          */
/*---------------------------------------------------------------------------*/
#ifndef ICCHSNMP_C
#define ICCHSNMP_C
#include "fssnmp.h"
#include "snmputil.h"
#include "icchincs.h"
#undef _SNMP_MIB_H
#include "fsicch.h"
#undef _SNMP_MIB_H

PRIVATE tSNMP_OID_TYPE *MakeObjIdFromDotNew (INT1 *textStr);

PRIVATE INT4        ParseSubIdNew (UINT1 **pptempPtr);
PRIVATE UINT4       ga_ICCH_SNMP2_TRAP_OID[] =
    { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
PRIVATE UINT4       ga_SNMP2_TRAP_OID[] =
    { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };

/*****************************************************************************/
/*  Function Name   :   IcchTrapSendNotifications                            */
/*  Description     :   This function sends the trap info. to SNMP agent     */
/*  Input(s)        :   u4Trap - The trap id                                 */
/*  Output(s)       :   None                                                 */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  gIcchInfo.u4NodeState - State of Node     */
/*                                 gIssSysGroupInfo.u4IssIcchIpAddress -     */
/*                                 Node Id                                   */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
PUBLIC VOID
IcchTrapSendNotifications (UINT4 u4Trap)
{
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT1               au1Buf[ICCH_MAX_BUFFER_LENGTH];
    tSNMP_COUNTER64_TYPE u8CounterVal = { ZERO, ZERO };

    MEMSET (au1Buf, 0, ICCH_MAX_BUFFER_LENGTH);

    pEnterpriseOid = alloc_oid (ICCH_SNMP_V2_TRAP_OID_LEN);
    if (NULL == pEnterpriseOid)
    {
        return;
    }

    MEMCPY (pEnterpriseOid->pu4_OidList, ga_ICCH_SNMP2_TRAP_OID,
            sizeof (ga_ICCH_SNMP2_TRAP_OID));
    pEnterpriseOid->u4_Length = pEnterpriseOid->u4_Length - ICCH_TWO;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = 0;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u4Trap;
    pSnmpTrapOid = alloc_oid (ICCH_SNMP_V2_TRAP_OID_LEN);
    if (NULL == pSnmpTrapOid)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }

    MEMCPY (pSnmpTrapOid->pu4_OidList, ga_SNMP2_TRAP_OID,
            ICCH_SNMP_V2_TRAP_OID_LEN * sizeof (UINT4));
    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       ZERO, ZERO, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    if (NULL == pVbList)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    pStartVb = pVbList;
    if (u4Trap ==  ICCH_STATUS_CHANGE_TRAP)
    {
        /* Fill the node ID */
        SPRINTF ((CHR1 *) au1Buf, "fsIcclSessionIpAddress");
        if (NULL == (pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)))
        {
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            SNMP_free_snmp_vb_list (pStartVb);
            return;
        }

        pVbList->pNextVarBind =
            (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                     SNMP_DATA_TYPE_INTEGER32,
                                                     ZERO, (INT4) (ICCH_GET_SELF_NODE_ID()),
                                                     NULL, NULL,
                                                     u8CounterVal);

        if (NULL == pVbList->pNextVarBind)
        {
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            SNMP_FreeOid (pOid);
            SNMP_free_snmp_vb_list (pStartVb);
            return;
        }

        pVbList = pVbList->pNextVarBind;

        /* Fill state of the node */
        SPRINTF ((CHR1 *) au1Buf, "fsIcclSessionNodeState");
        if (NULL == (pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)))
        {
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            SNMP_free_snmp_vb_list (pStartVb);
            return;
        }

        pVbList->pNextVarBind =
            (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                     SNMP_DATA_TYPE_INTEGER32,
                                                     ZERO, 
                                                     ICCH_GET_NODE_STATE () , NULL, NULL,
                                                     u8CounterVal);

        if (NULL == pVbList->pNextVarBind)
        {
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            SNMP_FreeOid (pOid);
            SNMP_free_snmp_vb_list (pStartVb);
            return;
        }

        pVbList = pVbList->pNextVarBind;
    }
    else if (u4Trap == ICCH_ENABLE_PROTO_SYNC_TRAP)
    {
        /* Fill Protocol sync status */
        SPRINTF ((CHR1 *) au1Buf, "fsIcchEnableProtoSync");
        if (NULL == (pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)))
        {
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            SNMP_free_snmp_vb_list (pStartVb);
            return;
        }

        pVbList->pNextVarBind =
            (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                     SNMP_DATA_TYPE_INTEGER32,
                                                     ZERO, (INT4) (gIcchInfo.u4EnableProtoSync),
                                                     NULL, NULL,
                                                     u8CounterVal);

        if (NULL == pVbList->pNextVarBind)
        {
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            SNMP_FreeOid (pOid);
            SNMP_free_snmp_vb_list (pStartVb);
            return;
        }

        pVbList = pVbList->pNextVarBind;


    }
    else if (u4Trap == ICCH_PEER_STATUS_CHANGE_TRAP)
    {
        /* Fill the Peer node ID */
        SPRINTF ((CHR1 *) au1Buf, "fsIcchPeerNodeIpAddress");
        if (NULL == (pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)))
        {
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            SNMP_free_snmp_vb_list (pStartVb);
            return;
        }

        pVbList->pNextVarBind =
            (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                     SNMP_DATA_TYPE_INTEGER32,
                                                     ZERO, (INT4) (ICCH_GET_PEER_NODE_ID),
                                                     NULL, NULL,
                                                     u8CounterVal);

        if (NULL == pVbList->pNextVarBind)
        {
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            SNMP_FreeOid (pOid);
            SNMP_free_snmp_vb_list (pStartVb);
            return;
        }

        pVbList = pVbList->pNextVarBind;

        /* Fill state of the peer node */
        SPRINTF ((CHR1 *) au1Buf, "fsIcchPeerNodeState");
        if (NULL == (pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)))
        {
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            SNMP_free_snmp_vb_list (pStartVb);
            return;
        }

        pVbList->pNextVarBind =
            (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                     SNMP_DATA_TYPE_INTEGER32,
                                                     ZERO, (INT4) (gIcchInfo.u4PeerNodeState), 
                                                     NULL, NULL,
                                                     u8CounterVal);
        if (NULL == pVbList->pNextVarBind)
        {
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            SNMP_FreeOid (pOid);
            SNMP_free_snmp_vb_list (pStartVb);
            return;
        }

        pVbList = pVbList->pNextVarBind;
    }

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
    return;
}

/*****************************************************************************/
/*  Function Name   :   MakeObjIdFromDotNew                                  */
/*  Description     :   This function gets the OID from the object           */
/*  Input(s)        :   textStr                                              */
/*  Output(s)       :   None                                                 */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  poidPtr or NULL                           */
/*****************************************************************************/

PRIVATE tSNMP_OID_TYPE *
MakeObjIdFromDotNew (INT1 *textStr)
{
    tSNMP_OID_TYPE     *poidPtr = NULL;
    INT1               *ptempPtr = NULL, *pdotPtr = NULL;
    INT2                i2Temp = 0;
    UINT2               u2dotCount = 0;
    UINT1              *pu1TmpPtr = NULL;
    UINT4               u4Len = 0;
    INT1                ai1tempBuffer[ICCH_MAX_BUFFER_LENGTH +
                                      ICCH_ONE];
    INT2                i2OidTableSize;
    struct MIB_OID     *mib_oid_table = NULL;

    mib_oid_table = orig_mib_oid_table;
    i2OidTableSize = (INT2) sizeof (orig_mib_oid_table);
    
    MEMSET (ai1tempBuffer, 0, ICCH_MAX_BUFFER_LENGTH + ICCH_ONE);

    /* see if there is an alpha descriptor at begining */
    if (0 != ISALPHA (*textStr))
    {
        pdotPtr = (INT1 *) STRCHR ((INT1 *) textStr, '.');

        /* if no dot, point to end of string */
        if (NULL == pdotPtr)
        {
            pdotPtr = textStr + STRLEN ((INT1 *) textStr);
        }
        ptempPtr = textStr;

        for (i2Temp = 0; ((ptempPtr < pdotPtr) &&
                          (i2Temp < ICCH_MAX_BUFFER_LENGTH)); i2Temp++)
        {
            ai1tempBuffer[i2Temp] = *ptempPtr++;
        }
        ai1tempBuffer[i2Temp] = '\0';

        for (i2Temp = 0;
             ((i2Temp < (i2OidTableSize / (INT2) sizeof (struct MIB_OID))) &&
              (mib_oid_table[i2Temp].pName) != NULL); i2Temp++)
        {
            if (0 == (STRCMP (mib_oid_table[i2Temp].pName,
                         (INT1 *) ai1tempBuffer))
                && (STRLEN ((INT1 *) ai1tempBuffer) ==
                    STRLEN (mib_oid_table[i2Temp].pName)))
            {
                u4Len = ((STRLEN (mib_oid_table[i2Temp].pNumber) <
                          sizeof (ai1tempBuffer)) ?
                         STRLEN (mib_oid_table[i2Temp].pNumber) :
                         sizeof (ai1tempBuffer) - 1);
                STRNCPY ((INT1 *) ai1tempBuffer,
                         mib_oid_table[i2Temp].pNumber, u4Len);
                ai1tempBuffer[u4Len] = '\0';
                break;
            }
        }

        if ((i2Temp < (i2OidTableSize / (INT2) sizeof (struct MIB_OID))) &&
            (NULL == mib_oid_table[i2Temp].pName))
        {
            return (NULL);
        }

        /* now concatenate the non-alpha part to the begining */
        if (STRLEN (ai1tempBuffer) + STRLEN (pdotPtr) < sizeof (ai1tempBuffer))
        {
            STRCAT ((INT1 *) ai1tempBuffer, (INT1 *) pdotPtr);
        }
    }
    else
    {   /* is not alpha, so just copy into ai1tempBuffer */
        STRCPY ((INT1 *) ai1tempBuffer, (INT1 *) textStr);
    }
    /* Now we've got something
     * with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2dotCount = 0;
    for (i2Temp = 0; ((i2Temp < ICCH_MAX_BUFFER_LENGTH) &&
                      (ai1tempBuffer[i2Temp] != '\0')); i2Temp++)
    {
        if ('.' == ai1tempBuffer[i2Temp])
        {
            u2dotCount++;
        }
    }
    if (NULL == (poidPtr = alloc_oid ((INT4)
                              (u2dotCount + ICCH_ONE))))
    {
        return (NULL);
    }

    poidPtr->u4_Length = (UINT4) (u2dotCount + 1);

    /* now we convert number.number.... strings */
    pu1TmpPtr = (UINT1 *) ai1tempBuffer;
    for (i2Temp = 0; i2Temp < u2dotCount + ICCH_ONE; i2Temp++)
    {
        if ((UINT4) -ICCH_ONE == (poidPtr->pu4_OidList[i2Temp] =
             ((UINT4) (ParseSubIdNew (&pu1TmpPtr))))) 
        {
            free_oid (poidPtr);
            return (NULL);
        }
        if ('.' == *pu1TmpPtr)
        {
            pu1TmpPtr++;        /* to skip over dot */
        }
        else if (*pu1TmpPtr != '\0')
        {
            free_oid (poidPtr);
            return (NULL);
        }
    }                            /* end of for loop */

    return (poidPtr);
}

/*****************************************************************************/
/*  Function Name   :   ParseSubIdNew                                        */
/*  Description     :   This function parses the sub ID                      */
/*  Input(s)        :   textStr                                              */
/*  Output(s)       :   None                                                 */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  i4value of ptempPtr or -1                 */
/*****************************************************************************/

PRIVATE INT4
ParseSubIdNew (UINT1 **pptempPtr)
{
    INT4                i4value = 0;
    UINT1              *ptmp = NULL;

    for (ptmp = *pptempPtr; (((*ptmp >= '0') && (*ptmp <= '9')) ||
                             ((*ptmp >= 'a') && (*ptmp <= 'f')) ||
                             ((*ptmp >= 'A') && (*ptmp <= 'F'))); ptmp++)
    {
        i4value = (i4value * 10) + (*ptmp & 0xf);
    }

    if (*pptempPtr == ptmp)
    {
        i4value = -ICCH_ONE;
    }
    *pptempPtr = ptmp;
    return (i4value);
}
#endif /* ICCHSNMP_C */
