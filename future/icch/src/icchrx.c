/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: icchrx.c,v 1.12 2016/06/29 11:22:22 siva Exp $
*
* Description: ICCH utility functions for processing of received sync-up
* messages.
***********************************************************************/
#ifndef _ICCHRX_C_
#define _ICCHRX_C_
#include "icchincs.h"
#include "fssocket.h"
#include "icchglob.h"

/******************************************************************************
 * Function           : IcchRxProcessBufferedPkt
 * Input(s)           : None.
 * Output(s)          : None
 * Returns            : ICCH_SUCCESS / ICCH_FAILURE
 * Action             : Function to process the buffered sync-up message
 *                      present in the RX buffer.
 ******************************************************************************/
INT4
IcchRxProcessBufferedPkt (VOID)
{
    UINT4                u4NextSeqNumber = 0;
    tIcchRxBufHashEntry  *pRxBufNode = NULL;

    if (ICCH_IS_RX_BUFF_PROCESSING_ALLOWED () == ICCH_FALSE)
    {
        ICCH_TRC (ICCH_NOTIF_TRC, "IcchRxProcessBufferedPkt: Rx buffer is LOCKED\r\n");
        return ICCH_SUCCESS;
    }

    if (ICCH_RX_BUFF_PKT_COUNT () == 0)
    {
        ICCH_TRC (ICCH_NOTIF_TRC, "IcchRxProcessBufferedPkt: Rx buffer is Empty\r\n");
        return ICCH_SUCCESS;
    }

    /* Get Next valid seqence number that needs to be fetched from the buffer */
    u4NextSeqNumber = ICCH_RX_GETNEXT_VALID_SEQNUM ();

    /* Search this seqence numbered pkt in the rx buffer */
    pRxBufNode = IcchRxBufSearchPkt (u4NextSeqNumber);

    if (pRxBufNode == NULL)
    {
            /* Expected sequence numberd packet is not present in the buffer */
            IcchRxSeqNumMisMatchHandler (u4NextSeqNumber);
            return ICCH_SUCCESS;
    }

    /* Valid sequence numbered message found in the rx buffer */
    if (ICCH_RX_SEQ_MISSMATCH_FLG () == ICCH_TRUE)
        /* valid seq number received while seq recovery timer is running */
    {
        IcchTmrStopTimer (ICCH_SEQ_RECOV_TIMER);
        ICCH_RX_SEQ_MISSMATCH_FLG () = ICCH_FALSE;
    }

    /* Update the last processed sequence number */
    ICCH_RX_INCR_LAST_SEQNUM_PROCESSED ();

    /* Post the buffer packet to protocol */
    if (IcchRxPostBufferedPktToProtocol (pRxBufNode) == ICCH_FAILURE)
    {
        /* Increment the Last Ack received count as the Ack timer is not
         * going to be started */
        ICCH_RX_LAST_ACK_RCVD_SEQ_NUM () = pRxBufNode->u4SeqNum;

        ICCH_TRC1 (ICCH_ALL_FAILURE_TRC, "IcchRxProcessBufferedPkt: Failed to "
                   "process the message with Seq# %d\r\n", u4NextSeqNumber);
        /* Precess the next bufferred pkt if any */
        if (ICCH_RX_BUFF_PKT_COUNT () != 0)
        {
            /* Post a self event which will invoke IcchRxProcessBufferedPkt
             * to process the next buffered packet. This is done to
             * avoid the recursive call to IcchRxProcessBufferedPk() from
             * here */
            if (ICCH_SEND_EVENT (ICCH_TASK_ID, ICCH_PROCS_RX_BUFF_SELF_EVENT) ==
                OSIX_FAILURE)
            {
                ICCH_TRC (ICCH_ALL_FAILURE_TRC, "Send Event "
                          "ICCH_PROCS_RX_BUFF_SELF_EVENT Failed\r\n");
            }
        }
    }
    else                        /* Pkt is posted to Protocol Successfully */
    {
        /* Block the rx buffer processing, until ACK is received for this message
         * form protocol. */
        ICCH_IS_RX_BUFF_PROCESSING_ALLOWED () = ICCH_FALSE;
        ICCH_TRC (ICCH_NOTIF_TRC, "IcchRxPostBufferedPktToProtocol: "
                  "LOCKING Rx Buff\r\n");
        /* Update statistics - for the buffered messages that is processed */
        ICCH_INCR_RX_STAT_SYNC_MSG_PROCESSED ();

        /* Start the ACK recovery timer */
        IcchTmrStartTimer (ICCH_ACK_RECOV_TIMER, ICCH_ACK_RECOV_TMR_INTERVAL);
    }

    /* Delete the Hash node from the rx buffer for this message */
    IcchRxBufDeleteNode (pRxBufNode);

    ICCH_TRC1 (ICCH_SYNC_MSG_TRC, 
               "IcchRxProcessBufferedPkt: Hash node count = %d\r\n",
               ICCH_RX_BUFF_PKT_COUNT ());

    return ICCH_SUCCESS;
}

/******************************************************************************
 * Function           : IcchRxPostBufferedPktToProtocol
 * Input(s)           : pRxBufNode - Hash node pointer for the buffered
 *                                   message present in the RX Buffer.
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS / ICCH_FAILURE
 * Action             : Allocate CRU buffer and copy the entire packet present
 *                      in the hash node to this buffer. This CRU buffer is
 *                      used to post the sync-up message to the destination
 *                      protocol based on u4DestEntId present in the ICCH header.
 *                      ICCH Header will not be stripped out from the CRU buffer.
 *                      protocol should take care of it.
 *                      NOTE: pRxBufNode->pu1Pkt will be freed by the caller
 *                      case of ICCH_FAILURE / ICCH_SUCCESS both the cases. It
 *                      will be freed by Hash Delete node function.
 ******************************************************************************/
INT4
IcchRxPostBufferedPktToProtocol (tIcchRxBufHashEntry * pRxBufNode)
{
    tIcchMsg            *pPkt = NULL;
    UINT4               u4SrcEntId=0;
    UINT4               u4DestEntId=0;
    UINT4               u4BufCount=0;
    if (pRxBufNode->pu1Pkt == NULL || pRxBufNode->u4PktLen == 0)
    {
        /* Packet buffer will be freed by the caller */
        ICCH_TRC (ICCH_ALL_FAILURE_TRC, 
                  "IcchRxPostBufferedPktToProtocol: "
                  "Invalid packet present in the Rx Buffer.\r\n");
        return ICCH_FAILURE;
    }

    if (pRxBufNode->u4SeqNum == ICCH_SEQNUM_WRAP_AROUND_VALUE)
    {
        ICCH_TRC (ICCH_SYNC_MSG_TRC, 
                  "IcchRxPostBufferedPktToProtocol: "
                  "WRAP AROUND happened for sequence numbers\r\n");
    }

    /* Allocate memory for CRU buffer. This mem will be freed by appls.
     * after processing. So, do not free this mem here. */
    if ((pPkt = ICCH_ALLOC_RX_BUF (pRxBufNode->u4PktLen)) == NULL)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC, 
                  "EXIT: Unable to allocate buf\r\n");
        return ICCH_FAILURE;
    }

    /* copy the linear buffer to CRU buffer */
    if (ICCH_COPY (pPkt, pRxBufNode->pu1Pkt, pRxBufNode->u4PktLen) == CRU_FAILURE)
    {
        ICCH_FREE (pPkt);            /* Release the CRU Buffer */
        ICCH_TRC (ICCH_ALL_FAILURE_TRC, 
                  "EXIT: Unable to copy linear to CRU buf\r\n");
        return ICCH_FAILURE;
    }

    /* pPkt should not be freed here. it will be freed while deleting the
     * rx buff node */

    ICCH_GET_DATA_4_BYTE (pPkt, ICCH_HDR_OFF_SRC_ENTID, u4SrcEntId);
    ICCH_GET_DATA_4_BYTE (pPkt, ICCH_HDR_OFF_DEST_ENTID, u4DestEntId);

    /* Update the last processed AppId */
    ICCH_RX_LAST_APPID_PROCESSED () = u4DestEntId;

    /* Post the message to protocol */
    u4BufCount = ICCH_RX_BUFF_PKT_COUNT () - 1;
    if (IcchUtilPostDataOrEvntToProtocol (u4DestEntId, (UINT1) ICCH_MESSAGE,
                                        pPkt, pRxBufNode->u4PktLen,
                                        pRxBufNode->u4SeqNum) == ICCH_FAILURE)
    {
        ICCH_FREE (pPkt);            /* Release the CRU Buffer */
        ICCH_TRC3 (ICCH_ALL_FAILURE_TRC | ICCH_SYNC_MSG_TRC |
                   ICCH_NOTIF_TRC, "IcchRxPostBufferedPktToProtocol: "
                   "HASH DROPPED [Seq# %d AppId-%s], "
                   "(hash count = %d)\r\n", pRxBufNode->u4SeqNum,
                   gaIcchAppName[u4DestEntId], u4BufCount);

        return ICCH_FAILURE;
    }

    ICCH_TRC3 ( ICCH_SYNC_MSG_TRC | ICCH_NOTIF_TRC, 
               "IcchRxPostBufferedPktToProtocol: "
               "HASH PROCESSED [Seq# %d AppId-%s], "
               "(hash count = %d)\r\n", pRxBufNode->u4SeqNum,
               gaIcchAppName[u4DestEntId], u4BufCount);

    return ICCH_SUCCESS;
}

/******************************************************************************
 * Function           : IcchRxSeqNumMisMatchHandler
 * Input(s)           : u4SeqNum - Sequence number that is not found in the
 *                                 RX Buffer.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This Function handles the situation is the desired
 *                      sequence number is not found in the RX Buffer while
 *                      processing. It starts the sequence recovery timer.
 *                      In case node transition in progress, no need to handle
 *                      the situation.
 ******************************************************************************/
VOID
IcchRxSeqNumMisMatchHandler (UINT4 u4SeqNum)
{
    ICCH_TRC2 (ICCH_NOTIF_TRC | ICCH_SYNC_MSG_TRC,
     "IcchRxSeqNumMisMatchHandler: HASH MISSED[Seq# %d], "
     "(hash count = %d)\r\n", u4SeqNum, ICCH_RX_BUFF_PKT_COUNT ());
    if (ICCH_RX_SEQ_MISSMATCH_FLG () == RM_TRUE)
    {
        ICCH_TRC (ICCH_NOTIF_TRC, "RmRxSeqNumMisMatchHandler: Seq Recov timer "
                "is already running\r\n");
        return;
    }
    else
    {

        ICCH_RX_SEQ_MISSMATCH_FLG () = ICCH_TRUE;
        IcchTmrStartTimer (ICCH_SEQ_RECOV_TIMER, ICCH_SEQ_RECOV_TMR_INTERVAL);
        ICCH_IS_RX_BUFF_PROCESSING_ALLOWED () = ICCH_TRUE;
        ICCH_TRC (ICCH_SYNC_MSG_TRC, 
                "IcchRxSeqNumMisMatchHandler: UNLOCKING Rx Buff\r\n");
    }
    return;
}

/******************************************************************************
 * Function           : IcchRxResetRxBuffering
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function is used to reset the flags and counters
 *                      used for the RX buffer.
 ******************************************************************************/
VOID
IcchRxResetRxBuffering (VOID)
{
    ICCH_TRC (ICCH_SYNC_MSG_TRC, "IcchRxResetRxBuffering: "
              "LOCKING Rx Buff\r\n");

    ICCH_RX_SEQ_MISSMATCH_FLG () = ICCH_FALSE;
    ICCH_RX_LAST_SEQ_NUM_PROCESSED () = 0;
    ICCH_TX_LAST_SEQ_NUM_PROCESSED () = 0;
    ICCH_RX_LAST_ACK_RCVD_SEQ_NUM () = 0;
    ICCH_RX_LAST_APPID_PROCESSED () = ICCH_APP_ID;
    return;
}

/******************************************************************************
 * Function           : IcchProcessRcvEvt
 * Input(s)           : pIcchSsnInfo - ICCH session information
 * Output(s)          : None
 * Returns            : ICCH_SUCCESS/ICCH_FAILURE
 * Action             : Routine to receive ICCH sync messages
 ******************************************************************************/
UINT4
IcchProcessRcvEvt (tIcchSsnInfo * pIcchSsnInfo)
{
    UINT1              *pu1RcvPkt = NULL;
    UINT1              *pu1Data = NULL;
    UINT1              *pu1Start = NULL;
    UINT4               u4DataLen = 0;
    UINT4               u4RelinquishCntr = 0;
    UINT4               u4RetVal = ICCH_SUCCESS;
    INT4                i4RecvBytes = 0;
    UINT2               u2HeaderLen = ICCH_HDR_LENGTH;
    UINT2               u2Cksum = 0;
    UINT1               u1SsnIndex = 0;
    BOOL1               bIsPartialDataRcvd = ICCH_FALSE;


    if (gIcchInfo.bIsRxBuffFull == ICCH_TRUE)
    {
        ICCH_TRC (ICCH_SYNC_EVT_TRC, 
                  "IcchProcessRcvEvt: Rx buffer is full \r\n");
        if (SelAddFd (pIcchSsnInfo->i4ConnFd, IcchPktRcvd) == OSIX_FAILURE)
        {
            ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                      "IcchProcessRcvEvt: SelAddFd Failure "
                      "while adding ICCH reliable socket\r\n");
        }
        return ICCH_SUCCESS;
    }
    ICCH_RXBUF_PKT_MEM_ALLOC (pu1RcvPkt, ICCH_MAX_SYNC_PKT_LEN);
    if (pu1RcvPkt == NULL)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC | ICCH_CRITICAL_TRC,
                  "IcchProcessRcvEvt: "
                  "Unable to allocate memory for ICCH dynamic sync message\r\n");
        return ICCH_FAILURE;
    }

    MEMSET (pu1RcvPkt, 0, ICCH_MAX_SYNC_PKT_LEN);
    /* Do continuous ICCH synchronization message read */
    while (1)
    {
        /* ICCH_SYNC_PDU =  ICCH_HDR_LENGTH + SYNC_DATA.
         * First Read the ICCH_DATA_LEN and based on
         * ICCH_DATA_LEN Read SYNC_DATA. If data is not available
         * fully during ICCH_DATA_LEN or SYNC_DATA the copy the
         * read data to pIcchSsnInfo->u2ResBufLen and update
         * pIcchSsnInfo->u2ResBufLen */

        ICCH_TRC (ICCH_SYNC_EVT_TRC, "IcchProcessRcvEvt: ICCH sync packets read\r\n");
        pu1Data = pu1Start = pu1RcvPkt;
        u2HeaderLen = ICCH_HDR_LENGTH;
        u4DataLen = 0;

        /* If Residual Data is present then First Copy the residual
         * data to pu1Data */

        if (pIcchSsnInfo->u2ResBufLen != 0)
        {
            ICCH_TRC1 (ICCH_SYNC_EVT_TRC,
                       "IcchProcessRcvEvt: Sync packets are exist "
                       "in residual buffer and size is=%d\r\n",
                       pIcchSsnInfo->u2ResBufLen);

            /* This routine copies the residual buffer data to linear buffer
             * and based on the residual buffer length, it determines the
             * ICCH header OR payload length to be read */
            /* Copy the residual buffer data to linear buffer */
            MEMCPY (pu1Data, pIcchSsnInfo->pResBuf, pIcchSsnInfo->u2ResBufLen);
            pu1Data += pIcchSsnInfo->u2ResBufLen;
            if (IcchHandleResBufData (pu1Start, pIcchSsnInfo,
                                    &u2HeaderLen, &u4DataLen) == ICCH_FAILURE)
            {
                pIcchSsnInfo->u2ResBufLen = 0;
                u4RetVal = ICCH_FAILURE;
                break;
            }
        }

        /* Read Header If it is already read and present in
         * residual data then this may zero */
        if (u2HeaderLen != 0)
        {
            ICCH_TRC1 (ICCH_SYNC_EVT_TRC, "IcchProcessRcvEvt: "
                       "ICCH header read to be started for size =%d\r\n",
                       u2HeaderLen);
            if (IcchRecvIcchHdrOrPayLoadFromConnFd
                (&pu1Data, pu1Start, pIcchSsnInfo, u2HeaderLen,
                 &bIsPartialDataRcvd) == ICCH_FAILURE)
            {
                u4RetVal = ICCH_FAILURE;
                break;
            }
            if (bIsPartialDataRcvd == ICCH_TRUE)
            {
                break;
            }
            /* If Full Header is present then read the Data Length
             * from the Header */
            MEMCPY ((UINT1 *) &u4DataLen,
                    (pu1Start + ICCH_HDR_OFF_TOT_LENGTH), sizeof (UINT4));
            u4DataLen = OSIX_NTOHL (u4DataLen);
            /* ICCH total length is [ICCH header length + payload length] so,
             * exclude the ICCH header to get the payload length */
            u4DataLen -= ICCH_HDR_LENGTH;
            if (u4DataLen > (ICCH_MAX_SYNC_PKT_LEN - ICCH_HDR_LENGTH))
            {
                /* Data Length is more than our  Buffer size so,
                 * drop the data. This may be peer data send
                 * issue  */
                ICCH_TRC1 (ICCH_ALL_FAILURE_TRC, "IcchProcessRcvEvt: "
                           "ICCH Data Length is more than the Buffer size "
                           "so, packet dropped size=%d\r\n", u4DataLen);
                pIcchSsnInfo->u2ResBufLen = 0;
                u4RetVal = ICCH_FAILURE;
                break;
            }
        }

        /* Read Data */
        if (u4DataLen != 0)
        {
            ICCH_TRC1 (ICCH_SYNC_EVT_TRC, "IcchProcessRcvEvt: "
                       "ICCH payload read to be started for size =%d\r\n",
                       u4DataLen);

            if (IcchRecvIcchHdrOrPayLoadFromConnFd
                (&pu1Data, pu1Start, pIcchSsnInfo, (UINT2) u4DataLen,
                 &bIsPartialDataRcvd) == ICCH_FAILURE)
            {
                u4RetVal = ICCH_FAILURE;
                break;
            }
            if (bIsPartialDataRcvd == ICCH_TRUE)
            {
                break;
            }
        }

        i4RecvBytes = pu1Data - pu1Start;
        pIcchSsnInfo->u2ResBufLen = 0;
        u2Cksum = ICCH_LINEAR_CALC_CKSUM ((const INT1 *) pu1RcvPkt, (UINT4) i4RecvBytes);
        i4RecvBytes = MEM_MAX_BYTES (i4RecvBytes, ICCH_MAX_SYNC_PKT_LEN);
        if (u2Cksum != 0)
        {
            ICCH_TRC (ICCH_CRITICAL_TRC | ICCH_ALL_FAILURE_TRC,
                      "!!! Pkt rcvd with invalid cksum. Discarding.... !!!\r\n");
            continue;
        }
        if (IcchHandleCompleteSyncMsg (pu1RcvPkt, i4RecvBytes) == ICCH_FAILURE)
        {
            if (gIcchInfo.bIsRxBuffFull == ICCH_TRUE)
            {
                ICCH_TRC (ICCH_SYNC_MSG_TRC | ICCH_CRITICAL_TRC | 
                          ICCH_ALL_FAILURE_TRC | ICCH_NOTIF_TRC,
                          "!!! Sync message reception is stopped "
                          "because Rx buffer is full !!!\r\n");
                u4RetVal = ICCH_SUCCESS;
            }
            else
            {
                u4RetVal = ICCH_FAILURE;
            }
            break;
        }
        /* This Relinquish point is for processing
         * the higher priority messages */
        u4RelinquishCntr++;
        if (u4RelinquishCntr > ICCH_RELINQUISH_CNTR)
        {
            ICCH_TRC (ICCH_SYNC_MSG_TRC, "IcchProcessRcvEvt: Relinquished from "
                      "Sync. message reception \r\n");
            IcchPktRcvd (pIcchSsnInfo->i4ConnFd);
            break;
        }
    }

    if (u4RetVal == ICCH_SUCCESS)
    {
        if (SelAddFd (pIcchSsnInfo->i4ConnFd, IcchPktRcvd) == OSIX_FAILURE)
        {
            ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                      "IcchProcessRcvEvt: SelAddFd Failure "
                      "while adding ICCH reliable socket\r\n");
        }
    }
    else
    {
        if (IcchGetSsnIndexFromConnFd (pIcchSsnInfo->i4ConnFd,
                                     &u1SsnIndex) == ICCH_FAILURE)
        {
            ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                      "IcchProcessRcvEvt: Session does not exist "
                      "for the connection fd\r\n");
        }
        IcchUtilCloseSockConn (u1SsnIndex);
    }
    ICCH_RXBUF_PKT_MEM_FREE (pu1RcvPkt);
    return u4RetVal;
}

/******************************************************************************
 * Function           : IcchHandleResBufData
 * Input(s)           : pu1Data - Pointer to Linear buffer
 *                      pu1Start - Pointer to starting location of the pointer
                        pIcchSsnInfo - Session information
                        pu2HeaderLen - Pointer to ICCH header length
                        pu4DataLen - Pointer to ICCH data length
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS / ICCH_FAILURE
 * Action             : Routine copies the residual buffer data to linear buffer
                        and determines the ICCH header length and ICCH payload length
                        based on the residual buffer data length.
 ******************************************************************************/
UINT4
IcchHandleResBufData (UINT1 *pu1Start, tIcchSsnInfo * pIcchSsnInfo,
                    UINT2 *pu2HeaderLen, UINT4 *pu4DataLen)
{
    UINT4               u4IcchDataLen = 0;
    UINT2               u2IcchHdrLen = 0;

    
    /* Get the ICCH header length to be read based on the
       residual buffer data */
    if (pIcchSsnInfo->u2ResBufLen < ICCH_HDR_LENGTH)
    {
        ICCH_TRC (ICCH_SYNC_MSG_TRC, "IcchHandleResBufData: "
                  "Residual sync packet < ICCH header length\r\n");
        u2IcchHdrLen = (UINT2) (ICCH_HDR_LENGTH - pIcchSsnInfo->u2ResBufLen);
    }
    else
    {
        ICCH_TRC (ICCH_SYNC_MSG_TRC, "IcchHandleResBufData: "
                  "Residual sync packet >= ICCH header length\r\n");
        u2IcchHdrLen = 0;
        /* If Full Header is present then read the Data Length
         * from the Header */
        MEMCPY ((UINT1 *) &u4IcchDataLen,
                (pu1Start + ICCH_HDR_OFF_TOT_LENGTH), sizeof (UINT4));
        u4IcchDataLen = OSIX_NTOHL (u4IcchDataLen);
        /* ICCH total length is [ICCH header length + payload length] so,
           exclude the ICCH header to get the payload length */
        u4IcchDataLen -= ICCH_HDR_LENGTH;

        if (u4IcchDataLen > (ICCH_MAX_SYNC_PKT_LEN - ICCH_HDR_LENGTH))
        {
            /* Data Length is more than our  Buffer size so,
             * drop the data. This may be peer data send
             * issue  */

            ICCH_TRC1 (ICCH_ALL_FAILURE_TRC, "IcchHandleResBufData: "
                       "ICCH Data Length is more than the Buffer size "
                       "so, packet dropped size=%d\r\n", u4IcchDataLen);
            return ICCH_FAILURE;
        }

        /* If pIcchSsnInfo->u2ResBufLen is greater than
         * Header length then some part of Data is already read so,
         * Adjust the Data length */
        if (pIcchSsnInfo->u2ResBufLen > ICCH_HDR_LENGTH)
        {
            u4IcchDataLen -= (pIcchSsnInfo->u2ResBufLen - ICCH_HDR_LENGTH);
            /* pu1Data is already adjusted  before this IF
             * condition */
        }
    }
    *pu2HeaderLen = u2IcchHdrLen;
    *pu4DataLen = u4IcchDataLen;
    return ICCH_SUCCESS;
}

/******************************************************************************
 * Function           : IcchRecvIcchHdrOrPayLoadFromConnFd
 * Input(s)           : pu1Data - Linear buffer
                        pIcchSsnInfo - Session information
                        u2MsgLen - ICCH header length to be read
                        pu4DataLen - Pointer to sync message payload length
                        pbIsPartialDataRcvd - Partial data received indicator
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS / ICCH_FAILURE
 * Action             : Routine receives the ICCH header from reliable channel
 ******************************************************************************/
UINT4
IcchRecvIcchHdrOrPayLoadFromConnFd (UINT1 **ppu1Data, UINT1 *pu1Start,
                                tIcchSsnInfo * pIcchSsnInfo, UINT2 u2MsgLen,
                                BOOL1 * pbIsPartialDataRcvd)
{
    UINT1              *pu1Data = *ppu1Data;
    UINT4               u4RetVal = ICCH_SUCCESS;
    INT4                i4Read = 0;
    UINT2               u2TotalRead = 0;

    *pbIsPartialDataRcvd = ICCH_FALSE;

    u4RetVal = IcchTcpSockRcv (pIcchSsnInfo->i4ConnFd, pu1Data, u2MsgLen, &i4Read);

    if (u4RetVal == ICCH_FAILURE)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC, "IcchRecvIcchHdrOrPayLoadFromConnFd: "
                  "Fatal error occurred in the reliable "
                  "socket communication\r\n");
        u4RetVal = ICCH_FAILURE;
    }
    else if (i4Read < (INT4)u2MsgLen)
    {
        pIcchSsnInfo->u2ResBufLen = 0;
        pu1Data = (i4Read < 0) ? pu1Data : (pu1Data + i4Read);
        u2TotalRead = (UINT2) (pu1Data - pu1Start);
        if (u2TotalRead != 0)
        {
            MEMCPY (pIcchSsnInfo->pResBuf, pu1Start, u2TotalRead);
            pIcchSsnInfo->u2ResBufLen = u2TotalRead;
        }
        if (i4Read > 0)
        {
            ICCH_TRC1 (ICCH_SYNC_MSG_TRC, "IcchRecvIcchHdrOrPayLoadFromConnFd: "
                       "ICCH header received < ICCH header to be received "
                       "and the received size is =%d\r\n", i4Read);
        }
        *pbIsPartialDataRcvd = ICCH_TRUE;
    }
    else
    {
        pu1Data += u2MsgLen;
        *ppu1Data = pu1Data;
        ICCH_TRC (ICCH_SYNC_MSG_TRC, "IcchRecvIcchHdrOrPayLoadFromConnFd: "
                  "The data to be read from the socket is completed\r\n");
    }
    return u4RetVal;
}

/******************************************************************************
 * Function           : IcchHandleCompleteSyncMsg
 * Input(s)           : pu1IcchSyncMsg - Linear sync message
 *                      i4IcchSyncMsgLen - Sync message length
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS / ICCH_FAILURE
 * Action             : Routine to handle the complete synchronization message
 ******************************************************************************/
UINT4
IcchHandleCompleteSyncMsg (UINT1 *pu1IcchSyncMsg, INT4 i4IcchSyncMsgLen)
{
    UINT4               u4SeqNum = 0;
    UINT4               u4SrcEntId = 0;
    UINT4               u4DestEntId = 0;

    PTR_FETCH4 (u4SeqNum, (pu1IcchSyncMsg + ICCH_HDR_OFF_SEQ_NO));
    PTR_FETCH4 (u4SrcEntId, (pu1IcchSyncMsg + ICCH_HDR_OFF_SRC_ENTID));
    PTR_FETCH4 (u4DestEntId, (pu1IcchSyncMsg + ICCH_HDR_OFF_DEST_ENTID));

    /* Check if the pkt is rcvd from a valid Entity (i.e., Application) */
    if ((u4SrcEntId != ICCH_APP_ID) &&
        ((gIcchInfo.apIcchAppInfo[u4SrcEntId] == NULL) ||
         (gIcchInfo.apIcchAppInfo[u4SrcEntId]->u4ValidEntry == FALSE)))
    {
        /* Pkt rcvd from and invalid EntId. Discard this packet */
        ICCH_TRC1 (ICCH_CRITICAL_TRC | ICCH_ALL_FAILURE_TRC,
                   "IcchHandleCompleteSyncMsg: "
                   "Pkt rcvd from invalid EntId %d. Discarding "
                   ".... !!!\r\n", u4SrcEntId);
        return ICCH_SUCCESS;
    }

    if (u4DestEntId >= ICCH_MAX_APPS)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC, "IcchHandleCompleteSyncMsg: "
                  "DestEntId invalid in rcvd pkt\r\n");
        return ICCH_SUCCESS;
    }

    ICCH_TRC3 (ICCH_PKT_DUMP_TRC | ICCH_SYNC_MSG_TRC | ICCH_NOTIF_TRC,
               "IcchHandleCompleteSyncMsg: Pkt received "
               "[SeqNum# %d, AppId=%s]\nLast processed seq# %d.\r\n",
               u4SeqNum, gaIcchAppName[u4DestEntId],
               ICCH_RX_LAST_SEQ_NUM_PROCESSED ());
    /* Update the statistics for all the received sync-up message.
     * 1. sync-up message which will be buffered
     * 2. sync-up message which will not be bufferd and processed
     *    directly */
    if (gIcchInfo.i4StatsEnable == ICCH_STATS_ENABLE)
    {
        ICCH_INCR_RX_STAT_SYNC_MSG_RECVD ();
    }

    /* Dumping the sync messsage before transmission */
    ICCH_PKT_DUMP (ICCH_PKT_DUMP_TRC, pu1IcchSyncMsg, i4IcchSyncMsgLen);

    if (IcchProcessSyncMsg (pu1IcchSyncMsg, i4IcchSyncMsgLen,
                            u4SeqNum) == ICCH_FAILURE)
    {
        return ICCH_FAILURE;
    }

    if (ICCH_RX_BUFF_PKT_COUNT () >= MAX_ICCH_RX_BUF_NODES)
    {
        /* Rx buff node count is reached the max limit,
         * so do not process the packet from socket */
        gIcchInfo.bIsRxBuffFull = RM_TRUE;
        return ICCH_FAILURE;
    }

    return ICCH_SUCCESS;
}

/******************************************************************************
 * Function           : IcchProcessSyncMsg
 * Input(s)           : pu1IcchSyncMsg - Linear sync message
 *                      i4IcchSyncMsgLen - Sync message length
 *                      u4DestEntId - Destination entity id
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS / ICCH_FAILURE
 * Action             : Routine to handle the 
 *                      synchronization message
 ******************************************************************************/
UINT4
IcchProcessSyncMsg (UINT1 *pu1IcchSyncMsg, INT4 i4IcchSyncMsgLen,
                    UINT4 u4SeqNum)
{
    UINT1              *pu1Pkt = NULL;


    /*if incoming packet is old sequence numbered packet, dont post it
     *in the buffer
     */

    if (u4SeqNum < ICCH_RX_GETNEXT_VALID_SEQNUM ())
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC | ICCH_SYNC_MSG_TRC,
                  "IcchProcessSyncMsg: "
                  "Discarding Old sequence numbered packet, silent ignore\r\n");
        return ICCH_SUCCESS;
    }

    /* Allocate memory for linear buffer to add it in the RX buff node.
     * Size of the buffers should be equeal to the received packet len.
     * pu1IcchSyncMsg is reused for recvfrom call so it cannot be
     * stored in the Rx buffer node. */

    ICCH_RXBUF_PKT_MEM_ALLOC (pu1Pkt, i4IcchSyncMsgLen);
    /* pu1Pkt will be freed after processing of RX buffer */

    if (pu1Pkt == NULL)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC | ICCH_CRITICAL_TRC,
                  "IcchProcessSyncMsg: "
                  "Unable to allocate memory for processing ICCH sync message\r\n");
        return ICCH_FAILURE;
    }

    MEMCPY (pu1Pkt, pu1IcchSyncMsg, i4IcchSyncMsgLen);

    if (IcchRxBufAddPkt (pu1Pkt, u4SeqNum, (UINT4) (i4IcchSyncMsgLen)) == ICCH_FAILURE)
    {
        /* clear the pkt memory */
        ICCH_RXBUF_PKT_MEM_FREE (pu1Pkt);
        ICCH_TRC (ICCH_ALL_FAILURE_TRC, "IcchProcessSyncMsg: "
                  "Failed to Add received packet " "to RX Buff.\r\n");
        return ICCH_FAILURE;
    }

    /* Call for RX buffer processing */
    IcchRxProcessBufferedPkt ();

    return ICCH_SUCCESS;
}

#endif /* _ICCHRX_C_ */
