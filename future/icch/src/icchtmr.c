/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: icchtmr.c,v 1.6 2016/03/04 11:10:33 siva Exp $
*
* Description: Timer related functions of ICCH module.
*********************************************************************/
#include "icchincs.h"
#include "icchglob.h"

PRIVATE VOID        IcchTmrInitTmrDesc (VOID);
/******************************************************************************
 * Function           : IcchTmrInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS/ICCH_FAILURE
 * Action             : Routine to create ICCH timer.
 ******************************************************************************/
UINT4
IcchTmrInit (VOID)
{
    UINT4               u4RetVal = ICCH_SUCCESS;

    if (TmrCreateTimerList ((CONST UINT1 *) ICCH_TASK_NAME,
                            ICCH_TMR_EXPIRY_EVENT, NULL,
                            (tTimerListId *) & (gIcchInfo.TmrListId))
        != TMR_SUCCESS)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                  "IcchTmrInit: ICCH core module timer list creation Failure !!!\n");
        u4RetVal = ICCH_FAILURE;
    }
    IcchTmrInitTmrDesc ();
    return (u4RetVal);
}

/******************************************************************************
 * Function           : IcchTmrDeInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS/ICCH_FAILURE
 * Action             : Routine to delete ICCH timer list
 ******************************************************************************/
UINT4
IcchTmrDeInit (VOID)
{
    UINT4               u4RetVal = ICCH_SUCCESS;

    if (gIcchInfo.TmrListId == (UINT4) 0)
    {
        u4RetVal = ICCH_FAILURE;
        ICCH_TRC (ICCH_ALL_FAILURE_TRC, "IcchTmrDeInit: "
                  "Timer is deleted already - EXIT\r\n");
    }
    if ((u4RetVal == ICCH_SUCCESS) &&
        (TmrDeleteTimerList (gIcchInfo.TmrListId) != TMR_SUCCESS))
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                "IcchTmrDeInit: "
                "ICCH core module timer list deletion Failure !!!\n");
        u4RetVal = ICCH_FAILURE;
    }
    gIcchInfo.TmrListId = (UINT4) 0;
    return (u4RetVal);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IcchTmrInitTmrDesc
 *
 *    DESCRIPTION      : This function initializes the timer desc for all
 *                       the timers in ICCH module.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
IcchTmrInitTmrDesc (VOID)
{
    gIcchInfo.aTmrDesc[ICCH_SEQ_RECOV_TIMER].TmrExpFn = 
        IcchHandleSeqRecovTmrExpiry;
    gIcchInfo.aTmrDesc[ICCH_ACK_RECOV_TIMER].TmrExpFn = 
        IcchHandleAckRecovTmrExpiry;
    gIcchInfo.aTmrDesc[ICCH_CONN_RETRY_TIMER].TmrExpFn = 
        IcchHandleConnRetryTmrExpiry;
    gIcchInfo.aTmrDesc[ICCH_BULK_UPDATE_TIMER].TmrExpFn = 
        IcchHandleBulkUpdateTmrExpiry;
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IcchTmrExpHandler
 *
 *    DESCRIPTION      : This function is called whenever a timer expiry
 *                       message is received by ICCH core task. Different timer
 *                       expiry handlers are called based on the timer type.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
VOID
IcchTmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;

    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gIcchInfo.TmrListId)) != NULL)
    {
        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;
        ICCH_TRC1 (ICCH_NOTIF_TRC, "IcchTmrExpHandler: "
                   "Timer Id=%d expired\r\n", u1TimerId);
        /* The timer function does not take any parameter. */
        (*(gIcchInfo.aTmrDesc[u1TimerId].TmrExpFn)) (NULL);
    }
    return;
}

/******************************************************************************
 * Function           : IcchTmrStartTimer
 * Input(s)           : u1TmrId - Timer id
                        u4TmrVal - Time duration of the timer
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS/ICCH_FAILURE
 * Action             : Routine to start the ICCH core module timers
 ******************************************************************************/
UINT4
IcchTmrStartTimer (UINT1 u1TmrId, UINT4 u4TmrVal)
{
    if (TmrStart (gIcchInfo.TmrListId,
                  &(gIcchInfo.IcchTmrBlk[u1TmrId]),
                  u1TmrId, 0, u4TmrVal) != TMR_FAILURE)
    {
        ICCH_TRC1 (ICCH_NOTIF_TRC, "IcchTmrStartTimer: Timer Id=%d "
                   "started successfully - EXIT\r\n", u1TmrId);
        return ICCH_SUCCESS;
    }
    return ICCH_FAILURE;
}

/******************************************************************************
 * Function           : IcchTmrStopTimer
 * Input(s)           : u1TmrId - Timer id
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS/ICCH_FAILURE
 * Action             : Routine to stop the ICCH core module timers
 ******************************************************************************/
UINT4
IcchTmrStopTimer (UINT1 u1TmrId)
{

    if (TmrStop (gIcchInfo.TmrListId,
                 &(gIcchInfo.IcchTmrBlk[u1TmrId])) != TMR_FAILURE)
    {
        ICCH_TRC1 (ICCH_NOTIF_TRC, "IcchTmrStartTimer: Timer Id=%d "
                   "stopped successfully - EXIT\r\n", u1TmrId);
        return ICCH_SUCCESS;
    }
    return ICCH_FAILURE;
}

/******************************************************************************
 * Function           : IcchHandleSeqRecovTmrExpiry
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This routine handles the sequence recovery timer
 *                      expiry.
 ******************************************************************************/
VOID
IcchHandleSeqRecovTmrExpiry (VOID *pArg)
{
    UINT4               u4LastSeqNum = 0;

    UNUSED_PARAM (pArg);

    ICCH_TRC (ICCH_SYNC_EVT_TRC,
              "Sequence Number Recovery Timer Expiry.\n");
    if (gIcchInfo.bIsRxBuffFull == ICCH_TRUE)
    {
        IcchTmrStartTimer (ICCH_SEQ_RECOV_TIMER, ICCH_SEQ_RECOV_TMR_INTERVAL);
        ICCH_TRC1 (ICCH_SYNC_MSG_TRC | ICCH_ALL_FAILURE_TRC, 
                   "IcchHandleSeqRecovTmrExpiry: "
                   "Sequence Number (%d) is expected from Remote\n",
                   ICCH_RX_GETNEXT_VALID_SEQNUM ());
        return;
    }

    /* Proceed with the next sequence number */
    ICCH_RX_INCR_LAST_SEQNUM_PROCESSED ();
    u4LastSeqNum = ICCH_RX_LAST_SEQ_NUM_PROCESSED ();
    ICCH_TRC1 (ICCH_SYNC_MSG_TRC | ICCH_ALL_FAILURE_TRC, 
               "IcchHandleSeqRecovTmrExpiry: "
               "Sequence Number (%d) is ignored\n", u4LastSeqNum);

    /* Update Statistics */
    ICCH_INCR_RX_STAT_SYNC_MSG_MISSED ();

    ICCH_RX_SEQ_MISSMATCH_FLG () = ICCH_FALSE;
    ICCH_IS_RX_BUFF_PROCESSING_ALLOWED () = ICCH_TRUE;
    ICCH_TRC (ICCH_SYNC_EVT_TRC, 
              "IcchHandleSeqRecovTmrExpiry: UNLOCKING Rx Buff\r\n");

    IcchRxProcessBufferedPkt ();
    return;
}

/******************************************************************************
 * Function           : IcchHandleConnRetryTmrExpiry
 * Input(s)           : pArg - VOID type arguments
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This routine handles the connection retry
 *                      timer expiry.
 ******************************************************************************/
VOID
IcchHandleConnRetryTmrExpiry (VOID *pArg)
{
    UNUSED_PARAM (pArg);
    if (ICCH_SEND_EVENT (ICCH_TASK_ID, ICCH_PKT_FROM_APP) != OSIX_SUCCESS)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC, "SendEvent failed in "
                  "IcchHandleConnRetryTmrExpiry\n");
    }
    return;
}

/******************************************************************************
 * Function           : IcchHandleAckRecovTmrExpiry
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This routine handles the acknowledgement recovery
 *                      timer expiry.
 ******************************************************************************/
VOID
IcchHandleAckRecovTmrExpiry (VOID *pArg)
{
    UINT4               u4LastSeqNum = 0;

    UNUSED_PARAM (pArg);

    u4LastSeqNum = ICCH_RX_LAST_SEQ_NUM_PROCESSED ();
    /* Proceed with next RX Buffered message processing */
    ICCH_TRC2 (ICCH_ALL_FAILURE_TRC | ICCH_SYNC_MSG_TRC, 
               "IcchHandleAckRecovTmrExpiry: ACK not received "
               "for Seq# %d, from AppId=%s \r\n",
               u4LastSeqNum, gaIcchAppName[ICCH_RX_LAST_APPID_PROCESSED ()]);

    /* Release Rx buff lock */
    ICCH_RX_LAST_ACK_RCVD_SEQ_NUM () = u4LastSeqNum;
    ICCH_IS_RX_BUFF_PROCESSING_ALLOWED () = ICCH_TRUE;
    ICCH_TRC (ICCH_SYNC_EVT_TRC,
             "IcchHandleAckRecovTmrExpiry: UNLOCKING Rx Buff\r\n");
    /* Process next message from Rx buffer */
    IcchRxProcessBufferedPkt ();
    return;
}

/******************************************************************************
 * Function           : IcchHandleBulkUpdateTmrExpiry
 * Input(s)           : pArg - VOID type arguments
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This routine handles the bulk update
 *                      timer expiry.
 ******************************************************************************/
VOID
IcchHandleBulkUpdateTmrExpiry (VOID *pArg)
{
    UNUSED_PARAM (pArg);
    gu4IcchBulkUpdtInProgress = ICCH_FALSE;
    return;
}

