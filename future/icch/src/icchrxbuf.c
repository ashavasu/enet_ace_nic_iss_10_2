/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: icchrxbuf.c,v 1.3 2015/02/24 11:49:44 siva Exp $
*
* Description: ICCH utility functions for the RX buffer handling.
***********************************************************************/
#ifndef _ICCH_RXBUF_C_
#define _ICCH_RXBUF_C_

#include "icchincs.h"

/******************************************************************************
 * Function           : IcchRxBufAddPkt
 * Input(s)           : pu1Pkt - packet pointer that needs to be added in the 
 *                               RX Buffer. packet should contatin the ICCH 
 *                               Header. it should be a liner buffer pointer.
 *                      u4SeqNum - Sequence number of the received packet
 *                      u4PktLen - length of the entire packet.
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS / ICCH_FAILURE
 * Action             : This function add the received sync-up data packet
 *                      to the RX Buffer (Hash Table).
 ******************************************************************************/
INT4
IcchRxBufAddPkt (UINT1 *pu1Pkt, UINT4 u4SeqNum, UINT4 u4PktLen)
{
    tIcchRxBufHashEntry  *pRxBufNode = NULL;

    /* Allocate memory for the hash node : tIcchRxBufHashEntry */
    if ((pRxBufNode =
         (tIcchRxBufHashEntry *) MemAllocMemBlk (gIcchInfo.IcchRxBufPoolId)) == NULL)
    {
        ICCH_TRC (ICCH_CRITICAL_TRC | ICCH_ALL_FAILURE_TRC | ICCH_BUFF_TRC,
                  "Memory allocation failed for the RX Buff node.\n");
    /*    IcchHandleSystemRecovery (ICCH_RX_BUFF_NODE_ALLOC_FAILED);*/
        /* pu1Pkt memory should be cleared by the caller function */
        return ICCH_FAILURE;
    }

    /* Update the node information */
    TMO_HASH_Init_Node (&pRxBufNode->RxBufHashNode);
    pRxBufNode->u4SeqNum = u4SeqNum;
    pRxBufNode->pu1Pkt = pu1Pkt;
    pRxBufNode->u4PktLen = u4PktLen;

    /* Add the node to the Hash Table */
    TMO_HASH_Add_Node (gIcchInfo.pRxBufHashTable, &pRxBufNode->RxBufHashNode,
                       ICCH_RX_GET_HASH_INDEX (pRxBufNode->u4SeqNum), NULL);

    ICCH_RX_BUFF_NODE_COUNT ()++;

    ICCH_TRC2 (ICCH_BUFF_TRC | ICCH_SYNC_MSG_TRC | ICCH_NOTIF_TRC, 
               "IcchRxBufAddPkt: HASH ADD[seq# %d], "
               "(hash count = %d)\r\n", u4SeqNum, ICCH_RX_BUFF_NODE_COUNT ());
    return ICCH_SUCCESS;

}

/******************************************************************************
 * Function           : IcchRxBufSearchPkt
 * Input(s)           : u4SeqNum - Sequenece number of the packet that needs
 *                                 to be searched in the RX Buffer.
 * Output(s)          : None.
 * Returns            : pointer to tIcchRxBufHashEntry node if entry found
 *                      else return NULL
 * Action             : This function searchs a desired sequence numbered
 *                      packet in the RX Buffer. If entry found then returns 
 *                      the Hash Node pointer to the caller else return NULL.
 ******************************************************************************/
tIcchRxBufHashEntry  *
IcchRxBufSearchPkt (UINT4 u4SeqNum)
{
    tIcchRxBufHashEntry  *pRxBufNode = NULL;
    tTMO_HASH_NODE     *pNode = NULL;
    UINT4               u4HashIndex = ICCH_RX_GET_HASH_INDEX (u4SeqNum);

    /* Get the hash index using the u4SeqNum */
    /* Scan only the required bucket. No need to scan the entire table */
    TMO_HASH_Scan_Bucket (gIcchInfo.pRxBufHashTable,
                          u4HashIndex, pNode, tTMO_HASH_NODE *)
    {
        pRxBufNode = (tIcchRxBufHashEntry *)
            (pNode - ICCH_RX_BUFF_GET_HASHNODE_OFFSET ());
        if (pRxBufNode->u4SeqNum == u4SeqNum)
        {
            /* Match found */
            return pRxBufNode;
        }
    }
    return NULL;
}

/******************************************************************************
 * Function           : IcchRxBufDeleteNode
 * Input(s)           : pRxBufNode - Pointer to the HashNode that needs to be
 *                                   deleted form the Hash Table
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS / ICCH_FAILURE
 * Action             : This function deletes the specified hash node from the
 *                      hash table (RX Buffer). It also takes care of 
 *                      releasing the memory used by the node.
 ******************************************************************************/
INT4
IcchRxBufDeleteNode (tIcchRxBufHashEntry * pRxBufNode)
{
    UINT1               u1SsnIndex = 0;

    TMO_HASH_Delete_Node (gIcchInfo.pRxBufHashTable, &pRxBufNode->RxBufHashNode,
                          ICCH_RX_GET_HASH_INDEX (pRxBufNode->u4SeqNum));

    /* Deleted from the Hash table. Now clean the memory for this node */
    IcchRxBufFreeHashNode (&pRxBufNode->RxBufHashNode);

    ICCH_RX_BUFF_NODE_COUNT ()--;

    if (gIcchInfo.bIsRxBuffFull == ICCH_TRUE)
    {
        gIcchInfo.bIsRxBuffFull = ICCH_FALSE;
        ICCH_TRC (ICCH_BUFF_TRC | ICCH_CRITICAL_TRC,
                  "!!! Sync message reception is resumed "
                  "because Rx buffer is available !!!\n");
        /* As we support 1:1 standby, 
         * there is only one session alive */
        IcchPktRcvd (ICCH_GET_SSN_INFO (u1SsnIndex).i4ConnFd);
    }
    return ICCH_SUCCESS;
}

/******************************************************************************
 * Function           : IcchRxBufCreate
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS / ICCH_FAILURE
 * Action             : This function creates the RX Buffer(i.e. Hash Table)
 *                     
 ******************************************************************************/
INT4
IcchRxBufCreate (VOID)
{
    /* Create the Hash Table to buffer the received sync-up messages in 
     * sequentional order */
    gIcchInfo.pRxBufHashTable =
        TMO_HASH_Create_Table (ICCH_RXBUF_MAX_HASH_BUCKET_COUNT, NULL, FALSE);

    if (gIcchInfo.pRxBufHashTable == NULL)
    {
        /* Hash table creation failed */
        ICCH_TRC (ICCH_BUFF_TRC | ICCH_CRITICAL_TRC, "RX Buff creation failed\n");
        return ICCH_FAILURE;
    }

    return ICCH_SUCCESS;
}

/******************************************************************************
 * Function           : IcchRxBufDestroy
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function deletes the entire RX buffer (hash table)
 *                      including all the nodes. It takes care of deleting the
 *                      memory used by the hash nodes.
 ******************************************************************************/
VOID
IcchRxBufDestroy (VOID)
{
    if (gIcchInfo.pRxBufHashTable != NULL)
    {
        TMO_HASH_Delete_Table (gIcchInfo.pRxBufHashTable, IcchRxBufFreeHashNode);
        gIcchInfo.pRxBufHashTable = NULL;
    }
}

/******************************************************************************
 * Function           : IcchRxBufFreeHashNode
 * Input(s)           : pNode - Hash node pointer
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function releases the memory used by a hash node.
 *                     
 ******************************************************************************/
VOID
IcchRxBufFreeHashNode (tTMO_HASH_NODE * pNode)
{
    tIcchRxBufHashEntry  *pRxBufNode = NULL;
    pRxBufNode = (tIcchRxBufHashEntry *)
        (pNode - ICCH_RX_BUFF_GET_HASHNODE_OFFSET ());

    /* Free the packet memory liner buffer pointer */
    ICCH_RXBUF_PKT_MEM_FREE (pRxBufNode->pu1Pkt);

    /* Release the memblock for the hash node */
    MemReleaseMemBlock (gIcchInfo.IcchRxBufPoolId, (UINT1 *) pRxBufNode);
}

/******************************************************************************
 * Function           : IcchRxBufClearRxBuffer
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function releases all the nodes added
 *                       in the RxBuffer
 ******************************************************************************/
VOID
IcchRxBufClearRxBuffer (VOID)
{
    tTMO_HASH_NODE     *pNode = NULL, *pTmpNode = NULL;
    UINT4               u4HashIndex = 0;

    TMO_HASH_Scan_Table (gIcchInfo.pRxBufHashTable, u4HashIndex)
    {
        TMO_HASH_DYN_Scan_Bucket (gIcchInfo.pRxBufHashTable, u4HashIndex,
                                  pNode, pTmpNode, tTMO_HASH_NODE *)
        {
            TMO_HASH_Delete_Node (gIcchInfo.pRxBufHashTable, pNode, u4HashIndex);
            IcchRxBufFreeHashNode (pNode);
            ICCH_RX_BUFF_NODE_COUNT ()--;
        }
    }
    return;
}

/******************************************************************************
 * Function           : IcchPrcsPendingRxBufMsgs
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : Process the pending RX buffer messages, in case of 
 *                      failure
 ******************************************************************************/
VOID
IcchPrcsPendingRxBufMsgs (VOID)
{
    ICCH_TRC (ICCH_CTRL_PATH_TRC, "IcchPrcsPendingRxBufMsgs: ENTRY \r\n");

    if ((ICCH_RX_BUFF_NODE_COUNT () == 0) &&
        (ICCH_RX_LAST_SEQ_NUM_PROCESSED () ==
         ICCH_RX_LAST_ACK_RCVD_SEQ_NUM ()))
    {
        ICCH_TRC (ICCH_CTRL_PATH_TRC, "IcchPrcsPendingRxBufMsgs: "
                "No Pending RX buffer\r\n");

        HbApiSendIcchEventToHb (HB_ICCH_SYNC_MSG_PROCESSED);
    /*    ICCH_SET_NODE_TRANSITION_IN_PROGS_STATE (ICCH_FALSE);*/
    }
    else
    {
        ICCH_TRC (ICCH_CTRL_PATH_TRC, "IcchPrcsPendingRxBufMsgs: "
                "RX buffer is pending\r\n");

        if (ICCH_RX_SEQ_MISSMATCH_FLG () == ICCH_TRUE)
        {
            ICCH_TRC (ICCH_SYNC_MSG_TRC,
                    "IcchPrcsPendingRxBufMsgs: Seq Recov timer is already running due to sequence no. mismatch\r\n");
            IcchTmrStopTimer (ICCH_SEQ_RECOV_TIMER);
            ICCH_RX_SEQ_MISSMATCH_FLG () = ICCH_FALSE;

            /* Update the last processed sequence number */
            ICCH_RX_INCR_LAST_SEQNUM_PROCESSED ();

            /* Update statistics - for the buffered messages that is processed */
            ICCH_RX_STAT_SYNC_MSG_PROCESSED ()++;

            ICCH_IS_RX_BUFF_PROCESSING_ALLOWED () = ICCH_TRUE;
            /*Now process the Rx Buffer */
            IcchRxProcessBufferedPkt ();
        }
        else
        {
            ICCH_TRC (ICCH_SYNC_MSG_TRC,
                    "IcchPrcsPendingRxBufMsgs: Buffer processing is going on for pending Rx packets \r\n");
        }
    }

    ICCH_TRC (ICCH_CTRL_PATH_TRC, "IcchPrcsPendingRxBufMsgs: EXIT \r\n");
}

#endif /* _ICCH_RXBUF_C_ */
