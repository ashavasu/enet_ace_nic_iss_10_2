/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: icchtrc.c,v 1.3 2016/01/06 11:02:33 siva Exp $
*
* Description: ICCH utility functions for trace message modifier.
***********************************************************************/
#ifndef _ICCH_TRC_C_
#define _ICCH_TRC_C_
#include "icchincs.h"

/******************************************************************************
 * Function           : IcchTrcHandle
 * Input(s)           : u4FlagVal - Debug messages flag value
 *                      pc1Fmt    - Format of the trace message
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function is used to print the trace message.
 ******************************************************************************/
VOID
IcchTrcHandle (UINT4 u4FlagVal, CONST CHR1 * pc1Fmt, ...)
{
    static CHR1         ac1Buf[ICCH_MAX_TRC_NAME_LEN];
    va_list             ap;
    CHR1               *pTemp = NULL;

    /* Verify the flag value with the global trace value */
    if (!(ICCH_TRC_FLAG & u4FlagVal))
    {
        /* This trace flag is not set */
        return;
    }

    MEMSET (ac1Buf, 0, sizeof (ac1Buf));
    pTemp = ac1Buf;
    /* Copy the trace time string */
    UtlGetTimeStr (pTemp);
    pTemp = pTemp + STRLEN (ac1Buf);
    switch (ICCH_GET_NODE_STATE ())
    {
        case ICCH_MASTER:
            SPRINTF (pTemp, ": ICCH[MASTER]: ");
            break;
        case ICCH_SLAVE:
            SPRINTF (pTemp, ": ICCH[SLAVE]: ");
            break;
        case ICCH_INIT:
            SPRINTF (pTemp, ": ICCH[INIT]: ");
            break;
        default:
            /* Invalid state for trace */
            return;
    }
    pTemp = pTemp + STRLEN (ac1Buf);
    va_start (ap, pc1Fmt);
    vsprintf (&ac1Buf[STRLEN (ac1Buf)], pc1Fmt, ap);
    if (u4FlagVal & ICCH_NOTIF_TRC)
    {
        /* Send the notification messages to SysLog module */
        SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gIcchInfo.u4SysLogId, " %s ", ac1Buf));
    }
    else
    {
        /* Send the messages to SysLog module */
        SYS_LOG_TRC (ICCH_MOD_NAME, ac1Buf);
        /* Other traces are printed in file or console using the
         * below utility function */
        UtlTrcPrint (ac1Buf);
    }
    va_end (ap);
    return;
}

/******************************************************************************
 * Function           : IcchPktDumpTrc
 * Input(s)           : pBuf      - Liner buffer pointer for the packet to be
 *                                  dumped
 *                      Length    - Length of the packet to be dumped
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function dumped the packet and also print the
 *                      details of the ICCH header. Payload is shown with Hex
 *                      values.
 ******************************************************************************/
VOID
IcchPktDumpTrc (UINT1 *pu1Buf, UINT4 u4Length)
{
    tIcchHdr            IcchHdr;
    UINT4               u4Count = 0;
    UINT4               u4PrintCount = 0;
    CHR1                au1OutBuf[ICCH_PRINT_BUF_LEN];    /* Print Buf */

    if (u4Length > ICCH_MAX_DUMP_LEN)
    {
        u4Length = ICCH_MAX_DUMP_LEN;
    }


    /* Read ICCH Header */
    MEMSET (&IcchHdr, 0, sizeof (tIcchHdr));
    PTR_FETCH2 (IcchHdr.u2Version, (pu1Buf + ICCH_HDR_OFF_VERSION));
    PTR_FETCH2 (IcchHdr.u2Chksum, (pu1Buf + ICCH_HDR_OFF_CKSUM));
    PTR_FETCH4 (IcchHdr.u4TotLen, (pu1Buf + ICCH_HDR_OFF_TOT_LENGTH));
    PTR_FETCH4 (IcchHdr.u4MsgType, (pu1Buf + ICCH_HDR_OFF_MSG_TYPE));
    PTR_FETCH4 (IcchHdr.u4SrcEntId, (pu1Buf + ICCH_HDR_OFF_SRC_ENTID));
    PTR_FETCH4 (IcchHdr.u4DestEntId, (pu1Buf + ICCH_HDR_OFF_DEST_ENTID));
    PTR_FETCH4 (IcchHdr.u4SeqNo, (pu1Buf + ICCH_HDR_OFF_SEQ_NO));

    /* Print the ICCH Header */
    UtlTrcPrint ("================== ICCH HEADER ==========================\r\n");
    MEMSET (&au1OutBuf[0], 0, ICCH_PRINT_BUF_LEN);
    SPRINTF ((CHR1 *) au1OutBuf, "%-22s: %d\r\n",
             "ICCH Version", IcchHdr.u2Version);
    UtlTrcPrint (au1OutBuf);

    MEMSET (&au1OutBuf[0], 0, ICCH_PRINT_BUF_LEN);
    SPRINTF ((CHR1 *) au1OutBuf, "%-22s: 0x%02x\r\n",
             "Checksum", IcchHdr.u2Chksum);
    UtlTrcPrint (au1OutBuf);

    MEMSET (&au1OutBuf[0], 0, ICCH_PRINT_BUF_LEN);
    SPRINTF ((CHR1 *) au1OutBuf, "%-22s: %u [Hdr(%d)+Payload(%u)]\r\n",
             "Total Length", IcchHdr.u4TotLen, (INT4) ICCH_HDR_LENGTH,
             (IcchHdr.u4TotLen - (INT4) ICCH_HDR_LENGTH));
    UtlTrcPrint (au1OutBuf);

    MEMSET (&au1OutBuf[0], 0, ICCH_PRINT_BUF_LEN);
    SPRINTF ((CHR1 *) au1OutBuf, "%-22s: %u\r\n",
             "Message Type", IcchHdr.u4MsgType);
    UtlTrcPrint (au1OutBuf);

    MEMSET (&au1OutBuf[0], 0, ICCH_PRINT_BUF_LEN);
    SPRINTF ((CHR1 *) au1OutBuf, "%-22s: %s\r\n",
             "Source Entity Id", gapc1IcchAppName[IcchHdr.u4SrcEntId]);
    UtlTrcPrint (au1OutBuf);

    MEMSET (&au1OutBuf[0], 0, ICCH_PRINT_BUF_LEN);
    SPRINTF ((CHR1 *) au1OutBuf, "%-22s: %s\r\n",
             "Destination Entity Id", gapc1IcchAppName[IcchHdr.u4DestEntId]);
    UtlTrcPrint (au1OutBuf);

    MEMSET (&au1OutBuf[0], 0, ICCH_PRINT_BUF_LEN);
    SPRINTF ((CHR1 *) au1OutBuf, "%-22s: %u\r\n",
             "Sequence Number", IcchHdr.u4SeqNo);
    UtlTrcPrint (au1OutBuf);

    /* Read and Print the Payload */
    UtlTrcPrint ("----- PAYLOAD: -------\r\n");
    MEMSET (&au1OutBuf[0], 0, ICCH_PRINT_BUF_LEN);

    u4PrintCount = 0;
    while (u4Count < u4Length)
    {
        SPRINTF ((CHR1 *) & au1OutBuf[u4PrintCount], "%02x ",
                 *(pu1Buf + u4Count));

        u4Count++;
        u4PrintCount = u4PrintCount + 3;    /* need 3 bytes for "%02x " */

        if (((u4Count % 16) == 0) ||    /* Go to next line */
            (u4Count == u4Length))    /* Payload may be less than 16 */
        {
            SPRINTF ((CHR1 *) & au1OutBuf[u4PrintCount], "\n");
            /* Tailing '\0' char will be added along with the above
             * statement */
            UtlTrcPrint (au1OutBuf);
            MEMSET (&au1OutBuf[0], 0, ICCH_PRINT_BUF_LEN);
            u4PrintCount = 0;
        }
    }
    UtlTrcPrint
        ("====================== END ============================\r\n\n");
}

#endif /* _ICCH_TRC_C_ */

