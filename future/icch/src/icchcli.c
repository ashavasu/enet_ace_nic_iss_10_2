/********************************************************************
 * Copyright (C) 2014 Aricent Inc . All Rights Reserved
 *
 * $Id: icchcli.c,v 1.33.2.1 2018/03/16 13:43:46 siva Exp $
 *
 * Description: Action routines of ICCH module specific CLI commands
 *********************************************************************/
#ifndef __ICCHCLI_C__
#define __ICCHCLI_C__

#include "icchincs.h"
#include "icchcli.h"
#include "fsicchcli.h"
/******************************************************************************
 * Function           : CliProcessIcchCmd
 * Input(s)           : CliHandle - CLI handler
 *                      u4Command - Command identifier
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function takes variable no. of arguments and
 *                      process the cli commands for ICCH module.
 ******************************************************************************/
INT4
CliProcessIcchCmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *au4Args[ICCH_CLI_MAX_ARGS];
    INT4                i4Argno = 0;
    UINT4               u4RetVal = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4SessionId = 0;

    va_start (ap, u4Command);

    while (1)
    {
        au4Args[i4Argno++] = va_arg (ap, UINT4 *);
        if (i4Argno == ICCH_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);
    switch (u4Command)
    {
        case ICCH_CLI_SET_ICCH_IP_MASK:
            u4RetVal = IcchCliSetIccIpMask (CliHandle,
                                            (INT4 *) au4Args[0],
                                            (INT4 *) au4Args[1],
                                            (INT4) CLI_PTR_TO_U4 (au4Args[2]),
                                            CLI_PTR_TO_U4 (au4Args[3]),
                                            *au4Args[4], *au4Args[5]);
            break;
        case ICCH_CLI_SET_STATS:
            u4RetVal = IcchCliSetIcchStatistics (CliHandle,
                                                 (INT4)
                                                 CLI_PTR_TO_U4 (au4Args[0]));
            break;
        case ICCH_CLI_SHOW_DETAIL:
            if (au4Args[1] == NULL)
            {
                i4SessionId = ICCH_NO_SESSION_ID;
            }
            else
            {
                i4SessionId = CLI_PTR_TO_I4 (*au4Args[1]);
            }
            u4RetVal =
                IcchCliShowIcchDetail (CliHandle,
                                       (INT1) CLI_PTR_TO_U4 (au4Args[0]),
                                       i4SessionId);
            break;
        case ICCH_CLI_TRC_DBG:
            u4RetVal = IcchCliDebugEnableDisable (CliHandle,
                                                  (UINT4)
                                                  CLI_PTR_TO_U4 (au4Args[0]),
                                                  (UINT1) u4Command);
            break;
        case ICCH_CLI_TRC_NO_DBG:
            u4RetVal = IcchCliDebugEnableDisable (CliHandle,
                                                  (UINT4)
                                                  CLI_PTR_TO_U4 (au4Args[0]),
                                                  (UINT1) u4Command);
            break;
        case ICCH_CLI_CLEAR_STATS:
            u4RetVal = IcchCliClearStatistics (CliHandle);
            break;
        case ICCH_INST_ID:
            u4RetVal =
                IcchCliInstanceMode (CliHandle, CLI_PTR_TO_U4 (au4Args[0]),
                                     ICCH_INST_ID);
            break;

        case ICCH_NO_INST_ID:
            u4RetVal =
                IcchCliInstanceMode (CliHandle, CLI_PTR_TO_U4 (au4Args[0]),
                                     ICCH_NO_INST_ID);
            break;

        case ICCH_CLI_SET_PROTOCOL_SYNC:
            u4RetVal =
                IcchCliSetProtocolSync (CliHandle,
                                        (UINT4) CLI_PTR_TO_U4 (au4Args[0]),
                                        (UINT4) CLI_PTR_TO_U4 (au4Args[1]));
            break;

        case ICCH_CLI_FETCH_REMOTE_FDB:
            u4RetVal = IcchCliFetchRemoteFdb (CliHandle);
            break;

        default:
            break;

    }
    if ((u4RetVal == CLI_FAILURE) && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_ICCH_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", IcchCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (u4RetVal);

    return ((INT4) u4RetVal);
}

/******************************************************************************
 * Function           : IcchCliSetIccIpMask
 * Input(s)           : CliHandle - CLI handler
 *                      u4IpAddress - Ip address of the node 
 *                      u4IpMask - Subnet mask of the node 
 *                      u4IfIndex - Interface index of the node 
 *                      u4IfType - Type of interface
 *                      u4IfId   - inteface ID
 *                      u4VlanId - Vlan ID
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE
 * Action             : This function configures the ip address/mask/interface 
 *                      for the ICCH node.
 ******************************************************************************/
UINT4
IcchCliSetIccIpMask (tCliHandle CliHandle, INT4 *pi4IpAddress, INT4 *pi4IpMask,
                     INT4 i4IfIndex, UINT4 u4IfType, UINT4 u4IfId,
                     UINT4 u4VlanId)
{
    tSNMP_OCTET_STRING_TYPE IfName;
    UINT4               u4ErrorCode = 0;
    UINT4               u4InstId = 0;
    UINT1              *pu1IfaceInfo = NULL;
    UINT1               au1PoStr[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (&IfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1PoStr, 0, CFA_MAX_PORT_NAME_LENGTH);

    /*Check whether iccl.conf file is present.If it is deleted at run time using "erase iccl"
       command we cannot access the file.It will be created only in next reboot */

    if (FlashFileExists (ICCL_CONF_FILE) == ISS_FAILURE)
    {
        CLI_SET_ERR (CLI_ICCL_CONF_FILE_NOT_EXISTS);
        return CLI_FAILURE;
    }

    if (u4IfType == CFA_LAGG)
    {
        pu1IfaceInfo = (UINT1 *) cli_get_iface_type_name (u4IfType);
        SPRINTF ((CHR1 *) au1PoStr, "%s%u", pu1IfaceInfo, u4IfId);
        IfName.pu1_OctetList = au1PoStr;
        IfName.i4_Length = (INT4) STRLEN (IfName.pu1_OctetList);
    }

    u4InstId = (UINT4) (ICCH_CLI_GET_INST_ID ());

    if (SNMP_SUCCESS != nmhTestv2FsIcclSessionInterface (&u4ErrorCode,
                                                         u4InstId, &IfName))
    {
        return CLI_FAILURE;
    }

    if (SNMP_SUCCESS != nmhTestv2FsIcclSessionIpAddress (&u4ErrorCode,
                                                         u4InstId,
                                                         (UINT4)
                                                         (*pi4IpAddress)))
    {
        return CLI_FAILURE;
    }
    if (SNMP_SUCCESS != nmhTestv2FsIcclSessionSubnetMask (&u4ErrorCode,
                                                          u4InstId,
                                                          (UINT4) (*pi4IpMask)))
    {
        return CLI_FAILURE;
    }
    if (SNMP_SUCCESS != nmhTestv2FsIcclSessionVlan (&u4ErrorCode,
                                                    u4InstId,
                                                    (INT4) (u4VlanId)))
    {
        return CLI_FAILURE;
    }
    if (CFA_SUCCESS == CfaIcclIpIfIsLocalNet ((UINT4) (*pi4IpAddress),
                                              (UINT4) (*pi4IpMask)))
    {
        CLI_SET_ERR (CLI_ICCH_IP_OVERLAP);
        return CLI_FAILURE;
    }
    if (SNMP_SUCCESS != nmhSetFsIcclSessionInterface (u4InstId, &IfName))

    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (SNMP_SUCCESS != nmhSetFsIcclSessionIpAddress (u4InstId, *pi4IpAddress))
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (SNMP_SUCCESS != nmhSetFsIcclSessionSubnetMask (u4InstId, *pi4IpMask))
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (SNMP_SUCCESS != nmhSetFsIcclSessionVlan (u4InstId, u4VlanId))
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle,
               "\r\n <Information> ICCL configuration successful. Requires system reboot\r\n");

    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4IfIndex);
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : IcchCliSetIcchStatistics
 * Input(s)           : CliHandle - CLI handler
 *                      i4SetStats - statistics value of the node
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE.
 * Action             : This function enables/disables the statistics
 *                      for the ICCH node.
 ******************************************************************************/
UINT4
IcchCliSetIcchStatistics (tCliHandle CliHandle, INT4 i4SetStats)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsIcchStatsEnable (&u4ErrCode, i4SetStats) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsIcchStatsEnable (i4SetStats) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : IcchCliSetProtocolSync
 * Input(s)           : CliHandle - CLI handler
 *                      u4SyncStatus - Protocol sync to be enabled or disabled
 *                      u4EnableStatus - Enable or disable a protocol sync
 * Output(s)          : None.
 * Returns            : CLI_FAILURE/CLI_SUCCESS
 * Action             : This function enables/disables the protocol sync
 *                      for the ICCH node.
 ******************************************************************************/
UINT4
IcchCliSetProtocolSync (tCliHandle CliHandle, UINT4 u4SyncStatus,
                        UINT4 u4EnableStatus)
{
    UINT4               u4ErrCode = 0;
    UNUSED_PARAM (CliHandle);

    /* If sync-up is enabled on ICCL, sync-up is enabled for all protocols which
     * supports dynamci sync-up
     */
    if (u4EnableStatus == OSIX_FALSE)
    {
        u4SyncStatus = ICCH_BLOCK_ALL_SYNC;
    }
    else
    {
        u4SyncStatus = ICCH_ENABLE_ALL_SYNC;
    }

    if (SNMP_SUCCESS !=
        nmhTestv2FsIcchEnableProtoSync (&u4ErrCode, u4SyncStatus))
    {
        return CLI_FAILURE;
    }

    if (SNMP_SUCCESS != nmhSetFsIcchEnableProtoSync (u4SyncStatus))
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (u4SyncStatus == ICCH_ENABLE_ALL_SYNC)
    {
        if ((gIcchInfo.u4IcchTrc & ICCH_CRITICAL_TRC) == 0)
        {
            CliPrintf (CliHandle, "Dynamic sync-up is enabled on ICCH\r\n");
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : IcchCliShowIcchDetail
 * Input(s)           : CliHandle - CLI handler
 *                      i1Value - value of dispaly detail
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE
 * Action             : This function displays the ICCH node info/statistics
 ******************************************************************************/
UINT4
IcchCliShowIcchDetail (tCliHandle CliHandle, INT1 i1Value, INT4 i4SessionId)
{
    tSNMP_OCTET_STRING_TYPE IfName;
    tSNMP_OCTET_STRING_TYPE IfNextName;
    tUtlInAddr          IcchIpAddr;
    tUtlInAddr          IcchIpMask;
    UINT4               u4IpAddress = 0;
    UINT4               u4NextIpAddress = 0;
    UINT4               u4InstanceId = 0;
    UINT4               u4NextInstanceId = 0;
    UINT4               u4IpMask = 0;
    UINT4               u4NextIpMask = 0;
    tVlanId             VlanId = 0;
    UINT4               u4SyncMsgTxCount = 0;
    UINT4               u4SyncMsgTxFailedCount = 0;
    UINT4               u4SyncMsgRxCount = 0;
    UINT4               u4SyncMsgProcCount = 0;
    UINT4               u4SyncMsgMissedCount = 0;
    UINT4               u4IcchSyncState = 0;
    INT4                i4VlanId = 0;
    INT4                i4StatsEnable = ICCH_STATS_DISABLE;
    INT4                i4NodeState = 0;
    UINT2               u2Index = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1Temp[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1Temp, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&IfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&IfNextName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (i1Value == ICCH_CLI_ICCH_INFO)
    {
        if (i4SessionId == ICCH_NO_SESSION_ID)
        {
            if (nmhGetFirstIndexFsIcclSessionTable (&u4InstanceId) ==
                SNMP_SUCCESS)
            {
                do
                {
                    CliPrintf (CliHandle,
                               "\r\n ICCL Instance %d\r\n", u4InstanceId);

                    CliPrintf (CliHandle,
                               "\r\n ----------------\r\n", u4InstanceId);

                    nmhGetFsIcclSessionIpAddress (u4InstanceId, &u4IpAddress);

                    IcchIpAddr.u4Addr = OSIX_NTOHL (u4IpAddress);
                    CliPrintf (CliHandle,
                               "\r\n Node IP Address        : %s\r\n",
                               INET_NTOA (IcchIpAddr));

                    nmhGetFsIcclSessionSubnetMask (u4InstanceId, &u4IpMask);

                    IcchIpMask.u4Addr = OSIX_NTOHL (u4IpMask);
                    CliPrintf (CliHandle,
                               "\r\n Node Subnet mask       : %s\r\n",
                               INET_NTOA (IcchIpMask));

                    nmhGetFsIcchPeerNodeIpAddress (&u4IpAddress);
                    IcchIpAddr.u4Addr = OSIX_NTOHL (u4IpAddress);
                    CliPrintf (CliHandle,
                               "\r\n Peer Node IP Address   : %s\r\n",
                               INET_NTOA (IcchIpAddr));

                    nmhGetFsIcclSessionInterface (u4InstanceId, &IfName);

                    CliPrintf (CliHandle,
                               "\r\n ICCL Interface         : %s\r\n",
                               IfName.pu1_OctetList);

                    nmhGetFsIcclSessionVlan (u4InstanceId, &i4VlanId);

                    CliPrintf (CliHandle,
                               "\r\n ICCL VLAN              : %d\r\n",
                               i4VlanId);

                    if (nmhGetFsIcclSessionNodeState
                        (u4InstanceId, &i4NodeState) != SNMP_FAILURE)
                    {
                        if (i4NodeState == ICCH_INIT)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Node State             : INIT \r\n");
                        }
                        else if (i4NodeState == ICCH_MASTER)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Node State             : MASTER \r\n");
                        }
                        else if (i4NodeState == ICCH_SLAVE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Node State             : SLAVE \r\n");
                        }
                    }

                    if (nmhGetNextIndexFsIcclSessionTable
                        (u4InstanceId, &u4NextInstanceId) != SNMP_SUCCESS)
                    {
                        break;
                    }
                    u4InstanceId = u4NextInstanceId;
                    IfName = IfNextName;
                    u4IpAddress = u4NextIpAddress;
                    u4IpMask = u4NextIpMask;
                }
                while (1);
            }
        }
        else if ((i4SessionId >= ICCH_MIN_INST_ID)
                 && (i4SessionId <= ICCH_MAX_INST_ID))
        {
            for (u2Index = 0; u2Index < ICCH_TOTAL_NUM_OF_ACTV_INST; u2Index++)
            {
                u4InstanceId = gIcchSessionInfo[u2Index].u4InstanceId;
                VlanId = gIcchSessionInfo[u2Index].VlanId;

                if ((u4InstanceId == (UINT4) i4SessionId) && (VlanId != 0))
                {
                    CliPrintf (CliHandle,
                               "\r\n ICCL Instance %d\r\n", u4InstanceId);

                    CliPrintf (CliHandle,
                               "\r\n ----------------\r\n", u4InstanceId);

                    nmhGetFsIcclSessionIpAddress (u4InstanceId, &u4IpAddress);

                    IcchIpAddr.u4Addr = OSIX_NTOHL (u4IpAddress);
                    CliPrintf (CliHandle,
                               "\r\n Node IP Address        : %s\r\n",
                               INET_NTOA (IcchIpAddr));

                    nmhGetFsIcclSessionSubnetMask (u4InstanceId, &u4IpMask);

                    IcchIpMask.u4Addr = OSIX_NTOHL (u4IpMask);
                    CliPrintf (CliHandle,
                               "\r\n Node Subnet mask       : %s\r\n",
                               INET_NTOA (IcchIpMask));

                    IcchIpAddr.u4Addr = OSIX_NTOHL (gIcchInfo.u4PeerNodeId);
                    CliPrintf (CliHandle,
                               "\r\n Peer Node IP Address   : %s\r\n",
                               INET_NTOA (IcchIpAddr));

                    nmhGetFsIcclSessionInterface (u4InstanceId, &IfName);

                    CliPrintf (CliHandle,
                               "\r\n ICCL Interface         : %s\r\n",
                               IfName.pu1_OctetList);

                    nmhGetFsIcclSessionVlan (u4InstanceId, &i4VlanId);

                    CliPrintf (CliHandle,
                               "\r\n ICCL VLAN              : %d\r\n",
                               i4VlanId);

                    if (nmhGetFsIcclSessionNodeState
                        (u4InstanceId, &i4NodeState) != SNMP_FAILURE)
                    {
                        if (i4NodeState == ICCH_INIT)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Node State             : INIT \r\n");
                        }
                        else if (i4NodeState == ICCH_MASTER)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Node State             : MASTER \r\n");
                        }
                        else if (i4NodeState == ICCH_SLAVE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Node State             : SLAVE \r\n");
                        }
                    }
                    break;
                }
            }
        }
        else
        {
            /* Instance ID specified in the command is wrong, so return */
            CliPrintf (CliHandle,
                       "\r%% Invalid ICCL instance Id specified\r\n");
            return CLI_FAILURE;
        }
        if (nmhGetFsIcchPeerNodeState (&i4NodeState) != SNMP_FAILURE)
        {
            if (i4NodeState == ICCH_PEER_DOWN)
            {
                CliPrintf (CliHandle, "\r\n Peer Node State        : DOWN\r\n");
            }
            else if (i4NodeState == ICCH_PEER_UP)
            {
                CliPrintf (CliHandle, "\r\n Peer Node State        : UP\r\n");
            }
        }

        nmhGetFsIcchEnableProtoSync (&u4IcchSyncState);
        if (u4IcchSyncState == ICCH_BLOCK_ALL_SYNC)
        {
            CliPrintf (CliHandle, "\r\n ICCH Sync              : Disabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n ICCH Sync              : Enabled\r\n");
        }
        CliPrintf (CliHandle, "\r\n");
    }
    else if (i1Value == ICCH_CLI_ICCH_STATS)
    {
        nmhGetFsIcchStatsEnable (&i4StatsEnable);
        if (i4StatsEnable == ICCH_STATS_DISABLE)
        {
            CliPrintf (CliHandle,
                       "\r\n ICCH statistics collection is disabled\r\n");
        }
        else
        {
            nmhGetFsIcchStatsSyncMsgTxCount (&u4SyncMsgTxCount);
            nmhGetFsIcchStatsSyncMsgTxFailedCount (&u4SyncMsgTxFailedCount);
            nmhGetFsIcchStatsSyncMsgRxCount (&u4SyncMsgRxCount);
            nmhGetFsIcchStatsSyncMsgProcCount (&u4SyncMsgProcCount);
            nmhGetFsIcchStatsSyncMsgMissedCount (&u4SyncMsgMissedCount);
            CliPrintf (CliHandle,
                       "\r\n Sync msg Tx count        : %d\r\n",
                       u4SyncMsgTxCount);
            CliPrintf (CliHandle,
                       "\r\n Sync msg Tx Failed count : %d\r\n",
                       u4SyncMsgTxFailedCount);
            CliPrintf (CliHandle,
                       "\r\n Sync msg Rx count        : %d\r\n",
                       u4SyncMsgRxCount);
            CliPrintf (CliHandle,
                       "\r\n Sync msg Processed count : %d\r\n",
                       u4SyncMsgProcCount);
            CliPrintf (CliHandle,
                       "\r\n Sync msg Missed count    : %d\r\n",
                       u4SyncMsgMissedCount);
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : IcchCliDebugEnableDisable
 * Input(s)           : CliHandle - CLI handler
 *                      u4TrcVal - Trace Value
 *                      u1DbgFlag - Enable/Disable ICCH Debug
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE
 * Action             : This function is to set the ICCH debug level
 *
 ******************************************************************************/
UINT4
IcchCliDebugEnableDisable (tCliHandle CliHandle,
                           UINT4 u4TrcVal, UINT1 u1DbgFlag)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4TraceValue = 0;

    if (u1DbgFlag == ICCH_CLI_TRC_NO_DBG)
    {
        /* When no form of all debug is executed all traces are to be
         * disabled */
        if (u4TrcVal == ICCH_ALL_TRC)
        {
            u4TraceValue = 0;
        }
        else
        {
            u4TraceValue = ICCH_GET_TRC_LEVEL & (~u4TrcVal);
        }
    }
    else if (u1DbgFlag == ICCH_CLI_TRC_DBG)
    {
        u4TraceValue = ICCH_GET_TRC_LEVEL | u4TrcVal;
    }

    if (nmhTestv2FsIcchTrcLevel (&u4ErrorCode, u4TraceValue) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsIcchTrcLevel (u4TraceValue) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : IcchCliClearStatistics
 * Input(s)           : CliHandle - CLI handler
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE
 * Action             : This function clears the statistics
 *
 ******************************************************************************/
UINT4
IcchCliClearStatistics (tCliHandle CliHandle)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsIcchClearStats (&u4ErrorCode, ICCH_CLEAR_STATS_TRUE)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsIcchClearStats (ICCH_CLEAR_STATS_TRUE) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : IcchShowRunningConfig
 * Input(s)           : CliHandle - CLI handler
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE
 * Action             : This function displays the ICCH module's
 *               current configuration details.
 *******************************************************************************/
UINT4
IcchShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4RetVal = 0;
    UINT4               u4EnableProtoSync = 0;

    nmhGetFsIcchStatsEnable (&i4RetVal);
    if (i4RetVal == ICCH_STATS_DISABLE)
    {
        CliPrintf (CliHandle, "set iccl stats disable\r\n");
    }
    nmhGetFsIcchEnableProtoSync (&u4EnableProtoSync);
    if (u4EnableProtoSync != ICCH_BLOCK_ALL_SYNC)
    {
        CliPrintf (CliHandle, "set icch sync-up enable\r\n");
    }

    IcchShowRunningConfigTables (CliHandle);

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : IcchCliInstanceMode
 * Input(s)           : CliHandle - CLI handler
 *                      u4InstId - Instance ID
 *                      u1Mode - goto into ICCH instance mode or remove the
 *                               entry in the instance
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE
 * Action             : Function to goto into the instance mode or delete iccl 
 *                      entry in the instance
 *
 ******************************************************************************/
UINT4
IcchCliInstanceMode (tCliHandle CliHandle, UINT4 u4InstId, UINT1 u1Mode)
{
    tSNMP_OCTET_STRING_TYPE IfName;
    tIcchSessInfo       IcchSessionInfo;
    UINT4               u4ErrorCode = 0;
    UINT1               au1Cmd[MAX_PROMPT_LEN];

    MEMSET (&IfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&IcchSessionInfo, 0, sizeof (tIcchSessInfo));
    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);

    if (u1Mode == ICCH_INST_ID)
    {
        /* Changing to instance id greater than 0 should be blocked */
        if (u4InstId > ICCH_MAX_INST_ID)
        {
            CliPrintf (CliHandle,
                       "\r%% Invalid ICCL instance Id specified\r\n");
            return CLI_FAILURE;
        }

        /* Enter into ICCL instance mode */
        SPRINTF ((CHR1 *) au1Cmd, "%s%u", ICCH_CLI_INST_MODE, u4InstId);
        if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to enter into ICCL instance configuration mode\r\n");
            return CLI_FAILURE;
        }
    }
    else if (u1Mode == ICCH_NO_INST_ID)
    {
        if (nmhTestv2FsIcclSessionRowStatus (&u4ErrorCode, u4InstId,
                                             DESTROY) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsIcclSessionRowStatus (u4InstId, DESTROY) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle,
                   "\r\n <Information> ICCL instance deletion is successful. Requires system reboot\r\n");
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : IcchCliGetConfigPrompt
 * Input(s)           : pi1ModeName - Cli mode name
 *                      pi1DispStr - display string
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function is used to get the prompt for display.
 ******************************************************************************/
INT1
IcchCliGetConfigPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (ICCH_CLI_INST_MODE);
    tVlanId             VlanId = 0;
    UINT1               au1Cmd[MAX_PROMPT_LEN];

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);

    if (!pi1DispStr)
    {
        return FALSE;
    }

    if (pi1ModeName == NULL)
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, ICCH_CLI_INST_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;

    VlanId = (tVlanId) (CLI_ATOI (pi1ModeName));

    ICCH_CLI_SET_INST_ID (VlanId);

    SPRINTF ((CHR1 *) au1Cmd, "%s%s%u)#", "(config-", ICCH_CLI_INST_MODE,
             VlanId);

    STRCPY (pi1DispStr, au1Cmd);
    return TRUE;
}

/******************************************************************************
 * Function           : IcchCliGetConfigPrompt
 * Input(s)           : CliHandle - CliHandle
 * Output(s)          : None.
 * Returns            : CLI_FAILURE/CLI_SUCCESS.
 * Action             : This function is used to get remote fdb entries from 
 *                      peer.
 ******************************************************************************/
UINT4
IcchCliFetchRemoteFdb (tCliHandle CliHandle)
{

    UINT4               u4ErrCode = 0;
    INT4                i4FetchRemoteFdb = ICCH_TRUE;

    if (SNMP_SUCCESS !=
        nmhTestv2FsIcchFetchRemoteFdb (&u4ErrCode, i4FetchRemoteFdb))
    {
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    if (SNMP_SUCCESS != nmhSetFsIcchFetchRemoteFdb (i4FetchRemoteFdb))
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * +* Function           : IcchShowRunningConfigTables
 * +* Input(s)           : CliHandle - CLI handler
 * +* Output(s)          : None.
 * +* Returns            : CLI_SUCCESS/CLI_FAILURE
 * +* Action             : This function displays the information present in 
 * +*                      iccl.conf file.
 * +*******************************************************************************/
UINT4
IcchShowRunningConfigTables (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE IfName;
    tUtlInAddr          IcchIpAddr;
    tUtlInAddr          IcchIpMask;
    UINT4               u4InstanceId = 0;
    UINT4               u4NextInstanceId = 0;
    UINT4               u4IpAddress = 0;
    UINT4               u4IpMask = 0;
    UINT4               u4PortChannelId = 0;
    INT4                i4VlanId = 0;
    BOOL1               bPrintFlag = ICCH_FALSE;

    MEMSET (&IfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&IcchIpAddr, 0, sizeof (tUtlInAddr));
    MEMSET (&IcchIpMask, 0, sizeof (tUtlInAddr));
    if (nmhGetFirstIndexFsIcclSessionTable (&u4InstanceId) == SNMP_SUCCESS)
    {
        do
        {
            nmhGetFsIcclSessionInterface (u4InstanceId, &IfName);
            nmhGetFsIcclSessionVlan (u4InstanceId, &i4VlanId);
            nmhGetFsIcclSessionIpAddress (u4InstanceId, &u4IpAddress);
            nmhGetFsIcclSessionSubnetMask (u4InstanceId, &u4IpMask);
            IcchIpAddr.u4Addr = OSIX_NTOHL (u4IpAddress);
            IcchIpMask.u4Addr = OSIX_NTOHL (u4IpMask);
            u4PortChannelId = (UINT4) (ATOI (IfName.pu1_OctetList + 2));

            if (STRNCMP
                (IfName.pu1_OctetList, ICCH_DEFAULT_INT,
                 STRLEN (IfName.pu1_OctetList)) != 0)
            {
                bPrintFlag = ICCH_TRUE;
            }
            if ((i4VlanId != ICCH_DEFAULT_VLAN) ||
                (STRNCMP
                 ((INET_NTOA (IcchIpAddr)), ICCH_DEFAULT_IP,
                  STRLEN (ICCH_DEFAULT_IP)) != 0)
                ||
                (STRNCMP
                 ((INET_NTOA (IcchIpMask)), ICCH_SUBNET_MASK,
                  STRLEN (ICCH_SUBNET_MASK)) != 0))
            {
                bPrintFlag = ICCH_TRUE;
            }
            if (bPrintFlag == ICCH_TRUE)
            {
                if (u4InstanceId == ICCH_DEFAULT_INST_ID)
                {
                    CliPrintf (CliHandle, "iccl instance default\r\n");
                }
                else
                {
                    CliPrintf (CliHandle, "iccl instance %d\r\n", u4InstanceId);
                }
                CliPrintf (CliHandle, "set iccl ip address %s",
                           INET_NTOA (IcchIpAddr));
                CliPrintf (CliHandle,
                           " %s interface port-channel %d vlan %d\r\n",
                           INET_NTOA (IcchIpMask), u4PortChannelId, i4VlanId);
            }
            CliPrintf (CliHandle, "!\r\n");

            if (nmhGetNextIndexFsIcclSessionTable
                (u4InstanceId, &u4NextInstanceId) != SNMP_SUCCESS)
            {
                break;
            }
            u4InstanceId = u4NextInstanceId;
        }
        while (1);
    }
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IssIcchShowDebugging                                */
/*                                                                         */
/*     Description   : This function is used to display debug level for    */
/*                     ICCH module                                         */
/*                                                                         */
/*     INPUT         : CliHandle                                           */
/*                                                                         */
/*     OUTPUT        : None                                                */
/*                                                                         */
/*     RETURNS       : None                                                */
/*                                                                         */
/***************************************************************************/
VOID
IssIcchShowDebugging (tCliHandle CliHandle)
{
    UINT4               u4CurTrcLevel = 0;

    nmhGetFsIcchTrcLevel (&u4CurTrcLevel);
    if (u4CurTrcLevel == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "\rICCH :");
    if ((u4CurTrcLevel & ICCH_MGMT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  ICCH management debugging is on");
    }
    if ((u4CurTrcLevel & ICCH_PKT_DUMP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  ICCH pkt-dump-trc debugging is on");
    }
    if ((u4CurTrcLevel & ICCH_ALL_FAILURE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  ICCH failure debugging is on");
    }
    if ((u4CurTrcLevel & ICCH_CRITICAL_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  ICCH critical debugging is on");
    }
    if ((u4CurTrcLevel & ICCH_SYNC_EVT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  ICCH syncup-event debugging is on");
    }
    if ((u4CurTrcLevel & ICCH_SYNC_MSG_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  ICCH syncup-msg debugging is on");
    }
    if ((u4CurTrcLevel & ICCH_NOTIF_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  ICCH notification debugging is on");
    }
    CliPrintf (CliHandle, "\r\n");
}

#endif
