/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: icchsz.c,v 1.2 2015/02/17 12:29:24 siva Exp $
 *
 * Description: This file contains sizing related parameters.
 *******************************************************************/

#define _ICCHSZ_C
#include "icchincs.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
IcchSizingMemCreateMemPools ()
{
    UINT4               u4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < ICCH_MAX_SIZING_ID; i4SizingId++)
    {
        u4RetVal = MemCreateMemPool (FsICCHSizingParams[i4SizingId].u4StructSize,
                                     FsICCHSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(IcchMemPoolIds[i4SizingId]));
        if (u4RetVal == MEM_FAILURE)
        {
            IcchSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
IcchSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsICCHSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, IcchMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
IcchSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < ICCH_MAX_SIZING_ID; i4SizingId++)
    {
        if (IcchMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (IcchMemPoolIds[i4SizingId]);
            IcchMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
