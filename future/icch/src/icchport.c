/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: icchport.c,v 1.2 2015/05/25 12:37:01 siva Exp $
*
* Description:  Inter Chassis Communication Handler portable functions .
***********************************************************************/
#ifndef _ICCHPORT_C_
#define _ICCHPORT_C_

#include "icchincs.h"

/******************************************************************************
 * Function Name      : IcchHbRegisterApps
 *
 * Description        : This is the function used by ICCH to register with HB. 
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None 
 *****************************************************************************/
VOID
IcchHbRegisterApps (VOID)
{
   HbRegisterApps (HB_ICCH_APP);
   return;
}


/******************************************************************************
 * Function Name      : IcchVlanDeleteRemoteFdb
 *
 * Description        : This is the function used by ICCH to delete the remote
 *                      learnt fdbs. 
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None 
 *****************************************************************************/
VOID
IcchVlanDeleteRemoteFdb (VOID)
{
   VlanApiDeleteRemoteFdb ();
   return;
}



/******************************************************************************
 * Function Name      : IcchVlanSendBulkReqMsg
 *
 * Description        : This is the function used by ICCH to send bulk request 
 *                      message.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None 
 *****************************************************************************/
VOID
IcchVlanSendBulkReqMsg (VOID)
{
    VlanApiSendBulkReq ();
    return ;
}
#endif /* _ICCHPORT_C_ */
