/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: fsicchlw.c,v 1.25 2017/09/25 12:11:45 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "icchincs.h"
# include "vrrp.h"
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIcchTrcLevel
 Input       :  The Indices

                The Object 
                retValFsIcchTrcLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIcchTrcLevel (UINT4 *pu4RetValFsIcchTrcLevel)
{
    ICCH_LOCK ();
    *pu4RetValFsIcchTrcLevel = ICCH_GET_TRC_LEVEL;
    ICCH_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIcchStatsEnable
 Input       :  The Indices

                The Object 
                retValFsIcchStatsEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIcchStatsEnable (INT4 *pi4RetValFsIcchStatsEnable)
{
    ICCH_LOCK ();
    *pi4RetValFsIcchStatsEnable = gIcchInfo.i4StatsEnable;
    ICCH_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIcchClearStats
 Input       :  The Indices

                The Object
                retValFsIcchClearStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIcchClearStats (INT4 *pi4RetValFsIcchClearStats)
{
    ICCH_LOCK ();
    *pi4RetValFsIcchClearStats = gIcchInfo.i4ClearStats;
    ICCH_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIcchEnableProtoSync
 Input       :  The Indices

                The Object
                retValFsIcchEnableProtoSync
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIcchEnableProtoSync (UINT4 *pu4RetValFsIcchEnableProtoSync)
{
    ICCH_LOCK ();
    *pu4RetValFsIcchEnableProtoSync = gIcchInfo.u4EnableProtoSync;
    ICCH_UNLOCK ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIcchFetchRemoteFdb
 Input       :  The Indices

                The Object 
                retValFsIcchFetchRemoteFdb
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIcchFetchRemoteFdb (INT4 *pi4RetValFsIcchFetchRemoteFdb)
{
    UNUSED_PARAM (pi4RetValFsIcchFetchRemoteFdb);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIcchPeerNodeIpAddress
 Input       :  The Indices

                The Object 
                retValFsIcchPeerNodeIpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIcchPeerNodeIpAddress (UINT4 *pu4RetValFsIcchPeerNodeIpAddress)
{
    ICCH_LOCK ();

    *pu4RetValFsIcchPeerNodeIpAddress = gIcchInfo.u4PeerNodeId;

    ICCH_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIcchPeerNodeState
 Input       :  The Indices

                The Object 
                retValFsIcchPeerNodeState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIcchPeerNodeState (INT4 *pi4RetValFsIcchPeerNodeState)
{
    ICCH_LOCK ();
    *pi4RetValFsIcchPeerNodeState = (INT4) (gIcchInfo.u4PeerNodeState);
    ICCH_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsIcchOverrideLocalAffinity
 Input       :  The Indices

                The Object 
                retValFsIcchOverrideLocalAffinity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
***************************************************************************/
INT1
nmhGetFsIcchOverrideLocalAffinity (INT4 *pi4RetValFsIcchOverrideLocalAffinity)
{
    ICCH_LOCK ();
    *pi4RetValFsIcchOverrideLocalAffinity = gIcchInfo.i4OverrideLocalAffinity;
    ICCH_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIcchTrcLevel
 Input       :  The Indices

                The Object 
                setValFsIcchTrcLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIcchTrcLevel (UINT4 u4SetValFsIcchTrcLevel)
{

    /* If the value is set already ,no need to set again */
    if (u4SetValFsIcchTrcLevel == ICCH_GET_TRC_LEVEL)
    {
        return SNMP_SUCCESS;
    }

    ICCH_LOCK ();

    /* If PKT_DUMP_TRC is enabled, disable other traces and enable it alone */
    if (ICCH_PKT_DUMP_TRC == (u4SetValFsIcchTrcLevel & ICCH_PKT_DUMP_TRC))
    {
        u4SetValFsIcchTrcLevel = u4SetValFsIcchTrcLevel & ICCH_PKT_DUMP_TRC;
    }

    gIcchInfo.u4IcchTrc = u4SetValFsIcchTrcLevel;
    ICCH_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIcchStatsEnable
 Input       :  The Indices

                The Object 
                setValFsIcchStatsEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIcchStatsEnable (INT4 i4SetValFsIcchStatsEnable)
{
    ICCH_LOCK ();
    gIcchInfo.i4StatsEnable = i4SetValFsIcchStatsEnable;
    ICCH_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIcchClearStats
 Input       :  The Indices

                The Object
                setValFsIcchClearStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIcchClearStats (INT4 i4SetValFsIcchClearStats)
{
    if (i4SetValFsIcchClearStats == ICCH_CLEAR_STATS_TRUE)
    {
        ICCH_LOCK ();
        gIcchInfo.i4ClearStats = ICCH_CLEAR_STATS_TRUE;
        gIcchInfo.IcchRxStats.u4SyncMsgMissedCount = 0;
        gIcchInfo.IcchRxStats.u4SyncMsgProcCount = 0;
        gIcchInfo.IcchRxStats.u4SyncMsgRxCount = 0;
        gIcchInfo.IcchTxStats.u4SyncMsgTxFailedCount = 0;
        gIcchInfo.IcchTxStats.u4SyncMsgTxCount = 0;
        gIcchInfo.i4ClearStats = ICCH_CLEAR_STATS_FALSE;
        ICCH_UNLOCK ();
        return SNMP_SUCCESS;
    }
    else
    {
        /* No change as gIcchInfo.i4ClearStats is ICCH_CLEAR_STATS_FALSE 
           by default */
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhSetFsIcchEnableProtoSync
 Input       :  The Indices

                The Object
                setValFsIcchEnableProtoSync
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIcchEnableProtoSync (UINT4 u4SetValFsIcchEnableProtoSync)
{
    ICCH_LOCK ();

    if (gIcchInfo.u4EnableProtoSync == u4SetValFsIcchEnableProtoSync)
    {
        /* The given value is already updated. No need to set it again */
        ICCH_UNLOCK ();
        return SNMP_SUCCESS;
    }

    gIcchInfo.u4EnableProtoSync = u4SetValFsIcchEnableProtoSync;

    ICCH_UNLOCK ();

    IcchTrapSendNotifications (ICCH_ENABLE_PROTO_SYNC_TRAP);

    if (u4SetValFsIcchEnableProtoSync == ICCH_ENABLE_ALL_SYNC)

    {
        /* Initiate VLAN bulk sync-up. Once VLAN bulk is completed, 
         * next protocol's bulk will be initiated 
         */
        ICCH_TRC (ICCH_SYNC_MSG_TRC | ICCH_CRITICAL_TRC,
                  "Dynamic sync-up is enabled on ICCH\r\n");
        IcchVlanSendBulkReqMsg ();
        SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gIcchInfo.u4SysLogId,
                      "Dynamic sync-up is enabled on ICCH"));

    }

    /* Delete the remote learnt FDB entries when protocol syncup is blocked */
    if (u4SetValFsIcchEnableProtoSync == ICCH_BLOCK_ALL_SYNC)
    {
        IcchVlanDeleteRemoteFdb ();
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIcchFetchRemoteFdb
 Input       :  The Indices

                The Object 
                setValFsIcchFetchRemoteFdb
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIcchFetchRemoteFdb (INT4 i4SetValFsIcchFetchRemoteFdb)
{
    ICCH_LOCK ();
    gIcchInfo.bFetchRemoteFdb = (BOOL1) i4SetValFsIcchFetchRemoteFdb;
    ICCH_UNLOCK ();

    if (i4SetValFsIcchFetchRemoteFdb == ICCH_TRUE)
    {
        VlanApiSendBulkReq ();
    }

    ICCH_LOCK ();
    gIcchInfo.bFetchRemoteFdb = ICCH_FALSE;
    ICCH_UNLOCK ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIcchOverrideLocalAffinity
 Input       :  The Indices

                The Object 
                setValFsIcchOverrideLocalAffinity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIcchOverrideLocalAffinity (INT4 i4SetValFsIcchOverrideLocalAffinity)
{
    ICCH_LOCK ();
    if (gIcchInfo.i4OverrideLocalAffinity ==
        i4SetValFsIcchOverrideLocalAffinity)
    {
        ICCH_UNLOCK ();
        return SNMP_SUCCESS;
    }
    gIcchInfo.i4OverrideLocalAffinity = i4SetValFsIcchOverrideLocalAffinity;
    ICCH_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIcchTrcLevel
 Input       :  The Indices

                The Object 
                testValFsIcchTrcLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIcchTrcLevel (UINT4 *pu4ErrorCode, UINT4 u4TestValFsIcchTrcLevel)
{

    /* Check if any trace value is passed other than the supported traces.If it is 
     *  true then throw error message */
    if ((u4TestValFsIcchTrcLevel & ~(ICCH_ALL_TRC | ICCH_PKT_DUMP_TRC)) > 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ICCH_INVALID_DEBUG_TRC_LEVEL);
        return SNMP_FAILURE;
    }

    /* When PKT_DUMP_TRC is enabled on the system, print the error string
     * in cases when user enables other traces.
     */

    if ((u4TestValFsIcchTrcLevel & ICCH_PKT_DUMP_TRC)
        && (u4TestValFsIcchTrcLevel & ICCH_ALL_TRC))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ICCH_PKT_DUMP_ENABLED);
        return SNMP_FAILURE;

    }
    /*Check packet dump trace is already enabled in the system */
    if (ICCH_PKT_DUMP_TRC == ICCH_GET_TRC_LEVEL)
    {
        /*If packet dump trace is enabled,we can only disable the trace value as zero.If
         *the value passed by the user is also packet dump trace (which is already enabled)
         *do nothing.if anyother value is passed throw error message*/
        if ((u4TestValFsIcchTrcLevel != 0)
            && (u4TestValFsIcchTrcLevel != ICCH_PKT_DUMP_TRC))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_ICCH_PKT_DUMP_ENABLED);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIcchStatsEnable
 Input       :  The Indices

                The Object 
                testValFsIcchStatsEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIcchStatsEnable (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsIcchStatsEnable)
{
    if ((i4TestValFsIcchStatsEnable != ICCH_STATS_ENABLE) &&
        (i4TestValFsIcchStatsEnable != ICCH_STATS_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ICCH_INVALID_STAT);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIcchClearStats
 Input       :  The Indices

                The Object
                testValFsIcchClearStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIcchClearStats (UINT4 *pu4ErrorCode, INT4 i4TestValFsIcchClearStats)
{
    if ((i4TestValFsIcchClearStats != ICCH_CLEAR_STATS_TRUE) &&
        (i4TestValFsIcchClearStats != ICCH_CLEAR_STATS_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ICCH_INVALID_CLEAR_STAT);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIcchEnableProtoSync
 Input       :  The Indices

                The Object
                testValFsIcchEnableProtoSync
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIcchEnableProtoSync (UINT4 *pu4ErrorCode,
                                UINT4 u4TestValFsIcchEnableProtoSync)
{
    if ((u4TestValFsIcchEnableProtoSync != ICCH_ENABLE_ALL_SYNC) &&
        (u4TestValFsIcchEnableProtoSync != ICCH_BLOCK_ALL_SYNC))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ICCH_INVALID_PROTO_SYNC);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsIcchFetchRemoteFdb
 Input       :  The Indices

                The Object 
                testValFsIcchFetchRemoteFdb
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIcchFetchRemoteFdb (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsIcchFetchRemoteFdb)
{
    ICCH_LOCK ();
    if (ICCH_ENABLE_ALL_SYNC != gIcchInfo.u4EnableProtoSync)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ICCH_SYNCUP_DISABLED);
        ICCH_UNLOCK ();
        return SNMP_FAILURE;
    }
    ICCH_UNLOCK ();

    if ((i4TestValFsIcchFetchRemoteFdb != ICCH_TRUE) &&
        (i4TestValFsIcchFetchRemoteFdb != ICCH_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIcchStatsSyncMsgTxCount
 Input       :  The Indices

                The Object 
                retValFsIcchStatsSyncMsgTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIcchStatsSyncMsgTxCount (UINT4 *pu4RetValFsIcchStatsSyncMsgTxCount)
{
    ICCH_LOCK ();
    *pu4RetValFsIcchStatsSyncMsgTxCount =
        gIcchInfo.IcchTxStats.u4SyncMsgTxCount;
    ICCH_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIcchStatsSyncMsgTxFailedCount
 Input       :  The Indices

                The Object 
                retValFsIcchStatsSyncMsgTxFailedCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIcchStatsSyncMsgTxFailedCount (UINT4
                                       *pu4RetValFsIcchStatsSyncMsgTxFailedCount)
{
    ICCH_LOCK ();
    *pu4RetValFsIcchStatsSyncMsgTxFailedCount =
        gIcchInfo.IcchTxStats.u4SyncMsgTxFailedCount;
    ICCH_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIcchStatsSyncMsgRxCount
 Input       :  The Indices

                The Object 
                retValFsIcchStatsSyncMsgRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIcchStatsSyncMsgRxCount (UINT4 *pu4RetValFsIcchStatsSyncMsgRxCount)
{
    ICCH_LOCK ();
    *pu4RetValFsIcchStatsSyncMsgRxCount =
        gIcchInfo.IcchRxStats.u4SyncMsgRxCount;
    ICCH_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIcchStatsSyncMsgProcCount
 Input       :  The Indices

                The Object 
                retValFsIcchStatsSyncMsgProcCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIcchStatsSyncMsgProcCount (UINT4 *pu4RetValFsIcchStatsSyncMsgProcCount)
{
    ICCH_LOCK ();
    *pu4RetValFsIcchStatsSyncMsgProcCount =
        gIcchInfo.IcchRxStats.u4SyncMsgProcCount;
    ICCH_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIcchStatsSyncMsgMissedCount
 Input       :  The Indices

                The Object 
                retValFsIcchStatsSyncMsgMissedCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIcchStatsSyncMsgMissedCount (UINT4
                                     *pu4RetValFsIcchStatsSyncMsgMissedCount)
{
    ICCH_LOCK ();
    *pu4RetValFsIcchStatsSyncMsgMissedCount =
        gIcchInfo.IcchRxStats.u4SyncMsgMissedCount;
    ICCH_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIcchFetchRemoteFdb
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIcchFetchRemoteFdb (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIcchOverrideLocalAffinity
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIcchOverrideLocalAffinity (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsIcclSessionTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIcclSessionTable
 Input       :  The Indices
                FsIcclSessionInstanceId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIcclSessionTable (UINT4 u4FsIcclSessionInstanceId)
{

    /* Instance ID should be 0 */
    if (u4FsIcclSessionInstanceId > ICCH_MAX_INST_ID)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIcclSessionTable
 Input       :  The Indices
                FsIcclSessionInstanceId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIcclSessionTable (UINT4 *pu4FsIcclSessionInstanceId)
{
    UINT2               u2Index = 0;

    ICCH_LOCK ();
    *pu4FsIcclSessionInstanceId = gIcchSessionInfo[u2Index].u4InstanceId;
    ICCH_UNLOCK ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIcclSessionTable
 Input       :  The Indices
                FsIcclSessionInstanceId
                nextFsIcclSessionInstanceId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIcclSessionTable (UINT4 u4FsIcclSessionInstanceId,
                                   UINT4 *pu4NextFsIcclSessionInstanceId)
{
    UINT2               u2NextIndex =
        (UINT2) (u4FsIcclSessionInstanceId + ICCH_ONE);

    if (u2NextIndex < ICCH_TOTAL_NUM_OF_ACTV_INST)
    {
        *pu4NextFsIcclSessionInstanceId =
            gIcchSessionInfo[u2NextIndex].u4InstanceId;
    }
    else
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIcclSessionInterface
 Input       :  The Indices
                FsIcclSessionInstanceId

                The Object
                retValFsIcclSessionInterface
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIcclSessionInterface (UINT4 u4FsIcclSessionInstanceId,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsIcclSessionInterface)
{
    ICCH_LOCK ();

    pRetValFsIcclSessionInterface->pu1_OctetList =
        (UINT1 *) gIcchSessionInfo[u4FsIcclSessionInstanceId].ac1IcclInterface;
    pRetValFsIcclSessionInterface->i4_Length =
        (INT4) (STRLEN
                (gIcchSessionInfo[u4FsIcclSessionInstanceId].ac1IcclInterface));

    ICCH_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIcclSessionIpAddress
 Input       :  The Indices
                FsIcclSessionInstanceId

                The Object
                retValFsIcclSessionIpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIcclSessionIpAddress (UINT4 u4FsIcclSessionInstanceId,
                              UINT4 *pu4RetValFsIcclSessionIpAddress)
{
    ICCH_LOCK ();

    *pu4RetValFsIcclSessionIpAddress =
        gIcchSessionInfo[u4FsIcclSessionInstanceId].u4IpAddress;

    ICCH_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIcclSessionSubnetMask
 Input       :  The Indices
                FsIcclSessionInstanceId

                The Object
                retValFsIcclSessionSubnetMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIcclSessionSubnetMask (UINT4 u4FsIcclSessionInstanceId,
                               UINT4 *pu4RetValFsIcclSessionSubnetMask)
{
    ICCH_LOCK ();

    *pu4RetValFsIcclSessionSubnetMask =
        gIcchSessionInfo[u4FsIcclSessionInstanceId].u4SubnetMask;

    ICCH_UNLOCK ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIcclSessionVlan
 Input       :  The Indices
                FsIcclSessionInstanceId

                The Object
                retValFsIcclSessionVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIcclSessionVlan (UINT4 u4FsIcclSessionInstanceId,
                         INT4 *pi4RetValFsIcclSessionVlan)
{
    ICCH_LOCK ();

    *pi4RetValFsIcclSessionVlan =
        gIcchSessionInfo[u4FsIcclSessionInstanceId].VlanId;

    ICCH_UNLOCK ();

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsIcclSessionNodeState
 Input       :  The Indices
                FsIcclSessionInstanceId

                The Object
                retValFsIcclSessionNodeState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIcclSessionNodeState (UINT4 u4FsIcclSessionInstanceId,
                              INT4 *pi4RetValFsIcclSessionNodeState)
{
    ICCH_LOCK ();

    *pi4RetValFsIcclSessionNodeState =
        gIcchSessionInfo[u4FsIcclSessionInstanceId].i4SessionNodeState;

    ICCH_UNLOCK ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIcclSessionRowStatus
 Input       :  The Indices
                FsIcclSessionInstanceId

                The Object
                retValFsIcclSessionRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIcclSessionRowStatus (UINT4 u4FsIcclSessionInstanceId,
                              INT4 *pi4RetValFsIcclSessionRowStatus)
{
    if (gIcchSessionInfo[u4FsIcclSessionInstanceId].u4InstanceId ==
        u4FsIcclSessionInstanceId)
    {
        *pi4RetValFsIcclSessionRowStatus = ACTIVE;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIcclSessionInterface
 Input       :  The Indices
                FsIcclSessionInstanceId

                The Object
                setValFsIcclSessionInterface
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIcclSessionInterface (UINT4 u4FsIcclSessionInstanceId,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValFsIcclSessionInterface)
{
    tIcchSessInfo       IcchSessionInfo;

    MEMSET (&IcchSessionInfo, 0, sizeof (tIcchSessInfo));

    IcchSessionInfo.u4InstanceId = u4FsIcclSessionInstanceId;
    STRNCPY (IcchSessionInfo.ac1IcclInterface,
             pSetValFsIcclSessionInterface->pu1_OctetList,
             CFA_MAX_PORT_NAME_LENGTH - 1);

    if (OSIX_FAILURE == IcchUpdateIcclConfFile (&IcchSessionInfo,
                                                ICCL_FILE_WRITE))
    {
        return SNMP_FAILURE;
    }
    STRNCPY (gIcchSessionInfo[u4FsIcclSessionInstanceId].ac1NextIcclInterface,
             pSetValFsIcclSessionInterface->pu1_OctetList,
             CFA_MAX_PORT_NAME_LENGTH - 1);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIcclSessionIpAddress
 Input       :  The Indices
                FsIcclSessionInstanceId

                The Object
                setValFsIcclSessionIpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIcclSessionIpAddress (UINT4 u4FsIcclSessionInstanceId,
                              UINT4 u4SetValFsIcclSessionIpAddress)
{
    tIcchSessInfo       IcchSessionInfo;
    UINT2               u2Index = ICCH_DEFAULT_INST_ID;

    MEMSET (&IcchSessionInfo, 0, sizeof (tIcchSessInfo));

    IcchSessionInfo.u4InstanceId = u4FsIcclSessionInstanceId;
    IcchSessionInfo.u4IpAddress = u4SetValFsIcclSessionIpAddress;

    if (OSIX_FAILURE == IcchUpdateIcclConfFile (&IcchSessionInfo,
                                                ICCL_FILE_WRITE))
    {
        return SNMP_FAILURE;
    }

    gIcchSessionInfo[u2Index].u4NextIpAddress = u4SetValFsIcclSessionIpAddress;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsIcclSessionSubnetMask
 Input       :  The Indices
                FsIcclSessionInstanceId

                The Object
                setValFsIcclSessionSubnetMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIcclSessionSubnetMask (UINT4 u4FsIcclSessionInstanceId,
                               UINT4 u4SetValFsIcclSessionSubnetMask)
{
    tIcchSessInfo       IcchSessionInfo;
    UINT2               u2Index = ICCH_DEFAULT_INST_ID;

    MEMSET (&IcchSessionInfo, 0, sizeof (tIcchSessInfo));

    IcchSessionInfo.u4InstanceId = u4FsIcclSessionInstanceId;
    IcchSessionInfo.u4SubnetMask = u4SetValFsIcclSessionSubnetMask;

    if (OSIX_FAILURE == IcchUpdateIcclConfFile (&IcchSessionInfo,
                                                ICCL_FILE_WRITE))
    {
        return SNMP_FAILURE;
    }

    gIcchSessionInfo[u2Index].u4NextSubnetMask =
        u4SetValFsIcclSessionSubnetMask;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsIcclSessionVlan
 Input       :  The Indices
                FsIcclSessionInstanceId

                The Object
                setValFsIcclSessionVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIcclSessionVlan (UINT4 u4FsIcclSessionInstanceId,
                         INT4 i4SetValFsIcclSessionVlan)
{
    tIcchSessInfo       IcchSessionInfo;

    MEMSET (&IcchSessionInfo, 0, sizeof (tIcchSessInfo));

    IcchSessionInfo.u4InstanceId = u4FsIcclSessionInstanceId;
    IcchSessionInfo.VlanId = (tVlanId) (i4SetValFsIcclSessionVlan);

    if (OSIX_FAILURE == IcchUpdateIcclConfFile (&IcchSessionInfo,
                                                ICCL_FILE_WRITE))
    {
        return SNMP_FAILURE;
    }
    gIcchSessionInfo[u4FsIcclSessionInstanceId].NextVlanId =
        (tVlanId) (i4SetValFsIcclSessionVlan);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIcclSessionRowStatus
 Input       :  The Indices
                FsIcclSessionInstanceId

                The Object
                setValFsIcclSessionRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIcclSessionRowStatus (UINT4 u4FsIcclSessionInstanceId,
                              INT4 i4SetValFsIcclSessionRowStatus)
{
    tIcchSessInfo       IcchSessionInfo;

    MEMSET (&IcchSessionInfo, 0, sizeof (tIcchSessInfo));

    IcchSessionInfo.u4InstanceId = u4FsIcclSessionInstanceId;

    switch (i4SetValFsIcclSessionRowStatus)
    {
        case CREATE_AND_GO:
            break;

        case DESTROY:

            /*When 'no iccl instance default' command is given,
               reset the ICCL parameters to default values */

            STRCPY (IcchSessionInfo.ac1IcclInterface, ICCH_DEFAULT_INT);
            IcchSessionInfo.VlanId = ICCH_DEFAULT_VLAN;
            IcchSessionInfo.u4IpAddress =
                OSIX_NTOHL (INET_ADDR (ICCH_DEFAULT_IP));
            IcchSessionInfo.u4SubnetMask = ISS_CUST_SYS_DEF_IP_MASK;

            if (IcchUpdateIcclConfFile (&IcchSessionInfo, ICCL_FILE_WRITE) ==
                OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

            break;
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIcclSessionInterface
 Input       :  The Indices
                FsIcclSessionInstanceId

                The Object
                testValFsIcclSessionInterface
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIcclSessionInterface (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsIcclSessionInstanceId,
                                 tSNMP_OCTET_STRING_TYPE
                                 * pTestValFsIcclSessionInterface)
{
    UINT4               u4PortChannelId = 0;

    /* ICCL interface should be port channel */
    if (0 != STRNCMP (pTestValFsIcclSessionInterface->pu1_OctetList, "po", 2))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ICCH_NO_PORT_CHNNEL_INT);
        return SNMP_FAILURE;
    }

    /* port channel should be between 1 to 65535 */
    u4PortChannelId =
        (UINT4) (ATOI (pTestValFsIcclSessionInterface->pu1_OctetList + 2));

    if ((u4PortChannelId < ICCH_MIN_PORT_CHNL_ID) ||
        (u4PortChannelId > ICCH_MAX_PORT_CHNL_ID))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ICCH_INVALID_PC_ID);
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceFsIcclSessionTable (u4FsIcclSessionInstanceId)
        != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ICCH_INVALID_INST_ID);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIcclSessionIpAddress
 Input       :  The Indices
                FsIcclSessionInstanceId

                The Object
                testValFsIcclSessionIpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIcclSessionIpAddress (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsIcclSessionInstanceId,
                                 UINT4 u4TestValFsIcclSessionIpAddress)
{
    UINT2               u2IfIndex = ICCH_DEFAULT_INST_ID;
    UINT4               u4IcclNetAddr = 0;

    /* Allow IP address which are Class A, B or C */
    if (!((ISS_IS_ADDR_CLASS_A (u4TestValFsIcclSessionIpAddress)) ||
          (ISS_IS_ADDR_CLASS_B (u4TestValFsIcclSessionIpAddress)) ||
          (ISS_IS_ADDR_CLASS_C (u4TestValFsIcclSessionIpAddress)) ||
          (ISS_IS_ADDR_VALID (u4TestValFsIcclSessionIpAddress))))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ICCH_INVALID_IP_ADDR);
        return SNMP_FAILURE;
    }
    /* If the newly configuring ICCL IP address overlaps with an existing IP interface
       in the system (other than ICCL interface), configuration should be blocked. */
    u4IcclNetAddr =
        u4TestValFsIcclSessionIpAddress & gIcchSessionInfo[u2IfIndex].
        u4SubnetMask;
    if (u4IcclNetAddr !=
        (gIcchSessionInfo[u2IfIndex].u4IpAddress & gIcchSessionInfo[u2IfIndex].
         u4SubnetMask))
    {
        if (SNMP_FAILURE == CfaIpIfIsLocalNet (u4TestValFsIcclSessionIpAddress))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_ICCH_IP_OVERLAP);
            return SNMP_FAILURE;
        }
    }

    if (nmhValidateIndexInstanceFsIcclSessionTable (u4FsIcclSessionInstanceId)
        != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ICCH_INVALID_INST_ID);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIcclSessionSubnetMask
 Input       :  The Indices
                FsIcclSessionInstanceId

                The Object
                testValFsIcclSessionSubnetMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIcclSessionSubnetMask (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsIcclSessionInstanceId,
                                  UINT4 u4TestValFsIcclSessionSubnetMask)
{
    /* Check the subnet is valid */
    if (IssValidateSubnetMask (u4TestValFsIcclSessionSubnetMask) == ISS_FAILURE
        || ICCL_MAX_SUBNET == u4TestValFsIcclSessionSubnetMask)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ICCH_INVALID_SUBNET);
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceFsIcclSessionTable (u4FsIcclSessionInstanceId)
        != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ICCH_INVALID_INST_ID);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIcclSessionVlan
 Input       :  The Indices
                FsIcclSessionInstanceId

                The Object
                testValFsIcclSessionVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIcclSessionVlan (UINT4 *pu4ErrorCode,
                            UINT4 u4FsIcclSessionInstanceId,
                            INT4 i4TestValFsIcclSessionVlan)
{
    /* Vlan ID should be between 1 and 4095 */
    if ((i4TestValFsIcclSessionVlan > ICCH_MAX_VLAN) ||
        (i4TestValFsIcclSessionVlan < ICCH_MIN_VLAN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ICCH_INVALID_VLAN);
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceFsIcclSessionTable (u4FsIcclSessionInstanceId)
        != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ICCH_INVALID_INST_ID);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIcclSessionRowStatus
 Input       :  The Indices
                FsIcclSessionInstanceId

                The Object
                testValFsIcclSessionRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIcclSessionRowStatus (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsIcclSessionInstanceId,
                                 INT4 i4TestValFsIcclSessionRowStatus)
{
    /* Only CreateAndGo and Destroy are the allowed values of this
     * Row status object.
     */
    if ((i4TestValFsIcclSessionRowStatus != CREATE_AND_GO) &&
        (i4TestValFsIcclSessionRowStatus != DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceFsIcclSessionTable (u4FsIcclSessionInstanceId)
        != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ICCH_INVALID_INST_ID);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIcchOverrideLocalAffinity
 Input       :  The Indices

                The Object 
                testValFsIcchOverrideLocalAffinity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIcchOverrideLocalAffinity (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFsIcchOverrideLocalAffinity)
{
    if ((i4TestValFsIcchOverrideLocalAffinity != ICCH_TRUE) &&
        (i4TestValFsIcchOverrideLocalAffinity != ICCH_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsIcclSessionTable
 Input       :  The Indices
                FsIcclSessionInstanceId
                FsIcclSessionInterface
                FsIcclSessionIpAddress
                FsIcclSessionSubnetMask
                FsIcclSessionVlan
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIcclSessionTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIcchTrcLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIcchTrcLevel (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIcchStatsEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIcchStatsEnable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIcchClearStats
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIcchClearStats (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIcchEnableProtoSync
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIcchEnableProtoSync (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
