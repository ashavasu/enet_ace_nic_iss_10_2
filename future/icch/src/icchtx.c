/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: icchtx.c,v 1.14.2.1 2018/03/16 13:43:47 siva Exp $
*
* Description: ICCH sync messages transmission related functions.
***********************************************************************/
#include "icchincs.h"
#include "fssocket.h"

/******************************************************************************
 * Function           : IcchSendMsg
 * Input(s)           : u1SsnIndex -> Session index
 *                      pu1Data -> Sync linear data
 *                      u4TotLen -> Total length of linear data
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS/ICCH_FAILURE
 * Action             : Routine to send out the ICCH msgs (which are arrived from
 *                      applications) to the peer.
*******************************************************************************/
UINT4
IcchSendMsg (UINT1 u1SsnIndex, INT4 i4ConnFd, UINT1 *pu1Data, UINT4 u4TotLen)
{
    UINT4               u4RetVal = ICCH_SUCCESS;
    INT4                i4WrittenBytes = 0;

    u4RetVal = IcchTcpSockSend (i4ConnFd, pu1Data, (UINT2) u4TotLen,
                                &i4WrittenBytes);
    if (u4RetVal == ICCH_FAILURE)
    {
        /* Send failure case to be handled */
        ICCH_TRC (ICCH_ALL_FAILURE_TRC, "IcchSendMsg: "
                  "Reliable sync message send failed\r\n");

        ICCH_INCR_TX_STAT_SYNC_MSG_FAILED ();

        IcchAddSyncMsgIntoTxList (u1SsnIndex, pu1Data, u4TotLen,
                                  (UINT4) i4WrittenBytes);
        IcchUtilCloseSockConn (u1SsnIndex);
        if (ICCH_SEND_EVENT (ICCH_TASK_ID, ICCH_PKT_FROM_APP) != OSIX_SUCCESS)
        {
            ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                      "SendEvent failed in IcchSendMsg\r\n");
        }
        return (ICCH_FAILURE);
    }
    if (i4WrittenBytes != (INT4) u4TotLen)
    {
        /* Partial data handling */
        IcchAddSyncMsgIntoTxList (u1SsnIndex, pu1Data, u4TotLen,
                                  (UINT4) i4WrittenBytes);
        ICCH_TRC1 (ICCH_SYNC_MSG_TRC,
                   "IcchSendMsg: " "TCP socket send is successful with "
                   "partial data and size=%d\r\n", i4WrittenBytes);
        IcchTcpWriteCallBackFn (i4ConnFd);
        return (ICCH_SUCCESS);
    }
    else
    {
        ICCH_INCR_TX_STAT_SYNC_MSG_SENT ();
    }
    if (gIcchInfo.bIsTxBuffFull == ICCH_TRUE)
    {
        gIcchInfo.bIsTxBuffFull = ICCH_FALSE;
    }
    /* Free the memory allocated for linear buf */
    MemReleaseMemBlock (gIcchInfo.IcchPktMsgPoolId, (UINT1 *) pu1Data);
    return (ICCH_SUCCESS);
}

/******************************************************************************
 * Function           : IcchSockWritableHandle
 * Input(s)           : i4ConnFd -> Connection identifier
 * Output(s)          : None
 * Returns            : None
 * Action             : Routines writes the TX list messages into TCP
 *                      socket send buffer.
 ******************************************************************************/
VOID
IcchSockWritableHandle (INT4 i4ConnFd)
{
    tIcchTxBufNode     *pIcchTxBufNode = NULL;
    UINT1              *pCurTxBuf = NULL;
    UINT1              *pResBuf = NULL;
    UINT4               u4CurTxBufSize = 0;
    UINT4               u4RetVal = ICCH_SUCCESS;
    UINT4               u4RelinquishCntr = 0;
    INT4                i4ConnState = 0;
    INT4                i4WrittenBytes = 0;
    UINT1               u1SsnIndex = 0;

    if (IcchGetSsnIndexFromConnFd (i4ConnFd, &u1SsnIndex) == ICCH_FAILURE)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                  "IcchSockWritableHandle: Session doesnot "
                  "exist for the connection fd\r\n");
        return;
    }
    if (ICCH_GET_SSN_INFO (u1SsnIndex).i1ConnStatus
        == ICCH_SOCK_CONNECT_IN_PROGRESS)
    {
        if (IcchTcpSockConnect (ICCH_GET_SSN_INFO (u1SsnIndex).u4PeerAddr,
                                &i4ConnFd, &i4ConnState) == ICCH_FAILURE)
        {
            ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                      "IcchSockWritableHandle: Connection retry " "failed\r\n");
            /* If connection failed, donot clear the queue, instead wait for
             * the peer to connect, if connection lost with peer,
             * HB task will intimate about the peer dead
             * upon this event arrival clear the TX List */
            IcchAddSyncMsgFromQToTxList (u1SsnIndex);
            ICCH_GET_SSN_INFO (u1SsnIndex).i4ConnFd = ICCH_INV_SOCK_FD;
            ICCH_GET_SSN_INFO (u1SsnIndex).i1ConnStatus =
                ICCH_SOCK_NOT_CONNECTED;
            return;
        }
        if (i4ConnState == ICCH_SOCK_CONNECT_IN_PROGRESS)
        {
            ICCH_GET_SSN_INFO (u1SsnIndex).i4ConnFd = i4ConnFd;
            ICCH_GET_SSN_INFO (u1SsnIndex).i1ConnStatus =
                ICCH_SOCK_CONNECT_IN_PROGRESS;
            ICCH_TRC (ICCH_SYNC_EVT_TRC, "IcchSockWritableHandle: Socket is in "
                      "Connection In progress state\r\n");
            return;
        }
        /* This residual memory allocation only be freed in the
         * peer down event handle */
        if (ICCH_GET_SSN_INFO (u1SsnIndex).pResBuf == NULL)
        {
            if (IcchMemAllocForResBuf (&pResBuf) == ICCH_FAILURE)
            {
                ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                          "IcchSockWritableHandle: Residual buffer memory "
                          "allocation failed\r\n");
                IcchUtilCloseSockConn (u1SsnIndex);
                return;
            }
            ICCH_GET_SSN_INFO (u1SsnIndex).pResBuf = pResBuf;
        }
        ICCH_GET_SSN_INFO (u1SsnIndex).i4ConnFd = i4ConnFd;
        ICCH_GET_SSN_INFO (u1SsnIndex).i1ConnStatus = ICCH_SOCK_CONNECTED;
    }
    if (TMO_SLL_Count (&ICCH_SSN_ENTRY_TX_LIST (ICCH_GET_SSN_INFO (u1SsnIndex)))
        == 0)
    {
        ICCH_TRC (ICCH_SYNC_MSG_TRC, "IcchSockWritableHandle: "
                  "No data in the Tx List\r\n");
        return;
    }
    /* If connection status connection in progress */

    pIcchTxBufNode = (tIcchTxBufNode *) TMO_SLL_First
        (&ICCH_SSN_ENTRY_TX_LIST (ICCH_GET_SSN_INFO (u1SsnIndex)));
    while (pIcchTxBufNode)
    {
        u4RelinquishCntr++;
        if (u4RelinquishCntr > ICCH_RELINQUISH_CNTR)
        {
            if (ICCH_SEND_EVENT (ICCH_TASK_ID, ICCH_PKT_FROM_APP) ==
                OSIX_FAILURE)
            {
                ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                          "IcchProcessPktFromApp: Send Event "
                          "ICCH_PKT_FROM_APP Failed\r\n");
            }
            break;
        }

        u4CurTxBufSize = (ICCH_TX_MSG_BUF_NODE_MSG_SIZE (pIcchTxBufNode)) -
            (ICCH_TX_MSG_BUF_NODE_OFFSET (pIcchTxBufNode));
        pCurTxBuf = (ICCH_TX_MSG_BUF_NODE_MSG (pIcchTxBufNode)) +
            (ICCH_TX_MSG_BUF_NODE_OFFSET (pIcchTxBufNode));

        u4RetVal = IcchTcpSockSend (i4ConnFd, pCurTxBuf,
                                    (UINT2) u4CurTxBufSize, &i4WrittenBytes);
        if (u4RetVal == ICCH_FAILURE)
        {
            ICCH_TRC (ICCH_ALL_FAILURE_TRC, "IcchSendMsg: "
                      "Reliable sync message send failed\r\n");
            IcchUtilCloseSockConn (u1SsnIndex);
            if (ICCH_SEND_EVENT (ICCH_TASK_ID, ICCH_PKT_FROM_APP) !=
                OSIX_SUCCESS)
            {
                ICCH_TRC (ICCH_ALL_FAILURE_TRC, "SendEvent failed in "
                          "IcchSockWritableHandle\r\n");
            }
            return;
        }
        if (i4WrittenBytes != (UINT2) u4CurTxBufSize)
        {
            ICCH_TRC1 (ICCH_SYNC_MSG_TRC, "IcchSockWritableHandle: "
                       "Tx List partial sync data written size=%d\r\n",
                       i4WrittenBytes);
            ICCH_TX_MSG_BUF_NODE_OFFSET (pIcchTxBufNode) +=
                (UINT4) (i4WrittenBytes);
            IcchTcpWriteCallBackFn (i4ConnFd);
            return;
        }

        ICCH_INCR_TX_STAT_SYNC_MSG_SENT ();

        ICCH_TRC1 (ICCH_SYNC_MSG_TRC, "IcchSockWritableHandle: "
                   "Tx List complete sync data written size=%d\r\n",
                   i4WrittenBytes);
        TMO_SLL_Delete (&ICCH_SSN_ENTRY_TX_LIST
                        (ICCH_GET_SSN_INFO (u1SsnIndex)),
                        &pIcchTxBufNode->TxNode);
        MemReleaseMemBlock (gIcchInfo.IcchPktMsgPoolId,
                            (UINT1
                             *) (ICCH_TX_MSG_BUF_NODE_MSG (pIcchTxBufNode)));
        if (MemReleaseMemBlock
            (gIcchInfo.IcchTxBufMemPoolId,
             (UINT1 *) pIcchTxBufNode) != MEM_SUCCESS)
        {
            ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                      "IcchCtrlMsgHandler: Mem release from ctrl msg "
                      "pool failed\r\n");
        }
        pIcchTxBufNode = (tIcchTxBufNode *) TMO_SLL_First
            (&ICCH_SSN_ENTRY_TX_LIST (ICCH_GET_SSN_INFO (u1SsnIndex)));
    }
    if (gIcchInfo.bIsTxBuffFull == ICCH_TRUE)
    {
        gIcchInfo.bIsTxBuffFull = ICCH_FALSE;
    }
}
