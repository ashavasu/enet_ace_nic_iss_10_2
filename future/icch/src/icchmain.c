/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: icchmain.c,v 1.26 2017/12/22 10:33:11 siva Exp $
*
* Description:  Inter Chassis Communication Handler functionality .
***********************************************************************/
#ifndef _ICCHMAIN_C_
#define _ICCHMAIN_C_

#include "icchincs.h"
#include "fssocket.h"

/******************************************************************************
 * Function           : IcchTaskMain
 * Input(s)           : Input arguments.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : ICCH Task main routine to initialize ICCH task parameters,
 *                      main loop processing.
 ******************************************************************************/
VOID
IcchTaskMain (INT1 *pArg)
{
    UINT4               u4Event = 0;
    UINT4               u4PeerIpAddr = 0;
    INT4                i4CliSockFd = ICCH_INV_SOCK_FD;

    UNUSED_PARAM (pArg);

    if (OsixTskIdSelf (&ICCH_TASK_ID) != OSIX_SUCCESS)
    {
        ICCH_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (IcchTaskInit () == ICCH_FAILURE)
    {
        /* Indicate the status of initialization to main routine */
        ICCH_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Indicate the status of initialization to main routine */
    ICCH_INIT_COMPLETE (OSIX_SUCCESS);

#ifdef SNMP_2_WANTED
    /* Register MIBs if there are any */
    RegisterFSICCH ();
#endif /* SNMP_2_WANTED */

    while (1)
    {
        ICCH_RECV_EVENT (ICCH_TASK_ID, ICCH_MODULE_EVENTS, OSIX_WAIT, &u4Event);

        ICCH_LOCK ();
        if (u4Event & ICCH_CTRL_QMSG_EVENT)
        {
            /* Control Queue messages handling */
            ICCH_TRC (ICCH_NOTIF_TRC, "IcchTaskMain: ICCH_CTRL_QMSG_EVENT\r\n");
            IcchCtrlMsgHandler ();
        }

        if (u4Event & ICCH_TCP_NEW_CONN_RCVD)
        {
            ICCH_TRC (ICCH_NOTIF_TRC,
                      "IcchTaskMain: ICCH_TCP_NEW_CONN_RCVD\r\n");
            /* New Connection accept */
            if (IcchTcpAcceptNewConnection (ICCH_TCP_SRV_SOCK_FD (),
                                            &i4CliSockFd, &u4PeerIpAddr)
                == ICCH_SUCCESS)
            {
                IcchTcpUpdateSsnTable (i4CliSockFd, u4PeerIpAddr);
            }
        }

        if (u4Event & ICCH_PKT_FROM_APP)
        {
            /* Handle pkts from applications */
            ICCH_TRC (ICCH_NOTIF_TRC, "IcchTaskMain: ICCH_PKT_FROM_APP\r\n");

            IcchProcessPktFromApp ();
        }

        if (u4Event & ICCH_PROCS_RX_BUFF_SELF_EVENT)
        {
            /* This will help to resume the rx buffer processing */
            ICCH_TRC (ICCH_NOTIF_TRC,
                      "IcchTaskMain: ICCH_PROCS_RX_BUFF_SELF_EVENT\r\n");
            IcchRxProcessBufferedPkt ();
        }

        if (u4Event & ICCH_TMR_EXPIRY_EVENT)
        {
            /* ICCH Timers expiry handling */
            ICCH_TRC (ICCH_NOTIF_TRC,
                      "IcchTaskMain: ICCH_TMR_EXPIRY_EVENT\r\n");
            IcchTmrExpHandler ();
        }

        if (u4Event & ICCH_INIT_BULK_REQUEST)
        {
            ICCH_TRC (ICCH_NOTIF_TRC,
                      "IcchTaskMain: ICCH_INIT_BULK_REQUEST\r\n");
            IcchHandleInitBulkUpdate ();
        }

        if (u4Event & ICCH_GO_MASTER)
        {
            /* ICCH node status MASTER handle */
            ICCH_TRC (ICCH_NOTIF_TRC, "IcchTaskMain: ICCH_GO_MASTER\r\n");
            IcchHandleNodeStateUpdate (ICCH_MASTER);
        }

        if (u4Event & ICCH_GO_SLAVE)
        {
            /* ICCH node status SLAVE handle */
            ICCH_TRC (ICCH_NOTIF_TRC, "IcchTaskMain: ICCH_GO_SLAVE\r\n");
            IcchHandleNodeStateUpdate (ICCH_SLAVE);
        }

        if (u4Event & ICCH_GO_INIT)
        {
            /* ICCH node status MASTER handle */
            ICCH_TRC (ICCH_NOTIF_TRC, "IcchTaskMain: ICCH_GO_INIT\r\n");
            IcchHandleNodeStateUpdate (ICCH_INIT);
        }

        if (u4Event & ICCH_PROCESS_PENDING_SYNC_MSG)
        {
            /* ICCH to process all the pending RX buffer messages */
            ICCH_TRC (ICCH_SYNC_EVT_TRC,
                      "IcchTaskMain: ICCH_PROCESS_PENDING_SYNC_MSG\r\n");
            IcchPrcsPendingRxBufMsgs ();
        }

        if (u4Event & ICCH_MCLAG_ENABLED)
        {

            ICCH_TRC (ICCH_SYNC_EVT_TRC,
                      "IcchTaskMain: ICCH_MCLAG_ENABLED\r\n");

            IcchNotifyMCLAGStatus (MCLAG_ENABLED);
        }
        if (u4Event & ICCH_MCLAG_DISABLED)
        {
            ICCH_TRC (ICCH_SYNC_EVT_TRC,
                      "IcchTaskMain: ICCH_MCLAG_DISABLED\r\n");
            IcchNotifyMCLAGStatus (MCLAG_DISABLED);
        }

        ICCH_UNLOCK ();
    }
    return;
}

/******************************************************************************
 * Function           : IcchTaskInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : ICCH_FAILURE/ICCH_SUCCESS
 * Action             : Initialises the ICCH task related variables
 ******************************************************************************/
UINT4
IcchTaskInit (VOID)
{
    UINT4               u4RetVal = ICCH_SUCCESS;

    /* Initialize the default value of the global struct */
    if (IcchInit () == ICCH_FAILURE)
    {
        u4RetVal = ICCH_FAILURE;
    }

    if (u4RetVal != ICCH_FAILURE)
    {
        if (IcchTmrInit () == ICCH_FAILURE)
        {
            u4RetVal = ICCH_FAILURE;
        }
    }
    return (u4RetVal);
}

/******************************************************************************
 * Function           : IcchInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS/ICCH_FAILURE
 * Action             : Routine to initialize ICCH global data structure.
 ******************************************************************************/
UINT4
IcchInit (VOID)
{
    /* Dummy pointers for system sizing during run time */
    tIcchPktBlock      *pIcchPktBlock = NULL;
    UINT4               u4ConnIdx = 0;

    UNUSED_PARAM (pIcchPktBlock);

    gIcchInfo.i4SrvSockFd = ICCH_INV_SOCK_FD;
    gIcchInfo.u4IcchTrc = ICCH_CRITICAL_TRC;

    /* Initialize the Rx buffer related parameters */
    ICCH_IS_RX_BUFF_PROCESSING_ALLOWED () = ICCH_FALSE;
    ICCH_RX_SEQ_MISSMATCH_FLG () = ICCH_FALSE;
    ICCH_RX_LAST_SEQ_NUM_PROCESSED () = 0;
    ICCH_TX_LAST_SEQ_NUM_PROCESSED () = 0;
    ICCH_RX_BUFF_PKT_COUNT () = 0;
    ICCH_RX_LAST_APPID_PROCESSED () = ICCH_APP_ID;    /* This is an invalid value
                                                       for Rx buffer processing */
    /* Initialize the statistics counters */
    ICCH_TX_STAT_SYNC_MSG_SENT () = 0;
    ICCH_TX_STAT_SYNC_MSG_FAILED () = 0;
    ICCH_RX_STAT_SYNC_MSG_RECVD () = 0;
    ICCH_RX_STAT_SYNC_MSG_PROCESSED () = 0;
    ICCH_RX_STAT_SYNC_MSG_MISSED () = 0;

    /* Create the ICCH Buffer (hash Table) */
    if (IcchRxBufCreate () == ICCH_FAILURE)
    {
        IcchDeInit ();
        ICCH_TRC (ICCH_CRITICAL_TRC,
                  "RX buffer creation Failed in IcchInit\r\n");
        return ICCH_FAILURE;
    }

    /* Create a Q in ICCH to listen on the msgs sent by Applications.
     * ICCH recvs msgs from Apps and send it to the peer. */
    if (ICCH_CREATE_QUEUE ((UINT1 *) ICCH_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                           ICCH_PKT_Q_DEPTH, &(ICCH_QUEUE_ID)) != OSIX_SUCCESS)
    {
        IcchDeInit ();
        ICCH_TRC (ICCH_CRITICAL_TRC,
                  "Creation of ICCH Q Failed in IcchInit\r\n");
        return ICCH_FAILURE;
    }

    /*Create a queue for sending/receiving control messages */
    if (ICCH_CREATE_QUEUE ((UINT1 *) ICCH_CTRL_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                           MAX_ICCH_CTRL_MSG_NODES,
                           &(ICCH_CTRL_QUEUE_ID)) != OSIX_SUCCESS)
    {
        IcchDeInit ();
        ICCH_TRC (ICCH_ALL_FAILURE_TRC | ICCH_CRITICAL_TRC,
                  "Creation of ICCH Q Failed in IcchInit\r\n");
        return ICCH_FAILURE;
    }

    /*Create a semaphore for ICCH module events */
    if (OsixCreateSem (ICCH_PROTO_SEM_NAME, 1, 0,
                       (tOsixSemId *) & (ICCH_PROTO_SEM ())) != OSIX_SUCCESS)
    {
        IcchDeInit ();
        ICCH_TRC (ICCH_ALL_FAILURE_TRC | ICCH_CRITICAL_TRC,
                  "Icch Protocol Sem Creation failed in IcchInit\r\n");
        return ICCH_FAILURE;
    }

    if (IcchSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        IcchDeInit ();
        ICCH_TRC (ICCH_ALL_FAILURE_TRC | ICCH_CRITICAL_TRC,
                  "MemPool Creation for ICCH Peer table Pool Failed in IcchInit\r\n");
        return ICCH_FAILURE;
    }

    gIcchInfo.IcchCtrlMsgPoolId =
        IcchMemPoolIds[MAX_ICCH_CTRL_MSG_NODES_SIZING_ID];
    gIcchInfo.IcchPktQMsgPoolId = IcchMemPoolIds[MAX_ICCH_PKTQ_MSG_SIZING_ID];
    gIcchInfo.IcchTxBufMemPoolId =
        IcchMemPoolIds[MAX_ICCH_TX_BUF_NODES_SIZING_ID];
    gIcchInfo.IcchRxBufPoolId = IcchMemPoolIds[MAX_ICCH_RX_BUF_NODES_SIZING_ID];
    gIcchInfo.IcchRxBufPktPoolId =
        IcchMemPoolIds[MAX_ICCH_RX_BUF_PKT_NODES_SIZING_ID];
    gIcchInfo.IcchPktMsgPoolId =
        IcchMemPoolIds[MAX_ICCH_PKT_MSG_BLOCKS_SIZING_ID];
    gIcchInfo.IcchRegMemPoolId = IcchMemPoolIds[MAX_ICCH_APPS_SIZING_ID];
    gIcchInfo.IcchMsgPoolId =
        IcchMemPoolIds[MAX_ICCH_NODE_INFO_BLOCKS_SIZING_ID];
    gIcchInfo.u4EnableProtoSync = ICCH_BLOCK_ALL_SYNC;

    /* Read Iccl.conf */
    if (OSIX_FAILURE == IcchLoadConfsFromConfFile ())
    {
        IcchDeInit ();
        ICCH_TRC (ICCH_CRITICAL_TRC, "iccl.conf file read failed\r\n");
        return ICCH_FAILURE;
    }

    IcchHbRegisterApps ();

    gIcchInfo.i4StatsEnable = ICCH_STATS_ENABLE;
    gIcchInfo.i4ClearStats = ICCH_CLEAR_STATS_FALSE;
    gIcchInfo.i4OverrideLocalAffinity = ICCH_TRUE;
    gu4IcchBulkUpdtReqNotSent = ICCH_FALSE;
    for (u4ConnIdx = 0; u4ConnIdx < ICCH_MAX_SYNC_SSNS_LIMIT; u4ConnIdx++)
    {
        ICCH_GET_SSN_INFO (u4ConnIdx).pResBuf = NULL;
        ICCH_GET_SSN_INFO (u4ConnIdx).i4ConnFd = ICCH_INV_SOCK_FD;
        ICCH_GET_SSN_INFO (u4ConnIdx).u4PeerAddr = 0;
        ICCH_GET_SSN_INFO (u4ConnIdx).u2ResBufLen = 0;
        ICCH_GET_SSN_INFO (u4ConnIdx).i1ConnStatus = ICCH_SOCK_NOT_CONNECTED;
        ICCH_SLL_INIT (&ICCH_SSN_ENTRY_TX_LIST (ICCH_GET_SSN_INFO (u4ConnIdx)));
    }

    return ICCH_SUCCESS;
}

/******************************************************************************
 * Function           : IcchDeInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS/ICCH_FAILURE
 * Action             : Routine to deinitialize ICCH global data structure.
 ******************************************************************************/
VOID
IcchDeInit (VOID)
{
    /*Delete all the Memory pools created */
    IcchSizingMemDeleteMemPools ();

    /*Delete all the queues which are created during init */
    if (ICCH_CTRL_QUEUE_ID != NULL)
    {
        ICCH_DELETE_QUEUE (ICCH_CTRL_QUEUE_ID);
        ICCH_CTRL_QUEUE_ID = NULL;
    }

    /*Delete all the semaphores which are created during init */
    if (ICCH_PROTO_SEM () != NULL)
    {
        ICCH_DELETE_SEMAPHORE (ICCH_PROTO_SEM ());
        ICCH_PROTO_SEM () = NULL;
    }
    IcchTmrDeInit ();
}

/******************************************************************************
 * Function           : IcchPeerInfoMsgHandle
 * Input(s)           : pIcchPeerCtrlMsg - Peer control message (Peer up/ Peer down)
 * Output(s)          : None
 * Returns            : None
 * Action             : Provides the peer id and the status (UP/DOWN)
 ******************************************************************************/
VOID
IcchPeerInfoMsgHandle (tIcchPeerCtrlMsg * pIcchPeerCtrlMsg)
{
    tIcchTxBufNode     *pIcchTxBufNode = NULL;
    tUtlInAddr          HbNodeId;
    tIcchProtoEvt       ProtoEvt;
    UINT1               u1SsnCnt = 0;

    MEMSET (&HbNodeId, 0, sizeof (tUtlInAddr));
    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));

    HbNodeId.u4Addr = OSIX_NTOHL (pIcchPeerCtrlMsg->u4PeerId);
    for (u1SsnCnt = 0; u1SsnCnt < ICCH_MAX_SYNC_SSNS_LIMIT; u1SsnCnt++)
    {
        if (pIcchPeerCtrlMsg->u1PeerState == ICCH_PEER_UP)
        {
            ICCH_TRC1 (ICCH_NOTIF_TRC,
                       "IcchPeerInfoMsgHandle: "
                       "Peer UP is received for the peer=%s\r\n",
                       INET_NTOA (HbNodeId));
            /* Session table initializations are required only when
             * connection status is ICCH_SOCK_NOT_CONNECTED, otherwise while
             * accepting the connection initialization will be done */
            if (ICCH_GET_SSN_INFO (u1SsnCnt).i1ConnStatus
                == ICCH_SOCK_NOT_CONNECTED)
            {
                ICCH_TRC (ICCH_NOTIF_TRC,
                          "IcchPeerInfoMsgHandle: New peer received \r\n");
                ICCH_SLL_INIT (&
                               (ICCH_SSN_ENTRY_TX_LIST
                                (ICCH_GET_SSN_INFO (u1SsnCnt))));
                ICCH_GET_SSN_INFO (u1SsnCnt).u4PeerAddr =
                    pIcchPeerCtrlMsg->u4PeerId;
                ICCH_SET_PEER_NODE_ID (pIcchPeerCtrlMsg->u4PeerId);
                ICCH_PEER_NODE_COUNT ()++;
            }
            ICCH_SET_PEER_NODE_STATE (ICCH_PEER_UP);
            if ((gu4IcchBulkUpdtReqNotSent == ICCH_TRUE) &&
                (ICCH_GET_NODE_STATE () == ICCH_SLAVE))
            {
                ICCH_TRC (ICCH_NOTIF_TRC,
                          "Initiate bulk request trigger after sending GO_SLAVE"
                          " to protocols\r\n");
                ProtoEvt.u4AppId = ICCH_VLAN_APP_ID;
                ProtoEvt.u4Error = ICCH_NONE;
                ProtoEvt.u4Event = ICCH_INITIATE_BULK_UPDATE;
                ICCH_UNLOCK ();
                IcchHandleProtocolEvent (&ProtoEvt);
                ICCH_LOCK ();
                gu4IcchBulkUpdtReqNotSent = ICCH_FALSE;
            }
            IcchTrapSendNotifications (ICCH_PEER_STATUS_CHANGE_TRAP);

            IcchUpdatePeerNodeCntToProtocols (ICCH_PEER_UP);
        }
        else if (pIcchPeerCtrlMsg->u1PeerState == ICCH_PEER_DOWN)
        {
            ICCH_TRC1 (ICCH_NOTIF_TRC,
                       "IcchPeerInfoMsgHandle: "
                       "Peer DOWN is received for the peer=%s\r\n",
                       INET_NTOA (HbNodeId));
            if (gu4IcchBulkUpdtInProgress == ICCH_TRUE)
            {
                IcchTmrStopTimer (ICCH_BULK_UPDATE_TIMER);
                gu4IcchBulkUpdtInProgress = ICCH_FALSE;
            }
            if ((ICCH_GET_SSN_INFO (u1SsnCnt).u4PeerAddr) !=
                (pIcchPeerCtrlMsg->u4PeerId))
            {
                continue;
            }
            gIcchInfo.bIsTxBuffFull = ICCH_FALSE;
            gIcchInfo.bIsRxBuffFull = ICCH_FALSE;
            ICCH_SET_PEER_NODE_ID (0);
            if (ICCH_SLL_COUNT
                (&ICCH_SSN_ENTRY_TX_LIST (ICCH_GET_SSN_INFO (u1SsnCnt))) != 0)
            {
                pIcchTxBufNode = (tIcchTxBufNode *) TMO_SLL_First
                    (&(ICCH_SSN_ENTRY_TX_LIST (ICCH_GET_SSN_INFO (u1SsnCnt))));
                while (pIcchTxBufNode != NULL)
                {
                    TMO_SLL_Delete (&
                                    (ICCH_SSN_ENTRY_TX_LIST
                                     (ICCH_GET_SSN_INFO (u1SsnCnt))),
                                    &pIcchTxBufNode->TxNode);
                    MemReleaseMemBlock (gIcchInfo.IcchPktMsgPoolId,
                                        (UINT1 *) pIcchTxBufNode->pu1Msg);
                    if (MemReleaseMemBlock
                        (gIcchInfo.IcchTxBufMemPoolId,
                         (UINT1 *) pIcchTxBufNode) != MEM_SUCCESS)
                    {
                        ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                                  "IcchPeerInfoMsgHandle: Mem release from ctrl msg "
                                  "pool failed\r\n");
                    }
                    pIcchTxBufNode = (tIcchTxBufNode *) TMO_SLL_First
                        (&
                         (ICCH_SSN_ENTRY_TX_LIST
                          (ICCH_GET_SSN_INFO (u1SsnCnt))));
                }
                SelRemoveWrFd (ICCH_GET_SSN_INFO (u1SsnCnt).i4ConnFd);
            }
            if (ICCH_GET_SSN_INFO (u1SsnCnt).pResBuf != NULL)
            {
                MemReleaseMemBlock (gIcchInfo.IcchPktMsgPoolId,
                                    (UINT1 *) ICCH_GET_SSN_INFO (u1SsnCnt).
                                    pResBuf);
                ICCH_GET_SSN_INFO (u1SsnCnt).pResBuf = NULL;
                ICCH_GET_SSN_INFO (u1SsnCnt).u2ResBufLen = 0;
            }

            IcchUtilCloseSockConn (u1SsnCnt);
            ICCH_GET_SSN_INFO (u1SsnCnt).u4PeerAddr = 0;
            ICCH_SLL_INIT (&ICCH_SSN_ENTRY_TX_LIST
                           (ICCH_GET_SSN_INFO (u1SsnCnt)));
            ICCH_PEER_NODE_COUNT ()--;
            ICCH_SET_PEER_NODE_STATE (ICCH_PEER_DOWN);
            IcchTrapSendNotifications (ICCH_PEER_STATUS_CHANGE_TRAP);
            IcchUpdatePeerNodeCntToProtocols (ICCH_PEER_DOWN);
            IcchTmrStopTimer (ICCH_SEQ_RECOV_TIMER);
            IcchTmrStopTimer (ICCH_ACK_RECOV_TIMER);
            ICCH_IS_RX_BUFF_PROCESSING_ALLOWED () = ICCH_TRUE;
        }
    }                            /* End of for */
}

/******************************************************************************
 * Function           : IcchUpdatePeerNodeCntToProtocols
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : Updates the peer up/peer down status to protocols.
 ******************************************************************************/
VOID
IcchUpdatePeerNodeCntToProtocols (UINT1 u1Event)
{
    UINT4               u4EntId = 0;
    tIcchNodeInfo      *pIcchNodeInfo[ICCH_MAX_APPS];

    for (u4EntId = ICCH_LA_APP_ID; u4EntId < ICCH_MAX_APPS; u4EntId++)
    {
        if ((gIcchInfo.apIcchAppInfo[u4EntId] != NULL) &&
            (gIcchInfo.apIcchAppInfo[u4EntId]->pFnRcvPkt != NULL) &&
            (gIcchInfo.apIcchAppInfo[u4EntId]->u4ValidEntry == TRUE))
        {
            if (MemAllocateMemBlock
                (gIcchInfo.IcchMsgPoolId, (UINT1 **) &(pIcchNodeInfo[u4EntId]))
                != MEM_SUCCESS)
            {
                ICCH_TRC1 (ICCH_CRITICAL_TRC,
                           "Unable to allocate memory for "
                           "peer node state (up/down) updation to protocol %d\r\n",
                           u4EntId);
                continue;
            }

            MEMSET (pIcchNodeInfo[u4EntId], 0, sizeof (tIcchNodeInfo));

            if (IcchUtilPostDataOrEvntToProtocol (u4EntId, u1Event,
                                                  (tIcchMsg *) (VOID *)
                                                  pIcchNodeInfo[u4EntId],
                                                  ICCH_NODE_COUNT_MSG_SIZE,
                                                  0) == ICCH_FAILURE)
            {
                if (MemReleaseMemBlock
                    (gIcchInfo.IcchMsgPoolId,
                     (UINT1 *) (pIcchNodeInfo[u4EntId])) != MEM_SUCCESS)
                {
                    ICCH_TRC1 (ICCH_CRITICAL_TRC,
                               "Mem block release failed for AppId %d\r\n",
                               u4EntId);
                    return;
                }
            }
        }
    }
    return;
}

/******************************************************************************
 * Function Name      : IcchAllocTxBuf
 *
 * Description        : This is the API function used by external modules
                        to allocate memory for sync message
 * Input(s)           : sync message size
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Sync message buffer
 *****************************************************************************/
tIcchMsg           *
IcchAllocTxBuf (UINT4 u4SyncMsgSize)
{
    tIcchMsg           *pMsg = NULL;

    if (gIcchInfo.bIsTxBuffFull == ICCH_TRUE)
    {
        ICCH_TRC1 (ICCH_CRITICAL_TRC, "IcchAllocTxBuf: Buffer allocation "
                   "for sync msg Tx is not allowed because ICCH Tx is not ready "
                   "and Tx buffer threshold (%d * 0.75) is reached \r\n",
                   MAX_ICCH_TX_BUF_NODES);
        return NULL;
    }
    pMsg =
        CRU_BUF_Allocate_MsgBufChain ((u4SyncMsgSize + ICCH_HDR_LENGTH),
                                      ICCH_HDR_LENGTH);

    return pMsg;
}

#endif /*_ICCHMAIN_C*/
