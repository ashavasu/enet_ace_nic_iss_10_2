/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: icchapi.c,v 1.28.2.1 2018/03/16 13:43:46 siva Exp $
*
* Description: ICCH APIs to interface with applications.
***********************************************************************/
#include "icchincs.h"

/*****************************************************************************/
/* Function Name      : IcchApiSendHbEvtToIcch                               */
/*                                                                           */
/* Description        : This is an exported API to interact with Heart Beat  */
/*                      Module. This API should be used appropriately when   */
/*                      External ICCH is defined.                            */
/*                                                                           */
/* Input(s)           : pIcchHbMsg - Pointer to an ICCH HB message           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ICCH_SUCCESS or ICCH_FAILURE                         */
/*****************************************************************************/
UINT4
IcchApiSendHbEvtToIcch (tIcchHbMsg * pIcchHbMsg)
{
    tIcchCtrlQMsg      *pIcchCtrlQMsg = NULL;
    UINT4               u4Event = 0;

    /* Events processed by this API *
     * 1. GO_MASTER -> GO_MASTER notification to ICCH core module 
     * 2. GO_SLAVE -> GO_SLAVE notification to ICCH core module
     * 3. ICCH_PEER_UP -> Peer node up notification
     * 4. ICCH_PEER_DOWN -> Peer node down notification
     */

    switch (pIcchHbMsg->u4Evt)
    {
        case GO_MASTER:
            u4Event = ICCH_GO_MASTER;
            ICCH_TRC (ICCH_NOTIF_TRC, "GO_MASTER event triggered from HB "
                      "to ICCH \r\n");
            break;
        case GO_SLAVE:
            u4Event = ICCH_GO_SLAVE;
            ICCH_TRC (ICCH_NOTIF_TRC, "GO_SLAVE event triggered from HB "
                      "to ICCH \r\n");
            break;
        case GO_INIT:
            u4Event = ICCH_GO_INIT;
            ICCH_TRC (ICCH_NOTIF_TRC, "GO_INIT event triggered from HB "
                      "to ICCH \r\n");
            break;

            /* Intentional fall through */
        case ICCH_PEER_UP:
        case ICCH_PEER_DOWN:
            if ((pIcchCtrlQMsg = (tIcchCtrlQMsg *) (MemAllocMemBlk
                                                    (gIcchInfo.
                                                     IcchCtrlMsgPoolId))) ==
                NULL)
            {
                ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                          "IcchApiSendHbEvtToIcch: Memory allocation failed for "
                          "Control Q msg\r\n");
                return ICCH_FAILURE;
            }

            MEMSET (pIcchCtrlQMsg, 0, sizeof (tIcchCtrlQMsg));

            pIcchCtrlQMsg->u4MsgType = ICCH_PEER_INFO_MSG;
            pIcchCtrlQMsg->PeerCtrlMsg.u4PeerId = pIcchHbMsg->u4PeerAddr;
            pIcchCtrlQMsg->PeerCtrlMsg.u1PeerState = (UINT1) pIcchHbMsg->u4Evt;
            if (IcchQueEnqCtrlMsg (pIcchCtrlQMsg) == ICCH_FAILURE)
            {
                ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                          "IcchApiSendHbEvtToIcch: Send to Control Q failed\r\n");
                return ICCH_FAILURE;
            }
            return ICCH_SUCCESS;

        case ICCH_PROC_PENDING_SYNC_MSG:
            u4Event = ICCH_PROCESS_PENDING_SYNC_MSG;
            break;
        default:
            return ICCH_FAILURE;
    }                            /* End of switch */

    if (ICCH_SEND_EVENT (ICCH_TASK_ID, u4Event) == OSIX_FAILURE)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                  "IcchApiSendHbEvtToIcch: Sending event to ICCH from HB failed\r\n");
        return ICCH_FAILURE;
    }
    return ICCH_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IcchRegisterProtcols                                 */
/*                                                                           */
/* Description        : Routine used by protocols to register with ICCH      */
/*                                                                           */
/* Input(s)           : pIcchReg - Reg. params to be provided by protocols   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gIcchInfo.apIcchAppInfo                              */
/*                                                                           */
/* Return Value(s)    : ICCH_SUCCESS or ICCH_FAILURE                         */
/*****************************************************************************/
UINT4
IcchRegisterProtocols (tIcchRegParams * pIcchReg)
{
    UINT4               u4SrcEntId = 0;

    if (pIcchReg == NULL)
    {
        ICCH_TRC (ICCH_CRITICAL_TRC,
                  "Invalid Registration params from application\r\n");
        return ICCH_FAILURE;
    }

    /* Validate the AppId given by application */
    u4SrcEntId = pIcchReg->u4EntId;
    if ((u4SrcEntId == 0) || (u4SrcEntId >= ICCH_MAX_APPS))
    {
        ICCH_TRC (ICCH_CRITICAL_TRC,
                  "Invalid application id during registration\r\n");
        return ICCH_FAILURE;
    }

    ICCH_LOCK ();

    /* Allocate memory for the new entry */
    if (MemAllocateMemBlock
        (gIcchInfo.IcchRegMemPoolId,
         (UINT1 **) &(gIcchInfo.apIcchAppInfo[u4SrcEntId])) != MEM_SUCCESS)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC | ICCH_CRITICAL_TRC,
                  "Unable to allocate memory for the new registration entry\r\n");
        ICCH_UNLOCK ();
        return ICCH_FAILURE;
    }

    MEMSET (gIcchInfo.apIcchAppInfo[u4SrcEntId], 0, sizeof (tIcchAppInfo));

    /* Update the application specific info */
    gIcchInfo.apIcchAppInfo[u4SrcEntId]->pFnRcvPkt = pIcchReg->pFnRcvPkt;
    gIcchInfo.apIcchAppInfo[u4SrcEntId]->u4SrcEntId = u4SrcEntId;
    gIcchInfo.apIcchAppInfo[u4SrcEntId]->u4DestEntId = u4SrcEntId;
    gIcchInfo.apIcchAppInfo[u4SrcEntId]->u4ValidEntry = TRUE;
    OSIX_BITLIST_SET_BIT (gIcchInfo.IcchRegMask, pIcchReg->u4EntId,
                          sizeof (tIcchAppList));

    ICCH_UNLOCK ();
    return (ICCH_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : IcchDeRegisterProtcols                               */
/*                                                                           */
/* Description        : Routine used by protocols to de-register with ICCH   */
/*                                                                           */
/* Input(s)           : u4SrcEntId - Appl Id of the applicantion             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gIcchInfo.apIcchAppInfo                              */
/*                                                                           */
/* Return Value(s)    : ICCH_SUCCESS or ICCH_FAILURE                         */
/*****************************************************************************/
UINT4
IcchDeRegisterProtocols (UINT4 u4SrcEntId)
{
    if (gIcchInfo.apIcchAppInfo[u4SrcEntId] == NULL)
    {
        ICCH_TRC1 (ICCH_CRITICAL_TRC,
                   "IcchDeRegistration failed for AppId %d\r\n", u4SrcEntId);
        return ICCH_FAILURE;
    }

    ICCH_LOCK ();
    /* Free the memory of this entry */
    if (MemReleaseMemBlock
        (gIcchInfo.IcchRegMemPoolId,
         (UINT1 *) gIcchInfo.apIcchAppInfo[u4SrcEntId]) != MEM_SUCCESS)
    {
        ICCH_TRC1 (ICCH_ALL_FAILURE_TRC | ICCH_CRITICAL_TRC,
                   "DeReg: Mem block release failed for AppId %d\r\n",
                   u4SrcEntId);
        ICCH_UNLOCK ();
        return ICCH_FAILURE;
    }

    OSIX_BITLIST_RESET_BIT (gIcchInfo.IcchRegMask, u4SrcEntId,
                            sizeof (tIcchAppList));
    gIcchInfo.apIcchAppInfo[u4SrcEntId] = NULL;

    ICCH_UNLOCK ();
    return (ICCH_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : IcchEnqMsgToIcchFromAppl                             */
/*                                                                           */
/* Description        : Routine to enqueue the message from applications to  */
/*                      ICCH. Unique sequence number is automatically        */
/*                      assigned to all sync-up messages before enqueue      */
/*                      to ICCH pkt queue.                                   */
/*                                                                           */
/* Input(s)           : pIcchMsg - message from appl                         */
/*                      u2DataLen - Length of message                        */
/*                      u4SrcEntId - EntId from which the msg has arrived    */
/*                      u4DestEntId - EntId to which the msg has to be sent  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ICCH_SUCCESS or ICCH_FAILURE                         */
/*****************************************************************************/
UINT4
IcchEnqMsgToIcchFromAppl (tIcchMsg * pIcchMsg, UINT2 u2DataLen,
                          UINT4 u4SrcEntId, UINT4 u4DestEntId)
{
    tIcchPktQMsg       *pIcchPktQMg = NULL;
    tIcchHdr            IcchHdr;
    UINT4               u4SeqNum = 0;

    MEMSET (&IcchHdr, 0, sizeof (tIcchHdr));

    if ((ICCH_HDR_LENGTH + u2DataLen) > ICCH_MAX_SYNC_PKT_LEN)
    {
        ICCH_TRC2 (ICCH_CRITICAL_TRC, "IcchEnqMsgToIcchFromAppl: "
                   "ICCH data size is exceeded the limit, "
                   "Sync-up is dropped for =%s application "
                   "and the dropped size %u\r\n",
                   gaIcchAppName[u4DestEntId], u2DataLen);
        return ICCH_FAILURE;
    }

    if (ICCH_GET_PEER_NODE_ID == 0)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                  "IcchEnqMsgToIcchFromAppl: Peer is not available \r\n");
        return ICCH_FAILURE;
    }

    /* Form ICCH header */
    IcchHdr.u2Version = OSIX_HTONS (ICCH_VERSION_NUM);
    /* Actual checksum will be calculated after copying the 
     * CRU buf to linear buf before sending to peer */
    IcchHdr.u2Chksum = OSIX_HTONS (0);
    IcchHdr.u4TotLen = OSIX_HTONL ((ICCH_HDR_LENGTH + u2DataLen));

    /* u4MsgType - REQ/RESP - to be filled while implementing re-transmission */
    IcchHdr.u4MsgType = OSIX_HTONL (ICCH_UNUSED);

    IcchHdr.u4SrcEntId = OSIX_HTONL (u4SrcEntId);
    IcchHdr.u4DestEntId = OSIX_HTONL (u4DestEntId);

    u4SeqNum = ICCH_TX_GETNEXT_VALID_SEQNUM ();
    ICCH_TX_INCR_LAST_SEQNUM_PROCESSED ();

    IcchHdr.u4SeqNo = OSIX_HTONL (u4SeqNum);

    if (u4SeqNum == ICCH_SEQNUM_WRAP_AROUND_VALUE)
    {
        ICCH_TRC (ICCH_SYNC_MSG_TRC,
                  "IcchEnqMsgToIcchFromAppl: WRAP AROUND happened "
                  "for sequence numbers\r\n");
    }

    ICCH_TRC2 (ICCH_SYNC_MSG_TRC, "IcchEnqMsgToIcchFromAppl: "
               "Sync pkt to be sent [AppId=%s, Seq#=%d]\r\n",
               gaIcchAppName[u4SrcEntId], u4SeqNum);
    /* Prepend ICCH hdr to ICCH data */
    if ((ICCH_PREPEND_BUF (pIcchMsg, (UINT1 *) &IcchHdr, sizeof (tIcchHdr))) !=
        CRU_SUCCESS)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC, "Prepend ICCH hdr - failed\r\n");
        return ICCH_FAILURE;
    }

    if ((pIcchPktQMg =
         (tIcchPktQMsg *) (MemAllocMemBlk (gIcchInfo.IcchPktQMsgPoolId))) ==
        NULL)
    {
        ICCH_TRC (ICCH_CRITICAL_TRC | ICCH_ALL_FAILURE_TRC,
                  "IcchEnqMsgToIcchFromAppl"
                  "Memory Allocation Failed for enqueuing pkt to ICCH!! \r\n");
        return ICCH_FAILURE;
    }

    MEMSET (pIcchPktQMg, 0, sizeof (tIcchPktQMsg));

    pIcchPktQMg->u1MsgType = ICCH_SYNCUP_MSG;
    pIcchPktQMg->pIcchMsg = pIcchMsg;

    if (ICCH_SEND_TO_QUEUE (ICCH_QUEUE_ID,
                            (UINT1 *) &pIcchPktQMg,
                            ICCH_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (MemReleaseMemBlock
            (gIcchInfo.IcchPktQMsgPoolId, (UINT1 *) pIcchPktQMg) != MEM_SUCCESS)
        {
            ICCH_TRC1 (ICCH_CRITICAL_TRC | ICCH_ALL_FAILURE_TRC,
                       "IcchUtilReleaseMemForPktQMsg Mem block release failed for"
                       " block %u\r\n", pIcchPktQMg);
        }
        pIcchPktQMg = NULL;

        ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                  "SendToQ failed in IcchEnqMsgToIcchFromAppl\r\n");
        return ICCH_FAILURE;
    }

    /* ICCH lock should be released after posting the message to queue, since
     * during force switch over, there should not be a scenario where a seq
     * number is reserved but message is not posted to queue. This is because
     * ICCH processes all the pending queue messages before aplying force switch
     * over
     */

    if (ICCH_SEND_EVENT (ICCH_TASK_ID, ICCH_PKT_FROM_APP) != OSIX_SUCCESS)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                  "SendEvent failed in IcchEnqMsgToIcchFromAppl\r\n");
    }
    return (ICCH_SUCCESS);
}

/******************************************************************************
 * Function           : IcchApiHandleProtocolEvent
 * Input(s)           : pEvt->u4AppId - Module Id.
 *                      pEvt->u4Event   - Event send by protocols
 *                      (ICCH_PROTOCOL_BULK_UPDT_COMPLETION /
 *                      ICCH_INITIATE_BULK_UPDATE / ICCH_BULK_UPDT_ABORT / 
 *                      ICCH_SLAVE_TO_MASTER_EVT_PROCESSED / 
 *                      ICCH_IDLE_TO_MASTER_EVT_PROCESSED / 
 *                      ICCH_SLAVE_EVT_PROCESSED)
 *                      pEvt->u4Error   - Error code (ICCH_MEMALLOC_FAIL / 
 *                      ICCH_SENDTO_FAIL / ICCH_PROCESS_FAIL / ICCH_NONE)
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS / ICCH_FAILURE.
 * Action             : Routine used by protocols/applications to intimate ICCH 
 *                      about the protocol operations. ICCH decides upon the 
 *                      events and send appropriate notifications to the 
 *                      management application or events to the protocols. 
 ******************************************************************************/
UINT1
IcchApiHandleProtocolEvent (tIcchProtoEvt * pEvt)
{
    UINT1               u1RetVal = ICCH_SUCCESS;
    u1RetVal = IcchHandleProtocolEvent (pEvt);
    return u1RetVal;
}

/*****************************************************************************/
/* Function Name      : IcchSendEventToIcchTask                              */
/*                                                                           */
/* Description        : Other modules should call this routine to post any   */
/*                      event to ICCH task.                                  */
/*                                                                           */
/* Input(s)           : au1TakName - Name of the task to send the event      */
/*                                                                           */
/* Output(s)          : u4Event - Event to be send                           */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ICCH_SUCCESS or ICCH_FAILURE                         */
/*****************************************************************************/
UINT4
IcchSendEventToIcchTask (UINT4 u4Event)
{
    if (ICCH_TASK_ID == ICCH_INIT)
    {
        if (ICCH_GET_TASK_ID (ICCH_SELF, (const UINT1 *) ICCH_TASK_NAME,
                              &ICCH_TASK_ID) != OSIX_SUCCESS)
        {

            ICCH_TRC (ICCH_ALL_FAILURE_TRC, "Failed to get ICCH Task ID\r\n");
            return ICCH_FAILURE;
        }
    }

    if (ICCH_SEND_EVENT (ICCH_TASK_ID, u4Event) != OSIX_SUCCESS)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                  "Failed to Send Event to ICCH task \r\n");
        return ICCH_FAILURE;
    }
    return ICCH_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IcchSetBulkUpdatesStatus                             */
/*                                                                           */
/* Description        : This function is triggered by the registered         */
/*                      protocols to inform the status of bulk updates       */
/*                      completion. This function will be used only the      */
/*                      protocols in the master node                         */
/*                                                                           */
/* Input(s)           : u4AppId - Protocol Application ID                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ICCH_SUCCESS or ICCH_FAILURE                         */
/*****************************************************************************/
INT4
IcchSetBulkUpdatesStatus (UINT4 u4AppId)
{
    if ((u4AppId == ICCH_APP_ID) || (u4AppId >= ICCH_MAX_APPS))
    {
        return ICCH_FAILURE;
    }

    ICCH_LOCK ();

    OSIX_BITLIST_SET_BIT (gIcchInfo.IcchBulkUpdSts, u4AppId,
                          sizeof (tIcchAppList));

    ICCH_UNLOCK ();

    if (u4AppId == ICCH_ARP_APP_ID)
    {
        IcchUtilCheckAndCompleteBulkUpdt ();
    }
    ICCH_TRC1 (ICCH_NOTIF_TRC | ICCH_SYNC_EVT_TRC,
               "IcchSetBulkUpdatesStatus: "
               "Dynamic Bulk Update is completed for AppId=%s\r\n",
               gaIcchAppName[u4AppId]);

    return ICCH_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IcchGetBulkUpdatesStatus                             */
/*                                                                           */
/* Description        : This function will be used by the registered         */
/*                      protocols in master node to check whether bulk       */
/*                      updates mechanism is completed by that protocol.     */
/*                                                                           */
/* Input(s)           : u4AppId - Protocol Application ID                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ICCH_SUCCESS or ICCH_FAILURE                         */
/*                                                                           */
/* Called By          : Registered protocols with ICCHGR                     */
/*****************************************************************************/
INT4
IcchGetBulkUpdatesStatus (UINT4 u4AppId)
{
    BOOL1               bResult = OSIX_FALSE;

    if ((u4AppId == ICCH_APP_ID) || (u4AppId >= ICCH_MAX_APPS))
    {
        return ICCH_FAILURE;
    }

    ICCH_LOCK ();

    OSIX_BITLIST_IS_BIT_SET (gIcchInfo.IcchBulkUpdStsMask, u4AppId,
                             sizeof (tIcchAppList), bResult);
    if (bResult == OSIX_FALSE)
    {
        /* Bulk updates mechanism are not required for this protocol.
         * So we can treat it as successful completion.
         */
        ICCH_UNLOCK ();
        return ICCH_SUCCESS;
    }

    OSIX_BITLIST_IS_BIT_SET (gIcchInfo.IcchBulkUpdSts, u4AppId,
                             sizeof (tIcchAppList), bResult);
    if (bResult == OSIX_FALSE)
    {
        ICCH_UNLOCK ();
        return ICCH_FAILURE;
    }

    ICCH_UNLOCK ();
    return ICCH_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IcchGetNodeState                                     */
/*                                                                           */
/* Description        : Routine to get the node state of ICCH                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gIcchSessionInfo[].i4SessionNodeState                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Node state of ICCH                                   */
/*****************************************************************************/
UINT4
IcchGetNodeState (VOID)
{
    UINT4               u4IcchNodeState;

    ICCH_LOCK ();
    u4IcchNodeState = (UINT4) (ICCH_GET_NODE_STATE ());
    ICCH_UNLOCK ();
    return (u4IcchNodeState);
}

/*****************************************************************************/
/* Function Name      : IcchGetPeerNodeState                                 */
/*                                                                           */
/* Description        : Routine to get the peer node state of ICCH           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gIcchInfo.u4PeerNodeState                            */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Peer Node state of ICCH                              */
/*****************************************************************************/
UINT4
IcchGetPeerNodeState (VOID)
{
    UINT4               u4IcchPeerNodeState;

    ICCH_LOCK ();
    u4IcchPeerNodeState = ICCH_GET_PEER_NODE_STATE;
    ICCH_UNLOCK ();
    return (u4IcchPeerNodeState);
}

/*****************************************************************************/
/* Function Name      : IcchGetNodePrevState                                 */
/*                                                                           */
/* Description        : Routine to get the previous node state of ICCH       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gIcchInfo.u4PrevNodeState                            */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Previous Node state of ICCH                          */
/*****************************************************************************/
UINT4
IcchGetNodePrevState (VOID)
{
    UINT4               u4IcchNodeState;

    ICCH_LOCK ();
    u4IcchNodeState = ICCH_GET_PREV_NODE_STATE ();
    ICCH_UNLOCK ();
    return (u4IcchNodeState);
}

/******************************************************************************
 * Function           : IcchApiGetSeqNumForSyncMsg           
 * Input(s)           : None.                             
 * Output(s)          : pu4SeqNum - return value for the next valid 
 *                                  sequence number. In every call of this API
 *                                  will reserve a unique sequence number for
 *                                  the sync-up message and provide it to 
 *                                  protocol.
 * Returns            : None.
 * Action             : This function used by protocols/applications to 
 *                      reserves  a unique sequence number for the sync-up 
 *                      message. Once a sequence number is reserved protocol
 *                      can form the sync-up message with this number and
 *                      transfer it to slave node through ICCH. Once a sequence
 *                      number is reserved it cannot be de-allocated. So 
 *                      protocol should send a sync-up message with the
 *                      allocated sequence number. It can send a dummy 
 *                      sync-up message if required in case it is not able to
 *                      send a valid sync-up message.
 ******************************************************************************/
VOID
IcchApiGetSeqNumForSyncMsg (UINT4 *pu4SeqNum)
{

    *pu4SeqNum = 0;                /* If node status is not MASTER return seq number 0 */

    ICCH_LOCK ();
    if (gIcchInfo.bIsTxBuffFull == ICCH_TRUE)
    {
        ICCH_TRC1 (ICCH_CRITICAL_TRC,
                   "IcchApiGetSeqNumForSyncMsg: Buffer allocation "
                   "for sync msg Tx is not allowed because ICCH Tx is not ready "
                   "and Tx buffer threshold (%d * 0.75) is reached \r\n",
                   MAX_ICCH_TX_BUF_NODES);
        ICCH_UNLOCK ();
        return;
    }

    *pu4SeqNum = ICCH_TX_GETNEXT_VALID_SEQNUM ();
    ICCH_TX_INCR_LAST_SEQNUM_PROCESSED ();
    ICCH_TRC1 (ICCH_SYNC_MSG_TRC,
               "IcchApiGetSeqNumForSyncMsg: Seq# %d is reserved\r\n",
               *pu4SeqNum);

    if (*pu4SeqNum == ICCH_SEQNUM_WRAP_AROUND_VALUE)
    {
        ICCH_TRC (ICCH_SYNC_MSG_TRC,
                  "IcchEnqMsgToIcchFromAppl: WRAP AROUND happened "
                  "for sequence numbers\r\n");
    }
    ICCH_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : IcchApiSendProtoAckToICCH                            */
/*                                                                           */
/* Description        : This is the function used by protocols/applications  */
/*                      to send acknowledgement to ICCH after processing the */
/*                      sync-up message.                                     */
/*                                                                           */
/* Input(s)           : tIcchProtoAck contains                               */
/*                      u4AppId  - Protocol Identifier                       */
/*                      u4SeqNum - Sequence number of the ICCH message for   */
/*                                 which this ACK is generated               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ICCH_SUCCESS or ICCH_FAILURE                         */
/*****************************************************************************/
INT4
IcchApiSendProtoAckToICCH (tIcchProtoAck * pProtoAck)
{
    tIcchCtrlQMsg      *pIcchCtrlQMsg = NULL;

    if (pProtoAck->u4SeqNumber == 0)
    {
        ICCH_TRC1 (ICCH_ALL_FAILURE_TRC,
                   "IcchApiSendProtoAckToICCH: ICCH Receives Proto ACK from "
                   "[AppId=%s] with Seq#=0. Ignoring it\r\n",
                   gaIcchAppName[pProtoAck->u4AppId]);
        return ICCH_SUCCESS;
    }

    ICCH_TRC2 (ICCH_SYNC_MSG_TRC, "IcchApiSendProtoAckToICCH: "
               "Protocol=%s processed the seq# %d message\r\n",
               gaIcchAppName[pProtoAck->u4AppId], pProtoAck->u4SeqNumber);

    if ((pIcchCtrlQMsg = (tIcchCtrlQMsg *) (MemAllocMemBlk
                                            (gIcchInfo.IcchCtrlMsgPoolId))) ==
        NULL)
    {
        ICCH_TRC (ICCH_CRITICAL_TRC | ICCH_ALL_FAILURE_TRC,
                  "IcchApiSendProtoAckToICCH: Memory allocation for "
                  "Control Queue message.\r\n");
        return ICCH_FAILURE;
    }

    pIcchCtrlQMsg->u4MsgType = ICCH_PROTO_ACK_MSG;
    pIcchCtrlQMsg->ProtoAckCtrlMsg.u4AppId = pProtoAck->u4AppId;
    pIcchCtrlQMsg->ProtoAckCtrlMsg.u4SeqNumber = pProtoAck->u4SeqNumber;

    if (IcchQueEnqCtrlMsg (pIcchCtrlQMsg) == ICCH_FAILURE)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                  "IcchApiHandlePeerInfo: Send to Control Q failed\r\n");
        return ICCH_FAILURE;
    }

    return ICCH_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IcchApiInitiateBulkRequest                           */
/*                                                                           */
/* Description        : This API initiates the bulk update process in the    */
/*                      slave node to obtain the configurations from the     */
/*                      master node after all the protocols have restarted.  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ICCH_SUCCESS or ICCH_FAILURE                         */
/*****************************************************************************/
UINT4
IcchApiInitiateBulkRequest (VOID)
{
    if (ICCH_SEND_EVENT (ICCH_TASK_ID, ICCH_INIT_BULK_REQUEST) != OSIX_SUCCESS)
    {
        ICCH_TRC (ICCH_SYNC_EVT_TRC | ICCH_CRITICAL_TRC, "SendEvent failed in "
                  "IcchApiInitiateBulkUpdate\r\n");
        return ICCH_FAILURE;
    }

    return ICCH_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IcchApiAllocTxBuf                                    */
/*                                                                           */
/* Description        : This is the API function used by external modules to */
/*                      allocate memory for sync message sync message size   */
/*                                                                           */
/* Input(s)           : u4SyncMsgSize - sync message size                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Sync message buffer                                  */
/*****************************************************************************/
tIcchMsg           *
IcchApiAllocTxBuf (UINT4 u4SyncMsgSize)
{
    tIcchMsg           *pMsg = NULL;

    pMsg = IcchAllocTxBuf (u4SyncMsgSize);
    return pMsg;
}

/*****************************************************************************/
/* Function Name      : IcchIsBulkUpdateComplete                             */
/*                                                                           */
/* Description        : This API returns                                     */
/*                           ICCH_SUCCESS -  Bulk update Completed           */
/*                           ICCH_FAILURE -  Bulk update Not Completed       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4IcchBulkUpdtInProgress                            */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ICCH_SUCCESS or ICCH_FAILURE                         */
/*****************************************************************************/
INT4
IcchIsBulkUpdateComplete (VOID)
{
    if (gu4IcchBulkUpdtInProgress == ICCH_TRUE)
    {
        /* Bulk update is in progress */
        return ICCH_FAILURE;
    }
    return ICCH_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IcchApiTcpSrvSockInit                                */
/*                                                                           */
/* Description        : Routine to create a TCP server scoket, set socket    */
/*                      options for accepting the connections from peer.     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gIcchInfo.i4SrvSockFd                                */
/*                                                                           */
/* Return Value(s)    : ICCH_SUCCESS or ICCH_FAILURE                         */
/*****************************************************************************/
VOID
IcchApiTcpSrvSockInit (INT4 *pi4SrvSockFd)
{
    UINT4               u4RetVal = ICCH_FAILURE;
    INT4                i4SrvSockFd = 0;

    u4RetVal = (UINT4) (IcchTcpSrvSockInit (&i4SrvSockFd));
    if (u4RetVal == ICCH_SUCCESS)
    {
        ICCH_TCP_SRV_SOCK_FD () = i4SrvSockFd;
        *pi4SrvSockFd = i4SrvSockFd;
    }

    return;
}

/******************************************************************************
 * Function           : IcchSetTxEnable
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : Routine to enable/disable the Tx for ICCH
 ******************************************************************************/
VOID
IcchSetTxEnable (UINT4 u4Value)
{
    UNUSED_PARAM (u4Value);
}

/*****************************************************************************/
/* Function Name      : IcchReleaseMemoryForMsg                              */
/*                                                                           */
/* Description        : Routine used by the protocols to release the memory  */
/*                      allocatd by ICCH module                              */
/*                                                                           */
/* Input(s)           : u4SrcEntId - Source App Id                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ICCH_SUCCESS or ICCH_FAILURE                         */
/*****************************************************************************/
UINT4
IcchReleaseMemoryForMsg (UINT1 *pu1Block)
{
    /* Free the memory of this entry */
    if (MemReleaseMemBlock (gIcchInfo.IcchMsgPoolId, pu1Block) != MEM_SUCCESS)
    {
        ICCH_TRC1 (ICCH_CRITICAL_TRC,
                   "Mem block release failed for blck %u\r\n", pu1Block);
        return ICCH_FAILURE;
    }
    pu1Block = NULL;
    return ICCH_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IcchApiIsMcLagInterface                              */
/*                                                                           */
/* Description        : This function is used to check if the given input    */
/*                      interface corresponds to MC-LAG.                     */
/*                                                                           */
/* Input(s)           : u4OfIndex - Interface Index.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE or OSIX_FALSE                              */
/*                                                                           */
/*****************************************************************************/
INT1
IcchApiIsMcLagInterface (UINT4 u4IfIndex)
{
    /* To check if the given interface index corresponds to MC-LAG 
     * interface. 
     */
    UNUSED_PARAM (u4IfIndex);
    return OSIX_TRUE;
}

/*****************************************************************************/
/* Function Name      : IcchGetPeerNodeId                                    */
/*                                                                           */
/* Description        : This function is used to fetch the ICCH Peer         */
/*                      interface index.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Peer node address of ICCH                            */
/*                                                                           */
/*****************************************************************************/
UINT4
IcchApiGetPeerAddress (VOID)
{
    return (ICCH_GET_PEER_NODE_ID);
}

/*****************************************************************************/
/* Function Name      : IcchGetIcclIfIndex                                   */
/*                                                                           */
/* Description        : This function is used to fetch the ICCL interface    */
/*                      index.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : *pu4IfIndex - ICCL interface index.                  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
IcchGetIcclIfIndex (UINT4 *pu4IfIndex)
{
    *pu4IfIndex = gIcchInfo.u4IcclIfIndex;
    return;
}

/*****************************************************************************/
/* Function Name      : IcchGetIcclL3IfIndex                                 */
/*                                                                           */
/* Description        : This function is used to fetch the ICCL interface    */
/*                      index.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : *pu4IfIndex - ICCL interface index.                  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
IcchGetIcclL3IfIndex (UINT4 *pu4IfIndex)
{
    *pu4IfIndex = (UINT4) gIcchInfo.u4IcclL3IfIndex;
    return;
}

/*****************************************************************************/
/* Function Name      : IcchSetIcclL3IfIndex                                 */
/*                                                                           */
/* Description        : This function is used to fetch the ICCL interface    */
/*                      index.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex - L3 Interface Index                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
IcchSetIcclL3IfIndex (UINT4 u4IfIndex)
{
    gIcchInfo.u4IcclL3IfIndex = u4IfIndex;
    return;
}

/*****************************************************************************/
/* Function Name      : IcchApiGetRolePlayed                                 */
/*                                                                           */
/* Description        : This function is used to fetch the node state(MASTER */
/*                      or SLAVE) of MCLAG switch                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gIcchInfo.u4NodeState                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Node state of the switch                             */
/*                                                                           */
/*****************************************************************************/
UINT4
IcchApiGetRolePlayed (VOID)
{
    return (UINT4) (ICCH_GET_NODE_STATE ());
}

/*****************************************************************************/
/* Function Name      : IcchApiSyslogRegister                                */
/*                                                                           */
/* Description        : This function is used to register ICCH in syslog     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gIcchInfo.u4SysLogId                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ICCH_SUCCESS/ICCH_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
UINT4
IcchApiSyslogRegister (VOID)
{
    gIcchInfo.u4SysLogId =
        (UINT4) (SYS_LOG_REGISTER ((CONST UINT1 *) "ICCH", SYSLOG_DEBUG_LEVEL));
    if (gIcchInfo.u4SysLogId <= 0)
    {
        ICCH_TRC (ICCH_CRITICAL_TRC, "ICCH Syslog Init Failure\r\n");
        return ICCH_FAILURE;
    }
    return ICCH_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IcchSetIcclIfIndex                                   */
/*                                                                           */
/* Description        : This function is used to set the ICCL interface      */
/*                      index.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE or OSIX_FALSE                              */
/*                                                                           */
/*****************************************************************************/
INT1
IcchSetIcclIfIndex (UINT4 u4IfIndex)
{
    gIcchInfo.u4IcclIfIndex = u4IfIndex;

    /* If MCLAG is enabled in the system already, Add port isolation rule
     * for the iccl interface.
     */
    if (LaGetMCLAGSystemStatus () == ICCH_LA_MCLAG_ENABLED)
    {
        VlanAddIsolationTblForIccl ();
    }
    return OSIX_TRUE;
}

/*****************************************************************************/
/*  Function Name   : IcchApiGetIcclIpInterface                              */
/*  Description     : This Function fetches the ICCH IP interface            */
/*  Input(s)        : None                                                   */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ICCH interface  from iccl.conf            */
/*****************************************************************************/
CHR1               *
IcchApiGetIcclIpInterface (UINT2 u2Index)
{
    return gIcchSessionInfo[u2Index].ac1IcclInterface;
}

/*****************************************************************************/
/*  Function Name   : IcchApiGetIcclIpMask                                   */
/*  Description     : This Function fetches the ICCH IP Mask                 */
/*  Input(s)        : None                                                   */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ICCH interface  from iccl.conf            */
/*****************************************************************************/
UINT4
IcchApiGetIcclIpMask (UINT2 u2Index)
{
    return gIcchSessionInfo[u2Index].u4SubnetMask;
}

/*****************************************************************************/
/*  Function Name   : IcchApiGetIcclIpAddr                                   */
/*  Description     : This Function fetches the ICCH IP Address              */
/*  Input(s)        : None                                                   */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ICCH IP Addresss from iccl.conf           */
/*****************************************************************************/
UINT4
IcchApiGetIcclIpAddr (UINT2 u2Index)
{
    return gIcchSessionInfo[u2Index].u4IpAddress;
}

/*****************************************************************************/
/*  Function Name   : IcchApiGetIcclVlanId                                   */
/*  Description     : This Function fetches the ICCH Vlan Id                 */
/*  Input(s)        : None                                                   */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ICCH IP Vlan ID from iccl.conf            */
/*****************************************************************************/
UINT4
IcchApiGetIcclVlanId (UINT2 u2Index)
{
    return gIcchSessionInfo[u2Index].VlanId;
}

/*****************************************************************************/
/*  Function Name   : IcchApiGetIcclVlanId                                   */
/*  Description     : This Function returns the total number of active ICCH  */
/*                    instances.                                             */
/*  Input(s)        : None                                                   */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ICCH IP Addresss from issnvram.txt        */
/*****************************************************************************/
UINT4
IcchApiGetTotalNumOfInst (VOID)
{
    return ICCH_TOTAL_NUM_OF_ACTV_INST;
}

/*****************************************************************************/
/*  Function Name   : IcchApiCheckProtoSyncEnabled                           */
/*  Description     : This function checks whether sync is blocked.            */
/*  Input(s)        : None                              */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  gIcchInfo.u4EnableProtoSync               */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  OSIX_SUCCESS/OSIX_FAILURE                 */
/*****************************************************************************/
UINT4
IcchApiCheckProtoSyncEnabled (VOID)
{
    if (gIcchInfo.u4EnableProtoSync == ICCH_ENABLE_ALL_SYNC)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/*****************************************************************************/
/*  Function Name   : IcchIsIcclVlan                                         */
/*  Description     : This function checks whether the VLAN ID is ICCL VLAN  */
/*  Input(s)        : u2VlanId - VLAN Identifier                             */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  gIcchSessionInfo                          */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  OSIX_SUCCESS or OSIX_FAILURE              */
/*****************************************************************************/
UINT4
IcchIsIcclVlan (UINT2 u2VlanId)
{
    UINT2               u2Index = 0;

    for (u2Index = 0; u2Index < ICCH_TOTAL_NUM_OF_ACTV_INST; u2Index++)
    {
        if (gIcchSessionInfo[u2Index].VlanId == u2VlanId)
        {
            return OSIX_SUCCESS;
        }
    }

    return OSIX_FAILURE;
}

/*****************************************************************************/
/*  Function Name   : IcchApiSendMCLAGStatus                                 */
/*  Description     : This Function posts an event to all modules for the    */
/*                    MCLAG enable/disable status                            */
/*  Input(s)        : i4MCLAGSystemStatus - Enable/Disable status            */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                         */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ICCH_SUCCESS/ICCH_FAILURE                 */
/*****************************************************************************/
INT4
IcchApiSendMCLAGStatus (INT4 i4MCLAGSystemStatus)
{
    UINT4               u4Event = 0;

    if (i4MCLAGSystemStatus == MCLAG_ENABLED)
    {
        u4Event = ICCH_MCLAG_ENABLED;
    }
    else
    {
        u4Event = ICCH_MCLAG_DISABLED;
    }

    if (ICCH_SEND_EVENT (ICCH_TASK_ID, u4Event) == OSIX_FAILURE)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                  "IcchApiSendHbEvtToIcch: Sending event to ICCH from HB failed\r\n");
        return ICCH_FAILURE;
    }
    return ICCH_SUCCESS;
}

/*****************************************************************************/
/* Function Name : IcchIsIcclInterface                                       */
/* Description : This Function checks whether the given interface is         */
/* ICCL Interface                                                            */
/* Input(s) : u4VlanId - VLAN Identifier                                     */
/* Output(s) : None                                                          */
/* <OPTIONAL Fields> : None                                                  */
/* Global Variables Referred : gIcchSessionInfo                              */
/* Global variables Modified : None                                          */
/* Exceptions or Operating System Error Handling : None                      */
/* Use of Recursion : None                                                   */
/* Returns : OSIX_TRUE or OSIX_FALSE                                         */
/*****************************************************************************/
INT1
IcchIsIcclInterface (UINT4 u4IfIndex)
{
    if (gIcchInfo.u4IcclIfIndex == u4IfIndex)
    {
        return OSIX_TRUE;
    }

    return OSIX_FALSE;
}

/*****************************************************************************/
/* Function Name      : IcchApiSendHbEvtToIcch                               */
/*                                                                           */
/* Description        : This Function is used to decide whether ICCL         */
/*                      route has to be redistributed or not.                */
/*                                                                           */
/*                      Redistribution is not done when network address is   */
/*                      ICCL network address                                 */
/*                                                                           */
/* Input(s)           : u4DestNetAddr - Destination network address of a     */
/*                                      route                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS or OSIX_FAILURE                         */
/*****************************************************************************/
UINT4
IcchApiIsIcclAddrRedistributable (UINT4 u4DestNetAddr)
{
    UINT4               u4IcclNetAddr = 0;
    UINT2               u2Index = ICCH_DEFAULT_INST_ID;

    /* Calculate ICCL network address from ICCL IP address and subnet */
    u4IcclNetAddr = gIcchSessionInfo[u2Index].u4IpAddress &
        gIcchSessionInfo[u2Index].u4SubnetMask;

    if (u4DestNetAddr == u4IcclNetAddr)
    {
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function Name      : IsIcchNextIpAddress                                  */
/*                                                                           */
/* Description        : This Function is used to check whether the given IP  */
/*         is configured as next ICCL IP address.               */
/*                                                                           */
/* Input(s)           : u4IpAddr -  IP address                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gIcchSessionInfo[u2Index].u4NextIpAddress            */
/*                 - Next ICCL IP configured                    */
/*         gIcchSessionInfo[u2Index].u4NextSubnetMask           */
/*             - Next ICCL subnet Mask configured       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS or OSIX_FAILURE                         */
/*****************************************************************************/
UINT4
IsIcchNextIpAddress (UINT4 u4IpAddr, UINT4 u4NetMask)
{
    UINT4               u4IcclNetAddr = 0;
    UINT4               u4MaxSubNetMask = u4NetMask;
    UINT2               u2Index = ICCH_DEFAULT_INST_ID;
    UINT4               u4SubnetMask =
        gIcchSessionInfo[u2Index].u4NextSubnetMask;

    /* Calculate ICCL network address from ICCL IP address and subnet */
    u4IcclNetAddr = gIcchSessionInfo[u2Index].u4NextIpAddress & u4SubnetMask;

    if (u4SubnetMask > u4NetMask)
    {
        u4MaxSubNetMask = u4SubnetMask;
    }
    if ((u4IcclNetAddr != 0) &&
        ((u4IpAddr & u4MaxSubNetMask) == (u4IcclNetAddr & u4MaxSubNetMask)))
    {
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function Name      : IcchApiIsSessionAvailable                            */
/*                                                                           */
/* Description        : This Function is used to check whether ICCH session  */
/*                      is in connected state or not.                        */
/*                                                                           */
/*                      The function returns,                                */
/*                      OSIX_SUCCESS - u4PeerAddr is in connected state      */
/*                      OSIX_FAILURE - u4PeerAddr is not in connected state  */
/*                                                                           */
/* Input(s)           : u4PeerAddr - Peer Network Addres                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
UINT4
IcchApiIsSessionAvailable (UINT4 u4PeerAddr)
{
    UINT1               u1SsnIndex = 0;

    /* Check if peer is UP */
    if (ICCH_PEER_DOWN == ICCH_GET_PEER_NODE_STATE)
    {
        return OSIX_FAILURE;
    }

    /* Check if ICCH session is established */
    if (ICCH_FAILURE == IcchGetSsnIndexFromPeerAddr (u4PeerAddr, &u1SsnIndex))
    {
        return OSIX_FAILURE;
    }

    /* Check if the ICCH session is in connected state */
    if (ICCH_SOCK_CONNECTED == ICCH_GET_SSN_INFO (u1SsnIndex).i1ConnStatus)
    {
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

#ifdef __APRESIA_AC1_AG1__        /*CSR Merge 119676 */
/*****************************************************************************/
/*  Function Name   : IcchApiSetProtoSync                                    */
/*  Description     : This function sets whether to enable Protocol          */
/*                    sync or not                                            */
/*  Input(s)        : u4ProtoSync -  Enable/Disable                          */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  gIcchInfo.u4EnableProtoSync               */
/*  Global variables Modified   :  gIcchInfo.u4EnableProtoSync               */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  OSIX_SUCCESS/OSIX_FAILURE                 */
/*****************************************************************************/
UINT4
IcchApiSetProtoSync (UINT4 u4ProtoSync)
{
    ICCH_LOCK ();
    gIcchInfo.u4EnableProtoSync = u4ProtoSync;
    ICCH_UNLOCK ();
    return OSIX_SUCCESS;
}
#endif
