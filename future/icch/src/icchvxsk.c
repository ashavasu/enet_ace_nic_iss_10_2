/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: icchvxsk.c,v 1.1.1.1 2015/01/29 11:28:17 siva Exp $
*
* Description: Inter chassis communication handler socket interface
*              functions.
*********************************************************************/

#include "icchincs.h"

/******************************************************************************
 * Function           : IcchTcpSrvSockInit
 * Input(s)           : pi4SrvSockFd - Pointer to server socket fd
 * Output(s)          : TCP server socket descriptor
 * Returns            : ICCH_SUCCESS/ICCH_FAILURE
 * Action             : Routine to create a TCP server scoket, set socket options
 *                      for accepting the connections from peer.
 ******************************************************************************/
INT4
IcchTcpSrvSockInit (INT4 *pi4SrvSockFd)
{
    UNUSED_PARAM(pi4SrvSockFd);
    return ICCH_SUCCESS;
}

/******************************************************************************
 * Function           : IcchTcpAcceptNewConnection
 * Input(s)           : i4SrvSockFd - Server socket fd
 *                      pi4SockFd - Pointer to client socket fd
 *                      pu4PeerAddr - Peer IP address
 * Output(s)          : New connection identifier
 * Returns            : None
 * Action             : This routine accepts the
 *                      connection from the TCP peer.
 ******************************************************************************/
INT4
IcchTcpAcceptNewConnection (INT4 i4SrvSockFd, INT4 *pi4SockFd, UINT4 *pu4PeerAddr)
{
    UNUSED_PARAM(i4SrvSockFd);
    UNUSED_PARAM(pi4SockFd);
    UNUSED_PARAM(pu4PeerAddr);
    return ICCH_SUCCESS;
}

/******************************************************************************
 * Function           : IcchTcpSockConnect
 * Input(s)           : u4PeerAddr -> Peer address to be connected
 *                      pi4Connfd -> Pointer to the connection fd
 *                      pi4ConnState -> Pointer to the connection status
 * Output(s)          : None
 * Returns            : ICCH_SUCCESS/ICCH_FAILURE
 * Action             : Routine used for connecting the TCP peer
 ******************************************************************************/
INT4
IcchTcpSockConnect (UINT4 u4PeerAddr, INT4 *pi4Connfd, INT4 *pi4ConnState)
{
    UNUSED_PARAM(u4PeerAddr);
    UNUSED_PARAM(pi4Connfd);
    UNUSED_PARAM(pi4ConnState);
    return ICCH_SUCCESS;
}

/******************************************************************************
 * Function           : IcchCloseSocket
 * Input(s)           : i4SockFd - Socket descriptor.
 * Output(s)          : None.
 * Returns            : 0 on success & -1 on failure
 * Action             : Routine to close the socket created for ICCH.
 ******************************************************************************/
INT4
IcchCloseSocket (INT4 i4SockFd)
{
    UNUSED_PARAM(i4SockFd);
    return ICCH_SUCCESS;
}

/******************************************************************************
 * Function           : IcchTcpSockSend
 * Input(s)           : i4ConnFd - Connection fd
 *                      pu1Data - Pointer to data to be send
 *                      u2PktLen - Packet length
 * Output(s)          : i4WrBytes - No. of bytes written
 * Returns            : ICCH_SUCCESS/ICCH_FAILURE
 * Action             : Routine to send ICCH protocol packets.
 ******************************************************************************/
UINT4
IcchTcpSockSend (INT4 i4ConnFd, UINT1 *pu1Data, UINT2 u2PktLen, INT4 *pi4WrBytes)
{
    UNUSED_PARAM(i4ConnFd);
    UNUSED_PARAM(pu1Data);
    UNUSED_PARAM(u2PktLen);
    UNUSED_PARAM(pi4WrBytes);
    return ICCH_SUCCESS;
}

/******************************************************************************
 * Function           : IcchTcpSockRcv
 * Input(s)           : i4ConnFd - Connection fd
 *                      pu1Data - Pointer to data to be send
 *                      u2PktLen - Packet length
 * Output(s)          : pi4RdBytes - No. of bytes received
 * Returns            : ICCH_SUCCESS/ICCH_FAILURE
 * Action             : Routine to send ICCH protocol packets.
 ******************************************************************************/
UINT4
IcchTcpSockRcv (INT4 i4ConnFd, UINT1 *pu1Data, UINT2 u2MsgLen, INT4 *pi4RdBytes)
{
    UNUSED_PARAM(i4ConnFd);
    UNUSED_PARAM(pu1Data);
    UNUSED_PARAM(u2MsgLen);
    UNUSED_PARAM(pi4RdBytes);
    return ICCH_SUCCESS;
}

