/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: fsicchwr.c,v 1.7 2017/09/25 12:11:45 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsicchlw.h"
# include  "fsicchwr.h"
# include  "fsicchdb.h"

VOID
RegisterFSICCH ()
{
    SNMPRegisterMib (&fsicchOID, &fsicchEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsicchOID, (const UINT1 *) "fsicch");
}

VOID
UnRegisterFSICCH ()
{
    SNMPUnRegisterMib (&fsicchOID, &fsicchEntry);
    SNMPDelSysorEntry (&fsicchOID, (const UINT1 *) "fsicch");
}

INT4
FsIcchTrcLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIcchTrcLevel (&(pMultiData->u4_ULongValue)));
}

INT4
FsIcchStatsEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIcchStatsEnable (&(pMultiData->i4_SLongValue)));
}

INT4
FsIcchClearStatsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIcchClearStats (&(pMultiData->i4_SLongValue)));
}

INT4
FsIcchEnableProtoSyncGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIcchEnableProtoSync (&(pMultiData->u4_ULongValue)));
}

INT4
FsIcchFetchRemoteFdbGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIcchFetchRemoteFdb (&(pMultiData->i4_SLongValue)));
}

INT4
FsIcchPeerNodeIpAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIcchPeerNodeIpAddress (&(pMultiData->u4_ULongValue)));
}

INT4
FsIcchPeerNodeStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIcchPeerNodeState (&(pMultiData->i4_SLongValue)));
}

INT4
FsIcchOverrideLocalAffinityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIcchOverrideLocalAffinity (&(pMultiData->i4_SLongValue)));
}

INT4
FsIcchTrcLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsIcchTrcLevel (pMultiData->u4_ULongValue));
}

INT4
FsIcchStatsEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsIcchStatsEnable (pMultiData->i4_SLongValue));
}

INT4
FsIcchClearStatsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsIcchClearStats (pMultiData->i4_SLongValue));
}

INT4
FsIcchEnableProtoSyncSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsIcchEnableProtoSync (pMultiData->u4_ULongValue));
}

INT4
FsIcchFetchRemoteFdbSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsIcchFetchRemoteFdb (pMultiData->i4_SLongValue));
}

INT4
FsIcchOverrideLocalAffinitySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsIcchOverrideLocalAffinity (pMultiData->i4_SLongValue));
}

INT4
FsIcchTrcLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsIcchTrcLevel (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsIcchStatsEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsIcchStatsEnable (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsIcchClearStatsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsIcchClearStats (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsIcchEnableProtoSyncTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsIcchEnableProtoSync
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsIcchFetchRemoteFdbTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsIcchFetchRemoteFdb
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsIcchOverrideLocalAffinityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsIcchOverrideLocalAffinity
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsIcchTrcLevelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIcchTrcLevel (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsIcchStatsEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIcchStatsEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsIcchClearStatsDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIcchClearStats (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsIcchEnableProtoSyncDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIcchEnableProtoSync
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsIcchFetchRemoteFdbDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIcchFetchRemoteFdb
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsIcchOverrideLocalAffinityDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIcchOverrideLocalAffinity
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsIcclSessionTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIcclSessionTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIcclSessionTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsIcclSessionInterfaceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIcclSessionTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIcclSessionInterface (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsIcclSessionIpAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIcclSessionTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIcclSessionIpAddress (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsIcclSessionSubnetMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIcclSessionTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIcclSessionSubnetMask (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsIcclSessionVlanGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIcclSessionTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIcclSessionVlan (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsIcclSessionNodeStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIcclSessionTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIcclSessionNodeState (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsIcclSessionRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIcclSessionTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIcclSessionRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsIcclSessionInterfaceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIcclSessionInterface (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsIcclSessionIpAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIcclSessionIpAddress (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsIcclSessionSubnetMaskSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIcclSessionSubnetMask (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
FsIcclSessionVlanSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIcclSessionVlan (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsIcclSessionRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIcclSessionRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsIcclSessionInterfaceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsIcclSessionInterface (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->pOctetStrValue));

}

INT4
FsIcclSessionIpAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsIcclSessionIpAddress (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->u4_ULongValue));

}

INT4
FsIcclSessionSubnetMaskTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsIcclSessionSubnetMask (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
FsIcclSessionVlanTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsIcclSessionVlan (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsIcclSessionRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsIcclSessionRowStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsIcclSessionTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIcclSessionTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsIcchStatsSyncMsgTxCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIcchStatsSyncMsgTxCount (&(pMultiData->u4_ULongValue)));
}

INT4
FsIcchStatsSyncMsgTxFailedCountGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIcchStatsSyncMsgTxFailedCount
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsIcchStatsSyncMsgRxCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIcchStatsSyncMsgRxCount (&(pMultiData->u4_ULongValue)));
}

INT4
FsIcchStatsSyncMsgProcCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIcchStatsSyncMsgProcCount (&(pMultiData->u4_ULongValue)));
}

INT4
FsIcchStatsSyncMsgMissedCountGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIcchStatsSyncMsgMissedCount (&(pMultiData->u4_ULongValue)));
}
