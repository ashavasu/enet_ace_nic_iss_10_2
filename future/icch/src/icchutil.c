/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: icchutil.c,v 1.33 2018/02/08 11:09:28 siva Exp $
 *
 * Description: ICCH utility functions.
 ***********************************************************************/
#include "icchincs.h"

extern UINT4        gu4IssCidrSubnetMask[ISS_MAX_CIDR + 1];

/*****************************************************************************/
/* Function Name      : IcchLock                                             */
/*                                                                           */
/* Description        : This function is used to take the ICCH mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ICCH_SUCCESS or ICCH_FAILURE                         */
/*                                                                           */
/* Called By          : ICCH, Protocols registered with ICCH & SNMP/CLI      */
/*****************************************************************************/
INT4
IcchLock (VOID)
{
    if (OsixSemTake (ICCH_PROTO_SEM ()) != OSIX_SUCCESS)
    {
        ICCH_TRC (ICCH_CRITICAL_TRC,
                  "Failed to Take ICCH Protocol "
                  "Mutual Exclusion Sema4 \r\n");
        return ICCH_FAILURE;
    }

    return ICCH_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IcchUnLock                                             */
/*                                                                           */
/* Description        : This function is used to give the ICCH mutual          */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ICCH_SUCCESS or ICCH_FAILURE                         */
/*                                                                           */
/* Called By          : ICCH, Protocols registered with ICCH & SNMP/CLI      */
/*****************************************************************************/
INT4
IcchUnLock (VOID)
{
    if (OsixSemGive (ICCH_PROTO_SEM ()) != OSIX_SUCCESS)
    {
        ICCH_TRC (ICCH_CRITICAL_TRC,
                  "Failed to Give ICCH Protocol "
                  "Mutual Exclusion Sema4 \r\n");
        return ICCH_FAILURE;
    }

    return ICCH_SUCCESS;
}

/******************************************************************************
 * Function           : IcchGetSsnIndexFromConnFd
 * Input(s)           : i4ConnFd - Reliable connection file descriptor
 *                      pu1SsnIndex - Pointer of session table
 * Output(s)          : None
 * Returns            : ICCH_SUCCESS/ICCH_FAILURE
 * Action             : Gives the session table associated with
 *                      the connection fd
 ******************************************************************************/
UINT1
IcchGetSsnIndexFromConnFd (INT4 i4ConnFd, UINT1 *pu1SsnIndex)
{
    UINT1               u1SsnCnt = 0;

    for (u1SsnCnt = 0; u1SsnCnt < ICCH_MAX_SYNC_SSNS_LIMIT; u1SsnCnt++)
    {
        if (ICCH_GET_SSN_INFO (u1SsnCnt).i4ConnFd == i4ConnFd)
        {
            *pu1SsnIndex = u1SsnCnt;
            return (ICCH_SUCCESS);
        }
    }
    ICCH_TRC (ICCH_ALL_FAILURE_TRC,
              "IcchGetSsnIndexFromPeerAddr: "
              "Session does not exist for the connection fd\r\n");
    return (ICCH_FAILURE);
}

/******************************************************************************
 * Function           : IcchTcpWriteCallBackFn
 * Input(s)           : i4SockFd - Writable socket fd
 * Output(s)          : None
 * Returns            : None
 * Action             : Callback function to indicate socket is writable
 ******************************************************************************/
VOID
IcchTcpWriteCallBackFn (INT4 i4SockFd)
{
    tIcchCtrlQMsg      *pIcchCtrlQMsg = NULL;

    if ((pIcchCtrlQMsg = (tIcchCtrlQMsg *) (MemAllocMemBlk
                                            (gIcchInfo.IcchCtrlMsgPoolId))) ==
        NULL)
    {
        ICCH_TRC (ICCH_CRITICAL_TRC | ICCH_ALL_FAILURE_TRC,
                  "IcchTcpWriteCallBackFn: Memory allocation "
                  "failed for control message\r\n");
        return;
    }

    MEMSET (pIcchCtrlQMsg, 0, sizeof (tIcchCtrlQMsg));

    pIcchCtrlQMsg->u4MsgType = ICCH_WR_SOCK_MSG;
    pIcchCtrlQMsg->SktCtrlMsg.i4SockId = i4SockFd;
    if (IcchQueEnqCtrlMsg (pIcchCtrlQMsg) == ICCH_FAILURE)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                  "IcchTcpWriteCallBackFn: Send to control Q " "failed\r\n");
        return;
    }
}

/******************************************************************************
 * Function           : IcchPktRcvd
 * Input(s)           : i4SockFd - socket descriptor.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Callback function registered to SELECT library.
 *                      This function is invoked when a ICCH packet arrives.
 ******************************************************************************/
VOID
IcchPktRcvd (INT4 i4SockFd)
{
    tIcchCtrlQMsg      *pIcchCtrlQMsg = NULL;

    HbApiSetIcchKeepAliveFlag (ICCH_TRUE);

    if ((pIcchCtrlQMsg = (tIcchCtrlQMsg *) (MemAllocMemBlk
                                            (gIcchInfo.IcchCtrlMsgPoolId))) ==
        NULL)
    {
        ICCH_TRC (ICCH_CRITICAL_TRC | ICCH_ALL_FAILURE_TRC,
                  "IcchPktRcvd: Memory allocation"
                  "failed for control message\r\n");
        return;
    }

    MEMSET (pIcchCtrlQMsg, 0, sizeof (tIcchCtrlQMsg));

    pIcchCtrlQMsg->u4MsgType = ICCH_RD_SOCK_MSG;
    pIcchCtrlQMsg->SktCtrlMsg.i4SockId = i4SockFd;
    if (IcchQueEnqCtrlMsg (pIcchCtrlQMsg) == ICCH_FAILURE)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC, "IcchPktRcvd: Send to control Q "
                  "failed\r\n");
        return;
    }
}

/******************************************************************************
 * Function           : IcchAddSyncMsgIntoTxList
 * Input(s)           : u1SsnIndex -> Session index
 *                      pu1Data -> Sync message
 *                      u4IcchPktLen -> Sync message length
 *                      u4IcchMsgOffset -> Message offset
 *                      to be sent to the peer
 * Output(s)          : None
 * Returns            : None
 * Action             : Adds the sync message to the TX LIST
 ******************************************************************************/
VOID
IcchAddSyncMsgIntoTxList (UINT1 u1SsnIndex, UINT1 *pu1Data,
                          UINT4 u4IcchPktLen, UINT4 u4IcchMsgOffset)
{
    tIcchTxBufNode     *pIcchTxBufNode = NULL;
    UINT4               u4TxListCnt = 0;

    /* Allocate BufNode and put the message in the peer tx queue */
    pIcchTxBufNode =
        (tIcchTxBufNode *) (MemAllocMemBlk (gIcchInfo.IcchTxBufMemPoolId));
    if (pIcchTxBufNode == NULL)
    {
        /* Send buffer overflow */
        /* Memory allocation failure */
        ICCH_TRC (ICCH_CRITICAL_TRC | ICCH_ALL_FAILURE_TRC,
                  "TX Buffer is overflow, this may be due to TCP session problem"
                  "Please restart the node.\r\n");
        return;
    }
    ICCH_SLL_NODE_INIT (&pIcchTxBufNode->TxNode);
    pIcchTxBufNode->pu1Msg = pu1Data;
    pIcchTxBufNode->u4MsgSize = u4IcchPktLen;
    pIcchTxBufNode->u4MsgOffset = u4IcchMsgOffset;
    ICCH_SLL_ADD (&ICCH_SSN_ENTRY_TX_LIST
                  (ICCH_GET_SSN_INFO (u1SsnIndex)), &pIcchTxBufNode->TxNode);
    u4TxListCnt = ICCH_SLL_COUNT (&ICCH_SSN_ENTRY_TX_LIST
                                  (ICCH_GET_SSN_INFO (u1SsnIndex)));
    if (u4TxListCnt >= ICCH_TX_BUF_NODES_THRESHOLD)
    {
        /* Tx buffer is full */
        gIcchInfo.bIsTxBuffFull = ICCH_TRUE;
        ICCH_TRC1 (ICCH_CRITICAL_TRC, "IcchAddSyncMsgIntoTxList: "
                   "Tx buffer threshold %d is reached \r\n", u4TxListCnt);
    }
    return;
}

/******************************************************************************
 * Function           : IcchUtilReleaseMemForPktQMsg
 * Input(s)           : pIcchPktQMsg - Block to be release
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS/ICCH_FAILURE
 * Action             : Routine to release the memory allocated for the Pkt Q
 *                      Message.
 ******************************************************************************/
INT1
IcchUtilReleaseMemForPktQMsg (tIcchPktQMsg * pIcchPktQMsg)
{
    if (pIcchPktQMsg->u1MsgType == ICCH_SYNCUP_MSG)
    {
        if (pIcchPktQMsg->pIcchMsg != NULL)
        {
            ICCH_FREE (pIcchPktQMsg->pIcchMsg);
            pIcchPktQMsg->pIcchMsg = NULL;
        }
    }

    if (MemReleaseMemBlock (gIcchInfo.IcchPktQMsgPoolId, (UINT1 *) pIcchPktQMsg)
        != MEM_SUCCESS)
    {
        ICCH_TRC1 (ICCH_CRITICAL_TRC | ICCH_ALL_FAILURE_TRC,
                   "IcchUtilReleaseMemForPktQMsg"
                   "Mem block release failed for blck %u\r\n", pIcchPktQMsg);
        return ICCH_FAILURE;
    }
    pIcchPktQMsg = NULL;
    return ICCH_SUCCESS;
}

/******************************************************************************
 * Function           : IcchMemAllocForResBuf
 * Input(s)           : ppResBuf -> Pointer to pointer residual buffer
 * Output(s)          : None
 * Returns            : ICCH_SUCCESS/ICCH_FAILURE
 * Action             : This routine allocates the memory for residual buffer
 ******************************************************************************/
UINT1
IcchMemAllocForResBuf (UINT1 **ppResBuf)
{
    *ppResBuf = MemAllocMemBlk (gIcchInfo.IcchPktMsgPoolId);
    if (*ppResBuf == NULL)
    {
        ICCH_TRC (ICCH_CRITICAL_TRC | ICCH_ALL_FAILURE_TRC,
                  "IcchMemAllocForResBuf: Memory allocation failed "
                  "for the residual buffer !!!\r\n");
        return ICCH_FAILURE;
        /* Reception residual buffer memory allocation failed */
    }
    MEMSET (*ppResBuf, 0, ICCH_MAX_SYNC_PKT_LEN);
    return ICCH_SUCCESS;
}

/******************************************************************************
 * Function           : IcchUtilHandleProtocolAck
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function handles the acknowledgement sent by
 *                      protocols.
 *****************************************************************************/
VOID
IcchUtilHandleProtocolAck (tIcchProtoAck ProtoAck)
{

    if (ProtoAck.u4SeqNumber != ICCH_RX_LAST_SEQ_NUM_PROCESSED ())
    {
        ICCH_TRC2 (ICCH_SYNC_MSG_TRC | ICCH_ALL_FAILURE_TRC | ICCH_NOTIF_TRC,
                   "IcchUtilHandleProtocolAck: INVALID protocol "
                   "ACK received.[AppId=%s, Seq# %d]. Ignoring it.\r\n",
                   gaIcchAppName[ProtoAck.u4AppId], ProtoAck.u4SeqNumber);
        return;
    }
    else
    {
        IcchTmrStopTimer (ICCH_ACK_RECOV_TIMER);
        ICCH_RX_LAST_ACK_RCVD_SEQ_NUM () = ProtoAck.u4SeqNumber;

        /* This scenario occurs in the following case:
         * VCM has sent an ack to ICCH module after processing dynamic
         * bulk tail message and sent an event to MSR to start the
         * static configuration restaore. In this case, the RX buffer
         * should not be released. The lock will be received after the
         * static configuration restore is completed
         */
        ICCH_TRC2 (ICCH_SYNC_EVT_TRC | ICCH_SYNC_MSG_TRC | ICCH_NOTIF_TRC,
                   "SyncMsg: Seq# %d, ACK %s->ICCH\r\n",
                   ProtoAck.u4SeqNumber, gaIcchAppName[ProtoAck.u4AppId]);
        ICCH_IS_RX_BUFF_PROCESSING_ALLOWED () = ICCH_TRUE;
        IcchRxProcessBufferedPkt ();
    }
    return;
}

/******************************************************************************
 * Function           : IcchUtilPostDataOrEvntToProtocol
 * Input(s)           : u4DestEntId - App Id of the protocol the where the
 *                                    packet has to be sent.
 *                      u1Event  - Valid events are GO_MASTER/GO_SLAVE/
 *                                 ICCH_MESSAGE.
 *                      pPkt - Received sync-up message buffer. ICCH header
 *                             should not be included.(valid only if
 *                             u1Event==ICCH_MESSAGE)
 *                      u4PktLen - Length of the packet.
 *                      u4SeqNum - Sequence number of the packet. protocol
 *                                 should return this sequence number with
 *                                 the Acknoledgement message after
 *                                 processing the packet.
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS / ICCH_FAILURE
 * Action             : Function to send the sync-up data or ICCH events
 *                      to the destination protocol based on the u4DestEntId.
 *
 ******************************************************************************/
INT4
IcchUtilPostDataOrEvntToProtocol (UINT4 u4DestEntId, UINT1 u1Event,
                                  tIcchMsg * pPkt, UINT4 u4PktLen,
                                  UINT4 u4SeqNum)
{
    if (gIcchInfo.apIcchAppInfo[u4DestEntId] != NULL)
    {
        if (gIcchInfo.apIcchAppInfo[u4DestEntId]->u4ValidEntry == TRUE)
        {
            if (gIcchInfo.apIcchAppInfo[u4DestEntId]->pFnRcvPkt != NULL)
            {
                /* Posting data and event to destination protocol */
                if ((*(gIcchInfo.apIcchAppInfo[u4DestEntId]->pFnRcvPkt))
                    (u1Event, pPkt, (UINT2) u4PktLen) == ICCH_SUCCESS)
                {
                    return ICCH_SUCCESS;
                }
            }
        }
    }
    ICCH_TRC3 (ICCH_SYNC_EVT_TRC | ICCH_ALL_FAILURE_TRC,
               "IcchUtilPostDataOrEvntToProtocol: Posting Msg/Event to Protocol "
               "failed [AppName=%s, Event=%d, Seq#=%d]\r\n",
               gaIcchAppName[u4DestEntId], u1Event, u4SeqNum);

    return ICCH_FAILURE;
}

/******************************************************************************
 * Function           : IcchTcpUpdateSsnTable
 * Input(s)           : i4CliSockFd - Newly accpeted connection identifier
 *                      u4IpAddr - Peer IP address
 * Output(s)          : None
 * Description        : It updates the session table based on the peer
 *                      information received in the scoket accept call.
 * Return Value(s)    : none
 * ******************************************************************************/
VOID
IcchTcpUpdateSsnTable (INT4 i4CliSockFd, UINT4 u4IpAddr)
{
    UINT1               u1SsnIndex = 0;
    UINT1              *pResBuf = NULL;

    for (u1SsnIndex = 0; u1SsnIndex < ICCH_MAX_SYNC_SSNS_LIMIT; u1SsnIndex++)
    {
        if (ICCH_GET_SSN_INFO (u1SsnIndex).i1ConnStatus == ICCH_SOCK_CONNECTED)
        {
            IcchUtilCloseSockConn (u1SsnIndex);
        }
        ICCH_GET_SSN_INFO (u1SsnIndex).u4PeerAddr = u4IpAddr;
        ICCH_GET_SSN_INFO (u1SsnIndex).i4ConnFd = i4CliSockFd;
        /* Allocate the memory for Rx residual buffer
         * only if not allocated i.e NULL */
        if (ICCH_GET_SSN_INFO (u1SsnIndex).pResBuf == NULL)
        {
            if (IcchMemAllocForResBuf (&pResBuf) == ICCH_FAILURE)
            {
                ICCH_TRC (ICCH_ALL_FAILURE_TRC, "IcchTcpUpdateSsnTable: "
                          "Residual buffer allocation failed \r\n");
                IcchUtilCloseSockConn (u1SsnIndex);
                return;
            }

            ICCH_GET_SSN_INFO (u1SsnIndex).pResBuf = pResBuf;
        }

        ICCH_GET_SSN_INFO (u1SsnIndex).i1ConnStatus = ICCH_SOCK_CONNECTED;
        if (ICCH_GET_PEER_NODE_ID != u4IpAddr)
        {
            /* Peer is not learnt thro. peer up, so, add it here */
            ICCH_SET_PEER_NODE_ID (u4IpAddr);
            ICCH_PEER_NODE_COUNT ()++;
        }
        /* Send the pending TX list thro. this new connection */

        IcchSockWritableHandle (ICCH_GET_SSN_INFO (u1SsnIndex).i4ConnFd);
    }
    return;
}

/******************************************************************************
 * Function           : IcchTcpSrvSockCallBk
 * Input(s)           : i4SockFd - socket descriptor.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Callback function registered to SELECT library.
 *                      This function is invoked when a pakcet arrives on
 *                      TCP server.
 ******************************************************************************/
VOID
IcchTcpSrvSockCallBk (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);

    if (ICCH_SEND_EVENT (ICCH_TASK_ID, ICCH_TCP_NEW_CONN_RCVD) == OSIX_FAILURE)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                  "IcchTcpSrvSockCallBk: Send Event ICCH_TCP_NEW_CONN_RCVD"
                  " Failed\r\n");
        return;
    }
}

/******************************************************************************
 * Function           : IcchGetSsnIndexFromPeerAddr
 * Input(s)           : u4PeerAddr -> Peer address
 *                      pu1SsnIndex -> Pointer to session index
 * Output(s)          : pu1SsnIndex -> Session index corresponding
 *                                     to the peer address
 * Returns            : ICCH_SUCCESS/ICCH_FAILURE
 * Action             : Gives the session index corresponding
 *                      to the peer address
 ******************************************************************************/
UINT1
IcchGetSsnIndexFromPeerAddr (UINT4 u4PeerAddr, UINT1 *pu1SsnIndex)
{
    UINT1               u1SsnCnt = 0;

    for (u1SsnCnt = 0; u1SsnCnt < ICCH_MAX_SYNC_SSNS_LIMIT; u1SsnCnt++)
    {
        if (ICCH_GET_SSN_INFO (u1SsnCnt).u4PeerAddr == u4PeerAddr)
        {
            *pu1SsnIndex = u1SsnCnt;
            return (ICCH_SUCCESS);
        }
    }
    ICCH_TRC1 (ICCH_ALL_FAILURE_TRC, "IcchGetSsnIndexFromPeerAddr: "
               "Session does not exist for the peer %x EXIT\r\n", u4PeerAddr);
    return (ICCH_FAILURE);
}

/******************************************************************************
 * Function           : IcchHandleInitBulkUpdate
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function starts up the bulk update process in the
 *                       local node 
 ******************************************************************************/
VOID
IcchHandleInitBulkUpdate (VOID)
{
    tIcchProtoEvt       ProtoEvt;
    UINT4               u4NextAppId = ICCH_APP_ID;

    ICCH_TRC (ICCH_NOTIF_TRC,
              "Initiate bulk request trigger after "
              "sending GO_SLAVE to protocols\r\n");
    if (IcchUtilGetNextAppId (ICCH_APP_ID, &u4NextAppId) == ICCH_FAILURE)
    {
        return;
    }
    /* Intiate bulk update for the lowest layer protocol */
    ProtoEvt.u4AppId = ICCH_VLAN_APP_ID;
    ProtoEvt.u4Error = ICCH_NONE;
    ProtoEvt.u4Event = ICCH_INITIATE_BULK_UPDATE;
    ICCH_UNLOCK ();
    IcchHandleProtocolEvent (&ProtoEvt);
    ICCH_LOCK ();

    return;
}

/******************************************************************************
 * Function           : IcchHandleProtocolEvent
 * Input(s)           : pEvt->u4AppId - Module Id.
 *                      pEvt->u4Event   - Event send by protocols
 *                      pEvt->u4Error   - Error code (ICCH_MEMALLOC_FAIL /
 *                      ICCH_SENDTO_FAIL / ICCH_PROCESS_FAIL / ICCH_NONE)
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS / ICCH_FAILURE.
 * Action             : Routine used by protocols/applications to intimate ICCH
 *                      about the protocol operations. ICCH decides upon the
 *                      events and send appropriate notifications to the
 *                      management application or events to the protocols.
 ******************************************************************************/
UINT1
IcchHandleProtocolEvent (tIcchProtoEvt * pEvt)
{
    UINT1               u1Event = 0;
    UINT1               u1IsNextHigherLayerPresent = ICCH_SUCCESS;
    BOOL1               bFetchRemoteFdb = ICCH_FALSE;
    switch (pEvt->u4Event)
    {
        case ICCH_INITIATE_BULK_UPDATE:
            /* Send start of bulk update notification. 
               This event is send
               by ICCH on  triggering PEER_UP event to protocols 
             */
            if (gu4IcchBulkUpdtInProgress == ICCH_FALSE)
            {
                gu4IcchBulkUpdtInProgress = ICCH_TRUE;
                IcchTmrStartTimer (ICCH_BULK_UPDATE_TIMER,
                                   ICCH_BULK_UPDATE_TMR_INT);
            }
            gIcchInfo.u1IsBulkUpdtInProgress = ICCH_TRUE;

            u1Event = ICCH_INITIATE_BULK_UPDATES;
            if ((pEvt->u4AppId == ICCH_VLAN_APP_ID) ||
                (pEvt->u4AppId == ICCH_ARP_APP_ID))
            {
                /* Protocol triggers the bulk update request initation (SWAudit
                 * mismatch / bulk update process failure).
                 * L2_INIT_BULK_UPDATES event will be posted to
                 * the same module. */
                if (IcchUtilPostDataOrEvntToProtocol (pEvt->u4AppId, u1Event,
                                                      NULL, 0,
                                                      0) == ICCH_FAILURE)
                {
                    return ICCH_FAILURE;
                }
            }
            break;
        case ICCH_PROTOCOL_BULK_UPDT_COMPLETION:
            bFetchRemoteFdb = gIcchInfo.bFetchRemoteFdb;
            if (bFetchRemoteFdb == ICCH_TRUE)
            {
                ICCH_LOCK ();
                gIcchInfo.bFetchRemoteFdb = ICCH_FALSE;
                ICCH_UNLOCK ();
            }

            ICCH_TRC1 (ICCH_SYNC_EVT_TRC | ICCH_NOTIF_TRC,
                       "Event: Bulk update completed by %s module\r\n",
                       gaIcchAppName[pEvt->u4AppId]);

            /* Bulk update is completed by the lower layer module.
             * Send L2_INIT_BULK_UPDATES event to next higher layer
             * module.*/

            u1Event = ICCH_INITIATE_BULK_UPDATES;
            u1IsNextHigherLayerPresent = IcchUtilTriggerNextHigherLayer
                (pEvt->u4AppId, u1Event);
            break;

        case ICCH_BULK_UPDT_ABORT:
        {
            ICCH_TRC1 (ICCH_ALL_FAILURE_TRC | ICCH_NOTIF_TRC,
                       "IcchApiHandleProtocolEvent: "
                       "ICCH bulk update aborted for : %s]\r\n",
                       gaIcchAppName[pEvt->u4AppId]);

            break;
        }

        case ICCH_PROTOCOL_SEND_EVENT:

            ICCH_TRC (ICCH_SYNC_EVT_TRC, "Sending event to ICCH module\r\n");

            if (ICCH_TASK_ID == ICCH_INIT)
            {
                if (ICCH_GET_TASK_ID (ICCH_SELF, (const UINT1 *) ICCH_TASK_NAME,
                                      &ICCH_TASK_ID) != OSIX_SUCCESS)
                {

                    ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                              "Failed to get ICCH Task ID\r\n");
                    return ICCH_FAILURE;
                }
            }

            if (ICCH_SEND_EVENT (ICCH_TASK_ID, pEvt->u4SendEvent) !=
                OSIX_SUCCESS)
            {
                ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                          "Failed to Send Event to ICCH task\r\n");
                return ICCH_FAILURE;
            }

            break;

        default:
            break;
    }

    switch (pEvt->u4Error)
    {
        case ICCH_SENDTO_FAIL:
            ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                      "Send Message to ICCH  Failed !!\r\n");
            break;

        case ICCH_MEMALLOC_FAIL:
            ICCH_TRC (ICCH_ALL_FAILURE_TRC, "ICCH MEMALLOC Failed !!\r\n");
            break;

        default:
            break;
    }

    if (u1IsNextHigherLayerPresent == ICCH_FAILURE)
    {
        /* No next protocol exists to initiate bulk update.
         * Send bulk update completion notififcation. */
        gIcchInfo.u1IsBulkUpdtInProgress = ICCH_FALSE;
    }
    return ICCH_SUCCESS;
}

/******************************************************************************
 * Function           : IcchUtilTriggerNextHigherLayer
 * Input(s)           : u4AppId - Module Id.
 *                      u4Event   - Event to be send to the protocols
 *                      (L2_INIT_BULK_UPDATES)
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS / ICCH_FAILURE.
 * Action             : Routine used by ICCH to send events to higher level
 *                      protocol modules.
 ******************************************************************************/
UINT1
IcchUtilTriggerNextHigherLayer (UINT4 u4AppId, UINT1 u1Event)
{
    UINT1               u1RetVal = ICCH_SUCCESS;
    UINT4               u4EntId = ICCH_APP_ID;
    switch (u1Event)
    {
        case ICCH_INITIATE_BULK_UPDATES:

            while ((u1RetVal =
                    IcchUtilGetNextAppId (u4AppId, &u4EntId)) == ICCH_SUCCESS)
            {
                /* Trigger to start bulk update by  the
                 * next higher layer module registered. */
                ICCH_TRC1 (ICCH_SYNC_EVT_TRC | ICCH_NOTIF_TRC,
                           "Event: Initiating bulk update for %s module\r\n",
                           gaIcchAppName[u4EntId]);
                u4EntId = MEM_MAX_BYTES (u4EntId, (ICCH_MAX_APPS - 1));

                if (u4EntId == ICCH_SNOOP_APP_ID)
                {
                    u4EntId = ICCH_ARP_APP_ID;
                }

                IcchUtilPostDataOrEvntToProtocol (u4EntId, u1Event, NULL, 0, 0);
                break;
            }

            break;
        default:
            break;
    }
    return u1RetVal;
}

/******************************************************************************
 * Function           : IcchUtilGetNextAppId
 * Input(s)           : u4AppId - Module Id.
 *                      pu4NextId - Pointer to next valid
 *                      registered module Id.
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS / ICCH_FAILURE.
 * Action             : Routine used by ICCH to return next valid registered
 *                      module Id.
 ******************************************************************************/
UINT1
IcchUtilGetNextAppId (UINT4 u4AppId, UINT4 *pu4NextId)
{
    UINT1               u1RetVal = ICCH_FAILURE;
    UINT4               u4ModId = 0;

    /* Get the next valid module ID. */
    for (u4ModId = ++u4AppId;
         (u4ModId < ICCH_MAX_APPS && u1RetVal == ICCH_FAILURE); u4ModId++)
    {
        if ((gIcchInfo.apIcchAppInfo[u4ModId] != NULL) &&
            (gIcchInfo.apIcchAppInfo[u4ModId]->pFnRcvPkt != NULL))
        {
            /* Valid entry found */
            *pu4NextId = u4ModId;
            u1RetVal = ICCH_SUCCESS;
        }

    }

    return u1RetVal;
}

/*****************************************************************************/
/* Function Name      : IcchCheckBulkUpdatesStatus                           */
/*                                                                           */
/* Description        : This function is used by ICCH to check the           */
/*                      configuration status of all the registered protocols */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ICCH_TRUE or ICCH_FALSE                              */
/*                                                                           */
/* Called By          : ICCH                                                 */
/*****************************************************************************/
INT4
IcchCheckBulkUpdatesStatus (VOID)
{
    UINT4               u4Index = 0;
    INT4                i4RetVal = ICCH_TRUE;
    UINT4               u4AppId = 0;

    for (u4Index = 0; u4Index < sizeof (tIcchAppList); u4Index++)
    {
        if (gIcchInfo.IcchBulkUpdStsMask[u4Index] == 0)
        {
            /* No applications present in this range of index */
            continue;
        }

        if ((gIcchInfo.IcchBulkUpdSts[u4Index] &
             gIcchInfo.IcchBulkUpdStsMask[u4Index]) !=
            gIcchInfo.IcchBulkUpdStsMask[u4Index])
        {
            i4RetVal = ICCH_FALSE;
            break;
        }
    }
    if (i4RetVal == ICCH_FALSE)
    {
        for (u4AppId = ICCH_LA_APP_ID; u4AppId < ICCH_MAX_APPS; u4AppId++)
        {
            ICCH_UNLOCK ();
            if (IcchGetBulkUpdatesStatus (u4AppId) == ICCH_FAILURE)
            {
                ICCH_TRC1 (ICCH_ALL_FAILURE_TRC | ICCH_NOTIF_TRC,
                           " Bulk update yet to be completed for %s module\r\n",
                           gaIcchAppName[u4AppId]);
            }
            ICCH_LOCK ();
        }
        return i4RetVal;
    }
    return i4RetVal;
}

/******************************************************************************
 * Function           : IcchUtilCheckAndCompleteBulkUpdt
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : This Function resets the gu4 IcchBulkUpdtInProgress 
 *                      flag and initiates the bulk update in the master
 *                      node.
 ******************************************************************************/
VOID
IcchUtilCheckAndCompleteBulkUpdt (VOID)
{
    if (gu4IcchBulkUpdtInProgress == ICCH_TRUE)
    {
        IcchTmrStopTimer (ICCH_BULK_UPDATE_TIMER);
        gu4IcchBulkUpdtInProgress = ICCH_FALSE;
    }
    if (ICCH_GET_NODE_STATE () == ICCH_MASTER)
    {
        IcchHandleInitBulkUpdate ();
    }
    return;
}

/******************************************************************************
 * Function           : IcchHandleNodeStateUpdate
 * Input(s)           : u1State - ICCH_MASTER/ ICCH_SLAVE
 * Output(s)          : None
 * Returns            : None
 * Action             : This routine updates the node status
 *                      and gives the GO_MASTER/GO_SLAVE to
 *                      ICCH enabled modules.
 ******************************************************************************/
VOID
IcchHandleNodeStateUpdate (UINT1 u1State)
{
    tIcchNotificationMsg NotifMsg;
    tIcchProtoEvt       ProtoEvt;
    UINT1               u1PrevState = 0;
    UINT1               u1Event = ICCH_INIT;
    UINT1               u1SsnCnt = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));
    MEMSET (&NotifMsg, 0, sizeof (tIcchNotificationMsg));

    u1PrevState = (UINT1) ICCH_GET_NODE_STATE ();
    ICCH_SET_PREV_NODE_STATE (u1PrevState);
    ICCH_SET_NODE_STATE (u1State);
    IcchTrapSendNotifications (ICCH_STATUS_CHANGE_TRAP);

    if (u1State == ICCH_MASTER)
    {
        ICCH_TRC (ICCH_NOTIF_TRC, "IcchHandleNodeStateUpdate: "
                  "Node state changed to MASTER \r\n");
        ICCH_SET_MASTER_NODE_ID (ICCH_GET_SELF_NODE_ID ());
    }
    else if (u1State == ICCH_SLAVE)
    {
        ICCH_TRC (ICCH_NOTIF_TRC, "IcchHandleNodeStateUpdate: "
                  "Node state changed to SLAVE\r\n");
        ICCH_SET_MASTER_NODE_ID (ICCH_GET_SSN_INFO (u1SsnCnt).u4PeerAddr);
    }
    else
    {
        ICCH_TRC (ICCH_NOTIF_TRC, "IcchHandleNodeStateUpdate: "
                  "Node state changed to INIT\r\n");
        ICCH_SET_MASTER_NODE_ID (0);
        ICCH_SET_PEER_NODE_ID (0);
    }

    if ((u1PrevState == ICCH_SLAVE) && (u1State == ICCH_MASTER))
    {
        if (ICCH_PEER_NODE_COUNT () == 0)
        {
            IcchUpdatePeerNodeCntToProtocols (ICCH_PEER_DOWN);
        }
    }

    /* Node state changes to SLAVE or IDLE */
    if (ICCH_GET_NODE_STATE () == ICCH_SLAVE)
    {
        u1Event = GO_SLAVE;
        ICCH_IS_RX_BUFF_PROCESSING_ALLOWED () = ICCH_TRUE;
    }
    else if (ICCH_GET_NODE_STATE () == ICCH_MASTER)
    {
        u1Event = GO_MASTER;
        ICCH_IS_RX_BUFF_PROCESSING_ALLOWED () = ICCH_TRUE;
    }
    if ((u1Event == GO_MASTER) || (u1Event == GO_SLAVE))
    {
        IcchNotifyProtocols (u1Event);
        /* Trigger bulk update initiation request,
         * after sending GO_SLAVE event */
        if ((u1Event == GO_SLAVE) && (ICCH_GET_PEER_NODE_ID != 0))
        {
            ICCH_TRC (ICCH_NOTIF_TRC,
                      "Initiate bulk request trigger after sending GO_SLAVE"
                      " to protocols\r\n");
            ProtoEvt.u4AppId = ICCH_VLAN_APP_ID;
            ProtoEvt.u4Error = ICCH_NONE;
            ProtoEvt.u4Event = ICCH_INITIATE_BULK_UPDATE;
            ICCH_UNLOCK ();
            IcchHandleProtocolEvent (&ProtoEvt);
            ICCH_LOCK ();
        }
        else
        {
            gu4IcchBulkUpdtReqNotSent = ICCH_TRUE;
        }
    }
    if ((u1PrevState == ICCH_SLAVE) && (u1State == ICCH_MASTER))
    {
    }
    if (u1Event == GO_SLAVE)
    {
        IcchProcessPktFromApp ();
    }
}

/******************************************************************************
 * Function           : IcchNotifyProtocols
 * Input(s)           : u1Event - Events like GO_MASTER/GO_SLAVE.
 * Output(s)          : None.
 * Returns            : Nome.
 * Action             : Routine used by ICCH to notify protocols.
 ******************************************************************************/
VOID
IcchNotifyProtocols (UINT1 u1Event)
{
    UINT4               u4EntId = 0;
    UINT4               u4AppId = 0;
    UINT1               u1RetVal = ICCH_SUCCESS;
    /* In case of VCM_WANTED, GO_SLAVE event should be send to VCM Module
     * After finishing, it calls the IcchSendEventToAppln API to send GO_SLAVE
     * to all other protocols*/

    if ((u1Event == GO_SLAVE) &&
        (ICCH_GET_PREV_NODE_STATE () == ICCH_INIT) &&
        (ICCH_GET_NODE_STATE () == ICCH_SLAVE))
    {
        for (u4EntId = 1; u4EntId < ICCH_MAX_APPS; u4EntId++)
        {
            IcchUtilPostDataOrEvntToProtocol (u4EntId, u1Event, NULL, 0, 0);
        }
        return;
    }

    if ((u1Event == GO_MASTER) && (ICCH_GET_PREV_NODE_STATE () == ICCH_INIT))
    {
        u4AppId = ICCH_APP_ID;
        while ((u1RetVal =
                IcchUtilGetNextAppId (u4AppId, &u4EntId)) == ICCH_SUCCESS)
        {
            /* Trigger to start bulk update by  the
             * next higher layer module registered. */
            ICCH_TRC1 (ICCH_SYNC_EVT_TRC | ICCH_NOTIF_TRC,
                       "Sending bulk update trigger to AppId %d\r\n", u4EntId);
            IcchUtilPostDataOrEvntToProtocol (u4EntId, (UINT1) GO_MASTER,
                                              NULL, 0, 0);
            break;
        }
        return;
    }

    /* Notify Node Status to the protcols registered with ICCH */
    for (u4EntId = 1; u4EntId < ICCH_MAX_APPS; u4EntId++)
    {
        IcchUtilPostDataOrEvntToProtocol (u4EntId, u1Event, NULL, 0, 0);
    }
    return;
}

/******************************************************************************
 * Function           : IcchRxBufAddPkt
 * Input(s)           : pu1Pkt - packet pointer that needs to be added in the 
 *                               RX Buffer. packet should contatin the ICCH 
 *                               Header. it should be a liner buffer pointer.
 *                      u4SeqNum - Sequence number of the received packet
 *                      u4PktLen - length of the entire packet.
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS / ICCH_FAILURE
 * Action             : This function add the received sync-up data packet
 *                      to the RX Buffer (Hash Table).
 ******************************************************************************/
INT4
IcchRxBufAddPkt (UINT1 *pu1Pkt, UINT4 u4SeqNum, UINT4 u4PktLen)
{
    tIcchRxBufHashEntry *pRxBufNode = NULL;

    /* Allocate memory for the hash node : tIcchRxBufHashEntry */
    if ((pRxBufNode =
         (tIcchRxBufHashEntry *) (MemAllocMemBlk (gIcchInfo.IcchRxBufPoolId)))
        == NULL)
    {
        ICCH_TRC (ICCH_CRITICAL_TRC | ICCH_ALL_FAILURE_TRC,
                  "Memory allocation failed for the RX Buff node.\r\n");
        /* pu1Pkt memory should be cleared by the caller function */
        return ICCH_FAILURE;
    }

    /* Update the node information */
    TMO_HASH_Init_Node (&pRxBufNode->RxBufHashNode);
    pRxBufNode->u4SeqNum = u4SeqNum;
    pRxBufNode->pu1Pkt = pu1Pkt;
    pRxBufNode->u4PktLen = u4PktLen;

    /* Add the node to the Hash Table */
    TMO_HASH_Add_Node (gIcchInfo.pRxBufHashTable, &pRxBufNode->RxBufHashNode,
                       ICCH_RX_GET_HASH_INDEX (pRxBufNode->u4SeqNum), NULL);

    ICCH_RX_BUFF_PKT_COUNT ()++;

    ICCH_TRC2 (ICCH_SYNC_MSG_TRC, "IcchRxBufAddPkt: "
               "HASH ADD[seq# %d], (hash count = %d)\r\n", u4SeqNum,
               ICCH_RX_BUFF_PKT_COUNT ());
    return ICCH_SUCCESS;

}

/******************************************************************************
 * Function           : IcchRxBufSearchPkt
 * Input(s)           : u4SeqNum - Sequenece number of the packet that needs
 *                                 to be searched in the RX Buffer.
 * Output(s)          : None.
 * Returns            : pointer to tIcchRxBufHashEntry node if entry found
 *                      else return NULL
 * Action             : This function searchs a desired sequence numbered
 *                      packet in the RX Buffer. If entry found then returns 
 *                      the Hash Node pointer to the caller else return NULL.
 ******************************************************************************/
tIcchRxBufHashEntry *
IcchRxBufSearchPkt (UINT4 u4SeqNum)
{
    tIcchRxBufHashEntry *pRxBufNode = NULL;
    tTMO_HASH_NODE     *pNode = NULL;
    UINT4               u4HashIndex = ICCH_RX_GET_HASH_INDEX (u4SeqNum);

    /* Get the hash index using the u4SeqNum */
    /* Scan only the required bucket. No need to scan the entire table */
    TMO_HASH_Scan_Bucket (gIcchInfo.pRxBufHashTable,
                          u4HashIndex, pNode, tTMO_HASH_NODE *)
    {
        pRxBufNode = (tIcchRxBufHashEntry *)
            (pNode - ICCH_RX_BUFF_GET_HASHNODE_OFFSET ());
        if (pRxBufNode->u4SeqNum == u4SeqNum)
        {
            /* Match found */
            return pRxBufNode;
        }
    }
    return NULL;
}

/******************************************************************************
 * Function           : IcchRxBufDeleteNode
 * Input(s)           : pRxBufNode - Pointer to the HashNode that needs to be
 *                                   deleted form the Hash Table
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS / ICCH_FAILURE
 * Action             : This function deletes the specified hash node from the
 *                      hash table (RX Buffer). It also takes care of 
 *                      releasing the memory used by the node.
 ******************************************************************************/
INT4
IcchRxBufDeleteNode (tIcchRxBufHashEntry * pRxBufNode)
{
    UINT1               u1SsnIndex = 0;

    TMO_HASH_Delete_Node (gIcchInfo.pRxBufHashTable, &pRxBufNode->RxBufHashNode,
                          ICCH_RX_GET_HASH_INDEX (pRxBufNode->u4SeqNum));

    /* Deleted from the Hash table. Now clean the memory for this node */
    IcchRxBufFreeHashNode (&pRxBufNode->RxBufHashNode);

    ICCH_RX_BUFF_PKT_COUNT ()--;

    if (gIcchInfo.bIsRxBuffFull == ICCH_TRUE)
    {
        gIcchInfo.bIsRxBuffFull = ICCH_FALSE;
        ICCH_TRC (ICCH_CRITICAL_TRC,
                  "!!! Sync message reception is resumed "
                  "because Rx buffer is available !!!\r\n");
        /* As we support 1:1 standby, 
         * there is only one session alive */
        IcchPktRcvd (ICCH_GET_SSN_INFO (u1SsnIndex).i4ConnFd);
    }
    return ICCH_SUCCESS;
}

/******************************************************************************
 * Function           : IcchRxBufCreate
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : ICCH_SUCCESS / ICCH_FAILURE
 * Action             : This function creates the RX Buffer(i.e. Hash Table)
 *                     
 ******************************************************************************/
INT4
IcchRxBufCreate (VOID)
{
    /* Create the Hash Table to buffer the received sync-up messages in 
     * sequentional order */
    gIcchInfo.pRxBufHashTable =
        TMO_HASH_Create_Table (ICCH_RXBUF_MAX_HASH_BUCKET_COUNT, NULL, FALSE);

    if (gIcchInfo.pRxBufHashTable == NULL)
    {
        /* Hash table creation failed */
        ICCH_TRC (ICCH_CRITICAL_TRC, "RX Buff creation failed\r\n");
        return ICCH_FAILURE;
    }

    return ICCH_SUCCESS;
}

/******************************************************************************
 * Function           : IcchRxBufDestroy
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function deletes the entire RX buffer (hash table)
 *                      including all the nodes. It takes care of deleting the
 *                      memory used by the hash nodes.
 ******************************************************************************/
VOID
IcchRxBufDestroy (VOID)
{
    if (gIcchInfo.pRxBufHashTable != NULL)
    {
        TMO_HASH_Delete_Table (gIcchInfo.pRxBufHashTable,
                               IcchRxBufFreeHashNode);
        gIcchInfo.pRxBufHashTable = NULL;
    }
}

/******************************************************************************
 * Function           : IcchRxBufFreeHashNode
 * Input(s)           : pNode - Hash node pointer
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function releases the memory used by a hash node.
 *                     
 ******************************************************************************/
VOID
IcchRxBufFreeHashNode (tTMO_HASH_NODE * pNode)
{
    tIcchRxBufHashEntry *pRxBufNode = NULL;
    pRxBufNode = (tIcchRxBufHashEntry *)
        (pNode - ICCH_RX_BUFF_GET_HASHNODE_OFFSET ());

    /* Free the packet memory liner buffer pointer */
    ICCH_RXBUF_PKT_MEM_FREE (pRxBufNode->pu1Pkt);

    /* Release the memblock for the hash node */
    MemReleaseMemBlock (gIcchInfo.IcchRxBufPoolId, (UINT1 *) pRxBufNode);
    pRxBufNode = NULL;
}

/******************************************************************************
 * Function           : IcchRxBufClearRxBuffer
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function releases all the nodes added
 *                       in the RxBuffer
 ******************************************************************************/
VOID
IcchRxBufClearRxBuffer (VOID)
{
    tTMO_HASH_NODE     *pNode = NULL;
    tTMO_HASH_NODE     *pTmpNode = NULL;
    UINT4               u4HashIndex = 0;

    TMO_HASH_Scan_Table (gIcchInfo.pRxBufHashTable, u4HashIndex)
    {
        TMO_HASH_DYN_Scan_Bucket (gIcchInfo.pRxBufHashTable, u4HashIndex,
                                  pNode, pTmpNode, tTMO_HASH_NODE *)
        {
            TMO_HASH_Delete_Node (gIcchInfo.pRxBufHashTable, pNode,
                                  u4HashIndex);
            IcchRxBufFreeHashNode (pNode);
            ICCH_RX_BUFF_PKT_COUNT ()--;
        }
    }
    return;
}

/******************************************************************************
 * Function           : IcchPrcsPendingRxBufMsgs
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : Process the pending RX buffer messages, in case of 
 *                      failure
 ******************************************************************************/
VOID
IcchPrcsPendingRxBufMsgs (VOID)
{

    if ((ICCH_RX_BUFF_PKT_COUNT () == 0) &&
        (ICCH_RX_LAST_SEQ_NUM_PROCESSED () == ICCH_RX_LAST_ACK_RCVD_SEQ_NUM ()))
    {

        HbApiSendIcchEventToHb (HB_ICCH_SYNC_MSG_PROCESSED);
    }
    else
    {

        if (ICCH_RX_SEQ_MISSMATCH_FLG () == ICCH_TRUE)
        {
            ICCH_TRC (ICCH_SYNC_MSG_TRC,
                      "IcchPrcsPendingRxBufMsgs: Seq Recov timer is already running due to sequence no. mismatch\r\n");
            IcchTmrStopTimer (ICCH_SEQ_RECOV_TIMER);
            ICCH_RX_SEQ_MISSMATCH_FLG () = ICCH_FALSE;

            /* Update the last processed sequence number */
            ICCH_RX_INCR_LAST_SEQNUM_PROCESSED ();

            /* Update statistics - for the buffered messages that is processed */
            ICCH_INCR_RX_STAT_SYNC_MSG_PROCESSED ();
            ICCH_IS_RX_BUFF_PROCESSING_ALLOWED () = ICCH_TRUE;
            /*Now process the Rx Buffer */
            IcchRxProcessBufferedPkt ();
        }
        else
        {
            ICCH_TRC (ICCH_SYNC_MSG_TRC,
                      "IcchPrcsPendingRxBufMsgs: Buffer processing is going on for pending Rx packets \r\n");
        }
    }

}

/******************************************************************************
 * Function           : IcchLoadConfsFromConfFile
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 * Action             : Parses the configurations from iccl.conf file.
                        Configurations are,
                        Instance   - ICCL instance ID
                        ICCL       - ICCL interface name
                        VLAN       - ICCL VLAN ID
                        IPAddress  - ICCL interface IP address
                        SubnetMask - ICCL interface subnet mask 
 ******************************************************************************/
UINT1
IcchLoadConfsFromConfFile (VOID)
{
    tUtlInAddr          IcchIpAddr;
    tUtlInAddr          IcchIpSubnet;
    UINT1               au1ReadBuff[ICCL_CONF_MAX_LEN];
    UINT1               au1IpAddress[ICCL_MAX_ADDR_LEN];
    UINT1               au1SubnetMask[ICCL_MAX_ADDR_LEN];
    CHR1                ac1Buf[ICCL_CONF_MAX_LEN];
    UINT4               u4PortChannelId = 0;
    INT4                i4Fd = 0;
    UINT2               u2Count = 0;
    UINT1               u1Index = 0;
    UINT1               u1IsIcclParamsChanged = ICCH_FALSE;

    MEMSET (au1ReadBuff, 0, ICCL_CONF_MAX_LEN);
    MEMSET (au1IpAddress, 0, ICCL_MAX_ADDR_LEN);
    MEMSET (au1SubnetMask, 0, ICCL_MAX_ADDR_LEN);
    MEMSET (ac1Buf, 0, ICCL_CONF_MAX_LEN);
    MEMSET (gIcchSessionInfo, 0, ICCH_MAX_SESSIONS * sizeof (tIcchSessInfo));

    if (FileStat (ICCL_CONF_FILE) != OSIX_SUCCESS)
    {
        i4Fd = FileOpen ((CONST UINT1 *) ICCL_CONF_FILE,
                         OSIX_FILE_WO | OSIX_FILE_TR | OSIX_FILE_CR);

        if (i4Fd < 0)
        {
            ICCH_TRC (ICCH_CRITICAL_TRC, "[ERROR]:Failed to open iccl.conf"
                      "configuration file in write mode\r\n");
            return OSIX_FAILURE;
        }

        FileWrite (i4Fd, ICCH_CONF_COMMENT, STRLEN (ICCH_CONF_COMMENT));

        SPRINTF (ac1Buf, "%-9d    %-7s %-4d    %-9s    %-10s\n",
                 ICCH_DEFAULT_INST_ID,
                 ICCH_DEFAULT_INT,
                 ICCH_DEFAULT_VLAN, ICCH_DEFAULT_IP, ICCH_SUBNET_MASK);

        FileWrite (i4Fd, ac1Buf, STRLEN (ac1Buf));
        FileClose (i4Fd);
    }

    i4Fd = FileOpen ((CONST UINT1 *) ICCL_CONF_FILE, OSIX_FILE_RO);

    if (i4Fd < 0)
    {
        ICCH_TRC (ICCH_CRITICAL_TRC, "[ERROR]:Failed to open iccl.conf"
                  "configuration file in read mode\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (au1ReadBuff, 0, ICCL_CONF_MAX_LEN);
    /* If iccl.conf is empty, default value is written */
    while ((u2Count < ICCH_MAX_SESSIONS) &&
           (FsUtlReadLine (i4Fd, (INT1 *) au1ReadBuff) != OSIX_SUCCESS))
    {
        FileClose (i4Fd);
        i4Fd = FileOpen ((CONST UINT1 *) ICCL_CONF_FILE,
                         OSIX_FILE_WO | OSIX_FILE_TR | OSIX_FILE_CR);

        if (i4Fd < 0)
        {
            ICCH_TRC (ICCH_CRITICAL_TRC, "[ERROR]:Failed to open iccl.conf"
                      "configuration file in write mode\r\n");
            return OSIX_FAILURE;
        }

        FileWrite (i4Fd, ICCH_CONF_COMMENT, STRLEN (ICCH_CONF_COMMENT));

        SPRINTF (ac1Buf, "%-9d    %-7s %-4d    %-9s    %-10s\n",
                 ICCH_DEFAULT_INST_ID,
                 ICCH_DEFAULT_INT,
                 ICCH_DEFAULT_VLAN, ICCH_DEFAULT_IP, ICCH_SUBNET_MASK);

        FileWrite (i4Fd, ac1Buf, STRLEN (ac1Buf));
        FileClose (i4Fd);

        i4Fd = FileOpen ((CONST UINT1 *) ICCL_CONF_FILE, OSIX_FILE_RO);
        if (i4Fd < 0)
        {
            ICCH_TRC (ICCH_CRITICAL_TRC, "[ERROR]:Failed to open iccl.conf"
                      "configuration file in read mode\r\n");
            return OSIX_FAILURE;
        }

        /* Skip comment in iccl.conf file */
        if (FsUtlReadLine (i4Fd, (INT1 *) au1ReadBuff) != OSIX_SUCCESS)
        {
            ICCH_TRC (ICCH_CRITICAL_TRC, "[ERROR]:Failed in FsUtlReadLine\r\n");
            FileClose (i4Fd);
            return OSIX_FAILURE;
        }
        u2Count++;
    }

    u2Count = 0;

    while ((u2Count < ICCH_MAX_SESSIONS) &&
           (FsUtlReadLine (i4Fd, (INT1 *) au1ReadBuff) == OSIX_SUCCESS))
    {
        MEMSET (au1IpAddress, 0, ICCL_MAX_ADDR_LEN);
        MEMSET (au1SubnetMask, 0, ICCL_MAX_ADDR_LEN);

        SSCANF ((CHR1 *) au1ReadBuff, "%u %s %hu %s %s",
                &gIcchSessionInfo[u2Count].u4InstanceId,
                gIcchSessionInfo[u2Count].ac1IcclInterface,
                &gIcchSessionInfo[u2Count].VlanId, au1IpAddress, au1SubnetMask);

        gIcchSessionInfo[u2Count].u4IpAddress =
            OSIX_NTOHL (INET_ADDR (au1IpAddress));
        gIcchSessionInfo[u2Count].u4SubnetMask =
            OSIX_NTOHL (INET_ADDR (au1SubnetMask));

        /* Handle when wrong value is there in iccl.conf */
        if (ICCL_MAX_SUBNET == gIcchSessionInfo[u2Count].u4SubnetMask)
        {
            gIcchSessionInfo[u2Count].u4SubnetMask = ISS_CUST_SYS_DEF_IP_MASK;
            u1IsIcclParamsChanged = ICCH_TRUE;
        }

        /* The valid subnet address is from 0 to 32 */
        for (u1Index = 0; u1Index <= ISS_MAX_CIDR; ++u1Index)
        {
            if (gu4IssCidrSubnetMask[u1Index] ==
                gIcchSessionInfo[u2Count].u4SubnetMask)
            {
                break;
            }
        }
        if (u1Index > ISS_MAX_CIDR)
        {
            ICCH_TRC (ICCH_CRITICAL_TRC,
                      "[ERROR]:Invalid Subnet Mask, default value is taken\r\n");
            gIcchSessionInfo[u2Count].u4SubnetMask = ISS_CUST_SYS_DEF_IP_MASK;
            u1IsIcclParamsChanged = ICCH_TRUE;
        }
        if (gIcchSessionInfo[u2Count].u4InstanceId > ICCH_MAX_INST_ID)
        {
            ICCH_TRC (ICCH_CRITICAL_TRC,
                      "[ERROR]:Invalid Instance ID, default value is taken\r\n");
            gIcchSessionInfo[u2Count].u4InstanceId = ICCH_DEFAULT_INST_ID;
            u1IsIcclParamsChanged = ICCH_TRUE;
        }
        if ((gIcchSessionInfo[u2Count].VlanId < ICCH_MIN_VLAN) ||
            (gIcchSessionInfo[u2Count].VlanId > ICCH_MAX_VLAN))
        {
            ICCH_TRC (ICCH_CRITICAL_TRC,
                      "[ERROR]:Invalid VLAN ID, default value is taken\r\n");
            gIcchSessionInfo[u2Count].VlanId = ICCH_DEFAULT_VLAN;
            u1IsIcclParamsChanged = ICCH_TRUE;
        }
        if (!((ISS_IS_ADDR_CLASS_A (INET_ADDR (au1IpAddress))) ||
              (ISS_IS_ADDR_CLASS_B (INET_ADDR (au1IpAddress))) ||
              (ISS_IS_ADDR_CLASS_C (INET_ADDR (au1IpAddress))) ||
              (ISS_IS_ADDR_VALID (INET_ADDR (au1IpAddress)))))
        {
            ICCH_TRC (ICCH_CRITICAL_TRC,
                      "[ERROR]:Invalid IP address, default value is taken\r\n");
            gIcchSessionInfo[u2Count].u4IpAddress =
                OSIX_NTOHL (INET_ADDR (ICCH_DEFAULT_IP));
            u1IsIcclParamsChanged = ICCH_TRUE;
        }

        /* ICCL interface should be port channel */
        if (0 !=
            STRNCMP (gIcchSessionInfo[u2Count].ac1IcclInterface, "po",
                     STRLEN ("po")))
        {
            ICCH_TRC (ICCH_CRITICAL_TRC, "[ERROR]:Invalid interface type\r\n");
            STRCPY (gIcchSessionInfo[u2Count].ac1IcclInterface,
                    ICCH_DEFAULT_INT);
            u1IsIcclParamsChanged = ICCH_TRUE;
        }

        /* port channel should be between 1 to 65535 */
        u4PortChannelId =
            (UINT4) (ATOI
                     (gIcchSessionInfo[u2Count].ac1IcclInterface +
                      STRLEN ("po")));

        if ((u4PortChannelId < ICCH_MIN_PORT_CHNL_ID) ||
            (u4PortChannelId > ICCH_MAX_PORT_CHNL_ID))
        {
            ICCH_TRC (ICCH_CRITICAL_TRC,
                      "[ERROR]:Invalid port channel index, default value is taken\r\n");
            STRCPY (gIcchSessionInfo[u2Count].ac1IcclInterface,
                    ICCH_DEFAULT_INT);
            u1IsIcclParamsChanged = ICCH_TRUE;
        }

        u2Count++;
        ICCH_TOTAL_NUM_OF_ACTV_INST++;
        MEMSET (au1ReadBuff, 0, ICCL_CONF_MAX_LEN);
    }
    FileClose (i4Fd);

    if (u1IsIcclParamsChanged == ICCH_TRUE)
    {
        /* Default value is taken for some of the ICCL parameters. This needs to
         * be re-written to iccl.conf file */
        i4Fd = FileOpen ((CONST UINT1 *) ICCL_CONF_FILE,
                         OSIX_FILE_WO | OSIX_FILE_TR | OSIX_FILE_CR);

        if (i4Fd < 0)
        {
            ICCH_TRC (ICCH_CRITICAL_TRC, "[ERROR]:Failed to open iccl.conf"
                      "configuration file in write mode\r\n");
            return OSIX_FAILURE;
        }

        FileWrite (i4Fd, ICCH_CONF_COMMENT, STRLEN (ICCH_CONF_COMMENT));

        for (u2Count = 0; u2Count < ICCH_TOTAL_NUM_OF_ACTV_INST; u2Count++)
        {
            MEMSET (&IcchIpAddr, 0, sizeof (tUtlInAddr));
            MEMSET (&IcchIpSubnet, 0, sizeof (tUtlInAddr));
            MEMSET (au1IpAddress, 0, sizeof (au1IpAddress));
            MEMSET (au1SubnetMask, 0, sizeof (au1SubnetMask));

            IcchIpAddr.u4Addr =
                OSIX_NTOHL (gIcchSessionInfo[u2Count].u4IpAddress);
            IcchIpSubnet.u4Addr =
                OSIX_NTOHL (gIcchSessionInfo[u2Count].u4SubnetMask);
            STRCPY (au1IpAddress, INET_NTOA (IcchIpAddr));
            STRCPY (au1SubnetMask, INET_NTOA (IcchIpSubnet));

            SPRINTF (ac1Buf, "%-9d    %-7s %-4d    %-9s    %-10s\n",
                     gIcchSessionInfo[u2Count].u4InstanceId,
                     gIcchSessionInfo[u2Count].ac1IcclInterface,
                     gIcchSessionInfo[u2Count].VlanId,
                     au1IpAddress, au1SubnetMask);

            FileWrite (i4Fd, ac1Buf, STRLEN (ac1Buf));
        }
        FileClose (i4Fd);
    }
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function           : IcchWriteConfsToConfFile
 * Input(s)           : pIcchSessionInfo - pointer to ICCH session info
                        u1IsWriteOrDel - To add or delete the entry
 * Output(s)          : None
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 * Action             : Function to add new entry, modify the entry or delete 
                        the entry in iccl.conf
 ******************************************************************************/
UINT1
IcchUpdateIcclConfFile (tIcchSessInfo * pIcchSessionInfo, UINT1 u1IsWriteOrDel)
{
    tIcchSessInfo       IcchReadSessInfo[ICCH_MAX_SESSIONS];
    UINT1               au1ReadBuff[ICCL_CONF_MAX_LEN];
    UINT1               au1IpAddress[ICCL_MAX_ADDR_LEN];
    UINT1               au1SubnetMask[ICCL_MAX_ADDR_LEN];
    CHR1                au1Buf[ICCL_CONF_MAX_LEN];
    tUtlInAddr          IcchIpAddr;
    tUtlInAddr          IcchIpSubnet;
    INT4                i4Fd = 0;
    INT4                i4ReadFd = 0;
    UINT2               u2NumOfEntry = 0;
    UINT2               u2Count = 0;
    UINT1               u1IsEntryExist = ICCH_FALSE;

    MEMSET (au1Buf, 0, ICCL_CONF_MAX_LEN);
    MEMSET (au1ReadBuff, 0, ICCL_CONF_MAX_LEN);
    MEMSET (au1IpAddress, 0, ICCL_MAX_ADDR_LEN);
    MEMSET (au1SubnetMask, 0, ICCL_MAX_ADDR_LEN);
    MEMSET (&IcchIpAddr, 0, sizeof (tUtlInAddr));
    MEMSET (&IcchIpSubnet, 0, sizeof (tUtlInAddr));
    MEMSET (IcchReadSessInfo, 0, sizeof (tIcchSessInfo));
    MEMSET (IcchReadSessInfo, 0, ICCH_MAX_SESSIONS * sizeof (tIcchSessInfo));

    i4ReadFd = FileOpen ((CONST UINT1 *) ICCL_CONF_FILE, OSIX_FILE_RO);

    if (i4ReadFd < 0)
    {
        ICCH_TRC (ICCH_CRITICAL_TRC, "[ERROR]:Failed to open iccl.conf"
                  "configuration file in read mode\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (au1ReadBuff, 0, ICCL_CONF_MAX_LEN);

    /* Skip comment in iccl.conf file */
    while ((u2Count < ICCH_MAX_SESSIONS) &&
           (FsUtlReadLine (i4ReadFd, (INT1 *) au1ReadBuff) != OSIX_SUCCESS))
    {
        ICCH_TRC (ICCH_CRITICAL_TRC, "[ERROR]:Failed in FsUtlReadLine\r\n");
        FileClose (i4ReadFd);
        return OSIX_FAILURE;
    }

    while (FsUtlReadLine (i4ReadFd, (INT1 *) au1ReadBuff) == OSIX_SUCCESS)
    {
        MEMSET (au1IpAddress, 0, ICCL_MAX_ADDR_LEN);
        MEMSET (au1SubnetMask, 0, ICCL_MAX_ADDR_LEN);

        SSCANF ((CHR1 *) au1ReadBuff, "%d %s %hu %s %s",
                &IcchReadSessInfo[u2Count].u4InstanceId,
                IcchReadSessInfo[u2Count].ac1IcclInterface,
                &IcchReadSessInfo[u2Count].VlanId, au1IpAddress, au1SubnetMask);

        IcchReadSessInfo[u2Count].u4IpAddress =
            OSIX_NTOHL (INET_ADDR (au1IpAddress));
        IcchReadSessInfo[u2Count].u4SubnetMask =
            OSIX_NTOHL (INET_ADDR (au1SubnetMask));

        u2Count++;
        MEMSET (au1ReadBuff, 0, ICCL_CONF_MAX_LEN);
    }

    u2NumOfEntry = u2Count;

    FileClose (i4ReadFd);

    i4Fd = FileOpen ((CONST UINT1 *) ICCL_CONF_FILE,
                     OSIX_FILE_WO | OSIX_FILE_TR | OSIX_FILE_CR);
    if (i4Fd < 0)
    {
        ICCH_TRC (ICCH_CRITICAL_TRC, "[ERROR]:Failed to open iccl.conf"
                  "configuration file in write mode\r\n");
        return OSIX_FAILURE;
    }

    FileWrite (i4Fd, ICCH_CONF_COMMENT, STRLEN (ICCH_CONF_COMMENT));

    for (u2Count = 0; u2Count < u2NumOfEntry; u2Count++)
    {
        MEMSET (au1IpAddress, 0, ICCL_MAX_ADDR_LEN);
        MEMSET (au1SubnetMask, 0, ICCL_MAX_ADDR_LEN);

        if (u1IsEntryExist == ICCH_FALSE)
        {
            if (IcchReadSessInfo[u2Count].u4InstanceId ==
                pIcchSessionInfo->u4InstanceId)
            {
                u1IsEntryExist = ICCH_TRUE;
                if (u1IsWriteOrDel == ICCL_FILE_WRITE)
                {
                    if (0 != pIcchSessionInfo->u4IpAddress)
                    {
                        IcchReadSessInfo[u2Count].u4IpAddress =
                            pIcchSessionInfo->u4IpAddress;
                    }
                    if (0 != pIcchSessionInfo->u4SubnetMask)
                    {
                        IcchReadSessInfo[u2Count].u4SubnetMask =
                            pIcchSessionInfo->u4SubnetMask;
                    }
                    if (0 != pIcchSessionInfo->VlanId)
                    {
                        IcchReadSessInfo[u2Count].VlanId =
                            pIcchSessionInfo->VlanId;
                    }
                    if (0 != pIcchSessionInfo->ac1IcclInterface[0])
                    {
                        STRNCPY (IcchReadSessInfo[u2Count].ac1IcclInterface,
                                 pIcchSessionInfo->ac1IcclInterface,
                                 CFA_MAX_PORT_NAME_LENGTH);
                    }

                    IcchIpAddr.u4Addr =
                        OSIX_NTOHL (IcchReadSessInfo[u2Count].u4IpAddress);
                    IcchIpSubnet.u4Addr =
                        OSIX_NTOHL (IcchReadSessInfo[u2Count].u4SubnetMask);

                    STRCPY (au1IpAddress, UtlInetNtoa (IcchIpAddr));
                    STRCPY (au1SubnetMask, UtlInetNtoa (IcchIpSubnet));
                    SNPRINTF ((CHR1 *) au1Buf, ICCL_CONF_MAX_LEN - 1,
                              "%-9d    %-7s %-4d    %-9s    %-10s\n",
                              IcchReadSessInfo[u2Count].u4InstanceId,
                              IcchReadSessInfo[u2Count].ac1IcclInterface,
                              IcchReadSessInfo[u2Count].VlanId, au1IpAddress,
                              au1SubnetMask);

                    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
                }
                continue;
            }
        }

        IcchIpAddr.u4Addr = OSIX_NTOHL (IcchReadSessInfo[u2Count].u4IpAddress);
        IcchIpSubnet.u4Addr =
            OSIX_NTOHL (IcchReadSessInfo[u2Count].u4SubnetMask);

        STRCPY (au1IpAddress, UtlInetNtoa (IcchIpAddr));
        STRCPY (au1SubnetMask, UtlInetNtoa (IcchIpSubnet));

        SNPRINTF ((CHR1 *) au1Buf, ICCL_CONF_MAX_LEN - 1,
                  "%-9d    %-7s %-8d    %-9s    %-10s\n",
                  IcchReadSessInfo[u2Count].u4InstanceId,
                  IcchReadSessInfo[u2Count].ac1IcclInterface,
                  IcchReadSessInfo[u2Count].VlanId, au1IpAddress,
                  au1SubnetMask);

        FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

        MEMSET (au1Buf, 0, ICCL_CONF_MAX_LEN);
    }

    if ((u1IsEntryExist == ICCH_FALSE) && (u1IsWriteOrDel == ICCL_FILE_WRITE))
    {
        MEMSET (au1IpAddress, 0, ICCL_MAX_ADDR_LEN);
        MEMSET (au1SubnetMask, 0, ICCL_MAX_ADDR_LEN);

        STRCPY (au1IpAddress, UtlInetNtoa (IcchIpAddr));
        STRCPY (au1SubnetMask, UtlInetNtoa (IcchIpSubnet));

        IcchIpAddr.u4Addr = OSIX_NTOHL (pIcchSessionInfo->u4IpAddress);
        IcchIpSubnet.u4Addr = OSIX_NTOHL (pIcchSessionInfo->u4SubnetMask);

        SNPRINTF ((CHR1 *) au1Buf, ICCL_CONF_MAX_LEN - 1,
                  "%-9d    %-7s %-4d    %-9s    %-10s\n",
                  pIcchSessionInfo->u4InstanceId,
                  pIcchSessionInfo->ac1IcclInterface, pIcchSessionInfo->VlanId,
                  au1IpAddress, au1SubnetMask);

        FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    }

    FileClose (i4Fd);
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function           : IcchNotifyMCLAGStatus
 * Input(s)           : u1Event - Events ICCH_MCLAG_ENABLED/ICCH_MCLAG_DISABLED
 * Output(s)          : None.
 * Returns            : Nome.
 * Action             : Routine used by ICCH to notify protocols on MCLAG Status.
 ******************************************************************************/
VOID
IcchNotifyMCLAGStatus (UINT1 u1Event)
{
    UINT4               u4EntId = 0;

    for (u4EntId = 1; u4EntId < ICCH_MAX_APPS; u4EntId++)
    {
        IcchUtilPostDataOrEvntToProtocol (u4EntId, u1Event, NULL, 0, 0);
    }

    return;
}
