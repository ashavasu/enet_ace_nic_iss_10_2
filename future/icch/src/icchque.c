/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: icchque.c,v 1.16 2016/09/17 12:41:38 siva Exp $
*
* Description: ICCH Queue processing functions.
***********************************************************************/
#include "icchincs.h"

/******************************************************************************
 * Function           : IcchProcessPktFromApp
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine to process the pkt from application.
 *                      i.e., Icch rcvs the pkt from app send it to peer.
 ******************************************************************************/
VOID
IcchProcessPktFromApp (VOID)
{
    tIcchPktQMsg       *pIcchPktQMsg = NULL;
    UINT1              *pResBuf = NULL;
    UINT1              *pu1Data = NULL;
    UINT4               u4DestAddr = 0;
    UINT4               u4IcchPktLen = 0;
    UINT4               u4RelinquishCntr = 0;
    INT4                i4ConnFd = ICCH_INV_SOCK_FD;
    INT4                i4ConnState = 0;
    UINT1               u1SsnIndex = 0;


    /* check if the peer node ip is known */
    /* If node state is in init then clear the sync messages,
     * In connection lost and restored case we will be in ICCH_INIT state
     * until the shut and start operation is completed*/
    u4DestAddr = ICCH_GET_PEER_NODE_ID;
    if ((u4DestAddr == 0) ||
         (ICCH_GET_NODE_STATE () == ICCH_INIT))
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC, "IcchProcessPktFromApp: "
                  "NO peer is learnt \r\n");
        IcchClearSyncMsgQueue ();
        return;
    }

    if (IcchGetSsnIndexFromPeerAddr (u4DestAddr, &u1SsnIndex) == ICCH_FAILURE) 
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC, "IcchProcessPktFromApp: "
                  "NO session for the peer \r\n");
        return;
    }
    /* If CRU buffer Mem pool has less then 1/4th of free units and the ICCH
     * session is not available the packet has to be dropped */
    if ((MemGetFreeUnits (1) < ((SYS_MAX_BUF_DESC) / 4))&&
        (OSIX_FAILURE == IcchApiIsSessionAvailable(ICCH_GET_PEER_NODE_ID)))
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC, "IcchEnqMsgToIcchFromAppl: Dropping"
                "packets due to CRU Buffer depletion & ICCH Session not"
                "in connected state! \r\n");
        IcchUtilCloseSockConn (u1SsnIndex);
        return;
    }

    if ((ICCH_GET_NODE_STATE () == ICCH_MASTER) &&
        ((ICCH_GET_SSN_INFO (u1SsnIndex).i1ConnStatus) != ICCH_SOCK_CONNECTED))
    {
        /* When there is no connection, instead of holding the cru bufffer
         * Add it to Tx List */
        IcchAddSyncMsgFromQToTxList (u1SsnIndex);
        ICCH_TRC (ICCH_NOTIF_TRC, "IcchProcessPktFromApp: "
                  "Connection in progress\r\n");
        return;
    }

    switch (ICCH_GET_SSN_INFO (u1SsnIndex).i1ConnStatus)
    {
        case ICCH_SOCK_NOT_CONNECTED:
            if (IcchTcpSockConnect
                (ICCH_GET_SSN_INFO (u1SsnIndex).u4PeerAddr,
                 &i4ConnFd, &i4ConnState) == ICCH_FAILURE)
            {
                ICCH_TRC (ICCH_ALL_FAILURE_TRC, "IcchProcessPktFromApp: "
                          "TCP client connect failed \r\n");
                /* If connection failed, donot clear the queue, instead wait for
                 * the peer to connect, if connection lost with peer,
                 * HB task will intimate about the peer dead
                 * upon this event arrival clear the TX List */
                IcchAddSyncMsgFromQToTxList (u1SsnIndex);
                if (gIcchInfo.u1ConnRetryCnt >= ICCH_MAX_CONN_RETRY_CNT)
                {
                        gIcchInfo.u1ConnRetryCnt = 0;
                }
                /* Start the connection retry timer */
                IcchTmrStartTimer (ICCH_CONN_RETRY_TIMER,
                                   ICCH_CONN_RETRY_TMR_INTERVAL);
                gIcchInfo.u1ConnRetryCnt++;
                return;
            }
            IcchTmrStopTimer (ICCH_CONN_RETRY_TIMER);
            gIcchInfo.u1ConnRetryCnt = 0;
            if (i4ConnState == ICCH_SOCK_CONNECT_IN_PROGRESS)
            {
                ICCH_GET_SSN_INFO (u1SsnIndex).i4ConnFd = i4ConnFd;
                ICCH_GET_SSN_INFO (u1SsnIndex).i1ConnStatus =
                    ICCH_SOCK_CONNECT_IN_PROGRESS;
                IcchAddSyncMsgFromQToTxList (u1SsnIndex);
                return;
            }
            /* This residual memory allocation only be freed in the
             * peer down event handle */
            if (ICCH_GET_SSN_INFO (u1SsnIndex).pResBuf == NULL)
            {
                if (IcchMemAllocForResBuf (&pResBuf) == ICCH_FAILURE)
                {
                    ICCH_TRC (ICCH_ALL_FAILURE_TRC, "IcchProcessPktFromApp: "
                              "Residual buffer allocation failed \r\n");
                    IcchUtilCloseSockConn (u1SsnIndex);
                    return;
                }
                ICCH_GET_SSN_INFO (u1SsnIndex).pResBuf = pResBuf;
            }
            ICCH_GET_SSN_INFO (u1SsnIndex).i4ConnFd = i4ConnFd;
            ICCH_GET_SSN_INFO (u1SsnIndex).i1ConnStatus = ICCH_SOCK_CONNECTED;
            ICCH_TRC1 (ICCH_NOTIF_TRC, "IcchProcessPktFromApp: "
                       "Connection is successful for the peer=%x\r\n",
                        ICCH_GET_SSN_INFO (u1SsnIndex).u4PeerAddr);
            break;

        case ICCH_SOCK_CONNECT_IN_PROGRESS:
      
            ICCH_TRC  (ICCH_NOTIF_TRC, "IcchProcessPktFromApp: "
                       "Sync packet is received when "
                       "in Connection In Progress case\r\n");
            IcchAddSyncMsgFromQToTxList (u1SsnIndex);
            return;

        case ICCH_SOCK_CONNECTED:
            if (ICCH_SLL_COUNT (&ICCH_SSN_ENTRY_TX_LIST
                                (ICCH_GET_SSN_INFO (u1SsnIndex))) != 0)
            {
                IcchAddSyncMsgFromQToTxList (u1SsnIndex);
                ICCH_TRC  (ICCH_NOTIF_TRC, "IcchProcessPktFromApp: "
                           "Pending sync ups exist in the TX List "
                            "so, append this sync data too\r\n");
                IcchTcpWriteCallBackFn (ICCH_GET_SSN_INFO (u1SsnIndex).i4ConnFd);
                return;
            }
            i4ConnFd = ICCH_GET_SSN_INFO (u1SsnIndex).i4ConnFd;
            ICCH_TRC   (ICCH_NOTIF_TRC,
                       "IcchProcessPktFromApp: Connection exists\r\n");
            break;
        default:
            return;
            /* Invalid state, error should be thrown */
    } /* End of switch */

    while (ICCH_RECEIVE_FROM_QUEUE (ICCH_QUEUE_ID, (UINT1 *) &pIcchPktQMsg,
                                    ICCH_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (IcchGetLinearBufAndLenFromPktQMsg (pIcchPktQMsg,
                                               &pu1Data,
                                               &u4IcchPktLen) == ICCH_SUCCESS)
        {
                if (ICCH_SLL_COUNT (&ICCH_SSN_ENTRY_TX_LIST
                                    (ICCH_GET_SSN_INFO (u1SsnIndex))) == 0)
                {
                    /* Send the pkt out to peer thru' RAW socket */
                    if (IcchSendMsg (u1SsnIndex, i4ConnFd,
                                     pu1Data, u4IcchPktLen) == ICCH_FAILURE)
                    {
                        ICCH_TRC (ICCH_ALL_FAILURE_TRC, "IcchProcessPktFromApp: "
                                  "IcchSendMsg -- Failed !!!\r\n");
                    }
                }
                else
                {
                    IcchAddSyncMsgIntoTxList (u1SsnIndex, pu1Data, u4IcchPktLen, 0);
                    IcchTcpWriteCallBackFn (i4ConnFd);
                }
        }
        IcchUtilReleaseMemForPktQMsg (pIcchPktQMsg);
        pIcchPktQMsg = NULL;
        /* This Relinquish point is for processing
         * the higher priority messages */
        u4RelinquishCntr++;
        if (u4RelinquishCntr > ICCH_RELINQUISH_CNTR)
        {
            if (ICCH_SEND_EVENT (ICCH_TASK_ID, ICCH_PKT_FROM_APP) == OSIX_FAILURE)
            {
                ICCH_TRC (ICCH_ALL_FAILURE_TRC, "IcchProcessPktFromApp: Send Event "
                          "ICCH_PKT_FROM_APP Failed\r\n");
            }
            break;
        }
    }
}

/******************************************************************************
 * Function           : IcchGetLinearBufAndLenFromPktQMsg
 * Input(s)           : pIcchPktQMsg - Packet Q message
 *                      ppu1Data - Pointer to pointer to the linear sync data
 *                      pu4IcchPktLen - Pointer to the Sync packet data length
 * Output(s)          : None
 * Returns            : ICCH_SUCCESS/ICCH_FAILUIRE
 * Action             : Gets the linear buffer and length from packet Queue.
 ******************************************************************************/
INT4
IcchGetLinearBufAndLenFromPktQMsg (tIcchPktQMsg * pIcchPktQMsg,
                                 UINT1 **ppu1Data, UINT4 *pu4IcchPktLen)
{
    tIcchMsg           *pIcchMsg = NULL;
    UINT1              *pu1Data = NULL;
    UINT4               u4IcchPktLen = 0;
    UINT2               u2Cksum = 0;

    /* For ICCH_DYNAMIC_SYNC_MSG */
    pIcchMsg = (tIcchMsg *) (pIcchPktQMsg->pIcchMsg);
    /* Get the length of ICCH pkt to be sent */
    ICCH_GET_DATA_4_BYTE (pIcchMsg, ICCH_HDR_OFF_TOT_LENGTH, u4IcchPktLen);
    /* Allocate memory for linear buffer */
    if (u4IcchPktLen > ICCH_MAX_SYNC_PKT_LEN)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                  "Icch linear buf size exceeded!!!\r\n");
        return (ICCH_FAILURE);
    }

    if ((pu1Data = MemAllocMemBlk (gIcchInfo.IcchPktMsgPoolId)) == NULL)
    {
         ICCH_TRC (ICCH_ALL_FAILURE_TRC, 
                   "IcchProcessPktFromApp: "
                   "Icch linear buf allocation - failure !!!\r\n");
        return (ICCH_FAILURE);
    }

    /* Copy the ICCH info to the pkt. */
    /* Copy the CRU buf to linear buf and free the CRU buf */
    if (ICCH_COPY_TO_LINEAR_BUF (pIcchMsg, pu1Data, 0, u4IcchPktLen) 
        == CRU_FAILURE)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC, 
                  "IcchProcessPktFromApp: "
                  "Copy from CRU buf to linear buf failed\r\n");
        MemReleaseMemBlock (gIcchInfo.IcchPktMsgPoolId, (UINT1 *) pu1Data);
        return (ICCH_FAILURE);
    }
    ICCH_TRC1 (ICCH_PKT_DUMP_TRC,"IcchProcessPktFromApp: "
               "Sync packet size to be sent with ICCH header=%d\r\n",u4IcchPktLen);

    u2Cksum = ICCH_LINEAR_CALC_CKSUM ((const INT1 *) pu1Data, u4IcchPktLen);
    PTR_ASSIGN2 ((pu1Data + ICCH_HDR_OFF_CKSUM), u2Cksum);
    *ppu1Data = pu1Data;
    *pu4IcchPktLen = u4IcchPktLen;

    /* Dumping the sync messsage before transmission */
    ICCH_PKT_DUMP (ICCH_PKT_DUMP_TRC, pu1Data, u4IcchPktLen);

    return (ICCH_SUCCESS);
}

/******************************************************************************
 * Function           : IcchAddSyncMsgFromQToTxList
 * Input(s)           : u1SsnIndex - Session index
 * Output(s)          : None
 * Returns            : None
 * Action             : Adds the ICCH sync message from Packet Q to TX List
 ******************************************************************************/
VOID
IcchAddSyncMsgFromQToTxList (UINT1 u1SsnIndex)
{
    tIcchPktQMsg       *pIcchPktQMsg = NULL;
    UINT1              *pu1Data = NULL;
    UINT4               u4IcchPktLen = 0;

    /* Pending nodes are exist,
     * so add the message to be sent in the Tx List */
    while (ICCH_RECEIVE_FROM_QUEUE (ICCH_QUEUE_ID, (UINT1 *) &pIcchPktQMsg,
                                    ICCH_DEF_MSG_LEN, OSIX_NO_WAIT) 
                                    == OSIX_SUCCESS)
    {
        if (IcchGetLinearBufAndLenFromPktQMsg (pIcchPktQMsg,
                                               &pu1Data,
                                               &u4IcchPktLen) == ICCH_SUCCESS)
        {
            IcchAddSyncMsgIntoTxList (u1SsnIndex, pu1Data, u4IcchPktLen, 0);
        }
        IcchUtilReleaseMemForPktQMsg (pIcchPktQMsg);
        pIcchPktQMsg = NULL;
    }
}

/******************************************************************************
 * Function           : IcchCtrlMsgHandler
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : This routine processes the peer information, node state
 *                      related information and ACK from protocols.
 ******************************************************************************/
VOID
IcchCtrlMsgHandler (VOID)
{
    tIcchCtrlQMsg       *pCtrlIcchMsg = NULL;
    UINT1               u1SsnIndex = 0;

    while (ICCH_RECEIVE_FROM_QUEUE (ICCH_CTRL_QUEUE_ID, (UINT1 *) &pCtrlIcchMsg,
                                    ICCH_DEF_MSG_LEN, OSIX_NO_WAIT) 
                                    == OSIX_SUCCESS)
    {
        switch (pCtrlIcchMsg->u4MsgType)
        {
            case ICCH_PEER_INFO_MSG:
                /* Peer id (ip address), status (Peer up/down) are sent through
                 * peer control message*/
                IcchPeerInfoMsgHandle (&(pCtrlIcchMsg->PeerCtrlMsg));
                break;
            case ICCH_WR_SOCK_MSG:
                /* Socket writable will be called when
                 * the socket send buffer is available or
                 * ack received from peer for socket connect
                 * 'connection in progress' case " */
                IcchSockWritableHandle (pCtrlIcchMsg->SktCtrlMsg.i4SockId);
                break;
            case ICCH_PROTO_ACK_MSG:
                ICCH_TRC (ICCH_SYNC_MSG_TRC,
                          "IcchCtrlMsgHandler: ICCH_PROTO_ACK_MSG\r\n");
                /* Protocol Acknowledgement for the processed sync-up message */
                IcchUtilHandleProtocolAck (pCtrlIcchMsg->ProtoAckCtrlMsg);
                break;
            case ICCH_RD_SOCK_MSG:
                /* Icch pkt rcvd in reliable socket -
                 * processing the msgs are handled here */
                if (IcchGetSsnIndexFromConnFd (pCtrlIcchMsg->SktCtrlMsg.i4SockId,
                                               &u1SsnIndex) == ICCH_FAILURE)
                {
                    ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                              "IcchCtrlMsgHandler: Session does not exist "
                              "for the socket fd\r\n");
                    break;
                }
                IcchProcessRcvEvt (&(ICCH_GET_SSN_INFO (u1SsnIndex)));
                break;
            default:
                break;
        }
        /* Control message pool is freed here */
        if (MemReleaseMemBlock (gIcchInfo.IcchCtrlMsgPoolId, (UINT1 *) pCtrlIcchMsg)
            != MEM_SUCCESS)
        {
            ICCH_TRC (ICCH_ALL_FAILURE_TRC,
                      "IcchCtrlMsgHandler: Mem release from ctrl msg "
                      "pool failed\r\n");
        }
    }
}

/******************************************************************************
 * Function           : IcchQueEnqCtrlMsg
 * Input(s)           : pIcchCtrlQMsg -> Control Q informations (Peer up/Peer down)
 *                                     socket writable informations etc..
 * Output(s)          : None
 * Returns            : ICCH_SUCCESS/ICCH_FAILURE
 * Action             : Enqueues the control packets to ICCH core module task
 ******************************************************************************/

INT4
IcchQueEnqCtrlMsg (tIcchCtrlQMsg * pIcchCtrlQMsg)
{
    if (ICCH_SEND_TO_QUEUE (ICCH_CTRL_QUEUE_ID,
                            (UINT1 *) &pIcchCtrlQMsg,
                            ICCH_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (MemReleaseMemBlock (gIcchInfo.IcchCtrlMsgPoolId,
                                (UINT1 *) pIcchCtrlQMsg) != MEM_SUCCESS)
        {
            ICCH_TRC (ICCH_ALL_FAILURE_TRC, "IcchQueEnqCtrlMsg : SendToQ - "
                      "Memory release failed\r\n");
        }
        ICCH_TRC (ICCH_ALL_FAILURE_TRC, "IcchQueEnqCtrlMsg : SendToQ failed\r\n");
        return ICCH_FAILURE;
    }
    if (ICCH_SEND_EVENT (ICCH_TASK_ID, ICCH_CTRL_QMSG_EVENT) == OSIX_FAILURE)
    {
        ICCH_TRC (ICCH_ALL_FAILURE_TRC, "IcchQueEnqCtrlMsg: Send Event "
                  "ICCH_CTRL_QMSG_EVENT Failed\r\n");
        return ICCH_FAILURE;
    }
    return ICCH_SUCCESS;
}

/******************************************************************************
 * Function           : IcchClearSyncMsgQueue
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : Clears the ICCH sync messages from ICCH packet Queue
 ******************************************************************************/
VOID
IcchClearSyncMsgQueue (VOID)
{
    tIcchPktQMsg         *pIcchPktQMsg = NULL;

    /* Realease the memory for the packet in queue and clear the queue */
    while (ICCH_RECEIVE_FROM_QUEUE (ICCH_QUEUE_ID, (UINT1 *) &pIcchPktQMsg,
                                    ICCH_DEF_MSG_LEN, OSIX_NO_WAIT) 
                                    == OSIX_SUCCESS)
    {
        IcchUtilReleaseMemForPktQMsg (pIcchPktQMsg);
    }

    IcchRxResetRxBuffering ();
    return;
}

