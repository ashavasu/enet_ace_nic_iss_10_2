####################################################
# Copyright (C) 2015 Aricent Inc . All Rights Reserved
#
# $Id: make.h,v 1.1.1.1 2015/01/29 11:28:17 siva Exp $
#
# Description: Linux make.h for ICCH.
####################################################

MODULE_NAME = FutureICCH

ICCH_BASE_DIR = $(BASE_DIR)/icch
ICCH_SRC_DIR = $(ICCH_BASE_DIR)/src
ICCH_INC_DIR = $(ICCH_BASE_DIR)/inc
ICCH_OBJ_DIR = $(ICCH_BASE_DIR)/obj

## currently there are no compilation switches
ICCH_COMPILATION_SWITCHES = -UFORTIFY

ICCH_INCLUDE_FILES = $(ICCH_INC_DIR)/icchdefn.h \
                $(ICCH_INC_DIR)/icchmacs.h  \
                $(ICCH_INC_DIR)/icchprot.h  \
                $(ICCH_INC_DIR)/icchtrc.h  \
                $(ICCH_INC_DIR)/icchincs.h \
                $(ICCH_INC_DIR)/icchglob.h

ICCH_INCLUDE_DIRS = -I$(ICCH_INC_DIR) \
                   $(COMMON_INCLUDE_DIRS)
                   

ifneq (${TARGET_OS},OS_VXWORKS)
ifeq (${ICCH_LNXIP},YES)
ICCH_COMPILATION_SWITCHES += -USLI_WANTED -DBSDCOMP_SLI_WANTED \
                           -DIS_SLI_WRAPPER_MODULE
endif
endif

ICCH_DEPENDENCIES = $(COMMON_DEPENDENCIES)  \
               $(ICCH_INCLUDE_FILES)     \
               $(ICCH_BASE_DIR)/Makefile \
               $(ICCH_BASE_DIR)/make.h 
