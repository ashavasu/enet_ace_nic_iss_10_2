/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: icchprot.h,v 1.13 2017/12/29 09:30:21 siva Exp $
*
* Description: Prototypes for the functions defined in ICCH module.
*********************************************************************/
/* Function Prototypes */ 
/* From icchapi.c */
VOID IcchApiSetNoLock (VOID);
VOID IcchApiSetLock (VOID);

VOID IcchDisableDynamicSyncup PROTO ((VOID));
UINT4 IcchRetrieveNodeState  PROTO ((VOID));
INT4 IcchApiSyncStaticConfig PROTO ((tIcchNotifConfChg * pIcchNotifConfChg ));
INT4 IcchApiConfAddToFailBuff (tSNMP_OID_TYPE * pOID , tSNMP_MULTI_DATA_TYPE * pVal );
INT1 IcchApiConfIsOIDInFilterList (tSNMP_OID_TYPE * pOID );
VOID IcchApiSystemRestart  PROTO ((VOID));
UINT4 IcchApiAllProtocolRestart  PROTO ((VOID));
VOID IcchApiIncrementRebootCntr  PROTO ((VOID));
VOID IcchExtIcchSetPeerNodeStatus (UINT4 u4PeerNodeId , UINT4 u4NodeStatus );
VOID IcchExtIcchSetNodeState (UINT4 u4IcchNodeState );
VOID IcchExtIcchSetForceSwitchOverFlag (UINT1 u1Flag );
UINT1 IcchGetHRFlag  PROTO ((VOID));

/* From icchmain.c */
UINT4 IcchTaskInit  PROTO ((VOID));
UINT4 IcchInit  PROTO ((VOID));
VOID IcchDeInit  PROTO ((VOID));
VOID IcchPeerInfoMsgHandle (tIcchPeerCtrlMsg * pIcchPeerCtrlMsg);
tIcchMsg * IcchAllocTxBuf (UINT4 u4SyncMsgSize);
VOID IcchUpdatePeerNodeCntToProtocols (UINT1 u1Event);
VOID IcchHbRegisterApps (VOID);
VOID IcchPrcsPendingRxBufMsgs (VOID);

/* From icchque.c */
VOID IcchProcessPktFromApp  PROTO ((VOID));
INT4 IcchGetLinearBufAndLenFromPktQMsg PROTO ((tIcchPktQMsg * pIcchPktQMsg , UINT1 **ppu1Data , UINT4 *pu4IcchPktLen ));
VOID IcchAddSyncMsgFromQToTxList (UINT1 u1SsnIndex );
VOID IcchCtrlMsgHandler  PROTO ((VOID));
INT4 IcchQueEnqCtrlMsg (tIcchCtrlQMsg * pIcchCtrlQMsg );
VOID IcchClearSyncMsgQueue (VOID);

/* From icchrxbuf.c */
INT4 IcchRxBufAddPkt (UINT1 *pu1Pkt , UINT4 u4SeqNum , UINT4 u4PktLen );
tIcchRxBufHashEntry * IcchRxBufSearchPkt (UINT4 u4SeqNum );
INT4 IcchRxBufDeleteNode (tIcchRxBufHashEntry * pRxBufNode );
INT4 IcchRxBufCreate  PROTO ((VOID));
VOID IcchRxBufDestroy  PROTO ((VOID));
VOID IcchRxBufFreeHashNode (tTMO_HASH_NODE * pNode );
VOID IcchRxBufClearRxBuffer  PROTO ((VOID));

/* From icchrx.c */
INT4 IcchRxProcessBufferedPkt  PROTO ((VOID));
INT4 IcchRxPostBufferedPktToProtocol (tIcchRxBufHashEntry * pRxBufNode );
VOID IcchRxSeqNumMisMatchHandler (UINT4 u4SeqNum );
VOID IcchRxResetRxBuffering  PROTO ((VOID));
UINT4 IcchProcessRcvEvt (tIcchSsnInfo * pIcchSsnInfo );
UINT4 IcchHandleResBufData (UINT1 *pu1Start , tIcchSsnInfo * pIcchSsnInfo , UINT2 *pu2HeaderLen , UINT4 *pu4DataLen );
UINT4 IcchRecvIcchHdrOrPayLoadFromConnFd (UINT1 **ppu1Data , UINT1 *pu1Start , tIcchSsnInfo * pIcchSsnInfo , UINT2 u2MsgLen , BOOL1 * pbIsPartialDataRcvd );
UINT4 IcchHandleCompleteSyncMsg (UINT1 *pu1IcchSyncMsg , INT4 i4IcchSyncMsgLen );
UINT4 IcchProcessSyncMsg (UINT1 *pu1IcchSyncMsg , INT4 i4IcchSyncMsgLen , UINT4 u4DestEntId );

/* From icchsock.c */
INT4 IcchTcpSockConnect (UINT4 u4PeerAddr , INT4 *pi4Connfd , INT4 *pi4ConnState );
INT4 IcchCloseSocket (INT4 i4SockFd );
UINT4 IcchTcpSockSend (INT4 i4ConnFd , UINT1 *pu1Data , UINT2 u2PktLen , INT4 *pi4WrBytes );
UINT4 IcchTcpSockRcv (INT4 i4ConnFd , UINT1 *pu1Data , UINT2 u2MsgLen , INT4 *pi4RdBytes );
VOID IcchTcpSrvSockCallBk (INT4 i4SockFd);
INT4 IcchTcpAcceptNewConnection (INT4 i4SrvSockFd, INT4 *pi4SockFd, UINT4 *pu4PeerAddr);
INT4 IcchTcpSrvSockInit (INT4 *pi4SrvSockFd);

/* From icchsync.c */
VOID IcchDataSyncUpTaskMain (INT1 *pi1Arg );
VOID IcchDeqSyncupMsg PROTO ((VOID));
INT4 IcchSyncupDBLock  PROTO ((VOID));
INT4 IcchSyncupDBUnLock  PROTO ((VOID));

/* From icchtmr.c */
UINT4 IcchTmrInit  PROTO ((VOID));
UINT4 IcchTmrDeInit  PROTO ((VOID));
VOID IcchTmrExpHandler  PROTO ((VOID));
UINT4 IcchTmrStartTimer (UINT1 u1TmrId , UINT4 u4TmrVal );
UINT4 IcchTmrStopTimer (UINT1 u1TmrId );
VOID IcchHandleSeqRecovTmrExpiry (VOID *pArg );
VOID IcchHandleConnRetryTmrExpiry (VOID *pArg );
VOID IcchHandleAckRecovTmrExpiry (VOID *pArg );
VOID IcchHandleBulkUpdateTmrExpiry (VOID *pArg);

/* From icchtx.c */
UINT4 IcchSendMsg (UINT1 u1SsnIndex , INT4 i4ConnFd , UINT1 *pu1Data , UINT4 u4TotLen );
VOID IcchSockWritableHandle (INT4 i4ConnFd );

/* From icchutil.c */
INT4 IcchLock  PROTO ((VOID));
INT4 IcchUnLock  PROTO ((VOID));
UINT1 IcchGetSsnIndexFromConnFd (INT4 i4ConnFd , UINT1 *pu1SsnIndex );
VOID IcchTcpWriteCallBackFn (INT4 i4SockFd );
VOID IcchPktRcvd (INT4 i4SockFd );
VOID IcchAddSyncMsgIntoTxList (UINT1 u1SsnIndex , UINT1 *pu1Data , UINT4 u4IcchPktLen , UINT4 u4IcchMsgOffset );
INT1 IcchUtilReleaseMemForPktQMsg (tIcchPktQMsg * pIcchPktQMsg);
UINT1 IcchMemAllocForResBuf (UINT1 **ppResBuf );
VOID IcchUtilCloseSockConn (UINT1 u1SsnIndex );
VOID IcchUtilHandleProtocolAck (tIcchProtoAck ProtoAck );
INT4 IcchUtilPostDataOrEvntToProtocol (UINT4 u4DestEntId , UINT1 u1Event , tIcchMsg * pPkt , UINT4 u4PktLen , UINT4 u4SeqNum );
UINT1 IcchGetSsnIndexFromPeerAddr (UINT4 u4PeerAddr, UINT1 *pu1SsnIndex);
VOID IcchTcpUpdateSsnTable (INT4 i4CliSockFd, UINT4 u4IpAddr);
VOID IcchSetTxEnable (UINT4 );
UINT1 IcchHandleProtocolEvent (tIcchProtoEvt * pEvt);
UINT1 IcchUtilTriggerNextHigherLayer (UINT4 u4AppId, UINT1 u1Event);
UINT1 IcchUtilGetNextAppId (UINT4 u4AppId, UINT4 *pu4NextId);
INT4 IcchCheckBulkUpdatesStatus (VOID);
VOID IcchHandleInitBulkUpdate (VOID);
VOID IcchUtilCheckAndCompleteBulkUpdt (VOID);
VOID IcchNotifyProtocols (UINT1 u1Event);
VOID IcchHandleNodeStateUpdate (UINT1 u1State);
UINT1 IcchLoadConfsFromConfFile(VOID);
UINT1
IcchUpdateIcclConfFile(tIcchSessInfo *IcchSessionInfo, UINT1 u1IsWriteOrDel);
VOID IcchNotifyMCLAGStatus (UINT1 u1Event);
/* From icchtrap.c */
VOID IcchTrapSendNotifications (UINT4);

/* From icchtrc.c */
VOID IcchTrcHandle (UINT4 u4FlagVal, CONST CHR1 *pc1Fmt, ...);
VOID
IcchVlanDeleteRemoteFdb (VOID);
VOID
IcchVlanSendBulkReqMsg (VOID);
VOID
IcchPktDumpTrc (UINT1 *pu1Buf, UINT4 u4Length);
