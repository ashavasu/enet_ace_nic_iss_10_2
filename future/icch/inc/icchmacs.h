/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: icchmacs.h,v 1.5 2015/05/18 13:18:05 siva Exp $
*
* Description: ICCH Macro definitions
*********************************************************************/
#ifndef _ICCH_MACS_H
#define _ICCH_MACS_H

/* Tx buffer related macros */
#define ICCH_TX_MSG_BUF_NODE_OFFSET(x)   (x->u4MsgOffset)
#define ICCH_TX_MSG_BUF_NODE_MSG_SIZE(x) (x->u4MsgSize)
#define ICCH_TX_MSG_BUF_NODE_MSG(x)      (x->pu1Msg)

/*------------------------------------------------------------------*/
/*-----------RX Memory operations related macros--------------------*/
/* Allocate CRU buf and set the valid offset to point to 0 */
#define ICCH_ALLOC_RX_BUF(size)    CRU_BUF_Allocate_MsgBufChain (size, 0)

/* Macro to prepend to ICCH Hdr to data */
#define ICCH_PREPEND_BUF(pBuf, pSrc, u4Size) \
        CRU_BUF_Prepend_BufChain(pBuf, pSrc, u4Size)

#define ICCH_RXBUF_PKT_MEM_ALLOC(pu1Ptr, i4PktLen) \
{\
    if (MemAllocateMemBlock (gIcchInfo.IcchRxBufPktPoolId, \
                             (UINT1 **) &pu1Ptr) == MEM_FAILURE) \
    { \
        pu1Ptr = NULL; \
    } \
}

#define ICCH_RXBUF_PKT_MEM_FREE(pu1Ptr) \
{\
    MemReleaseMemBlock (gIcchInfo.IcchRxBufPktPoolId, \
                        pu1Ptr);\
    pu1Ptr = NULL;\
}

#define ICCH_RXBUF_PKT_MEMSET(pu1Ptr, Const, u4Count) \
    MEMSET(pu1Ptr, Const, u4Count)

#define ICCH_RX_STAT_SYNC_MSG_RECVD() \
    gIcchInfo.IcchRxStats.u4SyncMsgRxCount
#define ICCH_RX_STAT_SYNC_MSG_PROCESSED() \
    gIcchInfo.IcchRxStats.u4SyncMsgProcCount
#define ICCH_RX_STAT_SYNC_MSG_MISSED() \
    gIcchInfo.IcchRxStats.u4SyncMsgMissedCount
#define ICCH_RX_STAT_STATIC_CONF_SYNC_FAILED() \
    gIcchInfo.IcchRxStats.u4SyncMsgStaticConfFailCount

#define   ICCH_HTONS(x)       OSIX_HTONS(x)
#define ICCH_RX_GET_HASH_INDEX(u4SeqNumber) \
    (u4SeqNumber % ICCH_RXBUF_MAX_HASH_BUCKET_COUNT)

#define ICCH_TX_GETNEXT_VALID_SEQNUM() \
    ((ICCH_TX_LAST_SEQ_NUM_PROCESSED() == ICCH_SEQNUM_WRAP_AROUND_VALUE)? \
     1 : (ICCH_TX_LAST_SEQ_NUM_PROCESSED() + 1))


#define ICCH_RX_GETNEXT_VALID_SEQNUM() \
    ((ICCH_RX_LAST_SEQ_NUM_PROCESSED() == ICCH_SEQNUM_WRAP_AROUND_VALUE)? \
     1 : (ICCH_RX_LAST_SEQ_NUM_PROCESSED() + 1))

#define ICCH_RX_INCR_LAST_SEQNUM_PROCESSED()  \
    ((ICCH_RX_LAST_SEQ_NUM_PROCESSED() == ICCH_SEQNUM_WRAP_AROUND_VALUE)?\
     (ICCH_RX_LAST_SEQ_NUM_PROCESSED() = 1) : ICCH_RX_LAST_SEQ_NUM_PROCESSED()++)

#define ICCH_TX_INCR_LAST_SEQNUM_PROCESSED()  \
    ((ICCH_TX_LAST_SEQ_NUM_PROCESSED() == ICCH_SEQNUM_WRAP_AROUND_VALUE)?\
     (ICCH_TX_LAST_SEQ_NUM_PROCESSED() = 1) : ICCH_TX_LAST_SEQ_NUM_PROCESSED()++)



#define ICCH_RX_SEQ_MISSMATCH_FLG() gIcchInfo.bSeqMissMatch
#define ICCH_IS_RX_BUFF_PROCESSING_ALLOWED()  gIcchInfo.bIsRxBufProcAllowed
#define ICCH_RX_LAST_ACK_RCVD_SEQ_NUM() gIcchInfo.u4RxAckRcvdSeqNum
#define ICCH_RX_LAST_APPID_PROCESSED() gIcchInfo.u4RxLastAppId

#define ICCH_RX_BUFF_GET_HASHNODE_OFFSET() \
    FSAP_OFFSETOF(tIcchRxBufHashEntry, RxBufHashNode)

#define ICCH_GET_SELF_NODE_ID()         (gIcchSessionInfo[ICCH_DEFAULT_INST].u4IpAddress)
#define ICCH_GET_NODE_STATE()           (gIcchSessionInfo[ICCH_DEFAULT_INST].i4SessionNodeState)
#define ICCH_GET_PREV_NODE_STATE()      (gIcchInfo.u4PrevNodeState)
#define ICCH_GET_PEER_NODE_ID           gIcchInfo.u4PeerNodeId
#define ICCH_SET_NODE_STATE(state)      (gIcchSessionInfo[ICCH_DEFAULT_INST].i4SessionNodeState = state)
#define ICCH_SET_PREV_NODE_STATE(state) \
                                (gIcchInfo.u4PrevNodeState = state)
#define ICCH_SET_PEER_NODE_ID(x)        (gIcchInfo.u4PeerNodeId = x)
#define ICCH_SET_PEER_NODE_STATE(x)     (gIcchInfo.u4PeerNodeState = x)
#define ICCH_GET_TRC_LEVEL              gIcchInfo.u4IcchTrc
#define ICCH_PEER_NODE_COUNT()          (gIcchInfo.u1NumPeers)
#define ICCH_TX_STAT_SYNC_MSG_FAILED()  \
                                (gIcchInfo.IcchTxStats.u4SyncMsgTxFailedCount)
#define ICCH_TX_STAT_SYNC_MSG_SENT()    (gIcchInfo.IcchTxStats.u4SyncMsgTxCount)
#define ICCH_SET_MASTER_NODE_ID(NodeIp) (gIcchInfo.u4MasterNode = NodeIp)
#define ICCH_GET_PEER_NODE_STATE     gIcchInfo.u4PeerNodeState

#define ICCH_INCR_TX_STAT_SYNC_MSG_SENT()  \
{ \
    if (gIcchInfo.i4StatsEnable == ICCH_STATS_ENABLE) \
    { \
        (gIcchInfo.IcchTxStats.u4SyncMsgTxCount) ++; \
    } \
}

#define ICCH_INCR_TX_STAT_SYNC_MSG_FAILED()  \
{ \
    if (gIcchInfo.i4StatsEnable == ICCH_STATS_ENABLE) \
    { \
        (gIcchInfo.IcchTxStats.u4SyncMsgTxFailedCount) ++; \
    } \
}

#define ICCH_INCR_RX_STAT_SYNC_MSG_RECVD()  \
{ \
    if (gIcchInfo.i4StatsEnable == ICCH_STATS_ENABLE) \
    { \
        (gIcchInfo.IcchRxStats.u4SyncMsgRxCount) ++; \
    } \
}

#define ICCH_INCR_RX_STAT_SYNC_MSG_PROCESSED()  \
{ \
    if (gIcchInfo.i4StatsEnable == ICCH_STATS_ENABLE) \
    { \
        (gIcchInfo.IcchRxStats.u4SyncMsgProcCount) ++; \
    } \
}

#define ICCH_INCR_RX_STAT_SYNC_MSG_MISSED()  \
{ \
    if (gIcchInfo.i4StatsEnable == ICCH_STATS_ENABLE) \
    { \
        (gIcchInfo.IcchRxStats.u4SyncMsgMissedCount) ++; \
    } \
}

#endif /* _ICCH_MACS_H */

