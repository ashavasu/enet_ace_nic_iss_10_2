/*$Id: icchsz.h,v 1.1.1.1 2015/01/29 11:28:17 siva Exp $*/

enum {
    MAX_ICCH_CTRL_MSG_NODES_SIZING_ID,
    MAX_ICCH_PKTQ_MSG_SIZING_ID,
    MAX_ICCH_TX_BUF_NODES_SIZING_ID,
    MAX_ICCH_RX_BUF_NODES_SIZING_ID,
    MAX_ICCH_RX_BUF_PKT_NODES_SIZING_ID,
    MAX_ICCH_PKT_MSG_BLOCKS_SIZING_ID,
    MAX_ICCH_APPS_SIZING_ID,
    MAX_ICCH_NODE_INFO_BLOCKS_SIZING_ID,
    ICCH_MAX_SIZING_ID
};

#ifdef  _ICCHSZ_C
tMemPoolId IcchMemPoolIds[ICCH_MAX_SIZING_ID];
INT4  IcchSizingMemCreateMemPools(VOID);
VOID  IcchSizingMemDeleteMemPools(VOID);
INT4  IcchSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else
extern tMemPoolId IcchMemPoolIds[];
extern INT4  IcchSizingMemCreateMemPools(VOID);
extern VOID  IcchSizingMemDeleteMemPools(VOID);
#endif

#ifdef  _ICCHSZ_C
tFsModSizingParams FsICCHSizingParams [] = {
{ "tIcchCtrlQMsg", "MAX_ICCH_CTRL_MSG_NODES", sizeof(tIcchCtrlQMsg),MAX_ICCH_CTRL_MSG_NODES, MAX_ICCH_CTRL_MSG_NODES,0 },
{ "tIcchPktQMsg", "MAX_ICCH_PKTQ_MSG", sizeof(tIcchPktQMsg),MAX_ICCH_PKTQ_MSG, MAX_ICCH_PKTQ_MSG,0 },
{ "tIcchTxBufNode", "MAX_ICCH_TX_BUF_NODES", sizeof(tIcchTxBufNode),MAX_ICCH_TX_BUF_NODES, MAX_ICCH_TX_BUF_NODES,0 },
{ "tIcchRxBufHashEntry", "MAX_ICCH_RX_BUF_NODES", sizeof(tIcchRxBufHashEntry),MAX_ICCH_RX_BUF_NODES, MAX_ICCH_RX_BUF_NODES,0 },
{ "tIcchPktBlock", "MAX_ICCH_RX_BUF_PKT_NODES", sizeof(tIcchPktBlock),MAX_ICCH_RX_BUF_PKT_NODES, MAX_ICCH_RX_BUF_PKT_NODES,0 },
{ "tIcchPktBlock", "MAX_ICCH_PKT_MSG_BLOCKS", sizeof(tIcchPktBlock),MAX_ICCH_PKT_MSG_BLOCKS, MAX_ICCH_PKT_MSG_BLOCKS,0 },
{ "tIcchAppInfo", "MAX_ICCH_APPS", sizeof(tIcchAppInfo),MAX_ICCH_APPS, MAX_ICCH_APPS,0 },
{ "tIcchNodeInfo", "MAX_ICCH_NODE_INFO_BLOCKS", sizeof(tIcchNodeInfo),MAX_ICCH_NODE_INFO_BLOCKS, MAX_ICCH_NODE_INFO_BLOCKS,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _ICCHSZ_C  */
extern tFsModSizingParams FsICCHSizingParams [];
#endif /*  _ICCHSZ_C  */

