/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: icchglob.h,v 1.5 2016/01/20 10:53:16 siva Exp $
*
* Description: This file contains the global variable declarations
*              used in ICCH Module
*********************************************************************/
#ifndef _ICCHGLOB_H
#define _ICCHGLOB_H
#ifdef _ICCHMAIN_C_
tICCHInfo             gIcchInfo;
UINT4 gu4IcchBulkUpdtInProgress = ICCH_FALSE;
UINT4 gu4IcchBulkUpdtReqNotSent = ICCH_FALSE;
tIcchSessInfo gIcchSessionInfo[ICCH_MAX_SESSIONS];
#else
extern tICCHInfo      gIcchInfo;
extern UINT4      gu4IcchBulkUpdtInProgress;
extern UINT4      gu4IcchBulkUpdtReqNotSent;
extern tIcchSessInfo gIcchSessionInfo[ICCH_MAX_SESSIONS];
#endif /*  _ICCHMAIN_C_ */
#endif /* _ICCHGLOB_H */

