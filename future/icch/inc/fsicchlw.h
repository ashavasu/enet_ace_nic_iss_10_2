/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: fsicchlw.h,v 1.7 2017/09/25 12:11:45 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIcchTrcLevel ARG_LIST((UINT4 *));

INT1
nmhGetFsIcchStatsEnable ARG_LIST((INT4 *));

INT1
nmhGetFsIcchClearStats ARG_LIST((INT4 *));

INT1
nmhGetFsIcchEnableProtoSync ARG_LIST((UINT4 *));

INT1
nmhGetFsIcchFetchRemoteFdb ARG_LIST((INT4 *));

INT1
nmhGetFsIcchPeerNodeIpAddress ARG_LIST((UINT4 *));

INT1
nmhGetFsIcchPeerNodeState ARG_LIST((INT4 *));

INT1
nmhGetFsIcchOverrideLocalAffinity ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIcchTrcLevel ARG_LIST((UINT4 ));

INT1
nmhSetFsIcchStatsEnable ARG_LIST((INT4 ));

INT1
nmhSetFsIcchClearStats ARG_LIST((INT4 ));

INT1
nmhSetFsIcchEnableProtoSync ARG_LIST((UINT4 ));

INT1
nmhSetFsIcchFetchRemoteFdb ARG_LIST((INT4 ));

INT1
nmhSetFsIcchOverrideLocalAffinity ARG_LIST((INT4 ));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIcchTrcLevel ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsIcchStatsEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIcchClearStats ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIcchEnableProtoSync ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsIcchFetchRemoteFdb ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIcchOverrideLocalAffinity ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIcchTrcLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIcchStatsEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIcchClearStats ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIcchEnableProtoSync ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIcchFetchRemoteFdb ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIcchOverrideLocalAffinity ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


/* Proto Validate Index Instance for FsIcclSessionTable. */
INT1
nmhValidateIndexInstanceFsIcclSessionTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIcclSessionTable  */

INT1
nmhGetFirstIndexFsIcclSessionTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIcclSessionTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIcclSessionInterface ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsIcclSessionIpAddress ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsIcclSessionSubnetMask ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsIcclSessionVlan ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsIcclSessionNodeState ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsIcclSessionRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIcclSessionInterface ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsIcclSessionIpAddress ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsIcclSessionSubnetMask ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsIcclSessionVlan ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsIcclSessionRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIcclSessionInterface ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsIcclSessionIpAddress ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsIcclSessionSubnetMask ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsIcclSessionVlan ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsIcclSessionRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIcclSessionTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIcchStatsSyncMsgTxCount ARG_LIST((UINT4 *));

INT1
nmhGetFsIcchStatsSyncMsgTxFailedCount ARG_LIST((UINT4 *));

INT1
nmhGetFsIcchStatsSyncMsgRxCount ARG_LIST((UINT4 *));

INT1
nmhGetFsIcchStatsSyncMsgProcCount ARG_LIST((UINT4 *));

INT1
nmhGetFsIcchStatsSyncMsgMissedCount ARG_LIST((UINT4 *));
