/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: fsicchdb.h,v 1.9 2017/09/25 12:11:45 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSICCHDB_H
#define _FSICCHDB_H

UINT1 FsIcclSessionTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsicch [] ={1,3,6,1,4,1,29601,2,94};
tSNMP_OID_TYPE fsicchOID = {9, fsicch};


UINT4 FsIcchTrcLevel [ ] ={1,3,6,1,4,1,29601,2,94,1,1};
UINT4 FsIcchStatsEnable [ ] ={1,3,6,1,4,1,29601,2,94,1,2};
UINT4 FsIcchClearStats [ ] ={1,3,6,1,4,1,29601,2,94,1,3};
UINT4 FsIcchEnableProtoSync [ ] ={1,3,6,1,4,1,29601,2,94,1,4};
UINT4 FsIcchFetchRemoteFdb [ ] ={1,3,6,1,4,1,29601,2,94,1,5};
UINT4 FsIcchPeerNodeIpAddress [ ] ={1,3,6,1,4,1,29601,2,94,1,6};
UINT4 FsIcchPeerNodeState [ ] ={1,3,6,1,4,1,29601,2,94,1,7};
UINT4 FsIcchOverrideLocalAffinity [ ] ={1,3,6,1,4,1,29601,2,94,1,8};
UINT4 FsIcclSessionInstanceId [ ] ={1,3,6,1,4,1,29601,2,94,4,1,1,1};
UINT4 FsIcclSessionInterface [ ] ={1,3,6,1,4,1,29601,2,94,4,1,1,2};
UINT4 FsIcclSessionIpAddress [ ] ={1,3,6,1,4,1,29601,2,94,4,1,1,3};
UINT4 FsIcclSessionSubnetMask [ ] ={1,3,6,1,4,1,29601,2,94,4,1,1,4};
UINT4 FsIcclSessionVlan [ ] ={1,3,6,1,4,1,29601,2,94,4,1,1,5};
UINT4 FsIcclSessionNodeState [ ] ={1,3,6,1,4,1,29601,2,94,4,1,1,6};
UINT4 FsIcclSessionRowStatus [ ] ={1,3,6,1,4,1,29601,2,94,4,1,1,7};
UINT4 FsIcchStatsSyncMsgTxCount [ ] ={1,3,6,1,4,1,29601,2,94,2,1};
UINT4 FsIcchStatsSyncMsgTxFailedCount [ ] ={1,3,6,1,4,1,29601,2,94,2,2};
UINT4 FsIcchStatsSyncMsgRxCount [ ] ={1,3,6,1,4,1,29601,2,94,2,3};
UINT4 FsIcchStatsSyncMsgProcCount [ ] ={1,3,6,1,4,1,29601,2,94,2,4};
UINT4 FsIcchStatsSyncMsgMissedCount [ ] ={1,3,6,1,4,1,29601,2,94,2,5};




tMbDbEntry fsicchMibEntry[]= {

{{11,FsIcchTrcLevel}, NULL, FsIcchTrcLevelGet, FsIcchTrcLevelSet, FsIcchTrcLevelTest, FsIcchTrcLevelDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsIcchStatsEnable}, NULL, FsIcchStatsEnableGet, FsIcchStatsEnableSet, FsIcchStatsEnableTest, FsIcchStatsEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsIcchClearStats}, NULL, FsIcchClearStatsGet, FsIcchClearStatsSet, FsIcchClearStatsTest, FsIcchClearStatsDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsIcchEnableProtoSync}, NULL, FsIcchEnableProtoSyncGet, FsIcchEnableProtoSyncSet, FsIcchEnableProtoSyncTest, FsIcchEnableProtoSyncDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsIcchFetchRemoteFdb}, NULL, FsIcchFetchRemoteFdbGet, FsIcchFetchRemoteFdbSet, FsIcchFetchRemoteFdbTest, FsIcchFetchRemoteFdbDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsIcchPeerNodeIpAddress}, NULL, FsIcchPeerNodeIpAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsIcchPeerNodeState}, NULL, FsIcchPeerNodeStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsIcchOverrideLocalAffinity}, NULL, FsIcchOverrideLocalAffinityGet, FsIcchOverrideLocalAffinitySet, FsIcchOverrideLocalAffinityTest, FsIcchOverrideLocalAffinityDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsIcchStatsSyncMsgTxCount}, NULL, FsIcchStatsSyncMsgTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsIcchStatsSyncMsgTxFailedCount}, NULL, FsIcchStatsSyncMsgTxFailedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsIcchStatsSyncMsgRxCount}, NULL, FsIcchStatsSyncMsgRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsIcchStatsSyncMsgProcCount}, NULL, FsIcchStatsSyncMsgProcCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsIcchStatsSyncMsgMissedCount}, NULL, FsIcchStatsSyncMsgMissedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,FsIcclSessionInstanceId}, GetNextIndexFsIcclSessionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsIcclSessionTableINDEX, 1, 0, 0, NULL},

{{13,FsIcclSessionInterface}, GetNextIndexFsIcclSessionTable, FsIcclSessionInterfaceGet, FsIcclSessionInterfaceSet, FsIcclSessionInterfaceTest, FsIcclSessionTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsIcclSessionTableINDEX, 1, 0, 0, "po4095"},

{{13,FsIcclSessionIpAddress}, GetNextIndexFsIcclSessionTable, FsIcclSessionIpAddressGet, FsIcclSessionIpAddressSet, FsIcclSessionIpAddressTest, FsIcclSessionTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsIcclSessionTableINDEX, 1, 0, 0, "2851995905"},

{{13,FsIcclSessionSubnetMask}, GetNextIndexFsIcclSessionTable, FsIcclSessionSubnetMaskGet, FsIcclSessionSubnetMaskSet, FsIcclSessionSubnetMaskTest, FsIcclSessionTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsIcclSessionTableINDEX, 1, 0, 0, "4278190080"},

{{13,FsIcclSessionVlan}, GetNextIndexFsIcclSessionTable, FsIcclSessionVlanGet, FsIcclSessionVlanSet, FsIcclSessionVlanTest, FsIcclSessionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIcclSessionTableINDEX, 1, 0, 0, "4095"},

{{13,FsIcclSessionNodeState}, GetNextIndexFsIcclSessionTable, FsIcclSessionNodeStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsIcclSessionTableINDEX, 1, 0, 0, NULL},

{{13,FsIcclSessionRowStatus}, GetNextIndexFsIcclSessionTable, FsIcclSessionRowStatusGet, FsIcclSessionRowStatusSet, FsIcclSessionRowStatusTest, FsIcclSessionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIcclSessionTableINDEX, 1, 0, 1, NULL},
};
tMibData fsicchEntry = { 20, fsicchMibEntry };


#endif /* _FSICCHDB_H */
