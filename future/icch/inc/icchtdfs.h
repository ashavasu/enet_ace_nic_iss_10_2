/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: icchtdfs.h,v 1.13 2017/09/25 12:11:45 siva Exp $
*
* Description: Data structure used by ICCH module.
*********************************************************************/
#ifndef _ICCHTDFS_H_
#define _ICCHTDFS_H_

/* Application specific info stored in ICCH */
typedef struct {
   UINT4 u4ValidEntry;
   UINT4 u4SrcEntId;
   UINT4 u4DestEntId;

   /* Icch delivers the pkt rcvd to protocols thru' this FnPtr. */
   INT4 (*pFnRcvPkt) (UINT1 u1Event, tCRU_BUF_CHAIN_HEADER *pBuf, UINT2 u2PktLen);
}tIcchAppInfo;

/* TX Statistic counters */
typedef struct  _IcchTxStats
{
    UINT4 u4SyncMsgTxCount; /* Number of sync-up message successfully sent */
    UINT4 u4SyncMsgTxFailedCount; /* Number of sync-up messages failed while
                                     sending */
}tIcchTxStats;

/* RX Statistic counters */
typedef struct  _IcchRxStats
{
   UINT4  u4SyncMsgRxCount; /* Total number of synchronization messages
                               received and added to Rx Buffer */
   UINT4  u4SyncMsgProcCount; /* Total number of synchronization messages
                                 processed from the RX buffer. */
   UINT4  u4SyncMsgMissedCount; /* Total number of synchronization messages
                                   not found in the RX Buffer during search */
}tIcchRxStats;

typedef struct {
    tTMO_SLL        TxList;    /* Tx buffer list */
    UINT1           *pResBuf;  /* Used at sync data reception side to wait
                                 for the complete data. */
    UINT4           u4PeerAddr;  /* Peer address */
    INT4            i4ConnFd;  /* Peer Connection identifier */
    UINT2           u2ResBufLen;    /* Residual buffer length */
    INT1            i1ConnStatus; /* Connection status */
    UINT1           u1Rsvd;
}tIcchSsnInfo;

/* global ICCH data structure */
typedef struct {
    tOsixSemId     IcchProtoSemId; /* ICCH protocol Semaphore Identifier */
    tOsixTaskId    IcchTaskId; /* ICCH Task Identifier */
    tOsixQId       IcchQueueId; /* ICCH message Queue Identifier */
    tOsixQId       IcchCtrlQueueId; /* Queue to handle high priority informations
                                       from HB task */
    /* MemPool for ICCH Registration Tbl */
    tMemPoolId     IcchCtrlMsgPoolId;
    tMemPoolId     IcchPktQMsgPoolId;
    tMemPoolId     IcchTxBufMemPoolId; /* Tx buffer pool id */
    tMemPoolId     IcchRxBufPoolId; /* Rx buffer pool id */
    tMemPoolId     IcchRxBufPktPoolId; /* Rx buffer pool id */
    tMemPoolId     IcchPktMsgPoolId;                         
    tMemPoolId     IcchRegMemPoolId;                         
    tMemPoolId     IcchMsgPoolId;                         
    /* Timers*/
    tTmrDesc       aTmrDesc[ICCH_MAX_TIMER_TYPES];
    /* Timer data struct that contains
       func ptrs for timer handling and
       offsets to identify the data
       struct containing timer block.
       Timer ID is the index to this
       data structure */
    /* ICCH Timer List ID */
    tTimerListId   TmrListId;
    /* ICCH Timer block to hold the timer id */
    tTmrBlk        IcchTmrBlk[ICCH_MAX_TIMER_TYPES];
    tIcchAppList   IcchRegMask; /* Indicates the list of applications
                                   that has registered with ICCH */
    tIcchAppList   IcchBulkUpdSts; /* Indicates the list of applications
                                      in the master node that completes
                                      it's bulk updates with peer node */
    tIcchAppList   IcchBulkUpdStsMask; /* Indicates the list of applications
                                          in the master node that should
                                          complete it's bulk updates */ 
    tIcchAppInfo   *apIcchAppInfo[ICCH_MAX_APPS]; /* Application specific info */
    tIcchSsnInfo   IcchSsnInfo[ICCH_MAX_SYNC_SSNS_LIMIT]; /* Session information */
    tIcchTxStats   IcchTxStats; /* TX Statistics */
    tIcchRxStats   IcchRxStats; /* RX Statistics */
    tTMO_HASH_TABLE    *pRxBufHashTable;/* Pointer to Received sequenced
                                           buffer Hash Table */
    UINT4          u4RxLastSeqNum; /* Last processed seq number for Tx of sync pkt. */
    UINT4          u4TxLastSeqNum; /* Last processed seq number for Rx of sync pkt. */
    UINT4          u4RxAckRcvdSeqNum; /* Last seq number for which the ACK has
                                         been received */
    UINT4          u4RxLastAppId; /* AppId of Last sync-up message processed.
                                     Bootup time Or switchover time Initialized
                                     to 0 */
    UINT4          u4RxBufNodeCount; /* Shows number of entries currently 
                                        buffered in RX Buffer */
    UINT4          u4IcchTrc;
    UINT4          u4IcclIfIndex;
    UINT4          u4MasterNode; /* Ip address of MASTER node */
    UINT4          u4PrevNodeState; /* Previous State of the node */
    UINT4          u4PeerNodeState; /* State of the peer node */
    UINT4          u4PeerNodeId; /* Ip address of peer node */
    UINT4          u4SysLogId;   /* Syslog ID for ICCH module */
    UINT4          u4EnableProtoSync; /* To enable or disable the sync-up 
                                         messages through ICCL */
    UINT4          u4TotalNumOfInst; /* Total number of instances which are active */
    UINT4          u4IcclL3IfIndex;
    INT4           i4StatsEnable; /* Enable status of ICCH statistics */
    INT4           i4SrvSockFd;
    INT4           i4ClearStats;
    INT4           i4OverrideLocalAffinity;
    UINT1          u1IsBulkUpdtInProgress; /* Flag to indicate bulk update is in
                                              progress or not. If the flag is
                                              set, bulk update is in progress. */
    UINT1          u1NumPeers; /* Number of peers that are up */
    UINT1          u1ConnRetryCnt; /* Connection retry count to reach the peer*/
    BOOL1          bIsRxBufProcAllowed; /* ICCH_TRUE / ICCH_FALSE
                                         Default Value is ICCH_TRUE */
    BOOL1          bSeqMissMatch; /* ICCH_TRUE = Sequence Mismatch detected
                                     and recovery timer is running */
    BOOL1          bIsRxBuffFull; /* Rx buffer full indicator */
    BOOL1          bIsTxBuffFull; /* Sync message transmission failed 
                                     indicator */
    BOOL1          bFetchRemoteFdb; /* To enable sync-up of remote learnt fdb entries */
}tICCHInfo;

typedef struct  {
  INT4 i4SockId; /* socket identifier */
}tIcchSktCtrlMsg;

typedef struct {
    tTMO_SLL_NODE   TxNode;  /* Tx buffer node information */
    UINT1           *pu1Msg;  /* Pointer of the send buffer */
    UINT4           u4MsgSize; /* Message size of the residual buffer */
    UINT4           u4MsgOffset; /* Residual buffer message offset.*/
}tIcchTxBufNode;

/* This structure is used to implement the Hash table for the RX buffering */
typedef struct _IcchRxBufHashEntry
{
   tTMO_HASH_NODE  RxBufHashNode;
   UINT4           u4SeqNum; /* Index of the entry */
   UINT4           u4PktLen;
   UINT1           *pu1Pkt; /* pointer of the received packet*/
}tIcchRxBufHashEntry;

typedef struct {
    UINT4 u4MsgType; /* Icch queue message type */
    union
    {
        tIcchPeerCtrlMsg  IcchPeerCtrlMsg; /* ICCH control message */
        tIcchSktCtrlMsg   IcchSktCtrlMsg;
        tIcchProtoAck     IcchProtoAckCtrlMsg;
    }unCtrlMsgParam;
#define PeerCtrlMsg  unCtrlMsgParam.IcchPeerCtrlMsg
#define SktCtrlMsg unCtrlMsgParam.IcchSktCtrlMsg
#define ProtoAckCtrlMsg   unCtrlMsgParam.IcchProtoAckCtrlMsg
}tIcchCtrlQMsg;

/* Used by the protocols and the management modules to post the dynamic
 * sync-up message and static configuration information to the ICCH task
 * respectively.*/
typedef struct {
    tIcchMsg  *pIcchMsg; /* Message posted by protocols*/
    UINT1      u1MsgType; /* ICCH_STATIC_CONF_INFO or ICCH_DYNAMIC_MSG*/
    UINT1      u1Padding;
    UINT2      u2Padding;
}tIcchPktQMsg;

/* ICCH Pkt Mem-Block */
typedef struct _tIcchPktBlock                                                                      {
    UINT1   au1IcchPktBlock[ICCH_MAX_SYNC_PKT_LEN];
}tIcchPktBlock;

/* Used on bootup to load the ICCH parameters from iccl.conf */
typedef struct {
    CHR1       ac1IcclInterface[CFA_MAX_PORT_NAME_LENGTH]; /*ICCL
                                        interface */
    CHR1       ac1NextIcclInterface[CFA_MAX_PORT_NAME_LENGTH]; /*Next ICCL
     interface */

    UINT4      u4InstanceId; /* ICCL instance ID */
    UINT4      u4IpAddress;  /* IP address of ICCL interface */
    UINT4      u4SubnetMask; /* Subnet mask of ICCL interface */
    UINT4      u4NextIpAddress;  /* Next IP address configured for ICCL interface */
    UINT4      u4NextSubnetMask; /* Next Subnet mask configured for ICCL interface */
    INT4       i4SessionNodeState; /* State of the MC-LAG system
                                    per instance */
    tVlanId    VlanId;       /* VLAN ID of ICCL interface */
    tVlanId    NextVlanId;   /* Next VLAN ID of ICCL interface  */
}tIcchSessInfo;

#endif
