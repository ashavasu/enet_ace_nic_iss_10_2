/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: icchtrc.h,v 1.4 2016/01/06 11:02:33 siva Exp $
*
* Description: Macros related to Trace of ICCH.
*********************************************************************/

#ifdef TRACE_WANTED
#define ICCH_MAX_TRC_NAME_LEN  256
#define ICCH_TRC_FLAG          (gIcchInfo.u4IcchTrc)
#define ICCH_MOD_NAME          ((const char *)"ICCH")

#define ICCH_PKT_DUMP(TraceType, pBuf, Length) \
{\
    if ((ICCH_TRC_FLAG & TraceType) == ICCH_PKT_DUMP_TRC)  \
    IcchPktDumpTrc (pBuf, (UINT4) Length);\
}

#define ICCH_TRC(TraceType, Str) \
     MOD_TRC(ICCH_TRC_FLAG, TraceType, ICCH_MOD_NAME, (const char *)Str) 

#define ICCH_TRC1(TraceType, Str, Arg1) \
     MOD_TRC_ARG1(ICCH_TRC_FLAG, TraceType, ICCH_MOD_NAME, (const char *)Str, Arg1)

#define ICCH_TRC2(TraceType, Str, Arg1, Arg2) \
     MOD_TRC_ARG2(ICCH_TRC_FLAG, TraceType, ICCH_MOD_NAME, (const char *)Str, Arg1, Arg2)

#define ICCH_TRC3(TraceType, Str, Arg1, Arg2, Arg3) \
     MOD_TRC_ARG3(ICCH_TRC_FLAG, TraceType, ICCH_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3)

#else

#define ICCH_PKT_DUMP(TraceType, pBuf, Length)
#define ICCH_TRC(Value, Fmt)
#define ICCH_TRC1(Value, Fmt, arg1)
#define ICCH_TRC2(Value, Fmt, arg1, arg2)
#define ICCH_TRC3(Value, Fmt, arg1, arg2, arg3)
#endif /* TRACE_WANTED */

