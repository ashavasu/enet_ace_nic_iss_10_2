/******************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: icchdefn.h,v 1.13 2017/09/25 12:11:45 siva Exp $
 *
 * Description: Data structure used by ICCH module.
 *********************************************************************/
#ifndef ICCH_DEFN_H
#define ICCH_DEFN_H

#define ICCH_MAX_FRAGMENT 65535
/*General definitions*/
#define ICCH_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)
#define ICCH_CREATE_QUEUE            OsixQueCrt
#define ICCH_RECEIVE_FROM_QUEUE      OsixQueRecv
#define ICCH_DELETE_SEMAPHORE(semid) OsixSemDel(semid)
#define ICCH_DELETE_QUEUE            OsixQueDel
#define ICCH_DEF_MSG_LEN             OSIX_DEF_MSG_LEN
#define ICCH_SEND_TO_QUEUE           OsixQueSend
#define ICCH_SEND_EVENT              OsixEvtSend
#define ICCH_RECV_EVENT              OsixEvtRecv
#define ICCH_GET_TASK_ID             OsixGetTaskId
#define ICCH_SLL_NODE_INIT           TMO_SLL_Init_Node
#define ICCH_SLL_INIT(x)             TMO_SLL_Init(x)
#define ICCH_SLL_ADD                 TMO_SLL_Add
#define ICCH_SLL_COUNT(x)            TMO_SLL_Count(x)
#define ICCH_LINEAR_CALC_CKSUM       UtlIpCSumLinBuf
#define ICCH_RXBUF_MAX_HASH_BUCKET_COUNT  200

#define ICCH_SELF              SELF

#define ICCH_SEQNUM_WRAP_AROUND_VALUE 0xffffffff

#define ICCH_VERSION_NUM  1

#define ICCH_MAX_BUFFER_LENGTH          256
#define ICCH_SNMP_V2_TRAP_OID_LEN       11
#define ICCH_ONE                        1
#define ICCH_TWO                        2

/* Maximum length of iccl.conf entry */
#define ICCL_CONF_MAX_LEN 60

/* Macros related to packet dump print */
#define ICCH_MAX_DUMP_LEN        512
#define ICCH_PRINT_BUF_LEN       80

/* iccl.conf file update operation */
enum
{
    ICCL_FILE_WRITE = 1,
    ICCL_FILE_DELETE = 2
};

/*Message type used to identify whether it is a syncup msg from protocol
 * or ..*/
enum
{
    ICCH_SYNCUP_MSG = 1,
};

/* ICCH control messages */
enum
{
    ICCH_PEER_INFO_MSG = 0,
    ICCH_WR_SOCK_MSG   = 1,
    ICCH_PROTO_ACK_MSG = 2,
    ICCH_RD_SOCK_MSG   = 3
};

enum
{
    ICCH_SEQ_RECOV_TIMER              = 1,
    ICCH_ACK_RECOV_TIMER              = 2,
    ICCH_CONN_RETRY_TIMER             = 3,
    ICCH_BULK_UPDATE_TIMER            = 4,
    ICCH_MAX_TIMER_TYPES              = 5 /* Max Timer types */
};

/*Traps in ICCH */
enum
{
    ICCH_STATUS_CHANGE_TRAP      = 1,
    ICCH_ENABLE_PROTO_SYNC_TRAP  = 2,
    ICCH_PEER_STATUS_CHANGE_TRAP = 3
};
#define ICCH_BULK_UPDATE_TMR_INT       10 /* 10 milli seconds */

/*ICCH module Info*/
#define ICCH_TASK_NAME            "ICCH"
#define ICCH_PROTO_SEM_NAME       ((CONST UINT1 *) "ICPS")
#define ICCH_CTRL_QUEUE_ID        (gIcchInfo.IcchCtrlQueueId)
#define ICCH_QUEUE_ID             (gIcchInfo.IcchQueueId)
#define ICCH_CTRL_Q_NAME          "ICCQ"
#define ICCH_Q_NAME          "ICPQ"
#define MAX_ICCH_CTRL_MSG_NODES   1000
#define ICCH_PKT_Q_DEPTH     32000

#define ICCH_RELINQUISH_CNTR      250
#define ICCH_TX_RELINQUISH_CNTR    250

#define ICCH_GET_SSN_INFO(x)      (gIcchInfo.IcchSsnInfo[x])
#define ICCH_TCP_SRV_SOCK_FD()     (gIcchInfo.i4SrvSockFd)
#define ICCH_SSN_ENTRY_TX_LIST(x) (x.TxList)
#define ICCH_TASK_ID              (gIcchInfo.IcchTaskId)
#define ICCH_PROTO_SEM()          (gIcchInfo.IcchProtoSemId)
#define ICCH_RX_LAST_SEQ_NUM_PROCESSED() gIcchInfo.u4RxLastSeqNum
#define ICCH_TX_LAST_SEQ_NUM_PROCESSED() gIcchInfo.u4TxLastSeqNum
#define ICCH_RX_BUFF_PKT_COUNT() (gIcchInfo.u4RxBufNodeCount)

#define ICCH_MAX_CONN_RETRY_CNT   10
/* ICCH Tx */
#define ICCH_TX_BUF_NODES_THRESHOLD (MAX_ICCH_TX_BUF_NODES * 0.75)

/*Socket related Info */
#define ICCH_SOCK_CONNECTED            1
#define ICCH_SOCK_NOT_CONNECTED        2
#define ICCH_SOCK_CONNECT_IN_PROGRESS  3

/*Invalid socket descriptor*/
#define ICCH_INV_SOCK_FD -1

/*Syncup related Info*/
#define ICCH_SYNCUP_TASK_ID()     gIcchSyncInfo.IcchSyncTaskId
#define ICCH_SYNCUP_SEMID()       gIcchSyncInfo.IcchSyncSemId
#define ICCH_SYNCUP_Q_ID()        gIcchSyncInfo.IcchSyncQId
#define ICCH_CONF_Q_MSG_POOL_ID() gIcchSyncInfo.IcchSyncQPoolId

/* Events to be posted from the ICCH task to the Syncup task*/
#define ICCH_SYNCUP_MSG_RCVD 0x01

/*LOCKS*/
#define ICCH_SYNC_LOCK()    IcchSyncupDBLock ()
#define ICCH_SYNC_UNLOCK()  IcchSyncupDBUnLock ()
#define ICCH_LOCK()         IcchLock ()
#define ICCH_UNLOCK()       IcchUnLock ()

/* Events to be posted from the ICCH task to the Config task*/
#define ICCH_CONFIG_MSG_RCVD 0x01
#define ICCH_DYNAMIC_BULK_UPDATE_COMPLETION  0x02

/* Macros relating to iccl.conf file */
#define ICCL_CONF_FILE          "iccl.conf"
#define ICCH_CONF_COMMENT       "#Instance    ICCL    VLAN    IPAddress    SubnetMask\n"
#define ICCH_DEFAULT_INST_ID    0
#define ICCH_DEFAULT_INT        "po4094"
#define ICCH_DEFAULT_VLAN       4094
#define ICCH_DEFAULT_IP         "169.254.1.1"
#define ICCH_SUBNET_MASK        "255.255.255.254"
/*Hex value for default subnet mask*/
#define ICCH_DEFAULT_SUBNET_MASK 0xfffffffe 
#define ICCL_MAX_ADDR_LEN       20
#define ICCL_MAX_SUBNET  0xffffffff

#define ICCH_MIN_INST_ID 0
#define ICCH_MAX_INST_ID 0

/* To increase the number of ICCH sessions, tune ICCH_MAX_INST_ID as per below
 * logic: Ex: No of ICCH sessions required - 5, ICCH_MAX_INST_ID shall be
 * set to 4*/
#define ICCH_MAX_SESSIONS (ICCH_MAX_INST_ID + 1)

#define ICCH_MIN_VLAN 1
#define ICCH_MAX_VLAN 4095

#define ICCH_MIN_PORT_CHNL_ID 1
#define ICCH_MAX_PORT_CHNL_ID 65535

/* Total number of ICCH instances which are currently active */
#define ICCH_TOTAL_NUM_OF_ACTV_INST gIcchInfo.u4TotalNumOfInst

#endif /*ICCH_DEFN_H*/
