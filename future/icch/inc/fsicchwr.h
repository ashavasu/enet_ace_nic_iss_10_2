/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: fsicchwr.h,v 1.7 2017/09/25 12:11:45 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

#ifndef _FSICCHWR_H
#define _FSICCHWR_H

VOID RegisterFSICCH(VOID);

VOID UnRegisterFSICCH(VOID);

INT4 FsIcchTrcLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsIcchStatsEnableGet(tSnmpIndex *, tRetVal *);
INT4 FsIcchClearStatsGet(tSnmpIndex *, tRetVal *);
INT4 FsIcchEnableProtoSyncGet(tSnmpIndex *, tRetVal *);
INT4 FsIcchFetchRemoteFdbGet(tSnmpIndex *, tRetVal *);
INT4 FsIcchPeerNodeIpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsIcchPeerNodeStateGet(tSnmpIndex *, tRetVal *);
INT4 FsIcchOverrideLocalAffinityGet(tSnmpIndex *, tRetVal *);
INT4 FsIcchTrcLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsIcchStatsEnableSet(tSnmpIndex *, tRetVal *);
INT4 FsIcchClearStatsSet(tSnmpIndex *, tRetVal *);
INT4 FsIcchEnableProtoSyncSet(tSnmpIndex *, tRetVal *);
INT4 FsIcchFetchRemoteFdbSet(tSnmpIndex *, tRetVal *);
INT4 FsIcchOverrideLocalAffinitySet(tSnmpIndex *, tRetVal *);
INT4 FsIcchTrcLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIcchStatsEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIcchClearStatsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIcchEnableProtoSyncTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIcchFetchRemoteFdbTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIcchOverrideLocalAffinityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIcchTrcLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIcchStatsEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIcchClearStatsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIcchEnableProtoSyncDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIcchFetchRemoteFdbDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIcchOverrideLocalAffinityDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsIcclSessionTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsIcclSessionInterfaceGet(tSnmpIndex *, tRetVal *);
INT4 FsIcclSessionIpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsIcclSessionSubnetMaskGet(tSnmpIndex *, tRetVal *);
INT4 FsIcclSessionVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsIcclSessionNodeStateGet(tSnmpIndex *, tRetVal *);
INT4 FsIcclSessionRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIcclSessionInterfaceSet(tSnmpIndex *, tRetVal *);
INT4 FsIcclSessionIpAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsIcclSessionSubnetMaskSet(tSnmpIndex *, tRetVal *);
INT4 FsIcclSessionVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsIcclSessionRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIcclSessionInterfaceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIcclSessionIpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIcclSessionSubnetMaskTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIcclSessionVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIcclSessionRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIcclSessionTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsIcchStatsSyncMsgTxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsIcchStatsSyncMsgTxFailedCountGet(tSnmpIndex *, tRetVal *);
INT4 FsIcchStatsSyncMsgRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsIcchStatsSyncMsgProcCountGet(tSnmpIndex *, tRetVal *);
INT4 FsIcchStatsSyncMsgMissedCountGet(tSnmpIndex *, tRetVal *);
#endif /* _FSICCHWR_H */
