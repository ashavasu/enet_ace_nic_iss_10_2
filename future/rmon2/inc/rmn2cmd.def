/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved               
*                                                                    
* $Id: rmn2cmd.def,v 1.5 2015/07/27 07:00:41 siva Exp $                                                         
*                                                                    
*********************************************************************/
/***************************RMON2 COMMANDS************************************/

/*****************************************************************************/
/*                         RMON2 GLOBALCFG COMMANDS                          */
/*****************************************************************************/

DEFINE GROUP: RMON2_GLBCFG_CMDS

    COMMAND : rmon2 {enable | disable}
    ACTION  :
              {
         	if($1 != NULL)
	        {
		    cli_process_rmon2_cmd (CliHandle, RMON2_CLI_STATUS, NULL,
				RMON2_ENABLE);
	        }
	        else if($2 != NULL)
	        {
		    cli_process_rmon2_cmd (CliHandle, RMON2_CLI_STATUS, NULL,
				RMON2_DISABLE);
	        }
              }

    SYNTAX  : rmon2 {enable | disable}
    PRVID   : 15
    HELP    : Enable or Disable RMON2
    CXT_HELP : rmon2 Configures RMONv2 related details | 
               enable Enables the RMONv2 feature | 
               disable Disables the RMONv2 feature | 
               <CR> Enable or Disable RMON2

END GROUP

/*****************************************************************************/
/*                         RMON2  PEXCFG COMMANDS                            */
/*****************************************************************************/

DEFINE GROUP: RMON2_PEXCFG_CMDS


    COMMAND : debug rmon2 {[func-entry][func-exit][critical][mem-fail][debug] | [ALL]}
    ACTION  : 
	    { 
	      UINT4 u4Value = 0;

	      if ($2 != NULL)
	      {
		  u4Value = u4Value | RMON2_CLI_TRACE_FUNC_ENTRY;

	      }
	      if ($3 != NULL)
	      {
		  u4Value = u4Value | RMON2_CLI_TRACE_FUNC_EXIT;

	      }
	      if ($4 != NULL)
	      {
		  u4Value = u4Value | RMON2_CLI_TRACE_CRITICAL;

	      }
	      if ($5 != NULL)
	      {
		  u4Value = u4Value | RMON2_CLI_TRACE_MEM_FAIL;

	      }
	      if ($6 != NULL)
	      {
		  u4Value = u4Value | RMON2_CLI_TRACE_DEBUG;

	      }
	      if ($7 != NULL)
	      {
		  u4Value = RMON2_CLI_TRACE_ALL;

	      }
	      if (u4Value == 0)
	      {
		  cli_process_rmon2_cmd(CliHandle, RMON2_CLI_SHOW_TRACE);
	      }
	      else
	      {
	          cli_process_rmon2_cmd(CliHandle, RMON2_CLI_TRACE, NULL, u4Value);
	      }

	    }  

    SYNTAX  : debug rmon2 {[func-entry][func-exit][critical][mem-fail][debug] | [ALL]} 
    PRVID   : 15
    HELP    : Enables debug trace.
    CXT_HELP : debug Configures trace for the protocol | 
               rmon2 RMONv2 related configuration | 
               func-entry Function Entry Traces | 
               func-exit Function Exit Traces | 
               critical Critical traces | 
               mem-fail Memory failures | 
               debug Debug Traces | 
               ALL All trace messages | 
               <CR> Enables debug trace

    COMMAND :no  debug rmon2 {[func-entry][func-exit][critical][mem-fail][debug] | [ALL]}
    ACTION  :
            {
  
          UINT4 u4Value = 0;

          if ($3 != NULL)
          {
          u4Value = u4Value | RMON2_CLI_TRACE_FUNC_ENTRY;

          }
          if ($4 != NULL)
          {
          u4Value = u4Value | RMON2_CLI_TRACE_FUNC_EXIT;

          }
          if ($5 != NULL)
          {
          u4Value = u4Value | RMON2_CLI_TRACE_CRITICAL;

          }
          if ($6 != NULL)
          {
          u4Value = u4Value |RMON2_CLI_TRACE_MEM_FAIL;

          }
          if ($7 != NULL)
          {
          u4Value = u4Value | RMON2_CLI_TRACE_DEBUG;

          }
          if ($8 != NULL)
          {
          u4Value = RMON2_CLI_TRACE_ALL;

          }
          if (u4Value == 0)
          {
          cli_process_rmon2_cmd(CliHandle, RMON2_CLI_SHOW_TRACE);
          }
          else
          {
              cli_process_rmon2_cmd(CliHandle,RMON2_CLI_RESET_TRACE, NULL, u4Value);
          }
        }

    SYNTAX  : no  debug rmon2 {[func-entry][func-exit][critical][mem-fail][debug] | [ALL]}
    PRVID   : 15
    HELP    : Disables debug option for RMON2 module
    CXT_HELP : no Disables the configuration / deletes the entry / resets to default value | 
               debug Configures trace for the protocol |
               rmon2 RMONv2 related configuration |
               func-entry Function Entry Traces |
               func-exit Function Exit Traces |
               critical Critical traces |
               mem-fail Memory failures |
               debug Debug Traces |
               ALL All trace messages |
               <CR> Disables debug option for RMON2 module
	    
END GROUP
