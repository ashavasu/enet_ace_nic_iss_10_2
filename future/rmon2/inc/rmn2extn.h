/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * Description: This file externs Global Varible declaration.
 *******************************************************************/
#ifndef _RMN2EXTN_H_
#define _RMN2EXTN_H_

PUBLIC tRmon2Globals gRmon2Globals;

PUBLIC UINT4 gIfEntryOIDTree[IFINDEX_OID_SIZE + 1];

PUBLIC UINT1 gu1IssUlLogFlag;

PUBLIC INT4  gi4MibSaveStatus;
PUBLIC INT4  gi4MibResStatus;

#endif

