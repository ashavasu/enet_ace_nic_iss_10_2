/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * Description: This file contains definitions of RMON2 constants.
 *
 * $Id: rmn2defn.h,v 1.8 2015/09/13 10:36:21 siva Exp $
 *******************************************************************/
#ifndef _RMN2DEFN_H
#define _RMN2DEFN_H

/* Maximum Protocol Dir ID Length */
#define     RMON2_MAX_PROT_DIR_ID_LEN          128
/* Minimum Protocol Dir ID Length */
#define     RMON2_MIN_PROT_DIR_ID_LEN           4   
/* Maximum Protocol Dir Param Length */
#define     RMON2_MAX_PROT_DIR_PARAM_LEN     32  
/* Minimum Protocol Dir Param Length */
#define     RMON2_MIN_PROT_DIR_PARAM_LEN        1   
/* Protocol Description Length */
#define     RMON2_PROT_DIR_DESC_LEN             64      
/* Owner Length */
#define     RMON2_OWNER_LEN                    127   

#define     RMON2_MAX_IPADDR_SIZE             255     
/* Probe Software Revision Length */
#define     RMON2_PROBE_SW_REV_LEN             15 
/* Probe Hardware Revision Length */
#define     RMON2_PROBE_HW_REV_LEN             31
/* Probe Date Time Length */
#define     RMON2_PROBE_DATE_TIME_LEN         11

/* TopN Control Time Remaining Default value */
#define     RMON2_TOPN_CTRL_TIMEREM             1800
/* TopN Control Requested Size Default value */
#define     RMON2_TOPN_CTRL_REQ_SIZE            150

/* Usr History Buckets Requested Default value */
#define     RMON2_USR_HISTORY_BUCKETS_REQ       50

/* Usr History Interval Default value */
#define     RMON2_USR_HISTORY_INTERVAL          1800

#define RMON2_MAX_OCTET_STRING_LEN 256
#define RMON2_MAX_OID_LEN   256

#define RMON2_MAX_IF_NAME_LEN 63
#define RMON2_MAX_MTU         9216
#define RMON2_INVALID_SOCK_DESC    -1
#define RMON_CMSG_DATA_LEN 24
#define RMON2_MAX_VLAN 4094

/* SEM Related Macros */
#define RMON2_SEM_NAME         ((const UINT1 *)"RN2M")
#define RMON2_TASK_NAME        ((const UINT1 *)"RMN2")
#define RMON2_QUEUE_NAME       ((const UINT1 *)"RN2Q")
#define RMON2_QUEUE_SIZE       50
#define RMON2_SEM_COUNT        1
#define RMON2_SEM_ID           gRmon2Globals.Rmon2SemId
#define RMON2_TASK_ID        gRmon2Globals.Rmon2TskId
#define RMON2_QUEUE_ID        gRmon2Globals.Rmon2QId
#define RMON2_IS_ENABLED()     (gRmon2Globals.u4Rmon2AdminStatus == RMON2_ENABLE)
#define IFINDEX_OID_SIZE       10  /* Data source OID size */

#define RMON2_INIT_COMPLETE(u4Status)    lrInitComplete(u4Status)

/* Timer Macros */

#define RMON2_ONE_SECOND                1
#define RMON2_ONE_SECOND_TIMER_EVENT    0x01
#define RMON2_PACKET_INFO_EVENT  0x02
#define RMON2_IF_STATUS_EVENT  0x04
#define RMON2_PKT_ARRIVAL_EVENT 0x08

#define RMON2_ENABLE    1
#define RMON2_DISABLE   2

/* Invalid value */
#define RMON2_INVALID_VALUE            -1

#define MAX_PROTO_ID_BASE_LEN 8

#define RMON2_VER_IPV4  0x4
#define RMON2_VER_IPV6  0x6

#define RMON2_IPV4_MAX_LEN     4   /* IPv4 Address length */
#define RMON2_IPV6_MAX_LEN    16   /* IPv6 Address length */

#define RMON2_MAX_LRU_REF    600        /* Max. LRU limit */
#define RMON2_MAX_STATS_PER_CYCLE   20  /* Maximum Statistics collection *
                                         * allowed per timer cycle */


/****************************************************************************/
/*    RMON2 RB Tree related constants                                       */
/****************************************************************************/
#define RMON2_RB_EQUAL             0
#define RMON2_RB_GREATER           1
#define RMON2_RB_LESS             -1

/* Max Data Entries */
#define RMON2_MAX_DATA_PER_CTL     20
#define RMON2_MAX_ADDRMAP_ENTRY    50 
#define RMON2_MAX_TOPN_ENTRY       20
#define RMON2_MAX_USR_HISTORY_BUCKETS_GRANT     10
/* New macro introduced for checking the max value of usrHistoryControlObjects 
   Changed as a part of zero regression failure in RMON2 module */
#define MAX_RMON2_USRHIST_OBJ 65535


/****************************************************************************/
/*    RMON2 Probe Supports  - Protocol Macros                               */             
/*                                                                          */
/****************************************************************************/

#define RMON2_PROBECAPACITY_PROTLIST "protlist.data"
#define RMON2_MAX_PROTTEXT_LEN 30

#define RMON2_ETHER2 0x0001
#define RMON2_IPV4   CFA_ENET_IPV4
#define RMON2_ARP    CFA_ENET_ARP
#define RMON2_IPV6   CFA_ENET_IPV6
#define RMON2_IPVX   0x8137 
#define RMON2_ICMP   CFA_ICMP
#define RMON2_TCP    CFA_TCP
#define RMON2_UDP    CFA_UDP
#define RMON2_TELNET 23 
#define RMON2_SMTP 25
#define RMON2_POP3 110
#define RMON2_TFTP 69
#define RMON2_FTP  21
#define RMON2_SNMP 161
#define RMON2_DNS  53

/* Groups Supported by probe */

#define RMON1_ETHERSTATS       0x80000000       /* Rmon1 EtherStats Group */
#define RMON1_HISTORY_CTRL     0x40000000       /* Rmon1 History Control Group */
#define RMON1_ETHERHISTORY     0x20000000       /* Rmon1 Ether History Group */
#define RMON1_ALARM            0x10000000       /* Rmon1 Alarm Group */
#define RMON1_HOST             0x08000000       /* Rmon1 Hosts Group */
#define RMON1_HOST_TOPN        0x04000000       /* Rmon1 Host TopN Group */
#define RMON1_MATRIX           0x02000000       /* Rmon1 Matrix Group */
#define RMON1_EVENT            0x00400000       /* Rmon1 Event Group */
#define RMON2_PROT_DIR         0x00002000       /* Rmon2 Protocol Directory Group */
#define RMON2_PROT_DIST        0x00001000       /* Rmon2 Protocol Distribution Group */
#define RMON2_ADDRESS_MAP      0x00000800       /* Rmon2 Address Map Group */
#define RMON2_NLHOST           0x00000400       /* Rmon2 NlHost Group */
#define RMON2_NLMATRIX         0x00000200       /* Rmon2 NlMatrix Group */
#define RMON2_ALHOST           0x00000100       /* Rmon2 AlHost Group */
#define RMON2_ALMATRIX         0x00000080       /* Rmon2 AlMatrix Group */
#define RMON2_USERHIST         0x00000040       /* Rmon2 User History Group */
#define RMON2_PROBE_CONFIG     0x00000020       /* Rmon2 Probe Configuration Group */

#define RMON2_PROBE_CAPABILITIES gRmon2Globals.u4ProbeCapabilities

/* Maximum Control Index value */
#define RMON2_MAX_CTRL_INDEX 65535
/* Maximum ProtocolDirLocalIndex value */
#define RMON2_MAX_PROTDIRLOC_INDEX 2147483647
/* Maximum Desired Entries in Data Table */
#define RMON2_MAX_DESIRED_ENTRIES 2147483647
/* Maximum Matrix TopN Time Remaining value */
#define RMON2_MAX_MTRX_TOPN_TIMEREM 2147483647
/* Maximum Matrix TopN Requested Size value */
#define RMON2_MAX_MTRX_TOPN_REQSIZE  2147483647
/* Maximum Matrix TopN Index value */
#define RMON2_MAX_MATRIX_TOPN_INDEX 65535

/* Maximum Usr History Object Index value */
#define RMON2_MAX_USRHIST_OBJECT_INDEX 65535
/* Maximum Usr Histroy Sample Index value */
#define RMON2_MAX_USRHIST_SAMPLE_INDEX 2147483647
/* Usr History Max Interval value */
#define RMON2_MAX_USR_HISTORY_INTERVAL 2147483647    

#define RMON2_ZERO 0
#define RMON2_ONE  1

/* Maximum Usr History Buckets Requested */
#define RMON2_MAX_USRHIST_BUCKETS_REQ 65535

/* Probe Capabilities Object */
#define RMON2_GET_PROBE_CAPAB_BYTE1     0
#define RMON2_GET_PROBE_CAPAB_BYTE2     1
#define RMON2_GET_PROBE_CAPAB_BYTE3     2
#define RMON2_GET_PROBE_CAPAB_BYTE4     3

/* Maximum ISS DATE Length */ 
#define ISS_MAX_DATE_LEN               40

/* Probe DateTime Objects */

#define RMON2_PROBE_SET_HOUR            4
#define RMON2_PROBE_SET_MIN             5
#define RMON2_PROBE_SET_SEC             6
#define RMON2_PROBE_SET_DAY             3
#define RMON2_PROBE_SET_MONTH           2
#define RMON2_PROBE_SET_YEAR_BYTE1      0
#define RMON2_PROBE_SET_YEAR_BYTE2      1

#define RMON2_PROBE_GET_YEAR_BYTE1      18
#define RMON2_PROBE_GET_YEAR_BYTE2      19
#define RMON2_PROBE_GET_YEAR_BYTE3      20
#define RMON2_PROBE_GET_YEAR_BYTE4      21
#define RMON2_PROBE_GET_YEAR_OCT1        0
#define RMON2_PROBE_GET_YEAR_OCT2        1
#define RMON2_PROBE_GET_MONTH            2
#define RMON2_PROBE_GET_MONTH_BYTE1      3
#define RMON2_PROBE_GET_MONTH_BYTE2      4
#define RMON2_PROBE_GET_DAY              3
#define RMON2_PROBE_GET_DAY_BYTE1        6
#define RMON2_PROBE_GET_DAY_BYTE2        7
#define RMON2_PROBE_GET_HOUR             4
#define RMON2_PROBE_GET_HOUR_BYTE1       9
#define RMON2_PROBE_GET_HOUR_BYTE2      10
#define RMON2_PROBE_GET_MINUTE           5
#define RMON2_PROBE_GET_MINUTE_BYTE1    12
#define RMON2_PROBE_GET_MINUTE_BYTE2    13
#define RMON2_PROBE_GET_SECOND           6
#define RMON2_PROBE_GET_SECOND_BYTE1    15
#define RMON2_PROBE_GET_SECOND_BYTE2    16
#define RMON2_PROBE_GET_DECI_SECOND      7
#define RMON2_PROBE_GET_DIRECT_FROM_UTC  8
#define RMON2_PROBE_GET_HOURS_FROM_UTC   9
#define RMON2_PROBE_GET_MINUTES_FROM_UTC 10




#define RMON2_SUCCESS                   0
#define RMON2_FAILURE                 (-1)
#define RMON2_INVALID_OID             (-2)

#define RMON2_GET_DATA_SOURCE_OID      Rmon2UtlFormDataSourceOid
#define RMON2_SET_DATA_SOURCE_OID      Rmon2UtlSetDataSourceOid
#define RMON2_TEST_DATA_SOURCE_OID     Rmon2UtlTestDataSourceOid

#define RMON2_GET_PRNT_ENTRY_ID(dest,src,len) if (len > 4 && len % 4 == 0) \
                       {\
                           MEMCPY(dest,src,len - 4); \
                       }

#define RMON2_GET_PRNT_ENTRY_PARAMS(dest,src,len) if (len > 1 ) \
                       {\
                           MEMCPY(dest,src,len - 1); \
                       }

#define MIB_SAVE_EVENT                0x01
#define MIB_ERASE_EVENT               0x2000

#endif
