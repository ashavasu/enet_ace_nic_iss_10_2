/********************************************************************                               
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * Description: This file contains all RMON2 enumerations.
 *
 *******************************************************************/
#ifndef _RMN2ENUM_H_
#define _RMN2ENUM_H_

/* NL Matrix SD/DS Table Id */
typedef enum {
  RMON2_NLMATRIXSD_TABLE = 0,    /* NL Matrix SD Table Id */
  RMON2_NLMATRIXDS_TABLE = 1,    /* NL Matrix DS Table Id */
  RMON2_MAX_NLMATRIX_TABLES = 2  /* Number of NL Matirx Tables */
} enRmon2NlMatrixId;

/* AL Matrix SD/DS Table Id */
typedef enum {
  RMON2_ALMATRIXSD_TABLE = 0,    /* AL Matrix SD Table Id */
  RMON2_ALMATRIXDS_TABLE = 1,    /* AL Matrix DS Table Id */
  RMON2_MAX_ALMATRIX_TABLES = 2  /* Number of AL Matirx Tables */
} enRmon2AlMatrixId;

/* ProtocolDirAddressMapConfig or ProtocolDirHostConfig or
 * ProtocolDirMatirxConfig Support Status */
typedef enum {
   NOT_SUPPORTED = 1,           /* Config Not Supported */
   SUPPORTED_OFF = 2,           /* Config Supported Off */
   SUPPORTED_ON = 3             /* Config Supported On  */
}enProtocolDirSupportStatus;

/* NlMatrixTopNControlRateBase */
typedef enum {
   NLMATRIX_TOPN_PKTS = 1,               /* NL Matrix TopN Pkts */
   NLMATRIX_TOPN_OCTETS = 2,             /* NL Matrix TopN octets */
   NLMATRIX_TOPN_HIGHCAPACITY_PKTS = 3,  /* NL Matrix TopN High capacity 
                                          * Pkts */
   NLMATRIX_TOPN_HIGHCAPACITY_OCTETS = 4 /* NL Matrix TopN High capacity 
                                          * Octets */
}enNlMatrixTopNControlRateBase; 

/* AlMatrixTopNControlRateBase */
typedef enum {
   ALMATRIX_TOPN_TERMINALS_PKTS = 1,   /* AL Matrix TopN Terminals Pkts */
   ALMATRIX_TOPN_TERMINALS_OCTETS = 2, /* AL Matrix TopN Terminals Octets */
   ALMATRIX_TOPN_ALL_PKTS = 3,         /* AL Matrix TopN ALL Pkts */
   ALMATRIX_TOPN_ALL_OCTETS = 4,       /* AL Matrix TopN ALL Pkts */
   ALMATRIX_TOPN_TERMINALS_HIGHCAPACITY_PKTS = 5,   /* AL Matrix TopN Terminals 
                                                     * High capacity Pkts */
   ALMATRIX_TOPN_TERMINALS_HIGHCAPACITY_OCTETS = 6, /* AL Matrix TopN Terminals 
                                                     * High capacity Octets */
   ALMATRIX_TOPN_ALL_HIGHCAPACITY_PKTS = 7,         /* AL Matrix TopN ALL High 
                                                     * capacity Pkts */
   ALMATRIX_TOPN_ALL_HIGHCAPACITY_OCTETS = 8        /* AL Matrix TopN ALL High
                                                     * capacity Octets */
}enAlMatrixTopNControlRateBase;

/* UsrHistoryObjectSampleType */
typedef enum  {
   RMON2_SAMPLE_TYPE_ABSOLUTE_VALUE = 1, /* Absolute Value */
   RMON2_SAMPLE_TYPE_DELTA_VALUE = 2     /* Delta Value */
}enUsrHistoryObjectSampleType;

/* ProbeResetControl */
typedef enum  {
   RUNNING   = 1,  /* Running */
   WARM_BOOT = 2,  /* Warm boot */
   COLD_BOOT = 3   /* Cold boot */
}enProbeResetControl;

/* UsrHistoryValStatus */
typedef enum  {
   VALUE_NOT_AVAILABLE = 1, /* Value Not Available  */
   VALUE_POSITIVE = 2,      /* Value Positive */
   VALUE_NEGATIVE = 3       /* Value Negative */
}enUsrHistoryValStatus;

/* Layer Type */
typedef enum {
LAYER2 = 2,        /* Layer2 Type */
LAYER3 = 3,        /* Layer3 Type */
LAYER4 = 4,        /* Layer4 Type */
LAYER5 = 5         /* Layer5 Type */
}enLayerType;

/* ProtocolDirType */
typedef enum {
   NOT_EXTENSIBLE_NO_ADDR_RECOGN = 0, /* Not Extensible & No Addr Recognition */
   EXTENSIBLE_NO_ADDR_RECOGN = 1,     /* Extensible & No Addr Recognition */
   NOT_EXTENSIBLE_ADDR_RECOGN = 2,    /* Not Extensible & Addr Recognition */
   EXTENSIBLE_ADDR_RECOGN = 3         /* Extensible & Addr Recognition */
}enExtensibleAddrRecogn;

#endif
