/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * Description: This file contains definitions for RMON2 Timer
 *******************************************************************/

#ifndef __RMON2TMR_H__
#define __RMON2TMR_H__



/* constants for timer types */
typedef enum {
    RMON2_POLL_TMR = 0,
    RMON2_MAX_TMRS = 1
} enRmon2TmrId;

#define     NO_OF_TICKS_PER_SEC             SYS_NUM_OF_TIME_UNITS_IN_A_SEC

typedef struct _RMON2_TIMER {
       tTmrAppTimer    tmrNode;     /* Timer node information */
       enRmon2TmrId    eRmon2TmrId; /* TimerId of the timer */
} tRmon2Tmr;

#endif  /* __RMON2TMR_H__  */


/*-----------------------------------------------------------------------*/
/*                       End of the file  rmn2tmr.h                      */
/*-----------------------------------------------------------------------*/

