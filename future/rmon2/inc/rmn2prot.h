/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 * $Id: rmn2prot.h,v 1.11 2017/01/17 14:10:30 siva Exp $ 
 * Description: This file contains all RMON2 function prototypes.
 *******************************************************************/
#ifndef _RMN2PROT_H_
#define _RMN2PROT_H_

/* Init APIs */
PUBLIC INT4 Rmon2MainLock ARG_LIST((VOID));
PUBLIC INT4 Rmon2MainUnLock ARG_LIST((VOID));
PUBLIC VOID Rmon2MainProcessPktInfo ARG_LIST((tPktHdrInfo *));
PUBLIC VOID Rmon2MainUpdateProtoChgs ARG_LIST((UINT4));
PUBLIC VOID Rmon2MainUpdatePeriodicTables ARG_LIST((VOID));
PUBLIC INT4 Rmon2MainUpdateTables PROTO((tPktHdrInfo *));
INT4 Rmon2CreateSocket (UINT4 u4IfIndex);
PUBLIC VOID Rmon2PktOnSocket (INT4 i4SockFd);
VOID Rmon2ProcessPacketOnSocket (INT4 i4SockId);
VOID Rmon2SetSocketIdForVlan (UINT4 u4IfIndex, INT4 i4PortDesc);
UINT4 Rmon2GetVlanForSocketId(INT4 i4PortDesc);
VOID
Rmon2CloseSocket (UINT4 u4Vlan);

/* Timers APIs */
PUBLIC VOID Rmon2TmrInitTmrDesc ARG_LIST((VOID));
PUBLIC INT4 Rmon2TmrInit ARG_LIST((VOID));
PUBLIC INT4 Rmon2TmrDeInit ARG_LIST((VOID));
PUBLIC INT4 Rmon2TmrStartTmr ARG_LIST((tRmon2Tmr *, enRmon2TmrId , UINT4));
PUBLIC INT4 Rmon2TmrStopTmr ARG_LIST((tRmon2Tmr *));
PUBLIC INT4 Rmon2TmrHandleExpiry ARG_LIST((VOID));
PUBLIC VOID Rmon2TmrPollTmr ARG_LIST((VOID *));

/* Packet Info Handler APIs */
PUBLIC tRmon2PktInfo* Rmon2PktInfoAddEntry ARG_LIST((tRmon2PktInfo *));
PUBLIC INT4 Rmon2PktInfoDelEntry ARG_LIST((tRmon2PktInfo *));
PUBLIC VOID Rmon2PktInfoRemoveEntries ARG_LIST((VOID));
PUBLIC tRmon2PktInfo* Rmon2PktInfoGetNextIndex ARG_LIST((tPktHeader *));
PUBLIC tRmon2PktInfo* Rmon2PktInfoGetEntry ARG_LIST((tPktHeader *));
PUBLIC tRmon2PktInfo* Rmon2PktInfoProcessIncomingPkt ARG_LIST((tRmon2PktInfo *));
PUBLIC VOID Rmon2PktInfoUpdateProtoChgs ARG_LIST((UINT4));
PUBLIC VOID Rmon2PktInfoUpdateIFIndexChgs ARG_LIST((UINT4 , UINT1));
PUBLIC VOID Rmon2PktInfoDelUnusedEntries ARG_LIST((VOID));

PUBLIC VOID Rmon2CfaIfaceUpdateChgs ARG_LIST((UINT4 , UINT1));
PUBLIC VOID Rmon2PktInfoDelIdlePkt ARG_LIST((tRmon2PktInfo *));

/* Porting APIs */
PUBLIC VOID Rmon2FetchHwStats ARG_LIST((tRmon2PktInfo*));
PUBLIC VOID Rmon2UpdateDataTables ARG_LIST((tRmon2PktInfo *, tRmon2Stats* ));

/* ProtDir APIS */
PUBLIC VOID Rmon2PDirInitProtDirTable ARG_LIST((VOID));
PUBLIC INT4 Rmon2PDirGiveProtDirLocIndices ARG_LIST((tRmon2PktInfo *));
PUBLIC tProtocolDir* Rmon2PDirGetNextProtDirIndex ARG_LIST((
    tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *));
PUBLIC tProtocolDir* Rmon2PDirAddProtDirEntry ARG_LIST((
                        tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT4 Rmon2PDirDelProtDirEntry ARG_LIST((tProtocolDir *));
PUBLIC tProtocolDir* Rmon2PDirGetProtDirEntry ARG_LIST((
                        tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT4 Rmon2PDirFindProtDirType ARG_LIST((
                        tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *));
PUBLIC UINT4 Rmon2ChkForPrntEntryAndExFeature ARG_LIST((
                        tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *));
PUBLIC UINT4 Rmon2PDirGetProtAddrMapConfig ARG_LIST((tSNMP_OCTET_STRING_TYPE *,
                                  tSNMP_OCTET_STRING_TYPE *));
PUBLIC UINT4 Rmon2PDirGetProtHostConfig ARG_LIST((tSNMP_OCTET_STRING_TYPE *,
                                  tSNMP_OCTET_STRING_TYPE *));
PUBLIC UINT4 Rmon2PDirGetProtMatrixConfig ARG_LIST((tSNMP_OCTET_STRING_TYPE *,
                                  tSNMP_OCTET_STRING_TYPE *));
PUBLIC tProtocolDir* Rmon2PDirGetProtocolNode ARG_LIST((UINT4));
PUBLIC UINT4 Rmon2DeleteChildProtocol ARG_LIST((tSNMP_OCTET_STRING_TYPE *,
                                        tSNMP_OCTET_STRING_TYPE *));
/* PDist APIS */
PUBLIC INT4 Rmon2PDistProcessPktInfo ARG_LIST((tRmon2PktInfo *, UINT4));
PUBLIC VOID Rmon2PDistUpdateIFIndexChgs ARG_LIST((UINT4 , UINT1));
PUBLIC VOID Rmon2PDistUpdateProtoChgs ARG_LIST((UINT4));
PUBLIC VOID Rmon2PDistDelDataEntries ARG_LIST((UINT4));
PUBLIC tProtocolDistControl* Rmon2PDistGetNextCtrlIndex ARG_LIST((UINT4));
PUBLIC tProtocolDistControl* Rmon2PDistAddCtrlEntry ARG_LIST((UINT4));
PUBLIC INT4 Rmon2PDistDelCtrlEntry ARG_LIST((tProtocolDistControl *));
PUBLIC tProtocolDistControl* Rmon2PDistGetCtrlEntry ARG_LIST((UINT4));
PUBLIC tProtocolDistStats* Rmon2PDistGetNextDataIndex ARG_LIST((UINT4 , UINT4));
PUBLIC tProtocolDistStats* Rmon2PDistAddDataEntry ARG_LIST((UINT4 , UINT4));
PUBLIC INT4 Rmon2PDistDelDataEntry ARG_LIST((tProtocolDistStats *));
PUBLIC tProtocolDistStats* Rmon2PDistGetDataEntry ARG_LIST((UINT4 , UINT4));
PUBLIC VOID Rmon2PDistDelInterDep ARG_LIST((tProtocolDistStats *));
PUBLIC VOID Rmon2PDistPopulateDataEntries ARG_LIST((tProtocolDistControl *));

/* AddrMap APIs */
PUBLIC INT4 Rmon2AddrMapProcessPktInfo ARG_LIST((tRmon2PktInfo *));
PUBLIC VOID Rmon2AddrMapUpdateIFIndexChgs ARG_LIST((UINT4 , UINT1));
PUBLIC VOID Rmon2AddrMapUpdateProtoChgs ARG_LIST((UINT4));
PUBLIC VOID Rmon2AddrMapDelDataEntries ARG_LIST((UINT4));
PUBLIC tAddressMapControl* Rmon2AddrMapGetNextCtrlIndex ARG_LIST((UINT4));
PUBLIC tAddressMapControl* Rmon2AddrMapAddCtrlEntry ARG_LIST((UINT4));
PUBLIC INT4 Rmon2AddrMapDelCtrlEntry ARG_LIST((tAddressMapControl *));
PUBLIC tAddressMapControl* Rmon2AddrMapGetCtrlEntry ARG_LIST((UINT4));
PUBLIC tAddressMap* Rmon2AddrMapGetNextIndex ARG_LIST((UINT4 , tIpAddr *, UINT4));
PUBLIC tAddressMap* Rmon2AddrMapAddEntry ARG_LIST((UINT4 , tIpAddr *, UINT4));
PUBLIC INT4 Rmon2AddrMapDelDataEntry ARG_LIST((tAddressMap *));
PUBLIC tAddressMap* Rmon2AddrMapGetDataEntry ARG_LIST((UINT4 , tIpAddr *, UINT4));
PUBLIC VOID Rmon2AddrMapDelInterDep ARG_LIST((tAddressMap *));
PUBLIC VOID Rmon2AddrMapPopulateDataEntries ARG_LIST((tAddressMapControl *));
PUBLIC tAddressMap * Rmon2AddrMapGiveLRUEntry ARG_LIST((VOID));

/* Nl and Al Host APIs */
PUBLIC INT4 Rmon2HostProcessPktInfo ARG_LIST((tRmon2PktInfo *, UINT4));
PUBLIC VOID Rmon2HostUpdateIFIndexChgs ARG_LIST((UINT4 , UINT1));
PUBLIC VOID Rmon2HostUpdateProtoChgs ARG_LIST((UINT4));
PUBLIC VOID Rmon2HostDelDataEntries ARG_LIST((UINT4));
PUBLIC tHlHostControl* Rmon2HlHostGetNextCtrlIndex ARG_LIST((UINT4));
PUBLIC tHlHostControl* Rmon2HlHostAddCtrlEntry ARG_LIST((UINT4));
PUBLIC INT4 Rmon2HlHostDelCtrlEntry ARG_LIST((tHlHostControl *));
PUBLIC tHlHostControl* Rmon2HlHostGetCtrlEntry ARG_LIST((UINT4));
PUBLIC tNlHost* Rmon2NlHostGetNextDataIndex ARG_LIST((UINT4 , UINT4 , tIpAddr *));
PUBLIC tNlHost* Rmon2NlHostAddDataEntry ARG_LIST((UINT4 , UINT4 , tIpAddr *));
PUBLIC INT4 Rmon2NlHostDelDataEntry ARG_LIST((tNlHost *));
PUBLIC tNlHost* Rmon2NlHostGetDataEntry ARG_LIST((UINT4 , UINT4 , tIpAddr *));
PUBLIC VOID Rmon2NlHostDelInterDep ARG_LIST((tNlHost *));
PUBLIC tAlHost* Rmon2AlHostGetNextDataIndex ARG_LIST((UINT4 , UINT4 , tIpAddr *, UINT4));
PUBLIC tAlHost* Rmon2AlHostAddDataEntry ARG_LIST((UINT4 , UINT4 , tIpAddr *, UINT4));
PUBLIC INT4 Rmon2AlHostDelDataEntry ARG_LIST((tAlHost *));
PUBLIC tAlHost* Rmon2AlHostGetDataEntry ARG_LIST((UINT4 , UINT4 , tIpAddr *, UINT4));
PUBLIC VOID Rmon2AlHostDelInterDep ARG_LIST((tAlHost *));
PUBLIC VOID Rmon2HostPopulateDataEntries ARG_LIST((tHlHostControl *));

/* Nl and Al Matrix APIs */
PUBLIC INT4 Rmon2MatrixProcessPktInfo ARG_LIST((tRmon2PktInfo *, UINT4));
PUBLIC VOID Rmon2MatrixUpdateIFIndexChgs ARG_LIST((UINT4 , UINT1));
PUBLIC VOID Rmon2MatrixUpdateProtoChgs ARG_LIST((UINT4));
PUBLIC VOID Rmon2MatrixDelDataEntries ARG_LIST((UINT4));
PUBLIC tHlMatrixControl* Rmon2HlMatrixGetNextCtrlIndex ARG_LIST((UINT4));
PUBLIC tHlMatrixControl* Rmon2HlMatrixAddCtrlEntry ARG_LIST((UINT4));
PUBLIC INT4 Rmon2HlMatrixDelCtrlEntry ARG_LIST((tHlMatrixControl *));
PUBLIC tHlMatrixControl* Rmon2HlMatrixGetCtrlEntry ARG_LIST((UINT4));
PUBLIC tNlMatrixSD* Rmon2NlMatrixGetNextDataIndex ARG_LIST((UINT4 , UINT4 , UINT4 ,
                                                     tIpAddr *, tIpAddr *));
PUBLIC tNlMatrixSD* Rmon2NlMatrixAddDataEntry ARG_LIST((UINT4 , UINT4 ,
                                                 tIpAddr *, tIpAddr *));
PUBLIC INT4 Rmon2NlMatrixDelDataEntry ARG_LIST((tNlMatrixSD *));
PUBLIC tNlMatrixSD* Rmon2NlMatrixGetDataEntry ARG_LIST((UINT4 , UINT4 , UINT4 , 
                                                 tIpAddr *, tIpAddr *));
PUBLIC VOID Rmon2NlMatrixDelInterDep ARG_LIST((tNlMatrixSD *));
PUBLIC tAlMatrixSD* Rmon2AlMatrixGetNextDataIndex ARG_LIST((UINT4 , UINT4 , UINT4 , 
                                           tIpAddr *, tIpAddr *, UINT4));
PUBLIC tAlMatrixSD* Rmon2AlMatrixAddDataEntry ARG_LIST((UINT4 , UINT4 ,
                         tIpAddr *, tIpAddr *, UINT4));
PUBLIC INT4 Rmon2AlMatrixDelDataEntry ARG_LIST((tAlMatrixSD *));
PUBLIC tAlMatrixSD* Rmon2AlMatrixGetDataEntry ARG_LIST((UINT4 , UINT4 , UINT4 , 
                                       tIpAddr *, tIpAddr *, UINT4));
PUBLIC VOID Rmon2AlMatrixDelInterDep ARG_LIST((tAlMatrixSD *));
PUBLIC VOID Rmon2MatrixPopulateDataEntries ARG_LIST((tHlMatrixControl *));

/* NlTopN APIs */
PUBLIC VOID Rmon2NlMatrixTopNNotifyCtlChgs ARG_LIST((UINT4, UINT4));
PUBLIC VOID Rmon2NlDeleteTopNEntries ARG_LIST((tNlMatrixTopNControl *));
PUBLIC tNlMatrixTopNControl* Rmon2NlMatrixGetNextTopNCtlIndex (UINT4);
PUBLIC tNlMatrixTopNControl* Rmon2NlMatrixAddTopNCtlEntry (UINT4);
PUBLIC INT4 Rmon2NlMatrixDelTopNCtlEntry (tNlMatrixTopNControl *);
PUBLIC tNlMatrixTopNControl* Rmon2NlMatrixGetTopNCtlEntry (UINT4);
PUBLIC tNlMatrixTopN* Rmon2NlMatrixGetNextTopNIndex (UINT4,UINT4);
PUBLIC tNlMatrixTopN* Rmon2NlMatrixAddTopNEntry (UINT4,tNlMatrixTopN *);
PUBLIC INT4 Rmon2NlMatrixDelTopNEntry (tNlMatrixTopN *);
PUBLIC tNlMatrixTopN* Rmon2NlMatrixGetTopNEntry (UINT4,UINT4);
PUBLIC VOID Rmon2UpdateNlMatrixTopNTable ARG_LIST ((VOID));

/* AlTopN APIs */
PUBLIC VOID Rmon2AlMatrixTopNNotifyCtlChgs ARG_LIST((UINT4, UINT4));
PUBLIC VOID Rmon2AlDeleteTopNEntries ARG_LIST((tAlMatrixTopNControl *));
PUBLIC tAlMatrixTopNControl* Rmon2AlMatrixGetNextTopNCtlIndex (UINT4);
PUBLIC tAlMatrixTopNControl* Rmon2AlMatrixAddTopNCtlEntry (UINT4);
PUBLIC INT4 Rmon2AlMatrixDelTopNCtlEntry (tAlMatrixTopNControl *);
PUBLIC tAlMatrixTopNControl* Rmon2AlMatrixGetTopNCtlEntry (UINT4);
PUBLIC tAlMatrixTopN* Rmon2AlMatrixGetNextTopNIndex (UINT4,UINT4);
PUBLIC tAlMatrixTopN* Rmon2AlMatrixAddTopNEntry (UINT4,tAlMatrixTopN *);
PUBLIC INT4 Rmon2AlMatrixDelTopNEntry (tAlMatrixTopN *);
PUBLIC tAlMatrixTopN* Rmon2AlMatrixGetTopNEntry (UINT4,UINT4);
PUBLIC VOID Rmon2UpdateAlMatrixTopNTable ARG_LIST((VOID));

/* Usr History APIs */
PUBLIC tUsrHistoryControl* Rmon2UsrHistGetCtrlEntry (UINT4);
PUBLIC tUsrHistoryControl* Rmon2UsrHistGetNextCtrlIndex (UINT4);
PUBLIC tUsrHistoryControl* Rmon2UsrHistAddCtrlEntry (UINT4);
PUBLIC INT4 Rmon2UsrHistDelCtrlEntry (tUsrHistoryControl *);
PUBLIC tUsrHistoryObject* Rmon2UsrHistGetNextObjectIndex (UINT4, UINT4);
PUBLIC UINT4 Rmon2UsrHistAddObjEntry (UINT4);
PUBLIC INT4 Rmon2UsrHistDelObjEntry (UINT4);
PUBLIC tUsrHistoryObject* Rmon2UsrHistGetObjectEntry (UINT4,UINT4);
PUBLIC tUsrHistory* Rmon2UsrHistGetNextDataIndex (UINT4,UINT4,UINT4);
PUBLIC INT4 Rmon2UsrHistAddDataEntry (UINT4);
PUBLIC INT4 Rmon2UsrHistDelDataEntry (UINT4);
PUBLIC tUsrHistory* Rmon2UsrHistGetDataEntry (UINT4,UINT4,UINT4);

PUBLIC VOID Rmon2UpdateUsrHistoryTable ARG_LIST ((VOID));

/* ProbeConfig APIs */
PUBLIC VOID Rmon2ProbeConfigInitProbeInfo ARG_LIST((VOID));
PUBLIC INT1 nmhGetIssHardwareVersion ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
UINT4  issGetSwitchDate (tSNMP_OCTET_STRING_TYPE *);

/* RBTree Compare Util APIs */
PUBLIC INT4 Rmon2UtlRBCmpPktInfo ARG_LIST((tRBElem *, tRBElem *));
PUBLIC INT4 Rmon2UtlRBCmpProtocolDir ARG_LIST((tRBElem *, tRBElem *));
PUBLIC INT4 Rmon2UtlRBCmpProtocolDistCtrl ARG_LIST((tRBElem *, tRBElem *));
PUBLIC INT4 Rmon2UtlRBCmpProtocolDistStats ARG_LIST((tRBElem *, tRBElem *));
PUBLIC INT4 Rmon2UtlRBCmpAddressMapCtrl ARG_LIST((tRBElem *, tRBElem *));
PUBLIC INT4 Rmon2UtlRBCmpAddressMap ARG_LIST((tRBElem *, tRBElem *));
PUBLIC INT4 Rmon2UtlRBCmpHlHostCtrl ARG_LIST((tRBElem *, tRBElem *));
PUBLIC INT4 Rmon2UtlRBCmpNlHost ARG_LIST((tRBElem *, tRBElem *));
PUBLIC INT4 Rmon2UtlRBCmpAlHost ARG_LIST((tRBElem *, tRBElem *));
PUBLIC INT4 Rmon2UtlRBCmpHlMatrixCtrl ARG_LIST((tRBElem *, tRBElem *));
PUBLIC INT4 Rmon2UtlRBCmpNlMatrixSD ARG_LIST((tRBElem *, tRBElem *));
PUBLIC INT4 Rmon2UtlRBCmpNlMatrixDS ARG_LIST((tRBElem *, tRBElem *));
PUBLIC INT4 Rmon2UtlRBCmpAlMatrixSD ARG_LIST((tRBElem *, tRBElem *));
PUBLIC INT4 Rmon2UtlRBCmpAlMatrixDS ARG_LIST((tRBElem *, tRBElem *));
PUBLIC INT4 Rmon2UtlRBCmpNlMatrixTopNCtrl ARG_LIST((tRBElem *, tRBElem *));
PUBLIC INT4 Rmon2UtlRBCmpNlMatrixTopN ARG_LIST((tRBElem *, tRBElem *));
PUBLIC INT4 Rmon2UtlRBCmpAlMatrixTopNCtrl ARG_LIST((tRBElem *, tRBElem *));
PUBLIC INT4 Rmon2UtlRBCmpAlMatrixTopN ARG_LIST((tRBElem *, tRBElem *));
PUBLIC INT4 Rmon2UtlRBCmpUsrHistoryCtrl ARG_LIST((tRBElem *, tRBElem *));

/* RBTree Free Util APIs */
PUBLIC INT4 Rmon2UtlRBFreePktInfo ARG_LIST((tRBElem *, UINT4));
PUBLIC INT4 Rmon2UtlRBFreeProtocolDir ARG_LIST((tRBElem *, UINT4));
PUBLIC INT4 Rmon2UtlRBFreeProtocolDistCtrl ARG_LIST((tRBElem *, UINT4));
PUBLIC INT4 Rmon2UtlRBFreeProtocolDistStats ARG_LIST((tRBElem *, UINT4));
PUBLIC INT4 Rmon2UtlRBFreeAddressMapCtrl ARG_LIST((tRBElem *, UINT4));
PUBLIC INT4 Rmon2UtlRBFreeAddressMap ARG_LIST((tRBElem *, UINT4));
PUBLIC INT4 Rmon2UtlRBFreeHlHostCtrl ARG_LIST((tRBElem *, UINT4));
PUBLIC INT4 Rmon2UtlRBFreeNlHost ARG_LIST((tRBElem *, UINT4));
PUBLIC INT4 Rmon2UtlRBFreeAlHost ARG_LIST((tRBElem *, UINT4));
PUBLIC INT4 Rmon2UtlRBFreeHlMatrixCtrl ARG_LIST((tRBElem *, UINT4));
PUBLIC INT4 Rmon2UtlRBFreeNlMatrixSD ARG_LIST((tRBElem *, UINT4));
PUBLIC INT4 Rmon2UtlRBFreeAlMatrixSD ARG_LIST((tRBElem *, UINT4));
PUBLIC INT4 Rmon2UtlRBFreeNlMatrixTopNCtrl ARG_LIST((tRBElem *, UINT4));
PUBLIC INT4 Rmon2UtlRBFreeNlMatrixTopN ARG_LIST((tRBElem *, UINT4));
PUBLIC INT4 Rmon2UtlRBFreeAlMatrixTopNCtrl ARG_LIST((tRBElem *, UINT4));
PUBLIC INT4 Rmon2UtlRBFreeAlMatrixTopN ARG_LIST((tRBElem *, UINT4));
PUBLIC INT4 Rmon2UtlRBFreeUsrHistoryCtrl ARG_LIST((tRBElem *, UINT4));

PUBLIC VOID Rmon2UtlFormDataSourceOid ARG_LIST((tSNMP_OID_TYPE *, UINT4));
PUBLIC VOID Rmon2UtlSetDataSourceOid ARG_LIST((tSNMP_OID_TYPE *, UINT4 *));
PUBLIC INT1 Rmon2UtlTestDataSourceOid  ARG_LIST((tSNMP_OID_TYPE *));
PUBLIC tSNMP_OID_TYPE *rmon2_alloc_oid (INT4 );
void rmon2_free_oid(tSNMP_OID_TYPE * temp);
void rmon2_free_octetstring(tSNMP_OCTET_STRING_TYPE * temp);
tSNMP_OCTET_STRING_TYPE * rmon2_allocmem_octetstring(INT4 i4_size);

PUBLIC INT4 Rmon2UtlDelAllCtrlEntries ARG_LIST((VOID));

INT1 nmhGetIssFirmwareVersion ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
INT1 nmhGetIssSwitchDate ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
INT1 nmhGetIssConfigSaveStatus ARG_LIST((INT4 *));
INT1 nmhGetIssConfigRestoreStatus ARG_LIST((INT4 *));
INT1 nmhSetIssInitiateConfigSave ARG_LIST((INT4 ));
INT1 nmhSetIssConfigSaveOption ARG_LIST((INT4 ));
INT1 nmhSetIssConfigRestoreOption ARG_LIST((INT4 ));
INT1 nmhTestv2IssSwitchDate ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
INT1 nmhSetIssSwitchDate ARG_LIST((tSNMP_OCTET_STRING_TYPE *));


#ifdef RMON_WANTED
PUBLIC INT1 nmhValidateIndexInstanceEtherStatsTable ARG_LIST((INT4 ));
PUBLIC INT1 nmhGetFirstIndexEtherStatsTable ARG_LIST((INT4 *));
PUBLIC INT1 nmhGetNextIndexEtherStatsTable ARG_LIST((INT4 , INT4 *));
PUBLIC INT1 nmhGetFirstIndexHistoryControlTable ARG_LIST((INT4 *));
PUBLIC INT1 nmhGetNextIndexHistoryControlTable ARG_LIST((INT4 , INT4 *));
PUBLIC INT1 nmhValidateIndexInstanceHistoryControlTable ARG_LIST((INT4 ));
PUBLIC INT1 nmhValidateIndexInstanceHostControlTable ARG_LIST((INT4 ));
PUBLIC INT1 nmhGetFirstIndexHostControlTable ARG_LIST((INT4 *));
PUBLIC INT1 nmhGetNextIndexHostControlTable ARG_LIST((INT4 , INT4 *));
PUBLIC INT1 nmhValidateIndexInstanceMatrixControlTable ARG_LIST((INT4 ));
PUBLIC INT1 nmhGetFirstIndexMatrixControlTable ARG_LIST((INT4 *));
PUBLIC INT1 nmhGetNextIndexMatrixControlTable ARG_LIST((INT4 , INT4 *));
#endif /* RMON_WANTED */
extern UINT1* CfaGddGetLnxIntfnameForPort (UINT2 u2Index);
extern INT4 CfaIwfRmon2UpdatePktHdr PROTO((UINT1 *, tPktHeader *));
#endif
