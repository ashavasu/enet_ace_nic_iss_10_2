/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: sdrmn2db.h,v 1.4 2011/03/25 12:12:07 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _SDRMN2DB_H
#define _SDRMN2DB_H

UINT1 ProtocolDirTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 ProtocolDistControlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 ProtocolDistStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 AddressMapControlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 AddressMapTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OBJECT_ID};
UINT1 HlHostControlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 NlHostTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 HlMatrixControlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 NlMatrixSDTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 NlMatrixDSTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 NlMatrixTopNControlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 NlMatrixTopNTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 AlHostTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 AlMatrixSDTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 AlMatrixDSTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 AlMatrixTopNControlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 AlMatrixTopNTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 UsrHistoryControlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 UsrHistoryObjectTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 UsrHistoryTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 SerialConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 NetConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 TrapDestTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 SerialConnectionTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 EtherStats2TableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 HistoryControl2TableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 HostControl2TableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 MatrixControl2TableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Channel2TableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 TokenRingMLStats2TableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 TokenRingPStats2TableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 RingStationControl2TableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 SourceRoutingStats2TableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Filter2TableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 sdrmn2 [] ={1,3,6,1,2,1,16};
tSNMP_OID_TYPE sdrmn2OID = {7, sdrmn2};


/* Generated OID's for tables */
UINT4 ProtocolDirTable [] ={1,3,6,1,2,1,16,11,2};
tSNMP_OID_TYPE ProtocolDirTableOID = {9, ProtocolDirTable};


UINT4 ProtocolDistControlTable [] ={1,3,6,1,2,1,16,12,1};
tSNMP_OID_TYPE ProtocolDistControlTableOID = {9, ProtocolDistControlTable};


UINT4 ProtocolDistStatsTable [] ={1,3,6,1,2,1,16,12,2};
tSNMP_OID_TYPE ProtocolDistStatsTableOID = {9, ProtocolDistStatsTable};


UINT4 AddressMapControlTable [] ={1,3,6,1,2,1,16,13,4};
tSNMP_OID_TYPE AddressMapControlTableOID = {9, AddressMapControlTable};


UINT4 AddressMapTable [] ={1,3,6,1,2,1,16,13,5};
tSNMP_OID_TYPE AddressMapTableOID = {9, AddressMapTable};


UINT4 HlHostControlTable [] ={1,3,6,1,2,1,16,14,1};
tSNMP_OID_TYPE HlHostControlTableOID = {9, HlHostControlTable};


UINT4 NlHostTable [] ={1,3,6,1,2,1,16,14,2};
tSNMP_OID_TYPE NlHostTableOID = {9, NlHostTable};


UINT4 HlMatrixControlTable [] ={1,3,6,1,2,1,16,15,1};
tSNMP_OID_TYPE HlMatrixControlTableOID = {9, HlMatrixControlTable};


UINT4 NlMatrixSDTable [] ={1,3,6,1,2,1,16,15,2};
tSNMP_OID_TYPE NlMatrixSDTableOID = {9, NlMatrixSDTable};


UINT4 NlMatrixDSTable [] ={1,3,6,1,2,1,16,15,3};
tSNMP_OID_TYPE NlMatrixDSTableOID = {9, NlMatrixDSTable};


UINT4 NlMatrixTopNControlTable [] ={1,3,6,1,2,1,16,15,4};
tSNMP_OID_TYPE NlMatrixTopNControlTableOID = {9, NlMatrixTopNControlTable};


UINT4 NlMatrixTopNTable [] ={1,3,6,1,2,1,16,15,5};
tSNMP_OID_TYPE NlMatrixTopNTableOID = {9, NlMatrixTopNTable};


UINT4 AlHostTable [] ={1,3,6,1,2,1,16,16,1};
tSNMP_OID_TYPE AlHostTableOID = {9, AlHostTable};


UINT4 AlMatrixSDTable [] ={1,3,6,1,2,1,16,17,1};
tSNMP_OID_TYPE AlMatrixSDTableOID = {9, AlMatrixSDTable};


UINT4 AlMatrixDSTable [] ={1,3,6,1,2,1,16,17,2};
tSNMP_OID_TYPE AlMatrixDSTableOID = {9, AlMatrixDSTable};


UINT4 AlMatrixTopNControlTable [] ={1,3,6,1,2,1,16,17,3};
tSNMP_OID_TYPE AlMatrixTopNControlTableOID = {9, AlMatrixTopNControlTable};


UINT4 AlMatrixTopNTable [] ={1,3,6,1,2,1,16,17,4};
tSNMP_OID_TYPE AlMatrixTopNTableOID = {9, AlMatrixTopNTable};


UINT4 UsrHistoryControlTable [] ={1,3,6,1,2,1,16,18,1};
tSNMP_OID_TYPE UsrHistoryControlTableOID = {9, UsrHistoryControlTable};


UINT4 UsrHistoryObjectTable [] ={1,3,6,1,2,1,16,18,2};
tSNMP_OID_TYPE UsrHistoryObjectTableOID = {9, UsrHistoryObjectTable};


UINT4 UsrHistoryTable [] ={1,3,6,1,2,1,16,18,3};
tSNMP_OID_TYPE UsrHistoryTableOID = {9, UsrHistoryTable};


UINT4 SerialConfigTable [] ={1,3,6,1,2,1,16,19,10};
tSNMP_OID_TYPE SerialConfigTableOID = {9, SerialConfigTable};


UINT4 NetConfigTable [] ={1,3,6,1,2,1,16,19,11};
tSNMP_OID_TYPE NetConfigTableOID = {9, NetConfigTable};


UINT4 TrapDestTable [] ={1,3,6,1,2,1,16,19,13};
tSNMP_OID_TYPE TrapDestTableOID = {9, TrapDestTable};


UINT4 SerialConnectionTable [] ={1,3,6,1,2,1,16,19,14};
tSNMP_OID_TYPE SerialConnectionTableOID = {9, SerialConnectionTable};


UINT4 EtherStats2Table [] ={1,3,6,1,2,1,16,1,4};
tSNMP_OID_TYPE EtherStats2TableOID = {9, EtherStats2Table};


UINT4 HistoryControl2Table [] ={1,3,6,1,2,1,16,2,5};
tSNMP_OID_TYPE HistoryControl2TableOID = {9, HistoryControl2Table};


UINT4 HostControl2Table [] ={1,3,6,1,2,1,16,4,4};
tSNMP_OID_TYPE HostControl2TableOID = {9, HostControl2Table};


UINT4 MatrixControl2Table [] ={1,3,6,1,2,1,16,6,4};
tSNMP_OID_TYPE MatrixControl2TableOID = {9, MatrixControl2Table};


UINT4 Channel2Table [] ={1,3,6,1,2,1,16,7,3};
tSNMP_OID_TYPE Channel2TableOID = {9, Channel2Table};


UINT4 TokenRingMLStats2Table [] ={1,3,6,1,2,1,16,1,5};
tSNMP_OID_TYPE TokenRingMLStats2TableOID = {9, TokenRingMLStats2Table};


UINT4 TokenRingPStats2Table [] ={1,3,6,1,2,1,16,1,6};
tSNMP_OID_TYPE TokenRingPStats2TableOID = {9, TokenRingPStats2Table};


UINT4 RingStationControl2Table [] ={1,3,6,1,2,1,16,10,7};
tSNMP_OID_TYPE RingStationControl2TableOID = {9, RingStationControl2Table};


UINT4 SourceRoutingStats2Table [] ={1,3,6,1,2,1,16,10,8};
tSNMP_OID_TYPE SourceRoutingStats2TableOID = {9, SourceRoutingStats2Table};


UINT4 Filter2Table [] ={1,3,6,1,2,1,16,7,4};
tSNMP_OID_TYPE Filter2TableOID = {9, Filter2Table};




UINT4 ProtocolDirLastChange [ ] ={1,3,6,1,2,1,16,11,1};
UINT4 ProtocolDirID [ ] ={1,3,6,1,2,1,16,11,2,1,1};
UINT4 ProtocolDirParameters [ ] ={1,3,6,1,2,1,16,11,2,1,2};
UINT4 ProtocolDirLocalIndex [ ] ={1,3,6,1,2,1,16,11,2,1,3};
UINT4 ProtocolDirDescr [ ] ={1,3,6,1,2,1,16,11,2,1,4};
UINT4 ProtocolDirType [ ] ={1,3,6,1,2,1,16,11,2,1,5};
UINT4 ProtocolDirAddressMapConfig [ ] ={1,3,6,1,2,1,16,11,2,1,6};
UINT4 ProtocolDirHostConfig [ ] ={1,3,6,1,2,1,16,11,2,1,7};
UINT4 ProtocolDirMatrixConfig [ ] ={1,3,6,1,2,1,16,11,2,1,8};
UINT4 ProtocolDirOwner [ ] ={1,3,6,1,2,1,16,11,2,1,9};
UINT4 ProtocolDirStatus [ ] ={1,3,6,1,2,1,16,11,2,1,10};
UINT4 ProtocolDistControlIndex [ ] ={1,3,6,1,2,1,16,12,1,1,1};
UINT4 ProtocolDistControlDataSource [ ] ={1,3,6,1,2,1,16,12,1,1,2};
UINT4 ProtocolDistControlDroppedFrames [ ] ={1,3,6,1,2,1,16,12,1,1,3};
UINT4 ProtocolDistControlCreateTime [ ] ={1,3,6,1,2,1,16,12,1,1,4};
UINT4 ProtocolDistControlOwner [ ] ={1,3,6,1,2,1,16,12,1,1,5};
UINT4 ProtocolDistControlStatus [ ] ={1,3,6,1,2,1,16,12,1,1,6};
UINT4 ProtocolDistStatsPkts [ ] ={1,3,6,1,2,1,16,12,2,1,1};
UINT4 ProtocolDistStatsOctets [ ] ={1,3,6,1,2,1,16,12,2,1,2};
UINT4 AddressMapInserts [ ] ={1,3,6,1,2,1,16,13,1};
UINT4 AddressMapDeletes [ ] ={1,3,6,1,2,1,16,13,2};
UINT4 AddressMapMaxDesiredEntries [ ] ={1,3,6,1,2,1,16,13,3};
UINT4 AddressMapControlIndex [ ] ={1,3,6,1,2,1,16,13,4,1,1};
UINT4 AddressMapControlDataSource [ ] ={1,3,6,1,2,1,16,13,4,1,2};
UINT4 AddressMapControlDroppedFrames [ ] ={1,3,6,1,2,1,16,13,4,1,3};
UINT4 AddressMapControlOwner [ ] ={1,3,6,1,2,1,16,13,4,1,4};
UINT4 AddressMapControlStatus [ ] ={1,3,6,1,2,1,16,13,4,1,5};
UINT4 AddressMapTimeMark [ ] ={1,3,6,1,2,1,16,13,5,1,1};
UINT4 AddressMapNetworkAddress [ ] ={1,3,6,1,2,1,16,13,5,1,2};
UINT4 AddressMapSource [ ] ={1,3,6,1,2,1,16,13,5,1,3};
UINT4 AddressMapPhysicalAddress [ ] ={1,3,6,1,2,1,16,13,5,1,4};
UINT4 AddressMapLastChange [ ] ={1,3,6,1,2,1,16,13,5,1,5};
UINT4 HlHostControlIndex [ ] ={1,3,6,1,2,1,16,14,1,1,1};
UINT4 HlHostControlDataSource [ ] ={1,3,6,1,2,1,16,14,1,1,2};
UINT4 HlHostControlNlDroppedFrames [ ] ={1,3,6,1,2,1,16,14,1,1,3};
UINT4 HlHostControlNlInserts [ ] ={1,3,6,1,2,1,16,14,1,1,4};
UINT4 HlHostControlNlDeletes [ ] ={1,3,6,1,2,1,16,14,1,1,5};
UINT4 HlHostControlNlMaxDesiredEntries [ ] ={1,3,6,1,2,1,16,14,1,1,6};
UINT4 HlHostControlAlDroppedFrames [ ] ={1,3,6,1,2,1,16,14,1,1,7};
UINT4 HlHostControlAlInserts [ ] ={1,3,6,1,2,1,16,14,1,1,8};
UINT4 HlHostControlAlDeletes [ ] ={1,3,6,1,2,1,16,14,1,1,9};
UINT4 HlHostControlAlMaxDesiredEntries [ ] ={1,3,6,1,2,1,16,14,1,1,10};
UINT4 HlHostControlOwner [ ] ={1,3,6,1,2,1,16,14,1,1,11};
UINT4 HlHostControlStatus [ ] ={1,3,6,1,2,1,16,14,1,1,12};
UINT4 NlHostTimeMark [ ] ={1,3,6,1,2,1,16,14,2,1,1};
UINT4 NlHostAddress [ ] ={1,3,6,1,2,1,16,14,2,1,2};
UINT4 NlHostInPkts [ ] ={1,3,6,1,2,1,16,14,2,1,3};
UINT4 NlHostOutPkts [ ] ={1,3,6,1,2,1,16,14,2,1,4};
UINT4 NlHostInOctets [ ] ={1,3,6,1,2,1,16,14,2,1,5};
UINT4 NlHostOutOctets [ ] ={1,3,6,1,2,1,16,14,2,1,6};
UINT4 NlHostOutMacNonUnicastPkts [ ] ={1,3,6,1,2,1,16,14,2,1,7};
UINT4 NlHostCreateTime [ ] ={1,3,6,1,2,1,16,14,2,1,8};
UINT4 HlMatrixControlIndex [ ] ={1,3,6,1,2,1,16,15,1,1,1};
UINT4 HlMatrixControlDataSource [ ] ={1,3,6,1,2,1,16,15,1,1,2};
UINT4 HlMatrixControlNlDroppedFrames [ ] ={1,3,6,1,2,1,16,15,1,1,3};
UINT4 HlMatrixControlNlInserts [ ] ={1,3,6,1,2,1,16,15,1,1,4};
UINT4 HlMatrixControlNlDeletes [ ] ={1,3,6,1,2,1,16,15,1,1,5};
UINT4 HlMatrixControlNlMaxDesiredEntries [ ] ={1,3,6,1,2,1,16,15,1,1,6};
UINT4 HlMatrixControlAlDroppedFrames [ ] ={1,3,6,1,2,1,16,15,1,1,7};
UINT4 HlMatrixControlAlInserts [ ] ={1,3,6,1,2,1,16,15,1,1,8};
UINT4 HlMatrixControlAlDeletes [ ] ={1,3,6,1,2,1,16,15,1,1,9};
UINT4 HlMatrixControlAlMaxDesiredEntries [ ] ={1,3,6,1,2,1,16,15,1,1,10};
UINT4 HlMatrixControlOwner [ ] ={1,3,6,1,2,1,16,15,1,1,11};
UINT4 HlMatrixControlStatus [ ] ={1,3,6,1,2,1,16,15,1,1,12};
UINT4 NlMatrixSDTimeMark [ ] ={1,3,6,1,2,1,16,15,2,1,1};
UINT4 NlMatrixSDSourceAddress [ ] ={1,3,6,1,2,1,16,15,2,1,2};
UINT4 NlMatrixSDDestAddress [ ] ={1,3,6,1,2,1,16,15,2,1,3};
UINT4 NlMatrixSDPkts [ ] ={1,3,6,1,2,1,16,15,2,1,4};
UINT4 NlMatrixSDOctets [ ] ={1,3,6,1,2,1,16,15,2,1,5};
UINT4 NlMatrixSDCreateTime [ ] ={1,3,6,1,2,1,16,15,2,1,6};
UINT4 NlMatrixDSTimeMark [ ] ={1,3,6,1,2,1,16,15,3,1,1};
UINT4 NlMatrixDSSourceAddress [ ] ={1,3,6,1,2,1,16,15,3,1,2};
UINT4 NlMatrixDSDestAddress [ ] ={1,3,6,1,2,1,16,15,3,1,3};
UINT4 NlMatrixDSPkts [ ] ={1,3,6,1,2,1,16,15,3,1,4};
UINT4 NlMatrixDSOctets [ ] ={1,3,6,1,2,1,16,15,3,1,5};
UINT4 NlMatrixDSCreateTime [ ] ={1,3,6,1,2,1,16,15,3,1,6};
UINT4 NlMatrixTopNControlIndex [ ] ={1,3,6,1,2,1,16,15,4,1,1};
UINT4 NlMatrixTopNControlMatrixIndex [ ] ={1,3,6,1,2,1,16,15,4,1,2};
UINT4 NlMatrixTopNControlRateBase [ ] ={1,3,6,1,2,1,16,15,4,1,3};
UINT4 NlMatrixTopNControlTimeRemaining [ ] ={1,3,6,1,2,1,16,15,4,1,4};
UINT4 NlMatrixTopNControlGeneratedReports [ ] ={1,3,6,1,2,1,16,15,4,1,5};
UINT4 NlMatrixTopNControlDuration [ ] ={1,3,6,1,2,1,16,15,4,1,6};
UINT4 NlMatrixTopNControlRequestedSize [ ] ={1,3,6,1,2,1,16,15,4,1,7};
UINT4 NlMatrixTopNControlGrantedSize [ ] ={1,3,6,1,2,1,16,15,4,1,8};
UINT4 NlMatrixTopNControlStartTime [ ] ={1,3,6,1,2,1,16,15,4,1,9};
UINT4 NlMatrixTopNControlOwner [ ] ={1,3,6,1,2,1,16,15,4,1,10};
UINT4 NlMatrixTopNControlStatus [ ] ={1,3,6,1,2,1,16,15,4,1,11};
UINT4 NlMatrixTopNIndex [ ] ={1,3,6,1,2,1,16,15,5,1,1};
UINT4 NlMatrixTopNProtocolDirLocalIndex [ ] ={1,3,6,1,2,1,16,15,5,1,2};
UINT4 NlMatrixTopNSourceAddress [ ] ={1,3,6,1,2,1,16,15,5,1,3};
UINT4 NlMatrixTopNDestAddress [ ] ={1,3,6,1,2,1,16,15,5,1,4};
UINT4 NlMatrixTopNPktRate [ ] ={1,3,6,1,2,1,16,15,5,1,5};
UINT4 NlMatrixTopNReversePktRate [ ] ={1,3,6,1,2,1,16,15,5,1,6};
UINT4 NlMatrixTopNOctetRate [ ] ={1,3,6,1,2,1,16,15,5,1,7};
UINT4 NlMatrixTopNReverseOctetRate [ ] ={1,3,6,1,2,1,16,15,5,1,8};
UINT4 AlHostTimeMark [ ] ={1,3,6,1,2,1,16,16,1,1,1};
UINT4 AlHostInPkts [ ] ={1,3,6,1,2,1,16,16,1,1,2};
UINT4 AlHostOutPkts [ ] ={1,3,6,1,2,1,16,16,1,1,3};
UINT4 AlHostInOctets [ ] ={1,3,6,1,2,1,16,16,1,1,4};
UINT4 AlHostOutOctets [ ] ={1,3,6,1,2,1,16,16,1,1,5};
UINT4 AlHostCreateTime [ ] ={1,3,6,1,2,1,16,16,1,1,6};
UINT4 AlMatrixSDTimeMark [ ] ={1,3,6,1,2,1,16,17,1,1,1};
UINT4 AlMatrixSDPkts [ ] ={1,3,6,1,2,1,16,17,1,1,2};
UINT4 AlMatrixSDOctets [ ] ={1,3,6,1,2,1,16,17,1,1,3};
UINT4 AlMatrixSDCreateTime [ ] ={1,3,6,1,2,1,16,17,1,1,4};
UINT4 AlMatrixDSTimeMark [ ] ={1,3,6,1,2,1,16,17,2,1,1};
UINT4 AlMatrixDSPkts [ ] ={1,3,6,1,2,1,16,17,2,1,2};
UINT4 AlMatrixDSOctets [ ] ={1,3,6,1,2,1,16,17,2,1,3};
UINT4 AlMatrixDSCreateTime [ ] ={1,3,6,1,2,1,16,17,2,1,4};
UINT4 AlMatrixTopNControlIndex [ ] ={1,3,6,1,2,1,16,17,3,1,1};
UINT4 AlMatrixTopNControlMatrixIndex [ ] ={1,3,6,1,2,1,16,17,3,1,2};
UINT4 AlMatrixTopNControlRateBase [ ] ={1,3,6,1,2,1,16,17,3,1,3};
UINT4 AlMatrixTopNControlTimeRemaining [ ] ={1,3,6,1,2,1,16,17,3,1,4};
UINT4 AlMatrixTopNControlGeneratedReports [ ] ={1,3,6,1,2,1,16,17,3,1,5};
UINT4 AlMatrixTopNControlDuration [ ] ={1,3,6,1,2,1,16,17,3,1,6};
UINT4 AlMatrixTopNControlRequestedSize [ ] ={1,3,6,1,2,1,16,17,3,1,7};
UINT4 AlMatrixTopNControlGrantedSize [ ] ={1,3,6,1,2,1,16,17,3,1,8};
UINT4 AlMatrixTopNControlStartTime [ ] ={1,3,6,1,2,1,16,17,3,1,9};
UINT4 AlMatrixTopNControlOwner [ ] ={1,3,6,1,2,1,16,17,3,1,10};
UINT4 AlMatrixTopNControlStatus [ ] ={1,3,6,1,2,1,16,17,3,1,11};
UINT4 AlMatrixTopNIndex [ ] ={1,3,6,1,2,1,16,17,4,1,1};
UINT4 AlMatrixTopNProtocolDirLocalIndex [ ] ={1,3,6,1,2,1,16,17,4,1,2};
UINT4 AlMatrixTopNSourceAddress [ ] ={1,3,6,1,2,1,16,17,4,1,3};
UINT4 AlMatrixTopNDestAddress [ ] ={1,3,6,1,2,1,16,17,4,1,4};
UINT4 AlMatrixTopNAppProtocolDirLocalIndex [ ] ={1,3,6,1,2,1,16,17,4,1,5};
UINT4 AlMatrixTopNPktRate [ ] ={1,3,6,1,2,1,16,17,4,1,6};
UINT4 AlMatrixTopNReversePktRate [ ] ={1,3,6,1,2,1,16,17,4,1,7};
UINT4 AlMatrixTopNOctetRate [ ] ={1,3,6,1,2,1,16,17,4,1,8};
UINT4 AlMatrixTopNReverseOctetRate [ ] ={1,3,6,1,2,1,16,17,4,1,9};
UINT4 UsrHistoryControlIndex [ ] ={1,3,6,1,2,1,16,18,1,1,1};
UINT4 UsrHistoryControlObjects [ ] ={1,3,6,1,2,1,16,18,1,1,2};
UINT4 UsrHistoryControlBucketsRequested [ ] ={1,3,6,1,2,1,16,18,1,1,3};
UINT4 UsrHistoryControlBucketsGranted [ ] ={1,3,6,1,2,1,16,18,1,1,4};
UINT4 UsrHistoryControlInterval [ ] ={1,3,6,1,2,1,16,18,1,1,5};
UINT4 UsrHistoryControlOwner [ ] ={1,3,6,1,2,1,16,18,1,1,6};
UINT4 UsrHistoryControlStatus [ ] ={1,3,6,1,2,1,16,18,1,1,7};
UINT4 UsrHistoryObjectIndex [ ] ={1,3,6,1,2,1,16,18,2,1,1};
UINT4 UsrHistoryObjectVariable [ ] ={1,3,6,1,2,1,16,18,2,1,2};
UINT4 UsrHistoryObjectSampleType [ ] ={1,3,6,1,2,1,16,18,2,1,3};
UINT4 UsrHistorySampleIndex [ ] ={1,3,6,1,2,1,16,18,3,1,1};
UINT4 UsrHistoryIntervalStart [ ] ={1,3,6,1,2,1,16,18,3,1,2};
UINT4 UsrHistoryIntervalEnd [ ] ={1,3,6,1,2,1,16,18,3,1,3};
UINT4 UsrHistoryAbsValue [ ] ={1,3,6,1,2,1,16,18,3,1,4};
UINT4 UsrHistoryValStatus [ ] ={1,3,6,1,2,1,16,18,3,1,5};
UINT4 ProbeCapabilities [ ] ={1,3,6,1,2,1,16,19,1};
UINT4 ProbeSoftwareRev [ ] ={1,3,6,1,2,1,16,19,2};
UINT4 ProbeHardwareRev [ ] ={1,3,6,1,2,1,16,19,3};
UINT4 ProbeDateTime [ ] ={1,3,6,1,2,1,16,19,4};
UINT4 ProbeResetControl [ ] ={1,3,6,1,2,1,16,19,5};
UINT4 ProbeDownloadFile [ ] ={1,3,6,1,2,1,16,19,6};
UINT4 ProbeDownloadTFTPServer [ ] ={1,3,6,1,2,1,16,19,7};
UINT4 ProbeDownloadAction [ ] ={1,3,6,1,2,1,16,19,8};
UINT4 ProbeDownloadStatus [ ] ={1,3,6,1,2,1,16,19,9};
UINT4 SerialMode [ ] ={1,3,6,1,2,1,16,19,10,1,1};
UINT4 SerialProtocol [ ] ={1,3,6,1,2,1,16,19,10,1,2};
UINT4 SerialTimeout [ ] ={1,3,6,1,2,1,16,19,10,1,3};
UINT4 SerialModemInitString [ ] ={1,3,6,1,2,1,16,19,10,1,4};
UINT4 SerialModemHangUpString [ ] ={1,3,6,1,2,1,16,19,10,1,5};
UINT4 SerialModemConnectResp [ ] ={1,3,6,1,2,1,16,19,10,1,6};
UINT4 SerialModemNoConnectResp [ ] ={1,3,6,1,2,1,16,19,10,1,7};
UINT4 SerialDialoutTimeout [ ] ={1,3,6,1,2,1,16,19,10,1,8};
UINT4 SerialStatus [ ] ={1,3,6,1,2,1,16,19,10,1,9};
UINT4 NetConfigIPAddress [ ] ={1,3,6,1,2,1,16,19,11,1,1};
UINT4 NetConfigSubnetMask [ ] ={1,3,6,1,2,1,16,19,11,1,2};
UINT4 NetConfigStatus [ ] ={1,3,6,1,2,1,16,19,11,1,3};
UINT4 NetDefaultGateway [ ] ={1,3,6,1,2,1,16,19,12};
UINT4 TrapDestIndex [ ] ={1,3,6,1,2,1,16,19,13,1,1};
UINT4 TrapDestCommunity [ ] ={1,3,6,1,2,1,16,19,13,1,2};
UINT4 TrapDestProtocol [ ] ={1,3,6,1,2,1,16,19,13,1,3};
UINT4 TrapDestAddress [ ] ={1,3,6,1,2,1,16,19,13,1,4};
UINT4 TrapDestOwner [ ] ={1,3,6,1,2,1,16,19,13,1,5};
UINT4 TrapDestStatus [ ] ={1,3,6,1,2,1,16,19,13,1,6};
UINT4 SerialConnectIndex [ ] ={1,3,6,1,2,1,16,19,14,1,1};
UINT4 SerialConnectDestIpAddress [ ] ={1,3,6,1,2,1,16,19,14,1,2};
UINT4 SerialConnectType [ ] ={1,3,6,1,2,1,16,19,14,1,3};
UINT4 SerialConnectDialString [ ] ={1,3,6,1,2,1,16,19,14,1,4};
UINT4 SerialConnectSwitchConnectSeq [ ] ={1,3,6,1,2,1,16,19,14,1,5};
UINT4 SerialConnectSwitchDisconnectSeq [ ] ={1,3,6,1,2,1,16,19,14,1,6};
UINT4 SerialConnectSwitchResetSeq [ ] ={1,3,6,1,2,1,16,19,14,1,7};
UINT4 SerialConnectOwner [ ] ={1,3,6,1,2,1,16,19,14,1,8};
UINT4 SerialConnectStatus [ ] ={1,3,6,1,2,1,16,19,14,1,9};
UINT4 EtherStatsDroppedFrames [ ] ={1,3,6,1,2,1,16,1,4,1,1};
UINT4 EtherStatsCreateTime [ ] ={1,3,6,1,2,1,16,1,4,1,2};
UINT4 HistoryControlDroppedFrames [ ] ={1,3,6,1,2,1,16,2,5,1,1};
UINT4 HostControlDroppedFrames [ ] ={1,3,6,1,2,1,16,4,4,1,1};
UINT4 HostControlCreateTime [ ] ={1,3,6,1,2,1,16,4,4,1,2};
UINT4 MatrixControlDroppedFrames [ ] ={1,3,6,1,2,1,16,6,4,1,1};
UINT4 MatrixControlCreateTime [ ] ={1,3,6,1,2,1,16,6,4,1,2};
UINT4 ChannelDroppedFrames [ ] ={1,3,6,1,2,1,16,7,3,1,1};
UINT4 ChannelCreateTime [ ] ={1,3,6,1,2,1,16,7,3,1,2};
UINT4 TokenRingMLStatsDroppedFrames [ ] ={1,3,6,1,2,1,16,1,5,1,1};
UINT4 TokenRingMLStatsCreateTime [ ] ={1,3,6,1,2,1,16,1,5,1,2};
UINT4 TokenRingPStatsDroppedFrames [ ] ={1,3,6,1,2,1,16,1,6,1,1};
UINT4 TokenRingPStatsCreateTime [ ] ={1,3,6,1,2,1,16,1,6,1,2};
UINT4 RingStationControlDroppedFrames [ ] ={1,3,6,1,2,1,16,10,7,1,1};
UINT4 RingStationControlCreateTime [ ] ={1,3,6,1,2,1,16,10,7,1,2};
UINT4 SourceRoutingStatsDroppedFrames [ ] ={1,3,6,1,2,1,16,10,8,1,1};
UINT4 SourceRoutingStatsCreateTime [ ] ={1,3,6,1,2,1,16,10,8,1,2};
UINT4 FilterProtocolDirDataLocalIndex [ ] ={1,3,6,1,2,1,16,7,4,1,1};
UINT4 FilterProtocolDirLocalIndex [ ] ={1,3,6,1,2,1,16,7,4,1,2};


tSNMP_OID_TYPE ProtocolDirLastChangeOID = {9, ProtocolDirLastChange};


tSNMP_OID_TYPE AddressMapInsertsOID = {9, AddressMapInserts};


tSNMP_OID_TYPE AddressMapDeletesOID = {9, AddressMapDeletes};


tSNMP_OID_TYPE AddressMapMaxDesiredEntriesOID = {9, AddressMapMaxDesiredEntries};


tSNMP_OID_TYPE ProbeCapabilitiesOID = {9, ProbeCapabilities};


tSNMP_OID_TYPE ProbeSoftwareRevOID = {9, ProbeSoftwareRev};


tSNMP_OID_TYPE ProbeHardwareRevOID = {9, ProbeHardwareRev};


tSNMP_OID_TYPE ProbeDateTimeOID = {9, ProbeDateTime};


tSNMP_OID_TYPE ProbeResetControlOID = {9, ProbeResetControl};


tSNMP_OID_TYPE ProbeDownloadFileOID = {9, ProbeDownloadFile};


tSNMP_OID_TYPE ProbeDownloadTFTPServerOID = {9, ProbeDownloadTFTPServer};


tSNMP_OID_TYPE ProbeDownloadActionOID = {9, ProbeDownloadAction};


tSNMP_OID_TYPE ProbeDownloadStatusOID = {9, ProbeDownloadStatus};


tSNMP_OID_TYPE NetDefaultGatewayOID = {9, NetDefaultGateway};




tMbDbEntry EtherStats2TableMibEntry[]= {

{{11,EtherStatsDroppedFrames}, GetNextIndexEtherStats2Table, EtherStatsDroppedFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStats2TableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsCreateTime}, GetNextIndexEtherStats2Table, EtherStatsCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, EtherStats2TableINDEX, 1, 0, 0, NULL},
};
tMibData EtherStats2TableEntry = { 2, EtherStats2TableMibEntry };

tMbDbEntry TokenRingMLStats2TableMibEntry[]= {

{{11,TokenRingMLStatsDroppedFrames}, GetNextIndexTokenRingMLStats2Table, TokenRingMLStatsDroppedFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, TokenRingMLStats2TableINDEX, 1, 1, 0, NULL},

{{11,TokenRingMLStatsCreateTime}, GetNextIndexTokenRingMLStats2Table, TokenRingMLStatsCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, TokenRingMLStats2TableINDEX, 1, 1, 0, NULL},
};
tMibData TokenRingMLStats2TableEntry = { 2, TokenRingMLStats2TableMibEntry };

tMbDbEntry TokenRingPStats2TableMibEntry[]= {

{{11,TokenRingPStatsDroppedFrames}, GetNextIndexTokenRingPStats2Table, TokenRingPStatsDroppedFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, TokenRingPStats2TableINDEX, 1, 1, 0, NULL},

{{11,TokenRingPStatsCreateTime}, GetNextIndexTokenRingPStats2Table, TokenRingPStatsCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, TokenRingPStats2TableINDEX, 1, 1, 0, NULL},
};
tMibData TokenRingPStats2TableEntry = { 2, TokenRingPStats2TableMibEntry };

tMbDbEntry HistoryControl2TableMibEntry[]= {

{{11,HistoryControlDroppedFrames}, GetNextIndexHistoryControl2Table, HistoryControlDroppedFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HistoryControl2TableINDEX, 1, 0, 0, NULL},
};
tMibData HistoryControl2TableEntry = { 1, HistoryControl2TableMibEntry };

tMbDbEntry HostControl2TableMibEntry[]= {

{{11,HostControlDroppedFrames}, GetNextIndexHostControl2Table, HostControlDroppedFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HostControl2TableINDEX, 1, 0, 0, NULL},

{{11,HostControlCreateTime}, GetNextIndexHostControl2Table, HostControlCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, HostControl2TableINDEX, 1, 0, 0, NULL},
};
tMibData HostControl2TableEntry = { 2, HostControl2TableMibEntry };

tMbDbEntry MatrixControl2TableMibEntry[]= {

{{11,MatrixControlDroppedFrames}, GetNextIndexMatrixControl2Table, MatrixControlDroppedFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MatrixControl2TableINDEX, 1, 0, 0, NULL},

{{11,MatrixControlCreateTime}, GetNextIndexMatrixControl2Table, MatrixControlCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MatrixControl2TableINDEX, 1, 0, 0, NULL},
};
tMibData MatrixControl2TableEntry = { 2, MatrixControl2TableMibEntry };

tMbDbEntry Channel2TableMibEntry[]= {

{{11,ChannelDroppedFrames}, GetNextIndexChannel2Table, ChannelDroppedFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Channel2TableINDEX, 1, 0, 0, NULL},

{{11,ChannelCreateTime}, GetNextIndexChannel2Table, ChannelCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Channel2TableINDEX, 1, 0, 0, NULL},
};
tMibData Channel2TableEntry = { 2, Channel2TableMibEntry };

tMbDbEntry Filter2TableMibEntry[]= {

{{11,FilterProtocolDirDataLocalIndex}, GetNextIndexFilter2Table, FilterProtocolDirDataLocalIndexGet, FilterProtocolDirDataLocalIndexSet, FilterProtocolDirDataLocalIndexTest, Filter2TableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Filter2TableINDEX, 1, 0, 0, "0"},

{{11,FilterProtocolDirLocalIndex}, GetNextIndexFilter2Table, FilterProtocolDirLocalIndexGet, FilterProtocolDirLocalIndexSet, FilterProtocolDirLocalIndexTest, Filter2TableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Filter2TableINDEX, 1, 0, 0, "0"},
};
tMibData Filter2TableEntry = { 2, Filter2TableMibEntry };

tMbDbEntry RingStationControl2TableMibEntry[]= {

{{11,RingStationControlDroppedFrames}, GetNextIndexRingStationControl2Table, RingStationControlDroppedFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, RingStationControl2TableINDEX, 1, 1, 0, NULL},

{{11,RingStationControlCreateTime}, GetNextIndexRingStationControl2Table, RingStationControlCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, RingStationControl2TableINDEX, 1, 1, 0, NULL},
};
tMibData RingStationControl2TableEntry = { 2, RingStationControl2TableMibEntry };

tMbDbEntry SourceRoutingStats2TableMibEntry[]= {

{{11,SourceRoutingStatsDroppedFrames}, GetNextIndexSourceRoutingStats2Table, SourceRoutingStatsDroppedFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, SourceRoutingStats2TableINDEX, 1, 1, 0, NULL},

{{11,SourceRoutingStatsCreateTime}, GetNextIndexSourceRoutingStats2Table, SourceRoutingStatsCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, SourceRoutingStats2TableINDEX, 1, 1, 0, NULL},
};
tMibData SourceRoutingStats2TableEntry = { 2, SourceRoutingStats2TableMibEntry };

tMbDbEntry ProtocolDirLastChangeMibEntry[]= {

{{9,ProtocolDirLastChange}, NULL, ProtocolDirLastChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData ProtocolDirLastChangeEntry = { 1, ProtocolDirLastChangeMibEntry };

tMbDbEntry ProtocolDirTableMibEntry[]= {

{{11,ProtocolDirID}, GetNextIndexProtocolDirTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, ProtocolDirTableINDEX, 2, 0, 0, NULL},

{{11,ProtocolDirParameters}, GetNextIndexProtocolDirTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, ProtocolDirTableINDEX, 2, 0, 0, NULL},

{{11,ProtocolDirLocalIndex}, GetNextIndexProtocolDirTable, ProtocolDirLocalIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, ProtocolDirTableINDEX, 2, 0, 0, NULL},

{{11,ProtocolDirDescr}, GetNextIndexProtocolDirTable, ProtocolDirDescrGet, ProtocolDirDescrSet, ProtocolDirDescrTest, ProtocolDirTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, ProtocolDirTableINDEX, 2, 0, 0, NULL},

{{11,ProtocolDirType}, GetNextIndexProtocolDirTable, ProtocolDirTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, ProtocolDirTableINDEX, 2, 0, 0, NULL},

{{11,ProtocolDirAddressMapConfig}, GetNextIndexProtocolDirTable, ProtocolDirAddressMapConfigGet, ProtocolDirAddressMapConfigSet, ProtocolDirAddressMapConfigTest, ProtocolDirTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, ProtocolDirTableINDEX, 2, 0, 0, NULL},

{{11,ProtocolDirHostConfig}, GetNextIndexProtocolDirTable, ProtocolDirHostConfigGet, ProtocolDirHostConfigSet, ProtocolDirHostConfigTest, ProtocolDirTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, ProtocolDirTableINDEX, 2, 0, 0, NULL},

{{11,ProtocolDirMatrixConfig}, GetNextIndexProtocolDirTable, ProtocolDirMatrixConfigGet, ProtocolDirMatrixConfigSet, ProtocolDirMatrixConfigTest, ProtocolDirTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, ProtocolDirTableINDEX, 2, 0, 0, NULL},

{{11,ProtocolDirOwner}, GetNextIndexProtocolDirTable, ProtocolDirOwnerGet, ProtocolDirOwnerSet, ProtocolDirOwnerTest, ProtocolDirTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, ProtocolDirTableINDEX, 2, 0, 0, NULL},

{{11,ProtocolDirStatus}, GetNextIndexProtocolDirTable, ProtocolDirStatusGet, ProtocolDirStatusSet, ProtocolDirStatusTest, ProtocolDirTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, ProtocolDirTableINDEX, 2, 0, 1, NULL},
};
tMibData ProtocolDirTableEntry = { 10, ProtocolDirTableMibEntry };

tMbDbEntry ProtocolDistControlTableMibEntry[]= {

{{11,ProtocolDistControlIndex}, GetNextIndexProtocolDistControlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, ProtocolDistControlTableINDEX, 1, 0, 0, NULL},

{{11,ProtocolDistControlDataSource}, GetNextIndexProtocolDistControlTable, ProtocolDistControlDataSourceGet, ProtocolDistControlDataSourceSet, ProtocolDistControlDataSourceTest, ProtocolDistControlTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, ProtocolDistControlTableINDEX, 1, 0, 0, NULL},

{{11,ProtocolDistControlDroppedFrames}, GetNextIndexProtocolDistControlTable, ProtocolDistControlDroppedFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, ProtocolDistControlTableINDEX, 1, 0, 0, NULL},

{{11,ProtocolDistControlCreateTime}, GetNextIndexProtocolDistControlTable, ProtocolDistControlCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, ProtocolDistControlTableINDEX, 1, 0, 0, NULL},

{{11,ProtocolDistControlOwner}, GetNextIndexProtocolDistControlTable, ProtocolDistControlOwnerGet, ProtocolDistControlOwnerSet, ProtocolDistControlOwnerTest, ProtocolDistControlTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, ProtocolDistControlTableINDEX, 1, 0, 0, NULL},

{{11,ProtocolDistControlStatus}, GetNextIndexProtocolDistControlTable, ProtocolDistControlStatusGet, ProtocolDistControlStatusSet, ProtocolDistControlStatusTest, ProtocolDistControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, ProtocolDistControlTableINDEX, 1, 0, 1, NULL},
};
tMibData ProtocolDistControlTableEntry = { 6, ProtocolDistControlTableMibEntry };

tMbDbEntry ProtocolDistStatsTableMibEntry[]= {

{{11,ProtocolDistStatsPkts}, GetNextIndexProtocolDistStatsTable, ProtocolDistStatsPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, ProtocolDistStatsTableINDEX, 2, 0, 0, NULL},

{{11,ProtocolDistStatsOctets}, GetNextIndexProtocolDistStatsTable, ProtocolDistStatsOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, ProtocolDistStatsTableINDEX, 2, 0, 0, NULL},
};
tMibData ProtocolDistStatsTableEntry = { 2, ProtocolDistStatsTableMibEntry };

tMbDbEntry AddressMapInsertsMibEntry[]= {

{{9,AddressMapInserts}, NULL, AddressMapInsertsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData AddressMapInsertsEntry = { 1, AddressMapInsertsMibEntry };

tMbDbEntry AddressMapDeletesMibEntry[]= {

{{9,AddressMapDeletes}, NULL, AddressMapDeletesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData AddressMapDeletesEntry = { 1, AddressMapDeletesMibEntry };

tMbDbEntry AddressMapMaxDesiredEntriesMibEntry[]= {

{{9,AddressMapMaxDesiredEntries}, NULL, AddressMapMaxDesiredEntriesGet, AddressMapMaxDesiredEntriesSet, AddressMapMaxDesiredEntriesTest, AddressMapMaxDesiredEntriesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData AddressMapMaxDesiredEntriesEntry = { 1, AddressMapMaxDesiredEntriesMibEntry };

tMbDbEntry AddressMapControlTableMibEntry[]= {

{{11,AddressMapControlIndex}, GetNextIndexAddressMapControlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, AddressMapControlTableINDEX, 1, 0, 0, NULL},

{{11,AddressMapControlDataSource}, GetNextIndexAddressMapControlTable, AddressMapControlDataSourceGet, AddressMapControlDataSourceSet, AddressMapControlDataSourceTest, AddressMapControlTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, AddressMapControlTableINDEX, 1, 0, 0, NULL},

{{11,AddressMapControlDroppedFrames}, GetNextIndexAddressMapControlTable, AddressMapControlDroppedFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, AddressMapControlTableINDEX, 1, 0, 0, NULL},

{{11,AddressMapControlOwner}, GetNextIndexAddressMapControlTable, AddressMapControlOwnerGet, AddressMapControlOwnerSet, AddressMapControlOwnerTest, AddressMapControlTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, AddressMapControlTableINDEX, 1, 0, 0, NULL},

{{11,AddressMapControlStatus}, GetNextIndexAddressMapControlTable, AddressMapControlStatusGet, AddressMapControlStatusSet, AddressMapControlStatusTest, AddressMapControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, AddressMapControlTableINDEX, 1, 0, 1, NULL},
};
tMibData AddressMapControlTableEntry = { 5, AddressMapControlTableMibEntry };

tMbDbEntry AddressMapTableMibEntry[]= {

{{11,AddressMapTimeMark}, GetNextIndexAddressMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, AddressMapTableINDEX, 4, 0, 0, NULL},

{{11,AddressMapNetworkAddress}, GetNextIndexAddressMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, AddressMapTableINDEX, 4, 0, 0, NULL},

{{11,AddressMapSource}, GetNextIndexAddressMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_NOACCESS, AddressMapTableINDEX, 4, 0, 0, NULL},

{{11,AddressMapPhysicalAddress}, GetNextIndexAddressMapTable, AddressMapPhysicalAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, AddressMapTableINDEX, 4, 0, 0, NULL},

{{11,AddressMapLastChange}, GetNextIndexAddressMapTable, AddressMapLastChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, AddressMapTableINDEX, 4, 0, 0, NULL},
};
tMibData AddressMapTableEntry = { 5, AddressMapTableMibEntry };

tMbDbEntry HlHostControlTableMibEntry[]= {

{{11,HlHostControlIndex}, GetNextIndexHlHostControlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, HlHostControlTableINDEX, 1, 0, 0, NULL},

{{11,HlHostControlDataSource}, GetNextIndexHlHostControlTable, HlHostControlDataSourceGet, HlHostControlDataSourceSet, HlHostControlDataSourceTest, HlHostControlTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, HlHostControlTableINDEX, 1, 0, 0, NULL},

{{11,HlHostControlNlDroppedFrames}, GetNextIndexHlHostControlTable, HlHostControlNlDroppedFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HlHostControlTableINDEX, 1, 0, 0, NULL},

{{11,HlHostControlNlInserts}, GetNextIndexHlHostControlTable, HlHostControlNlInsertsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HlHostControlTableINDEX, 1, 0, 0, NULL},

{{11,HlHostControlNlDeletes}, GetNextIndexHlHostControlTable, HlHostControlNlDeletesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HlHostControlTableINDEX, 1, 0, 0, NULL},

{{11,HlHostControlNlMaxDesiredEntries}, GetNextIndexHlHostControlTable, HlHostControlNlMaxDesiredEntriesGet, HlHostControlNlMaxDesiredEntriesSet, HlHostControlNlMaxDesiredEntriesTest, HlHostControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, HlHostControlTableINDEX, 1, 0, 0, NULL},

{{11,HlHostControlAlDroppedFrames}, GetNextIndexHlHostControlTable, HlHostControlAlDroppedFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HlHostControlTableINDEX, 1, 0, 0, NULL},

{{11,HlHostControlAlInserts}, GetNextIndexHlHostControlTable, HlHostControlAlInsertsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HlHostControlTableINDEX, 1, 0, 0, NULL},

{{11,HlHostControlAlDeletes}, GetNextIndexHlHostControlTable, HlHostControlAlDeletesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HlHostControlTableINDEX, 1, 0, 0, NULL},

{{11,HlHostControlAlMaxDesiredEntries}, GetNextIndexHlHostControlTable, HlHostControlAlMaxDesiredEntriesGet, HlHostControlAlMaxDesiredEntriesSet, HlHostControlAlMaxDesiredEntriesTest, HlHostControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, HlHostControlTableINDEX, 1, 0, 0, NULL},

{{11,HlHostControlOwner}, GetNextIndexHlHostControlTable, HlHostControlOwnerGet, HlHostControlOwnerSet, HlHostControlOwnerTest, HlHostControlTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, HlHostControlTableINDEX, 1, 0, 0, NULL},

{{11,HlHostControlStatus}, GetNextIndexHlHostControlTable, HlHostControlStatusGet, HlHostControlStatusSet, HlHostControlStatusTest, HlHostControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, HlHostControlTableINDEX, 1, 0, 1, NULL},
};
tMibData HlHostControlTableEntry = { 12, HlHostControlTableMibEntry };

tMbDbEntry NlHostTableMibEntry[]= {

{{11,NlHostTimeMark}, GetNextIndexNlHostTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, NlHostTableINDEX, 4, 0, 0, NULL},

{{11,NlHostAddress}, GetNextIndexNlHostTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, NlHostTableINDEX, 4, 0, 0, NULL},

{{11,NlHostInPkts}, GetNextIndexNlHostTable, NlHostInPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NlHostTableINDEX, 4, 0, 0, NULL},

{{11,NlHostOutPkts}, GetNextIndexNlHostTable, NlHostOutPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NlHostTableINDEX, 4, 0, 0, NULL},

{{11,NlHostInOctets}, GetNextIndexNlHostTable, NlHostInOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NlHostTableINDEX, 4, 0, 0, NULL},

{{11,NlHostOutOctets}, GetNextIndexNlHostTable, NlHostOutOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NlHostTableINDEX, 4, 0, 0, NULL},

{{11,NlHostOutMacNonUnicastPkts}, GetNextIndexNlHostTable, NlHostOutMacNonUnicastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NlHostTableINDEX, 4, 0, 0, NULL},

{{11,NlHostCreateTime}, GetNextIndexNlHostTable, NlHostCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NlHostTableINDEX, 4, 0, 0, NULL},
};
tMibData NlHostTableEntry = { 8, NlHostTableMibEntry };

tMbDbEntry HlMatrixControlTableMibEntry[]= {

{{11,HlMatrixControlIndex}, GetNextIndexHlMatrixControlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, HlMatrixControlTableINDEX, 1, 0, 0, NULL},

{{11,HlMatrixControlDataSource}, GetNextIndexHlMatrixControlTable, HlMatrixControlDataSourceGet, HlMatrixControlDataSourceSet, HlMatrixControlDataSourceTest, HlMatrixControlTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, HlMatrixControlTableINDEX, 1, 0, 0, NULL},

{{11,HlMatrixControlNlDroppedFrames}, GetNextIndexHlMatrixControlTable, HlMatrixControlNlDroppedFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HlMatrixControlTableINDEX, 1, 0, 0, NULL},

{{11,HlMatrixControlNlInserts}, GetNextIndexHlMatrixControlTable, HlMatrixControlNlInsertsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HlMatrixControlTableINDEX, 1, 0, 0, NULL},

{{11,HlMatrixControlNlDeletes}, GetNextIndexHlMatrixControlTable, HlMatrixControlNlDeletesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HlMatrixControlTableINDEX, 1, 0, 0, NULL},

{{11,HlMatrixControlNlMaxDesiredEntries}, GetNextIndexHlMatrixControlTable, HlMatrixControlNlMaxDesiredEntriesGet, HlMatrixControlNlMaxDesiredEntriesSet, HlMatrixControlNlMaxDesiredEntriesTest, HlMatrixControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, HlMatrixControlTableINDEX, 1, 0, 0, NULL},

{{11,HlMatrixControlAlDroppedFrames}, GetNextIndexHlMatrixControlTable, HlMatrixControlAlDroppedFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HlMatrixControlTableINDEX, 1, 0, 0, NULL},

{{11,HlMatrixControlAlInserts}, GetNextIndexHlMatrixControlTable, HlMatrixControlAlInsertsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HlMatrixControlTableINDEX, 1, 0, 0, NULL},

{{11,HlMatrixControlAlDeletes}, GetNextIndexHlMatrixControlTable, HlMatrixControlAlDeletesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HlMatrixControlTableINDEX, 1, 0, 0, NULL},

{{11,HlMatrixControlAlMaxDesiredEntries}, GetNextIndexHlMatrixControlTable, HlMatrixControlAlMaxDesiredEntriesGet, HlMatrixControlAlMaxDesiredEntriesSet, HlMatrixControlAlMaxDesiredEntriesTest, HlMatrixControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, HlMatrixControlTableINDEX, 1, 0, 0, NULL},

{{11,HlMatrixControlOwner}, GetNextIndexHlMatrixControlTable, HlMatrixControlOwnerGet, HlMatrixControlOwnerSet, HlMatrixControlOwnerTest, HlMatrixControlTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, HlMatrixControlTableINDEX, 1, 0, 0, NULL},

{{11,HlMatrixControlStatus}, GetNextIndexHlMatrixControlTable, HlMatrixControlStatusGet, HlMatrixControlStatusSet, HlMatrixControlStatusTest, HlMatrixControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, HlMatrixControlTableINDEX, 1, 0, 1, NULL},
};
tMibData HlMatrixControlTableEntry = { 12, HlMatrixControlTableMibEntry };

tMbDbEntry NlMatrixSDTableMibEntry[]= {

{{11,NlMatrixSDTimeMark}, GetNextIndexNlMatrixSDTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, NlMatrixSDTableINDEX, 5, 0, 0, NULL},

{{11,NlMatrixSDSourceAddress}, GetNextIndexNlMatrixSDTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, NlMatrixSDTableINDEX, 5, 0, 0, NULL},

{{11,NlMatrixSDDestAddress}, GetNextIndexNlMatrixSDTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, NlMatrixSDTableINDEX, 5, 0, 0, NULL},

{{11,NlMatrixSDPkts}, GetNextIndexNlMatrixSDTable, NlMatrixSDPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NlMatrixSDTableINDEX, 5, 0, 0, NULL},

{{11,NlMatrixSDOctets}, GetNextIndexNlMatrixSDTable, NlMatrixSDOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NlMatrixSDTableINDEX, 5, 0, 0, NULL},

{{11,NlMatrixSDCreateTime}, GetNextIndexNlMatrixSDTable, NlMatrixSDCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NlMatrixSDTableINDEX, 5, 0, 0, NULL},
};
tMibData NlMatrixSDTableEntry = { 6, NlMatrixSDTableMibEntry };

tMbDbEntry NlMatrixDSTableMibEntry[]= {

{{11,NlMatrixDSTimeMark}, GetNextIndexNlMatrixDSTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, NlMatrixDSTableINDEX, 5, 0, 0, NULL},

{{11,NlMatrixDSSourceAddress}, GetNextIndexNlMatrixDSTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, NlMatrixDSTableINDEX, 5, 0, 0, NULL},

{{11,NlMatrixDSDestAddress}, GetNextIndexNlMatrixDSTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, NlMatrixDSTableINDEX, 5, 0, 0, NULL},

{{11,NlMatrixDSPkts}, GetNextIndexNlMatrixDSTable, NlMatrixDSPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NlMatrixDSTableINDEX, 5, 0, 0, NULL},

{{11,NlMatrixDSOctets}, GetNextIndexNlMatrixDSTable, NlMatrixDSOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NlMatrixDSTableINDEX, 5, 0, 0, NULL},

{{11,NlMatrixDSCreateTime}, GetNextIndexNlMatrixDSTable, NlMatrixDSCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NlMatrixDSTableINDEX, 5, 0, 0, NULL},
};
tMibData NlMatrixDSTableEntry = { 6, NlMatrixDSTableMibEntry };

tMbDbEntry NlMatrixTopNControlTableMibEntry[]= {

{{11,NlMatrixTopNControlIndex}, GetNextIndexNlMatrixTopNControlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, NlMatrixTopNControlTableINDEX, 1, 0, 0, NULL},

{{11,NlMatrixTopNControlMatrixIndex}, GetNextIndexNlMatrixTopNControlTable, NlMatrixTopNControlMatrixIndexGet, NlMatrixTopNControlMatrixIndexSet, NlMatrixTopNControlMatrixIndexTest, NlMatrixTopNControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NlMatrixTopNControlTableINDEX, 1, 0, 0, NULL},

{{11,NlMatrixTopNControlRateBase}, GetNextIndexNlMatrixTopNControlTable, NlMatrixTopNControlRateBaseGet, NlMatrixTopNControlRateBaseSet, NlMatrixTopNControlRateBaseTest, NlMatrixTopNControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NlMatrixTopNControlTableINDEX, 1, 0, 0, NULL},

{{11,NlMatrixTopNControlTimeRemaining}, GetNextIndexNlMatrixTopNControlTable, NlMatrixTopNControlTimeRemainingGet, NlMatrixTopNControlTimeRemainingSet, NlMatrixTopNControlTimeRemainingTest, NlMatrixTopNControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NlMatrixTopNControlTableINDEX, 1, 0, 0, "1800"},

{{11,NlMatrixTopNControlGeneratedReports}, GetNextIndexNlMatrixTopNControlTable, NlMatrixTopNControlGeneratedReportsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NlMatrixTopNControlTableINDEX, 1, 0, 0, NULL},

{{11,NlMatrixTopNControlDuration}, GetNextIndexNlMatrixTopNControlTable, NlMatrixTopNControlDurationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NlMatrixTopNControlTableINDEX, 1, 0, 0, NULL},

{{11,NlMatrixTopNControlRequestedSize}, GetNextIndexNlMatrixTopNControlTable, NlMatrixTopNControlRequestedSizeGet, NlMatrixTopNControlRequestedSizeSet, NlMatrixTopNControlRequestedSizeTest, NlMatrixTopNControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NlMatrixTopNControlTableINDEX, 1, 0, 0, "150"},

{{11,NlMatrixTopNControlGrantedSize}, GetNextIndexNlMatrixTopNControlTable, NlMatrixTopNControlGrantedSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NlMatrixTopNControlTableINDEX, 1, 0, 0, NULL},

{{11,NlMatrixTopNControlStartTime}, GetNextIndexNlMatrixTopNControlTable, NlMatrixTopNControlStartTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NlMatrixTopNControlTableINDEX, 1, 0, 0, NULL},

{{11,NlMatrixTopNControlOwner}, GetNextIndexNlMatrixTopNControlTable, NlMatrixTopNControlOwnerGet, NlMatrixTopNControlOwnerSet, NlMatrixTopNControlOwnerTest, NlMatrixTopNControlTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NlMatrixTopNControlTableINDEX, 1, 0, 0, NULL},

{{11,NlMatrixTopNControlStatus}, GetNextIndexNlMatrixTopNControlTable, NlMatrixTopNControlStatusGet, NlMatrixTopNControlStatusSet, NlMatrixTopNControlStatusTest, NlMatrixTopNControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NlMatrixTopNControlTableINDEX, 1, 0, 1, NULL},
};
tMibData NlMatrixTopNControlTableEntry = { 11, NlMatrixTopNControlTableMibEntry };

tMbDbEntry NlMatrixTopNTableMibEntry[]= {

{{11,NlMatrixTopNIndex}, GetNextIndexNlMatrixTopNTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, NlMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{11,NlMatrixTopNProtocolDirLocalIndex}, GetNextIndexNlMatrixTopNTable, NlMatrixTopNProtocolDirLocalIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NlMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{11,NlMatrixTopNSourceAddress}, GetNextIndexNlMatrixTopNTable, NlMatrixTopNSourceAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NlMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{11,NlMatrixTopNDestAddress}, GetNextIndexNlMatrixTopNTable, NlMatrixTopNDestAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NlMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{11,NlMatrixTopNPktRate}, GetNextIndexNlMatrixTopNTable, NlMatrixTopNPktRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NlMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{11,NlMatrixTopNReversePktRate}, GetNextIndexNlMatrixTopNTable, NlMatrixTopNReversePktRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NlMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{11,NlMatrixTopNOctetRate}, GetNextIndexNlMatrixTopNTable, NlMatrixTopNOctetRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NlMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{11,NlMatrixTopNReverseOctetRate}, GetNextIndexNlMatrixTopNTable, NlMatrixTopNReverseOctetRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NlMatrixTopNTableINDEX, 2, 0, 0, NULL},
};
tMibData NlMatrixTopNTableEntry = { 8, NlMatrixTopNTableMibEntry };

tMbDbEntry AlHostTableMibEntry[]= {

{{11,AlHostTimeMark}, GetNextIndexAlHostTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, AlHostTableINDEX, 5, 0, 0, NULL},

{{11,AlHostInPkts}, GetNextIndexAlHostTable, AlHostInPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, AlHostTableINDEX, 5, 0, 0, NULL},

{{11,AlHostOutPkts}, GetNextIndexAlHostTable, AlHostOutPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, AlHostTableINDEX, 5, 0, 0, NULL},

{{11,AlHostInOctets}, GetNextIndexAlHostTable, AlHostInOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, AlHostTableINDEX, 5, 0, 0, NULL},

{{11,AlHostOutOctets}, GetNextIndexAlHostTable, AlHostOutOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, AlHostTableINDEX, 5, 0, 0, NULL},

{{11,AlHostCreateTime}, GetNextIndexAlHostTable, AlHostCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, AlHostTableINDEX, 5, 0, 0, NULL},
};
tMibData AlHostTableEntry = { 6, AlHostTableMibEntry };

tMbDbEntry AlMatrixSDTableMibEntry[]= {

{{11,AlMatrixSDTimeMark}, GetNextIndexAlMatrixSDTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, AlMatrixSDTableINDEX, 6, 0, 0, NULL},

{{11,AlMatrixSDPkts}, GetNextIndexAlMatrixSDTable, AlMatrixSDPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, AlMatrixSDTableINDEX, 6, 0, 0, NULL},

{{11,AlMatrixSDOctets}, GetNextIndexAlMatrixSDTable, AlMatrixSDOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, AlMatrixSDTableINDEX, 6, 0, 0, NULL},

{{11,AlMatrixSDCreateTime}, GetNextIndexAlMatrixSDTable, AlMatrixSDCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, AlMatrixSDTableINDEX, 6, 0, 0, NULL},
};
tMibData AlMatrixSDTableEntry = { 4, AlMatrixSDTableMibEntry };

tMbDbEntry AlMatrixDSTableMibEntry[]= {

{{11,AlMatrixDSTimeMark}, GetNextIndexAlMatrixDSTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, AlMatrixDSTableINDEX, 6, 0, 0, NULL},

{{11,AlMatrixDSPkts}, GetNextIndexAlMatrixDSTable, AlMatrixDSPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, AlMatrixDSTableINDEX, 6, 0, 0, NULL},

{{11,AlMatrixDSOctets}, GetNextIndexAlMatrixDSTable, AlMatrixDSOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, AlMatrixDSTableINDEX, 6, 0, 0, NULL},

{{11,AlMatrixDSCreateTime}, GetNextIndexAlMatrixDSTable, AlMatrixDSCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, AlMatrixDSTableINDEX, 6, 0, 0, NULL},
};
tMibData AlMatrixDSTableEntry = { 4, AlMatrixDSTableMibEntry };

tMbDbEntry AlMatrixTopNControlTableMibEntry[]= {

{{11,AlMatrixTopNControlIndex}, GetNextIndexAlMatrixTopNControlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, AlMatrixTopNControlTableINDEX, 1, 0, 0, NULL},

{{11,AlMatrixTopNControlMatrixIndex}, GetNextIndexAlMatrixTopNControlTable, AlMatrixTopNControlMatrixIndexGet, AlMatrixTopNControlMatrixIndexSet, AlMatrixTopNControlMatrixIndexTest, AlMatrixTopNControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, AlMatrixTopNControlTableINDEX, 1, 0, 0, NULL},

{{11,AlMatrixTopNControlRateBase}, GetNextIndexAlMatrixTopNControlTable, AlMatrixTopNControlRateBaseGet, AlMatrixTopNControlRateBaseSet, AlMatrixTopNControlRateBaseTest, AlMatrixTopNControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, AlMatrixTopNControlTableINDEX, 1, 0, 0, NULL},

{{11,AlMatrixTopNControlTimeRemaining}, GetNextIndexAlMatrixTopNControlTable, AlMatrixTopNControlTimeRemainingGet, AlMatrixTopNControlTimeRemainingSet, AlMatrixTopNControlTimeRemainingTest, AlMatrixTopNControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, AlMatrixTopNControlTableINDEX, 1, 0, 0, "1800"},

{{11,AlMatrixTopNControlGeneratedReports}, GetNextIndexAlMatrixTopNControlTable, AlMatrixTopNControlGeneratedReportsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, AlMatrixTopNControlTableINDEX, 1, 0, 0, NULL},

{{11,AlMatrixTopNControlDuration}, GetNextIndexAlMatrixTopNControlTable, AlMatrixTopNControlDurationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, AlMatrixTopNControlTableINDEX, 1, 0, 0, NULL},

{{11,AlMatrixTopNControlRequestedSize}, GetNextIndexAlMatrixTopNControlTable, AlMatrixTopNControlRequestedSizeGet, AlMatrixTopNControlRequestedSizeSet, AlMatrixTopNControlRequestedSizeTest, AlMatrixTopNControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, AlMatrixTopNControlTableINDEX, 1, 0, 0, "150"},

{{11,AlMatrixTopNControlGrantedSize}, GetNextIndexAlMatrixTopNControlTable, AlMatrixTopNControlGrantedSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, AlMatrixTopNControlTableINDEX, 1, 0, 0, NULL},

{{11,AlMatrixTopNControlStartTime}, GetNextIndexAlMatrixTopNControlTable, AlMatrixTopNControlStartTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, AlMatrixTopNControlTableINDEX, 1, 0, 0, NULL},

{{11,AlMatrixTopNControlOwner}, GetNextIndexAlMatrixTopNControlTable, AlMatrixTopNControlOwnerGet, AlMatrixTopNControlOwnerSet, AlMatrixTopNControlOwnerTest, AlMatrixTopNControlTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, AlMatrixTopNControlTableINDEX, 1, 0, 0, NULL},

{{11,AlMatrixTopNControlStatus}, GetNextIndexAlMatrixTopNControlTable, AlMatrixTopNControlStatusGet, AlMatrixTopNControlStatusSet, AlMatrixTopNControlStatusTest, AlMatrixTopNControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, AlMatrixTopNControlTableINDEX, 1, 0, 1, NULL},
};
tMibData AlMatrixTopNControlTableEntry = { 11, AlMatrixTopNControlTableMibEntry };

tMbDbEntry AlMatrixTopNTableMibEntry[]= {

{{11,AlMatrixTopNIndex}, GetNextIndexAlMatrixTopNTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, AlMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{11,AlMatrixTopNProtocolDirLocalIndex}, GetNextIndexAlMatrixTopNTable, AlMatrixTopNProtocolDirLocalIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, AlMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{11,AlMatrixTopNSourceAddress}, GetNextIndexAlMatrixTopNTable, AlMatrixTopNSourceAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, AlMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{11,AlMatrixTopNDestAddress}, GetNextIndexAlMatrixTopNTable, AlMatrixTopNDestAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, AlMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{11,AlMatrixTopNAppProtocolDirLocalIndex}, GetNextIndexAlMatrixTopNTable, AlMatrixTopNAppProtocolDirLocalIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, AlMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{11,AlMatrixTopNPktRate}, GetNextIndexAlMatrixTopNTable, AlMatrixTopNPktRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, AlMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{11,AlMatrixTopNReversePktRate}, GetNextIndexAlMatrixTopNTable, AlMatrixTopNReversePktRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, AlMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{11,AlMatrixTopNOctetRate}, GetNextIndexAlMatrixTopNTable, AlMatrixTopNOctetRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, AlMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{11,AlMatrixTopNReverseOctetRate}, GetNextIndexAlMatrixTopNTable, AlMatrixTopNReverseOctetRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, AlMatrixTopNTableINDEX, 2, 0, 0, NULL},
};
tMibData AlMatrixTopNTableEntry = { 9, AlMatrixTopNTableMibEntry };

tMbDbEntry UsrHistoryControlTableMibEntry[]= {

{{11,UsrHistoryControlIndex}, GetNextIndexUsrHistoryControlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, UsrHistoryControlTableINDEX, 1, 0, 0, NULL},

{{11,UsrHistoryControlObjects}, GetNextIndexUsrHistoryControlTable, UsrHistoryControlObjectsGet, UsrHistoryControlObjectsSet, UsrHistoryControlObjectsTest, UsrHistoryControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, UsrHistoryControlTableINDEX, 1, 0, 0, NULL},

{{11,UsrHistoryControlBucketsRequested}, GetNextIndexUsrHistoryControlTable, UsrHistoryControlBucketsRequestedGet, UsrHistoryControlBucketsRequestedSet, UsrHistoryControlBucketsRequestedTest, UsrHistoryControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, UsrHistoryControlTableINDEX, 1, 0, 0, "50"},

{{11,UsrHistoryControlBucketsGranted}, GetNextIndexUsrHistoryControlTable, UsrHistoryControlBucketsGrantedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, UsrHistoryControlTableINDEX, 1, 0, 0, NULL},

{{11,UsrHistoryControlInterval}, GetNextIndexUsrHistoryControlTable, UsrHistoryControlIntervalGet, UsrHistoryControlIntervalSet, UsrHistoryControlIntervalTest, UsrHistoryControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, UsrHistoryControlTableINDEX, 1, 0, 0, "1800"},

{{11,UsrHistoryControlOwner}, GetNextIndexUsrHistoryControlTable, UsrHistoryControlOwnerGet, UsrHistoryControlOwnerSet, UsrHistoryControlOwnerTest, UsrHistoryControlTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, UsrHistoryControlTableINDEX, 1, 0, 0, NULL},

{{11,UsrHistoryControlStatus}, GetNextIndexUsrHistoryControlTable, UsrHistoryControlStatusGet, UsrHistoryControlStatusSet, UsrHistoryControlStatusTest, UsrHistoryControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, UsrHistoryControlTableINDEX, 1, 0, 1, NULL},
};
tMibData UsrHistoryControlTableEntry = { 7, UsrHistoryControlTableMibEntry };

tMbDbEntry UsrHistoryObjectTableMibEntry[]= {

{{11,UsrHistoryObjectIndex}, GetNextIndexUsrHistoryObjectTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, UsrHistoryObjectTableINDEX, 2, 0, 0, NULL},

{{11,UsrHistoryObjectVariable}, GetNextIndexUsrHistoryObjectTable, UsrHistoryObjectVariableGet, UsrHistoryObjectVariableSet, UsrHistoryObjectVariableTest, UsrHistoryObjectTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, UsrHistoryObjectTableINDEX, 2, 0, 0, NULL},

{{11,UsrHistoryObjectSampleType}, GetNextIndexUsrHistoryObjectTable, UsrHistoryObjectSampleTypeGet, UsrHistoryObjectSampleTypeSet, UsrHistoryObjectSampleTypeTest, UsrHistoryObjectTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, UsrHistoryObjectTableINDEX, 2, 0, 0, NULL},
};
tMibData UsrHistoryObjectTableEntry = { 3, UsrHistoryObjectTableMibEntry };

tMbDbEntry UsrHistoryTableMibEntry[]= {

{{11,UsrHistorySampleIndex}, GetNextIndexUsrHistoryTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, UsrHistoryTableINDEX, 3, 0, 0, NULL},

{{11,UsrHistoryIntervalStart}, GetNextIndexUsrHistoryTable, UsrHistoryIntervalStartGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, UsrHistoryTableINDEX, 3, 0, 0, NULL},

{{11,UsrHistoryIntervalEnd}, GetNextIndexUsrHistoryTable, UsrHistoryIntervalEndGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, UsrHistoryTableINDEX, 3, 0, 0, NULL},

{{11,UsrHistoryAbsValue}, GetNextIndexUsrHistoryTable, UsrHistoryAbsValueGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, UsrHistoryTableINDEX, 3, 0, 0, NULL},

{{11,UsrHistoryValStatus}, GetNextIndexUsrHistoryTable, UsrHistoryValStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, UsrHistoryTableINDEX, 3, 0, 0, NULL},
};
tMibData UsrHistoryTableEntry = { 5, UsrHistoryTableMibEntry };

tMbDbEntry ProbeCapabilitiesMibEntry[]= {

{{9,ProbeCapabilities}, NULL, ProbeCapabilitiesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData ProbeCapabilitiesEntry = { 1, ProbeCapabilitiesMibEntry };

tMbDbEntry ProbeSoftwareRevMibEntry[]= {

{{9,ProbeSoftwareRev}, NULL, ProbeSoftwareRevGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData ProbeSoftwareRevEntry = { 1, ProbeSoftwareRevMibEntry };

tMbDbEntry ProbeHardwareRevMibEntry[]= {

{{9,ProbeHardwareRev}, NULL, ProbeHardwareRevGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData ProbeHardwareRevEntry = { 1, ProbeHardwareRevMibEntry };

tMbDbEntry ProbeDateTimeMibEntry[]= {

{{9,ProbeDateTime}, NULL, ProbeDateTimeGet, ProbeDateTimeSet, ProbeDateTimeTest, ProbeDateTimeDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData ProbeDateTimeEntry = { 1, ProbeDateTimeMibEntry };

tMbDbEntry ProbeResetControlMibEntry[]= {

{{9,ProbeResetControl}, NULL, ProbeResetControlGet, ProbeResetControlSet, ProbeResetControlTest, ProbeResetControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData ProbeResetControlEntry = { 1, ProbeResetControlMibEntry };

tMbDbEntry ProbeDownloadFileMibEntry[]= {

{{9,ProbeDownloadFile}, NULL, ProbeDownloadFileGet, ProbeDownloadFileSet, ProbeDownloadFileTest, ProbeDownloadFileDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 1, 0, NULL},
};
tMibData ProbeDownloadFileEntry = { 1, ProbeDownloadFileMibEntry };

tMbDbEntry ProbeDownloadTFTPServerMibEntry[]= {

{{9,ProbeDownloadTFTPServer}, NULL, ProbeDownloadTFTPServerGet, ProbeDownloadTFTPServerSet, ProbeDownloadTFTPServerTest, ProbeDownloadTFTPServerDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 1, 0, NULL},
};
tMibData ProbeDownloadTFTPServerEntry = { 1, ProbeDownloadTFTPServerMibEntry };

tMbDbEntry ProbeDownloadActionMibEntry[]= {

{{9,ProbeDownloadAction}, NULL, ProbeDownloadActionGet, ProbeDownloadActionSet, ProbeDownloadActionTest, ProbeDownloadActionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 1, 0, NULL},
};
tMibData ProbeDownloadActionEntry = { 1, ProbeDownloadActionMibEntry };

tMbDbEntry ProbeDownloadStatusMibEntry[]= {

{{9,ProbeDownloadStatus}, NULL, ProbeDownloadStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 1, 0, NULL},
};
tMibData ProbeDownloadStatusEntry = { 1, ProbeDownloadStatusMibEntry };

tMbDbEntry SerialConfigTableMibEntry[]= {

{{11,SerialMode}, GetNextIndexSerialConfigTable, SerialModeGet, SerialModeSet, SerialModeTest, SerialConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SerialConfigTableINDEX, 1, 1, 0, "1"},

{{11,SerialProtocol}, GetNextIndexSerialConfigTable, SerialProtocolGet, SerialProtocolSet, SerialProtocolTest, SerialConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SerialConfigTableINDEX, 1, 1, 0, "2"},

{{11,SerialTimeout}, GetNextIndexSerialConfigTable, SerialTimeoutGet, SerialTimeoutSet, SerialTimeoutTest, SerialConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, SerialConfigTableINDEX, 1, 1, 0, "300"},

{{11,SerialModemInitString}, GetNextIndexSerialConfigTable, SerialModemInitStringGet, SerialModemInitStringSet, SerialModemInitStringTest, SerialConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SerialConfigTableINDEX, 1, 1, 0, NULL},

{{11,SerialModemHangUpString}, GetNextIndexSerialConfigTable, SerialModemHangUpStringGet, SerialModemHangUpStringSet, SerialModemHangUpStringTest, SerialConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SerialConfigTableINDEX, 1, 1, 0, NULL},

{{11,SerialModemConnectResp}, GetNextIndexSerialConfigTable, SerialModemConnectRespGet, SerialModemConnectRespSet, SerialModemConnectRespTest, SerialConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SerialConfigTableINDEX, 1, 1, 0, NULL},

{{11,SerialModemNoConnectResp}, GetNextIndexSerialConfigTable, SerialModemNoConnectRespGet, SerialModemNoConnectRespSet, SerialModemNoConnectRespTest, SerialConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SerialConfigTableINDEX, 1, 1, 0, NULL},

{{11,SerialDialoutTimeout}, GetNextIndexSerialConfigTable, SerialDialoutTimeoutGet, SerialDialoutTimeoutSet, SerialDialoutTimeoutTest, SerialConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, SerialConfigTableINDEX, 1, 1, 0, "20"},

{{11,SerialStatus}, GetNextIndexSerialConfigTable, SerialStatusGet, SerialStatusSet, SerialStatusTest, SerialConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SerialConfigTableINDEX, 1, 1, 1, NULL},
};
tMibData SerialConfigTableEntry = { 9, SerialConfigTableMibEntry };

tMbDbEntry NetConfigTableMibEntry[]= {

{{11,NetConfigIPAddress}, GetNextIndexNetConfigTable, NetConfigIPAddressGet, NetConfigIPAddressSet, NetConfigIPAddressTest, NetConfigTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NetConfigTableINDEX, 1, 1, 0, NULL},

{{11,NetConfigSubnetMask}, GetNextIndexNetConfigTable, NetConfigSubnetMaskGet, NetConfigSubnetMaskSet, NetConfigSubnetMaskTest, NetConfigTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NetConfigTableINDEX, 1, 1, 0, NULL},

{{11,NetConfigStatus}, GetNextIndexNetConfigTable, NetConfigStatusGet, NetConfigStatusSet, NetConfigStatusTest, NetConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NetConfigTableINDEX, 1, 1, 1, NULL},
};
tMibData NetConfigTableEntry = { 3, NetConfigTableMibEntry };

tMbDbEntry NetDefaultGatewayMibEntry[]= {

{{9,NetDefaultGateway}, NULL, NetDefaultGatewayGet, NetDefaultGatewaySet, NetDefaultGatewayTest, NetDefaultGatewayDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 1, 0, NULL},
};
tMibData NetDefaultGatewayEntry = { 1, NetDefaultGatewayMibEntry };

tMbDbEntry TrapDestTableMibEntry[]= {

{{11,TrapDestIndex}, GetNextIndexTrapDestTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, TrapDestTableINDEX, 1, 1, 0, NULL},

{{11,TrapDestCommunity}, GetNextIndexTrapDestTable, TrapDestCommunityGet, TrapDestCommunitySet, TrapDestCommunityTest, TrapDestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, TrapDestTableINDEX, 1, 1, 0, NULL},

{{11,TrapDestProtocol}, GetNextIndexTrapDestTable, TrapDestProtocolGet, TrapDestProtocolSet, TrapDestProtocolTest, TrapDestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, TrapDestTableINDEX, 1, 1, 0, NULL},

{{11,TrapDestAddress}, GetNextIndexTrapDestTable, TrapDestAddressGet, TrapDestAddressSet, TrapDestAddressTest, TrapDestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, TrapDestTableINDEX, 1, 1, 0, NULL},

{{11,TrapDestOwner}, GetNextIndexTrapDestTable, TrapDestOwnerGet, TrapDestOwnerSet, TrapDestOwnerTest, TrapDestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, TrapDestTableINDEX, 1, 1, 0, NULL},

{{11,TrapDestStatus}, GetNextIndexTrapDestTable, TrapDestStatusGet, TrapDestStatusSet, TrapDestStatusTest, TrapDestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, TrapDestTableINDEX, 1, 1, 1, NULL},
};
tMibData TrapDestTableEntry = { 6, TrapDestTableMibEntry };

tMbDbEntry SerialConnectionTableMibEntry[]= {

{{11,SerialConnectIndex}, GetNextIndexSerialConnectionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, SerialConnectionTableINDEX, 1, 1, 0, NULL},

{{11,SerialConnectDestIpAddress}, GetNextIndexSerialConnectionTable, SerialConnectDestIpAddressGet, SerialConnectDestIpAddressSet, SerialConnectDestIpAddressTest, SerialConnectionTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, SerialConnectionTableINDEX, 1, 1, 0, NULL},

{{11,SerialConnectType}, GetNextIndexSerialConnectionTable, SerialConnectTypeGet, SerialConnectTypeSet, SerialConnectTypeTest, SerialConnectionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SerialConnectionTableINDEX, 1, 1, 0, "1"},

{{11,SerialConnectDialString}, GetNextIndexSerialConnectionTable, SerialConnectDialStringGet, SerialConnectDialStringSet, SerialConnectDialStringTest, SerialConnectionTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SerialConnectionTableINDEX, 1, 1, 0, NULL},

{{11,SerialConnectSwitchConnectSeq}, GetNextIndexSerialConnectionTable, SerialConnectSwitchConnectSeqGet, SerialConnectSwitchConnectSeqSet, SerialConnectSwitchConnectSeqTest, SerialConnectionTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SerialConnectionTableINDEX, 1, 1, 0, NULL},

{{11,SerialConnectSwitchDisconnectSeq}, GetNextIndexSerialConnectionTable, SerialConnectSwitchDisconnectSeqGet, SerialConnectSwitchDisconnectSeqSet, SerialConnectSwitchDisconnectSeqTest, SerialConnectionTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SerialConnectionTableINDEX, 1, 1, 0, NULL},

{{11,SerialConnectSwitchResetSeq}, GetNextIndexSerialConnectionTable, SerialConnectSwitchResetSeqGet, SerialConnectSwitchResetSeqSet, SerialConnectSwitchResetSeqTest, SerialConnectionTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SerialConnectionTableINDEX, 1, 1, 0, NULL},

{{11,SerialConnectOwner}, GetNextIndexSerialConnectionTable, SerialConnectOwnerGet, SerialConnectOwnerSet, SerialConnectOwnerTest, SerialConnectionTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SerialConnectionTableINDEX, 1, 1, 0, NULL},

{{11,SerialConnectStatus}, GetNextIndexSerialConnectionTable, SerialConnectStatusGet, SerialConnectStatusSet, SerialConnectStatusTest, SerialConnectionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SerialConnectionTableINDEX, 1, 1, 1, NULL},
};
tMibData SerialConnectionTableEntry = { 9, SerialConnectionTableMibEntry };

#endif /* _STDRMODB_H */

