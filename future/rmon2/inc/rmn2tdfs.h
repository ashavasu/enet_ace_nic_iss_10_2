/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 * 
 * Description: This file contains all RMON2 data structure
 *              Definitions.
  * $Id: rmn2tdfs.h,v 1.12 2012/04/10 12:35:47 siva Exp $
 *******************************************************************/

#ifndef _RMN2TDFS_H_
#define _RMN2TDFS_H_

typedef tRmn2IpAddr tIpAddr;

typedef struct RMONv2GLOBALS {
    struct Rmon2PktInfo *pRmon2LastVisted;    /* Last Visited PktInfo Tree Entry */
    tRBTree    ProtocolDirRBTree;              /* RB Tree for Protocol Dir */
    tRBTree    ProtocolDistControlRBTree;      /* RB Tree for Protocol Dist Control */
    tRBTree    ProtocolDistStatsRBTree;        /* RB Tree for Protocol Dist Stats */
    tRBTree    AddressMapControlRBTree;        /* RB Tree for Address Map Control */
    tRBTree    AddressMapRBTree;               /* RB Tree for Address Map */
    tRBTree    HlHostControlRBTree;            /* RB Tree for Hl Host Control */
    tRBTree    NlHostRBTree;                   /* RB Tree for Nl Host */
    tRBTree    AlHostRBTree;                   /* RB Tree for Al Host */
    tRBTree    HlMatrixControlRBTree;          /* RB Tree for Hl Matrix Control */
    tRBTree    NlMatrixRBTrees [RMON2_MAX_NLMATRIX_TABLES];
                                       /* RB Tree for Nl Matrix SD and Nl Matrix DS */
    tRBTree    AlMatrixRBTrees [RMON2_MAX_ALMATRIX_TABLES];
                                       /* RB Tree for Al Matrix SD and Al Matrix DS */
    tRBTree    NlMatrixTopNControlRBTree;      /* RB Tree for Nl Matrix TopN Control */
    tRBTree    NlMatrixTopNRBTree;             /* RB Tree for Nl Matrix TopN */
    tRBTree    AlMatrixTopNControlRBTree;      /* RB Tree for Al Matrix TopN Control */
    tRBTree    AlMatrixTopNRBTree;      /* RB Tree for Al Matrix TopN */
    tRBTree    UsrHistoryControlRBTree;        /* RB Tree for Usr History Control */
    tRBTree    Rmon2PktInfoRBTree;               /* RB Tree for PktInfo */
    tMemPoolId ProtocolDirPoolId;              /* Pool for Protocol Dir */ 
    tMemPoolId ProtocolDistControlPoolId;      /* Pool for Protocol Dist Control */
    tMemPoolId ProtocolDistStatsPoolId;        /* Pool for Protocol Dist Stats */
    tMemPoolId AddressMapControlPoolId;        /* Pool for Address Map Control */
    tMemPoolId AddressMapPoolId;               /* Pool for Address Map */
    tMemPoolId HlHostControlPoolId;            /* Pool for Hl Host Control */
    tMemPoolId NlHostPoolId;                   /* Pool for Nl Host */
    tMemPoolId AlHostPoolId;                   /* Pool for Al Host */
    tMemPoolId HlMatrixControlPoolId;          /* Pool for Hl Matrix Control */
    tMemPoolId NlMatrixSDPoolId;               /* Pool for Nl Matrix SD */
    tMemPoolId NlMatrixDSPoolId;               /* Pool for Nl Matrix DS */
    tMemPoolId AlMatrixSDPoolId;               /* Pool for Al Matrix SD */
    tMemPoolId AlMatrixDSPoolId;               /* Pool for Al Matrix DS */
    tMemPoolId NlMatrixTopNControlPoolId;      /* Pool for Nl Matrix TopN Control */
    tMemPoolId NlMatrixTopNPoolId;             /* Pool for Nl Matrix TopN */
    tMemPoolId AlMatrixTopNControlPoolId;      /* Pool for Al Matrix TopN Control */
    tMemPoolId AlMatrixTopNPoolId;             /* Pool for Al Matrix TopN */
    tMemPoolId UsrHistoryControlPoolId;        /* Pool for Usr History Control */    
    tMemPoolId UsrHistoryObjectPoolId;         /* Pool for Usr History Object */
    tMemPoolId Rmon2PktInfoPoolId;             /* Pool for PktInfo */
    tMemPoolId Rmon2QMsgPoolId;   /* Pool for Msg Q */
    tMemPoolId UsrHistoryOidListPoolId;        /* Pool for Usr History OID List */
    tMemPoolId UsrHistoryCtrlBucketsPoolId;    /* Pool for Usr History Ctrl Buckets */
    tMemPoolId AlMatrixSampleTopNPoolId;       /* Pool for Al Matrix Sample Top N */
    tMemPoolId NlMatrixSampleTopNPoolId;       /* Pool for Nl Matrix Sample Top N */ 
    tOsixSemId Rmon2SemId;                     /* Rmon2 Semaphore */
    tOsixTaskId Rmon2TskId;         /* Rmon2 Task Id */
    tOsixQId Rmon2QId;                         /* Rmon2 Queue Id */
    tTmrDesc aRmon2TmrDesc[RMON2_MAX_TMRS];   /* Timer data struct that contains
                                               * func ptrs for timer handling and
                                               * offsets to identify the data
                                               * struct containing timer block.
                                               * Timer ID is the index to this
                                               * data structure */
    tTimerListId Rmon2TmrLst;                  /* Rmon2 Timer List Id */
    tRmon2Tmr Rmon2PollTmr;                    /* Rmon2 Poll timer node */
    tSNMP_OID_TYPE InterfaceOid;               /* OID of interface index */
    UINT4 u4Rmon2AdminStatus;                  /* Rmon2 Admin Status */
    UINT4 u4Rmon2Trace;                        /* Rmon2 Trace ststus */
    UINT4 u4ProtDirLocIndex;                   /* Protocol Dir Local Index */
    UINT4 u4ProtocolDirLastChange;             /* Sys Up Time when the ProtocolDir
      * entry was last modified */
    UINT4 u4AddressMapInserts;                 /* Number of times address mapping entry is 
             * inserted to AddressMap table */
    UINT4 u4AddressMapDeletes;                 /* Number of times address mapping entry is 
             * deleted from AddressMap table */
    INT4 i4AddressMapMaxDesiredEntries;        /* Number of entries desired in 
             * AddressMap table */    
    UINT4 u4AddressMapMaxDesiredSupported;     /* Number of entries desired supported in 
             * AddressMap table */    
    UINT4    u4ProbeCapabilities;              /* RMON groups supported by the Probe */
    UINT4    u4ProbeResetControl;              /* Probe Reset Control */
    UINT1    au1ProbeSoftwareRev[RMON2_PROBE_SW_REV_LEN+1];
                                               /* Software Revision of the Probe */
    UINT1    au1ProbeHardwareRev[RMON2_PROBE_HW_REV_LEN+1];
                                               /* Hardware Revision of the Probe */
    tSNMP_OCTET_STRING_TYPE ProbeDateTime;
    tSnmpIndex Rmon2IndexPool[SNMP_MAX_INDICES];
    tSNMP_MULTI_DATA_TYPE Rmon2MultiPool[SNMP_MAX_INDICES];
    tSNMP_OCTET_STRING_TYPE Rmon2OctetPool[SNMP_MAX_INDICES];
    tSNMP_OID_TYPE  Rmon2OIDPool[SNMP_MAX_INDICES];
    UINT1 au1Rmon2Data[SNMP_MAX_INDICES][1024];
                                               /* Probe current Date and Time */
}tRmon2Globals;

/*---------------------- Protocol Directory Group -----------------------------*/

typedef struct ProtocolDirEntry {
    tRBNodeEmbd    ProtocolDirRBNode;             /* RB Tree */ 
    UINT4          u4ProtocolDirAddressMapConfig; /* Protocol Dir
         * Address Map Config Support */
    UINT4          u4ProtocolDirHostConfig;       /* Protocol Dir 
         * Host Config Support */
    UINT4          u4ProtocolDirMatrixConfig;     /* Protocol Dir
         * Matrix Config Support */    
    UINT4          u4ProtocolDirLocalIndex;       /* Protocol Dir Local Index*/    
    UINT4          u4ProtocolDirStatus;           /* Status of the row */
    UINT1          au1ProtocolDirID[RMON2_MAX_PROT_DIR_ID_LEN+4]; 
                                                  /* INDEX1: Protocol Dir ID*/
    UINT4          u4ProtocolDirIDLen;            /* Protocol Dir ID Length */
    UINT1          au1ProtocolDirParameters[RMON2_MAX_PROT_DIR_PARAM_LEN+4];
                                                  /* INDEX2: Protocol Dir Parameters */
    UINT4          u4ProtocolDirParametersLen;    /* Protocol Dir Parameters Length */
    UINT1          au1ProtocolDirDescr[RMON2_PROT_DIR_DESC_LEN+4];   
                                                  /* Protocol Dir Description */
    UINT1          au1ProtocolDirOwner[RMON2_OWNER_LEN+1]; 
                                                  /* Owner of the Protocol Dir entry */
    UINT1          u1ProtocolDirType;             /* Protocol Dir Type */    
    UINT1          au1Padding[3];
}tProtocolDir;

/*---------------------- Protocol Distribution Group -----------------------------*/

typedef struct ProtocolDistControlEntry {
    tRBNodeEmbd    ProtocolDistControlRBNode;     /* RB Tree */ 
    UINT4    u4ProtocolDistControlIndex;          /* INDEX1: Protocol Dist Control Index */
    UINT4    u4ProtocolDistControlDataSource;     /* Interface index */
    UINT4    u4ProtocolDistControlDroppedFrames;  /* Number of dropped frames */
    UINT4    u4ProtocolDistControlCreateTime;     /* Sys up time when this entry was last 
                                                   * activated */     
    UINT4    u4ProtocolDistControlStatus;         /* Status of the row */
    UINT1    au1ProtocolDistControlOwner[RMON2_OWNER_LEN+1];   
                                                  /* Owner of the Protocol Dist entry */
}tProtocolDistControl;

typedef struct ProtocolDistStatsEntry {
    tProtocolDistControl *pPDistCtrl;             /* Pointer to Protocol Dist Control */
    struct ProtocolDistStatsEntry *pPrevPDist;    /* Reference to Previous PDist entry */
    struct ProtocolDistStatsEntry *pNextPDist;    /* Reference to Next PDist entry */
    tRBNodeEmbd    ProtocolDistStatsRBNode;       /* RB Tree */ 
    UINT4          u4ProtocolDistControlIndex;    /* INDEX1: Protocol Dist Control Index */
    UINT4          u4ProtocolDirLocalIndex;       /* INDEX2: Protocol directory Local Index */ 
    UINT4          u4ProtocolDistStatsPkts;       /* Protocol specific packet counter */
    UINT4          u4ProtocolDistStatsOctets;     /* Protocol specific octet counter */
    UINT4          u4PktRefCount;                 /* Packet Reference count */
}tProtocolDistStats;

/*------------------------- Address Map Group -------------------------*/

typedef struct AddressMapControlEntry {
    tRBNodeEmbd    AddressMapControlRBNode;       /* RB Tree */  
    UINT4    u4AddressMapControlIndex;            /* INDEX1: Address Map Control Index */
    UINT4    u4AddressMapControlDataSource;       /* Interface index */        
    UINT4    u4AddressMapControlDroppedFrames;    /* Number of dropped frames */       
    UINT4    u4AddressMapControlStatus;           /* Status of the row */  
    UINT1    au1AddressMapControlOwner[RMON2_OWNER_LEN+1]; 
                                                  /* Owner of the Address Map entry */
}tAddressMapControl;

typedef struct AddressMapEntry {
    struct AddressMapEntry *pPrevAddrMap;         /* Reference to Previous Addr Map entry */
    struct AddressMapEntry *pNextAddrMap;         /* Reference to Next Addr Map entry */
    tRBNodeEmbd    AddressMapRBNode;              /* RB Tree */ 
    tIpAddr        AddressMapNetworkAddress;      /* INDEX3: Network Address of this entry */
    UINT4          u4AddressMapTimeMark;          /* INDEX1: Time filter entry */
    UINT4          u4ProtocolDirLocalIndex;       /* INDEX2: Protocol directory Local Index */    
    UINT4          u4AddressMapSource;            /* INDEX4: Interface or Port on which this address mapping *
                                                   * was discovered */
    UINT4          u4AddressMapLastChange;        /* Sys Up Time when this entry was last created
                                                   * or the value of the physical address changed */
    UINT4          u4Rmon2AddressMapIpAddrLen;    /* IP Address Length */
    UINT4          u4PktRefCount;                 /* Packet Reference count */
    tMacAddr       AddressMapPhysicalAddress;     /* Physical Address of this entry */
    UINT2          u2Rmon2Rsvd;                   /* Padding */
}tAddressMap;

/*---------------------- Network Layer Host Group -----------------------------*/

typedef struct HlHostControlEntry {
    tRBNodeEmbd    HlHostControlRBNode;           /* RB Tree */ 
    UINT4    u4HlHostControlIndex;                /* INDEX1: Host Control Index */
    UINT4    u4HlHostControlDataSource;           /* Interface index */
    UINT4    u4HlHostControlNlDroppedFrames;      /* Number of dropped frames */
    UINT4    u4HlHostControlNlInserts;            /* Number of times NlHost entry is
                                                   * inserted to NlHost table */
    UINT4    u4HlHostControlNlDeletes;            /* Number of times NlHost entry is 
                                                   * deleted from NlHost table */ 
    INT4     i4HlHostControlNlMaxDesiredEntries;  /* Number of entries desired in 
                                                   * NlHost entry corresponding to 
                                                   * this control entry */ 
    UINT4    u4HlHostControlNlMaxDesiredSupported;/* Number of entries desired supported in 
                                                   * NlHost entry corresponding to 
                                                   * this control entry */ 
    UINT4    u4HlHostControlAlDroppedFrames;      /* Number of dropped frames */
    UINT4    u4HlHostControlAlInserts;            /* Number of times AlHost entry is
                                                   * inserted to AlHost table */
    UINT4    u4HlHostControlAlDeletes;            /* Number of times AlHost entry is 
                                                   * deleted from AlHost table */ 
    INT4     i4HlHostControlAlMaxDesiredEntries;  /* Number of entries desired in 
                                                   * AlHost entry corresponding to 
                                                   * this control entry */               
    UINT4    u4HlHostControlAlMaxDesiredSupported;/* Number of entries desired supported in 
                                                   * AlHost entry corresponding to 
                                                   * this control entry */               
    UINT4    u4HlHostControlStatus;               /* Status of the row */
    UINT1    au1HlHostControlOwner[RMON2_OWNER_LEN+1];   
                                                  /* Owner of the Host Control entry */  
}tHlHostControl;

typedef struct NlHostEntry {
    tHlHostControl *pHlHostCtrl;                  /* Pointer to Hl Host Control */
    struct NlHostEntry *pPrevNlHost;              /* Reference to Previous NlHost entry */
    struct NlHostEntry *pNextNlHost;              /* Reference to Next NlHost entry */
    tRBNodeEmbd    NlHostRBNode;                  /* RB Tree */ 
    tIpAddr        NlHostAddress;                 /* INDEX4: Network address of this entry */
    UINT4          u4HlHostControlIndex;          /* INDEX1: Host Control Index */
    UINT4          u4NlHostTimeMark;              /* INDEX2: Time filter entry */     
    UINT4          u4ProtocolDirLocalIndex;       /* INDEX3: Protocol directory Local Index */ 
    UINT4          u4NlHostInPkts;                /* Number of incoming packets per host */
    UINT4          u4NlHostOutPkts;               /* Number of outgoing packets per host */
    UINT4          u4NlHostInOctets;              /* Number of incoming octets per host */
    UINT4          u4NlHostOutOctets;             /* Number of outgoing octets per host */
    UINT4          u4NlHostOutMacNonUnicastPkts;  /* Number of outgoing packets to any mac
         * broadcast/multicast addresses per host */
    UINT4          u4NlHostCreateTime;            /* Sys up time when this entry was last 
                                                   * activated */ 
    UINT4          u4Rmon2NlHostIpAddrLen;        /* IP Address Length */
    UINT4          u4PktRefCount;                 /* Packet Reference count */
}tNlHost;

/*---------------------- Application Layer Host Group -----------------------------*/

typedef struct AlHostEntry {
    tHlHostControl *pHlHostCtrl;                  /* Pointer to Hl Host Control */
    struct AlHostEntry *pPrevAlHost;              /* Reference to Previous AlHost entry */
    struct AlHostEntry *pNextAlHost;              /* Reference to Next AlHost entry */
    tRBNodeEmbd    AlHostRBNode;                  /* RB Tree */
    tIpAddr        NlHostAddress;                 /* INDEX4: Network Layer Address */
    UINT4          u4HlHostControlIndex;          /* INDEX1: Host Control Index */
    UINT4          u4AlHostTimeMark;              /* INDEX2: Time filter entry */
    UINT4          u4NlProtocolDirLocalIndex;     /* INDEX3: Network Layer Protocol directory Local Index */   
    UINT4          u4AlProtocolDirLocalIndex;     /* INDEX5: Application Layer Protocol directory Local Index */
    UINT4          u4AlHostInPkts;                /* Number of incoming packets for this protocol type */
    UINT4          u4AlHostOutPkts;               /* Number of outgoing packets for this protocol type */
    UINT4          u4AlHostInOctets;              /* Number of incoming octets for this protocol type */             
    UINT4          u4AlHostOutOctets;             /* Number of outgoing octets for this protocol type */
    UINT4          u4AlHostCreateTime;            /* Sys up time when this entry was last
                                                   * activated */
    UINT4          u4Rmon2NlHostIpAddrLen;        /* IP Address Length */
    UINT4          u4PktRefCount;                 /* Packet Reference count */
}tAlHost;

/*---------------------- Network Layer Matrix Group -----------------------------*/

typedef struct HlMatrixControlEntry {
    tRBNodeEmbd    HlMatrixControlRBNode;         /* RB Tree */ 
    UINT4    u4HlMatrixControlIndex;              /* INDEX1: Hl Matrix Control Index */
    UINT4    u4HlMatrixControlDataSource;         /* Interface index */
    UINT4    u4HlMatrixControlNlDroppedFrames;    /* Number of dropped frames */
    UINT4    u4HlMatrixControlNlInserts;          /* Number of times NlMatrix entry is
                                                   * inserted to NlMatrix table */
    UINT4    u4HlMatrixControlNlDeletes;          /* Number of times NlMatrix entry is 
                                                   * deleted from NlMatrix table */ 
    INT4     i4HlMatrixControlNlMaxDesiredEntries;/* Number of entries desired in 
                                                   * NlMatrix entry corresponding to 
                                                   * this control entry */ 
    UINT4    u4HlMatrixControlNlMaxDesiredSupported;/* Number of entries desired supported in 
                                                   * NlMatrix entry corresponding to 
                                                   * this control entry */ 
    UINT4    u4HlMatrixControlAlDroppedFrames;    /* Number of dropped frames */
    UINT4    u4HlMatrixControlAlInserts;          /* Number of times AlMatrix entry is
                                                   * inserted to AlMatrix table */
    UINT4    u4HlMatrixControlAlDeletes;          /* Number of times AlMatrix entry is 
                                                   * deleted from AlMatrix table */ 
    INT4     i4HlMatrixControlAlMaxDesiredEntries;/* Number of entries desired in 
                                                   * AlMatrix entry corresponding to 
                                                   * this control entry */                 
    UINT4    u4HlMatrixControlAlMaxDesiredSupported;/* Number of entries desired supported in 
                                                   * AlMatrix entry corresponding to 
                                                   * this control entry */                 
    UINT4    u4HlMatrixControlStatus;             /* Status of the row */
    UINT1    au1HlMatrixControlOwner[RMON2_OWNER_LEN+1];  
                                                  /* Owner of the Matrix Control entry */ 
}tHlMatrixControl;

typedef struct NlMatrixSDSample {
    UINT4    u4NlMatrixSDPkts;                    /* Number of Nl Matrix SD packets */
    UINT4    u4NlMatrixSDOctets;                  /* Number of Nl Matrix SD octets */
}tNlMatrixSDSample;    

typedef struct NlMatrixSDEntry {  
    tHlMatrixControl     *pHlMatrixCtrl;          /* Pointer to Hl Matrix Control */
    struct NlMatrixSDEntry *pPrevNlMatrixSD;      /* Reference to Previous NlMatrixSD entry */
    struct NlMatrixSDEntry *pNextNlMatrixSD;      /* Reference to Next NlMatrixSD entry */
    tRBNodeEmbd          NlMatrixSDRBNode;        /* RB Tree for Nl Matrix SD */ 
    tRBNodeEmbd          NlMatrixDSRBNode;        /* RB Tree for Nl Matrix DS */
    tNlMatrixSDSample    NlMatrixSDCurSample;     /* Nl Matrix SD Current Sample */
    tNlMatrixSDSample    NlMatrixSDPrevSample;    /* Nl Matrix SD Previous Sample */
    tIpAddr              NlMatrixSDSourceAddress; /* INDEX4: Network Source Address */
    tIpAddr              NlMatrixSDDestAddress;   /* INDEX5: Network Destination Address */
    UINT4                u4HlMatrixControlIndex;  /* INDEX1: Hl Matrix Control Index */
    UINT4                u4NlMatrixSDTimeMark;    /* INDEX2: Time filter entry */   
    UINT4                u4ProtocolDirLocalIndex; /* INDEX3: Protocol directory Local Index */ 
    UINT4                u4NlMatrixSDCreateTime;  /* Sys up time when this entry was last
                                                   * activated */
    UINT4                u4Rmon2NlMatrixSDIpAddrLen; 
                                                  /* IP Address Length */
    UINT4                u4PktRefCount;           /* Reference Count */
}tNlMatrixSD;

/*---------------------- Application Layer Matrix Group -----------------------------*/

typedef struct AlMatrixSDSample {
    UINT4    u4AlMatrixSDPkts;                    /* Number of Al Matrix SD packets */
    UINT4    u4AlMatrixSDOctets;                  /* Number of Al Matrix SD octets */
}tAlMatrixSDSample;    

typedef struct AlMatrixSDEntry {
    tHlMatrixControl     *pHlMatrixCtrl;         /* Pointer to Hl Matrix Control */
    struct AlMatrixSDEntry *pPrevAlMatrixSD;      /* Reference to Previous AlMatrixSD entry */
    struct AlMatrixSDEntry *pNextAlMatrixSD;      /* Reference to Next AlMatrixSD entry */
    tRBNodeEmbd          AlMatrixSDRBNode;          /* RB Tree for Al Matrix SD */ 
    tRBNodeEmbd          AlMatrixDSRBNode;          /* RB Tree for Al Matrix DS */
    tAlMatrixSDSample    AlMatrixSDCurSample;       /* Al Matrix SD Current Sample */
    tAlMatrixSDSample    AlMatrixSDPrevSample;      /* Al Matrix SD Previous Sample */
    tIpAddr              NlMatrixSDSourceAddress;   /* INDEX4: Network Layer Address of the Source Host */
    tIpAddr             NlMatrixSDDestAddress;     /* INDEX5: Network Layer Address of the Destination Host */
    UINT4                u4HlMatrixControlIndex;    /* INDEX1: Hl Matrix Control Index */
    UINT4                u4AlMatrixSDTimeMark;      /* INDEX2: Time filter entry */
    UINT4          u4NlProtocolDirLocalIndex;       /* INDEX3: Network Layer Protocol directory Local Index */   
    UINT4          u4AlProtocolDirLocalIndex;       /* INDEX6: Application Layer Protocol directory Local Index */
    UINT4                u4AlMatrixSDCreateTime;    /* Sys up time when this entry was last activated */
    UINT4                u4Rmon2NlMatrixSDIpAddrLen;
                                                     /* IP Address Length */
    UINT4                u4PktRefCount;              /* Reference Count */
}tAlMatrixSD;

/*---------------------- Network Layer Matrix TopN -----------------------------*/

typedef struct NlMatrixTopNControlEntry {
    tRBNodeEmbd      NlMatrixTopNControlRBNode;   /* RB Tree */
    UINT4      u4NlMatrixTopNControlIndex;  /* INDEX1: NL Matrix TopN Ctrl Index */
    UINT4            u4NlMatrixTopNControlMatrixIndex; 
                                                  /* HL Matrix Control Index */    
    UINT4            u4NlMatrixTopNControlRateBase;
                                                  /* NlMatrixSD entry on which topN Entries
                                                   * sorting as to be done */
    UINT4            u4NlMatrixTopNControlTimeRemaining;   
                                                  /* Time remaining for the sample to
                                                   * be taken */ 
    UINT4            u4NlMatrixTopNControlGeneratedReports; 
                                                  /* Number of reports generated */
    UINT4            u4NlMatrixTopNControlDuration;
                                                  /* Total duration for collecting sample */ 
    UINT4            u4NlMatrixTopNControlRequestedSize;
                                                  /* Number of topN entries requested 
                                                   * by the administrator */ 
    UINT4            u4NlMatrixTopNControlGrantedSize;     
                                                  /* Number of topN entries granted */
    UINT4            u4NlMatrixTopNControlStartTime; 
                                                  /* Sys up time when the collection 
                                                   * was last started */     
    UINT4            u4NlMatrixTopNControlStatus; /* Status of the row */
    UINT1            au1NlMatrixTopNControlOwner[RMON2_OWNER_LEN+1];     
                                                  /* Owner of the control entry */ 
}tNlMatrixTopNControl;

typedef struct NlMatrixTopNEntry {
    tRBNodeEmbd      NlMatrixTopNRBNode;          /* RB Tree */ 
    tIpAddr  NlMatrixTopNSourceAddress;           /* Address of the source host 
                                                   * identified in this entry */
    tIpAddr  NlMatrixTopNDestAddress;             /* Address of the destination host 
                                                   * identified in this entry */
    UINT4    u4NlMatrixTopNIpAddrLen;             /* IP Address Length */
    UINT4    u4NlMatrixTopNControlIndex;          /* INDEX1: NL Matrix TopN Ctrl Index */
    UINT4    u4NlMatrixTopNIndex;                 /* INDEX2: NL Matrix TopN Index whose
                                                   * is between 1 & N */
    UINT4    u4NlMatrixTopNProtocolDirLocalIndex; /* Protocol Dir Local Index of the 
                       * NL protocol */   
    UINT4    u4NlMatrixTopNPktRate;               /* Number of packets from source to 
                                                   * destination in the sampling interval */
    UINT4    u4NlMatrixTopNReversePktRate;        /* Number of packets from destination to 
                                                   * source in the sampling interval */
    UINT4    u4NlMatrixTopNOctetRate;             /* Number of octets from source to 
                                                   * destination in the sampling interval*/
    UINT4    u4NlMatrixTopNReverseOctetRate;      /* Number of octets from destination to 
                                                   * source in the sampling interval */
}tNlMatrixTopN;

/*---------------------- Application Layer Matrix TopN -----------------------------*/

typedef struct AlMatrixTopNControlEntry {
    tRBNodeEmbd      AlMatrixTopNControlRBNode;     /* RB Tree */
    UINT4            u4AlMatrixTopNControlIndex;    /* INDEX1: AL Matrix TopN Ctrl Index */
    UINT4            u4AlMatrixTopNControlMatrixIndex; 
                                                    /* HL Matrix Control Index */ 
    UINT4            u4AlMatrixTopNControlRateBase; /* AlMatrixSD entry on which topN Entries
                                                     * sorting as to be done */
    UINT4            u4AlMatrixTopNControlTimeRemaining; 
                                                    /* Time remaining for the sample to
                                                     * be taken */ 
    UINT4            u4AlMatrixTopNControlGeneratedReports; 
                                                    /* Number of reports generated */
    UINT4            u4AlMatrixTopNControlDuration; /* Total duration for collecting 
                                                     * sample */ 
    UINT4            u4AlMatrixTopNControlRequestedSize;  
                                                    /* Number of topN entries requested 
                                                     * by the administrator */ 
    UINT4            u4AlMatrixTopNControlGrantedSize;
                                                    /* Number of topN entries granted */
    UINT4            u4AlMatrixTopNControlStartTime;/* Sys up time when the collection 
                                                     * was last started */     
    UINT4            u4AlMatrixTopNControlStatus;   /* Status of the row */
    UINT1            au1AlMatrixTopNControlOwner[RMON2_OWNER_LEN+1]; 
                                                    /* Owner of the control entry */ 
}tAlMatrixTopNControl;

typedef struct AlMatrixTopNEntry {
    tRBNodeEmbd      AlMatrixTopNRBNode;            /* RB Tree */
    tIpAddr  AlMatrixTopNSourceAddress;             /* Address of the source host 
                                                     * identified in this entry */ 
    tIpAddr  AlMatrixTopNDestAddress;               /* Address of the destination host 
                                                     * identified in this entry */ 
    UINT4    u4AlMatrixTopNIpAddrLen;               /* IP Address Length */
    UINT4    u4AlMatrixTopNControlIndex;            /* INDEX1: AL Matrix TopN Ctrl Index */
    UINT4    u4AlMatrixTopNIndex;                   /* INDEX2: AL Matrix TopN Index whose
                                                     * is between 1 & N */
    UINT4    u4AlMatrixTopNProtocolDirLocalIndex;   /* Protocol Dir Local Index of the 
           * NL protocol */ 
    UINT4    u4AlMatrixTopNAppProtocolDirLocalIndex;/* Protocol Dir Local Index of the 
           * AL protocol */ 
    UINT4    u4AlMatrixTopNPktRate;                 /* Number of packets from source to 
                                                     * destination in the sampling interval */
    UINT4    u4AlMatrixTopNReversePktRate;          /* Number of packets from destination to 
                                                     * source in the sampling interval */
    UINT4    u4AlMatrixTopNOctetRate;               /* Number of octets from source to 
                                                     * destination in the sampling interval*/
    UINT4    u4AlMatrixTopNReverseOctetRate;        /* Number of octets from destination to 
                                                     * source in the sampling interval */
}tAlMatrixTopN;

/*---------------------- User History Collection Group -----------------------------*/

typedef struct UsrHistoryControlEntry {    
    tTMO_SLL    UsrHistoryObject;                   /* Head node for Usr History Object Entry */
    tRBNodeEmbd UsrHistoryControlRBNode;            /* RB Tree */
    UINT4       u4UsrHistoryControlIndex;           /* INDEX1: Usr History Control Index */          
    UINT4       u4UsrHistoryControlObjects;         /* Number of MIB objects to be collected
                                                     * in Usr History Table */
    UINT4       u4UsrHistoryControlBucketsRequested;/* Requested number of discrete time intervals */
    UINT4       u4UsrHistoryControlBucketsGranted;  /* Actual number of discrete time intervals */
    UINT4       u4UsrHistoryControlInterval;        /* Time Interval in seconds */             
    UINT4       u4UsrHistoryControlStatus;          /* Status of the row */
    UINT1       au1UsrHistoryControlOwner[RMON2_OWNER_LEN+1];
                                                    /* Owner of the Usr History Control entry */  
    UINT4       u4RemainingTime;
}tUsrHistoryControl;

typedef struct UsrHistoryObjectEntry {
    tTMO_SLL_NODE    nextUsrHistoryObject;          /* Link to next node */
    struct UsrHistoryEntry   *pUsrHistory;          /* List of Usr History samples */  
    tSNMP_OID_TYPE   UsrHistoryObjectVariable;      /* Object Identifier of the particular variable */
    UINT4            u4UsrHistoryObjectIndex;       /* INDEX2: Usr History Object Index */
    UINT4            u4UsrHistoryObjectSampleType;  /* Method of sampling the variable */
}tUsrHistoryObject;

typedef struct UsrHistoryEntry { 
    UINT4    u4UsrHistoryControlIndex;              /* INDEX1: Usr History Control Index */
    UINT4    u4UsrHistoryObjectIndex;               /* INDEX3: Usr History Object Index */
    UINT4    u4UsrHistorySampleIndex;               /* INDEX2: Usr History Sample Index */
    UINT4    u4UsrHistoryIntervalStart;             /* Sys Up Time at the start of the interval */
    UINT4    u4UsrHistoryIntervalEnd;               /* Sys Up Time at the end of the interval */
    UINT4    u4UsrHistoryAbsValueCurSample;         /* Absolute value for Current Sample */
    UINT4    u4UsrHistoryAbsValuePrevSample;     /* Absolute value for Delta Sample */
    UINT4    u4UsrHistoryValStatus;                 /* Validity and sign of the data */
}tUsrHistory;

typedef struct Rmon2PktInfo {
    tRBNodeEmbd   Rmon2PktInfoRBNode;           /* RBTree */
    tAlMatrixSD   *pAlMatrixSD;  /* Reference to AlMatrix SD for L5 Protocol */
    tNlMatrixSD   *pNlMatrixSD;  /* Reference to NlMatrix SD */
    tAlHost   *pInAlHost;          /* Reference to AlHost */
    tAlHost   *pOutAlHost;          /* Reference to AlHost */
    tNlHost   *pInNlHost;          /* Reference to NlHost */
    tNlHost   *pOutNlHost;          /* Reference to NlHost */
    tProtocolDistStats *pPDist; /* Reference to PDist Stats */
    tAddressMap *pAddrMap; /* Reference to AddressMap */
    tPktHeader PktHdr;        /* Packet Header Info */
    UINT4 u4IdleRef;            /* Least recently used counter reference */
    UINT4 u4ProtoNLIndex;          /* L3 layer protocol local index */
    UINT4 u4ProtoL4ALIndex;          /* L4 layer protocol local index */
    UINT4 u4ProtoL5ALIndex;     /* L5 layer protocol local index */
    UINT1 u1IsL3Vlan;           /* Is L3 VLAN Ifindex is configured */
    UINT1 u1IsNPFlowAdded; /* Is NP Flow Added */
    UINT1 u1Rsvd[2];
}tRmon2PktInfo;

/* User History Control Mem-Block */
typedef struct Rmon2UsrHistCtrlBlock
{
    tUsrHistory   Rmon2UsrHistCtrlBlock[RMON2_MAX_USR_HISTORY_BUCKETS_GRANT];
}tRmon2UsrHistCtrlBlock;

typedef struct Rmon2AlMatrixTopN
{
    tAlMatrixTopN       SampleTopNList[RMON2_MAX_TOPN_ENTRY];
}tAlMatrixSampleTopNList;

typedef struct Rmon2NlMatrixTopN
{
    tNlMatrixTopN       SampleTopNList[RMON2_MAX_TOPN_ENTRY];
}tNlMatrixSampleTopNList;



#endif
