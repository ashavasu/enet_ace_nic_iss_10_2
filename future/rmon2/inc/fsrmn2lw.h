/********************************************************************
* Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
*
* $Id: fsrmn2lw.h,v 1.4 2011/09/24 06:59:30 siva Exp $
*
* Description: Proto types for Low Level  Routines
********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRmon2Trace ARG_LIST((UINT4 *));

INT1
nmhGetFsRmon2AdminStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRmon2Trace ARG_LIST((UINT4 ));

INT1
nmhSetFsRmon2AdminStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRmon2Trace ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsRmon2AdminStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRmon2Trace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRmon2AdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


