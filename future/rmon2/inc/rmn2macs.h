/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 * 
   $Id: rmn2macs.h,v 1.4 2012/04/10 12:35:47 siva Exp $
 * Description: This file contains all RMON2 Macro definitons.
 *******************************************************************/
#ifndef _RMN2MACS_H
#define _RMN2MACS_H

/* Rmon2 RB Tree Macros */

#define RMON2_PKTINFO_TREE        gRmon2Globals.Rmon2PktInfoRBTree
#define RMON2_PROTDIR_TREE           gRmon2Globals.ProtocolDirRBTree
#define RMON2_PDISTCTRL_TREE         gRmon2Globals.ProtocolDistControlRBTree
#define RMON2_PDISTSTATS_TREE        gRmon2Globals.ProtocolDistStatsRBTree
#define RMON2_ADDRMAPCTRL_TREE       gRmon2Globals.AddressMapControlRBTree
#define RMON2_ADDRMAP_TREE           gRmon2Globals.AddressMapRBTree
#define RMON2_HLHOSTCTRL_TREE        gRmon2Globals.HlHostControlRBTree
#define RMON2_NLHOST_TREE            gRmon2Globals.NlHostRBTree
#define RMON2_ALHOST_TREE            gRmon2Globals.AlHostRBTree
#define RMON2_HLMATRIXCTRL_TREE      gRmon2Globals.HlMatrixControlRBTree
#define RMON2_NLMATRIX_TREE          gRmon2Globals.NlMatrixRBTrees
#define RMON2_ALMATRIX_TREE          gRmon2Globals.AlMatrixRBTrees
#define RMON2_NLMATRIXTOPNCTRL_TREE  gRmon2Globals.NlMatrixTopNControlRBTree
#define RMON2_NLMATRIXTOPN_TREE      gRmon2Globals.NlMatrixTopNRBTree
#define RMON2_ALMATRIXTOPNCTRL_TREE  gRmon2Globals.AlMatrixTopNControlRBTree
#define RMON2_ALMATRIXTOPN_TREE      gRmon2Globals.AlMatrixTopNRBTree
#define RMON2_USRHISTORYCTRL_TREE    gRmon2Globals.UsrHistoryControlRBTree

/* Rmon2 Mempool Macros */

#define RMON2_PKTINFO_POOL        gRmon2Globals.Rmon2PktInfoPoolId
#define RMON2_QMSG_POOL   gRmon2Globals.Rmon2QMsgPoolId
#define RMON2_PROTDIR_POOL       gRmon2Globals.ProtocolDirPoolId
#define RMON2_PDISTCTRL_POOL     gRmon2Globals.ProtocolDistControlPoolId
#define RMON2_PDISTSTATS_POOL     gRmon2Globals.ProtocolDistStatsPoolId
#define RMON2_ADDRMAPCTRL_POOL        gRmon2Globals.AddressMapControlPoolId
#define RMON2_ADDRMAP_POOL     gRmon2Globals.AddressMapPoolId
#define RMON2_HLHOSTCTRL_POOL        gRmon2Globals.HlHostControlPoolId
#define RMON2_NLHOST_POOL    gRmon2Globals.NlHostPoolId
#define RMON2_ALHOST_POOL        gRmon2Globals.AlHostPoolId
#define RMON2_HLMATRIXCTRL_POOL       gRmon2Globals.HlMatrixControlPoolId
#define RMON2_NLMATRIXSD_POOL        gRmon2Globals.NlMatrixSDPoolId
#define RMON2_ALMATRIXSD_POOL        gRmon2Globals.AlMatrixSDPoolId
#define RMON2_NLMATRIX_TOPNCTRL_POOL         gRmon2Globals.NlMatrixTopNControlPoolId
#define RMON2_NLMATRIX_TOPN_POOL     gRmon2Globals.NlMatrixTopNPoolId
#define RMON2_ALMATRIX_TOPNCTRL_POOL     gRmon2Globals.AlMatrixTopNControlPoolId
#define RMON2_ALMATRIX_TOPN_POOL   gRmon2Globals.AlMatrixTopNPoolId
#define RMON2_USRHISTCTRL_POOL      gRmon2Globals.UsrHistoryControlPoolId
#define RMON2_USRHISTOBJ_POOL      gRmon2Globals.UsrHistoryObjectPoolId
#define RMON2_HISOIDLIST_POOL      gRmon2Globals.UsrHistoryOidListPoolId
#define RMON2_HISCTRLBUCKETS_POOL   gRmon2Globals.UsrHistoryCtrlBucketsPoolId
#define RMON2_ALMATRIX_SAMPLE_TOPN_POOL gRmon2Globals.AlMatrixSampleTopNPoolId
#define RMON2_NLMATRIX_SAMPLE_TOPN_POOL gRmon2Globals.NlMatrixSampleTopNPoolId

#endif
