/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 * $Id: rmn2trce.h,v 1.3 2015/07/27 07:00:41 siva Exp $
 * Description: This file contains RMON2 Trace Macros.
 *******************************************************************/
#ifndef __RMN2TRCE_H_
#define __RMN2TRCE_H_

#ifdef TRACE_WANTED

#define RMON2_TRC_FLAG gRmon2Globals.u4Rmon2Trace
#define RMON2_NAME      "RMON2"
#define RMON2_TRC(Value, Fmt)           UtlTrcLog(RMON2_TRC_FLAG, \
                                                Value,        \
                                                RMON2_NAME,        \
                                                Fmt)

#define RMON2_TRC1(Value, Fmt, Arg)     UtlTrcLog(RMON2_TRC_FLAG, \
                                                Value,        \
                                                RMON2_NAME,        \
                                                Fmt,          \
                                                Arg)

#define RMON2_TRC2(Value, Fmt, Arg1, Arg2)                      \
                                      UtlTrcLog(RMON2_TRC_FLAG, \
                                                Value,        \
                                                RMON2_NAME,        \
                                                Fmt,          \
                                                Arg1, Arg2)
#define RMON2_TRC3(Value, Fmt, Arg1, Arg2, Arg3)                \
                                      UtlTrcLog(RMON2_TRC_FLAG, \
                                                Value,        \
                                                RMON2_NAME,        \
                                                Fmt,          \
                                                Arg1, Arg2, Arg3)

#define RMON2_TRC4(Value, Fmt, Arg1, Arg2, Arg3, Arg4)          \
                                      UtlTrcLog(RMON2_TRC_FLAG, \
                                                Value,        \
                                                RMON2_NAME,        \
                                                Fmt,          \
                                                Arg1, Arg2,   \
                                                Arg3, Arg4)

#define RMON2_TRC5(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)    \
                                      UtlTrcLog(RMON2_TRC_FLAG, \
                                                Value,        \
                                                RMON2_NAME,        \
                                                Fmt,          \
                                                Arg1, Arg2, Arg3, \
                                                Arg4, Arg5)

#define RMON2_TRC6(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
                                      UtlTrcLog(RMON2_TRC_FLAG, \
                                                Value,        \
                                                RMON2_NAME,        \
                                                Fmt,          \
                                                Arg1, Arg2, Arg3, \
                                                Arg4, Arg5, Arg6)

#define RMON2_TRC7(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)  \
                                      UtlTrcLog(RMON2_TRC_FLAG, \
                                                Value,        \
                                                RMON2_NAME,        \
                                                Fmt,          \
                                                Arg1, Arg2, Arg3, \
                                                Arg4, Arg5, Arg6, Arg7)
#else /* TRACE_WANTED */

#define  RMON2_TRC_FLAG
#define  RMON2_NAME
#define  RMON2_TRC(Value, Fmt)
#define  RMON2_TRC1(Value, Fmt, Arg)
#define  RMON2_TRC2(Value, Fmt, Arg1, Arg2)
#define  RMON2_TRC3(Value, Fmt, Arg1, Arg2, Arg3)
#define  RMON2_TRC4(Value, Fmt, Arg1, Arg2, Arg3, Arg4)
#define  RMON2_TRC5(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)
#define  RMON2_TRC6(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)
#define  RMON2_TRC7(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)

#endif /* TRACE_WANTED */

#define RMON2_MIN_TRACE_LEVEL 1
#define RMON2_MAX_TRACE_LEVEL 6
#define RMON2_MAX_FN_ENTRY  0x00000020

/* RMON2 FUNCTION Specific Trace Categories */
#define  RMON2_FN_ENTRY     0x00000001
#define  RMON2_FN_EXIT      0x00000002

/* Critical Trace */
#define  RMON2_CRITICAL_TRC     0x00000004

/* RMON2 MEMORY RESOURCE Specific Trace Categories */
#define  RMON2_MEM_FAIL     0x00000008
#define  RMON2_DEBUG_TRC    0x00000010
#endif /* __RMN2TRCE_H__ */

