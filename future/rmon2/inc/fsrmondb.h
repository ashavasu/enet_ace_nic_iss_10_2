/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrmondb.h,v 1.1.1.2 2009/03/20 14:19:34 nswamy-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSRMONDB_H
#define _FSRMONDB_H


UINT4 fsrmon [] ={1,3,6,1,4,1,2076,44};
tSNMP_OID_TYPE fsrmonOID = {8, fsrmon};


UINT4 FsRmon2Trace [ ] ={1,3,6,1,4,1,2076,44,1};
UINT4 FsRmon2AdminStatus [ ] ={1,3,6,1,4,1,2076,44,2};


tMbDbEntry fsrmonMibEntry[]= {

{{9,FsRmon2Trace}, NULL, FsRmon2TraceGet, FsRmon2TraceSet, FsRmon2TraceTest, FsRmon2TraceDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,FsRmon2AdminStatus}, NULL, FsRmon2AdminStatusGet, FsRmon2AdminStatusSet, FsRmon2AdminStatusTest, FsRmon2AdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData fsrmonEntry = { 2, fsrmonMibEntry };
#endif /* _FSRMONDB_H */

