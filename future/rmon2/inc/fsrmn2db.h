/********************************************************************
* Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
*
* $Id: fsrmn2db.h,v 1.2 2009/02/28 09:21:11 nswamy-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSRMN2DB_H
#define _FSRMN2DB_H


UINT4 fsrmn2 [] ={1,3,6,1,4,1,29601,2,19};
tSNMP_OID_TYPE fsrmn2OID = {9, fsrmn2};


UINT4 FsRmon2Trace [ ] ={1,3,6,1,4,1,29601,2,19,1};
UINT4 FsRmon2AdminStatus [ ] ={1,3,6,1,4,1,29601,2,19,2};


tMbDbEntry fsrmn2MibEntry[]= {

{{10,FsRmon2Trace}, NULL, FsRmon2TraceGet, FsRmon2TraceSet, FsRmon2TraceTest, FsRmon2TraceDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRmon2AdminStatus}, NULL, FsRmon2AdminStatusGet, FsRmon2AdminStatusSet, FsRmon2AdminStatusTest, FsRmon2AdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData fsrmn2Entry = { 2, fsrmn2MibEntry };
#endif /* _FSRMN2DB_H */

