/********************************************************************
* Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
*
* $Id: sdrmn2lw.h,v 1.2 2009/02/28 09:21:11 nswamy-iss Exp $
*
* Description: Proto types for Low Level  Routines
********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetProtocolDirLastChange ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for ProtocolDirTable. */
INT1
nmhValidateIndexInstanceProtocolDirTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for ProtocolDirTable  */

INT1
nmhGetFirstIndexProtocolDirTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexProtocolDirTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetProtocolDirLocalIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetProtocolDirDescr ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetProtocolDirType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetProtocolDirAddressMapConfig ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetProtocolDirHostConfig ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetProtocolDirMatrixConfig ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetProtocolDirOwner ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetProtocolDirStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetProtocolDirDescr ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetProtocolDirAddressMapConfig ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetProtocolDirHostConfig ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetProtocolDirMatrixConfig ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetProtocolDirOwner ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetProtocolDirStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2ProtocolDirDescr ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2ProtocolDirAddressMapConfig ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2ProtocolDirHostConfig ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2ProtocolDirMatrixConfig ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2ProtocolDirOwner ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2ProtocolDirStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2ProtocolDirTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for ProtocolDistControlTable. */
INT1
nmhValidateIndexInstanceProtocolDistControlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for ProtocolDistControlTable  */

INT1
nmhGetFirstIndexProtocolDistControlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexProtocolDistControlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetProtocolDistControlDataSource ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetProtocolDistControlDroppedFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetProtocolDistControlCreateTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetProtocolDistControlOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetProtocolDistControlStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetProtocolDistControlDataSource ARG_LIST((INT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetProtocolDistControlOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetProtocolDistControlStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2ProtocolDistControlDataSource ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2ProtocolDistControlOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2ProtocolDistControlStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2ProtocolDistControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for ProtocolDistStatsTable. */
INT1
nmhValidateIndexInstanceProtocolDistStatsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for ProtocolDistStatsTable  */

INT1
nmhGetFirstIndexProtocolDistStatsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexProtocolDistStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetProtocolDistStatsPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetProtocolDistStatsOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetAddressMapInserts ARG_LIST((UINT4 *));

INT1
nmhGetAddressMapDeletes ARG_LIST((UINT4 *));

INT1
nmhGetAddressMapMaxDesiredEntries ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetAddressMapMaxDesiredEntries ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2AddressMapMaxDesiredEntries ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2AddressMapMaxDesiredEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for AddressMapControlTable. */
INT1
nmhValidateIndexInstanceAddressMapControlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for AddressMapControlTable  */

INT1
nmhGetFirstIndexAddressMapControlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexAddressMapControlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetAddressMapControlDataSource ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetAddressMapControlDroppedFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetAddressMapControlOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetAddressMapControlStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetAddressMapControlDataSource ARG_LIST((INT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetAddressMapControlOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetAddressMapControlStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2AddressMapControlDataSource ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2AddressMapControlOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2AddressMapControlStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2AddressMapControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for AddressMapTable. */
INT1
nmhValidateIndexInstanceAddressMapTable ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE *));

/* Proto Type for Low Level GET FIRST fn for AddressMapTable  */

INT1
nmhGetFirstIndexAddressMapTable ARG_LIST((UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OID_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexAddressMapTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OID_TYPE *, tSNMP_OID_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetAddressMapPhysicalAddress ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetAddressMapLastChange ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE *,UINT4 *));

/* Proto Validate Index Instance for HlHostControlTable. */
INT1
nmhValidateIndexInstanceHlHostControlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for HlHostControlTable  */

INT1
nmhGetFirstIndexHlHostControlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexHlHostControlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetHlHostControlDataSource ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetHlHostControlNlDroppedFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetHlHostControlNlInserts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetHlHostControlNlDeletes ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetHlHostControlNlMaxDesiredEntries ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetHlHostControlAlDroppedFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetHlHostControlAlInserts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetHlHostControlAlDeletes ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetHlHostControlAlMaxDesiredEntries ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetHlHostControlOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetHlHostControlStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetHlHostControlDataSource ARG_LIST((INT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetHlHostControlNlMaxDesiredEntries ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetHlHostControlAlMaxDesiredEntries ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetHlHostControlOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetHlHostControlStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2HlHostControlDataSource ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2HlHostControlNlMaxDesiredEntries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2HlHostControlAlMaxDesiredEntries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2HlHostControlOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2HlHostControlStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2HlHostControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for NlHostTable. */
INT1
nmhValidateIndexInstanceNlHostTable ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for NlHostTable  */

INT1
nmhGetFirstIndexNlHostTable ARG_LIST((INT4 * , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexNlHostTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetNlHostInPkts ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetNlHostOutPkts ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetNlHostInOctets ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetNlHostOutOctets ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetNlHostOutMacNonUnicastPkts ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetNlHostCreateTime ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto Validate Index Instance for HlMatrixControlTable. */
INT1
nmhValidateIndexInstanceHlMatrixControlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for HlMatrixControlTable  */

INT1
nmhGetFirstIndexHlMatrixControlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexHlMatrixControlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetHlMatrixControlDataSource ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetHlMatrixControlNlDroppedFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetHlMatrixControlNlInserts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetHlMatrixControlNlDeletes ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetHlMatrixControlNlMaxDesiredEntries ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetHlMatrixControlAlDroppedFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetHlMatrixControlAlInserts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetHlMatrixControlAlDeletes ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetHlMatrixControlAlMaxDesiredEntries ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetHlMatrixControlOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetHlMatrixControlStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetHlMatrixControlDataSource ARG_LIST((INT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetHlMatrixControlNlMaxDesiredEntries ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetHlMatrixControlAlMaxDesiredEntries ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetHlMatrixControlOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetHlMatrixControlStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2HlMatrixControlDataSource ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2HlMatrixControlNlMaxDesiredEntries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2HlMatrixControlAlMaxDesiredEntries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2HlMatrixControlOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2HlMatrixControlStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2HlMatrixControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for NlMatrixSDTable. */
INT1
nmhValidateIndexInstanceNlMatrixSDTable ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for NlMatrixSDTable  */

INT1
nmhGetFirstIndexNlMatrixSDTable ARG_LIST((INT4 * , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexNlMatrixSDTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetNlMatrixSDPkts ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetNlMatrixSDOctets ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetNlMatrixSDCreateTime ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto Validate Index Instance for NlMatrixDSTable. */
INT1
nmhValidateIndexInstanceNlMatrixDSTable ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for NlMatrixDSTable  */

INT1
nmhGetFirstIndexNlMatrixDSTable ARG_LIST((INT4 * , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexNlMatrixDSTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetNlMatrixDSPkts ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetNlMatrixDSOctets ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetNlMatrixDSCreateTime ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto Validate Index Instance for NlMatrixTopNControlTable. */
INT1
nmhValidateIndexInstanceNlMatrixTopNControlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for NlMatrixTopNControlTable  */

INT1
nmhGetFirstIndexNlMatrixTopNControlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexNlMatrixTopNControlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetNlMatrixTopNControlMatrixIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetNlMatrixTopNControlRateBase ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetNlMatrixTopNControlTimeRemaining ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetNlMatrixTopNControlGeneratedReports ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetNlMatrixTopNControlDuration ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetNlMatrixTopNControlRequestedSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetNlMatrixTopNControlGrantedSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetNlMatrixTopNControlStartTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetNlMatrixTopNControlOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetNlMatrixTopNControlStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetNlMatrixTopNControlMatrixIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetNlMatrixTopNControlRateBase ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetNlMatrixTopNControlTimeRemaining ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetNlMatrixTopNControlRequestedSize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetNlMatrixTopNControlOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetNlMatrixTopNControlStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2NlMatrixTopNControlMatrixIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2NlMatrixTopNControlRateBase ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2NlMatrixTopNControlTimeRemaining ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2NlMatrixTopNControlRequestedSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2NlMatrixTopNControlOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2NlMatrixTopNControlStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2NlMatrixTopNControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for NlMatrixTopNTable. */
INT1
nmhValidateIndexInstanceNlMatrixTopNTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for NlMatrixTopNTable  */

INT1
nmhGetFirstIndexNlMatrixTopNTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexNlMatrixTopNTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetNlMatrixTopNProtocolDirLocalIndex ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetNlMatrixTopNSourceAddress ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetNlMatrixTopNDestAddress ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetNlMatrixTopNPktRate ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetNlMatrixTopNReversePktRate ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetNlMatrixTopNOctetRate ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetNlMatrixTopNReverseOctetRate ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for AlHostTable. */
INT1
nmhValidateIndexInstanceAlHostTable ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for AlHostTable  */

INT1
nmhGetFirstIndexAlHostTable ARG_LIST((INT4 * , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexAlHostTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetAlHostInPkts ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetAlHostOutPkts ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetAlHostInOctets ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetAlHostOutOctets ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetAlHostCreateTime ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

/* Proto Validate Index Instance for AlMatrixSDTable. */
INT1
nmhValidateIndexInstanceAlMatrixSDTable ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for AlMatrixSDTable  */

INT1
nmhGetFirstIndexAlMatrixSDTable ARG_LIST((INT4 * , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexAlMatrixSDTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetAlMatrixSDPkts ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetAlMatrixSDOctets ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetAlMatrixSDCreateTime ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

/* Proto Validate Index Instance for AlMatrixDSTable. */
INT1
nmhValidateIndexInstanceAlMatrixDSTable ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for AlMatrixDSTable  */

INT1
nmhGetFirstIndexAlMatrixDSTable ARG_LIST((INT4 * , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexAlMatrixDSTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetAlMatrixDSPkts ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetAlMatrixDSOctets ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetAlMatrixDSCreateTime ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

/* Proto Validate Index Instance for AlMatrixTopNControlTable. */
INT1
nmhValidateIndexInstanceAlMatrixTopNControlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for AlMatrixTopNControlTable  */

INT1
nmhGetFirstIndexAlMatrixTopNControlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexAlMatrixTopNControlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetAlMatrixTopNControlMatrixIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetAlMatrixTopNControlRateBase ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetAlMatrixTopNControlTimeRemaining ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetAlMatrixTopNControlGeneratedReports ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetAlMatrixTopNControlDuration ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetAlMatrixTopNControlRequestedSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetAlMatrixTopNControlGrantedSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetAlMatrixTopNControlStartTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetAlMatrixTopNControlOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetAlMatrixTopNControlStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetAlMatrixTopNControlMatrixIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetAlMatrixTopNControlRateBase ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetAlMatrixTopNControlTimeRemaining ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetAlMatrixTopNControlRequestedSize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetAlMatrixTopNControlOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetAlMatrixTopNControlStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2AlMatrixTopNControlMatrixIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2AlMatrixTopNControlRateBase ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2AlMatrixTopNControlTimeRemaining ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2AlMatrixTopNControlRequestedSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2AlMatrixTopNControlOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2AlMatrixTopNControlStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2AlMatrixTopNControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for AlMatrixTopNTable. */
INT1
nmhValidateIndexInstanceAlMatrixTopNTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for AlMatrixTopNTable  */

INT1
nmhGetFirstIndexAlMatrixTopNTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexAlMatrixTopNTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetAlMatrixTopNProtocolDirLocalIndex ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetAlMatrixTopNSourceAddress ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetAlMatrixTopNDestAddress ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetAlMatrixTopNAppProtocolDirLocalIndex ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetAlMatrixTopNPktRate ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetAlMatrixTopNReversePktRate ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetAlMatrixTopNOctetRate ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetAlMatrixTopNReverseOctetRate ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for UsrHistoryControlTable. */
INT1
nmhValidateIndexInstanceUsrHistoryControlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for UsrHistoryControlTable  */

INT1
nmhGetFirstIndexUsrHistoryControlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexUsrHistoryControlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetUsrHistoryControlObjects ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetUsrHistoryControlBucketsRequested ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetUsrHistoryControlBucketsGranted ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetUsrHistoryControlInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetUsrHistoryControlOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetUsrHistoryControlStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetUsrHistoryControlObjects ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetUsrHistoryControlBucketsRequested ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetUsrHistoryControlInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetUsrHistoryControlOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetUsrHistoryControlStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2UsrHistoryControlObjects ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2UsrHistoryControlBucketsRequested ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2UsrHistoryControlInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2UsrHistoryControlOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2UsrHistoryControlStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2UsrHistoryControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for UsrHistoryObjectTable. */
INT1
nmhValidateIndexInstanceUsrHistoryObjectTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for UsrHistoryObjectTable  */

INT1
nmhGetFirstIndexUsrHistoryObjectTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexUsrHistoryObjectTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetUsrHistoryObjectVariable ARG_LIST((INT4  , INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetUsrHistoryObjectSampleType ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetUsrHistoryObjectVariable ARG_LIST((INT4  , INT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetUsrHistoryObjectSampleType ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2UsrHistoryObjectVariable ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2UsrHistoryObjectSampleType ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2UsrHistoryObjectTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for UsrHistoryTable. */
INT1
nmhValidateIndexInstanceUsrHistoryTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for UsrHistoryTable  */

INT1
nmhGetFirstIndexUsrHistoryTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexUsrHistoryTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetUsrHistoryIntervalStart ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetUsrHistoryIntervalEnd ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetUsrHistoryAbsValue ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetUsrHistoryValStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetProbeCapabilities ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetProbeSoftwareRev ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetProbeHardwareRev ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetProbeDateTime ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetProbeResetControl ARG_LIST((INT4 *));

INT1
nmhGetProbeDownloadFile ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetProbeDownloadTFTPServer ARG_LIST((UINT4 *));

INT1
nmhGetProbeDownloadAction ARG_LIST((INT4 *));

INT1
nmhGetProbeDownloadStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetProbeDateTime ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetProbeResetControl ARG_LIST((INT4 ));

INT1
nmhSetProbeDownloadFile ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetProbeDownloadTFTPServer ARG_LIST((UINT4 ));

INT1
nmhSetProbeDownloadAction ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2ProbeDateTime ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2ProbeResetControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2ProbeDownloadFile ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2ProbeDownloadTFTPServer ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2ProbeDownloadAction ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2ProbeDateTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2ProbeResetControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2ProbeDownloadFile ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2ProbeDownloadTFTPServer ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2ProbeDownloadAction ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for SerialConfigTable. */
INT1
nmhValidateIndexInstanceSerialConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for SerialConfigTable  */

INT1
nmhGetFirstIndexSerialConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexSerialConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSerialMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetSerialProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetSerialTimeout ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetSerialModemInitString ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSerialModemHangUpString ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSerialModemConnectResp ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSerialModemNoConnectResp ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSerialDialoutTimeout ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetSerialStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetSerialMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetSerialProtocol ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetSerialTimeout ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetSerialModemInitString ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSerialModemHangUpString ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSerialModemConnectResp ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSerialModemNoConnectResp ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSerialDialoutTimeout ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetSerialStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2SerialMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2SerialProtocol ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2SerialTimeout ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2SerialModemInitString ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SerialModemHangUpString ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SerialModemConnectResp ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SerialModemNoConnectResp ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SerialDialoutTimeout ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2SerialStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2SerialConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for NetConfigTable. */
INT1
nmhValidateIndexInstanceNetConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for NetConfigTable  */

INT1
nmhGetFirstIndexNetConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexNetConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetNetConfigIPAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetNetConfigSubnetMask ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetNetConfigStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetNetConfigIPAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetNetConfigSubnetMask ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetNetConfigStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2NetConfigIPAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2NetConfigSubnetMask ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2NetConfigStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2NetConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetNetDefaultGateway ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetNetDefaultGateway ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2NetDefaultGateway ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2NetDefaultGateway ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for TrapDestTable. */
INT1
nmhValidateIndexInstanceTrapDestTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for TrapDestTable  */

INT1
nmhGetFirstIndexTrapDestTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexTrapDestTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetTrapDestCommunity ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetTrapDestProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetTrapDestAddress ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetTrapDestOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetTrapDestStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetTrapDestCommunity ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetTrapDestProtocol ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetTrapDestAddress ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetTrapDestOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetTrapDestStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2TrapDestCommunity ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2TrapDestProtocol ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2TrapDestAddress ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2TrapDestOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2TrapDestStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2TrapDestTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for SerialConnectionTable. */
INT1
nmhValidateIndexInstanceSerialConnectionTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for SerialConnectionTable  */

INT1
nmhGetFirstIndexSerialConnectionTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexSerialConnectionTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSerialConnectDestIpAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetSerialConnectType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetSerialConnectDialString ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSerialConnectSwitchConnectSeq ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSerialConnectSwitchDisconnectSeq ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSerialConnectSwitchResetSeq ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSerialConnectOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSerialConnectStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetSerialConnectDestIpAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetSerialConnectType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetSerialConnectDialString ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSerialConnectSwitchConnectSeq ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSerialConnectSwitchDisconnectSeq ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSerialConnectSwitchResetSeq ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSerialConnectOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSerialConnectStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2SerialConnectDestIpAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2SerialConnectType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2SerialConnectDialString ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SerialConnectSwitchConnectSeq ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SerialConnectSwitchDisconnectSeq ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SerialConnectSwitchResetSeq ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SerialConnectOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SerialConnectStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2SerialConnectionTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for EtherStats2Table. */
INT1
nmhValidateIndexInstanceEtherStats2Table ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for EtherStats2Table  */

INT1
nmhGetFirstIndexEtherStats2Table ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexEtherStats2Table ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetEtherStatsDroppedFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsCreateTime ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for HistoryControl2Table. */
INT1
nmhValidateIndexInstanceHistoryControl2Table ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for HistoryControl2Table  */

INT1
nmhGetFirstIndexHistoryControl2Table ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexHistoryControl2Table ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetHistoryControlDroppedFrames ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for HostControl2Table. */
INT1
nmhValidateIndexInstanceHostControl2Table ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for HostControl2Table  */

INT1
nmhGetFirstIndexHostControl2Table ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexHostControl2Table ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetHostControlDroppedFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetHostControlCreateTime ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for MatrixControl2Table. */
INT1
nmhValidateIndexInstanceMatrixControl2Table ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for MatrixControl2Table  */

INT1
nmhGetFirstIndexMatrixControl2Table ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMatrixControl2Table ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMatrixControlDroppedFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetMatrixControlCreateTime ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for Channel2Table. */
INT1
nmhValidateIndexInstanceChannel2Table ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Channel2Table  */

INT1
nmhGetFirstIndexChannel2Table ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexChannel2Table ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetChannelDroppedFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetChannelCreateTime ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for TokenRingMLStats2Table. */
INT1
nmhValidateIndexInstanceTokenRingMLStats2Table ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for TokenRingMLStats2Table  */

INT1
nmhGetFirstIndexTokenRingMLStats2Table ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexTokenRingMLStats2Table ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetTokenRingMLStatsDroppedFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetTokenRingMLStatsCreateTime ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for TokenRingPStats2Table. */
INT1
nmhValidateIndexInstanceTokenRingPStats2Table ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for TokenRingPStats2Table  */

INT1
nmhGetFirstIndexTokenRingPStats2Table ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexTokenRingPStats2Table ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetTokenRingPStatsDroppedFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetTokenRingPStatsCreateTime ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for RingStationControl2Table. */
INT1
nmhValidateIndexInstanceRingStationControl2Table ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for RingStationControl2Table  */

INT1
nmhGetFirstIndexRingStationControl2Table ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexRingStationControl2Table ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetRingStationControlDroppedFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRingStationControlCreateTime ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for SourceRoutingStats2Table. */
INT1
nmhValidateIndexInstanceSourceRoutingStats2Table ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for SourceRoutingStats2Table  */

INT1
nmhGetFirstIndexSourceRoutingStats2Table ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexSourceRoutingStats2Table ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSourceRoutingStatsDroppedFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetSourceRoutingStatsCreateTime ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for Filter2Table. */
INT1
nmhValidateIndexInstanceFilter2Table ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Filter2Table  */

INT1
nmhGetFirstIndexFilter2Table ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFilter2Table ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFilterProtocolDirDataLocalIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFilterProtocolDirLocalIndex ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFilterProtocolDirDataLocalIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFilterProtocolDirLocalIndex ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FilterProtocolDirDataLocalIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FilterProtocolDirLocalIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Filter2Table ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
