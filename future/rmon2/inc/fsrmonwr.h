#ifndef _FSRMONWR_H
#define _FSRMONWR_H

VOID RegisterFSRMON(VOID);

VOID UnRegisterFSRMON(VOID);
INT4 FsRmon2TraceGet(tSnmpIndex *, tRetVal *);
INT4 FsRmon2AdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsRmon2TraceSet(tSnmpIndex *, tRetVal *);
INT4 FsRmon2AdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsRmon2TraceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRmon2AdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRmon2TraceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRmon2AdminStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _FSRMONWR_H */
