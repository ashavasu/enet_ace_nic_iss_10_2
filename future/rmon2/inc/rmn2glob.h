/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * Description: This file externs RMON2 global variable declaration.
 *******************************************************************/
#ifndef _RMN2GLOB_H_
#define _RMN2GLOB_H_

tRmon2Globals gRmon2Globals;
tRmon2PktInfo gRmon2PktInfo;

UINT4 gIfEntryOIDTree[IFINDEX_OID_SIZE + 1] = {1,3,6,1,2,1,2,2,1,1};
#endif

