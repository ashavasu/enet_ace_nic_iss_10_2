/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * $Id: rmn2inc.h,v 1.12 2015/09/13 10:36:21 siva Exp $
 *
* Description: This file contains inclusion of all RMON2 
 *              Header files.
 *******************************************************************/

#ifndef _RMN2INC_H_
#define _RMN2INC_H_

#include "lr.h"
#include "cfa.h"
#include "msr.h"
#include "rmon.h"
#include "rmon2.h"
#include "l2iwf.h"
#include "npapi.h"

#include "rmn2macs.h"
#include "rmn2defn.h"
#include "rmn2enum.h"
#include "rmn2tmr.h"
#include "rmn2tdfs.h"
#include "fsutil.h"
#ifdef BSDCOMP_SLI_WANTED
#include "fssocket.h"
#endif
#ifdef _RMN2MAIN_C_
    #include "rmn2glob.h"
#else
    #include "rmn2extn.h"
#endif /* _RMN2MAIN_C_ */

#include "rmon2np.h"

#include "rmn2trce.h"
#include "rmn2prot.h"

#include "sdrmn2wr.h"
#include "sdrmn2lw.h"
#include "fsrmn2wr.h"
#include "fsrmn2lw.h"

#ifdef SNMP_2_WANTED
#include "snmputil.h"
#endif

#include "rmon2sz.h"

#ifdef NPAPI_WANTED
#include "rmn2npwr.h"
#endif

#include  "vcm.h"
#endif /* _RMN2INC_H_ */

