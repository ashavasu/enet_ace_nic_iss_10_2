/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * Description: Proto types for RMON2 wrapper Routines
 *******************************************************************/

#ifndef _FSRMN2WR_H
#define _FSRMN2WR_H

VOID RegisterFSRMN2(VOID);

VOID UnRegisterFSRMN2(VOID);
INT4 FsRmon2TraceGet(tSnmpIndex *, tRetVal *);
INT4 FsRmon2AdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsRmon2TraceSet(tSnmpIndex *, tRetVal *);
INT4 FsRmon2AdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsRmon2TraceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRmon2AdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRmon2TraceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRmon2AdminStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _FSRMN2WR_H */
