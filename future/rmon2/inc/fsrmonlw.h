/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrmonlw.h,v 1.1.1.2 2009/03/20 14:19:34 nswamy-iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRmon2Trace ARG_LIST((UINT4 *));

INT1
nmhGetFsRmon2AdminStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRmon2Trace ARG_LIST((UINT4 ));

INT1
nmhSetFsRmon2AdminStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRmon2Trace ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsRmon2AdminStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRmon2Trace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRmon2AdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
