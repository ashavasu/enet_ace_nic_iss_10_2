####################################################################
#### Copyright (C) 2009 Aricent Inc . All Rights Reserved        ####
##|                                                               |##
##|    FILE NAME               ::  Makefile                       |##
##|                                                               |##
##|    DESCRIPTION             ::  Makefile for rmon2             |##
##|                                                               |##
##|###############################################################|##

include ../LR/make.h
include ../LR/make.rule
include ./make.h

.PHONY :  START ECHO DONE clean


C_FLAGS           = ${CC_FLAGS} ${TOTAL_OPNS} ${INCLUDES}
RMON2_FINAL_OBJ    = ${RMON2_OBJ_DIR}/FutureRMON2.o

START : ECHO ${RMON2_FINAL_OBJ} DONE

ECHO  :
	@echo Making RMON2 Module...
DONE  :
	@echo RMON2 Module done.

#object module list

RMON2_OBJS =\
       ${RMON2_OBJ_DIR}/rmn2main.o \
       ${RMON2_OBJ_DIR}/rmn2tmr.o \
       ${RMON2_OBJ_DIR}/rmn2pkth.o \
       ${RMON2_OBJ_DIR}/rmn2util.o \
       ${RMON2_OBJ_DIR}/rmn2cfif.o \
       ${RMON2_OBJ_DIR}/rmn2pdir.o \
       ${RMON2_OBJ_DIR}/rmn2pdst.o \
       ${RMON2_OBJ_DIR}/rmn2admp.o \
       ${RMON2_OBJ_DIR}/rmn2host.o \
       ${RMON2_OBJ_DIR}/rmn2mtrx.o \
       ${RMON2_OBJ_DIR}/rmn2nltn.o \
       ${RMON2_OBJ_DIR}/rmn2altn.o \
       ${RMON2_OBJ_DIR}/rmn2uhst.o \
       ${RMON2_OBJ_DIR}/rmn2pcfg.o \
       ${RMON2_OBJ_DIR}/rmn2pdlw.o \
       ${RMON2_OBJ_DIR}/rmn2pslw.o \
       ${RMON2_OBJ_DIR}/rmn2amlw.o \
       ${RMON2_OBJ_DIR}/rmn2htlw.o \
       ${RMON2_OBJ_DIR}/rmn2mxlw.o \
       ${RMON2_OBJ_DIR}/rmn2ntlw.o \
       ${RMON2_OBJ_DIR}/rmn2atlw.o \
       ${RMON2_OBJ_DIR}/rmn2uhlw.o \
       ${RMON2_OBJ_DIR}/rmn2exlw.o \
       ${RMON2_OBJ_DIR}/rmn2pclw.o \
       ${RMON2_OBJ_DIR}/fsrmn2lw.o \
       ${RMON2_OBJ_DIR}/rmon2sz.o 

ifeq (${NPAPI}, YES)
RMON2_OBJS += \
       ${RMON2_OBJ_DIR}/rmn2port.o \
       ${RMON2_OBJ_DIR}/rmn2npwr.o \
       ${RMON2_OBJ_DIR}/rmn2npapi.o
endif

ifeq (${SNMP_2}, YES)
RMON2_OBJS += \
       ${RMON2_OBJ_DIR}/sdrmn2wr.o \
       ${RMON2_OBJ_DIR}/fsrmn2wr.o
endif

ifeq (${CLI}, YES)
RMON2_OBJS += \
       ${RMON2_OBJ_DIR}/rmn2cli.o
endif

#object module dependency
${RMON2_FINAL_OBJ} : obj ${RMON2_OBJS}
	${LD} ${LD_FLAGS} ${CC_COMMON_OPTIONS} -o ${RMON2_FINAL_OBJ} ${RMON2_OBJS}


EXTERNAL_DEPENDENCIES = \
        ${COMN_INCL_DIR}/lr.h \
        ${COMN_INCL_DIR}/cfa.h \
        ${COMN_INCL_DIR}/l2iwf.h \
        ${COMN_INCL_DIR}/rmon.h \
        ${COMN_INCL_DIR}/rmon2.h \
        ${COMN_INCL_DIR}/npapi/rmon2np.h

INTERNAL_DEPENDENCIES = \
        ${RMON2_BASE_DIR}/make.h   \
        ${RMON2_BASE_DIR}/Makefile \
        ${RMON2_INC_DIR}/rmn2tdfs.h \
        ${RMON2_INC_DIR}/rmn2defn.h \
        ${RMON2_INC_DIR}/rmn2macs.h \
        ${RMON2_INC_DIR}/rmn2glob.h \
        ${RMON2_INC_DIR}/rmn2enum.h \
        ${RMON2_INC_DIR}/rmn2prot.h \
        ${RMON2_INC_DIR}/rmn2tmr.h \
        ${RMON2_INC_DIR}/rmn2trce.h \
        ${RMON2_INC_DIR}/rmn2inc.h

ifeq (${NPAPI}, YES)
INTERNAL_DEPENDENCIES = \
        ${COMN_INCL_DIR}/rmn2npwr.h 
endif

DEPENDENCIES =  \
    ${EXTERNAL_DEPENDENCIES} \
    ${INTERNAL_DEPENDENCIES} \
    ${COMMON_DEPENDENCIES}

obj:
ifdef MKDIR
	$(MKDIR) $(MKDIR_FLAGS) $(RMON2_OBJ_DIR)
endif


$(RMON2_OBJ_DIR)/rmn2main.o : \
   $(RMON2_SRC_DIR)/rmn2main.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2main.c

$(RMON2_OBJ_DIR)/rmn2tmr.o : \
   $(RMON2_SRC_DIR)/rmn2tmr.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2tmr.c
   
$(RMON2_OBJ_DIR)/rmn2pkth.o : \
   $(RMON2_SRC_DIR)/rmn2pkth.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2pkth.c

$(RMON2_OBJ_DIR)/rmn2util.o : \
   $(RMON2_SRC_DIR)/rmn2util.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2util.c
   
$(RMON2_OBJ_DIR)/rmn2port.o : \
   $(RMON2_SRC_DIR)/rmn2port.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2port.c

$(RMON2_OBJ_DIR)/rmn2cfif.o : \
   $(RMON2_SRC_DIR)/rmn2cfif.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2cfif.c
  
$(RMON2_OBJ_DIR)/rmn2pdir.o : \
   $(RMON2_SRC_DIR)/rmn2pdir.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2pdir.c

$(RMON2_OBJ_DIR)/rmn2pdst.o : \
   $(RMON2_SRC_DIR)/rmn2pdst.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2pdst.c

$(RMON2_OBJ_DIR)/rmn2admp.o : \
   $(RMON2_SRC_DIR)/rmn2admp.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2admp.c

$(RMON2_OBJ_DIR)/rmn2host.o : \
   $(RMON2_SRC_DIR)/rmn2host.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2host.c

$(RMON2_OBJ_DIR)/rmn2mtrx.o : \
   $(RMON2_SRC_DIR)/rmn2mtrx.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2mtrx.c

$(RMON2_OBJ_DIR)/rmn2nltn.o : \
   $(RMON2_SRC_DIR)/rmn2nltn.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2nltn.c
   
$(RMON2_OBJ_DIR)/rmn2altn.o : \
   $(RMON2_SRC_DIR)/rmn2altn.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2altn.c

$(RMON2_OBJ_DIR)/rmn2uhst.o : \
   $(RMON2_SRC_DIR)/rmn2uhst.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2uhst.c

$(RMON2_OBJ_DIR)/rmn2pcfg.o : \
  $(RMON2_SRC_DIR)/rmn2pcfg.c \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2pcfg.c

$(RMON2_OBJ_DIR)/rmn2pdlw.o : \
   $(RMON2_SRC_DIR)/rmn2pdlw.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2pdlw.c

$(RMON2_OBJ_DIR)/rmn2pslw.o : \
   $(RMON2_SRC_DIR)/rmn2pslw.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2pslw.c

$(RMON2_OBJ_DIR)/rmn2amlw.o : \
   $(RMON2_SRC_DIR)/rmn2amlw.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2amlw.c

$(RMON2_OBJ_DIR)/rmn2htlw.o : \
   $(RMON2_SRC_DIR)/rmn2htlw.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2htlw.c

$(RMON2_OBJ_DIR)/rmn2mxlw.o : \
   $(RMON2_SRC_DIR)/rmn2mxlw.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2mxlw.c

$(RMON2_OBJ_DIR)/rmn2ntlw.o : \
   $(RMON2_SRC_DIR)/rmn2ntlw.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2ntlw.c

$(RMON2_OBJ_DIR)/rmn2atlw.o : \
   $(RMON2_SRC_DIR)/rmn2atlw.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2atlw.c

$(RMON2_OBJ_DIR)/rmn2uhlw.o : \
   $(RMON2_SRC_DIR)/rmn2uhlw.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2uhlw.c

$(RMON2_OBJ_DIR)/rmn2pclw.o : \
   $(RMON2_SRC_DIR)/rmn2pclw.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2pclw.c

$(RMON2_OBJ_DIR)/rmn2exlw.o : \
   $(RMON2_SRC_DIR)/rmn2exlw.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2exlw.c
    
$(RMON2_OBJ_DIR)/fsrmn2lw.o : \
   $(RMON2_SRC_DIR)/fsrmn2lw.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/fsrmn2lw.c

$(RMON2_OBJ_DIR)/sdrmn2wr.o : \
   $(RMON2_SRC_DIR)/sdrmn2wr.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/sdrmn2wr.c

$(RMON2_OBJ_DIR)/fsrmn2wr.o : \
   $(RMON2_SRC_DIR)/fsrmn2wr.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/fsrmn2wr.c

$(RMON2_OBJ_DIR)/rmn2cli.o : \
   $(RMON2_SRC_DIR)/rmn2cli.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2cli.c

$(RMON2_OBJ_DIR)/rmon2sz.o : \
   $(RMON2_SRC_DIR)/rmon2sz.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmon2sz.c

$(RMON2_OBJ_DIR)/rmn2npwr.o : \
   $(RMON2_SRC_DIR)/rmn2npwr.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2npwr.c

$(RMON2_OBJ_DIR)/rmn2npapi.o : \
   $(RMON2_SRC_DIR)/rmn2npapi.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) -o $@ $(RMON2_SRC_DIR)/rmn2npapi.c

clean:
	@echo "Cleaning Rmon2 Module............ "
	$(RM) $(RM_FLAGS) $(RMON2_OBJ_DIR)/*.o
	@echo "Cleaning Rmon2 Module Done ...... "

pkg:
	CUR_PWD=${PWD};\
        cd ${BASE_DIR}/../..;\
        tar cvzf ${ISS_PKG_CREATE_DIR}/rmon2.tgz -T ${BASE_DIR}/rmon2/FILES.NEW;\
        cd ${CUR_PWD};

