/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * $Id: rmn2pdst.c,v 1.10 2015/10/14 12:56:08 siva Exp $
 *
 * Description: This file contains the functional routine for
 *              RMON2 Protocol Distribution module
 *
 *******************************************************************/
#include "rmn2inc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDistProcessPktInfo                                   */
/*                                                                           */
/* Description  : Process Incoming Packet from Control Plane and Update      */
/*                Pdist Table                                                */
/*                                                                           */
/* Input        : pPktInfo                                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rmon2PDistProcessPktInfo (tRmon2PktInfo * pPktInfo, UINT4 u4PktSize)
{
    tProtocolDistControl *pPDistCtrl = NULL;
    tProtocolDistStats *pCurPDist = NULL, *pPrevPDist = NULL;

    UINT4               u4ProtocolDistControlIndex = 0;
    INT4                i4RetVal = OSIX_FAILURE;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PDistProcessPktInfo\r\n");

    pPDistCtrl = Rmon2PDistGetNextCtrlIndex (u4ProtocolDistControlIndex);

    while (pPDistCtrl != NULL)
    {
        if ((pPDistCtrl->u4ProtocolDistControlStatus == ACTIVE) &&
            ((pPDistCtrl->u4ProtocolDistControlDataSource ==
              pPktInfo->PktHdr.u4IfIndex) ||
             (pPDistCtrl->u4ProtocolDistControlDataSource ==
              pPktInfo->PktHdr.u4VlanIfIndex)))
        {
            if (pPDistCtrl->u4ProtocolDistControlDataSource ==
                pPktInfo->PktHdr.u4VlanIfIndex)
            {
                pPktInfo->u1IsL3Vlan = OSIX_TRUE;
            }

            if (pPktInfo->u4ProtoL5ALIndex != 0)
            {
                /* Checking for an PDistStats Entry for L5 Protocol,
                 * already exist or not */

                pCurPDist =
                    Rmon2PDistGetDataEntry (pPDistCtrl->
                                            u4ProtocolDistControlIndex,
                                            pPktInfo->u4ProtoL5ALIndex);
                if (pCurPDist == NULL)
                {
                    pCurPDist =
                        Rmon2PDistAddDataEntry (pPDistCtrl->
                                                u4ProtocolDistControlIndex,
                                                pPktInfo->u4ProtoL5ALIndex);

                    if (pCurPDist == NULL)
                    {
                        pPDistCtrl->u4ProtocolDistControlDroppedFrames++;
                        pPDistCtrl =
                            Rmon2PDistGetNextCtrlIndex (pPDistCtrl->
                                                        u4ProtocolDistControlIndex);
                        continue;
                    }
                    else
                    {
                        pCurPDist->pPDistCtrl = pPDistCtrl;
                    }
                }
                pCurPDist->u4ProtocolDistStatsPkts++;
                pCurPDist->u4ProtocolDistStatsOctets += u4PktSize;
                pCurPDist->u4PktRefCount++;
                if (pPrevPDist == NULL)
                {
                    pPktInfo->pPDist = pCurPDist;
                }
                else
                {
                    pPrevPDist->pNextPDist = pCurPDist;
                    pCurPDist->pPrevPDist = pPrevPDist;
                }
                pPrevPDist = pCurPDist;
                i4RetVal = OSIX_SUCCESS;
            }

            if (pPktInfo->u4ProtoL4ALIndex != 0)
            {
                /* Checking for an PDistStats Entry for L4 Protocol,
                 * already exist or not */

                pCurPDist =
                    Rmon2PDistGetDataEntry (pPDistCtrl->
                                            u4ProtocolDistControlIndex,
                                            pPktInfo->u4ProtoL4ALIndex);
                if (pCurPDist == NULL)
                {
                    pCurPDist =
                        Rmon2PDistAddDataEntry (pPDistCtrl->
                                                u4ProtocolDistControlIndex,
                                                pPktInfo->u4ProtoL4ALIndex);

                    if (pCurPDist == NULL)
                    {
                        pPDistCtrl->u4ProtocolDistControlDroppedFrames++;
                        pPDistCtrl =
                            Rmon2PDistGetNextCtrlIndex (pPDistCtrl->
                                                        u4ProtocolDistControlIndex);
                        continue;
                    }
                    else
                    {
                        pCurPDist->pPDistCtrl = pPDistCtrl;
                    }
                }
                pCurPDist->u4ProtocolDistStatsPkts++;
                pCurPDist->u4ProtocolDistStatsOctets += u4PktSize;
                pCurPDist->u4PktRefCount++;
                if (pPrevPDist == NULL)
                {
                    pPktInfo->pPDist = pCurPDist;
                }
                else
                {
                    pPrevPDist->pNextPDist = pCurPDist;
                    pCurPDist->pPrevPDist = pPrevPDist;
                }
                pPrevPDist = pCurPDist;
                i4RetVal = OSIX_SUCCESS;
            }

            if (pPktInfo->u4ProtoNLIndex != 0)
            {
                /* Checking for an PDistStats Entry for Nl Protocol,
                 * already exist or not */
                pCurPDist =
                    Rmon2PDistGetDataEntry (pPDistCtrl->
                                            u4ProtocolDistControlIndex,
                                            pPktInfo->u4ProtoNLIndex);

                if (pCurPDist == NULL)
                {
                    pCurPDist =
                        Rmon2PDistAddDataEntry (pPDistCtrl->
                                                u4ProtocolDistControlIndex,
                                                pPktInfo->u4ProtoNLIndex);

                    if (pCurPDist == NULL)
                    {
                        pPDistCtrl->u4ProtocolDistControlDroppedFrames++;
                        pPDistCtrl =
                            Rmon2PDistGetNextCtrlIndex (pPDistCtrl->
                                                        u4ProtocolDistControlIndex);
                        continue;
                    }
                    else
                    {
                        pCurPDist->pPDistCtrl = pPDistCtrl;
                    }
                }
                pCurPDist->u4ProtocolDistStatsPkts++;
                pCurPDist->u4ProtocolDistStatsOctets += u4PktSize;
                pCurPDist->u4PktRefCount++;
                if (pPrevPDist == NULL)
                {
                    pPktInfo->pPDist = pCurPDist;
                }
                else
                {
                    pPrevPDist->pNextPDist = pCurPDist;
                    pCurPDist->pPrevPDist = pPrevPDist;
                }
                pPrevPDist = pCurPDist;
                i4RetVal = OSIX_SUCCESS;
            }

        }                        /* End of if check */

        pPDistCtrl =
            Rmon2PDistGetNextCtrlIndex (pPDistCtrl->u4ProtocolDistControlIndex);
    }                            /* End of while */
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2PDistProcessPktInfo\r\n");

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDistUpdateIFIndexChgs                                */
/*                                                                           */
/* Description  : If Operstatus is CFA_IF_DOWN, then set appropriate control */
/*                entry to NOT_READY state thus deleting the associated      */
/*                Data Table. If OperStatus is CFA_IF_UP, set NOT_READY      */
/*                entry to ACTIVE state                                      */
/*                                                                           */
/* Input        : u4IfIndex, u1OperStatus                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2PDistUpdateIFIndexChgs (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tProtocolDistControl *pPDistCtrl = NULL;

    UINT4               u4ProtocolDistControlIndex = 0;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PDistUpdateIFIndexChgs\r\n");

    switch (u1OperStatus)
    {
        case CFA_IF_DOWN:
            /* Get 1st Hl Matrix Ctrl Index */
            pPDistCtrl =
                Rmon2PDistGetNextCtrlIndex (u4ProtocolDistControlIndex);

            while (pPDistCtrl != NULL)
            {
                u4ProtocolDistControlIndex =
                    pPDistCtrl->u4ProtocolDistControlIndex;
                if ((pPDistCtrl->u4ProtocolDistControlStatus == ACTIVE) &&
                    (pPDistCtrl->u4ProtocolDistControlDataSource == u4IfIndex))
                {

                    /* Remove Data Entries for this control index from 
                     * PDistStats Table */

                    Rmon2PDistDelDataEntries (u4ProtocolDistControlIndex);

                    /* Change RowStatus to NOT_READY */
                    pPDistCtrl->u4ProtocolDistControlStatus = NOT_READY;

                }                /* End of if check */
                pPDistCtrl =
                    Rmon2PDistGetNextCtrlIndex (u4ProtocolDistControlIndex);

            }                    /* End of while */
            break;

        case CFA_IF_UP:
            /* Get 1st Hl Matrix Ctrl Index */
            pPDistCtrl =
                Rmon2PDistGetNextCtrlIndex (u4ProtocolDistControlIndex);
            while (pPDistCtrl != NULL)
            {
                u4ProtocolDistControlIndex =
                    pPDistCtrl->u4ProtocolDistControlIndex;
                if ((pPDistCtrl->u4ProtocolDistControlStatus == NOT_READY) &&
                    (pPDistCtrl->u4ProtocolDistControlDataSource == u4IfIndex))
                {
                    /* Set RowStatus to ACTIVE */
                    nmhSetProtocolDistControlStatus ((INT4)
                                                     u4ProtocolDistControlIndex,
                                                     ACTIVE);
                }                /* End of if check */
                pPDistCtrl =
                    Rmon2PDistGetNextCtrlIndex (u4ProtocolDistControlIndex);
            }                    /* End of while */
            break;

        default:
            break;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2PDistUpdateIFIndexChgs\r\n");

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDistUpdateProtoChgs                                  */
/*                                                                           */
/* Description  : Removes Protocol Distribution Data Table Entries for       */
/*                change in Protocol Dir Entry Status                        */
/*                                                                           */
/* Input        : u4ProtocolDirLocalIndex                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2PDistUpdateProtoChgs (UINT4 u4ProtocolIndex)
{
    tProtocolDistStats *pProtocolDistStats = NULL;
    UINT4               u4ProtocolDistControlIndex =
        0, u4ProtocolDirLocalIndex = 0;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PDistUpdateProtoChgs\r\n");

    /* Get First Entry in Nl Host Table */

    pProtocolDistStats = Rmon2PDistGetNextDataIndex (u4ProtocolDistControlIndex,
                                                     u4ProtocolDirLocalIndex);

    while (pProtocolDistStats != NULL)
    {
        u4ProtocolDistControlIndex =
            pProtocolDistStats->u4ProtocolDistControlIndex;
        u4ProtocolDirLocalIndex = pProtocolDistStats->u4ProtocolDirLocalIndex;

        if (pProtocolDistStats->u4ProtocolDirLocalIndex == u4ProtocolIndex)
        {
            /* Remove Inter table dependencies */
            Rmon2PDistDelInterDep (pProtocolDistStats);

            /* After Reference changes, free PDist entry */
            Rmon2PDistDelDataEntry (pProtocolDistStats);
        }

        pProtocolDistStats =
            Rmon2PDistGetNextDataIndex (u4ProtocolDistControlIndex,
                                        u4ProtocolDirLocalIndex);
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2PDistUpdateProtoChgs\r\n");

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDistDelDataEntries                                   */
/*                                                                           */
/* Description  : Removes Protocol Dist Stats Data Table Entries for         */
/*                given Control Index                                        */
/*                                                                           */
/* Input        : u4PDistCtrlIndex                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2PDistDelDataEntries (UINT4 u4ProtocolDistControlIndex)
{
    tProtocolDistStats *pPDist = NULL;
    UINT4               u4ProtocolDirLocalIndex = 0;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PDistDelDataEntries\r\n");

    /* Remove Data Entries for this control index from PDist Stats Table */
    pPDist = Rmon2PDistGetNextDataIndex (u4ProtocolDistControlIndex,
                                         u4ProtocolDirLocalIndex);

    while (pPDist != NULL)
    {
        if (pPDist->u4ProtocolDistControlIndex != u4ProtocolDistControlIndex)
        {
            break;
        }

        u4ProtocolDirLocalIndex = pPDist->u4ProtocolDirLocalIndex;

        /* Remove Inter table references */
        Rmon2PDistDelInterDep (pPDist);

        /* After Reference changes, free PDist Stats entry */
        Rmon2PDistDelDataEntry (pPDist);
        pPDist = Rmon2PDistGetNextDataIndex (u4ProtocolDistControlIndex,
                                             u4ProtocolDirLocalIndex);
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2PDistDelDataEntries\r\n");

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDistGetNextCtrlIndex                                 */
/*                                                                           */
/* Description  : Get First / Next PDist Ctrl Index                          */
/*                                                                           */
/* Input        : u4ProtocolDistControlIndex                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tProtocolDistControl / NULL Pointer                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC tProtocolDistControl *
Rmon2PDistGetNextCtrlIndex (UINT4 u4ProtocolDistControlIndex)
{
    tProtocolDistControl PDistCtrl;
    MEMSET (&PDistCtrl, 0, sizeof (tProtocolDistControl));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PDistGetNextCtrlIndex\r\n");

    PDistCtrl.u4ProtocolDistControlIndex = u4ProtocolDistControlIndex;
    return ((tProtocolDistControl *) RBTreeGetNext (RMON2_PDISTCTRL_TREE,
                                                    (tRBElem *) & PDistCtrl,
                                                    NULL));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDistAddCtrlEntry                                     */
/*                                                                           */
/* Description  : Allocates memory and Adds PDist Control Entry into         */
/*                ProtocolDistControlRBTree                                  */
/*                                                                           */
/* Input        : pPDistCtrl                                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tProtocolDistControl / NULL Pointer                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC tProtocolDistControl *
Rmon2PDistAddCtrlEntry (UINT4 u4ProtocolDistControlIndex)
{
    tProtocolDistControl *pPDistCtrl = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PDistAddCtrlEntry\r\n");

    if ((pPDistCtrl = (tProtocolDistControl *)
         (MemAllocMemBlk (RMON2_PDISTCTRL_POOL))) == NULL)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for ProtocolDistControlEntry\r\n");
        return NULL;
    }
    MEMSET (pPDistCtrl, 0, sizeof (tProtocolDistControl));

    pPDistCtrl->u4ProtocolDistControlIndex = u4ProtocolDistControlIndex;
    pPDistCtrl->u4ProtocolDistControlDroppedFrames = RMON2_ZERO;
    pPDistCtrl->u4ProtocolDistControlCreateTime = OsixGetSysUpTime ();

    if (RBTreeAdd (RMON2_PDISTCTRL_TREE, (tRBElem *) pPDistCtrl) == RB_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Rmon2PDistAddCtrlEntry: ProtocolDistControlEntry is added\r\n");
        return pPDistCtrl;
    }
    RMON2_TRC (RMON2_CRITICAL_TRC,
               "Rmon2PDistAddCtrlEntry: Adding ProtocolDistControlEntry Failed\r\n");

    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (RMON2_PDISTCTRL_POOL, (UINT1 *) pPDistCtrl);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2PDistAddCtrlEntry\r\n");

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDistDelCtrlEntry                                     */
/*                                                                           */
/* Description  : Releases memory and Removes PDist Control Entry from       */
/*                ProtocolDistControlRBTree                                  */
/*                                                                           */
/* Input        : pPDistCtrl                                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rmon2PDistDelCtrlEntry (tProtocolDistControl * pPDistCtrl)
{
    tPortList          *pVlanPortList = NULL;
    tCfaIfInfo          IfInfo;
    UINT2               u2VlanId = 0;

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
    pVlanPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pVlanPortList == NULL)
    {
        RMON2_TRC (RMON2_FN_ENTRY,
                   "Rmon2PDistDelCtrlEntry: "
                   "Error in allocating memory for pVlanPortList\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pVlanPortList, 0, sizeof (tPortList));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PDistDelCtrlEntry\r\n");
    if (CfaGetIfInfo (pPDistCtrl->u4ProtocolDistControlDataSource,
                      &IfInfo) != CFA_FAILURE)
    {
        if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
            (CfaGetVlanId (pPDistCtrl->u4ProtocolDistControlDataSource,
                           &u2VlanId) != CFA_FAILURE))
        {
            L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList);
        }
    }

#ifdef NPAPI_WANTED
    /* Remove Probe support from H/W Table */
    Rmonv2FsRMONv2DisableProbe (pPDistCtrl->u4ProtocolDistControlDataSource,
                                u2VlanId, *pVlanPortList);
#endif

    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
    /* Remove PDist Ctrl node from ProtocolDistControlRBTree */
    RBTreeRem (RMON2_PDISTCTRL_TREE, (tRBElem *) pPDistCtrl);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (RMON2_PDISTCTRL_POOL,
                            (UINT1 *) pPDistCtrl) != MEM_SUCCESS)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for ProtocolDistControlEntry\r\n");
        return OSIX_FAILURE;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2PDistDelCtrlEntry\r\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDistGetCtrlEntry                                     */
/*                                                                           */
/* Description  : Get PDist Ctrl Entry for the given control index           */
/*                                                                           */
/* Input        : u4ProtocolDistControlIndex                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tProtocolDistControl / NULL Pointer                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC tProtocolDistControl *
Rmon2PDistGetCtrlEntry (UINT4 u4ProtocolDistControlIndex)
{
    tProtocolDistControl PDistCtrl;
    MEMSET (&PDistCtrl, 0, sizeof (tProtocolDistControl));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PDistGetCtrlEntry\r\n");

    PDistCtrl.u4ProtocolDistControlIndex = u4ProtocolDistControlIndex;
    return ((tProtocolDistControl *) RBTreeGet (RMON2_PDISTCTRL_TREE,
                                                (tRBElem *) & PDistCtrl));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDistGetNextDataIndex                                 */
/*                                                                           */
/* Description  : Get First / Next PDist Stats Index                         */
/*                                                                           */
/* Input        : u4ProtocolDistControlIndex, u4ProtocolDirLocalIndex        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tProtocolDistStats / NULL Pointer                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC tProtocolDistStats *
Rmon2PDistGetNextDataIndex (UINT4 u4ProtocolDistControlIndex,
                            UINT4 u4ProtocolDirLocalIndex)
{
    tProtocolDistStats  PDistStats, *pPDistStats = NULL;
    MEMSET (&PDistStats, 0, sizeof (tProtocolDistStats));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PDistGetNextDataIndex\r\n");
    if ((u4ProtocolDistControlIndex != 0) && (u4ProtocolDirLocalIndex == 0))
    {
        pPDistStats =
            (tProtocolDistStats *) RBTreeGetFirst (RMON2_PDISTSTATS_TREE);
        while (pPDistStats != NULL)
        {
            if (pPDistStats->u4ProtocolDistControlIndex ==
                u4ProtocolDistControlIndex)
            {
                break;
            }
            MEMCPY (&PDistStats, pPDistStats, sizeof (tProtocolDistStats));
            pPDistStats =
                ((tProtocolDistStats *) RBTreeGetNext (RMON2_PDISTSTATS_TREE,
                                                       (tRBElem *) & PDistStats,
                                                       NULL));
        }
    }
    else
    {

        PDistStats.u4ProtocolDistControlIndex = u4ProtocolDistControlIndex;
        PDistStats.u4ProtocolDirLocalIndex = u4ProtocolDirLocalIndex;
        pPDistStats =
            ((tProtocolDistStats *) RBTreeGetNext (RMON2_PDISTSTATS_TREE,
                                                   (tRBElem *) & PDistStats,
                                                   NULL));
    }
    return pPDistStats;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDistAddDataEntry                                     */
/*                                                                           */
/* Description  : Allocates memory and Adds PDist Stats Entry into           */
/*                ProtocolDistStatsRBTree                                    */
/*                                                                           */
/* Input        : u4ProtocolDistControlIndex, u4ProtocolDirLocalIndex        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tProtocolDistStats / NULL Pointer                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC tProtocolDistStats *
Rmon2PDistAddDataEntry (UINT4 u4ProtocolDistControlIndex,
                        UINT4 u4ProtocolDirLocalIndex)
{
    tProtocolDistStats *pPDist = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PDistAddDataEntry\r\n");

    if ((pPDist = (tProtocolDistStats *)
         (MemAllocMemBlk (RMON2_PDISTSTATS_POOL))) == NULL)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for ProtocolDistStatsEntry\r\n");
        return NULL;
    }

    MEMSET (pPDist, 0, sizeof (tProtocolDistStats));

    pPDist->u4ProtocolDistControlIndex = u4ProtocolDistControlIndex;
    pPDist->u4ProtocolDirLocalIndex = u4ProtocolDirLocalIndex;

    if (RBTreeAdd (RMON2_PDISTSTATS_TREE, (tRBElem *) pPDist) == RB_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Rmon2PDistAddDataEntry: ProtocolDistStatsEntry is added\r\n");
        return pPDist;
    }
    RMON2_TRC (RMON2_CRITICAL_TRC,
               "Rmon2PDistAddDataEntry: Adding ProtocolDistStatsEntry Failed\r\n");
    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (RMON2_PDISTSTATS_POOL, (UINT1 *) pPDist);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2PDistAddDataEntry\r\n");

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDistDelDataEntry                                     */
/*                                                                           */
/* Description  : Releases memory and Removes PDist Stats Entry from         */
/*                ProtocolDistStatsRBTree                                    */
/*                                                                           */
/* Input        : pPDist                                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rmon2PDistDelDataEntry (tProtocolDistStats * pPDist)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PDistDelDataEntry\r\n");
    /* Remove PDist Stats node from ProtocolDistStatsRBTree */
    RBTreeRem (RMON2_PDISTSTATS_TREE, (tRBElem *) pPDist);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (RMON2_PDISTSTATS_POOL,
                            (UINT1 *) pPDist) != MEM_SUCCESS)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for ProtocolDistStatsEntry\r\n");
        return OSIX_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2PDistDelDataEntry\r\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDistGetDataEntry                                     */
/*                                                                           */
/* Description  : Get PDist Stats Entry for the given index                  */
/*                                                                           */
/* Input        : u4ProtocolDistControlIndex, u4ProtocolDirLocalIndex        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tProtocolDistStats / NULL Pointer                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC tProtocolDistStats *
Rmon2PDistGetDataEntry (UINT4 u4ProtocolDistControlIndex,
                        UINT4 u4ProtocolDirLocalIndex)
{
    tProtocolDistStats  PDistStats;
    MEMSET (&PDistStats, 0, sizeof (tProtocolDistStats));
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PDistGetDataEntry\r\n");

    PDistStats.u4ProtocolDistControlIndex = u4ProtocolDistControlIndex;
    PDistStats.u4ProtocolDirLocalIndex = u4ProtocolDirLocalIndex;
    return ((tProtocolDistStats *) RBTreeGet (RMON2_PDISTSTATS_TREE,
                                              (tRBElem *) & PDistStats));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDistDelInterDep                                      */
/*                                                                           */
/* Description  : Removes the inter-dependencies and re-order the links      */
/*                                                                           */
/* Input        : pPDist                                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2PDistDelInterDep (tProtocolDistStats * pPDist)
{
    tRmon2PktInfo      *pPktInfo = NULL;
    tPktHeader          PktHdr;

    MEMSET (&PktHdr, 0, sizeof (tPktHeader));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PDistDelInterDep\r\n");

    if (pPDist->pPrevPDist == NULL)
    {
        /* Update PktInfo nodes to point next pPDist Entry */
        pPktInfo = Rmon2PktInfoGetNextIndex (&PktHdr);

        while (pPktInfo != NULL)
        {
            if (pPktInfo->pPDist == pPDist)
            {
                pPktInfo->pPDist = pPDist->pNextPDist;
                if (pPktInfo->pPDist != NULL)
                {
                    pPktInfo->pPDist->pPrevPDist = NULL;
                }
            }
            pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
        }
    }
    else if (pPDist->pNextPDist != NULL)
    {
        pPDist->pPrevPDist->pNextPDist = pPDist->pNextPDist;
    }
    else
    {
        pPDist->pPrevPDist->pNextPDist = NULL;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2PDistDelInterDep\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDistPopulateDataEntries                              */
/*                                                                           */
/* Description  : Populates data entries if the same data source is already  */
/*                present and Adds the inter-dependencies b/w other tables   */
/*                                                                           */
/* Input        : pPDistCtrl                                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2PDistPopulateDataEntries (tProtocolDistControl * pPDistCtrl)
{
    tRmon2PktInfo      *pPktInfo = NULL;
    tProtocolDistStats *pPDist = NULL;
    tPktHeader          PktHdr;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PDistPopulateDataEntries\r\n");

    MEMSET (&PktHdr, 0, sizeof (tPktHeader));

    /* Check for the presence of data source in PktInfo table */
    pPktInfo = Rmon2PktInfoGetNextIndex (&PktHdr);

    while (pPktInfo != NULL)
    {
        if (pPktInfo->PktHdr.u4IfIndex ==
            pPDistCtrl->u4ProtocolDistControlDataSource)
        {

            /* Add PDistStats Entry for Nl Protocol */
            if (pPktInfo->u4ProtoNLIndex != 0)
            {
                pPDist =
                    Rmon2PDistAddDataEntry (pPDistCtrl->
                                            u4ProtocolDistControlIndex,
                                            pPktInfo->u4ProtoNLIndex);

                if (pPDist == NULL)
                {
                    pPDistCtrl->u4ProtocolDistControlDroppedFrames++;
                    pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
                    continue;
                }
                pPDist->pPDistCtrl = pPDistCtrl;
            }

            /* Add PDistStats Entry for L4 Al Protocol */
            if (pPktInfo->u4ProtoL4ALIndex != 0)
            {
                pPDist =
                    Rmon2PDistAddDataEntry (pPDistCtrl->
                                            u4ProtocolDistControlIndex,
                                            pPktInfo->u4ProtoL4ALIndex);

                if (pPDist == NULL)
                {

                    pPDistCtrl->u4ProtocolDistControlDroppedFrames++;
                    pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
                    continue;
                }
                pPDist->pPDistCtrl = pPDistCtrl;
                pPDist->u4PktRefCount++;
            }

            /* Add PDistStats Entry for L5 Al Protocol */
            if (pPktInfo->u4ProtoL5ALIndex != 0)
            {
                pPDist =
                    Rmon2PDistAddDataEntry (pPDistCtrl->
                                            u4ProtocolDistControlIndex,
                                            pPktInfo->u4ProtoL5ALIndex);

                if (pPDist == NULL)
                {
                    pPDistCtrl->u4ProtocolDistControlDroppedFrames++;
                    pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
                    continue;
                }
                pPDist->pPDistCtrl = pPDistCtrl;
                pPDist->u4PktRefCount++;
            }

        }                        /* End of if check */
        pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);

    }                            /* End of While - PktInfo */
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2PDistPopulateDataEntries\r\n");
    return;
}
