/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * $Id: rmn2amlw.c,v 1.13 2014/02/05 12:28:59 siva Exp $
 *
 * Description: This file contains the low level nmh routines for
 *              RMON2 Address Map module.
 *
 *******************************************************************/
#include "rmn2inc.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetAddressMapInserts
 Input       :  The Indices

                The Object
                retValAddressMapInserts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAddressMapInserts (UINT4 *pu4RetValAddressMapInserts)
{
    *pu4RetValAddressMapInserts = gRmon2Globals.u4AddressMapInserts;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetAddressMapDeletes
 Input       :  The Indices

                The Object
                retValAddressMapDeletes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAddressMapDeletes (UINT4 *pu4RetValAddressMapDeletes)
{
    *pu4RetValAddressMapDeletes = gRmon2Globals.u4AddressMapDeletes;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetAddressMapMaxDesiredEntries
 Input       :  The Indices

                The Object
                retValAddressMapMaxDesiredEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAddressMapMaxDesiredEntries (INT4 *pi4RetValAddressMapMaxDesiredEntries)
{
    *pi4RetValAddressMapMaxDesiredEntries =
        gRmon2Globals.i4AddressMapMaxDesiredEntries;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetAddressMapMaxDesiredEntries
 Input       :  The Indices

                The Object
                setValAddressMapMaxDesiredEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetAddressMapMaxDesiredEntries (INT4 i4SetValAddressMapMaxDesiredEntries)
{
    UINT4               u4Minimum = 0;
    INT4                i4Count = 0;
    UINT4               u4Result = 0;
    UINT4               u4EntryCount = 0;
    tAddressMap        *pAddrMapLRUEntry = NULL;

    u4Minimum = (UINT4) ((i4SetValAddressMapMaxDesiredEntries != -1) ?
                         ((i4SetValAddressMapMaxDesiredEntries <
                           RMON2_MAX_ADDRMAP_ENTRY) ?
                          i4SetValAddressMapMaxDesiredEntries :
                          RMON2_MAX_ADDRMAP_ENTRY) : RMON2_MAX_ADDRMAP_ENTRY);

    i4Count =
        (INT4) ((UINT4) gRmon2Globals.i4AddressMapMaxDesiredEntries -
                u4Minimum);

    if (i4Count > 0)
    {
        u4Result = RBTreeCount (RMON2_ADDRMAP_TREE, &u4EntryCount);
        if ((u4Result == RB_SUCCESS) && (u4EntryCount != 0))
        {
            i4Count = (INT4) (u4EntryCount - u4Minimum);
            if (i4Count > 0)
            {
                while (i4Count != 0)
                {
                    pAddrMapLRUEntry = Rmon2AddrMapGiveLRUEntry ();
                    if (pAddrMapLRUEntry != NULL)
                    {
                        Rmon2AddrMapDelInterDep (pAddrMapLRUEntry);
                        gRmon2Globals.u4AddressMapDeletes++;
                        Rmon2AddrMapDelDataEntry (pAddrMapLRUEntry);
                        i4Count--;
                    }
                    else
                    {
                        break;
                    }
                }
            }

        }
    }

    gRmon2Globals.i4AddressMapMaxDesiredEntries =
        i4SetValAddressMapMaxDesiredEntries;
    gRmon2Globals.u4AddressMapMaxDesiredSupported = u4Minimum;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2AddressMapMaxDesiredEntries
 Input       :  The Indices

                The Object
                testValAddressMapMaxDesiredEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2AddressMapMaxDesiredEntries (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValAddressMapMaxDesiredEntries)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2AddressMapMaxDesiredEntries \n");

    /* Testing the input for less than negative value alone is sufficient, because
     * RMON2 allows this configuration till the maximum value of interger 32 */
    if (i4TestValAddressMapMaxDesiredEntries < -1)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Error: Invalid AddressMapMaxDesiredEntries\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhTestv2AddressMapMaxDesiredEntries \n");
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2AddressMapMaxDesiredEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2AddressMapMaxDesiredEntries (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : AddressMapControlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceAddressMapControlTable
 Input       :  The Indices
                AddressMapControlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceAddressMapControlTable (INT4 i4AddressMapControlIndex)
{
    tAddressMapControl *pAddrMapCtrlEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "nmhValidateIndexInstanceAddressMapControlTable \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4AddressMapControlIndex < RMON2_ONE) ||
        (i4AddressMapControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid AddressMap Control Index \n");
        return SNMP_FAILURE;
    }
    pAddrMapCtrlEntry =
        Rmon2AddrMapGetCtrlEntry ((UINT4) i4AddressMapControlIndex);

    if (pAddrMapCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AddressMap Control table"
                    " for index %d is failed \r\n", i4AddressMapControlIndex);
        return SNMP_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "nmhValidateIndexInstanceAddressMapControlTable \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexAddressMapControlTable
 Input       :  The Indices
                AddressMapControlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexAddressMapControlTable (INT4 *pi4AddressMapControlIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetFirstIndexAddressMapControlTable \n");

    return nmhGetNextIndexAddressMapControlTable (RMON2_ZERO,
                                                  pi4AddressMapControlIndex);

}

/****************************************************************************
 Function    :  nmhGetNextIndexAddressMapControlTable
 Input       :  The Indices
                AddressMapControlIndex
                nextAddressMapControlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexAddressMapControlTable (INT4 i4AddressMapControlIndex,
                                       INT4 *pi4NextAddressMapControlIndex)
{
    tAddressMapControl *pAddrMapCtrlEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetNextIndexAddressMapControlTable \n");

    pAddrMapCtrlEntry =
        Rmon2AddrMapGetNextCtrlIndex ((UINT4) i4AddressMapControlIndex);

    if (pAddrMapCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AddressMap Control table"
                    " for index %d is failed \r\n", i4AddressMapControlIndex);
        return SNMP_FAILURE;
    }
    *pi4NextAddressMapControlIndex =
        (INT4) pAddrMapCtrlEntry->u4AddressMapControlIndex;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT  nmhGetNextIndexAddressMapControlTable \n");
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetAddressMapControlDataSource
 Input       :  The Indices
                AddressMapControlIndex

                The Object 
                retValAddressMapControlDataSource
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAddressMapControlDataSource (INT4 i4AddressMapControlIndex,
                                   tSNMP_OID_TYPE *
                                   pRetValAddressMapControlDataSource)
{

    tAddressMapControl *pAddrMapCtrlEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetAddressMapControlDataSource \n");

    pAddrMapCtrlEntry =
        Rmon2AddrMapGetCtrlEntry ((UINT4) i4AddressMapControlIndex);

    if (pAddrMapCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AddressMap Control table"
                    " for index %d is failed \r\n", i4AddressMapControlIndex);
        return SNMP_FAILURE;
    }

    RMON2_GET_DATA_SOURCE_OID (pRetValAddressMapControlDataSource,
                               pAddrMapCtrlEntry->
                               u4AddressMapControlDataSource);
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT  nmhGetAddressMapControlDataSource \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetAddressMapControlDroppedFrames
 Input       :  The Indices
                AddressMapControlIndex

                The Object 
                retValAddressMapControlDroppedFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAddressMapControlDroppedFrames (INT4 i4AddressMapControlIndex,
                                      UINT4
                                      *pu4RetValAddressMapControlDroppedFrames)
{
    tAddressMapControl *pAddrMapCtrlEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetAddressMapControlDroppedFrames \n");

    pAddrMapCtrlEntry =
        Rmon2AddrMapGetCtrlEntry ((UINT4) i4AddressMapControlIndex);

    if (pAddrMapCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AddressMap Control table"
                    " for index %d is failed \r\n", i4AddressMapControlIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValAddressMapControlDroppedFrames =
        pAddrMapCtrlEntry->u4AddressMapControlDroppedFrames;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT  nmhGetAddressMapControlDroppedFrames \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetAddressMapControlOwner
 Input       :  The Indices
                AddressMapControlIndex

                The Object 
                retValAddressMapControlOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAddressMapControlOwner (INT4 i4AddressMapControlIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValAddressMapControlOwner)
{
    tAddressMapControl *pAddrMapCtrlEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetAddressMapControlOwner \n");

    pAddrMapCtrlEntry =
        Rmon2AddrMapGetCtrlEntry ((UINT4) i4AddressMapControlIndex);

    if (pAddrMapCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AddressMap Control table"
                    " for index %d is failed \r\n", i4AddressMapControlIndex);
        return SNMP_FAILURE;
    }

    pRetValAddressMapControlOwner->i4_Length =
        (INT4) STRLEN (pAddrMapCtrlEntry->au1AddressMapControlOwner);
    MEMCPY (pRetValAddressMapControlOwner->pu1_OctetList,
            pAddrMapCtrlEntry->au1AddressMapControlOwner,
            pRetValAddressMapControlOwner->i4_Length);

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT  nmhGetAddressMapControlOwner \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetAddressMapControlStatus
 Input       :  The Indices
                AddressMapControlIndex

                The Object 
                retValAddressMapControlStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAddressMapControlStatus (INT4 i4AddressMapControlIndex,
                               INT4 *pi4RetValAddressMapControlStatus)
{
    tAddressMapControl *pAddrMapCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetAddressMapControlStatus \n");

    pAddrMapCtrlEntry =
        Rmon2AddrMapGetCtrlEntry ((UINT4) i4AddressMapControlIndex);

    if (pAddrMapCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AddressMap Control table"
                    " for index %d is failed \r\n", i4AddressMapControlIndex);
        return SNMP_FAILURE;
    }

    *pi4RetValAddressMapControlStatus =
        (INT4) pAddrMapCtrlEntry->u4AddressMapControlStatus;

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT  nmhGetAddressMapControlStatus \n");
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetAddressMapControlDataSource
 Input       :  The Indices
                AddressMapControlIndex

                The Object 
                setValAddressMapControlDataSource
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetAddressMapControlDataSource (INT4 i4AddressMapControlIndex,
                                   tSNMP_OID_TYPE *
                                   pSetValAddressMapControlDataSource)
{
    tAddressMapControl *pAddrMapCtrlEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhSetAddressMapControlDataSource \n");

    pAddrMapCtrlEntry =
        Rmon2AddrMapGetCtrlEntry ((UINT4) i4AddressMapControlIndex);

    if (pAddrMapCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AddressMap Control table"
                    " for index %d is failed \r\n", i4AddressMapControlIndex);
        return SNMP_FAILURE;
    }

    RMON2_SET_DATA_SOURCE_OID (pSetValAddressMapControlDataSource,
                               &(pAddrMapCtrlEntry->
                                 u4AddressMapControlDataSource));
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT  nmhSetAddressMapControlDataSource \n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetAddressMapControlOwner
 Input       :  The Indices
                AddressMapControlIndex

                The Object 
                setValAddressMapControlOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetAddressMapControlOwner (INT4 i4AddressMapControlIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValAddressMapControlOwner)
{
    UINT4               u4Minimum;
    tAddressMapControl *pAddrMapCtrlEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhSetAddressMapControlOwner \n");

    u4Minimum =
        (UINT4) ((pSetValAddressMapControlOwner->i4_Length <
                  RMON2_OWNER_LEN) ? pSetValAddressMapControlOwner->
                 i4_Length : RMON2_OWNER_LEN);

    pAddrMapCtrlEntry =
        Rmon2AddrMapGetCtrlEntry ((UINT4) i4AddressMapControlIndex);

    if (pAddrMapCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AddressMap Control table"
                    " for index %d is failed \r\n", i4AddressMapControlIndex);
        return SNMP_FAILURE;
    }

    MEMCPY (pAddrMapCtrlEntry->au1AddressMapControlOwner,
            pSetValAddressMapControlOwner->pu1_OctetList, u4Minimum);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT  nmhSetAddressMapControlOwner \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetAddressMapControlStatus
 Input       :  The Indices
                AddressMapControlIndex

                The Object 
                setValAddressMapControlStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetAddressMapControlStatus (INT4 i4AddressMapControlIndex,
                               INT4 i4SetValAddressMapControlStatus)
{
    tPortList          *pVlanPortList = NULL;
    tAddressMapControl *pAddrMapCtrlEntry = NULL;
    INT4                i4Result;
    UINT2               u2VlanId = 0;
    tCfaIfInfo          IfInfo;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhSetAddressMapControlStatus \n");

    switch (i4SetValAddressMapControlStatus)
    {
        case CREATE_AND_WAIT:

            pAddrMapCtrlEntry =
                Rmon2AddrMapAddCtrlEntry ((UINT4) i4AddressMapControlIndex);
            if (pAddrMapCtrlEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC | RMON2_MEM_FAIL,
                           "Adding to AddressMap control "
                           "table entry is failed \n");
                return SNMP_FAILURE;
            }
            pAddrMapCtrlEntry->u4AddressMapControlStatus = NOT_READY;
            break;

        case ACTIVE:

            pAddrMapCtrlEntry =
                Rmon2AddrMapGetCtrlEntry ((UINT4) i4AddressMapControlIndex);
            if (pAddrMapCtrlEntry == NULL)
            {
                RMON2_TRC1 (RMON2_DEBUG_TRC,
                            "Get entry of AddressMap Control table"
                            " for index %d is failed \r\n",
                            i4AddressMapControlIndex);
                return SNMP_FAILURE;
            }
            if (pAddrMapCtrlEntry->u4AddressMapControlDataSource == RMON2_ZERO)
            {
                RMON2_TRC (RMON2_DEBUG_TRC, "Data Source is not configured \n");
                return SNMP_FAILURE;
            }
            if (CfaGetIfInfo
                (pAddrMapCtrlEntry->u4AddressMapControlDataSource,
                 &IfInfo) == CFA_FAILURE)
            {
                /* ifindex is invalid */
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "SNMP Failure: Invalid interface Index \n");
                return SNMP_FAILURE;
            }

            if (((IfInfo.u1IfOperStatus != CFA_IF_UP)
                 || (IfInfo.u1IfAdminStatus != CFA_IF_UP)) &&
                ((gi4MibResStatus != MIB_RESTORE_IN_PROGRESS)))
            {
                RMON2_TRC (RMON2_DEBUG_TRC, "SNMP Failure: "
                           "Interface OperStatus or Admin Status is in "
                           "Down state \n");
                return SNMP_FAILURE;
            }
            pVlanPortList =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pVlanPortList == NULL)
            {
                RMON2_TRC (RMON2_FN_ENTRY,
                           "nmhSetAddressMapControlStatus: "
                           "Error in allocating memory for pVlanPortList\r\n");
                return SNMP_FAILURE;
            }
            MEMSET (pVlanPortList, 0, sizeof (tPortList));
            if (IfInfo.u1IfType == CFA_L3IPVLAN)
            {
                /* Get the VlanId from interface index */
                if (CfaGetVlanId
                    (pAddrMapCtrlEntry->u4AddressMapControlDataSource,
                     &u2VlanId) == CFA_FAILURE)
                {
                    RMON2_TRC1 (RMON2_DEBUG_TRC, "SNMP Failure: \
                 Unable to Fetch VlanId of IfIndex %d\n", pAddrMapCtrlEntry->u4AddressMapControlDataSource);
                    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                    return SNMP_FAILURE;
                }

                if (L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList) ==
                    L2IWF_FAILURE)
                {
                    RMON2_TRC1 (RMON2_DEBUG_TRC, "SNMP Failure: \
                 Get Member Port List for VlanId %d failed \n", u2VlanId);
                    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                    return SNMP_FAILURE;
                }
            }

            pAddrMapCtrlEntry->u4AddressMapControlStatus = ACTIVE;

#ifdef NPAPI_WANTED
            Rmonv2FsRMONv2EnableProbe (pAddrMapCtrlEntry->
                                       u4AddressMapControlDataSource, u2VlanId,
                                       *pVlanPortList);
#endif
            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
            Rmon2AddrMapPopulateDataEntries (pAddrMapCtrlEntry);
            Rmon2PktInfoDelUnusedEntries ();

            break;

        case NOT_IN_SERVICE:

            pAddrMapCtrlEntry =
                Rmon2AddrMapGetCtrlEntry ((UINT4) i4AddressMapControlIndex);
            if (pAddrMapCtrlEntry == NULL)
            {
                RMON2_TRC1 (RMON2_DEBUG_TRC,
                            "Get entry of AddressMap Control table"
                            " for index %d is failed \r\n",
                            i4AddressMapControlIndex);
                return SNMP_FAILURE;
            }

            pAddrMapCtrlEntry->u4AddressMapControlStatus = NOT_IN_SERVICE;

            Rmon2AddrMapDelDataEntries (pAddrMapCtrlEntry->
                                        u4AddressMapControlDataSource);
            pVlanPortList =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pVlanPortList == NULL)
            {
                RMON2_TRC (RMON2_FN_ENTRY,
                           "nmhSetAddressMapControlStatus: "
                           "Error in allocating memory for pVlanPortList\r\n");
                return SNMP_FAILURE;
            }
            MEMSET (pVlanPortList, 0, sizeof (tPortList));
            if (CfaGetIfInfo
                (pAddrMapCtrlEntry->u4AddressMapControlDataSource,
                 &IfInfo) != CFA_FAILURE)
            {
                if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
                    (CfaGetVlanId
                     (pAddrMapCtrlEntry->u4AddressMapControlDataSource,
                      &u2VlanId) != CFA_FAILURE))
                {
                    L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList);
                }
            }

            Rmon2PktInfoDelUnusedEntries ();
#ifdef NPAPI_WANTED
            Rmonv2FsRMONv2DisableProbe (pAddrMapCtrlEntry->
                                        u4AddressMapControlDataSource, u2VlanId,
                                        *pVlanPortList);
#endif
            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
            break;

        case DESTROY:

            pAddrMapCtrlEntry =
                Rmon2AddrMapGetCtrlEntry ((UINT4) i4AddressMapControlIndex);
            if (pAddrMapCtrlEntry == NULL)
            {
                RMON2_TRC1 (RMON2_DEBUG_TRC,
                            "Get entry of AddressMap Control table"
                            " for index %d is failed \r\n",
                            i4AddressMapControlIndex);
                return SNMP_FAILURE;
            }

            Rmon2AddrMapDelDataEntries (pAddrMapCtrlEntry->
                                        u4AddressMapControlDataSource);
            pVlanPortList =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pVlanPortList == NULL)
            {
                RMON2_TRC (RMON2_FN_ENTRY,
                           "nmhSetAddressMapControlStatus: "
                           "Error in allocating memory for pVlanPortList\r\n");
                return SNMP_FAILURE;
            }
            MEMSET (pVlanPortList, 0, sizeof (tPortList));
            if (CfaGetIfInfo
                (pAddrMapCtrlEntry->u4AddressMapControlDataSource,
                 &IfInfo) != CFA_FAILURE)
            {
                if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
                    (CfaGetVlanId
                     (pAddrMapCtrlEntry->u4AddressMapControlDataSource,
                      &u2VlanId) != CFA_FAILURE))
                {
                    L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList);
                }
            }

#ifdef NPAPI_WANTED
            Rmonv2FsRMONv2DisableProbe (pAddrMapCtrlEntry->
                                        u4AddressMapControlDataSource, u2VlanId,
                                        *pVlanPortList);
#endif
            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
            i4Result = Rmon2AddrMapDelCtrlEntry (pAddrMapCtrlEntry);
            if (i4Result == OSIX_FAILURE)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "RBTreeRemove Failure for AddressMap control "
                           "entry\n");
                return SNMP_FAILURE;
            }

            Rmon2PktInfoDelUnusedEntries ();
            break;
        default:
            RMON2_TRC (RMON2_DEBUG_TRC, "Unknown option\n");
            return SNMP_FAILURE;
            break;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT  nmhSetAddressMapControlStatus \n");
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2AddressMapControlDataSource
 Input       :  The Indices
                AddressMapControlIndex

                The Object 
                testValAddressMapControlDataSource
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2AddressMapControlDataSource (UINT4 *pu4ErrorCode,
                                      INT4 i4AddressMapControlIndex,
                                      tSNMP_OID_TYPE *
                                      pTestValAddressMapControlDataSource)
{
    tAddressMapControl *pAddrMapCtrlEntry = NULL;
    UINT4               u4DataSource = RMON2_ZERO;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2AddressMapControlDataSource \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4AddressMapControlIndex < RMON2_ONE) ||
        (i4AddressMapControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid AddressMap Control Index \n");
        return SNMP_FAILURE;
    }

    /* Checking whether the data source is valid or not */
    if (RMON2_TEST_DATA_SOURCE_OID (pTestValAddressMapControlDataSource))
    {
        RMON2_SET_DATA_SOURCE_OID (pTestValAddressMapControlDataSource,
                                   &u4DataSource);

        pAddrMapCtrlEntry =
            Rmon2AddrMapGetCtrlEntry ((UINT4) i4AddressMapControlIndex);

        /* Checking whether the Control entry exists or not */
        if (pAddrMapCtrlEntry != NULL)
        {
            /* Checking whether the Status is not ACTIVE */
            if (pAddrMapCtrlEntry->u4AddressMapControlStatus != ACTIVE)
            {
                RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                           "SNMP Success: Data Source id is valid. \n");
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }

            RMON2_TRC1 (RMON2_DEBUG_TRC,
                        "Row Status of Address Map  Control table is "
                        "Active for index : %d \r\n", i4AddressMapControlIndex);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "Error - Entry is not exist \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;

    }
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: INVALID DataSource Oid \n");
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT  nmhTestv2AddressMapControlDataSource \n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2AddressMapControlOwner
 Input       :  The Indices
                AddressMapControlIndex

                The Object 
                testValAddressMapControlOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2AddressMapControlOwner (UINT4 *pu4ErrorCode,
                                 INT4 i4AddressMapControlIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValAddressMapControlOwner)
{
    tAddressMapControl *pAddrMapCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2AddressMapControlOwner \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4AddressMapControlIndex < RMON2_ONE) ||
        (i4AddressMapControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid AddressMap Control Index \n");
        return SNMP_FAILURE;
    }

    if ((pTestValAddressMapControlOwner->i4_Length < RMON2_ZERO) ||
        (pTestValAddressMapControlOwner->i4_Length > RMON2_OWNER_LEN))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Wrong length for AddressMapControlOwner "
                   "object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValAddressMapControlOwner->pu1_OctetList,
                              pTestValAddressMapControlOwner->i4_Length)
        == OSIX_FAILURE)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Invalid Characters for Owner string object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    pAddrMapCtrlEntry =
        Rmon2AddrMapGetCtrlEntry ((UINT4) i4AddressMapControlIndex);

    if (pAddrMapCtrlEntry != NULL)
    {
        if (pAddrMapCtrlEntry->u4AddressMapControlStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of Address Map  Control table is Active "
                    "for index : %d \r\n", i4AddressMapControlIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry is not exist \n");
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT  nmhTestv2AddressMapControlOwner \n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2AddressMapControlStatus
 Input       :  The Indices
                AddressMapControlIndex

                The Object 
                testValAddressMapControlStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2AddressMapControlStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4AddressMapControlIndex,
                                  INT4 i4TestValAddressMapControlStatus)
{
    tAddressMapControl *pAddrMapCtrlEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2AddressMapControlStatus \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4AddressMapControlIndex < RMON2_ONE) ||
        (i4AddressMapControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid AddressMap Control Index \n");
        return SNMP_FAILURE;
    }

    if ((i4TestValAddressMapControlStatus < ACTIVE) ||
        (i4TestValAddressMapControlStatus > DESTROY))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Invalid value for AddressMapControlStatus "
                   "object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pAddrMapCtrlEntry =
        Rmon2AddrMapGetCtrlEntry ((UINT4) i4AddressMapControlIndex);

    if (pAddrMapCtrlEntry == NULL)
    {
        if (i4TestValAddressMapControlStatus == CREATE_AND_WAIT)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "SNMP Failure: Entry Status is not exist \n");
        return SNMP_FAILURE;
    }
    else
    {
        if ((i4TestValAddressMapControlStatus == ACTIVE) ||
            (i4TestValAddressMapControlStatus == NOT_IN_SERVICE) ||
            (i4TestValAddressMapControlStatus == DESTROY))
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "The given entry is already exist \n");
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhTestv2AddressMapControlStatus \n");
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2AddressMapControlTable
 Input       :  The Indices
                AddressMapControlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2AddressMapControlTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : AddressMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceAddressMapTable
 Input       :  The Indices
                AddressMapTimeMark
                ProtocolDirLocalIndex
                AddressMapNetworkAddress
                AddressMapSource
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceAddressMapTable (UINT4 u4AddressMapTimeMark,
                                         INT4 i4ProtocolDirLocalIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pAddressMapNetworkAddress,
                                         tSNMP_OID_TYPE * pAddressMapSource)
{
    tAddressMap        *pAddrMapEntry = NULL;
    tIpAddr             AddressMapNetworkAddress;
    UINT4               u4AddressMapSource = 0;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "nmhValidateIndexInstanceAddressMapTable \n");

    UNUSED_PARAM (u4AddressMapTimeMark);

    MEMSET (&AddressMapNetworkAddress, 0, sizeof (tIpAddr));

    if (pAddressMapNetworkAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&AddressMapNetworkAddress,
                pAddressMapNetworkAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&AddressMapNetworkAddress.u4_addr[3],
                pAddressMapNetworkAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }
    AddressMapNetworkAddress.u4_addr[0] =
        OSIX_NTOHL (AddressMapNetworkAddress.u4_addr[0]);
    AddressMapNetworkAddress.u4_addr[1] =
        OSIX_NTOHL (AddressMapNetworkAddress.u4_addr[1]);
    AddressMapNetworkAddress.u4_addr[2] =
        OSIX_NTOHL (AddressMapNetworkAddress.u4_addr[2]);
    AddressMapNetworkAddress.u4_addr[3] =
        OSIX_NTOHL (AddressMapNetworkAddress.u4_addr[3]);

    RMON2_SET_DATA_SOURCE_OID (pAddressMapSource, &u4AddressMapSource);

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    /* Validating the input for negative value alone is sufficient, because
     * RMON2 allows this configuration till the maximum value of interger 32 */
    if (i4ProtocolDirLocalIndex < RMON2_ONE)
    {
        RMON2_TRC (RMON2_DEBUG_TRC, "SNMP Failure: Invalid "
                   "ProtocolDirLocalIndex \n");
        return SNMP_FAILURE;
    }
    pAddrMapEntry =
        Rmon2AddrMapGetDataEntry ((UINT4) i4ProtocolDirLocalIndex,
                                  &AddressMapNetworkAddress,
                                  u4AddressMapSource);

    if (pAddrMapEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of Address Map table is failed \r\n");
        return SNMP_FAILURE;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "nmhValidateIndexInstanceAddressMapTable \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexAddressMapTable
 Input       :  The Indices
                AddressMapTimeMark
                ProtocolDirLocalIndex
                AddressMapNetworkAddress
                AddressMapSource
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexAddressMapTable (UINT4 *pu4AddressMapTimeMark,
                                 INT4 *pi4ProtocolDirLocalIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pAddressMapNetworkAddress,
                                 tSNMP_OID_TYPE * pAddressMapSource)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetFirstIndexAddressMapTable \n");

    return nmhGetNextIndexAddressMapTable (RMON2_ZERO, pu4AddressMapTimeMark,
                                           RMON2_ZERO, pi4ProtocolDirLocalIndex,
                                           RMON2_ZERO,
                                           pAddressMapNetworkAddress,
                                           RMON2_ZERO, pAddressMapSource);

}

/****************************************************************************
 Function    :  nmhGetNextIndexAddressMapTable
 Input       :  The Indices
                AddressMapTimeMark
                nextAddressMapTimeMark
                ProtocolDirLocalIndex
                nextProtocolDirLocalIndex
                AddressMapNetworkAddress
                nextAddressMapNetworkAddress
                AddressMapSource
                nextAddressMapSource
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexAddressMapTable (UINT4 u4AddressMapTimeMark,
                                UINT4 *pu4NextAddressMapTimeMark,
                                INT4 i4ProtocolDirLocalIndex,
                                INT4 *pi4NextProtocolDirLocalIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pAddressMapNetworkAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pNextAddressMapNetworkAddress,
                                tSNMP_OID_TYPE * pAddressMapSource,
                                tSNMP_OID_TYPE * pNextAddressMapSource)
{
    tAddressMap        *pAddrMapEntry = NULL;
    tIpAddr             AddressMapNetworkAddress;
    UINT4               u4AddressMapSource;

    UNUSED_PARAM (u4AddressMapTimeMark);

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetNextIndexAddressMapTable \n");
    if (pAddressMapNetworkAddress == NULL)
    {
        MEMSET (&AddressMapNetworkAddress, 0, sizeof (tIpAddr));
    }
    else
    {
        MEMSET (&AddressMapNetworkAddress, 0, sizeof (tIpAddr));
        if (pAddressMapNetworkAddress->i4_Length == RMON2_IPV6_MAX_LEN)
        {
            MEMCPY (&AddressMapNetworkAddress,
                    pAddressMapNetworkAddress->pu1_OctetList,
                    RMON2_IPV6_MAX_LEN);
        }
        else
        {
            MEMCPY (&AddressMapNetworkAddress.u4_addr[3],
                    pAddressMapNetworkAddress->pu1_OctetList,
                    RMON2_IPV4_MAX_LEN);
        }
    }

    AddressMapNetworkAddress.u4_addr[0] =
        OSIX_NTOHL (AddressMapNetworkAddress.u4_addr[0]);
    AddressMapNetworkAddress.u4_addr[1] =
        OSIX_NTOHL (AddressMapNetworkAddress.u4_addr[1]);
    AddressMapNetworkAddress.u4_addr[2] =
        OSIX_NTOHL (AddressMapNetworkAddress.u4_addr[2]);
    AddressMapNetworkAddress.u4_addr[3] =
        OSIX_NTOHL (AddressMapNetworkAddress.u4_addr[3]);

    RMON2_SET_DATA_SOURCE_OID (pAddressMapSource, &u4AddressMapSource);

    pAddrMapEntry = Rmon2AddrMapGetNextIndex ((UINT4) i4ProtocolDirLocalIndex,
                                              &AddressMapNetworkAddress,
                                              u4AddressMapSource);

    if (pAddrMapEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of Address Map table is failed \r\n");
        return SNMP_FAILURE;
    }

    *pu4NextAddressMapTimeMark = RMON2_ZERO;
    *pi4NextProtocolDirLocalIndex =
        (INT4) pAddrMapEntry->u4ProtocolDirLocalIndex;

    AddressMapNetworkAddress.u4_addr[0] =
        OSIX_HTONL (pAddrMapEntry->AddressMapNetworkAddress.u4_addr[0]);
    AddressMapNetworkAddress.u4_addr[1] =
        OSIX_HTONL (pAddrMapEntry->AddressMapNetworkAddress.u4_addr[1]);
    AddressMapNetworkAddress.u4_addr[2] =
        OSIX_HTONL (pAddrMapEntry->AddressMapNetworkAddress.u4_addr[2]);
    AddressMapNetworkAddress.u4_addr[3] =
        OSIX_HTONL (pAddrMapEntry->AddressMapNetworkAddress.u4_addr[3]);

    pNextAddressMapNetworkAddress->i4_Length =
        (INT4) pAddrMapEntry->u4Rmon2AddressMapIpAddrLen;

    if (pAddrMapEntry->u4Rmon2AddressMapIpAddrLen == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (pNextAddressMapNetworkAddress->pu1_OctetList,
                (UINT1 *) &(AddressMapNetworkAddress), RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (pNextAddressMapNetworkAddress->pu1_OctetList,
                (UINT1 *) &(AddressMapNetworkAddress.u4_addr[3]),
                RMON2_IPV4_MAX_LEN);
    }
    RMON2_GET_DATA_SOURCE_OID (pNextAddressMapSource,
                               pAddrMapEntry->u4AddressMapSource);

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNextIndexAddressMapTable \n");
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetAddressMapPhysicalAddress
 Input       :  The Indices
                AddressMapTimeMark
                ProtocolDirLocalIndex
                AddressMapNetworkAddress
                AddressMapSource

                The Object 
                retValAddressMapPhysicalAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAddressMapPhysicalAddress (UINT4 u4AddressMapTimeMark,
                                 INT4 i4ProtocolDirLocalIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pAddressMapNetworkAddress,
                                 tSNMP_OID_TYPE * pAddressMapSource,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValAddressMapPhysicalAddress)
{
    tAddressMap        *pAddrMapEntry = NULL;
    tIpAddr             AddressMapNetworkAddress;
    UINT4               u4AddressMapSource;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetAddressMapPhysicalAddress \n");
    MEMSET (&AddressMapNetworkAddress, 0, sizeof (tIpAddr));

    if (pAddressMapNetworkAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&AddressMapNetworkAddress,
                pAddressMapNetworkAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&AddressMapNetworkAddress.u4_addr[3],
                pAddressMapNetworkAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    AddressMapNetworkAddress.u4_addr[0] =
        OSIX_NTOHL (AddressMapNetworkAddress.u4_addr[0]);
    AddressMapNetworkAddress.u4_addr[1] =
        OSIX_NTOHL (AddressMapNetworkAddress.u4_addr[1]);
    AddressMapNetworkAddress.u4_addr[2] =
        OSIX_NTOHL (AddressMapNetworkAddress.u4_addr[2]);
    AddressMapNetworkAddress.u4_addr[3] =
        OSIX_NTOHL (AddressMapNetworkAddress.u4_addr[3]);

    RMON2_SET_DATA_SOURCE_OID (pAddressMapSource, &u4AddressMapSource);

    pAddrMapEntry =
        Rmon2AddrMapGetDataEntry ((UINT4) i4ProtocolDirLocalIndex,
                                  &AddressMapNetworkAddress,
                                  u4AddressMapSource);
    if (pAddrMapEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of Address Map table is failed \r\n");
        return SNMP_FAILURE;
    }
    if (pAddrMapEntry->u4AddressMapTimeMark < u4AddressMapTimeMark)
    {
        return SNMP_FAILURE;
    }

    pRetValAddressMapPhysicalAddress->i4_Length = sizeof (tMacAddr);
    MEMCPY (pRetValAddressMapPhysicalAddress->pu1_OctetList,
            &pAddrMapEntry->AddressMapPhysicalAddress,
            pRetValAddressMapPhysicalAddress->i4_Length);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetAddressMapPhysicalAddress \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetAddressMapLastChange
 Input       :  The Indices
                AddressMapTimeMark
                ProtocolDirLocalIndex
                AddressMapNetworkAddress
                AddressMapSource

                The Object 
                retValAddressMapLastChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAddressMapLastChange (UINT4 u4AddressMapTimeMark,
                            INT4 i4ProtocolDirLocalIndex,
                            tSNMP_OCTET_STRING_TYPE * pAddressMapNetworkAddress,
                            tSNMP_OID_TYPE * pAddressMapSource,
                            UINT4 *pu4RetValAddressMapLastChange)
{
    tAddressMap        *pAddrMapEntry = NULL;
    tIpAddr             AddressMapNetworkAddress;
    UINT4               u4AddressMapSource;

    UNUSED_PARAM (u4AddressMapTimeMark);

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetAddressMapLastChange \n");

    MEMSET (&AddressMapNetworkAddress, 0, sizeof (tIpAddr));

    if (pAddressMapNetworkAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&AddressMapNetworkAddress,
                pAddressMapNetworkAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&AddressMapNetworkAddress.u4_addr[3],
                pAddressMapNetworkAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    AddressMapNetworkAddress.u4_addr[0] =
        OSIX_NTOHL (AddressMapNetworkAddress.u4_addr[0]);
    AddressMapNetworkAddress.u4_addr[1] =
        OSIX_NTOHL (AddressMapNetworkAddress.u4_addr[1]);
    AddressMapNetworkAddress.u4_addr[2] =
        OSIX_NTOHL (AddressMapNetworkAddress.u4_addr[2]);
    AddressMapNetworkAddress.u4_addr[3] =
        OSIX_NTOHL (AddressMapNetworkAddress.u4_addr[3]);

    RMON2_SET_DATA_SOURCE_OID (pAddressMapSource, &u4AddressMapSource);

    pAddrMapEntry =
        Rmon2AddrMapGetDataEntry ((UINT4) i4ProtocolDirLocalIndex,
                                  &AddressMapNetworkAddress,
                                  u4AddressMapSource);

    if (pAddrMapEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of Address Map table is failed \r\n");
        return SNMP_FAILURE;
    }

    if (pAddrMapEntry->u4AddressMapTimeMark < u4AddressMapTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValAddressMapLastChange = pAddrMapEntry->u4AddressMapLastChange;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetAddressMapLastChange \n");

    return SNMP_SUCCESS;
}
