/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009   *
 *                                                                  *
 * Description: Contains Function to notify the RMON2 Task abt the  *
 *              change in Interface Status                          *
 *                                                                  *
 *******************************************************************/

#include "rmn2inc.h"
#include "dsmon.h"

/*****************************************************************************/
/* Function        : Rmon2CfaIfaceUpdateChgs                                 */
/* Description     : This function is called from CFA whenever the Interface */
/*                   Status is changed.                                      */
/* Input(s)        : u2Port, u1OperStatus                                    */
/*                                                                           */
/* Output(s)       : None                                                    */
/*                                                                           */
/* Returns         : None                                                    */
/*                                                                           */
/* Action          : Invoked by CFA to post an Event to the RMON2 Task.      */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2CfaIfaceUpdateChgs (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2CfaIfaceUpdateChgs\r\n");
    switch (u1OperStatus)
    {
        case CFA_IF_UP:
        case CFA_IF_DOWN:

            /* Notify to PktInfo */
            Rmon2PktInfoUpdateIFIndexChgs (u4IfIndex, u1OperStatus);

            /* Notify to PDist Group */
            Rmon2PDistUpdateIFIndexChgs (u4IfIndex, u1OperStatus);

            /* Notify to AddrMap Group */
            Rmon2AddrMapUpdateIFIndexChgs (u4IfIndex, u1OperStatus);

            /* Notify to Nl & Al Host Group */
            Rmon2HostUpdateIFIndexChgs (u4IfIndex, u1OperStatus);

            /* Notify to Nl & Al Matrix Group */
            Rmon2MatrixUpdateIFIndexChgs (u4IfIndex, u1OperStatus);
            break;
            /* When Interface is UP, nothing to be done. */
        case CFA_IF_DORM:
            break;
        default:
            break;
    }                            /* End of Switch */

#ifdef DSMON_WANTED
    /* If DSMON is enabled, give indication to DSMON as well */
    DsmonCfaIfaceUpdateChgs (u4IfIndex, u1OperStatus);
#endif

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2CfaIfaceUpdateChgs\r\n");
}

/*****************************************************************************/
/* Function        :   Rmon2CfaIndicateIfUpdate                             */
/* Description     : This function is called from CFA whenever the Interface */
/*                   Status is changed.                                      */
/* Input(s)        : u2Port, u1OperStatus                                    */
/*                                                                           */
/* Output(s)       : None                                                    */
/*                                                                           */
/* Returns         : None                                                    */
/*                                                                           */
/* Action          : Invoked by CFA to post an Event to the RMON2 Task.      */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2CfaIndicateIfUpdate (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tLinkStatInfo      *pMsgBuf;

    pMsgBuf = (tLinkStatInfo *) (MemAllocMemBlk (RMON2_QMSG_POOL));
    if (pMsgBuf == NULL)
    {
        return;
    }
    MEMSET (pMsgBuf, 0, sizeof (tPktHdrInfo));

    pMsgBuf->u4IfIndex = u4IfIndex;
    pMsgBuf->u1OperStatus = u1OperStatus;

    if (OsixQueSend (RMON2_QUEUE_ID, (UINT1 *) &pMsgBuf,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (RMON2_QMSG_POOL, (UINT1 *) pMsgBuf);
        return;
    }

    OsixEvtSend (RMON2_TASK_ID, RMON2_IF_STATUS_EVENT);

    return;
}
