
/********************************************************************
 *                                                                  *
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved             *
 *
 *  $Id: rmn2cli.c,v 1.11 2016/07/12 12:42:10 siva Exp $
 *  
 * Description: This file contains CLI SET/GET routines for the mib *
 *        objects specified in fsrmon2.mib                          *
 *                                                                  *
 *******************************************************************/

#ifndef _RMON2CLI_C
#define _RMON2CLI_C
#endif

#include "rmn2inc.h"
#include "rmn2cli.h"
#include "fsrmon2cli.h"

/*************************************************************************
 *
 *  FUNCTION NAME   : cli_process_rmon2_cmd
 *
 *  DESCRIPTION     : Protocol CLI message handler function
 *
 *  INPUT           : CliHandle - CliContext ID
 *                    u4Command - Command identifier
 *                    ... -Variable command argument list
 *
 *  OUTPUT          : None
 *
 *  RETURNS         : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
cli_process_rmon2_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[RMON2_MAX_ARGS];
    INT1                argno = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
/*    UINT4               u4ErrCode = 0;*/
    INT4                i4StatusVal = 0;

    va_start (ap, u4Command);

    va_arg (ap, UINT1 *);

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == RMON2_MAX_ARGS)
            break;
    }

    va_end (ap);

    switch (u4Command)
    {
        case RMON2_CLI_STATUS:

            /* apu1args[0] contains enable/disable status */

            i4StatusVal =
                (CLI_PTR_TO_I4 (args[0]) ==
                 RMON2_ENABLE) ? RMON2_ENABLE : RMON2_DISABLE;

            i4RetStatus = Rmon2CliSetModuleStatus (CliHandle, i4StatusVal);

            break;

        case RMON2_CLI_SHOW_TRACE:

            Rmon2CliShowTrace (CliHandle);

            break;

        case RMON2_CLI_TRACE:

            /* apu1args[0] contains Debug level */

            i4RetStatus = Rmon2CliSetTrace (CliHandle, CLI_PTR_TO_I4 (args[0]));

            break;

        case RMON2_CLI_RESET_TRACE:

            i4RetStatus = Rmon2CliResetTrace (CliHandle,CLI_PTR_TO_I4 (args[0]));

            break;

        default:

            CliPrintf (CliHandle, "%% Unknown command \r\n");

            return CLI_FAILURE;

    }

/*    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
    if ((u4ErrCode > 0) && (u4ErrCode < CLI_DSMON_MAX_ERR))
    {
        CliPrintf (CliHandle, "%% %s", DsmonCliErrString[u4ErrCode]);
    }
    CLI_SET_ERR (0);
    }
*/
    UNUSED_PARAM (i4RetStatus);
    return CLI_SUCCESS;
}

/****************************************************************************
*
*     FUNCTION NAME    : Rmon2CliSetModuleStatus
*
*     DESCRIPTION      : This function will enable/disable Rmon2 module
*
*     INPUT            : CliHandle  - CliContext ID
*                        i4Status   - Rmon2 Module status
*
*     OUTPUT           : None
*
*     RETURNS          : CLI_SUCCESS/CLI_FAILURE
*
*****************************************************************************/
PUBLIC INT4
Rmon2CliSetModuleStatus (tCliHandle CliHandle, INT4 i4Status)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsRmon2AdminStatus (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsRmon2AdminStatus (i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : Rmon2CliSetTrace
 *
 *     DESCRIPTION      : This function will set the debug level
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Val - Rmon2 Debug Level
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/

PUBLIC INT4
Rmon2CliSetTrace (tCliHandle CliHandle, INT4 i4Val)
{
    INT1                i1RetValue = 0;
    UINT4               u4ErrorCode = 0;
    UINT4                i4SetTrace = 0;
    INT4                 i4RetVal = 0;
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i1RetValue);
    if(i4Val == RMON2_CLI_TRACE_ALL)
    {
        i4RetVal = CliDisplayMessageAndUserPromptResponse
                     ("This will enable all RMON traces ", 1, Rmon2MainLock , Rmon2MainUnLock);
        if (i4RetVal != CLI_SUCCESS)
        {
            return CLI_SUCCESS;
        }

    }
    nmhGetFsRmon2Trace(&i4SetTrace);
    if (nmhTestv2FsRmon2Trace (&u4ErrorCode, (UINT4) i4Val) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    i4SetTrace  |= (UINT4)((i4Val));
    i1RetValue = nmhSetFsRmon2Trace ( i4SetTrace);

    return CLI_SUCCESS;
}

/***************************************************************************
 *
 *     FUNCTION NAME    : Rmon2CliResetTrace
 *
 *     DESCRIPTION      : This function will reset the debug level
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Val - Rmon2 Debug Level
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/

PUBLIC INT4
Rmon2CliResetTrace (tCliHandle CliHandle,INT4 i4Val)
{

    INT1                i1RetValue = 0;
    UINT4                i4ResetTrace = 0;
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i1RetValue);

     nmhGetFsRmon2Trace(&i4ResetTrace);
    if (nmhTestv2FsRmon2Trace (&u4ErrorCode, i4Val) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    i4ResetTrace  &= (UINT4)(~(i4Val));
    i1RetValue = nmhSetFsRmon2Trace (i4ResetTrace);

    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : Rmon2CliShowTrace
 *
 *     DESCRIPTION      : This function prints the RMON2 debug level
 *
 *     INPUT            : CliHandle  - CliContext ID
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
PUBLIC VOID
Rmon2CliShowTrace (tCliHandle CliHandle)
{
    UINT4               u4DbgLevel = 0;
    INT4                i4Status = 0;

    nmhGetFsRmon2AdminStatus (&i4Status);

    if (i4Status == RMON2_DISABLE)
    {
        return;
    }
    nmhGetFsRmon2Trace (&u4DbgLevel);

    if (u4DbgLevel == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "\rRMON2 :");

    if ((u4DbgLevel & RMON2_FN_ENTRY) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RMON2 Function entry trace is on");
    }
    if ((u4DbgLevel & RMON2_FN_EXIT) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RMON2 Function exit trace is on");
    }
    if ((u4DbgLevel & RMON2_CRITICAL_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RMON2 Critical trace is on");
    }
    if ((u4DbgLevel & RMON2_MEM_FAIL) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RMON2 Memory fail trace is on");
    }
    if ((u4DbgLevel & RMON2_DEBUG_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RMON2 Debug trace is on");
    }
    CliPrintf (CliHandle, "\r\n");

    return;

}
/****************************************************************************
 *
 *     FUNCTION NAME    : Rmon2ShowRunningConfig
 *
 *     DESCRIPTION      : This function prints the RMON2 configuration in 
 *                             show running-config
 *
 *     INPUT            : CliHandle  - CliContext ID
 *
 *     OUTPUT           : None
 *
 *****************************************************************************/
PUBLIC VOID
Rmon2ShowRunningConfig (tCliHandle CliHandle)
{

    INT4                i4Status = 0;
    
    nmhGetFsRmon2AdminStatus (&i4Status);

    if (i4Status == RMON2_DISABLE)
    {
        return;
    }
    CliPrintf (CliHandle, "\r\n!\r\n");
    CliPrintf (CliHandle, "\r\n rmon2 enable\r\n");

    return;
}

