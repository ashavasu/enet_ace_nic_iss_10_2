/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmn2pdir.c,v 1.12 2013/07/12 12:33:23 siva Exp $
 *
 * Description: This file has the routines which are customer
 *              specific
 *
 *******************************************************************/

/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * Description: This file contains the functional routine for
 *              RMON2 Protocol Directory module.
 *
 *******************************************************************/
#include "rmn2inc.h"

typedef struct Rmon2_lookup_table
{
    UINT1               u1Protocol[MAX_PROTO_ID_BASE_LEN];
    UINT1               u1ProtoExtOrAddrRecognType;
} tProtocolLookUpTable;

tProtocolLookUpTable gProtoLookUp[RMON2_MAX_PROTOCOLS_LIMIT] = {
    {{0, 0, 0, 1}, EXTENSIBLE_ADDR_RECOGN},    /* ether2 */
    {{0, 0, 0, 2}, EXTENSIBLE_ADDR_RECOGN},    /* llc */
    {{0, 0, 0, 3}, EXTENSIBLE_ADDR_RECOGN},    /* snap */
    {{0, 0, 0, 4}, EXTENSIBLE_ADDR_RECOGN},    /* vsnap */
    {{0, 0, 0, 5}, EXTENSIBLE_ADDR_RECOGN},    /* ianaAssigned */
    {{0, 0, 8, 0}, EXTENSIBLE_ADDR_RECOGN},    /* ip */
    {{0, 0, 134, 221}, EXTENSIBLE_ADDR_RECOGN},    /* ipv6 */
    {{0, 0, 129, 55}, EXTENSIBLE_ADDR_RECOGN},    /* ipx */
    {{0, 0, 0, 6}, EXTENSIBLE_NO_ADDR_RECOGN},    /* tcp */
    {{0, 0, 0, 17}, EXTENSIBLE_NO_ADDR_RECOGN},    /* udp */
    {{0, 0, 0, 161}, NOT_EXTENSIBLE_NO_ADDR_RECOGN},    /* snmp */
    {{0, 0, 0, 23}, NOT_EXTENSIBLE_NO_ADDR_RECOGN},    /* telnet */
    {{0, 0, 0, 21}, NOT_EXTENSIBLE_NO_ADDR_RECOGN},    /* ftp */
    {{0, 0, 0, 69}, NOT_EXTENSIBLE_NO_ADDR_RECOGN},    /* tftp */
    {{0, 0, 0, 22}, NOT_EXTENSIBLE_NO_ADDR_RECOGN},    /* ssh */
    {{0, 0, 0, 80}, NOT_EXTENSIBLE_NO_ADDR_RECOGN},    /* http */
    {{0, 0, 0, 53}, NOT_EXTENSIBLE_NO_ADDR_RECOGN},    /* dns */
};

PRIVATE VOID Rmon2PDirGetProtDirParams ARG_LIST ((UINT4, UINT1 *));
PRIVATE VOID Rmon2PDirAllocateProtocolIndex ARG_LIST ((VOID));
PRIVATE UINT4 Rmon2PDirGetProtDirType ARG_LIST ((UINT4));
PRIVATE UINT4 Rmon2PDirGetAddrMapSupport ARG_LIST ((UINT4));
PRIVATE UINT4 Rmon2PDirGetMatrixSupport ARG_LIST ((UINT4));
PRIVATE UINT4 Rmon2PDirGetHostSupport ARG_LIST ((UINT4));

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDirInitProtDirTable                                  */
/*                                                                           */
/* Description  : Initializes all Protocols supported by RMON2 Probe Capacity*/
/*                into ProtocolDirTable, when RMON2 is enabled.              */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2PDirInitProtDirTable (VOID)
{
    tProtocolDir       *pProtDir = NULL;
    FILE               *pFile = NULL;
    CHR1                ac1ProtoInpStr[RMON2_MAX_PROTTEXT_LEN];
    CHR1               *c1Token = NULL;
    UINT1               au1ProtDirID[RMON2_MAX_PROT_DIR_ID_LEN];
    UINT1               au1ProtDirParams[RMON2_MAX_PROT_DIR_PARAM_LEN];
    tSNMP_OCTET_STRING_TYPE ProtDirID;
    tSNMP_OCTET_STRING_TYPE ProtDirParams;
    UINT4               u4LayerType = 1, u4ProtDirType = 0;
    UINT4               u4AddrMapConfig = 0, u4HostConfig = 0;
    UINT4               u4MatrixConfig = 0;
    UINT2               u2EtherType = 0;
    UINT1              *pu1ProtDirID = NULL;
    UINT1               au1ProtDirIDTemp[RMON2_MAX_PROT_DIR_ID_LEN];
    UINT1               au1ProtDirParamsTemp[RMON2_MAX_PROT_DIR_PARAM_LEN];

    MEMSET (au1ProtDirIDTemp, 0, RMON2_MAX_PROT_DIR_ID_LEN);
    MEMSET (au1ProtDirParamsTemp, 0, RMON2_MAX_PROT_DIR_PARAM_LEN);

    ProtDirID.pu1_OctetList = au1ProtDirIDTemp;
    ProtDirID.i4_Length = RMON2_MAX_PROT_DIR_ID_LEN;
    ProtDirParams.pu1_OctetList = au1ProtDirParamsTemp;
    ProtDirParams.i4_Length = RMON2_MAX_PROT_DIR_ID_LEN;

    MEMSET (ac1ProtoInpStr, 0, sizeof (RMON2_MAX_PROTTEXT_LEN));

    pu1ProtDirID = au1ProtDirID;

    pFile = FOPEN (RMON2_PROBECAPACITY_PROTLIST, "r");
    if (pFile == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Rmon2PDirInitProtDirTable: File Open failed\r\n");
        return;
    }
    while (fgets (ac1ProtoInpStr, RMON2_MAX_PROTTEXT_LEN, pFile) != NULL)
    {
        MEMSET (au1ProtDirID, 0, RMON2_MAX_PROT_DIR_ID_LEN);
        MEMSET (au1ProtDirParams, 0, RMON2_MAX_PROT_DIR_PARAM_LEN);

        c1Token = (CHR1 *) STRTOK (ac1ProtoInpStr, ".\n");
        while (c1Token != NULL)
        {
            u4LayerType++;
            switch (u4LayerType)
            {
                case LAYER2:
                    if (STRCMP ("ether2", c1Token) == 0)
                    {
                        SPRINTF ((CHR1 *) pu1ProtDirID + STRLEN (pu1ProtDirID),
                                 "000%d", 1);
                    }
                    break;

                case LAYER3:
                    if (STRCMP ("ip", c1Token) == 0)
                    {
                        u2EtherType = RMON2_IPV4;
                        SPRINTF ((CHR1 *) pu1ProtDirID + STRLEN (pu1ProtDirID),
                                 "00%c%c",
                                 *(UINT1 *) ((1 + (UINT1 *) &u2EtherType)),
                                 *(UINT1 *) &u2EtherType);
                    }
                    else if (STRCMP ("ipv6", c1Token) == 0)
                    {
                        u2EtherType = RMON2_IPV6;
                        SPRINTF ((CHR1 *) pu1ProtDirID + STRLEN (pu1ProtDirID),
                                 "00%c%c",
                                 *(UINT1 *) ((1 + (UINT1 *) &u2EtherType)),
                                 *(UINT1 *) &u2EtherType);
                    }
                    else if (STRCMP ("ipx", c1Token) == 0)
                    {
                        u2EtherType = RMON2_IPVX;
                        SPRINTF ((CHR1 *) pu1ProtDirID + STRLEN (pu1ProtDirID),
                                 "00%c%c",
                                 *(UINT1 *) ((1 + (UINT1 *) &u2EtherType)),
                                 *(UINT1 *) &u2EtherType);
                    }
                    break;

                case LAYER4:
                    if (STRCMP ("tcp", c1Token) == 0)
                    {
                        SPRINTF ((CHR1 *) pu1ProtDirID + STRLEN (pu1ProtDirID),
                                 "000%d", RMON2_TCP);
                    }
                    else if (STRCMP ("udp", c1Token) == 0)
                    {
                        SPRINTF ((CHR1 *) pu1ProtDirID + STRLEN (pu1ProtDirID),
                                 "000%d", RMON2_UDP);
                    }
                    else if (STRCMP ("icmp", c1Token) == 0)
                    {
                        SPRINTF ((CHR1 *) pu1ProtDirID + STRLEN (pu1ProtDirID),
                                 "000%d", RMON2_ICMP);
                    }
                    break;

                case LAYER5:
                    if (STRCMP ("telnet", c1Token) == 0)
                    {
                        SPRINTF ((CHR1 *) pu1ProtDirID + STRLEN (pu1ProtDirID),
                                 "000%d", RMON2_TELNET);
                    }
                    else if (STRCMP ("dns", c1Token) == 0)
                    {
                        SPRINTF ((CHR1 *) pu1ProtDirID + STRLEN (pu1ProtDirID),
                                 "000%d", RMON2_DNS);
                    }
                    else if (STRCMP ("tftp", c1Token) == 0)
                    {
                        SPRINTF ((CHR1 *) pu1ProtDirID + STRLEN (pu1ProtDirID),
                                 "000%d", RMON2_TFTP);
                    }
                    else if (STRCMP ("ftp", c1Token) == 0)
                    {
                        SPRINTF ((CHR1 *) pu1ProtDirID + STRLEN (pu1ProtDirID),
                                 "000%d", RMON2_FTP);
                    }
                    else if (STRCMP ("snmp", c1Token) == 0)
                    {
                        SPRINTF ((CHR1 *) pu1ProtDirID + STRLEN (pu1ProtDirID),
                                 "000%d", RMON2_SNMP);
                    }
                    else if (STRCMP ("pop3", c1Token) == 0)
                    {
                        SPRINTF ((CHR1 *) pu1ProtDirID + STRLEN (pu1ProtDirID),
                                 "000%d", RMON2_POP3);
                    }
                    else if (STRCMP ("smtp", c1Token) == 0)
                    {
                        SPRINTF ((CHR1 *) pu1ProtDirID + STRLEN (pu1ProtDirID),
                                 "000%d", RMON2_SMTP);
                    }
                    break;

                default:
                    break;
            }
            c1Token = (CHR1 *) STRTOK (NULL, ".\n");
        }                        /* end of Token while */

        Rmon2PDirGetProtDirParams (u4LayerType, au1ProtDirParams);
        u4ProtDirType = Rmon2PDirGetProtDirType (u4LayerType);
        u4AddrMapConfig = Rmon2PDirGetAddrMapSupport (u4LayerType);
        u4HostConfig = Rmon2PDirGetHostSupport (u4LayerType);
        u4MatrixConfig = Rmon2PDirGetMatrixSupport (u4LayerType);

        ProtDirID.i4_Length = (INT4) STRLEN (au1ProtDirID);
        ProtDirParams.i4_Length = (INT4) STRLEN (au1ProtDirParams);

        MEMCPY (ProtDirID.pu1_OctetList, au1ProtDirID, ProtDirID.i4_Length);
        MEMCPY (ProtDirParams.pu1_OctetList, au1ProtDirParams,
                ProtDirParams.i4_Length);

        pProtDir = Rmon2PDirAddProtDirEntry (&ProtDirID, &ProtDirParams);

        if (pProtDir == NULL)
        {
            RMON2_TRC2 (RMON2_DEBUG_TRC,
                        "Failed in adding Protocol Dir entry in ProtocolDir Table"
                        "for index : %s %s \r\n", au1ProtDirID,
                        au1ProtDirParams);
            fclose (pFile);
            return;
        }

        pProtDir->u1ProtocolDirType = (UINT1) u4ProtDirType;
        pProtDir->u4ProtocolDirAddressMapConfig = u4AddrMapConfig;
        pProtDir->u4ProtocolDirHostConfig = u4HostConfig;
        pProtDir->u4ProtocolDirMatrixConfig = u4MatrixConfig;
        pProtDir->u4ProtocolDirStatus = NOT_READY;
        gRmon2Globals.u4ProtocolDirLastChange = OsixGetSysUpTime ();
        u4LayerType = 1;
    }                            /* end of File while */
    fclose (pFile);
    return;
}

/*****************************************************************************/

/*                                                                           */
/* Function     : Rmon2PDirGetProtDirParams                                  */
/*                                                                           */
/* Description  : This function gives ProtDirParameters for a Protocol.      */
/*                                                                           */
/* Input        : u4LayerType, pu1Param                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
Rmon2PDirGetProtDirParams (UINT4 u4LayerType, UINT1 *pu1Param)
{
    switch (u4LayerType)
    {
        case LAYER2:
            SPRINTF ((CHR1 *) pu1Param, "%s", "0");
            break;
        case LAYER3:
            SPRINTF ((CHR1 *) pu1Param, "%s", "00");
            break;
        case LAYER4:
            SPRINTF ((CHR1 *) pu1Param, "%s", "000");
            break;
        case LAYER5:
            SPRINTF ((CHR1 *) pu1Param, "%s", "0000");
            break;
        default:
            break;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDirAllocateProtocolIndex                           */
/*                                                                           */
/* Description  : This function assigns ProtDirLocalIndex for a PDir Entry.  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
Rmon2PDirAllocateProtocolIndex (VOID)
{
    tProtocolDir       *pProtDir = NULL;
    tSNMP_OCTET_STRING_TYPE ProtocolDirID, ProtocolDirParameters;

    UINT1               au1ProtDirID[RMON2_MAX_PROT_DIR_ID_LEN + 1];
    UINT1               au1ProtDirParams[RMON2_MAX_PROT_DIR_PARAM_LEN + 1];

    MEMSET (au1ProtDirID, 0, RMON2_MAX_PROT_DIR_ID_LEN);
    MEMSET (au1ProtDirParams, 0, RMON2_MAX_PROT_DIR_PARAM_LEN);

    ProtocolDirID.pu1_OctetList = au1ProtDirID;
    ProtocolDirID.i4_Length = 0;

    ProtocolDirParameters.pu1_OctetList = au1ProtDirParams;
    ProtocolDirParameters.i4_Length = 0;

    gRmon2Globals.u4ProtDirLocIndex++;

    /* Get First Entry in Protocol Dir Table */
    pProtDir = Rmon2PDirGetNextProtDirIndex (&ProtocolDirID,
                                             &ProtocolDirParameters);

    while (pProtDir != NULL)
    {
        MEMCPY (ProtocolDirID.pu1_OctetList, pProtDir->au1ProtocolDirID,
                pProtDir->u4ProtocolDirIDLen);
        MEMCPY (ProtocolDirParameters.pu1_OctetList,
                pProtDir->au1ProtocolDirParameters,
                pProtDir->u4ProtocolDirParametersLen);
        ProtocolDirID.i4_Length = (INT4) pProtDir->u4ProtocolDirIDLen;
        ProtocolDirParameters.i4_Length =
            (INT4) pProtDir->u4ProtocolDirParametersLen;

        /* If Protocol Loc Index reahces Maximum limit, ReInitialise to zero */

        if (gRmon2Globals.u4ProtDirLocIndex > RMON2_MAX_PROTDIRLOC_INDEX)
        {
            gRmon2Globals.u4ProtDirLocIndex = 1;

            MEMSET (au1ProtDirID, 0, RMON2_MAX_PROT_DIR_ID_LEN);
            MEMSET (au1ProtDirParams, 0, RMON2_MAX_PROT_DIR_PARAM_LEN);

            ProtocolDirID.pu1_OctetList = au1ProtDirID;
            ProtocolDirID.i4_Length = 0;

            pProtDir =
                Rmon2PDirGetNextProtDirIndex (&ProtocolDirID,
                                              &ProtocolDirParameters);
            continue;
        }

        /* If ProtDirLocIndex Matches with ProtDirLocIndex of   *
         * any existing entry in PDir Table,                    *
         * search for next free index                           */

        if (pProtDir->u4ProtocolDirLocalIndex ==
            gRmon2Globals.u4ProtDirLocIndex)
        {
            gRmon2Globals.u4ProtDirLocIndex++;
            pProtDir =
                Rmon2PDirGetNextProtDirIndex (&ProtocolDirID,
                                              &ProtocolDirParameters);
            continue;
        }
        pProtDir =
            Rmon2PDirGetNextProtDirIndex (&ProtocolDirID,
                                          &ProtocolDirParameters);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDirGetProtDirType                                    */
/*                                                                           */
/* Description  : This function describes two attributes: extensible and     */
/*                address recognition capable of a Protocol                  */
/*                                                                           */
/* Input        : u4LayerType                                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : u1ProtoExtOrAddrRecognType on Success                      */
/*               -1 on Failure                                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rmon2PDirFindProtDirType (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                          tSNMP_OCTET_STRING_TYPE * pProtocolDirParameters)
{
    UINT4               u4Index = 0;
    UINT1               au1TempId[RMON2_MAX_PROT_DIR_ID_LEN];

    MEMSET (au1TempId, 0, RMON2_MAX_PROT_DIR_ID_LEN);

    UNUSED_PARAM (pProtocolDirParameters);

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PDirFindProtDirType \r\n");

    MEMCPY (au1TempId,
            ((pProtocolDirID->pu1_OctetList + pProtocolDirID->i4_Length) - 4),
            MEM_MAX_BYTES (4, RMON2_MAX_PROT_DIR_ID_LEN));

    while (u4Index < RMON2_MAX_PROTOCOLS_LIMIT)
    {
        /* Searching in Protocol LookUp Table to identfiy *
         * Protocol Type */
        if (MEMCMP (gProtoLookUp[u4Index].u1Protocol, au1TempId, 4) == 0)
        {
            return gProtoLookUp[u4Index].u1ProtoExtOrAddrRecognType;
        }
        u4Index++;
    }
    RMON2_TRC (RMON2_DEBUG_TRC,
               "Rmon2PDirFindProtDirType: Protocol Type Unknown \r\n");

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2PDirFindProtDirType \r\n");

    return RMON2_INVALID_VALUE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDirGetProtocolNode                                   */
/*                                                                           */
/* Description  : This function gets Protocol Entry for the given            */
/*                ProtDirLocalIndex.                                         */
/*                                                                           */
/* Input        : u4ProtDirLocalIndex                                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC tProtocolDir *
Rmon2PDirGetProtocolNode (UINT4 u4ProtDirLocalIndex)
{
    tProtocolDir       *pProtDir = NULL;
    tSNMP_OCTET_STRING_TYPE ProtocolDirID, ProtocolDirParameters;

    UINT1               au1ProtDirID[RMON2_MAX_PROT_DIR_ID_LEN + 1];
    UINT1               au1ProtDirParams[RMON2_MAX_PROT_DIR_PARAM_LEN + 1];

    MEMSET (au1ProtDirID, 0, RMON2_MAX_PROT_DIR_ID_LEN);
    MEMSET (au1ProtDirParams, 0, RMON2_MAX_PROT_DIR_PARAM_LEN);

    ProtocolDirID.pu1_OctetList = au1ProtDirID;
    ProtocolDirID.i4_Length = 0;

    ProtocolDirParameters.pu1_OctetList = au1ProtDirParams;
    ProtocolDirParameters.i4_Length = 0;

    /* Get First Entry in Protocol Dir Table */
    pProtDir = Rmon2PDirGetNextProtDirIndex (&ProtocolDirID,
                                             &ProtocolDirParameters);

    while (pProtDir != NULL)
    {
        ProtocolDirID.i4_Length = (INT4) pProtDir->u4ProtocolDirIDLen;
        MEMCPY (ProtocolDirID.pu1_OctetList, pProtDir->au1ProtocolDirID,
                pProtDir->u4ProtocolDirIDLen);
        ProtocolDirParameters.i4_Length =
            (INT4) pProtDir->u4ProtocolDirParametersLen;
        MEMCPY (ProtocolDirParameters.pu1_OctetList,
                pProtDir->au1ProtocolDirParameters,
                pProtDir->u4ProtocolDirParametersLen);

        if (pProtDir->u4ProtocolDirLocalIndex == u4ProtDirLocalIndex)
        {
            break;
        }

        pProtDir =
            Rmon2PDirGetNextProtDirIndex (&ProtocolDirID,
                                          &ProtocolDirParameters);
    }
    return pProtDir;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDirGetProtDirType                                    */
/*                                                                           */
/* Description  : This function describes two attributes: extensible and     */
/*                address recognition capable of a Protocol                  */
/*                                                                           */
/* Input        : u4LayerType                                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : u4ProtDirType                                              */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT4
Rmon2PDirGetProtDirType (UINT4 u4LayerType)
{
    UINT4               u4ProtDirType = 0;
    switch (u4LayerType)
    {
        case LAYER2:

        case LAYER3:
            u4ProtDirType = EXTENSIBLE_ADDR_RECOGN;
            break;
        case LAYER4:
            u4ProtDirType = EXTENSIBLE_NO_ADDR_RECOGN;
            break;
        case LAYER5:
            u4ProtDirType = NOT_EXTENSIBLE_NO_ADDR_RECOGN;
            break;
        default:
            break;
    }
    return u4ProtDirType;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDirGetAddrMapSupport                                 */
/*                                                                           */
/* Description  : This function gives AddressMapSupport capability for a     */
/*                Protocol                                                   */
/*                                                                           */
/* Input        : u4LayerType                                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : u4AddrMapSupport                                           */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT4
Rmon2PDirGetAddrMapSupport (UINT4 u4LayerType)
{
    UINT4               u4AddrMapSupport = 0;
    switch (u4LayerType)
    {
        case LAYER2:

        case LAYER3:
            u4AddrMapSupport = SUPPORTED_ON;
            break;
        case LAYER4:

        case LAYER5:
            u4AddrMapSupport = NOT_SUPPORTED;
            break;
        default:
            break;
    }
    return u4AddrMapSupport;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDirGetHostSupport                                    */
/*                                                                           */
/* Description  : This function gives HostSupport capability for a Protocol  */
/*                                                                           */
/* Input        : u4LayerType                                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : u4HostSupport                                              */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT4
Rmon2PDirGetHostSupport (UINT4 u4LayerType)
{
    UINT4               u4HostSupport = 0;
    switch (u4LayerType)
    {
        case LAYER2:
            u4HostSupport = NOT_SUPPORTED;
            break;
        case LAYER3:

        case LAYER4:

        case LAYER5:
            u4HostSupport = SUPPORTED_ON;
            break;
        default:
            break;
    }
    return u4HostSupport;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDirGetMatrixSupport                                  */
/*                                                                           */
/* Description  : This function gives MatrixSupport capability for a Protocol*/
/*                                                                           */
/* Input        : u4LayerType                                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : u4MatrixSupport                                            */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT4
Rmon2PDirGetMatrixSupport (UINT4 u4LayerType)
{
    UINT4               u4MatrixSupport = 0;
    switch (u4LayerType)
    {
        case LAYER2:
            u4MatrixSupport = NOT_SUPPORTED;
            break;
        case LAYER3:

        case LAYER4:

        case LAYER5:
            u4MatrixSupport = SUPPORTED_ON;
            break;
        default:
            break;
    }
    return u4MatrixSupport;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDirGiveProtDirLocIndices                             */
/*                                                                           */
/* Description  : This function processes Protocol Information from Packet   */
/*                and gives it to Protocol Directory Table to find           */
/*                Protocol Dir local Index for LAYER3,                       */
/*                LAYER4 and LAYER5 Protocols.                               */
/*                                                                           */
/* Input        : pPktInfo                                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rmon2PDirGiveProtDirLocIndices (tRmon2PktInfo * pPktInfo)
{
    tProtocolDir       *pProtDir = NULL;
    UINT1               au1ProtDirID[RMON2_MAX_PROT_DIR_ID_LEN];
    UINT1               au1ProtDirParams[RMON2_MAX_PROT_DIR_PARAM_LEN];
    tSNMP_OCTET_STRING_TYPE ProtDirID;
    tSNMP_OCTET_STRING_TYPE ProtDirParams;
    UINT1              *pu1ProtDirID = NULL;
    UINT1              *pu1ProtDirParams = NULL;
    UINT2               u2EtherType = 0, u2DstPort = 0;
    UINT1               u1IpProtocol = 0;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY Rmon2PDirGiveProtDirLocIndices\r\n");

    pu1ProtDirID = au1ProtDirID;
    pu1ProtDirParams = au1ProtDirParams;

    MEMSET (au1ProtDirID, 0, RMON2_MAX_PROT_DIR_ID_LEN);
    MEMSET (au1ProtDirParams, 0, RMON2_MAX_PROT_DIR_PARAM_LEN);

    u2EtherType = pPktInfo->PktHdr.u2EtherType;
    u1IpProtocol = pPktInfo->PktHdr.u1IpProtocol;
    u2DstPort = pPktInfo->PktHdr.u2DstPort;

    SPRINTF ((CHR1 *) & au1ProtDirID[3], "%c", 1);
    SPRINTF ((CHR1 *) & au1ProtDirID[6], "%c", (u2EtherType >> 8));
    SPRINTF ((CHR1 *) & au1ProtDirID[7], "%c", (u2EtherType & 0xff));

    /* Get ProtocolDir Entry for LAYER3 Protocol */

    ProtDirID.pu1_OctetList = pu1ProtDirID;
    ProtDirID.i4_Length = 8;
    ProtDirParams.pu1_OctetList = pu1ProtDirParams;
    ProtDirParams.i4_Length = 2;

    pProtDir = Rmon2PDirGetProtDirEntry (&ProtDirID, &ProtDirParams);

    if ((pProtDir != NULL) && (pProtDir->u4ProtocolDirStatus == ACTIVE))
    {
        pPktInfo->u4ProtoNLIndex = pProtDir->u4ProtocolDirLocalIndex;

        SPRINTF ((CHR1 *) & au1ProtDirID[11], "%c", u1IpProtocol);

        /* Get ProtocolDir Entry for LAYER4 Protocol only if 
         * ProtocolDir Entry exist for LAYER3 Protocol */

        ProtDirID.pu1_OctetList = pu1ProtDirID;
        ProtDirID.i4_Length = 12;
        ProtDirParams.pu1_OctetList = pu1ProtDirParams;
        ProtDirParams.i4_Length = 3;

        pProtDir = Rmon2PDirGetProtDirEntry (&ProtDirID, &ProtDirParams);

        if ((pProtDir != NULL) && (pProtDir->u4ProtocolDirStatus == ACTIVE))
        {
            pPktInfo->u4ProtoL4ALIndex = pProtDir->u4ProtocolDirLocalIndex;

            SPRINTF ((CHR1 *) & au1ProtDirID[15], "%c", u2DstPort);

            /* Get ProtocolDir Entry for LAYER5 Protocol only if
             * ProtocolDir Entry exist for LAYER4 Protocol*/

            ProtDirID.pu1_OctetList = pu1ProtDirID;
            ProtDirID.i4_Length = 16;
            ProtDirParams.pu1_OctetList = pu1ProtDirParams;
            ProtDirParams.i4_Length = 4;

            pProtDir = Rmon2PDirGetProtDirEntry (&ProtDirID, &ProtDirParams);

            if ((pProtDir != NULL) && (pProtDir->u4ProtocolDirStatus == ACTIVE))
            {
                pPktInfo->u4ProtoL5ALIndex = pProtDir->u4ProtocolDirLocalIndex;
            }
        }
        return OSIX_SUCCESS;
    }

    RMON2_TRC (RMON2_DEBUG_TRC,
               "Rmon2PDirGiveProtDirLocIndices: Protocol Local Index "
               "not found for L3 Protocol\r\n");

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2PDirGiveProtDirLocIndices\r\n");
    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDirGetNextProtDirIndex                               */
/*                                                                           */
/* Description  : Get First / Next ProtDir Index                             */
/*                                                                           */
/* Input        : pu1ProtocolDirID, pu1ProtocolDirParams                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tProtocolDir / NULL Pointer                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC tProtocolDir *
Rmon2PDirGetNextProtDirIndex (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                              tSNMP_OCTET_STRING_TYPE * pProtocolDirParams)
{
    tProtocolDir        ProtDir;

    MEMSET (&ProtDir, 0, sizeof (tProtocolDir));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PDirGetNextProtDirIndex \r\n");

    MEMCPY (ProtDir.au1ProtocolDirID, pProtocolDirID->pu1_OctetList,
            pProtocolDirID->i4_Length);
    MEMCPY (ProtDir.au1ProtocolDirParameters, pProtocolDirParams->pu1_OctetList,
            pProtocolDirParams->i4_Length);
    ProtDir.u4ProtocolDirIDLen = (UINT4) pProtocolDirID->i4_Length;
    ProtDir.u4ProtocolDirParametersLen = (UINT4) pProtocolDirParams->i4_Length;

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2PDirGetNextProtDirIndex\r\n");

    return ((tProtocolDir *) RBTreeGetNext (RMON2_PROTDIR_TREE, (tRBElem *)
                                            & ProtDir, NULL));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDirAddProtDirEntry                                   */
/*                                                                           */
/* Description  : Allocates memory and Adds Protocol Directory Entry into    */
/*                ProtocolDirRBTree                                          */
/*                                                                           */
/* Input        : pu1ProtocolDirID, pu1ProtocolDirParams                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tProtocolDir / NULL Pointer                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC tProtocolDir *
Rmon2PDirAddProtDirEntry (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                          tSNMP_OCTET_STRING_TYPE * pProtocolDirParams)
{
    tProtocolDir       *pProtDir = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PDirAddProtDirEntry \r\n");

    if ((pProtDir = (tProtocolDir *) (MemAllocMemBlk (RMON2_PROTDIR_POOL)))
        == NULL)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for ProtocolDirEntry\r\n");
        return NULL;
    }
    MEMSET (pProtDir, 0, sizeof (tProtocolDir));

    MEMCPY (pProtDir->au1ProtocolDirID, pProtocolDirID->pu1_OctetList,
            pProtocolDirID->i4_Length);

    pProtDir->u4ProtocolDirIDLen = (UINT4) pProtocolDirID->i4_Length;

    MEMCPY (pProtDir->au1ProtocolDirParameters,
            pProtocolDirParams->pu1_OctetList, pProtocolDirParams->i4_Length);

    pProtDir->u4ProtocolDirParametersLen =
        (UINT4) pProtocolDirParams->i4_Length;

    if (RBTreeAdd (RMON2_PROTDIR_TREE, (tRBElem *) pProtDir) == RB_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Rmon2PDirAddProtDirEntry: ProtocolDirEntry is added\r\n");
        Rmon2PDirAllocateProtocolIndex ();
        pProtDir->u4ProtocolDirLocalIndex = gRmon2Globals.u4ProtDirLocIndex;
        return pProtDir;
    }

    RMON2_TRC (RMON2_CRITICAL_TRC,
               "Rmon2PDirAddProtDirEntry: Adding ProtocolDirEntry Failed\r\n");

    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (RMON2_PROTDIR_POOL, (UINT1 *) pProtDir);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2PDirAddProtDirEntry \r\n");
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDirDelProtDirEntry                                   */
/*                                                                           */
/* Description  : Releases memory and Removes Prot Dir Entry from            */
/*                ProtocolDirRBTree                                          */
/*                                                                           */
/* Input        : pProtDir                                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rmon2PDirDelProtDirEntry (tProtocolDir * pProtDir)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PDirDelProtDirEntry \r\n");
    /* Remove Prot Dir Entry from ProtocolDirRBTree */
    RBTreeRem (RMON2_PROTDIR_TREE, (tRBElem *) pProtDir);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (RMON2_PROTDIR_POOL,
                            (UINT1 *) pProtDir) != MEM_SUCCESS)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for ProtocolDirEntry\r\n");
        return OSIX_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2PDirDelProtDirEntry \r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDirGetProtDirEntry                                   */
/*                                                                           */
/* Description  : Get Prot Dir Entry for the given Index                     */
/*                                                                           */
/* Input        : pu1ProtocolDirID, pu1ProtocolDirParams                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tProtocolDir / NULL Pointer                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC tProtocolDir *
Rmon2PDirGetProtDirEntry (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                          tSNMP_OCTET_STRING_TYPE * pProtocolDirParams)
{
    tProtocolDir        ProtDir;

    MEMSET (&ProtDir, 0, sizeof (tProtocolDir));
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PDirGetProtDirEntry \r\n");

    MEMCPY (ProtDir.au1ProtocolDirID, pProtocolDirID->pu1_OctetList,
            pProtocolDirID->i4_Length);
    MEMCPY (ProtDir.au1ProtocolDirParameters, pProtocolDirParams->pu1_OctetList,
            pProtocolDirParams->i4_Length);
    ProtDir.u4ProtocolDirIDLen = (UINT4) pProtocolDirID->i4_Length;
    ProtDir.u4ProtocolDirParametersLen = (UINT4) pProtocolDirParams->i4_Length;

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2PDirGetProtDirEntry \r\n");

    return ((tProtocolDir *) RBTreeGet (RMON2_PROTDIR_TREE, (tRBElem *)
                                        & ProtDir));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2ChkForPrntEntryAndExFeature                           */
/*                                                                           */
/* Description  : Check for Parent Protocol Entry present or not and         */
/*                Extensiblity to create child protocol for this protocol    */
/*                                                                           */
/* Input        : pu1ProtocolDirID, pu1ProtocolDirParams                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
Rmon2ChkForPrntEntryAndExFeature (tSNMP_OCTET_STRING_TYPE * pu1ProtocolDirID,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pu1ProtocolDirParams)
{
    UINT1               au1PrntProtocolDirID[30];
    UINT1               au1PrntProtocolDirParams[5];
    tSNMP_OCTET_STRING_TYPE PrntProtocolDirID;
    tSNMP_OCTET_STRING_TYPE PrntProtocolDirParams;
    tProtocolDir       *pProtDir;

    MEMSET (au1PrntProtocolDirID, 0, sizeof (au1PrntProtocolDirID));
    MEMSET (au1PrntProtocolDirParams, 0, sizeof (au1PrntProtocolDirParams));

    /* If the length of the Protocol ID is 4, it is L2 Protocol
     * So invalid to check for his parent hence return SUCCESS */
    if (pu1ProtocolDirID->i4_Length == 4)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Rmon2ChkForPrntEntryAndExFeature:  Parent Id\r\n");
        return OSIX_SUCCESS;
    }
    PrntProtocolDirID.pu1_OctetList = au1PrntProtocolDirID;
    PrntProtocolDirParams.pu1_OctetList = au1PrntProtocolDirParams;
    if (pu1ProtocolDirID->i4_Length >= 30)
    {
        return OSIX_FAILURE;
    }

    RMON2_GET_PRNT_ENTRY_ID (PrntProtocolDirID.pu1_OctetList,
                             pu1ProtocolDirID->pu1_OctetList,
                             pu1ProtocolDirID->i4_Length);

    PrntProtocolDirID.i4_Length = pu1ProtocolDirID->i4_Length - 4;
    if (pu1ProtocolDirParams->i4_Length >= 5)
    {
        return OSIX_FAILURE;
    }

    RMON2_GET_PRNT_ENTRY_PARAMS (PrntProtocolDirParams.pu1_OctetList,
                                 pu1ProtocolDirParams->pu1_OctetList,
                                 pu1ProtocolDirParams->i4_Length);

    PrntProtocolDirParams.i4_Length = pu1ProtocolDirParams->i4_Length - 1;

    if ((pProtDir =
         Rmon2PDirGetProtDirEntry (&PrntProtocolDirID, &PrntProtocolDirParams))
        == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Rmon2ChkForPrntEntryAndExFeature:  No Parent Id\r\n");
        return OSIX_FAILURE;
    }

    if (pProtDir->u1ProtocolDirType == EXTENSIBLE_NO_ADDR_RECOGN ||
        pProtDir->u1ProtocolDirType == EXTENSIBLE_ADDR_RECOGN)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDirGetProtAddrMapConfig                              */
/*                                                                           */
/* Description  : Gives Address Map support configuration for a Protocol     */
/*                                                                           */
/*                                                                           */
/* Input        : pProtocolDirID,pProtocolDirParams                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : NOT_SUPPORTED / SUPPORTED_OFF                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
Rmon2PDirGetProtAddrMapConfig (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                               tSNMP_OCTET_STRING_TYPE * pProtocolDirParams)
{

    tProtocolDir       *pProtocolDirEntry;

    pProtocolDirEntry =
        Rmon2PDirGetProtDirEntry (pProtocolDirID, pProtocolDirParams);
    if (NULL == pProtocolDirEntry)
    {
        return NOT_SUPPORTED;
    }
    if (((pProtocolDirID->i4_Length > 8)) &&
        ((pProtocolDirEntry->u1ProtocolDirType == EXTENSIBLE_NO_ADDR_RECOGN) ||
         (pProtocolDirEntry->u1ProtocolDirType ==
          NOT_EXTENSIBLE_NO_ADDR_RECOGN)))
    {
        return NOT_SUPPORTED;
    }
    return SUPPORTED_OFF;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDirGetProtHostConfig                                 */
/*                                                                           */
/* Description  : Gives Host support configuration for a Protocol            */
/*                                                                           */
/*                                                                           */
/* Input        : pProtocolDirID,pProtocolDirParams                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : NOT_SUPPORTED / SUPPORTED_OFF                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
Rmon2PDirGetProtHostConfig (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                            tSNMP_OCTET_STRING_TYPE * pProtocolDirParams)
{
    tProtocolDir       *pProtocolDirEntry;
    UINT1               au1NtwrkProtocolDirID[30];
    UINT1               au1NtwrkProtocolDirParams[5];
    tSNMP_OCTET_STRING_TYPE NtwrkProtocolDirID;
    tSNMP_OCTET_STRING_TYPE NtwrkProtocolDirParams;

    MEMSET (au1NtwrkProtocolDirID, 0, sizeof (au1NtwrkProtocolDirID));
    MEMSET (au1NtwrkProtocolDirParams, 0, sizeof (au1NtwrkProtocolDirParams));

    NtwrkProtocolDirID.pu1_OctetList = au1NtwrkProtocolDirID;
    NtwrkProtocolDirParams.pu1_OctetList = au1NtwrkProtocolDirParams;

    if (pProtocolDirID->i4_Length >= 8)
    {
        MEMCPY (NtwrkProtocolDirID.pu1_OctetList,
                pProtocolDirID->pu1_OctetList, 8);
        MEMCPY (NtwrkProtocolDirParams.pu1_OctetList,
                pProtocolDirParams->pu1_OctetList, 2);

        NtwrkProtocolDirID.i4_Length = 8;
        NtwrkProtocolDirParams.i4_Length = 2;

        pProtocolDirEntry =
            Rmon2PDirGetProtDirEntry (&NtwrkProtocolDirID,
                                      &NtwrkProtocolDirParams);
        if (NULL == pProtocolDirEntry)
        {
            return NOT_SUPPORTED;
        }
        if (pProtocolDirEntry->u1ProtocolDirType == EXTENSIBLE_ADDR_RECOGN)
        {
            return SUPPORTED_OFF;
        }
    }
    return NOT_SUPPORTED;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PDirGetProtMatrixConfig                               */
/*                                                                           */
/* Description  : Gives Matrix support configuration for a Protocol          */
/*                                                                           */
/*                                                                           */
/* Input        : pProtocolDirID,pProtocolDirParams                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : NOT_SUPPORTED / SUPPORTED_OFF                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
Rmon2PDirGetProtMatrixConfig (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                              tSNMP_OCTET_STRING_TYPE * pProtocolDirParams)
{
    tProtocolDir       *pProtocolDirEntry;
    UINT1               au1NtwrkProtocolDirID[30];
    UINT1               au1NtwrkProtocolDirParams[5];
    tSNMP_OCTET_STRING_TYPE NtwrkProtocolDirID;
    tSNMP_OCTET_STRING_TYPE NtwrkProtocolDirParams;

    MEMSET (au1NtwrkProtocolDirID, 0, sizeof (au1NtwrkProtocolDirID));
    MEMSET (au1NtwrkProtocolDirParams, 0, sizeof (au1NtwrkProtocolDirParams));

    NtwrkProtocolDirID.pu1_OctetList = au1NtwrkProtocolDirID;
    NtwrkProtocolDirParams.pu1_OctetList = au1NtwrkProtocolDirParams;

    if (pProtocolDirID->i4_Length >= 8)
    {
        MEMCPY (NtwrkProtocolDirID.pu1_OctetList,
                pProtocolDirID->pu1_OctetList, 8);
        MEMCPY (NtwrkProtocolDirParams.pu1_OctetList,
                pProtocolDirParams->pu1_OctetList, 2);

        NtwrkProtocolDirID.i4_Length = 8;
        NtwrkProtocolDirParams.i4_Length = 2;

        pProtocolDirEntry =
            Rmon2PDirGetProtDirEntry (&NtwrkProtocolDirID,
                                      &NtwrkProtocolDirParams);
        if (NULL == pProtocolDirEntry)
        {
            return NOT_SUPPORTED;
        }
        if (pProtocolDirEntry->u1ProtocolDirType == EXTENSIBLE_ADDR_RECOGN)
        {
            return SUPPORTED_OFF;
        }
    }
    return NOT_SUPPORTED;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2DeleteChildProtocol                                   */
/*                                                                           */
/* Description  : Deletes all the children protocol entries                  */
/*                                                                           */
/*                                                                           */
/* Input        : pProtocolDirID,pProtocolDirParameters                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT4
Rmon2DeleteChildProtocol (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                          tSNMP_OCTET_STRING_TYPE * pProtocolDirParameters)
{
    tProtocolDir       *pProtocolDirEntry = NULL;
    tProtocolDir       *pNextDirEntry = NULL;
    UINT1               au1ProtocolDirID[RMON2_MAX_PROT_DIR_ID_LEN + 1];
    UINT1               au1ProtocolDirParameters[RMON2_MAX_PROT_DIR_PARAM_LEN +
                                                 1];
    tSNMP_OCTET_STRING_TYPE NextDirID;
    tSNMP_OCTET_STRING_TYPE NextDirParams;
    INT4                i4Result;
    INT4                memcmpretval = -1;

    NextDirID.pu1_OctetList = au1ProtocolDirID;

    NextDirParams.pu1_OctetList = au1ProtocolDirParameters;

    pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                  pProtocolDirParameters);

    if (NULL == pProtocolDirEntry)
    {
        return OSIX_FAILURE;
    }
    pNextDirEntry = Rmon2PDirGetNextProtDirIndex (pProtocolDirID,
                                                  pProtocolDirParameters);

    while (pNextDirEntry != NULL)
    {
        MEMSET (au1ProtocolDirID, 0, sizeof (au1ProtocolDirID));
        MEMSET (au1ProtocolDirParameters, 0, sizeof (au1ProtocolDirParameters));
        NextDirID.i4_Length = (INT4) pNextDirEntry->u4ProtocolDirIDLen;
        NextDirParams.i4_Length =
            (INT4) pNextDirEntry->u4ProtocolDirParametersLen;
        MEMCPY (NextDirID.pu1_OctetList, pNextDirEntry->au1ProtocolDirID,
                pNextDirEntry->u4ProtocolDirIDLen);
        MEMCPY (NextDirParams.pu1_OctetList,
                pNextDirEntry->au1ProtocolDirParameters,
                pNextDirEntry->u4ProtocolDirParametersLen);

        if (pProtocolDirEntry->u4ProtocolDirIDLen <
            pNextDirEntry->u4ProtocolDirIDLen)
        {
            memcmpretval = MEMCMP (pProtocolDirEntry->au1ProtocolDirID,
                                   pNextDirEntry->au1ProtocolDirID,
                                   pProtocolDirEntry->u4ProtocolDirIDLen);

            if (memcmpretval == 0)
            {
#ifdef NPAPI_WANTED
                Rmonv2FsRMONv2DisableProtocol (pNextDirEntry->
                                               u4ProtocolDirLocalIndex);
#endif

                Rmon2MainUpdateProtoChgs (pNextDirEntry->
                                          u4ProtocolDirLocalIndex);
#ifdef DSMON_WANTED
                DsmonMainUpdateProtoChgs (pNextDirEntry->
                                          u4ProtocolDirLocalIndex);
#endif
                i4Result = Rmon2PDirDelProtDirEntry (pNextDirEntry);

                if (i4Result == OSIX_FAILURE)
                {
                    RMON2_TRC (RMON2_DEBUG_TRC,
                               "RBTreeRemove Failure for Protocol Directory "
                               "entry \n");
                    return OSIX_FAILURE;
                }

            }
        }

        pNextDirEntry =
            Rmon2PDirGetNextProtDirIndex (&NextDirID, &NextDirParams);

    }

    return OSIX_SUCCESS;

}
