/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * $Id: rmn2util.c,v 1.8 2015/07/14 03:10:39 siva Exp $
 *
* Description: This file contains the utility procedures used by 
 *              RMON2 module.
 *
 *******************************************************************/

#include "rmn2inc.h"

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBCmpPktInfo
 *
 *    DESCRIPTION      : RBTree compare function for Rmon2PktInfoRBTree. 
 *                       Index of this table -
 *                       u4IfIndex
 *                       u4ProtoNLIndex
 *                       u4ProtoALIndex
 *                       SrcIp
 *                       DstIp
 *
 *    INPUT            : pRBElem1 - Pointer to the PktInfo node1
 *                       pRBElem2 - Pointer to the PktInfo  node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : RMON2_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       RMON2_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       RMON2_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBCmpPktInfo (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tRmon2PktInfo      *pPktInfoEntry1 = (tRmon2PktInfo *) pRBElem1;
    tRmon2PktInfo      *pPktInfoEntry2 = (tRmon2PktInfo *) pRBElem2;
    INT4                i4MemcmpRetVal = RMON2_INVALID_VALUE;

    /* First Index - Interface Index */
    if (pPktInfoEntry1->PktHdr.u4IfIndex > pPktInfoEntry2->PktHdr.u4IfIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pPktInfoEntry1->PktHdr.u4IfIndex <
             pPktInfoEntry2->PktHdr.u4IfIndex)
    {
        return RMON2_RB_LESS;
    }

    /* Second Index - EtherType */
    if (pPktInfoEntry1->PktHdr.u2EtherType > pPktInfoEntry2->PktHdr.u2EtherType)
    {
        return RMON2_RB_GREATER;
    }
    else if (pPktInfoEntry1->PktHdr.u2EtherType <
             pPktInfoEntry2->PktHdr.u2EtherType)
    {
        return RMON2_RB_LESS;
    }

    /* Third Index - IpProtocol */
    if (pPktInfoEntry1->PktHdr.u1IpProtocol >
        pPktInfoEntry2->PktHdr.u1IpProtocol)
    {
        return RMON2_RB_GREATER;
    }
    else if (pPktInfoEntry1->PktHdr.u1IpProtocol <
             pPktInfoEntry2->PktHdr.u1IpProtocol)
    {
        return RMON2_RB_LESS;
    }

    /* Forth Index - Source IP */
    i4MemcmpRetVal = MEMCMP (&(pPktInfoEntry1->PktHdr.SrcIp),
                             &(pPktInfoEntry2->PktHdr.SrcIp), sizeof (tIpAddr));
    if (i4MemcmpRetVal > 0)
    {
        return RMON2_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return RMON2_RB_LESS;
    }

    /* Fifth Index - Destination IP */
    i4MemcmpRetVal = MEMCMP (&(pPktInfoEntry1->PktHdr.DstIp),
                             &(pPktInfoEntry2->PktHdr.DstIp), sizeof (tIpAddr));
    if (i4MemcmpRetVal > 0)
    {
        return RMON2_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return RMON2_RB_LESS;
    }

    /* Sixth Index - Source Port */
    if (pPktInfoEntry1->PktHdr.u2SrcPort > pPktInfoEntry2->PktHdr.u2SrcPort)
    {
        return RMON2_RB_GREATER;
    }
    else if (pPktInfoEntry1->PktHdr.u2SrcPort <
             pPktInfoEntry2->PktHdr.u2SrcPort)
    {
        return RMON2_RB_LESS;
    }

    /* Seventh Index - Destination Port */
    if (pPktInfoEntry1->PktHdr.u2DstPort > pPktInfoEntry2->PktHdr.u2DstPort)
    {
        return RMON2_RB_GREATER;
    }
    else if (pPktInfoEntry1->PktHdr.u2DstPort <
             pPktInfoEntry2->PktHdr.u2DstPort)
    {
        return RMON2_RB_LESS;
    }

    /* Eighth Index - DSCP value */
    if (pPktInfoEntry1->PktHdr.u1DSCP > pPktInfoEntry2->PktHdr.u1DSCP)
    {
        return RMON2_RB_GREATER;
    }
    else if (pPktInfoEntry1->PktHdr.u1DSCP < pPktInfoEntry2->PktHdr.u1DSCP)
    {
        return RMON2_RB_LESS;
    }
    return RMON2_RB_EQUAL;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBCmpProtocolDir
 *
 *    DESCRIPTION      : RBTree compare function for ProtocolDirRBTree. This
 *                       RB Tree is useful for SNMP operations on Protocol
 *                       Directory Table.
 *                       Index of this table -
 *                       au1ProtocolDirID  
 *                       au1ProtocolDirParameters
 *                   
 *    INPUT            : pRBElem1 - Pointer to the ProtocolDir node1
 *                       pRBElem2 - Pointer to the ProtocolDir node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : RMON2_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       RMON2_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       RMON2_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBCmpProtocolDir (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tProtocolDir       *pProtocolDirEntry1 = (tProtocolDir *) pRBElem1;
    tProtocolDir       *pProtocolDirEntry2 = (tProtocolDir *) pRBElem2;
    INT4                i4MemcmpRetVal = RMON2_INVALID_VALUE;

    /* First Index - Protocol ID */

    if (pProtocolDirEntry1->u4ProtocolDirIDLen >
        pProtocolDirEntry2->u4ProtocolDirIDLen)
    {
        return RMON2_RB_GREATER;
    }
    else if (pProtocolDirEntry1->u4ProtocolDirIDLen <
             pProtocolDirEntry2->u4ProtocolDirIDLen)
    {
        return RMON2_RB_LESS;
    }
    i4MemcmpRetVal = MEMCMP (pProtocolDirEntry1->au1ProtocolDirID,
                             pProtocolDirEntry2->au1ProtocolDirID,
                             pProtocolDirEntry1->u4ProtocolDirIDLen);

    if (i4MemcmpRetVal > 0)
    {
        return RMON2_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return RMON2_RB_LESS;
    }

    /* Second Index - Protocol Params */
    if (pProtocolDirEntry1->u4ProtocolDirParametersLen >
        pProtocolDirEntry2->u4ProtocolDirParametersLen)
    {
        return RMON2_RB_GREATER;
    }
    else if (pProtocolDirEntry1->u4ProtocolDirParametersLen <
             pProtocolDirEntry2->u4ProtocolDirParametersLen)
    {
        return RMON2_RB_LESS;
    }

    i4MemcmpRetVal = MEMCMP (pProtocolDirEntry1->au1ProtocolDirParameters,
                             pProtocolDirEntry2->au1ProtocolDirParameters,
                             pProtocolDirEntry2->u4ProtocolDirParametersLen);

    if (i4MemcmpRetVal > 0)
    {
        return RMON2_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return RMON2_RB_LESS;
    }
    else
    {
        return RMON2_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBCmpProtocolDistCtrl
 *
 *    DESCRIPTION      : RBTree compare function for ProtocolDistControlRBTree. 
 *                       This RB Tree is useful for SNMP operations on Protocol
 *                       Distribution Control Table.
 *                       Index of this table -
 *                       u4ProtocolDistControlIndex
 *
 *    INPUT            : pRBElem1 - Pointer to the ProtocolDistControl node1
 *                       pRBElem2 - Pointer to the ProtocolDistControl node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : RMON2_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       RMON2_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       RMON2_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBCmpProtocolDistCtrl (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tProtocolDistControl *pPDistCtrlEntry1 = (tProtocolDistControl *) pRBElem1;
    tProtocolDistControl *pPDistCtrlEntry2 = (tProtocolDistControl *) pRBElem2;

    /* First Index - PDist Ctrl Index */
    if (pPDistCtrlEntry1->u4ProtocolDistControlIndex >
        pPDistCtrlEntry2->u4ProtocolDistControlIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pPDistCtrlEntry1->u4ProtocolDistControlIndex <
             pPDistCtrlEntry2->u4ProtocolDistControlIndex)
    {
        return RMON2_RB_LESS;
    }
    else
    {
        return RMON2_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBCmpProtocolDistStats
 *
 *    DESCRIPTION      : RBTree compare function for ProtocolDistStatsRBTree. 
 *                       This RB Tree is useful for SNMP operations on Protocol
 *                       Distribution Statistics Table.
 *                       Index of this table -
 *                       u4ProtocolDistControlIndex
 *                       u4ProtocolDirLocalIndex
 *
 *    INPUT            : pRBElem1 - Pointer to the ProtocolDistStats node1
 *                       pRBElem2 - Pointer to the ProtocolDistStats node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : RMON2_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       RMON2_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       RMON2_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBCmpProtocolDistStats (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tProtocolDistStats *pProtDistStatsEntry1 = (tProtocolDistStats *) pRBElem1;
    tProtocolDistStats *pProtDistStatsEntry2 = (tProtocolDistStats *) pRBElem2;

    /* First Index - PDist Ctrl Index */
    if (pProtDistStatsEntry1->u4ProtocolDistControlIndex >
        pProtDistStatsEntry2->u4ProtocolDistControlIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pProtDistStatsEntry1->u4ProtocolDistControlIndex <
             pProtDistStatsEntry2->u4ProtocolDistControlIndex)
    {
        return RMON2_RB_LESS;
    }

    /*  Second Index - Protocol Dir Local Index */
    if (pProtDistStatsEntry1->u4ProtocolDirLocalIndex >
        pProtDistStatsEntry2->u4ProtocolDirLocalIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pProtDistStatsEntry1->u4ProtocolDirLocalIndex <
             pProtDistStatsEntry2->u4ProtocolDirLocalIndex)
    {
        return RMON2_RB_LESS;
    }
    else
    {
        return RMON2_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBCmpAddressMapCtrl
 *
 *    DESCRIPTION      : RBTree compare function for AddressMapControlRBTree. This
 *                       RB Tree is useful for SNMP operations on Address Map
 *                       Control Table.
 *                       Index of this table -
 *                       u4AddressMapControlIndex
 *
 *    INPUT            : pRBElem1 - Pointer to the AddressMapControl node1
 *                       pRBElem2 - Pointer to the AddressMapControl node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : RMON2_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       RMON2_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       RMON2_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBCmpAddressMapCtrl (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tAddressMapControl *pAddrMapCtrlEntry1 = (tAddressMapControl *) pRBElem1;
    tAddressMapControl *pAddrMapCtrlEntry2 = (tAddressMapControl *) pRBElem2;

    /* First Index - Address Map Ctrl Index */
    if (pAddrMapCtrlEntry1->u4AddressMapControlIndex >
        pAddrMapCtrlEntry2->u4AddressMapControlIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pAddrMapCtrlEntry1->u4AddressMapControlIndex <
             pAddrMapCtrlEntry2->u4AddressMapControlIndex)
    {
        return RMON2_RB_LESS;
    }
    else
    {
        return RMON2_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBCmpAddressMap
 *
 *    DESCRIPTION      : RBTree compare function for AddressMapRBTree. 
 *                       This RB Tree is useful for SNMP operations on 
 *                       Address Map Table.
 *                       Index of this table -
 *                       u4AddressMapTimeMark
 *                       u4ProtocolDirLocalIndex
 *                       au1AddressMapNetworkAddress
 *                       u4AddressMapSource
 *
 *    INPUT            : pRBElem1 - Pointer to the AddressMap node1
 *                       pRBElem2 - Pointer to the AddressMap node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : RMON2_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       RMON2_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       RMON2_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBCmpAddressMap (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tAddressMap        *pAddrMapEntry1 = (tAddressMap *) pRBElem1;
    tAddressMap        *pAddrMapEntry2 = (tAddressMap *) pRBElem2;
    INT4                i4MemcmpRetVal = RMON2_INVALID_VALUE;

    /* First Index - Time Mark - Ignore */

    /* Second Index - Protocol Dir Local Index */
    if (pAddrMapEntry1->u4ProtocolDirLocalIndex >
        pAddrMapEntry2->u4ProtocolDirLocalIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pAddrMapEntry1->u4ProtocolDirLocalIndex <
             pAddrMapEntry2->u4ProtocolDirLocalIndex)
    {
        return RMON2_RB_LESS;
    }

    /* Third Index - Address Map Network Address */
    i4MemcmpRetVal = MEMCMP (&(pAddrMapEntry1->AddressMapNetworkAddress),
                             &(pAddrMapEntry2->AddressMapNetworkAddress),
                             sizeof (tIpAddr));
    if (i4MemcmpRetVal > 0)
    {
        return RMON2_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return RMON2_RB_LESS;
    }

    /* Fourth Index - Address Map Source */
    if (pAddrMapEntry1->u4AddressMapSource > pAddrMapEntry2->u4AddressMapSource)
    {
        return RMON2_RB_GREATER;
    }
    else if (pAddrMapEntry1->u4AddressMapSource <
             pAddrMapEntry2->u4AddressMapSource)
    {
        return RMON2_RB_LESS;
    }
    else
    {
        return RMON2_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBCmpHlHostCtrl
 *
 *    DESCRIPTION      : RBTree compare function for HlHostControlRBTree. This
 *                       RB Tree is useful for SNMP operations on Higher layer 
 *                       Host Control Table.
 *                       Index of this table -
 *                       u4HlHostControlIndex
 *
 *    INPUT            : pRBElem1 - Pointer to the HlHostControl node1
 *                       pRBElem2 - Pointer to the HlHostControl node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : RMON2_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       RMON2_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       RMON2_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBCmpHlHostCtrl (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tHlHostControl     *pHlHostCtrlEntry1 = (tHlHostControl *) pRBElem1;
    tHlHostControl     *pHlHostCtrlEntry2 = (tHlHostControl *) pRBElem2;

    /* First Index - Hl Host Ctrl Index */
    if (pHlHostCtrlEntry1->u4HlHostControlIndex >
        pHlHostCtrlEntry2->u4HlHostControlIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pHlHostCtrlEntry1->u4HlHostControlIndex <
             pHlHostCtrlEntry2->u4HlHostControlIndex)
    {
        return RMON2_RB_LESS;
    }
    else
    {
        return RMON2_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBCmpNlHost
 *
 *    DESCRIPTION      : RBTree compare function for NlHostRBTree. 
 *                       This RB Tree is useful for SNMP operations on 
 *                       Network Layer Host Table.
 *                       Index of this table -
 *                       u4HlHostControlIndex
 *                       u4NlHostTimeMark
 *                       u4ProtocolDirLocalIndex
 *                       NlHostAddress
 *
 *    INPUT            : pRBElem1 - Pointer to the NlHost node1
 *                       pRBElem2 - Pointer to the NlHost node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : RMON2_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       RMON2_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       RMON2_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBCmpNlHost (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tNlHost            *pNlHostEntry1 = (tNlHost *) pRBElem1;
    tNlHost            *pNlHostEntry2 = (tNlHost *) pRBElem2;
    INT4                i4MemcmpRetVal = RMON2_INVALID_VALUE;

    /* First Index - Hl Host Ctrl Index */
    if (pNlHostEntry1->u4HlHostControlIndex >
        pNlHostEntry2->u4HlHostControlIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pNlHostEntry1->u4HlHostControlIndex <
             pNlHostEntry2->u4HlHostControlIndex)
    {
        return RMON2_RB_LESS;
    }

    /* Second Index - Time Mark - Ignore */

    /* Third Index - Protocol Dir Local Index */
    if (pNlHostEntry1->u4ProtocolDirLocalIndex >
        pNlHostEntry2->u4ProtocolDirLocalIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pNlHostEntry1->u4ProtocolDirLocalIndex <
             pNlHostEntry2->u4ProtocolDirLocalIndex)
    {
        return RMON2_RB_LESS;
    }

    /* Fourth Index - Nl Host Address */
    i4MemcmpRetVal = MEMCMP (&(pNlHostEntry1->NlHostAddress),
                             &(pNlHostEntry2->NlHostAddress), sizeof (tIpAddr));
    if (i4MemcmpRetVal > 0)
    {
        return RMON2_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return RMON2_RB_LESS;
    }
    else
    {
        return RMON2_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBCmpAlHost
 *
 *    DESCRIPTION      : RBTree compare function for AlHostRBTree. 
 *                       This RB Tree is useful for SNMP operations on 
 *                       Application Layer Host Table.
 *                       Index of this table -
 *                       u4HlHostControlIndex
 *                       u4AlHostTimeMark
 *                       u4ProtocolDirLocalIndex
 *                       NlHostAddress
 *
 *    INPUT            : pRBElem1 - Pointer to the AlHost node1
 *                       pRBElem2 - Pointer to the AlHost node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : RMON2_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       RMON2_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       RMON2_RB_GREATER - if node1 key is greater than i
 *                                         node2's key.
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBCmpAlHost (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tAlHost            *pAlHostEntry1 = (tAlHost *) pRBElem1;
    tAlHost            *pAlHostEntry2 = (tAlHost *) pRBElem2;
    INT4                i4MemcmpRetVal = RMON2_INVALID_VALUE;

    /* First Index - Hl Host Ctrl Index */
    if (pAlHostEntry1->u4HlHostControlIndex >
        pAlHostEntry2->u4HlHostControlIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pAlHostEntry1->u4HlHostControlIndex <
             pAlHostEntry2->u4HlHostControlIndex)
    {
        return RMON2_RB_LESS;
    }

    /* Second Index - Time Mark - Ignore */

    /* Third Index - Nl Protocol Dir Local Index */
    if (pAlHostEntry1->u4NlProtocolDirLocalIndex >
        pAlHostEntry2->u4NlProtocolDirLocalIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pAlHostEntry1->u4NlProtocolDirLocalIndex <
             pAlHostEntry2->u4NlProtocolDirLocalIndex)
    {
        return RMON2_RB_LESS;
    }

    /* Fourth Index - Nl Host Address */
    i4MemcmpRetVal = MEMCMP (&(pAlHostEntry1->NlHostAddress),
                             &(pAlHostEntry2->NlHostAddress), sizeof (tIpAddr));
    if (i4MemcmpRetVal > 0)
    {
        return RMON2_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return RMON2_RB_LESS;
    }

    /* Fifth Index - Al Protocol Dir Local Index */
    if (pAlHostEntry1->u4AlProtocolDirLocalIndex >
        pAlHostEntry2->u4AlProtocolDirLocalIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pAlHostEntry1->u4AlProtocolDirLocalIndex <
             pAlHostEntry2->u4AlProtocolDirLocalIndex)
    {
        return RMON2_RB_LESS;
    }
    else
    {
        return RMON2_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBCmpHlMatrixCtrl
 *
 *    DESCRIPTION      : RBTree compare function for HlMatrixControlRBTree. This
 *                       RB Tree is useful for SNMP operations on Higher layer 
 *                       Matrix Control Table.
 *                       Index of this table -
 *                       u4HlMatrixControlIndex
 *
 *    INPUT            : pRBElem1 - Pointer to the HlMatrixControl node1
 *                       pRBElem2 - Pointer to the HlMatrixControl node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : RMON2_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       RMON2_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       RMON2_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBCmpHlMatrixCtrl (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry1 = (tHlMatrixControl *) pRBElem1;
    tHlMatrixControl   *pHlMatrixCtrlEntry2 = (tHlMatrixControl *) pRBElem2;

    /* First Index - Hl Matrix Ctrl Index */
    if (pHlMatrixCtrlEntry1->u4HlMatrixControlIndex >
        pHlMatrixCtrlEntry2->u4HlMatrixControlIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pHlMatrixCtrlEntry1->u4HlMatrixControlIndex <
             pHlMatrixCtrlEntry2->u4HlMatrixControlIndex)
    {
        return RMON2_RB_LESS;
    }
    else
    {
        return RMON2_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBCmpNlMatrixSD
 *
 *    DESCRIPTION      : RBTree compare function for NlMatrixSDRBTree. 
 *                       This RB Tree is useful for SNMP operations on 
 *                       Network Layer Matrix SD Table.
 *                       Index of this table -
 *                       u4HlMatrixControlIndex
 *                       u4NlMatrixSDTimeMark
 *                       u4ProtocolDirLocalIndex
 *                       NlMatrixSDSourceAddress
 *                       NlMatrixSDDestAddress
 *
 *    INPUT            : pRBElem1 - Pointer to the NlMatrixSD node1
 *                       pRBElem2 - Pointer to the NlMatrixSD node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : RMON2_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       RMON2_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       RMON2_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBCmpNlMatrixSD (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tNlMatrixSD        *pNlMatrixSDEntry1 = (tNlMatrixSD *) pRBElem1;
    tNlMatrixSD        *pNlMatrixSDEntry2 = (tNlMatrixSD *) pRBElem2;
    INT4                i4MemcmpRetVal = RMON2_INVALID_VALUE;

    /* First Index - Hl Matrix Ctrl Index */
    if (pNlMatrixSDEntry1->u4HlMatrixControlIndex >
        pNlMatrixSDEntry2->u4HlMatrixControlIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pNlMatrixSDEntry1->u4HlMatrixControlIndex <
             pNlMatrixSDEntry2->u4HlMatrixControlIndex)
    {
        return RMON2_RB_LESS;
    }

    /* Second Index - Time Mark - Ignore */

    /* Third Index - Protocol Dir Local Index */
    if (pNlMatrixSDEntry1->u4ProtocolDirLocalIndex >
        pNlMatrixSDEntry2->u4ProtocolDirLocalIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pNlMatrixSDEntry1->u4ProtocolDirLocalIndex <
             pNlMatrixSDEntry2->u4ProtocolDirLocalIndex)
    {
        return RMON2_RB_LESS;
    }

    /* Fourth Index - Nl Matrix SD Source Address */
    i4MemcmpRetVal = MEMCMP (&(pNlMatrixSDEntry1->NlMatrixSDSourceAddress),
                             &(pNlMatrixSDEntry2->NlMatrixSDSourceAddress),
                             sizeof (tIpAddr));
    if (i4MemcmpRetVal > 0)
    {
        return RMON2_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return RMON2_RB_LESS;
    }

    /* Fifth Index - Nl Matrix SD Dest Address */
    i4MemcmpRetVal = MEMCMP (&(pNlMatrixSDEntry1->NlMatrixSDDestAddress),
                             &(pNlMatrixSDEntry2->NlMatrixSDDestAddress),
                             sizeof (tIpAddr));
    if (i4MemcmpRetVal > 0)
    {
        return RMON2_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return RMON2_RB_LESS;
    }
    else
    {
        return RMON2_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBCmpNlMatrixDS
 *
 *    DESCRIPTION      : RBTree compare function for NlMatrixDSRBTree. 
 *                       This RB Tree is useful for SNMP operations on 
 *                       Network Layer Matrix DS Table.
 *                       Index of this table -
 *                       u4HlMatrixControlIndex
 *                       u4NlMatrixSDTimeMark
 *                       u4ProtocolDirLocalIndex
 *                       NlMatrixSDDestAddress
 *                       NlMatrixSDSourceAddress
 *
 *    INPUT            : pRBElem1 - Pointer to the NlMatrixSD node1
 *                       pRBElem2 - Pointer to the NlMatrixSD node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : RMON2_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       RMON2_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       RMON2_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBCmpNlMatrixDS (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tNlMatrixSD        *pNlMatrixSDEntry1 = (tNlMatrixSD *) pRBElem1;
    tNlMatrixSD        *pNlMatrixSDEntry2 = (tNlMatrixSD *) pRBElem2;
    INT4                i4MemcmpRetVal = RMON2_INVALID_VALUE;

    /* First Index - Hl Matrix Ctrl Index */
    if (pNlMatrixSDEntry1->u4HlMatrixControlIndex >
        pNlMatrixSDEntry2->u4HlMatrixControlIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pNlMatrixSDEntry1->u4HlMatrixControlIndex <
             pNlMatrixSDEntry2->u4HlMatrixControlIndex)
    {
        return RMON2_RB_LESS;
    }

    /* Second Index - Time Mark - Ignore */

    /* Third Index - Protocol Dir Local Index */
    if (pNlMatrixSDEntry1->u4ProtocolDirLocalIndex >
        pNlMatrixSDEntry2->u4ProtocolDirLocalIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pNlMatrixSDEntry1->u4ProtocolDirLocalIndex <
             pNlMatrixSDEntry2->u4ProtocolDirLocalIndex)
    {
        return RMON2_RB_LESS;
    }

    /* Fourth Index - Nl Matrix SD Dest Address */
    i4MemcmpRetVal = MEMCMP (&(pNlMatrixSDEntry1->NlMatrixSDDestAddress),
                             &(pNlMatrixSDEntry2->NlMatrixSDDestAddress),
                             sizeof (tIpAddr));
    if (i4MemcmpRetVal > 0)
    {
        return RMON2_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return RMON2_RB_LESS;
    }

    /* Fifth Index - Nl Matrix SD Source Address */
    i4MemcmpRetVal = MEMCMP (&(pNlMatrixSDEntry1->NlMatrixSDSourceAddress),
                             &(pNlMatrixSDEntry2->NlMatrixSDSourceAddress),
                             sizeof (tIpAddr));
    if (i4MemcmpRetVal > 0)
    {
        return RMON2_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return RMON2_RB_LESS;
    }
    else
    {
        return RMON2_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBCmpAlMatrixSD
 *
 *    DESCRIPTION      : RBTree compare function for AlMatrixSDRBTree. 
 *                       This RB Tree is useful for SNMP operations on 
 *                       Application Layer Matrix SD Table.
 *                       Index of this table -
 *                       u4HlMatrixControlIndex
 *                       u4AlMatrixSDTimeMark
 *                       u4NlProtocolDirLocalIndex
 *                       NlMatrixSDSourceAddress
 *                       NlMatrixSDDestAddress
 *                       u4AlProtocolDirLocalIndex 
 *
 *    INPUT            : pRBElem1 - Pointer to the AlMatrixSD node1
 *                       pRBElem2 - Pointer to the AlMatrixSD node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : RMON2_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       RMON2_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       RMON2_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBCmpAlMatrixSD (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tAlMatrixSD        *pAlMatrixSDEntry1 = (tAlMatrixSD *) pRBElem1;
    tAlMatrixSD        *pAlMatrixSDEntry2 = (tAlMatrixSD *) pRBElem2;
    INT4                i4MemcmpRetVal = RMON2_INVALID_VALUE;

    /* First Index - Hl Matrix Ctrl Index */
    if (pAlMatrixSDEntry1->u4HlMatrixControlIndex >
        pAlMatrixSDEntry2->u4HlMatrixControlIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pAlMatrixSDEntry1->u4HlMatrixControlIndex <
             pAlMatrixSDEntry2->u4HlMatrixControlIndex)
    {
        return RMON2_RB_LESS;
    }

    /* Second Index - Time Mark - Ignore */

    /* Third Index - Nl Protocol Dir Local Index */
    if (pAlMatrixSDEntry1->u4NlProtocolDirLocalIndex >
        pAlMatrixSDEntry2->u4NlProtocolDirLocalIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pAlMatrixSDEntry1->u4NlProtocolDirLocalIndex <
             pAlMatrixSDEntry2->u4NlProtocolDirLocalIndex)
    {
        return RMON2_RB_LESS;
    }

    /* Fourth Index - Nl Matrix SD Source Address */
    i4MemcmpRetVal = MEMCMP (&(pAlMatrixSDEntry1->NlMatrixSDSourceAddress),
                             &(pAlMatrixSDEntry2->NlMatrixSDSourceAddress),
                             sizeof (tIpAddr));
    if (i4MemcmpRetVal > 0)
    {
        return RMON2_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return RMON2_RB_LESS;
    }

    /* Fifth Index - Nl Matrix SD Dest Address */
    i4MemcmpRetVal = MEMCMP (&(pAlMatrixSDEntry1->NlMatrixSDDestAddress),
                             &(pAlMatrixSDEntry2->NlMatrixSDDestAddress),
                             sizeof (tIpAddr));
    if (i4MemcmpRetVal > 0)
    {
        return RMON2_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return RMON2_RB_LESS;
    }

    /* Sixth Index - Al Protocol Dir Local Index */
    if (pAlMatrixSDEntry1->u4AlProtocolDirLocalIndex >
        pAlMatrixSDEntry2->u4AlProtocolDirLocalIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pAlMatrixSDEntry1->u4AlProtocolDirLocalIndex <
             pAlMatrixSDEntry2->u4AlProtocolDirLocalIndex)
    {
        return RMON2_RB_LESS;
    }
    else
    {
        return RMON2_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBCmpAlMatrixDS
 *
 *    DESCRIPTION      : RBTree compare function for AlMatrixDSRBTree. 
 *                       This RB Tree is useful for SNMP operations on 
 *                       Network Layer Matrix DS Table.
 *                       Index of this table -
 *                       u4HlMatrixControlIndex
 *                       u4AlMatrixSDTimeMark
 *                       u4NlProtocolDirLocalIndex
 *                       NlMatrixSDDestAddress
 *                       NlMatrixSDSourceAddress
 *                       u4AlProtocolDirLocalIndex
 *
 *    INPUT            : pRBElem1 - Pointer to the AlMatrixSD node1
 *                       pRBElem2 - Pointer to the AlMatrixSD node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : RMON2_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       RMON2_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       RMON2_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBCmpAlMatrixDS (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tAlMatrixSD        *pAlMatrixSDEntry1 = (tAlMatrixSD *) pRBElem1;
    tAlMatrixSD        *pAlMatrixSDEntry2 = (tAlMatrixSD *) pRBElem2;
    INT4                i4MemcmpRetVal = RMON2_INVALID_VALUE;

    /* First Index - Hl Matrix Ctrl Index */
    if (pAlMatrixSDEntry1->u4HlMatrixControlIndex >
        pAlMatrixSDEntry2->u4HlMatrixControlIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pAlMatrixSDEntry1->u4HlMatrixControlIndex <
             pAlMatrixSDEntry2->u4HlMatrixControlIndex)
    {
        return RMON2_RB_LESS;
    }

    /* Second Index - Time Mark - Ignore */

    /* Third Index - Nl Protocol Dir Local Index */
    if (pAlMatrixSDEntry1->u4NlProtocolDirLocalIndex >
        pAlMatrixSDEntry2->u4NlProtocolDirLocalIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pAlMatrixSDEntry1->u4NlProtocolDirLocalIndex <
             pAlMatrixSDEntry2->u4NlProtocolDirLocalIndex)
    {
        return RMON2_RB_LESS;
    }

    /* Fourth Index -  Nl Matrix SD Dest Address */
    i4MemcmpRetVal = MEMCMP (&(pAlMatrixSDEntry1->NlMatrixSDDestAddress),
                             &(pAlMatrixSDEntry2->NlMatrixSDDestAddress),
                             sizeof (tIpAddr));
    if (i4MemcmpRetVal > 0)
    {
        return RMON2_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return RMON2_RB_LESS;
    }

    /* Fifth Index - Nl Matrix SD Source Address */
    i4MemcmpRetVal = MEMCMP (&(pAlMatrixSDEntry1->NlMatrixSDSourceAddress),
                             &(pAlMatrixSDEntry2->NlMatrixSDSourceAddress),
                             sizeof (tIpAddr));
    if (i4MemcmpRetVal > 0)
    {
        return RMON2_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return RMON2_RB_LESS;
    }

    /* Sixth Index - Al Protocol Dir Local Index */
    if (pAlMatrixSDEntry1->u4AlProtocolDirLocalIndex >
        pAlMatrixSDEntry2->u4AlProtocolDirLocalIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pAlMatrixSDEntry1->u4AlProtocolDirLocalIndex <
             pAlMatrixSDEntry2->u4AlProtocolDirLocalIndex)
    {
        return RMON2_RB_LESS;
    }
    else
    {
        return RMON2_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBCmpNlMatrixTopNCtrl
 *
 *    DESCRIPTION      : RBTree compare function for NlMatrixTopNControlRBTree. 
 *                       This RB Tree is useful for SNMP operations on Network
 *                       Layer Matrix TopN Control Table.
 *                       Index of this table -
 *                       u4NlMatrixTopNControlIndex
 *
 *    INPUT            : pRBElem1 - Pointer to the NlMatrixTopNControl node1
 *                       pRBElem2 - Pointer to the NlMatrixTopNControl node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : RMON2_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       RMON2_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       RMON2_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBCmpNlMatrixTopNCtrl (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry1 =
        (tNlMatrixTopNControl *) pRBElem1;
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry2 =
        (tNlMatrixTopNControl *) pRBElem2;

    /* First Index - Nl Matrix TopN Ctrl Index */
    if (pNlMatrixTopNCtrlEntry1->u4NlMatrixTopNControlIndex >
        pNlMatrixTopNCtrlEntry2->u4NlMatrixTopNControlIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pNlMatrixTopNCtrlEntry1->u4NlMatrixTopNControlIndex <
             pNlMatrixTopNCtrlEntry2->u4NlMatrixTopNControlIndex)
    {
        return RMON2_RB_LESS;
    }
    else
    {
        return RMON2_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBCmpNlMatrixTopN
 *
 *    DESCRIPTION      : RBTree compare function for NlMatrixTopNControlRBTree. 
 *                       This RB Tree is useful for SNMP operations on Network
 *                       Layer Matrix TopN Control Table.
 *                       Index of this table -
 *                       u4NlMatrixTopNControlIndex
 *                       u4NlMatrixTopNIndex
 *
 *    INPUT            : pRBElem1 - Pointer to the NlMatrixTopN node1
 *                       pRBElem2 - Pointer to the NlMatrixTopN node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : RMON2_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       RMON2_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       RMON2_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBCmpNlMatrixTopN (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tNlMatrixTopN      *pNlMatrixTopNEntry1 = (tNlMatrixTopN *) pRBElem1;
    tNlMatrixTopN      *pNlMatrixTopNEntry2 = (tNlMatrixTopN *) pRBElem2;

    /* First Index - Nl Matrix TopN Ctrl Index */
    if (pNlMatrixTopNEntry1->u4NlMatrixTopNControlIndex >
        pNlMatrixTopNEntry2->u4NlMatrixTopNControlIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pNlMatrixTopNEntry1->u4NlMatrixTopNControlIndex <
             pNlMatrixTopNEntry2->u4NlMatrixTopNControlIndex)
    {
        return RMON2_RB_LESS;
    }

    /* Second Index - Nl Matrix TopN Index */
    if (pNlMatrixTopNEntry1->u4NlMatrixTopNIndex >
        pNlMatrixTopNEntry2->u4NlMatrixTopNIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pNlMatrixTopNEntry1->u4NlMatrixTopNIndex <
             pNlMatrixTopNEntry2->u4NlMatrixTopNIndex)
    {
        return RMON2_RB_LESS;
    }
    else
    {
        return RMON2_RB_EQUAL;
    }

}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBCmpAlMatrixTopNCtrl
 *
 *    DESCRIPTION      : RBTree compare function for AlMatrixTopNControlRBTree. 
 *                       This RB Tree is useful for SNMP operations on Application
 *                       Layer Matrix TopN Control Table.
 *                       Index of this table -
 *                       u4AlMatrixTopNControlIndex
 *
 *    INPUT            : pRBElem1 - Pointer to the AlMatrixTopNControl node1
 *                       pRBElem2 - Pointer to the AlMatrixTopNControl node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : RMON2_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       RMON2_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       RMON2_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBCmpAlMatrixTopNCtrl (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry1 =
        (tAlMatrixTopNControl *) pRBElem1;
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry2 =
        (tAlMatrixTopNControl *) pRBElem2;

    /* First Index - Al Matrix TopN Ctrl Index */
    if (pAlMatrixTopNCtrlEntry1->u4AlMatrixTopNControlIndex >
        pAlMatrixTopNCtrlEntry2->u4AlMatrixTopNControlIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pAlMatrixTopNCtrlEntry1->u4AlMatrixTopNControlIndex <
             pAlMatrixTopNCtrlEntry2->u4AlMatrixTopNControlIndex)
    {
        return RMON2_RB_LESS;
    }
    else
    {
        return RMON2_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBCmpAlMatrixTopN
 *
 *    DESCRIPTION      : RBTree compare function for AlMatrixTopNRBTree. 
 *                       This RB Tree is useful for SNMP operations on Application
 *                       Layer Matrix TopN Control Table.
 *                       Index of this table -
 *                       u4AlMatrixTopNControlIndex
 *                       u4AlMatrixTopNIndex
 *
 *    INPUT            : pRBElem1 - Pointer to the AlMatrixTopN node1
 *                       pRBElem2 - Pointer to the AlMatrixTopN node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : RMON2_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       RMON2_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       RMON2_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBCmpAlMatrixTopN (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tAlMatrixTopN      *pAlMatrixTopNEntry1 = (tAlMatrixTopN *) pRBElem1;
    tAlMatrixTopN      *pAlMatrixTopNEntry2 = (tAlMatrixTopN *) pRBElem2;

    /* First Index - Al Matrix TopN Ctrl Index */
    if (pAlMatrixTopNEntry1->u4AlMatrixTopNControlIndex >
        pAlMatrixTopNEntry2->u4AlMatrixTopNControlIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pAlMatrixTopNEntry1->u4AlMatrixTopNControlIndex <
             pAlMatrixTopNEntry2->u4AlMatrixTopNControlIndex)
    {
        return RMON2_RB_LESS;
    }

    /* Second Index - Al Matrix TopN Index */
    if (pAlMatrixTopNEntry1->u4AlMatrixTopNIndex >
        pAlMatrixTopNEntry2->u4AlMatrixTopNIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pAlMatrixTopNEntry1->u4AlMatrixTopNIndex <
             pAlMatrixTopNEntry2->u4AlMatrixTopNIndex)
    {
        return RMON2_RB_LESS;
    }
    else
    {
        return RMON2_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBCmpUsrHistoryCtrl
 *
 *    DESCRIPTION      : RBTree compare function for UsrHistoryControlRBTree. 
 *                       This RB Tree is useful for SNMP operations on User
 *                       History Control Table.
 *                       Index of this table -
 *                       u4UsrHistoryControlIndex
 *
 *    INPUT            : pRBElem1 - Pointer to the UsrHistoryControl node1
 *                       pRBElem2 - Pointer to the UsrHistoryControl node2
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : RMON2_RB_EQUAL   - if all the keys matched for both
 *                                         the nodes.
 *                       RMON2_RB_LESS    - if node1 key is less than node2's
 *                                         key.
 *                       RMON2_RB_GREATER - if node1 key is greater than node2's
 *                                         key.
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBCmpUsrHistoryCtrl (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry1 = (tUsrHistoryControl *) pRBElem1;
    tUsrHistoryControl *pUsrHistoryCtrlEntry2 = (tUsrHistoryControl *) pRBElem2;

    /* First Index - Usr History Ctrl Index */
    if (pUsrHistoryCtrlEntry1->u4UsrHistoryControlIndex >
        pUsrHistoryCtrlEntry2->u4UsrHistoryControlIndex)
    {
        return RMON2_RB_GREATER;
    }
    else if (pUsrHistoryCtrlEntry1->u4UsrHistoryControlIndex <
             pUsrHistoryCtrlEntry2->u4UsrHistoryControlIndex)
    {
        return RMON2_RB_LESS;
    }
    else
    {
        return RMON2_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBFreePktInfo
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the Rmon2PktInfoRBTree.
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBFreePktInfo (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
#ifdef NPAPI_WANTED
        Rmonv2FsRMONv2RemoveFlowStats (((tRmon2PktInfo *) pRBElem)->PktHdr);
#endif
        MemReleaseMemBlock (RMON2_PKTINFO_POOL, (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBFreeProtocolDir
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the ProtocolDirRBTree.
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBFreeProtocolDir (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (RMON2_PROTDIR_POOL, (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBFreeProtocolDistCtrl
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the ProtocolDistControlRBTree.
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBFreeProtocolDistCtrl (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (RMON2_PDISTCTRL_POOL, (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBFreeProtocolDistStats
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the ProtocolDistStatsRBTree.
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBFreeProtocolDistStats (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (RMON2_PDISTSTATS_POOL, (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBFreeAddressMapCtrl
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the AddressMapControlRBTree.
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBFreeAddressMapCtrl (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (RMON2_ADDRMAPCTRL_POOL, (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBFreeAddressMap
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the AddressMapRBTree.
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBFreeAddressMap (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (RMON2_ADDRMAP_POOL, (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBFreeHlHostCtrl
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the HlHostControlRBTree.
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBFreeHlHostCtrl (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (RMON2_HLHOSTCTRL_POOL, (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBFreeNlHost
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the NlHostRBTree.
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBFreeNlHost (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (RMON2_NLHOST_POOL, (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBFreeAlHost
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the AlHostRBTree.
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBFreeAlHost (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (RMON2_ALHOST_POOL, (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBFreeHlMatrixCtrl
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the HlMatrixControlRBTree.
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBFreeHlMatrixCtrl (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (RMON2_HLMATRIXCTRL_POOL, (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBFreeNlMatrixSD
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the NlMatrixSDRBTree.
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBFreeNlMatrixSD (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (RMON2_NLMATRIXSD_POOL, (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBFreeAlMatrixSD
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the AlMatrixSDRBTree.
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBFreeAlMatrixSD (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (RMON2_ALMATRIXSD_POOL, (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBFreeNlMatrixTopNCtrl
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the NlMatrixTopNControlRBTree.
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBFreeNlMatrixTopNCtrl (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (RMON2_NLMATRIX_TOPNCTRL_POOL, (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBFreeNlMatrixTopN
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the NlMatrixTopNRBTree.
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBFreeNlMatrixTopN (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (RMON2_NLMATRIX_TOPN_POOL, (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBFreeAlMatrixTopNCtrl
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the AlMatrixTopNControlRBTree.
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBFreeAlMatrixTopNCtrl (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (RMON2_ALMATRIX_TOPNCTRL_POOL, (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBFreeAlMatrixTopN
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the AlMatrixTopNRBTree.
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBFreeAlMatrixTopN (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (RMON2_ALMATRIX_TOPN_POOL, (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2UtlRBFreeUsrHistoryCtrl
 *
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the UsrHistoryControlRBTree.
 *
 *    INPUT            : pRBElem - pointer to the node to be freed.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
Rmon2UtlRBFreeUsrHistoryCtrl (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (RMON2_USRHISTCTRL_POOL, (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *    FUNCTION NAME    : Rmon2UtlFormDataSourceOid                      *
 *                                                                      *
 *    DESCRIPTION      : Rmon2UtlFormDataSourceOid function  forms the  *
 *                       DataSource Oid from the interface number       *
 *                                                                      *
 *    INPUT            : pDataSourceOid, u4IfIndex                      *
 *                                                                      *
 *    OUTPUT           : None                                           *
 *                                                                      *
 *    RETURNS          : None                                           *
 ************************************************************************/
PUBLIC VOID
Rmon2UtlFormDataSourceOid (tSNMP_OID_TYPE * pDataSourceOid, UINT4 u4IfIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY, " FUNC: ENTRY Rmon2UtlFormDataSourceOid \n");

    MEMCPY (pDataSourceOid->pu4_OidList,
            gRmon2Globals.InterfaceOid.pu4_OidList,
            (gRmon2Globals.InterfaceOid.u4_Length * sizeof (UINT4)));
    pDataSourceOid->pu4_OidList[gRmon2Globals.InterfaceOid.u4_Length] =
        u4IfIndex;
    pDataSourceOid->u4_Length = (gRmon2Globals.InterfaceOid.u4_Length + 1);

    RMON2_TRC (RMON2_FN_EXIT, " FUNC: EXIT Rmon2UtlFormDataSourceOid \n");
}

/************************************************************************
 *    FUNCTION NAME    : Rmon2UtlSetDataSourceOid                       *
 *                                                                      *
 *    DESCRIPTION      : Rmon2UtlSetDataSourceOid function extracts the *
 *                       interface number from the DataSource Oid       *
 *                                                                      *
 *    INPUT            : pDataSourceOid, u4IfIndex                      *
 *                                                                      *
 *    OUTPUT           : None                                           *
 *                                                                      *
 *    RETURNS          : None                                           *
 ************************************************************************/
PUBLIC VOID
Rmon2UtlSetDataSourceOid (tSNMP_OID_TYPE * pDataSourceOid, UINT4 *pu4IfIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY, " FUNC: ENTRY Rmon2UtlSetDataSourceOid \n");

    if (pDataSourceOid != NULL)
    {
        *pu4IfIndex = *(pDataSourceOid->pu4_OidList +
                        gRmon2Globals.InterfaceOid.u4_Length);
    }

    RMON2_TRC (RMON2_FN_EXIT, " FUNC: EXIT Rmon2UtlSetDataSourceOid \n");
}

/************************************************************************
 *    FUNCTION NAME    : RmonTestDataSourceOid                          *
 *                                                                      *
 *    DESCRIPTION      : RmonTestDataSourceOid function validates the   *
 *                       DataSource Oid                                 *
 *                                                                      *
 *    INPUT            : pDataSourceOid                                 *
 *                                                                      *
 *    OUTPUT           : None                                           *
 *                                                                      *
 *    RETURNS          : TRUE / FALSE                                   *
 ************************************************************************/
PUBLIC INT1
Rmon2UtlTestDataSourceOid (tSNMP_OID_TYPE * pDataSourceOid)
{
    RMON2_TRC (RMON2_FN_ENTRY, " FUNC: ENTRY Rmon2UtlTestDataSourceOid\n");

    if ((pDataSourceOid->u4_Length ==
         (gRmon2Globals.InterfaceOid.u4_Length + 1)) &&
        (MEMCMP (pDataSourceOid->pu4_OidList,
                 gRmon2Globals.InterfaceOid.pu4_OidList,
                 gRmon2Globals.InterfaceOid.u4_Length) == 0) &&
        (CfaValidateIfIndex
         ((UINT4) pDataSourceOid->pu4_OidList
          [gRmon2Globals.InterfaceOid.u4_Length]) == CFA_SUCCESS))
    {
        return TRUE;
    }

    RMON2_TRC (RMON2_FN_EXIT, " FUNC: EXIT Rmon2UtlTestDataSourceOid\n");
    return FALSE;
}

/******************************************************************************
 * Function Name   :  rmon2_alloc_oid                                         *
 * Description     :  This Function Allocates the Memory for an Object of OID *
 *                    Type.                                                   *
 * Input           :  i4_size: Size of Oid.                                   *
 *                                                                            *
 * Output          :  temp   : Allocation of Memory for the Oid for Given Size*
 * Returns         :  Pointer of tSNMP_OID_TYPE.                              *
 *****************************************************************************/
tSNMP_OID_TYPE     *
rmon2_alloc_oid (INT4 i4_size)
{
    tSNMP_OID_TYPE     *temp = NULL;
#ifdef SNMP_2_WANTED
    temp = alloc_oid (i4_size);
#else
    UNUSED_PARAM (i4_size);
#endif

    return temp;
}

/******************************************************************************
 * Function Name   :  rmon2_free_oid                                          *
 * Description     :  This Function Frees the Memory for an Object of OID     *
 *                    Type.                                                   *
 * Input           :  temp : The Ptr which is to be freed.                    *
 * Output          :  Frees the Given Oid Pointer.                            *
 * Returns         :  None                                                    *
 *****************************************************************************/
void
rmon2_free_oid (tSNMP_OID_TYPE * temp)
{
#ifdef SNMP_2_WANTED
    if (temp == NULL)
    {
        return;
    }
    free_oid (temp);
#else
    UNUSED_PARAM (temp);
#endif
    return;
}

/****************************************************************************
 * Function    :  rmon2_allocmem_octetstring                                *
 * Description :  This Function Allocates Memory for Octet String Type.     *
 * Input       :  i4_size : The Size of Octet String.                       *
 * Output      :  Allocated Octet String Pointer.                           *
 * Returns     :  Pointer to an Octet String Variable.                      *
 ***************************************************************************/

tSNMP_OCTET_STRING_TYPE *
rmon2_allocmem_octetstring (INT4 i4_size)
{
    tSNMP_OCTET_STRING_TYPE *temp = NULL;
#ifdef SNMP_2_WANTED
    temp = allocmem_octetstring (i4_size);
#else
    UNUSED_PARAM (i4_size);
#endif
    return temp;
}

/****************************************************************************
 * Function    :  rmon2_free_octetstring                                    *   
 * Description :  This Function Frees Memory for Octet String Type Var.     *
 * Input       :  temp : The Pointer to the Octet String.                   *
 * Output      :  Frees the Octet String Pointer.                           *
 * Returns     :  None.                                                     *
 ***************************************************************************/
void
rmon2_free_octetstring (tSNMP_OCTET_STRING_TYPE * temp)
{
#ifdef SNMP_2_WANTED
    if (temp == NULL)
    {
        return;
    }
    free_octetstring (temp);
#else
    UNUSED_PARAM (temp);
#endif
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2UtlDelPDistCtrlEntries                                */
/*                                                                           */
/* Description  : This routine deletes all control table entries and the     */
/*                corresponding data table entries in Protocol distribution  */
/*                group                                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
Rmon2UtlDelPDistCtrlEntries (VOID)
{
    tProtocolDistControl *pProtDistCtrlEntry = NULL;
    UINT4               u4CtlIndex = 0;
    INT4                i4Result = 0;
    pProtDistCtrlEntry = Rmon2PDistGetNextCtrlIndex (u4CtlIndex);

    RMON2_TRC (RMON2_FN_ENTRY, " FUNC: ENTRY Rmon2UtlDelPDistCtrlEntries \n");

    while (pProtDistCtrlEntry != NULL)
    {
        u4CtlIndex = pProtDistCtrlEntry->u4ProtocolDistControlIndex;

        /* Deleting Corresponding entries in protocol statistics table */
        Rmon2PDistDelDataEntries (u4CtlIndex);

        i4Result = Rmon2PDistDelCtrlEntry (pProtDistCtrlEntry);

        if (i4Result == OSIX_FAILURE)
        {
            RMON2_TRC (RMON2_DEBUG_TRC,
                       "SNMP Failure: Invalid ProtDist Control Index "
                       "RBTreeRemove failed - Protocol Distribution Control Table\n");
            return SNMP_FAILURE;
        }
        pProtDistCtrlEntry = Rmon2PDistGetNextCtrlIndex (u4CtlIndex);

    }
    RMON2_TRC (RMON2_FN_EXIT, " FUNC: EXIT Rmon2UtlDelPDistCtrlEntries \n");

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2UtlDelAddrMapCtrlDataEntries                          */
/*                                                                           */
/* Description  : This routine deletes all control table entries and the     */
/*                corresponding data table entries in Address Map            */
/*                group                                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
Rmon2UtlDelAddrMapCtrlEntries (VOID)
{
    tAddressMapControl *pAddrMapCtrlEntry = NULL;
    UINT4               u4CtlIndex = 0;
    INT4                i4Result = 0;
    RMON2_TRC (RMON2_FN_ENTRY, " FUNC: ENTRY Rmon2UtlDelAddrMapCtrlEntries \n");

    pAddrMapCtrlEntry = Rmon2AddrMapGetNextCtrlIndex (u4CtlIndex);

    while (pAddrMapCtrlEntry != NULL)
    {
        u4CtlIndex = pAddrMapCtrlEntry->u4AddressMapControlIndex;

        /* Deleting Corresponding entries in Address map table */
        Rmon2AddrMapDelDataEntries (pAddrMapCtrlEntry->
                                    u4AddressMapControlDataSource);

        i4Result = Rmon2AddrMapDelCtrlEntry (pAddrMapCtrlEntry);

        if (i4Result == OSIX_FAILURE)
        {
            RMON2_TRC (RMON2_DEBUG_TRC,
                       "SNMP Failure: Invalid AddrMap Control Index "
                       "RBTreeRemove failed - Address Map Control Table\n");
            return SNMP_FAILURE;
        }
        pAddrMapCtrlEntry = Rmon2AddrMapGetNextCtrlIndex (u4CtlIndex);
    }

    RMON2_TRC (RMON2_FN_EXIT, " FUNC: EXIT Rmon2UtlDelAddrMapCtrlEntries \n");
    return SNMP_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2UtlDelHostCtrlDataEntries                             */
/*                                                                           */
/* Description  : This routine deletes all control table entries and the     */
/*                corresponding data table entries in HlHost                 */
/*                group                                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
Rmon2UtlDelHostCtrlEntries (VOID)
{
    tHlHostControl     *pHlHostCtrlEntry = NULL;
    UINT4               u4CtlIndex = 0;
    INT4                i4Result = 0;

    RMON2_TRC (RMON2_FN_ENTRY, " FUNC: ENTRY Rmon2UtlDelHostCtrlEntries \n");
    pHlHostCtrlEntry = Rmon2HlHostGetNextCtrlIndex (u4CtlIndex);

    while (pHlHostCtrlEntry != NULL)
    {
        u4CtlIndex = pHlHostCtrlEntry->u4HlHostControlIndex;

        /* Deleting Corresponding entries in NlHost and AlHost tables */
        Rmon2HostDelDataEntries (u4CtlIndex);

        i4Result = Rmon2HlHostDelCtrlEntry (pHlHostCtrlEntry);

        if (i4Result == OSIX_FAILURE)
        {
            RMON2_TRC (RMON2_DEBUG_TRC,
                       "SNMP Failure: Invalid HlHost Control Index "
                       "RBTreeRemove failed - HlHost Control Table\n");
            return SNMP_FAILURE;
        }
        pHlHostCtrlEntry = Rmon2HlHostGetNextCtrlIndex (u4CtlIndex);
    }

    RMON2_TRC (RMON2_FN_EXIT, " FUNC: EXIT Rmon2UtlDelHostCtrlEntries \n");
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2UtlDelMatrixCtrlDataEntries                           */
/*                                                                           */
/* Description  : This routine deletes all control table entries and the     */
/*                corresponding data table entries in HlMatrix               */
/*                group                                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
Rmon2UtlDelMatrixCtrlEntries (VOID)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;
    UINT4               u4CtlIndex = 0;
    INT4                i4Result = 0;

    RMON2_TRC (RMON2_FN_ENTRY, " FUNC: ENTRY Rmon2UtlDelMatrixCtrlEntries \n");
    pHlMatrixCtrlEntry = Rmon2HlMatrixGetNextCtrlIndex (u4CtlIndex);

    while (pHlMatrixCtrlEntry != NULL)
    {
        u4CtlIndex = pHlMatrixCtrlEntry->u4HlMatrixControlIndex;

        /* Deleting Corresponding entries in NlMatrixSD,DS,
         * AlMatrixSD,DS and TopN tables */

        Rmon2NlMatrixTopNNotifyCtlChgs (u4CtlIndex, DESTROY);
        Rmon2AlMatrixTopNNotifyCtlChgs (u4CtlIndex, DESTROY);
        Rmon2MatrixDelDataEntries (u4CtlIndex);

        i4Result = Rmon2HlMatrixDelCtrlEntry (pHlMatrixCtrlEntry);

        if (i4Result == OSIX_FAILURE)
        {
            RMON2_TRC (RMON2_DEBUG_TRC,
                       "SNMP Failure: Invalid HlMatrix Control Index "
                       "RBTreeRemove failed - HlMatrix Control Table\n");
            return SNMP_FAILURE;
        }
        pHlMatrixCtrlEntry = Rmon2HlMatrixGetNextCtrlIndex (u4CtlIndex);
    }

    RMON2_TRC (RMON2_FN_EXIT, " FUNC: EXIT Rmon2UtlDelMatrixCtrlEntries \n");
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2UtlDelUsrHistCtrlDataEntries                          */
/*                                                                           */
/* Description  : This routine deletes all control table entries and the     */
/*                corresponding data table entries in User History           */
/*                group                                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
Rmon2UtlDelUsrHistCtrlEntries (VOID)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;
    UINT4               u4CtlIndex = 0;
    INT4                i4Result = 0;

    RMON2_TRC (RMON2_FN_ENTRY, " FUNC: ENTRY Rmon2UtlDelUsrHistCtrlEntries \n");
    pUsrHistoryCtrlEntry = Rmon2UsrHistGetNextCtrlIndex (u4CtlIndex);

    while (pUsrHistoryCtrlEntry != NULL)
    {
        u4CtlIndex = pUsrHistoryCtrlEntry->u4UsrHistoryControlIndex;

        /* Deleting Corresponding entries in User History object 
         * and user history tables */
        Rmon2UsrHistDelDataEntry (pUsrHistoryCtrlEntry->
                                  u4UsrHistoryControlIndex);
        Rmon2UsrHistDelObjEntry (pUsrHistoryCtrlEntry->
                                 u4UsrHistoryControlIndex);

        i4Result = Rmon2UsrHistDelCtrlEntry (pUsrHistoryCtrlEntry);

        if (i4Result == OSIX_FAILURE)
        {
            RMON2_TRC (RMON2_DEBUG_TRC,
                       "SNMP Failure: Invalid UserHistory Control Index "
                       "RBTreeRemove failed - User History Control Table\n");
            return SNMP_FAILURE;
        }

        pUsrHistoryCtrlEntry = Rmon2UsrHistGetNextCtrlIndex (u4CtlIndex);
    }

    RMON2_TRC (RMON2_FN_EXIT, " FUNC: EXIT Rmon2UtlDelAddrMapCtrlEntries \n");
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2UtlDelAllCtrlEntries                                  */
/*                                                                           */
/* Description  : This routine deletes all control table entries and the     */
/*                corresponding data table entries                           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
Rmon2UtlDelAllCtrlEntries (VOID)
{

    RMON2_TRC (RMON2_FN_ENTRY, " FUNC: ENTRY Rmon2UtlDelAllCtrlEntries \n");
    /* Deleting control table entries and correponding data table entries
     * in Protocol distribution group */

    if (Rmon2UtlDelPDistCtrlEntries () == SNMP_FAILURE)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Failure in Deletion of Control and data table entries in"
                   "protocol distribution group \n");
        return SNMP_FAILURE;
    }

    /* Deleting control table entries and correponding data table entries
     * in Address map group */

    if (Rmon2UtlDelAddrMapCtrlEntries () == SNMP_FAILURE)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Failure in Deletion of Control and data table entries in"
                   "address map group \n");
        return SNMP_FAILURE;
    }

    /* Deleting control table entries and correponding Nl and Al Host table 
     * entries in HlHost group */

    if (Rmon2UtlDelHostCtrlEntries () == SNMP_FAILURE)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Failure in Deletion of Control and data table entries in"
                   "HlHost group \n");
        return SNMP_FAILURE;
    }

    /* Deleting control table entries and correponding Nl and Al matrix table
     * entries in HlMatrix group */

    if (Rmon2UtlDelMatrixCtrlEntries () == SNMP_FAILURE)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Failure in Deletion of Control and data table entries in"
                   "HlMatrix group \n");
        return SNMP_FAILURE;
    }

    /* Deleting control table entries and correponding user history object 
     * user history table entries in User history group */

    if (Rmon2UtlDelUsrHistCtrlEntries () == SNMP_FAILURE)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Failure in Deletion of Control and data table entries in"
                   "user history group \n");
        return SNMP_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT, " FUNC: EXIT Rmon2UtlDelAllCtrlEntries \n");

    return SNMP_SUCCESS;
}
