/********************************************************************
* Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
*
* $Id: fsrmn2lw.c,v 1.7 2015/07/27 07:00:40 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "rmn2inc.h"

extern INT1 nmhSetFsDsmonAdminStatus ARG_LIST ((INT4));

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRmon2Trace
 Input       :  The Indices

                The Object 
                retValFsRmon2Trace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmon2Trace (UINT4 *pu4RetValFsRmon2Trace)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY " "nmhGetFsRmon2Trace \n");

    *pu4RetValFsRmon2Trace = gRmon2Globals.u4Rmon2Trace;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmon2AdminStatus
 Input       :  The Indices

                The Object 
                retValFsRmon2AdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmon2AdminStatus (INT4 *pi4RetValFsRmon2AdminStatus)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY " "nmhGetFsRmon2AdminStatus \n");
    if (!(RMON2_IS_ENABLED ()))
    {
        *pi4RetValFsRmon2AdminStatus = (INT4) RMON2_DISABLE;

        return SNMP_SUCCESS;
    }

    *pi4RetValFsRmon2AdminStatus = (INT4) (gRmon2Globals.u4Rmon2AdminStatus);

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT " "nmhGetFsRmon2AdminStatus \n");

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRmon2Trace
 Input       :  The Indices

                The Object 
                setValFsRmon2Trace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRmon2Trace (UINT4 u4SetValFsRmon2Trace)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY " "nmhSetFsRmon2Trace \n");
    gRmon2Globals.u4Rmon2Trace = u4SetValFsRmon2Trace;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT " "nmhSetFsRmon2Trace \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRmon2AdminStatus
 Input       :  The Indices

                The Object 
                setValFsRmon2AdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRmon2AdminStatus (INT4 i4SetValFsRmon2AdminStatus)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY " "nmhSetFdRmon2AdminStatus \n");

    if (gRmon2Globals.u4Rmon2AdminStatus == (UINT4) i4SetValFsRmon2AdminStatus)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC, "Rmon2 is already in %d State\n",
                    i4SetValFsRmon2AdminStatus);

        return SNMP_SUCCESS;
    }

    if (i4SetValFsRmon2AdminStatus == RMON2_ENABLE)
    {
        if (Rmon2TmrStartTmr (&gRmon2Globals.Rmon2PollTmr, RMON2_POLL_TMR,
                              RMON2_ONE_SECOND) != TMR_SUCCESS)
        {
            RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                       "Tmr Start Failure\n");

            return SNMP_FAILURE;
        }
    }

    if (i4SetValFsRmon2AdminStatus == RMON2_DISABLE)
    {
        Rmon2TmrStopTmr (&gRmon2Globals.Rmon2PollTmr);
        /* Deleting all ctrl and data table entries */
        Rmon2UtlDelAllCtrlEntries ();

#ifdef DSMON_WANTED
        nmhSetFsDsmonAdminStatus (i4SetValFsRmon2AdminStatus);
#endif
    }
    Rmon2PktInfoDelUnusedEntries ();
#ifdef NPAPI_WANTED
    Rmonv2FsRMONv2HwSetStatus ((UINT4) i4SetValFsRmon2AdminStatus);
#endif

    gRmon2Globals.u4Rmon2AdminStatus = (UINT4) i4SetValFsRmon2AdminStatus;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT " "nmhSetFdRmon2AdminStatus \n");
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRmon2Trace
 Input       :  The Indices

                The Object 
                testValFsRmon2Trace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRmon2Trace (UINT4 *pu4ErrorCode, UINT4 u4TestValFsRmon2Trace)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY " "nmhTestv2FsRmon2Trace \n");
    if ((u4TestValFsRmon2Trace > RMON2_MAX_FN_ENTRY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT " "nmhTestv2FsRmon2Trace \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRmon2AdminStatus
 Input       :  The Indices

                The Object 
                testValFsRmon2AdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRmon2AdminStatus (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsRmon2AdminStatus)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY " "nmhTestv2FsRmon2AdminStatus\n");

    if ((i4TestValFsRmon2AdminStatus != RMON2_ENABLE) &&
        (i4TestValFsRmon2AdminStatus != RMON2_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT " "nmhTestv2FsRmon2AdminStatus\n");
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRmon2Trace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRmon2Trace (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRmon2AdminStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRmon2AdminStatus (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
