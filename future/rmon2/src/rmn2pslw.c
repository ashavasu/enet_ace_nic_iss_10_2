/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * $Id: rmn2pslw.c,v 1.8 2015/09/13 10:36:20 siva Exp $
 *
 * Description: This file contains the low level nmh routines for
 *              RMON2 Protocol Distribution module.
 *
 *******************************************************************/
#include "rmn2inc.h"

/* LOW LEVEL Routines for Table : ProtocolDistControlTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceProtocolDistControlTable
Input       :  The Indices
               ProtocolDistControlIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceProtocolDistControlTable (INT4
                                                  i4ProtocolDistControlIndex)
{
    tProtocolDistControl *pProtDistCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "nmhValidateIndexInstanceProtocolDistControlTable \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4ProtocolDistControlIndex < RMON2_ONE) ||
        (i4ProtocolDistControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid ProtocolDistControlIndex \n");
        return SNMP_FAILURE;
    }
    pProtDistCtrlEntry =
        Rmon2PDistGetCtrlEntry ((UINT4) i4ProtocolDistControlIndex);

    if (pProtDistCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of Protocol Distribution Control table"
                    " for Index %d is failed \r\n", i4ProtocolDistControlIndex);
        return SNMP_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT  "
               "nmhValidateIndexInstanceProtocolDistControlTable \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexProtocolDistControlTable
Input       :  The Indices
               ProtocolDistControlIndex
Output      :  The Get First Routines gets the Lexicographicaly
               First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexProtocolDistControlTable (INT4 *pi4ProtocolDistControlIndex)
{

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetFirstIndexProtocolDistControlTable \n");

    return nmhGetNextIndexProtocolDistControlTable (RMON2_ZERO,
                                                    pi4ProtocolDistControlIndex);

}

/****************************************************************************
Function    :  nmhGetNextIndexProtocolDistControlTable
Input       :  The Indices
               ProtocolDistControlIndex
               nextProtocolDistControlIndex
Output      :  The Get Next function gets the Next Index for
               the Index Value given in the Index Values. The
               Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexProtocolDistControlTable (INT4 i4ProtocolDistControlIndex,
                                         INT4 *pi4NextProtocolDistControlIndex)
{
    tProtocolDistControl *pProtDistCtrlEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetNextIndexProtocolDistControlTable \n");

    pProtDistCtrlEntry =
        Rmon2PDistGetNextCtrlIndex ((UINT4) i4ProtocolDistControlIndex);

    if (pProtDistCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of Protocol Distribution Control table"
                    " for Index %d is failed \r\n", i4ProtocolDistControlIndex);

        return SNMP_FAILURE;
    }
    *pi4NextProtocolDistControlIndex =
        (INT4) pProtDistCtrlEntry->u4ProtocolDistControlIndex;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT  nmhGetNextIndexProtocolDistControlTable \n");
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetProtocolDistControlDataSource
Input       :  The Indices
               ProtocolDistControlIndex

               The Object 
               retValProtocolDistControlDataSource
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProtocolDistControlDataSource (INT4 i4ProtocolDistControlIndex,
                                     tSNMP_OID_TYPE *
                                     pRetValProtocolDistControlDataSource)
{
    tProtocolDistControl *pProtDistCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetProtocolDistControlDataSource \n");

    pProtDistCtrlEntry =
        Rmon2PDistGetCtrlEntry ((UINT4) i4ProtocolDistControlIndex);

    if (pProtDistCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of Protocol Distribution Control "
                    "table for Index %d is failed \r\n",
                    i4ProtocolDistControlIndex);
        return SNMP_FAILURE;
    }

    RMON2_GET_DATA_SOURCE_OID (pRetValProtocolDistControlDataSource,
                               pProtDistCtrlEntry->
                               u4ProtocolDistControlDataSource);
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT  nmhGetProtocolDistControlDataSource \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetProtocolDistControlDroppedFrames
Input       :  The Indices
               ProtocolDistControlIndex

               The Object 
               retValProtocolDistControlDroppedFrames
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProtocolDistControlDroppedFrames (INT4 i4ProtocolDistControlIndex,
                                        UINT4
                                        *pu4RetValProtocolDistControlDroppedFrames)
{
    tProtocolDistControl *pProtDistCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetProtocolDistControlDroppedFrames \n");

    pProtDistCtrlEntry =
        Rmon2PDistGetCtrlEntry ((UINT4) i4ProtocolDistControlIndex);

    if (pProtDistCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of Protocol Distribution Control "
                    "table for Index %d is failed \r\n",
                    i4ProtocolDistControlIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValProtocolDistControlDroppedFrames =
        pProtDistCtrlEntry->u4ProtocolDistControlDroppedFrames;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT  nmhGetProtocolDistControlDroppedFrames \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetProtocolDistControlCreateTime
Input       :  The Indices
               ProtocolDistControlIndex

               The Object 
               retValProtocolDistControlCreateTime
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProtocolDistControlCreateTime (INT4 i4ProtocolDistControlIndex,
                                     UINT4
                                     *pu4RetValProtocolDistControlCreateTime)
{
    tProtocolDistControl *pProtDistCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetProtocolDistControlCreateTime \n");

    pProtDistCtrlEntry =
        Rmon2PDistGetCtrlEntry ((UINT4) i4ProtocolDistControlIndex);

    if (pProtDistCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of Protocol Distribution Control "
                    "table for Index %d is failed \r\n",
                    i4ProtocolDistControlIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValProtocolDistControlCreateTime =
        pProtDistCtrlEntry->u4ProtocolDistControlCreateTime;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT  nmhGetProtocolDistControlCreateTime \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetProtocolDistControlOwner
Input       :  The Indices
               ProtocolDistControlIndex

               The Object 
               retValProtocolDistControlOwner
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProtocolDistControlOwner (INT4 i4ProtocolDistControlIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValProtocolDistControlOwner)
{
    tProtocolDistControl *pProtDistCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetProtocolDistControlOwner \n");

    pProtDistCtrlEntry =
        Rmon2PDistGetCtrlEntry ((UINT4) i4ProtocolDistControlIndex);

    if (pProtDistCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of Protocol Distribution Control "
                    "table for Index %d is failed \r\n",
                    i4ProtocolDistControlIndex);
        return SNMP_FAILURE;
    }

    pRetValProtocolDistControlOwner->i4_Length =
        (INT4) STRLEN (pProtDistCtrlEntry->au1ProtocolDistControlOwner);
    MEMCPY (pRetValProtocolDistControlOwner->pu1_OctetList,
            pProtDistCtrlEntry->au1ProtocolDistControlOwner,
            pRetValProtocolDistControlOwner->i4_Length);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT  nmhGetProtocolDistControlOwner \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetProtocolDistControlStatus
Input       :  The Indices
               ProtocolDistControlIndex

               The Object 
               retValProtocolDistControlStatus
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProtocolDistControlStatus (INT4 i4ProtocolDistControlIndex,
                                 INT4 *pi4RetValProtocolDistControlStatus)
{
    tProtocolDistControl *pProtDistCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetProtocolDistControlStatus \n");

    pProtDistCtrlEntry =
        Rmon2PDistGetCtrlEntry ((UINT4) i4ProtocolDistControlIndex);

    if (pProtDistCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of Protocol Distribution Control "
                    "table for Index %d is failed \r\n",
                    i4ProtocolDistControlIndex);
        return SNMP_FAILURE;
    }

    *pi4RetValProtocolDistControlStatus =
        (INT4) pProtDistCtrlEntry->u4ProtocolDistControlStatus;

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT  nmhGetProtocolDistControlStatus \n");
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetProtocolDistControlDataSource
Input       :  The Indices
               ProtocolDistControlIndex

               The Object 
               setValProtocolDistControlDataSource
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetProtocolDistControlDataSource (INT4 i4ProtocolDistControlIndex,
                                     tSNMP_OID_TYPE *
                                     pSetValProtocolDistControlDataSource)
{
    tProtocolDistControl *pProtDistCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhSetProtocolDistControlDataSource \n");

    pProtDistCtrlEntry =
        Rmon2PDistGetCtrlEntry ((UINT4) i4ProtocolDistControlIndex);

    if (pProtDistCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of Protocol Distribution Control "
                    "table for Index %d is failed \r\n",
                    i4ProtocolDistControlIndex);
        return SNMP_FAILURE;
    }

    RMON2_SET_DATA_SOURCE_OID (pSetValProtocolDistControlDataSource,
                               &(pProtDistCtrlEntry->
                                 u4ProtocolDistControlDataSource));
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT  nmhSetProtocolDistControlDataSource \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetProtocolDistControlOwner
Input       :  The Indices
               ProtocolDistControlIndex

               The Object 
               setValProtocolDistControlOwner
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetProtocolDistControlOwner (INT4 i4ProtocolDistControlIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValProtocolDistControlOwner)
{
    UINT4               u4Minimum;
    tProtocolDistControl *pProtDistCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhSetProtocolDistControlOwner \n");

    u4Minimum =
        (UINT4) ((pSetValProtocolDistControlOwner->i4_Length <
                  RMON2_OWNER_LEN) ? pSetValProtocolDistControlOwner->
                 i4_Length : RMON2_OWNER_LEN);

    pProtDistCtrlEntry =
        Rmon2PDistGetCtrlEntry ((UINT4) i4ProtocolDistControlIndex);

    if (pProtDistCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of Protocol Distribution Control "
                    "table for Index %d is failed \r\n",
                    i4ProtocolDistControlIndex);
        return SNMP_FAILURE;
    }

    MEMCPY (pProtDistCtrlEntry->au1ProtocolDistControlOwner,
            pSetValProtocolDistControlOwner->pu1_OctetList, u4Minimum);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT  nmhSetProtocolDistControlOwner \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetProtocolDistControlStatus
Input       :  The Indices
               ProtocolDistControlIndex

               The Object 
               setValProtocolDistControlStatus
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetProtocolDistControlStatus (INT4 i4ProtocolDistControlIndex,
                                 INT4 i4SetValProtocolDistControlStatus)
{

    tPortList          *pVlanPortList = NULL;
    tProtocolDistControl *pProtDistCtrlEntry = NULL;
    INT4                i4Result;
    tCfaIfInfo          IfInfo;
    UINT2               u2VlanId = 0;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhSetProtocolDistControlStatus \n");

    switch (i4SetValProtocolDistControlStatus)
    {
        case CREATE_AND_WAIT:

            pProtDistCtrlEntry =
                Rmon2PDistAddCtrlEntry ((UINT4) i4ProtocolDistControlIndex);
            if (pProtDistCtrlEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC | RMON2_MEM_FAIL,
                           "Adding to Protocol Distribution "
                           "control table is failed\n");
                return SNMP_FAILURE;
            }
            pProtDistCtrlEntry->u4ProtocolDistControlStatus = NOT_READY;
            break;

        case ACTIVE:
            pProtDistCtrlEntry =
                Rmon2PDistGetCtrlEntry ((UINT4) i4ProtocolDistControlIndex);
            if (pProtDistCtrlEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "Get Entry of Protocol Distribution "
                           "control table is failed \n");
                return SNMP_FAILURE;
            }
            if (pProtDistCtrlEntry->u4ProtocolDistControlDataSource ==
                RMON2_ZERO)
            {
                RMON2_TRC (RMON2_DEBUG_TRC, "Data Source is not configured \n");
                return SNMP_FAILURE;
            }
            if (CfaGetIfInfo
                (pProtDistCtrlEntry->u4ProtocolDistControlDataSource,
                 &IfInfo) == CFA_FAILURE)
            {
                /* ifindex is invalid */
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "SNMP Failure: Invalid interface Index \n");
                return SNMP_FAILURE;
            }

            if (((IfInfo.u1IfOperStatus != CFA_IF_UP)
                 || (IfInfo.u1IfAdminStatus != CFA_IF_UP)) &&
                ((gi4MibResStatus != MIB_RESTORE_IN_PROGRESS)))
            {
                RMON2_TRC (RMON2_DEBUG_TRC, "SNMP Failure: "
                           "Interface OperStatus or Admin Status is in "
                           "Down state \n");
                return SNMP_FAILURE;
            }
            pVlanPortList =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pVlanPortList == NULL)
            {
                RMON2_TRC (RMON2_FN_ENTRY,
                           "nmhSetProtocolDistControlStatus: "
                           "Error in allocating memory for pVlanPortList\r\n");
                return SNMP_FAILURE;
            }
            MEMSET (pVlanPortList, 0, sizeof (tPortList));
            if (IfInfo.u1IfType == CFA_L3IPVLAN)
            {
                /* Get the VlanId from interface index */
                if (CfaGetVlanId
                    (pProtDistCtrlEntry->u4ProtocolDistControlDataSource,
                     &u2VlanId) == CFA_FAILURE)
                {
                    RMON2_TRC1 (RMON2_DEBUG_TRC, "SNMP Failure: \
                 Unable to Fetch VlanId of IfIndex %d\n", pProtDistCtrlEntry->u4ProtocolDistControlDataSource);
                    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                    return SNMP_FAILURE;
                }

                if (L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList) ==
                    L2IWF_FAILURE)
                {
                    RMON2_TRC1 (RMON2_DEBUG_TRC, "SNMP Failure: \
                 Get Member Port List for VlanId %d failed \n", u2VlanId);
                    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                    return SNMP_FAILURE;
                }
            }

            pProtDistCtrlEntry->u4ProtocolDistControlCreateTime =
                OsixGetSysUpTime ();
            pProtDistCtrlEntry->u4ProtocolDistControlStatus = ACTIVE;

#ifdef NPAPI_WANTED
            Rmonv2FsRMONv2EnableProbe (pProtDistCtrlEntry->
                                       u4ProtocolDistControlDataSource,
                                       u2VlanId, *pVlanPortList);
#endif


           /*create a socket to trap L3 and My Mac Packet */ 
#ifdef LNXIP4_WANTED 
            Rmon2CreateSocket((UINT4)pProtDistCtrlEntry->u4ProtocolDistControlDataSource);
#endif
            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
            Rmon2PDistPopulateDataEntries (pProtDistCtrlEntry);
            Rmon2PktInfoDelUnusedEntries ();
            break;

        case NOT_IN_SERVICE:

            pProtDistCtrlEntry =
                Rmon2PDistGetCtrlEntry ((UINT4) i4ProtocolDistControlIndex);
            if (pProtDistCtrlEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "Get Entry of Protocol Distribution "
                           "control table is failed \n");
                return SNMP_FAILURE;
            }

            pProtDistCtrlEntry->u4ProtocolDistControlStatus = NOT_IN_SERVICE;

            Rmon2PDistDelDataEntries ((UINT4) i4ProtocolDistControlIndex);
            pVlanPortList =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pVlanPortList == NULL)
            {
                RMON2_TRC (RMON2_FN_ENTRY,
                           "nmhSetProtocolDistControlStatus: "
                           "Error in allocating memory for pVlanPortList\r\n");
                return SNMP_FAILURE;
            }
            MEMSET (pVlanPortList, 0, sizeof (tPortList));

            if (CfaGetIfInfo
                (pProtDistCtrlEntry->u4ProtocolDistControlDataSource,
                 &IfInfo) != CFA_FAILURE)
            {
                if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
                    (CfaGetVlanId
                     (pProtDistCtrlEntry->u4ProtocolDistControlStatus,
                      &u2VlanId) != CFA_FAILURE))
                {
                    L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList);
                }
            }

#ifdef NPAPI_WANTED
            Rmonv2FsRMONv2DisableProbe (pProtDistCtrlEntry->
                                        u4ProtocolDistControlDataSource,
                                        u2VlanId, *pVlanPortList);
#endif
            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
            Rmon2PktInfoDelUnusedEntries ();
            break;
        case DESTROY:

            pProtDistCtrlEntry =
                Rmon2PDistGetCtrlEntry ((UINT4) i4ProtocolDistControlIndex);
            if (pProtDistCtrlEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "Get Entry of Protocol Distribution "
                           "control table is failed \n");
                return SNMP_FAILURE;
            }

            Rmon2PDistDelDataEntries ((UINT4) i4ProtocolDistControlIndex);
#if defined (LNXIP4_WANTED)
            Rmon2CloseSocket (pProtDistCtrlEntry->u4ProtocolDistControlDataSource);
#endif
            i4Result = Rmon2PDistDelCtrlEntry (pProtDistCtrlEntry);

            if (i4Result == OSIX_FAILURE)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "RBTreeRemove Failure for Protocol Distribution "
                           "control entry\n");
                return SNMP_FAILURE;
            }

            Rmon2PktInfoDelUnusedEntries ();
            break;
        default:
            RMON2_TRC (RMON2_DEBUG_TRC, "Unknown option\n");
            return SNMP_FAILURE;
            break;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT  nmhSetProtocolDistControlStatus \n");
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2ProtocolDistControlDataSource
Input       :  The Indices
               ProtocolDistControlIndex

               The Object 
               testValProtocolDistControlDataSource
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ProtocolDistControlDataSource (UINT4 *pu4ErrorCode,
                                        INT4 i4ProtocolDistControlIndex,
                                        tSNMP_OID_TYPE *
                                        pTestValProtocolDistControlDataSource)
{

    tProtocolDistControl *pProtDistCtrlEntry = NULL;
    UINT4               u4DataSource = RMON2_ZERO;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhTestv2ProtocolDistControlDataSource \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4ProtocolDistControlIndex < RMON2_ONE) ||
        (i4ProtocolDistControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid ProtocolDistControlIndex \n");
        return SNMP_FAILURE;
    }

    /* Checking whether the data source is valid or not */
    if (RMON2_TEST_DATA_SOURCE_OID (pTestValProtocolDistControlDataSource))
    {
        RMON2_SET_DATA_SOURCE_OID (pTestValProtocolDistControlDataSource,
                                   &u4DataSource);

        pProtDistCtrlEntry =
            Rmon2PDistGetCtrlEntry ((UINT4) i4ProtocolDistControlIndex);

        /*Checking whether the Control entry exists or not */
        if (pProtDistCtrlEntry != NULL)
        {
            /* Check ProtocolDistStatus is not ACTIVE */
            if (pProtDistCtrlEntry->u4ProtocolDistControlStatus != ACTIVE)
            {
                RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                           "SNMP Success: Data Source id is valid. \n");
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }
            RMON2_TRC1 (RMON2_DEBUG_TRC,
                        "Row Status of Protocol Distribution Control table "
                        "is Active for index : %d \r\n",
                        i4ProtocolDistControlIndex);

            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "Error - Entry is not exist \n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;

    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: INVALID DataSource Oid \n");
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT   nmhTestv2ProtocolDistControlDataSource \n");

    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2ProtocolDistControlOwner
Input       :  The Indices
               ProtocolDistControlIndex

               The Object 
               testValProtocolDistControlOwner
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ProtocolDistControlOwner (UINT4 *pu4ErrorCode,
                                   INT4 i4ProtocolDistControlIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValProtocolDistControlOwner)
{
    tProtocolDistControl *pProtDistCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "nmhTestv2ProtocolDistControlOwner \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4ProtocolDistControlIndex < RMON2_ONE) ||
        (i4ProtocolDistControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid ProtocolDistControlIndex \n");
        return SNMP_FAILURE;
    }

    if ((pTestValProtocolDistControlOwner->i4_Length < RMON2_ZERO) ||
        (pTestValProtocolDistControlOwner->i4_Length > RMON2_OWNER_LEN))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Wrong length for ProtocolDistControlOwner "
                   "object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValProtocolDistControlOwner->pu1_OctetList,
                              pTestValProtocolDistControlOwner->i4_Length)
        == OSIX_FAILURE)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Invalid Characters for Owner string object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    pProtDistCtrlEntry =
        Rmon2PDistGetCtrlEntry ((UINT4) i4ProtocolDistControlIndex);

    if (pProtDistCtrlEntry != NULL)
    {
        if (pProtDistCtrlEntry->u4ProtocolDistControlStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of Protocol Distribution Control table is "
                    "Active for index : %d \r\n", i4ProtocolDistControlIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry Status is not exist \n");
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT  nmhTestv2ProtocolDistControlOwner \n");
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhTestv2ProtocolDistControlStatus
Input       :  The Indices
               ProtocolDistControlIndex

               The Object 
               testValProtocolDistControlStatus
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ProtocolDistControlStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4ProtocolDistControlIndex,
                                    INT4 i4TestValProtocolDistControlStatus)
{
    tProtocolDistControl *pProtDistCtrlEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2ProtocolDistControlStatus \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4ProtocolDistControlIndex < RMON2_ONE) ||
        (i4ProtocolDistControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid ProtocolDistControlIndex \n");
        return SNMP_FAILURE;
    }

    if ((i4TestValProtocolDistControlStatus < ACTIVE) ||
        (i4TestValProtocolDistControlStatus > DESTROY))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Wrong value for ProtocolDistControlStatus "
                   "object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pProtDistCtrlEntry =
        Rmon2PDistGetCtrlEntry ((UINT4) i4ProtocolDistControlIndex);

    if (pProtDistCtrlEntry == NULL)
    {
        if (i4TestValProtocolDistControlStatus == CREATE_AND_WAIT)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "SNMP Failure: Entry is not exist \n");
        return SNMP_FAILURE;

    }
    else
    {
        if ((i4TestValProtocolDistControlStatus == ACTIVE) ||
            (i4TestValProtocolDistControlStatus == NOT_IN_SERVICE) ||
            (i4TestValProtocolDistControlStatus == DESTROY))
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "The given entry is already exist \n");
    }

    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhTestv2ProtocolDistControlStatus \n");
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2ProtocolDistControlTable
Input       :  The Indices
               ProtocolDistControlIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
               check whether dependency is met or not.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2ProtocolDistControlTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : ProtocolDistStatsTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceProtocolDistStatsTable
Input       :  The Indices
               ProtocolDistControlIndex
               ProtocolDirLocalIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceProtocolDistStatsTable (INT4 i4ProtocolDistControlIndex,
                                                INT4 i4ProtocolDirLocalIndex)
{
    tProtocolDistStats *pProtDistStatsEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  "
               "nmhValidateIndexInstanceProtocolDistStatsTable \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4ProtocolDistControlIndex < RMON2_ONE) ||
        (i4ProtocolDistControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid ProtocolDistControlIndex \n");
        return SNMP_FAILURE;
    }

    /* Testing the input for negative value alone is sufficient, because
     * RMON2 allows this configuration till the maximum value of interger 32 */
    if (i4ProtocolDirLocalIndex < RMON2_ONE)
    {
        RMON2_TRC (RMON2_DEBUG_TRC, "SNMP Failure: Unknown Protocol \n");
        return SNMP_FAILURE;
    }

    pProtDistStatsEntry =
        Rmon2PDistGetDataEntry ((UINT4) i4ProtocolDistControlIndex,
                                (UINT4) i4ProtocolDirLocalIndex);

    if (pProtDistStatsEntry == NULL)
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "Get entry of Protocol Distribution Statistics "
                    "table for Index %d ,%d is failed \r\n",
                    i4ProtocolDistControlIndex, i4ProtocolDirLocalIndex);
        return SNMP_FAILURE;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT  "
               "nmhValidateIndexInstanceProtocolDistStatsTable \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFirstIndexProtocolDistStatsTable
Input       :  The Indices
               ProtocolDistControlIndex
               ProtocolDirLocalIndex
Output      :  The Get First Routines gets the Lexicographicaly
               First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexProtocolDistStatsTable (INT4 *pi4ProtocolDistControlIndex,
                                        INT4 *pi4ProtocolDirLocalIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetFirstIndexProtocolDistStatsTable \n");

    return nmhGetNextIndexProtocolDistStatsTable (RMON2_ZERO,
                                                  pi4ProtocolDistControlIndex,
                                                  RMON2_ZERO,
                                                  pi4ProtocolDirLocalIndex);

}

/****************************************************************************
Function    :  nmhGetNextIndexProtocolDistStatsTable
Input       :  The Indices
               ProtocolDistControlIndex
               nextProtocolDistControlIndex
               ProtocolDirLocalIndex
               nextProtocolDirLocalIndex
Output      :  The Get Next function gets the Next Index for
               the Index Value given in the Index Values. The
               Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexProtocolDistStatsTable (INT4 i4ProtocolDistControlIndex,
                                       INT4 *pi4NextProtocolDistControlIndex,
                                       INT4 i4ProtocolDirLocalIndex,
                                       INT4 *pi4NextProtocolDirLocalIndex)
{
    tProtocolDistStats *pProtDistStatsEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNextIndexProtocolDistStatsTable \n");

    pProtDistStatsEntry =
        Rmon2PDistGetNextDataIndex ((UINT4) i4ProtocolDistControlIndex,
                                    (UINT4) i4ProtocolDirLocalIndex);
    if (pProtDistStatsEntry == NULL)
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "Get entry of Protocol Distribution Statistics "
                    "table for Index %d ,%d is failed \r\n",
                    i4ProtocolDistControlIndex, i4ProtocolDirLocalIndex);
        return SNMP_FAILURE;
    }

    *pi4NextProtocolDistControlIndex =
        (INT4) pProtDistStatsEntry->u4ProtocolDistControlIndex;
    *pi4NextProtocolDirLocalIndex =
        (INT4) pProtDistStatsEntry->u4ProtocolDirLocalIndex;

    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT  nmhGetNextIndexProtocolDistStatsTable \n");
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetProtocolDistStatsPkts
Input       :  The Indices
               ProtocolDistControlIndex
               ProtocolDirLocalIndex

               The Object 
               retValProtocolDistStatsPkts
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetProtocolDistStatsPkts (INT4 i4ProtocolDistControlIndex,
                             INT4 i4ProtocolDirLocalIndex,
                             UINT4 *pu4RetValProtocolDistStatsPkts)
{
    tProtocolDistStats *pProtDistStatsEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetProtocolDistStatsPkts \n");

    pProtDistStatsEntry =
        Rmon2PDistGetDataEntry ((UINT4) i4ProtocolDistControlIndex,
                                (UINT4) i4ProtocolDirLocalIndex);

    if (pProtDistStatsEntry == NULL)
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "Get entry of Protocol Distribution Statistics "
                    "table for Index %d ,%d is failed \r\n",
                    i4ProtocolDistControlIndex, i4ProtocolDirLocalIndex);
        return SNMP_FAILURE;
    }

    *pu4RetValProtocolDistStatsPkts =
        pProtDistStatsEntry->u4ProtocolDistStatsPkts;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT  nmhGetProtocolDistStatsPkts \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetProtocolDistStatsOctets
Input       :  The Indices
               ProtocolDistControlIndex
               ProtocolDirLocalIndex

               The Object 
               retValProtocolDistStatsOctets
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetProtocolDistStatsOctets (INT4 i4ProtocolDistControlIndex,
                               INT4 i4ProtocolDirLocalIndex,
                               UINT4 *pu4RetValProtocolDistStatsOctets)
{
    tProtocolDistStats *pProtDistStatsEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetProtocolDistStatsPkts \n");

    pProtDistStatsEntry =
        Rmon2PDistGetDataEntry ((UINT4) i4ProtocolDistControlIndex,
                                (UINT4) i4ProtocolDirLocalIndex);

    if (pProtDistStatsEntry == NULL)
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "Get entry of Protocol Distribution Statistics "
                    "table for Index %d ,%d is failed \r\n",
                    i4ProtocolDistControlIndex, i4ProtocolDirLocalIndex);

        return SNMP_FAILURE;
    }

    *pu4RetValProtocolDistStatsOctets =
        pProtDistStatsEntry->u4ProtocolDistStatsOctets;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT  nmhGetProtocolDistStatsPkts \n");
    return SNMP_SUCCESS;

}
