/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * $Id: rmn2port.c,v 1.5 2012/07/10 05:57:10 siva Exp $
 *
 * Description: This file contains the functional routine of
 *              RMON2 Porting module
 *
 *******************************************************************/
#include "rmn2inc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2FetchHwStats                                          */
/*                                                                           */
/* Description  : This Task informs NPAPI to collect the statistics from     */
/*                hardware for every one second                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
Rmon2FetchHwStats (tRmon2PktInfo * pLVEntry)
{
    tRmon2PktInfo      *pPktInfo = NULL;
    tRmon2Stats         HwStats;
    UINT4               u4EntriesVistited = 0;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY Rmon2FetchHwStats\r\n");

    pPktInfo = Rmon2PktInfoGetNextIndex (&pLVEntry->PktHdr);

    /* If last visited entry reaches end of tree,
     * then reset the entry values to start it from start
     * of the RBTree */

    if (pPktInfo == NULL)
    {
        MEMSET (pLVEntry, 0, sizeof (tRmon2PktInfo));
        pPktInfo = Rmon2PktInfoGetNextIndex (&pLVEntry->PktHdr);
        if (pPktInfo == NULL)
        {
            RMON2_TRC (RMON2_DEBUG_TRC,
                       "Rmon2FetchHwStats No PktInfo entry\r\n");
            return;
        }
    }

    while ((u4EntriesVistited++ < RMON2_MAX_STATS_PER_CYCLE) &&
           (pPktInfo != NULL))
    {
        MEMSET (&HwStats, 0, sizeof (tRmon2Stats));
        MEMCPY (pLVEntry, pPktInfo, sizeof (tRmon2PktInfo));
#ifdef NPAPI_WANTED
        Rmonv2FsRMONv2CollectStats (pPktInfo->PktHdr, &HwStats);
#endif
        Rmon2UpdateDataTables (pPktInfo, &HwStats);

        pPktInfo = Rmon2PktInfoGetNextIndex (&pLVEntry->PktHdr);
    }
    if (pPktInfo == NULL)
    {
        MEMSET (pLVEntry, 0, sizeof (tRmon2PktInfo));
    }
    else
    {
        MEMCPY (pLVEntry, pPktInfo, sizeof (tRmon2PktInfo));
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC:EXIT Rmon2FetchHwStats\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2UpdateDataTables                                      */
/*                                                                           */
/* Description  : This function will be invoked by NPAPI to update data      */
/*                entries for the given tuple                                */
/*                                                                           */
/* Input        : pPktInfo, Counter value                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
Rmon2UpdateDataTables (tRmon2PktInfo * pPktInfo, tRmon2Stats * pHwStats)
{
    tProtocolDistStats *pPDistStats = NULL;
    tNlHost            *pNlHost = NULL;
    tAlHost            *pAlHost = NULL;
    tNlMatrixSD        *pNlMatrixSD = NULL;
    tAlMatrixSD        *pAlMatrixSD = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY Rmon2UpdateDataTables\r\n");

    if (pPktInfo == NULL)
    {
        return;
    }

    /* If Packet and Octet counts are Zero
     * then increment LRU reference and if it reaches
     * the max value then remove PktInfo and associated data entries */
    if ((pHwStats->u8PktCnt.u4LoWord == 0) &&
        (pHwStats->u8OctetCnt.u4LoWord == 0))
    {
        if (++pPktInfo->u4IdleRef >= RMON2_MAX_LRU_REF)
        {
            Rmon2PktInfoDelIdlePkt (pPktInfo);
        }
        return;
    }
    else
    {
        /* Reset Reference Count */
        pPktInfo->u4IdleRef = 0;
    }

    /* Update NlMatrixSD Table */

    pNlMatrixSD = pPktInfo->pNlMatrixSD;

    while (pNlMatrixSD != NULL)
    {
        pNlMatrixSD->NlMatrixSDCurSample.u4NlMatrixSDPkts +=
            pHwStats->u8PktCnt.u4LoWord;
        pNlMatrixSD->NlMatrixSDCurSample.u4NlMatrixSDOctets +=
            pHwStats->u8OctetCnt.u4LoWord;
        pNlMatrixSD->u4NlMatrixSDTimeMark = OsixGetSysUpTime ();
        pNlMatrixSD = pNlMatrixSD->pNextNlMatrixSD;
    }

    /* Update AlMatrixSD Table for AL Protocol */

    pAlMatrixSD = pPktInfo->pAlMatrixSD;

    while (pAlMatrixSD != NULL)
    {
        pAlMatrixSD->AlMatrixSDCurSample.u4AlMatrixSDPkts +=
            pHwStats->u8PktCnt.u4LoWord;
        pAlMatrixSD->AlMatrixSDCurSample.u4AlMatrixSDOctets +=
            pHwStats->u8OctetCnt.u4LoWord;
        pAlMatrixSD->u4AlMatrixSDTimeMark = OsixGetSysUpTime ();
        pAlMatrixSD = pAlMatrixSD->pNextAlMatrixSD;
    }

    /* Update NlHost Table */
    pNlHost = pPktInfo->pOutNlHost;

    while (pNlHost != NULL)
    {
        pNlHost->u4NlHostOutPkts += pHwStats->u8PktCnt.u4LoWord;
        pNlHost->u4NlHostOutOctets += pHwStats->u8OctetCnt.u4LoWord;
        pNlHost->u4NlHostTimeMark = OsixGetSysUpTime ();
        pNlHost = pNlHost->pNextNlHost;
    }

    pNlHost = pPktInfo->pInNlHost;

    while (pNlHost != NULL)
    {
        pNlHost->u4NlHostInPkts += pHwStats->u8PktCnt.u4LoWord;
        pNlHost->u4NlHostInOctets += pHwStats->u8OctetCnt.u4LoWord;
        pNlHost->u4NlHostTimeMark = OsixGetSysUpTime ();
        pNlHost = pNlHost->pNextNlHost;
    }

    /* Update AlHost Table for L4 Protocol */
    pAlHost = pPktInfo->pOutAlHost;

    while (pAlHost != NULL)
    {
        pAlHost->u4AlHostOutPkts += pHwStats->u8PktCnt.u4LoWord;
        pAlHost->u4AlHostOutOctets += pHwStats->u8OctetCnt.u4LoWord;
        pAlHost->u4AlHostTimeMark = OsixGetSysUpTime ();
        pAlHost = pAlHost->pNextAlHost;
    }

    pAlHost = pPktInfo->pInAlHost;

    while (pAlHost != NULL)
    {
        pAlHost->u4AlHostInPkts += pHwStats->u8PktCnt.u4LoWord;
        pAlHost->u4AlHostInOctets += pHwStats->u8OctetCnt.u4LoWord;
        pAlHost->u4AlHostTimeMark = OsixGetSysUpTime ();
        pAlHost = pAlHost->pNextAlHost;
    }

    /* Update Pdist Table for L3 Protocol entry */

    pPDistStats = pPktInfo->pPDist;
    while (pPDistStats != NULL)
    {
        pPDistStats->u4ProtocolDistStatsPkts += pHwStats->u8PktCnt.u4LoWord;
        pPDistStats->u4ProtocolDistStatsOctets += pHwStats->u8OctetCnt.u4LoWord;
        pPDistStats = pPDistStats->pNextPDist;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC:EXIT Rmon2UpdateDataTables\r\n");
}
