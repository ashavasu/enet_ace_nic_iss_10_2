/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 * 
 * $Id: rmn2pcfg.c,v 1.3 2015/11/06 08:17:56 siva Exp $
 *
 * Description: This file contains the functional routine for
 *                RMON2 Probe configuration module
 * 
 *******************************************************************/

#include "rmn2inc.h"

/*****************************************************************************/
/* Function Name      : Rmon2ProbeConfigInitProbeInfo                        */
/*                                                                           */
/* Description        : This function is used to store Probe details         */
/*                                                                           */
/*                                                                           */
/* Input(s)           : -                                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2ProbeConfigInitProbeInfo (VOID)
{
    tSNMP_OCTET_STRING_TYPE HwVersion;
    UINT1               au1HwVersion[RMON2_PROBE_HW_REV_LEN];
    RMON2_PROBE_CAPABILITIES = 0;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY Rmon2ProbeConfigInitProbeInfo \n");
    /* Rmon1 Groups support */
    RMON2_PROBE_CAPABILITIES |= (RMON1_ETHERSTATS | RMON1_HISTORY_CTRL);
    RMON2_PROBE_CAPABILITIES |= (RMON1_ETHERHISTORY | RMON1_ALARM);
    RMON2_PROBE_CAPABILITIES |= (RMON1_HOST | RMON1_HOST_TOPN);
    RMON2_PROBE_CAPABILITIES |= (RMON1_MATRIX | RMON1_EVENT);

    /* Rmon2 Groups support */
    RMON2_PROBE_CAPABILITIES |= (RMON2_PROT_DIR | RMON2_PROT_DIST);
    RMON2_PROBE_CAPABILITIES |= (RMON2_ADDRESS_MAP | RMON2_NLHOST);
    RMON2_PROBE_CAPABILITIES |= (RMON2_NLMATRIX | RMON2_ALHOST);
    RMON2_PROBE_CAPABILITIES |= (RMON2_ALMATRIX | RMON2_USERHIST);
    RMON2_PROBE_CAPABILITIES |= RMON2_PROBE_CONFIG;

    MEMSET (au1HwVersion, 0, RMON2_PROBE_HW_REV_LEN);

    HwVersion.pu1_OctetList = &au1HwVersion[0];
    HwVersion.i4_Length = RMON2_PROBE_HW_REV_LEN;

    /* Getting Probe's Hardware version */
    nmhGetIssHardwareVersion (&HwVersion);
    MEMCPY (gRmon2Globals.au1ProbeHardwareRev, HwVersion.pu1_OctetList,
            HwVersion.i4_Length);
    /* Getting Probe's Software version number */
    MEMCPY (gRmon2Globals.au1ProbeSoftwareRev, IssGetSoftwareVersion (),
            STRLEN (IssGetSoftwareVersion ())); 

    /* Probe Reset Control */
    gRmon2Globals.u4ProbeResetControl = RUNNING;

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2ProbeConfigInitProbeInfo \n");
    return;
}
