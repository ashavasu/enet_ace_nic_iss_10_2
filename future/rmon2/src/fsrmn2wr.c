/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * Description: This file contains SNMP wrapper functions
 *******************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "fsrmn2lw.h"
# include  "fsrmn2wr.h"
# include  "fsrmn2db.h"

VOID
RegisterFSRMN2 ()
{
    SNMPRegisterMib (&fsrmn2OID, &fsrmn2Entry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsrmn2OID, (const UINT1 *) "fsrmn2");
}

VOID
UnRegisterFSRMN2 ()
{
    SNMPUnRegisterMib (&fsrmn2OID, &fsrmn2Entry);
    SNMPDelSysorEntry (&fsrmn2OID, (const UINT1 *) "fsrmn2");
}

INT4
FsRmon2TraceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmon2Trace (&(pMultiData->u4_ULongValue)));
}

INT4
FsRmon2AdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmon2AdminStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsRmon2TraceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRmon2Trace (pMultiData->u4_ULongValue));
}

INT4
FsRmon2AdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRmon2AdminStatus (pMultiData->i4_SLongValue));
}

INT4
FsRmon2TraceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRmon2Trace (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsRmon2AdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRmon2AdminStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRmon2TraceDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRmon2Trace (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRmon2AdminStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRmon2AdminStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
