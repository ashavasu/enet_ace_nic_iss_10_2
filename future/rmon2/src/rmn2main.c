/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009    
 *                                                                   
 * $Id: rmn2main.c,v 1.23 2015/11/09 09:42:47 siva Exp $                                                                   
 * Description:                                                      
 *        1) Creates the timer lists and RMON2 Task.                 
 *        2) On failure to create the timer lists, deletes the       
 *           already created resources.                              
 *        3) Probes and fetches counters periodically.               
 *        4) At the configured intervals, snapshot of                
 *           aperiodic tables will be taken for the periodic         
 *           groups.                                                 
 *                                                                   
 *******************************************************************/
#define _RMN2MAIN_C_
#include "rmn2inc.h"
#ifdef BSDCOMP_SLI_WANTED
INT4               gi4Rmon2PortMapping[RMON2_MAX_VLAN];
PRIVATE UINT1              gi1Rmon2RecvBuff[RMON2_MAX_MTU];
#endif


/************************************************************************
 * Function              : Rmon2MainSetInterfaceOid                         *
 * Input (s)             : None                                         *
 * Output (s)            : None                                         *
 * Returns               : None                                         *
 * Global Variables Used : gRmon2Globals.InterfaceOid                    *
 * Side effects          : None                                         *
 * Action                : Sets the Interface Oid                       *
 ************************************************************************/

PRIVATE INT4
Rmon2MainSetInterfaceOid (VOID)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY Rmon2MainSetInterfaceOid \r\n");

    gRmon2Globals.InterfaceOid.u4_Length = IFINDEX_OID_SIZE;
    gRmon2Globals.InterfaceOid.pu4_OidList = gIfEntryOIDTree;

    RMON2_TRC (RMON2_FN_EXIT, "FUNC:EXIT Rmon2MainSetInterfaceOid \r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : Rmon2MainMemInit                                           *
*                                                                           *
* Description  : Allocates all the memory that is required for RMON2.       *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS, if memory allocation successful              *
*                OSIX_FAILURE, otherwise                                    *
*                                                                           *
*****************************************************************************/

PRIVATE INT4
Rmon2MainMemInit (VOID)
{
    /* Dummy pointers for system sizing during run time */
    tRmon2UsrHistCtrlBlock *pRmon2UsrHistCtrlBlock = NULL;

    UNUSED_PARAM (pRmon2UsrHistCtrlBlock);

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY Rmon2MainMemInit \r\n");

    if (Rmon2SizingMemCreateMemPools () == OSIX_FAILURE)
    {
        RMON2_TRC (OS_RESOURCE_TRC, "Memory Pool Creation Failed\r\n");
        return OSIX_FAILURE;
    }

    RMON2_PKTINFO_POOL = RMON2MemPoolIds[MAX_RMON2_PKTINFO_SIZING_ID];
    RMON2_QMSG_POOL = RMON2MemPoolIds[MAX_RMON2_QUEUE_SIZE_SIZING_ID];
    RMON2_PROTDIR_POOL = RMON2MemPoolIds[MAX_RMON2_PROTOCOLS_SIZING_ID];
    RMON2_PDISTCTRL_POOL =
        RMON2MemPoolIds[MAX_RMON2_PDISTCTRL_BLOCKS_SIZING_ID];
    RMON2_PDISTSTATS_POOL =
        RMON2MemPoolIds[MAX_RMON2_PDISTSTATS_BLOCKS_SIZING_ID];
    RMON2_ADDRMAPCTRL_POOL =
        RMON2MemPoolIds[MAX_RMON2_ADDRMAPCTRL_BLOCKS_SIZING_ID];
    RMON2_ADDRMAP_POOL = RMON2MemPoolIds[MAX_RMON2_ADDRMAP_BLOCKS_SIZING_ID];
    RMON2_HLHOSTCTRL_POOL =
        RMON2MemPoolIds[MAX_RMON2_HLHOSTCTRL_BLOCKS_SIZING_ID];
    RMON2_NLHOST_POOL = RMON2MemPoolIds[MAX_RMON2_NLHOST_BLOCKS_SIZING_ID];
    RMON2_ALHOST_POOL = RMON2MemPoolIds[MAX_RMON2_ALHOST_BLOCKS_SIZING_ID];
    RMON2_HLMATRIXCTRL_POOL =
        RMON2MemPoolIds[MAX_RMON2_HLMATRIXCTRL_BLOCKS_SIZING_ID];
    RMON2_NLMATRIXSD_POOL =
        RMON2MemPoolIds[MAX_RMON2_NLMATRIXSD_BLOCKS_SIZING_ID];
    RMON2_ALMATRIXSD_POOL =
        RMON2MemPoolIds[MAX_RMON2_ALMATRIXSD_BLOCKS_SIZING_ID];
    RMON2_NLMATRIX_TOPNCTRL_POOL =
        RMON2MemPoolIds[MAX_RMON2_NLMATRIX_TOPNCTRL_BLOCKS_SIZING_ID];
    RMON2_NLMATRIX_TOPN_POOL =
        RMON2MemPoolIds[MAX_RMON2_NLMATRIX_TOPN_BLOCKS_SIZING_ID];
    RMON2_ALMATRIX_TOPNCTRL_POOL =
        RMON2MemPoolIds[MAX_RMON2_ALMATRIX_TOPNCTRL_BLOCKS_SIZING_ID];
    RMON2_ALMATRIX_TOPN_POOL =
        RMON2MemPoolIds[MAX_RMON2_ALMATRIX_TOPN_BLOCKS_SIZING_ID];
    RMON2_USRHISTCTRL_POOL = RMON2MemPoolIds[MAX_RMON2_USRHISTCTRL_SIZING_ID];
    RMON2_USRHISTOBJ_POOL = RMON2MemPoolIds[MAX_RMON2_USRHISTOBJ_SIZING_ID];
    RMON2_HISOIDLIST_POOL =
        RMON2MemPoolIds[MAX_RMON2_HISOIDLIST_BLOCKS_SIZING_ID];
    RMON2_HISCTRLBUCKETS_POOL =
        RMON2MemPoolIds[MAX_RMON2_HISCTRLBUCKETS_BLOCKS_SIZING_ID];

    RMON2_ALMATRIX_SAMPLE_TOPN_POOL =
        RMON2MemPoolIds[MAX_RMON2_ALMATRIX_SAMPLE_TOPN_BLOCKS_SIZING_ID];
    RMON2_NLMATRIX_SAMPLE_TOPN_POOL =
        RMON2MemPoolIds[MAX_RMON2_NLMATRIX_SAMPLE_TOPN_BLOCKS_SIZING_ID];

    RMON2_TRC (RMON2_FN_EXIT, "FUNC:EXIT Rmon2MainMemInit \r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : Rmon2MainRBTreeInit                                        *
*                                                                           *
* Description  : Creates all the Table RBTree that is required for RMON2.   *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS, if memory allocation successful              *
*                OSIX_FAILURE, otherwise                                    *
*                                                                           *
*****************************************************************************/
PRIVATE INT4
Rmon2MainRBTreeInit (VOID)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY Rmon2MainRBTreeInit \r\n");

    /* Creating RBTree for PktInfo Table */
    RMON2_PKTINFO_TREE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tRmon2PktInfo, Rmon2PktInfoRBNode),
                              Rmon2UtlRBCmpPktInfo);

    /* Creating RBTree for Protocol Dir Table */
    RMON2_PROTDIR_TREE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tProtocolDir, ProtocolDirRBNode),
                              Rmon2UtlRBCmpProtocolDir);

    /* Creating RBTree for Protocol Dist Control Table */
    RMON2_PDISTCTRL_TREE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tProtocolDistControl,
                                             ProtocolDistControlRBNode),
                              Rmon2UtlRBCmpProtocolDistCtrl);

    /* Creating RBTree for Protocol Dist Stats Table */
    RMON2_PDISTSTATS_TREE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tProtocolDistStats,
                                             ProtocolDistStatsRBNode),
                              Rmon2UtlRBCmpProtocolDistStats);

    /* Creating RBTree for Address Map Control Table */
    RMON2_ADDRMAPCTRL_TREE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tAddressMapControl,
                                             AddressMapControlRBNode),
                              Rmon2UtlRBCmpAddressMapCtrl);

    /* Creating RBTree for Address Map Table */
    RMON2_ADDRMAP_TREE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tAddressMap, AddressMapRBNode),
                              Rmon2UtlRBCmpAddressMap);

    /* Creating RBTree for Hl Host Control Table */
    RMON2_HLHOSTCTRL_TREE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tHlHostControl,
                                             HlHostControlRBNode),
                              Rmon2UtlRBCmpHlHostCtrl);

    /* Creating RBTree for Nl Host Table */
    RMON2_NLHOST_TREE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tNlHost, NlHostRBNode),
                              Rmon2UtlRBCmpNlHost);

    /* Creating RBTree for Al Host Table */
    RMON2_ALHOST_TREE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tAlHost, AlHostRBNode),
                              Rmon2UtlRBCmpAlHost);

    /* Creating RBTree for Hl Matrix Control Table */
    RMON2_HLMATRIXCTRL_TREE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tHlMatrixControl,
                                             HlMatrixControlRBNode),
                              Rmon2UtlRBCmpHlMatrixCtrl);

    /* Creating RBTree for Nl Matrix SD Table */
    RMON2_NLMATRIX_TREE[RMON2_NLMATRIXSD_TABLE] =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tNlMatrixSD, NlMatrixSDRBNode),
                              Rmon2UtlRBCmpNlMatrixSD);

    /* Creating RBTree for Nl Matrix DS Table */
    RMON2_NLMATRIX_TREE[RMON2_NLMATRIXDS_TABLE] =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tNlMatrixSD, NlMatrixDSRBNode),
                              Rmon2UtlRBCmpNlMatrixDS);

    /* Creating RBTree for Al Matrix SD Table */
    RMON2_ALMATRIX_TREE[RMON2_ALMATRIXSD_TABLE] =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tAlMatrixSD, AlMatrixSDRBNode),
                              Rmon2UtlRBCmpAlMatrixSD);

    /* Creating RBTree for Al Matrix DS Table */
    RMON2_ALMATRIX_TREE[RMON2_ALMATRIXDS_TABLE] =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tAlMatrixSD, AlMatrixDSRBNode),
                              Rmon2UtlRBCmpAlMatrixDS);

    /* Creating RBTree for Nl Matrix TopN Control Table */
    RMON2_NLMATRIXTOPNCTRL_TREE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tNlMatrixTopNControl,
                                             NlMatrixTopNControlRBNode),
                              Rmon2UtlRBCmpNlMatrixTopNCtrl);

    /* Creating RBTree for Nl Matrix TopN Table */
    RMON2_NLMATRIXTOPN_TREE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tNlMatrixTopN, NlMatrixTopNRBNode),
                              Rmon2UtlRBCmpNlMatrixTopN);

    /* Creating RBTree for Al Matrix TopN Control Table */
    RMON2_ALMATRIXTOPNCTRL_TREE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tAlMatrixTopNControl,
                                             AlMatrixTopNControlRBNode),
                              Rmon2UtlRBCmpAlMatrixTopNCtrl);

    /* Creating RBTree for Al Matrix TopN Table */
    RMON2_ALMATRIXTOPN_TREE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tAlMatrixTopN, AlMatrixTopNRBNode),
                              Rmon2UtlRBCmpAlMatrixTopN);

    /* Creating RBTree for Usr History Control Table */
    RMON2_USRHISTORYCTRL_TREE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tUsrHistoryControl,
                                             UsrHistoryControlRBNode),
                              Rmon2UtlRBCmpUsrHistoryCtrl);

    RMON2_TRC (RMON2_FN_EXIT, "FUNC:EXIT Rmon2MainRBTreeInit \r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : Rmon2MainTaskInit                                          *
*                                                                           *
* Description  : Rmon2 initialization routine.                              *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*                                                                           *
*****************************************************************************/

PRIVATE INT4
Rmon2MainTaskInit (VOID)
{
    UINT4               u4Count = RMON2_ZERO;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY Rmon2MainTaskInit\r\n");
    gRmon2Globals.pRmon2LastVisted = &gRmon2PktInfo;

    MEMSET (gRmon2Globals.pRmon2LastVisted, 0, sizeof (tRmon2PktInfo));

    /* Create Rmon2 Queue */
    if (OsixCreateQ (RMON2_QUEUE_NAME, RMON2_QUEUE_SIZE, (UINT4) 0,
                     &(RMON2_QUEUE_ID)) != OSIX_SUCCESS)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC, "RN2Q Creation Failed\r\n");
        return OSIX_FAILURE;
    }

    /* Create Rmon2 Semaphore */
    if (OsixCreateSem (RMON2_SEM_NAME, RMON2_SEM_COUNT, OSIX_DEFAULT_SEM_MODE,
                       &(RMON2_SEM_ID)) != OSIX_SUCCESS)
    {
        RMON2_TRC1 (OS_RESOURCE_TRC | RMON2_CRITICAL_TRC,
                    "Rmon2MainTaskInit: Semaphore Creation failure for %s \r\n",
                    RMON2_SEM_NAME);
        return OSIX_FAILURE;
    }

    /* Initialize the InterfaceOid */
    Rmon2MainSetInterfaceOid ();
    /* Create Memory Pools for Data Structures */
    if (Rmon2MainMemInit () == OSIX_FAILURE)
    {
        RMON2_TRC (OS_RESOURCE_TRC,
                   "Rmon2MainTaskInit: Memory Pool Creation Failed\r\n");
        return OSIX_FAILURE;
    }

    /* Create RB Trees */
    Rmon2MainRBTreeInit ();
    /* Timer Initialization */
    if (Rmon2TmrInit () == OSIX_FAILURE)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC,
                   "Rmon2MainTaskInit: Timer Initialization Failed\r\n");
        return OSIX_FAILURE;
    }

    /* Initializing Protocol Local Index */
    gRmon2Globals.u4ProtDirLocIndex = 0;

    /* Protocol Table Initialization */
    Rmon2PDirInitProtDirTable ();

    /* Initialize Probe Capabilities */
    Rmon2ProbeConfigInitProbeInfo ();

    for (u4Count = 0; u4Count < SNMP_MAX_INDICES; u4Count++)
    {
        gRmon2Globals.Rmon2IndexPool[u4Count].pIndex =
            &(gRmon2Globals.Rmon2MultiPool[u4Count]);
        gRmon2Globals.Rmon2MultiPool[u4Count].pOctetStrValue =
            &(gRmon2Globals.Rmon2OctetPool[u4Count]);
        gRmon2Globals.Rmon2MultiPool[u4Count].pOidValue =
            &(gRmon2Globals.Rmon2OIDPool[u4Count]);
        gRmon2Globals.Rmon2MultiPool[u4Count].pOctetStrValue->pu1_OctetList
            = (UINT1 *) gRmon2Globals.au1Rmon2Data[u4Count];
        gRmon2Globals.Rmon2MultiPool[u4Count].pOidValue->pu4_OidList
            = (UINT4 *) (VOID *) gRmon2Globals.au1Rmon2Data[u4Count];
    }

#ifdef NPAPI_WANTED
    /* Initialize the Mem Pools and RBTrees */
    if (Rmonv2FsRMONv2NPInit () == FNP_FAILURE)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC,
                   "Rmon2MainTaskInit: RMONv2 NP Init Failed\r\n");
        return OSIX_FAILURE;
    }
#endif

    RMON2_TRC (RMON2_FN_EXIT, "FUNC:EXIT Rmon2MainTaskInit \r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2MainMemDeInit
 *
 *    DESCRIPTION      : Function that deletes all memory pools
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
Rmon2MainMemDeInit (VOID)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY Rmon2MainMemDeInit \r\n");

    Rmon2SizingMemDeleteMemPools ();

    RMON2_TRC (RMON2_FN_EXIT, "FUNC:EXIT Rmon2MainMemDeInit \r\n");
    return;
}

/****************************************************************************
 *    FUNCTION NAME    : Rmon2MainRBTreeDeInit
 *
 *    DESCRIPTION      : Function Deletes all RBtrees in RMON2 module
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 ****************************************************************************/
PRIVATE INT4
Rmon2MainRBTreeDeInit (VOID)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY Rmon2MainRBTreeDeInit \r\n");

    RBTreeDestroy (RMON2_PKTINFO_TREE, Rmon2UtlRBFreePktInfo, 0);

    RBTreeDestroy (RMON2_PROTDIR_TREE, Rmon2UtlRBFreeProtocolDir, 0);

    RBTreeDestroy (RMON2_PDISTCTRL_TREE, Rmon2UtlRBFreeProtocolDistCtrl, 0);

    RBTreeDestroy (RMON2_PDISTSTATS_TREE, Rmon2UtlRBFreeProtocolDistStats, 0);

    RBTreeDestroy (RMON2_ADDRMAPCTRL_TREE, Rmon2UtlRBFreeAddressMapCtrl, 0);

    RBTreeDestroy (RMON2_ADDRMAP_TREE, Rmon2UtlRBFreeAddressMap, 0);

    RBTreeDestroy (RMON2_HLHOSTCTRL_TREE, Rmon2UtlRBFreeHlHostCtrl, 0);

    RBTreeDestroy (RMON2_NLHOST_TREE, Rmon2UtlRBFreeNlHost, 0);

    RBTreeDestroy (RMON2_ALHOST_TREE, Rmon2UtlRBFreeAlHost, 0);

    RBTreeDestroy (RMON2_HLMATRIXCTRL_TREE, Rmon2UtlRBFreeHlMatrixCtrl, 0);

    RBTreeDestroy (RMON2_NLMATRIX_TREE[RMON2_NLMATRIXSD_TABLE],
                   Rmon2UtlRBFreeNlMatrixSD, 0);

    RBTreeDestroy (RMON2_NLMATRIX_TREE[RMON2_NLMATRIXDS_TABLE], NULL, 0);

    RBTreeDestroy (RMON2_ALMATRIX_TREE[RMON2_ALMATRIXSD_TABLE],
                   Rmon2UtlRBFreeAlMatrixSD, 0);

    RBTreeDestroy (RMON2_ALMATRIX_TREE[RMON2_ALMATRIXDS_TABLE], NULL, 0);

    RBTreeDestroy (RMON2_NLMATRIXTOPNCTRL_TREE,
                   Rmon2UtlRBFreeNlMatrixTopNCtrl, 0);

    RBTreeDestroy (RMON2_NLMATRIXTOPN_TREE, Rmon2UtlRBFreeNlMatrixTopN, 0);

    RBTreeDestroy (RMON2_ALMATRIXTOPNCTRL_TREE,
                   Rmon2UtlRBFreeAlMatrixTopNCtrl, 0);

    RBTreeDestroy (RMON2_ALMATRIXTOPN_TREE, Rmon2UtlRBFreeAlMatrixTopN, 0);

    RBTreeDestroy (RMON2_USRHISTORYCTRL_TREE, Rmon2UtlRBFreeUsrHistoryCtrl, 0);

    RMON2_TRC (RMON2_FN_EXIT, "FUNC:EXIT Rmon2MainRBTreeDeInit \r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rmon2MainTaskDeInit
 *
 *    DESCRIPTION      : Function called when the initialization of RMON2
 *                       fails at any point during initialization.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
Rmon2MainTaskDeInit (VOID)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY Rmon2MainTaskDeInit \r\n");

    /* Delete Semaphore */
    OsixDeleteSem (0, RMON2_SEM_NAME);

    /* Delete All RBTrees */
    Rmon2MainRBTreeDeInit ();

    /* Delete All Memory Pools */
    Rmon2MainMemDeInit ();

    /* DeInit Timer */
    if (gRmon2Globals.Rmon2TmrLst != 0)
    {
        if (Rmon2TmrDeInit () == OSIX_FAILURE)
        {
            RMON2_TRC (OS_RESOURCE_TRC, "Rmon2MainTaskDeInit: "
                       "Timer Deinit FAILED\r\n");
        }
    }
#ifdef NPAPI_WANTED
    Rmonv2FsRMONv2NPDeInit ();
#endif
    RMON2_TRC (RMON2_FN_EXIT, "FUNC:EXIT Rmon2MainTaskDeInit \r\n");
    return;
}

/****************************************************************************
*                                                                           *
* Function     : Rmon2MainTask                                              *
*                                                                           *
* Description  : Main function of Rmon2.                                    *
*                                                                           *
* Input        : pTaskId                                                    *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : VOID                                                       *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
Rmon2MainTask (INT1 *pTaskId)
{
    UINT4               u4Event = 0;
    tLinkStatInfo      *pLinkStatInfo = NULL;
    tPktHdrInfo        *pPktHdrInfo = NULL;
    tOsixMsg           *pQMsg = NULL;
#ifdef BSDCOMP_SLI_WANTED
    INT4                i4SockId = RMON2_INVALID_SOCK_DESC;
#endif
    UNUSED_PARAM (pTaskId);

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY Rmon2MainTask \r\n");

    if (Rmon2MainTaskInit () == OSIX_FAILURE)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC,
                   "Rmon2MainTask: !!!!! RMON2 TASK INIT FAILURE  !!!!! \r\n");
        Rmon2MainTaskDeInit ();
        RMON2_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

#ifdef SNMP_2_WANTED
    /* Register the protocol MIBs with SNMP */
    RegisterSDRMN2 ();
    RegisterFSRMN2 ();
#endif
    if (OsixTskIdSelf (&RMON2_TASK_ID) != OSIX_SUCCESS)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC,
                   "Rmon2MainTask - OsixTskIdSelf Failed\r\n");
        Rmon2MainTaskDeInit ();
        RMON2_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    RMON2_INIT_COMPLETE (OSIX_SUCCESS);
    while (1)
    {

        OsixReceiveEvent (RMON2_ONE_SECOND_TIMER_EVENT |
                          RMON2_PACKET_INFO_EVENT |
                          RMON2_IF_STATUS_EVENT |
                          RMON2_PKT_ARRIVAL_EVENT, OSIX_WAIT, (UINT4) 0,
                          (UINT4 *) &(u4Event));
        /* Mutual exclusion flag ON */
        Rmon2MainLock ();

        RMON2_TRC1 (CONTROL_PLANE_TRC, "Rx Event %d\r\n", u4Event);

        if (u4Event & RMON2_ONE_SECOND_TIMER_EVENT)
        {
            RMON2_TRC (CONTROL_PLANE_TRC, "RMON2_TIMER_EVENT\r\n");
            if (Rmon2TmrHandleExpiry () == OSIX_FAILURE)
            {
                RMON2_TRC (RMON2_CRITICAL_TRC,
                           "Rmon2MainTask: Timer Expiry Handler Failed\r\n");
                Rmon2MainTaskDeInit ();
                return;
            }
        }

        if (u4Event & RMON2_PACKET_INFO_EVENT)
        {
            RMON2_TRC (CONTROL_PLANE_TRC, "RMON2_PACKET_INFO_EVENT\r\n");
            while (OsixReceiveFromQ (SELF, RMON2_QUEUE_NAME, OSIX_NO_WAIT,
                                     0, &pQMsg) == OSIX_SUCCESS)
            {
                pPktHdrInfo = (tPktHdrInfo *) pQMsg;
                Rmon2MainProcessPktInfo (pPktHdrInfo);
                MemReleaseMemBlock (RMON2_QMSG_POOL, (UINT1 *) pPktHdrInfo);
            }

        }
        if (u4Event & RMON2_IF_STATUS_EVENT)
        {
            RMON2_TRC (CONTROL_PLANE_TRC, "RMON2_PACKET_INFO_EVENT\r\n");
            while (OsixReceiveFromQ (SELF, RMON2_QUEUE_NAME, OSIX_NO_WAIT,
                                     0, &pQMsg) == OSIX_SUCCESS)
            {
                pLinkStatInfo = (tLinkStatInfo *) pQMsg;
                Rmon2CfaIfaceUpdateChgs (pLinkStatInfo->u4IfIndex,
                                         pLinkStatInfo->u1OperStatus);
                MemReleaseMemBlock (RMON2_QMSG_POOL, (UINT1 *) pLinkStatInfo);
            }

        }
#ifdef BSDCOMP_SLI_WANTED
        if (u4Event & RMON2_PKT_ARRIVAL_EVENT)
        {
            RMON2_TRC (CONTROL_PLANE_TRC, "RMON2_PKT_ARRIVAL_EVENT\r\n");
            while (OsixReceiveFromQ (SELF, RMON2_QUEUE_NAME, OSIX_NO_WAIT,
                                     0, &pQMsg) == OSIX_SUCCESS)
            {
                pLinkStatInfo = (tLinkStatInfo *) pQMsg;
                i4SockId =(INT4) pLinkStatInfo->u4SockId;
                Rmon2ProcessPacketOnSocket(i4SockId);
                if (i4SockId > RMON2_ZERO)
                {
                    if (SelAddFd (i4SockId, Rmon2PktOnSocket) != OSIX_SUCCESS)
                    {
                        Rmon2MainUnLock ();
                        return;
                    }
                }
            }
        }
#endif
	/* Mutual exclusion flag OFF */
        Rmon2MainUnLock ();

    }                            /* End of While loop */

}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2MainLock                                              */
/*                                                                           */
/* Description  : Lock the Rmon2 procedure                                   */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rmon2MainLock (VOID)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY Rmon2MainLock \r\n");

    if (OsixTakeSem (SELF, RMON2_SEM_NAME, OSIX_WAIT, 0) != OSIX_SUCCESS)
    {
        RMON2_TRC1 (OS_RESOURCE_TRC | RMON2_CRITICAL_TRC,
                    "Rmon2MainLock: TakeSem failure for %s \n", RMON2_SEM_NAME);
        return SNMP_FAILURE;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC:EXIT Rmon2MainLock \r\n");
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2MainUnLock                                                */
/*                                                                           */
/* Description  : UnLock the Rmon2 procedure                                 */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rmon2MainUnLock (VOID)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY Rmon2MainUnLock \r\n");

    if (OsixGiveSem (SELF, RMON2_SEM_NAME) != OSIX_SUCCESS)
    {
        RMON2_TRC1 (OS_RESOURCE_TRC | RMON2_CRITICAL_TRC,
                    "Rmon2MainUnLock: GiveSem failure for %s \n",
                    RMON2_SEM_NAME);
        return SNMP_FAILURE;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC:EXIT Rmon2MainUnLock \r\n");
    return SNMP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : Rmon2MainProcessPktInfo                           */
/*                                                                      */
/*  Description     : The function process the incoming packet from NP  */
/*                    when RMONv2 is enabled                            */
/*                                                                      */
/*  Input(s)        : PktHdr                                            */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : None                                              */
/************************************************************************/
PUBLIC VOID
Rmon2MainProcessPktInfo (tPktHdrInfo * pPktHdrInfo)
{
    tRmon2PktInfo      *pNewPktInfo = NULL, *pTravPktInfo = NULL;
    tRmon2PktInfo       PktInfo;
    tAlHost            *pAlHost = NULL;
    tNlHost            *pNlHost = NULL;
    BOOL1               b1IsEntryAdded = OSIX_FALSE;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY Rmon2MainProcessPktInfo \r\n");

    MEMSET (&PktInfo, 0, sizeof (tRmon2PktInfo));

    MEMCPY (&PktInfo.PktHdr, &pPktHdrInfo->PktHdr, sizeof (tPktHeader));

    /* Process Protocol value from Packet Header and gives its *
     * Protocol Local Index into Packet Info Database */

    if (Rmon2PDirGiveProtDirLocIndices (&PktInfo) == OSIX_FAILURE)
    {
        /* If the incoming protocol is not supported by RMONv2
         * don't process the packet header */
        return;
    }
#ifdef DSMON_WANTED
    DsmonMainProcessPktInfo (pPktHdrInfo, PktInfo.u4ProtoNLIndex,
                             PktInfo.u4ProtoL4ALIndex,
                             PktInfo.u4ProtoL5ALIndex);
#endif

    /* Process Incoming Packet Info */
    pNewPktInfo = Rmon2PktInfoProcessIncomingPkt (&PktInfo);

    if (pNewPktInfo != NULL)
    {
        /* Process Matrix Table */
        if ((Rmon2MatrixProcessPktInfo (pNewPktInfo, pPktHdrInfo->u4PktSize)) ==
            OSIX_SUCCESS)
        {
            b1IsEntryAdded = OSIX_TRUE;
        }

        /* Process Host Table */
        if ((Rmon2HostProcessPktInfo (pNewPktInfo, pPktHdrInfo->u4PktSize)) ==
            OSIX_SUCCESS)
        {
            b1IsEntryAdded = OSIX_TRUE;
        }

        /* Process AddrMap Table */
        if ((Rmon2AddrMapProcessPktInfo (pNewPktInfo)) == OSIX_SUCCESS)
        {
            b1IsEntryAdded = OSIX_TRUE;
        }

        /* Process PDistStats Table */
        if ((Rmon2PDistProcessPktInfo (pNewPktInfo, pPktHdrInfo->u4PktSize)) ==
            OSIX_SUCCESS)
        {
            b1IsEntryAdded = OSIX_TRUE;
        }

        MEMSET (&PktInfo, 0, sizeof (tRmon2PktInfo));

        pTravPktInfo = Rmon2PktInfoGetNextIndex (&PktInfo.PktHdr);

        while (pTravPktInfo != NULL)
        {
            if ((MEMCMP
                 (&pTravPktInfo->PktHdr.SrcIp, &pPktHdrInfo->PktHdr.DstIp,
                  sizeof (tIpAddr)) == 0)
                &&
                (MEMCMP
                 (&pTravPktInfo->PktHdr.DstIp, &pPktHdrInfo->PktHdr.SrcIp,
                  sizeof (tIpAddr)) == 0)
                && (pTravPktInfo->PktHdr.u2SrcPort == PktInfo.PktHdr.u2DstPort)
                && (pTravPktInfo->PktHdr.u2DstPort == PktInfo.PktHdr.u2SrcPort))
            {
                pNewPktInfo->pInNlHost = pTravPktInfo->pOutNlHost;
                pNewPktInfo->pInAlHost = pTravPktInfo->pOutAlHost;
                pNlHost = pNewPktInfo->pInNlHost;
                while (pNlHost != NULL)
                {
                    pNlHost->u4NlHostInPkts++;
                    pNlHost->u4NlHostInOctets += pPktHdrInfo->u4PktSize;
                    pNlHost->u4NlHostTimeMark = OsixGetSysUpTime ();
                    pNlHost = pNlHost->pNextNlHost;
                }
                pAlHost = pNewPktInfo->pInAlHost;
                while (pAlHost != NULL)
                {
                    pAlHost->u4AlHostInPkts++;
                    pAlHost->u4AlHostInOctets += pPktHdrInfo->u4PktSize;
                    pAlHost->u4AlHostTimeMark = OsixGetSysUpTime ();
                    pAlHost = pAlHost->pNextAlHost;
                }

                /* If the new packet has created data entries,
                 * then make the above links for the existing node */
                if (b1IsEntryAdded == OSIX_TRUE)
                {
                    pTravPktInfo->pInNlHost = pNewPktInfo->pOutNlHost;
                    pTravPktInfo->pInAlHost = pNewPktInfo->pOutAlHost;
                }
                b1IsEntryAdded = OSIX_TRUE;
                if (pTravPktInfo->u1IsL3Vlan == OSIX_TRUE)
                {
                    pNewPktInfo->u1IsL3Vlan = OSIX_TRUE;
                }
                break;
            }
            pTravPktInfo = Rmon2PktInfoGetNextIndex (&pTravPktInfo->PktHdr);
        }

        if (b1IsEntryAdded == OSIX_FALSE)
        {
            /* Still entry not found in Pktinfo tree
             * then remove the newly allocated entry */
            Rmon2PktInfoDelEntry (pNewPktInfo);
        }
        else
        {
            if (pNewPktInfo->u1IsL3Vlan == OSIX_FALSE)
            {
                pNewPktInfo->PktHdr.u2VlanId = 0;
            }
#ifdef NPAPI_WANTED
            Rmonv2FsRMONv2AddFlowStats (pNewPktInfo->PktHdr);
#endif
            pNewPktInfo->u1IsNPFlowAdded = OSIX_TRUE;
        }
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC:EXIT Rmon2MainProcessPktInfo \r\n");
}

/************************************************************************/
/*  Function Name   : Rmon2MainUpdateProtoChgs                          */
/*                                                                      */
/*  Description     : If protocol entry is removed or monitoring support*/
/*                    changes for any protocol in ProtocolDir Table,this*/
/*                    function will remove statistics for this protocol */
/*                    in All Data Tables.                               */
/*                    corresponding tables                              */
/*                                                                      */
/*  Input(s)        : u4ProtDirLocalIndex                               */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : None                                              */
/************************************************************************/
PUBLIC VOID
Rmon2MainUpdateProtoChgs (UINT4 u4ProtDirLocalIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY Rmon2MainUpdateProtoChgs \r\n");

    /* Update PktInfo Table */
    Rmon2PktInfoUpdateProtoChgs (u4ProtDirLocalIndex);

    /* Update PdistStats Table */
    Rmon2PDistUpdateProtoChgs (u4ProtDirLocalIndex);

    /* Update AddrMap Table */
    Rmon2AddrMapUpdateProtoChgs (u4ProtDirLocalIndex);

    /* Update Host Table */
    Rmon2HostUpdateProtoChgs (u4ProtDirLocalIndex);

    /* Update Matrix Table */
    Rmon2MatrixUpdateProtoChgs (u4ProtDirLocalIndex);

    RMON2_TRC (RMON2_FN_EXIT, "FUNC:EXIT Rmon2MainUpdateProtoChgs \r\n");
}

/************************************************************************/
/*  Function Name   : Rmon2MainUpdatePeriodicTables                     */
/*                                                                      */
/*  Description     : For every one sec timer event, Periodic module    */
/*                    update TopN tables if valid topn control entries, */
/*                    were present                                      */
/*                                                                      */
/*  Input(s)        :                                                   */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : None                                              */
/************************************************************************/
PUBLIC VOID
Rmon2MainUpdatePeriodicTables (VOID)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY Rmon2MainUpdatePeriodicTables\r\n");

    /* Update NlMatrix TopN Table */
    Rmon2UpdateNlMatrixTopNTable ();

    /* Update AlMatrix TopN Table */
    Rmon2UpdateAlMatrixTopNTable ();

    /* Update Usr History Table */
    Rmon2UpdateUsrHistoryTable ();

    RMON2_TRC (RMON2_FN_EXIT, "FUNC:EXIT Rmon2MainUpdatePeriodicTables\r\n");
}
/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : Rmon2MainUpdateTables                         *
 *                                                                      *
 *    Description      : This function update Stat, Pdist, Host         *
 *                       and Matrix Tables in RMONv2 and DSMON          *
 *                                                                      *
 *    Input(s)         : tPktHeader *pPktHdrd,                          *
 *                       UINT4 u4PktSize                                *
 *                                                                      *
 *    Output(s)        : CFA_SUCCESS or CFA_FAILURE                     *
 *                                                                      *
 *    Global Variables Referred : tPktHdr                               *
 *                                                                      *
 *    Global Variables Modified : None.                                 *
 *                                                                      *
 *    Exceptions or Operating                                           *
 *    System Error Handling     : None.                                 *
 *                                                                      *
 *    Use of Recursion          : None.                                 *
 *                                                                      *
 *    Returns                   : None.                                 *
 *                                                                      *
 *                                                                      *
 ************************************************************************/

PUBLIC INT4
Rmon2MainUpdateTables (tPktHdrInfo * pPktHdrInfo)
{
    tPktHdrInfo        *pMsgBuf = NULL;
    pMsgBuf = (tPktHdrInfo *) (MemAllocMemBlk (RMON2_QMSG_POOL));
    if (pMsgBuf == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pMsgBuf, 0, sizeof (tPktHdrInfo));
    MEMCPY (pMsgBuf, pPktHdrInfo, sizeof (tPktHdrInfo));

    /* Post the PktHdr to RMON2 Queue */
    if (OsixSendToQ (SELF, RMON2_QUEUE_NAME, (tOsixMsg *) (VOID *) pMsgBuf,
                     OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC, "Pkt info Enqueue to RMON2 failed\r\n");
        return OSIX_FAILURE;
    }
    else
    {
        /* send an explicit event to the task */
        if (OsixSendEvent (SELF, RMON2_TASK_NAME, RMON2_PACKET_INFO_EVENT)
            != OSIX_SUCCESS)
        {
            RMON2_TRC (RMON2_CRITICAL_TRC,
                       "Sending Packet Info Event to RMON2\r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

#ifdef LNXIP4_WANTED
/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : Rmon2CreateSocket                              *
 *                                                                      *
 *    Description      : This function creates the socket for each port *
 *                                                                      *
 *    Input(s)         :                                                *
 *                       UINT4 u4Ifindex                                *
 *                                                                      *
 *    Output(s)        : OSIX_SUCCESS/OSIX_FAILURE                     *
 *                                                                      *
 *    Global Variables Referred : tPktHdr                               *
 *                                                                      *
 *    Global Variables Modified : None.                                 *
 *                                                                      *
 *    Exceptions or Operating                                           *
 *    System Error Handling     : None.                                 *
 *                                                                      *
 *    Use of Recursion          : None.                                 *
 *                                                                      *
 *    Returns                   : None.                                 *
 *                                                                      *
 *                                                                      *
 ************************************************************************/
INT4
Rmon2CreateSocket (UINT4 u4IfIndex)
{
    struct sockaddr_ll  Enet;
    struct ifreq        ifr;
    INT4                i4PortDesc = RMON2_INVALID_SOCK_DESC;
    UINT2               u2Pvid = RMON2_ZERO;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1BridgedIfaceStatus;
    tIpConfigInfo       IpIntfInfo;


    MEMSET (&Enet, RMON2_ZERO, sizeof (Enet));
    MEMSET (&ifr, RMON2_ZERO, sizeof (ifr));
    MEMSET (au1IfName, RMON2_ZERO, CFA_MAX_PORT_NAME_LENGTH);
    CfaGetIfBridgedIfaceStatus ((UINT4)u4IfIndex,
                                &u1BridgedIfaceStatus);

    if(u1BridgedIfaceStatus != CFA_DISABLED)
    {
        /* get vlan id from port interface */
        L2IwfGetVlanPortPvid (u4IfIndex, &u2Pvid);
         SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
                          "vlan",u2Pvid);

        SPRINTF (ifr.ifr_name, "%s",au1IfName);

    }
    else
    {
        if (CfaIpIfGetIfInfo (u4IfIndex, &IpIntfInfo) == CFA_FAILURE)
        {
            return OSIX_FAILURE;
        }
        u2Pvid = IpIntfInfo.u2PortVlanId;

        SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
                  "vlan",u2Pvid);
        SPRINTF (ifr.ifr_name, "%s",au1IfName);


    }
    if ((i4PortDesc = socket (PF_PACKET, SOCK_RAW, htons (ETH_P_ALL))) < RMON2_ZERO)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,"Rmon2CreateSocket: Socket Creation failed\n");
        return OSIX_FAILURE;
    }
    Rmon2SetSocketIdForVlan (u2Pvid,i4PortDesc);
    
    if (ioctl (i4PortDesc, SIOCGIFINDEX, (char *) &ifr) < RMON2_ZERO)
    {
        close (i4PortDesc);
        return OSIX_FAILURE;
    }

    Enet.sll_family = AF_PACKET;
    Enet.sll_ifindex = ifr.ifr_ifindex;
    Enet.sll_protocol = htons (ETH_P_IP);

    if (bind (i4PortDesc, (struct sockaddr *) &Enet, sizeof (Enet)) < RMON2_ZERO)
    {
        close (i4PortDesc);
        return OSIX_FAILURE;
    }

    /* set options for non-blocking mode */
    if (fcntl (i4PortDesc, F_SETFL, O_NONBLOCK) < RMON2_ZERO)
    {
        perror ("Failure in setting ETH SOCK option\n");
        close (i4PortDesc);
	    return OSIX_FAILURE;
     }
    if (SelAddFd (i4PortDesc, Rmon2PktOnSocket) != OSIX_SUCCESS)
    {
	    RMON2_TRC (RMON2_DEBUG_TRC,"Rmon2CreateSocket: SelAddFd failed\n");
	    close (i4PortDesc);
	    return OSIX_FAILURE;
    }
    RMON2_TRC (RMON2_DEBUG_TRC,"Rmon2CreateSocket: Success\n");

    return OSIX_SUCCESS;
}

/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : Rmon2SetSocketIdForVlan                        *
 *                                                                      *
 *    Description      : This function maps the socket id to a port     *
 *                                                                      *
 *    Input(s)         :                                                *
 *                       UINT4 u4Ifindex                                *
 *                                                                      *
 *    Returns          : OSIX_SUCCESS/OSIX_FAILURE                      *
 ************************************************************************/
VOID Rmon2SetSocketIdForVlan(UINT4 u4IfIndex, INT4 i4PortDesc)
{
 gi4Rmon2PortMapping[u4IfIndex] = i4PortDesc;
}

/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : Rmon2GetVlanForSocketId                   *
 *                                                                      *
 *    Description      : This function returns the index corresponding  *
 *                       to socket id                                   *
 *    Input(s)         :                                                *
 *                       UINT4 u4Ifindex                                *
 *                                                                      *
 *    Returns          : OSIX_SUCCESS/OSIX_FAILURE                      *
 ************************************************************************/
UINT4 Rmon2GetVlanForSocketId(INT4 i4PortDesc)
{
    UINT4 u4Index = RMON2_ZERO;
	for(u4Index = RMON2_ZERO; u4Index < RMON2_MAX_VLAN; u4Index++)
	{
		if(gi4Rmon2PortMapping[u4Index] == i4PortDesc)
		{ 
			return u4Index;
		}
	}
	return OSIX_FAILURE;       
}
/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : Rmon2PktOnSocket                               *
 *                                                                      *
 *    Description      : This function maps the socket id to a port     *
 *                                                                      *
 *    Input(s)         :                                                *
 *                       UINT4 i4SockFd                                 *
 *    Output(s)        : NONE                                           *
 *                                                                      *
 *    Use of Recursion          : None.                                 *
 *                                                                      *
 *    Returns                   : None.                                 *
 *                                                                      *
 *                                                                      *
 ************************************************************************/
PUBLIC 
           VOID Rmon2PktOnSocket (INT4 i4SockFd)
{

    tLinkStatInfo      *pMsgBuf;

    pMsgBuf = (tLinkStatInfo *) (MemAllocMemBlk (RMON2_QMSG_POOL));
    if (pMsgBuf == NULL)
    {
        return;
    }
    MEMSET (pMsgBuf, RMON2_ZERO, sizeof(tLinkStatInfo));

    pMsgBuf->u4SockId= (UINT4)i4SockFd;

    if (OsixQueSend (RMON2_QUEUE_ID, (UINT1 *) &pMsgBuf,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (RMON2_QMSG_POOL, (UINT1 *) pMsgBuf);
        return;
    }
    else
    {
        /* send an explicit event to the task */
        if (OsixSendEvent (SELF, RMON2_TASK_NAME, RMON2_PKT_ARRIVAL_EVENT)
            != OSIX_SUCCESS)
        {  
            RMON2_TRC (RMON2_CRITICAL_TRC,
                       "Sending Packet Info Event to RMON2\r\n");
            MemReleaseMemBlock (RMON2_QMSG_POOL, (UINT1 *) pMsgBuf);
            return;
        }
    }
    MemReleaseMemBlock (RMON2_QMSG_POOL, (UINT1 *) pMsgBuf);
    return;
}

/************************************************************************/
/*  Function Name   : Rmon2ProcessPacketOnSocket                        */
/*  Description     : The function process the packet                   */
/*  Input(s)        : i4SockId = socket number                          */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/

VOID
Rmon2ProcessPacketOnSocket (INT4 i4SockId)
{

    struct sockaddr_in  PeerAddr;
    struct msghdr       PktInfo;
    struct in_pktinfo  *pIpPktInfo = NULL;
    struct iovec        Iov;
    struct cmsghdr     *pCmsgInfo = NULL;
    struct ifreq        ifr;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tPktHdrInfo         PktHdrInfo;
    INT4                i4DataLen = RMON2_ZERO;
    INT4                Index = RMON2_ZERO;
    UINT4               u4VlanIndex = RMON2_ZERO;
    UINT4               u4VlanIfIndex = CFA_INVALID_INDEX;
    UINT4               u4L2CxtId = RMON2_ZERO;
    UINT1               au1Cmsg[RMON_CMSG_DATA_LEN];    /* For storing Auxillary Data - 
                                           IP Packet INFO. */
    UINT1              *pData = NULL;
    UINT1              *pFrame = NULL;
    UINT1               au1IfName[RMON_CMSG_DATA_LEN];
    UINT1               au1Mac[CFA_ENET_ADDR_LEN];

    MEMSET (&au1Mac, RMON2_ZERO, CFA_ENET_ADDR_LEN);
    MEMSET (au1IfName, RMON2_ZERO, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&ifr, RMON2_ZERO, sizeof (ifr));
    MEMSET (au1Cmsg, RMON2_ZERO, sizeof (au1Cmsg));
    MEMSET (&PktInfo, RMON2_ZERO, sizeof (struct msghdr));
    MEMSET (&PeerAddr, RMON2_ZERO, sizeof (struct sockaddr_in));
    MEMSET (gi1Rmon2RecvBuff,RMON2_ZERO,RMON2_MAX_MTU);


    PktInfo.msg_name = (void *) &PeerAddr;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
    Iov.iov_base = gi1Rmon2RecvBuff;
    Iov.iov_len = sizeof (gi1Rmon2RecvBuff);
    PktInfo.msg_iov = &Iov;

    PktInfo.msg_iovlen = 1;
    PktInfo.msg_control = (void *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = IPPROTO_IP;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    u4VlanIndex = Rmon2GetVlanForSocketId(i4SockId);   
    if ((i4DataLen = recvmsg (i4SockId, &PktInfo, RMON2_ZERO)) > RMON2_ZERO)
    {
        pIpPktInfo = (struct in_pktinfo *) (VOID *)
        CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));


        if (pIpPktInfo != NULL)
        {
            if ((pBuf = CRU_BUF_Allocate_MsgBufChain ((UINT4)i4DataLen,
                                                      RMON2_ZERO)) != NULL)
            {

                CRU_BUF_Copy_OverBufChain (pBuf,
                                           (UINT1 *) gi1Rmon2RecvBuff, RMON2_ZERO,
                                           (UINT4)i4DataLen);
                pData = CRU_BUF_GetDataPtr (pBuf->pFirstValidDataDesc);
                MEMSET (&PktHdrInfo, 0, sizeof (tPktHdrInfo));
                pFrame = pData;
                pFrame += RMON2_MAC_ADDRESS_LEN;
                MEMCPY (au1Mac, pFrame, RMON2_MAC_ADDRESS_LEN);
#ifdef NPAPI_WANTED
                Rmonv2FsRMONv2GetPortID(au1Mac,u4VlanIndex,&Index);

#endif
                PktHdrInfo.PktHdr.u4IfIndex = (UINT4)Index ; 
                PktHdrInfo.u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

                VcmGetContextIdFromCfaIfIndex ((UINT4)Index, &u4L2CxtId);

                u4VlanIfIndex = CfaGetVlanInterfaceIndexInCxt (u4L2CxtId,(tVlanIfaceVlanId)u4VlanIndex);
                PktHdrInfo.PktHdr.u4VlanIfIndex = u4VlanIfIndex; 
                PktHdrInfo.PktHdr.u2VlanId = (UINT2)u4VlanIndex;
#ifdef NPAPI_WANTED
                if (CfaIwfRmon2UpdatePktHdr (pData, &PktHdrInfo.PktHdr) ==
                    OSIX_SUCCESS)
                {
                    Rmon2MainProcessPktInfo (&PktHdrInfo);
                }
#endif

            }
        }
    }
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    return;

}


/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2CloseSocket                                           */
/*                                                                           */
/* Description  : Close Socket for specific Vlan ID                          */
/*                                                                           */
/* Input        : u4Port = port number                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
Rmon2CloseSocket (UINT4 u4Port)
{
    tIpConfigInfo       IpIntfInfo;
    INT4                i4SockId = RMON2_INVALID_SOCK_DESC;
    UINT2               u2Pvid = 0;
    UINT1               u1BridgedIfaceStatus;

    CfaGetIfBridgedIfaceStatus ((UINT4)u4Port,
                                &u1BridgedIfaceStatus);

    if(u1BridgedIfaceStatus != CFA_DISABLED)
    {
        /* get vlan id from port interface */
        L2IwfGetVlanPortPvid (u4Port, &u2Pvid);
    }
    else
    {

    if (CfaIpIfGetIfInfo (u4Port, &IpIntfInfo) == CFA_FAILURE)
    {
        return;
    }
    u2Pvid = IpIntfInfo.u2PortVlanId;
    }
	i4SockId = gi4Rmon2PortMapping[u2Pvid];
	SelRemoveFd (i4SockId);
	close (i4SockId);
	gi4Rmon2PortMapping[u2Pvid]= RMON2_INVALID_SOCK_DESC;
    return;
}
#endif
/*-----------------------------------------------------------------------*/
/*                       End of the file  rmn2main.c                      */
/*-----------------------------------------------------------------------*/
