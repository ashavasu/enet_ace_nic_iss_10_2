/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 * 
    $Id: rmn2nltn.c,v 1.13 2014/02/24 11:40:07 siva Exp $
 * Description: This file contains the functional routine for
 *              RMON2 Network Layer Matrix TopN module
 *
 *******************************************************************/

#include "rmn2inc.h"

/*****************************************************************************/
/* Function Name      : Rmon2NlDeleteTopNEntries                             */
/*                                                                           */
/* Description        : This function is used to Update NlMatrixTopN data    */
/*                      table for Ctrl Status Changes in                     */
/*                        NlMatrixTopNCtrl table                             */
/*                                                                           */
/* Input(s)           : pNlMatrixTopNControl                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS / SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2NlDeleteTopNEntries (tNlMatrixTopNControl * pNlMatrixTopNControl)
{
    tNlMatrixTopN      *pNlMatrixTopN = NULL;
    UINT4               u4NlMatrixTopNControlIndex, u4NlMatrixTopNIndex;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY " "Rmon2NlDeleteTopNEntries \n");

    /* Remove Data Entries for this control index from Nl Host Table */
    pNlMatrixTopN = Rmon2NlMatrixGetNextTopNIndex
        (pNlMatrixTopNControl->u4NlMatrixTopNControlIndex, RMON2_ZERO);

    while (pNlMatrixTopN != NULL)
    {
        if (pNlMatrixTopN->u4NlMatrixTopNControlIndex
            != pNlMatrixTopNControl->u4NlMatrixTopNControlIndex)
        {
            RMON2_TRC (RMON2_DEBUG_TRC,
                       "NlMatrixTopNControlIndex in NlMatrixTopNControl table "
                       "and NlMatrixTopN table are not same\n");
            break;
        }

        u4NlMatrixTopNControlIndex = pNlMatrixTopN->u4NlMatrixTopNControlIndex;
        u4NlMatrixTopNIndex = pNlMatrixTopN->u4NlMatrixTopNIndex;

        /* Free NlMatrix TopN Data entry */
        Rmon2NlMatrixDelTopNEntry (pNlMatrixTopN);
        pNlMatrixTopN = Rmon2NlMatrixGetNextTopNIndex
            (u4NlMatrixTopNControlIndex, u4NlMatrixTopNIndex);
    }                            /* end of while */
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT " "Rmon2NlDeleteTopNEntries \n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2NlMatrixTopNNotifyCtrlChgs                            */
/*                                                                           */
/* Description  : This function is used to update NlMatrixTopN data table    */
/*                and NlMatrixTopN ctrl table for Ctrl status changes        */
/*                in HlMatrix Ctrl table                                     */
/*                                                                           */
/* Input        : u4HlMatrixControlIndex                                     */
/*                u4SetValHlMatrixControlStatus                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2NlMatrixTopNNotifyCtlChgs (UINT4 u4HlMatrixControlIndex,
                                UINT4 u4HlMatrixControlStatus)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrl = NULL;
    tNlMatrixTopN      *pNlMatrixTopN = NULL;
    UINT4               u4NlMatrixTopNControlIndex = 0, u4NlMatrixTopNIndex = 0;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "Rmon2NlMatrixTopNNotifyCtrlChgs \n");

    /* Get First Entry in NlMatrix TopN Ctrl Table */
    pNlMatrixTopNCtrl =
        Rmon2NlMatrixGetNextTopNCtlIndex (u4NlMatrixTopNControlIndex);

    while (pNlMatrixTopNCtrl != NULL)
    {
        u4NlMatrixTopNControlIndex =
            pNlMatrixTopNCtrl->u4NlMatrixTopNControlIndex;
        if ((pNlMatrixTopNCtrl->u4NlMatrixTopNControlMatrixIndex) ==
            u4HlMatrixControlIndex)
        {
            if (u4HlMatrixControlStatus != ACTIVE)
            {
                /* Free NlMatrix TopN Data entry */
                pNlMatrixTopN = Rmon2NlMatrixGetNextTopNIndex
                    (u4NlMatrixTopNControlIndex, u4NlMatrixTopNIndex);
                while (pNlMatrixTopN != NULL)
                {
                    if (pNlMatrixTopN->u4NlMatrixTopNControlIndex !=
                        u4NlMatrixTopNControlIndex)
                    {
                        RMON2_TRC (RMON2_DEBUG_TRC,
                                   "NlMatrixTopNControlIndex in "
                                   "NlMatrixTopNControl table "
                                   "and NlMatrixTopN table are not same\n");
                        break;
                    }

                    u4NlMatrixTopNIndex = pNlMatrixTopN->u4NlMatrixTopNIndex;

                    /* Free NlMatrix TopN Data entry */
                    Rmon2NlMatrixDelTopNEntry (pNlMatrixTopN);

                    pNlMatrixTopN = Rmon2NlMatrixGetNextTopNIndex
                        (u4NlMatrixTopNControlIndex, u4NlMatrixTopNIndex);
                }                /* end of while */

                pNlMatrixTopNCtrl->u4NlMatrixTopNControlTimeRemaining =
                    pNlMatrixTopNCtrl->u4NlMatrixTopNControlDuration;
                pNlMatrixTopNCtrl->u4NlMatrixTopNControlStatus = NOT_READY;
            }
            else if ((u4HlMatrixControlStatus == ACTIVE) &&
                     (pNlMatrixTopNCtrl->u4NlMatrixTopNControlStatus ==
                      NOT_READY))
            {
                pNlMatrixTopNCtrl->u4NlMatrixTopNControlStatus = ACTIVE;
            }
        }                        /* End of If */

        pNlMatrixTopNCtrl =
            Rmon2NlMatrixGetNextTopNCtlIndex (u4NlMatrixTopNControlIndex);
    }                            /* End of while */
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "Rmon2NlMatrixTopNNotifyCtrlChgs \n");
    return;
}

/*****************************************************************************/
/* Function Name      : Rmon2NlMatrixGetNextTopNCtlIndex                     */
/*                                                                           */
/* Description        : This function is used to get next NlMatrix TopN      */
/*                      control table entry for the interface index          */
/*                      'u4IfIndex'.                                         */
/*                                                                           */
/* Input(s)           : Interface Index                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/* Return Value(s)    : NULL / Pointer to the next NlMatrix TopN control     */
/*                      table entry                                          */
/*                                                                           */
/*****************************************************************************/
tNlMatrixTopNControl *
Rmon2NlMatrixGetNextTopNCtlIndex (UINT4 u4NlMatrixTopNCtrlIndex)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;
    tNlMatrixTopNControl NlMatrixTopNCtrlEntry;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "Rmon2NlMatrixGetNextTopNCtlIndex \n");

    MEMSET (&NlMatrixTopNCtrlEntry, 0, sizeof (NlMatrixTopNCtrlEntry));

    NlMatrixTopNCtrlEntry.u4NlMatrixTopNControlIndex = u4NlMatrixTopNCtrlIndex;

    pNlMatrixTopNCtrlEntry = (tNlMatrixTopNControl *)
        RBTreeGetNext (RMON2_NLMATRIXTOPNCTRL_TREE,
                       (tRBElem *) & NlMatrixTopNCtrlEntry, NULL);

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "Rmon2NlMatrixGetNextTopNCtlIndex \n");
    return pNlMatrixTopNCtrlEntry;

}

/*****************************************************************************/
/* Function Name      : Rmon2NlMatrixAddTopNCtrlEntry                        */
/*                                                                           */
/* Description        : This function is used to add NlMatrixTopN            */
/*                      control table entry in the RBTree                    */
/*                                                                           */
/* Input(s)           : u4NlMatrixTopNCtrlIndex                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NULL / Pointer to the NlMatrixTopN control           */
/*                      table entry.                                         */
/*****************************************************************************/

PUBLIC tNlMatrixTopNControl *
Rmon2NlMatrixAddTopNCtlEntry (UINT4 u4NlMatrixTopNCtrlIndex)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlNode;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2NlMatrixAddTopNCtlEntry \n");
    if ((pNlMatrixTopNCtrlNode = (tNlMatrixTopNControl *)
         (MemAllocMemBlk (RMON2_NLMATRIX_TOPNCTRL_POOL))) == NULL)
    {
        /* Alloc mem block failed */
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_MEM_FAIL,
                   "MemAlloc Failure for NlMatrixTopN control table Entry \n");
        return NULL;
    }

    MEMSET (pNlMatrixTopNCtrlNode, 0, sizeof (tNlMatrixTopNControl));

    pNlMatrixTopNCtrlNode->u4NlMatrixTopNControlIndex = u4NlMatrixTopNCtrlIndex;
    pNlMatrixTopNCtrlNode->u4NlMatrixTopNControlTimeRemaining =
        RMON2_TOPN_CTRL_TIMEREM;

    pNlMatrixTopNCtrlNode->u4NlMatrixTopNControlGeneratedReports = RMON2_ZERO;
    pNlMatrixTopNCtrlNode->u4NlMatrixTopNControlDuration = RMON2_ZERO;
    pNlMatrixTopNCtrlNode->u4NlMatrixTopNControlRequestedSize =
        RMON2_TOPN_CTRL_REQ_SIZE;

    pNlMatrixTopNCtrlNode->u4NlMatrixTopNControlGrantedSize =
        RMON2_MAX_TOPN_ENTRY;

    pNlMatrixTopNCtrlNode->u4NlMatrixTopNControlStartTime = OsixGetSysUpTime ();

    if (RBTreeAdd (RMON2_NLMATRIXTOPNCTRL_TREE, pNlMatrixTopNCtrlNode)
        == RB_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Rmon2NlMatrixAddTopNCtrlEntry: NlMatrixAddTopNCtrlEntry "
                   "is added \n");
        return pNlMatrixTopNCtrlNode;
    }

    MemReleaseMemBlock (RMON2_NLMATRIX_TOPNCTRL_POOL,
                        (UINT1 *) pNlMatrixTopNCtrlNode);
    RMON2_TRC (RMON2_DEBUG_TRC,
               "Rmon2NlMatrixAddTopNCtrlEntry: Adding NlMatrixAddTopNCtrlEntry "
               "failed \n");

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT " "Rmon2NlMatrixAddTopNCtlEntry \n");
    return NULL;

}

/*****************************************************************************/
/* Function Name      : Rmon2NlMatrixDelTopNCtlEntry                         */
/*                                                                           */
/* Description        : This function is used to remove NlMatrixTopN         */
/*                      control table entry from the RBTree                  */
/*                                                                           */
/* Input(s)           : NlMatrixTopN control node                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS/SNMP_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
Rmon2NlMatrixDelTopNCtlEntry (tNlMatrixTopNControl * pNlMatrixTopNCtrlNode)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2NlMatrixDelTopNCtlEntry \n");
    if (RBTreeRemove (RMON2_NLMATRIXTOPNCTRL_TREE, pNlMatrixTopNCtrlNode)
        == RB_FAILURE)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "RBTreeRemove Failure for NlMatrixTopN Control table "
                   "Entry \n");
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (RMON2_NLMATRIX_TOPNCTRL_POOL,
                        (UINT1 *) pNlMatrixTopNCtrlNode);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2NlMatrixDelTopNCtlEntry \n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : Rmon2NlMatrixGetTopNCtlEntry                         */
/*                                                                           */
/* Description        : This function is used to get NlMatrix TopN control   */
/*                      table entry for the interface index 'u4IfIndex'.     */
/*                                                                           */
/* Input(s)           : Interface Index                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/* Return Value(s)    : NULL / Pointer to the NlMatrix TopN control table    */
/*                      entry                                                */
/*                                                                           */
/*****************************************************************************/

tNlMatrixTopNControl *
Rmon2NlMatrixGetTopNCtlEntry (UINT4 u4NlMatrixTopNCtrlIndex)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;
    tNlMatrixTopNControl NlMatrixTopNCtrlEntry;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "Rmon2NlMatrixGetTopNCtlEntry \n");

    MEMSET (&NlMatrixTopNCtrlEntry, 0, sizeof (NlMatrixTopNCtrlEntry));

    NlMatrixTopNCtrlEntry.u4NlMatrixTopNControlIndex = u4NlMatrixTopNCtrlIndex;

    /* Get the pointer to the interface index 'u4NlMatrixTopNCtrlIndex' */
    pNlMatrixTopNCtrlEntry = (tNlMatrixTopNControl *)
        RBTreeGet (RMON2_NLMATRIXTOPNCTRL_TREE,
                   (tRBElem *) & NlMatrixTopNCtrlEntry);

    if (pNlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "RBTreeGet Failure for NlMatrixTopN control "
                   "table Entry \n");
        return NULL;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT " "Rmon2NlMatrixGetTopNCtlEntry \n");
    return pNlMatrixTopNCtrlEntry;
}

/*****************************************************************************/
/* Function Name      : Rmon2NlMatrixGetNextTopNIndex                        */
/*                                                                           */
/* Description        : This function is used to get next NlMatrix TopN      */
/*                      table entry for the interface index                  */
/*                      'u4NlMatrixTopNControlIndex,u4NlMatrixTopNIndex'.    */
/*                                                                           */
/* Input(s)           : u4NlMatrixTopNControlIndex,u4NlMatrixTopNIndex       */
/*                                                                           */
/* Output(s)          : None                                                 */
/* Return Value(s)    : NULL / Pointer to the next NlMatrix TopN             */
/*                      table entry                                          */
/*                                                                           */
/*****************************************************************************/
tNlMatrixTopN      *
Rmon2NlMatrixGetNextTopNIndex (UINT4 u4NlMatrixTopNControlIndex,
                               UINT4 u4NlMatrixTopNIndex)
{
    tNlMatrixTopN       NlMatrixTopNEntry;
    tNlMatrixTopN      *pNlMatrixTopNEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "Rmon2NlMatrixGetNextTopNIndex \n");
    MEMSET (&NlMatrixTopNEntry, 0, sizeof (NlMatrixTopNEntry));

    /* Get the pointer to the interface index 'u4NlMatrixTopNCtrlIndex,
     * u4NlMatrixTopNIndex' */
    NlMatrixTopNEntry.u4NlMatrixTopNControlIndex = u4NlMatrixTopNControlIndex;
    NlMatrixTopNEntry.u4NlMatrixTopNIndex = u4NlMatrixTopNIndex;
    pNlMatrixTopNEntry = (tNlMatrixTopN *)
        RBTreeGetNext (RMON2_NLMATRIXTOPN_TREE,
                       (tRBElem *) & NlMatrixTopNEntry, NULL);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "Rmon2NlMatrixGetNextTopNDataIndex \n");
    return pNlMatrixTopNEntry;
}

/*****************************************************************************/
/* Function Name      : Rmon2NlMatrixAddTopNEntry                            */
/*                                                                           */
/* Description        : This function is used to add NlMatrixTopN            */
/*                      table entry in the RBTree                            */
/*                                                                           */
/* Input(s)           : u4NlMatrixTopNIndex,pSampleTopNList                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NULL / Pointer to the NlMatrixTopN                   */
/*                      table entry.                                         */
/*****************************************************************************/
PUBLIC tNlMatrixTopN *
Rmon2NlMatrixAddTopNEntry (UINT4 u4NlMatrixTopNIndex,
                           tNlMatrixTopN * pSampleTopNList)
{
    tNlMatrixTopN      *pNlMatrixTopNNode = NULL;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2NlMatrixAddTopNEntry \n");

    if ((pNlMatrixTopNNode = (tNlMatrixTopN *)
         (MemAllocMemBlk (RMON2_NLMATRIX_TOPN_POOL))) == NULL)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for Matrix TopN\r\n");
        return NULL;
    }

    MEMSET (pNlMatrixTopNNode, 0, sizeof (tNlMatrixTopN));
    MEMCPY (pNlMatrixTopNNode, pSampleTopNList, sizeof (tNlMatrixTopN));
    pNlMatrixTopNNode->u4NlMatrixTopNIndex = u4NlMatrixTopNIndex;

    if (RBTreeAdd (RMON2_NLMATRIXTOPN_TREE, (tRBElem *) pNlMatrixTopNNode)
        == RB_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Rmon2NlMatrixAddTopNDataEntry: NlMatrixAddTopNEntry"
                   "is added \n");

        return pNlMatrixTopNNode;
    }
    RMON2_TRC (RMON2_DEBUG_TRC,
               "Rmon2NlMatrixAddTopNDataEntry: Adding NlMatrixAddTopNDataEntry"
               "failed \n");

    MemReleaseMemBlock (RMON2_NLMATRIX_TOPN_POOL, (UINT1 *) pNlMatrixTopNNode);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2NlMatrixAddTopNDataEntry \n");

    return NULL;
}

/*****************************************************************************/
/* Function Name      : Rmon2NlMatrixDelTopNEntry                            */
/*                                                                           */
/* Description        : This function is used to delete NlMatrixTopN         */
/*                      table entries in the RBTree                          */
/*                                                                           */
/* Input(s)           : pNlMatrixTopN                                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS / SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rmon2NlMatrixDelTopNEntry (tNlMatrixTopN * pNlMatrixTopN)
{

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY " "Rmon2NlMatrixDelTopNEntry \n");
    /* Remove NlMatrixTopN. node from NlMatrixTopNRBTree */
    RBTreeRem (RMON2_NLMATRIXTOPN_TREE, (tRBElem *) pNlMatrixTopN);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (RMON2_NLMATRIX_TOPN_POOL,
                            (UINT1 *) pNlMatrixTopN) != MEM_SUCCESS)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for  NlMatrixTopN.\r\n");
        return OSIX_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT " "Rmon2NlMatrixDelTopNEntry \n");

    return OSIX_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : Rmon2NlMatrixGetTopNEntry                            */
/*                                                                           */
/* Description        : This function is used to get NlMatrix TopN           */
/*                      table entry for the interface index                  */
/*                      'u4NlMatrixTopNControlIndex,u4NlMatrixTopNIndex'.    */
/*                                                                           */
/* Input(s)           : u4NlMatrixTopNControlIndex,u4NlMatrixTopNIndex       */
/*                                                                           */
/* Output(s)          : None                                                 */
/* Return Value(s)    : NULL / Pointer to the NlMatrix TopN table            */
/*                      entry                                                */
/*                                                                           */
/*****************************************************************************/
tNlMatrixTopN      *
Rmon2NlMatrixGetTopNEntry (UINT4 u4NlMatrixTopNControlIndex,
                           UINT4 u4NlMatrixTopNIndex)
{
    tNlMatrixTopN       NlMatrixTopNEntry;
    tNlMatrixTopN      *pNlMatrixTopNEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY " "Rmon2NlMatrixGetTopNEntry \n");

    MEMSET (&NlMatrixTopNEntry, 0, sizeof (NlMatrixTopNEntry));

    NlMatrixTopNEntry.u4NlMatrixTopNControlIndex = u4NlMatrixTopNControlIndex;
    NlMatrixTopNEntry.u4NlMatrixTopNIndex = u4NlMatrixTopNIndex;
    pNlMatrixTopNEntry = (tNlMatrixTopN *)
        RBTreeGet (RMON2_NLMATRIXTOPN_TREE, (tRBElem *) & NlMatrixTopNEntry);

    if (pNlMatrixTopNEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "RBTreeGet Failure for NlMatrixTopN table Entry \n");
        return NULL;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT " "Rmon2NlMatrixGetTopNEntry \n");
    return pNlMatrixTopNEntry;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FillMatrixSampleTopNRate                                   */
/*                                                                           */
/* Description  : Fill the topn rate values                                  */
/*                                                                           */
/* Input        : pSampleTopN,pMatrixSDNode,u4SampleCount,                   */
/*                u4Pktcount,u4Octetcount,u4MatrixTopNCtlIndex               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
FillNlMatrixSampleTopNRate (tNlMatrixTopN * pSampleTopN, UINT4 u4SampleCount,
                            tNlMatrixSD * pNlMatrixSDNode, UINT4 u4Pktcount,
                            UINT4 u4Octetcount,
                            tNlMatrixTopNControl * pNlMatrixTopNCtl)
{
    UINT4               u4Index = RMON2_ONE;
    UINT4               u4MinPktRate;
    UINT4               u4MinOctRate;
    UINT4               u4MinPktIndex = RMON2_ZERO;
    UINT4               u4MinOctIndex = RMON2_ZERO;
    UINT4               u4ReplaceIndex = RMON2_ZERO;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY FillMatrixSampleTopNRate\r\n");

    if (u4SampleCount < pNlMatrixTopNCtl->u4NlMatrixTopNControlGrantedSize)
    {

        pSampleTopN[u4SampleCount].u4NlMatrixTopNControlIndex =
            pNlMatrixTopNCtl->u4NlMatrixTopNControlIndex;

        pSampleTopN[u4SampleCount].u4NlMatrixTopNIndex = u4SampleCount;

        pSampleTopN[u4SampleCount].u4NlMatrixTopNIpAddrLen =
            pNlMatrixSDNode->u4Rmon2NlMatrixSDIpAddrLen;

        MEMCPY (&pSampleTopN[u4SampleCount].NlMatrixTopNSourceAddress,
                &pNlMatrixSDNode->NlMatrixSDSourceAddress,
                sizeof (pNlMatrixSDNode->NlMatrixSDSourceAddress));

        MEMCPY (&pSampleTopN[u4SampleCount].NlMatrixTopNDestAddress,
                &pNlMatrixSDNode->NlMatrixSDDestAddress,
                sizeof (pNlMatrixSDNode->NlMatrixSDDestAddress));

        pSampleTopN[u4SampleCount].u4NlMatrixTopNProtocolDirLocalIndex =
            pNlMatrixSDNode->u4ProtocolDirLocalIndex;

        pSampleTopN[u4SampleCount].u4NlMatrixTopNPktRate = u4Pktcount;

        pSampleTopN[u4SampleCount].u4NlMatrixTopNReversePktRate = u4Pktcount;

        pSampleTopN[u4SampleCount].u4NlMatrixTopNOctetRate = u4Octetcount;

        pSampleTopN[u4SampleCount].u4NlMatrixTopNReverseOctetRate =
            u4Octetcount;
    }
    else
    {
        u4MinPktRate = pSampleTopN[RMON2_ZERO].u4NlMatrixTopNPktRate;
        u4MinOctRate = pSampleTopN[RMON2_ZERO].u4NlMatrixTopNOctetRate;

        /* finding which topN entry has least Pkt Rate and Octet Rate */
        while (u4Index < pNlMatrixTopNCtl->u4NlMatrixTopNControlGrantedSize)
        {
            if (pSampleTopN[u4Index].u4NlMatrixTopNPktRate < u4MinPktRate)
            {
                u4MinPktRate = pSampleTopN[u4Index].u4NlMatrixTopNPktRate;
                u4MinPktIndex = u4Index;
            }

            if (pSampleTopN[u4Index].u4NlMatrixTopNOctetRate < u4MinOctRate)
            {
                u4MinOctRate = pSampleTopN[u4Index].u4NlMatrixTopNOctetRate;
                u4MinOctIndex = u4Index;
            }
            u4Index++;
        }                        /* end of while */

        /* Replacing the TopN entry which has least PktRate when 
         * NlMatrixTopNControlRateBase is NLMATRIX_TOPN_PKTS and 
         * least Octet Rate when NlMatrixTopNControlRateBase is
         * NLMATRIX_TOPN_OCTETS */

        if (pNlMatrixTopNCtl->u4NlMatrixTopNControlRateBase ==
            NLMATRIX_TOPN_PKTS)
        {
            u4ReplaceIndex = u4MinPktIndex;
        }
        else if (pNlMatrixTopNCtl->u4NlMatrixTopNControlRateBase ==
                 NLMATRIX_TOPN_OCTETS)
        {
            u4ReplaceIndex = u4MinOctIndex;
        }
        else
        {
            RMON2_TRC (RMON2_DEBUG_TRC,
                       "HIGH CAPACITY PKTS and HIGH CAPACITY OCTETS are not "
                       "supported \n");
            return;
        }

        if ((u4MinPktRate <= u4Pktcount) && (u4MinOctRate <= u4Octetcount))
        {
            pSampleTopN[u4ReplaceIndex].u4NlMatrixTopNControlIndex =
                pNlMatrixTopNCtl->u4NlMatrixTopNControlIndex;

            pSampleTopN[u4ReplaceIndex].u4NlMatrixTopNIndex = u4ReplaceIndex;

            pSampleTopN[u4ReplaceIndex].u4NlMatrixTopNIpAddrLen =
                pNlMatrixSDNode->u4Rmon2NlMatrixSDIpAddrLen;

            MEMCPY (&pSampleTopN[u4ReplaceIndex].NlMatrixTopNSourceAddress,
                    &pNlMatrixSDNode->NlMatrixSDSourceAddress,
                    sizeof (pNlMatrixSDNode->NlMatrixSDSourceAddress));

            MEMCPY (&pSampleTopN[u4ReplaceIndex].NlMatrixTopNDestAddress,
                    &pNlMatrixSDNode->NlMatrixSDDestAddress,
                    sizeof (pNlMatrixSDNode->NlMatrixSDDestAddress));

            pSampleTopN[u4ReplaceIndex].u4NlMatrixTopNProtocolDirLocalIndex =
                pNlMatrixSDNode->u4ProtocolDirLocalIndex;

            pSampleTopN[u4ReplaceIndex].u4NlMatrixTopNPktRate = u4Pktcount;

            pSampleTopN[u4ReplaceIndex].u4NlMatrixTopNReversePktRate =
                u4Pktcount;

            pSampleTopN[u4ReplaceIndex].u4NlMatrixTopNOctetRate = u4Octetcount;

            pSampleTopN[u4ReplaceIndex].u4NlMatrixTopNReverseOctetRate =
                u4Octetcount;

        }

    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC:EXIT FillMatrixSampleTopNRate\r\n");

}

/****************************************************************************
* Function           : GetSamplesFromMatrixTable                            *
*                                                                           *
* Input (s)          : HlMatrix control index                               *
*                                                                           *
* Output (s)         : None.                                                *
*                                                                           *
* Returns            : None.                                                *
*                                                                           *
* Gets Sample from a list of matrix entries attached to a index and places  *
* the in the specified address.                                             *
****************************************************************************/

PRIVATE VOID
GetSamplesFromNlMatrixTable (UINT4 u4CtlIndex)
{
    tNlMatrixSD        *pNlMatrixSDEntry = NULL;
    UINT4               u4ProtocolDirLocalIndex = RMON2_ZERO;
    tIpAddr             NlMatrixSDSourceAddress;
    tIpAddr             NlMatrixSDDestAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY " "GetSamplesFromMatrixTable \n");
    MEMSET (&NlMatrixSDSourceAddress, RMON2_ZERO,
            sizeof (NlMatrixSDSourceAddress));

    MEMSET (&NlMatrixSDDestAddress, RMON2_ZERO, sizeof (NlMatrixSDDestAddress));

    pNlMatrixSDEntry = Rmon2NlMatrixGetNextDataIndex (RMON2_NLMATRIXSD_TABLE,
                                                      u4CtlIndex,
                                                      u4ProtocolDirLocalIndex,
                                                      &NlMatrixSDSourceAddress,
                                                      &NlMatrixSDDestAddress);
    /* while is for traversing SD table entries */
    while (pNlMatrixSDEntry != NULL)
    {
        if (pNlMatrixSDEntry->u4HlMatrixControlIndex != u4CtlIndex)
        {
            RMON2_TRC (RMON2_DEBUG_TRC,
                       "HlMatrixControlIndex in NlMatrixSD table is not same "
                       "as ctrlIndex \n");
            break;
        }

        MEMCPY (&pNlMatrixSDEntry->NlMatrixSDPrevSample,
                &pNlMatrixSDEntry->NlMatrixSDCurSample,
                sizeof (tNlMatrixSDSample));

        pNlMatrixSDEntry =
            Rmon2NlMatrixGetNextDataIndex (RMON2_NLMATRIXSD_TABLE,
                                           pNlMatrixSDEntry->
                                           u4HlMatrixControlIndex,
                                           pNlMatrixSDEntry->
                                           u4ProtocolDirLocalIndex,
                                           &pNlMatrixSDEntry->
                                           NlMatrixSDSourceAddress,
                                           &pNlMatrixSDEntry->
                                           NlMatrixSDDestAddress);
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT " "GetSamplesFromMatrixTable \n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GetMatrixTopNEntries                                       */
/*                                                                           */
/* Description  : Get the current samples from Matrix table                  */
/*                                                                           */
/* Input        : pMatrixTopNCtl,pSampleTopNList                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT4
GetNlMatrixTopNEntries (tNlMatrixTopNControl * pNlMatrixTopNCtl,
                        tNlMatrixTopN * pSampleTopNList)
{
    UINT4               u4SampleCount = RMON2_ZERO;
    UINT4               u4DeltaPkt = RMON2_ZERO, u4DeltaOct = RMON2_ZERO;
    UINT4               u4ProtocolDirLocalIndex = RMON2_ZERO;
    tIpAddr             NlMatrixSDSourceAddress;
    tIpAddr             NlMatrixSDDestAddress;
    tNlMatrixSD        *pNlMatrixSDEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY " "GetMatrixTopNEntries \n");

    MEMSET (&NlMatrixSDSourceAddress, RMON2_ZERO,
            sizeof (NlMatrixSDSourceAddress));

    MEMSET (&NlMatrixSDDestAddress, RMON2_ZERO, sizeof (NlMatrixSDDestAddress));

    pNlMatrixSDEntry =
        Rmon2NlMatrixGetNextDataIndex (RMON2_NLMATRIXSD_TABLE,
                                       pNlMatrixTopNCtl->
                                       u4NlMatrixTopNControlMatrixIndex,
                                       u4ProtocolDirLocalIndex,
                                       &NlMatrixSDSourceAddress,
                                       &NlMatrixSDDestAddress);
    while (pNlMatrixSDEntry != NULL)
    {
        if (pNlMatrixSDEntry->u4HlMatrixControlIndex !=
            pNlMatrixTopNCtl->u4NlMatrixTopNControlMatrixIndex)
        {
            RMON2_TRC (RMON2_DEBUG_TRC,
                       "HlMatrixControlIndex in NlMatrixSD table is not same "
                       "as NlMatrixTopNControlMatrixIndex in NlMatrixTopN Ctrl "
                       "table \n");
            break;
        }
        switch (pNlMatrixTopNCtl->u4NlMatrixTopNControlRateBase)
        {

            case NLMATRIX_TOPN_PKTS:

                if (pNlMatrixSDEntry->NlMatrixSDPrevSample.u4NlMatrixSDPkts !=
                    RMON2_ZERO)
                {
                    u4DeltaPkt =
                        (pNlMatrixSDEntry->NlMatrixSDCurSample.
                         u4NlMatrixSDPkts) -
                        (pNlMatrixSDEntry->NlMatrixSDPrevSample.
                         u4NlMatrixSDPkts);

                    u4DeltaOct =
                        (pNlMatrixSDEntry->NlMatrixSDCurSample.
                         u4NlMatrixSDOctets) -
                        (pNlMatrixSDEntry->NlMatrixSDPrevSample.
                         u4NlMatrixSDOctets);

                    if (u4DeltaPkt > RMON2_ZERO)
                    {
                        FillNlMatrixSampleTopNRate (pSampleTopNList,
                                                    u4SampleCount,
                                                    pNlMatrixSDEntry,
                                                    u4DeltaPkt, u4DeltaOct,
                                                    pNlMatrixTopNCtl);
                        u4SampleCount++;
                    }

                }

                break;

            case NLMATRIX_TOPN_OCTETS:

                if (pNlMatrixSDEntry->NlMatrixSDPrevSample.u4NlMatrixSDOctets !=
                    RMON2_ZERO)
                {
                    u4DeltaOct =
                        (pNlMatrixSDEntry->NlMatrixSDCurSample.
                         u4NlMatrixSDOctets) -
                        (pNlMatrixSDEntry->NlMatrixSDPrevSample.
                         u4NlMatrixSDOctets);

                    u4DeltaPkt =
                        (pNlMatrixSDEntry->NlMatrixSDCurSample.
                         u4NlMatrixSDPkts) -
                        (pNlMatrixSDEntry->NlMatrixSDPrevSample.
                         u4NlMatrixSDPkts);
                    if (u4DeltaOct > RMON2_ZERO)
                    {
                        FillNlMatrixSampleTopNRate (pSampleTopNList,
                                                    u4SampleCount,
                                                    pNlMatrixSDEntry,
                                                    u4DeltaPkt, u4DeltaOct,
                                                    pNlMatrixTopNCtl);
                        u4SampleCount++;
                    }

                }

                break;

            case NLMATRIX_TOPN_HIGHCAPACITY_PKTS:
                RMON2_TRC (RMON2_DEBUG_TRC, "High Capacity Packets are : "
                           "not supported \n");
                break;
            case NLMATRIX_TOPN_HIGHCAPACITY_OCTETS:
                RMON2_TRC (RMON2_DEBUG_TRC, "High Capacity Octets are : "
                           "not supported \n");
                break;
            default:
                RMON2_TRC (RMON2_DEBUG_TRC, "Unknown Option \n");
                break;

        }                        /* end of switch case */

        pNlMatrixSDEntry =
            Rmon2NlMatrixGetNextDataIndex (RMON2_NLMATRIXSD_TABLE,
                                           pNlMatrixSDEntry->
                                           u4HlMatrixControlIndex,
                                           pNlMatrixSDEntry->
                                           u4ProtocolDirLocalIndex,
                                           &pNlMatrixSDEntry->
                                           NlMatrixSDSourceAddress,
                                           &pNlMatrixSDEntry->
                                           NlMatrixSDDestAddress);

    }                            /*end of while */

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT " "GetMatrixTopNEntries \n");
    return u4SampleCount;
}

/**********************************************************************
* Function           : Rmon2CmpNlPktsFunction                         *
*                                                                     *
* Input (s)          : 2 input parameters such as sample1 and sample2 *
*                      to compare                                     *
*                                                                     *
* Output (s)         : None                                           *
*                                                                     *
* Returns            : 1 if sample1 >sample2,-1 if sample1 < sample2  *
*                      0 otherwise                                    *
*                                                                     *
* Action :                                                            *
* Called  To compare NlMatrixSD entries....                           *
**********************************************************************/
PRIVATE INT4
Rmon2CmpNlPktsFunction (CONST VOID *pParam1, CONST VOID *pParam2)
{
    CONST tNlMatrixTopN *pSample1 = (CONST tNlMatrixTopN *) pParam1;

    CONST tNlMatrixTopN *pSample2 = (CONST tNlMatrixTopN *) pParam2;

    if (pSample1->u4NlMatrixTopNPktRate < pSample2->u4NlMatrixTopNPktRate)
    {
        return -1;
    }
    if (pSample1->u4NlMatrixTopNPktRate > pSample2->u4NlMatrixTopNPktRate)
    {
        return 1;
    }
    if (pSample1->u4NlMatrixTopNPktRate == pSample2->u4NlMatrixTopNPktRate)
    {
        return 0;
    }

    return OSIX_SUCCESS;

}

/**********************************************************************
* Function           : Rmon2CmpNlOctetsFunction                       *
*                                                                     *
* Input (s)          : 2 input parameters such as sample1 and sample2 *
*                      to compare                                     *
*                                                                     *
* Output (s)         : None                                           *
*                                                                     *
* Returns            : 1 if sample1 >sample2,-1 if sample1 < sample2  *
*                      0 otherwise                                    *
*                                                                     *
* Action :                                                            *
* Called  To compare NlMatrixSD entries....                           *
**********************************************************************/

PRIVATE INT4
Rmon2CmpNlOctetsFunction (CONST VOID *pParam1, CONST VOID *pParam2)
{
    CONST tNlMatrixTopN *pSample1 = (CONST tNlMatrixTopN *) pParam1;

    CONST tNlMatrixTopN *pSample2 = (CONST tNlMatrixTopN *) pParam2;

    if (pSample1->u4NlMatrixTopNOctetRate < pSample2->u4NlMatrixTopNOctetRate)
    {
        return -1;
    }
    if (pSample1->u4NlMatrixTopNOctetRate > pSample2->u4NlMatrixTopNOctetRate)
    {
        return 1;
    }
    if (pSample1->u4NlMatrixTopNOctetRate == pSample2->u4NlMatrixTopNOctetRate)
    {
        return 0;
    }

    return OSIX_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : SetSortedMatrixTopNTable                                   */
/*                                                                           */
/* Description  : Generates topn report of size, granted size                */
/*                                                                           */
/* Input        : pSampleTopN,u4GrantedSize                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT4
SetSortedNlMatrixTopNTable (tNlMatrixTopN * pSampleTopN, UINT4 u4Count,
                            UINT4 u4GrantedSize)
{
    tNlMatrixTopN      *pNlMatrixTopN = NULL;
    UINT4               u4Pass;
    UINT4               u4EntryCount = RMON2_ZERO;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY " "SetSortedMatrixTopNTable \n");
    u4EntryCount = (u4Count < u4GrantedSize) ? u4Count : u4GrantedSize;
    u4Count = u4EntryCount - 1;

    for (u4Pass = RMON2_ONE; u4Pass <= u4EntryCount; u4Pass++)
    {
        pNlMatrixTopN =
            Rmon2NlMatrixAddTopNEntry (u4Pass, &pSampleTopN[u4Count]);

        if (pNlMatrixTopN == NULL)
        {
            RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_MEM_FAIL,
                       " Memory Allocation "
                       "RBTree Add Failed - Matrix TopN Table \n");

            return OSIX_FAILURE;
        }
        u4Count--;
    }                            /* end of for */
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT " "SetSortedMatrixTopNTable \n");
    return OSIX_SUCCESS;
}

/**********************************************************************
* Function           : Rmon2UpdateNlMatrixTopNTable                   *
*                                                                     *
* Description        : Called on each second To update Valid Matrix   *
*                      TopN Table entries....                         *
*                                                                     *
* Input (s)          : None.                                          *
*                                                                     *
* Output (s)         : None.                                          *
*                                                                     *
* Returns            : None.                                          *
*                                                                     *
**********************************************************************/
PUBLIC VOID         Rmon2UpdateNlMatrixTopNTable
ARG_LIST ((VOID))
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrl = NULL;
    tNlMatrixSampleTopNList *pSampleTopNList = NULL;
    UINT4               u4EntryCount = RMON2_ZERO;
    UINT4               u4Count = RMON2_ZERO;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY RmonUpdateNlMatrixTopNTable \n");

    if ((pSampleTopNList = (tNlMatrixSampleTopNList *)
         (MemAllocMemBlk (RMON2_NLMATRIX_SAMPLE_TOPN_POOL))) == NULL)
    {
        /* Alloc mem block failed */
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_MEM_FAIL,
                   "MemAlloc Failure for NlMatrix Sample TopN List "
                   "Entry \n");
        return;
    }

    MEMSET (pSampleTopNList, 0, sizeof (tNlMatrixSampleTopNList));
    pNlMatrixTopNCtrl = Rmon2NlMatrixGetNextTopNCtlIndex (RMON2_ZERO);

    /* while is for traversing TopN Control table */

    while (pNlMatrixTopNCtrl != NULL)
    {
        if (pNlMatrixTopNCtrl->u4NlMatrixTopNControlStatus == ACTIVE)
        {
            if (pNlMatrixTopNCtrl->u4NlMatrixTopNControlTimeRemaining >
                RMON2_ZERO)
            {
                if (pNlMatrixTopNCtrl->u4NlMatrixTopNControlTimeRemaining ==
                    pNlMatrixTopNCtrl->u4NlMatrixTopNControlDuration)
                {
                    GetSamplesFromNlMatrixTable (pNlMatrixTopNCtrl->
                                                 u4NlMatrixTopNControlMatrixIndex);
                }

                else
                {
                    if (pNlMatrixTopNCtrl->u4NlMatrixTopNControlTimeRemaining
                        == RMON2_ONE)
                    {

                        /* Get the TopN entries from NlMatrixSD table and 
                         * store in pSampleTopNList*/
                        u4Count = GetNlMatrixTopNEntries (pNlMatrixTopNCtrl,
                                                          (tNlMatrixTopN *)
                                                          pSampleTopNList->
                                                          SampleTopNList);

                        if (u4Count > 0)
                        {
                            /* Has to free TopN Entries if they were already present in
                             * RmonNlTopNRBTree */
                            Rmon2NlDeleteTopNEntries (pNlMatrixTopNCtrl);

                            u4EntryCount = (u4Count < pNlMatrixTopNCtrl->
                                            u4NlMatrixTopNControlGrantedSize) ?
                                u4Count : pNlMatrixTopNCtrl->
                                u4NlMatrixTopNControlGrantedSize;

                            if (pNlMatrixTopNCtrl->
                                u4NlMatrixTopNControlRateBase ==
                                NLMATRIX_TOPN_PKTS)
                            {
                                qsort ((tNlMatrixTopN *) pSampleTopNList->
                                       SampleTopNList, u4EntryCount,
                                       sizeof (tNlMatrixTopN),
                                       (CONST VOID *) &Rmon2CmpNlPktsFunction);
                            }
                            else if (pNlMatrixTopNCtrl->
                                     u4NlMatrixTopNControlRateBase ==
                                     NLMATRIX_TOPN_OCTETS)
                            {
                                qsort ((tNlMatrixTopN *) pSampleTopNList->
                                       SampleTopNList, u4EntryCount,
                                       sizeof (tNlMatrixTopN),
                                       (CONST VOID *)
                                       &Rmon2CmpNlOctetsFunction);
                            }
                            else
                            {
                                RMON2_TRC (RMON2_DEBUG_TRC,
                                           "HIGH CAPACITY PKTS and HIGH CAPACITY OCTETS "
                                           "are not supported \n");
                                MemReleaseMemBlock
                                    (RMON2_NLMATRIX_SAMPLE_TOPN_POOL,
                                     (UINT1 *) pSampleTopNList);
                                return;
                            }

                            if ((SetSortedNlMatrixTopNTable
                                 ((tNlMatrixTopN *) pSampleTopNList->
                                  SampleTopNList, u4Count,
                                  pNlMatrixTopNCtrl->
                                  u4NlMatrixTopNControlGrantedSize)) ==
                                OSIX_FAILURE)
                            {
                                RMON2_TRC (RMON2_DEBUG_TRC,
                                           "Adding to NLMatrix TopN RBtree "
                                           "failed\n");
                                MemReleaseMemBlock
                                    (RMON2_NLMATRIX_SAMPLE_TOPN_POOL,
                                     (UINT1 *) pSampleTopNList);
                                return;

                            }
                            else
                            {
                                pNlMatrixTopNCtrl->
                                    u4NlMatrixTopNControlGeneratedReports++;
                            }
                        }

                        /* start another collection */

                        pNlMatrixTopNCtrl->u4NlMatrixTopNControlTimeRemaining =
                            pNlMatrixTopNCtrl->u4NlMatrixTopNControlDuration +
                            1;
                        pNlMatrixTopNCtrl->u4NlMatrixTopNControlStartTime =
                            OsixGetSysUpTime ();

                    }            /* end of if - Time Remaining == 1 */

                }                /* end of else - Time Remaining >0 */

                pNlMatrixTopNCtrl->u4NlMatrixTopNControlTimeRemaining--;

            }                    /* end of if - Time Remaining >0 */

        }                        /* end of if - Row status active */

        pNlMatrixTopNCtrl =
            Rmon2NlMatrixGetNextTopNCtlIndex (pNlMatrixTopNCtrl->
                                              u4NlMatrixTopNControlIndex);
    }                            /* end of while */

    MemReleaseMemBlock (RMON2_NLMATRIX_SAMPLE_TOPN_POOL,
                        (UINT1 *) pSampleTopNList);

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT RmonUpdateNlMatrixTopNTable \n");

}
