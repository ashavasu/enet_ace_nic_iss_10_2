/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009    
 *                                                                   
 * $Id: rmn2host.c,v 1.13 2013/07/12 12:33:23 siva Exp $
 *
 * Description: This file contains the functional routine for
 *              RMON2 Network Layer and Application Layer
 *              Host modules                                         
 *                                                                   
 *******************************************************************/
#include "rmn2inc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2NlHostGiveLRUEntry                                    */
/*                                                                           */
/* Description  : Gives the LRU Entry                                        */
/*                                                                           */
/* Input        : UINT4 u4HlHostControlIndex                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pNlHost / NULL                                             */
/*                                                                           */
/*****************************************************************************/
PRIVATE tNlHost    *
Rmon2NlHostGiveLRUEntry (UINT4 u4HlHostControlIndex)
{
    tNlHost            *pNlHost = NULL, *pLRUEntry = NULL;
    UINT4               u4NlProtocolDirLocalIndex = 0;
    tIpAddr             NlHostAddress;

    MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2NlHostGiveLRUEntry\r\n");

    /* Get First Entry in NlHost Table */

    pNlHost =
        Rmon2NlHostGetNextDataIndex (u4HlHostControlIndex,
                                     u4NlProtocolDirLocalIndex, &NlHostAddress);
    /* Mark First Entry as LRU entry */

    pLRUEntry = pNlHost;

    while (pNlHost != NULL)
    {
        if (pNlHost->u4HlHostControlIndex != u4HlHostControlIndex)
        {
            break;
        }

        u4NlProtocolDirLocalIndex = pNlHost->u4ProtocolDirLocalIndex;
        MEMCPY (&NlHostAddress, &(pNlHost->NlHostAddress), sizeof (tIpAddr));

        if (pLRUEntry->u4NlHostTimeMark > pNlHost->u4NlHostTimeMark)
        {
            pLRUEntry = pNlHost;
        }
        pNlHost = Rmon2NlHostGetNextDataIndex (u4HlHostControlIndex,
                                               u4NlProtocolDirLocalIndex,
                                               &NlHostAddress);
    }                            /* End of While */

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2NlHostGiveLRUEntry\r\n");
    return pLRUEntry;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AlHostGiveLRUEntry                                    */
/*                                                                           */
/* Description  : Gives the LRU Entry                                        */
/*                                                                           */
/* Input        : UINT4 u4HlHostControlIndex                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pAlHost / NULL                                             */
/*                                                                           */
/*****************************************************************************/
PRIVATE tAlHost    *
Rmon2AlHostGiveLRUEntry (UINT4 u4HlHostControlIndex)
{
    tAlHost            *pAlHost = NULL, *pLRUEntry = NULL;
    UINT4               u4NlProtocolDirLocalIndex = 0;
    UINT4               u4AlProtocolDirLocalIndex = 0;
    tIpAddr             NlHostAddress;

    MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AlHostGiveLRUEntry\r\n");

    /* Get First Entry in AlHost Table */

    pAlHost =
        Rmon2AlHostGetNextDataIndex (u4HlHostControlIndex,
                                     u4NlProtocolDirLocalIndex,
                                     &NlHostAddress, u4AlProtocolDirLocalIndex);

    /* Mark First Entry as LRU entry */

    pLRUEntry = pAlHost;

    while (pAlHost != NULL)
    {
        if (pAlHost->u4HlHostControlIndex != u4HlHostControlIndex)
        {
            break;
        }

        u4NlProtocolDirLocalIndex = pAlHost->u4NlProtocolDirLocalIndex;
        MEMCPY (&NlHostAddress, &(pAlHost->NlHostAddress), sizeof (tIpAddr));
        u4AlProtocolDirLocalIndex = pAlHost->u4AlProtocolDirLocalIndex;

        if (pLRUEntry->u4AlHostTimeMark > pAlHost->u4AlHostTimeMark)
        {
            pLRUEntry = pAlHost;
        }

        pAlHost = Rmon2AlHostGetNextDataIndex (u4HlHostControlIndex,
                                               u4NlProtocolDirLocalIndex,
                                               &NlHostAddress,
                                               u4AlProtocolDirLocalIndex);
    }                            /* End of While */

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AlHostGiveLRUEntry\r\n");
    return pLRUEntry;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2HostProcessPktInfo                                    */
/*                                                                           */
/* Description  : Process Incoming Packet from Control Plane and Update      */
/*                Nl and Al Host Tables                                      */
/*                                                                           */
/* Input        : pPktInfo                                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rmon2HostProcessPktInfo (tRmon2PktInfo * pPktInfo, UINT4 u4PktSize)
{
    tHlHostControl     *pHlHostCtrl = NULL;
    tProtocolDir       *pNlProtDirEntry = NULL, *pAlProtDirEntry = NULL;
    tNlHost            *pCurNlHost = NULL, *pPrevNlHost = NULL;
    tAlHost            *pCurAlHost = NULL, *pPrevAlHost = NULL;
    UINT4               u4HlHostControlIndex = 0;
    INT4                i4RetVal = OSIX_FAILURE;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2HostProcessPktInfo\r\n");
    if (NULL == pPktInfo)
    {

        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get Protocol Entry failed for NLProtocolLocIndex\r\n");
        return i4RetVal;
    }
    pHlHostCtrl = Rmon2HlHostGetNextCtrlIndex (u4HlHostControlIndex);

    while (pHlHostCtrl != NULL)
    {
        u4HlHostControlIndex = pHlHostCtrl->u4HlHostControlIndex;

        if ((pHlHostCtrl->u4HlHostControlStatus == ACTIVE) &&
            ((pHlHostCtrl->u4HlHostControlDataSource ==
              pPktInfo->PktHdr.u4IfIndex) ||
             (pHlHostCtrl->u4HlHostControlDataSource ==
              pPktInfo->PktHdr.u4VlanIfIndex)))
        {
            if (pHlHostCtrl->u4HlHostControlDataSource ==
                pPktInfo->PktHdr.u4VlanIfIndex)
            {
                pPktInfo->u1IsL3Vlan = OSIX_TRUE;
            }
            pNlProtDirEntry =
                Rmon2PDirGetProtocolNode (pPktInfo->u4ProtoNLIndex);
            if (pNlProtDirEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "Get Protocol Entry failed for NLProtocolLocIndex\r\n");
                pHlHostCtrl = Rmon2HlHostGetNextCtrlIndex (pHlHostCtrl->
                                                           u4HlHostControlIndex);
                continue;
            }
            if ((pPktInfo->u4ProtoNLIndex != 0) &&
                (pNlProtDirEntry->u4ProtocolDirHostConfig == SUPPORTED_ON))
            {
                /* Checking for an NlHost Entry already exist or not */
                pCurNlHost =
                    Rmon2NlHostGetDataEntry (pHlHostCtrl->u4HlHostControlIndex,
                                             pPktInfo->u4ProtoNLIndex,
                                             &(pPktInfo->PktHdr.SrcIp));
                if (pCurNlHost == NULL)
                {
                    if (pHlHostCtrl->u4HlHostControlNlMaxDesiredSupported == 0)
                    {
                        pHlHostCtrl->u4HlHostControlNlDroppedFrames++;
                        pHlHostCtrl->u4HlHostControlAlDroppedFrames++;
                        pHlHostCtrl =
                            Rmon2HlHostGetNextCtrlIndex (pHlHostCtrl->
                                                         u4HlHostControlIndex);
                        continue;
                    }

                    /* Check whether NlHost data entries reached max limit */
                    if ((pHlHostCtrl->u4HlHostControlNlInserts -
                         pHlHostCtrl->u4HlHostControlNlDeletes) >=
                        pHlHostCtrl->u4HlHostControlNlMaxDesiredSupported)
                    {
                        pCurNlHost = Rmon2NlHostGiveLRUEntry
                            (pHlHostCtrl->u4HlHostControlIndex);

                        if (pCurNlHost != NULL)
                        {
                            RMON2_TRC (RMON2_DEBUG_TRC,
                                       "Deleting NlHost LRU Entry\r\n");

                            /* Remove Inter table references */
                            Rmon2NlHostDelInterDep (pCurNlHost);

                            pCurNlHost->pHlHostCtrl->u4HlHostControlNlDeletes++;

                            /* After Reference changes, free Nl Host entry */
                            Rmon2NlHostDelDataEntry (pCurNlHost);

                            pCurNlHost = NULL;
                        }
                    }

                    /* Add New NlHost Entry */
                    pCurNlHost =
                        Rmon2NlHostAddDataEntry (pHlHostCtrl->
                                                 u4HlHostControlIndex,
                                                 pPktInfo->u4ProtoNLIndex,
                                                 &(pPktInfo->PktHdr.SrcIp));

                    if (pCurNlHost == NULL)
                    {
                        pHlHostCtrl->u4HlHostControlNlDroppedFrames++;
                        /* Get Next HlHost Ctrl Index */
                        pHlHostCtrl =
                            Rmon2HlHostGetNextCtrlIndex (u4HlHostControlIndex);
                        continue;
                    }
                    else
                    {
                        pCurNlHost->pHlHostCtrl = pHlHostCtrl;
                        pHlHostCtrl->u4HlHostControlNlInserts++;
                        if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV4)
                        {
                            pCurNlHost->u4Rmon2NlHostIpAddrLen =
                                RMON2_IPV4_MAX_LEN;
                        }
                        else if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV6)
                        {
                            pCurNlHost->u4Rmon2NlHostIpAddrLen =
                                RMON2_IPV6_MAX_LEN;
                        }
                    }
                }
                pCurNlHost->u4NlHostOutPkts++;
                pCurNlHost->u4NlHostOutOctets += u4PktSize;
                pCurNlHost->u4PktRefCount++;
                pCurNlHost->u4NlHostTimeMark = OsixGetSysUpTime ();

                if (pPrevNlHost == NULL)
                {
                    pPktInfo->pOutNlHost = pCurNlHost;
                }
                else
                {
                    pPrevNlHost->pNextNlHost = pCurNlHost;
                    pCurNlHost->pPrevNlHost = pPrevNlHost;
                }
                pPrevNlHost = pCurNlHost;
                i4RetVal = OSIX_SUCCESS;
            }

            pAlProtDirEntry =
                Rmon2PDirGetProtocolNode (pPktInfo->u4ProtoL5ALIndex);
            if (pAlProtDirEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "Get Protocol Entry failed for L5ProtocolLocIndex\r\n");
                pHlHostCtrl = Rmon2HlHostGetNextCtrlIndex (pHlHostCtrl->
                                                           u4HlHostControlIndex);
                continue;
            }

            if ((pPktInfo->u4ProtoNLIndex != 0) &&
                (pPktInfo->u4ProtoL5ALIndex != 0) &&
                (pNlProtDirEntry->u4ProtocolDirHostConfig == SUPPORTED_ON) &&
                (pAlProtDirEntry->u4ProtocolDirHostConfig == SUPPORTED_ON))
            {
                pCurAlHost = NULL;

                /* Checking for an AlHost Entry (for L5 Protocol) 
                 * already exist or not */
                pCurAlHost =
                    Rmon2AlHostGetDataEntry (pHlHostCtrl->u4HlHostControlIndex,
                                             pPktInfo->u4ProtoNLIndex,
                                             &(pPktInfo->PktHdr.SrcIp),
                                             pPktInfo->u4ProtoL5ALIndex);
                /* Add pAlHost Entry only if pNlHost Entry exist */
                if ((pCurAlHost == NULL) && (pCurNlHost != NULL))
                {
                    if (pHlHostCtrl->u4HlHostControlAlMaxDesiredSupported == 0)
                    {
                        pHlHostCtrl->u4HlHostControlAlDroppedFrames++;
                        pHlHostCtrl =
                            Rmon2HlHostGetNextCtrlIndex (pHlHostCtrl->
                                                         u4HlHostControlIndex);
                        continue;
                    }
                    /* Check whether AlHost data entries reached max limit */
                    if ((pHlHostCtrl->u4HlHostControlAlInserts -
                         pHlHostCtrl->u4HlHostControlAlDeletes) >=
                        pHlHostCtrl->u4HlHostControlAlMaxDesiredSupported)
                    {
                        pCurAlHost = Rmon2AlHostGiveLRUEntry
                            (pHlHostCtrl->u4HlHostControlIndex);

                        if (pCurAlHost != NULL)
                        {
                            RMON2_TRC (RMON2_DEBUG_TRC,
                                       "Deleting AlHost LRU Entry\r\n");

                            /* Remove Inter table references */
                            Rmon2AlHostDelInterDep (pCurAlHost);

                            pCurAlHost->pHlHostCtrl->u4HlHostControlAlDeletes++;

                            /* After Reference changes, free Al Host entry */
                            Rmon2AlHostDelDataEntry (pCurAlHost);

                            pCurAlHost = NULL;
                        }
                    }
                    pCurAlHost =
                        Rmon2AlHostAddDataEntry (pHlHostCtrl->
                                                 u4HlHostControlIndex,
                                                 pPktInfo->u4ProtoNLIndex,
                                                 &(pPktInfo->PktHdr.SrcIp),
                                                 pPktInfo->u4ProtoL5ALIndex);

                    if (pCurAlHost == NULL)
                    {
                        pHlHostCtrl->u4HlHostControlAlDroppedFrames++;
                        pHlHostCtrl =
                            Rmon2HlHostGetNextCtrlIndex (pHlHostCtrl->
                                                         u4HlHostControlIndex);
                        continue;
                    }
                    else
                    {
                        pCurAlHost->pHlHostCtrl = pHlHostCtrl;
                        pHlHostCtrl->u4HlHostControlAlInserts++;
                        if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV4)
                        {
                            pCurAlHost->u4Rmon2NlHostIpAddrLen =
                                RMON2_IPV4_MAX_LEN;
                        }
                        else if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV6)
                        {
                            pCurAlHost->u4Rmon2NlHostIpAddrLen =
                                RMON2_IPV6_MAX_LEN;
                        }
                    }
                }
                pCurAlHost->u4PktRefCount++;
                pCurAlHost->u4AlHostOutPkts++;
                pCurAlHost->u4AlHostOutOctets += u4PktSize;
                pCurAlHost->u4AlHostTimeMark = OsixGetSysUpTime ();

                if (pPrevAlHost == NULL)
                {
                    pPktInfo->pOutAlHost = pCurAlHost;
                }
                else
                {
                    pPrevAlHost->pNextAlHost = pCurAlHost;
                    pCurAlHost->pPrevAlHost = pPrevAlHost;
                }
                pPrevAlHost = pCurAlHost;
                i4RetVal = OSIX_SUCCESS;
            }

            pAlProtDirEntry =
                Rmon2PDirGetProtocolNode (pPktInfo->u4ProtoL4ALIndex);
            if (pAlProtDirEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "Get Protocol Entry failed for L4ProtocolLocIndex\r\n");
                pHlHostCtrl = Rmon2HlHostGetNextCtrlIndex (pHlHostCtrl->
                                                           u4HlHostControlIndex);
                continue;
            }

            if ((pPktInfo->u4ProtoNLIndex != 0) &&
                (pPktInfo->u4ProtoL4ALIndex != 0) &&
                (pNlProtDirEntry->u4ProtocolDirHostConfig == SUPPORTED_ON) &&
                (pAlProtDirEntry->u4ProtocolDirHostConfig == SUPPORTED_ON))
            {
                /* Checking for an AlHost Entry (for L4 Protocol) 
                 * already exist or not */
                pCurAlHost =
                    Rmon2AlHostGetDataEntry (pHlHostCtrl->u4HlHostControlIndex,
                                             pPktInfo->u4ProtoNLIndex,
                                             &(pPktInfo->PktHdr.SrcIp),
                                             pPktInfo->u4ProtoL4ALIndex);
                /* Add pAlHost Entry only if pNlHost Entry exist */
                if ((pCurAlHost == NULL) && (pCurNlHost != NULL))
                {
                    if (pHlHostCtrl->u4HlHostControlAlMaxDesiredSupported == 0)
                    {
                        pHlHostCtrl->u4HlHostControlAlDroppedFrames++;
                        pHlHostCtrl =
                            Rmon2HlHostGetNextCtrlIndex (pHlHostCtrl->
                                                         u4HlHostControlIndex);
                        continue;
                    }
                    /* Check whether AlHost data entries reached max limit */
                    if ((pHlHostCtrl->u4HlHostControlAlInserts -
                         pHlHostCtrl->u4HlHostControlAlDeletes) >=
                        pHlHostCtrl->u4HlHostControlAlMaxDesiredSupported)
                    {
                        pCurAlHost = Rmon2AlHostGiveLRUEntry
                            (pHlHostCtrl->u4HlHostControlIndex);

                        if (pCurAlHost != NULL)
                        {
                            RMON2_TRC (RMON2_DEBUG_TRC,
                                       "Deleting AlHost LRU Entry\r\n");

                            /* Remove Inter table references */
                            Rmon2AlHostDelInterDep (pCurAlHost);

                            pCurAlHost->pHlHostCtrl->u4HlHostControlAlDeletes++;

                            /* After Reference changes, free Al Host entry */
                            Rmon2AlHostDelDataEntry (pCurAlHost);

                            pCurAlHost = NULL;
                        }
                    }
                    pCurAlHost =
                        Rmon2AlHostAddDataEntry (pHlHostCtrl->
                                                 u4HlHostControlIndex,
                                                 pPktInfo->u4ProtoNLIndex,
                                                 &(pPktInfo->PktHdr.SrcIp),
                                                 pPktInfo->u4ProtoL4ALIndex);

                    if (pCurAlHost == NULL)
                    {
                        pHlHostCtrl->u4HlHostControlAlDroppedFrames++;
                        pHlHostCtrl =
                            Rmon2HlHostGetNextCtrlIndex (pHlHostCtrl->
                                                         u4HlHostControlIndex);
                        continue;
                    }
                    else
                    {
                        pCurAlHost->pHlHostCtrl = pHlHostCtrl;
                        pHlHostCtrl->u4HlHostControlAlInserts++;
                        if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV4)
                        {
                            pCurAlHost->u4Rmon2NlHostIpAddrLen =
                                RMON2_IPV4_MAX_LEN;
                        }
                        else if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV6)
                        {
                            pCurAlHost->u4Rmon2NlHostIpAddrLen =
                                RMON2_IPV6_MAX_LEN;
                        }
                    }
                }

                pCurAlHost->u4AlHostOutPkts++;
                pCurAlHost->u4AlHostOutOctets += u4PktSize;
                pCurAlHost->u4PktRefCount++;
                pCurAlHost->u4AlHostTimeMark = OsixGetSysUpTime ();
                if (pPrevAlHost == NULL)
                {
                    pPktInfo->pOutAlHost = pCurAlHost;
                }
                else
                {
                    pPrevAlHost->pNextAlHost = pCurAlHost;
                    pCurAlHost->pPrevAlHost = pPrevAlHost;
                }
                pPrevAlHost = pCurAlHost;
                i4RetVal = OSIX_SUCCESS;
            }

        }                        /* End of if check */
        pHlHostCtrl =
            Rmon2HlHostGetNextCtrlIndex (pHlHostCtrl->u4HlHostControlIndex);
    }                            /* End of while */

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2HostProcessPktInfo\r\n");
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2HostUpdateIFIndexChgs                                 */
/*                                                                           */
/* Description  : If Operstatus is CFA_IF_DOWN, then set appropriate control */
/*                entries to NOT_READY state thus deleting the associated    */
/*                Data Table. If OperStatus is CFA_IF_UP, set NOT_READY      */
/*                entries to ACTIVE state                                    */
/*                                                                           */
/* Input        : u4IfIndex, u1OperStatus                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2HostUpdateIFIndexChgs (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tHlHostControl     *pHlHostCtrl = NULL;
    UINT4               u4HlHostControlIndex = 0;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2HostUpdateIFIndexChgs\r\n");

    switch (u1OperStatus)
    {
        case CFA_IF_DOWN:
            /* Get 1st Hl Matrix Ctrl Index */
            pHlHostCtrl = Rmon2HlHostGetNextCtrlIndex (u4HlHostControlIndex);
            while (pHlHostCtrl != NULL)
            {
                u4HlHostControlIndex = pHlHostCtrl->u4HlHostControlIndex;
                if ((pHlHostCtrl->u4HlHostControlStatus == ACTIVE) &&
                    (pHlHostCtrl->u4HlHostControlDataSource == u4IfIndex))
                {

                    /* Remove Data Entries for this control index 
                     * from Nl & Al Host Table */

                    Rmon2HostDelDataEntries (u4HlHostControlIndex);

                    /* Change RowStatus to NOT_READY */
                    pHlHostCtrl->u4HlHostControlStatus = NOT_READY;
                }                /* End of if check */
                pHlHostCtrl =
                    Rmon2HlHostGetNextCtrlIndex (u4HlHostControlIndex);
            }                    /* End of while */
            break;
        case CFA_IF_UP:
            /* Get 1st Hl Matrix Ctrl Index */
            pHlHostCtrl = Rmon2HlHostGetNextCtrlIndex (u4HlHostControlIndex);

            while (pHlHostCtrl != NULL)
            {
                u4HlHostControlIndex = pHlHostCtrl->u4HlHostControlIndex;
                if ((pHlHostCtrl->u4HlHostControlStatus == NOT_READY) &&
                    (pHlHostCtrl->u4HlHostControlDataSource == u4IfIndex))
                {
                    /* Set RowStatus to ACTIVE */
                    nmhSetHlHostControlStatus ((INT4) u4HlHostControlIndex,
                                               ACTIVE);
                }                /* End of if check */
                pHlHostCtrl =
                    Rmon2HlHostGetNextCtrlIndex (u4HlHostControlIndex);
            }                    /* End of while */
            break;
        default:
            break;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2HostUpdateIFIndexChgs\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2HostUpdateProtoChgs                                   */
/*                                                                           */
/* Description  : Removes Nl and Al Host Data Table Entries for change in    */
/*                Protocol Dir Entry Status or Host config support is        */
/*                turned off for a Protocol Dir Entry.                       */
/*                                                                           */
/* Input        : u4ProtocolDirLocalIndex                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2HostUpdateProtoChgs (UINT4 u4ProtocolIndex)
{
    tNlHost            *pNlHost = NULL;
    tAlHost            *pAlHost = NULL;
    UINT4               u4HlHostControlIndex = 0;
    UINT4               u4NlProtocolDirLocalIndex = 0;
    UINT4               u4AlProtocolDirLocalIndex = 0;
    tIpAddr             NlHostAddress;

    MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2HostUpdateProtoChgs\r\n");
    /* Get First Entry in Nl Host Table */

    pNlHost = Rmon2NlHostGetNextDataIndex (u4HlHostControlIndex,
                                           u4NlProtocolDirLocalIndex,
                                           &NlHostAddress);

    while (pNlHost != NULL)
    {
        u4HlHostControlIndex = pNlHost->u4HlHostControlIndex;
        u4NlProtocolDirLocalIndex = pNlHost->u4ProtocolDirLocalIndex;
        MEMCPY (&NlHostAddress, &(pNlHost->NlHostAddress), sizeof (tIpAddr));

        if (pNlHost->u4ProtocolDirLocalIndex == u4ProtocolIndex)
        {
            /* Remove Inter table dependencies */
            Rmon2NlHostDelInterDep (pNlHost);

            pNlHost->pHlHostCtrl->u4HlHostControlNlDeletes++;

            /* After Reference changes, free Nl Host entry */
            Rmon2NlHostDelDataEntry (pNlHost);
        }

        pNlHost = Rmon2NlHostGetNextDataIndex (u4HlHostControlIndex,
                                               u4NlProtocolDirLocalIndex,
                                               &NlHostAddress);
    }

    u4HlHostControlIndex = 0;
    u4NlProtocolDirLocalIndex = 0;
    MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));

    /* Get First Entry in Al Host Table for NL/AL protocol */

    pAlHost = Rmon2AlHostGetNextDataIndex (u4HlHostControlIndex,
                                           u4NlProtocolDirLocalIndex,
                                           &NlHostAddress,
                                           u4AlProtocolDirLocalIndex);

    while (pAlHost != NULL)
    {
        u4HlHostControlIndex = pAlHost->u4HlHostControlIndex;
        u4NlProtocolDirLocalIndex = pAlHost->u4NlProtocolDirLocalIndex;
        MEMCPY (&NlHostAddress, &(pAlHost->NlHostAddress), sizeof (tIpAddr));
        u4AlProtocolDirLocalIndex = pAlHost->u4AlProtocolDirLocalIndex;

        if ((pAlHost->u4NlProtocolDirLocalIndex == u4ProtocolIndex) ||
            (pAlHost->u4AlProtocolDirLocalIndex == u4ProtocolIndex))
        {
            /* Remove Inter table dependencies */
            Rmon2AlHostDelInterDep (pAlHost);

            pAlHost->pHlHostCtrl->u4HlHostControlAlDeletes++;

            /* After Reference changes, free Al Host entry 
             * for NL/AL protocol */
            Rmon2AlHostDelDataEntry (pAlHost);
        }

        pAlHost = Rmon2AlHostGetNextDataIndex (u4HlHostControlIndex,
                                               u4NlProtocolDirLocalIndex,
                                               &NlHostAddress,
                                               u4AlProtocolDirLocalIndex);
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2HostUpdateProtoChgs\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2HostDelDataEntries                                    */
/*                                                                           */
/* Description  : Removes Nl and Al Host Data Table Entries for              */
/*                given Control Index                                        */
/*                                                                           */
/* Input        : u4HlHostControlIndex                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2HostDelDataEntries (UINT4 u4HlHostControlIndex)
{
    tNlHost            *pNlHost = NULL;
    tAlHost            *pAlHost = NULL;

    UINT4               u4NlProtocolDirLocalIndex = 0;
    UINT4               u4AlProtocolDirLocalIndex = 0;
    tIpAddr             NlHostAddress;

    MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2HostDelDataEntries\r\n");

    /* Remove Data Entries for this control index from Nl Host Table */
    pNlHost = Rmon2NlHostGetNextDataIndex (u4HlHostControlIndex,
                                           u4NlProtocolDirLocalIndex,
                                           &NlHostAddress);

    while (pNlHost != NULL)
    {
        if (pNlHost->u4HlHostControlIndex != u4HlHostControlIndex)
        {
            break;
        }

        u4NlProtocolDirLocalIndex = pNlHost->u4ProtocolDirLocalIndex;
        MEMCPY (&NlHostAddress, &(pNlHost->NlHostAddress), sizeof (tIpAddr));

        /* Remove Inter table references */
        Rmon2NlHostDelInterDep (pNlHost);

        pNlHost->pHlHostCtrl->u4HlHostControlNlDeletes++;

        /* After Reference changes, free Nl Host entry */
        Rmon2NlHostDelDataEntry (pNlHost);
        pNlHost = Rmon2NlHostGetNextDataIndex (u4HlHostControlIndex,
                                               u4NlProtocolDirLocalIndex,
                                               &NlHostAddress);
    }

    u4NlProtocolDirLocalIndex = 0;
    MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));

    /* Remove Data Entries for this control index from Al Host Table */
    pAlHost =
        Rmon2AlHostGetNextDataIndex (u4HlHostControlIndex,
                                     u4NlProtocolDirLocalIndex,
                                     &NlHostAddress, u4AlProtocolDirLocalIndex);

    while (pAlHost != NULL)
    {
        if (pAlHost->u4HlHostControlIndex != u4HlHostControlIndex)
        {
            break;
        }

        u4NlProtocolDirLocalIndex = pAlHost->u4NlProtocolDirLocalIndex;
        MEMCPY (&NlHostAddress, &(pAlHost->NlHostAddress), sizeof (tIpAddr));
        u4AlProtocolDirLocalIndex = pAlHost->u4AlProtocolDirLocalIndex;

        /* Update Inter table references */
        Rmon2AlHostDelInterDep (pAlHost);

        pAlHost->pHlHostCtrl->u4HlHostControlAlDeletes++;

        /* After Reference changes, free Al Host entry */
        Rmon2AlHostDelDataEntry (pAlHost);
        pAlHost = Rmon2AlHostGetNextDataIndex (u4HlHostControlIndex,
                                               u4NlProtocolDirLocalIndex,
                                               &NlHostAddress,
                                               u4AlProtocolDirLocalIndex);
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2HostDelDataEntries\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2HlHostGetNextCtrlIndex                                */
/*                                                                           */
/* Description  : Get First / Next Hl Host Ctrl Index                        */
/*                                                                           */
/* Input        : u4HlHostControlIndex                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tHlHostControl / NULL Pointer                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC tHlHostControl *
Rmon2HlHostGetNextCtrlIndex (UINT4 u4HlHostControlIndex)
{
    tHlHostControl      HlHostCtrl;
    MEMSET (&HlHostCtrl, 0, sizeof (tHlHostControl));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2HlHostGetNextCtrlIndex\r\n");

    HlHostCtrl.u4HlHostControlIndex = u4HlHostControlIndex;
    return ((tHlHostControl *) RBTreeGetNext (RMON2_HLHOSTCTRL_TREE,
                                              (tRBElem *) & HlHostCtrl, NULL));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2HlHostAddCtrlEntry                                    */
/*                                                                           */
/* Description  : Allocates memory and Adds Hl Host Control Entry into       */
/*                HlHostControlRBTree                                        */
/*                                                                           */
/* Input        : pHlHostCtrl                                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tHlHostControl / NULL Pointer                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC tHlHostControl *
Rmon2HlHostAddCtrlEntry (UINT4 u4HlHostControlIndex)
{
    tHlHostControl     *pHlHostCtrl = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2HlHostAddCtrlEntry\r\n");

    if ((pHlHostCtrl = (tHlHostControl *)
         (MemAllocMemBlk (RMON2_HLHOSTCTRL_POOL))) == NULL)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for HlHostControlEntry\r\n");
        return NULL;
    }
    MEMSET (pHlHostCtrl, 0, sizeof (tHlHostControl));
    pHlHostCtrl->u4HlHostControlIndex = u4HlHostControlIndex;

    if (RBTreeAdd (RMON2_HLHOSTCTRL_TREE, (tRBElem *) pHlHostCtrl)
        == RB_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Rmon2HlHostAddCtrlEntry: HlHostControlEntry is added\r\n");
        return pHlHostCtrl;
    }
    RMON2_TRC (RMON2_CRITICAL_TRC,
               "Rmon2HlHostAddCtrlEntry: Adding HlHostControlEntry Failed\r\n");

    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (RMON2_HLHOSTCTRL_POOL, (UINT1 *) pHlHostCtrl);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2HlHostAddCtrlEntry\r\n");

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2HlHostDelCtrlEntry                                    */
/*                                                                           */
/* Description  : Releases memory and Removes Hl Host Control Entry from     */
/*                HlHostControlRBTree                                        */
/*                                                                           */
/* Input        : pHlHostCtrl                                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rmon2HlHostDelCtrlEntry (tHlHostControl * pHlHostCtrl)
{
    tPortList          *pVlanPortList = NULL;
    tCfaIfInfo          IfInfo;
    UINT2               u2VlanId = 0;

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
    pVlanPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pVlanPortList == NULL)
    {
        RMON2_TRC (RMON2_FN_ENTRY,
                   "Rmon2HlHostDelCtrlEntry: "
                   "Error in allocating memory for pVlanPortList\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pVlanPortList, 0, sizeof (tPortList));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2HlHostDelCtrlEntry\r\n");

    if (CfaGetIfInfo (pHlHostCtrl->u4HlHostControlDataSource, &IfInfo) !=
        CFA_FAILURE)
    {
        if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
            (CfaGetVlanId (pHlHostCtrl->u4HlHostControlDataSource, &u2VlanId) !=
             CFA_FAILURE))
        {
            L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList);
        }
    }
#ifdef NPAPI_WANTED
    /* Remove Probe support from H/W Table */
    Rmonv2FsRMONv2DisableProbe (pHlHostCtrl->u4HlHostControlDataSource,
                                u2VlanId, *pVlanPortList);
#endif

    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
    /* Remove HlHost Ctrl node from HlHostControlRBTree */
    RBTreeRem (RMON2_HLHOSTCTRL_TREE, (tRBElem *) pHlHostCtrl);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (RMON2_HLHOSTCTRL_POOL,
                            (UINT1 *) pHlHostCtrl) != MEM_SUCCESS)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for HlHostControlEntry\r\n");
        return OSIX_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2HlHostDelCtrlEntry\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2HlHostGetCtrlEntry                                    */
/*                                                                           */
/* Description  : Get Hl Host Ctrl Entry for the given control index         */
/*                                                                           */
/* Input        : u4HlHostControlIndex                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tHlHostControl / NULL Pointer                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC tHlHostControl *
Rmon2HlHostGetCtrlEntry (UINT4 u4HlHostControlIndex)
{
    tHlHostControl      HlHostCtrl;
    MEMSET (&HlHostCtrl, 0, sizeof (tHlHostControl));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2HlHostGetCtrlEntry\r\n");

    HlHostCtrl.u4HlHostControlIndex = u4HlHostControlIndex;

    return ((tHlHostControl *) RBTreeGet (RMON2_HLHOSTCTRL_TREE,
                                          (tRBElem *) & HlHostCtrl));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2NlHostGetNextDataIndex                                */
/*                                                                           */
/* Description  : Get First / Next Nl Host Index                             */
/*                                                                           */
/* Input        : u4HlHostControlIndex, u4ProtocolDirLocalIndex              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tNlHost / NULL Pointer                                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC tNlHost     *
Rmon2NlHostGetNextDataIndex (UINT4 u4HlHostControlIndex,
                             UINT4 u4ProtocolDirLocalIndex,
                             tIpAddr * pNlHostAddress)
{
    tNlHost             NlHost, *pNlHost = NULL;
    MEMSET (&NlHost, 0, sizeof (tNlHost));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2NlHostGetNextDataIndex\r\n");
    if ((u4HlHostControlIndex != 0) && (u4ProtocolDirLocalIndex == 0))
    {
        pNlHost = (tNlHost *) RBTreeGetFirst (RMON2_NLHOST_TREE);
        while (pNlHost != NULL)
        {
            if (pNlHost->u4HlHostControlIndex == u4HlHostControlIndex)
            {
                break;
            }

            MEMCPY (&NlHost, pNlHost, sizeof (tNlHost));

            pNlHost = ((tNlHost *) RBTreeGetNext (RMON2_NLHOST_TREE,
                                                  (tRBElem *) & NlHost, NULL));
        }
    }
    else
    {
        NlHost.u4HlHostControlIndex = u4HlHostControlIndex;
        NlHost.u4ProtocolDirLocalIndex = u4ProtocolDirLocalIndex;
        MEMCPY (&(NlHost.NlHostAddress), pNlHostAddress, sizeof (tIpAddr));

        RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2NlHostGetNextDataIndex\r\n");
        pNlHost = ((tNlHost *) RBTreeGetNext (RMON2_NLHOST_TREE,
                                              (tRBElem *) & NlHost, NULL));
    }
    return pNlHost;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2NlHostAddDataEntry                                    */
/*                                                                           */
/* Description  : Allocates memory and Adds Nl Host Entry into               */
/*                NlHostRBTree                                               */
/*                                                                           */
/* Input        : pNlHost                                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tNlHost / NULL Pointer                                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC tNlHost     *
Rmon2NlHostAddDataEntry (UINT4 u4HlHostControlIndex,
                         UINT4 u4ProtocolDirLocalIndex,
                         tIpAddr * pNlHostAddress)
{
    tNlHost            *pNlHost = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2NlHostAddDataEntry\r\n");

    if ((pNlHost = (tNlHost *) (MemAllocMemBlk (RMON2_NLHOST_POOL))) == NULL)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for NlHostEntry\r\n");
        return NULL;
    }

    MEMSET (pNlHost, 0, sizeof (tNlHost));

    pNlHost->u4HlHostControlIndex = u4HlHostControlIndex;
    pNlHost->u4ProtocolDirLocalIndex = u4ProtocolDirLocalIndex;
    pNlHost->u4NlHostTimeMark = OsixGetSysUpTime ();
    pNlHost->u4NlHostCreateTime = OsixGetSysUpTime ();

    MEMCPY (&(pNlHost->NlHostAddress), pNlHostAddress, sizeof (tIpAddr));

    if (RBTreeAdd (RMON2_NLHOST_TREE, (tRBElem *) pNlHost) == RB_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Rmon2NlHostAddDataEntry: NlHostEntry is added\r\n");
        return pNlHost;
    }
    RMON2_TRC (RMON2_CRITICAL_TRC,
               "Rmon2NlHostAddDataEntry: Adding NlHostEntry Failed\r\n");
    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (RMON2_NLHOST_POOL, (UINT1 *) pNlHost);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2NlHostAddDataEntry\r\n");

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2NlHostDelDataEntry                                    */
/*                                                                           */
/* Description  : Releases memory and Removes Nl Host Entry from             */
/*                NlHostRBTree                                               */
/*                                                                           */
/* Input        : pNlHost                                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rmon2NlHostDelDataEntry (tNlHost * pNlHost)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2NlHostDelDataEntry\r\n");

    /* Remove Nl Host node from NlHostRBTree */
    RBTreeRem (RMON2_NLHOST_TREE, (tRBElem *) pNlHost);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (RMON2_NLHOST_POOL,
                            (UINT1 *) pNlHost) != MEM_SUCCESS)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for NlHostEntry\r\n");
        return OSIX_FAILURE;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2NlHostDelDataEntry\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2NlHostGetDataEntry                                    */
/*                                                                           */
/* Description  : Get Nl Host Entry for the given index                      */
/*                                                                           */
/* Input        : u4HlHostControlIndex, u4ProtocolDirLocalIndex              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tNlHost / NULL Pointer                                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC tNlHost     *
Rmon2NlHostGetDataEntry (UINT4 u4HlHostControlIndex,
                         UINT4 u4ProtocolDirLocalIndex,
                         tIpAddr * pNlHostAddress)
{
    tNlHost             NlHost;
    MEMSET (&NlHost, 0, sizeof (tNlHost));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2NlHostGetDataEntry\r\n");

    NlHost.u4HlHostControlIndex = u4HlHostControlIndex;
    NlHost.u4ProtocolDirLocalIndex = u4ProtocolDirLocalIndex;
    MEMCPY (&(NlHost.NlHostAddress), pNlHostAddress, sizeof (tIpAddr));

    return ((tNlHost *) RBTreeGet (RMON2_NLHOST_TREE, (tRBElem *) & NlHost));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2NlHostDelInterDep                                     */
/*                                                                           */
/* Description  : This routine updates the inter table references, before    */
/*                the data entry is removed and intimate the changes to NPAPI*/
/*                                                                           */
/* Input        : pNlHost                                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2NlHostDelInterDep (tNlHost * pNlHost)
{
    tRmon2PktInfo      *pPktInfo = NULL;
    tPktHeader          PktHdr;

    MEMSET (&PktHdr, 0, sizeof (tPktHeader));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2NlHostDelInterDep\r\n");

    if (pNlHost->pPrevNlHost == NULL)
    {
        /* Update PktInfo nodes to point next pHost Entry */
        pPktInfo = Rmon2PktInfoGetNextIndex (&PktHdr);

        while (pPktInfo != NULL)
        {
            if (pPktInfo->pOutNlHost == pNlHost)
            {
                pPktInfo->pOutNlHost = pNlHost->pNextNlHost;
                if (pPktInfo->pOutNlHost != NULL)
                {
                    pPktInfo->pOutNlHost->pPrevNlHost = NULL;
                }
            }
            pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
        }
    }
    else if (pNlHost->pNextNlHost != NULL)
    {
        pNlHost->pPrevNlHost->pNextNlHost = pNlHost->pNextNlHost;
    }
    else
    {
        pNlHost->pPrevNlHost->pNextNlHost = NULL;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2NlHostDelInterDep\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AlHostGetNextDataIndex                                */
/*                                                                           */
/* Description  : Get First / Next Al Host Index                             */
/*                                                                           */
/* Input        : u4HlHostControlIndex, u4NlProtocolDirLocalIndex,           */
/*                pNlHostAddress, u4AlProtocolDirLocalIndex                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tAlHost / NULL Pointer                                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC tAlHost     *
Rmon2AlHostGetNextDataIndex (UINT4 u4HlHostControlIndex,
                             UINT4 u4NlProtocolDirLocalIndex,
                             tIpAddr * pNlHostAddress,
                             UINT4 u4AlProtocolDirLocalIndex)
{
    tAlHost             AlHost, *pAlHost = NULL;
    MEMSET (&AlHost, 0, sizeof (tAlHost));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AlHostGetNextDataIndex\r\n");

    if ((u4HlHostControlIndex != 0) && (u4NlProtocolDirLocalIndex == 0))
    {
        pAlHost = (tAlHost *) RBTreeGetFirst (RMON2_ALHOST_TREE);
        while (pAlHost != NULL)
        {
            if (pAlHost->u4HlHostControlIndex == u4HlHostControlIndex)
            {
                break;
            }

            MEMCPY (&AlHost, pAlHost, sizeof (tAlHost));

            pAlHost = ((tAlHost *) RBTreeGetNext (RMON2_ALHOST_TREE,
                                                  (tRBElem *) & AlHost, NULL));
        }
    }
    else
    {
        AlHost.u4HlHostControlIndex = u4HlHostControlIndex;
        AlHost.u4NlProtocolDirLocalIndex = u4NlProtocolDirLocalIndex;
        AlHost.u4AlProtocolDirLocalIndex = u4AlProtocolDirLocalIndex;
        MEMCPY (&(AlHost.NlHostAddress), pNlHostAddress, sizeof (tIpAddr));
        pAlHost = ((tAlHost *) RBTreeGetNext (RMON2_ALHOST_TREE,
                                              (tRBElem *) & AlHost, NULL));
    }
    return pAlHost;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AlHostAddDataEntry                                    */
/*                                                                           */
/* Description  : Allocates memory and Adds Al Host Entry into               */
/*                AlHostRBTree                                               */
/*                                                                           */
/* Input        : pAlHost                                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tAlHost / NULL Pointer                                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC tAlHost     *
Rmon2AlHostAddDataEntry (UINT4 u4HlHostControlIndex,
                         UINT4 u4NlProtocolDirLocalIndex,
                         tIpAddr * pNlHostAddress,
                         UINT4 u4AlProtocolDirLocalIndex)
{
    tAlHost            *pAlHost = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AlHostAddDataEntry\r\n");

    if ((pAlHost = (tAlHost *) (MemAllocMemBlk (RMON2_ALHOST_POOL))) == NULL)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for AlHostEntry\r\n");
        return NULL;
    }
    MEMSET (pAlHost, 0, sizeof (tAlHost));

    pAlHost->u4HlHostControlIndex = u4HlHostControlIndex;
    pAlHost->u4NlProtocolDirLocalIndex = u4NlProtocolDirLocalIndex;
    MEMCPY (&(pAlHost->NlHostAddress), pNlHostAddress, sizeof (tIpAddr));
    pAlHost->u4AlProtocolDirLocalIndex = u4AlProtocolDirLocalIndex;
    pAlHost->u4AlHostTimeMark = OsixGetSysUpTime ();
    pAlHost->u4AlHostCreateTime = OsixGetSysUpTime ();

    if (RBTreeAdd (RMON2_ALHOST_TREE, (tRBElem *) pAlHost) == RB_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Rmon2AlHostAddDataEntry: AlHostEntry is added\r\n");
        return pAlHost;
    }
    RMON2_TRC (RMON2_CRITICAL_TRC,
               "Rmon2AlHostAddDataEntry: Adding AlHostEntry Failed\r\n");
    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (RMON2_ALHOST_POOL, (UINT1 *) pAlHost);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AlHostAddDataEntry\r\n");

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AlHostDelDataEntry                                    */
/*                                                                           */
/* Description  : Releases memory and Removes Al Host Entry from             */
/*                AlHostRBTree                                               */
/*                                                                           */
/* Input        : pAlHost                                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rmon2AlHostDelDataEntry (tAlHost * pAlHost)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AlHostDelDataEntry\r\n");

    /* Remove Al Host node from AlHostRBTree */
    RBTreeRem (RMON2_ALHOST_TREE, (tRBElem *) pAlHost);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (RMON2_ALHOST_POOL,
                            (UINT1 *) pAlHost) != MEM_SUCCESS)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for  Stats.\r\n");
        return OSIX_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AlHostDelDataEntry\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AlHostGetDataEntry                                    */
/*                                                                           */
/* Description  : Get Al Host Entry for the given index                      */
/*                                                                           */
/* Input        : u4HlHostControlIndex, u4NlProtocolDirLocalIndex,           */
/*                pNlHostAddress, u4AlProtocolDirLocalIndex                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tAlHost / NULL Pointer                                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC tAlHost     *
Rmon2AlHostGetDataEntry (UINT4 u4HlHostControlIndex,
                         UINT4 u4NlProtocolDirLocalIndex,
                         tIpAddr * pNlHostAddress,
                         UINT4 u4AlProtocolDirLocalIndex)
{
    tAlHost             AlHost;
    MEMSET (&AlHost, 0, sizeof (tAlHost));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AlHostGetDataEntry\r\n");

    AlHost.u4HlHostControlIndex = u4HlHostControlIndex;
    AlHost.u4NlProtocolDirLocalIndex = u4NlProtocolDirLocalIndex;
    MEMCPY (&(AlHost.NlHostAddress), pNlHostAddress, sizeof (tIpAddr));
    AlHost.u4AlProtocolDirLocalIndex = u4AlProtocolDirLocalIndex;

    return ((tAlHost *) RBTreeGet (RMON2_ALHOST_TREE, (tRBElem *) & AlHost));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AlHostDelInterDep                                     */
/*                                                                           */
/* Description  : This routine updates the inter table references, before    */
/*                the data entry is removed and intimate the changes to NPAPI*/
/*                                                                           */
/* Input        : pAlHost                                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2AlHostDelInterDep (tAlHost * pAlHost)
{
    tRmon2PktInfo      *pPktInfo = NULL;
    tPktHeader          PktHdr;

    MEMSET (&PktHdr, 0, sizeof (tPktHeader));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AlHostDelInterDep\r\n");

    if (pAlHost->pPrevAlHost == NULL)
    {
        /* Update PktInfo nodes to point next pHost Entry */
        pPktInfo = Rmon2PktInfoGetNextIndex (&PktHdr);

        while (pPktInfo != NULL)
        {
            if (pPktInfo->pOutAlHost == pAlHost)
            {
                pPktInfo->pOutAlHost = pAlHost->pNextAlHost;
                if (pPktInfo->pOutAlHost != NULL)
                {
                    pPktInfo->pOutAlHost->pPrevAlHost = NULL;
                }
            }
            pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
        }
    }
    else if (pAlHost->pNextAlHost != NULL)
    {
        pAlHost->pPrevAlHost->pNextAlHost = pAlHost->pNextAlHost;
    }
    else
    {
        pAlHost->pPrevAlHost->pNextAlHost = NULL;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AlHostDelInterDep\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2HostPopulateDataEntries                               */
/*                                                                           */
/* Description  : Populates data entries if the same data source is already  */
/*                present and Adds the inter-dependencies b/w other tables   */
/*                                                                           */
/* Input        : pHlHostCtrl                                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2HostPopulateDataEntries (tHlHostControl * pHlHostCtrl)
{
    tRmon2PktInfo      *pPktInfo = NULL;
    tProtocolDir       *pNlProtDirEntry = NULL, *pAlProtDirEntry = NULL;
    tNlHost            *pNlHost = NULL, *pNewNlHost = NULL;
    tAlHost            *pAlHost = NULL, *pNewAlHost = NULL;
    tPktHeader          PktHdr;

    MEMSET (&PktHdr, 0, sizeof (tPktHeader));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2HostPopulateDataEntries\r\n");

    /* Check for the presence of data source in PktInfo table */
    pPktInfo = Rmon2PktInfoGetNextIndex (&PktHdr);
    while (pPktInfo != NULL)
    {
        if (pPktInfo->PktHdr.u4IfIndex ==
            pHlHostCtrl->u4HlHostControlDataSource)
        {

            if (pHlHostCtrl->u4HlHostControlNlMaxDesiredSupported == 0)
            {
                pHlHostCtrl->u4HlHostControlNlDroppedFrames++;
                pHlHostCtrl->u4HlHostControlAlDroppedFrames++;
                pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
                continue;
            }

            /* Check whether NlHost data entries reached max limit */
            if ((pHlHostCtrl->u4HlHostControlNlInserts -
                 pHlHostCtrl->u4HlHostControlNlDeletes) >=
                pHlHostCtrl->u4HlHostControlNlMaxDesiredSupported)
            {
                pNewNlHost = Rmon2NlHostGiveLRUEntry
                    (pHlHostCtrl->u4HlHostControlIndex);

                if (pNewNlHost != NULL)
                {
                    RMON2_TRC (RMON2_DEBUG_TRC,
                               "Deleting NlHost LRU Entry\r\n");

                    /* Remove Inter table references */
                    Rmon2NlHostDelInterDep (pNewNlHost);

                    pNewNlHost->pHlHostCtrl->u4HlHostControlNlDeletes++;

                    /* After Reference changes, free Nl Host entry */
                    Rmon2NlHostDelDataEntry (pNewNlHost);

                    pNewNlHost = NULL;
                }
            }

            pNlProtDirEntry =
                Rmon2PDirGetProtocolNode (pPktInfo->u4ProtoNLIndex);
            if (pNlProtDirEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "Get Protocol Entry failed for NLProtocolLocIndex\r\n");
                pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
                continue;
            }

            if ((pPktInfo->u4ProtoNLIndex != 0) &&
                (pNlProtDirEntry->u4ProtocolDirHostConfig == SUPPORTED_ON))
            {
                /* Add NlHost Entry */
                pNewNlHost =
                    Rmon2NlHostAddDataEntry (pHlHostCtrl->u4HlHostControlIndex,
                                             pPktInfo->u4ProtoNLIndex,
                                             &(pPktInfo->PktHdr.SrcIp));

                if (pNewNlHost == NULL)
                {
                    pHlHostCtrl->u4HlHostControlNlDroppedFrames++;
                    pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
                    continue;
                }
                else
                {
                    pNewNlHost->pHlHostCtrl = pHlHostCtrl;
                    pHlHostCtrl->u4HlHostControlNlInserts++;
                }

                if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV4)
                {
                    pNewNlHost->u4Rmon2NlHostIpAddrLen = RMON2_IPV4_MAX_LEN;
                }
                else if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV6)
                {
                    pNewNlHost->u4Rmon2NlHostIpAddrLen = RMON2_IPV6_MAX_LEN;
                }

                pNlHost = pPktInfo->pOutNlHost;
                if (pNlHost == NULL)
                {
                    pPktInfo->pOutNlHost = pNewNlHost;
                }
                else
                {
                    while (pNlHost->pNextNlHost != NULL)
                    {
                        pNlHost = pNlHost->pNextNlHost;
                    }
                    pNlHost->pNextNlHost = pNewNlHost;
                    pNewNlHost->pPrevNlHost = pNlHost;
                }
            }

            pAlProtDirEntry =
                Rmon2PDirGetProtocolNode (pPktInfo->u4ProtoL5ALIndex);
            if (pAlProtDirEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "Get Protocol Entry failed for L5ProtocolLocIndex\r\n");
                pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
                continue;
            }

            if ((pPktInfo->u4ProtoNLIndex != 0) &&
                (pPktInfo->u4ProtoL5ALIndex != 0) &&
                (pNlProtDirEntry->u4ProtocolDirHostConfig == SUPPORTED_ON) &&
                (pAlProtDirEntry->u4ProtocolDirHostConfig == SUPPORTED_ON))
            {
                pNewAlHost = NULL;

                /* Add AlHost Entry for L5 Protocol only if NlHost Entry is added */
                if (pNewNlHost != NULL)
                {

                    /* Check whether AlHost data entries reached max limit */
                    if ((pHlHostCtrl->u4HlHostControlAlInserts -
                         pHlHostCtrl->u4HlHostControlAlDeletes) >=
                        pHlHostCtrl->u4HlHostControlAlMaxDesiredSupported)
                    {
                        pNewAlHost = Rmon2AlHostGiveLRUEntry
                            (pHlHostCtrl->u4HlHostControlIndex);

                        if (pNewAlHost != NULL)
                        {
                            RMON2_TRC (RMON2_DEBUG_TRC,
                                       "Deleting AlHost LRU Entry\r\n");

                            /* Remove Inter table references */
                            Rmon2AlHostDelInterDep (pNewAlHost);

                            pNewAlHost->pHlHostCtrl->u4HlHostControlAlDeletes++;

                            /* After Reference changes, free Al Host entry */
                            Rmon2AlHostDelDataEntry (pNewAlHost);

                            pNewAlHost = NULL;
                        }
                    }
                    pNewAlHost =
                        Rmon2AlHostAddDataEntry (pHlHostCtrl->
                                                 u4HlHostControlIndex,
                                                 pPktInfo->u4ProtoNLIndex,
                                                 &(pPktInfo->PktHdr.SrcIp),
                                                 pPktInfo->u4ProtoL5ALIndex);

                    if (pNewAlHost == NULL)
                    {
                        pHlHostCtrl->u4HlHostControlAlDroppedFrames++;
                        pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
                        continue;
                    }
                    else
                    {
                        pNewAlHost->pHlHostCtrl = pHlHostCtrl;
                        pHlHostCtrl->u4HlHostControlAlInserts++;
                    }

                }

                if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV4)
                {
                    pNewAlHost->u4Rmon2NlHostIpAddrLen = RMON2_IPV4_MAX_LEN;
                }
                else if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV6)
                {
                    pNewAlHost->u4Rmon2NlHostIpAddrLen = RMON2_IPV6_MAX_LEN;
                }

                pAlHost = pPktInfo->pOutAlHost;
                if (pAlHost == NULL)
                {
                    pPktInfo->pOutAlHost = pNewAlHost;
                }
                else
                {
                    while (pAlHost->pNextAlHost != NULL)
                    {
                        pAlHost = pAlHost->pNextAlHost;
                    }
                    pAlHost->pNextAlHost = pNewAlHost;
                    pNewAlHost->pPrevAlHost = pAlHost;
                }
            }

            pAlProtDirEntry =
                Rmon2PDirGetProtocolNode (pPktInfo->u4ProtoL4ALIndex);
            if (pAlProtDirEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "Get Protocol Entry failed for L4ProtocolLocIndex\r\n");
                pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
                continue;
            }

            if ((pPktInfo->u4ProtoNLIndex != 0) &&
                (pPktInfo->u4ProtoL4ALIndex != 0) &&
                (pNlProtDirEntry->u4ProtocolDirHostConfig == SUPPORTED_ON) &&
                (pAlProtDirEntry->u4ProtocolDirHostConfig == SUPPORTED_ON))
            {
                /* Add AlHost Entry for L4 Protocol only if NlHost Entry is added */
                if (pNewNlHost != NULL)
                {
                    /* Check whether AlHost data entries reached max limit */
                    if ((pHlHostCtrl->u4HlHostControlAlInserts -
                         pHlHostCtrl->u4HlHostControlAlDeletes) >=
                        pHlHostCtrl->u4HlHostControlAlMaxDesiredSupported)
                    {
                        pNewAlHost = Rmon2AlHostGiveLRUEntry
                            (pHlHostCtrl->u4HlHostControlIndex);

                        if (pNewAlHost != NULL)
                        {
                            RMON2_TRC (RMON2_DEBUG_TRC,
                                       "Deleting AlHost LRU Entry\r\n");

                            /* Remove Inter table references */
                            Rmon2AlHostDelInterDep (pNewAlHost);

                            pNewAlHost->pHlHostCtrl->u4HlHostControlAlDeletes++;

                            /* After Reference changes, free Al Host entry */
                            Rmon2AlHostDelDataEntry (pNewAlHost);

                            pNewAlHost = NULL;
                        }
                    }

                    pNewAlHost =
                        Rmon2AlHostAddDataEntry (pHlHostCtrl->
                                                 u4HlHostControlIndex,
                                                 pPktInfo->u4ProtoNLIndex,
                                                 &(pPktInfo->PktHdr.SrcIp),
                                                 pPktInfo->u4ProtoL4ALIndex);

                    if (pNewAlHost == NULL)
                    {
                        pHlHostCtrl->u4HlHostControlAlDroppedFrames++;
                        pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
                        continue;
                    }
                    else
                    {
                        pNewAlHost->pHlHostCtrl = pHlHostCtrl;
                        pHlHostCtrl->u4HlHostControlAlInserts++;
                    }
                }

                if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV4)
                {
                    pNewAlHost->u4Rmon2NlHostIpAddrLen = RMON2_IPV4_MAX_LEN;
                }
                else if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV6)
                {
                    pNewAlHost->u4Rmon2NlHostIpAddrLen = RMON2_IPV6_MAX_LEN;
                }

                pAlHost = pPktInfo->pOutAlHost;
                if (pAlHost == NULL)
                {
                    pPktInfo->pOutAlHost = pNewAlHost;
                }
                else
                {
                    while (pAlHost->pNextAlHost != NULL)
                    {
                        pAlHost = pAlHost->pNextAlHost;
                    }
                    pAlHost->pNextAlHost = pNewAlHost;
                    pNewAlHost->pPrevAlHost = pAlHost;
                }
            }

        }                        /* End of if check */
        pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);

    }                            /* End of While - PktInfo */
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2HostPopulateDataEntries\r\n");
    return;
}
