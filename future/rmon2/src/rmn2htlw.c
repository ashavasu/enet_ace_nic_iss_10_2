/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * $Id: rmn2htlw.c,v 1.10 2014/02/05 12:28:59 siva Exp $
 *
 * Description: This file contains the low level nmh routines for
 *              RMON2 Network Layer and Application Layer 
 *              Host module.            
 *
 *******************************************************************/
#include "rmn2inc.h"

/* LOW LEVEL Routines for Table : HlHostControlTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceHlHostControlTable
Input       :  The Indices
               HlHostControlIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceHlHostControlTable (INT4 i4HlHostControlIndex)
{
    tHlHostControl     *pHlHostCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  "
               "nmhValidateIndexInstanceHlHostControlTable \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HlHostControlIndex < RMON2_ONE) ||
        (i4HlHostControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid HlHostControlIndex \n");
        return SNMP_FAILURE;
    }
    pHlHostCtrlEntry = Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);

    if (pHlHostCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlHost Control table"
                    " for index %d is failed \r\n", i4HlHostControlIndex);
        return SNMP_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "nmhValidateIndexInstanceHlHostControlTable \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexHlHostControlTable
Input       :  The Indices
               HlHostControlIndex
Output      :  The Get First Routines gets the Lexicographicaly
               First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexHlHostControlTable (INT4 *pi4HlHostControlIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetFirstIndexHlHostControlTable \n");

    return nmhGetNextIndexHlHostControlTable (RMON2_ZERO,
                                              pi4HlHostControlIndex);

}

/****************************************************************************
Function    :  nmhGetNextIndexHlHostControlTable
Input       :  The Indices
               HlHostControlIndex
               nextHlHostControlIndex
Output      :  The Get Next function gets the Next Index for
               the Index Value given in the Index Values. The
               Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexHlHostControlTable (INT4 i4HlHostControlIndex,
                                   INT4 *pi4NextHlHostControlIndex)
{
    tHlHostControl     *pHlHostCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetNextIndexHlHostControlTable \n");

    pHlHostCtrlEntry =
        Rmon2HlHostGetNextCtrlIndex ((UINT4) i4HlHostControlIndex);

    if (pHlHostCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlHost Control table"
                    " for index %d is failed \r\n", i4HlHostControlIndex);
        return SNMP_FAILURE;
    }
    *pi4NextHlHostControlIndex = (INT4) pHlHostCtrlEntry->u4HlHostControlIndex;

    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetNextIndexHlHostControlTable \n");
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetHlHostControlDataSource
Input       :  The Indices
               HlHostControlIndex

               The Object 
               retValHlHostControlDataSource
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHlHostControlDataSource (INT4 i4HlHostControlIndex,
                               tSNMP_OID_TYPE * pRetValHlHostControlDataSource)
{
    tHlHostControl     *pHlHostCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetHlHostControlDataSource \n");

    pHlHostCtrlEntry = Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);

    if (pHlHostCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlHost Control table"
                    " for index %d is failed \r\n", i4HlHostControlIndex);
        return SNMP_FAILURE;
    }

    RMON2_GET_DATA_SOURCE_OID (pRetValHlHostControlDataSource,
                               pHlHostCtrlEntry->u4HlHostControlDataSource);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetHlHostControlDataSource \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetHlHostControlNlDroppedFrames
Input       :  The Indices
               HlHostControlIndex

               The Object 
               retValHlHostControlNlDroppedFrames
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHlHostControlNlDroppedFrames (INT4 i4HlHostControlIndex,
                                    UINT4
                                    *pu4RetValHlHostControlNlDroppedFrames)
{
    tHlHostControl     *pHlHostCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetHlHostControlNlDroppedFrames \n");

    pHlHostCtrlEntry = Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);

    if (pHlHostCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlHost Control table"
                    " for index %d is failed \r\n", i4HlHostControlIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValHlHostControlNlDroppedFrames =
        pHlHostCtrlEntry->u4HlHostControlNlDroppedFrames;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetHlHostControlNlDroppedFrames \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetHlHostControlNlInserts
Input       :  The Indices
               HlHostControlIndex

               The Object 
               retValHlHostControlNlInserts
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHlHostControlNlInserts (INT4 i4HlHostControlIndex,
                              UINT4 *pu4RetValHlHostControlNlInserts)
{
    tHlHostControl     *pHlHostCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetHlHostControlNlInserts \n");

    pHlHostCtrlEntry = Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);

    if (pHlHostCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlHost Control table"
                    " for index %d is failed \r\n", i4HlHostControlIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValHlHostControlNlInserts =
        pHlHostCtrlEntry->u4HlHostControlNlInserts;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetHlHostControlNlInserts \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetHlHostControlNlDeletes
Input       :  The Indices
               HlHostControlIndex

               The Object 
               retValHlHostControlNlDeletes
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHlHostControlNlDeletes (INT4 i4HlHostControlIndex,
                              UINT4 *pu4RetValHlHostControlNlDeletes)
{
    tHlHostControl     *pHlHostCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetHlHostControlNlDeletes \n");

    pHlHostCtrlEntry = Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);

    if (pHlHostCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlHost Control table"
                    " for index %d is failed \r\n", i4HlHostControlIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValHlHostControlNlDeletes =
        pHlHostCtrlEntry->u4HlHostControlNlDeletes;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetHlHostControlNlDeletes \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetHlHostControlNlMaxDesiredEntries
Input       :  The Indices
               HlHostControlIndex

               The Object 
               retValHlHostControlNlMaxDesiredEntries
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHlHostControlNlMaxDesiredEntries (INT4 i4HlHostControlIndex,
                                        INT4
                                        *pi4RetValHlHostControlNlMaxDesiredEntries)
{
    tHlHostControl     *pHlHostCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetHlHostControlNlMaxDesiredEntries \n");

    pHlHostCtrlEntry = Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);

    if (pHlHostCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlHost Control table"
                    " for index %d is failed \r\n", i4HlHostControlIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValHlHostControlNlMaxDesiredEntries =
        pHlHostCtrlEntry->i4HlHostControlNlMaxDesiredEntries;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetHlHostControlNlMaxDesiredEntries \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetHlHostControlAlDroppedFrames
Input       :  The Indices
               HlHostControlIndex

               The Object 
               retValHlHostControlAlDroppedFrames
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHlHostControlAlDroppedFrames (INT4 i4HlHostControlIndex,
                                    UINT4
                                    *pu4RetValHlHostControlAlDroppedFrames)
{
    tHlHostControl     *pHlHostCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetHlHostControlAlDroppedFrames \n");

    pHlHostCtrlEntry = Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);

    if (pHlHostCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlHost Control table"
                    " for index %d is failed \r\n", i4HlHostControlIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValHlHostControlAlDroppedFrames =
        pHlHostCtrlEntry->u4HlHostControlAlDroppedFrames;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetHlHostControlAlDroppedFrames \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetHlHostControlAlInserts
Input       :  The Indices
               HlHostControlIndex

               The Object 
               retValHlHostControlAlInserts
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHlHostControlAlInserts (INT4 i4HlHostControlIndex,
                              UINT4 *pu4RetValHlHostControlAlInserts)
{
    tHlHostControl     *pHlHostCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetHlHostControlAlInserts \n");

    pHlHostCtrlEntry = Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);

    if (pHlHostCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlHost Control table"
                    " for index %d is failed \r\n", i4HlHostControlIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValHlHostControlAlInserts =
        pHlHostCtrlEntry->u4HlHostControlAlInserts;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetHlHostControlAlInserts \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetHlHostControlAlDeletes
Input       :  The Indices
               HlHostControlIndex

               The Object 
               retValHlHostControlAlDeletes
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHlHostControlAlDeletes (INT4 i4HlHostControlIndex,
                              UINT4 *pu4RetValHlHostControlAlDeletes)
{
    tHlHostControl     *pHlHostCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetHlHostControlAlDeletes \n");

    pHlHostCtrlEntry = Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);

    if (pHlHostCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlHost Control table"
                    " for index %d is failed \r\n", i4HlHostControlIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValHlHostControlAlDeletes =
        pHlHostCtrlEntry->u4HlHostControlAlDeletes;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetHlHostControlAlDeletes \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetHlHostControlAlMaxDesiredEntries
Input       :  The Indices
               HlHostControlIndex

               The Object 
               retValHlHostControlAlMaxDesiredEntries
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHlHostControlAlMaxDesiredEntries (INT4 i4HlHostControlIndex,
                                        INT4
                                        *pi4RetValHlHostControlAlMaxDesiredEntries)
{
    tHlHostControl     *pHlHostCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetHlHostControlAlMaxDesiredEntries \n");

    pHlHostCtrlEntry = Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);

    if (pHlHostCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlHost Control table"
                    " for index %d is failed \r\n", i4HlHostControlIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValHlHostControlAlMaxDesiredEntries =
        pHlHostCtrlEntry->i4HlHostControlAlMaxDesiredEntries;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetHlHostControlAlMaxDesiredEntries \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetHlHostControlOwner
Input       :  The Indices
               HlHostControlIndex

               The Object 
               retValHlHostControlOwner
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHlHostControlOwner (INT4 i4HlHostControlIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValHlHostControlOwner)
{
    tHlHostControl     *pHlHostCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetHlHostControlOwner \n");

    pHlHostCtrlEntry = Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);

    if (pHlHostCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlHost Control table"
                    " for index %d is failed \r\n", i4HlHostControlIndex);
        return SNMP_FAILURE;
    }

    pRetValHlHostControlOwner->i4_Length =
        (INT4) STRLEN (pHlHostCtrlEntry->au1HlHostControlOwner);
    MEMCPY (pRetValHlHostControlOwner->pu1_OctetList,
            pHlHostCtrlEntry->au1HlHostControlOwner,
            pRetValHlHostControlOwner->i4_Length);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetHlHostControlOwner \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetHlHostControlStatus
Input       :  The Indices
               HlHostControlIndex

               The Object 
               retValHlHostControlStatus
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHlHostControlStatus (INT4 i4HlHostControlIndex,
                           INT4 *pi4RetValHlHostControlStatus)
{
    tHlHostControl     *pHlHostCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetHlHostControlStatus \n");

    pHlHostCtrlEntry = Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);

    if (pHlHostCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlHost Control table"
                    " for index %d is failed \r\n", i4HlHostControlIndex);
        return SNMP_FAILURE;
    }

    *pi4RetValHlHostControlStatus =
        (INT4) pHlHostCtrlEntry->u4HlHostControlStatus;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetHlHostControlStatus \n");
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetHlHostControlDataSource
Input       :  The Indices
               HlHostControlIndex

               The Object 
               setValHlHostControlDataSource
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetHlHostControlDataSource (INT4 i4HlHostControlIndex,
                               tSNMP_OID_TYPE * pSetValHlHostControlDataSource)
{
    tHlHostControl     *pHlHostCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhSetHlHostControlDataSource \n");

    pHlHostCtrlEntry = Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);

    if (pHlHostCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlHost Control table"
                    " for index %d is failed \r\n", i4HlHostControlIndex);
        return SNMP_FAILURE;
    }
    RMON2_SET_DATA_SOURCE_OID (pSetValHlHostControlDataSource,
                               &(pHlHostCtrlEntry->u4HlHostControlDataSource));
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhSetHlHostControlDataSource \n");

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetHlHostControlNlMaxDesiredEntries
Input       :  The Indices
               HlHostControlIndex

               The Object 
               setValHlHostControlNlMaxDesiredEntries
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetHlHostControlNlMaxDesiredEntries (INT4 i4HlHostControlIndex,
                                        INT4
                                        i4SetValHlHostControlNlMaxDesiredEntries)
{
    tHlHostControl     *pHlHostCtrlEntry = NULL;
    UINT4               u4Minimum = 0;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhSetHlHostControlNlMaxDesiredEntries \n");

    pHlHostCtrlEntry = Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);

    if (pHlHostCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlHost Control table"
                    " for index %d is failed \r\n", i4HlHostControlIndex);
        return SNMP_FAILURE;
    }

    u4Minimum = (UINT4) ((i4SetValHlHostControlNlMaxDesiredEntries != -1) ?
                         ((i4SetValHlHostControlNlMaxDesiredEntries <
                           RMON2_MAX_DATA_PER_CTL) ?
                          i4SetValHlHostControlNlMaxDesiredEntries :
                          RMON2_MAX_DATA_PER_CTL) : RMON2_MAX_DATA_PER_CTL);

    pHlHostCtrlEntry->i4HlHostControlNlMaxDesiredEntries =
        i4SetValHlHostControlNlMaxDesiredEntries;
    pHlHostCtrlEntry->u4HlHostControlNlMaxDesiredSupported = u4Minimum;

    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhSetHlHostControlNlMaxDesiredEntries \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetHlHostControlAlMaxDesiredEntries
Input       :  The Indices
               HlHostControlIndex

               The Object 
               setValHlHostControlAlMaxDesiredEntries
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetHlHostControlAlMaxDesiredEntries (INT4 i4HlHostControlIndex,
                                        INT4
                                        i4SetValHlHostControlAlMaxDesiredEntries)
{
    tHlHostControl     *pHlHostCtrlEntry = NULL;
    UINT4               u4Minimum = 0;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhSetHlHostControlAlMaxDesiredEntries \n");

    pHlHostCtrlEntry = Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);

    if (pHlHostCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlHost Control table"
                    " for index %d is failed \r\n", i4HlHostControlIndex);
        return SNMP_FAILURE;
    }

    u4Minimum = (UINT4) ((i4SetValHlHostControlAlMaxDesiredEntries != -1) ?
                         ((i4SetValHlHostControlAlMaxDesiredEntries <
                           RMON2_MAX_DATA_PER_CTL) ?
                          i4SetValHlHostControlAlMaxDesiredEntries :
                          RMON2_MAX_DATA_PER_CTL) : RMON2_MAX_DATA_PER_CTL);

    pHlHostCtrlEntry->i4HlHostControlAlMaxDesiredEntries =
        i4SetValHlHostControlAlMaxDesiredEntries;
    pHlHostCtrlEntry->u4HlHostControlAlMaxDesiredSupported = u4Minimum;

    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhSetHlHostControlAlMaxDesiredEntries \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetHlHostControlOwner
Input       :  The Indices
               HlHostControlIndex

               The Object 
               setValHlHostControlOwner
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetHlHostControlOwner (INT4 i4HlHostControlIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValHlHostControlOwner)
{
    UINT4               u4Minimum;
    tHlHostControl     *pHlHostCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhSetHlHostControlOwner \n");

    u4Minimum =
        (UINT4) ((pSetValHlHostControlOwner->i4_Length <
                  RMON2_OWNER_LEN) ? pSetValHlHostControlOwner->
                 i4_Length : RMON2_OWNER_LEN);

    pHlHostCtrlEntry = Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);

    if (pHlHostCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlHost Control table"
                    " for index %d is failed \r\n", i4HlHostControlIndex);
        return SNMP_FAILURE;
    }
    MEMCPY (pHlHostCtrlEntry->au1HlHostControlOwner,
            pSetValHlHostControlOwner->pu1_OctetList, u4Minimum);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhSetHlHostControlOwner \n");

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetHlHostControlStatus
Input       :  The Indices
               HlHostControlIndex

               The Object 
               setValHlHostControlStatus
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetHlHostControlStatus (INT4 i4HlHostControlIndex,
                           INT4 i4SetValHlHostControlStatus)
{
    tPortList          *pVlanPortList = NULL;
    tHlHostControl     *pHlHostCtrlEntry = NULL;
    INT4                i4Result;
    tCfaIfInfo          IfInfo;
    UINT2               u2VlanId = 0;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhSetHlHostControlStatus \n");

    switch (i4SetValHlHostControlStatus)
    {
        case CREATE_AND_WAIT:

            pHlHostCtrlEntry =
                Rmon2HlHostAddCtrlEntry ((UINT4) i4HlHostControlIndex);
            if (pHlHostCtrlEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "Adding to HlHost control table " "is failed \n");
                return SNMP_FAILURE;
            }
            pHlHostCtrlEntry->u4HlHostControlStatus = NOT_READY;
            break;

        case ACTIVE:

            pHlHostCtrlEntry =
                Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);
            if (pHlHostCtrlEntry == NULL)
            {
                RMON2_TRC1 (RMON2_DEBUG_TRC,
                            "Get entry of HlHost Control table"
                            " for index %d is failed \r\n",
                            i4HlHostControlIndex);
                return SNMP_FAILURE;
            }

            if (pHlHostCtrlEntry->u4HlHostControlDataSource == RMON2_ZERO)
            {
                RMON2_TRC (RMON2_DEBUG_TRC, "Data Source is not configured \n");
                return SNMP_FAILURE;
            }
            if (CfaGetIfInfo
                (pHlHostCtrlEntry->u4HlHostControlDataSource,
                 &IfInfo) == CFA_FAILURE)
            {
                /* ifindex is invalid */
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "SNMP Failure: Invalid interface Index \n");
                return SNMP_FAILURE;
            }

            if (((IfInfo.u1IfOperStatus != CFA_IF_UP)
                 || (IfInfo.u1IfAdminStatus != CFA_IF_UP)) &&
                ((gi4MibResStatus != MIB_RESTORE_IN_PROGRESS)))
            {
                RMON2_TRC (RMON2_DEBUG_TRC, "SNMP Failure: "
                           "Interface OperStatus or Admin Status "
                           "is in Down state \n");
                return SNMP_FAILURE;
            }
            pVlanPortList =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pVlanPortList == NULL)
            {
                RMON2_TRC (RMON2_FN_ENTRY,
                           "nmhSetHlHostControlStatus: "
                           "Error in allocating memory for pVlanPortList\r\n");
                return SNMP_FAILURE;
            }
            MEMSET (pVlanPortList, 0, sizeof (tPortList));
            if (IfInfo.u1IfType == CFA_L3IPVLAN)
            {
                /* Get the VlanId from interface index */
                if (CfaGetVlanId
                    (pHlHostCtrlEntry->u4HlHostControlDataSource,
                     &u2VlanId) == CFA_FAILURE)
                {
                    RMON2_TRC1 (RMON2_DEBUG_TRC, "SNMP Failure: \
                 Unable to Fetch VlanId of IfIndex %d\n", pHlHostCtrlEntry->u4HlHostControlDataSource);
                    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                    return SNMP_FAILURE;
                }

                if (L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList) ==
                    L2IWF_FAILURE)
                {
                    RMON2_TRC1 (RMON2_DEBUG_TRC, "SNMP Failure: \
                 Get Member Port List for VlanId %d failed \n", u2VlanId);
                    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                    return SNMP_FAILURE;
                }
            }

            pHlHostCtrlEntry->u4HlHostControlStatus = ACTIVE;

#ifdef NPAPI_WANTED
            Rmonv2FsRMONv2EnableProbe (pHlHostCtrlEntry->
                                       u4HlHostControlDataSource, u2VlanId,
                                       *pVlanPortList);
#endif
            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
            Rmon2HostPopulateDataEntries (pHlHostCtrlEntry);
            Rmon2PktInfoDelUnusedEntries ();

            break;

        case NOT_IN_SERVICE:

            pHlHostCtrlEntry =
                Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);
            if (pHlHostCtrlEntry == NULL)
            {
                RMON2_TRC1 (RMON2_DEBUG_TRC,
                            "Get entry of HlHost Control table"
                            " for index %d is failed \r\n",
                            i4HlHostControlIndex);
                return SNMP_FAILURE;
            }

            pHlHostCtrlEntry->u4HlHostControlStatus = NOT_IN_SERVICE;

            Rmon2HostDelDataEntries ((UINT4) i4HlHostControlIndex);
            pVlanPortList =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pVlanPortList == NULL)
            {
                RMON2_TRC (RMON2_FN_ENTRY,
                           "nmhSetHlHostControlStatus: "
                           "Error in allocating memory for pVlanPortList\r\n");
                return SNMP_FAILURE;
            }
            MEMSET (pVlanPortList, 0, sizeof (tPortList));
            if (CfaGetIfInfo
                (pHlHostCtrlEntry->u4HlHostControlDataSource,
                 &IfInfo) != CFA_FAILURE)
            {
                if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
                    (CfaGetVlanId
                     (pHlHostCtrlEntry->u4HlHostControlDataSource,
                      &u2VlanId) != CFA_FAILURE))
                {
                    L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList);
                }
            }

#ifdef NPAPI_WANTED
            Rmonv2FsRMONv2DisableProbe (pHlHostCtrlEntry->
                                        u4HlHostControlDataSource, u2VlanId,
                                        *pVlanPortList);
#endif
            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
            Rmon2PktInfoDelUnusedEntries ();
            break;

        case DESTROY:

            pHlHostCtrlEntry =
                Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);
            if (pHlHostCtrlEntry == NULL)
            {
                RMON2_TRC1 (RMON2_DEBUG_TRC,
                            "Get entry of HlHost Control table"
                            " for index %d is failed \r\n",
                            i4HlHostControlIndex);
                return SNMP_FAILURE;
            }

            Rmon2HostDelDataEntries ((UINT4) i4HlHostControlIndex);

            i4Result = Rmon2HlHostDelCtrlEntry (pHlHostCtrlEntry);

            if (i4Result == OSIX_FAILURE)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "RBTreeRemove Failure for HlHost control entry \n");
                return SNMP_FAILURE;
            }

            Rmon2PktInfoDelUnusedEntries ();
            break;
        default:
            RMON2_TRC (RMON2_DEBUG_TRC, "Unknown option \n");
            return SNMP_FAILURE;
            break;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhSetHlHostControlStatus \n");

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2HlHostControlDataSource
Input       :  The Indices
               HlHostControlIndex

               The Object 
               testValHlHostControlDataSource
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2HlHostControlDataSource (UINT4 *pu4ErrorCode,
                                  INT4 i4HlHostControlIndex,
                                  tSNMP_OID_TYPE *
                                  pTestValHlHostControlDataSource)
{
    tHlHostControl     *pHlHostCtrlEntry = NULL;
    UINT4               u4DataSource = RMON2_ZERO;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2HlHostControlDataSource \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HlHostControlIndex < RMON2_ONE) ||
        (i4HlHostControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid HlHostControlIndex \n");
        return SNMP_FAILURE;
    }

    /* Checking whether the data source is valid or not */
    if (RMON2_TEST_DATA_SOURCE_OID (pTestValHlHostControlDataSource))
    {
        RMON2_SET_DATA_SOURCE_OID (pTestValHlHostControlDataSource,
                                   &u4DataSource);

        pHlHostCtrlEntry =
            Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);

        /*Checking whether the Control entry exists or not */
        if (pHlHostCtrlEntry != NULL)
        {
            /* Checking whether the Status is not ACTIVE */
            if (pHlHostCtrlEntry->u4HlHostControlStatus != ACTIVE)
            {
                RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                           "SNMP Success: Data Source id is valid. \n");
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }
            RMON2_TRC1 (RMON2_DEBUG_TRC,
                        "Row Status of HlHost Control table is Active for "
                        "index : %d \r\n", i4HlHostControlIndex);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "SNMP Failure: Entry is not exist \n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;

    }
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: INVALID DataSource Oid \n");
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT  nmhTestv2HlHostControlDataSource \n");
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2HlHostControlNlMaxDesiredEntries
Input       :  The Indices
               HlHostControlIndex

               The Object 
               testValHlHostControlNlMaxDesiredEntries
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2HlHostControlNlMaxDesiredEntries (UINT4 *pu4ErrorCode,
                                           INT4 i4HlHostControlIndex,
                                           INT4
                                           i4TestValHlHostControlNlMaxDesiredEntries)
{
    tHlHostControl     *pHlHostCtrlEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2HlHostControlNlMaxDesiredEntries \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HlHostControlIndex < RMON2_ONE) ||
        (i4HlHostControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid HlHostControlIndex \n");
        return SNMP_FAILURE;
    }

    /* Testing the input for negative value alone is sufficient, because
     * RMON2 allows this configuration till the maximum value of interger 32 */
    if (i4TestValHlHostControlNlMaxDesiredEntries < -1)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Error: Invalid "
                   "i4TestValHlHostControlNlMaxDesiredEntries \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pHlHostCtrlEntry = Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);

    if (pHlHostCtrlEntry != NULL)
    {
        if (pHlHostCtrlEntry->u4HlHostControlStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of HlHost Control table is Active for "
                    "index : %d \r\n", i4HlHostControlIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry is not exist \n");
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT  nmhTestv2HlHostControlNlMaxDesiredEntries \n");
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2HlHostControlAlMaxDesiredEntries
Input       :  The Indices
               HlHostControlIndex

               The Object 
               testValHlHostControlAlMaxDesiredEntries
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2HlHostControlAlMaxDesiredEntries (UINT4 *pu4ErrorCode,
                                           INT4 i4HlHostControlIndex,
                                           INT4
                                           i4TestValHlHostControlAlMaxDesiredEntries)
{
    tHlHostControl     *pHlHostCtrlEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2HlHostControlAlMaxDesiredEntries \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HlHostControlIndex < RMON2_ONE) ||
        (i4HlHostControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid HlHostControlIndex \n");
        return SNMP_FAILURE;
    }

    /* Testing the input for negative value alone is sufficient, because
     * RMON2 allows this configuration till the maximum value of interger 32 */
    if (i4TestValHlHostControlAlMaxDesiredEntries < -1)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Error: Invalid "
                   "i4TestValHlHostControlAlMaxDesiredEntries \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pHlHostCtrlEntry = Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);

    if (pHlHostCtrlEntry != NULL)
    {
        if (pHlHostCtrlEntry->u4HlHostControlStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of HlHost Control table is Active for "
                    "index : %d \r\n", i4HlHostControlIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry is not exist \n");
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT  nmhTestv2HlHostControlAlMaxDesiredEntries \n");
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2HlHostControlOwner
Input       :  The Indices
               HlHostControlIndex

               The Object 
               testValHlHostControlOwner
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2HlHostControlOwner (UINT4 *pu4ErrorCode, INT4 i4HlHostControlIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValHlHostControlOwner)
{
    tHlHostControl     *pHlHostCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhTestv2HlHostControlOwner \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HlHostControlIndex < RMON2_ONE) ||
        (i4HlHostControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid HlHostControlIndex \n");
        return SNMP_FAILURE;
    }

    if ((pTestValHlHostControlOwner->i4_Length < RMON2_ZERO) ||
        (pTestValHlHostControlOwner->i4_Length > RMON2_OWNER_LEN))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Wrong length for HlHostControlOwner object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValHlHostControlOwner->pu1_OctetList,
                              pTestValHlHostControlOwner->i4_Length)
        == OSIX_FAILURE)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Invalid Characters for Owner string object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    pHlHostCtrlEntry = Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);

    if (pHlHostCtrlEntry != NULL)
    {
        if (pHlHostCtrlEntry->u4HlHostControlStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of HlHost Control table is Active for "
                    "index : %d \r\n", i4HlHostControlIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry Status is not exist \n");
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhTestv2HlHostControlOwner \n");
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2HlHostControlStatus
Input       :  The Indices
               HlHostControlIndex

               The Object 
               testValHlHostControlStatus
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2HlHostControlStatus (UINT4 *pu4ErrorCode, INT4 i4HlHostControlIndex,
                              INT4 i4TestValHlHostControlStatus)
{
    tHlHostControl     *pHlHostCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhTestv2HlHostControlStatus \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HlHostControlIndex < RMON2_ONE) ||
        (i4HlHostControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid HlHostControlIndex \n");
        return SNMP_FAILURE;
    }

    if ((i4TestValHlHostControlStatus < ACTIVE) ||
        (i4TestValHlHostControlStatus > DESTROY))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Wrong value for HlHostControlStatus " "object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pHlHostCtrlEntry = Rmon2HlHostGetCtrlEntry ((UINT4) i4HlHostControlIndex);

    if (pHlHostCtrlEntry == NULL)
    {
        if (i4TestValHlHostControlStatus == CREATE_AND_WAIT)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "SNMP Failure: Entry Status is not exist \n");
        return SNMP_FAILURE;
    }
    else
    {
        if ((i4TestValHlHostControlStatus == ACTIVE) ||
            (i4TestValHlHostControlStatus == NOT_IN_SERVICE) ||
            (i4TestValHlHostControlStatus == DESTROY))
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "The given entry is already exist \n");
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhTestv2HlHostControlStatus \n");
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2HlHostControlTable
Input       :  The Indices
               HlHostControlIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
               check whether dependency is met or not.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2HlHostControlTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routiner for Table : NlHostTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceNlHostTable
Input       :  The Indices
               HlHostControlIndex
               NlHostTimeMark
               ProtocolDirLocalIndex
               NlHostAddress
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceNlHostTable (INT4 i4HlHostControlIndex,
                                     UINT4 u4NlHostTimeMark,
                                     INT4 i4ProtocolDirLocalIndex,
                                     tSNMP_OCTET_STRING_TYPE * pNlHostAddress)
{
    tNlHost            *pNlHostEntry = NULL;
    tIpAddr             NlHostAddress;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "nmhValidateIndexInstanceNlHostTable \n");
    UNUSED_PARAM (u4NlHostTimeMark);
    MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));

    if (pNlHostAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlHostAddress, pNlHostAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlHostAddress.u4_addr[3],
                pNlHostAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlHostAddress.u4_addr[0] = OSIX_NTOHL (NlHostAddress.u4_addr[0]);
    NlHostAddress.u4_addr[1] = OSIX_NTOHL (NlHostAddress.u4_addr[1]);
    NlHostAddress.u4_addr[2] = OSIX_NTOHL (NlHostAddress.u4_addr[2]);
    NlHostAddress.u4_addr[3] = OSIX_NTOHL (NlHostAddress.u4_addr[3]);

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HlHostControlIndex < RMON2_ONE) ||
        (i4HlHostControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid HlHostControlIndex \n");
        return SNMP_FAILURE;
    }

    /* Testing the input for negative value alone is sufficient, because
     * RMON2 allows this configuration till the maximum value of interger 32 */
    if (i4ProtocolDirLocalIndex < RMON2_ONE)
    {
        RMON2_TRC (RMON2_DEBUG_TRC, "SNMP Failure: Invalid "
                   "ProtocolDirLocalIndex \n");
        return SNMP_FAILURE;
    }

    pNlHostEntry =
        Rmon2NlHostGetDataEntry ((UINT4) i4HlHostControlIndex,
                                 (UINT4) i4ProtocolDirLocalIndex,
                                 &NlHostAddress);

    if (pNlHostEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC, "Get entry of NlHost table is failed \r\n");
        return SNMP_FAILURE;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "nmhValidateIndexInstanceNlHostTable \n");

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFirstIndexNlHostTable
Input       :  The Indices
               HlHostControlIndex
               NlHostTimeMark
               ProtocolDirLocalIndex
               NlHostAddress
Output      :  The Get First Routines gets the Lexicographicaly
               First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexNlHostTable (INT4 *pi4HlHostControlIndex,
                             UINT4 *pu4NlHostTimeMark,
                             INT4 *pi4ProtocolDirLocalIndex,
                             tSNMP_OCTET_STRING_TYPE * pNlHostAddress)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetFirstIndexNlHostTable \n");

    return nmhGetNextIndexNlHostTable (RMON2_ZERO, pi4HlHostControlIndex,
                                       RMON2_ZERO, pu4NlHostTimeMark,
                                       RMON2_ZERO, pi4ProtocolDirLocalIndex,
                                       RMON2_ZERO, pNlHostAddress);
}

/****************************************************************************
Function    :  nmhGetNextIndexNlHostTable
Input       :  The Indices
               HlHostControlIndex
               nextHlHostControlIndex
               NlHostTimeMark
               nextNlHostTimeMark
               ProtocolDirLocalIndex
               nextProtocolDirLocalIndex
               NlHostAddress
               nextNlHostAddress
Output      :  The Get Next function gets the Next Index for
               the Index Value given in the Index Values. The
               Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexNlHostTable (INT4 i4HlHostControlIndex,
                            INT4 *pi4NextHlHostControlIndex,
                            UINT4 u4NlHostTimeMark,
                            UINT4 *pu4NextNlHostTimeMark,
                            INT4 i4ProtocolDirLocalIndex,
                            INT4 *pi4NextProtocolDirLocalIndex,
                            tSNMP_OCTET_STRING_TYPE * pNlHostAddress,
                            tSNMP_OCTET_STRING_TYPE * pNextNlHostAddress)
{
    tNlHost            *pNlHostEntry = NULL;
    tIpAddr             NlHostAddress, HostAddress;
    UNUSED_PARAM (pu4NextNlHostTimeMark);
    UNUSED_PARAM (u4NlHostTimeMark);

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetNextIndexNlHostTable \n");
    if (pNlHostAddress == NULL)
    {
        MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));
    }
    else
    {
        MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));
        if (pNlHostAddress->i4_Length == RMON2_IPV6_MAX_LEN)
        {
            MEMCPY (&NlHostAddress, pNlHostAddress->pu1_OctetList,
                    RMON2_IPV6_MAX_LEN);
        }
        else
        {
            MEMCPY (&NlHostAddress.u4_addr[3],
                    pNlHostAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);

        }
    }

    NlHostAddress.u4_addr[0] = OSIX_NTOHL (NlHostAddress.u4_addr[0]);
    NlHostAddress.u4_addr[1] = OSIX_NTOHL (NlHostAddress.u4_addr[1]);
    NlHostAddress.u4_addr[2] = OSIX_NTOHL (NlHostAddress.u4_addr[2]);
    NlHostAddress.u4_addr[3] = OSIX_NTOHL (NlHostAddress.u4_addr[3]);

    pNlHostEntry =
        Rmon2NlHostGetNextDataIndex ((UINT4) i4HlHostControlIndex,
                                     (UINT4) i4ProtocolDirLocalIndex,
                                     &NlHostAddress);
    if (pNlHostEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC, "Get first entry of NlHost table is "
                   "failed \r\n");
        return SNMP_FAILURE;
    }

    *pi4NextHlHostControlIndex = (INT4) pNlHostEntry->u4HlHostControlIndex;
    *pu4NextNlHostTimeMark = RMON2_ZERO;
    *pi4NextProtocolDirLocalIndex =
        (INT4) pNlHostEntry->u4ProtocolDirLocalIndex;

    pNextNlHostAddress->i4_Length = (INT4) pNlHostEntry->u4Rmon2NlHostIpAddrLen;

    HostAddress.u4_addr[0] =
        OSIX_HTONL (pNlHostEntry->NlHostAddress.u4_addr[0]);
    HostAddress.u4_addr[1] =
        OSIX_HTONL (pNlHostEntry->NlHostAddress.u4_addr[1]);
    HostAddress.u4_addr[2] =
        OSIX_HTONL (pNlHostEntry->NlHostAddress.u4_addr[2]);
    HostAddress.u4_addr[3] =
        OSIX_HTONL (pNlHostEntry->NlHostAddress.u4_addr[3]);

    if (pNlHostEntry->u4Rmon2NlHostIpAddrLen == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (pNextNlHostAddress->pu1_OctetList,
                (UINT1 *) &HostAddress, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (pNextNlHostAddress->pu1_OctetList,
                (UINT1 *) &HostAddress.u4_addr[3], RMON2_IPV4_MAX_LEN);
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNextIndexNlHostTable \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNlHostInPkts
Input       :  The Indices
               HlHostControlIndex
               NlHostTimeMark
               ProtocolDirLocalIndex
               NlHostAddress

               The Object 
               retValNlHostInPkts
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlHostInPkts (INT4 i4HlHostControlIndex, UINT4 u4NlHostTimeMark,
                    INT4 i4ProtocolDirLocalIndex,
                    tSNMP_OCTET_STRING_TYPE * pNlHostAddress,
                    UINT4 *pu4RetValNlHostInPkts)
{
    tNlHost            *pNlHostEntry = NULL;
    tIpAddr             NlHostAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetNlHostInPkts \n");
    MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));

    if (pNlHostAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlHostAddress, pNlHostAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlHostAddress.u4_addr[3],
                pNlHostAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlHostAddress.u4_addr[0] = OSIX_NTOHL (NlHostAddress.u4_addr[0]);
    NlHostAddress.u4_addr[1] = OSIX_NTOHL (NlHostAddress.u4_addr[1]);
    NlHostAddress.u4_addr[2] = OSIX_NTOHL (NlHostAddress.u4_addr[2]);
    NlHostAddress.u4_addr[3] = OSIX_NTOHL (NlHostAddress.u4_addr[3]);

    pNlHostEntry =
        Rmon2NlHostGetDataEntry ((UINT4) i4HlHostControlIndex,
                                 (UINT4) i4ProtocolDirLocalIndex,
                                 &NlHostAddress);
    if (pNlHostEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC, "Get entry of NlHost table is failed \r\n");
        return SNMP_FAILURE;
    }
    if (pNlHostEntry->u4NlHostTimeMark < u4NlHostTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValNlHostInPkts = pNlHostEntry->u4NlHostInPkts;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNlHostInPkts \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetNlHostOutPkts
Input       :  The Indices
               HlHostControlIndex
               NlHostTimeMark
               ProtocolDirLocalIndex
               NlHostAddress

               The Object 
               retValNlHostOutPkts
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlHostOutPkts (INT4 i4HlHostControlIndex, UINT4 u4NlHostTimeMark,
                     INT4 i4ProtocolDirLocalIndex,
                     tSNMP_OCTET_STRING_TYPE * pNlHostAddress,
                     UINT4 *pu4RetValNlHostOutPkts)
{
    tNlHost            *pNlHostEntry = NULL;
    tIpAddr             NlHostAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetNlHostOutPkts \n");

    MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));

    if (pNlHostAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlHostAddress, pNlHostAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlHostAddress.u4_addr[3],
                pNlHostAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlHostAddress.u4_addr[0] = OSIX_NTOHL (NlHostAddress.u4_addr[0]);
    NlHostAddress.u4_addr[1] = OSIX_NTOHL (NlHostAddress.u4_addr[1]);
    NlHostAddress.u4_addr[2] = OSIX_NTOHL (NlHostAddress.u4_addr[2]);
    NlHostAddress.u4_addr[3] = OSIX_NTOHL (NlHostAddress.u4_addr[3]);

    pNlHostEntry =
        Rmon2NlHostGetDataEntry ((UINT4) i4HlHostControlIndex,
                                 (UINT4) i4ProtocolDirLocalIndex,
                                 &NlHostAddress);
    if (pNlHostEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC, "Get entry of NlHost table is failed \r\n");
        return SNMP_FAILURE;
    }
    if (pNlHostEntry->u4NlHostTimeMark < u4NlHostTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValNlHostOutPkts = pNlHostEntry->u4NlHostOutPkts;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNlHostOutPkts \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetNlHostInOctets
Input       :  The Indices
               HlHostControlIndex
               NlHostTimeMark
               ProtocolDirLocalIndex
               NlHostAddress

               The Object 
               retValNlHostInOctets
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlHostInOctets (INT4 i4HlHostControlIndex, UINT4 u4NlHostTimeMark,
                      INT4 i4ProtocolDirLocalIndex,
                      tSNMP_OCTET_STRING_TYPE * pNlHostAddress,
                      UINT4 *pu4RetValNlHostInOctets)
{

    tNlHost            *pNlHostEntry = NULL;
    tIpAddr             NlHostAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetNlHostInOctets \n");
    MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));

    if (pNlHostAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlHostAddress, pNlHostAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlHostAddress.u4_addr[3],
                pNlHostAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlHostAddress.u4_addr[0] = OSIX_NTOHL (NlHostAddress.u4_addr[0]);
    NlHostAddress.u4_addr[1] = OSIX_NTOHL (NlHostAddress.u4_addr[1]);
    NlHostAddress.u4_addr[2] = OSIX_NTOHL (NlHostAddress.u4_addr[2]);
    NlHostAddress.u4_addr[3] = OSIX_NTOHL (NlHostAddress.u4_addr[3]);

    pNlHostEntry =
        Rmon2NlHostGetDataEntry ((UINT4) i4HlHostControlIndex,
                                 (UINT4) i4ProtocolDirLocalIndex,
                                 &NlHostAddress);
    if (pNlHostEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC, "Get entry of NlHost table is failed \r\n");
        return SNMP_FAILURE;
    }
    if (pNlHostEntry->u4NlHostTimeMark < u4NlHostTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValNlHostInOctets = pNlHostEntry->u4NlHostInOctets;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNlHostInOctets \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNlHostOutOctets
Input       :  The Indices
               HlHostControlIndex
               NlHostTimeMark
               ProtocolDirLocalIndex
               NlHostAddress

               The Object 
               retValNlHostOutOctets
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetNlHostOutOctets (INT4 i4HlHostControlIndex, UINT4 u4NlHostTimeMark,
                       INT4 i4ProtocolDirLocalIndex,
                       tSNMP_OCTET_STRING_TYPE * pNlHostAddress,
                       UINT4 *pu4RetValNlHostOutOctets)
{
    tNlHost            *pNlHostEntry = NULL;
    tIpAddr             NlHostAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetNlHostOutOctets \n");

    MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));

    if (pNlHostAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlHostAddress, pNlHostAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlHostAddress.u4_addr[3],
                pNlHostAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlHostAddress.u4_addr[0] = OSIX_NTOHL (NlHostAddress.u4_addr[0]);
    NlHostAddress.u4_addr[1] = OSIX_NTOHL (NlHostAddress.u4_addr[1]);
    NlHostAddress.u4_addr[2] = OSIX_NTOHL (NlHostAddress.u4_addr[2]);
    NlHostAddress.u4_addr[3] = OSIX_NTOHL (NlHostAddress.u4_addr[3]);

    pNlHostEntry =
        Rmon2NlHostGetDataEntry ((UINT4) i4HlHostControlIndex,
                                 (UINT4) i4ProtocolDirLocalIndex,
                                 &NlHostAddress);
    if (pNlHostEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC, "Get entry of NlHost table is failed \r\n");
        return SNMP_FAILURE;
    }
    if (pNlHostEntry->u4NlHostTimeMark < u4NlHostTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValNlHostOutOctets = pNlHostEntry->u4NlHostOutOctets;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNlHostOutOctets \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetNlHostOutMacNonUnicastPkts
Input       :  The Indices
               HlHostControlIndex
               NlHostTimeMark
               ProtocolDirLocalIndex
               NlHostAddress

               The Object 
               retValNlHostOutMacNonUnicastPkts
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetNlHostOutMacNonUnicastPkts (INT4 i4HlHostControlIndex,
                                  UINT4 u4NlHostTimeMark,
                                  INT4 i4ProtocolDirLocalIndex,
                                  tSNMP_OCTET_STRING_TYPE * pNlHostAddress,
                                  UINT4 *pu4RetValNlHostOutMacNonUnicastPkts)
{
    tNlHost            *pNlHostEntry = NULL;
    tIpAddr             NlHostAddress;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetNlHostOutMacNonUnicastPkts \n");
    MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));

    if (pNlHostAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlHostAddress, pNlHostAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlHostAddress.u4_addr[3],
                pNlHostAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlHostAddress.u4_addr[0] = OSIX_NTOHL (NlHostAddress.u4_addr[0]);
    NlHostAddress.u4_addr[1] = OSIX_NTOHL (NlHostAddress.u4_addr[1]);
    NlHostAddress.u4_addr[2] = OSIX_NTOHL (NlHostAddress.u4_addr[2]);
    NlHostAddress.u4_addr[3] = OSIX_NTOHL (NlHostAddress.u4_addr[3]);

    pNlHostEntry =
        Rmon2NlHostGetDataEntry ((UINT4) i4HlHostControlIndex,
                                 (UINT4) i4ProtocolDirLocalIndex,
                                 &NlHostAddress);
    if (pNlHostEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC, "Get entry of NlHost table is failed \r\n");
        return SNMP_FAILURE;
    }
    if (pNlHostEntry->u4NlHostTimeMark < u4NlHostTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValNlHostOutMacNonUnicastPkts =
        pNlHostEntry->u4NlHostOutMacNonUnicastPkts;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNlHostOutMacNonUnicastPkts \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetNlHostCreateTime
Input       :  The Indices
               HlHostControlIndex
               NlHostTimeMark
               ProtocolDirLocalIndex
               NlHostAddress

               The Object 
               retValNlHostCreateTime
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlHostCreateTime (INT4 i4HlHostControlIndex, UINT4 u4NlHostTimeMark,
                        INT4 i4ProtocolDirLocalIndex,
                        tSNMP_OCTET_STRING_TYPE * pNlHostAddress,
                        UINT4 *pu4RetValNlHostCreateTime)
{
    tNlHost            *pNlHostEntry = NULL;
    tIpAddr             NlHostAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetNlHostCreateTime \n");

    MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));

    if (pNlHostAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlHostAddress, pNlHostAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlHostAddress.u4_addr[3],
                pNlHostAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlHostAddress.u4_addr[0] = OSIX_NTOHL (NlHostAddress.u4_addr[0]);
    NlHostAddress.u4_addr[1] = OSIX_NTOHL (NlHostAddress.u4_addr[1]);
    NlHostAddress.u4_addr[2] = OSIX_NTOHL (NlHostAddress.u4_addr[2]);
    NlHostAddress.u4_addr[3] = OSIX_NTOHL (NlHostAddress.u4_addr[3]);

    pNlHostEntry =
        Rmon2NlHostGetDataEntry ((UINT4) i4HlHostControlIndex,
                                 (UINT4) i4ProtocolDirLocalIndex,
                                 &NlHostAddress);
    if (pNlHostEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC, "Get entry of NlHost table is failed \r\n");
        return SNMP_FAILURE;
    }
    if (pNlHostEntry->u4NlHostTimeMark < u4NlHostTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValNlHostCreateTime = pNlHostEntry->u4NlHostCreateTime;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNlHostCreateTime \n");
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : AlHostTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceAlHostTable
Input       :  The Indices
               HlHostControlIndex
               AlHostTimeMark
               NlProtocolDirLocalIndex
               NlHostAddress
               AlProtocolDirLocalIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceAlHostTable (INT4 i4HlHostControlIndex,
                                     UINT4 u4AlHostTimeMark,
                                     INT4 i4NlProtocolDirLocalIndex,
                                     tSNMP_OCTET_STRING_TYPE * pNlHostAddress,
                                     INT4 i4AlProtocolDirLocalIndex)
{
    tAlHost            *pAlHostEntry = NULL;
    tIpAddr             NlHostAddress;
    UNUSED_PARAM (u4AlHostTimeMark);

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "nmhValidateIndexInstanceAlHostTable \n");
    UNUSED_PARAM (u4AlHostTimeMark);

    MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));

    if (pNlHostAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlHostAddress, pNlHostAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlHostAddress.u4_addr[3],
                pNlHostAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlHostAddress.u4_addr[0] = OSIX_NTOHL (NlHostAddress.u4_addr[0]);
    NlHostAddress.u4_addr[1] = OSIX_NTOHL (NlHostAddress.u4_addr[1]);
    NlHostAddress.u4_addr[2] = OSIX_NTOHL (NlHostAddress.u4_addr[2]);
    NlHostAddress.u4_addr[3] = OSIX_NTOHL (NlHostAddress.u4_addr[3]);

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HlHostControlIndex < RMON2_ONE) ||
        (i4HlHostControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid HlHostControlIndex \n");
        return SNMP_FAILURE;
    }

    /* Testing the input for negative value alone is sufficient, because
     * RMON2 allows this configuration till the maximum value of interger 32 */
    if (i4NlProtocolDirLocalIndex < RMON2_ONE)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid NlProtocolDirLocalIndex \n");
        return SNMP_FAILURE;
    }

    /* Testing the input for negative value alone is sufficient, because
     * RMON2 allows this configuration till the maximum value of interger 32 */
    if (i4AlProtocolDirLocalIndex < RMON2_ONE)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid AlProtocolDirLocalIndex \n");
        return SNMP_FAILURE;
    }

    pAlHostEntry =
        Rmon2AlHostGetDataEntry ((UINT4) i4HlHostControlIndex,
                                 (UINT4) i4NlProtocolDirLocalIndex,
                                 &NlHostAddress,
                                 (UINT4) i4AlProtocolDirLocalIndex);
    if (pAlHostEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC, "Get Entry of AlHost table is failed \r\n");
        return SNMP_FAILURE;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "nmhValidateIndexInstanceAlHostTable \n");

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFirstIndexAlHostTable
Input       :  The Indices
               HlHostControlIndex
               AlHostTimeMark
               NlProtocolDirLocalIndex
               NlHostAddress
               AlProtocolDirLocalIndex
Output      :  The Get First Routines gets the Lexicographicaly
               First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexAlHostTable (INT4 *pi4HlHostControlIndex,
                             UINT4 *pu4AlHostTimeMark,
                             INT4 *pi4NlProtocolDirLocalIndex,
                             tSNMP_OCTET_STRING_TYPE * pNlHostAddress,
                             INT4 *pi4AlProtocolDirLocalIndex)
{

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetFirstIndexAlHostTable \n");
    return nmhGetNextIndexAlHostTable (RMON2_ZERO, pi4HlHostControlIndex,
                                       RMON2_ZERO, pu4AlHostTimeMark,
                                       RMON2_ZERO, pi4NlProtocolDirLocalIndex,
                                       RMON2_ZERO, pNlHostAddress, RMON2_ZERO,
                                       pi4AlProtocolDirLocalIndex);
}

/****************************************************************************
Function    :  nmhGetNextIndexAlHostTable
Input       :  The Indices
               HlHostControlIndex
               nextHlHostControlIndex
               AlHostTimeMark
               nextAlHostTimeMark
               NlProtocolDirLocalIndex
               nextProtocolDirLocalIndex
               NlHostAddress
               nextNlHostAddress
               AlProtocolDirLocalIndex
               nextProtocolDirLocalIndex
Output      :  The Get Next function gets the Next Index for
               the Index Value given in the Index Values. The
               Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexAlHostTable (INT4 i4HlHostControlIndex,
                            INT4 *pi4NextHlHostControlIndex,
                            UINT4 u4AlHostTimeMark,
                            UINT4 *pu4NextAlHostTimeMark,
                            INT4 i4NlProtocolDirLocalIndex,
                            INT4 *pi4NextNlProtocolDirLocalIndex,
                            tSNMP_OCTET_STRING_TYPE * pNlHostAddress,
                            tSNMP_OCTET_STRING_TYPE * pNextNlHostAddress,
                            INT4 i4AlProtocolDirLocalIndex,
                            INT4 *pi4NextAlProtocolDirLocalIndex)
{
    tAlHost            *pAlHostEntry = NULL;
    tIpAddr             NlHostAddress, HostAddress;
    UNUSED_PARAM (u4AlHostTimeMark);

    if (pNlHostAddress == NULL)
    {
        MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));
    }
    else
    {
        MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));
        if (pNlHostAddress->i4_Length == RMON2_IPV6_MAX_LEN)
        {
            MEMCPY (&NlHostAddress, pNlHostAddress->pu1_OctetList,
                    RMON2_IPV6_MAX_LEN);
        }
        else
        {
            MEMCPY (&NlHostAddress.u4_addr[3],
                    pNlHostAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
        }
    }

    NlHostAddress.u4_addr[0] = OSIX_NTOHL (NlHostAddress.u4_addr[0]);
    NlHostAddress.u4_addr[1] = OSIX_NTOHL (NlHostAddress.u4_addr[1]);
    NlHostAddress.u4_addr[2] = OSIX_NTOHL (NlHostAddress.u4_addr[2]);
    NlHostAddress.u4_addr[3] = OSIX_NTOHL (NlHostAddress.u4_addr[3]);

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetNextIndexAlHostTable \n");

    pAlHostEntry = Rmon2AlHostGetNextDataIndex ((UINT4) i4HlHostControlIndex,
                                                (UINT4)
                                                i4NlProtocolDirLocalIndex,
                                                &NlHostAddress,
                                                (UINT4)
                                                i4AlProtocolDirLocalIndex);
    if (pAlHostEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC, "Get entry of AlHost table is failed \r\n");
        return SNMP_FAILURE;
    }

    *pi4NextHlHostControlIndex = (INT4) pAlHostEntry->u4HlHostControlIndex;
    *pu4NextAlHostTimeMark = RMON2_ZERO;
    *pi4NextNlProtocolDirLocalIndex =
        (INT4) pAlHostEntry->u4NlProtocolDirLocalIndex;

    *pi4NextAlProtocolDirLocalIndex =
        (INT4) pAlHostEntry->u4AlProtocolDirLocalIndex;

    pNextNlHostAddress->i4_Length = (INT4) pAlHostEntry->u4Rmon2NlHostIpAddrLen;

    HostAddress.u4_addr[0] =
        OSIX_HTONL (pAlHostEntry->NlHostAddress.u4_addr[0]);
    HostAddress.u4_addr[1] =
        OSIX_HTONL (pAlHostEntry->NlHostAddress.u4_addr[1]);
    HostAddress.u4_addr[2] =
        OSIX_HTONL (pAlHostEntry->NlHostAddress.u4_addr[2]);
    HostAddress.u4_addr[3] =
        OSIX_HTONL (pAlHostEntry->NlHostAddress.u4_addr[3]);

    if (pAlHostEntry->u4Rmon2NlHostIpAddrLen == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (pNextNlHostAddress->pu1_OctetList,
                (UINT1 *) &HostAddress, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (pNextNlHostAddress->pu1_OctetList,
                (UINT1 *) &HostAddress.u4_addr[3], RMON2_IPV4_MAX_LEN);
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNextIndexAlHostTable \n");
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetAlHostInPkts
Input       :  The Indices
               HlHostControlIndex
               AlHostTimeMark
               NlProtocolDirLocalIndex
               NlHostAddress
               AlProtocolDirLocalIndex

               The Object 
               retValAlHostInPkts
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlHostInPkts (INT4 i4HlHostControlIndex, UINT4 u4AlHostTimeMark,
                    INT4 i4NlProtocolDirLocalIndex,
                    tSNMP_OCTET_STRING_TYPE * pNlHostAddress,
                    INT4 i4AlProtocolDirLocalIndex,
                    UINT4 *pu4RetValAlHostInPkts)
{
    tAlHost            *pAlHostEntry = NULL;
    tIpAddr             NlHostAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetAlHostInPkts \n");

    MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));

    if (pNlHostAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlHostAddress, pNlHostAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlHostAddress.u4_addr[3],
                pNlHostAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlHostAddress.u4_addr[0] = OSIX_NTOHL (NlHostAddress.u4_addr[0]);
    NlHostAddress.u4_addr[1] = OSIX_NTOHL (NlHostAddress.u4_addr[1]);
    NlHostAddress.u4_addr[2] = OSIX_NTOHL (NlHostAddress.u4_addr[2]);
    NlHostAddress.u4_addr[3] = OSIX_NTOHL (NlHostAddress.u4_addr[3]);

    pAlHostEntry =
        Rmon2AlHostGetDataEntry ((UINT4) i4HlHostControlIndex,
                                 (UINT4) i4NlProtocolDirLocalIndex,
                                 &NlHostAddress,
                                 (UINT4) i4AlProtocolDirLocalIndex);
    if (pAlHostEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC, "Get Entry of AlHost table is failed \r\n");
        return SNMP_FAILURE;
    }
    if (pAlHostEntry->u4AlHostTimeMark < u4AlHostTimeMark)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValAlHostInPkts = pAlHostEntry->u4AlHostInPkts;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetAlHostInPkts \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetAlHostOutPkts
Input       :  The Indices
               HlHostControlIndex
               AlHostTimeMark
               NlProtocolDirLocalIndex
               NlHostAddress
               AlProtocolDirLocalIndex

               The Object 
               retValAlHostOutPkts
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlHostOutPkts (INT4 i4HlHostControlIndex, UINT4 u4AlHostTimeMark,
                     INT4 i4NlProtocolDirLocalIndex,
                     tSNMP_OCTET_STRING_TYPE * pNlHostAddress,
                     INT4 i4AlProtocolDirLocalIndex,
                     UINT4 *pu4RetValAlHostOutPkts)
{
    tAlHost            *pAlHostEntry = NULL;
    tIpAddr             NlHostAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetAlHostOutPkts \n");

    MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));

    if (pNlHostAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlHostAddress, pNlHostAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlHostAddress.u4_addr[3],
                pNlHostAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlHostAddress.u4_addr[0] = OSIX_NTOHL (NlHostAddress.u4_addr[0]);
    NlHostAddress.u4_addr[1] = OSIX_NTOHL (NlHostAddress.u4_addr[1]);
    NlHostAddress.u4_addr[2] = OSIX_NTOHL (NlHostAddress.u4_addr[2]);
    NlHostAddress.u4_addr[3] = OSIX_NTOHL (NlHostAddress.u4_addr[3]);

    pAlHostEntry =
        Rmon2AlHostGetDataEntry ((UINT4) i4HlHostControlIndex,
                                 (UINT4) i4NlProtocolDirLocalIndex,
                                 &NlHostAddress,
                                 (UINT4) i4AlProtocolDirLocalIndex);

    if (pAlHostEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC, "Get entry of AlHost table is failed \r\n");
        return SNMP_FAILURE;
    }
    if (pAlHostEntry->u4AlHostTimeMark < u4AlHostTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValAlHostOutPkts = pAlHostEntry->u4AlHostOutPkts;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetAlHostOutPkts \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetAlHostInOctets
Input       :  The Indices
               HlHostControlIndex
               AlHostTimeMark
               NlProtocolDirLocalIndex
               NlHostAddress
               AlProtocolDirLocalIndex

               The Object 
               retValAlHostInOctets
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetAlHostInOctets (INT4 i4HlHostControlIndex, UINT4 u4AlHostTimeMark,
                      INT4 i4NlProtocolDirLocalIndex,
                      tSNMP_OCTET_STRING_TYPE * pNlHostAddress,
                      INT4 i4AlProtocolDirLocalIndex,
                      UINT4 *pu4RetValAlHostInOctets)
{
    tAlHost            *pAlHostEntry = NULL;
    tIpAddr             NlHostAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetAlHostInOctets \n");

    MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));

    if (pNlHostAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlHostAddress, pNlHostAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlHostAddress.u4_addr[3],
                pNlHostAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlHostAddress.u4_addr[0] = OSIX_NTOHL (NlHostAddress.u4_addr[0]);
    NlHostAddress.u4_addr[1] = OSIX_NTOHL (NlHostAddress.u4_addr[1]);
    NlHostAddress.u4_addr[2] = OSIX_NTOHL (NlHostAddress.u4_addr[2]);
    NlHostAddress.u4_addr[3] = OSIX_NTOHL (NlHostAddress.u4_addr[3]);

    pAlHostEntry =
        Rmon2AlHostGetDataEntry ((UINT4) i4HlHostControlIndex,
                                 (UINT4) i4NlProtocolDirLocalIndex,
                                 &NlHostAddress,
                                 (UINT4) i4AlProtocolDirLocalIndex);

    if (pAlHostEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC, "Get entry of AlHost table is failed \r\n");
        return SNMP_FAILURE;
    }
    if (pAlHostEntry->u4AlHostTimeMark < u4AlHostTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValAlHostInOctets = pAlHostEntry->u4AlHostInOctets;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetAlHostInOctets \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetAlHostOutOctets
Input       :  The Indices
               HlHostControlIndex
               AlHostTimeMark
               NlProtocolDirLocalIndex
               NlHostAddress
               AlProtocolDirLocalIndex

               The Object 
               retValAlHostOutOctets
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlHostOutOctets (INT4 i4HlHostControlIndex, UINT4 u4AlHostTimeMark,
                       INT4 i4NlProtocolDirLocalIndex,
                       tSNMP_OCTET_STRING_TYPE * pNlHostAddress,
                       INT4 i4AlProtocolDirLocalIndex,
                       UINT4 *pu4RetValAlHostOutOctets)
{
    tAlHost            *pAlHostEntry = NULL;
    tIpAddr             NlHostAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetAlHostOutOctets \n");

    MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));

    if (pNlHostAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlHostAddress, pNlHostAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlHostAddress.u4_addr[3],
                pNlHostAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlHostAddress.u4_addr[0] = OSIX_NTOHL (NlHostAddress.u4_addr[0]);
    NlHostAddress.u4_addr[1] = OSIX_NTOHL (NlHostAddress.u4_addr[1]);
    NlHostAddress.u4_addr[2] = OSIX_NTOHL (NlHostAddress.u4_addr[2]);
    NlHostAddress.u4_addr[3] = OSIX_NTOHL (NlHostAddress.u4_addr[3]);

    pAlHostEntry =
        Rmon2AlHostGetDataEntry ((UINT4) i4HlHostControlIndex,
                                 (UINT4) i4NlProtocolDirLocalIndex,
                                 &NlHostAddress,
                                 (UINT4) i4AlProtocolDirLocalIndex);

    if (pAlHostEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC, "Get entry of AlHost table is failed \r\n");
        return SNMP_FAILURE;
    }
    if (pAlHostEntry->u4AlHostTimeMark < u4AlHostTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValAlHostOutOctets = pAlHostEntry->u4AlHostOutOctets;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetAlHostOutOctets \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetAlHostCreateTime
Input       :  The Indices
               HlHostControlIndex
               AlHostTimeMark
               NlProtocolDirLocalIndex
               NlHostAddress
               AlProtocolDirLocalIndex

               The Object 
               retValAlHostCreateTime
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlHostCreateTime (INT4 i4HlHostControlIndex, UINT4 u4AlHostTimeMark,
                        INT4 i4NlProtocolDirLocalIndex,
                        tSNMP_OCTET_STRING_TYPE * pNlHostAddress,
                        INT4 i4AlProtocolDirLocalIndex,
                        UINT4 *pu4RetValAlHostCreateTime)
{
    tAlHost            *pAlHostEntry = NULL;
    tIpAddr             NlHostAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetAlHostCreateTime \n");

    MEMSET (&NlHostAddress, 0, sizeof (tIpAddr));

    if (pNlHostAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlHostAddress, pNlHostAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlHostAddress.u4_addr[3],
                pNlHostAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlHostAddress.u4_addr[0] = OSIX_NTOHL (NlHostAddress.u4_addr[0]);
    NlHostAddress.u4_addr[1] = OSIX_NTOHL (NlHostAddress.u4_addr[1]);
    NlHostAddress.u4_addr[2] = OSIX_NTOHL (NlHostAddress.u4_addr[2]);
    NlHostAddress.u4_addr[3] = OSIX_NTOHL (NlHostAddress.u4_addr[3]);

    pAlHostEntry =
        Rmon2AlHostGetDataEntry ((UINT4) i4HlHostControlIndex,
                                 (UINT4) i4NlProtocolDirLocalIndex,
                                 &NlHostAddress,
                                 (UINT4) i4AlProtocolDirLocalIndex);

    if (pAlHostEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC, "Get entry of AlHost table is failed \r\n");
        return SNMP_FAILURE;
    }
    if (pAlHostEntry->u4AlHostTimeMark < u4AlHostTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValAlHostCreateTime = pAlHostEntry->u4AlHostCreateTime;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetAlHostCreateTime \n");
    return SNMP_SUCCESS;
}
