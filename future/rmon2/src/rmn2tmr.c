/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * $Id: rmn2tmr.c,v 1.3 2013/12/18 12:48:01 siva Exp $
 *
 * Description : This file contains procedures containing 
 *               Rmon2 Timer related operations.
 *               
 *******************************************************************/
#include "rmn2inc.h"

/****************************************************************************
*                                                                           *
* Function     : Rmon2TmrInitTmrDesc                                        *
*                                                                           *
* Description  : Initialize Rmon2 Timer Descriptors                         *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
Rmon2TmrInitTmrDesc (VOID)
{

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY Rmon2TmrInitTmrDesc\n");

    gRmon2Globals.aRmon2TmrDesc[RMON2_POLL_TMR].TmrExpFn = Rmon2TmrPollTmr;
    gRmon2Globals.aRmon2TmrDesc[RMON2_POLL_TMR].i2Offset =
        (INT2) (FSAP_OFFSETOF (tRmon2Globals, Rmon2PollTmr));

    RMON2_TRC (RMON2_FN_EXIT, "FUNC:EXIT Rmon2TmrInitTmrDesc\n");

}

/****************************************************************************
 *                                                                          *
 *    FUNCTION NAME    : Rmon2TmrInit                                       *
 *                                                                          *
 *    DESCRIPTION      : This function creates a timer list                 *
 *                       for all the timers in RMON2 module.                *
 *                                                                          *
 *    INPUT            : None                                               *
 *                                                                          *
 *    OUTPUT           : None                                               *
 *                                                                          *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                          *
 *                                                                          *
 ****************************************************************************/
PUBLIC INT4
Rmon2TmrInit (VOID)
{
    if (TmrCreateTimerList (RMON2_TASK_NAME, RMON2_ONE_SECOND_TIMER_EVENT, NULL,
                            &(gRmon2Globals.Rmon2TmrLst)) == TMR_FAILURE)
    {
        RMON2_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC | CONTROL_PLANE_TRC),
                   "Rmon2TmrInit: Timer list creation FAILED !!!\r\n");
        return OSIX_FAILURE;
    }

    Rmon2TmrInitTmrDesc ();

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 *    FUNCTION NAME    : Rmon2TmrDeInit                                     *
 *                                                                          *
 *    DESCRIPTION      : This function de-initializes the timer list        *
 *                                                                          *
 *    INPUT            : None                                               *
 *                                                                          *
 *    OUTPUT           : None                                               *
 *                                                                          *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                          *
 *                                                                          *
 ****************************************************************************/
PUBLIC INT4
Rmon2TmrDeInit (VOID)
{
    if (TmrDeleteTimerList (gRmon2Globals.Rmon2TmrLst) == TMR_FAILURE)
    {
        RMON2_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC | CONTROL_PLANE_TRC),
                   "Rmon2TmrDeInit: Timer list deletion FAILED !!!\r\n");
        return OSIX_FAILURE;
    }

    RMON2_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC | CONTROL_PLANE_TRC),
               "Rmon2TmrDeInit: Timer list deletion successful !!!\r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : Rmon2TmrStartTmr                                           *
*                                                                           *
* Description  : Starts Dsmom Timer                                         *
*                                                                           *
* Input        : pRmon2Tmr   - pointer to Rmon2Tmr structure                *
*                eRmon2TmrId - RMON2 timer ID                               *
*                u4Secs      - ticks in seconds                             *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC INT4
Rmon2TmrStartTmr (tRmon2Tmr * pRmon2Tmr, enRmon2TmrId eRmon2TmrId, UINT4 u4Secs)
{

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY Rmon2TmrStartTmr\n");

    pRmon2Tmr->eRmon2TmrId = eRmon2TmrId;

    if (TmrStartTimer (gRmon2Globals.Rmon2TmrLst, &(pRmon2Tmr->tmrNode),
                       (UINT4) NO_OF_TICKS_PER_SEC * u4Secs) != TMR_SUCCESS)
    {
        RMON2_TRC (OS_RESOURCE_TRC, "Tmr Start Failure\n");
        return OSIX_FAILURE;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: Exit Rmon2TmrStartTmr\n");

    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : Rmon2TmrStopTmr                                            *
*                                                                           *
* Description  : Stop Rmon2 Timer                                           *
*                                                                           *
* Input        : pRmon2Tmr - pointer to Rmon2Tmr structure                  *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC INT4
Rmon2TmrStopTmr (tRmon2Tmr * pRmon2Tmr)
{

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY Rmon2TmrStopTmr\n");

    if (TmrStopTimer (gRmon2Globals.Rmon2TmrLst,
                      &(pRmon2Tmr->tmrNode)) != TMR_SUCCESS)
    {
        RMON2_TRC (OS_RESOURCE_TRC, "Tmr Stop Failure\n");
        return OSIX_FAILURE;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: Exit Rmon2TmrStopTmr\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : Rmon2TmrHandleExpiry                                       *
*                                                                           *
* Description  : This procedure is invoked when the event indicating a time *
*                expiry occurs. This procedure finds the expired timers and *
*                invokes the corresponding timer routines.                  *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/
PUBLIC INT4
Rmon2TmrHandleExpiry (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    enRmon2TmrId        eRmon2TmrId;
    INT2                i2Offset = 0;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY Rmon2TmrHandleExpiry\n");
    while ((pExpiredTimers = TmrGetNextExpiredTimer (gRmon2Globals.Rmon2TmrLst))
           != NULL)
    {
        RMON2_TRC1 (OS_RESOURCE_TRC,
                    "Tmr Blk %x To Be Processed\n", pExpiredTimers);

        eRmon2TmrId = ((tRmon2Tmr *) pExpiredTimers)->eRmon2TmrId;
        i2Offset = gRmon2Globals.aRmon2TmrDesc[eRmon2TmrId].i2Offset;

        if (i2Offset == -1)
        {

            /* The timer function does not take any parameter */

            (*(gRmon2Globals.aRmon2TmrDesc[eRmon2TmrId].TmrExpFn)) (NULL);

        }
        else
        {

            /* The timer function requires a parameter */

            (*(gRmon2Globals.aRmon2TmrDesc[eRmon2TmrId].TmrExpFn))
                ((UINT1 *) pExpiredTimers - i2Offset);
        }
        /* ReStart Timer */
        if (Rmon2TmrStartTmr (&gRmon2Globals.Rmon2PollTmr,
                              eRmon2TmrId, RMON2_ONE_SECOND) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }

    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: Exit Rmon2TmrHandleExpiry\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : Rmon2TmrPollTmr                                            *
*                                                                           *
* Description  : Handles Poll timer has expiry.                             *
*                                                                           *
* Input        : pRmon2Tmr - pointer to Rmon2Tmr structure                  *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
Rmon2TmrPollTmr (VOID *pArg)
{
    UNUSED_PARAM (pArg);

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY Rmon2TmrMoveReqTmr\n");

    /* Update  Data Tables */
#ifdef NPAPI_WANTED
    Rmon2FetchHwStats (gRmon2Globals.pRmon2LastVisted);
#endif

    /* Update TopN & UserHistory Data Tables */
    Rmon2MainUpdatePeriodicTables ();

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: Exit Rmon2TmrMoveReqTmr\n");
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  rmn2tmr.c                      */
/*-----------------------------------------------------------------------*/
