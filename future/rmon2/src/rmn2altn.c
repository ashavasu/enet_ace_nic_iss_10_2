/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 * 
   $Id: rmn2altn.c,v 1.15 2014/02/24 11:40:06 siva Exp $
 * Description: This file contains the functional routine for
 *              RMON2 Application Layer Matrix TopN module
 *
 *******************************************************************/

#include "rmn2inc.h"

/*****************************************************************************/
/* Function Name      : Rmon2AlDeleteTopNEntries                             */
/*                                                                           */
/* Description        : This function is used to Update AlMatrixTopN data    */
/*                      table for Ctrl Status Changes in                     */
/*                      AlMatrixTopNCtrl table                               */
/*                                                                           */
/* Input(s)           : pAlMatrixTopNControl                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS / SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2AlDeleteTopNEntries (tAlMatrixTopNControl * pAlMatrixTopNControl)
{
    tAlMatrixTopN      *pAlMatrixTopN = NULL;
    UINT4               u4AlMatrixTopNControlIndex, u4AlMatrixTopNIndex;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AlDeleteTopNEntries \n");

    pAlMatrixTopN = Rmon2AlMatrixGetNextTopNIndex
        (pAlMatrixTopNControl->u4AlMatrixTopNControlIndex, RMON2_ZERO);

    while (pAlMatrixTopN != NULL)
    {
        if (pAlMatrixTopN->u4AlMatrixTopNControlIndex
            != pAlMatrixTopNControl->u4AlMatrixTopNControlIndex)
        {
            RMON2_TRC (RMON2_DEBUG_TRC,
                       "AlMatrixTopNControlIndex in AlMatrixTopNControl table "
                       "and AlMatrixTopN table are not same \n");
            break;
        }

        u4AlMatrixTopNControlIndex = pAlMatrixTopN->u4AlMatrixTopNControlIndex;
        u4AlMatrixTopNIndex = pAlMatrixTopN->u4AlMatrixTopNIndex;

        /* Free AlMatrix TopN Data entry */
        Rmon2AlMatrixDelTopNEntry (pAlMatrixTopN);
        pAlMatrixTopN = Rmon2AlMatrixGetNextTopNIndex
            (u4AlMatrixTopNControlIndex, u4AlMatrixTopNIndex);
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AlDeleteTopNEntries \n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AlMatrixTopNNotifyCtlChgs                             */
/*                                                                           */
/* Description  : This function is used to update AlMatrixTopN data table    */
/*                and AlMatrixTopN ctrl table for Ctrl status changes        */
/*                in HlMatrix Ctrl table                                     */
/*                                                                           */
/* Input        : u4HlMatrixControlIndex                                     */
/*                u4SetValHlMatrixControlStatus                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2AlMatrixTopNNotifyCtlChgs (UINT4 u4HlMatrixControlIndex,
                                UINT4 u4HlMatrixControlStatus)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrl = NULL;
    tAlMatrixTopN      *pAlMatrixTopN = NULL;
    UINT4               u4AlMatrixTopNControlIndex = 0, u4AlMatrixTopNIndex = 0;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY Rmon2AlMatrixTopNNotifyCtrlChgs \n");

    /* Get First Entry in NlMatrix TopN Ctrl Table */
    pAlMatrixTopNCtrl =
        Rmon2AlMatrixGetNextTopNCtlIndex (u4AlMatrixTopNControlIndex);

    while (pAlMatrixTopNCtrl != NULL)
    {
        u4AlMatrixTopNControlIndex =
            pAlMatrixTopNCtrl->u4AlMatrixTopNControlIndex;
        if (pAlMatrixTopNCtrl->u4AlMatrixTopNControlMatrixIndex ==
            u4HlMatrixControlIndex)
        {
            if (u4HlMatrixControlStatus != ACTIVE)
            {
                /* Free AlMatrix TopN Data entry */
                pAlMatrixTopN = Rmon2AlMatrixGetNextTopNIndex
                    (u4AlMatrixTopNControlIndex, u4AlMatrixTopNIndex);
                while (pAlMatrixTopN != NULL)
                {
                    if (pAlMatrixTopN->u4AlMatrixTopNControlIndex !=
                        u4AlMatrixTopNControlIndex)
                    {
                        RMON2_TRC (RMON2_DEBUG_TRC,
                                   "AlMatrixTopNControlIndex in "
                                   "AlMatrixTopNControl table "
                                   "and AlMatrixTopN table are not same\n");
                        break;
                    }

                    u4AlMatrixTopNIndex = pAlMatrixTopN->u4AlMatrixTopNIndex;

                    /* Free AlMatrix TopN Data entry */
                    Rmon2AlMatrixDelTopNEntry (pAlMatrixTopN);

                    pAlMatrixTopN = Rmon2AlMatrixGetNextTopNIndex
                        (u4AlMatrixTopNControlIndex, u4AlMatrixTopNIndex);
                }                /* End of while */

                pAlMatrixTopNCtrl->u4AlMatrixTopNControlTimeRemaining =
                    pAlMatrixTopNCtrl->u4AlMatrixTopNControlDuration;
                pAlMatrixTopNCtrl->u4AlMatrixTopNControlStatus = NOT_READY;
            }
            else if ((u4HlMatrixControlStatus == ACTIVE) &&
                     (pAlMatrixTopNCtrl->u4AlMatrixTopNControlStatus ==
                      NOT_READY))
            {
                pAlMatrixTopNCtrl->u4AlMatrixTopNControlStatus = ACTIVE;
            }
        }                        /* End of If */

        pAlMatrixTopNCtrl = Rmon2AlMatrixGetNextTopNCtlIndex
            (pAlMatrixTopNCtrl->u4AlMatrixTopNControlIndex);
    }                            /* End of while */
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AlMatrixTopNNotifyCtrlChgs \n");
    return;
}

/*****************************************************************************/
/* Function Name      : Rmon2AlMatrixGetNextTopNCtlIndex                     */
/*                                                                           */
/* Description        : This function is used to get next AlMatrix TopN      */
/*                      control table entry for the interface index          */
/*                      'u4AlMatrixTopNCtrlIndex'.                           */
/*                                                                           */
/* Input(s)           : Interface Index                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/* Return Value(s)    : NULL / Pointer to the next AlMatrix TopN control table*/
/*                      entry                                                */
/*                                                                           */
/*****************************************************************************/
tAlMatrixTopNControl *
Rmon2AlMatrixGetNextTopNCtlIndex (UINT4 u4AlMatrixTopNCtrlIndex)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;
    tAlMatrixTopNControl AlMatrixTopNCtrlEntry;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY Rmon2AlMatrixGetNextTopNCtlIndex \n");

    MEMSET (&AlMatrixTopNCtrlEntry, 0, sizeof (AlMatrixTopNCtrlEntry));

    AlMatrixTopNCtrlEntry.u4AlMatrixTopNControlIndex = u4AlMatrixTopNCtrlIndex;

    /* Get the pointer to the interface index 'u4AlMatrixTopNCtrlIndex' */
    pAlMatrixTopNCtrlEntry = (tAlMatrixTopNControl *)
        RBTreeGetNext (RMON2_ALMATRIXTOPNCTRL_TREE,
                       (tRBElem *) & AlMatrixTopNCtrlEntry, NULL);

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AlMatrixGetNextTopNCtlIndex \n");
    return pAlMatrixTopNCtrlEntry;

}

/*****************************************************************************/
/* Function Name      : Rmon2AlMatrixAddTopNCtlEntry                         */
/*                                                                           */
/* Description        : This function is used to add AlMatrixTopN            */
/*                      control table entry in the RBTree                    */
/*                                                                           */
/* Input(s)           : u4AlMatrixTopNCtrlIndex                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NULL / Pointer to the AlMatrixTopN control           */
/*                      table entry.                                         */
/*****************************************************************************/

PUBLIC tAlMatrixTopNControl *
Rmon2AlMatrixAddTopNCtlEntry (UINT4 u4AlMatrixTopNCtrlIndex)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlNode;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AlMatrixAddTopNCtlEntry \n");

    if ((pAlMatrixTopNCtrlNode = (tAlMatrixTopNControl *)
         (MemAllocMemBlk (RMON2_ALMATRIX_TOPNCTRL_POOL))) == NULL)
    {
        /* Alloc mem block failed */
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_MEM_FAIL,
                   "MemAlloc Failure for AlMatrixTopN Control table "
                   "Entry \n");
        return NULL;
    }

    MEMSET (pAlMatrixTopNCtrlNode, 0, sizeof (tAlMatrixTopNControl));

    pAlMatrixTopNCtrlNode->u4AlMatrixTopNControlIndex = u4AlMatrixTopNCtrlIndex;
    pAlMatrixTopNCtrlNode->u4AlMatrixTopNControlTimeRemaining =
        RMON2_TOPN_CTRL_TIMEREM;
    pAlMatrixTopNCtrlNode->u4AlMatrixTopNControlGeneratedReports = RMON2_ZERO;
    pAlMatrixTopNCtrlNode->u4AlMatrixTopNControlDuration = RMON2_ZERO;
    pAlMatrixTopNCtrlNode->u4AlMatrixTopNControlRequestedSize =
        RMON2_TOPN_CTRL_REQ_SIZE;
    pAlMatrixTopNCtrlNode->u4AlMatrixTopNControlGrantedSize =
        RMON2_MAX_TOPN_ENTRY;

    pAlMatrixTopNCtrlNode->u4AlMatrixTopNControlStartTime = OsixGetSysUpTime ();

    if (RBTreeAdd (RMON2_ALMATRIXTOPNCTRL_TREE, pAlMatrixTopNCtrlNode)
        == RB_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Rmon2AlMatrixAddTopNCtrlEntry: AlMatrixAddTopNCtrlEntry "
                   "is added \n");
        return pAlMatrixTopNCtrlNode;
    }
    RMON2_TRC (RMON2_DEBUG_TRC,
               "Rmon2AlMatrixAddTopNCtrlEntry: Adding AlMatrixAddTopNCtrlEntry "
               "failed \n");

    MemReleaseMemBlock (RMON2_ALMATRIX_TOPNCTRL_POOL,
                        (UINT1 *) pAlMatrixTopNCtrlNode);

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AlMatrixAddTopNCtlEntry \n");
    return NULL;
}

/*****************************************************************************/
/* Function Name      : Rmon2AlMatrixDelTopNCtlEntry                         */
/*                                                                           */
/* Description        : This function is used to remove AlMatrixTopN         */
/*                      control table entry from the RBTree                  */
/*                                                                           */
/* Input(s)           : AlMatrixTopN control node                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS/SNMP_FAILURE                            */
/*****************************************************************************/

PUBLIC INT4
Rmon2AlMatrixDelTopNCtlEntry (tAlMatrixTopNControl * pAlMatrixTopNCtrlNode)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  "
               "Rmon2AlMatrixDelTopNCtlEntry \n");

    if (RBTreeRemove (RMON2_ALMATRIXTOPNCTRL_TREE, pAlMatrixTopNCtrlNode) ==
        RB_FAILURE)
    {
        RMON2_TRC (RMON2_MEM_FAIL,
                   "RBTreeRemove Failure for AlMatrixTopN Control table "
                   "Entry \n");
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (RMON2_ALMATRIX_TOPNCTRL_POOL,
                        (UINT1 *) pAlMatrixTopNCtrlNode);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT " "Rmon2AlMatrixDelTopNCtlEntry \n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : Rmon2AlMatrixGetTopNCtlEntry                         */
/*                                                                           */
/* Description        : This function is used to get AlMatrix TopN control   */
/*                      table entry for the interface index                  */
/*                      'u4AlMatrixTopNCtrlIndex'.                           */
/*                                                                           */
/* Input(s)           : u4AlMatrixTopNCtrlIndex                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/* Return Value(s)    : NULL / Pointer to the AlMatrix TopN control table    */
/*                      entry                                                */
/*                                                                           */
/*****************************************************************************/
tAlMatrixTopNControl *
Rmon2AlMatrixGetTopNCtlEntry (UINT4 u4AlMatrixTopNCtrlIndex)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;
    tAlMatrixTopNControl AlMatrixTopNCtrlEntry;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AlMatrixGetTopNCtrlEntry \n");
    MEMSET (&AlMatrixTopNCtrlEntry, 0, sizeof (AlMatrixTopNCtrlEntry));

    AlMatrixTopNCtrlEntry.u4AlMatrixTopNControlIndex = u4AlMatrixTopNCtrlIndex;

    pAlMatrixTopNCtrlEntry = (tAlMatrixTopNControl *)
        RBTreeGet (RMON2_ALMATRIXTOPNCTRL_TREE,
                   (tRBElem *) & AlMatrixTopNCtrlEntry);

    if (pAlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "RBTreeGet Failure for AlMatrixTopN Control "
                   "table Entry \n");
        return NULL;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AlMatrixGetTopNCtlEntry \n");
    return pAlMatrixTopNCtrlEntry;

}

/*****************************************************************************/
/* Function Name      : Rmon2AlMatrixGetNextTopNIndex                        */
/*                                                                           */
/* Description        : This function is used to get next AlMatrix TopN      */
/*                      table entry for the interface index                  */
/*                      'u4AlMatrixTopNCtrlIndex'.                           */
/*                                                                           */
/* Input(s)           : u4AlMatrixTopNCtrlIndex                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/* Return Value(s)    : NULL / Pointer to the next AlMatrix TopN             */
/*                      table entry                                          */
/*                                                                           */
/*****************************************************************************/

tAlMatrixTopN      *
Rmon2AlMatrixGetNextTopNIndex (UINT4 u4AlMatrixTopNControlIndex,
                               UINT4 u4AlMatrixTopNIndex)
{
    tAlMatrixTopN      *pAlMatrixTopNEntry = NULL;
    tAlMatrixTopN       AlMatrixTopNEntry;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AlMatrixGetNextTopNIndex \n");
    MEMSET (&AlMatrixTopNEntry, 0, sizeof (AlMatrixTopNEntry));

    AlMatrixTopNEntry.u4AlMatrixTopNControlIndex = u4AlMatrixTopNControlIndex;
    AlMatrixTopNEntry.u4AlMatrixTopNIndex = u4AlMatrixTopNIndex;
    pAlMatrixTopNEntry = (tAlMatrixTopN *)
        RBTreeGetNext (RMON2_ALMATRIXTOPN_TREE,
                       (tRBElem *) & AlMatrixTopNEntry, NULL);

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AlMatrixGetNextTopNIndex \n");

    return pAlMatrixTopNEntry;

}

/*****************************************************************************/
/* Function Name      : Rmon2AlMatrixAddTopNEntry                            */
/*                                                                           */
/* Description        : This function is used to add AlMatrixTopN            */
/*                      table entry in the RBTree                            */
/*                                                                           */
/* Input(s)           : u4AlMatrixTopNIndex,pSampleTopNList                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NULL / Pointer to the AlMatrixTopN                   */
/*                      table entry.                                         */
/*****************************************************************************/
PUBLIC tAlMatrixTopN *
Rmon2AlMatrixAddTopNEntry (UINT4 u4AlMatrixTopNIndex,
                           tAlMatrixTopN * pSampleTopNList)
{
    tAlMatrixTopN      *pAlMatrixTopNNode = NULL;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AlMatrixAddTopNEntry \n");

    if ((pAlMatrixTopNNode = (tAlMatrixTopN *)
         (MemAllocMemBlk (RMON2_ALMATRIX_TOPN_POOL))) == NULL)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for Matrix TopN \r\n");
        return NULL;
    }

    MEMSET (pAlMatrixTopNNode, 0, sizeof (tAlMatrixTopN));
    MEMCPY (pAlMatrixTopNNode, pSampleTopNList, sizeof (tAlMatrixTopN));
    pAlMatrixTopNNode->u4AlMatrixTopNIndex = u4AlMatrixTopNIndex;

    if (RBTreeAdd (RMON2_ALMATRIXTOPN_TREE, (tRBElem *) pAlMatrixTopNNode)
        == RB_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Rmon2AlMatrixAddTopNDataEntry: AlMatrixAddTopNEntry "
                   "is added \n");
        return pAlMatrixTopNNode;
    }
    RMON2_TRC (RMON2_DEBUG_TRC,
               "Rmon2AlMatrixAddTopNDataEntry: Adding AlMatrixAddTopNEntry "
               "failed \n");

    MemReleaseMemBlock (RMON2_ALMATRIX_TOPN_POOL, (UINT1 *) pAlMatrixTopNNode);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AlMatrixAddTopNDataEntry \n");

    return NULL;
}

/*****************************************************************************/
/* Function Name      : Rmon2AlMatrixDelTopNEntry                            */
/*                                                                           */
/* Description        : This function is used to delete NlMatrixTopN         */
/*                      table entries in the RBTree                          */
/*                                                                           */
/* Input(s)           : pAlMatrixTopN                                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS / SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rmon2AlMatrixDelTopNEntry (tAlMatrixTopN * pAlMatrixTopN)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AlMatrixDelTopNEntry \n");

    /* Remove NlMatrixTopN. node from NlMatrixTopNRBTree */
    RBTreeRem (RMON2_ALMATRIXTOPN_TREE, (tRBElem *) pAlMatrixTopN);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (RMON2_ALMATRIX_TOPN_POOL,
                            (UINT1 *) pAlMatrixTopN) != MEM_SUCCESS)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for  AlMatrixTopN. \r\n");
        return OSIX_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AlMatrixDelTopNEntry \n");
    return OSIX_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : Rmon2AlMatrixGetTopNEntry                            */
/*                                                                           */
/* Description        : This function is used to get AlMatrix TopN           */
/*                      table entry for the interface index                  */
/*                      'u4AlMatrixTopNControlIndex,u4AlMatrixTopNIndex'.    */
/*                                                                           */
/* Input(s)           : u4AlMatrixTopNControlIndex,u4AlMatrixTopNIndex       */
/*                                                                           */
/* Output(s)          : None                                                 */
/* Return Value(s)    : NULL / Pointer to the AlMatrix TopN table            */
/*                      entry                                                */
/*                                                                           */
/*****************************************************************************/
tAlMatrixTopN      *
Rmon2AlMatrixGetTopNEntry (UINT4 u4AlMatrixTopNControlIndex,
                           UINT4 u4AlMatrixTopNIndex)
{
    tAlMatrixTopN       AlMatrixTopNEntry;
    tAlMatrixTopN      *pAlMatrixTopNEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AlMatrixGetTopNEntry \n");

    MEMSET (&AlMatrixTopNEntry, 0, sizeof (AlMatrixTopNEntry));

    AlMatrixTopNEntry.u4AlMatrixTopNControlIndex = u4AlMatrixTopNControlIndex;
    AlMatrixTopNEntry.u4AlMatrixTopNIndex = u4AlMatrixTopNIndex;
    pAlMatrixTopNEntry = (tAlMatrixTopN *)
        RBTreeGet (RMON2_ALMATRIXTOPN_TREE, (tRBElem *) & AlMatrixTopNEntry);

    if (pAlMatrixTopNEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "RBTreeGet Failure for AlMatrixTopN table Entry \n");
        return NULL;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AlMatrixGetTopNEntry \n");
    return pAlMatrixTopNEntry;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FillMatrixSampleTopNRate                                   */
/*                                                                           */
/* Description  : Fill the topn rate values                                  */
/*                                                                           */
/* Input        : pSampleTopN,pMatrixSDNode,u4SampleCount                    */
/*                u4Pktcount,u4Octetcount,u4MatrixTopNCtlIndex               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
FillAlMatrixSampleTopNRate (tAlMatrixTopN * pSampleTopN, UINT4 u4SampleCount,
                            tAlMatrixSD * pAlMatrixSDNode, UINT4 u4Pktcount,
                            UINT4 u4Octetcount,
                            tAlMatrixTopNControl * pAlMatrixTopNCtl)
{
    UINT4               u4Index = RMON2_ONE;
    UINT4               u4MinPktRate;
    UINT4               u4MinOctRate;
    UINT4               u4MinPktIndex = RMON2_ZERO;
    UINT4               u4MinOctIndex = RMON2_ZERO;
    UINT4               u4ReplaceIndex = RMON2_ZERO;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY FillMatrixSampleTopNRate \r\n");

    if (u4SampleCount < pAlMatrixTopNCtl->u4AlMatrixTopNControlGrantedSize)
    {
        pSampleTopN[u4SampleCount].u4AlMatrixTopNControlIndex =
            pAlMatrixTopNCtl->u4AlMatrixTopNControlIndex;

        pSampleTopN[u4SampleCount].u4AlMatrixTopNIndex = u4SampleCount;

        pSampleTopN[u4SampleCount].u4AlMatrixTopNIpAddrLen =
            pAlMatrixSDNode->u4Rmon2NlMatrixSDIpAddrLen;

        MEMCPY (&pSampleTopN[u4SampleCount].AlMatrixTopNSourceAddress,
                &pAlMatrixSDNode->NlMatrixSDSourceAddress,
                sizeof (pAlMatrixSDNode->NlMatrixSDSourceAddress));

        MEMCPY (&pSampleTopN[u4SampleCount].AlMatrixTopNDestAddress,
                &pAlMatrixSDNode->NlMatrixSDDestAddress,
                sizeof (pAlMatrixSDNode->NlMatrixSDDestAddress));

        pSampleTopN[u4SampleCount].u4AlMatrixTopNProtocolDirLocalIndex =
            pAlMatrixSDNode->u4NlProtocolDirLocalIndex;

        pSampleTopN[u4SampleCount].u4AlMatrixTopNAppProtocolDirLocalIndex =
            pAlMatrixSDNode->u4AlProtocolDirLocalIndex;

        pSampleTopN[u4SampleCount].u4AlMatrixTopNPktRate = u4Pktcount;

        pSampleTopN[u4SampleCount].u4AlMatrixTopNReversePktRate = u4Pktcount;

        pSampleTopN[u4SampleCount].u4AlMatrixTopNOctetRate = u4Octetcount;

        pSampleTopN[u4SampleCount].u4AlMatrixTopNReverseOctetRate =
            u4Octetcount;
    }
    else
    {
        u4MinPktRate = pSampleTopN[RMON2_ZERO].u4AlMatrixTopNPktRate;
        u4MinOctRate = pSampleTopN[RMON2_ZERO].u4AlMatrixTopNOctetRate;

        /* finding which topN entry has least Pkt Rate and Octet Rate */
        while (u4Index < pAlMatrixTopNCtl->u4AlMatrixTopNControlGrantedSize)
        {
            if (pSampleTopN[u4Index].u4AlMatrixTopNPktRate < u4MinPktRate)
            {
                u4MinPktRate = pSampleTopN[u4Index].u4AlMatrixTopNPktRate;
                u4MinPktIndex = u4Index;
            }

            if (pSampleTopN[u4Index].u4AlMatrixTopNOctetRate < u4MinOctRate)
            {
                u4MinOctRate = pSampleTopN[u4Index].u4AlMatrixTopNOctetRate;
                u4MinOctIndex = u4Index;
            }
            u4Index++;
        }                        /* end of while */

        /* Replacing the TopN entry which has least PktRate when 
         * NlMatrixTopNControlRateBase is NLMATRIX_TOPN_PKTS and 
         * least Octet Rate when NlMatrixTopNControlRateBase is
         * NLMATRIX_TOPN_OCTETS */

        if ((pAlMatrixTopNCtl->u4AlMatrixTopNControlRateBase ==
             ALMATRIX_TOPN_ALL_PKTS) ||
            (pAlMatrixTopNCtl->u4AlMatrixTopNControlRateBase ==
             ALMATRIX_TOPN_TERMINALS_PKTS))
        {

            u4ReplaceIndex = u4MinPktIndex;
        }
        else if ((pAlMatrixTopNCtl->u4AlMatrixTopNControlRateBase ==
                  ALMATRIX_TOPN_ALL_OCTETS) ||
                 (pAlMatrixTopNCtl->u4AlMatrixTopNControlRateBase ==
                  ALMATRIX_TOPN_TERMINALS_OCTETS))
        {
            u4ReplaceIndex = u4MinOctIndex;

        }
        else
        {
            RMON2_TRC (RMON2_DEBUG_TRC,
                       "HIGH CAPACITY PKTS and HIGH CAPACITY OCTETS are not "
                       "supported \n");
            return;
        }

        if ((u4MinPktRate <= u4Pktcount) && (u4MinOctRate <= u4Octetcount))
        {
            pSampleTopN[u4ReplaceIndex].u4AlMatrixTopNControlIndex =
                pAlMatrixTopNCtl->u4AlMatrixTopNControlIndex;

            pSampleTopN[u4ReplaceIndex].u4AlMatrixTopNIndex = u4ReplaceIndex;

            pSampleTopN[u4ReplaceIndex].u4AlMatrixTopNIpAddrLen =
                pAlMatrixSDNode->u4Rmon2NlMatrixSDIpAddrLen;

            MEMCPY (&pSampleTopN[u4ReplaceIndex].AlMatrixTopNSourceAddress,
                    &pAlMatrixSDNode->NlMatrixSDSourceAddress,
                    sizeof (pAlMatrixSDNode->NlMatrixSDSourceAddress));

            MEMCPY (&pSampleTopN[u4ReplaceIndex].AlMatrixTopNDestAddress,
                    &pAlMatrixSDNode->NlMatrixSDDestAddress,
                    sizeof (pAlMatrixSDNode->NlMatrixSDDestAddress));

            pSampleTopN[u4ReplaceIndex].u4AlMatrixTopNProtocolDirLocalIndex =
                pAlMatrixSDNode->u4NlProtocolDirLocalIndex;

            pSampleTopN[u4ReplaceIndex].u4AlMatrixTopNAppProtocolDirLocalIndex =
                pAlMatrixSDNode->u4AlProtocolDirLocalIndex;

            pSampleTopN[u4ReplaceIndex].u4AlMatrixTopNPktRate = u4Pktcount;

            pSampleTopN[u4ReplaceIndex].u4AlMatrixTopNReversePktRate =
                u4Pktcount;

            pSampleTopN[u4ReplaceIndex].u4AlMatrixTopNOctetRate = u4Octetcount;

            pSampleTopN[u4ReplaceIndex].u4AlMatrixTopNReverseOctetRate =
                u4Octetcount;
        }
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC:EXIT FillMatrixSampleTopNRate \r\n");

}

/****************************************************************************
* Function           : GetSamplesFromMatrixTable                            *
*                                                                           *
* Input (s)          : HlMatrix control index                               *
*                                                                           *
* Output (s)         : None.                                                *
*                                                                           *
* Returns            : None.                                                *
*                                                                           *
* Action :                                                                  *
*                                                                           *
* Gets Sample from a list of host attached to a index and places            *
* the in the specified address.                                             *
****************************************************************************/

PRIVATE VOID
GetSamplesFromAlMatrixTable (UINT4 u4CtlIndex)
{
    tAlMatrixSD        *pAlMatrixSDEntry = NULL;
    UINT4               u4NlProtocolDirLocalIndex = RMON2_ZERO;
    UINT4               u4AlProtocolDirLocalIndex = RMON2_ZERO;
    tIpAddr             NlMatrixSDSourceAddress;
    tIpAddr             NlMatrixSDDestAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY " "GetSamplesFromMatrixTable \n");
    MEMSET (&NlMatrixSDSourceAddress, RMON2_ZERO,
            sizeof (NlMatrixSDSourceAddress));

    MEMSET (&NlMatrixSDDestAddress, RMON2_ZERO, sizeof (NlMatrixSDDestAddress));

    pAlMatrixSDEntry = Rmon2AlMatrixGetNextDataIndex (RMON2_ALMATRIXSD_TABLE,
                                                      u4CtlIndex,
                                                      u4NlProtocolDirLocalIndex,
                                                      &NlMatrixSDSourceAddress,
                                                      &NlMatrixSDDestAddress,
                                                      u4AlProtocolDirLocalIndex);
    /* while is for traversing SD table entries */
    while (pAlMatrixSDEntry != NULL)
    {
        if (pAlMatrixSDEntry->u4HlMatrixControlIndex != u4CtlIndex)
        {
            RMON2_TRC (RMON2_DEBUG_TRC,
                       "HlMatrixControlIndex in AlMatrixSD table is not same "
                       "as ctrlIndex \n");
            break;
        }

        MEMCPY (&pAlMatrixSDEntry->AlMatrixSDPrevSample,
                &pAlMatrixSDEntry->AlMatrixSDCurSample,
                sizeof (tAlMatrixSDSample));

        pAlMatrixSDEntry =
            Rmon2AlMatrixGetNextDataIndex (RMON2_ALMATRIXSD_TABLE,
                                           pAlMatrixSDEntry->
                                           u4HlMatrixControlIndex,
                                           pAlMatrixSDEntry->
                                           u4NlProtocolDirLocalIndex,
                                           &pAlMatrixSDEntry->
                                           NlMatrixSDSourceAddress,
                                           &pAlMatrixSDEntry->
                                           NlMatrixSDDestAddress,
                                           pAlMatrixSDEntry->
                                           u4AlProtocolDirLocalIndex);
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT " "GetSamplesFromMatrixTable \n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GetMatrixTopNEntries                                       */
/*                                                                           */
/* Description  : Get the current samples from Matrix table                  */
/*                                                                           */
/* Input        : pMatrixTopNCtl,pSampleTopNList                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : u4SampleCount                                              */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT4
GetAlMatrixTopNEntries (tAlMatrixTopNControl * pAlMatrixTopNCtl,
                        tAlMatrixTopN * pSampleTopNList)
{
    UINT4               u4SampleCount = RMON2_ZERO;
    UINT4               u4DeltaPkt = RMON2_ZERO, u4DeltaOct = RMON2_ZERO;
    UINT4               u4NlProtocolDirLocalIndex = RMON2_ZERO;
    UINT4               u4AlProtocolDirLocalIndex = RMON2_ZERO;
    tIpAddr             NlMatrixSDSourceAddress;
    tIpAddr             NlMatrixSDDestAddress;
    UINT1               au1ProtocolDirID[RMON2_MAX_PROT_DIR_ID_LEN];
    UINT1               au1ProtocolDirParams[RMON2_MAX_PROT_DIR_PARAM_LEN];
    tSNMP_OCTET_STRING_TYPE ProtocolDirID, ProtocolDirParameters;
    tAlMatrixSD        *pAlMatrixSDEntry = NULL;
    tProtocolDir       *pProtoDirEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY " "GetMatrixTopNEntries \n");

    MEMSET (&NlMatrixSDSourceAddress, RMON2_ZERO,
            sizeof (NlMatrixSDSourceAddress));

    MEMSET (&NlMatrixSDDestAddress, RMON2_ZERO, sizeof (NlMatrixSDDestAddress));

    pAlMatrixSDEntry =
        Rmon2AlMatrixGetNextDataIndex (RMON2_ALMATRIXSD_TABLE,
                                       pAlMatrixTopNCtl->
                                       u4AlMatrixTopNControlMatrixIndex,
                                       u4NlProtocolDirLocalIndex,
                                       &NlMatrixSDSourceAddress,
                                       &NlMatrixSDDestAddress,
                                       u4AlProtocolDirLocalIndex);
    while (pAlMatrixSDEntry != NULL)
    {
        if (pAlMatrixSDEntry->u4HlMatrixControlIndex !=
            pAlMatrixTopNCtl->u4AlMatrixTopNControlMatrixIndex)
        {
            RMON2_TRC (RMON2_DEBUG_TRC,
                       "HlMatrixControlIndex in NlMatrixSD table is not same "
                       "as NlMatrixTopNControlMatrixIndex in NlMatrixTopN Ctrl "
                       "table \n");
            break;
        }
        switch (pAlMatrixTopNCtl->u4AlMatrixTopNControlRateBase)
        {

            case ALMATRIX_TOPN_ALL_PKTS:

                if (pAlMatrixSDEntry->AlMatrixSDPrevSample.u4AlMatrixSDPkts !=
                    RMON2_ZERO)
                {
                    u4DeltaPkt =
                        (pAlMatrixSDEntry->AlMatrixSDCurSample.
                         u4AlMatrixSDPkts) -
                        (pAlMatrixSDEntry->AlMatrixSDPrevSample.
                         u4AlMatrixSDPkts);
                    u4DeltaOct =
                        (pAlMatrixSDEntry->AlMatrixSDCurSample.
                         u4AlMatrixSDOctets) -
                        (pAlMatrixSDEntry->AlMatrixSDPrevSample.
                         u4AlMatrixSDOctets);

                    if (u4DeltaPkt > RMON2_ZERO)
                    {
                        FillAlMatrixSampleTopNRate (pSampleTopNList,
                                                    u4SampleCount,
                                                    pAlMatrixSDEntry,
                                                    u4DeltaPkt, u4DeltaOct,
                                                    pAlMatrixTopNCtl);
                        u4SampleCount++;
                    }

                }

                break;
            case ALMATRIX_TOPN_TERMINALS_PKTS:

                /* Get ProtocolDirectory entry which has no 
                 * child for finding ALMATRIX_TOPN_TERMINALS_PKTS */

                MEMSET (au1ProtocolDirID, 0, RMON2_MAX_PROT_DIR_ID_LEN);

                MEMSET (au1ProtocolDirParams, 0, RMON2_MAX_PROT_DIR_PARAM_LEN);

                ProtocolDirID.pu1_OctetList = au1ProtocolDirID;
                ProtocolDirID.i4_Length = 0;

                ProtocolDirParameters.pu1_OctetList = au1ProtocolDirParams;
                ProtocolDirParameters.i4_Length = 0;

                pProtoDirEntry =
                    Rmon2PDirGetNextProtDirIndex (&ProtocolDirID,
                                                  &ProtocolDirParameters);

                /* while is for traversing protocol directory table for checking 
                 * whether the SD table entry has child or not*/
                while (pProtoDirEntry != NULL)
                {
                    ProtocolDirID.i4_Length =
                        (INT4) pProtoDirEntry->u4ProtocolDirIDLen;
                    MEMCPY (ProtocolDirID.pu1_OctetList,
                            pProtoDirEntry->au1ProtocolDirID,
                            pProtoDirEntry->u4ProtocolDirIDLen);
                    ProtocolDirParameters.i4_Length =
                        (INT4) pProtoDirEntry->u4ProtocolDirParametersLen;
                    MEMCPY (ProtocolDirParameters.pu1_OctetList,
                            pProtoDirEntry->au1ProtocolDirParameters,
                            pProtoDirEntry->u4ProtocolDirParametersLen);

                    if ((pProtoDirEntry->u4ProtocolDirLocalIndex ==
                         pAlMatrixSDEntry->u4AlProtocolDirLocalIndex) &&
                        ((UINT4) pProtoDirEntry->u1ProtocolDirType ==
                         NOT_EXTENSIBLE_NO_ADDR_RECOGN))
                    {
                        if (pAlMatrixSDEntry->AlMatrixSDPrevSample.
                            u4AlMatrixSDPkts != RMON2_ZERO)
                        {
                            u4DeltaPkt =
                                (pAlMatrixSDEntry->AlMatrixSDCurSample.
                                 u4AlMatrixSDPkts) -
                                (pAlMatrixSDEntry->AlMatrixSDPrevSample.
                                 u4AlMatrixSDPkts);
                            u4DeltaOct =
                                (pAlMatrixSDEntry->AlMatrixSDCurSample.
                                 u4AlMatrixSDOctets) -
                                (pAlMatrixSDEntry->AlMatrixSDPrevSample.
                                 u4AlMatrixSDOctets);

                            if (u4DeltaPkt > RMON2_ZERO)
                            {
                                FillAlMatrixSampleTopNRate (pSampleTopNList,
                                                            u4SampleCount,
                                                            pAlMatrixSDEntry,
                                                            u4DeltaPkt,
                                                            u4DeltaOct,
                                                            pAlMatrixTopNCtl);
                                u4SampleCount++;
                            }

                        }        /* end of if */
                    }            /* end of if */
                    pProtoDirEntry =
                        Rmon2PDirGetNextProtDirIndex (&ProtocolDirID,
                                                      &ProtocolDirParameters);
                }                /*end of while */
                break;

            case ALMATRIX_TOPN_ALL_OCTETS:

                if (pAlMatrixSDEntry->AlMatrixSDPrevSample.u4AlMatrixSDOctets !=
                    RMON2_ZERO)
                {
                    u4DeltaOct =
                        (pAlMatrixSDEntry->AlMatrixSDCurSample.
                         u4AlMatrixSDOctets) -
                        (pAlMatrixSDEntry->AlMatrixSDPrevSample.
                         u4AlMatrixSDOctets);

                    u4DeltaPkt =
                        (pAlMatrixSDEntry->AlMatrixSDCurSample.
                         u4AlMatrixSDPkts) -
                        (pAlMatrixSDEntry->AlMatrixSDPrevSample.
                         u4AlMatrixSDPkts);

                    if (u4DeltaOct > RMON2_ZERO)
                    {
                        FillAlMatrixSampleTopNRate (pSampleTopNList,
                                                    u4SampleCount,
                                                    pAlMatrixSDEntry,
                                                    u4DeltaPkt, u4DeltaOct,
                                                    pAlMatrixTopNCtl);
                        u4SampleCount++;
                    }

                }

                break;

            case ALMATRIX_TOPN_TERMINALS_OCTETS:
                /* Get ProtocolDirectory entry which has no
                 * child for finding ALMATRIX_TOPN_TERMINALS_OCTETS */

                MEMSET (au1ProtocolDirID, 0, RMON2_MAX_PROT_DIR_ID_LEN);

                MEMSET (au1ProtocolDirParams, 0, RMON2_MAX_PROT_DIR_PARAM_LEN);

                ProtocolDirID.pu1_OctetList = au1ProtocolDirID;
                ProtocolDirID.i4_Length = RMON2_MAX_PROT_DIR_ID_LEN;

                ProtocolDirParameters.pu1_OctetList = au1ProtocolDirParams;
                ProtocolDirParameters.i4_Length = RMON2_MAX_PROT_DIR_PARAM_LEN;

                pProtoDirEntry =
                    Rmon2PDirGetNextProtDirIndex (&ProtocolDirID,
                                                  &ProtocolDirParameters);

                /* while is for traversing protocol directory table for checking
                 * whether the SD table entry has child or not*/
                while (pProtoDirEntry != NULL)
                {
                    ProtocolDirID.i4_Length =
                        (INT4) pProtoDirEntry->u4ProtocolDirIDLen;
                    MEMCPY (ProtocolDirID.pu1_OctetList,
                            pProtoDirEntry->au1ProtocolDirID,
                            pProtoDirEntry->u4ProtocolDirIDLen);

                    ProtocolDirParameters.i4_Length =
                        (INT4) pProtoDirEntry->u4ProtocolDirParametersLen;
                    MEMCPY (ProtocolDirParameters.pu1_OctetList,
                            pProtoDirEntry->au1ProtocolDirParameters,
                            pProtoDirEntry->u4ProtocolDirParametersLen);

                    if ((pProtoDirEntry->u4ProtocolDirLocalIndex ==
                         pAlMatrixSDEntry->u4AlProtocolDirLocalIndex) &&
                        ((UINT4) pProtoDirEntry->u1ProtocolDirType ==
                         NOT_EXTENSIBLE_NO_ADDR_RECOGN))
                    {
                        if (pAlMatrixSDEntry->AlMatrixSDPrevSample.
                            u4AlMatrixSDOctets != RMON2_ZERO)
                        {
                            u4DeltaOct =
                                (pAlMatrixSDEntry->AlMatrixSDCurSample.
                                 u4AlMatrixSDOctets) -
                                (pAlMatrixSDEntry->AlMatrixSDPrevSample.
                                 u4AlMatrixSDOctets);
                            u4DeltaPkt =
                                (pAlMatrixSDEntry->AlMatrixSDCurSample.
                                 u4AlMatrixSDPkts) -
                                (pAlMatrixSDEntry->AlMatrixSDPrevSample.
                                 u4AlMatrixSDPkts);

                            if (u4DeltaOct > RMON2_ZERO)
                            {
                                FillAlMatrixSampleTopNRate (pSampleTopNList,
                                                            u4SampleCount,
                                                            pAlMatrixSDEntry,
                                                            u4DeltaPkt,
                                                            u4DeltaOct,
                                                            pAlMatrixTopNCtl);
                                u4SampleCount++;
                            }

                        }        /* end of if */
                    }            /* end of if */
                    pProtoDirEntry =
                        Rmon2PDirGetNextProtDirIndex (&ProtocolDirID,
                                                      &ProtocolDirParameters);
                }                /*end of while */
                break;

            case ALMATRIX_TOPN_ALL_HIGHCAPACITY_PKTS:
                RMON2_TRC (RMON2_DEBUG_TRC, "High Capacity Packets are : "
                           "not supported \n");
                break;
            case ALMATRIX_TOPN_TERMINALS_HIGHCAPACITY_PKTS:
                RMON2_TRC (RMON2_DEBUG_TRC, "High Capacity Packets are : "
                           "not supported \n");
                break;
            case ALMATRIX_TOPN_ALL_HIGHCAPACITY_OCTETS:
                RMON2_TRC (RMON2_DEBUG_TRC, "High Capacity Octets are : "
                           "not supported \n");
                break;
            case ALMATRIX_TOPN_TERMINALS_HIGHCAPACITY_OCTETS:
                RMON2_TRC (RMON2_DEBUG_TRC, "High Capacity Octets are : "
                           "not supported \n");
                break;
            default:
                RMON2_TRC (RMON2_DEBUG_TRC, "Unknown Option\n");
                break;

        }                        /* end of switch case */

        pAlMatrixSDEntry =
            Rmon2AlMatrixGetNextDataIndex (RMON2_ALMATRIXSD_TABLE,
                                           pAlMatrixSDEntry->
                                           u4HlMatrixControlIndex,
                                           pAlMatrixSDEntry->
                                           u4NlProtocolDirLocalIndex,
                                           &pAlMatrixSDEntry->
                                           NlMatrixSDSourceAddress,
                                           &pAlMatrixSDEntry->
                                           NlMatrixSDDestAddress,
                                           pAlMatrixSDEntry->
                                           u4AlProtocolDirLocalIndex);

    }                            /*end of while */

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT " "GetMatrixTopNEntries \n");
    return u4SampleCount;
}

/**********************************************************************
* Function           : Rmon2CmpAlPktsFunction                         *
*                                                                     *
* Input (s)          : 2 input parameters such as sample1 and sample2 *
*                      to compare                                     *
*                                                                     *
* Output (s)         : None                                           *
*                                                                     *
* Returns            : 1 if sample1 >sample2,-1 if sample1 < sample2  *
*                      0 otherwise                                    *
*                                                                     *
* Action :                                                            *
* Called  To compare AlMatrixSD entries....                           *
**********************************************************************/
PRIVATE INT4
Rmon2CmpAlPktsFunction (CONST VOID *pParam1, CONST VOID *pParam2)
{
    CONST tAlMatrixTopN *pSample1 = (CONST tAlMatrixTopN *) pParam1;

    CONST tAlMatrixTopN *pSample2 = (CONST tAlMatrixTopN *) pParam2;

    if (pSample1->u4AlMatrixTopNPktRate < pSample2->u4AlMatrixTopNPktRate)
    {
        return -1;
    }
    if (pSample1->u4AlMatrixTopNPktRate > pSample2->u4AlMatrixTopNPktRate)
    {
        return 1;
    }
    if (pSample1->u4AlMatrixTopNPktRate == pSample2->u4AlMatrixTopNPktRate)
    {
        return 0;
    }

    return OSIX_SUCCESS;
}

/**********************************************************************
* Function           : Rmon2CmpAlOctetsFunction                       *
*                                                                     *
* Input (s)          : 2 input parameters such as sample1 and sample2 *
*                      to compare                                     *
*                                                                     *
* Output (s)         : None                                           *
*                                                                     *
* Returns            : 1 if sample1 >sample2,-1 if sample1 < sample2  *
*                      0 otherwise                                    *
*                                                                     *
* Action :                                                            *
* Called  To compare AlMatrixSD entries....                           *
**********************************************************************/
PRIVATE INT4
Rmon2CmpAlOctetsFunction (CONST VOID *pParam1, CONST VOID *pParam2)
{
    CONST tAlMatrixTopN *pSample1 = (CONST tAlMatrixTopN *) pParam1;

    CONST tAlMatrixTopN *pSample2 = (CONST tAlMatrixTopN *) pParam2;

    if (pSample1->u4AlMatrixTopNOctetRate < pSample2->u4AlMatrixTopNOctetRate)
    {
        return -1;
    }
    if (pSample1->u4AlMatrixTopNOctetRate > pSample2->u4AlMatrixTopNOctetRate)
    {
        return 1;
    }
    if (pSample1->u4AlMatrixTopNOctetRate == pSample2->u4AlMatrixTopNOctetRate)
    {
        return 0;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : SetSortedMatrixTopNTable                                   */
/*                                                                           */
/* Description  : Generates topn report of size, granted size                */
/*                                                                           */
/* Input        : pSampleTopN,u4GrantedSize                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT4
SetSortedAlMatrixTopNTable (tAlMatrixTopN * pSampleTopN, UINT4 u4Count,
                            UINT4 u4GrantedSize)
{
    tAlMatrixTopN      *pAlMatrixTopN = NULL;
    UINT4               u4Pass;
    UINT4               u4EntryCount = RMON2_ZERO;
    u4EntryCount = (u4Count < u4GrantedSize) ? u4Count : u4GrantedSize;
    u4Count = u4EntryCount - 1;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY " "SetSortedMatrixTopNTable \n");

    for (u4Pass = RMON2_ONE; u4Pass <= u4EntryCount; u4Pass++)
    {
        pAlMatrixTopN =
            Rmon2AlMatrixAddTopNEntry (u4Pass, &pSampleTopN[u4Count]);

        if (pAlMatrixTopN == NULL)
        {
            RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_MEM_FAIL,
                       " Memory Allocation "
                       "RBTree Add Failed - Matrix TopN Table \n");

            return OSIX_FAILURE;
        }
        u4Count--;
    }
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: EXIT " "SetSortedMatrixTopNTable \n");
    return OSIX_SUCCESS;
}

/**********************************************************************
* Function           : Rmon2UpdateAlMatrixTopNTable                   *
*                                                                     *
* Description        : Called on each second To update Valid Matrix   *
*                      TopN Table entries....                         *
*                                                                     *
* Input (s)          : None.                                          *
*                                                                     *
* Output (s)         : None.                                          *
*                                                                     *
* Returns            : None.                                          *
*                                                                     *
**********************************************************************/
PUBLIC VOID         Rmon2UpdateAlMatrixTopNTable
ARG_LIST ((VOID))
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrl = NULL;
    tAlMatrixSampleTopNList *pSampleTopNList = NULL;
    UINT4               u4EntryCount = RMON2_ZERO;
    UINT4               u4Count = RMON2_ZERO;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY RmonUpdateAlMatrixTopNTable \n");

    if ((pSampleTopNList = (tAlMatrixSampleTopNList *)
         (MemAllocMemBlk (RMON2_ALMATRIX_SAMPLE_TOPN_POOL))) == NULL)
    {
        /* Alloc mem block failed */
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_MEM_FAIL,
                   "MemAlloc Failure for AlMatrix Sample TopN List "
                   "Entry \n");
        return;
    }

    MEMSET (pSampleTopNList, 0, sizeof (tAlMatrixSampleTopNList));

    pAlMatrixTopNCtrl = Rmon2AlMatrixGetNextTopNCtlIndex (RMON2_ZERO);

    /* while is for traversing TopN Control table */

    while (pAlMatrixTopNCtrl != NULL)
    {
        if (pAlMatrixTopNCtrl->u4AlMatrixTopNControlStatus == ACTIVE)
        {
            if (pAlMatrixTopNCtrl->u4AlMatrixTopNControlTimeRemaining >
                RMON2_ZERO)
            {
                if (pAlMatrixTopNCtrl->u4AlMatrixTopNControlTimeRemaining ==
                    pAlMatrixTopNCtrl->u4AlMatrixTopNControlDuration)
                {
                    GetSamplesFromAlMatrixTable (pAlMatrixTopNCtrl->
                                                 u4AlMatrixTopNControlMatrixIndex);
                }

                else
                {
                    if (pAlMatrixTopNCtrl->u4AlMatrixTopNControlTimeRemaining ==
                        RMON2_ONE)
                    {
                        /* Get the TopN entries from AlMatrixSD table and 
                         * store in pSampleTopNList*/
                        u4Count = GetAlMatrixTopNEntries (pAlMatrixTopNCtrl,
                                                          (tAlMatrixTopN *)
                                                          pSampleTopNList->
                                                          SampleTopNList);

                        if (u4Count > 0)
                        {
                            /* Has to free TopN Entries if they were already present in
                             * RmonAlTopNRBTree */
                            Rmon2AlDeleteTopNEntries (pAlMatrixTopNCtrl);
                            u4EntryCount = (u4Count < pAlMatrixTopNCtrl->
                                            u4AlMatrixTopNControlGrantedSize) ?
                                u4Count : pAlMatrixTopNCtrl->
                                u4AlMatrixTopNControlGrantedSize;

                            if ((pAlMatrixTopNCtrl->
                                 u4AlMatrixTopNControlRateBase ==
                                 ALMATRIX_TOPN_ALL_PKTS)
                                || (pAlMatrixTopNCtrl->
                                    u4AlMatrixTopNControlRateBase ==
                                    ALMATRIX_TOPN_TERMINALS_PKTS))
                            {
                                qsort ((tAlMatrixTopN *) pSampleTopNList->
                                       SampleTopNList, u4EntryCount,
                                       sizeof (tAlMatrixTopN),
                                       (VOID *) Rmon2CmpAlPktsFunction);
                            }
                            else if ((pAlMatrixTopNCtrl->
                                      u4AlMatrixTopNControlRateBase ==
                                      ALMATRIX_TOPN_ALL_OCTETS)
                                     || (pAlMatrixTopNCtrl->
                                         u4AlMatrixTopNControlRateBase ==
                                         ALMATRIX_TOPN_TERMINALS_OCTETS))
                            {
                                qsort ((tAlMatrixTopN *) pSampleTopNList->
                                       SampleTopNList, u4EntryCount,
                                       sizeof (tAlMatrixTopN),
                                       (VOID *) Rmon2CmpAlOctetsFunction);

                            }
                            else
                            {
                                RMON2_TRC (RMON2_DEBUG_TRC,
                                           "HIGH CAPACITY PKTS and HIGH CAPACITY OCTETS "
                                           "are not supported \n");
                                MemReleaseMemBlock
                                    (RMON2_ALMATRIX_SAMPLE_TOPN_POOL,
                                     (UINT1 *) pSampleTopNList);
                                return;
                            }

                            if ((SetSortedAlMatrixTopNTable
                                 ((tAlMatrixTopN *) pSampleTopNList->
                                  SampleTopNList, u4Count,
                                  pAlMatrixTopNCtrl->
                                  u4AlMatrixTopNControlGrantedSize)) ==
                                OSIX_FAILURE)
                            {
                                RMON2_TRC (RMON2_DEBUG_TRC,
                                           "Adding to AlMatrixTopN RBtree failed \n");
                                MemReleaseMemBlock
                                    (RMON2_ALMATRIX_SAMPLE_TOPN_POOL,
                                     (UINT1 *) pSampleTopNList);
                                return;

                            }
                            else
                            {
                                pAlMatrixTopNCtrl->
                                    u4AlMatrixTopNControlGeneratedReports++;
                            }
                        }
                        /* start another collection */

                        pAlMatrixTopNCtrl->u4AlMatrixTopNControlTimeRemaining =
                            pAlMatrixTopNCtrl->u4AlMatrixTopNControlDuration +
                            1;
                        pAlMatrixTopNCtrl->u4AlMatrixTopNControlStartTime =
                            OsixGetSysUpTime ();

                    }            /* end of if - Time Remaining == 1 */

                }                /* end of else - Time Remaining >0 */

                pAlMatrixTopNCtrl->u4AlMatrixTopNControlTimeRemaining--;
            }                    /* end of if - Time Remaining >0 */

        }                        /* end of if - Row status active */
        pAlMatrixTopNCtrl =
            Rmon2AlMatrixGetNextTopNCtlIndex (pAlMatrixTopNCtrl->
                                              u4AlMatrixTopNControlIndex);
    }                            /* end of while */

    MemReleaseMemBlock (RMON2_ALMATRIX_SAMPLE_TOPN_POOL,
                        (UINT1 *) pSampleTopNList);

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT RmonUpdateAlMatrixTopNTable \n");

}
