/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 *  $Id: rmn2exlw.c,v 1.8 2011/03/25 12:12:38 siva Exp $
 *
 * Description: This file contains the low level nmh routines for
 *              RMON2 Extension for RMON1 Tables.
 *
 *******************************************************************/
#include  "rmn2inc.h"

/* LOW LEVEL Routines for Table : EtherStats2Table. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceEtherStats2Table
 Input       :  The Indices
                EtherStatsIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceEtherStats2Table (INT4 i4EtherStatsIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhValidateIndexInstanceEtherStats2Table\n");
#ifdef RMON_WANTED
    RMON_LOCK ();
    if (nmhValidateIndexInstanceEtherStatsTable (i4EtherStatsIndex)
        != SNMP_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Error in function nmhValidateIndexInstanceEtherStatsTable\n");
        RMON_UNLOCK ();
        return SNMP_FAILURE;
    }
    RMON_UNLOCK ();
#else
    UNUSED_PARAM (i4EtherStatsIndex);
#endif
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhValidateIndexInstanceEtherStats2Table\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexEtherStats2Table
 Input       :  The Indices
                EtherStatsIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexEtherStats2Table (INT4 *pi4EtherStatsIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetFirstIndexEtherStats2Table \n");
#ifdef RMON_WANTED
    RMON_LOCK ();
    if (nmhGetFirstIndexEtherStatsTable (pi4EtherStatsIndex) != SNMP_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Error in function nmhGetFirstIndexEtherStatsTable\n");
        RMON_UNLOCK ();
        return SNMP_FAILURE;
    }
    RMON_UNLOCK ();
#else
    UNUSED_PARAM (pi4EtherStatsIndex);
#endif
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetFirstIndexEtherStats2Table \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexEtherStats2Table
 Input       :  The Indices
                EtherStatsIndex
                nextEtherStatsIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexEtherStats2Table (INT4 i4EtherStatsIndex,
                                 INT4 *pi4NextEtherStatsIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetNextIndexEtherStats2Table \n");
#ifdef RMON_WANTED
    RMON_LOCK ();
    if (nmhGetNextIndexEtherStatsTable (i4EtherStatsIndex,
                                        pi4NextEtherStatsIndex) != SNMP_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Error in function nmhGetNextIndexEtherStatsTable\n");
        RMON_UNLOCK ();
        return SNMP_FAILURE;
    }
    RMON_UNLOCK ();
#else
    UNUSED_PARAM (i4EtherStatsIndex);
    UNUSED_PARAM (pi4NextEtherStatsIndex);
#endif
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNextIndexEtherStats2Table \n");
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetEtherStatsDroppedFrames
 Input       :  The Indices
                EtherStatsIndex

                The Object 
                retValEtherStatsDroppedFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEtherStatsDroppedFrames (INT4 i4EtherStatsIndex,
                               UINT4 *pu4RetValEtherStatsDroppedFrames)
{
#ifdef RMON_WANTED
    tRmonExtnNode       EtherExtn;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetEtherStatsDroppedFrames \n");

    MEMSET (&EtherExtn, RMON2_ZERO, sizeof (tRmonExtnNode));
   
    RMON_LOCK ();
    RmonUtlGetEtherStatsExtn (i4EtherStatsIndex, &EtherExtn);
    RMON_UNLOCK ();

    *pu4RetValEtherStatsDroppedFrames = EtherExtn.u4DroppedFrames;
#else
    UNUSED_PARAM (i4EtherStatsIndex);
    UNUSED_PARAM (pu4RetValEtherStatsDroppedFrames);
#endif
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetEtherStatsDroppedFrames \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetEtherStatsCreateTime
 Input       :  The Indices
                EtherStatsIndex

                The Object 
                retValEtherStatsCreateTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEtherStatsCreateTime (INT4 i4EtherStatsIndex,
                            UINT4 *pu4RetValEtherStatsCreateTime)
{
#ifdef RMON_WANTED
    tRmonExtnNode       EtherExtn;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetEtherStatsCreateTime \n");

    MEMSET (&EtherExtn, RMON2_ZERO, sizeof (tRmonExtnNode));

    RMON_LOCK ();
    RmonUtlGetEtherStatsExtn (i4EtherStatsIndex, &EtherExtn);
    RMON_UNLOCK ();

    *pu4RetValEtherStatsCreateTime = EtherExtn.u4CreateTime;
#else
    UNUSED_PARAM (i4EtherStatsIndex);
    UNUSED_PARAM (pu4RetValEtherStatsCreateTime);
#endif
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetEtherStatsCreateTime \n");
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : HistoryControl2Table. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceHistoryControl2Table
 Input       :  The Indices
                HistoryControlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceHistoryControl2Table (INT4 i4HistoryControlIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhValidateIndexInstanceHistoryControl2Table  \n");
#ifdef RMON_WANTED
    RMON_LOCK ();
    if (nmhValidateIndexInstanceHistoryControlTable (i4HistoryControlIndex)
        != SNMP_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Error in function nmhValidateIndexInstanceHistoryControlTable\n");
        RMON_UNLOCK ();
        return SNMP_FAILURE;
    }
    RMON_UNLOCK ();
#else
    UNUSED_PARAM (i4HistoryControlIndex);
#endif
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhValidateIndexInstanceHistoryControl2Table  \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexHistoryControl2Table
 Input       :  The Indices
                HistoryControlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexHistoryControl2Table (INT4 *pi4HistoryControlIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetFirstIndexHistoryControl2Table  \n");

#ifdef RMON_WANTED
    RMON_LOCK ();
    if (nmhGetFirstIndexHistoryControlTable (pi4HistoryControlIndex)
        != SNMP_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Error in function nmhGetFirstIndexHistoryControlTable\n");
        RMON_UNLOCK ();
        return SNMP_FAILURE;
    }
    RMON_UNLOCK ();
#else
    UNUSED_PARAM (pi4HistoryControlIndex);
#endif
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetFirstIndexHistoryControl2Table  \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexHistoryControl2Table
 Input       :  The Indices
                HistoryControlIndex
                nextHistoryControlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexHistoryControl2Table (INT4 i4HistoryControlIndex,
                                     INT4 *pi4NextHistoryControlIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetNextIndexHistoryControl2Table  \n");
#ifdef RMON_WANTED
    RMON_LOCK ();
    if (nmhGetNextIndexHistoryControlTable (i4HistoryControlIndex,
                                            pi4NextHistoryControlIndex) !=
        SNMP_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Error in function nmhGetNextIndexHistoryControlTable\n");
        RMON_UNLOCK ();
        return SNMP_FAILURE;
    }
    RMON_UNLOCK ();
#else
    UNUSED_PARAM (i4HistoryControlIndex);
    UNUSED_PARAM (pi4NextHistoryControlIndex);
#endif
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetNextIndexHistoryControl2Table  \n");
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetHistoryControlDroppedFrames
 Input       :  The Indices
                HistoryControlIndex

                The Object 
                retValHistoryControlDroppedFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHistoryControlDroppedFrames (INT4 i4HistoryControlIndex,
                                   UINT4 *pu4RetValHistoryControlDroppedFrames)
{
#ifdef RMON_WANTED
    tRmonExtnNode       HistCtlExtn;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetHistoryControlDroppedFrames\n");

    MEMSET (&HistCtlExtn, RMON2_ZERO, sizeof (tRmonExtnNode));
    
    RMON_LOCK ();
    RmonUtlGetHistCtlExtn (i4HistoryControlIndex, &HistCtlExtn);
    RMON_UNLOCK ();

    *pu4RetValHistoryControlDroppedFrames = HistCtlExtn.u4DroppedFrames;

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4HistoryControlIndex);
    UNUSED_PARAM (pu4RetValHistoryControlDroppedFrames);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetHistoryControlDroppedFrames\n");
    return SNMP_FAILURE;
#endif
}

/* LOW LEVEL Routines for Table : HostControl2Table. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceHostControl2Table
 Input       :  The Indices
                HostControlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceHostControl2Table (INT4 i4HostControlIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhValidateIndexInstanceHostControl2Table\n");
#ifdef RMON_WANTED
    RMON_LOCK ();
    if (nmhValidateIndexInstanceHostControlTable (i4HostControlIndex)
        != SNMP_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Error in function nmhValidateIndexInstanceHostControlTable\n");
        RMON_UNLOCK ();
        return SNMP_FAILURE;
    }
    RMON_UNLOCK ();
#else
    UNUSED_PARAM (i4HostControlIndex);
#endif
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhValidateIndexInstanceHostControl2Table\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexHostControl2Table
 Input       :  The Indices
                HostControlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexHostControl2Table (INT4 *pi4HostControlIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetFirstIndexHostControl2Table\n");
#ifdef RMON_WANTED
    RMON_LOCK ();
    if (nmhGetFirstIndexHostControlTable (pi4HostControlIndex) != SNMP_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Error in function nmhGetFirstIndexHostControlTable\n");
        RMON_UNLOCK ();
        return SNMP_FAILURE;
    }
    RMON_UNLOCK ();
#else
    UNUSED_PARAM (pi4HostControlIndex);
#endif
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetFirstIndexHostControl2Table\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexHostControl2Table
 Input       :  The Indices
                HostControlIndex
                nextHostControlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexHostControl2Table (INT4 i4HostControlIndex,
                                  INT4 *pi4NextHostControlIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetNextIndexHostControl2Table\n");
#ifdef RMON_WANTED
    RMON_LOCK ();
    if (nmhGetNextIndexHostControlTable (i4HostControlIndex,
                                         pi4NextHostControlIndex) !=
        SNMP_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Error in function nmhGetNextIndexHostControlTable\n");
        RMON_UNLOCK ();
        return SNMP_FAILURE;
    }
    RMON_UNLOCK ();
#else
    UNUSED_PARAM (i4HostControlIndex);
    UNUSED_PARAM (pi4NextHostControlIndex);
#endif
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNextIndexHostControl2Table\n");
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetHostControlDroppedFrames
 Input       :  The Indices
                HostControlIndex

                The Object 
                retValHostControlDroppedFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHostControlDroppedFrames (INT4 i4HostControlIndex,
                                UINT4 *pu4RetValHostControlDroppedFrames)
{
#ifdef RMON_WANTED
    tRmonExtnNode       HostCtlExtn;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetHostControlDroppedFrames \n");

    MEMSET (&HostCtlExtn, RMON2_ZERO, sizeof (tRmonExtnNode));
    
    RMON_LOCK ();
    RmonUtlGetHostCtlExtn (i4HostControlIndex, &HostCtlExtn);
    RMON_UNLOCK ();

    *pu4RetValHostControlDroppedFrames = HostCtlExtn.u4DroppedFrames;

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4HostControlIndex);
    UNUSED_PARAM (pu4RetValHostControlDroppedFrames);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT  nmhGetHostControlDroppedFrames \n");
    return SNMP_FAILURE;
#endif

}

/****************************************************************************
 Function    :  nmhGetHostControlCreateTime
 Input       :  The Indices
                HostControlIndex

                The Object 
                retValHostControlCreateTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHostControlCreateTime (INT4 i4HostControlIndex,
                             UINT4 *pu4RetValHostControlCreateTime)
{
#ifdef RMON_WANTED
    tRmonExtnNode       HostCtlExtn;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetHostControlCreateTime \n");

    MEMSET (&HostCtlExtn, RMON2_ZERO, sizeof (tRmonExtnNode));
    RMON_LOCK ();
    RmonUtlGetHostCtlExtn (i4HostControlIndex, &HostCtlExtn);
    RMON_UNLOCK ();
    *pu4RetValHostControlCreateTime = HostCtlExtn.u4CreateTime;

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4HostControlIndex);
    UNUSED_PARAM (pu4RetValHostControlCreateTime);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT  nmhGetHostControlCreateTime \n");

    return SNMP_FAILURE;
#endif
}

/* LOW LEVEL Routines for Table : MatrixControl2Table. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMatrixControl2Table
 Input       :  The Indices
                MatrixControlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMatrixControl2Table (INT4 i4MatrixControlIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhValidateIndexInstanceMatrixControl2Table \n");
#ifdef RMON_WANTED
    RMON_LOCK ();
    if (nmhValidateIndexInstanceMatrixControlTable (i4MatrixControlIndex)
        != SNMP_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Error in function nmhValidateIndexInstanceMatrixControlTable\n");
        RMON_UNLOCK ();
        return SNMP_FAILURE;
    }
    RMON_LOCK ();
#else
    UNUSED_PARAM (i4MatrixControlIndex);
#endif
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhValidateIndexInstanceMatrixControl2Table \n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMatrixControl2Table
 Input       :  The Indices
                MatrixControlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMatrixControl2Table (INT4 *pi4MatrixControlIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetFirstIndexMatrixControl2Table \n");
#ifdef RMON_WANTED
    RMON_LOCK ();
    if (nmhGetFirstIndexMatrixControlTable (pi4MatrixControlIndex)
        != SNMP_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Error in function nmhGetFirstIndexMatrixControlTable\n");
        RMON_UNLOCK ();
        return SNMP_FAILURE;
    }
    RMON_UNLOCK ();
#else
    UNUSED_PARAM (pi4MatrixControlIndex);
#endif
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT  nmhGetFirstIndexMatrixControl2Table \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMatrixControl2Table
 Input       :  The Indices
                MatrixControlIndex
                nextMatrixControlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMatrixControl2Table (INT4 i4MatrixControlIndex,
                                    INT4 *pi4NextMatrixControlIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetNextIndexMatrixControl2Table \n");
#ifdef RMON_WANTED
    RMON_LOCK ();
    if (nmhGetNextIndexMatrixControlTable (i4MatrixControlIndex,
                                           pi4NextMatrixControlIndex) !=
        SNMP_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Error in function nmhGetNextIndexMatrixControlTable\n");
        RMON_UNLOCK ();
        return SNMP_FAILURE;
    }
    RMON_UNLOCK ();
#else
    UNUSED_PARAM (i4MatrixControlIndex);
    UNUSED_PARAM (pi4NextMatrixControlIndex);
#endif
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetNextIndexMatrixControl2Table \n");
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMatrixControlDroppedFrames
 Input       :  The Indices
                MatrixControlIndex

                The Object 
                retValMatrixControlDroppedFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMatrixControlDroppedFrames (INT4 i4MatrixControlIndex,
                                  UINT4 *pu4RetValMatrixControlDroppedFrames)
{
#ifdef RMON_WANTED
    tRmonExtnNode       MtrxCtlExtn;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetMatrixControlDroppedFrames \n");

    MEMSET (&MtrxCtlExtn, RMON2_ZERO, sizeof (tRmonExtnNode));
    RMON_LOCK ();
    RmonUtlGetMtrxCtlExtn (i4MatrixControlIndex, &MtrxCtlExtn);
    RMON_UNLOCK ();

    *pu4RetValMatrixControlDroppedFrames = MtrxCtlExtn.u4DroppedFrames;

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4MatrixControlIndex);
    UNUSED_PARAM (pu4RetValMatrixControlDroppedFrames);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetMatrixControlDroppedFrames \n");

    return SNMP_FAILURE;
#endif

}

/****************************************************************************
 Function    :  nmhGetMatrixControlCreateTime
 Input       :  The Indices
                MatrixControlIndex

                The Object 
                retValMatrixControlCreateTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMatrixControlCreateTime (INT4 i4MatrixControlIndex,
                               UINT4 *pu4RetValMatrixControlCreateTime)
{
#ifdef RMON_WANTED
    tRmonExtnNode       MtrxCtlExtn;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetMatrixControlCreateTime \n");

    MEMSET (&MtrxCtlExtn, RMON2_ZERO, sizeof (tRmonExtnNode));
    RMON_LOCK ();
    RmonUtlGetMtrxCtlExtn (i4MatrixControlIndex, &MtrxCtlExtn);
    RMON_UNLOCK ();

    *pu4RetValMatrixControlCreateTime = MtrxCtlExtn.u4CreateTime;

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4MatrixControlIndex);
    UNUSED_PARAM (pu4RetValMatrixControlCreateTime);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetMatrixControlCreateTime \n");

    return SNMP_FAILURE;
#endif
}

/* LOW LEVEL Routines for Table : Channel2Table. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceChannel2Table
 Input       :  The Indices
                ChannelIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceChannel2Table (INT4 i4ChannelIndex)
{
    UNUSED_PARAM (i4ChannelIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexChannel2Table
 Input       :  The Indices
                ChannelIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexChannel2Table (INT4 *pi4ChannelIndex)
{
    UNUSED_PARAM (pi4ChannelIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexChannel2Table
 Input       :  The Indices
                ChannelIndex
                nextChannelIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1
nmhGetNextIndexChannel2Table (INT4 i4ChannelIndex, INT4 *pi4NextChannelIndex)
{
    UNUSED_PARAM (i4ChannelIndex);
    UNUSED_PARAM (pi4NextChannelIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetChannelDroppedFrames
 Input       :  The Indices
                ChannelIndex

                The Object 
                retValChannelDroppedFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetChannelDroppedFrames (INT4 i4ChannelIndex,
                            UINT4 *pu4RetValChannelDroppedFrames)
{
    UNUSED_PARAM (i4ChannelIndex);
    UNUSED_PARAM (pu4RetValChannelDroppedFrames);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetChannelCreateTime
 Input       :  The Indices
                ChannelIndex

                The Object 
                retValChannelCreateTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetChannelCreateTime (INT4 i4ChannelIndex, UINT4 *pu4RetValChannelCreateTime)
{
    UNUSED_PARAM (i4ChannelIndex);
    UNUSED_PARAM (pu4RetValChannelCreateTime);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : TokenRingMLStats2Table. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceTokenRingMLStats2Table
 Input       :  The Indices
                TokenRingMLStatsIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceTokenRingMLStats2Table (INT4 i4TokenRingMLStatsIndex)
{
    UNUSED_PARAM (i4TokenRingMLStatsIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexTokenRingMLStats2Table
 Input       :  The Indices
                TokenRingMLStatsIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexTokenRingMLStats2Table (INT4 *pi4TokenRingMLStatsIndex)
{
    UNUSED_PARAM (pi4TokenRingMLStatsIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexTokenRingMLStats2Table
 Input       :  The Indices
                TokenRingMLStatsIndex
                nextTokenRingMLStatsIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexTokenRingMLStats2Table (INT4 i4TokenRingMLStatsIndex,
                                       INT4 *pi4NextTokenRingMLStatsIndex)
{
    UNUSED_PARAM (i4TokenRingMLStatsIndex);
    UNUSED_PARAM (pi4NextTokenRingMLStatsIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetTokenRingMLStatsDroppedFrames
 Input       :  The Indices
                TokenRingMLStatsIndex

                The Object 
                retValTokenRingMLStatsDroppedFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTokenRingMLStatsDroppedFrames (INT4 i4TokenRingMLStatsIndex,
                                     UINT4
                                     *pu4RetValTokenRingMLStatsDroppedFrames)
{
    UNUSED_PARAM (i4TokenRingMLStatsIndex);
    UNUSED_PARAM (pu4RetValTokenRingMLStatsDroppedFrames);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetTokenRingMLStatsCreateTime
 Input       :  The Indices
                TokenRingMLStatsIndex

                The Object 
                retValTokenRingMLStatsCreateTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTokenRingMLStatsCreateTime (INT4 i4TokenRingMLStatsIndex,
                                  UINT4 *pu4RetValTokenRingMLStatsCreateTime)
{
    UNUSED_PARAM (i4TokenRingMLStatsIndex);
    UNUSED_PARAM (pu4RetValTokenRingMLStatsCreateTime);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : TokenRingPStats2Table. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceTokenRingPStats2Table
 Input       :  The Indices
                TokenRingPStatsIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceTokenRingPStats2Table (INT4 i4TokenRingPStatsIndex)
{
    UNUSED_PARAM (i4TokenRingPStatsIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexTokenRingPStats2Table
 Input       :  The Indices
                TokenRingPStatsIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexTokenRingPStats2Table (INT4 *pi4TokenRingPStatsIndex)
{
    UNUSED_PARAM (pi4TokenRingPStatsIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexTokenRingPStats2Table
 Input       :  The Indices
                TokenRingPStatsIndex
                nextTokenRingPStatsIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1
nmhGetNextIndexTokenRingPStats2Table (INT4 i4TokenRingPStatsIndex,
                                      INT4 *pi4NextTokenRingPStatsIndex)
{
    UNUSED_PARAM (i4TokenRingPStatsIndex);
    UNUSED_PARAM (pi4NextTokenRingPStatsIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetTokenRingPStatsDroppedFrames
 Input       :  The Indices
                TokenRingPStatsIndex

                The Object 
                retValTokenRingPStatsDroppedFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTokenRingPStatsDroppedFrames (INT4 i4TokenRingPStatsIndex,
                                    UINT4
                                    *pu4RetValTokenRingPStatsDroppedFrames)
{
    UNUSED_PARAM (i4TokenRingPStatsIndex);
    UNUSED_PARAM (pu4RetValTokenRingPStatsDroppedFrames);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetTokenRingPStatsCreateTime
 Input       :  The Indices
                TokenRingPStatsIndex

                The Object 
                retValTokenRingPStatsCreateTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTokenRingPStatsCreateTime (INT4 i4TokenRingPStatsIndex,
                                 UINT4 *pu4RetValTokenRingPStatsCreateTime)
{
    UNUSED_PARAM (i4TokenRingPStatsIndex);
    UNUSED_PARAM (pu4RetValTokenRingPStatsCreateTime);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : RingStationControl2Table. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceRingStationControl2Table
 Input       :  The Indices
                RingStationControlIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
nmhValidateIndexInstanceRingStationControl2Table
    (INT4 i4RingStationControlIfIndex)
{
    UNUSED_PARAM (i4RingStationControlIfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexRingStationControl2Table
 Input       :  The Indices
                RingStationControlIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexRingStationControl2Table (INT4 *pi4RingStationControlIfIndex)
{
    UNUSED_PARAM (pi4RingStationControlIfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexRingStationControl2Table
 Input       :  The Indices
                RingStationControlIfIndex
                nextRingStationControlIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexRingStationControl2Table (INT4 i4RingStationControlIfIndex,
                                         INT4 *pi4NextRingStationControlIfIndex)
{
    UNUSED_PARAM (i4RingStationControlIfIndex);
    UNUSED_PARAM (pi4NextRingStationControlIfIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetRingStationControlDroppedFrames
 Input       :  The Indices
                RingStationControlIfIndex

                The Object 
                retValRingStationControlDroppedFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetRingStationControlDroppedFrames (INT4 i4RingStationControlIfIndex,
                                       UINT4
                                       *pu4RetValRingStationControlDroppedFrames)
{
    UNUSED_PARAM (i4RingStationControlIfIndex);
    UNUSED_PARAM (pu4RetValRingStationControlDroppedFrames);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetRingStationControlCreateTime
 Input       :  The Indices
                RingStationControlIfIndex

                The Object 
                retValRingStationControlCreateTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetRingStationControlCreateTime (INT4 i4RingStationControlIfIndex,
                                    UINT4
                                    *pu4RetValRingStationControlCreateTime)
{
    UNUSED_PARAM (i4RingStationControlIfIndex);
    UNUSED_PARAM (pu4RetValRingStationControlCreateTime);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : SourceRoutingStats2Table. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceSourceRoutingStats2Table
 Input       :  The Indices
                SourceRoutingStatsIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
nmhValidateIndexInstanceSourceRoutingStats2Table
    (INT4 i4SourceRoutingStatsIfIndex)
{
    UNUSED_PARAM (i4SourceRoutingStatsIfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexSourceRoutingStats2Table
 Input       :  The Indices
                SourceRoutingStatsIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexSourceRoutingStats2Table (INT4 *pi4SourceRoutingStatsIfIndex)
{
    UNUSED_PARAM (pi4SourceRoutingStatsIfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexSourceRoutingStats2Table
 Input       :  The Indices
                SourceRoutingStatsIfIndex
                nextSourceRoutingStatsIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexSourceRoutingStats2Table (INT4 i4SourceRoutingStatsIfIndex,
                                         INT4 *pi4NextSourceRoutingStatsIfIndex)
{
    UNUSED_PARAM (i4SourceRoutingStatsIfIndex);
    UNUSED_PARAM (pi4NextSourceRoutingStatsIfIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSourceRoutingStatsDroppedFrames
 Input       :  The Indices
                SourceRoutingStatsIfIndex

                The Object 
                retValSourceRoutingStatsDroppedFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSourceRoutingStatsDroppedFrames (INT4 i4SourceRoutingStatsIfIndex,
                                       UINT4
                                       *pu4RetValSourceRoutingStatsDroppedFrames)
{
    UNUSED_PARAM (i4SourceRoutingStatsIfIndex);
    UNUSED_PARAM (pu4RetValSourceRoutingStatsDroppedFrames);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSourceRoutingStatsCreateTime
 Input       :  The Indices
                SourceRoutingStatsIfIndex

                The Object 
                retValSourceRoutingStatsCreateTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSourceRoutingStatsCreateTime (INT4 i4SourceRoutingStatsIfIndex,
                                    UINT4
                                    *pu4RetValSourceRoutingStatsCreateTime)
{
    UNUSED_PARAM (i4SourceRoutingStatsIfIndex);
    UNUSED_PARAM (pu4RetValSourceRoutingStatsCreateTime);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Filter2Table. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFilter2Table
 Input       :  The Indices
                FilterIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFilter2Table (INT4 i4FilterIndex)
{
    UNUSED_PARAM (i4FilterIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFilter2Table
 Input       :  The Indices
                FilterIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFilter2Table (INT4 *pi4FilterIndex)
{
    UNUSED_PARAM (pi4FilterIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFilter2Table
 Input       :  The Indices
                FilterIndex
                nextFilterIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFilter2Table (INT4 i4FilterIndex, INT4 *pi4NextFilterIndex)
{
    UNUSED_PARAM (i4FilterIndex);
    UNUSED_PARAM (pi4NextFilterIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFilterProtocolDirDataLocalIndex
 Input       :  The Indices
                FilterIndex

                The Object 
                retValFilterProtocolDirDataLocalIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFilterProtocolDirDataLocalIndex (INT4 i4FilterIndex,
                                       INT4
                                       *pi4RetValFilterProtocolDirDataLocalIndex)
{
    UNUSED_PARAM (i4FilterIndex);
    UNUSED_PARAM (pi4RetValFilterProtocolDirDataLocalIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFilterProtocolDirLocalIndex
 Input       :  The Indices
                FilterIndex

                The Object 
                retValFilterProtocolDirLocalIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFilterProtocolDirLocalIndex (INT4 i4FilterIndex,
                                   INT4 *pi4RetValFilterProtocolDirLocalIndex)
{
    UNUSED_PARAM (i4FilterIndex);
    UNUSED_PARAM (pi4RetValFilterProtocolDirLocalIndex);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFilterProtocolDirDataLocalIndex
 Input       :  The Indices
                FilterIndex

                The Object 
                setValFilterProtocolDirDataLocalIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFilterProtocolDirDataLocalIndex (INT4 i4FilterIndex,
                                       INT4
                                       i4SetValFilterProtocolDirDataLocalIndex)
{
    UNUSED_PARAM (i4FilterIndex);
    UNUSED_PARAM (i4SetValFilterProtocolDirDataLocalIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFilterProtocolDirLocalIndex
 Input       :  The Indices
                FilterIndex

                The Object 
                setValFilterProtocolDirLocalIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFilterProtocolDirLocalIndex (INT4 i4FilterIndex,
                                   INT4 i4SetValFilterProtocolDirLocalIndex)
{
    UNUSED_PARAM (i4FilterIndex);
    UNUSED_PARAM (i4SetValFilterProtocolDirLocalIndex);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FilterProtocolDirDataLocalIndex
 Input       :  The Indices
                FilterIndex

                The Object 
                testValFilterProtocolDirDataLocalIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FilterProtocolDirDataLocalIndex (UINT4 *pu4ErrorCode,
                                          INT4 i4FilterIndex,
                                          INT4
                                          i4TestValFilterProtocolDirDataLocalIndex)
{
    UNUSED_PARAM (i4FilterIndex);
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFilterProtocolDirDataLocalIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FilterProtocolDirLocalIndex
 Input       :  The Indices
                FilterIndex

                The Object 
                testValFilterProtocolDirLocalIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FilterProtocolDirLocalIndex (UINT4 *pu4ErrorCode,
                                      INT4 i4FilterIndex,
                                      INT4 i4TestValFilterProtocolDirLocalIndex)
{
    UNUSED_PARAM (i4FilterIndex);
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFilterProtocolDirLocalIndex);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Filter2Table
 Input       :  The Indices
                FilterIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Filter2Table (UINT4 *pu4ErrorCode,
                      tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
