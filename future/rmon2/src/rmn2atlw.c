/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * $Id: rmn2atlw.c,v 1.7 2014/02/05 12:28:59 siva Exp $
 *
 * Description: This file contains the low level nmh routines for
 *              RMON2 Application Layer Matrix TopN module.            
 *
 *******************************************************************/
#include "rmn2inc.h"

/* LOW LEVEL Routines for Table : AlMatrixTopNControlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceAlMatrixTopNControlTable
 Input       :  The Indices
                AlMatrixTopNControlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceAlMatrixTopNControlTable (INT4
                                                  i4AlMatrixTopNControlIndex)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "nmhValidateIndexInstanceAlMatrixTopNControlTable \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4AlMatrixTopNControlIndex < RMON2_ONE) ||
        (i4AlMatrixTopNControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid AlMatrixTopNControlIndex \n");
        return SNMP_FAILURE;
    }
    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetTopNCtlEntry ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AlMatrixTopN Control table"
                    "for index : %d is failed \r\n",
                    i4AlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "nmhValidateIndexInstanceAlMatrixTopNControlTable \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexAlMatrixTopNControlTable
 Input       :  The Indices
                AlMatrixTopNControlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexAlMatrixTopNControlTable (INT4 *pi4AlMatrixTopNControlIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetFirstIndexAlMatrixTopNControlTable \n");

    if ((nmhGetNextIndexAlMatrixTopNControlTable (RMON2_ZERO,
                                                  pi4AlMatrixTopNControlIndex))
        != SNMP_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get first entry of AlMatrixTopN Control table is "
                   "failed\r\n");
        return SNMP_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetFirstIndexAlMatrixTopNControlTable \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexAlMatrixTopNControlTable
 Input       :  The Indices
                AlMatrixTopNControlIndex
                nextAlMatrixTopNControlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexAlMatrixTopNControlTable (INT4 i4AlMatrixTopNControlIndex,
                                         INT4 *pi4NextAlMatrixTopNControlIndex)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNextIndexAlMatrixTopNControlTable \n");

    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetNextTopNCtlIndex ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AlMatrixTopN Control table"
                    "for index : %d is failed \r\n",
                    i4AlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }
    *pi4NextAlMatrixTopNControlIndex =
        (INT4) pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlIndex;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetNextIndexAlMatrixTopNControlTable \n");
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetAlMatrixTopNControlMatrixIndex
 Input       :  The Indices
                AlMatrixTopNControlIndex

                The Object 
                retValAlMatrixTopNControlMatrixIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlMatrixTopNControlMatrixIndex (INT4 i4AlMatrixTopNControlIndex,
                                      INT4
                                      *pi4RetValAlMatrixTopNControlMatrixIndex)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetAlMatrixTopNControlMatrixIndex \n");

    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetTopNCtlEntry ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AlMatrixTopN Control table"
                    "for index : %d is failed \r\n",
                    i4AlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValAlMatrixTopNControlMatrixIndex =
        (INT4) pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlMatrixIndex;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetAlMatrixTopNControlMatrixIndex \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetAlMatrixTopNControlRateBase
 Input       :  The Indices
                AlMatrixTopNControlIndex

                The Object 
                retValAlMatrixTopNControlRateBase
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlMatrixTopNControlRateBase (INT4 i4AlMatrixTopNControlIndex,
                                   INT4 *pi4RetValAlMatrixTopNControlRateBase)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetAlMatrixTopNControlRateBase \n");

    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetTopNCtlEntry ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AlMatrixTopN Control table"
                    "for index : %d is failed \r\n",
                    i4AlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValAlMatrixTopNControlRateBase =
        (INT4) pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlRateBase;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetAlMatrixTopNControlRateBase \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetAlMatrixTopNControlTimeRemaining
 Input       :  The Indices
                AlMatrixTopNControlIndex

                The Object 
                retValAlMatrixTopNControlTimeRemaining
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlMatrixTopNControlTimeRemaining (INT4 i4AlMatrixTopNControlIndex,
                                        INT4
                                        *pi4RetValAlMatrixTopNControlTimeRemaining)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetAlMatrixTopNControlTimeRemaining \n");

    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetTopNCtlEntry ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AlMatrixTopN Control table"
                    "for index : %d is failed \r\n",
                    i4AlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }

    if (gi4MibSaveStatus == MIB_SAVE_IN_PROGRESS)
    {
        *pi4RetValAlMatrixTopNControlTimeRemaining =
            (INT4) pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlDuration;

        return SNMP_SUCCESS;

    }

    *pi4RetValAlMatrixTopNControlTimeRemaining =
        (INT4) pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlTimeRemaining;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetAlMatrixTopNControlTimeRemaining \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetAlMatrixTopNControlGeneratedReports
 Input       :  The Indices
                AlMatrixTopNControlIndex

                The Object 
                retValAlMatrixTopNControlGeneratedReports
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlMatrixTopNControlGeneratedReports (INT4 i4AlMatrixTopNControlIndex,
                                           UINT4
                                           *pu4RetValAlMatrixTopNControlGeneratedReports)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetAlMatrixTopNControlGeneratedReports \n");

    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetTopNCtlEntry ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AlMatrixTopN Control table"
                    "for index : %d is failed \r\n",
                    i4AlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValAlMatrixTopNControlGeneratedReports =
        pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlGeneratedReports;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetAlMatrixTopNControlGeneratedReports \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetAlMatrixTopNControlDuration
 Input       :  The Indices
                AlMatrixTopNControlIndex

                The Object 
                retValAlMatrixTopNControlDuration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlMatrixTopNControlDuration (INT4 i4AlMatrixTopNControlIndex,
                                   INT4 *pi4RetValAlMatrixTopNControlDuration)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetAlMatrixTopNControlDuration \n");

    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetTopNCtlEntry ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AlMatrixTopN Control table"
                    "for index : %d is failed \r\n",
                    i4AlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValAlMatrixTopNControlDuration =
        (INT4) pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlDuration;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetAlMatrixTopNControlDuration \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetAlMatrixTopNControlRequestedSize
 Input       :  The Indices
                AlMatrixTopNControlIndex

                The Object 
                retValAlMatrixTopNControlRequestedSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlMatrixTopNControlRequestedSize (INT4 i4AlMatrixTopNControlIndex,
                                        INT4
                                        *pi4RetValAlMatrixTopNControlRequestedSize)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetAlMatrixTopNControlRequestedSize \n");

    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetTopNCtlEntry ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AlMatrixTopN Control table"
                    "for index : %d is failed \r\n",
                    i4AlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValAlMatrixTopNControlRequestedSize =
        (INT4) pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlRequestedSize;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetAlMatrixTopNControlRequestedSize \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetAlMatrixTopNControlGrantedSize
 Input       :  The Indices
                AlMatrixTopNControlIndex

                The Object 
                retValAlMatrixTopNControlGrantedSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlMatrixTopNControlGrantedSize (INT4 i4AlMatrixTopNControlIndex,
                                      INT4
                                      *pi4RetValAlMatrixTopNControlGrantedSize)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetAlMatrixTopNControlGrantedSize \n");

    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetTopNCtlEntry ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AlMatrixTopN Control table"
                    "for index : %d is failed \r\n",
                    i4AlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValAlMatrixTopNControlGrantedSize =
        (INT4) pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlGrantedSize;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetAlMatrixTopNControlGrantedSize \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetAlMatrixTopNControlStartTime
 Input       :  The Indices
                AlMatrixTopNControlIndex

                The Object 
                retValAlMatrixTopNControlStartTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlMatrixTopNControlStartTime (INT4 i4AlMatrixTopNControlIndex,
                                    UINT4
                                    *pu4RetValAlMatrixTopNControlStartTime)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetAlMatrixTopNControlStartTime \n");

    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetTopNCtlEntry ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AlMatrixTopN Control table"
                    "for index : %d is failed \r\n",
                    i4AlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValAlMatrixTopNControlStartTime =
        pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlStartTime;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetAlMatrixTopNControlStartTime \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetAlMatrixTopNControlOwner
 Input       :  The Indices
                AlMatrixTopNControlIndex

                The Object 
                retValAlMatrixTopNControlOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlMatrixTopNControlOwner (INT4 i4AlMatrixTopNControlIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValAlMatrixTopNControlOwner)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetAlMatrixTopNControlOwner \n");

    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetTopNCtlEntry ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AlMatrixTopN Control table"
                    "for index : %d is failed \r\n",
                    i4AlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }

    pRetValAlMatrixTopNControlOwner->i4_Length =
        (INT4) STRLEN (pAlMatrixTopNCtrlEntry->au1AlMatrixTopNControlOwner);
    MEMCPY (pRetValAlMatrixTopNControlOwner->pu1_OctetList,
            pAlMatrixTopNCtrlEntry->au1AlMatrixTopNControlOwner,
            pRetValAlMatrixTopNControlOwner->i4_Length);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetAlMatrixTopNControlOwner \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetAlMatrixTopNControlStatus
 Input       :  The Indices
                AlMatrixTopNControlIndex

                The Object 
                retValAlMatrixTopNControlStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlMatrixTopNControlStatus (INT4 i4AlMatrixTopNControlIndex,
                                 INT4 *pi4RetValAlMatrixTopNControlStatus)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetAlMatrixTopNControlStatus \n");

    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetTopNCtlEntry ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AlMatrixTopN Control table"
                    "for index : %d is failed \r\n",
                    i4AlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }

    *pi4RetValAlMatrixTopNControlStatus =
        (INT4) pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlStatus;

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetAlMatrixTopNControlStatus \n");
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetAlMatrixTopNControlMatrixIndex
 Input       :  The Indices
                AlMatrixTopNControlIndex

                The Object 
                setValAlMatrixTopNControlMatrixIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetAlMatrixTopNControlMatrixIndex (INT4 i4AlMatrixTopNControlIndex,
                                      INT4
                                      i4SetValAlMatrixTopNControlMatrixIndex)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhSetAlMatrixTopNControlMatrixIndex \n");

    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetTopNCtlEntry ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AlMatrixTopN Control table"
                    "for index : %d is failed \r\n",
                    i4AlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }

    pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlMatrixIndex =
        (UINT4) i4SetValAlMatrixTopNControlMatrixIndex;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhSetAlMatrixTopNControlMatrixIndex \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetAlMatrixTopNControlRateBase
 Input       :  The Indices
                AlMatrixTopNControlIndex

                The Object 
                setValAlMatrixTopNControlRateBase
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetAlMatrixTopNControlRateBase (INT4 i4AlMatrixTopNControlIndex,
                                   INT4 i4SetValAlMatrixTopNControlRateBase)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhSetAlMatrixTopNControlRateBase \n");

    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetTopNCtlEntry ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AlMatrixTopN Control table"
                    "for index : %d is failed \r\n",
                    i4AlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }

    pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlRateBase =
        (UINT4) i4SetValAlMatrixTopNControlRateBase;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhSetAlMatrixTopNControlRateBase \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetAlMatrixTopNControlTimeRemaining
 Input       :  The Indices
                AlMatrixTopNControlIndex

                The Object 
                setValAlMatrixTopNControlTimeRemaining
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetAlMatrixTopNControlTimeRemaining (INT4 i4AlMatrixTopNControlIndex,
                                        INT4
                                        i4SetValAlMatrixTopNControlTimeRemaining)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhSetAlMatrixTopNControlTimeRemaining \n");

    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetTopNCtlEntry ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AlMatrixTopN Control table"
                    "for index : %d is failed \r\n",
                    i4AlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }
    pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlTimeRemaining =
        (UINT4) i4SetValAlMatrixTopNControlTimeRemaining;
    pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlDuration =
        (UINT4) i4SetValAlMatrixTopNControlTimeRemaining;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhSetAlMatrixTopNControlTimeRemaining \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetAlMatrixTopNControlRequestedSize
 Input       :  The Indices
                AlMatrixTopNControlIndex

                The Object 
                setValAlMatrixTopNControlRequestedSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetAlMatrixTopNControlRequestedSize (INT4 i4AlMatrixTopNControlIndex,
                                        INT4
                                        i4SetValAlMatrixTopNControlRequestedSize)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;
    UINT4               u4Minimum = 0;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhSetAlMatrixTopNControlRequestedSize \n");

    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetTopNCtlEntry ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AlMatrixTopN Control table"
                    "for index : %d is failed \r\n",
                    i4AlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }

    pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlRequestedSize =
        (UINT4) i4SetValAlMatrixTopNControlRequestedSize;

    u4Minimum = (UINT4) ((i4SetValAlMatrixTopNControlRequestedSize
                          < RMON2_MAX_TOPN_ENTRY) ?
                         i4SetValAlMatrixTopNControlRequestedSize :
                         RMON2_MAX_TOPN_ENTRY);

    pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlGrantedSize = u4Minimum;

    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhSetAlMatrixTopNControlRequestedSize \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetAlMatrixTopNControlOwner
 Input       :  The Indices
                AlMatrixTopNControlIndex

                The Object 
                setValAlMatrixTopNControlOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetAlMatrixTopNControlOwner (INT4 i4AlMatrixTopNControlIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValAlMatrixTopNControlOwner)
{
    UINT4               u4Minimum = 0;
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhSetAlMatrixTopNControlOwner \n");

    u4Minimum =
        (UINT4) ((pSetValAlMatrixTopNControlOwner->i4_Length <
                  RMON2_OWNER_LEN) ? pSetValAlMatrixTopNControlOwner->
                 i4_Length : RMON2_OWNER_LEN);

    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetTopNCtlEntry ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of AlMatrixTopN Control table"
                    "for index : %d is failed \r\n",
                    i4AlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }
    MEMCPY (pAlMatrixTopNCtrlEntry->au1AlMatrixTopNControlOwner,
            pSetValAlMatrixTopNControlOwner->pu1_OctetList, u4Minimum);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhSetAlMatrixTopNControlOwner \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetAlMatrixTopNControlStatus
 Input       :  The Indices
                AlMatrixTopNControlIndex

                The Object 
                setValAlMatrixTopNControlStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetAlMatrixTopNControlStatus (INT4 i4AlMatrixTopNControlIndex,
                                 INT4 i4SetValAlMatrixTopNControlStatus)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;
    INT4                i4Result = 0;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhSetAlMatrixTopNControlStatus \n");

    switch (i4SetValAlMatrixTopNControlStatus)
    {
        case CREATE_AND_WAIT:

            pAlMatrixTopNCtrlEntry =
                Rmon2AlMatrixAddTopNCtlEntry ((UINT4)
                                              i4AlMatrixTopNControlIndex);
            if (pAlMatrixTopNCtrlEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC | RMON2_MEM_FAIL,
                           "Adding to AlMatrixTopN control "
                           "table is failed \n");
                return SNMP_FAILURE;
            }
            pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlStatus = NOT_READY;
            break;

        case ACTIVE:

            pAlMatrixTopNCtrlEntry =
                Rmon2AlMatrixGetTopNCtlEntry ((UINT4)
                                              i4AlMatrixTopNControlIndex);
            if (pAlMatrixTopNCtrlEntry == NULL)
            {
                RMON2_TRC1 (RMON2_DEBUG_TRC,
                            "Get entry of AlMatrixTopN Control table"
                            "for index : %d is failed \r\n",
                            i4AlMatrixTopNControlIndex);
                return SNMP_FAILURE;
            }
            pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlTimeRemaining =
                pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlDuration;

            pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlStatus = ACTIVE;
            pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlStartTime =
                OsixGetSysUpTime ();
            break;

        case NOT_IN_SERVICE:

            pAlMatrixTopNCtrlEntry =
                Rmon2AlMatrixGetTopNCtlEntry ((UINT4)
                                              i4AlMatrixTopNControlIndex);
            if (pAlMatrixTopNCtrlEntry == NULL)
            {
                RMON2_TRC1 (RMON2_DEBUG_TRC,
                            "Get entry of AlMatrixTopN Control table"
                            "for index : %d is failed \r\n",
                            i4AlMatrixTopNControlIndex);
                return SNMP_FAILURE;
            }
            Rmon2AlDeleteTopNEntries (pAlMatrixTopNCtrlEntry);
            pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlStatus =
                NOT_IN_SERVICE;
            break;

        case DESTROY:

            pAlMatrixTopNCtrlEntry =
                Rmon2AlMatrixGetTopNCtlEntry ((UINT4)
                                              i4AlMatrixTopNControlIndex);
            if (pAlMatrixTopNCtrlEntry == NULL)
            {
                RMON2_TRC1 (RMON2_DEBUG_TRC,
                            "Get entry of AlMatrixTopN Control table"
                            "for index : %d is failed \r\n",
                            i4AlMatrixTopNControlIndex);
                return SNMP_FAILURE;
            }
            Rmon2AlDeleteTopNEntries (pAlMatrixTopNCtrlEntry);
            i4Result = Rmon2AlMatrixDelTopNCtlEntry (pAlMatrixTopNCtrlEntry);
            if (i4Result == OSIX_FAILURE)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "RBTreeRemove Failure for AlMatrixTopN control "
                           "entry\n");
                return SNMP_FAILURE;
            }
            break;
        default:
            RMON2_TRC (RMON2_DEBUG_TRC, "Unknown option\n");
            return SNMP_FAILURE;
            break;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhSetAlMatrixTopNControlStatus \n");
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2AlMatrixTopNControlMatrixIndex
 Input       :  The Indices
                AlMatrixTopNControlIndex

                The Object 
                testValAlMatrixTopNControlMatrixIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2AlMatrixTopNControlMatrixIndex (UINT4 *pu4ErrorCode,
                                         INT4 i4AlMatrixTopNControlIndex,
                                         INT4
                                         i4TestValAlMatrixTopNControlMatrixIndex)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2AlMatrixTopNControlMatrixIndex \n");
    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4AlMatrixTopNControlIndex < RMON2_ONE) ||
        (i4AlMatrixTopNControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid AlMatrixTopNControlIndex \n");
        return SNMP_FAILURE;
    }

    if ((i4TestValAlMatrixTopNControlMatrixIndex < RMON2_ONE) ||
        (i4TestValAlMatrixTopNControlMatrixIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Error : Invalid "
                   "i4TestValAlMatrixTopNControlMatrixIndex \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetTopNCtlEntry ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry != NULL)
    {
        if (pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of NlMatrixTopN Control table is Active "
                    "for index : %d \r\n", i4AlMatrixTopNControlIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry is not exist \n");
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhTestv2AlMatrixTopNControlMatrixIndex \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2AlMatrixTopNControlRateBase
 Input       :  The Indices
                AlMatrixTopNControlIndex

                The Object 
                testValAlMatrixTopNControlRateBase
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2AlMatrixTopNControlRateBase (UINT4 *pu4ErrorCode,
                                      INT4 i4AlMatrixTopNControlIndex,
                                      INT4 i4TestValAlMatrixTopNControlRateBase)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2AlMatrixTopNControlRateBase \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4AlMatrixTopNControlIndex < RMON2_ONE) ||
        (i4AlMatrixTopNControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid AlMatrixTopNControlIndex \n");
        return SNMP_FAILURE;
    }

    if ((i4TestValAlMatrixTopNControlRateBase < ALMATRIX_TOPN_TERMINALS_PKTS) ||
        (i4TestValAlMatrixTopNControlRateBase >
         ALMATRIX_TOPN_ALL_HIGHCAPACITY_OCTETS))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Error : Invalid "
                   "i4TestValAlMatrixTopNControlRateBase \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetTopNCtlEntry ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry != NULL)
    {
        if (pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of NlMatrixTopN Control table is Active "
                    "for index : %d \r\n", i4AlMatrixTopNControlIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry is not exist \n");
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhTestv2AlMatrixTopNControlRateBase \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2AlMatrixTopNControlTimeRemaining
 Input       :  The Indices
                AlMatrixTopNControlIndex

                The Object 
                testValAlMatrixTopNControlTimeRemaining
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2AlMatrixTopNControlTimeRemaining (UINT4 *pu4ErrorCode,
                                           INT4 i4AlMatrixTopNControlIndex,
                                           INT4
                                           i4TestValAlMatrixTopNControlTimeRemaining)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2AlMatrixTopNControlTimeRemaining \n");
    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4AlMatrixTopNControlIndex < RMON2_ONE) ||
        (i4AlMatrixTopNControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid AlMatrixTopNControlIndex \n");
        return SNMP_FAILURE;
    }

    /* Testing the input for negative value alone is sufficient, because
     * RMON2 allows this configuration till the maximum value of interger 32 */
    if (i4TestValAlMatrixTopNControlTimeRemaining < RMON2_ZERO)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Error : Invalid "
                   "i4TestValAlMatrixTopNControlTimeRemaining \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetTopNCtlEntry ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry != NULL)
    {
        if (pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of NlMatrixTopN Control table is Active "
                    "for index : %d \r\n", i4AlMatrixTopNControlIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry is not exist \n");
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhTestv2AlMatrixTopNControlTimeRemaining \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2AlMatrixTopNControlRequestedSize
 Input       :  The Indices
                AlMatrixTopNControlIndex

                The Object 
                testValAlMatrixTopNControlRequestedSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2AlMatrixTopNControlRequestedSize (UINT4 *pu4ErrorCode,
                                           INT4 i4AlMatrixTopNControlIndex,
                                           INT4
                                           i4TestValAlMatrixTopNControlRequestedSize)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2AlMatrixTopNControlRequestedSize \n");
    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4AlMatrixTopNControlIndex < RMON2_ONE) ||
        (i4AlMatrixTopNControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid AlMatrixTopNControlIndex \n");
        return SNMP_FAILURE;
    }

    /* Testing the input for negative value alone is sufficient, because
     * RMON2 allows this configuration till the maximum value of interger 32 */
    if (i4TestValAlMatrixTopNControlRequestedSize < RMON2_ZERO)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Error : Invalid "
                   "i4TestValAlMatrixTopNControlRequestedSize \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetTopNCtlEntry ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry != NULL)
    {
        if (pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of NlMatrixTopN Control table is Active "
                    "for index : %d \r\n", i4AlMatrixTopNControlIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry is not exist \n");
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhTestv2AlMatrixTopNControlRequestedSize \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2AlMatrixTopNControlOwner
 Input       :  The Indices
                AlMatrixTopNControlIndex

                The Object 
                testValAlMatrixTopNControlOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2AlMatrixTopNControlOwner (UINT4 *pu4ErrorCode,
                                   INT4 i4AlMatrixTopNControlIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValAlMatrixTopNControlOwner)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhTestv2AlMatrixTopNControlOwner \n");
    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4AlMatrixTopNControlIndex < RMON2_ONE) ||
        (i4AlMatrixTopNControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid AlMatrixTopNControlIndex \n");
        return SNMP_FAILURE;
    }

    if ((pTestValAlMatrixTopNControlOwner->i4_Length < RMON2_ZERO) ||
        (pTestValAlMatrixTopNControlOwner->i4_Length > RMON2_OWNER_LEN))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "\tERROR - Wrong length for AlMatrixTopNControlOwner "
                   "object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValAlMatrixTopNControlOwner->pu1_OctetList,
                              pTestValAlMatrixTopNControlOwner->i4_Length)
        == OSIX_FAILURE)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Invalid Characters for Owner string object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetTopNCtlEntry ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry != NULL)
    {
        if (pAlMatrixTopNCtrlEntry->u4AlMatrixTopNControlStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of NlMatrixTopN Control table is Active "
                    "for index : %d \r\n", i4AlMatrixTopNControlIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry is not exist \n");
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhTestv2AlMatrixTopNControlOwner \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2AlMatrixTopNControlStatus
 Input       :  The Indices
                AlMatrixTopNControlIndex

                The Object 
                testValAlMatrixTopNControlStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2AlMatrixTopNControlStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4AlMatrixTopNControlIndex,
                                    INT4 i4TestValAlMatrixTopNControlStatus)
{
    tAlMatrixTopNControl *pAlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2AlMatrixTopNControlStatus \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4AlMatrixTopNControlIndex < RMON2_ONE) ||
        (i4AlMatrixTopNControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid AlMatrixTopNControlIndex \n");
        return SNMP_FAILURE;
    }

    if ((i4TestValAlMatrixTopNControlStatus < ACTIVE) ||
        (i4TestValAlMatrixTopNControlStatus > DESTROY))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Error : Invalid "
                   "i4TestValAlMatrixTopNControlStatus \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pAlMatrixTopNCtrlEntry =
        Rmon2AlMatrixGetTopNCtlEntry ((UINT4) i4AlMatrixTopNControlIndex);

    if (pAlMatrixTopNCtrlEntry == NULL)
    {
        if (i4TestValAlMatrixTopNControlStatus == CREATE_AND_WAIT)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "SNMP Failure: Entry is not exist \n");
        return SNMP_FAILURE;
    }
    else
    {
        if ((i4TestValAlMatrixTopNControlStatus == ACTIVE) ||
            (i4TestValAlMatrixTopNControlStatus == NOT_IN_SERVICE) ||
            (i4TestValAlMatrixTopNControlStatus == DESTROY))
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "The given entry is already exist \n");
    }

    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhTestv2AlMatrixTopNControlStatus \n");
    return SNMP_FAILURE;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2AlMatrixTopNControlTable
 Input       :  The Indices
                AlMatrixTopNControlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2AlMatrixTopNControlTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : AlMatrixTopNTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceAlMatrixTopNTable
 Input       :  The Indices
                AlMatrixTopNControlIndex
                AlMatrixTopNIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceAlMatrixTopNTable (INT4 i4AlMatrixTopNControlIndex,
                                           INT4 i4AlMatrixTopNIndex)
{
    tAlMatrixTopN      *pAlMatrixTopNEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "nmhValidateIndexInstanceAlMatrixTopNTable \n");
    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4AlMatrixTopNControlIndex < RMON2_ONE) ||
        (i4AlMatrixTopNControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid AlMatrixTopNControlIndex \n");
        return SNMP_FAILURE;
    }

    if ((i4AlMatrixTopNIndex < RMON2_ONE) ||
        (i4AlMatrixTopNIndex > RMON2_MAX_MATRIX_TOPN_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid AlMatrixTopNIndex \n");
        return SNMP_FAILURE;
    }

    pAlMatrixTopNEntry =
        Rmon2AlMatrixGetTopNEntry ((UINT4) i4AlMatrixTopNControlIndex,
                                   (UINT4) i4AlMatrixTopNIndex);

    if (pAlMatrixTopNEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of AlMatrixTopN table is failed \r\n");
        return SNMP_FAILURE;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "nmhValidateIndexInstanceAlMatrixTopNTable \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexAlMatrixTopNTable
 Input       :  The Indices
                AlMatrixTopNControlIndex
                AlMatrixTopNIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexAlMatrixTopNTable (INT4 *pi4AlMatrixTopNControlIndex,
                                   INT4 *pi4AlMatrixTopNIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetFirstIndexAlMatrixTopNTable \n");

    if ((nmhGetNextIndexAlMatrixTopNTable (RMON2_ZERO,
                                           pi4AlMatrixTopNControlIndex,
                                           RMON2_ZERO,
                                           pi4AlMatrixTopNIndex)) !=
        SNMP_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Failed in Get first of AlMatrixTopN table \r\n");
        return SNMP_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetFirstIndexAlMatrixTopNTable \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexAlMatrixTopNTable
 Input       :  The Indices
                AlMatrixTopNControlIndex
                nextAlMatrixTopNControlIndex
                AlMatrixTopNIndex
                nextAlMatrixTopNIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexAlMatrixTopNTable (INT4 i4AlMatrixTopNControlIndex,
                                  INT4 *pi4NextAlMatrixTopNControlIndex,
                                  INT4 i4AlMatrixTopNIndex,
                                  INT4 *pi4NextAlMatrixTopNIndex)
{
    tAlMatrixTopN      *pAlMatrixTopNEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNextIndexAlMatrixTopNTable \n");

    pAlMatrixTopNEntry =
        Rmon2AlMatrixGetNextTopNIndex ((UINT4) i4AlMatrixTopNControlIndex,
                                       (UINT4) i4AlMatrixTopNIndex);

    if (pAlMatrixTopNEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of AlMatrixTopN table is failed \r\n");
        return SNMP_FAILURE;
    }
    *pi4NextAlMatrixTopNControlIndex =
        (INT4) pAlMatrixTopNEntry->u4AlMatrixTopNControlIndex;
    *pi4NextAlMatrixTopNIndex = (INT4) pAlMatrixTopNEntry->u4AlMatrixTopNIndex;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNextIndexAlMatrixTopNTable \n");
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetAlMatrixTopNProtocolDirLocalIndex
 Input       :  The Indices
                AlMatrixTopNControlIndex
                AlMatrixTopNIndex

                The Object 
                retValAlMatrixTopNProtocolDirLocalIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlMatrixTopNProtocolDirLocalIndex (INT4 i4AlMatrixTopNControlIndex,
                                         INT4 i4AlMatrixTopNIndex,
                                         INT4
                                         *pi4RetValAlMatrixTopNProtocolDirLocalIndex)
{
    tAlMatrixTopN      *pAlMatrixTopNEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetAlMatrixTopNProtocolDirLocalIndex \n");

    pAlMatrixTopNEntry =
        Rmon2AlMatrixGetTopNEntry ((UINT4) i4AlMatrixTopNControlIndex,
                                   (UINT4) i4AlMatrixTopNIndex);

    if (pAlMatrixTopNEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of AlMatrixTopN table is failed \r\n");
        return SNMP_FAILURE;
    }
    *pi4RetValAlMatrixTopNProtocolDirLocalIndex =
        (INT4) pAlMatrixTopNEntry->u4AlMatrixTopNProtocolDirLocalIndex;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetAlMatrixTopNProtocolDirLocalIndex \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetAlMatrixTopNSourceAddress
 Input       :  The Indices
                AlMatrixTopNControlIndex
                AlMatrixTopNIndex

                The Object 
                retValAlMatrixTopNSourceAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlMatrixTopNSourceAddress (INT4 i4AlMatrixTopNControlIndex,
                                 INT4 i4AlMatrixTopNIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValAlMatrixTopNSourceAddress)
{
    tAlMatrixTopN      *pAlMatrixTopNEntry = NULL;
    tIpAddr             SrcAddress;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetAlMatrixTopNSourceAddress \n");

    MEMSET (&SrcAddress, 0, sizeof (tIpAddr));

    pAlMatrixTopNEntry =
        Rmon2AlMatrixGetTopNEntry ((UINT4) i4AlMatrixTopNControlIndex,
                                   (UINT4) i4AlMatrixTopNIndex);

    if (pAlMatrixTopNEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of AlMatrixTopN table is failed \r\n");
        return SNMP_FAILURE;
    }

    pRetValAlMatrixTopNSourceAddress->i4_Length =
        (INT4) pAlMatrixTopNEntry->u4AlMatrixTopNIpAddrLen;

    SrcAddress.u4_addr[0] =
        OSIX_NTOHL (pAlMatrixTopNEntry->AlMatrixTopNSourceAddress.u4_addr[0]);
    SrcAddress.u4_addr[1] =
        OSIX_NTOHL (pAlMatrixTopNEntry->AlMatrixTopNSourceAddress.u4_addr[1]);
    SrcAddress.u4_addr[2] =
        OSIX_NTOHL (pAlMatrixTopNEntry->AlMatrixTopNSourceAddress.u4_addr[2]);
    SrcAddress.u4_addr[3] =
        OSIX_NTOHL (pAlMatrixTopNEntry->AlMatrixTopNSourceAddress.u4_addr[3]);

    if (pAlMatrixTopNEntry->u4AlMatrixTopNIpAddrLen == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (pRetValAlMatrixTopNSourceAddress->pu1_OctetList,
                (UINT1 *) &SrcAddress, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (pRetValAlMatrixTopNSourceAddress->pu1_OctetList,
                (UINT1 *) &SrcAddress.u4_addr[3], RMON2_IPV4_MAX_LEN);
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetAlMatrixTopNSourceAddress \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetAlMatrixTopNDestAddress
 Input       :  The Indices
                AlMatrixTopNControlIndex
                AlMatrixTopNIndex

                The Object 
                retValAlMatrixTopNDestAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlMatrixTopNDestAddress (INT4 i4AlMatrixTopNControlIndex,
                               INT4 i4AlMatrixTopNIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValAlMatrixTopNDestAddress)
{
    tAlMatrixTopN      *pAlMatrixTopNEntry = NULL;
    tIpAddr             DestAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhGetAlMatrixTopNDestAddress \n");

    MEMSET (&DestAddress, 0, sizeof (tIpAddr));

    pAlMatrixTopNEntry =
        Rmon2AlMatrixGetTopNEntry ((UINT4) i4AlMatrixTopNControlIndex,
                                   (UINT4) i4AlMatrixTopNIndex);

    if (pAlMatrixTopNEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of AlMatrixTopN table is failed \r\n");
        return SNMP_FAILURE;
    }

    pRetValAlMatrixTopNDestAddress->i4_Length =
        (INT4) pAlMatrixTopNEntry->u4AlMatrixTopNIpAddrLen;

    DestAddress.u4_addr[0] =
        OSIX_NTOHL (pAlMatrixTopNEntry->AlMatrixTopNDestAddress.u4_addr[0]);
    DestAddress.u4_addr[1] =
        OSIX_NTOHL (pAlMatrixTopNEntry->AlMatrixTopNDestAddress.u4_addr[1]);
    DestAddress.u4_addr[2] =
        OSIX_NTOHL (pAlMatrixTopNEntry->AlMatrixTopNDestAddress.u4_addr[2]);
    DestAddress.u4_addr[3] =
        OSIX_NTOHL (pAlMatrixTopNEntry->AlMatrixTopNDestAddress.u4_addr[3]);

    if (pAlMatrixTopNEntry->u4AlMatrixTopNIpAddrLen == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (pRetValAlMatrixTopNDestAddress->pu1_OctetList,
                (UINT1 *) &DestAddress, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (pRetValAlMatrixTopNDestAddress->pu1_OctetList,
                (UINT1 *) &DestAddress.u4_addr[3], RMON2_IPV4_MAX_LEN);
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetAlMatrixTopNDestAddress \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetAlMatrixTopNAppProtocolDirLocalIndex
 Input       :  The Indices
                AlMatrixTopNControlIndex
                AlMatrixTopNIndex

                The Object 
                retValAlMatrixTopNAppProtocolDirLocalIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlMatrixTopNAppProtocolDirLocalIndex (INT4 i4AlMatrixTopNControlIndex,
                                            INT4 i4AlMatrixTopNIndex,
                                            INT4
                                            *pi4RetValAlMatrixTopNAppProtocolDirLocalIndex)
{
    tAlMatrixTopN      *pAlMatrixTopNEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetAlMatrixTopNProtocolDirLocalIndex \n");

    pAlMatrixTopNEntry =
        Rmon2AlMatrixGetTopNEntry ((UINT4) i4AlMatrixTopNControlIndex,
                                   (UINT4) i4AlMatrixTopNIndex);

    if (pAlMatrixTopNEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of AlMatrixTopN table is failed \r\n");
        return SNMP_FAILURE;
    }
    *pi4RetValAlMatrixTopNAppProtocolDirLocalIndex =
        (INT4) pAlMatrixTopNEntry->u4AlMatrixTopNAppProtocolDirLocalIndex;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetAlMatrixTopNProtocolDirLocalIndex \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetAlMatrixTopNPktRate
 Input       :  The Indices
                AlMatrixTopNControlIndex
                AlMatrixTopNIndex

                The Object 
                retValAlMatrixTopNPktRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlMatrixTopNPktRate (INT4 i4AlMatrixTopNControlIndex,
                           INT4 i4AlMatrixTopNIndex,
                           UINT4 *pu4RetValAlMatrixTopNPktRate)
{
    tAlMatrixTopN      *pAlMatrixTopNEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhGetAlMatrixTopNPktRate \n");

    pAlMatrixTopNEntry =
        Rmon2AlMatrixGetTopNEntry ((UINT4) i4AlMatrixTopNControlIndex,
                                   (UINT4) i4AlMatrixTopNIndex);

    if (pAlMatrixTopNEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of AlMatrixTopN table is failed \r\n");
        return SNMP_FAILURE;
    }
    *pu4RetValAlMatrixTopNPktRate = pAlMatrixTopNEntry->u4AlMatrixTopNPktRate;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetAlMatrixTopNPktRate \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetAlMatrixTopNReversePktRate
 Input       :  The Indices
                AlMatrixTopNControlIndex
                AlMatrixTopNIndex

                The Object 
                retValAlMatrixTopNReversePktRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlMatrixTopNReversePktRate (INT4 i4AlMatrixTopNControlIndex,
                                  INT4 i4AlMatrixTopNIndex,
                                  UINT4 *pu4RetValAlMatrixTopNReversePktRate)
{
    tAlMatrixTopN      *pAlMatrixTopNEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetAlMatrixTopNReversePktRate \n");

    pAlMatrixTopNEntry =
        Rmon2AlMatrixGetTopNEntry ((UINT4) i4AlMatrixTopNControlIndex,
                                   (UINT4) i4AlMatrixTopNIndex);

    if (pAlMatrixTopNEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of AlMatrixTopN table is failed \r\n");
        return SNMP_FAILURE;
    }
    *pu4RetValAlMatrixTopNReversePktRate =
        pAlMatrixTopNEntry->u4AlMatrixTopNReversePktRate;

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetAlMatrixTopNReversePktRate \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetAlMatrixTopNOctetRate
 Input       :  The Indices
                AlMatrixTopNControlIndex
                AlMatrixTopNIndex

                The Object 
                retValAlMatrixTopNOctetRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlMatrixTopNOctetRate (INT4 i4AlMatrixTopNControlIndex,
                             INT4 i4AlMatrixTopNIndex,
                             UINT4 *pu4RetValAlMatrixTopNOctetRate)
{
    tAlMatrixTopN      *pAlMatrixTopNEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhGetAlMatrixTopNOctetRate \n");

    pAlMatrixTopNEntry =
        Rmon2AlMatrixGetTopNEntry ((UINT4) i4AlMatrixTopNControlIndex,
                                   (UINT4) i4AlMatrixTopNIndex);

    if (pAlMatrixTopNEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of AlMatrixTopN table is failed \r\n");
        return SNMP_FAILURE;
    }
    *pu4RetValAlMatrixTopNOctetRate =
        pAlMatrixTopNEntry->u4AlMatrixTopNOctetRate;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetAlMatrixTopNOctetRate \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetAlMatrixTopNReverseOctetRate
 Input       :  The Indices
                AlMatrixTopNControlIndex
                AlMatrixTopNIndex

                The Object 
                retValAlMatrixTopNReverseOctetRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlMatrixTopNReverseOctetRate (INT4 i4AlMatrixTopNControlIndex,
                                    INT4 i4AlMatrixTopNIndex,
                                    UINT4
                                    *pu4RetValAlMatrixTopNReverseOctetRate)
{
    tAlMatrixTopN      *pAlMatrixTopNEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetAlMatrixTopNReverseOctetRate \n");

    pAlMatrixTopNEntry =
        Rmon2AlMatrixGetTopNEntry ((UINT4) i4AlMatrixTopNControlIndex,
                                   (UINT4) i4AlMatrixTopNIndex);

    if (pAlMatrixTopNEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of AlMatrixTopN table is failed \r\n");
        return SNMP_FAILURE;
    }
    *pu4RetValAlMatrixTopNReverseOctetRate =
        (UINT4) pAlMatrixTopNEntry->u4AlMatrixTopNReverseOctetRate;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetAlMatrixTopNReverseOctetRate \n");
    return SNMP_SUCCESS;
}
