# include  "lr.h"
# include  "fssnmp.h"
# include  "stdrmolw.h"
# include  "stdrmowr.h"
# include  "stdrmodb.h"

VOID
RegisterSTDRMO ()
{
    SNMPRegisterMib (&stdrmoOID, &stdrmoEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&stdrmoOID, (const UINT1 *) "stdrmonv2");
}

VOID
UnRegisterSTDRMO ()
{
    SNMPUnRegisterMib (&stdrmoOID, &stdrmoEntry);
    SNMPDelSysorEntry (&stdrmoOID, (const UINT1 *) "stdrmonv2");
}

INT4
ProtocolDirLastChangeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetProtocolDirLastChange (&(pMultiData->u4_ULongValue)));
}

INT4
GetNextIndexProtocolDirTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexProtocolDirTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexProtocolDirTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
ProtocolDirLocalIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceProtocolDirTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetProtocolDirLocalIndex (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
ProtocolDirDescrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceProtocolDirTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetProtocolDirDescr (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->pOctetStrValue));

}

INT4
ProtocolDirTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceProtocolDirTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetProtocolDirType (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiData->pOctetStrValue));

}

INT4
ProtocolDirAddressMapConfigGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceProtocolDirTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetProtocolDirAddressMapConfig
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
ProtocolDirHostConfigGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceProtocolDirTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetProtocolDirHostConfig (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
ProtocolDirMatrixConfigGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceProtocolDirTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetProtocolDirMatrixConfig
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
ProtocolDirOwnerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceProtocolDirTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetProtocolDirOwner (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->pOctetStrValue));

}

INT4
ProtocolDirStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceProtocolDirTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetProtocolDirStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
ProtocolDirDescrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetProtocolDirDescr (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->pOctetStrValue));

}

INT4
ProtocolDirAddressMapConfigSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetProtocolDirAddressMapConfig
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
ProtocolDirHostConfigSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetProtocolDirHostConfig (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
ProtocolDirMatrixConfigSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetProtocolDirMatrixConfig
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
ProtocolDirOwnerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetProtocolDirOwner (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->pOctetStrValue));

}

INT4
ProtocolDirStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetProtocolDirStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiData->i4_SLongValue));

}

INT4
ProtocolDirDescrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2ProtocolDirDescr (pu4Error,
                                       pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->pOctetStrValue));

}

INT4
ProtocolDirAddressMapConfigTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2ProtocolDirAddressMapConfig (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[1].
                                                  pOctetStrValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
ProtocolDirHostConfigTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2ProtocolDirHostConfig (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiIndex->pIndex[1].
                                            pOctetStrValue,
                                            pMultiData->i4_SLongValue));

}

INT4
ProtocolDirMatrixConfigTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2ProtocolDirMatrixConfig (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              pOctetStrValue,
                                              pMultiIndex->pIndex[1].
                                              pOctetStrValue,
                                              pMultiData->i4_SLongValue));

}

INT4
ProtocolDirOwnerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2ProtocolDirOwner (pu4Error,
                                       pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->pOctetStrValue));

}

INT4
ProtocolDirStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2ProtocolDirStatus (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
ProtocolDirTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2ProtocolDirTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexProtocolDistControlTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexProtocolDistControlTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexProtocolDistControlTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
ProtocolDistControlDataSourceGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceProtocolDistControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetProtocolDistControlDataSource
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOidValue));

}

INT4
ProtocolDistControlDroppedFramesGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceProtocolDistControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetProtocolDistControlDroppedFrames
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
ProtocolDistControlCreateTimeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceProtocolDistControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetProtocolDistControlCreateTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
ProtocolDistControlOwnerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceProtocolDistControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetProtocolDistControlOwner
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
ProtocolDistControlStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceProtocolDistControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetProtocolDistControlStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
ProtocolDistControlDataSourceSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetProtocolDistControlDataSource
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOidValue));

}

INT4
ProtocolDistControlOwnerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetProtocolDistControlOwner
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
ProtocolDistControlStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetProtocolDistControlStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
ProtocolDistControlDataSourceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2ProtocolDistControlDataSource (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->pOidValue));

}

INT4
ProtocolDistControlOwnerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2ProtocolDistControlOwner (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->pOctetStrValue));

}

INT4
ProtocolDistControlStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2ProtocolDistControlStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
ProtocolDistControlTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2ProtocolDistControlTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexProtocolDistStatsTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexProtocolDistStatsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexProtocolDistStatsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
ProtocolDistStatsPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceProtocolDistStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetProtocolDistStatsPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
ProtocolDistStatsOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceProtocolDistStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetProtocolDistStatsOctets (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
AddressMapInsertsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetAddressMapInserts (&(pMultiData->u4_ULongValue)));
}

INT4
AddressMapDeletesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetAddressMapDeletes (&(pMultiData->u4_ULongValue)));
}

INT4
AddressMapMaxDesiredEntriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetAddressMapMaxDesiredEntries (&(pMultiData->i4_SLongValue)));
}

INT4
AddressMapMaxDesiredEntriesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetAddressMapMaxDesiredEntries (pMultiData->i4_SLongValue));
}

INT4
AddressMapMaxDesiredEntriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2AddressMapMaxDesiredEntries
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
AddressMapMaxDesiredEntriesDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2AddressMapMaxDesiredEntries
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexAddressMapControlTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexAddressMapControlTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexAddressMapControlTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
AddressMapControlDataSourceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAddressMapControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAddressMapControlDataSource
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOidValue));

}

INT4
AddressMapControlDroppedFramesGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAddressMapControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAddressMapControlDroppedFrames
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
AddressMapControlOwnerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAddressMapControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAddressMapControlOwner (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
AddressMapControlStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAddressMapControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAddressMapControlStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
AddressMapControlDataSourceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetAddressMapControlDataSource
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOidValue));

}

INT4
AddressMapControlOwnerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetAddressMapControlOwner (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
AddressMapControlStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetAddressMapControlStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
AddressMapControlDataSourceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2AddressMapControlDataSource (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->pOidValue));

}

INT4
AddressMapControlOwnerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2AddressMapControlOwner (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->pOctetStrValue));

}

INT4
AddressMapControlStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2AddressMapControlStatus (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
AddressMapControlTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2AddressMapControlTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexAddressMapTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexAddressMapTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[3].pOidValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexAddressMapTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].pOidValue,
             pNextMultiIndex->pIndex[3].pOidValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
AddressMapPhysicalAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAddressMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].pOidValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAddressMapPhysicalAddress
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].pOidValue, pMultiData->pOctetStrValue));

}

INT4
AddressMapLastChangeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAddressMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].pOidValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAddressMapLastChange (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiIndex->pIndex[2].pOctetStrValue,
                                        pMultiIndex->pIndex[3].pOidValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexHlHostControlTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexHlHostControlTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexHlHostControlTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
HlHostControlDataSourceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHlHostControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHlHostControlDataSource (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOidValue));

}

INT4
HlHostControlNlDroppedFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHlHostControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHlHostControlNlDroppedFrames
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
HlHostControlNlInsertsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHlHostControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHlHostControlNlInserts (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
HlHostControlNlDeletesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHlHostControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHlHostControlNlDeletes (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
HlHostControlNlMaxDesiredEntriesGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHlHostControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHlHostControlNlMaxDesiredEntries
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
HlHostControlAlDroppedFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHlHostControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHlHostControlAlDroppedFrames
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
HlHostControlAlInsertsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHlHostControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHlHostControlAlInserts (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
HlHostControlAlDeletesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHlHostControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHlHostControlAlDeletes (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
HlHostControlAlMaxDesiredEntriesGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHlHostControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHlHostControlAlMaxDesiredEntries
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
HlHostControlOwnerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHlHostControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHlHostControlOwner (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
HlHostControlStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHlHostControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHlHostControlStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
HlHostControlDataSourceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetHlHostControlDataSource (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOidValue));

}

INT4
HlHostControlNlMaxDesiredEntriesSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetHlHostControlNlMaxDesiredEntries
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
HlHostControlAlMaxDesiredEntriesSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetHlHostControlAlMaxDesiredEntries
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
HlHostControlOwnerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetHlHostControlOwner (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
HlHostControlStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetHlHostControlStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
HlHostControlDataSourceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2HlHostControlDataSource (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->pOidValue));

}

INT4
HlHostControlNlMaxDesiredEntriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2HlHostControlNlMaxDesiredEntries (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
HlHostControlAlMaxDesiredEntriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2HlHostControlAlMaxDesiredEntries (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
HlHostControlOwnerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2HlHostControlOwner (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
HlHostControlStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2HlHostControlStatus (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
HlHostControlTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2HlHostControlTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexNlHostTable (tSnmpIndex * pFirstMultiIndex,
                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexNlHostTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pNextMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexNlHostTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].pOctetStrValue,
             pNextMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
NlHostInPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlHostTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlHostInPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                pMultiIndex->pIndex[2].i4_SLongValue,
                                pMultiIndex->pIndex[3].pOctetStrValue,
                                &(pMultiData->u4_ULongValue)));

}

INT4
NlHostOutPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlHostTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlHostOutPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiIndex->pIndex[1].u4_ULongValue,
                                 pMultiIndex->pIndex[2].i4_SLongValue,
                                 pMultiIndex->pIndex[3].pOctetStrValue,
                                 &(pMultiData->u4_ULongValue)));

}

INT4
NlHostInOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlHostTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlHostInOctets (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiIndex->pIndex[2].i4_SLongValue,
                                  pMultiIndex->pIndex[3].pOctetStrValue,
                                  &(pMultiData->u4_ULongValue)));

}

INT4
NlHostOutOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlHostTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlHostOutOctets (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].i4_SLongValue,
                                   pMultiIndex->pIndex[3].pOctetStrValue,
                                   &(pMultiData->u4_ULongValue)));

}

INT4
NlHostOutMacNonUnicastPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlHostTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlHostOutMacNonUnicastPkts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
NlHostCreateTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlHostTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlHostCreateTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    pMultiIndex->pIndex[3].pOctetStrValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexHlMatrixControlTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexHlMatrixControlTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexHlMatrixControlTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
HlMatrixControlDataSourceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHlMatrixControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHlMatrixControlDataSource
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOidValue));

}

INT4
HlMatrixControlNlDroppedFramesGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHlMatrixControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHlMatrixControlNlDroppedFrames
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
HlMatrixControlNlInsertsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHlMatrixControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHlMatrixControlNlInserts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
HlMatrixControlNlDeletesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHlMatrixControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHlMatrixControlNlDeletes
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
HlMatrixControlNlMaxDesiredEntriesGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHlMatrixControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHlMatrixControlNlMaxDesiredEntries
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
HlMatrixControlAlDroppedFramesGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHlMatrixControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHlMatrixControlAlDroppedFrames
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
HlMatrixControlAlInsertsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHlMatrixControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHlMatrixControlAlInserts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
HlMatrixControlAlDeletesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHlMatrixControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHlMatrixControlAlDeletes
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
HlMatrixControlAlMaxDesiredEntriesGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHlMatrixControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHlMatrixControlAlMaxDesiredEntries
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
HlMatrixControlOwnerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHlMatrixControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHlMatrixControlOwner (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
HlMatrixControlStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHlMatrixControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHlMatrixControlStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
HlMatrixControlDataSourceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetHlMatrixControlDataSource
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOidValue));

}

INT4
HlMatrixControlNlMaxDesiredEntriesSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetHlMatrixControlNlMaxDesiredEntries
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
HlMatrixControlAlMaxDesiredEntriesSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetHlMatrixControlAlMaxDesiredEntries
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
HlMatrixControlOwnerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetHlMatrixControlOwner (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
HlMatrixControlStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetHlMatrixControlStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
HlMatrixControlDataSourceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2HlMatrixControlDataSource (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->pOidValue));

}

INT4
HlMatrixControlNlMaxDesiredEntriesTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2HlMatrixControlNlMaxDesiredEntries (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
HlMatrixControlAlMaxDesiredEntriesTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2HlMatrixControlAlMaxDesiredEntries (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
HlMatrixControlOwnerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2HlMatrixControlOwner (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
HlMatrixControlStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2HlMatrixControlStatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
HlMatrixControlTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2HlMatrixControlTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexNlMatrixSDTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexNlMatrixSDTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pNextMultiIndex->pIndex[3].pOctetStrValue,
             pNextMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexNlMatrixSDTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].pOctetStrValue,
             pNextMultiIndex->pIndex[3].pOctetStrValue,
             pFirstMultiIndex->pIndex[4].pOctetStrValue,
             pNextMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
NlMatrixSDPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixSDTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixSDPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiIndex->pIndex[2].i4_SLongValue,
                                  pMultiIndex->pIndex[3].pOctetStrValue,
                                  pMultiIndex->pIndex[4].pOctetStrValue,
                                  &(pMultiData->u4_ULongValue)));

}

INT4
NlMatrixSDOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixSDTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixSDOctets (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    pMultiIndex->pIndex[3].pOctetStrValue,
                                    pMultiIndex->pIndex[4].pOctetStrValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
NlMatrixSDCreateTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixSDTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixSDCreateTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].pOctetStrValue,
                                        pMultiIndex->pIndex[4].pOctetStrValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexNlMatrixDSTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexNlMatrixDSTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pNextMultiIndex->pIndex[3].pOctetStrValue,
             pNextMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexNlMatrixDSTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].pOctetStrValue,
             pNextMultiIndex->pIndex[3].pOctetStrValue,
             pFirstMultiIndex->pIndex[4].pOctetStrValue,
             pNextMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
NlMatrixDSPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixDSTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixDSPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiIndex->pIndex[2].i4_SLongValue,
                                  pMultiIndex->pIndex[3].pOctetStrValue,
                                  pMultiIndex->pIndex[4].pOctetStrValue,
                                  &(pMultiData->u4_ULongValue)));

}

INT4
NlMatrixDSOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixDSTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixDSOctets (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    pMultiIndex->pIndex[3].pOctetStrValue,
                                    pMultiIndex->pIndex[4].pOctetStrValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
NlMatrixDSCreateTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixDSTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixDSCreateTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].pOctetStrValue,
                                        pMultiIndex->pIndex[4].pOctetStrValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexNlMatrixTopNControlTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexNlMatrixTopNControlTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexNlMatrixTopNControlTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
NlMatrixTopNControlMatrixIndexGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixTopNControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixTopNControlMatrixIndex
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
NlMatrixTopNControlRateBaseGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixTopNControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixTopNControlRateBase
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
NlMatrixTopNControlTimeRemainingGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixTopNControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixTopNControlTimeRemaining
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
NlMatrixTopNControlGeneratedReportsGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixTopNControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixTopNControlGeneratedReports
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
NlMatrixTopNControlDurationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixTopNControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixTopNControlDuration
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
NlMatrixTopNControlRequestedSizeGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixTopNControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixTopNControlRequestedSize
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
NlMatrixTopNControlGrantedSizeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixTopNControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixTopNControlGrantedSize
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
NlMatrixTopNControlStartTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixTopNControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixTopNControlStartTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
NlMatrixTopNControlOwnerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixTopNControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixTopNControlOwner
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
NlMatrixTopNControlStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixTopNControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixTopNControlStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
NlMatrixTopNControlMatrixIndexSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetNlMatrixTopNControlMatrixIndex
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
NlMatrixTopNControlRateBaseSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetNlMatrixTopNControlRateBase
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
NlMatrixTopNControlTimeRemainingSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetNlMatrixTopNControlTimeRemaining
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
NlMatrixTopNControlRequestedSizeSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetNlMatrixTopNControlRequestedSize
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
NlMatrixTopNControlOwnerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetNlMatrixTopNControlOwner
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
NlMatrixTopNControlStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetNlMatrixTopNControlStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
NlMatrixTopNControlMatrixIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2NlMatrixTopNControlMatrixIndex (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
NlMatrixTopNControlRateBaseTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2NlMatrixTopNControlRateBase (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
NlMatrixTopNControlTimeRemainingTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2NlMatrixTopNControlTimeRemaining (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
NlMatrixTopNControlRequestedSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2NlMatrixTopNControlRequestedSize (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
NlMatrixTopNControlOwnerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2NlMatrixTopNControlOwner (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->pOctetStrValue));

}

INT4
NlMatrixTopNControlStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2NlMatrixTopNControlStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
NlMatrixTopNControlTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2NlMatrixTopNControlTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexNlMatrixTopNTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexNlMatrixTopNTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexNlMatrixTopNTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
NlMatrixTopNProtocolDirLocalIndexGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixTopNTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixTopNProtocolDirLocalIndex
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
NlMatrixTopNSourceAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixTopNTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixTopNSourceAddress
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
NlMatrixTopNDestAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixTopNTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixTopNDestAddress (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
NlMatrixTopNPktRateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixTopNTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixTopNPktRate (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
NlMatrixTopNReversePktRateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixTopNTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixTopNReversePktRate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
NlMatrixTopNOctetRateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixTopNTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixTopNOctetRate (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
NlMatrixTopNReverseOctetRateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNlMatrixTopNTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNlMatrixTopNReverseOctetRate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexAlHostTable (tSnmpIndex * pFirstMultiIndex,
                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexAlHostTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pNextMultiIndex->pIndex[3].pOctetStrValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexAlHostTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].pOctetStrValue,
             pNextMultiIndex->pIndex[3].pOctetStrValue,
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
AlHostInPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlHostTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlHostInPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                pMultiIndex->pIndex[2].i4_SLongValue,
                                pMultiIndex->pIndex[3].pOctetStrValue,
                                pMultiIndex->pIndex[4].i4_SLongValue,
                                &(pMultiData->u4_ULongValue)));

}

INT4
AlHostOutPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlHostTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlHostOutPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiIndex->pIndex[1].u4_ULongValue,
                                 pMultiIndex->pIndex[2].i4_SLongValue,
                                 pMultiIndex->pIndex[3].pOctetStrValue,
                                 pMultiIndex->pIndex[4].i4_SLongValue,
                                 &(pMultiData->u4_ULongValue)));

}

INT4
AlHostInOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlHostTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlHostInOctets (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiIndex->pIndex[2].i4_SLongValue,
                                  pMultiIndex->pIndex[3].pOctetStrValue,
                                  pMultiIndex->pIndex[4].i4_SLongValue,
                                  &(pMultiData->u4_ULongValue)));

}

INT4
AlHostOutOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlHostTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlHostOutOctets (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].i4_SLongValue,
                                   pMultiIndex->pIndex[3].pOctetStrValue,
                                   pMultiIndex->pIndex[4].i4_SLongValue,
                                   &(pMultiData->u4_ULongValue)));

}

INT4
AlHostCreateTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlHostTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlHostCreateTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    pMultiIndex->pIndex[3].pOctetStrValue,
                                    pMultiIndex->pIndex[4].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexAlMatrixSDTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexAlMatrixSDTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pNextMultiIndex->pIndex[3].pOctetStrValue,
             pNextMultiIndex->pIndex[4].pOctetStrValue,
             &(pNextMultiIndex->pIndex[5].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexAlMatrixSDTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].pOctetStrValue,
             pNextMultiIndex->pIndex[3].pOctetStrValue,
             pFirstMultiIndex->pIndex[4].pOctetStrValue,
             pNextMultiIndex->pIndex[4].pOctetStrValue,
             pFirstMultiIndex->pIndex[5].i4_SLongValue,
             &(pNextMultiIndex->pIndex[5].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
AlMatrixSDPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixSDTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].pOctetStrValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixSDPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiIndex->pIndex[2].i4_SLongValue,
                                  pMultiIndex->pIndex[3].pOctetStrValue,
                                  pMultiIndex->pIndex[4].pOctetStrValue,
                                  pMultiIndex->pIndex[5].i4_SLongValue,
                                  &(pMultiData->u4_ULongValue)));

}

INT4
AlMatrixSDOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixSDTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].pOctetStrValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixSDOctets (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    pMultiIndex->pIndex[3].pOctetStrValue,
                                    pMultiIndex->pIndex[4].pOctetStrValue,
                                    pMultiIndex->pIndex[5].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
AlMatrixSDCreateTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixSDTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].pOctetStrValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixSDCreateTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].pOctetStrValue,
                                        pMultiIndex->pIndex[4].pOctetStrValue,
                                        pMultiIndex->pIndex[5].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexAlMatrixDSTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexAlMatrixDSTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pNextMultiIndex->pIndex[3].pOctetStrValue,
             pNextMultiIndex->pIndex[4].pOctetStrValue,
             &(pNextMultiIndex->pIndex[5].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexAlMatrixDSTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].pOctetStrValue,
             pNextMultiIndex->pIndex[3].pOctetStrValue,
             pFirstMultiIndex->pIndex[4].pOctetStrValue,
             pNextMultiIndex->pIndex[4].pOctetStrValue,
             pFirstMultiIndex->pIndex[5].i4_SLongValue,
             &(pNextMultiIndex->pIndex[5].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
AlMatrixDSPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixDSTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].pOctetStrValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixDSPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiIndex->pIndex[2].i4_SLongValue,
                                  pMultiIndex->pIndex[3].pOctetStrValue,
                                  pMultiIndex->pIndex[4].pOctetStrValue,
                                  pMultiIndex->pIndex[5].i4_SLongValue,
                                  &(pMultiData->u4_ULongValue)));

}

INT4
AlMatrixDSOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixDSTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].pOctetStrValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixDSOctets (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    pMultiIndex->pIndex[3].pOctetStrValue,
                                    pMultiIndex->pIndex[4].pOctetStrValue,
                                    pMultiIndex->pIndex[5].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
AlMatrixDSCreateTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixDSTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].pOctetStrValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixDSCreateTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].pOctetStrValue,
                                        pMultiIndex->pIndex[4].pOctetStrValue,
                                        pMultiIndex->pIndex[5].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexAlMatrixTopNControlTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexAlMatrixTopNControlTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexAlMatrixTopNControlTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
AlMatrixTopNControlMatrixIndexGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixTopNControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixTopNControlMatrixIndex
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
AlMatrixTopNControlRateBaseGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixTopNControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixTopNControlRateBase
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
AlMatrixTopNControlTimeRemainingGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixTopNControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixTopNControlTimeRemaining
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
AlMatrixTopNControlGeneratedReportsGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixTopNControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixTopNControlGeneratedReports
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
AlMatrixTopNControlDurationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixTopNControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixTopNControlDuration
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
AlMatrixTopNControlRequestedSizeGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixTopNControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixTopNControlRequestedSize
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
AlMatrixTopNControlGrantedSizeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixTopNControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixTopNControlGrantedSize
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
AlMatrixTopNControlStartTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixTopNControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixTopNControlStartTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
AlMatrixTopNControlOwnerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixTopNControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixTopNControlOwner
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
AlMatrixTopNControlStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixTopNControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixTopNControlStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
AlMatrixTopNControlMatrixIndexSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetAlMatrixTopNControlMatrixIndex
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
AlMatrixTopNControlRateBaseSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetAlMatrixTopNControlRateBase
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
AlMatrixTopNControlTimeRemainingSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetAlMatrixTopNControlTimeRemaining
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
AlMatrixTopNControlRequestedSizeSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetAlMatrixTopNControlRequestedSize
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
AlMatrixTopNControlOwnerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetAlMatrixTopNControlOwner
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
AlMatrixTopNControlStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetAlMatrixTopNControlStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
AlMatrixTopNControlMatrixIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2AlMatrixTopNControlMatrixIndex (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
AlMatrixTopNControlRateBaseTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2AlMatrixTopNControlRateBase (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
AlMatrixTopNControlTimeRemainingTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2AlMatrixTopNControlTimeRemaining (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
AlMatrixTopNControlRequestedSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2AlMatrixTopNControlRequestedSize (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
AlMatrixTopNControlOwnerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2AlMatrixTopNControlOwner (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->pOctetStrValue));

}

INT4
AlMatrixTopNControlStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2AlMatrixTopNControlStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
AlMatrixTopNControlTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2AlMatrixTopNControlTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexAlMatrixTopNTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexAlMatrixTopNTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexAlMatrixTopNTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
AlMatrixTopNProtocolDirLocalIndexGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixTopNTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixTopNProtocolDirLocalIndex
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
AlMatrixTopNSourceAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixTopNTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixTopNSourceAddress
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
AlMatrixTopNDestAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixTopNTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixTopNDestAddress (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
AlMatrixTopNAppProtocolDirLocalIndexGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixTopNTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixTopNAppProtocolDirLocalIndex
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
AlMatrixTopNPktRateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixTopNTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixTopNPktRate (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
AlMatrixTopNReversePktRateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixTopNTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixTopNReversePktRate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
AlMatrixTopNOctetRateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixTopNTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixTopNOctetRate (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
AlMatrixTopNReverseOctetRateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceAlMatrixTopNTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetAlMatrixTopNReverseOctetRate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexUsrHistoryControlTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexUsrHistoryControlTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexUsrHistoryControlTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
UsrHistoryControlObjectsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsrHistoryControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsrHistoryControlObjects
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
UsrHistoryControlBucketsRequestedGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsrHistoryControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsrHistoryControlBucketsRequested
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
UsrHistoryControlBucketsGrantedGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsrHistoryControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsrHistoryControlBucketsGranted
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
UsrHistoryControlIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsrHistoryControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsrHistoryControlInterval
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
UsrHistoryControlOwnerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsrHistoryControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsrHistoryControlOwner (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
UsrHistoryControlStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsrHistoryControlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsrHistoryControlStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
UsrHistoryControlObjectsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetUsrHistoryControlObjects
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
UsrHistoryControlBucketsRequestedSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetUsrHistoryControlBucketsRequested
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
UsrHistoryControlIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetUsrHistoryControlInterval
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
UsrHistoryControlOwnerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetUsrHistoryControlOwner (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
UsrHistoryControlStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetUsrHistoryControlStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
UsrHistoryControlObjectsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2UsrHistoryControlObjects (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
UsrHistoryControlBucketsRequestedTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2UsrHistoryControlBucketsRequested (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
UsrHistoryControlIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2UsrHistoryControlInterval (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
UsrHistoryControlOwnerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2UsrHistoryControlOwner (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->pOctetStrValue));

}

INT4
UsrHistoryControlStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2UsrHistoryControlStatus (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
UsrHistoryControlTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2UsrHistoryControlTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexUsrHistoryObjectTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexUsrHistoryObjectTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexUsrHistoryObjectTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
UsrHistoryObjectVariableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsrHistoryObjectTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsrHistoryObjectVariable
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->pOidValue));

}

INT4
UsrHistoryObjectSampleTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsrHistoryObjectTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsrHistoryObjectSampleType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
UsrHistoryObjectVariableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetUsrHistoryObjectVariable
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->pOidValue));

}

INT4
UsrHistoryObjectSampleTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetUsrHistoryObjectSampleType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
UsrHistoryObjectVariableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2UsrHistoryObjectVariable (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiData->pOidValue));

}

INT4
UsrHistoryObjectSampleTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2UsrHistoryObjectSampleType (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
UsrHistoryObjectTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2UsrHistoryObjectTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexUsrHistoryTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexUsrHistoryTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexUsrHistoryTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
UsrHistoryIntervalStartGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsrHistoryTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsrHistoryIntervalStart (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
UsrHistoryIntervalEndGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsrHistoryTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsrHistoryIntervalEnd (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
UsrHistoryAbsValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsrHistoryTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsrHistoryAbsValue (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
UsrHistoryValStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsrHistoryTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsrHistoryValStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
ProbeCapabilitiesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetProbeCapabilities (pMultiData->pOctetStrValue));
}

INT4
ProbeSoftwareRevGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetProbeSoftwareRev (pMultiData->pOctetStrValue));
}

INT4
ProbeHardwareRevGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetProbeHardwareRev (pMultiData->pOctetStrValue));
}

INT4
ProbeDateTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetProbeDateTime (pMultiData->pOctetStrValue));
}

INT4
ProbeResetControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetProbeResetControl (&(pMultiData->i4_SLongValue)));
}

INT4
ProbeDownloadFileGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetProbeDownloadFile (pMultiData->pOctetStrValue));
}

INT4
ProbeDownloadTFTPServerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetProbeDownloadTFTPServer (&(pMultiData->u4_ULongValue)));
}

INT4
ProbeDownloadActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetProbeDownloadAction (&(pMultiData->i4_SLongValue)));
}

INT4
ProbeDownloadStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetProbeDownloadStatus (&(pMultiData->i4_SLongValue)));
}

INT4
ProbeDateTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetProbeDateTime (pMultiData->pOctetStrValue));
}

INT4
ProbeResetControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetProbeResetControl (pMultiData->i4_SLongValue));
}

INT4
ProbeDownloadFileSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetProbeDownloadFile (pMultiData->pOctetStrValue));
}

INT4
ProbeDownloadTFTPServerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetProbeDownloadTFTPServer (pMultiData->u4_ULongValue));
}

INT4
ProbeDownloadActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetProbeDownloadAction (pMultiData->i4_SLongValue));
}

INT4
ProbeDateTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2ProbeDateTime (pu4Error, pMultiData->pOctetStrValue));
}

INT4
ProbeResetControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2ProbeResetControl (pu4Error, pMultiData->i4_SLongValue));
}

INT4
ProbeDownloadFileTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2ProbeDownloadFile (pu4Error, pMultiData->pOctetStrValue));
}

INT4
ProbeDownloadTFTPServerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2ProbeDownloadTFTPServer
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
ProbeDownloadActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2ProbeDownloadAction (pu4Error, pMultiData->i4_SLongValue));
}

INT4
ProbeDateTimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2ProbeDateTime (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
ProbeResetControlDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2ProbeResetControl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
ProbeDownloadFileDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2ProbeDownloadFile
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
ProbeDownloadTFTPServerDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2ProbeDownloadTFTPServer
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
ProbeDownloadActionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2ProbeDownloadAction
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexSerialConfigTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexSerialConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexSerialConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
SerialModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSerialConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSerialMode (pMultiIndex->pIndex[0].i4_SLongValue,
                              &(pMultiData->i4_SLongValue)));

}

INT4
SerialProtocolGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSerialConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSerialProtocol (pMultiIndex->pIndex[0].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
SerialTimeoutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSerialConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSerialTimeout (pMultiIndex->pIndex[0].i4_SLongValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
SerialModemInitStringGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSerialConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSerialModemInitString (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
SerialModemHangUpStringGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSerialConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSerialModemHangUpString (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
SerialModemConnectRespGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSerialConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSerialModemConnectResp (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
SerialModemNoConnectRespGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSerialConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSerialModemNoConnectResp
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
SerialDialoutTimeoutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSerialConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSerialDialoutTimeout (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
SerialStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSerialConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSerialStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                &(pMultiData->i4_SLongValue)));

}

INT4
SerialModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSerialMode (pMultiIndex->pIndex[0].i4_SLongValue,
                              pMultiData->i4_SLongValue));

}

INT4
SerialProtocolSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSerialProtocol (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
SerialTimeoutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSerialTimeout (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiData->i4_SLongValue));

}

INT4
SerialModemInitStringSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSerialModemInitString (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
SerialModemHangUpStringSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSerialModemHangUpString (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
SerialModemConnectRespSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSerialModemConnectResp (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
SerialModemNoConnectRespSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSerialModemNoConnectResp
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
SerialDialoutTimeoutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSerialDialoutTimeout (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
SerialStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSerialStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                pMultiData->i4_SLongValue));

}

INT4
SerialModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2SerialMode (pu4Error,
                                 pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiData->i4_SLongValue));

}

INT4
SerialProtocolTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2SerialProtocol (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
SerialTimeoutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2SerialTimeout (pu4Error,
                                    pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
SerialModemInitStringTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2SerialModemInitString (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
SerialModemHangUpStringTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2SerialModemHangUpString (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->pOctetStrValue));

}

INT4
SerialModemConnectRespTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2SerialModemConnectResp (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->pOctetStrValue));

}

INT4
SerialModemNoConnectRespTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2SerialModemNoConnectResp (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->pOctetStrValue));

}

INT4
SerialDialoutTimeoutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2SerialDialoutTimeout (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
SerialStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    return (nmhTestv2SerialStatus (pu4Error,
                                   pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
SerialConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SerialConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexNetConfigTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexNetConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexNetConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
NetConfigIPAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNetConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNetConfigIPAddress (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
NetConfigSubnetMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNetConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNetConfigSubnetMask (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
NetConfigStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceNetConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetNetConfigStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
NetConfigIPAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetNetConfigIPAddress (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->u4_ULongValue));

}

INT4
NetConfigSubnetMaskSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetNetConfigSubnetMask (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
NetConfigStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetNetConfigStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
NetConfigIPAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2NetConfigIPAddress (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
NetConfigSubnetMaskTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2NetConfigSubnetMask (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
NetConfigStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2NetConfigStatus (pu4Error,
                                      pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
NetConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2NetConfigTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
NetDefaultGatewayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetNetDefaultGateway (&(pMultiData->u4_ULongValue)));
}

INT4
NetDefaultGatewaySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetNetDefaultGateway (pMultiData->u4_ULongValue));
}

INT4
NetDefaultGatewayTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2NetDefaultGateway (pu4Error, pMultiData->u4_ULongValue));
}

INT4
NetDefaultGatewayDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2NetDefaultGateway
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexTrapDestTable (tSnmpIndex * pFirstMultiIndex,
                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexTrapDestTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexTrapDestTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
TrapDestCommunityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceTrapDestTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetTrapDestCommunity (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->pOctetStrValue));

}

INT4
TrapDestProtocolGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceTrapDestTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetTrapDestProtocol (pMultiIndex->pIndex[0].i4_SLongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
TrapDestAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceTrapDestTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetTrapDestAddress (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiData->pOctetStrValue));

}

INT4
TrapDestOwnerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceTrapDestTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetTrapDestOwner (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiData->pOctetStrValue));

}

INT4
TrapDestStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceTrapDestTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetTrapDestStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
TrapDestCommunitySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetTrapDestCommunity (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->pOctetStrValue));

}

INT4
TrapDestProtocolSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetTrapDestProtocol (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
TrapDestAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetTrapDestAddress (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiData->pOctetStrValue));

}

INT4
TrapDestOwnerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetTrapDestOwner (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiData->pOctetStrValue));

}

INT4
TrapDestStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetTrapDestStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
TrapDestCommunityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2TrapDestCommunity (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
TrapDestProtocolTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2TrapDestProtocol (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
TrapDestAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2TrapDestAddress (pu4Error,
                                      pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
TrapDestOwnerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2TrapDestOwner (pu4Error,
                                    pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->pOctetStrValue));

}

INT4
TrapDestStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2TrapDestStatus (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
TrapDestTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2TrapDestTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexSerialConnectionTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexSerialConnectionTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexSerialConnectionTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
SerialConnectDestIpAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSerialConnectionTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSerialConnectDestIpAddress
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
SerialConnectTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSerialConnectionTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSerialConnectType (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
SerialConnectDialStringGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSerialConnectionTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSerialConnectDialString (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
SerialConnectSwitchConnectSeqGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSerialConnectionTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSerialConnectSwitchConnectSeq
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
SerialConnectSwitchDisconnectSeqGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSerialConnectionTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSerialConnectSwitchDisconnectSeq
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
SerialConnectSwitchResetSeqGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSerialConnectionTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSerialConnectSwitchResetSeq
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
SerialConnectOwnerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSerialConnectionTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSerialConnectOwner (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
SerialConnectStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSerialConnectionTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSerialConnectStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
SerialConnectDestIpAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSerialConnectDestIpAddress
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
SerialConnectTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSerialConnectType (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
SerialConnectDialStringSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSerialConnectDialString (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
SerialConnectSwitchConnectSeqSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetSerialConnectSwitchConnectSeq
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
SerialConnectSwitchDisconnectSeqSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetSerialConnectSwitchDisconnectSeq
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
SerialConnectSwitchResetSeqSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSerialConnectSwitchResetSeq
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
SerialConnectOwnerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSerialConnectOwner (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
SerialConnectStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSerialConnectStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
SerialConnectDestIpAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2SerialConnectDestIpAddress (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
SerialConnectTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2SerialConnectType (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
SerialConnectDialStringTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2SerialConnectDialString (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->pOctetStrValue));

}

INT4
SerialConnectSwitchConnectSeqTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2SerialConnectSwitchConnectSeq (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->
                                                    pOctetStrValue));

}

INT4
SerialConnectSwitchDisconnectSeqTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2SerialConnectSwitchDisconnectSeq (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       pOctetStrValue));

}

INT4
SerialConnectSwitchResetSeqTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2SerialConnectSwitchResetSeq (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->pOctetStrValue));

}

INT4
SerialConnectOwnerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2SerialConnectOwner (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
SerialConnectStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2SerialConnectStatus (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
SerialConnectionTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SerialConnectionTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexEtherStats2Table (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexEtherStats2Table
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexEtherStats2Table
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
EtherStatsDroppedFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceEtherStats2Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetEtherStatsDroppedFrames (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
EtherStatsCreateTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceEtherStats2Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetEtherStatsCreateTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexHistoryControl2Table (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexHistoryControl2Table
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexHistoryControl2Table
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
HistoryControlDroppedFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHistoryControl2Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHistoryControlDroppedFrames
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexHostControl2Table (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexHostControl2Table
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexHostControl2Table
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
HostControlDroppedFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHostControl2Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHostControlDroppedFrames
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
HostControlCreateTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceHostControl2Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetHostControlCreateTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexMatrixControl2Table (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMatrixControl2Table
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMatrixControl2Table
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MatrixControlDroppedFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMatrixControl2Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMatrixControlDroppedFrames
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MatrixControlCreateTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMatrixControl2Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMatrixControlCreateTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexChannel2Table (tSnmpIndex * pFirstMultiIndex,
                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexChannel2Table
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexChannel2Table
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
ChannelDroppedFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceChannel2Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetChannelDroppedFrames (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
ChannelCreateTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceChannel2Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetChannelCreateTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexTokenRingMLStats2Table (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexTokenRingMLStats2Table
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexTokenRingMLStats2Table
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
TokenRingMLStatsDroppedFramesGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceTokenRingMLStats2Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetTokenRingMLStatsDroppedFrames
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
TokenRingMLStatsCreateTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceTokenRingMLStats2Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetTokenRingMLStatsCreateTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexTokenRingPStats2Table (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexTokenRingPStats2Table
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexTokenRingPStats2Table
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
TokenRingPStatsDroppedFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceTokenRingPStats2Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetTokenRingPStatsDroppedFrames
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
TokenRingPStatsCreateTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceTokenRingPStats2Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetTokenRingPStatsCreateTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexRingStationControl2Table (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexRingStationControl2Table
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexRingStationControl2Table
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
RingStationControlDroppedFramesGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceRingStationControl2Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRingStationControlDroppedFrames
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
RingStationControlCreateTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceRingStationControl2Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRingStationControlCreateTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexSourceRoutingStats2Table (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexSourceRoutingStats2Table
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexSourceRoutingStats2Table
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
SourceRoutingStatsDroppedFramesGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSourceRoutingStats2Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSourceRoutingStatsDroppedFrames
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
SourceRoutingStatsCreateTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSourceRoutingStats2Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSourceRoutingStatsCreateTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexFilter2Table (tSnmpIndex * pFirstMultiIndex,
                          tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFilter2Table
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFilter2Table
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FilterProtocolDirDataLocalIndexGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFilter2Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFilterProtocolDirDataLocalIndex
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FilterProtocolDirLocalIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFilter2Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFilterProtocolDirLocalIndex
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FilterProtocolDirDataLocalIndexSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFilterProtocolDirDataLocalIndex
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FilterProtocolDirLocalIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFilterProtocolDirLocalIndex
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FilterProtocolDirDataLocalIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FilterProtocolDirDataLocalIndex (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
FilterProtocolDirLocalIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FilterProtocolDirLocalIndex (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Filter2TableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Filter2Table (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
