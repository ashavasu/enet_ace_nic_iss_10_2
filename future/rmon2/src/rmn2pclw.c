/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * $Id: rmn2pclw.c,v 1.12 2015/11/17 06:51:41 siva Exp $
 *
 * Description: This file contains the low level nmh routines for
 *              RMON2 Probe Configuration module.            
 *
 *******************************************************************/
#include "rmn2inc.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetProbeCapabilities
 Input       :  The Indices

                The Object 
                retValProbeCapabilities
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProbeCapabilities (tSNMP_OCTET_STRING_TYPE * pRetValProbeCapabilities)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetProbeCapabilities \n");
    pRetValProbeCapabilities->i4_Length =
        sizeof (gRmon2Globals.u4ProbeCapabilities);

    pRetValProbeCapabilities->pu1_OctetList[RMON2_GET_PROBE_CAPAB_BYTE1] =
        (UINT1) ((gRmon2Globals.u4ProbeCapabilities >> 24) & 0x000000ff);
    pRetValProbeCapabilities->pu1_OctetList[RMON2_GET_PROBE_CAPAB_BYTE2] =
        (UINT1) ((gRmon2Globals.u4ProbeCapabilities >> 16) & 0x000000ff);
    pRetValProbeCapabilities->pu1_OctetList[RMON2_GET_PROBE_CAPAB_BYTE3] =
        (UINT1) ((gRmon2Globals.u4ProbeCapabilities >> 8) & 0x000000ff);
    pRetValProbeCapabilities->pu1_OctetList[RMON2_GET_PROBE_CAPAB_BYTE4] =
        (UINT1) ((gRmon2Globals.u4ProbeCapabilities) & 0x000000ff);

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetProbeCapabilities \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetProbeSoftwareRev
 Input       :  The Indices

                The Object 
                retValProbeSoftwareRev
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProbeSoftwareRev (tSNMP_OCTET_STRING_TYPE * pRetValProbeSoftwareRev)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetProbeSoftwareRev \n");
    pRetValProbeSoftwareRev->i4_Length =
        (INT4) STRLEN (gRmon2Globals.au1ProbeSoftwareRev);
    MEMCPY (pRetValProbeSoftwareRev->pu1_OctetList,
            gRmon2Globals.au1ProbeSoftwareRev,
            pRetValProbeSoftwareRev->i4_Length);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetProbeSoftwareRev \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetProbeHardwareRev
 Input       :  The Indices

                The Object 
                retValProbeHardwareRev
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProbeHardwareRev (tSNMP_OCTET_STRING_TYPE * pRetValProbeHardwareRev)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetProbeHardwareRev \n");
    pRetValProbeHardwareRev->i4_Length =
        (INT4) STRLEN (gRmon2Globals.au1ProbeHardwareRev);
    MEMCPY (pRetValProbeHardwareRev->pu1_OctetList,
            gRmon2Globals.au1ProbeHardwareRev,
            pRetValProbeHardwareRev->i4_Length);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetProbeHardwareRev \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetProbeDateTime
 Input       :  The Indices

                The Object 
                retValProbeDateTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProbeDateTime (tSNMP_OCTET_STRING_TYPE * pRetValProbeDateTime)
{
    tSNMP_OCTET_STRING_TYPE ProbeDate;
    UINT1               au1Date[ISS_MAX_DATE_LEN];
    UINT1               au1DateTime[RMON2_PROBE_DATE_TIME_LEN];
    UINT4               u4Year;
    tSNMP_OCTET_STRING_TYPE ProbeDateTime;
    MEMSET (au1Date, 0, ISS_MAX_DATE_LEN);
    MEMSET (au1DateTime, 0, RMON2_PROBE_DATE_TIME_LEN);

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetProbeDateTime \n");

    ProbeDate.pu1_OctetList = &au1Date[0];
    ProbeDate.i4_Length = ISS_MAX_DATE_LEN;
    ProbeDateTime.pu1_OctetList = &au1DateTime[0];
    /* Getting Probe's DateTime version */
    nmhGetIssSwitchDate (&ProbeDate);

    ProbeDateTime.i4_Length = RMON2_PROBE_DATE_TIME_LEN;

    u4Year =
        ((((UINT4) ProbeDate.pu1_OctetList[RMON2_PROBE_GET_YEAR_BYTE1]) -
          48) * 1000) +
        ((((UINT4) ProbeDate.pu1_OctetList[RMON2_PROBE_GET_YEAR_BYTE2]) -
          48) * 100) +
        ((((UINT4) ProbeDate.pu1_OctetList[RMON2_PROBE_GET_YEAR_BYTE3]) -
          48) * 10) +
        (((UINT4) ProbeDate.pu1_OctetList[RMON2_PROBE_GET_YEAR_BYTE4]) - 48);

    ProbeDateTime.pu1_OctetList[RMON2_PROBE_GET_YEAR_OCT1] =
        (UINT1) (u4Year >> 8 & 0x00ff);
    ProbeDateTime.pu1_OctetList[RMON2_PROBE_GET_YEAR_OCT2] =
        (UINT1) u4Year & 0x00ff;

    ProbeDateTime.pu1_OctetList[RMON2_PROBE_GET_MONTH] =
        (UINT1) (((((UINT4) ProbeDate.
                    pu1_OctetList[RMON2_PROBE_GET_MONTH_BYTE1]) - 48) * 10) +
                 ((((UINT4) ProbeDate.
                    pu1_OctetList[RMON2_PROBE_GET_MONTH_BYTE2]) - 48)));
    ProbeDateTime.pu1_OctetList[RMON2_PROBE_GET_DAY] =
        (UINT1) (((((UINT4) ProbeDate.
                    pu1_OctetList[RMON2_PROBE_GET_DAY_BYTE1]) - 48) * 10) +
                 ((((UINT4) ProbeDate.
                    pu1_OctetList[RMON2_PROBE_GET_DAY_BYTE2]) - 48)));
    ProbeDateTime.pu1_OctetList[RMON2_PROBE_GET_HOUR] =
        (UINT1) (((((UINT4) ProbeDate.
                    pu1_OctetList[RMON2_PROBE_GET_HOUR_BYTE1]) - 48) * 10) +
                 ((((UINT4) ProbeDate.
                    pu1_OctetList[RMON2_PROBE_GET_HOUR_BYTE2]) - 48)));
    ProbeDateTime.pu1_OctetList[RMON2_PROBE_GET_MINUTE] =
        (UINT1) (((((UINT4) ProbeDate.
                    pu1_OctetList[RMON2_PROBE_GET_MINUTE_BYTE1]) - 48) * 10) +
                 ((((UINT4) ProbeDate.
                    pu1_OctetList[RMON2_PROBE_GET_MINUTE_BYTE2]) - 48)));
    ProbeDateTime.pu1_OctetList[RMON2_PROBE_GET_SECOND] =
        (UINT1) (((((UINT4) ProbeDate.
                    pu1_OctetList[RMON2_PROBE_GET_SECOND_BYTE1]) - 48) * 10) +
                 ((((UINT4) ProbeDate.
                    pu1_OctetList[RMON2_PROBE_GET_SECOND_BYTE2]) - 48)));
    ProbeDateTime.pu1_OctetList[RMON2_PROBE_GET_DECI_SECOND] = 0;
    ProbeDateTime.pu1_OctetList[RMON2_PROBE_GET_DIRECT_FROM_UTC] = 0;
    ProbeDateTime.pu1_OctetList[RMON2_PROBE_GET_HOURS_FROM_UTC] = 0;
    ProbeDateTime.pu1_OctetList[RMON2_PROBE_GET_MINUTES_FROM_UTC] = 0;
    pRetValProbeDateTime->i4_Length = RMON2_PROBE_DATE_TIME_LEN;
    MEMCPY (pRetValProbeDateTime->pu1_OctetList,
            ProbeDateTime.pu1_OctetList, pRetValProbeDateTime->i4_Length);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetProbeDateTime \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetProbeResetControl
 Input       :  The Indices

                The Object 
                retValProbeResetControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProbeResetControl (INT4 *pi4RetValProbeResetControl)
{
    *pi4RetValProbeResetControl = (INT4) gRmon2Globals.u4ProbeResetControl;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetProbeDownloadFile
 Input       :  The Indices

                The Object 
                retValProbeDownloadFile
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProbeDownloadFile (tSNMP_OCTET_STRING_TYPE * pRetValProbeDownloadFile)
{
    UNUSED_PARAM (pRetValProbeDownloadFile);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetProbeDownloadTFTPServer
 Input       :  The Indices

                The Object 
                retValProbeDownloadTFTPServer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProbeDownloadTFTPServer (UINT4 *pu4RetValProbeDownloadTFTPServer)
{
    UNUSED_PARAM (pu4RetValProbeDownloadTFTPServer);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetProbeDownloadAction
 Input       :  The Indices

                The Object 
                retValProbeDownloadAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProbeDownloadAction (INT4 *pi4RetValProbeDownloadAction)
{
    UNUSED_PARAM (pi4RetValProbeDownloadAction);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetProbeDownloadStatus
 Input       :  The Indices

                The Object 
                retValProbeDownloadStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProbeDownloadStatus (INT4 *pi4RetValProbeDownloadStatus)
{
    UNUSED_PARAM (pi4RetValProbeDownloadStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetProbeDateTime
 Input       :  The Indices

                The Object 
                setValProbeDateTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetProbeDateTime (tSNMP_OCTET_STRING_TYPE * pSetValProbeDateTime)
{
    tSNMP_OCTET_STRING_TYPE Date;
    UINT4               u4ErrCode;
    UINT1               au1Temp[ISS_MAX_DATE_LEN];
    UINT1               u1Hrs;
    UINT1               u1Mins;
    UINT1               u1Secs;
    UINT4               u4Day;
    UINT4               u4Month;
    UINT4               u4Year;

    MEMSET (au1Temp, RMON2_ZERO, ISS_MAX_DATE_LEN);
    Date.pu1_OctetList = &au1Temp[0];

    u1Hrs = (UINT1) (pSetValProbeDateTime->pu1_OctetList[RMON2_PROBE_SET_HOUR]);

    u1Mins = (UINT1) (pSetValProbeDateTime->pu1_OctetList[RMON2_PROBE_SET_MIN]);
    u1Secs = (UINT1) (pSetValProbeDateTime->pu1_OctetList[RMON2_PROBE_SET_SEC]);

    u4Day =
        (((UINT4) pSetValProbeDateTime->pu1_OctetList[RMON2_PROBE_SET_DAY]));

    u4Month =
        (((UINT4) pSetValProbeDateTime->pu1_OctetList[RMON2_PROBE_SET_MONTH]));

    u4Year =
        (((UINT4)
          (pSetValProbeDateTime->
           pu1_OctetList[RMON2_PROBE_SET_YEAR_BYTE1] << 8) +
          (pSetValProbeDateTime->pu1_OctetList[RMON2_PROBE_SET_YEAR_BYTE2])));

    SPRINTF ((CHR1 *) au1Temp, "%.2d:%.2d:%.2d %.2u %.2u %.4u",
             u1Hrs, u1Mins, u1Secs, u4Day, u4Month, u4Year);
    Date.i4_Length = (INT4) STRLEN (au1Temp);

    if (nmhTestv2IssSwitchDate (&u4ErrCode, &Date) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (nmhSetIssSwitchDate (&Date) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetProbeResetControl
 Input       :  The Indices

                The Object 
                setValProbeResetControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetProbeResetControl (INT4 i4SetValProbeResetControl)
{
    INT4                i4MibSaveResult = 0;
    INT4                i4RestoreStatus = 0;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhSetProbeResetControl \n");

    gRmon2Globals.u4ProbeResetControl = (UINT4) i4SetValProbeResetControl;

    if ((UINT4) i4SetValProbeResetControl == RUNNING)
    {
        /* By Default Probe is in Running State */
    }

    else if ((UINT4) i4SetValProbeResetControl == WARM_BOOT)
    {
        /* Restart the Application Software with Current configuration 
         * saved in non-volatile memory */

        nmhSetIssConfigSaveOption (ISS_CONFIG_STARTUP_SAVE);
        nmhSetIssConfigRestoreOption (ISS_CONFIG_RESTORE);

        nmhGetIssConfigSaveStatus (&i4MibSaveResult);
        if (i4MibSaveResult == MIB_SAVE_IN_PROGRESS)
        {
            RMON2_TRC (RMON2_DEBUG_TRC, "Local save already in progress\r\n");
        }
        nmhGetIssConfigRestoreStatus (&i4RestoreStatus);
        if (i4RestoreStatus == MIB_RESTORE_IN_PROGRESS)
        {
            RMON2_TRC (RMON2_DEBUG_TRC,
                       "Config restore already in progress\r\n");
        }

        /* Going to trigger MSR & MSR requires ISS_LOCK(through SNMP)
         * so Release it here. */
        Rmon2MainUnLock (); 
        ISS_UNLOCK ();

        /* Triggering MSR task */
        gu1IssUlLogFlag = UNSET_FLAG;

        if (nmhSetIssInitiateConfigSave (ISS_TRUE) != SNMP_SUCCESS)
        {
            RMON2_TRC (RMON2_DEBUG_TRC,
                       "Cannot save now. Please try after some time\r\n");
        }

        while (1)
        {
            OsixTskDelay (RMON2_ONE_SECOND * SYS_TIME_TICKS_IN_A_SEC);

            /* Get the status of Remote save operation */

            nmhGetIssConfigSaveStatus (&i4MibSaveResult);

            /* Check the progress of backup operation.
             * Break from the while loop if the operation
             * is complete or on time out */
            if ((i4MibSaveResult == LAST_MIB_SAVE_SUCCESSFUL) ||
                (i4MibSaveResult == LAST_MIB_SAVE_FAILED))
            {
                break;
            }
        }

        if (i4MibSaveResult == LAST_MIB_SAVE_SUCCESSFUL)
        {
            RMON2_TRC (RMON2_DEBUG_TRC, "Saving Current Configuration"
                       "is Successful \r\n");
#ifdef NPAPI_WANTED
            IssSystemRestart ();
#endif
        }
        ISS_LOCK ();
        Rmon2MainLock ();
    }

    else if ((UINT4) i4SetValProbeResetControl == COLD_BOOT)
    {
        /* ReInitialize Configuration parameters in non-volatile memory to 
         * default values and Restart the Application Software */

        nmhSetIssConfigSaveOption (ISS_CONFIG_NO_SAVE);
        nmhSetIssConfigRestoreOption (ISS_CONFIG_NO_RESTORE);
#ifdef NPAPI_WANTED
        IssSystemRestart ();
#endif
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhSetProbeResetControl \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetProbeDownloadFile
 Input       :  The Indices

                The Object 
                setValProbeDownloadFile
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetProbeDownloadFile (tSNMP_OCTET_STRING_TYPE * pSetValProbeDownloadFile)
{
    UNUSED_PARAM (pSetValProbeDownloadFile);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetProbeDownloadTFTPServer
 Input       :  The Indices

                The Object 
                setValProbeDownloadTFTPServer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetProbeDownloadTFTPServer (UINT4 u4SetValProbeDownloadTFTPServer)
{
    UNUSED_PARAM (u4SetValProbeDownloadTFTPServer);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetProbeDownloadAction
 Input       :  The Indices

                The Object 
                setValProbeDownloadAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetProbeDownloadAction (INT4 i4SetValProbeDownloadAction)
{
    UNUSED_PARAM (i4SetValProbeDownloadAction);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2ProbeDateTime
 Input       :  The Indices

                The Object 
                testValProbeDateTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ProbeDateTime (UINT4 *pu4ErrorCode,
                        tSNMP_OCTET_STRING_TYPE * pTestValProbeDateTime)
{

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhTestv2ProbeDateTime \n");

    if ((pTestValProbeDateTime->i4_Length < RMON2_ZERO) ||
        (pTestValProbeDateTime->i4_Length > RMON2_PROBE_DATE_TIME_LEN))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "\t ERROR - Wrong length for nmhTestv2ProbeDateTime \
                      object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhTestv2ProbeDateTime \n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2ProbeResetControl
 Input       :  The Indices

                The Object 
                testValProbeResetControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ProbeResetControl (UINT4 *pu4ErrorCode,
                            INT4 i4TestValProbeResetControl)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhTestv2ProbeResetControl \n");

    if ((i4TestValProbeResetControl < RUNNING)
        || (i4TestValProbeResetControl > COLD_BOOT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhTestv2ProbeResetControl \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2ProbeDownloadFile
 Input       :  The Indices

                The Object 
                testValProbeDownloadFile
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ProbeDownloadFile (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pTestValProbeDownloadFile)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (pTestValProbeDownloadFile);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2ProbeDownloadTFTPServer
 Input       :  The Indices

                The Object 
                testValProbeDownloadTFTPServer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ProbeDownloadTFTPServer (UINT4 *pu4ErrorCode,
                                  UINT4 u4TestValProbeDownloadTFTPServer)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (u4TestValProbeDownloadTFTPServer);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2ProbeDownloadAction
 Input       :  The Indices

                The Object 
                testValProbeDownloadAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ProbeDownloadAction (UINT4 *pu4ErrorCode,
                              INT4 i4TestValProbeDownloadAction)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4TestValProbeDownloadAction);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2ProbeDateTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2ProbeDateTime (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2ProbeResetControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2ProbeResetControl (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2ProbeDownloadFile
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2ProbeDownloadFile (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2ProbeDownloadTFTPServer
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2ProbeDownloadTFTPServer (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2ProbeDownloadAction
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2ProbeDownloadAction (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : SerialConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceSerialConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceSerialConfigTable (INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexSerialConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexSerialConfigTable (INT4 *pi4IfIndex)
{
    UNUSED_PARAM (pi4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexSerialConfigTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexSerialConfigTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4NextIfIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSerialMode
 Input       :  The Indices
                IfIndex

                The Object 
                retValSerialMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSerialMode (INT4 i4IfIndex, INT4 *pi4RetValSerialMode)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4RetValSerialMode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSerialProtocol
 Input       :  The Indices
                IfIndex

                The Object 
                retValSerialProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSerialProtocol (INT4 i4IfIndex, INT4 *pi4RetValSerialProtocol)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4RetValSerialProtocol);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSerialTimeout
 Input       :  The Indices
                IfIndex

                The Object 
                retValSerialTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSerialTimeout (INT4 i4IfIndex, INT4 *pi4RetValSerialTimeout)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4RetValSerialTimeout);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSerialModemInitString
 Input       :  The Indices
                IfIndex

                The Object 
                retValSerialModemInitString
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSerialModemInitString (INT4 i4IfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValSerialModemInitString)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pRetValSerialModemInitString);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSerialModemHangUpString
 Input       :  The Indices
                IfIndex

                The Object 
                retValSerialModemHangUpString
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSerialModemHangUpString (INT4 i4IfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValSerialModemHangUpString)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pRetValSerialModemHangUpString);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSerialModemConnectResp
 Input       :  The Indices
                IfIndex

                The Object 
                retValSerialModemConnectResp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSerialModemConnectResp (INT4 i4IfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValSerialModemConnectResp)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pRetValSerialModemConnectResp);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSerialModemNoConnectResp
 Input       :  The Indices
                IfIndex

                The Object 
                retValSerialModemNoConnectResp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSerialModemNoConnectResp (INT4 i4IfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValSerialModemNoConnectResp)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pRetValSerialModemNoConnectResp);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSerialDialoutTimeout
 Input       :  The Indices
                IfIndex

                The Object 
                retValSerialDialoutTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSerialDialoutTimeout (INT4 i4IfIndex, INT4 *pi4RetValSerialDialoutTimeout)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4RetValSerialDialoutTimeout);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSerialStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValSerialStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSerialStatus (INT4 i4IfIndex, INT4 *pi4RetValSerialStatus)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4RetValSerialStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetSerialMode
 Input       :  The Indices
                IfIndex

                The Object 
                setValSerialMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSerialMode (INT4 i4IfIndex, INT4 i4SetValSerialMode)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValSerialMode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSerialProtocol
 Input       :  The Indices
                IfIndex

                The Object 
                setValSerialProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSerialProtocol (INT4 i4IfIndex, INT4 i4SetValSerialProtocol)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValSerialProtocol);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSerialTimeout
 Input       :  The Indices
                IfIndex

                The Object 
                setValSerialTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSerialTimeout (INT4 i4IfIndex, INT4 i4SetValSerialTimeout)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValSerialTimeout);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSerialModemInitString
 Input       :  The Indices
                IfIndex

                The Object 
                setValSerialModemInitString
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSerialModemInitString (INT4 i4IfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValSerialModemInitString)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pSetValSerialModemInitString);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSerialModemHangUpString
 Input       :  The Indices
                IfIndex

                The Object 
                setValSerialModemHangUpString
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSerialModemHangUpString (INT4 i4IfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValSerialModemHangUpString)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pSetValSerialModemHangUpString);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSerialModemConnectResp
 Input       :  The Indices
                IfIndex

                The Object 
                setValSerialModemConnectResp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSerialModemConnectResp (INT4 i4IfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValSerialModemConnectResp)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pSetValSerialModemConnectResp);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSerialModemNoConnectResp
 Input       :  The Indices
                IfIndex

                The Object 
                setValSerialModemNoConnectResp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSerialModemNoConnectResp (INT4 i4IfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValSerialModemNoConnectResp)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pSetValSerialModemNoConnectResp);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSerialDialoutTimeout
 Input       :  The Indices
                IfIndex

                The Object 
                setValSerialDialoutTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSerialDialoutTimeout (INT4 i4IfIndex, INT4 i4SetValSerialDialoutTimeout)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValSerialDialoutTimeout);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSerialStatus
 Input       :  The Indices
                IfIndex

                The Object 
                setValSerialStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSerialStatus (INT4 i4IfIndex, INT4 i4SetValSerialStatus)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValSerialStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2SerialMode
 Input       :  The Indices
                IfIndex

                The Object 
                testValSerialMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SerialMode (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                     INT4 i4TestValSerialMode)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValSerialMode);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2SerialProtocol
 Input       :  The Indices
                IfIndex

                The Object 
                testValSerialProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SerialProtocol (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                         INT4 i4TestValSerialProtocol)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValSerialProtocol);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2SerialTimeout
 Input       :  The Indices
                IfIndex

                The Object 
                testValSerialTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SerialTimeout (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                        INT4 i4TestValSerialTimeout)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValSerialTimeout);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2SerialModemInitString
 Input       :  The Indices
                IfIndex

                The Object 
                testValSerialModemInitString
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SerialModemInitString (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValSerialModemInitString)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pTestValSerialModemInitString);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2SerialModemHangUpString
 Input       :  The Indices
                IfIndex

                The Object 
                testValSerialModemHangUpString
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SerialModemHangUpString (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValSerialModemHangUpString)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pTestValSerialModemHangUpString);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2SerialModemConnectResp
 Input       :  The Indices
                IfIndex

                The Object 
                testValSerialModemConnectResp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SerialModemConnectResp (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValSerialModemConnectResp)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pTestValSerialModemConnectResp);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2SerialModemNoConnectResp
 Input       :  The Indices
                IfIndex

                The Object 
                testValSerialModemNoConnectResp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SerialModemNoConnectResp (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValSerialModemNoConnectResp)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pTestValSerialModemNoConnectResp);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2SerialDialoutTimeout
 Input       :  The Indices
                IfIndex

                The Object 
                testValSerialDialoutTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SerialDialoutTimeout (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               INT4 i4TestValSerialDialoutTimeout)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValSerialDialoutTimeout);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2SerialStatus
 Input       :  The Indices
                IfIndex

                The Object 
                testValSerialStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SerialStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                       INT4 i4TestValSerialStatus)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValSerialStatus);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2SerialConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SerialConfigTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : NetConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceNetConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceNetConfigTable (INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexNetConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexNetConfigTable (INT4 *pi4IfIndex)
{
    UNUSED_PARAM (pi4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexNetConfigTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexNetConfigTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4NextIfIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetNetConfigIPAddress
 Input       :  The Indices
                IfIndex

                The Object 
                retValNetConfigIPAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNetConfigIPAddress (INT4 i4IfIndex, UINT4 *pu4RetValNetConfigIPAddress)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValNetConfigIPAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNetConfigSubnetMask
 Input       :  The Indices
                IfIndex

                The Object 
                retValNetConfigSubnetMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNetConfigSubnetMask (INT4 i4IfIndex, UINT4 *pu4RetValNetConfigSubnetMask)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValNetConfigSubnetMask);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNetConfigStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValNetConfigStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNetConfigStatus (INT4 i4IfIndex, INT4 *pi4RetValNetConfigStatus)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4RetValNetConfigStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetNetConfigIPAddress
 Input       :  The Indices
                IfIndex

                The Object 
                setValNetConfigIPAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNetConfigIPAddress (INT4 i4IfIndex, UINT4 u4SetValNetConfigIPAddress)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValNetConfigIPAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetNetConfigSubnetMask
 Input       :  The Indices
                IfIndex

                The Object 
                setValNetConfigSubnetMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNetConfigSubnetMask (INT4 i4IfIndex, UINT4 u4SetValNetConfigSubnetMask)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValNetConfigSubnetMask);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetNetConfigStatus
 Input       :  The Indices
                IfIndex

                The Object 
                setValNetConfigStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNetConfigStatus (INT4 i4IfIndex, INT4 i4SetValNetConfigStatus)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValNetConfigStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2NetConfigIPAddress
 Input       :  The Indices
                IfIndex

                The Object 
                testValNetConfigIPAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NetConfigIPAddress (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                             UINT4 u4TestValNetConfigIPAddress)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValNetConfigIPAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2NetConfigSubnetMask
 Input       :  The Indices
                IfIndex

                The Object 
                testValNetConfigSubnetMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NetConfigSubnetMask (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                              UINT4 u4TestValNetConfigSubnetMask)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValNetConfigSubnetMask);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2NetConfigStatus
 Input       :  The Indices
                IfIndex

                The Object 
                testValNetConfigStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NetConfigStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                          INT4 i4TestValNetConfigStatus)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValNetConfigStatus);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2NetConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NetConfigTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetNetDefaultGateway
 Input       :  The Indices

                The Object 
                retValNetDefaultGateway
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNetDefaultGateway (UINT4 *pu4RetValNetDefaultGateway)
{
    UNUSED_PARAM (pu4RetValNetDefaultGateway);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetNetDefaultGateway
 Input       :  The Indices

                The Object 
                setValNetDefaultGateway
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNetDefaultGateway (UINT4 u4SetValNetDefaultGateway)
{
    UNUSED_PARAM (u4SetValNetDefaultGateway);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2NetDefaultGateway
 Input       :  The Indices

                The Object 
                testValNetDefaultGateway
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NetDefaultGateway (UINT4 *pu4ErrorCode,
                            UINT4 u4TestValNetDefaultGateway)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (u4TestValNetDefaultGateway);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2NetDefaultGateway
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NetDefaultGateway (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : TrapDestTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceTrapDestTable
 Input       :  The Indices
                TrapDestIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceTrapDestTable (INT4 i4TrapDestIndex)
{
    UNUSED_PARAM (i4TrapDestIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexTrapDestTable
 Input       :  The Indices
                TrapDestIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexTrapDestTable (INT4 *pi4TrapDestIndex)
{
    UNUSED_PARAM (pi4TrapDestIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexTrapDestTable
 Input       :  The Indices
                TrapDestIndex
                nextTrapDestIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexTrapDestTable (INT4 i4TrapDestIndex, INT4 *pi4NextTrapDestIndex)
{
    UNUSED_PARAM (i4TrapDestIndex);
    UNUSED_PARAM (pi4NextTrapDestIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetTrapDestCommunity
 Input       :  The Indices
                TrapDestIndex

                The Object 
                retValTrapDestCommunity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTrapDestCommunity (INT4 i4TrapDestIndex,
                         tSNMP_OCTET_STRING_TYPE * pRetValTrapDestCommunity)
{
    UNUSED_PARAM (i4TrapDestIndex);
    UNUSED_PARAM (pRetValTrapDestCommunity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetTrapDestProtocol
 Input       :  The Indices
                TrapDestIndex

                The Object 
                retValTrapDestProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTrapDestProtocol (INT4 i4TrapDestIndex, INT4 *pi4RetValTrapDestProtocol)
{
    UNUSED_PARAM (i4TrapDestIndex);
    UNUSED_PARAM (pi4RetValTrapDestProtocol);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetTrapDestAddress
 Input       :  The Indices
                TrapDestIndex

                The Object 
                retValTrapDestAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTrapDestAddress (INT4 i4TrapDestIndex,
                       tSNMP_OCTET_STRING_TYPE * pRetValTrapDestAddress)
{
    UNUSED_PARAM (i4TrapDestIndex);
    UNUSED_PARAM (pRetValTrapDestAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetTrapDestOwner
 Input       :  The Indices
                TrapDestIndex

                The Object 
                retValTrapDestOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTrapDestOwner (INT4 i4TrapDestIndex,
                     tSNMP_OCTET_STRING_TYPE * pRetValTrapDestOwner)
{
    UNUSED_PARAM (i4TrapDestIndex);
    UNUSED_PARAM (pRetValTrapDestOwner);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetTrapDestStatus
 Input       :  The Indices
                TrapDestIndex

                The Object 
                retValTrapDestStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTrapDestStatus (INT4 i4TrapDestIndex, INT4 *pi4RetValTrapDestStatus)
{
    UNUSED_PARAM (i4TrapDestIndex);
    UNUSED_PARAM (pi4RetValTrapDestStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetTrapDestCommunity
 Input       :  The Indices
                TrapDestIndex

                The Object 
                setValTrapDestCommunity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTrapDestCommunity (INT4 i4TrapDestIndex,
                         tSNMP_OCTET_STRING_TYPE * pSetValTrapDestCommunity)
{
    UNUSED_PARAM (i4TrapDestIndex);
    UNUSED_PARAM (pSetValTrapDestCommunity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetTrapDestProtocol
 Input       :  The Indices
                TrapDestIndex

                The Object 
                setValTrapDestProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTrapDestProtocol (INT4 i4TrapDestIndex, INT4 i4SetValTrapDestProtocol)
{
    UNUSED_PARAM (i4TrapDestIndex);
    UNUSED_PARAM (i4SetValTrapDestProtocol);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetTrapDestAddress
 Input       :  The Indices
                TrapDestIndex

                The Object 
                setValTrapDestAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTrapDestAddress (INT4 i4TrapDestIndex,
                       tSNMP_OCTET_STRING_TYPE * pSetValTrapDestAddress)
{
    UNUSED_PARAM (i4TrapDestIndex);
    UNUSED_PARAM (pSetValTrapDestAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetTrapDestOwner
 Input       :  The Indices
                TrapDestIndex

                The Object 
                setValTrapDestOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTrapDestOwner (INT4 i4TrapDestIndex,
                     tSNMP_OCTET_STRING_TYPE * pSetValTrapDestOwner)
{
    UNUSED_PARAM (i4TrapDestIndex);
    UNUSED_PARAM (pSetValTrapDestOwner);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetTrapDestStatus
 Input       :  The Indices
                TrapDestIndex

                The Object 
                setValTrapDestStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTrapDestStatus (INT4 i4TrapDestIndex, INT4 i4SetValTrapDestStatus)
{
    UNUSED_PARAM (i4TrapDestIndex);
    UNUSED_PARAM (i4SetValTrapDestStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2TrapDestCommunity
 Input       :  The Indices
                TrapDestIndex

                The Object 
                testValTrapDestCommunity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TrapDestCommunity (UINT4 *pu4ErrorCode, INT4 i4TrapDestIndex,
                            tSNMP_OCTET_STRING_TYPE * pTestValTrapDestCommunity)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4TrapDestIndex);
    UNUSED_PARAM (pTestValTrapDestCommunity);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2TrapDestProtocol
 Input       :  The Indices
                TrapDestIndex

                The Object 
                testValTrapDestProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TrapDestProtocol (UINT4 *pu4ErrorCode, INT4 i4TrapDestIndex,
                           INT4 i4TestValTrapDestProtocol)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4TrapDestIndex);
    UNUSED_PARAM (i4TestValTrapDestProtocol);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2TrapDestAddress
 Input       :  The Indices
                TrapDestIndex

                The Object 
                testValTrapDestAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TrapDestAddress (UINT4 *pu4ErrorCode, INT4 i4TrapDestIndex,
                          tSNMP_OCTET_STRING_TYPE * pTestValTrapDestAddress)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4TrapDestIndex);
    UNUSED_PARAM (pTestValTrapDestAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2TrapDestOwner
 Input       :  The Indices
                TrapDestIndex

                The Object 
                testValTrapDestOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TrapDestOwner (UINT4 *pu4ErrorCode, INT4 i4TrapDestIndex,
                        tSNMP_OCTET_STRING_TYPE * pTestValTrapDestOwner)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4TrapDestIndex);
    UNUSED_PARAM (pTestValTrapDestOwner);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2TrapDestStatus
 Input       :  The Indices
                TrapDestIndex

                The Object 
                testValTrapDestStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TrapDestStatus (UINT4 *pu4ErrorCode, INT4 i4TrapDestIndex,
                         INT4 i4TestValTrapDestStatus)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4TrapDestIndex);
    UNUSED_PARAM (i4TestValTrapDestStatus);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2TrapDestTable
 Input       :  The Indices
                TrapDestIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2TrapDestTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : SerialConnectionTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceSerialConnectionTable
 Input       :  The Indices
                SerialConnectIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceSerialConnectionTable (INT4 i4SerialConnectIndex)
{
    UNUSED_PARAM (i4SerialConnectIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexSerialConnectionTable
 Input       :  The Indices
                SerialConnectIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexSerialConnectionTable (INT4 *pi4SerialConnectIndex)
{
    UNUSED_PARAM (pi4SerialConnectIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexSerialConnectionTable
 Input       :  The Indices
                SerialConnectIndex
                nextSerialConnectIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexSerialConnectionTable (INT4 i4SerialConnectIndex,
                                      INT4 *pi4NextSerialConnectIndex)
{
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (pi4NextSerialConnectIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSerialConnectDestIpAddress
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                retValSerialConnectDestIpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSerialConnectDestIpAddress (INT4 i4SerialConnectIndex,
                                  UINT4 *pu4RetValSerialConnectDestIpAddress)
{
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (pu4RetValSerialConnectDestIpAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSerialConnectType
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                retValSerialConnectType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSerialConnectType (INT4 i4SerialConnectIndex,
                         INT4 *pi4RetValSerialConnectType)
{
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (pi4RetValSerialConnectType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSerialConnectDialString
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                retValSerialConnectDialString
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSerialConnectDialString (INT4 i4SerialConnectIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValSerialConnectDialString)
{
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (pRetValSerialConnectDialString);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSerialConnectSwitchConnectSeq
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                retValSerialConnectSwitchConnectSeq
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSerialConnectSwitchConnectSeq (INT4 i4SerialConnectIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValSerialConnectSwitchConnectSeq)
{
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (pRetValSerialConnectSwitchConnectSeq);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSerialConnectSwitchDisconnectSeq
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                retValSerialConnectSwitchDisconnectSeq
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSerialConnectSwitchDisconnectSeq (INT4 i4SerialConnectIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValSerialConnectSwitchDisconnectSeq)
{
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (pRetValSerialConnectSwitchDisconnectSeq);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSerialConnectSwitchResetSeq
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                retValSerialConnectSwitchResetSeq
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSerialConnectSwitchResetSeq (INT4 i4SerialConnectIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValSerialConnectSwitchResetSeq)
{
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (pRetValSerialConnectSwitchResetSeq);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSerialConnectOwner
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                retValSerialConnectOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSerialConnectOwner (INT4 i4SerialConnectIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValSerialConnectOwner)
{
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (pRetValSerialConnectOwner);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSerialConnectStatus
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                retValSerialConnectStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSerialConnectStatus (INT4 i4SerialConnectIndex,
                           INT4 *pi4RetValSerialConnectStatus)
{
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (pi4RetValSerialConnectStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetSerialConnectDestIpAddress
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                setValSerialConnectDestIpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSerialConnectDestIpAddress (INT4 i4SerialConnectIndex,
                                  UINT4 u4SetValSerialConnectDestIpAddress)
{
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (u4SetValSerialConnectDestIpAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSerialConnectType
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                setValSerialConnectType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSerialConnectType (INT4 i4SerialConnectIndex,
                         INT4 i4SetValSerialConnectType)
{
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (i4SetValSerialConnectType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSerialConnectDialString
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                setValSerialConnectDialString
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSerialConnectDialString (INT4 i4SerialConnectIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValSerialConnectDialString)
{
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (pSetValSerialConnectDialString);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSerialConnectSwitchConnectSeq
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                setValSerialConnectSwitchConnectSeq
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSerialConnectSwitchConnectSeq (INT4 i4SerialConnectIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValSerialConnectSwitchConnectSeq)
{
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (pSetValSerialConnectSwitchConnectSeq);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSerialConnectSwitchDisconnectSeq
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                setValSerialConnectSwitchDisconnectSeq
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSerialConnectSwitchDisconnectSeq (INT4 i4SerialConnectIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pSetValSerialConnectSwitchDisconnectSeq)
{
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (pSetValSerialConnectSwitchDisconnectSeq);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSerialConnectSwitchResetSeq
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                setValSerialConnectSwitchResetSeq
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSerialConnectSwitchResetSeq (INT4 i4SerialConnectIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValSerialConnectSwitchResetSeq)
{
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (pSetValSerialConnectSwitchResetSeq);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSerialConnectOwner
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                setValSerialConnectOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSerialConnectOwner (INT4 i4SerialConnectIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValSerialConnectOwner)
{
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (pSetValSerialConnectOwner);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSerialConnectStatus
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                setValSerialConnectStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSerialConnectStatus (INT4 i4SerialConnectIndex,
                           INT4 i4SetValSerialConnectStatus)
{
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (i4SetValSerialConnectStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2SerialConnectDestIpAddress
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                testValSerialConnectDestIpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SerialConnectDestIpAddress (UINT4 *pu4ErrorCode,
                                     INT4 i4SerialConnectIndex,
                                     UINT4 u4TestValSerialConnectDestIpAddress)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (u4TestValSerialConnectDestIpAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2SerialConnectType
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                testValSerialConnectType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SerialConnectType (UINT4 *pu4ErrorCode, INT4 i4SerialConnectIndex,
                            INT4 i4TestValSerialConnectType)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (i4TestValSerialConnectType);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2SerialConnectDialString
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                testValSerialConnectDialString
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SerialConnectDialString (UINT4 *pu4ErrorCode,
                                  INT4 i4SerialConnectIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValSerialConnectDialString)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (pTestValSerialConnectDialString);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2SerialConnectSwitchConnectSeq
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                testValSerialConnectSwitchConnectSeq
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SerialConnectSwitchConnectSeq (UINT4 *pu4ErrorCode,
                                        INT4 i4SerialConnectIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValSerialConnectSwitchConnectSeq)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (pTestValSerialConnectSwitchConnectSeq);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2SerialConnectSwitchDisconnectSeq
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                testValSerialConnectSwitchDisconnectSeq
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SerialConnectSwitchDisconnectSeq (UINT4 *pu4ErrorCode,
                                           INT4 i4SerialConnectIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pTestValSerialConnectSwitchDisconnectSeq)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (pTestValSerialConnectSwitchDisconnectSeq);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2SerialConnectSwitchResetSeq
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                testValSerialConnectSwitchResetSeq
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SerialConnectSwitchResetSeq (UINT4 *pu4ErrorCode,
                                      INT4 i4SerialConnectIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValSerialConnectSwitchResetSeq)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (pTestValSerialConnectSwitchResetSeq);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2SerialConnectOwner
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                testValSerialConnectOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SerialConnectOwner (UINT4 *pu4ErrorCode, INT4 i4SerialConnectIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValSerialConnectOwner)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (pTestValSerialConnectOwner);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2SerialConnectStatus
 Input       :  The Indices
                SerialConnectIndex

                The Object 
                testValSerialConnectStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SerialConnectStatus (UINT4 *pu4ErrorCode, INT4 i4SerialConnectIndex,
                              INT4 i4TestValSerialConnectStatus)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4SerialConnectIndex);
    UNUSED_PARAM (i4TestValSerialConnectStatus);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2SerialConnectionTable
 Input       :  The Indices
                SerialConnectIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SerialConnectionTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
