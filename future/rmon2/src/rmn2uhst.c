/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 * $Id: rmn2uhst.c,v 1.16 2013/10/23 09:45:27 siva Exp $ 
 *
 * Description: This file contains the functional routine for
 *              RMON2 Usr History module
 *
 *******************************************************************/

#include "rmn2inc.h"

/*****************************************************************************/
/* Function Name      : Rmon2UsrHistGetNextCtrlIndex                         */
/*                                                                           */
/* Description        : This function is used to get next User History       */
/*                      control table entry for the index                    */
/*                      'u4UsrHistoryCtrlndex'                               */
/*                                                                           */
/* Input(s)           : u4UsrHistoryCtrlndex                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NULL / Pointer to the next user history control      */
/*                      entry.                                               */
/*****************************************************************************/
PUBLIC tUsrHistoryControl *
Rmon2UsrHistGetNextCtrlIndex (UINT4 u4UsrHistoryCtrlIndex)
{
    tUsrHistoryControl  UsrHistoryCtrlEntry;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2UsrHistGetNextCtrlIndex \n");

    MEMSET (&UsrHistoryCtrlEntry, 0, sizeof (UsrHistoryCtrlEntry));

    UsrHistoryCtrlEntry.u4UsrHistoryControlIndex = u4UsrHistoryCtrlIndex;

    return ((tUsrHistoryControl *)
            RBTreeGetNext (RMON2_USRHISTORYCTRL_TREE,
                           (tRBElem *) & (UsrHistoryCtrlEntry), NULL));
}

/*****************************************************************************/
/* Function Name      : Rmon2UsrHistAddCtrlEntry                             */
/*                                                                           */
/* Description        : This function is used to add User History            */
/*                      control table entry in the RBTree                    */
/*                                                                           */
/* Input(s)           : u4UsrHistoryCtrlIndex                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NULL / Pointer to the user history control           */
/*                      table entry.                                         */
/*****************************************************************************/

PUBLIC tUsrHistoryControl *
Rmon2UsrHistAddCtrlEntry (UINT4 u4UsrHistoryCtrlIndex)
{
    tUsrHistoryControl *pUsrHistoryCtrlNode;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2UsrHistAddCtrlEntry \n");

    pUsrHistoryCtrlNode =
        (tUsrHistoryControl *) MemAllocMemBlk (RMON2_USRHISTCTRL_POOL);

    if (pUsrHistoryCtrlNode == NULL)
    {
        /* Alloc mem block failed */
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_MEM_FAIL,
                   "MemAlloc Failure for UsrHistory control table Entry \n");
        return NULL;
    }

    MEMSET (pUsrHistoryCtrlNode, 0, sizeof (tUsrHistoryControl));

    pUsrHistoryCtrlNode->u4UsrHistoryControlIndex = u4UsrHistoryCtrlIndex;
    pUsrHistoryCtrlNode->u4UsrHistoryControlBucketsRequested =
        RMON2_USR_HISTORY_BUCKETS_REQ;

    pUsrHistoryCtrlNode->u4UsrHistoryControlBucketsGranted =
        RMON2_MAX_USR_HISTORY_BUCKETS_GRANT;

    pUsrHistoryCtrlNode->u4UsrHistoryControlInterval =
        RMON2_USR_HISTORY_INTERVAL;

    if (RBTreeAdd (RMON2_USRHISTORYCTRL_TREE, pUsrHistoryCtrlNode) ==
        RB_FAILURE)
    {
        RMON2_TRC (RMON2_MEM_FAIL,
                   "RBTreeAdd Failure for UsrHistory control table Entry \n");
        MemReleaseMemBlock (RMON2_USRHISTCTRL_POOL,
                            (UINT1 *) pUsrHistoryCtrlNode);
        return NULL;
    }
    RMON2_TRC (RMON2_DEBUG_TRC, "Rmon2UsrHistAddCtrlEntry :UsrHistAddCtrlEntry"
               " is added \n");

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2UsrHistAddCtrlEntry \n");

    return pUsrHistoryCtrlNode;
}

/*****************************************************************************/
/* Function Name      : Rmon2UsrHistDelCtrlEntry                             */
/*                                                                           */
/* Description        : This function is used to remove user History         */
/*                      control table entry from the RBTree                  */
/*                                                                           */
/* Input(s)           : User History control node                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/

PUBLIC INT4
Rmon2UsrHistDelCtrlEntry (tUsrHistoryControl * pUsrHistoryCtrlNode)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2UsrHistDelCtrlEntry \n");

    if (RBTreeRemove (RMON2_USRHISTORYCTRL_TREE, pUsrHistoryCtrlNode) ==
        RB_FAILURE)
    {
        RMON2_TRC (RMON2_MEM_FAIL,
                   "RBTreeRemove Failure for UsrHistory Control table Entry \n");
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (RMON2_USRHISTCTRL_POOL, (UINT1 *) pUsrHistoryCtrlNode);
    pUsrHistoryCtrlNode = NULL;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2UsrHistDelCtrlEntry \n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : Rmon2UsrHistGetCtrlEntry                             */
/*                                                                           */
/* Description        : This function is used to get User History            */
/*                      control table entry for the index                    */
/*                      'u4UsrHistoryCtrlIndex'                              */
/*                                                                           */
/* Input(s)           : u4UsrHistoryCtrlIndex                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NULL / Pointer to the user history control           */
/*                      entry.                                               */
/*****************************************************************************/

PUBLIC tUsrHistoryControl *
Rmon2UsrHistGetCtrlEntry (UINT4 u4UsrHistoryCtrlIndex)
{
    tUsrHistoryControl  UsrHistoryCtrlEntry;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2UsrHistGetCtrlEntry \n");

    MEMSET (&UsrHistoryCtrlEntry, 0, sizeof (UsrHistoryCtrlEntry));

    UsrHistoryCtrlEntry.u4UsrHistoryControlIndex = u4UsrHistoryCtrlIndex;

    return ((tUsrHistoryControl *)
            RBTreeGet (RMON2_USRHISTORYCTRL_TREE,
                       (tRBElem *) & (UsrHistoryCtrlEntry)));
}

/*****************************************************************************/
/* Function Name      : Rmon2UsrHistGetNextObjectIndex                       */
/*                                                                           */
/* Description        : This function is used to get next User History       */
/*                      object table entry for the index                     */
/*                      'u4UsrHistoryCtrlIndex,u4UsrHistoryObjectIndex'      */
/*                                                                           */
/* Input(s)           : u4UsrHistoryCtrlIndex,u4UsrHistoryObjectIndex        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NULL / Pointer to the next user history object       */
/*                      entry.                                               */
/*****************************************************************************/

PUBLIC tUsrHistoryObject *
Rmon2UsrHistGetNextObjectIndex (UINT4 u4UsrHistoryCtrlIndex,
                                UINT4 u4UsrHistoryObjectIndex)
{
    tUsrHistoryObject  *pUsrHistoryObjectEntry = NULL;
    tUsrHistoryObject  *pNextUsrHistoryObjectEntry = NULL;
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;
    UINT4               u4Flag = RMON2_ZERO;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2UsrHistGetNextObjectIndex \n");

    /* Get the pointer to the index 'u4UsrHistoryCtrlIndex' */
    pUsrHistoryCtrlEntry = Rmon2UsrHistGetCtrlEntry (u4UsrHistoryCtrlIndex);

    if (pUsrHistoryCtrlEntry != NULL)
    {
        if (pUsrHistoryCtrlEntry->u4UsrHistoryControlObjects == RMON2_ZERO)
        {
            return NULL;
        }
        else
        {
            if (u4UsrHistoryObjectIndex == 0)
            {
                pNextUsrHistoryObjectEntry =
                    (tUsrHistoryObject *)
                    TMO_SLL_First (&(pUsrHistoryCtrlEntry->UsrHistoryObject));
                return pNextUsrHistoryObjectEntry;
            }

            TMO_SLL_Scan (&(pUsrHistoryCtrlEntry->UsrHistoryObject),
                          pUsrHistoryObjectEntry, tUsrHistoryObject *)
            {
                if (pUsrHistoryObjectEntry->u4UsrHistoryObjectIndex ==
                    u4UsrHistoryObjectIndex)
                {
                    RMON2_TRC (RMON2_DEBUG_TRC, "Valid user history object "
                               "Index \n");
                    u4Flag = RMON2_ONE;
                    break;
                }
            }

            if (u4Flag == RMON2_ZERO)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "The given UsrHistoryObjectEntry is not"
                           " exist \n");
                return NULL;
            }

            pNextUsrHistoryObjectEntry = (tUsrHistoryObject *)
                TMO_SLL_Next (&(pUsrHistoryCtrlEntry->UsrHistoryObject),
                              &pUsrHistoryObjectEntry->nextUsrHistoryObject);

            if (pNextUsrHistoryObjectEntry != NULL)
            {
                return pNextUsrHistoryObjectEntry;
            }
            return NULL;
        }
    }                            /* end of while */

    RMON2_TRC (RMON2_DEBUG_TRC,
               "Get entry of User history control table is failed \n");
    return NULL;

}

/*****************************************************************************/
/* Function Name      : Rmon2UsrHistAddObjEntry                              */
/*                                                                           */
/* Description        : This function is used to add User History            */
/*                      object table entry in the SLL                        */
/*                                                                           */
/* Input(s)            :UsrHistoryCtrlIndex                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT4
Rmon2UsrHistAddObjEntry (UINT4 u4UsrHistoryCtrlIndex)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;
    tUsrHistoryObject  *pUsrHistoryObjectEntry = NULL;
    UINT4               u4NoofObjects;
    UINT4               u4Index = RMON2_ONE;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2UsrHistAddObjEntry \n");

    /* Get the pointer to the index 'UsrHistoryCtrlIndex' */
    pUsrHistoryCtrlEntry = Rmon2UsrHistGetCtrlEntry (u4UsrHistoryCtrlIndex);

    if (pUsrHistoryCtrlEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of UsrHistory Control table is failed \n");
        return OSIX_FAILURE;
    }
    else
    {
        u4NoofObjects = pUsrHistoryCtrlEntry->u4UsrHistoryControlObjects;
        TMO_SLL_Init (&pUsrHistoryCtrlEntry->UsrHistoryObject);
        while (u4Index <= u4NoofObjects)
        {
            pUsrHistoryObjectEntry = (tUsrHistoryObject *) MemAllocMemBlk
                (RMON2_USRHISTOBJ_POOL);
            if (pUsrHistoryObjectEntry == NULL)
            {
                RMON2_TRC (RMON2_CRITICAL_TRC,
                           "Rmon2UsrHistAddObjEntry Object Memory Allocation Failed\n");
                return OSIX_FAILURE;
            }
            MEMSET (pUsrHistoryObjectEntry, 0, sizeof (tUsrHistoryObject));
            pUsrHistoryObjectEntry->UsrHistoryObjectVariable.pu4_OidList =
                (UINT4 *) MemAllocMemBlk (RMON2_HISOIDLIST_POOL);
            if (NULL == pUsrHistoryObjectEntry->
                UsrHistoryObjectVariable.pu4_OidList)
            {

                MemReleaseMemBlock (RMON2_USRHISTOBJ_POOL,
                                    (UINT1 *) pUsrHistoryObjectEntry);
                return OSIX_FAILURE;
            }
            MEMSET (pUsrHistoryObjectEntry->
                    UsrHistoryObjectVariable.pu4_OidList, 0,
                    RMON2_MAX_OID_LENGTH);

            TMO_SLL_Init_Node (&pUsrHistoryObjectEntry->nextUsrHistoryObject);

            pUsrHistoryObjectEntry->u4UsrHistoryObjectIndex = u4Index;
            pUsrHistoryObjectEntry->u4UsrHistoryObjectSampleType =
                RMON2_SAMPLE_TYPE_ABSOLUTE_VALUE;
            TMO_SLL_Insert (&(pUsrHistoryCtrlEntry->UsrHistoryObject),
                            TMO_SLL_Last (&(pUsrHistoryCtrlEntry->
                                            UsrHistoryObject)),
                            &pUsrHistoryObjectEntry->nextUsrHistoryObject);
            u4Index++;
            KW_FALSEPOSITIVE_FIX (pUsrHistoryObjectEntry);
        }
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2UsrHistAddObjEntry \n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : Rmon2UsrHistDelObjEntry                              */
/*                                                                           */
/* Description        : This function is used to delete User History         */
/*                      object table entry from the SLL                      */
/*                                                                           */
/* Input(s)           : UsrHistoryCtrlIndex                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
Rmon2UsrHistDelObjEntry (UINT4 u4UsrHistoryCtrlIndex)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;
    tUsrHistoryObject  *pUsrHistoryObjectEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2UsrHistDelObjEntry \n");

    /* Get the pointer to the index 'UsrHistoryCtrlIndex' */
    pUsrHistoryCtrlEntry = Rmon2UsrHistGetCtrlEntry (u4UsrHistoryCtrlIndex);
    if (pUsrHistoryCtrlEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of user history control table is failed \n");
    }
    else
    {
        TMO_SLL_Scan (&(pUsrHistoryCtrlEntry->UsrHistoryObject),
                      pUsrHistoryObjectEntry, tUsrHistoryObject *)
        {
            if (pUsrHistoryObjectEntry != NULL)
            {
                TMO_SLL_Delete (&(pUsrHistoryCtrlEntry->UsrHistoryObject),
                                &pUsrHistoryObjectEntry->nextUsrHistoryObject);
                if (pUsrHistoryObjectEntry->UsrHistoryObjectVariable.pu4_OidList
                    != NULL)
                {
                    MemReleaseMemBlock (RMON2_HISOIDLIST_POOL, (UINT1 *)
                                        pUsrHistoryObjectEntry->
                                        UsrHistoryObjectVariable.pu4_OidList);
                }
                MemReleaseMemBlock (RMON2_USRHISTOBJ_POOL,
                                    (UINT1 *) pUsrHistoryObjectEntry);
                pUsrHistoryObjectEntry = NULL;
            }
        }
        return OSIX_SUCCESS;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2UsrHistDelObjEntry \n");

    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function Name      : Rmon2UsrHistGetObjectEntry                           */
/*                                                                           */
/* Description        : This function is used to get User History            */
/*                      object table entry for the index                     */
/*                      'u4UsrHistoryCtrlIndex,u4UsrHistoryObjectIndex'      */
/*                                                                           */
/* Input(s)           : u4UsrHistoryCtrlIndex,u4UsrHistoryObjectIndex        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NULL / Pointer to the user history object            */
/*                      entry.                                               */
/*****************************************************************************/

PUBLIC tUsrHistoryObject *
Rmon2UsrHistGetObjectEntry (UINT4 u4UsrHistoryCtrlIndex,
                            UINT4 u4UsrHistoryObjectIndex)
{
    tUsrHistoryObject  *pUsrHistoryObjectEntry = NULL;
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2UsrHistGetObjectEntry \n");

    /* Get the pointer to the index 'u4UsrHistoryCtrlIndex' */
    pUsrHistoryCtrlEntry = Rmon2UsrHistGetCtrlEntry (u4UsrHistoryCtrlIndex);

    if (pUsrHistoryCtrlEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of UsrHistory control table is failed \n");
        return NULL;
    }
    else
    {
        if (u4UsrHistoryObjectIndex == 0)
        {
            pUsrHistoryObjectEntry =
                (tUsrHistoryObject *)
                TMO_SLL_First (&(pUsrHistoryCtrlEntry->UsrHistoryObject));
            return pUsrHistoryObjectEntry;
        }

        TMO_SLL_Scan (&(pUsrHistoryCtrlEntry->UsrHistoryObject),
                      pUsrHistoryObjectEntry, tUsrHistoryObject *)
        {
            if (pUsrHistoryObjectEntry->u4UsrHistoryObjectIndex ==
                u4UsrHistoryObjectIndex)
            {
                RMON2_TRC (RMON2_DEBUG_TRC, "SNMP Success: Valid Index \n");
                return pUsrHistoryObjectEntry;

            }
        }
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of UsrHistory Object table is failed \n");
    }
    return NULL;
}

/*****************************************************************************/
/* Function Name      : Rmon2UsrHistGetNextDataIndex                         */
/*                                                                           */
/* Description        : This function is used to get nextUser History        */
/*                      table entry for the index                            */
/*                      'u4UsrHistoryCtrlIndex,u4srHistorySampleIndex        */
/*                       u4UsrHistoryObjectIndex'                            */
/*                                                                           */
/* Input(s)           : u4UsrHistoryCtrlIndex,u4srHistorySampleIndex         */
/*                       u4UsrHistoryObjectIndex                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NULL / Pointer to the next user history              */
/*                      entry.                                               */
/*****************************************************************************/

PUBLIC tUsrHistory *
Rmon2UsrHistGetNextDataIndex (UINT4 u4UsrHistoryCtrlIndex,
                              UINT4 u4UsrHistorySampleIndex,
                              UINT4 u4UsrHistoryObjectIndex)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;
    tUsrHistoryObject  *pUsrHistoryObjectEntry = NULL;
    tUsrHistory        *pUsrHistoryEntry = NULL;
    UINT4               u4Flag = RMON2_ZERO;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2UsrHistGetNextDataIndex \n");

    /* Get the pointer to the index 'u4UsrHistoryCtrlIndex' */
    pUsrHistoryCtrlEntry = Rmon2UsrHistGetCtrlEntry (u4UsrHistoryCtrlIndex);

    if (pUsrHistoryCtrlEntry != NULL)
    {
        if (u4UsrHistorySampleIndex >= pUsrHistoryCtrlEntry->
            u4UsrHistoryControlBucketsGranted)
        {
            return NULL;
        }

        TMO_SLL_Scan (&(pUsrHistoryCtrlEntry->UsrHistoryObject),
                      pUsrHistoryObjectEntry, tUsrHistoryObject *)
        {
            if (pUsrHistoryObjectEntry->u4UsrHistoryObjectIndex ==
                u4UsrHistoryObjectIndex)
            {
                u4Flag = RMON2_ONE;
                RMON2_TRC (RMON2_DEBUG_TRC, "SNMP Success: Valid Object"
                           "Index \n");
                break;
            }
        }
        if (u4Flag == RMON2_ZERO)
        {
            RMON2_TRC (RMON2_DEBUG_TRC, "Invalid UsrHistoryObjectEntry \n");
            return NULL;
        }

        pUsrHistoryEntry =
            &(pUsrHistoryObjectEntry->pUsrHistory[u4UsrHistorySampleIndex]);

        if (pUsrHistoryEntry != NULL)
        {
            return pUsrHistoryEntry;
        }

    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2UsrHistAddObjectEntry \n");
    return NULL;
}

/*****************************************************************************/
/* Function Name      : Rmon2UsrHistAddDataEntry                             */
/*                                                                           */
/* Description        : This function is used to add User History            */
/*                      data table entry in the SLL                          */
/*                                                                           */
/* Input(s)           : U4UsrHistoryCtrlIndex                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS / SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
Rmon2UsrHistAddDataEntry (UINT4 u4UsrHistoryCtrlIndex)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;
    tUsrHistoryObject  *pUsrHistoryObjectEntry = NULL;
    tUsrHistory        *pUsrHistoryEntry = NULL;
    UINT4               u4NoofSamples = RMON2_ZERO;
    UINT4               u4SampleIndex = RMON2_ZERO;

    pUsrHistoryCtrlEntry = Rmon2UsrHistGetCtrlEntry (u4UsrHistoryCtrlIndex);

    if (pUsrHistoryCtrlEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of User History Control table is failed \n");
        return OSIX_FAILURE;
    }

    TMO_SLL_Scan (&(pUsrHistoryCtrlEntry->UsrHistoryObject),
                  pUsrHistoryObjectEntry, tUsrHistoryObject *)
    {
        pUsrHistoryObjectEntry->pUsrHistory =
            MemAllocMemBlk (RMON2_HISCTRLBUCKETS_POOL);
        if (pUsrHistoryObjectEntry->pUsrHistory == NULL)
        {
            RMON2_TRC (RMON2_DEBUG_TRC,
                       "User History Memory allocation is failed \n");
            return OSIX_FAILURE;
        }
        MEMSET (pUsrHistoryObjectEntry->pUsrHistory, 0,
                sizeof (tUsrHistory) *
                pUsrHistoryCtrlEntry->u4UsrHistoryControlBucketsGranted);

        u4SampleIndex = RMON2_ZERO;
        u4NoofSamples = pUsrHistoryCtrlEntry->u4UsrHistoryControlBucketsGranted;
        while (u4SampleIndex < u4NoofSamples)
        {
            pUsrHistoryEntry =
                &pUsrHistoryObjectEntry->pUsrHistory[u4SampleIndex];
            if ((pUsrHistoryEntry->u4UsrHistoryValStatus != VALUE_POSITIVE) &&
                (pUsrHistoryEntry->u4UsrHistoryValStatus != VALUE_NEGATIVE))
            {
                pUsrHistoryEntry->u4UsrHistoryValStatus = VALUE_NOT_AVAILABLE;
            }
            u4SampleIndex++;
        }

    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : Rmon2UsrHistDelDataEntry                            */
/*                                                                           */
/* Description        : This function is used to delete User History         */
/*                      data table entry in the SLL                          */
/*                                                                           */
/* Input(s)           : U4UsrHistoryCtrlIndex                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS / SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
Rmon2UsrHistDelDataEntry (UINT4 u4UsrHistoryCtrlIndex)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;
    tUsrHistoryObject  *pUsrHistoryObjectEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2UsrHistDelDataEntry \n");

    /* Get the pointer to the index 'u4UsrHistoryCtrlIndex' */
    pUsrHistoryCtrlEntry = Rmon2UsrHistGetCtrlEntry (u4UsrHistoryCtrlIndex);

    if (pUsrHistoryCtrlEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of User History Control table is failed \n");
        return OSIX_FAILURE;
    }
    else
    {
        TMO_SLL_Scan (&(pUsrHistoryCtrlEntry->UsrHistoryObject),
                      pUsrHistoryObjectEntry, tUsrHistoryObject *)
        {
            if ((pUsrHistoryObjectEntry != NULL) &&
                (pUsrHistoryObjectEntry->pUsrHistory != NULL))
            {
                MemReleaseMemBlock (RMON2_HISCTRLBUCKETS_POOL,
                                    (UINT1 *) pUsrHistoryObjectEntry->
                                    pUsrHistory);
            }
        }
        return OSIX_SUCCESS;
    }
}

/*****************************************************************************/
/* Function Name      : Rmon2UsrHistGetDataEntry                             */
/*                                                                           */
/* Description        : This function is used to get User History            */
/*                      table entry for the index                            */
/*                      'u4UsrHistoryCtrlIndex,u4UsrHistorySampleIndex       */
/*                      u4UsrHistoryObjectIndex'                             */
/*                                                                           */
/* Input(s)           : u4UsrHistoryCtrlIndex,u4UsrHistorySampleIndex        */
/*                      u4UsrHistoryObjectIndex                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NULL / Pointer to the user history                   */
/*                      entry.                                               */
/*****************************************************************************/

PUBLIC tUsrHistory *
Rmon2UsrHistGetDataEntry (UINT4 u4UsrHistoryCtrlIndex,
                          UINT4 u4UsrHistorySampleIndex,
                          UINT4 u4UsrHistoryObjectIndex)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;
    tUsrHistoryObject  *pUsrHistoryObjectEntry = NULL;
    tUsrHistory        *pUsrHistoryEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2UsrHistGetDataEntry \n");

    /* Get the pointer to the index 'u4UsrHistoryCtrlIndex' */
    pUsrHistoryCtrlEntry = Rmon2UsrHistGetCtrlEntry (u4UsrHistoryCtrlIndex);

    if (pUsrHistoryCtrlEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC, "Invalid UsrHistoryCtrlEntry \n");
        return NULL;
    }
    else
    {
        TMO_SLL_Scan (&pUsrHistoryCtrlEntry->UsrHistoryObject,
                      pUsrHistoryObjectEntry, tUsrHistoryObject *)
        {
            if (pUsrHistoryObjectEntry->u4UsrHistoryObjectIndex ==
                u4UsrHistoryObjectIndex)
            {
                RMON2_TRC (RMON2_DEBUG_TRC, "SNMP Success: Valid Index \n");
                u4UsrHistorySampleIndex -= 1;
                if (u4UsrHistorySampleIndex < pUsrHistoryCtrlEntry->
                    u4UsrHistoryControlBucketsGranted &&
                    pUsrHistoryObjectEntry->pUsrHistory != NULL)
                {
                    pUsrHistoryEntry =
                        &pUsrHistoryObjectEntry->
                        pUsrHistory[u4UsrHistorySampleIndex];
                    if (pUsrHistoryEntry != NULL)
                    {
                        return pUsrHistoryEntry;
                    }
                }

                RMON2_TRC (RMON2_DEBUG_TRC,
                           "Getting Next sample in the user history table \
                           is failed \n");
                return NULL;

            }
        }
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2UsrHistGetObjectEntry \n");
    return NULL;
}

/****************************************************************************/
/* Function Name        : Rmon2GetMibVariableValue                          */
/*                                                                          */
/* Description          : RmonGetMibVariableValue function gets the value   */
/*                        of a MIB variable given its OID.                  */
/*                                                                          */
/* Input(s)             : pObjName,pUsrHistoryEntry,u4SampleType            */
/*                        u4UsrHistoryObjectIndex                           */
/*                                                                          */
/* Output(s)            : None                                              */
/*                                                                          */
/* Return Value         : RMON2_SUCCESS/RMON2_FAILURE                       */
/****************************************************************************/
PRIVATE INT4
Rmon2GetMibVariableValue (tSNMP_OID_TYPE * pObjName,
                          tUsrHistory * pUsrHistoryEntry, UINT4 u4SampleType)
{
    tSnmpIndex         *pUsrHistIndexPool = gRmon2Globals.Rmon2IndexPool;
    UINT4               u4Error = RMON2_ZERO;
    tSNMP_MULTI_DATA_TYPE MultiData;

    MEMSET (&MultiData, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    RMON2_TRC (RMON2_FN_ENTRY, " FUNC: ENTRY Rmon2GetMibVariableValue\n");

    Rmon2MainUnLock ();
    if (SNMPGet (*pObjName, &MultiData, &u4Error, pUsrHistIndexPool,
                 SNMP_DEFAULT_CONTEXT) == SNMP_SUCCESS)
    {
        /* Get the Object variable value from the multi-data type */
        switch (MultiData.i2_DataType)
        {
            case SNMP_DATA_TYPE_INTEGER32:
                if (u4SampleType == RMON2_SAMPLE_TYPE_ABSOLUTE_VALUE)
                {
                    pUsrHistoryEntry->u4UsrHistoryAbsValueCurSample =
                        (UINT4) abs (MultiData.i4_SLongValue);

                    if (MultiData.i4_SLongValue < RMON2_ZERO)
                    {
                        pUsrHistoryEntry->u4UsrHistoryValStatus =
                            VALUE_NEGATIVE;
                    }
                    else
                    {
                        pUsrHistoryEntry->u4UsrHistoryValStatus =
                            VALUE_POSITIVE;
                    }
                }
                else
                {
                    pUsrHistoryEntry->u4UsrHistoryAbsValueCurSample =
                        (UINT4) abs (MultiData.i4_SLongValue -
                                     (INT4) pUsrHistoryEntry->
                                     u4UsrHistoryAbsValuePrevSample);

                    if (((MultiData.i4_SLongValue == 0) &&
                         (pUsrHistoryEntry->u4UsrHistoryAbsValuePrevSample ==
                          0))
                        || (pUsrHistoryEntry->u4UsrHistoryAbsValueCurSample ==
                            0))
                    {
                        pUsrHistoryEntry->u4UsrHistoryValStatus =
                            VALUE_NOT_AVAILABLE;
                        break;
                    }

                    if (((MultiData.i4_SLongValue) -
                         ((INT4) pUsrHistoryEntry->
                          u4UsrHistoryAbsValuePrevSample)) < RMON2_ZERO)
                    {
                        pUsrHistoryEntry->u4UsrHistoryValStatus =
                            VALUE_NEGATIVE;
                    }
                    else
                    {
                        pUsrHistoryEntry->u4UsrHistoryValStatus =
                            VALUE_POSITIVE;
                    }
                }

                break;
            case SNMP_DATA_TYPE_TIME_TICKS:
            case SNMP_DATA_TYPE_GAUGE:
            case SNMP_DATA_TYPE_COUNTER:

                if (u4SampleType == RMON2_SAMPLE_TYPE_ABSOLUTE_VALUE)
                {
                    pUsrHistoryEntry->u4UsrHistoryAbsValueCurSample =
                        MultiData.u4_ULongValue;
                    pUsrHistoryEntry->u4UsrHistoryValStatus = VALUE_POSITIVE;
                }
                else
                {
                    pUsrHistoryEntry->u4UsrHistoryAbsValueCurSample =
                        (UINT4) abs ((INT4) (MultiData.u4_ULongValue -
                                             pUsrHistoryEntry->
                                             u4UsrHistoryAbsValuePrevSample));
                    if (((MultiData.u4_ULongValue == 0)
                         && (pUsrHistoryEntry->u4UsrHistoryAbsValuePrevSample ==
                             0))
                        || (pUsrHistoryEntry->u4UsrHistoryAbsValueCurSample ==
                            0))
                    {
                        pUsrHistoryEntry->u4UsrHistoryValStatus =
                            VALUE_NOT_AVAILABLE;
                    }
                    else
                    {
                        pUsrHistoryEntry->u4UsrHistoryValStatus =
                            VALUE_POSITIVE;
                    }

                }
                break;

            default:
                pUsrHistoryEntry->u4UsrHistoryAbsValueCurSample = 0;
                pUsrHistoryEntry->u4UsrHistoryValStatus = VALUE_NOT_AVAILABLE;
                Rmon2MainLock ();
                return RMON2_FAILURE;
        }
    }
    else
    {
#ifdef SNMP_3_WANTED
        SnmpApiDecrementCounter (u4Error);
#endif
        pUsrHistoryEntry->u4UsrHistoryAbsValueCurSample = 0;
        pUsrHistoryEntry->u4UsrHistoryValStatus = VALUE_NOT_AVAILABLE;
        Rmon2MainLock ();
        return RMON2_FAILURE;
    }

    Rmon2MainLock ();
    RMON2_TRC (RMON2_FN_EXIT, " FUNC: EXIT Rmon2GetMibVariableValue\n");
    return RMON2_SUCCESS;
}

/**********************************************************************/
/* Function           : Rmon2UpdateUsrHistoryTable                    */
/*                                                                    */
/* Description        : Called on each second To update usr History   */
/*             Table entries                                 */
/*                                                                    */
/* Input (s)          : None.                                         */
/*                                                                    */
/* Output (s)         : None.                                         */
/*                                                                    */
/* Returns            : None.                                         */
/**********************************************************************/
PUBLIC VOID         Rmon2UpdateUsrHistoryTable
ARG_LIST ((VOID))
{
    UINT4               u4NoofSamples = RMON2_ZERO;
    UINT4               u4SampleIndex = RMON2_ZERO;
    UINT4               u4SampleType;
    UINT4               u4TempSampleIndex = RMON2_ZERO;
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;
    tUsrHistoryObject  *pUsrHistoryObjectEntry = NULL;
    tUsrHistory        *pUsrHistoryEntry = NULL;
    tSNMP_MULTI_DATA_TYPE MultiData;

    MEMSET (&MultiData, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    RMON2_TRC (RMON2_FN_ENTRY, " FUNC: ENTRY Rmon2UpdateUsrHistoryTable\n");

    pUsrHistoryCtrlEntry = Rmon2UsrHistGetNextCtrlIndex (RMON2_ZERO);

    /* while is for traversing User history control table */
    while (pUsrHistoryCtrlEntry != NULL)
    {
        /* checking wheather the row status of the user history control 
           entry is ACTIVE */
        if (pUsrHistoryCtrlEntry->u4UsrHistoryControlStatus == ACTIVE)
        {
            u4NoofSamples =
                pUsrHistoryCtrlEntry->u4UsrHistoryControlBucketsGranted;

            /* Traversing Object table */
            TMO_SLL_Scan (&(pUsrHistoryCtrlEntry->UsrHistoryObject),
                          pUsrHistoryObjectEntry, tUsrHistoryObject *)
            {
                if (pUsrHistoryObjectEntry != NULL)
                {
                    u4SampleType =
                        pUsrHistoryObjectEntry->u4UsrHistoryObjectSampleType;
                    u4SampleIndex = RMON2_ZERO;
                    /* while is for traversing all samples in the user 
                     * history table */
                    while (u4SampleIndex < u4NoofSamples)
                    {
                        pUsrHistoryEntry =
                            &pUsrHistoryObjectEntry->pUsrHistory[u4SampleIndex];

                        if (pUsrHistoryEntry->u4UsrHistoryValStatus ==
                            VALUE_NOT_AVAILABLE)
                        {
                            if (pUsrHistoryCtrlEntry->u4RemainingTime ==
                                pUsrHistoryCtrlEntry->
                                u4UsrHistoryControlInterval)
                            {
                                pUsrHistoryEntry->u4UsrHistoryIntervalStart =
                                    OsixGetSysUpTime ();
                                break;
                            }
                            /* Storing the sample values */
                            else if (pUsrHistoryCtrlEntry->u4RemainingTime ==
                                     RMON2_ZERO)
                            {
                                pUsrHistoryEntry->u4UsrHistorySampleIndex =
                                    u4SampleIndex + 1;
                                pUsrHistoryEntry->u4UsrHistoryControlIndex =
                                    pUsrHistoryCtrlEntry->
                                    u4UsrHistoryControlIndex;
                                pUsrHistoryEntry->u4UsrHistoryObjectIndex =
                                    pUsrHistoryObjectEntry->
                                    u4UsrHistoryObjectIndex;

                                if (Rmon2GetMibVariableValue
                                    (&(pUsrHistoryObjectEntry->
                                       UsrHistoryObjectVariable),
                                     pUsrHistoryEntry,
                                     u4SampleType) == RMON2_SUCCESS)
                                {
                                    RMON2_TRC (RMON2_DEBUG_TRC,
                                               "Successfully got the"
                                               "Rmon2GetMibVariableValue \n");

                                    pUsrHistoryEntry->u4UsrHistoryIntervalEnd =
                                        OsixGetSysUpTime ();
                                    switch (MultiData.i2_DataType)
                                    {
                                        case SNMP_DATA_TYPE_INTEGER32:
                                            pUsrHistoryEntry->
                                                u4UsrHistoryAbsValueCurSample =
                                                (UINT4) MultiData.i4_SLongValue;
                                            break;
                                        case SNMP_DATA_TYPE_TIME_TICKS:
                                        case SNMP_DATA_TYPE_GAUGE:
                                        case SNMP_DATA_TYPE_COUNTER:
                                            pUsrHistoryEntry->
                                                u4UsrHistoryAbsValueCurSample =
                                                MultiData.u4_ULongValue;
                                            break;

                                        default:
                                            break;
                                    }

                                    pUsrHistoryEntry->
                                        u4UsrHistoryAbsValuePrevSample =
                                        pUsrHistoryEntry->
                                        u4UsrHistoryAbsValueCurSample;
                                    if (pUsrHistoryObjectEntry->
                                        u4UsrHistoryObjectSampleType > RMON2_ONE
                                        && u4SampleIndex > RMON2_ZERO)
                                    {
                                        pUsrHistoryEntry->
                                            u4UsrHistoryAbsValueCurSample =
                                            pUsrHistoryEntry->
                                            u4UsrHistoryAbsValueCurSample -
                                            pUsrHistoryObjectEntry->
                                            pUsrHistory[u4SampleIndex -
                                                        1].
                                            u4UsrHistoryAbsValuePrevSample;
                                    }
                                }
                                else
                                {
                                    pUsrHistoryEntry->u4UsrHistoryIntervalEnd =
                                        OsixGetSysUpTime ();
                                }
                                if (u4SampleIndex + 1 == u4NoofSamples)
                                {
                                    while (u4TempSampleIndex < u4NoofSamples)
                                    {
                                        pUsrHistoryObjectEntry->
                                            pUsrHistory[u4TempSampleIndex].
                                            u4UsrHistoryValStatus =
                                            VALUE_NOT_AVAILABLE;
                                        u4TempSampleIndex++;

                                    }
                                }

                                break;

                            }

                            break;
                        }
                        else
                        {
                            u4SampleIndex++;
                        }

                    }            /* end of while */
                }                /* ObjectEntry != NULL */

            }                    /* TMO_SLL_Scan */
            if (pUsrHistoryCtrlEntry->u4RemainingTime ==
                pUsrHistoryCtrlEntry->u4UsrHistoryControlInterval)
            {
                pUsrHistoryCtrlEntry->u4RemainingTime--;
            }
            else if (pUsrHistoryCtrlEntry->u4RemainingTime == RMON2_ZERO)
            {
                pUsrHistoryCtrlEntry->u4RemainingTime =
                    pUsrHistoryCtrlEntry->u4UsrHistoryControlInterval;
            }
            else
            {
                pUsrHistoryCtrlEntry->u4RemainingTime--;
            }
        }                        /*end of if */
        pUsrHistoryCtrlEntry =
            Rmon2UsrHistGetNextCtrlIndex (pUsrHistoryCtrlEntry->
                                          u4UsrHistoryControlIndex);
    }                            /* end of while */
    RMON2_TRC (RMON2_FN_EXIT, " FUNC: EXIT Rmon2UpdateUsrHistoryTable\n");
}
