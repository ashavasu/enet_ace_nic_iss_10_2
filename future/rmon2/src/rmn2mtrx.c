/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * $Id: rmn2mtrx.c,v 1.15 2015/10/30 09:04:39 siva Exp $
 *
 * Description: This file contains the functional routine for
 *              RMON2 Network Layer and Application Layer 
 *              Matrix modules
 *
 *******************************************************************/
#include "rmn2inc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2NlMatrixGiveLRUEntry                                  */
/*                                                                           */
/* Description  : Gives the LRU Entry                                        */
/*                                                                           */
/* Input        : u4HlMatrixControlIndex                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pNlMatrixSD / NULL                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE tNlMatrixSD *
Rmon2NlMatrixGiveLRUEntry (UINT4 u4HlMatrixControlIndex)
{
    tNlMatrixSD        *pNlMatrixSD = NULL, *pLRUEntry = NULL;

    UINT4               u4NlProtocolDirLocalIndex = 0;
    tIpAddr             NlMatrixSourceAddress, NlMatrixDestAddress;

    MEMSET (&NlMatrixSourceAddress, 0, sizeof (tIpAddr));
    MEMSET (&NlMatrixDestAddress, 0, sizeof (tIpAddr));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2NlMatrixGiveLRUEntry\r\n");

    /* Get First Entry in NlMatrixSD Table */
    pNlMatrixSD = Rmon2NlMatrixGetNextDataIndex (RMON2_NLMATRIXSD_TABLE,
                                                 u4HlMatrixControlIndex,
                                                 u4NlProtocolDirLocalIndex,
                                                 &NlMatrixSourceAddress,
                                                 &NlMatrixDestAddress);

    /* Mark First Entry as LRU entry */

    pLRUEntry = pNlMatrixSD;

    while (pNlMatrixSD != NULL)
    {
        if (pNlMatrixSD->u4HlMatrixControlIndex != u4HlMatrixControlIndex)
        {
            break;
        }

        u4NlProtocolDirLocalIndex = pNlMatrixSD->u4ProtocolDirLocalIndex;
        MEMCPY (&NlMatrixSourceAddress, &(pNlMatrixSD->NlMatrixSDSourceAddress),
                sizeof (tIpAddr));
        MEMCPY (&NlMatrixDestAddress, &(pNlMatrixSD->NlMatrixSDDestAddress),
                sizeof (tIpAddr));

        if (pLRUEntry->u4NlMatrixSDTimeMark > pNlMatrixSD->u4NlMatrixSDTimeMark)
        {
            pLRUEntry = pNlMatrixSD;
        }

        pNlMatrixSD = Rmon2NlMatrixGetNextDataIndex (RMON2_NLMATRIXSD_TABLE,
                                                     u4HlMatrixControlIndex,
                                                     u4NlProtocolDirLocalIndex,
                                                     &NlMatrixSourceAddress,
                                                     &NlMatrixDestAddress);
    }                            /* End of while */

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2NlMatrixGiveLRUEntry\r\n");
    return pLRUEntry;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AlMatrixGiveLRUEntry                                  */
/*                                                                           */
/* Description  : Gives the LRU Entry                                        */
/*                                                                           */
/* Input        : u4HlMatrixControlIndex                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pAlMatrixSD / NULL                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE tAlMatrixSD *
Rmon2AlMatrixGiveLRUEntry (UINT4 u4HlMatrixControlIndex)
{
    tAlMatrixSD        *pAlMatrixSD = NULL, *pLRUEntry = NULL;

    UINT4               u4NlProtocolDirLocalIndex =
        0, u4AlProtocolDirLocalIndex = 0;
    tIpAddr             NlMatrixSourceAddress, NlMatrixDestAddress;

    MEMSET (&NlMatrixSourceAddress, 0, sizeof (tIpAddr));
    MEMSET (&NlMatrixDestAddress, 0, sizeof (tIpAddr));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AlMatrixGiveLRUEntry\r\n");

    /* Get First Entry in AlMatrixSD Table */

    pAlMatrixSD = Rmon2AlMatrixGetNextDataIndex (RMON2_ALMATRIXSD_TABLE,
                                                 u4HlMatrixControlIndex,
                                                 u4NlProtocolDirLocalIndex,
                                                 &NlMatrixSourceAddress,
                                                 &NlMatrixDestAddress,
                                                 u4AlProtocolDirLocalIndex);

    /* Mark First Entry as LRU entry */

    pLRUEntry = pAlMatrixSD;

    while (pAlMatrixSD != NULL)
    {
        if (pAlMatrixSD->u4HlMatrixControlIndex != u4HlMatrixControlIndex)
        {
            break;
        }

        u4NlProtocolDirLocalIndex = pAlMatrixSD->u4NlProtocolDirLocalIndex;
        MEMCPY (&NlMatrixSourceAddress, &(pAlMatrixSD->NlMatrixSDSourceAddress),
                sizeof (tIpAddr));
        MEMCPY (&NlMatrixDestAddress, &(pAlMatrixSD->NlMatrixSDDestAddress),
                sizeof (tIpAddr));
        u4AlProtocolDirLocalIndex = pAlMatrixSD->u4AlProtocolDirLocalIndex;

        if (pLRUEntry->u4AlMatrixSDTimeMark > pAlMatrixSD->u4AlMatrixSDTimeMark)
        {
            pLRUEntry = pAlMatrixSD;
        }

        pAlMatrixSD = Rmon2AlMatrixGetNextDataIndex (RMON2_ALMATRIXSD_TABLE,
                                                     u4HlMatrixControlIndex,
                                                     u4NlProtocolDirLocalIndex,
                                                     &NlMatrixSourceAddress,
                                                     &NlMatrixDestAddress,
                                                     u4AlProtocolDirLocalIndex);
    }                            /* End of while */

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AlMatrixGiveLRUEntry\r\n");
    return pLRUEntry;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2MatrixProcessPktInfo                                  */
/*                                                                           */
/* Description  : Process Incoming Packet from Control Plane and Update      */
/*                Nl and Al Matrix Tables                                    */
/*                                                                           */
/* Input        : pPktInfo                                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rmon2MatrixProcessPktInfo (tRmon2PktInfo * pPktInfo, UINT4 u4PktSize)
{
    tHlMatrixControl   *pHlMatrixCtrl = NULL;
    tProtocolDir       *pNlProtDirEntry = NULL, *pAlProtDirEntry = NULL;
    tNlMatrixSD        *pCurNlMatrixSD = NULL, *pPrevNlMatrixSD = NULL;
    tAlMatrixSD        *pCurAlMatrixSD = NULL, *pPrevAlMatrixSD = NULL;
    tAlMatrixSD        *pTravAlMatrixSD = NULL;
    UINT4               u4HlMatrixControlIndex = 0;
    INT4                i4RetVal = OSIX_FAILURE;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2MatrixProcessPktInfo\r\n");

    pHlMatrixCtrl = Rmon2HlMatrixGetNextCtrlIndex (u4HlMatrixControlIndex);

    while (pHlMatrixCtrl != NULL)
    {
        if ((pHlMatrixCtrl->u4HlMatrixControlStatus == ACTIVE) &&
            ((pHlMatrixCtrl->u4HlMatrixControlDataSource ==
              pPktInfo->PktHdr.u4IfIndex) ||
             (pHlMatrixCtrl->u4HlMatrixControlDataSource ==
              pPktInfo->PktHdr.u4VlanIfIndex)))
        {
            if (pHlMatrixCtrl->u4HlMatrixControlDataSource ==
                pPktInfo->PktHdr.u4VlanIfIndex)
            {
                pPktInfo->u1IsL3Vlan = OSIX_TRUE;
            }
            pNlProtDirEntry =
                Rmon2PDirGetProtocolNode (pPktInfo->u4ProtoNLIndex);
            if (pNlProtDirEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "Get Protocol Entry failed for NLProtocolLocIndex\r\n");
                pHlMatrixCtrl = Rmon2HlMatrixGetNextCtrlIndex (pHlMatrixCtrl->
                                                               u4HlMatrixControlIndex);
                continue;
            }
            if ((pPktInfo->u4ProtoNLIndex != 0) &&
                (pNlProtDirEntry->u4ProtocolDirMatrixConfig == SUPPORTED_ON))
            {
                /* Checking for an NlMatrixSD Entry for L3 Protocol
                 * already exist or not */
                pCurNlMatrixSD =
                    Rmon2NlMatrixGetDataEntry (RMON2_NLMATRIXSD_TABLE,
                                               pHlMatrixCtrl->
                                               u4HlMatrixControlIndex,
                                               pPktInfo->u4ProtoNLIndex,
                                               &(pPktInfo->PktHdr.SrcIp),
                                               &(pPktInfo->PktHdr.DstIp));
                if (pCurNlMatrixSD == NULL)
                {
                    if (pHlMatrixCtrl->u4HlMatrixControlNlMaxDesiredSupported
                        == 0)
                    {
                        pHlMatrixCtrl->u4HlMatrixControlNlDroppedFrames++;
                        pHlMatrixCtrl->u4HlMatrixControlAlDroppedFrames++;
                        pHlMatrixCtrl =
                            Rmon2HlMatrixGetNextCtrlIndex (pHlMatrixCtrl->
                                                           u4HlMatrixControlIndex);
                        continue;
                    }
                    /* Check whether NlMatrixSD data entries reached max limit */
                    if ((pHlMatrixCtrl->u4HlMatrixControlNlInserts -
                         pHlMatrixCtrl->u4HlMatrixControlNlDeletes) >=
                        pHlMatrixCtrl->u4HlMatrixControlNlMaxDesiredSupported)
                    {
                        pCurNlMatrixSD = Rmon2NlMatrixGiveLRUEntry
                            (pHlMatrixCtrl->u4HlMatrixControlIndex);

                        if (pCurNlMatrixSD != NULL)
                        {
                            RMON2_TRC (RMON2_DEBUG_TRC,
                                       "Deleting NlMatrixSD LRU Entry\r\n");

                            /* Remove Inter table references */
                            Rmon2NlMatrixDelInterDep (pCurNlMatrixSD);

                            pCurNlMatrixSD->pHlMatrixCtrl->
                                u4HlMatrixControlNlDeletes += 2;

                            /* After Reference changes, free NlMatrixSD entry */
                            Rmon2NlMatrixDelDataEntry (pCurNlMatrixSD);

                            pCurNlMatrixSD = NULL;
                        }
                    }

                    /* Add Nl Matrix SD & DS Entry */
                    pCurNlMatrixSD =
                        Rmon2NlMatrixAddDataEntry (pHlMatrixCtrl->
                                                   u4HlMatrixControlIndex,
                                                   pPktInfo->u4ProtoNLIndex,
                                                   &(pPktInfo->PktHdr.SrcIp),
                                                   &(pPktInfo->PktHdr.DstIp));

                    if (pCurNlMatrixSD == NULL)
                    {
                        pHlMatrixCtrl->u4HlMatrixControlNlDroppedFrames++;
                    }
                    else
                    {
                        pCurNlMatrixSD->pHlMatrixCtrl = pHlMatrixCtrl;

                        /* Increase by 2 for NlMatrix SD & DS table entry */
                        pHlMatrixCtrl->u4HlMatrixControlNlInserts += 2;
                        if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV4)
                        {
                            pCurNlMatrixSD->u4Rmon2NlMatrixSDIpAddrLen =
                                RMON2_IPV4_MAX_LEN;
                        }
                        else if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV6)
                        {
                            pCurNlMatrixSD->u4Rmon2NlMatrixSDIpAddrLen =
                                RMON2_IPV6_MAX_LEN;
                        }
                        pCurNlMatrixSD->u4PktRefCount++;
                        pCurNlMatrixSD->NlMatrixSDCurSample.u4NlMatrixSDPkts++;
                        pCurNlMatrixSD->NlMatrixSDCurSample.
                            u4NlMatrixSDOctets += u4PktSize;
                    }
                }
                else
                {
                    pCurNlMatrixSD->u4PktRefCount++;
                    pPktInfo->pNlMatrixSD = pCurNlMatrixSD;
                    pCurNlMatrixSD->NlMatrixSDCurSample.u4NlMatrixSDPkts++;
                    pCurNlMatrixSD->NlMatrixSDCurSample.u4NlMatrixSDOctets +=
                        u4PktSize;
                    pCurNlMatrixSD->u4NlMatrixSDTimeMark = OsixGetSysUpTime ();
                }

                if (pCurNlMatrixSD != NULL)
                {
                    if (pPrevNlMatrixSD == NULL)
                    {
                        pPktInfo->pNlMatrixSD = pCurNlMatrixSD;
                    }
                    else
                    {
                        pPrevNlMatrixSD->pNextNlMatrixSD = pCurNlMatrixSD;
                        pCurNlMatrixSD->pPrevNlMatrixSD = pPrevNlMatrixSD;
                    }
                }
                pPrevNlMatrixSD = pCurNlMatrixSD;
                i4RetVal = OSIX_SUCCESS;
            }

            pAlProtDirEntry =
                Rmon2PDirGetProtocolNode (pPktInfo->u4ProtoL5ALIndex);
            if (pAlProtDirEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "Get Protocol Entry failed for L5ProtocolLocIndex\r\n");
                i4RetVal = OSIX_FAILURE;
                return i4RetVal;
            }

            if ((pPktInfo->u4ProtoNLIndex != 0) &&
                (pPktInfo->u4ProtoL5ALIndex != 0) &&
                (pNlProtDirEntry->u4ProtocolDirMatrixConfig == SUPPORTED_ON) &&
                (pAlProtDirEntry->u4ProtocolDirMatrixConfig == SUPPORTED_ON))
            {
                pCurAlMatrixSD = NULL;

                /* Checking for an AlMatrixSD Entry for L5 Protocol 
                 * already exist or not */
                pCurAlMatrixSD =
                    Rmon2AlMatrixGetDataEntry (RMON2_ALMATRIXSD_TABLE,
                                               pHlMatrixCtrl->
                                               u4HlMatrixControlIndex,
                                               pPktInfo->u4ProtoNLIndex,
                                               &(pPktInfo->PktHdr.SrcIp),
                                               &(pPktInfo->PktHdr.DstIp),
                                               pPktInfo->u4ProtoL5ALIndex);

                /* Add AlMatrixSD Entry only if NlMatrixSD Entry exist */
                if ((pCurAlMatrixSD == NULL) && (pCurNlMatrixSD != NULL))
                {
                    if (pHlMatrixCtrl->u4HlMatrixControlAlMaxDesiredSupported
                        == 0)
                    {
                        pHlMatrixCtrl->u4HlMatrixControlAlDroppedFrames++;
                        return OSIX_FAILURE;
                    }
                    /* Check whether AlMatrixSD data entries reached max limit */
                    if ((pHlMatrixCtrl->u4HlMatrixControlAlInserts -
                         pHlMatrixCtrl->u4HlMatrixControlAlDeletes) >=
                        pHlMatrixCtrl->u4HlMatrixControlAlMaxDesiredSupported)
                    {
                        pCurAlMatrixSD = Rmon2AlMatrixGiveLRUEntry
                            (pHlMatrixCtrl->u4HlMatrixControlIndex);

                        if (pCurAlMatrixSD != NULL)
                        {
                            RMON2_TRC (RMON2_DEBUG_TRC,
                                       "Deleting AlMatrixSD LRU Entry\r\n");

                            /* Remove Inter table references */
                            Rmon2AlMatrixDelInterDep (pCurAlMatrixSD);

                            pCurAlMatrixSD->pHlMatrixCtrl->
                                u4HlMatrixControlAlDeletes += 2;

                            /* After Reference changes, free AlMatrixSD entry */
                            Rmon2AlMatrixDelDataEntry (pCurAlMatrixSD);

                            pCurAlMatrixSD = NULL;
                        }
                    }

                    pCurAlMatrixSD =
                        Rmon2AlMatrixAddDataEntry (pHlMatrixCtrl->
                                                   u4HlMatrixControlIndex,
                                                   pPktInfo->u4ProtoNLIndex,
                                                   &(pPktInfo->PktHdr.SrcIp),
                                                   &(pPktInfo->PktHdr.DstIp),
                                                   pPktInfo->u4ProtoL5ALIndex);

                    if (pCurAlMatrixSD == NULL)
                    {
                        pHlMatrixCtrl->u4HlMatrixControlAlDroppedFrames++;
                        pHlMatrixCtrl =
                            Rmon2HlMatrixGetNextCtrlIndex (pHlMatrixCtrl->
                                                           u4HlMatrixControlIndex);
                        continue;
                    }
                    else
                    {
                        pCurAlMatrixSD->pHlMatrixCtrl = pHlMatrixCtrl;

                        /* Increase by 2 for AlMatrix SD & DS table entry */
                        pHlMatrixCtrl->u4HlMatrixControlAlInserts += 2;
                        if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV4)
                        {
                            pCurAlMatrixSD->u4Rmon2NlMatrixSDIpAddrLen =
                                RMON2_IPV4_MAX_LEN;
                        }
                        else if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV6)
                        {
                            pCurAlMatrixSD->u4Rmon2NlMatrixSDIpAddrLen =
                                RMON2_IPV6_MAX_LEN;
                        }
                        pCurAlMatrixSD->u4PktRefCount++;
                        pCurAlMatrixSD->AlMatrixSDCurSample.u4AlMatrixSDPkts++;
                        pCurAlMatrixSD->AlMatrixSDCurSample.
                            u4AlMatrixSDOctets += u4PktSize;
                    }

                    if (pPrevAlMatrixSD == NULL)
                    {
                        pPktInfo->pAlMatrixSD = pCurAlMatrixSD;
                    }
                    else
                    {
                        pPrevAlMatrixSD->pNextAlMatrixSD = pCurAlMatrixSD;
                        pCurAlMatrixSD->pPrevAlMatrixSD = pPrevAlMatrixSD;
                    }
                }
                else if (pCurAlMatrixSD != NULL)
                {
                    pCurAlMatrixSD->u4PktRefCount++;
                    pPktInfo->pAlMatrixSD = pCurAlMatrixSD;
                    pCurAlMatrixSD->AlMatrixSDCurSample.u4AlMatrixSDPkts++;
                    pCurAlMatrixSD->AlMatrixSDCurSample.u4AlMatrixSDOctets +=
                        u4PktSize;
                    pCurAlMatrixSD->u4AlMatrixSDTimeMark = OsixGetSysUpTime ();
                }

                pPrevAlMatrixSD = pCurAlMatrixSD;
                i4RetVal = OSIX_SUCCESS;
            }

            pAlProtDirEntry =
                Rmon2PDirGetProtocolNode (pPktInfo->u4ProtoL4ALIndex);
            if (pAlProtDirEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "Get Protocol Entry failed for L4ProtocolLocIndex\r\n");
                return OSIX_FAILURE;
            }

            if ((pPktInfo->u4ProtoNLIndex != 0) &&
                (pPktInfo->u4ProtoL4ALIndex != 0) &&
                (pNlProtDirEntry->u4ProtocolDirMatrixConfig == SUPPORTED_ON) &&
                (pAlProtDirEntry->u4ProtocolDirMatrixConfig == SUPPORTED_ON))
            {
                /* Checking for an AlMatrixSD Entry for L4 Protocol 
                 * already exist or not */
                pCurAlMatrixSD =
                    Rmon2AlMatrixGetDataEntry (RMON2_ALMATRIXSD_TABLE,
                                               pHlMatrixCtrl->
                                               u4HlMatrixControlIndex,
                                               pPktInfo->u4ProtoNLIndex,
                                               &(pPktInfo->PktHdr.SrcIp),
                                               &(pPktInfo->PktHdr.DstIp),
                                               pPktInfo->u4ProtoL4ALIndex);

                /* Add AlMatrixSD Entry only if NlMatrixSD Entry exist */
                if ((pCurAlMatrixSD == NULL) && (pCurNlMatrixSD != NULL))
                {
                    if (pHlMatrixCtrl->u4HlMatrixControlAlMaxDesiredSupported
                        == 0)
                    {
                        pHlMatrixCtrl->u4HlMatrixControlAlDroppedFrames++;
                        pHlMatrixCtrl =
                            Rmon2HlMatrixGetNextCtrlIndex (pHlMatrixCtrl->
                                                           u4HlMatrixControlIndex);
                        continue;
                    }
                    /* Check whether AlMatrixSD data entries reached max limit */
                    if ((pHlMatrixCtrl->u4HlMatrixControlAlInserts -
                         pHlMatrixCtrl->u4HlMatrixControlAlDeletes) >=
                        pHlMatrixCtrl->u4HlMatrixControlAlMaxDesiredSupported)
                    {
                        pCurAlMatrixSD = Rmon2AlMatrixGiveLRUEntry
                            (pHlMatrixCtrl->u4HlMatrixControlIndex);

                        if (pCurAlMatrixSD != NULL)
                        {
                            RMON2_TRC (RMON2_DEBUG_TRC,
                                       "Deleting AlMatrixSD LRU Entry\r\n");

                            /* Remove Inter table references */
                            Rmon2AlMatrixDelInterDep (pCurAlMatrixSD);

                            pCurAlMatrixSD->pHlMatrixCtrl->
                                u4HlMatrixControlAlDeletes += 2;

                            /* After Reference changes, free AlMatrixSD entry */
                            Rmon2AlMatrixDelDataEntry (pCurAlMatrixSD);

                            pCurAlMatrixSD = NULL;
                        }
                    }

                    pCurAlMatrixSD =
                        Rmon2AlMatrixAddDataEntry (pHlMatrixCtrl->
                                                   u4HlMatrixControlIndex,
                                                   pPktInfo->u4ProtoNLIndex,
                                                   &(pPktInfo->PktHdr.SrcIp),
                                                   &(pPktInfo->PktHdr.DstIp),
                                                   pPktInfo->u4ProtoL4ALIndex);

                    if (pCurAlMatrixSD == NULL)
                    {
                        pHlMatrixCtrl->u4HlMatrixControlAlDroppedFrames++;
                    }
                    else
                    {
                        pCurAlMatrixSD->pHlMatrixCtrl = pHlMatrixCtrl;

                        /* Increase by 2 for AlMatrix SD & DS table entry */
                        pHlMatrixCtrl->u4HlMatrixControlAlInserts += 2;
                        if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV4)
                        {
                            pCurAlMatrixSD->u4Rmon2NlMatrixSDIpAddrLen =
                                RMON2_IPV4_MAX_LEN;
                        }
                        else if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV6)
                        {
                            pCurAlMatrixSD->u4Rmon2NlMatrixSDIpAddrLen =
                                RMON2_IPV6_MAX_LEN;
                        }
                        pCurAlMatrixSD->u4PktRefCount++;
                        pCurAlMatrixSD->AlMatrixSDCurSample.u4AlMatrixSDPkts++;
                        pCurAlMatrixSD->AlMatrixSDCurSample.
                            u4AlMatrixSDOctets += u4PktSize;
                    }
                    if (pCurAlMatrixSD != NULL)
                    {
                        if (pPrevAlMatrixSD == NULL)
                        {
                            pPktInfo->pAlMatrixSD = pCurAlMatrixSD;
                        }
                        else
                        {
                            pPrevAlMatrixSD->pNextAlMatrixSD = pCurAlMatrixSD;
                            pCurAlMatrixSD->pPrevAlMatrixSD = pPrevAlMatrixSD;
                        }
                    }
                }
                else if (pCurAlMatrixSD != NULL)
                {
                    pCurAlMatrixSD->u4PktRefCount++;
                    pCurAlMatrixSD->AlMatrixSDCurSample.u4AlMatrixSDPkts++;
                    pCurAlMatrixSD->AlMatrixSDCurSample.u4AlMatrixSDOctets +=
                        u4PktSize;
                    pCurAlMatrixSD->u4AlMatrixSDTimeMark = OsixGetSysUpTime ();
                    pTravAlMatrixSD = pPktInfo->pAlMatrixSD;
                    if (pTravAlMatrixSD == NULL)
                    {
                        pPktInfo->pAlMatrixSD = pCurAlMatrixSD;
                    }
                    else
                    {
                        while (pTravAlMatrixSD->pNextAlMatrixSD != NULL)
                            pTravAlMatrixSD = pTravAlMatrixSD->pNextAlMatrixSD;
                        pTravAlMatrixSD->pNextAlMatrixSD = pCurAlMatrixSD;

                         pTravAlMatrixSD->pNextAlMatrixSD->pNextAlMatrixSD = NULL;
                    }
                }

                pPrevAlMatrixSD = pCurAlMatrixSD;
                i4RetVal = OSIX_SUCCESS;
            }
        }                        /* End of if check */
        pHlMatrixCtrl =
            Rmon2HlMatrixGetNextCtrlIndex (pHlMatrixCtrl->
                                           u4HlMatrixControlIndex);
    }                            /* End of while */
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2MatrixProcessPktInfo\r\n");
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2MatrixUpdateIFIndexChgs                               */
/*                                                                           */
/* Description  : If Operstatus is CFA_IF_DOWN, then set appropriate control */
/*                entries to NOT_READY state thus deleting the associated    */
/*                Data Table. If OperStatus is CFA_IF_UP, set NOT_READY      */
/*                entries to ACTIVE state                                    */
/*                                                                           */
/* Input        : u4IfIndex, u1OperStatus                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2MatrixUpdateIFIndexChgs (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tPortList          *pVlanPortList = NULL;
    tHlMatrixControl   *pHlMatrixCtrl = NULL;
    UINT4               u4HlMatrixControlIndex = 0;
    tCfaIfInfo          IfInfo;
    UINT2               u2VlanId = 0;

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2MatrixUpdateIFIndexChgs\r\n");

    switch (u1OperStatus)
    {
        case CFA_IF_DOWN:
            /* Get 1st Hl Matrix Ctrl Index */
            pHlMatrixCtrl =
                Rmon2HlMatrixGetNextCtrlIndex (u4HlMatrixControlIndex);
            pVlanPortList =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pVlanPortList == NULL)
            {
                RMON2_TRC (RMON2_FN_ENTRY,
                           "Rmon2MatrixUpdateIFIndexChgs: "
                           "Error in allocating memory for pVlanPortList\r\n");
                return;
            }
            MEMSET (pVlanPortList, 0, sizeof (tPortList));
            while (pHlMatrixCtrl != NULL)
            {
                u4HlMatrixControlIndex = pHlMatrixCtrl->u4HlMatrixControlIndex;
                if ((pHlMatrixCtrl->u4HlMatrixControlStatus == ACTIVE) &&
                    (pHlMatrixCtrl->u4HlMatrixControlDataSource == u4IfIndex))
                {
                    /* Remove Data Entries for this control index 
                     * from Nl & Al Matrix Tables */

                    Rmon2MatrixDelDataEntries (u4HlMatrixControlIndex);

                    /* Change RowStatus to NOT_READY */
                    pHlMatrixCtrl->u4HlMatrixControlStatus = NOT_READY;
                    /* check for valid datasource */

                    if (CfaGetIfInfo
                        (pHlMatrixCtrl->u4HlMatrixControlDataSource,
                         &IfInfo) != CFA_FAILURE)
                    {
                        if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
                            (CfaGetVlanId
                             (pHlMatrixCtrl->u4HlMatrixControlDataSource,
                              &u2VlanId) != CFA_FAILURE))
                        {
                            L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList);
                        }
                    }
#ifdef NPAPI_WANTED
                    Rmonv2FsRMONv2DisableProbe (pHlMatrixCtrl->
                                                u4HlMatrixControlDataSource,
                                                u2VlanId, *pVlanPortList);
#endif
                }                /* End of if check */
                pHlMatrixCtrl =
                    Rmon2HlMatrixGetNextCtrlIndex (u4HlMatrixControlIndex);
            }                    /* End of while */
            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
            break;
        case CFA_IF_UP:
            /* Get 1st Hl Matrix Ctrl Index */
            pHlMatrixCtrl =
                Rmon2HlMatrixGetNextCtrlIndex (u4HlMatrixControlIndex);
            while (pHlMatrixCtrl != NULL)
            {
                u4HlMatrixControlIndex = pHlMatrixCtrl->u4HlMatrixControlIndex;
                if ((pHlMatrixCtrl->u4HlMatrixControlStatus == NOT_READY) &&
                    (pHlMatrixCtrl->u4HlMatrixControlDataSource == u4IfIndex))
                {
                    /* Change RowStatus to ACTIVE */
                    nmhSetHlMatrixControlStatus ((INT4) pHlMatrixCtrl->
                                                 u4HlMatrixControlIndex,
                                                 ACTIVE);
                }                /* End of if check */
                pHlMatrixCtrl =
                    Rmon2HlMatrixGetNextCtrlIndex (u4HlMatrixControlIndex);
            }                    /* End of while */
            break;
        default:
            break;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2MatrixUpdateIFIndexChgs\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2MatrixUpdateProtoChgs                                 */
/*                                                                           */
/* Description  : Removes Nl and Al Matrix Data Table Entries for change in  */
/*                Protocol Dir Entry Status or Matrix config support is      */
/*                turned off for a Protocol Dir Entry.                       */
/*                                                                           */
/* Input        : u4NlProtocolDirLocalIndex                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2MatrixUpdateProtoChgs (UINT4 u4ProtocolIndex)
{
    tNlMatrixSD        *pNlMatrixSD = NULL;
    tAlMatrixSD        *pAlMatrixSD = NULL;
    UINT4               u4HlMatrixControlIndex = 0;
    UINT4               u4NlProtocolDirLocalIndex =
        0, u4AlProtocolDirLocalIndex = 0;
    tIpAddr             NlMatrixSourceAddress, NlMatrixDestAddress;

    MEMSET (&NlMatrixSourceAddress, 0, sizeof (tIpAddr));
    MEMSET (&NlMatrixDestAddress, 0, sizeof (tIpAddr));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2MatrixUpdateProtoChgs\r\n");

    /* Get First Entry in Nl Matrix SD Table */

    pNlMatrixSD = Rmon2NlMatrixGetNextDataIndex (RMON2_NLMATRIXSD_TABLE,
                                                 u4HlMatrixControlIndex,
                                                 u4NlProtocolDirLocalIndex,
                                                 &NlMatrixSourceAddress,
                                                 &NlMatrixDestAddress);

    while (pNlMatrixSD != NULL)
    {
        u4HlMatrixControlIndex = pNlMatrixSD->u4HlMatrixControlIndex;
        u4NlProtocolDirLocalIndex = pNlMatrixSD->u4ProtocolDirLocalIndex;
        MEMCPY (&NlMatrixSourceAddress, &(pNlMatrixSD->NlMatrixSDSourceAddress),
                sizeof (tIpAddr));
        MEMCPY (&NlMatrixDestAddress, &(pNlMatrixSD->NlMatrixSDDestAddress),
                sizeof (tIpAddr));

        if (pNlMatrixSD->u4ProtocolDirLocalIndex == u4ProtocolIndex)
        {
            /* Remove Inter table dependencies */
            Rmon2NlMatrixDelInterDep (pNlMatrixSD);

            pNlMatrixSD->pHlMatrixCtrl->u4HlMatrixControlNlDeletes += 2;

            /* After Reference changes, free Nl Matrix entry for NL Protocol */
            Rmon2NlMatrixDelDataEntry (pNlMatrixSD);
        }

        pNlMatrixSD = Rmon2NlMatrixGetNextDataIndex (RMON2_NLMATRIXSD_TABLE,
                                                     u4HlMatrixControlIndex,
                                                     u4NlProtocolDirLocalIndex,
                                                     &NlMatrixSourceAddress,
                                                     &NlMatrixDestAddress);
    }                            /* End of while */

    u4HlMatrixControlIndex = 0;
    u4NlProtocolDirLocalIndex = 0;
    MEMSET (&NlMatrixSourceAddress, 0, sizeof (tIpAddr));
    MEMSET (&NlMatrixDestAddress, 0, sizeof (tIpAddr));

    /* Get First Entry in Al Matrix SD Table for NL/AL protocol */

    pAlMatrixSD = Rmon2AlMatrixGetNextDataIndex (RMON2_ALMATRIXSD_TABLE,
                                                 u4HlMatrixControlIndex,
                                                 u4NlProtocolDirLocalIndex,
                                                 &NlMatrixSourceAddress,
                                                 &NlMatrixDestAddress,
                                                 u4AlProtocolDirLocalIndex);

    while (pAlMatrixSD != NULL)
    {
        u4HlMatrixControlIndex = pAlMatrixSD->u4HlMatrixControlIndex;
        u4NlProtocolDirLocalIndex = pAlMatrixSD->u4NlProtocolDirLocalIndex;
        MEMCPY (&NlMatrixSourceAddress, &(pAlMatrixSD->NlMatrixSDSourceAddress),
                sizeof (tIpAddr));
        MEMCPY (&NlMatrixDestAddress, &(pAlMatrixSD->NlMatrixSDDestAddress),
                sizeof (tIpAddr));

        u4AlProtocolDirLocalIndex = pAlMatrixSD->u4AlProtocolDirLocalIndex;

        if ((pAlMatrixSD->u4NlProtocolDirLocalIndex == u4ProtocolIndex) ||
            (pAlMatrixSD->u4AlProtocolDirLocalIndex == u4ProtocolIndex))
        {
            /* Remove Inter table dependencies */
            Rmon2AlMatrixDelInterDep (pAlMatrixSD);

            pAlMatrixSD->pHlMatrixCtrl->u4HlMatrixControlAlDeletes += 2;

            /* After Reference changes, free Al Matrix entry 
             * for NL/AL protocol */
            Rmon2AlMatrixDelDataEntry (pAlMatrixSD);
        }

        pAlMatrixSD = Rmon2AlMatrixGetNextDataIndex (RMON2_ALMATRIXSD_TABLE,
                                                     u4HlMatrixControlIndex,
                                                     u4NlProtocolDirLocalIndex,
                                                     &NlMatrixSourceAddress,
                                                     &NlMatrixDestAddress,
                                                     u4AlProtocolDirLocalIndex);
    }                            /* End of while */

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2MatrixUpdateProtoChgs\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2MatrixDelDataEntries                                  */
/*                                                                           */
/* Description  : Removes Nl and Al Matrix Data Table Entries for            */
/*                given Control Index                                        */
/*                                                                           */
/* Input        : u4HlMatrixControlIndex                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2MatrixDelDataEntries (UINT4 u4HlMatrixControlIndex)
{
    tNlMatrixSD        *pNlMatrixSD = NULL;
    tAlMatrixSD        *pAlMatrixSD = NULL;

    UINT4               u4NlProtocolDirLocalIndex =
        0, u4AlProtocolDirLocalIndex = 0;
    tIpAddr             NlMatrixSourceAddress, NlMatrixDestAddress;

    MEMSET (&NlMatrixSourceAddress, 0, sizeof (tIpAddr));
    MEMSET (&NlMatrixDestAddress, 0, sizeof (tIpAddr));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2MatrixDelDataEntries\r\n");

    /* Remove Data Entries for this control index from Nl MatrixSD Table */
    pNlMatrixSD = Rmon2NlMatrixGetNextDataIndex (RMON2_NLMATRIXSD_TABLE,
                                                 u4HlMatrixControlIndex,
                                                 u4NlProtocolDirLocalIndex,
                                                 &NlMatrixSourceAddress,
                                                 &NlMatrixDestAddress);

    while (pNlMatrixSD != NULL)
    {
        if (pNlMatrixSD->u4HlMatrixControlIndex != u4HlMatrixControlIndex)
        {
            break;
        }

        u4NlProtocolDirLocalIndex = pNlMatrixSD->u4ProtocolDirLocalIndex;
        MEMCPY (&NlMatrixSourceAddress, &(pNlMatrixSD->NlMatrixSDSourceAddress),
                sizeof (tIpAddr));
        MEMCPY (&NlMatrixDestAddress, &(pNlMatrixSD->NlMatrixSDDestAddress),
                sizeof (tIpAddr));

        /* Remove Inter table references */
        Rmon2NlMatrixDelInterDep (pNlMatrixSD);

        pNlMatrixSD->pHlMatrixCtrl->u4HlMatrixControlNlDeletes += 2;

        /* After Reference changes, free Nl MatrixSD entry */
        Rmon2NlMatrixDelDataEntry (pNlMatrixSD);
        pNlMatrixSD = Rmon2NlMatrixGetNextDataIndex (RMON2_NLMATRIXSD_TABLE,
                                                     u4HlMatrixControlIndex,
                                                     u4NlProtocolDirLocalIndex,
                                                     &NlMatrixSourceAddress,
                                                     &NlMatrixDestAddress);
    }                            /* End of while */

    u4NlProtocolDirLocalIndex = 0;
    MEMSET (&NlMatrixSourceAddress, 0, sizeof (tIpAddr));
    MEMSET (&NlMatrixDestAddress, 0, sizeof (tIpAddr));

    /* Remove Data Entries for this control index from Al MatrixSD Table */
    pAlMatrixSD = Rmon2AlMatrixGetNextDataIndex (RMON2_ALMATRIXSD_TABLE,
                                                 u4HlMatrixControlIndex,
                                                 u4NlProtocolDirLocalIndex,
                                                 &NlMatrixSourceAddress,
                                                 &NlMatrixDestAddress,
                                                 u4AlProtocolDirLocalIndex);

    while (pAlMatrixSD != NULL)
    {
        if (pAlMatrixSD->u4HlMatrixControlIndex != u4HlMatrixControlIndex)
        {
            break;
        }

        u4NlProtocolDirLocalIndex = pAlMatrixSD->u4NlProtocolDirLocalIndex;
        MEMCPY (&NlMatrixSourceAddress, &(pAlMatrixSD->NlMatrixSDSourceAddress),
                sizeof (tIpAddr));
        MEMCPY (&NlMatrixDestAddress, &(pAlMatrixSD->NlMatrixSDDestAddress),
                sizeof (tIpAddr));
        u4AlProtocolDirLocalIndex = pAlMatrixSD->u4AlProtocolDirLocalIndex;

        /* Remove Inter table references */
        Rmon2AlMatrixDelInterDep (pAlMatrixSD);

        pAlMatrixSD->pHlMatrixCtrl->u4HlMatrixControlAlDeletes += 2;

        /* After Reference changes, free Al MatrixSD entry */
        Rmon2AlMatrixDelDataEntry (pAlMatrixSD);
        pAlMatrixSD = Rmon2AlMatrixGetNextDataIndex (RMON2_ALMATRIXSD_TABLE,
                                                     u4HlMatrixControlIndex,
                                                     u4NlProtocolDirLocalIndex,
                                                     &NlMatrixSourceAddress,
                                                     &NlMatrixDestAddress,
                                                     u4AlProtocolDirLocalIndex);
    }                            /* End of while */

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2MatrixDelDataEntries\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2HlMatrixGetNextCtrlIndex                              */
/*                                                                           */
/* Description  : Get First / Next Hl Matrix Ctrl Index                      */
/*                                                                           */
/* Input        : u4HlMatrixControlIndex                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tHlMatrixControl / NULL Pointer                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tHlMatrixControl *
Rmon2HlMatrixGetNextCtrlIndex (UINT4 u4HlMatrixControlIndex)
{
    tHlMatrixControl    HlMatrixCtrl;
    MEMSET (&HlMatrixCtrl, 0, sizeof (tHlMatrixControl));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2HlMatrixGetNextCtrlIndex\r\n");

    HlMatrixCtrl.u4HlMatrixControlIndex = u4HlMatrixControlIndex;

    return ((tHlMatrixControl *) RBTreeGetNext (RMON2_HLMATRIXCTRL_TREE,
                                                (tRBElem *) & HlMatrixCtrl,
                                                NULL));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2HlMatrixAddCtrlEntry                                  */
/*                                                                           */
/* Description  : Allocates memory and Adds Hl Matrix Control Entry into     */
/*                HlMatrixControlRBTree                                      */
/*                                                                           */
/* Input        : pHlMatrixCtrl                                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tHlMatrixControl / NULL Pointer                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tHlMatrixControl *
Rmon2HlMatrixAddCtrlEntry (UINT4 u4HlMatrixControlIndex)
{
    tHlMatrixControl   *pHlMatrixCtrl = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2HlMatrixAddCtrlEntry\r\n");

    if ((pHlMatrixCtrl = (tHlMatrixControl *)
         (MemAllocMemBlk (RMON2_HLMATRIXCTRL_POOL))) == NULL)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for HlMatrixControlEntry\r\n");
        return NULL;
    }
    MEMSET (pHlMatrixCtrl, 0, sizeof (tHlMatrixControl));
    pHlMatrixCtrl->u4HlMatrixControlIndex = u4HlMatrixControlIndex;

    if (RBTreeAdd (RMON2_HLMATRIXCTRL_TREE, (tRBElem *) pHlMatrixCtrl)
        == RB_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Rmon2HlMatrixAddCtrlEntry: HlMatrixControlEntry is added\r\n");
        return pHlMatrixCtrl;
    }
    RMON2_TRC (RMON2_CRITICAL_TRC,
               "Rmon2HlMatrixAddCtrlEntry: Adding HlMatrixControlEntry Failed\r\n");

    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (RMON2_HLMATRIXCTRL_POOL, (UINT1 *) pHlMatrixCtrl);

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2HlMatrixAddCtrlEntry\r\n");
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2HlMatrixDelCtrlEntry                                  */
/*                                                                           */
/* Description  : Releases memory and Removes Hl Matrix Control Entry from   */
/*                HlMatrixControlRBTree                                      */
/*                                                                           */
/* Input        : pHlMatrixCtrl                                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rmon2HlMatrixDelCtrlEntry (tHlMatrixControl * pHlMatrixCtrl)
{
    tPortList          *pVlanPortList = NULL;
    tCfaIfInfo          IfInfo;
    UINT2               u2VlanId = 0;

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2HlMatrixDelCtrlEntry\r\n");
    pVlanPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pVlanPortList == NULL)
    {
        RMON2_TRC (RMON2_FN_ENTRY,
                   "Rmon2HlMatrixDelCtrlEntry: "
                   "Error in allocating memory for pVlanPortList\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pVlanPortList, 0, sizeof (tPortList));

    if (CfaGetIfInfo (pHlMatrixCtrl->u4HlMatrixControlDataSource, &IfInfo) !=
        CFA_FAILURE)
    {
        if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
            (CfaGetVlanId
             (pHlMatrixCtrl->u4HlMatrixControlDataSource,
              &u2VlanId) != CFA_FAILURE))
        {
            L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList);
        }
    }
#ifdef NPAPI_WANTED
    /* Remove Probe support from H/W Table */
    Rmonv2FsRMONv2DisableProbe (pHlMatrixCtrl->u4HlMatrixControlDataSource,
                                u2VlanId, *pVlanPortList);
#endif

    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
    /* Remove HlMatrix Ctrl node from HlMatrixControlRBTree */
    RBTreeRem (RMON2_HLMATRIXCTRL_TREE, (tRBElem *) pHlMatrixCtrl);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (RMON2_HLMATRIXCTRL_POOL,
                            (UINT1 *) pHlMatrixCtrl) != MEM_SUCCESS)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for HlMatrixControlEntry\r\n");
        return OSIX_FAILURE;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2HlMatrixDelCtrlEntry\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2HlMatrixGetCtrlEntry                                  */
/*                                                                           */
/* Description  : Get Hl Matrix Ctrl Entry for the given control index       */
/*                                                                           */
/* Input        : u4HlMatrixControlIndex                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tHlMatrixControl / NULL Pointer                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tHlMatrixControl *
Rmon2HlMatrixGetCtrlEntry (UINT4 u4HlMatrixControlIndex)
{
    tHlMatrixControl    HlMatrixCtrl;
    MEMSET (&HlMatrixCtrl, 0, sizeof (tHlMatrixControl));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2HlMatrixGetCtrlEntry\r\n");

    HlMatrixCtrl.u4HlMatrixControlIndex = u4HlMatrixControlIndex;

    return ((tHlMatrixControl *) RBTreeGet (RMON2_HLMATRIXCTRL_TREE,
                                            (tRBElem *) & HlMatrixCtrl));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2NlMatrixGetNextDataIndex                              */
/*                                                                           */
/* Description  : Get First / Next Nl Matrix Index                           */
/*                                                                           */
/* Input        : u4HlMatrixControlIndex, u4ProtocolDirLocalIndex            */
/*                pNlMatrixSourceAddress, pNlMatrixDestAddress               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tNlMatrixSD / NULL Pointer                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC tNlMatrixSD *
Rmon2NlMatrixGetNextDataIndex (UINT4 u4NlMatrixTableId,
                               UINT4 u4HlMatrixControlIndex,
                               UINT4 u4ProtocolDirLocalIndex,
                               tIpAddr * pNlMatrixSourceAddress,
                               tIpAddr * pNlMatrixDestAddress)
{
    tNlMatrixSD        *pNlMatrixSD = NULL;
    tNlMatrixSD         NlMatrixSD;
    MEMSET (&NlMatrixSD, 0, sizeof (tNlMatrixSD));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2NlMatrixGetNextDataIndex\r\n");
    if ((u4HlMatrixControlIndex != 0) && (u4ProtocolDirLocalIndex == 0))
    {
        pNlMatrixSD = (tNlMatrixSD *) RBTreeGetFirst
            (RMON2_NLMATRIX_TREE[u4NlMatrixTableId]);
        while (pNlMatrixSD != NULL)
        {
            if (pNlMatrixSD->u4HlMatrixControlIndex == u4HlMatrixControlIndex)
            {
                break;
            }
            MEMCPY (&NlMatrixSD, pNlMatrixSD, sizeof (tNlMatrixSD));

            pNlMatrixSD = (tNlMatrixSD *) RBTreeGetNext
                (RMON2_NLMATRIX_TREE[u4NlMatrixTableId],
                 (tRBElem *) & NlMatrixSD, NULL);
        }
    }
    else
    {

        NlMatrixSD.u4HlMatrixControlIndex = u4HlMatrixControlIndex;
        NlMatrixSD.u4ProtocolDirLocalIndex = u4ProtocolDirLocalIndex;
        MEMCPY (&(NlMatrixSD.NlMatrixSDSourceAddress), pNlMatrixSourceAddress,
                sizeof (tIpAddr));
        MEMCPY (&(NlMatrixSD.NlMatrixSDDestAddress), pNlMatrixDestAddress,
                sizeof (tIpAddr));
        pNlMatrixSD =
            (tNlMatrixSD *)
            RBTreeGetNext (RMON2_NLMATRIX_TREE[u4NlMatrixTableId],
                           (tRBElem *) & NlMatrixSD, NULL);
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2NlMatrixGetNextDataIndex\r\n");
    return pNlMatrixSD;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2NlMatrixAddDataEntry                                  */
/*                                                                           */
/* Description  : Allocates memory and Adds Nl Matrix Entry into             */
/*                NlMatrixSDRBTree                                           */
/*                                                                           */
/* Input        : u4HlMatrixControlIndex, u4ProtocolDirLocalIndex            */
/*                pNlMatrixSourceAddress, pNlMatrixDestAddress               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tNlMatrixSD / NULL Pointer                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC tNlMatrixSD *
Rmon2NlMatrixAddDataEntry (UINT4 u4HlMatrixControlIndex,
                           UINT4 u4ProtocolDirLocalIndex,
                           tIpAddr * pNlMatrixSourceAddress,
                           tIpAddr * pNlMatrixDestAddress)
{
    tNlMatrixSD        *pNlMatrixSD = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2NlMatrixAddDataEntry\r\n");

    if ((pNlMatrixSD = (tNlMatrixSD *)
         (MemAllocMemBlk (RMON2_NLMATRIXSD_POOL))) == NULL)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for NlMatrixSDEntry\r\n");
        return NULL;
    }

    MEMSET (pNlMatrixSD, 0, sizeof (tNlMatrixSD));

    pNlMatrixSD->u4HlMatrixControlIndex = u4HlMatrixControlIndex;
    pNlMatrixSD->u4ProtocolDirLocalIndex = u4ProtocolDirLocalIndex;
    pNlMatrixSD->u4NlMatrixSDTimeMark = OsixGetSysUpTime ();
    pNlMatrixSD->u4NlMatrixSDCreateTime = OsixGetSysUpTime ();

    MEMCPY (&(pNlMatrixSD->NlMatrixSDSourceAddress), pNlMatrixSourceAddress,
            sizeof (tIpAddr));
    MEMCPY (&(pNlMatrixSD->NlMatrixSDDestAddress), pNlMatrixDestAddress,
            sizeof (tIpAddr));

    if (RBTreeAdd (RMON2_NLMATRIX_TREE[RMON2_NLMATRIXSD_TABLE],
                   (tRBElem *) pNlMatrixSD) == RB_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Rmon2NlMatrixAddDataEntry: NlMatrixSDEntry is added\r\n");
        if (RBTreeAdd (RMON2_NLMATRIX_TREE[RMON2_NLMATRIXDS_TABLE],
                       (tRBElem *) pNlMatrixSD) == RB_SUCCESS)
        {
            RMON2_TRC (RMON2_DEBUG_TRC,
                       "Rmon2NlMatrixAddDataEntry: NlMatrixDSEntry is added\r\n");
            return pNlMatrixSD;
        }
        RMON2_TRC (RMON2_CRITICAL_TRC,
                   "Rmon2NlMatrixAddDataEntry: Adding NlMatrixDSEntry Failed\r\n");
        RBTreeRem (RMON2_NLMATRIX_TREE[RMON2_NLMATRIXSD_TABLE],
                   (tRBElem *) pNlMatrixSD);
    }
    RMON2_TRC (RMON2_CRITICAL_TRC,
               "Rmon2NlMatrixAddDataEntry: Adding NlMatrixSDEntry Failed\r\n");
    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (RMON2_NLMATRIXSD_POOL, (UINT1 *) pNlMatrixSD);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2NlMatrixAddDataEntry\r\n");
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2NlMatrixDelDataEntry                                  */
/*                                                                           */
/* Description  : Releases memory and Removes Nl Matrix Entry from           */
/*                NlMatrixSDRBTree                                           */
/*                                                                           */
/* Input        : pNlMatrixSD                                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rmon2NlMatrixDelDataEntry (tNlMatrixSD * pNlMatrixSD)
{

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2NlMatrixDelDataEntry\r\n");

    /* Remove Nl MatrixSD node from NlMatrixSDRBTree */
    RBTreeRem (RMON2_NLMATRIX_TREE[RMON2_NLMATRIXSD_TABLE],
               (tRBElem *) pNlMatrixSD);

    /* Remove Nl MatrixDS node from NlMatrixDSRBTree */
    RBTreeRem (RMON2_NLMATRIX_TREE[RMON2_NLMATRIXDS_TABLE],
               (tRBElem *) pNlMatrixSD);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (RMON2_NLMATRIXSD_POOL,
                            (UINT1 *) pNlMatrixSD) != MEM_SUCCESS)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for NlMatrixSDEntry\r\n");
        return OSIX_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2NlMatrixDelDataEntry\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2NlMatrixGetDataEntry                                  */
/*                                                                           */
/* Description  : Get Nl Matrix Entry for the given index                    */
/*                                                                           */
/* Input        : u4HlMatrixControlIndex, u4ProtocolDirLocalIndex            */
/*                pNlMatrixSourceAddress, pNlMatrixDestAddress               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tNlMatrixSD / NULL Pointer                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC tNlMatrixSD *
Rmon2NlMatrixGetDataEntry (UINT4 u4NlMatrixTableId,
                           UINT4 u4HlMatrixControlIndex,
                           UINT4 u4ProtocolDirLocalIndex,
                           tIpAddr * pNlMatrixSourceAddress,
                           tIpAddr * pNlMatrixDestAddress)
{
    tNlMatrixSD        *pNlMatrixSD = NULL;
    tNlMatrixSD         NlMatrixSD;

    MEMSET (&NlMatrixSD, 0, sizeof (tNlMatrixSD));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2NlMatrixGetDataEntry\r\n");

    NlMatrixSD.u4HlMatrixControlIndex = u4HlMatrixControlIndex;
    NlMatrixSD.u4ProtocolDirLocalIndex = u4ProtocolDirLocalIndex;
    MEMCPY (&(NlMatrixSD.NlMatrixSDSourceAddress), pNlMatrixSourceAddress,
            sizeof (tIpAddr));
    MEMCPY (&(NlMatrixSD.NlMatrixSDDestAddress), pNlMatrixDestAddress,
            sizeof (tIpAddr));

    pNlMatrixSD =
        (tNlMatrixSD *) RBTreeGet (RMON2_NLMATRIX_TREE[u4NlMatrixTableId],
                                   (tRBElem *) & NlMatrixSD);
    return pNlMatrixSD;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2NlMatrixDelInterDep                                   */
/*                                                                           */
/* Description  : Removes the inter-dependencies and re-order the links      */
/*                                                                           */
/* Input        : pNlMatrixSD                                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2NlMatrixDelInterDep (tNlMatrixSD * pNlMatrixSD)
{
    tRmon2PktInfo      *pPktInfo = NULL;
    tPktHeader          PktHdr;

    MEMSET (&PktHdr, 0, sizeof (tPktHeader));
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2NlMatrixDelInterDep\r\n");

    if (pNlMatrixSD->pPrevNlMatrixSD == NULL)
    {
        /* Update PktInfo nodes to point next pNlMatrixSD Entry */
        pPktInfo = Rmon2PktInfoGetNextIndex (&PktHdr);

        while (pPktInfo != NULL)
        {
            if (pPktInfo->pNlMatrixSD == pNlMatrixSD)
            {
                pPktInfo->pNlMatrixSD = pNlMatrixSD->pNextNlMatrixSD;
                if (pPktInfo->pNlMatrixSD != NULL)
                {
                    pPktInfo->pNlMatrixSD->pPrevNlMatrixSD = NULL;
                }
            }
            pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
        }
    }
    else if (pNlMatrixSD->pNextNlMatrixSD != NULL)
    {
        pNlMatrixSD->pPrevNlMatrixSD->pNextNlMatrixSD =
            pNlMatrixSD->pNextNlMatrixSD;
    }
    else
    {
        pNlMatrixSD->pPrevNlMatrixSD->pNextNlMatrixSD = NULL;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2NlMatrixDelInterDep\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AlMatrixGetNextDataIndex                              */
/*                                                                           */
/* Description  : Get First / Next Al Host Index                             */
/*                                                                           */
/* Input        : u4HlMatrixControlIndex, u4NlProtocolDirLocalIndex,         */
/*                pNlMatrixSourceAddress, pNlMatrixDestAddress,              */
/*                u4AlProtocolDirLocalIndex                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tAlMatrixSD / NULL Pointer                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC tAlMatrixSD *
Rmon2AlMatrixGetNextDataIndex (UINT4 u4AlMatrixTableId,
                               UINT4 u4HlMatrixControlIndex,
                               UINT4 u4NlProtocolDirLocalIndex,
                               tIpAddr * pNlMatrixSourceAddress,
                               tIpAddr * pNlMatrixDestAddress,
                               UINT4 u4AlProtocolDirLocalIndex)
{
    tAlMatrixSD        *pAlMatrixSD = NULL;
    tAlMatrixSD         AlMatrixSD;
    MEMSET (&AlMatrixSD, 0, sizeof (tAlMatrixSD));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AlMatrixGetNextDataIndex\r\n");

    if ((u4HlMatrixControlIndex != 0) && (u4NlProtocolDirLocalIndex == 0))
    {
        pAlMatrixSD = (tAlMatrixSD *) RBTreeGetFirst
            (RMON2_ALMATRIX_TREE[u4AlMatrixTableId]);
        while (pAlMatrixSD != NULL)
        {
            if (pAlMatrixSD->u4HlMatrixControlIndex == u4HlMatrixControlIndex)
            {
                break;
            }
            MEMCPY (&AlMatrixSD, pAlMatrixSD, sizeof (tAlMatrixSD));

            pAlMatrixSD = (tAlMatrixSD *) RBTreeGetNext
                (RMON2_ALMATRIX_TREE[u4AlMatrixTableId],
                 (tRBElem *) & AlMatrixSD, NULL);
        }
    }
    else
    {
        AlMatrixSD.u4HlMatrixControlIndex = u4HlMatrixControlIndex;
        AlMatrixSD.u4NlProtocolDirLocalIndex = u4NlProtocolDirLocalIndex;
        MEMCPY (&(AlMatrixSD.NlMatrixSDSourceAddress), pNlMatrixSourceAddress,
                sizeof (tIpAddr));
        MEMCPY (&(AlMatrixSD.NlMatrixSDDestAddress), pNlMatrixDestAddress,
                sizeof (tIpAddr));
        AlMatrixSD.u4AlProtocolDirLocalIndex = u4AlProtocolDirLocalIndex;
        pAlMatrixSD =
            (tAlMatrixSD *)
            RBTreeGetNext (RMON2_ALMATRIX_TREE[u4AlMatrixTableId],
                           (tRBElem *) & AlMatrixSD, NULL);
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AlMatrixGetNextDataIndex\r\n");
    return pAlMatrixSD;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AlMatrixAddDataEntry                                  */
/*                                                                           */
/* Description  : Allocates memory and Adds Al MatrixSD Entry into           */
/*                AlMatrixSDRBTree                                           */
/*                                                                           */
/* Input        : u4HlMatrixControlIndex, u4NlProtocolDirLocalIndex,         */
/*                pNlMatrixSourceAddress, pNlMatrixDestAddress,              */
/*                u4AlProtocolDirLocalIndex                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tAlMatrixSD / NULL Pointer                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC tAlMatrixSD *
Rmon2AlMatrixAddDataEntry (UINT4 u4HlMatrixControlIndex,
                           UINT4 u4NlProtocolDirLocalIndex,
                           tIpAddr * pNlMatrixSourceAddress,
                           tIpAddr * pNlMatrixDestAddress,
                           UINT4 u4AlProtocolDirLocalIndex)
{
    tAlMatrixSD        *pAlMatrixSD = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AlMatrixAddDataEntry\r\n");

    if ((pAlMatrixSD = (tAlMatrixSD *)
         (MemAllocMemBlk (RMON2_ALMATRIXSD_POOL))) == NULL)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for AlMatrixSDEntry\r\n");
        return NULL;
    }

    MEMSET (pAlMatrixSD, 0, sizeof (tAlMatrixSD));

    pAlMatrixSD->u4HlMatrixControlIndex = u4HlMatrixControlIndex;
    pAlMatrixSD->u4NlProtocolDirLocalIndex = u4NlProtocolDirLocalIndex;
    MEMCPY (&(pAlMatrixSD->NlMatrixSDSourceAddress),
            pNlMatrixSourceAddress, sizeof (tIpAddr));
    MEMCPY (&(pAlMatrixSD->NlMatrixSDDestAddress),
            pNlMatrixDestAddress, sizeof (tIpAddr));

    pAlMatrixSD->u4AlProtocolDirLocalIndex = u4AlProtocolDirLocalIndex;
    pAlMatrixSD->u4AlMatrixSDTimeMark = OsixGetSysUpTime ();
    pAlMatrixSD->u4AlMatrixSDCreateTime = OsixGetSysUpTime ();

    if (RBTreeAdd (RMON2_ALMATRIX_TREE[RMON2_ALMATRIXSD_TABLE],
                   (tRBElem *) pAlMatrixSD) == RB_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Rmon2AlMatrixAddDataEntry: AlMatrixSDEntry is added\r\n");
        if (RBTreeAdd (RMON2_ALMATRIX_TREE[RMON2_ALMATRIXDS_TABLE],
                       (tRBElem *) pAlMatrixSD) == RB_SUCCESS)
        {
            RMON2_TRC (RMON2_DEBUG_TRC,
                       "Rmon2AlMatrixAddDataEntry: AlMatrixDSEntry is added\r\n");
            return pAlMatrixSD;
        }
        RMON2_TRC (RMON2_CRITICAL_TRC,
                   "Rmon2AlMatrixAddDataEntry: Adding AlMatrixDSEntry Failed\r\n");
        RBTreeRem (RMON2_ALMATRIX_TREE[RMON2_ALMATRIXSD_TABLE],
                   (tRBElem *) pAlMatrixSD);

    }
    RMON2_TRC (RMON2_CRITICAL_TRC,
               "Rmon2AlMatrixAddDataEntry: Adding AlMatrixSDEntry Failed\r\n");
    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (RMON2_ALMATRIXSD_POOL, (UINT1 *) pAlMatrixSD);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AlMatrixAddDataEntry\r\n");
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AlMatrixDelDataEntry                                  */
/*                                                                           */
/* Description  : Releases memory and Removes Al Host Entry from             */
/*                AlMatrixSDRBTree                                           */
/*                                                                           */
/* Input        : pAlMatrixSD                                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rmon2AlMatrixDelDataEntry (tAlMatrixSD * pAlMatrixSD)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AlMatrixDelDataEntry\r\n");

    /* Remove AlMatrixSD node from AlMatrixSDRBTree */
    RBTreeRem (RMON2_ALMATRIX_TREE[RMON2_ALMATRIXSD_TABLE],
               (tRBElem *) pAlMatrixSD);

    /* Remove AlMatrixSD node from AlMatrixDSRBTree */
    RBTreeRem (RMON2_ALMATRIX_TREE[RMON2_ALMATRIXDS_TABLE],
               (tRBElem *) pAlMatrixSD);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (RMON2_ALMATRIXSD_POOL,
                            (UINT1 *) pAlMatrixSD) != MEM_SUCCESS)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for AlMatrixSDEntry\r\n");
        return OSIX_FAILURE;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AlMatrixDelDataEntry\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AlMatrixGetDataEntry                                  */
/*                                                                           */
/* Description  : Get Al Host Entry for the given index                      */
/*                                                                           */
/* Input        : u4HlMatrixControlIndex, u4NlProtocolDirLocalIndex,         */
/*                pNlMatrixSourceAddress, pNlMatrixDestAddress               */
/*                u4AlProtocolDirLocalIndex                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tAlMatrixSD / NULL Pointer                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC tAlMatrixSD *
Rmon2AlMatrixGetDataEntry (UINT4 u4AlMatrixTableId,
                           UINT4 u4HlMatrixControlIndex,
                           UINT4 u4NlProtocolDirLocalIndex,
                           tIpAddr * pNlMatrixSourceAddress,
                           tIpAddr * pNlMatrixDestAddress,
                           UINT4 u4AlProtocolDirLocalIndex)
{
    tAlMatrixSD        *pAlMatrixSD = NULL;
    tAlMatrixSD         AlMatrixSD;
    MEMSET (&AlMatrixSD, 0, sizeof (tAlMatrixSD));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AlMatrixGetDataEntry\r\n");

    AlMatrixSD.u4HlMatrixControlIndex = u4HlMatrixControlIndex;
    AlMatrixSD.u4NlProtocolDirLocalIndex = u4NlProtocolDirLocalIndex;
    MEMCPY (&(AlMatrixSD.NlMatrixSDSourceAddress), pNlMatrixSourceAddress,
            sizeof (tIpAddr));
    MEMCPY (&(AlMatrixSD.NlMatrixSDDestAddress), pNlMatrixDestAddress,
            sizeof (tIpAddr));
    AlMatrixSD.u4AlProtocolDirLocalIndex = u4AlProtocolDirLocalIndex;

    pAlMatrixSD =
        (tAlMatrixSD *) RBTreeGet (RMON2_ALMATRIX_TREE[u4AlMatrixTableId],
                                   (tRBElem *) & AlMatrixSD);
    return pAlMatrixSD;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AlMatrixDelInterDep                                   */
/*                                                                           */
/* Description  : Removes the inter-dependencies and re-order the links      */
/*                                                                           */
/* Input        : pAlMatrixSD                                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2AlMatrixDelInterDep (tAlMatrixSD * pAlMatrixSD)
{
    tRmon2PktInfo      *pPktInfo = NULL;
    tPktHeader          PktHdr;

    MEMSET (&PktHdr, 0, sizeof (tPktHeader));
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AlMatrixDelInterDep\r\n");

    if (pAlMatrixSD->pPrevAlMatrixSD == NULL)
    {
        /* Update PktInfo nodes to point next pAlMatrixSD Entry */
        pPktInfo = Rmon2PktInfoGetNextIndex (&PktHdr);

        while (pPktInfo != NULL)
        {
            if (pPktInfo->pAlMatrixSD == pAlMatrixSD)
            {
                pPktInfo->pAlMatrixSD = pAlMatrixSD->pNextAlMatrixSD;
                if (pPktInfo->pAlMatrixSD != NULL)
                {
                    pPktInfo->pAlMatrixSD->pPrevAlMatrixSD = NULL;
                }
            }

            pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
        }
    }
    else if (pAlMatrixSD->pNextAlMatrixSD != NULL)
    {
        pAlMatrixSD->pPrevAlMatrixSD->pNextAlMatrixSD =
            pAlMatrixSD->pNextAlMatrixSD;
    }
    else
    {
        pAlMatrixSD->pPrevAlMatrixSD->pNextAlMatrixSD = NULL;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AlMatrixDelInterDep\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2MatrixPopulateDataEntries                             */
/*                                                                           */
/* Description  : Populates data entries if the same data source is already  */
/*                present and Adds the inter-dependencies b/w other tables   */
/*                                                                           */
/* Input        : pHlMatrixCtrl                                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2MatrixPopulateDataEntries (tHlMatrixControl * pHlMatrixCtrl)
{
    tRmon2PktInfo      *pPktInfo = NULL;
    tProtocolDir       *pNlProtDirEntry = NULL, *pAlProtDirEntry = NULL;
    tNlMatrixSD        *pNlMatrixSD = NULL, *pNewNlMatrixSD = NULL;
    tAlMatrixSD        *pAlMatrixSD = NULL, *pNewAlMatrixSD = NULL;
    tPktHeader          PktHdr;

    MEMSET (&PktHdr, 0, sizeof (tPktHeader));

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY Rmon2MatrixPopulateDataEntries\r\n");

    /* Check for the presence of data source in PktInfo table */
    pPktInfo = Rmon2PktInfoGetNextIndex (&PktHdr);
    while (pPktInfo != NULL)
    {
        if (pPktInfo->PktHdr.u4IfIndex ==
            pHlMatrixCtrl->u4HlMatrixControlDataSource)
        {
            if (pHlMatrixCtrl->u4HlMatrixControlNlMaxDesiredSupported == 0)
            {
                pHlMatrixCtrl->u4HlMatrixControlNlDroppedFrames++;
                pHlMatrixCtrl->u4HlMatrixControlAlDroppedFrames++;
                pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
                continue;
            }

            pNlProtDirEntry =
                Rmon2PDirGetProtocolNode (pPktInfo->u4ProtoNLIndex);
            if (pNlProtDirEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "Get Protocol Entry failed for NLProtocolLocIndex\r\n");
                pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
                continue;
            }

            if ((pPktInfo->u4ProtoNLIndex != 0) &&
                (pNlProtDirEntry->u4ProtocolDirMatrixConfig == SUPPORTED_ON))
            {
                /* Check whether NlMatrixSD data entries reached max limit */
                if ((pHlMatrixCtrl->u4HlMatrixControlNlInserts -
                     pHlMatrixCtrl->u4HlMatrixControlNlDeletes) >=
                    pHlMatrixCtrl->u4HlMatrixControlNlMaxDesiredSupported)
                {
                    pNewNlMatrixSD = Rmon2NlMatrixGiveLRUEntry
                        (pHlMatrixCtrl->u4HlMatrixControlIndex);

                    if (pNewNlMatrixSD != NULL)
                    {
                        RMON2_TRC (RMON2_DEBUG_TRC,
                                   "Deleting NlMatrixSD LRU Entry\r\n");

                        /* Remove Inter table references */
                        Rmon2NlMatrixDelInterDep (pNewNlMatrixSD);

                        pNewNlMatrixSD->pHlMatrixCtrl->
                            u4HlMatrixControlNlDeletes += 2;

                        /* After Reference changes, free NlMatrixSD entry */
                        Rmon2NlMatrixDelDataEntry (pNewNlMatrixSD);

                        pNewNlMatrixSD = NULL;
                    }
                }
                /* Add NlMatrix SD Entry */
                pNewNlMatrixSD =
                    Rmon2NlMatrixAddDataEntry (pHlMatrixCtrl->
                                               u4HlMatrixControlIndex,
                                               pPktInfo->u4ProtoNLIndex,
                                               &(pPktInfo->PktHdr.SrcIp),
                                               &(pPktInfo->PktHdr.DstIp));

                if (pNewNlMatrixSD == NULL)
                {
                    pHlMatrixCtrl->u4HlMatrixControlNlDroppedFrames++;
                    pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
                    continue;
                }
                else
                {
                    pNewNlMatrixSD->pHlMatrixCtrl = pHlMatrixCtrl;
                    pHlMatrixCtrl->u4HlMatrixControlNlInserts += 2;
                }

                if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV4)
                {
                    pNewNlMatrixSD->u4Rmon2NlMatrixSDIpAddrLen =
                        RMON2_IPV4_MAX_LEN;
                }
                else if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV6)
                {
                    pNewNlMatrixSD->u4Rmon2NlMatrixSDIpAddrLen =
                        RMON2_IPV6_MAX_LEN;
                }

                pNlMatrixSD = pPktInfo->pNlMatrixSD;
                if (pNlMatrixSD == NULL)
                {
                    pPktInfo->pNlMatrixSD = pNewNlMatrixSD;
                }
                else
                {
                    while (pNlMatrixSD->pNextNlMatrixSD != NULL)
                    {
                        pNlMatrixSD = pNlMatrixSD->pNextNlMatrixSD;
                    }
                    pNlMatrixSD->pNextNlMatrixSD = pNewNlMatrixSD;
                    pNewNlMatrixSD->pPrevNlMatrixSD = pNlMatrixSD;
                }
            }

            pAlProtDirEntry =
                Rmon2PDirGetProtocolNode (pPktInfo->u4ProtoL5ALIndex);
            if (pAlProtDirEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "Get Protocol Entry failed for L5ProtocolLocIndex\r\n");
                pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
                continue;
            }

            if ((pPktInfo->u4ProtoNLIndex != 0) &&
                (pPktInfo->u4ProtoL5ALIndex != 0) &&
                (pNlProtDirEntry->u4ProtocolDirMatrixConfig == SUPPORTED_ON) &&
                (pAlProtDirEntry->u4ProtocolDirMatrixConfig == SUPPORTED_ON))
            {
                pNewAlMatrixSD = NULL;

                /* Add AlMatrixSD Entry for L5 Protocol only if 
                 * NlMatrixSD Entry is present */
                if (pNewNlMatrixSD != NULL)
                {
                    /* Check whether AlMatrixSD data entries reached max limit */
                    if ((pHlMatrixCtrl->u4HlMatrixControlAlInserts -
                         pHlMatrixCtrl->u4HlMatrixControlAlDeletes) >=
                        pHlMatrixCtrl->u4HlMatrixControlAlMaxDesiredSupported)
                    {
                        pNewAlMatrixSD = Rmon2AlMatrixGiveLRUEntry
                            (pHlMatrixCtrl->u4HlMatrixControlIndex);

                        if (pNewAlMatrixSD != NULL)
                        {
                            RMON2_TRC (RMON2_DEBUG_TRC,
                                       "Deleting AlMatrixSD LRU Entry\r\n");

                            /* Remove Inter table references */
                            Rmon2AlMatrixDelInterDep (pNewAlMatrixSD);

                            pNewAlMatrixSD->pHlMatrixCtrl->
                                u4HlMatrixControlAlDeletes += 2;

                            /* After Reference changes, free AlMatrixSD entry */
                            Rmon2AlMatrixDelDataEntry (pNewAlMatrixSD);

                            pNewAlMatrixSD = NULL;
                        }
                    }

                    /* Add AlMatrix SD Entry for L5 Protocol */
                    pNewAlMatrixSD =
                        Rmon2AlMatrixAddDataEntry (pHlMatrixCtrl->
                                                   u4HlMatrixControlIndex,
                                                   pPktInfo->u4ProtoNLIndex,
                                                   &(pPktInfo->PktHdr.SrcIp),
                                                   &(pPktInfo->PktHdr.DstIp),
                                                   pPktInfo->u4ProtoL5ALIndex);

                    if (pNewAlMatrixSD == NULL)
                    {
                        pHlMatrixCtrl->u4HlMatrixControlAlDroppedFrames++;
                        pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
                        continue;
                    }
                    else
                    {
                        pNewAlMatrixSD->pHlMatrixCtrl = pHlMatrixCtrl;
                        pHlMatrixCtrl->u4HlMatrixControlAlInserts += 2;
                    }
                }

                if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV4)
                {
                    pNewAlMatrixSD->u4Rmon2NlMatrixSDIpAddrLen =
                        RMON2_IPV4_MAX_LEN;
                }
                else if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV6)
                {
                    pNewAlMatrixSD->u4Rmon2NlMatrixSDIpAddrLen =
                        RMON2_IPV6_MAX_LEN;
                }

                pAlMatrixSD = pPktInfo->pAlMatrixSD;
                if (pAlMatrixSD == NULL)
                {
                    pPktInfo->pAlMatrixSD = pNewAlMatrixSD;
                }
                else
                {
                    while (pAlMatrixSD->pNextAlMatrixSD != NULL)
                    {
                        pAlMatrixSD = pAlMatrixSD->pNextAlMatrixSD;
                    }
                    pAlMatrixSD->pNextAlMatrixSD = pNewAlMatrixSD;
                    pNewAlMatrixSD->pPrevAlMatrixSD = pAlMatrixSD;
                }
            }

            pAlProtDirEntry =
                Rmon2PDirGetProtocolNode (pPktInfo->u4ProtoL4ALIndex);
            if (pAlProtDirEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "Get Protocol Entry failed for L4ProtocolLocIndex\r\n");
                pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
                continue;
            }

            if ((pPktInfo->u4ProtoNLIndex != 0) &&
                (pPktInfo->u4ProtoL4ALIndex != 0) &&
                (pNlProtDirEntry->u4ProtocolDirMatrixConfig == SUPPORTED_ON) &&
                (pAlProtDirEntry->u4ProtocolDirMatrixConfig == SUPPORTED_ON))
            {
                /* Add AlMatrixSD Entry for L4 Protocol only if 
                 * NlMatrixSD Entry is present */

                if (pNewNlMatrixSD != NULL)
                {
                    /* Check whether AlMatrixSD data entries reached max limit */
                    if ((pHlMatrixCtrl->u4HlMatrixControlAlInserts -
                         pHlMatrixCtrl->u4HlMatrixControlAlDeletes) >=
                        pHlMatrixCtrl->u4HlMatrixControlAlMaxDesiredSupported)
                    {
                        pNewAlMatrixSD = Rmon2AlMatrixGiveLRUEntry
                            (pHlMatrixCtrl->u4HlMatrixControlIndex);

                        if (pNewAlMatrixSD != NULL)
                        {
                            RMON2_TRC (RMON2_DEBUG_TRC,
                                       "Deleting AlMatrixSD LRU Entry\r\n");

                            /* Remove Inter table references */
                            Rmon2AlMatrixDelInterDep (pNewAlMatrixSD);

                            pNewAlMatrixSD->pHlMatrixCtrl->
                                u4HlMatrixControlAlDeletes += 2;

                            /* After Reference changes, free AlMatrixSD entry */
                            Rmon2AlMatrixDelDataEntry (pNewAlMatrixSD);

                            pNewAlMatrixSD = NULL;
                        }
                    }

                    /* Add AlMatrix SD Entry for L4 Protocol */
                    pNewAlMatrixSD =
                        Rmon2AlMatrixAddDataEntry (pHlMatrixCtrl->
                                                   u4HlMatrixControlIndex,
                                                   pPktInfo->u4ProtoNLIndex,
                                                   &(pPktInfo->PktHdr.SrcIp),
                                                   &(pPktInfo->PktHdr.DstIp),
                                                   pPktInfo->u4ProtoL4ALIndex);

                    if (pNewAlMatrixSD == NULL)
                    {
                        pHlMatrixCtrl->u4HlMatrixControlAlDroppedFrames++;
                        pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
                        continue;
                    }
                    else
                    {
                        pNewAlMatrixSD->pHlMatrixCtrl = pHlMatrixCtrl;
                        pHlMatrixCtrl->u4HlMatrixControlAlInserts += 2;
                    }
                }
                if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV4)
                {
                    pNewAlMatrixSD->u4Rmon2NlMatrixSDIpAddrLen =
                        RMON2_IPV4_MAX_LEN;
                }
                else if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV6)
                {
                    pNewAlMatrixSD->u4Rmon2NlMatrixSDIpAddrLen =
                        RMON2_IPV6_MAX_LEN;
                }
                pAlMatrixSD = pPktInfo->pAlMatrixSD;
                if (pAlMatrixSD == NULL)
                {
                    pPktInfo->pAlMatrixSD = pNewAlMatrixSD;
                }
                else
                {
                    while (pAlMatrixSD->pNextAlMatrixSD != NULL)
                    {
                        pAlMatrixSD = pAlMatrixSD->pNextAlMatrixSD;
                    }
                    pAlMatrixSD->pNextAlMatrixSD = pNewAlMatrixSD;
                    pNewAlMatrixSD->pPrevAlMatrixSD = pAlMatrixSD;
                }
            }

        }                        /* End of if check */
        pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);

    }                            /* End of While - PktInfo */
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2MatrixPopulateDataEntries\r\n");
    return;
}
