/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * $Id: rmn2mxlw.c,v 1.11 2014/02/05 12:29:00 siva Exp $
 *
 * Description: This file contains the low level nmh routines for
 *              RMON2 Network Layer and Application Layer 
 *              Matrix module.            
 *
 *******************************************************************/
#include "rmn2inc.h"

/* LOW LEVEL Routines for Table : HlMatrixControlTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceHlMatrixControlTable
Input       :  The Indices
               HlMatrixControlIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceHlMatrixControlTable (INT4 i4HlMatrixControlIndex)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  "
               "nmhValidateIndexInstanceHlMatrixControlTable \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HlMatrixControlIndex < RMON2_ONE) ||
        (i4HlMatrixControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid HlMatrixControlIndex \n");
        return SNMP_FAILURE;
    }
    pHlMatrixCtrlEntry =
        Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);

    if (pHlMatrixCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlMatrix Control table"
                    " for index %d is failed \r\n", i4HlMatrixControlIndex);
        return SNMP_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "nmhValidateIndexInstanceHlMatrixControlTable \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexHlMatrixControlTable
Input       :  The Indices
               HlMatrixControlIndex
Output      :  The Get First Routines gets the Lexicographicaly
               First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexHlMatrixControlTable (INT4 *pi4HlMatrixControlIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetFirstIndexHlMatrixControlTable \n");

    return nmhGetNextIndexHlMatrixControlTable (RMON2_ZERO,
                                                pi4HlMatrixControlIndex);
}

/****************************************************************************
Function    :  nmhGetNextIndexHlMatrixControlTable
Input       :  The Indices
               HlMatrixControlIndex
               nextHlMatrixControlIndex
Output      :  The Get Next function gets the Next Index for
               the Index Value given in the Index Values. The
               Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexHlMatrixControlTable (INT4 i4HlMatrixControlIndex,
                                     INT4 *pi4NextHlMatrixControlIndex)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNextIndexHlMatrixControlTable \n");

    pHlMatrixCtrlEntry =
        Rmon2HlMatrixGetNextCtrlIndex ((UINT4) i4HlMatrixControlIndex);

    if (pHlMatrixCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlMatrix Control table"
                    " for index %d is failed \r\n", i4HlMatrixControlIndex);
        return SNMP_FAILURE;
    }

    *pi4NextHlMatrixControlIndex =
        (INT4) pHlMatrixCtrlEntry->u4HlMatrixControlIndex;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetNextIndexHlMatrixControlTable \n");
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetHlMatrixControlDataSource
Input       :  The Indices
               HlMatrixControlIndex

               The Object 
               retValHlMatrixControlDataSource
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHlMatrixControlDataSource (INT4 i4HlMatrixControlIndex,
                                 tSNMP_OID_TYPE *
                                 pRetValHlMatrixControlDataSource)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetHlMatrixControlDataSource \n");

    pHlMatrixCtrlEntry =
        Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);

    if (pHlMatrixCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlMatrix Control table"
                    " for index %d is failed \r\n", i4HlMatrixControlIndex);
        return SNMP_FAILURE;
    }

    RMON2_GET_DATA_SOURCE_OID (pRetValHlMatrixControlDataSource,
                               pHlMatrixCtrlEntry->u4HlMatrixControlDataSource);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetHlMatrixControlDataSource \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetHlMatrixControlNlDroppedFrames
Input       :  The Indices
               HlMatrixControlIndex
 
               The Object 
               retValHlMatrixControlNlDroppedFrames
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHlMatrixControlNlDroppedFrames (INT4 i4HlMatrixControlIndex,
                                      UINT4
                                      *pu4RetValHlMatrixControlNlDroppedFrames)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetHlMatrixControlNlDroppedFrames \n");

    pHlMatrixCtrlEntry =
        Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);

    if (pHlMatrixCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlMatrix Control table"
                    " for index %d is failed \r\n", i4HlMatrixControlIndex);
        return SNMP_FAILURE;
    }

    *pu4RetValHlMatrixControlNlDroppedFrames =
        pHlMatrixCtrlEntry->u4HlMatrixControlNlDroppedFrames;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetHlMatrixControlNlDroppedFrames \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetHlMatrixControlNlInserts
Input       :  The Indices
               HlMatrixControlIndex

               The Object 
               retValHlMatrixControlNlInserts
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHlMatrixControlNlInserts (INT4 i4HlMatrixControlIndex,
                                UINT4 *pu4RetValHlMatrixControlNlInserts)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetHlMatrixControlNlInserts \n");

    pHlMatrixCtrlEntry =
        Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);

    if (pHlMatrixCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlMatrix Control table"
                    " for index %d is failed \r\n", i4HlMatrixControlIndex);
        return SNMP_FAILURE;
    }

    *pu4RetValHlMatrixControlNlInserts =
        pHlMatrixCtrlEntry->u4HlMatrixControlNlInserts;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetHlMatrixControlNlInserts \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetHlMatrixControlNlDeletes
Input       :  The Indices
               HlMatrixControlIndex

               The Object 
               retValHlMatrixControlNlDeletes
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHlMatrixControlNlDeletes (INT4 i4HlMatrixControlIndex,
                                UINT4 *pu4RetValHlMatrixControlNlDeletes)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetHlMatrixControlNlDeletes \n");

    pHlMatrixCtrlEntry =
        Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);

    if (pHlMatrixCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlMatrix Control table"
                    " for index %d is failed \r\n", i4HlMatrixControlIndex);
        return SNMP_FAILURE;
    }

    *pu4RetValHlMatrixControlNlDeletes =
        pHlMatrixCtrlEntry->u4HlMatrixControlNlDeletes;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetHlMatrixControlNlDeletes \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetHlMatrixControlNlMaxDesiredEntries
Input       :  The Indices
               HlMatrixControlIndex

               The Object 
               retValHlMatrixControlNlMaxDesiredEntries
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHlMatrixControlNlMaxDesiredEntries (INT4 i4HlMatrixControlIndex,
                                          INT4
                                          *pi4RetValHlMatrixControlNlMaxDesiredEntries)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetHlMatrixControlNlMaxDesiredEntries \n");

    pHlMatrixCtrlEntry =
        Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);

    if (pHlMatrixCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlMatrix Control table"
                    " for index %d is failed \r\n", i4HlMatrixControlIndex);
        return SNMP_FAILURE;
    }

    *pi4RetValHlMatrixControlNlMaxDesiredEntries =
        pHlMatrixCtrlEntry->i4HlMatrixControlNlMaxDesiredEntries;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetHlMatrixControlNlMaxDesiredEntries \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetHlMatrixControlAlDroppedFrames
Input       :  The Indices
               HlMatrixControlIndex

               The Object 
               retValHlMatrixControlAlDroppedFrames
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHlMatrixControlAlDroppedFrames (INT4 i4HlMatrixControlIndex,
                                      UINT4
                                      *pu4RetValHlMatrixControlAlDroppedFrames)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetHlMatrixControlAlDroppedFrames \n");

    pHlMatrixCtrlEntry =
        Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);

    if (pHlMatrixCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlMatrix Control table"
                    " for index %d is failed \r\n", i4HlMatrixControlIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValHlMatrixControlAlDroppedFrames =
        pHlMatrixCtrlEntry->u4HlMatrixControlAlDroppedFrames;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetHlMatrixControlAlDroppedFrames \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetHlMatrixControlAlInserts
Input       :  The Indices
               HlMatrixControlIndex

               The Object 
               retValHlMatrixControlAlInserts
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHlMatrixControlAlInserts (INT4 i4HlMatrixControlIndex,
                                UINT4 *pu4RetValHlMatrixControlAlInserts)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetHlMatrixControlAlInserts \n");

    pHlMatrixCtrlEntry =
        Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);

    if (pHlMatrixCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlMatrix Control table"
                    " for index %d is failed \r\n", i4HlMatrixControlIndex);
        return SNMP_FAILURE;
    }

    *pu4RetValHlMatrixControlAlInserts =
        pHlMatrixCtrlEntry->u4HlMatrixControlAlInserts;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetHlMatrixControlAlInserts \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetHlMatrixControlAlDeletes
Input       :  The Indices
               HlMatrixControlIndex

               The Object 
               retValHlMatrixControlAlDeletes
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHlMatrixControlAlDeletes (INT4 i4HlMatrixControlIndex,
                                UINT4 *pu4RetValHlMatrixControlAlDeletes)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetHlMatrixControlAlDeletes \n");

    pHlMatrixCtrlEntry =
        Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);

    if (pHlMatrixCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlMatrix Control table"
                    " for index %d is failed \r\n", i4HlMatrixControlIndex);
        return SNMP_FAILURE;
    }

    *pu4RetValHlMatrixControlAlDeletes =
        pHlMatrixCtrlEntry->u4HlMatrixControlAlDeletes;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetHlMatrixControlAlDeletes \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetHlMatrixControlAlMaxDesiredEntries
Input       :  The Indices
               HlMatrixControlIndex

               The Object 
               retValHlMatrixControlAlMaxDesiredEntries
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetHlMatrixControlAlMaxDesiredEntries (INT4 i4HlMatrixControlIndex,
                                          INT4
                                          *pi4RetValHlMatrixControlAlMaxDesiredEntries)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetHlMatrixControlAlMaxDesiredEntries \n");

    pHlMatrixCtrlEntry =
        Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);

    if (pHlMatrixCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlMatrix Control table"
                    " for index %d is failed \r\n", i4HlMatrixControlIndex);
        return SNMP_FAILURE;
    }

    *pi4RetValHlMatrixControlAlMaxDesiredEntries =
        pHlMatrixCtrlEntry->i4HlMatrixControlAlMaxDesiredEntries;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetHlMatrixControlAlMaxDesiredEntries \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetHlMatrixControlOwner
Input       :  The Indices
               HlMatrixControlIndex

               The Object 
               retValHlMatrixControlOwner
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHlMatrixControlOwner (INT4 i4HlMatrixControlIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValHlMatrixControlOwner)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhGetHlMatrixControlOwner \n");

    pHlMatrixCtrlEntry =
        Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);

    if (pHlMatrixCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlMatrix Control table"
                    " for index %d is failed \r\n", i4HlMatrixControlIndex);
        return SNMP_FAILURE;
    }
    pRetValHlMatrixControlOwner->i4_Length =
        (INT4) STRLEN (pHlMatrixCtrlEntry->au1HlMatrixControlOwner);
    MEMCPY (pRetValHlMatrixControlOwner->pu1_OctetList,
            pHlMatrixCtrlEntry->au1HlMatrixControlOwner,
            pRetValHlMatrixControlOwner->i4_Length);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetHlMatrixControlOwner \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetHlMatrixControlStatus
Input       :  The Indices
               HlMatrixControlIndex

               The Object 
               retValHlMatrixControlStatus
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetHlMatrixControlStatus (INT4 i4HlMatrixControlIndex,
                             INT4 *pi4RetValHlMatrixControlStatus)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhGetHlMatrixControlStatus \n");

    pHlMatrixCtrlEntry =
        Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);

    if (pHlMatrixCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlMatrix Control table"
                    " for index %d is failed \r\n", i4HlMatrixControlIndex);
        return SNMP_FAILURE;
    }

    *pi4RetValHlMatrixControlStatus =
        (INT4) pHlMatrixCtrlEntry->u4HlMatrixControlStatus;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetHlMatrixControlStatus \n");
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetHlMatrixControlDataSource
Input       :  The Indices
               HlMatrixControlIndex

               The Object 
               setValHlMatrixControlDataSource
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetHlMatrixControlDataSource (INT4 i4HlMatrixControlIndex,
                                 tSNMP_OID_TYPE *
                                 pSetValHlMatrixControlDataSource)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhSetHlMatrixControlDataSource \n");

    pHlMatrixCtrlEntry =
        Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);

    if (pHlMatrixCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlMatrix Control table"
                    " for index %d is failed \r\n", i4HlMatrixControlIndex);
        return SNMP_FAILURE;
    }

    RMON2_SET_DATA_SOURCE_OID (pSetValHlMatrixControlDataSource,
                               &(pHlMatrixCtrlEntry->
                                 u4HlMatrixControlDataSource));
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhSetHlMatrixControlDataSource \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetHlMatrixControlNlMaxDesiredEntries
Input       :  The Indices
               HlMatrixControlIndex

               The Object 
               setValHlMatrixControlNlMaxDesiredEntries
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetHlMatrixControlNlMaxDesiredEntries (INT4 i4HlMatrixControlIndex,
                                          INT4
                                          i4SetValHlMatrixControlNlMaxDesiredEntries)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;
    UINT4               u4Minimum = 0;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhSetHlMatrixControlNlMaxDesiredEntries \n");

    pHlMatrixCtrlEntry =
        Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);

    if (pHlMatrixCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlMatrix Control table"
                    " for index %d is failed \r\n", i4HlMatrixControlIndex);
        return SNMP_FAILURE;
    }

    u4Minimum = (UINT4) ((i4SetValHlMatrixControlNlMaxDesiredEntries != -1) ?
                         ((i4SetValHlMatrixControlNlMaxDesiredEntries <
                           RMON2_MAX_DATA_PER_CTL) ?
                          i4SetValHlMatrixControlNlMaxDesiredEntries :
                          RMON2_MAX_DATA_PER_CTL) : RMON2_MAX_DATA_PER_CTL);

    pHlMatrixCtrlEntry->i4HlMatrixControlNlMaxDesiredEntries =
        i4SetValHlMatrixControlNlMaxDesiredEntries;
    pHlMatrixCtrlEntry->u4HlMatrixControlNlMaxDesiredSupported = u4Minimum;

    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhSetHlMatrixControlNlMaxDesiredEntries \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetHlMatrixControlAlMaxDesiredEntries
Input       :  The Indices
               HlMatrixControlIndex

               The Object 
               setValHlMatrixControlAlMaxDesiredEntries
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetHlMatrixControlAlMaxDesiredEntries (INT4 i4HlMatrixControlIndex,
                                          INT4
                                          i4SetValHlMatrixControlAlMaxDesiredEntries)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;
    UINT4               u4Minimum = 0;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhSetHlMatrixControlAlMaxDesiredEntries \n");

    pHlMatrixCtrlEntry =
        Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);

    if (pHlMatrixCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlMatrix Control table"
                    " for index %d is failed \r\n", i4HlMatrixControlIndex);
        return SNMP_FAILURE;
    }

    u4Minimum = (UINT4) ((i4SetValHlMatrixControlAlMaxDesiredEntries != -1) ?
                         ((i4SetValHlMatrixControlAlMaxDesiredEntries <
                           RMON2_MAX_DATA_PER_CTL) ?
                          i4SetValHlMatrixControlAlMaxDesiredEntries :
                          RMON2_MAX_DATA_PER_CTL) : RMON2_MAX_DATA_PER_CTL);

    pHlMatrixCtrlEntry->i4HlMatrixControlAlMaxDesiredEntries =
        i4SetValHlMatrixControlAlMaxDesiredEntries;
    pHlMatrixCtrlEntry->u4HlMatrixControlAlMaxDesiredSupported = u4Minimum;

    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT :  nmhSetHlMatrixControlAlMaxDesiredEntries \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetHlMatrixControlOwner
Input       :  The Indices
               HlMatrixControlIndex

               The Object 
               setValHlMatrixControlOwner
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetHlMatrixControlOwner (INT4 i4HlMatrixControlIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValHlMatrixControlOwner)
{
    UINT4               u4Minimum;
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhSetHlMatrixControlOwner \n");

    u4Minimum =
        (UINT4) ((pSetValHlMatrixControlOwner->i4_Length <
                  RMON2_OWNER_LEN) ? pSetValHlMatrixControlOwner->
                 i4_Length : RMON2_OWNER_LEN);

    pHlMatrixCtrlEntry =
        Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);

    if (pHlMatrixCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of HlMatrix Control table"
                    " for index %d is failed \r\n", i4HlMatrixControlIndex);
        return SNMP_FAILURE;
    }

    MEMCPY (pHlMatrixCtrlEntry->au1HlMatrixControlOwner,
            pSetValHlMatrixControlOwner->pu1_OctetList, u4Minimum);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhSetHlMatrixControlOwner \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetHlMatrixControlStatus
Input       :  The Indices
               HlMatrixControlIndex

               The Object 
               setValHlMatrixControlStatus
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetHlMatrixControlStatus (INT4 i4HlMatrixControlIndex,
                             INT4 i4SetValHlMatrixControlStatus)
{
    tPortList          *pVlanPortList = NULL;
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;
    tCfaIfInfo          IfInfo;
    UINT2               u2VlanId = 0;

    INT4                i4Result;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhSetHlMatrixControlStatus \n");

    switch (i4SetValHlMatrixControlStatus)
    {
        case CREATE_AND_WAIT:

            pHlMatrixCtrlEntry =
                Rmon2HlMatrixAddCtrlEntry ((UINT4) i4HlMatrixControlIndex);
            if (pHlMatrixCtrlEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC | RMON2_MEM_FAIL,
                           "Adding to HlMatrix control table \
                            is failed\n");
                return SNMP_FAILURE;
            }
            pHlMatrixCtrlEntry->u4HlMatrixControlStatus = NOT_READY;
            break;

        case ACTIVE:

            pHlMatrixCtrlEntry =
                Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);
            if (pHlMatrixCtrlEntry == NULL)
            {
                RMON2_TRC1 (RMON2_DEBUG_TRC,
                            "Get entry of HlMatrix Control table"
                            " for index %d is failed\r\n",
                            i4HlMatrixControlIndex);
                return SNMP_FAILURE;
            }

            if (pHlMatrixCtrlEntry->u4HlMatrixControlDataSource == RMON2_ZERO)
            {
                RMON2_TRC (RMON2_DEBUG_TRC, "Data Source is not configured \n");
                return SNMP_FAILURE;
            }

            if (CfaGetIfInfo
                (pHlMatrixCtrlEntry->u4HlMatrixControlDataSource,
                 &IfInfo) == CFA_FAILURE)
            {
                /* ifindex is invalid */
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "SNMP Failure: Invalid interface Index \n");
                return SNMP_FAILURE;
            }

            if (((IfInfo.u1IfOperStatus != CFA_IF_UP)
                 || (IfInfo.u1IfAdminStatus != CFA_IF_UP)) &&
                ((gi4MibResStatus != MIB_RESTORE_IN_PROGRESS)))
            {
                RMON2_TRC (RMON2_DEBUG_TRC, "SNMP Failure: "
                           "Interface OperStatus or Admin Status is in "
                           "Down state \n");
                return SNMP_FAILURE;
            }
            pVlanPortList =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pVlanPortList == NULL)
            {
                RMON2_TRC (RMON2_FN_ENTRY,
                           "nmhSetHlMatrixControlStatus: "
                           "Error in allocating memory for pVlanPortList\r\n");
                return SNMP_FAILURE;
            }
            MEMSET (pVlanPortList, 0, sizeof (tPortList));

            if (IfInfo.u1IfType == CFA_L3IPVLAN)
            {
                /* Get the VlanId from interface index */
                if (CfaGetVlanId
                    (pHlMatrixCtrlEntry->u4HlMatrixControlDataSource,
                     &u2VlanId) == CFA_FAILURE)
                {
                    RMON2_TRC1 (RMON2_DEBUG_TRC, "SNMP Failure: \
                 Unable to Fetch VlanId of IfIndex %d\n", pHlMatrixCtrlEntry->u4HlMatrixControlDataSource);
                    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                    return SNMP_FAILURE;
                }

                if (L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList) ==
                    L2IWF_FAILURE)
                {
                    RMON2_TRC1 (RMON2_DEBUG_TRC, "SNMP Failure: \
                 Get Member Port List for VlanId %d failed \n", u2VlanId);
                    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                    return SNMP_FAILURE;
                }
            }

            pHlMatrixCtrlEntry->u4HlMatrixControlStatus = ACTIVE;

#ifdef NPAPI_WANTED
            Rmonv2FsRMONv2EnableProbe (pHlMatrixCtrlEntry->
                                       u4HlMatrixControlDataSource, u2VlanId,
                                       *pVlanPortList);
#endif
            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);

            Rmon2MatrixPopulateDataEntries (pHlMatrixCtrlEntry);

            Rmon2NlMatrixTopNNotifyCtlChgs ((UINT4) i4HlMatrixControlIndex,
                                            (UINT4)
                                            i4SetValHlMatrixControlStatus);

            Rmon2AlMatrixTopNNotifyCtlChgs ((UINT4) i4HlMatrixControlIndex,
                                            (UINT4)
                                            i4SetValHlMatrixControlStatus);
            Rmon2PktInfoDelUnusedEntries ();

            break;

        case NOT_IN_SERVICE:

            pHlMatrixCtrlEntry =
                Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);
            if (pHlMatrixCtrlEntry == NULL)
            {
                RMON2_TRC1 (RMON2_DEBUG_TRC,
                            "Get entry of HlMatrix Control table"
                            " for index %d  is failed \r\n",
                            i4HlMatrixControlIndex);
                return SNMP_FAILURE;
            }

            pHlMatrixCtrlEntry->u4HlMatrixControlStatus = NOT_IN_SERVICE;

            Rmon2MatrixDelDataEntries ((UINT4) i4HlMatrixControlIndex);

            Rmon2NlMatrixTopNNotifyCtlChgs ((UINT4) i4HlMatrixControlIndex,
                                            (UINT4)
                                            i4SetValHlMatrixControlStatus);

            Rmon2AlMatrixTopNNotifyCtlChgs ((UINT4) i4HlMatrixControlIndex,
                                            (UINT4)
                                            i4SetValHlMatrixControlStatus);

            pVlanPortList =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pVlanPortList == NULL)
            {
                RMON2_TRC (RMON2_FN_ENTRY,
                           "nmhSetHlMatrixControlStatus: "
                           "Error in allocating memory for pVlanPortList\r\n");
                return SNMP_FAILURE;
            }
            MEMSET (pVlanPortList, 0, sizeof (tPortList));
            /* check for valid datasource */

            if (CfaGetIfInfo
                (pHlMatrixCtrlEntry->u4HlMatrixControlDataSource,
                 &IfInfo) != CFA_FAILURE)
            {
                if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
                    (CfaGetVlanId
                     (pHlMatrixCtrlEntry->u4HlMatrixControlDataSource,
                      &u2VlanId) != CFA_FAILURE))
                {
                    L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList);
                }
            }

#ifdef NPAPI_WANTED
            Rmonv2FsRMONv2DisableProbe (pHlMatrixCtrlEntry->
                                        u4HlMatrixControlDataSource, u2VlanId,
                                        *pVlanPortList);
#endif
            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
            Rmon2PktInfoDelUnusedEntries ();
            break;

        case DESTROY:

            pHlMatrixCtrlEntry =
                Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);
            if (pHlMatrixCtrlEntry == NULL)
            {
                RMON2_TRC1 (RMON2_DEBUG_TRC,
                            "Get entry of HlMatrix Control table"
                            " for index %d is failed \r\n",
                            i4HlMatrixControlIndex);
                return SNMP_FAILURE;
            }

            Rmon2MatrixDelDataEntries ((UINT4) i4HlMatrixControlIndex);

            Rmon2NlMatrixTopNNotifyCtlChgs ((UINT4) i4HlMatrixControlIndex,
                                            (UINT4)
                                            i4SetValHlMatrixControlStatus);

            Rmon2AlMatrixTopNNotifyCtlChgs ((UINT4) i4HlMatrixControlIndex,
                                            (UINT4)
                                            i4SetValHlMatrixControlStatus);

            i4Result = Rmon2HlMatrixDelCtrlEntry (pHlMatrixCtrlEntry);
            if (i4Result == OSIX_FAILURE)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "RBTreeRemove Failure for HlMatrix control entry\n");
                return SNMP_FAILURE;
            }

            Rmon2PktInfoDelUnusedEntries ();
            break;
        default:
            RMON2_TRC (RMON2_DEBUG_TRC, "Unknown option\n");
            return SNMP_FAILURE;
            break;
    }

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: EXIT :  nmhSetHlMatrixControlStatus \n");
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2HlMatrixControlDataSource
Input       :  The Indices
               HlMatrixControlIndex

               The Object 
               testValHlMatrixControlDataSource
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2HlMatrixControlDataSource (UINT4 *pu4ErrorCode,
                                    INT4 i4HlMatrixControlIndex,
                                    tSNMP_OID_TYPE *
                                    pTestValHlMatrixControlDataSource)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;
    UINT4               u4DataSource = RMON2_ZERO;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY   nmhTestv2HlMatrixControlDataSource \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HlMatrixControlIndex < RMON2_ONE) ||
        (i4HlMatrixControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid HlMatrixControlIndex \n");
        return SNMP_FAILURE;
    }

    /* Checking whether the data source is valid or not */
    if (RMON2_TEST_DATA_SOURCE_OID (pTestValHlMatrixControlDataSource))
    {
        RMON2_SET_DATA_SOURCE_OID (pTestValHlMatrixControlDataSource,
                                   &u4DataSource);

        pHlMatrixCtrlEntry =
            Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);

        /*Checking whether the Control entry exists or not */
        if (pHlMatrixCtrlEntry != NULL)
        {
            /* Checking whether the Status is not ACTIVE */
            if (pHlMatrixCtrlEntry->u4HlMatrixControlStatus != ACTIVE)
            {
                RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                           "SNMP Success: Data Source id is valid. \n");
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }

            RMON2_TRC1 (RMON2_DEBUG_TRC,
                        "Row Status of HlMatrix Control table is Active for "
                        "index : %d \r\n", i4HlMatrixControlIndex);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "SNMP Error:- Entry is not exist \n");
        return SNMP_FAILURE;

    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: INVALID DataSource Oid \n");
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT  nmhTestv2HlMatrixControlDataSource \n");
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2HlMatrixControlNlMaxDesiredEntries
Input       :  The Indices
               HlMatrixControlIndex

               The Object 
               testValHlMatrixControlNlMaxDesiredEntries
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2HlMatrixControlNlMaxDesiredEntries (UINT4 *pu4ErrorCode,
                                             INT4 i4HlMatrixControlIndex,
                                             INT4
                                             i4TestValHlMatrixControlNlMaxDesiredEntries)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY   nmhTestv2HlMatrixControlNlMaxDesiredEntries \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HlMatrixControlIndex < RMON2_ONE) ||
        (i4HlMatrixControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid HlMatrixControlIndex \n");
        return SNMP_FAILURE;
    }

    /* Testing the input for negative value alone is sufficient, because
     * RMON2 allows this configuration till the maximum value of interger 32 */
    if (i4TestValHlMatrixControlNlMaxDesiredEntries < -1)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Error : Invalid "
                   "i4TestValHlMatrixControlNlMaxDesiredEntries \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pHlMatrixCtrlEntry =
        Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);

    if (pHlMatrixCtrlEntry != NULL)
    {
        if (pHlMatrixCtrlEntry->u4HlMatrixControlStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of HlMatrix Control table is Active for "
                    "index : %d \r\n", i4HlMatrixControlIndex);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry is not exist \n");

    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT  nmhTestv2HlMatrixControlNlMaxDesiredEntries \n");
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2HlMatrixControlAlMaxDesiredEntries
Input       :  The Indices
               HlMatrixControlIndex

               The Object 
               testValHlMatrixControlAlMaxDesiredEntries
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2HlMatrixControlAlMaxDesiredEntries (UINT4 *pu4ErrorCode,
                                             INT4 i4HlMatrixControlIndex,
                                             INT4
                                             i4TestValHlMatrixControlAlMaxDesiredEntries)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY   nmhTestv2HlMatrixControlAlMaxDesiredEntries \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HlMatrixControlIndex < RMON2_ONE) ||
        (i4HlMatrixControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid HlMatrixControlIndex \n");
        return SNMP_FAILURE;
    }

    /* Testing the input for negative value alone is sufficient, because
     * RMON2 allows this configuration till the maximum value of interger 32 */
    if (i4TestValHlMatrixControlAlMaxDesiredEntries < -1)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Error : Invalid "
                   "i4TestValHlMatrixControlNlMaxDesiredEntries \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    pHlMatrixCtrlEntry =
        Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);

    if (pHlMatrixCtrlEntry != NULL)
    {
        if (pHlMatrixCtrlEntry->u4HlMatrixControlStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of HlMatrix Control table is Active for "
                    "index : %d \r\n", i4HlMatrixControlIndex);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry is not exist \n");

    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT  nmhTestv2HlMatrixControlAlMaxDesiredEntries \n");
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2HlMatrixControlOwner
Input       :  The Indices
               HlMatrixControlIndex

               The Object 
               testValHlMatrixControlOwner
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2HlMatrixControlOwner (UINT4 *pu4ErrorCode, INT4 i4HlMatrixControlIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValHlMatrixControlOwner)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhTestv2HlMatrixControlOwner \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HlMatrixControlIndex < RMON2_ONE) ||
        (i4HlMatrixControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid HlMatrixControlIndex \n");
        return SNMP_FAILURE;
    }

    if ((pTestValHlMatrixControlOwner->i4_Length < RMON2_ZERO) ||
        (pTestValHlMatrixControlOwner->i4_Length > RMON2_OWNER_LEN))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Wrong length for HlMatrixControlOwner object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValHlMatrixControlOwner->pu1_OctetList,
                              pTestValHlMatrixControlOwner->i4_Length)
        == OSIX_FAILURE)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Invalid Characters for Owner string object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif
    pHlMatrixCtrlEntry =
        Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);

    if (pHlMatrixCtrlEntry != NULL)
    {
        if (pHlMatrixCtrlEntry->u4HlMatrixControlStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of HlMatrix Control table is Active for "
                    "index : %d \r\n", i4HlMatrixControlIndex);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry is not exist \n");
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhTestv2HlMatrixControlOwner \n");
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhTestv2HlMatrixControlStatus
Input       :  The Indices
               HlMatrixControlIndex

               The Object 
               testValHlMatrixControlStatus
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2HlMatrixControlStatus (UINT4 *pu4ErrorCode,
                                INT4 i4HlMatrixControlIndex,
                                INT4 i4TestValHlMatrixControlStatus)
{
    tHlMatrixControl   *pHlMatrixCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhTestv2HlMatrixControlStatus \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HlMatrixControlIndex < RMON2_ONE) ||
        (i4HlMatrixControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid HlMatrixControlIndex \n");
        return SNMP_FAILURE;
    }

    if ((i4TestValHlMatrixControlStatus < ACTIVE) ||
        (i4TestValHlMatrixControlStatus > DESTROY))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Wrong value for ProtocolDistControlStatus "
                   "object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pHlMatrixCtrlEntry =
        Rmon2HlMatrixGetCtrlEntry ((UINT4) i4HlMatrixControlIndex);

    if (pHlMatrixCtrlEntry == NULL)
    {
        if (i4TestValHlMatrixControlStatus == CREATE_AND_WAIT)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "SNMP Failure: Entry is not exist \n");
        return SNMP_FAILURE;

    }
    else
    {
        if ((i4TestValHlMatrixControlStatus == ACTIVE) ||
            (i4TestValHlMatrixControlStatus == NOT_IN_SERVICE) ||
            (i4TestValHlMatrixControlStatus == DESTROY))
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "The given entry is already exist \n");
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhTestv2HlMatrixControlStatus \n");
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2HlMatrixControlTable
Input       :  The Indices
               HlMatrixControlIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
               check whether dependency is met or not.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2HlMatrixControlTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : NlMatrixSDTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceNlMatrixSDTable
Input       :  The Indices
               HlMatrixControlIndex
               NlMatrixSDTimeMark
               ProtocolDirLocalIndex
               NlMatrixSDSourceAddress
               NlMatrixSDDestAddress
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceNlMatrixSDTable (INT4 i4HlMatrixControlIndex,
                                         UINT4 u4NlMatrixSDTimeMark,
                                         INT4 i4ProtocolDirLocalIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNlMatrixSDSourceAddress,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNlMatrixSDDestAddress)
{
    tNlMatrixSD        *pNlMatrixSDEntry = NULL;
    tIpAddr             NlMatrixSDSourceAddress, NlMatrixSDDestAddress;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "nmhValidateIndexInstanceNlMatrixSDTable \n");

    UNUSED_PARAM (u4NlMatrixSDTimeMark);

    MEMSET (&NlMatrixSDSourceAddress, 0, sizeof (tIpAddr));
    MEMSET (&NlMatrixSDDestAddress, 0, sizeof (tIpAddr));

    if (pNlMatrixSDSourceAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixSDSourceAddress,
                pNlMatrixSDSourceAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixSDSourceAddress.u4_addr[3],
                pNlMatrixSDSourceAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    if (pNlMatrixSDDestAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixSDDestAddress, pNlMatrixSDDestAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixSDDestAddress.u4_addr[3],
                pNlMatrixSDDestAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }
    NlMatrixSDSourceAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[0]);
    NlMatrixSDSourceAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[1]);
    NlMatrixSDSourceAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[2]);
    NlMatrixSDSourceAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[3]);

    NlMatrixSDDestAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[0]);
    NlMatrixSDDestAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[1]);
    NlMatrixSDDestAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[2]);
    NlMatrixSDDestAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[3]);

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HlMatrixControlIndex < RMON2_ONE) ||
        (i4HlMatrixControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid HlMatrixControlIndex \n");
        return SNMP_FAILURE;
    }

    /* Testing the input for negative value alone is sufficient, because
     * RMON2 allows this configuration till the maximum value of interger 32 */
    if (i4ProtocolDirLocalIndex < RMON2_ONE)
    {
        RMON2_TRC (RMON2_DEBUG_TRC, "SNMP Failure: Invalid "
                   "ProtocolDirLocalIndex \n");
        return SNMP_FAILURE;
    }
    pNlMatrixSDEntry =
        Rmon2NlMatrixGetDataEntry (RMON2_NLMATRIXSD_TABLE,
                                   (UINT4) i4HlMatrixControlIndex,
                                   (UINT4) i4ProtocolDirLocalIndex,
                                   &NlMatrixSDSourceAddress,
                                   &NlMatrixSDDestAddress);

    if (pNlMatrixSDEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of NlMatrixSD table is failed \r\n");
        return SNMP_FAILURE;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "nmhValidateIndexInstanceNlMatrixSDTable \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexNlMatrixSDTable
Input       :  The Indices
               HlMatrixControlIndex
               NlMatrixSDTimeMark
               ProtocolDirLocalIndex
               NlMatrixSDSourceAddress
               NlMatrixSDDestAddress
Output      :  The Get First Routines gets the Lexicographicaly
               First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexNlMatrixSDTable (INT4 *pi4HlMatrixControlIndex,
                                 UINT4 *pu4NlMatrixSDTimeMark,
                                 INT4 *pi4ProtocolDirLocalIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNlMatrixSDSourceAddress,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNlMatrixSDDestAddress)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetFirstIndexNlMatrixSDTable \n");

    return nmhGetNextIndexNlMatrixSDTable (RMON2_ZERO, pi4HlMatrixControlIndex,
                                           RMON2_ZERO, pu4NlMatrixSDTimeMark,
                                           RMON2_ZERO, pi4ProtocolDirLocalIndex,
                                           RMON2_ZERO, pNlMatrixSDSourceAddress,
                                           RMON2_ZERO, pNlMatrixSDDestAddress);
}

/****************************************************************************
Function    :  nmhGetNextIndexNlMatrixSDTable
Input       :  The Indices
               HlMatrixControlIndex
               nextHlMatrixControlIndex
               NlMatrixSDTimeMark
               nextNlMatrixSDTimeMark
               ProtocolDirLocalIndex
               nextProtocolDirLocalIndex
               NlMatrixSDSourceAddress
               nextNlMatrixSDSourceAddress
               NlMatrixSDDestAddress
               nextNlMatrixSDDestAddress
Output      :  The Get Next function gets the Next Index for
               the Index Value given in the Index Values. The
               Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexNlMatrixSDTable (INT4 i4HlMatrixControlIndex,
                                INT4 *pi4NextHlMatrixControlIndex,
                                UINT4 u4NlMatrixSDTimeMark,
                                UINT4 *pu4NextNlMatrixSDTimeMark,
                                INT4 i4ProtocolDirLocalIndex,
                                INT4 *pi4NextProtocolDirLocalIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pNlMatrixSDSourceAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pNextNlMatrixSDSourceAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pNlMatrixSDDestAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pNextNlMatrixSDDestAddress)
{
    tNlMatrixSD        *pNlMatrixSDEntry = NULL;
    tIpAddr             NlMatrixSDSourceAddress, NlMatrixSDDestAddress;
    tIpAddr             SrcAddr, DestAddr;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNextIndexNlMatrixSDTable \n");
    UNUSED_PARAM (u4NlMatrixSDTimeMark);

    if (pNlMatrixSDSourceAddress == NULL)
    {
        MEMSET (&NlMatrixSDSourceAddress, 0, sizeof (tIpAddr));
    }
    else
    {
        MEMSET (&NlMatrixSDSourceAddress, 0, sizeof (tIpAddr));
        if (pNlMatrixSDSourceAddress->i4_Length == RMON2_IPV6_MAX_LEN)
        {
            MEMCPY (&NlMatrixSDSourceAddress,
                    pNlMatrixSDSourceAddress->pu1_OctetList,
                    RMON2_IPV6_MAX_LEN);
        }
        else
        {
            MEMCPY (&NlMatrixSDSourceAddress.u4_addr[3],
                    pNlMatrixSDSourceAddress->pu1_OctetList,
                    RMON2_IPV4_MAX_LEN);
        }
    }

    if (pNlMatrixSDDestAddress == NULL)
    {
        MEMSET (&NlMatrixSDDestAddress, 0, sizeof (tIpAddr));
    }
    else
    {
        MEMSET (&NlMatrixSDDestAddress, 0, sizeof (tIpAddr));
        if (pNlMatrixSDDestAddress->i4_Length == RMON2_IPV6_MAX_LEN)
        {
            MEMCPY (&NlMatrixSDDestAddress,
                    pNlMatrixSDDestAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
        }
        else
        {
            MEMCPY (&NlMatrixSDDestAddress.u4_addr[3],
                    pNlMatrixSDDestAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
        }
    }

    NlMatrixSDSourceAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[0]);
    NlMatrixSDSourceAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[1]);
    NlMatrixSDSourceAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[2]);
    NlMatrixSDSourceAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[3]);

    NlMatrixSDDestAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[0]);
    NlMatrixSDDestAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[1]);
    NlMatrixSDDestAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[2]);
    NlMatrixSDDestAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[3]);

    pNlMatrixSDEntry =
        Rmon2NlMatrixGetNextDataIndex (RMON2_NLMATRIXSD_TABLE,
                                       (UINT4) i4HlMatrixControlIndex,
                                       (UINT4) i4ProtocolDirLocalIndex,
                                       &NlMatrixSDSourceAddress,
                                       &NlMatrixSDDestAddress);
    if (pNlMatrixSDEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get first entry of NlMatrixSD table is failed \r\n");
        return SNMP_FAILURE;
    }

    *pi4NextHlMatrixControlIndex =
        (INT4) pNlMatrixSDEntry->u4HlMatrixControlIndex;
    *pu4NextNlMatrixSDTimeMark = RMON2_ZERO;
    *pi4NextProtocolDirLocalIndex =
        (INT4) pNlMatrixSDEntry->u4ProtocolDirLocalIndex;

    pNextNlMatrixSDSourceAddress->i4_Length =
        (INT4) pNlMatrixSDEntry->u4Rmon2NlMatrixSDIpAddrLen;
    pNextNlMatrixSDDestAddress->i4_Length =
        (INT4) pNlMatrixSDEntry->u4Rmon2NlMatrixSDIpAddrLen;

    SrcAddr.u4_addr[0] =
        OSIX_HTONL (pNlMatrixSDEntry->NlMatrixSDSourceAddress.u4_addr[0]);
    SrcAddr.u4_addr[1] =
        OSIX_HTONL (pNlMatrixSDEntry->NlMatrixSDSourceAddress.u4_addr[1]);
    SrcAddr.u4_addr[2] =
        OSIX_HTONL (pNlMatrixSDEntry->NlMatrixSDSourceAddress.u4_addr[2]);
    SrcAddr.u4_addr[3] =
        OSIX_HTONL (pNlMatrixSDEntry->NlMatrixSDSourceAddress.u4_addr[3]);

    DestAddr.u4_addr[0] =
        OSIX_HTONL (pNlMatrixSDEntry->NlMatrixSDDestAddress.u4_addr[0]);
    DestAddr.u4_addr[1] =
        OSIX_HTONL (pNlMatrixSDEntry->NlMatrixSDDestAddress.u4_addr[1]);
    DestAddr.u4_addr[2] =
        OSIX_HTONL (pNlMatrixSDEntry->NlMatrixSDDestAddress.u4_addr[2]);
    DestAddr.u4_addr[3] =
        OSIX_HTONL (pNlMatrixSDEntry->NlMatrixSDDestAddress.u4_addr[3]);

    if (pNlMatrixSDEntry->u4Rmon2NlMatrixSDIpAddrLen == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (pNextNlMatrixSDSourceAddress->pu1_OctetList,
                (UINT1 *) &SrcAddr, RMON2_IPV6_MAX_LEN);
        MEMCPY (pNextNlMatrixSDDestAddress->pu1_OctetList,
                (UINT1 *) &DestAddr, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (pNextNlMatrixSDSourceAddress->pu1_OctetList,
                (UINT1 *) &SrcAddr.u4_addr[3], RMON2_IPV4_MAX_LEN);
        MEMCPY (pNextNlMatrixSDDestAddress->pu1_OctetList,
                (UINT1 *) &DestAddr.u4_addr[3], RMON2_IPV4_MAX_LEN);
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNextIndexNlMatrixSDTable \n");
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetNlMatrixSDPkts
Input       :  The Indices
               HlMatrixControlIndex
               NlMatrixSDTimeMark
               ProtocolDirLocalIndex
               NlMatrixSDSourceAddress
               NlMatrixSDDestAddress

               The Object 
               retValNlMatrixSDPkts
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetNlMatrixSDPkts (INT4 i4HlMatrixControlIndex, UINT4 u4NlMatrixSDTimeMark,
                      INT4 i4ProtocolDirLocalIndex,
                      tSNMP_OCTET_STRING_TYPE * pNlMatrixSDSourceAddress,
                      tSNMP_OCTET_STRING_TYPE * pNlMatrixSDDestAddress,
                      UINT4 *pu4RetValNlMatrixSDPkts)
{
    tNlMatrixSD        *pNlMatrixSDEntry = NULL;
    tIpAddr             NlMatrixSDSourceAddress, NlMatrixSDDestAddress;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhGetNlMatrixSDPkts \n");

    UNUSED_PARAM (u4NlMatrixSDTimeMark);
    MEMSET (&NlMatrixSDSourceAddress, 0, sizeof (tIpAddr));
    MEMSET (&NlMatrixSDDestAddress, 0, sizeof (tIpAddr));

    if (pNlMatrixSDSourceAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixSDSourceAddress,
                pNlMatrixSDSourceAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixSDSourceAddress.u4_addr[3],
                pNlMatrixSDSourceAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    if (pNlMatrixSDDestAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixSDDestAddress, pNlMatrixSDDestAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixSDDestAddress.u4_addr[3],
                pNlMatrixSDDestAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlMatrixSDSourceAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[0]);
    NlMatrixSDSourceAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[1]);
    NlMatrixSDSourceAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[2]);
    NlMatrixSDSourceAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[3]);

    NlMatrixSDDestAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[0]);
    NlMatrixSDDestAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[1]);
    NlMatrixSDDestAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[2]);
    NlMatrixSDDestAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[3]);

    pNlMatrixSDEntry =
        Rmon2NlMatrixGetDataEntry (RMON2_NLMATRIXSD_TABLE,
                                   (UINT4) i4HlMatrixControlIndex,
                                   (UINT4) i4ProtocolDirLocalIndex,
                                   &NlMatrixSDSourceAddress,
                                   &NlMatrixSDDestAddress);
    if (pNlMatrixSDEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of NlMatrixSD table is failed \r\n");
        return SNMP_FAILURE;
    }
    if (pNlMatrixSDEntry->u4NlMatrixSDTimeMark < u4NlMatrixSDTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValNlMatrixSDPkts =
        pNlMatrixSDEntry->NlMatrixSDCurSample.u4NlMatrixSDPkts;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNlMatrixSDPkts \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNlMatrixSDOctets
Input       :  The Indices
               HlMatrixControlIndex
               NlMatrixSDTimeMark
               ProtocolDirLocalIndex
               NlMatrixSDSourceAddress
               NlMatrixSDDestAddress 

               The Object  
               retValNlMatrixSDOctets
Output      :  The Get Low Lev Routine Take the Indices & 
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlMatrixSDOctets (INT4 i4HlMatrixControlIndex, UINT4 u4NlMatrixSDTimeMark,
                        INT4 i4ProtocolDirLocalIndex,
                        tSNMP_OCTET_STRING_TYPE * pNlMatrixSDSourceAddress,
                        tSNMP_OCTET_STRING_TYPE * pNlMatrixSDDestAddress,
                        UINT4 *pu4RetValNlMatrixSDOctets)
{
    tNlMatrixSD        *pNlMatrixSDEntry = NULL;
    tIpAddr             NlMatrixSDSourceAddress, NlMatrixSDDestAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhGetNlMatrixSDOctets \n");

    UNUSED_PARAM (u4NlMatrixSDTimeMark);
    MEMSET (&NlMatrixSDSourceAddress, 0, sizeof (tIpAddr));
    MEMSET (&NlMatrixSDDestAddress, 0, sizeof (tIpAddr));

    if (pNlMatrixSDSourceAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixSDSourceAddress,
                pNlMatrixSDSourceAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixSDSourceAddress.u4_addr[3],
                pNlMatrixSDSourceAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    if (pNlMatrixSDDestAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixSDDestAddress, pNlMatrixSDDestAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixSDDestAddress.u4_addr[3],
                pNlMatrixSDDestAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlMatrixSDSourceAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[0]);
    NlMatrixSDSourceAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[1]);
    NlMatrixSDSourceAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[2]);
    NlMatrixSDSourceAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[3]);

    NlMatrixSDDestAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[0]);
    NlMatrixSDDestAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[1]);
    NlMatrixSDDestAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[2]);
    NlMatrixSDDestAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[3]);

    pNlMatrixSDEntry =
        Rmon2NlMatrixGetDataEntry (RMON2_NLMATRIXSD_TABLE,
                                   (UINT4) i4HlMatrixControlIndex,
                                   (UINT4) i4ProtocolDirLocalIndex,
                                   &NlMatrixSDSourceAddress,
                                   &NlMatrixSDDestAddress);

    if (pNlMatrixSDEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of NlMatrixSD table is failed \r\n");
        return SNMP_FAILURE;
    }
    if (pNlMatrixSDEntry->u4NlMatrixSDTimeMark < u4NlMatrixSDTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValNlMatrixSDOctets =
        pNlMatrixSDEntry->NlMatrixSDCurSample.u4NlMatrixSDOctets;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNlMatrixSDOctets \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNlMatrixSDCreateTime
Input       :  The Indices
               HlMatrixControlIndex
               NlMatrixSDTimeMark 
               ProtocolDirLocalIndex
               NlMatrixSDSourceAddress
               NlMatrixSDDestAddress

               The Object 
               retValNlMatrixSDCreateTime
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlMatrixSDCreateTime (INT4 i4HlMatrixControlIndex,
                            UINT4 u4NlMatrixSDTimeMark,
                            INT4 i4ProtocolDirLocalIndex,
                            tSNMP_OCTET_STRING_TYPE * pNlMatrixSDSourceAddress,
                            tSNMP_OCTET_STRING_TYPE * pNlMatrixSDDestAddress,
                            UINT4 *pu4RetValNlMatrixSDCreateTime)
{
    tNlMatrixSD        *pNlMatrixSDEntry = NULL;
    tIpAddr             NlMatrixSDSourceAddress, NlMatrixSDDestAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhGetNlMatrixSDPkts \n");

    UNUSED_PARAM (u4NlMatrixSDTimeMark);
    MEMSET (&NlMatrixSDSourceAddress, 0, sizeof (tIpAddr));
    MEMSET (&NlMatrixSDDestAddress, 0, sizeof (tIpAddr));

    if (pNlMatrixSDSourceAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixSDSourceAddress,
                pNlMatrixSDSourceAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixSDSourceAddress.u4_addr[3],
                pNlMatrixSDSourceAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    if (pNlMatrixSDDestAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixSDDestAddress, pNlMatrixSDDestAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixSDDestAddress.u4_addr[3],
                pNlMatrixSDDestAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlMatrixSDSourceAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[0]);
    NlMatrixSDSourceAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[1]);
    NlMatrixSDSourceAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[2]);
    NlMatrixSDSourceAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[3]);

    NlMatrixSDDestAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[0]);
    NlMatrixSDDestAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[1]);
    NlMatrixSDDestAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[2]);
    NlMatrixSDDestAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[3]);

    pNlMatrixSDEntry =
        Rmon2NlMatrixGetDataEntry (RMON2_NLMATRIXSD_TABLE,
                                   (UINT4) i4HlMatrixControlIndex,
                                   (UINT4) i4ProtocolDirLocalIndex,
                                   &NlMatrixSDSourceAddress,
                                   &NlMatrixSDDestAddress);
    if (pNlMatrixSDEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Get entry of NlMatrixSD table is failed\r\n");
        return SNMP_FAILURE;
    }
    if (pNlMatrixSDEntry->u4NlMatrixSDTimeMark < u4NlMatrixSDTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValNlMatrixSDCreateTime = pNlMatrixSDEntry->u4NlMatrixSDCreateTime;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNlMatrixSDPkts \n");
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : NlMatrixDSTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceNlMatrixDSTable
Input       :  The Indices
               HlMatrixControlIndex
               NlMatrixDSTimeMark
               ProtocolDirLocalIndex
               NlMatrixDSDestAddress
               NlMatrixDSSourceAddress
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceNlMatrixDSTable (INT4 i4HlMatrixControlIndex,
                                         UINT4 u4NlMatrixDSTimeMark,
                                         INT4 i4ProtocolDirLocalIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNlMatrixDSDestAddress,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNlMatrixDSSourceAddress)
{
    tNlMatrixSD        *pNlMatrixDSEntry = NULL;
    tIpAddr             NlMatrixDSDestAddress, NlMatrixDSSourceAddress;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "nmhValidateIndexInstanceNlMatrixDSTable \n");
    UNUSED_PARAM (u4NlMatrixDSTimeMark);

    MEMSET (&NlMatrixDSSourceAddress, 0, sizeof (tIpAddr));
    MEMSET (&NlMatrixDSDestAddress, 0, sizeof (tIpAddr));

    if (pNlMatrixDSSourceAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixDSSourceAddress,
                pNlMatrixDSSourceAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixDSSourceAddress.u4_addr[3],
                pNlMatrixDSSourceAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    if (pNlMatrixDSDestAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixDSDestAddress, pNlMatrixDSDestAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixDSDestAddress.u4_addr[3],
                pNlMatrixDSDestAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlMatrixDSSourceAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[0]);
    NlMatrixDSSourceAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[1]);
    NlMatrixDSSourceAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[2]);
    NlMatrixDSSourceAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[3]);

    NlMatrixDSDestAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[0]);
    NlMatrixDSDestAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[1]);
    NlMatrixDSDestAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[2]);
    NlMatrixDSDestAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[3]);

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HlMatrixControlIndex < RMON2_ONE) ||
        (i4HlMatrixControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid HlMatrixControlIndex \n");
        return SNMP_FAILURE;
    }

    /* Testing the input for negative value alone is sufficient, because
     * RMON2 allows this configuration till the maximum value of interger 32 */
    if (i4ProtocolDirLocalIndex < RMON2_ONE)
    {
        RMON2_TRC (RMON2_DEBUG_TRC, "SNMP Failure: Invalid "
                   "ProtocolDirLocalIndex \n");
        return SNMP_FAILURE;
    }

    pNlMatrixDSEntry =
        Rmon2NlMatrixGetDataEntry (RMON2_NLMATRIXDS_TABLE,
                                   (UINT4) i4HlMatrixControlIndex,
                                   (UINT4) i4ProtocolDirLocalIndex,
                                   &NlMatrixDSSourceAddress,
                                   &NlMatrixDSDestAddress);
    if (pNlMatrixDSEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Get entry of NlMatrixDS table is failed  \r\n");
        return SNMP_FAILURE;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "nmhValidateIndexInstanceNlMatrixDSTable \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexNlMatrixDSTable
Input       :  The Indices 
               HlMatrixControlIndex
               NlMatrixDSTimeMark
               ProtocolDirLocalIndex
               NlMatrixDSDestAddress
               NlMatrixDSSourceAddress
Output      :  The Get First Routines gets the Lexicographicaly
               First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexNlMatrixDSTable (INT4 *pi4HlMatrixControlIndex,
                                 UINT4 *pu4NlMatrixDSTimeMark,
                                 INT4 *pi4ProtocolDirLocalIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNlMatrixDSDestAddress,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNlMatrixDSSourceAddress)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetFirstIndexNlMatrixDSTable \n");

    return nmhGetNextIndexNlMatrixDSTable (RMON2_ZERO, pi4HlMatrixControlIndex,
                                           RMON2_ZERO, pu4NlMatrixDSTimeMark,
                                           RMON2_ZERO, pi4ProtocolDirLocalIndex,
                                           RMON2_ZERO, pNlMatrixDSDestAddress,
                                           RMON2_ZERO,
                                           pNlMatrixDSSourceAddress);

}

/****************************************************************************
Function    :  nmhGetNextIndexNlMatrixDSTable
Input       :  The Indices
               HlMatrixControlIndex
               nextHlMatrixControlIndex
               NlMatrixDSTimeMark
               nextNlMatrixDSTimeMark
               ProtocolDirLocalIndex
               nextProtocolDirLocalIndex
               NlMatrixDSDestAddress
               nextNlMatrixDSDestAddress
               NlMatrixDSSourceAddress
               nextNlMatrixDSSourceAddress
Output      :  The Get Next function gets the Next Index for
               the Index Value given in the Index Values. The
               Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexNlMatrixDSTable (INT4 i4HlMatrixControlIndex,
                                INT4 *pi4NextHlMatrixControlIndex,
                                UINT4 u4NlMatrixDSTimeMark,
                                UINT4 *pu4NextNlMatrixDSTimeMark,
                                INT4 i4ProtocolDirLocalIndex,
                                INT4 *pi4NextProtocolDirLocalIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pNlMatrixDSDestAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pNextNlMatrixDSDestAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pNlMatrixDSSourceAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pNextNlMatrixDSSourceAddress)
{
    tNlMatrixSD        *pNlMatrixDSEntry = NULL;
    tIpAddr             NlMatrixDSDestAddress, NlMatrixDSSourceAddress;
    tIpAddr             DestAddr, SrcAddr;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNextIndexNlMatrixDSTable \n");

    UNUSED_PARAM (u4NlMatrixDSTimeMark);

    if (pNlMatrixDSSourceAddress == NULL)
    {
        MEMSET (&NlMatrixDSSourceAddress, 0, sizeof (tIpAddr));
    }
    else
    {
        MEMSET (&NlMatrixDSSourceAddress, 0, sizeof (tIpAddr));
        if (pNlMatrixDSSourceAddress->i4_Length == RMON2_IPV6_MAX_LEN)
        {
            MEMCPY (&NlMatrixDSSourceAddress,
                    pNlMatrixDSSourceAddress->pu1_OctetList,
                    RMON2_IPV6_MAX_LEN);
        }
        else
        {
            MEMCPY (&NlMatrixDSSourceAddress.u4_addr[3],
                    pNlMatrixDSSourceAddress->pu1_OctetList,
                    RMON2_IPV4_MAX_LEN);
        }
    }

    if (pNlMatrixDSDestAddress == NULL)
    {
        MEMSET (&NlMatrixDSDestAddress, 0, sizeof (tIpAddr));
    }
    else
    {
        MEMSET (&NlMatrixDSDestAddress, 0, sizeof (tIpAddr));
        if (pNlMatrixDSDestAddress->i4_Length == RMON2_IPV6_MAX_LEN)
        {
            MEMCPY (&NlMatrixDSDestAddress,
                    pNlMatrixDSDestAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
        }
        else
        {
            MEMCPY (&NlMatrixDSDestAddress.u4_addr[3],
                    pNlMatrixDSDestAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
        }
    }

    NlMatrixDSSourceAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[0]);
    NlMatrixDSSourceAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[1]);
    NlMatrixDSSourceAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[2]);
    NlMatrixDSSourceAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[3]);

    NlMatrixDSDestAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[0]);
    NlMatrixDSDestAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[1]);
    NlMatrixDSDestAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[2]);
    NlMatrixDSDestAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[3]);

    pNlMatrixDSEntry =
        Rmon2NlMatrixGetNextDataIndex (RMON2_NLMATRIXDS_TABLE,
                                       (UINT4) i4HlMatrixControlIndex,
                                       (UINT4) i4ProtocolDirLocalIndex,
                                       &NlMatrixDSSourceAddress,
                                       &NlMatrixDSDestAddress);
    if (pNlMatrixDSEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Get entry of NlMatrixDS table is failed  \r\n");
        return SNMP_FAILURE;
    }

    *pi4NextHlMatrixControlIndex =
        (INT4) pNlMatrixDSEntry->u4HlMatrixControlIndex;
    *pu4NextNlMatrixDSTimeMark = RMON2_ZERO;
    *pi4NextProtocolDirLocalIndex =
        (INT4) pNlMatrixDSEntry->u4ProtocolDirLocalIndex;

    pNextNlMatrixDSSourceAddress->i4_Length =
        (INT4) pNlMatrixDSEntry->u4Rmon2NlMatrixSDIpAddrLen;
    pNextNlMatrixDSDestAddress->i4_Length =
        (INT4) pNlMatrixDSEntry->u4Rmon2NlMatrixSDIpAddrLen;

    SrcAddr.u4_addr[0] =
        OSIX_HTONL (pNlMatrixDSEntry->NlMatrixSDSourceAddress.u4_addr[0]);
    SrcAddr.u4_addr[1] =
        OSIX_HTONL (pNlMatrixDSEntry->NlMatrixSDSourceAddress.u4_addr[1]);
    SrcAddr.u4_addr[2] =
        OSIX_HTONL (pNlMatrixDSEntry->NlMatrixSDSourceAddress.u4_addr[2]);
    SrcAddr.u4_addr[3] =
        OSIX_HTONL (pNlMatrixDSEntry->NlMatrixSDSourceAddress.u4_addr[3]);

    DestAddr.u4_addr[0] =
        OSIX_HTONL (pNlMatrixDSEntry->NlMatrixSDDestAddress.u4_addr[0]);
    DestAddr.u4_addr[1] =
        OSIX_HTONL (pNlMatrixDSEntry->NlMatrixSDDestAddress.u4_addr[1]);
    DestAddr.u4_addr[2] =
        OSIX_HTONL (pNlMatrixDSEntry->NlMatrixSDDestAddress.u4_addr[2]);
    DestAddr.u4_addr[3] =
        OSIX_HTONL (pNlMatrixDSEntry->NlMatrixSDDestAddress.u4_addr[3]);

    if (pNlMatrixDSEntry->u4Rmon2NlMatrixSDIpAddrLen == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (pNextNlMatrixDSSourceAddress->pu1_OctetList,
                (UINT1 *) &SrcAddr, RMON2_IPV6_MAX_LEN);
        MEMCPY (pNextNlMatrixDSDestAddress->pu1_OctetList,
                (UINT1 *) &DestAddr, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (pNextNlMatrixDSSourceAddress->pu1_OctetList,
                (UINT1 *) &SrcAddr.u4_addr[3], RMON2_IPV4_MAX_LEN);
        MEMCPY (pNextNlMatrixDSDestAddress->pu1_OctetList,
                (UINT1 *) &DestAddr.u4_addr[3], RMON2_IPV4_MAX_LEN);
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNextIndexNlMatrixDSTable \n");
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetNlMatrixDSPkts
Input       :  The Indices
               HlMatrixControlIndex
               NlMatrixDSTimeMark
               ProtocolDirLocalIndex
               NlMatrixDSDestAddress
               NlMatrixDSSourceAddress

               The Object 
               retValNlMatrixDSPkts
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlMatrixDSPkts (INT4 i4HlMatrixControlIndex, UINT4 u4NlMatrixDSTimeMark,
                      INT4 i4ProtocolDirLocalIndex,
                      tSNMP_OCTET_STRING_TYPE * pNlMatrixDSDestAddress,
                      tSNMP_OCTET_STRING_TYPE * pNlMatrixDSSourceAddress,
                      UINT4 *pu4RetValNlMatrixDSPkts)
{
    tNlMatrixSD        *pNlMatrixDSEntry = NULL;
    tIpAddr             NlMatrixDSDestAddress, NlMatrixDSSourceAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhGetNlMatrixDSPkts \n");

    UNUSED_PARAM (u4NlMatrixDSTimeMark);
    MEMSET (&NlMatrixDSSourceAddress, 0, sizeof (tIpAddr));
    MEMSET (&NlMatrixDSDestAddress, 0, sizeof (tIpAddr));

    if (pNlMatrixDSSourceAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixDSSourceAddress,
                pNlMatrixDSSourceAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixDSSourceAddress.u4_addr[3],
                pNlMatrixDSSourceAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    if (pNlMatrixDSDestAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixDSDestAddress, pNlMatrixDSDestAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixDSDestAddress.u4_addr[3],
                pNlMatrixDSDestAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlMatrixDSSourceAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[0]);
    NlMatrixDSSourceAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[1]);
    NlMatrixDSSourceAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[2]);
    NlMatrixDSSourceAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[3]);

    NlMatrixDSDestAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[0]);
    NlMatrixDSDestAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[1]);
    NlMatrixDSDestAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[2]);
    NlMatrixDSDestAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[3]);

    pNlMatrixDSEntry =
        Rmon2NlMatrixGetDataEntry (RMON2_NLMATRIXDS_TABLE,
                                   (UINT4) i4HlMatrixControlIndex,
                                   (UINT4) i4ProtocolDirLocalIndex,
                                   &NlMatrixDSSourceAddress,
                                   &NlMatrixDSDestAddress);

    if (pNlMatrixDSEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Get entry of NlMatrixDS table is failed  \r\n");
        return SNMP_FAILURE;
    }
    if (pNlMatrixDSEntry->u4NlMatrixSDTimeMark < u4NlMatrixDSTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValNlMatrixDSPkts =
        pNlMatrixDSEntry->NlMatrixSDCurSample.u4NlMatrixSDPkts;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNlMatrixDSPkts \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNlMatrixDSOctets
Input       :  The Indices
               HlMatrixControlIndex
               NlMatrixDSTimeMark
               ProtocolDirLocalIndex
               NlMatrixDSDestAddress
               NlMatrixDSSourceAddress

               The Object  
               retValNlMatrixDSOctets
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlMatrixDSOctets (INT4 i4HlMatrixControlIndex, UINT4 u4NlMatrixDSTimeMark,
                        INT4 i4ProtocolDirLocalIndex,
                        tSNMP_OCTET_STRING_TYPE * pNlMatrixDSDestAddress,
                        tSNMP_OCTET_STRING_TYPE * pNlMatrixDSSourceAddress,
                        UINT4 *pu4RetValNlMatrixDSOctets)
{
    tNlMatrixSD        *pNlMatrixDSEntry = NULL;
    tIpAddr             NlMatrixDSDestAddress, NlMatrixDSSourceAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhGetNlMatrixDSOctets \n");

    UNUSED_PARAM (u4NlMatrixDSTimeMark);

    MEMSET (&NlMatrixDSSourceAddress, 0, sizeof (tIpAddr));
    MEMSET (&NlMatrixDSDestAddress, 0, sizeof (tIpAddr));

    if (pNlMatrixDSSourceAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixDSSourceAddress,
                pNlMatrixDSSourceAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixDSSourceAddress.u4_addr[3],
                pNlMatrixDSSourceAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    if (pNlMatrixDSDestAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixDSDestAddress, pNlMatrixDSDestAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixDSDestAddress.u4_addr[3],
                pNlMatrixDSDestAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlMatrixDSSourceAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[0]);
    NlMatrixDSSourceAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[1]);
    NlMatrixDSSourceAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[2]);
    NlMatrixDSSourceAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[3]);

    NlMatrixDSDestAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[0]);
    NlMatrixDSDestAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[1]);
    NlMatrixDSDestAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[2]);
    NlMatrixDSDestAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[3]);

    pNlMatrixDSEntry =
        Rmon2NlMatrixGetDataEntry (RMON2_NLMATRIXDS_TABLE,
                                   (UINT4) i4HlMatrixControlIndex,
                                   (UINT4) i4ProtocolDirLocalIndex,
                                   &NlMatrixDSSourceAddress,
                                   &NlMatrixDSDestAddress);

    if (pNlMatrixDSEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Get entry of NlMatrixDS table is failed  \r\n");
        return SNMP_FAILURE;
    }
    if (pNlMatrixDSEntry->u4NlMatrixSDTimeMark < u4NlMatrixDSTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValNlMatrixDSOctets =
        pNlMatrixDSEntry->NlMatrixSDCurSample.u4NlMatrixSDOctets;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNlMatrixDSOctets \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetNlMatrixDSCreateTime
Input       :  The Indices
               HlMatrixControlIndex
               NlMatrixDSTimeMark 
               ProtocolDirLocalIndex
               NlMatrixDSDestAddress
               NlMatrixDSSourceAddress

               The Object 
               retValNlMatrixDSCreateTime
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlMatrixDSCreateTime (INT4 i4HlMatrixControlIndex,
                            UINT4 u4NlMatrixDSTimeMark,
                            INT4 i4ProtocolDirLocalIndex,
                            tSNMP_OCTET_STRING_TYPE * pNlMatrixDSDestAddress,
                            tSNMP_OCTET_STRING_TYPE * pNlMatrixDSSourceAddress,
                            UINT4 *pu4RetValNlMatrixDSCreateTime)
{
    tNlMatrixSD        *pNlMatrixDSEntry = NULL;
    tIpAddr             NlMatrixDSDestAddress, NlMatrixDSSourceAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhGetNlMatrixDSCreateTime \n");

    UNUSED_PARAM (u4NlMatrixDSTimeMark);
    MEMSET (&NlMatrixDSSourceAddress, 0, sizeof (tIpAddr));
    MEMSET (&NlMatrixDSDestAddress, 0, sizeof (tIpAddr));

    if (pNlMatrixDSSourceAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixDSSourceAddress,
                pNlMatrixDSSourceAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixDSSourceAddress.u4_addr[3],
                pNlMatrixDSSourceAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    if (pNlMatrixDSDestAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixDSDestAddress, pNlMatrixDSDestAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixDSDestAddress.u4_addr[3],
                pNlMatrixDSDestAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlMatrixDSSourceAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[0]);
    NlMatrixDSSourceAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[1]);
    NlMatrixDSSourceAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[2]);
    NlMatrixDSSourceAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[3]);

    NlMatrixDSDestAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[0]);
    NlMatrixDSDestAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[1]);
    NlMatrixDSDestAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[2]);
    NlMatrixDSDestAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[3]);

    pNlMatrixDSEntry =
        Rmon2NlMatrixGetDataEntry (RMON2_NLMATRIXDS_TABLE,
                                   (UINT4) i4HlMatrixControlIndex,
                                   (UINT4) i4ProtocolDirLocalIndex,
                                   &NlMatrixDSSourceAddress,
                                   &NlMatrixDSDestAddress);

    if (pNlMatrixDSEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Get entry of NlMatrixDS table is failed  \r\n");
        return SNMP_FAILURE;
    }
    if (pNlMatrixDSEntry->u4NlMatrixSDTimeMark < u4NlMatrixDSTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValNlMatrixDSCreateTime = pNlMatrixDSEntry->u4NlMatrixSDCreateTime;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNlMatrixDSCreateTime \n");
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : AlMatrixSDTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceAlMatrixSDTable
Input       :  The Indices
               HlMatrixControlIndex
               AlMatrixSDTimeMark
               NlProtocolDirLocalIndex
               AlMatrixSDSourceAddress
               AlMatrixSDDestAddress
               AlProtocolDirLocalIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceAlMatrixSDTable (INT4 i4HlMatrixControlIndex,
                                         UINT4 u4AlMatrixSDTimeMark,
                                         INT4 i4NlProtocolDirLocalIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNlMatrixSDSourceAddress,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNlMatrixSDDestAddress,
                                         INT4 i4AlProtocolDirLocalIndex)
{
    tIpAddr             NlMatrixSDSourceAddress, NlMatrixSDDestAddress;
    tAlMatrixSD        *pAlMatrixSDEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "nmhValidateIndexInstanceAlMatrixSDTable \n");
    UNUSED_PARAM (u4AlMatrixSDTimeMark);

    MEMSET (&NlMatrixSDSourceAddress, 0, sizeof (tIpAddr));
    MEMSET (&NlMatrixSDDestAddress, 0, sizeof (tIpAddr));

    if (pNlMatrixSDSourceAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixSDSourceAddress,
                pNlMatrixSDSourceAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixSDSourceAddress.u4_addr[3],
                pNlMatrixSDSourceAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    if (pNlMatrixSDDestAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixSDDestAddress, pNlMatrixSDDestAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixSDDestAddress.u4_addr[3],
                pNlMatrixSDDestAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlMatrixSDSourceAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[0]);
    NlMatrixSDSourceAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[1]);
    NlMatrixSDSourceAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[2]);
    NlMatrixSDSourceAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[3]);

    NlMatrixSDDestAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[0]);
    NlMatrixSDDestAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[1]);
    NlMatrixSDDestAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[2]);
    NlMatrixSDDestAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[3]);

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HlMatrixControlIndex < RMON2_ONE) ||
        (i4HlMatrixControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid HlMatrixControlIndex \n");
        return SNMP_FAILURE;
    }

    /* Testing the input for negative value alone is sufficient, because
     * RMON2 allows this configuration till the maximum value of interger 32 */
    if (i4NlProtocolDirLocalIndex < RMON2_ONE)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid NlProtocolDirLocalIndex \n");
        return SNMP_FAILURE;
    }

    /* Testing the input for negative value alone is sufficient, because
     * RMON2 allows this configuration till the maximum value of interger 32 */
    if (i4AlProtocolDirLocalIndex < RMON2_ONE)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid AlProtocolDirLocalIndex \n");
        return SNMP_FAILURE;
    }
    pAlMatrixSDEntry =
        Rmon2AlMatrixGetDataEntry (RMON2_ALMATRIXSD_TABLE,
                                   (UINT4) i4HlMatrixControlIndex,
                                   (UINT4) i4NlProtocolDirLocalIndex,
                                   &NlMatrixSDSourceAddress,
                                   &NlMatrixSDDestAddress,
                                   (UINT4) i4AlProtocolDirLocalIndex);
    if (pAlMatrixSDEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Get entry of AlMatrixSD table is failed  \r\n");
        return SNMP_FAILURE;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "nmhValidateIndexInstanceAlMatrixSDTable \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexAlMatrixSDTable
Input       :  The Indices
               HlMatrixControlIndex
               AlMatrixSDTimeMark
               NlProtocolDirLocalIndex
               AlMatrixSDSourceAddress
               AlMatrixSDDestAddress
               AlProtocolDirLocalIndex
Output      :  The Get First Routines gets the Lexicographicaly
               First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexAlMatrixSDTable (INT4 *pi4HlMatrixControlIndex,
                                 UINT4 *pu4AlMatrixSDTimeMark,
                                 INT4 *pi4NlProtocolDirLocalIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNlMatrixSDSourceAddress,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNlMatrixSDDestAddress,
                                 INT4 *pi4AlProtocolDirLocalIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetFirstIndexAlMatrixSDTable \n");

    return nmhGetNextIndexAlMatrixSDTable (RMON2_ZERO, pi4HlMatrixControlIndex,
                                           RMON2_ZERO, pu4AlMatrixSDTimeMark,
                                           RMON2_ZERO,
                                           pi4NlProtocolDirLocalIndex,
                                           RMON2_ZERO, pNlMatrixSDSourceAddress,
                                           RMON2_ZERO, pNlMatrixSDDestAddress,
                                           RMON2_ZERO,
                                           pi4AlProtocolDirLocalIndex);

}

/****************************************************************************
Function    :  nmhGetNextIndexAlMatrixSDTable
Input       :  The Indices
               HlMatrixControlIndex
               nextHlMatrixControlIndex
               AlMatrixSDTimeMark
               nextAlMatrixSDTimeMark
               NlProtocolDirLocalIndex
               nextProtocolDirLocalIndex
               AlMatrixSDSourceAddress
               nextAlMatrixSDSourceAddress
               AlMatrixSDDestAddress
               nextAlMatrixSDDestAddress
               AlProtocolDirLocalIndex
               nextProtocolDirLocalIndex
Output      :  The Get Next function gets the Next Index for
               the Index Value given in the Index Values. The
               Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexAlMatrixSDTable (INT4 i4HlMatrixControlIndex,
                                INT4 *pi4NextHlMatrixControlIndex,
                                UINT4 u4AlMatrixSDTimeMark,
                                UINT4 *pu4NextAlMatrixSDTimeMark,
                                INT4 i4NlProtocolDirLocalIndex,
                                INT4 *pi4NextNlProtocolDirLocalIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pNlMatrixSDSourceAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pNextNlMatrixSDSourceAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pNlMatrixSDDestAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pNextNlMatrixSDDestAddress,
                                INT4 i4AlProtocolDirLocalIndex,
                                INT4 *pi4NextAlProtocolDirLocalIndex)
{
    tAlMatrixSD        *pAlMatrixSDEntry = NULL;
    tIpAddr             NlMatrixSDSourceAddress, NlMatrixSDDestAddress;
    tIpAddr             SrcAddr, DestAddr;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNextIndexAlMatrixSDTable \n");

    UNUSED_PARAM (u4AlMatrixSDTimeMark);

    if (pNlMatrixSDSourceAddress == NULL)
    {
        MEMSET (&NlMatrixSDSourceAddress, 0, sizeof (tIpAddr));
    }
    else
    {
        MEMSET (&NlMatrixSDSourceAddress, 0, sizeof (tIpAddr));
        if (pNlMatrixSDSourceAddress->i4_Length == RMON2_IPV6_MAX_LEN)
        {
            MEMCPY (&NlMatrixSDSourceAddress,
                    pNlMatrixSDSourceAddress->pu1_OctetList,
                    RMON2_IPV6_MAX_LEN);
        }
        else
        {
            MEMCPY (&NlMatrixSDSourceAddress.u4_addr[3],
                    pNlMatrixSDSourceAddress->pu1_OctetList,
                    RMON2_IPV4_MAX_LEN);
        }
    }

    if (pNlMatrixSDDestAddress == NULL)
    {
        MEMSET (&NlMatrixSDDestAddress, 0, sizeof (tIpAddr));
    }
    else
    {
        MEMSET (&NlMatrixSDDestAddress, 0, sizeof (tIpAddr));
        if (pNlMatrixSDDestAddress->i4_Length == RMON2_IPV6_MAX_LEN)
        {
            MEMCPY (&NlMatrixSDDestAddress,
                    pNlMatrixSDDestAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
        }
        else
        {
            MEMCPY (&NlMatrixSDDestAddress.u4_addr[3],
                    pNlMatrixSDDestAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
        }
    }

    NlMatrixSDSourceAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[0]);
    NlMatrixSDSourceAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[1]);
    NlMatrixSDSourceAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[2]);
    NlMatrixSDSourceAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[3]);

    NlMatrixSDDestAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[0]);
    NlMatrixSDDestAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[1]);
    NlMatrixSDDestAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[2]);
    NlMatrixSDDestAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[3]);

    pAlMatrixSDEntry =
        Rmon2AlMatrixGetNextDataIndex (RMON2_ALMATRIXSD_TABLE,
                                       (UINT4) i4HlMatrixControlIndex,
                                       (UINT4) i4NlProtocolDirLocalIndex,
                                       &NlMatrixSDSourceAddress,
                                       &NlMatrixSDDestAddress,
                                       (UINT4) i4AlProtocolDirLocalIndex);

    if (pAlMatrixSDEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Get entry of AlMatrixSD table is failed  \r\n");
        return SNMP_FAILURE;
    }

    *pi4NextHlMatrixControlIndex =
        (INT4) pAlMatrixSDEntry->u4HlMatrixControlIndex;
    *pu4NextAlMatrixSDTimeMark = RMON2_ZERO;
    *pi4NextNlProtocolDirLocalIndex =
        (INT4) pAlMatrixSDEntry->u4NlProtocolDirLocalIndex;
    *pi4NextAlProtocolDirLocalIndex =
        (INT4) pAlMatrixSDEntry->u4AlProtocolDirLocalIndex;

    pNextNlMatrixSDSourceAddress->i4_Length =
        (INT4) pAlMatrixSDEntry->u4Rmon2NlMatrixSDIpAddrLen;
    pNextNlMatrixSDDestAddress->i4_Length =
        (INT4) pAlMatrixSDEntry->u4Rmon2NlMatrixSDIpAddrLen;

    SrcAddr.u4_addr[0] =
        OSIX_HTONL (pAlMatrixSDEntry->NlMatrixSDSourceAddress.u4_addr[0]);
    SrcAddr.u4_addr[1] =
        OSIX_HTONL (pAlMatrixSDEntry->NlMatrixSDSourceAddress.u4_addr[1]);
    SrcAddr.u4_addr[2] =
        OSIX_HTONL (pAlMatrixSDEntry->NlMatrixSDSourceAddress.u4_addr[2]);
    SrcAddr.u4_addr[3] =
        OSIX_HTONL (pAlMatrixSDEntry->NlMatrixSDSourceAddress.u4_addr[3]);

    DestAddr.u4_addr[0] =
        OSIX_HTONL (pAlMatrixSDEntry->NlMatrixSDDestAddress.u4_addr[0]);
    DestAddr.u4_addr[1] =
        OSIX_HTONL (pAlMatrixSDEntry->NlMatrixSDDestAddress.u4_addr[1]);
    DestAddr.u4_addr[2] =
        OSIX_HTONL (pAlMatrixSDEntry->NlMatrixSDDestAddress.u4_addr[2]);
    DestAddr.u4_addr[3] =
        OSIX_HTONL (pAlMatrixSDEntry->NlMatrixSDDestAddress.u4_addr[3]);

    if (pAlMatrixSDEntry->u4Rmon2NlMatrixSDIpAddrLen == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (pNextNlMatrixSDSourceAddress->pu1_OctetList,
                (UINT1 *) &SrcAddr, RMON2_IPV6_MAX_LEN);
        MEMCPY (pNextNlMatrixSDDestAddress->pu1_OctetList,
                (UINT1 *) &DestAddr, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (pNextNlMatrixSDSourceAddress->pu1_OctetList,
                (UINT1 *) &SrcAddr.u4_addr[3], RMON2_IPV4_MAX_LEN);
        MEMCPY (pNextNlMatrixSDDestAddress->pu1_OctetList,
                (UINT1 *) &DestAddr.u4_addr[3], RMON2_IPV4_MAX_LEN);
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNextIndexAlMatrixSDTable \n");
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetAlMatrixSDPkts
Input       :  The Indices
               HlMatrixControlIndex
               AlMatrixSDTimeMark
               NlProtocolDirLocalIndex
               AlMatrixSDSourceAddress
               AlMatrixSDDestAddress
               AlProtocolDirLocalIndex

               The Object 
               retValAlMatrixSDPkts
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetAlMatrixSDPkts (INT4 i4HlMatrixControlIndex, UINT4 u4AlMatrixSDTimeMark,
                      INT4 i4NlProtocolDirLocalIndex,
                      tSNMP_OCTET_STRING_TYPE * pNlMatrixSDSourceAddress,
                      tSNMP_OCTET_STRING_TYPE * pNlMatrixSDDestAddress,
                      INT4 i4AlProtocolDirLocalIndex,
                      UINT4 *pu4RetValAlMatrixSDPkts)
{
    tAlMatrixSD        *pAlMatrixSDEntry = NULL;
    tIpAddr             NlMatrixSDSourceAddress, NlMatrixSDDestAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhGetAlMatrixSDPkts \n");

    UNUSED_PARAM (u4AlMatrixSDTimeMark);
    MEMSET (&NlMatrixSDSourceAddress, 0, sizeof (tIpAddr));
    MEMSET (&NlMatrixSDDestAddress, 0, sizeof (tIpAddr));

    if (pNlMatrixSDSourceAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixSDSourceAddress,
                pNlMatrixSDSourceAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixSDSourceAddress.u4_addr[3],
                pNlMatrixSDSourceAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    if (pNlMatrixSDDestAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixSDDestAddress, pNlMatrixSDDestAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixSDDestAddress.u4_addr[3],
                pNlMatrixSDDestAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlMatrixSDSourceAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[0]);
    NlMatrixSDSourceAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[1]);
    NlMatrixSDSourceAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[2]);
    NlMatrixSDSourceAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[3]);

    NlMatrixSDDestAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[0]);
    NlMatrixSDDestAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[1]);
    NlMatrixSDDestAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[2]);
    NlMatrixSDDestAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[3]);

    pAlMatrixSDEntry =
        Rmon2AlMatrixGetDataEntry (RMON2_ALMATRIXSD_TABLE,
                                   (UINT4) i4HlMatrixControlIndex,
                                   (UINT4) i4NlProtocolDirLocalIndex,
                                   &NlMatrixSDSourceAddress,
                                   &NlMatrixSDDestAddress,
                                   (UINT4) i4AlProtocolDirLocalIndex);
    if (pAlMatrixSDEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Get entry of AlMatrixSD table is failed  \r\n");
        return SNMP_FAILURE;
    }
    if (pAlMatrixSDEntry->u4AlMatrixSDTimeMark < u4AlMatrixSDTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValAlMatrixSDPkts =
        pAlMatrixSDEntry->AlMatrixSDCurSample.u4AlMatrixSDPkts;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetAlMatrixSDPkts \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetAlMatrixSDOctets
Input       :  The Indices
               HlMatrixControlIndex
               AlMatrixSDTimeMark
               NlProtocolDirLocalIndex
               AlMatrixSDSourceAddress
               AlMatrixSDDestAddress
               AlProtocolDirLocalIndex

               The Object 
               retValAlMatrixSDOctets
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlMatrixSDOctets (INT4 i4HlMatrixControlIndex, UINT4 u4AlMatrixSDTimeMark,
                        INT4 i4NlProtocolDirLocalIndex,
                        tSNMP_OCTET_STRING_TYPE * pNlMatrixSDSourceAddress,
                        tSNMP_OCTET_STRING_TYPE * pNlMatrixSDDestAddress,
                        INT4 i4AlProtocolDirLocalIndex,
                        UINT4 *pu4RetValAlMatrixSDOctets)
{
    tAlMatrixSD        *pAlMatrixSDEntry = NULL;
    tIpAddr             NlMatrixSDSourceAddress, NlMatrixSDDestAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhGetAlMatrixSDOctets \n");

    UNUSED_PARAM (u4AlMatrixSDTimeMark);
    MEMSET (&NlMatrixSDSourceAddress, 0, sizeof (tIpAddr));
    MEMSET (&NlMatrixSDDestAddress, 0, sizeof (tIpAddr));

    if (pNlMatrixSDSourceAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixSDSourceAddress,
                pNlMatrixSDSourceAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixSDSourceAddress.u4_addr[3],
                pNlMatrixSDSourceAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    if (pNlMatrixSDDestAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixSDDestAddress, pNlMatrixSDDestAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixSDDestAddress.u4_addr[3],
                pNlMatrixSDDestAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlMatrixSDSourceAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[0]);
    NlMatrixSDSourceAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[1]);
    NlMatrixSDSourceAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[2]);
    NlMatrixSDSourceAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[3]);

    NlMatrixSDDestAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[0]);
    NlMatrixSDDestAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[1]);
    NlMatrixSDDestAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[2]);
    NlMatrixSDDestAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[3]);

    pAlMatrixSDEntry =
        Rmon2AlMatrixGetDataEntry (RMON2_ALMATRIXSD_TABLE,
                                   (UINT4) i4HlMatrixControlIndex,
                                   (UINT4) i4NlProtocolDirLocalIndex,
                                   &NlMatrixSDSourceAddress,
                                   &NlMatrixSDDestAddress,
                                   (UINT4) i4AlProtocolDirLocalIndex);

    if (pAlMatrixSDEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Get entry of AlMatrixSD table is failed  \r\n");
        return SNMP_FAILURE;
    }
    if (pAlMatrixSDEntry->u4AlMatrixSDTimeMark < u4AlMatrixSDTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValAlMatrixSDOctets =
        pAlMatrixSDEntry->AlMatrixSDCurSample.u4AlMatrixSDOctets;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetAlMatrixSDOctetss \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetAlMatrixSDCreateTime
Input       :  The Indices
               HlMatrixControlIndex
               AlMatrixSDTimeMark
               NlProtocolDirLocalIndex
               AlMatrixSDSourceAddress
               AlMatrixSDDestAddress
               AlProtocolDirLocalIndex

               The Object 
               retValAlMatrixSDCreateTime
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetAlMatrixSDCreateTime (INT4 i4HlMatrixControlIndex,
                            UINT4 u4AlMatrixSDTimeMark,
                            INT4 i4NlProtocolDirLocalIndex,
                            tSNMP_OCTET_STRING_TYPE * pNlMatrixSDSourceAddress,
                            tSNMP_OCTET_STRING_TYPE * pNlMatrixSDDestAddress,
                            INT4 i4AlProtocolDirLocalIndex,
                            UINT4 *pu4RetValAlMatrixSDCreateTime)
{
    tAlMatrixSD        *pAlMatrixSDEntry = NULL;
    tIpAddr             NlMatrixSDSourceAddress, NlMatrixSDDestAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhGetAlMatrixSDCreateTime \n");

    UNUSED_PARAM (u4AlMatrixSDTimeMark);
    MEMSET (&NlMatrixSDSourceAddress, 0, sizeof (tIpAddr));
    MEMSET (&NlMatrixSDDestAddress, 0, sizeof (tIpAddr));

    if (pNlMatrixSDSourceAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixSDSourceAddress,
                pNlMatrixSDSourceAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixSDSourceAddress.u4_addr[3],
                pNlMatrixSDSourceAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    if (pNlMatrixSDDestAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixSDDestAddress, pNlMatrixSDDestAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixSDDestAddress.u4_addr[3],
                pNlMatrixSDDestAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlMatrixSDSourceAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[0]);
    NlMatrixSDSourceAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[1]);
    NlMatrixSDSourceAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[2]);
    NlMatrixSDSourceAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixSDSourceAddress.u4_addr[3]);

    NlMatrixSDDestAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[0]);
    NlMatrixSDDestAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[1]);
    NlMatrixSDDestAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[2]);
    NlMatrixSDDestAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixSDDestAddress.u4_addr[3]);

    pAlMatrixSDEntry =
        Rmon2AlMatrixGetDataEntry (RMON2_ALMATRIXSD_TABLE,
                                   (UINT4) i4HlMatrixControlIndex,
                                   (UINT4) i4NlProtocolDirLocalIndex,
                                   &NlMatrixSDSourceAddress,
                                   &NlMatrixSDDestAddress,
                                   (UINT4) i4AlProtocolDirLocalIndex);

    if (pAlMatrixSDEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Get entry of AlMatrixSD table is failed  \r\n");
        return SNMP_FAILURE;
    }
    if (pAlMatrixSDEntry->u4AlMatrixSDTimeMark < u4AlMatrixSDTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValAlMatrixSDCreateTime = pAlMatrixSDEntry->u4AlMatrixSDCreateTime;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetAlMatrixSDCreateTime \n");
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : AlMatrixDSTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceAlMatrixDSTable
Input       :  The Indices
               HlMatrixControlIndex
               AlMatrixDSTimeMark
               NlProtocolDirLocalIndex
               AlMatrixDSDestAddress
               AlMatrixDSSourceAddress
               AlProtocolDirLocalIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceAlMatrixDSTable (INT4 i4HlMatrixControlIndex,
                                         UINT4 u4AlMatrixDSTimeMark,
                                         INT4 i4NlProtocolDirLocalIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNlMatrixDSDestAddress,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNlMatrixDSSourceAddress,
                                         INT4 i4AlProtocolDirLocalIndex)
{
    tAlMatrixSD        *pAlMatrixDSEntry = NULL;
    tIpAddr             NlMatrixDSDestAddress, NlMatrixDSSourceAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "nmhValidateIndexInstanceAlMatrixDSTable \n");
    UNUSED_PARAM (u4AlMatrixDSTimeMark);

    MEMSET (&NlMatrixDSSourceAddress, 0, sizeof (tIpAddr));
    MEMSET (&NlMatrixDSDestAddress, 0, sizeof (tIpAddr));

    if (pNlMatrixDSSourceAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixDSSourceAddress,
                pNlMatrixDSSourceAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixDSSourceAddress.u4_addr[3],
                pNlMatrixDSSourceAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    if (pNlMatrixDSDestAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixDSDestAddress, pNlMatrixDSDestAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixDSDestAddress.u4_addr[3],
                pNlMatrixDSDestAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlMatrixDSSourceAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[0]);
    NlMatrixDSSourceAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[1]);
    NlMatrixDSSourceAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[2]);
    NlMatrixDSSourceAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[3]);

    NlMatrixDSDestAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[0]);
    NlMatrixDSDestAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[1]);
    NlMatrixDSDestAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[2]);
    NlMatrixDSDestAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[3]);

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HlMatrixControlIndex < RMON2_ONE) ||
        (i4HlMatrixControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid HlMatrixControlIndex \n");
        return SNMP_FAILURE;
    }

    /* Testing the input for negative value alone is sufficient, because
     * RMON2 allows this configuration till the maximum value of interger 32 */
    if (i4NlProtocolDirLocalIndex < RMON2_ONE)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid NlProtocolDirLocalIndex \n");
        return SNMP_FAILURE;
    }

    /* Testing the input for negative value alone is sufficient, because
     * RMON2 allows this configuration till the maximum value of interger 32 */
    if (i4AlProtocolDirLocalIndex < RMON2_ONE)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid AlProtocolDirLocalIndex \n");
        return SNMP_FAILURE;
    }

    pAlMatrixDSEntry =
        Rmon2AlMatrixGetDataEntry (RMON2_ALMATRIXDS_TABLE,
                                   (UINT4) i4HlMatrixControlIndex,
                                   (UINT4) i4NlProtocolDirLocalIndex,
                                   &NlMatrixDSSourceAddress,
                                   &NlMatrixDSDestAddress,
                                   (UINT4) i4AlProtocolDirLocalIndex);
    if (pAlMatrixDSEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Get entry of AlMatrixDS table is failed \r\n");
        return SNMP_FAILURE;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "nmhValidateIndexInstanceAlMatrixDSTable \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexAlMatrixDSTable
Input       :  The Indices
               HlMatrixControlIndex
               AlMatrixDSTimeMark
               NlProtocolDirLocalIndex
               AlMatrixDSDestAddress
               AlMatrixDSSourceAddress
               AlProtocolDirLocalIndex
Output      :  The Get First Routines gets the Lexicographicaly
               First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexAlMatrixDSTable (INT4 *pi4HlMatrixControlIndex,
                                 UINT4 *pu4AlMatrixDSTimeMark,
                                 INT4 *pi4NlProtocolDirLocalIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNlMatrixDSDestAddress,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNlMatrixDSSourceAddress,
                                 INT4 *pi4AlProtocolDirLocalIndex)
{

    return nmhGetNextIndexAlMatrixDSTable (RMON2_ZERO, pi4HlMatrixControlIndex,
                                           RMON2_ZERO, pu4AlMatrixDSTimeMark,
                                           RMON2_ZERO,
                                           pi4NlProtocolDirLocalIndex,
                                           RMON2_ZERO, pNlMatrixDSDestAddress,
                                           RMON2_ZERO, pNlMatrixDSSourceAddress,
                                           RMON2_ZERO,
                                           pi4AlProtocolDirLocalIndex);

}

/****************************************************************************
Function    :  nmhGetNextIndexAlMatrixDSTable
Input       :  The Indices
               HlMatrixControlIndex
               nextHlMatrixControlIndex
               AlMatrixDSTimeMark
               nextAlMatrixDSTimeMark
               NlProtocolDirLocalIndex
               nextProtocolDirLocalIndex
               AlMatrixDSDestAddress
               nextAlMatrixDSDestAddress
               AlMatrixDSSourceAddress
               nextAlMatrixDSSourceAddress
               AlProtocolDirLocalIndex
               nextProtocolDirLocalIndex
Output      :  The Get Next function gets the Next Index for
               the Index Value given in the Index Values. The
               Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexAlMatrixDSTable (INT4 i4HlMatrixControlIndex,
                                INT4 *pi4NextHlMatrixControlIndex,
                                UINT4 u4AlMatrixDSTimeMark,
                                UINT4 *pu4NextAlMatrixDSTimeMark,
                                INT4 i4NlProtocolDirLocalIndex,
                                INT4 *pi4NextNlProtocolDirLocalIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pNlMatrixDSDestAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pNextNlMatrixDSDestAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pNlMatrixDSSourceAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pNextNlMatrixDSSourceAddress,
                                INT4 i4AlProtocolDirLocalIndex,
                                INT4 *pi4NextAlProtocolDirLocalIndex)
{
    tAlMatrixSD        *pAlMatrixDSEntry = NULL;
    tIpAddr             NlMatrixDSDestAddress, NlMatrixDSSourceAddress;
    tIpAddr             DestAddr, SrcAddr;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNextIndexAlMatrixDSTable \n");

    UNUSED_PARAM (u4AlMatrixDSTimeMark);

    if (pNlMatrixDSSourceAddress == NULL)
    {
        MEMSET (&NlMatrixDSSourceAddress, 0, sizeof (tIpAddr));
    }
    else
    {
        MEMSET (&NlMatrixDSSourceAddress, 0, sizeof (tIpAddr));
        if (pNlMatrixDSSourceAddress->i4_Length == RMON2_IPV6_MAX_LEN)
        {
            MEMCPY (&NlMatrixDSSourceAddress,
                    pNlMatrixDSSourceAddress->pu1_OctetList,
                    RMON2_IPV6_MAX_LEN);
        }
        else
        {
            MEMCPY (&NlMatrixDSSourceAddress.u4_addr[3],
                    pNlMatrixDSSourceAddress->pu1_OctetList,
                    RMON2_IPV4_MAX_LEN);
        }
    }

    if (pNlMatrixDSDestAddress == NULL)
    {
        MEMSET (&NlMatrixDSDestAddress, 0, sizeof (tIpAddr));
    }
    else
    {
        MEMSET (&NlMatrixDSDestAddress, 0, sizeof (tIpAddr));
        if (pNlMatrixDSDestAddress->i4_Length == RMON2_IPV6_MAX_LEN)
        {
            MEMCPY (&NlMatrixDSDestAddress,
                    pNlMatrixDSDestAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
        }
        else
        {
            MEMCPY (&NlMatrixDSDestAddress.u4_addr[3],
                    pNlMatrixDSDestAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
        }
    }

    NlMatrixDSSourceAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[0]);
    NlMatrixDSSourceAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[1]);
    NlMatrixDSSourceAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[2]);
    NlMatrixDSSourceAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[3]);

    NlMatrixDSDestAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[0]);
    NlMatrixDSDestAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[1]);
    NlMatrixDSDestAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[2]);
    NlMatrixDSDestAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[3]);

    pAlMatrixDSEntry =
        Rmon2AlMatrixGetNextDataIndex (RMON2_ALMATRIXDS_TABLE,
                                       (UINT4) i4HlMatrixControlIndex,
                                       (UINT4) i4NlProtocolDirLocalIndex,
                                       &NlMatrixDSSourceAddress,
                                       &NlMatrixDSDestAddress,
                                       (UINT4) i4AlProtocolDirLocalIndex);

    if (pAlMatrixDSEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Get entry of AlMatrixDS table is failed \r\n");
        return SNMP_FAILURE;
    }

    *pi4NextHlMatrixControlIndex =
        (INT4) pAlMatrixDSEntry->u4HlMatrixControlIndex;
    *pu4NextAlMatrixDSTimeMark = RMON2_ZERO;
    *pi4NextNlProtocolDirLocalIndex =
        (INT4) pAlMatrixDSEntry->u4NlProtocolDirLocalIndex;
    *pi4NextAlProtocolDirLocalIndex =
        (INT4) pAlMatrixDSEntry->u4AlProtocolDirLocalIndex;

    pNextNlMatrixDSSourceAddress->i4_Length =
        (INT4) pAlMatrixDSEntry->u4Rmon2NlMatrixSDIpAddrLen;
    pNextNlMatrixDSDestAddress->i4_Length =
        (INT4) pAlMatrixDSEntry->u4Rmon2NlMatrixSDIpAddrLen;

    MEMSET (&SrcAddr, 0, sizeof (tIpAddr));
    MEMSET (&DestAddr, 0, sizeof (tIpAddr));

    SrcAddr.u4_addr[0] =
        OSIX_HTONL (pAlMatrixDSEntry->NlMatrixSDSourceAddress.u4_addr[0]);
    SrcAddr.u4_addr[1] =
        OSIX_HTONL (pAlMatrixDSEntry->NlMatrixSDSourceAddress.u4_addr[1]);
    SrcAddr.u4_addr[2] =
        OSIX_HTONL (pAlMatrixDSEntry->NlMatrixSDSourceAddress.u4_addr[2]);
    SrcAddr.u4_addr[3] =
        OSIX_HTONL (pAlMatrixDSEntry->NlMatrixSDSourceAddress.u4_addr[3]);

    DestAddr.u4_addr[0] =
        OSIX_HTONL (pAlMatrixDSEntry->NlMatrixSDDestAddress.u4_addr[0]);
    DestAddr.u4_addr[1] =
        OSIX_HTONL (pAlMatrixDSEntry->NlMatrixSDDestAddress.u4_addr[1]);
    DestAddr.u4_addr[2] =
        OSIX_HTONL (pAlMatrixDSEntry->NlMatrixSDDestAddress.u4_addr[2]);
    DestAddr.u4_addr[3] =
        OSIX_HTONL (pAlMatrixDSEntry->NlMatrixSDDestAddress.u4_addr[3]);

    if (pAlMatrixDSEntry->u4Rmon2NlMatrixSDIpAddrLen == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (pNextNlMatrixDSSourceAddress->pu1_OctetList,
                (UINT1 *) &SrcAddr, RMON2_IPV6_MAX_LEN);
        MEMCPY (pNextNlMatrixDSDestAddress->pu1_OctetList,
                (UINT1 *) &DestAddr, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (pNextNlMatrixDSSourceAddress->pu1_OctetList,
                (UINT1 *) &SrcAddr.u4_addr[3], RMON2_IPV4_MAX_LEN);
        MEMCPY (pNextNlMatrixDSDestAddress->pu1_OctetList,
                (UINT1 *) &DestAddr.u4_addr[3], RMON2_IPV4_MAX_LEN);
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNextIndexAlMatrixDSTable \n");
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetAlMatrixDSPkts
Input       :  The Indices
               HlMatrixControlIndex
               AlMatrixDSTimeMark
               NlProtocolDirLocalIndex
               AlMatrixDSDestAddress
               AlMatrixDSSourceAddress
               AlProtocolDirLocalIndex

               The Object 
               retValAlMatrixDSPkts
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlMatrixDSPkts (INT4 i4HlMatrixControlIndex, UINT4 u4AlMatrixDSTimeMark,
                      INT4 i4NlProtocolDirLocalIndex,
                      tSNMP_OCTET_STRING_TYPE * pNlMatrixDSDestAddress,
                      tSNMP_OCTET_STRING_TYPE * pNlMatrixDSSourceAddress,
                      INT4 i4AlProtocolDirLocalIndex,
                      UINT4 *pu4RetValAlMatrixDSPkts)
{
    tAlMatrixSD        *pAlMatrixDSEntry = NULL;
    tIpAddr             NlMatrixDSDestAddress, NlMatrixDSSourceAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhGetAlMatrixDSPkts \n");

    UNUSED_PARAM (u4AlMatrixDSTimeMark);
    MEMSET (&NlMatrixDSSourceAddress, 0, sizeof (tIpAddr));
    MEMSET (&NlMatrixDSDestAddress, 0, sizeof (tIpAddr));

    if (pNlMatrixDSSourceAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixDSSourceAddress,
                pNlMatrixDSSourceAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixDSSourceAddress.u4_addr[3],
                pNlMatrixDSSourceAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    if (pNlMatrixDSDestAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixDSDestAddress, pNlMatrixDSDestAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixDSDestAddress.u4_addr[3],
                pNlMatrixDSDestAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlMatrixDSSourceAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[0]);
    NlMatrixDSSourceAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[1]);
    NlMatrixDSSourceAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[2]);
    NlMatrixDSSourceAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[3]);

    NlMatrixDSDestAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[0]);
    NlMatrixDSDestAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[1]);
    NlMatrixDSDestAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[2]);
    NlMatrixDSDestAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[3]);

    pAlMatrixDSEntry =
        Rmon2AlMatrixGetDataEntry (RMON2_ALMATRIXDS_TABLE,
                                   (UINT4) i4HlMatrixControlIndex,
                                   (UINT4) i4NlProtocolDirLocalIndex,
                                   &NlMatrixDSSourceAddress,
                                   &NlMatrixDSDestAddress,
                                   (UINT4) i4AlProtocolDirLocalIndex);

    if (pAlMatrixDSEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Get entry of AlMatrixDS table is failed \r\n");
        return SNMP_FAILURE;
    }
    if (pAlMatrixDSEntry->u4AlMatrixSDTimeMark < u4AlMatrixDSTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValAlMatrixDSPkts =
        pAlMatrixDSEntry->AlMatrixSDCurSample.u4AlMatrixSDPkts;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetAlMatrixDSPkts \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetAlMatrixDSOctets
Input       :  The Indices
               HlMatrixControlIndex
               AlMatrixDSTimeMark
               NlProtocolDirLocalIndex
               AlMatrixDSDestAddress
               AlMatrixDSSourceAddress
               AlProtocolDirLocalIndex

               The Object 
               retValAlMatrixDSOctets
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetAlMatrixDSOctets (INT4 i4HlMatrixControlIndex, UINT4 u4AlMatrixDSTimeMark,
                        INT4 i4NlProtocolDirLocalIndex,
                        tSNMP_OCTET_STRING_TYPE * pNlMatrixDSDestAddress,
                        tSNMP_OCTET_STRING_TYPE * pNlMatrixDSSourceAddress,
                        INT4 i4AlProtocolDirLocalIndex,
                        UINT4 *pu4RetValAlMatrixDSOctets)
{
    tAlMatrixSD        *pAlMatrixDSEntry = NULL;
    tIpAddr             NlMatrixDSDestAddress, NlMatrixDSSourceAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhGetAlMatrixDSOctets \n");

    UNUSED_PARAM (u4AlMatrixDSTimeMark);
    MEMSET (&NlMatrixDSSourceAddress, 0, sizeof (tIpAddr));
    MEMSET (&NlMatrixDSDestAddress, 0, sizeof (tIpAddr));

    if (pNlMatrixDSSourceAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixDSSourceAddress,
                pNlMatrixDSSourceAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixDSSourceAddress.u4_addr[3],
                pNlMatrixDSSourceAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    if (pNlMatrixDSDestAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixDSDestAddress, pNlMatrixDSDestAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixDSDestAddress.u4_addr[3],
                pNlMatrixDSDestAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlMatrixDSSourceAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[0]);
    NlMatrixDSSourceAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[1]);
    NlMatrixDSSourceAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[2]);
    NlMatrixDSSourceAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[3]);

    NlMatrixDSDestAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[0]);
    NlMatrixDSDestAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[1]);
    NlMatrixDSDestAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[2]);
    NlMatrixDSDestAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[3]);

    pAlMatrixDSEntry =
        Rmon2AlMatrixGetDataEntry (RMON2_ALMATRIXDS_TABLE,
                                   (UINT4) i4HlMatrixControlIndex,
                                   (UINT4) i4NlProtocolDirLocalIndex,
                                   &NlMatrixDSSourceAddress,
                                   &NlMatrixDSDestAddress,
                                   (UINT4) i4AlProtocolDirLocalIndex);

    if (pAlMatrixDSEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Get entry of AlMatrixDS table is failed \r\n");
        return SNMP_FAILURE;
    }
    if (pAlMatrixDSEntry->u4AlMatrixSDTimeMark < u4AlMatrixDSTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValAlMatrixDSOctets =
        pAlMatrixDSEntry->AlMatrixSDCurSample.u4AlMatrixSDOctets;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetAlMatrixDSOctets \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetAlMatrixDSCreateTime
Input       :  The Indices
               HlMatrixControlIndex
               AlMatrixDSTimeMark
               NlProtocolDirLocalIndex
               AlMatrixDSDestAddress
               AlMatrixDSSourceAddress
               AlProtocolDirLocalIndex

               The Object 
               retValAlMatrixDSCreateTime
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetAlMatrixDSCreateTime (INT4 i4HlMatrixControlIndex,
                            UINT4 u4AlMatrixDSTimeMark,
                            INT4 i4NlProtocolDirLocalIndex,
                            tSNMP_OCTET_STRING_TYPE * pNlMatrixDSDestAddress,
                            tSNMP_OCTET_STRING_TYPE * pNlMatrixDSSourceAddress,
                            INT4 i4AlProtocolDirLocalIndex,
                            UINT4 *pu4RetValAlMatrixDSCreateTime)
{
    tAlMatrixSD        *pAlMatrixDSEntry = NULL;
    tIpAddr             NlMatrixDSDestAddress, NlMatrixDSSourceAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhGetAlMatrixDSCreateTime \n");

    UNUSED_PARAM (u4AlMatrixDSTimeMark);
    MEMSET (&NlMatrixDSSourceAddress, 0, sizeof (tIpAddr));
    MEMSET (&NlMatrixDSDestAddress, 0, sizeof (tIpAddr));

    if (pNlMatrixDSSourceAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixDSSourceAddress,
                pNlMatrixDSSourceAddress->pu1_OctetList, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixDSSourceAddress.u4_addr[3],
                pNlMatrixDSSourceAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    if (pNlMatrixDSDestAddress->i4_Length == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (&NlMatrixDSDestAddress, pNlMatrixDSDestAddress->pu1_OctetList,
                RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&NlMatrixDSDestAddress.u4_addr[3],
                pNlMatrixDSDestAddress->pu1_OctetList, RMON2_IPV4_MAX_LEN);
    }

    NlMatrixDSSourceAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[0]);
    NlMatrixDSSourceAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[1]);
    NlMatrixDSSourceAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[2]);
    NlMatrixDSSourceAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixDSSourceAddress.u4_addr[3]);

    NlMatrixDSDestAddress.u4_addr[0] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[0]);
    NlMatrixDSDestAddress.u4_addr[1] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[1]);
    NlMatrixDSDestAddress.u4_addr[2] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[2]);
    NlMatrixDSDestAddress.u4_addr[3] =
        OSIX_NTOHL (NlMatrixDSDestAddress.u4_addr[3]);

    pAlMatrixDSEntry =
        Rmon2AlMatrixGetDataEntry (RMON2_ALMATRIXDS_TABLE,
                                   (UINT4) i4HlMatrixControlIndex,
                                   (UINT4) i4NlProtocolDirLocalIndex,
                                   &NlMatrixDSSourceAddress,
                                   &NlMatrixDSDestAddress,
                                   (UINT4) i4AlProtocolDirLocalIndex);
    if (pAlMatrixDSEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   " Get entry of AlMatrixDS table is failed \r\n");
        return SNMP_FAILURE;
    }
    if (pAlMatrixDSEntry->u4AlMatrixSDTimeMark < u4AlMatrixDSTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValAlMatrixDSCreateTime = pAlMatrixDSEntry->u4AlMatrixSDCreateTime;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetAlMatrixDSCreateTime \n");
    return SNMP_SUCCESS;

}
