/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmon2sz.c,v 1.3 2013/11/29 11:04:15 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _RMON2SZ_C
#include "rmn2inc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
Rmon2SizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < RMON2_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsRMON2SizingParams[i4SizingId].u4StructSize,
                              FsRMON2SizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(RMON2MemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            Rmon2SizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
Rmon2SzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsRMON2SizingParams);
    IssSzRegisterModulePoolId (pu1ModName, RMON2MemPoolIds);
    return OSIX_SUCCESS;
}

VOID
Rmon2SizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < RMON2_MAX_SIZING_ID; i4SizingId++)
    {
        if (RMON2MemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (RMON2MemPoolIds[i4SizingId]);
            RMON2MemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
