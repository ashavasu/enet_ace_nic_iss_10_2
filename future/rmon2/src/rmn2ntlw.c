/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * $Id: rmn2ntlw.c,v 1.7 2014/02/05 12:29:00 siva Exp $
 *
 * Description: This file contains the low level nmh routines for
 *              RMON2 Network Layer Matrix TopN module.            
 *
 *******************************************************************/
#include "rmn2inc.h"

/* LOW LEVEL Routines for Table : NlMatrixTopNControlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceNlMatrixTopNControlTable
 Input       :  The Indices
                NlMatrixTopNControlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceNlMatrixTopNControlTable (INT4
                                                  i4NlMatrixTopNControlIndex)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  "
               " nmhValidateIndexInstanceNlMatrixTopNControlTable \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4NlMatrixTopNControlIndex < RMON2_ONE) ||
        (i4NlMatrixTopNControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid NlMatrixTopNControlIndex\n");
        return SNMP_FAILURE;
    }
    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetTopNCtlEntry ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of NlMatrixTopN Control table"
                    " for index %d is failed \r\n", i4NlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "nmhValidateIndexInstanceNlMatrixTopNControlTable \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexNlMatrixTopNControlTable
 Input       :  The Indices
                NlMatrixTopNControlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexNlMatrixTopNControlTable (INT4 *pi4NlMatrixTopNControlIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetFirstIndexNlMatrixTopNControlTable \n");

    if ((nmhGetNextIndexNlMatrixTopNControlTable (RMON2_ZERO,
                                                  pi4NlMatrixTopNControlIndex))
        != SNMP_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get first entry of NlMatrixTopN Control table "
                   "is failed \r\n");
        return SNMP_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetFirstIndexNlMatrixTopNControlTable \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexNlMatrixTopNControlTable
 Input       :  The Indices
                NlMatrixTopNControlIndex
                nextNlMatrixTopNControlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexNlMatrixTopNControlTable (INT4 i4NlMatrixTopNControlIndex,
                                         INT4 *pi4NextNlMatrixTopNControlIndex)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNextIndexNlMatrixTopNControlTable \n");

    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetNextTopNCtlIndex ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of NlMatrixTopN Control table"
                    "for index %d is failed \r\n", i4NlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }
    *pi4NextNlMatrixTopNControlIndex =
        (INT4) pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlIndex;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetNextIndexNlMatrixTopNControlTable \n");
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetNlMatrixTopNControlMatrixIndex
 Input       :  The Indices
                NlMatrixTopNControlIndex

                The Object 
                retValNlMatrixTopNControlMatrixIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlMatrixTopNControlMatrixIndex (INT4 i4NlMatrixTopNControlIndex,
                                      INT4
                                      *pi4RetValNlMatrixTopNControlMatrixIndex)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNlMatrixTopNControlMatrixIndex \n");

    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetTopNCtlEntry ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of NlMatrixTopN Control table"
                    " for index %d is failed \r\n", i4NlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValNlMatrixTopNControlMatrixIndex =
        (INT4) pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlMatrixIndex;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetNlMatrixTopNControlMatrixIndex \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNlMatrixTopNControlRateBase
 Input       :  The Indices
                NlMatrixTopNControlIndex

                The Object 
                retValNlMatrixTopNControlRateBase
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlMatrixTopNControlRateBase (INT4 i4NlMatrixTopNControlIndex,
                                   INT4 *pi4RetValNlMatrixTopNControlRateBase)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNlMatrixTopNControlRateBase \n");

    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetTopNCtlEntry ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of NlMatrixTopN Control table"
                    " for index %d is failed \r\n", i4NlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValNlMatrixTopNControlRateBase =
        (INT4) pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlRateBase;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetNlMatrixTopNControlRateBase \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNlMatrixTopNControlTimeRemaining
 Input       :  The Indices
                NlMatrixTopNControlIndex

                The Object 
                retValNlMatrixTopNControlTimeRemaining
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlMatrixTopNControlTimeRemaining (INT4 i4NlMatrixTopNControlIndex,
                                        INT4
                                        *pi4RetValNlMatrixTopNControlTimeRemaining)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNlMatrixTopNControlTimeRemaining \n");

    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetTopNCtlEntry ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of NlMatrixTopN Control table"
                    " for index %d is failed \r\n", i4NlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }

    if (gi4MibSaveStatus == MIB_SAVE_IN_PROGRESS)
    {
        *pi4RetValNlMatrixTopNControlTimeRemaining =
            (INT4) pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlDuration;

        return SNMP_SUCCESS;

    }

    *pi4RetValNlMatrixTopNControlTimeRemaining =
        (INT4) pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlTimeRemaining;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetNlMatrixTopNControlTimeRemaining \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNlMatrixTopNControlGeneratedReports
 Input       :  The Indices
                NlMatrixTopNControlIndex

                The Object 
                retValNlMatrixTopNControlGeneratedReports
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlMatrixTopNControlGeneratedReports (INT4 i4NlMatrixTopNControlIndex,
                                           UINT4
                                           *pu4RetValNlMatrixTopNControlGeneratedReports)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNlMatrixTopNControlGeneratedReports \n");

    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetTopNCtlEntry ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of NlMatrixTopN Control table"
                    " for index %d is failed \r\n", i4NlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValNlMatrixTopNControlGeneratedReports =
        pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlGeneratedReports;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetNlMatrixTopNControlGeneratedReports \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNlMatrixTopNControlDuration
 Input       :  The Indices
                NlMatrixTopNControlIndex

                The Object 
                retValNlMatrixTopNControlDuration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlMatrixTopNControlDuration (INT4 i4NlMatrixTopNControlIndex,
                                   INT4 *pi4RetValNlMatrixTopNControlDuration)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNlMatrixTopNControlDuration \n");

    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetTopNCtlEntry ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of NlMatrixTopN Control table"
                    " for index %d is failed \r\n", i4NlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValNlMatrixTopNControlDuration =
        (INT4) pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlDuration;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetNlMatrixTopNControlDuration \n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNlMatrixTopNControlRequestedSize
 Input       :  The Indices
                NlMatrixTopNControlIndex

                The Object 
                retValNlMatrixTopNControlRequestedSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlMatrixTopNControlRequestedSize (INT4 i4NlMatrixTopNControlIndex,
                                        INT4
                                        *pi4RetValNlMatrixTopNControlRequestedSize)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNlMatrixTopNControlRequestedSize \n");

    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetTopNCtlEntry ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of NlMatrixTopN Control table"
                    " for index %d is failed \r\n", i4NlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValNlMatrixTopNControlRequestedSize =
        (INT4) pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlRequestedSize;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetNlMatrixTopNControlRequestedSize \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNlMatrixTopNControlGrantedSize
 Input       :  The Indices
                NlMatrixTopNControlIndex

                The Object 
                retValNlMatrixTopNControlGrantedSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlMatrixTopNControlGrantedSize (INT4 i4NlMatrixTopNControlIndex,
                                      INT4
                                      *pi4RetValNlMatrixTopNControlGrantedSize)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNlMatrixTopNControlGrantedSize \n");

    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetTopNCtlEntry ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of NlMatrixTopN Control table"
                    " for index %d is failed \r\n", i4NlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValNlMatrixTopNControlGrantedSize =
        (INT4) pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlGrantedSize;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetNlMatrixTopNControlGrantedSize \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNlMatrixTopNControlStartTime
 Input       :  The Indices
                NlMatrixTopNControlIndex

                The Object 
                retValNlMatrixTopNControlStartTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlMatrixTopNControlStartTime (INT4 i4NlMatrixTopNControlIndex,
                                    UINT4
                                    *pu4RetValNlMatrixTopNControlStartTime)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNlMatrixTopNControlStartTime \n");

    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetTopNCtlEntry ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of NlMatrixTopN Control table"
                    " for index %d is failed  \r\n",
                    i4NlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValNlMatrixTopNControlStartTime =
        pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlStartTime;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetNlMatrixTopNControlStartTime \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNlMatrixTopNControlOwner
 Input       :  The Indices
                NlMatrixTopNControlIndex

                The Object 
                retValNlMatrixTopNControlOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlMatrixTopNControlOwner (INT4 i4NlMatrixTopNControlIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValNlMatrixTopNControlOwner)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNlMatrixTopNControlOwner \n");

    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetTopNCtlEntry ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of NlMatrixTopN Control table"
                    " for index %d is failed  \r\n",
                    i4NlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }

    pRetValNlMatrixTopNControlOwner->i4_Length =
        (INT4) STRLEN (pNlMatrixTopNCtrlEntry->au1NlMatrixTopNControlOwner);
    MEMCPY (pRetValNlMatrixTopNControlOwner->pu1_OctetList,
            pNlMatrixTopNCtrlEntry->au1NlMatrixTopNControlOwner,
            pRetValNlMatrixTopNControlOwner->i4_Length);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNlMatrixTopNControlOwner \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNlMatrixTopNControlStatus
 Input       :  The Indices
                NlMatrixTopNControlIndex

                The Object 
                retValNlMatrixTopNControlStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlMatrixTopNControlStatus (INT4 i4NlMatrixTopNControlIndex,
                                 INT4 *pi4RetValNlMatrixTopNControlStatus)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNlMatrixTopNControlStatus \n");

    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetTopNCtlEntry ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of NlMatrixTopN Control table"
                    " for index %d is failed  \r\n",
                    i4NlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }

    *pi4RetValNlMatrixTopNControlStatus =
        (INT4) pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlStatus;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNlMatrixTopNControlStatus \n");
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetNlMatrixTopNControlMatrixIndex
 Input       :  The Indices
                NlMatrixTopNControlIndex

                The Object 
                setValNlMatrixTopNControlMatrixIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNlMatrixTopNControlMatrixIndex (INT4 i4NlMatrixTopNControlIndex,
                                      INT4
                                      i4SetValNlMatrixTopNControlMatrixIndex)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhSetNlMatrixTopNControlMatrixIndex \n");

    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetTopNCtlEntry ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of NlMatrixTopN Control table"
                    " for index %d is failed  \r\n",
                    i4NlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }

    pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlMatrixIndex =
        (UINT4) i4SetValNlMatrixTopNControlMatrixIndex;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhSetNlMatrixTopNControlMatrixIndex \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetNlMatrixTopNControlRateBase
 Input       :  The Indices
                NlMatrixTopNControlIndex

                The Object 
                setValNlMatrixTopNControlRateBase
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNlMatrixTopNControlRateBase (INT4 i4NlMatrixTopNControlIndex,
                                   INT4 i4SetValNlMatrixTopNControlRateBase)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhSetNlMatrixTopNControlRateBase \n");

    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetTopNCtlEntry ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of NlMatrixTopN Control table"
                    " for index %d is failed  \r\n",
                    i4NlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }

    pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlRateBase =
        (UINT4) i4SetValNlMatrixTopNControlRateBase;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhSetNlMatrixTopNControlRateBase \n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetNlMatrixTopNControlTimeRemaining
 Input       :  The Indices
                NlMatrixTopNControlIndex

                The Object 
                setValNlMatrixTopNControlTimeRemaining
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNlMatrixTopNControlTimeRemaining (INT4 i4NlMatrixTopNControlIndex,
                                        INT4
                                        i4SetValNlMatrixTopNControlTimeRemaining)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhSetNlMatrixTopNControlTimeRemaining \n");

    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetTopNCtlEntry ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of NlMatrixTopN Control table"
                    " for index %d is failed  \r\n",
                    i4NlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }

    pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlTimeRemaining =
        (UINT4) i4SetValNlMatrixTopNControlTimeRemaining;
    pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlDuration =
        (UINT4) i4SetValNlMatrixTopNControlTimeRemaining;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhSetNlMatrixTopNControlTimeRemaining \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetNlMatrixTopNControlRequestedSize
 Input       :  The Indices
                NlMatrixTopNControlIndex

                The Object 
                setValNlMatrixTopNControlRequestedSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNlMatrixTopNControlRequestedSize (INT4 i4NlMatrixTopNControlIndex,
                                        INT4
                                        i4SetValNlMatrixTopNControlRequestedSize)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;
    UINT4               u4Minimum = 0;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhSetNlMatrixTopNControlRequestedSize \n");

    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetTopNCtlEntry ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of NlMatrixTopN Control table"
                    " for index %d is failed  \r\n",
                    i4NlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }

    pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlRequestedSize =
        (UINT4) i4SetValNlMatrixTopNControlRequestedSize;

    u4Minimum = (UINT4) ((i4SetValNlMatrixTopNControlRequestedSize
                          < RMON2_MAX_TOPN_ENTRY) ?
                         i4SetValNlMatrixTopNControlRequestedSize :
                         RMON2_MAX_TOPN_ENTRY);

    pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlGrantedSize = u4Minimum;

    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhSetNlMatrixTopNControlRequestedSize \n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetNlMatrixTopNControlOwner
 Input       :  The Indices
                NlMatrixTopNControlIndex

                The Object 
                setValNlMatrixTopNControlOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNlMatrixTopNControlOwner (INT4 i4NlMatrixTopNControlIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValNlMatrixTopNControlOwner)
{
    UINT4               u4Minimum = 0;
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhSetNlMatrixTopNControlOwner \n");

    u4Minimum =
        (UINT4) ((pSetValNlMatrixTopNControlOwner->i4_Length <
                  RMON2_OWNER_LEN) ? pSetValNlMatrixTopNControlOwner->
                 i4_Length : RMON2_OWNER_LEN);

    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetTopNCtlEntry ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of NlMatrixTopN Control table"
                    " for index %d is failed  \r\n",
                    i4NlMatrixTopNControlIndex);
        return SNMP_FAILURE;
    }

    MEMCPY (pNlMatrixTopNCtrlEntry->au1NlMatrixTopNControlOwner,
            pSetValNlMatrixTopNControlOwner->pu1_OctetList, u4Minimum);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhSetNlMatrixTopNControlOwner \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetNlMatrixTopNControlStatus
 Input       :  The Indices
                NlMatrixTopNControlIndex

                The Object 
                setValNlMatrixTopNControlStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNlMatrixTopNControlStatus (INT4 i4NlMatrixTopNControlIndex,
                                 INT4 i4SetValNlMatrixTopNControlStatus)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;
    INT4                i4Result = 0;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhSetNlMatrixTopNControlStatus \n");

    switch (i4SetValNlMatrixTopNControlStatus)
    {
        case CREATE_AND_WAIT:

            pNlMatrixTopNCtrlEntry =
                Rmon2NlMatrixAddTopNCtlEntry ((UINT4)
                                              i4NlMatrixTopNControlIndex);
            if (pNlMatrixTopNCtrlEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC | RMON2_MEM_FAIL,
                           "Adding to NlMatrixTopN control "
                           "table is failed \n");
                return SNMP_FAILURE;
            }
            pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlStatus = NOT_READY;
            break;

        case ACTIVE:

            pNlMatrixTopNCtrlEntry =
                Rmon2NlMatrixGetTopNCtlEntry ((UINT4)
                                              i4NlMatrixTopNControlIndex);
            if (pNlMatrixTopNCtrlEntry == NULL)
            {
                RMON2_TRC1 (RMON2_DEBUG_TRC,
                            "Get entry of NlMatrixTopN Control table"
                            " for index %d is failed  \r\n",
                            i4NlMatrixTopNControlIndex);
                return SNMP_FAILURE;
            }
            pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlTimeRemaining =
                pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlDuration;

            pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlStatus = ACTIVE;
            pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlStartTime =
                OsixGetSysUpTime ();
            break;

        case NOT_IN_SERVICE:

            pNlMatrixTopNCtrlEntry =
                Rmon2NlMatrixGetTopNCtlEntry ((UINT4)
                                              i4NlMatrixTopNControlIndex);
            if (pNlMatrixTopNCtrlEntry == NULL)
            {
                RMON2_TRC1 (RMON2_DEBUG_TRC,
                            "Get entry of NlMatrixTopN Control table"
                            " for index %d is failed  \r\n",
                            i4NlMatrixTopNControlIndex);
                return SNMP_FAILURE;
            }
            Rmon2NlDeleteTopNEntries (pNlMatrixTopNCtrlEntry);
            pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlStatus =
                NOT_IN_SERVICE;
            break;

        case DESTROY:

            pNlMatrixTopNCtrlEntry =
                Rmon2NlMatrixGetTopNCtlEntry ((UINT4)
                                              i4NlMatrixTopNControlIndex);
            if (pNlMatrixTopNCtrlEntry == NULL)
            {
                RMON2_TRC1 (RMON2_DEBUG_TRC,
                            "Get entry of NlMatrixTopN Control table"
                            " for index %d is failed  \r\n",
                            i4NlMatrixTopNControlIndex);
                return SNMP_FAILURE;
            }
            Rmon2NlDeleteTopNEntries (pNlMatrixTopNCtrlEntry);
            i4Result = Rmon2NlMatrixDelTopNCtlEntry (pNlMatrixTopNCtrlEntry);
            if (i4Result == OSIX_FAILURE)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "RBTreeRemove Failure for NlMatrixTopN control entry\n");
                return SNMP_FAILURE;
            }

            break;
        default:
            RMON2_TRC (RMON2_DEBUG_TRC, " Unknown option\n");
            return SNMP_FAILURE;
            break;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhSetNlMatrixTopNControlStatus \n");
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2NlMatrixTopNControlMatrixIndex
 Input       :  The Indices
                NlMatrixTopNControlIndex

                The Object 
                testValNlMatrixTopNControlMatrixIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NlMatrixTopNControlMatrixIndex (UINT4 *pu4ErrorCode,
                                         INT4 i4NlMatrixTopNControlIndex,
                                         INT4
                                         i4TestValNlMatrixTopNControlMatrixIndex)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2NlMatrixTopNControlMatrixIndex \n");
    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4NlMatrixTopNControlIndex < RMON2_ONE) ||
        (i4NlMatrixTopNControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid NlMatrixTopNControlIndex\n");
        return SNMP_FAILURE;
    }

    if ((i4TestValNlMatrixTopNControlMatrixIndex < RMON2_ONE) ||
        (i4TestValNlMatrixTopNControlMatrixIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Error : Invalid "
                   "i4TestValNlMatrixTopNControlMatrixIndex \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetTopNCtlEntry ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry != NULL)
    {
        if (pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of NlMatrixTopN Control table is Active "
                    "for index %d \r\n", i4NlMatrixTopNControlIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry Status is not exist \n");

    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhTestv2NlMatrixTopNControlMatrixIndex \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2NlMatrixTopNControlRateBase
 Input       :  The Indices
                NlMatrixTopNControlIndex

                The Object 
                testValNlMatrixTopNControlRateBase
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NlMatrixTopNControlRateBase (UINT4 *pu4ErrorCode,
                                      INT4 i4NlMatrixTopNControlIndex,
                                      INT4 i4TestValNlMatrixTopNControlRateBase)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2NlMatrixTopNControlRateBase \n");
    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4NlMatrixTopNControlIndex < RMON2_ONE) ||
        (i4NlMatrixTopNControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid NlMatrixTopNControlIndex\n");
        return SNMP_FAILURE;
    }

    if ((i4TestValNlMatrixTopNControlRateBase < NLMATRIX_TOPN_PKTS) ||
        (i4TestValNlMatrixTopNControlRateBase >
         NLMATRIX_TOPN_HIGHCAPACITY_OCTETS))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Error : Invalid i4TestValNlMatrixTopNControlRateBase \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetTopNCtlEntry ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry != NULL)
    {
        if (pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of NlMatrixTopN Control table is Active "
                    "for index %d \r\n", i4NlMatrixTopNControlIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry Status is not exist \n");
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhTestv2NlMatrixTopNControlRateBase \n");

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2NlMatrixTopNControlTimeRemaining
 Input       :  The Indices
                NlMatrixTopNControlIndex

                The Object 
                testValNlMatrixTopNControlTimeRemaining
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NlMatrixTopNControlTimeRemaining (UINT4 *pu4ErrorCode,
                                           INT4 i4NlMatrixTopNControlIndex,
                                           INT4
                                           i4TestValNlMatrixTopNControlTimeRemaining)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2NlMatrixTopNControlTimeRemaining \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4NlMatrixTopNControlIndex < RMON2_ONE) ||
        (i4NlMatrixTopNControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid NlMatrixTopNControlIndex\n");
        return SNMP_FAILURE;
    }

    /* Testing the input for negative value alone is sufficient, because
     * RMON2 allows this configuration till the maximum value of interger 32 */
    if (i4TestValNlMatrixTopNControlTimeRemaining < RMON2_ZERO)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Error : Invalid "
                   "i4TestValNlMatrixTopNControlTimeRemaining \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetTopNCtlEntry ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry != NULL)
    {
        if (pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of NlMatrixTopN Control table is Active "
                    "for index %d \r\n", i4NlMatrixTopNControlIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry Status is not exist \n");
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhTestv2NlMatrixTopNControlTimeRemaining \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2NlMatrixTopNControlRequestedSize
 Input       :  The Indices
                NlMatrixTopNControlIndex

                The Object 
                testValNlMatrixTopNControlRequestedSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NlMatrixTopNControlRequestedSize (UINT4 *pu4ErrorCode,
                                           INT4 i4NlMatrixTopNControlIndex,
                                           INT4
                                           i4TestValNlMatrixTopNControlRequestedSize)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2NlMatrixTopNControlRequestedSize \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4NlMatrixTopNControlIndex < RMON2_ONE) ||
        (i4NlMatrixTopNControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid NlMatrixTopNControlIndex\n");
        return SNMP_FAILURE;
    }

    /* Testing the input for negative value alone is sufficient, because
     * RMON2 allows this configuration till the maximum value of interger 32 */
    if (i4TestValNlMatrixTopNControlRequestedSize < RMON2_ZERO)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Error : Invalid "
                   "i4TestValNlMatrixTopNControlRequestedSize \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetTopNCtlEntry ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry != NULL)
    {
        if (pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of NlMatrixTopN Control table is Active "
                    "for index %d \r\n", i4NlMatrixTopNControlIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry Status is not exist \n");
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhTestv2NlMatrixTopNControlRequestedSize \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2NlMatrixTopNControlOwner
 Input       :  The Indices
                NlMatrixTopNControlIndex

                The Object 
                testValNlMatrixTopNControlOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NlMatrixTopNControlOwner (UINT4 *pu4ErrorCode,
                                   INT4 i4NlMatrixTopNControlIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValNlMatrixTopNControlOwner)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhTestv2NlMatrixTopNControlOwner \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4NlMatrixTopNControlIndex < RMON2_ONE) ||
        (i4NlMatrixTopNControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid NlMatrixTopNControlIndex\n");
        return SNMP_FAILURE;
    }

    if ((pTestValNlMatrixTopNControlOwner->i4_Length < RMON2_ZERO) ||
        (pTestValNlMatrixTopNControlOwner->i4_Length > RMON2_OWNER_LEN))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Wrong length for NlMatrixTopNControlOwner "
                   "object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValNlMatrixTopNControlOwner->pu1_OctetList,
                              pTestValNlMatrixTopNControlOwner->i4_Length)
        == OSIX_FAILURE)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Invalid Characters for Owner string object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif
    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetTopNCtlEntry ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry != NULL)
    {
        if (pNlMatrixTopNCtrlEntry->u4NlMatrixTopNControlStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of NlMatrixTopN Control table is Active "
                    "for index %d \r\n", i4NlMatrixTopNControlIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry is not exist \n");
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhTestv2NlMatrixTopNControlOwner \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2NlMatrixTopNControlStatus
 Input       :  The Indices
                NlMatrixTopNControlIndex

                The Object 
                testValNlMatrixTopNControlStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NlMatrixTopNControlStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4NlMatrixTopNControlIndex,
                                    INT4 i4TestValNlMatrixTopNControlStatus)
{
    tNlMatrixTopNControl *pNlMatrixTopNCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2NlMatrixTopNControlStatus \n");
    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4NlMatrixTopNControlIndex < RMON2_ONE) ||
        (i4NlMatrixTopNControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid NlMatrixTopNControlIndex\n");
        return SNMP_FAILURE;
    }
    if ((i4TestValNlMatrixTopNControlStatus < ACTIVE) ||
        (i4TestValNlMatrixTopNControlStatus > DESTROY))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Wrong value for NlMatrixTopNControlStatus "
                   "object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pNlMatrixTopNCtrlEntry =
        Rmon2NlMatrixGetTopNCtlEntry ((UINT4) i4NlMatrixTopNControlIndex);

    if (pNlMatrixTopNCtrlEntry == NULL)
    {
        if (i4TestValNlMatrixTopNControlStatus == CREATE_AND_WAIT)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "SNMP Failure: Entry is not exist \n");
        return SNMP_FAILURE;

    }
    else
    {
        if ((i4TestValNlMatrixTopNControlStatus == ACTIVE) ||
            (i4TestValNlMatrixTopNControlStatus == NOT_IN_SERVICE) ||
            (i4TestValNlMatrixTopNControlStatus == DESTROY))
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "The given entry is already exist \n");
    }

    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhTestv2NlMatrixTopNControlStatus \n");
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2NlMatrixTopNControlTable
 Input       :  The Indices
                NlMatrixTopNControlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NlMatrixTopNControlTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : NlMatrixTopNTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceNlMatrixTopNTable
 Input       :  The Indices
                NlMatrixTopNControlIndex
                NlMatrixTopNIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceNlMatrixTopNTable (INT4 i4NlMatrixTopNControlIndex,
                                           INT4 i4NlMatrixTopNIndex)
{
    tNlMatrixTopN      *pNlMatrixTopNEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "nmhValidateIndexInstanceNlMatrixTopNTable \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4NlMatrixTopNControlIndex < RMON2_ONE) ||
        (i4NlMatrixTopNControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid NlMatrixTopNControlIndex\n");
        return SNMP_FAILURE;
    }

    if ((i4NlMatrixTopNIndex < RMON2_ONE) ||
        (i4NlMatrixTopNIndex > RMON2_MAX_MATRIX_TOPN_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid NlMatrixTopNIndex \n");
        return SNMP_FAILURE;
    }

    pNlMatrixTopNEntry =
        Rmon2NlMatrixGetTopNEntry ((UINT4) i4NlMatrixTopNControlIndex,
                                   (UINT4) i4NlMatrixTopNIndex);

    if (pNlMatrixTopNEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of NlMatrixTopN table is failed \r\n");
        return SNMP_FAILURE;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "nmhValidateIndexInstanceNlMatrixTopNTable \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexNlMatrixTopNTable
 Input       :  The Indices
                NlMatrixTopNControlIndex
                NlMatrixTopNIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexNlMatrixTopNTable (INT4 *pi4NlMatrixTopNControlIndex,
                                   INT4 *pi4NlMatrixTopNIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetFirstIndexNlMatrixTopNTable \n");

    if (nmhGetNextIndexNlMatrixTopNTable (RMON2_ZERO,
                                          pi4NlMatrixTopNControlIndex,
                                          RMON2_ZERO,
                                          pi4NlMatrixTopNIndex) != SNMP_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get first entry of NlMatrixTopN table is failed \r\n");
        return SNMP_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetFirstIndexNlMatrixTopNTable \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexNlMatrixTopNTable
 Input       :  The Indices
                NlMatrixTopNControlIndex
                nextNlMatrixTopNControlIndex
                NlMatrixTopNIndex
                nextNlMatrixTopNIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexNlMatrixTopNTable (INT4 i4NlMatrixTopNControlIndex,
                                  INT4 *pi4NextNlMatrixTopNControlIndex,
                                  INT4 i4NlMatrixTopNIndex,
                                  INT4 *pi4NextNlMatrixTopNIndex)
{
    tNlMatrixTopN      *pNlMatrixTopNEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNextIndexNlMatrixTopNTable \n");

    pNlMatrixTopNEntry =
        Rmon2NlMatrixGetNextTopNIndex ((UINT4) i4NlMatrixTopNControlIndex,
                                       (UINT4) i4NlMatrixTopNIndex);

    if (pNlMatrixTopNEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of NlMatrixTopN table is failed \r\n");
        return SNMP_FAILURE;
    }
    *pi4NextNlMatrixTopNControlIndex =
        (INT4) pNlMatrixTopNEntry->u4NlMatrixTopNControlIndex;
    *pi4NextNlMatrixTopNIndex = (INT4) pNlMatrixTopNEntry->u4NlMatrixTopNIndex;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNextIndexNlMatrixTopNTable \n");
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetNlMatrixTopNProtocolDirLocalIndex
 Input       :  The Indices
                NlMatrixTopNControlIndex
                NlMatrixTopNIndex

                The Object 
                retValNlMatrixTopNProtocolDirLocalIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlMatrixTopNProtocolDirLocalIndex (INT4 i4NlMatrixTopNControlIndex,
                                         INT4 i4NlMatrixTopNIndex,
                                         INT4
                                         *pi4RetValNlMatrixTopNProtocolDirLocalIndex)
{
    tNlMatrixTopN      *pNlMatrixTopNEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNlMatrixTopNProtocolDirLocalIndex \n");

    pNlMatrixTopNEntry =
        Rmon2NlMatrixGetTopNEntry ((UINT4) i4NlMatrixTopNControlIndex,
                                   (UINT4) i4NlMatrixTopNIndex);

    if (pNlMatrixTopNEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of NlMatrixTopN table is failed \r\n");
        return SNMP_FAILURE;
    }
    *pi4RetValNlMatrixTopNProtocolDirLocalIndex =
        (INT4) pNlMatrixTopNEntry->u4NlMatrixTopNProtocolDirLocalIndex;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetNlMatrixTopNProtocolDirLocalIndex \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNlMatrixTopNSourceAddress
 Input       :  The Indices
                NlMatrixTopNControlIndex
                NlMatrixTopNIndex

                The Object 
                retValNlMatrixTopNSourceAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlMatrixTopNSourceAddress (INT4 i4NlMatrixTopNControlIndex,
                                 INT4 i4NlMatrixTopNIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValNlMatrixTopNSourceAddress)
{
    tNlMatrixTopN      *pNlMatrixTopNEntry = NULL;
    tIpAddr             SrcAddress;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNlMatrixTopNSourceAddress \n");

    MEMSET (&SrcAddress, 0, sizeof (tIpAddr));

    pNlMatrixTopNEntry =
        Rmon2NlMatrixGetTopNEntry ((UINT4) i4NlMatrixTopNControlIndex,
                                   (UINT4) i4NlMatrixTopNIndex);

    if (pNlMatrixTopNEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of NlMatrixTopN table is failed \r\n");
        return SNMP_FAILURE;
    }

    pRetValNlMatrixTopNSourceAddress->i4_Length =
        (INT4) pNlMatrixTopNEntry->u4NlMatrixTopNIpAddrLen;

    SrcAddress.u4_addr[0] =
        OSIX_NTOHL (pNlMatrixTopNEntry->NlMatrixTopNSourceAddress.u4_addr[0]);
    SrcAddress.u4_addr[1] =
        OSIX_NTOHL (pNlMatrixTopNEntry->NlMatrixTopNSourceAddress.u4_addr[1]);
    SrcAddress.u4_addr[2] =
        OSIX_NTOHL (pNlMatrixTopNEntry->NlMatrixTopNSourceAddress.u4_addr[2]);
    SrcAddress.u4_addr[3] =
        OSIX_NTOHL (pNlMatrixTopNEntry->NlMatrixTopNSourceAddress.u4_addr[3]);

    if (pNlMatrixTopNEntry->u4NlMatrixTopNIpAddrLen == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (pRetValNlMatrixTopNSourceAddress->pu1_OctetList,
                (UINT1 *) &SrcAddress, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (pRetValNlMatrixTopNSourceAddress->pu1_OctetList,
                (UINT1 *) &SrcAddress.u4_addr[3], RMON2_IPV4_MAX_LEN);
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNlMatrixTopNSourceAddress \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNlMatrixTopNDestAddress
 Input       :  The Indices
                NlMatrixTopNControlIndex
                NlMatrixTopNIndex

                The Object 
                retValNlMatrixTopNDestAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlMatrixTopNDestAddress (INT4 i4NlMatrixTopNControlIndex,
                               INT4 i4NlMatrixTopNIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValNlMatrixTopNDestAddress)
{
    tNlMatrixTopN      *pNlMatrixTopNEntry = NULL;
    tIpAddr             DestAddress;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhGetNlMatrixTopNDestAddress \n");

    MEMSET (&DestAddress, 0, sizeof (tIpAddr));

    pNlMatrixTopNEntry =
        Rmon2NlMatrixGetTopNEntry ((UINT4) i4NlMatrixTopNControlIndex,
                                   (UINT4) i4NlMatrixTopNIndex);

    if (pNlMatrixTopNEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of NlMatrixTopN table is failed \r\n");
        return SNMP_FAILURE;
    }
    pRetValNlMatrixTopNDestAddress->i4_Length =
        (INT4) pNlMatrixTopNEntry->u4NlMatrixTopNIpAddrLen;

    DestAddress.u4_addr[0] =
        OSIX_NTOHL (pNlMatrixTopNEntry->NlMatrixTopNDestAddress.u4_addr[0]);
    DestAddress.u4_addr[1] =
        OSIX_NTOHL (pNlMatrixTopNEntry->NlMatrixTopNDestAddress.u4_addr[1]);
    DestAddress.u4_addr[2] =
        OSIX_NTOHL (pNlMatrixTopNEntry->NlMatrixTopNDestAddress.u4_addr[2]);
    DestAddress.u4_addr[3] =
        OSIX_NTOHL (pNlMatrixTopNEntry->NlMatrixTopNDestAddress.u4_addr[3]);

    if (pNlMatrixTopNEntry->u4NlMatrixTopNIpAddrLen == RMON2_IPV6_MAX_LEN)
    {
        MEMCPY (pRetValNlMatrixTopNDestAddress->pu1_OctetList,
                &DestAddress, RMON2_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (pRetValNlMatrixTopNDestAddress->pu1_OctetList,
                &DestAddress.u4_addr[3], RMON2_IPV4_MAX_LEN);
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNlMatrixTopNDestAddress \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNlMatrixTopNPktRate
 Input       :  The Indices
                NlMatrixTopNControlIndex
                NlMatrixTopNIndex

                The Object 
                retValNlMatrixTopNPktRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlMatrixTopNPktRate (INT4 i4NlMatrixTopNControlIndex,
                           INT4 i4NlMatrixTopNIndex,
                           UINT4 *pu4RetValNlMatrixTopNPktRate)
{
    tNlMatrixTopN      *pNlMatrixTopNEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhGetNlMatrixTopNDestAddress \n");

    pNlMatrixTopNEntry =
        Rmon2NlMatrixGetTopNEntry ((UINT4) i4NlMatrixTopNControlIndex,
                                   (UINT4) i4NlMatrixTopNIndex);

    if (pNlMatrixTopNEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of NlMatrixTopN table is failed \r\n");
        return SNMP_FAILURE;
    }
    *pu4RetValNlMatrixTopNPktRate = pNlMatrixTopNEntry->u4NlMatrixTopNPktRate;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNlMatrixTopNDestAddress \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNlMatrixTopNReversePktRate
 Input       :  The Indices
                NlMatrixTopNControlIndex
                NlMatrixTopNIndex

                The Object 
                retValNlMatrixTopNReversePktRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlMatrixTopNReversePktRate (INT4 i4NlMatrixTopNControlIndex,
                                  INT4 i4NlMatrixTopNIndex,
                                  UINT4 *pu4RetValNlMatrixTopNReversePktRate)
{
    tNlMatrixTopN      *pNlMatrixTopNEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNlMatrixTopNReversePktRate \n");

    pNlMatrixTopNEntry =
        Rmon2NlMatrixGetTopNEntry ((UINT4) i4NlMatrixTopNControlIndex,
                                   (UINT4) i4NlMatrixTopNIndex);

    if (pNlMatrixTopNEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of NlMatrixTopN table is failed \r\n");
        return SNMP_FAILURE;
    }
    *pu4RetValNlMatrixTopNReversePktRate =
        pNlMatrixTopNEntry->u4NlMatrixTopNReversePktRate;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNlMatrixTopNReversePktRate \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNlMatrixTopNOctetRate
 Input       :  The Indices
                NlMatrixTopNControlIndex
                NlMatrixTopNIndex

                The Object 
                retValNlMatrixTopNOctetRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlMatrixTopNOctetRate (INT4 i4NlMatrixTopNControlIndex,
                             INT4 i4NlMatrixTopNIndex,
                             UINT4 *pu4RetValNlMatrixTopNOctetRate)
{
    tNlMatrixTopN      *pNlMatrixTopNEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhGetNlMatrixTopNOctetRate \n");

    pNlMatrixTopNEntry =
        Rmon2NlMatrixGetTopNEntry ((UINT4) i4NlMatrixTopNControlIndex,
                                   (UINT4) i4NlMatrixTopNIndex);

    if (pNlMatrixTopNEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of NlMatrixTopN table is failed \r\n");
        return SNMP_FAILURE;
    }
    *pu4RetValNlMatrixTopNOctetRate =
        (UINT4) pNlMatrixTopNEntry->u4NlMatrixTopNOctetRate;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNlMatrixTopNOctetRate \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNlMatrixTopNReverseOctetRate
 Input       :  The Indices
                NlMatrixTopNControlIndex
                NlMatrixTopNIndex

                The Object 
                retValNlMatrixTopNReverseOctetRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNlMatrixTopNReverseOctetRate (INT4 i4NlMatrixTopNControlIndex,
                                    INT4 i4NlMatrixTopNIndex,
                                    UINT4
                                    *pu4RetValNlMatrixTopNReverseOctetRate)
{
    tNlMatrixTopN      *pNlMatrixTopNEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNlMatrixTopNReverseOctetRate \n");

    pNlMatrixTopNEntry =
        Rmon2NlMatrixGetTopNEntry ((UINT4) i4NlMatrixTopNControlIndex,
                                   (UINT4) i4NlMatrixTopNIndex);

    if (pNlMatrixTopNEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get entry of NlMatrixTopN table is failed \r\n");
        return SNMP_FAILURE;
    }
    *pu4RetValNlMatrixTopNReverseOctetRate =
        (UINT4) pNlMatrixTopNEntry->u4NlMatrixTopNReverseOctetRate;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetNlMatrixTopNReverseOctetRate \n");
    return SNMP_SUCCESS;
}
