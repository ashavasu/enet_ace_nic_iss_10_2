/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * Description: This file contains the low level nmh routines for
 *              RMON2 Usr History module.            
 *
 * $Id: rmn2uhlw.c,v 1.11 2014/02/05 12:29:00 siva Exp $
 *******************************************************************/
#include "rmn2inc.h"

/* LOW LEVEL Routines for Table : UsrHistoryControlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceUsrHistoryControlTable
 Input       :  The Indices
                UsrHistoryControlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceUsrHistoryControlTable (INT4 i4UsrHistoryControlIndex)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "nmhValidateIndexInstanceUsrHistoryControlTable \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4UsrHistoryControlIndex < RMON2_ONE) ||
        (i4UsrHistoryControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid UsrHistoryControlIndex \n");
        return SNMP_FAILURE;
    }
    pUsrHistoryCtrlEntry =
        Rmon2UsrHistGetCtrlEntry ((UINT4) i4UsrHistoryControlIndex);

    if (pUsrHistoryCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of User History Control table"
                    " for index : %d is failed \r\n", i4UsrHistoryControlIndex);
        return SNMP_FAILURE;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "nmhValidateIndexInstanceUsrHistoryControlTable \n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexUsrHistoryControlTable
 Input       :  The Indices
                UsrHistoryControlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexUsrHistoryControlTable (INT4 *pi4UsrHistoryControlIndex)
{

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetFirstIndexUsrHistoryControlTable \n");

    if (nmhGetNextIndexUsrHistoryControlTable (RMON2_ZERO,
                                               pi4UsrHistoryControlIndex)
        != SNMP_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get first entry of UsrHistory Control table is "
                   "failed \r\n");
        return SNMP_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetFirstIndexUsrHistoryControlTable \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexUsrHistoryControlTable
 Input       :  The Indices
                UsrHistoryControlIndex
                nextUsrHistoryControlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexUsrHistoryControlTable (INT4 i4UsrHistoryControlIndex,
                                       INT4 *pi4NextUsrHistoryControlIndex)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNextIndexUsrHistoryControlTable \n");

    pUsrHistoryCtrlEntry =
        Rmon2UsrHistGetNextCtrlIndex ((UINT4) i4UsrHistoryControlIndex);

    if (pUsrHistoryCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of User History Control table"
                    " for index : %d is failed \r\n", i4UsrHistoryControlIndex);
        return SNMP_FAILURE;
    }
    *pi4NextUsrHistoryControlIndex =
        (INT4) pUsrHistoryCtrlEntry->u4UsrHistoryControlIndex;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetNextIndexUsrHistoryControlTable \n");
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetUsrHistoryControlObjects
 Input       :  The Indices
                UsrHistoryControlIndex

                The Object 
                retValUsrHistoryControlObjects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsrHistoryControlObjects (INT4 i4UsrHistoryControlIndex,
                                INT4 *pi4RetValUsrHistoryControlObjects)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetUsrHistoryControlObjects \n");

    pUsrHistoryCtrlEntry =
        Rmon2UsrHistGetCtrlEntry ((UINT4) i4UsrHistoryControlIndex);

    if (pUsrHistoryCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of User History Control table"
                    " for index : %d is failed \r\n", i4UsrHistoryControlIndex);
        return SNMP_FAILURE;
    }

    *pi4RetValUsrHistoryControlObjects =
        (INT4) pUsrHistoryCtrlEntry->u4UsrHistoryControlObjects;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetUsrHistoryControlObjects \n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUsrHistoryControlBucketsRequested
 Input       :  The Indices
                UsrHistoryControlIndex

                The Object 
                retValUsrHistoryControlBucketsRequested
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsrHistoryControlBucketsRequested (INT4 i4UsrHistoryControlIndex,
                                         INT4
                                         *pi4RetValUsrHistoryControlBucketsRequested)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetUsrHistoryControlBucketsRequested \n");

    pUsrHistoryCtrlEntry =
        Rmon2UsrHistGetCtrlEntry ((UINT4) i4UsrHistoryControlIndex);

    if (pUsrHistoryCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of User History Control table"
                    " for index : %d is failed \r\n", i4UsrHistoryControlIndex);
        return SNMP_FAILURE;
    }

    *pi4RetValUsrHistoryControlBucketsRequested =
        (INT4) pUsrHistoryCtrlEntry->u4UsrHistoryControlBucketsRequested;

    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetUsrHistoryControlBucketsRequested \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUsrHistoryControlBucketsGranted
 Input       :  The Indices
                UsrHistoryControlIndex

                The Object 
                retValUsrHistoryControlBucketsGranted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsrHistoryControlBucketsGranted (INT4 i4UsrHistoryControlIndex,
                                       INT4
                                       *pi4RetValUsrHistoryControlBucketsGranted)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetUsrHistoryControlBucketsGranted \n");

    pUsrHistoryCtrlEntry =
        Rmon2UsrHistGetCtrlEntry ((UINT4) i4UsrHistoryControlIndex);

    if (pUsrHistoryCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of User History Control table"
                    " for index : %d is failed \r\n", i4UsrHistoryControlIndex);
        return SNMP_FAILURE;
    }

    *pi4RetValUsrHistoryControlBucketsGranted =
        (INT4) pUsrHistoryCtrlEntry->u4UsrHistoryControlBucketsGranted;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetUsrHistoryControlBucketsGranted \n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUsrHistoryControlInterval
 Input       :  The Indices
                UsrHistoryControlIndex

                The Object 
                retValUsrHistoryControlInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsrHistoryControlInterval (INT4 i4UsrHistoryControlIndex,
                                 INT4 *pi4RetValUsrHistoryControlInterval)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetUsrHistoryControlInterval \n");

    pUsrHistoryCtrlEntry =
        Rmon2UsrHistGetCtrlEntry ((UINT4) i4UsrHistoryControlIndex);

    if (pUsrHistoryCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of User History Control table"
                    " for index : %d is failed \r\n", i4UsrHistoryControlIndex);
        return SNMP_FAILURE;
    }

    *pi4RetValUsrHistoryControlInterval =
        (INT4) pUsrHistoryCtrlEntry->u4UsrHistoryControlInterval;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetUsrHistoryControlInterval \n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetUsrHistoryControlOwner
 Input       :  The Indices
                UsrHistoryControlIndex

                The Object 
                retValUsrHistoryControlOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsrHistoryControlOwner (INT4 i4UsrHistoryControlIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValUsrHistoryControlOwner)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhGetUsrHistoryControlOwner \n");

    pUsrHistoryCtrlEntry =
        Rmon2UsrHistGetCtrlEntry ((UINT4) i4UsrHistoryControlIndex);

    if (pUsrHistoryCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of User History Control table"
                    " for index : %d is failed \r\n", i4UsrHistoryControlIndex);
        return SNMP_FAILURE;
    }

    pRetValUsrHistoryControlOwner->i4_Length =
        (INT4) STRLEN (pUsrHistoryCtrlEntry->au1UsrHistoryControlOwner);
    MEMCPY (pRetValUsrHistoryControlOwner->pu1_OctetList,
            pUsrHistoryCtrlEntry->au1UsrHistoryControlOwner,
            pRetValUsrHistoryControlOwner->i4_Length);

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetUsrHistoryControlOwner \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUsrHistoryControlStatus
 Input       :  The Indices
                UsrHistoryControlIndex

                The Object 
                retValUsrHistoryControlStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsrHistoryControlStatus (INT4 i4UsrHistoryControlIndex,
                               INT4 *pi4RetValUsrHistoryControlStatus)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhGetUsrHistoryControlStatus \n");

    pUsrHistoryCtrlEntry =
        Rmon2UsrHistGetCtrlEntry ((UINT4) i4UsrHistoryControlIndex);

    if (pUsrHistoryCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of User History Control table"
                    " for index : %d is failed \r\n", i4UsrHistoryControlIndex);
        return SNMP_FAILURE;
    }

    *pi4RetValUsrHistoryControlStatus =
        (INT4) pUsrHistoryCtrlEntry->u4UsrHistoryControlStatus;

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetUsrHistoryControlStatus \n");
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetUsrHistoryControlObjects
 Input       :  The Indices
                UsrHistoryControlIndex

                The Object 
                setValUsrHistoryControlObjects
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetUsrHistoryControlObjects (INT4 i4UsrHistoryControlIndex,
                                INT4 i4SetValUsrHistoryControlObjects)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhSetUsrHistoryControlObjects \n");

    pUsrHistoryCtrlEntry =
        Rmon2UsrHistGetCtrlEntry ((UINT4) i4UsrHistoryControlIndex);

    if (pUsrHistoryCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of User History Control table"
                    " for index : %d is failed \r\n", i4UsrHistoryControlIndex);
        return SNMP_FAILURE;
    }

    pUsrHistoryCtrlEntry->u4UsrHistoryControlObjects =
        (UINT4) i4SetValUsrHistoryControlObjects;
    Rmon2UsrHistDelObjEntry ((UINT4) i4UsrHistoryControlIndex);
    Rmon2UsrHistAddObjEntry ((UINT4) i4UsrHistoryControlIndex);

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhSetUsrHistoryControlObjects \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetUsrHistoryControlBucketsRequested
 Input       :  The Indices
                UsrHistoryControlIndex

                The Object 
                setValUsrHistoryControlBucketsRequested
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetUsrHistoryControlBucketsRequested (INT4 i4UsrHistoryControlIndex,
                                         INT4
                                         i4SetValUsrHistoryControlBucketsRequested)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;
    UINT4               u4Minimum = 0;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhSetUsrHistoryControlBucketsRequested \n");

    pUsrHistoryCtrlEntry =
        Rmon2UsrHistGetCtrlEntry ((UINT4) i4UsrHistoryControlIndex);

    if (pUsrHistoryCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of User History Control table"
                    " for index : %d is failed \r\n", i4UsrHistoryControlIndex);
        return SNMP_FAILURE;
    }

    pUsrHistoryCtrlEntry->u4UsrHistoryControlBucketsRequested =
        (UINT4) i4SetValUsrHistoryControlBucketsRequested;

    u4Minimum = (UINT4) ((i4SetValUsrHistoryControlBucketsRequested
                          < RMON2_MAX_USR_HISTORY_BUCKETS_GRANT) ?
                         i4SetValUsrHistoryControlBucketsRequested :
                         RMON2_MAX_USR_HISTORY_BUCKETS_GRANT);

    pUsrHistoryCtrlEntry->u4UsrHistoryControlBucketsGranted = u4Minimum;

    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhSetUsrHistoryControlBucketsRequested \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetUsrHistoryControlInterval
 Input       :  The Indices
                UsrHistoryControlIndex

                The Object 
                setValUsrHistoryControlInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetUsrHistoryControlInterval (INT4 i4UsrHistoryControlIndex,
                                 INT4 i4SetValUsrHistoryControlInterval)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhSetUsrHistoryControlInterval \n");

    pUsrHistoryCtrlEntry =
        Rmon2UsrHistGetCtrlEntry ((UINT4) i4UsrHistoryControlIndex);

    if (pUsrHistoryCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of User History Control table"
                    " for index : %d is failed \r\n", i4UsrHistoryControlIndex);
        return SNMP_FAILURE;
    }

    pUsrHistoryCtrlEntry->u4UsrHistoryControlInterval =
        (UINT4) i4SetValUsrHistoryControlInterval;
    pUsrHistoryCtrlEntry->u4RemainingTime =
        (UINT4) i4SetValUsrHistoryControlInterval;

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhSetUsrHistoryControlInterval \n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetUsrHistoryControlOwner
 Input       :  The Indices
                UsrHistoryControlIndex

                The Object 
                setValUsrHistoryControlOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetUsrHistoryControlOwner (INT4 i4UsrHistoryControlIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValUsrHistoryControlOwner)
{
    UINT4               u4Minimum;
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhSetUsrHistoryControlOwner \n");

    u4Minimum =
        (UINT4) ((pSetValUsrHistoryControlOwner->i4_Length <
                  RMON2_OWNER_LEN) ? pSetValUsrHistoryControlOwner->
                 i4_Length : RMON2_OWNER_LEN);

    pUsrHistoryCtrlEntry =
        Rmon2UsrHistGetCtrlEntry ((UINT4) i4UsrHistoryControlIndex);

    if (pUsrHistoryCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of User History Control table"
                    " for index : %d is failed \r\n", i4UsrHistoryControlIndex);
        return SNMP_FAILURE;
    }

    MEMCPY (pUsrHistoryCtrlEntry->au1UsrHistoryControlOwner,
            pSetValUsrHistoryControlOwner->pu1_OctetList, u4Minimum);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhSetUsrHistoryControlOwner \n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetUsrHistoryControlStatus
 Input       :  The Indices
                UsrHistoryControlIndex

                The Object 
                setValUsrHistoryControlStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetUsrHistoryControlStatus (INT4 i4UsrHistoryControlIndex,
                               INT4 i4SetValUsrHistoryControlStatus)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;
    INT4                i4Result;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY  nmhSetUsrHistoryControlStatus \n");

    switch (i4SetValUsrHistoryControlStatus)
    {
        case CREATE_AND_WAIT:

            pUsrHistoryCtrlEntry =
                Rmon2UsrHistAddCtrlEntry ((UINT4) i4UsrHistoryControlIndex);
            if (pUsrHistoryCtrlEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC | RMON2_MEM_FAIL,
                           "Adding of UsrHistory control table "
                           "is failed \n");
                return SNMP_FAILURE;
            }
            pUsrHistoryCtrlEntry->u4UsrHistoryControlStatus = NOT_READY;
            break;

        case ACTIVE:

            pUsrHistoryCtrlEntry =
                Rmon2UsrHistGetCtrlEntry ((UINT4) i4UsrHistoryControlIndex);
            if (pUsrHistoryCtrlEntry == NULL)
            {
                RMON2_TRC1 (RMON2_DEBUG_TRC,
                            "Get entry of User History Control table"
                            " for index : %d is failed \r\n",
                            i4UsrHistoryControlIndex);
                return SNMP_FAILURE;
            }

            if (Rmon2UsrHistAddDataEntry ((UINT4) i4UsrHistoryControlIndex)
                != OSIX_SUCCESS)
            {
                RMON2_TRC1 (RMON2_DEBUG_TRC,
                            "User History creation"
                            " for control index : %d is failed \r\n",
                            i4UsrHistoryControlIndex);
                return SNMP_FAILURE;
            }
            pUsrHistoryCtrlEntry->u4UsrHistoryControlStatus = ACTIVE;
            break;

        case NOT_IN_SERVICE:

            pUsrHistoryCtrlEntry =
                Rmon2UsrHistGetCtrlEntry ((UINT4) i4UsrHistoryControlIndex);
            if (pUsrHistoryCtrlEntry == NULL)
            {
                RMON2_TRC1 (RMON2_DEBUG_TRC,
                            "Get entry of User History Control table"
                            " for index : %d is failed \r\n",
                            i4UsrHistoryControlIndex);
                return SNMP_FAILURE;
            }

            Rmon2UsrHistDelDataEntry ((UINT4) i4UsrHistoryControlIndex);
            pUsrHistoryCtrlEntry->u4UsrHistoryControlStatus = NOT_IN_SERVICE;

            break;

        case DESTROY:

            pUsrHistoryCtrlEntry =
                Rmon2UsrHistGetCtrlEntry ((UINT4) i4UsrHistoryControlIndex);
            if (pUsrHistoryCtrlEntry == NULL)
            {
                RMON2_TRC1 (RMON2_DEBUG_TRC,
                            "Get entry of User History Control table"
                            " for index : %d is failed \r\n",
                            i4UsrHistoryControlIndex);
                return SNMP_FAILURE;
            }

            Rmon2UsrHistDelDataEntry ((UINT4) i4UsrHistoryControlIndex);
            Rmon2UsrHistDelObjEntry ((UINT4) i4UsrHistoryControlIndex);
            i4Result = Rmon2UsrHistDelCtrlEntry (pUsrHistoryCtrlEntry);
            if (i4Result == OSIX_FAILURE)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "RBTreeRemove Failure for Usr History control "
                           "entry \n");
                return SNMP_FAILURE;
            }
            break;
        default:
            RMON2_TRC (RMON2_DEBUG_TRC, "Unknown option \n");
            break;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhSetUsrHistoryControlStatus \n");
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2UsrHistoryControlObjects
 Input       :  The Indices
                UsrHistoryControlIndex

                The Object 
                testValUsrHistoryControlObjects
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2UsrHistoryControlObjects (UINT4 *pu4ErrorCode,
                                   INT4 i4UsrHistoryControlIndex,
                                   INT4 i4TestValUsrHistoryControlObjects)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhTestv2UsrHistoryControlObjects \n");
    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4UsrHistoryControlIndex < RMON2_ONE) ||
        (i4UsrHistoryControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid UsrHistoryControlIndex \n");
        return SNMP_FAILURE;
    }

    /* MAX_RMON2_USRHISTOBJ is changed to MAX_RMON2_USRHIST_OBJ (65535)
       changed as a part of zero regression failure in RMON module */
    if ((i4TestValUsrHistoryControlObjects < RMON2_ONE) ||
        (i4TestValUsrHistoryControlObjects > MAX_RMON2_USRHIST_OBJ))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Wrong value for UsrHistoryControlObjects "
                   " object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pUsrHistoryCtrlEntry =
        Rmon2UsrHistGetCtrlEntry ((UINT4) i4UsrHistoryControlIndex);

    if (pUsrHistoryCtrlEntry != NULL)
    {
        if (pUsrHistoryCtrlEntry->u4UsrHistoryControlStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of UsrHistory Control table is Active"
                    " for index : %d \r\n", i4UsrHistoryControlIndex);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry is not exist \n");
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhTestv2UsrHistoryControlObjects \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2UsrHistoryControlBucketsRequested
 Input       :  The Indices
                UsrHistoryControlIndex

                The Object 
                testValUsrHistoryControlBucketsRequested
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2UsrHistoryControlBucketsRequested (UINT4 *pu4ErrorCode,
                                            INT4 i4UsrHistoryControlIndex,
                                            INT4
                                            i4TestValUsrHistoryControlBucketsRequested)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhTestv2UsrHistoryControlBucketsRequested \n");
    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4UsrHistoryControlIndex < RMON2_ONE) ||
        (i4UsrHistoryControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid UsrHistoryControlIndex \n");
        return SNMP_FAILURE;
    }

    if ((i4TestValUsrHistoryControlBucketsRequested < RMON2_ONE) ||
        (i4TestValUsrHistoryControlBucketsRequested >
         RMON2_MAX_USRHIST_BUCKETS_REQ))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Wrong value for UsrHistoryControlBucketsRequested "
                   "object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pUsrHistoryCtrlEntry =
        Rmon2UsrHistGetCtrlEntry ((UINT4) i4UsrHistoryControlIndex);

    if (pUsrHistoryCtrlEntry != NULL)
    {
        if (pUsrHistoryCtrlEntry->u4UsrHistoryControlStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of UsrHistory Control table is Active "
                    "for index : %d \r\n", i4UsrHistoryControlIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry is not exist \n");
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhTestv2UsrHistoryControlBucketsRequested\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2UsrHistoryControlInterval
 Input       :  The Indices
                UsrHistoryControlIndex

                The Object 
                testValUsrHistoryControlInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2UsrHistoryControlInterval (UINT4 *pu4ErrorCode,
                                    INT4 i4UsrHistoryControlIndex,
                                    INT4 i4TestValUsrHistoryControlInterval)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;
    UINT4               u4TestValUsrHistoryControlInterval =
        (UINT4) i4TestValUsrHistoryControlInterval;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhTestv2UsrHistoryControlInterval\n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4UsrHistoryControlIndex < RMON2_ONE) ||
        (i4UsrHistoryControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid UsrHistoryControlIndex \n");
        return SNMP_FAILURE;
    }

    if ((i4TestValUsrHistoryControlInterval < RMON2_ONE) ||
        (u4TestValUsrHistoryControlInterval > RMON2_MAX_USR_HISTORY_INTERVAL))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Wrong value for UsrHistoryControlInterval "
                   "object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pUsrHistoryCtrlEntry =
        Rmon2UsrHistGetCtrlEntry ((UINT4) i4UsrHistoryControlIndex);

    if (pUsrHistoryCtrlEntry != NULL)
    {
        if (pUsrHistoryCtrlEntry->u4UsrHistoryControlStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of UsrHistory Control table is Active "
                    "for index : %d \r\n", i4UsrHistoryControlIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry is not exist \n");
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhTestv2UsrHistoryControlInterval\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2UsrHistoryControlOwner
 Input       :  The Indices
                UsrHistoryControlIndex

                The Object 
                testValUsrHistoryControlOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2UsrHistoryControlOwner (UINT4 *pu4ErrorCode,
                                 INT4 i4UsrHistoryControlIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValUsrHistoryControlOwner)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhTestv2UsrHistoryControlOwner \n");
    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4UsrHistoryControlIndex < RMON2_ONE) ||
        (i4UsrHistoryControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid UsrHistoryControlIndex \n");
        return SNMP_FAILURE;
    }

    if ((pTestValUsrHistoryControlOwner->i4_Length < RMON2_ZERO) ||
        (pTestValUsrHistoryControlOwner->i4_Length > RMON2_OWNER_LEN))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Wrong length for UsrHistoryControlOwner "
                   "object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValUsrHistoryControlOwner->pu1_OctetList,
                              pTestValUsrHistoryControlOwner->i4_Length)
        == OSIX_FAILURE)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Invalid Characters for UsrHistoryControlOwner "
                   "string object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    pUsrHistoryCtrlEntry =
        Rmon2UsrHistGetCtrlEntry ((UINT4) i4UsrHistoryControlIndex);

    if (pUsrHistoryCtrlEntry != NULL)
    {
        if (pUsrHistoryCtrlEntry->u4UsrHistoryControlStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of UsrHistory Control table is Active "
                    "for index : %d \r\n", i4UsrHistoryControlIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry is not exist \n");
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhTestv2UsrHistoryControlOwner \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2UsrHistoryControlStatus
 Input       :  The Indices
                UsrHistoryControlIndex

                The Object 
                testValUsrHistoryControlStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2UsrHistoryControlStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4UsrHistoryControlIndex,
                                  INT4 i4TestValUsrHistoryControlStatus)
{
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2UsrHistoryControlStatus \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4UsrHistoryControlIndex < RMON2_ONE) ||
        (i4UsrHistoryControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid UsrHistoryControlIndex \n");
        return SNMP_FAILURE;
    }

    if ((i4TestValUsrHistoryControlStatus < ACTIVE) ||
        (i4TestValUsrHistoryControlStatus > DESTROY))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Wrong value for UsrHistoryControlStatus "
                   "object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pUsrHistoryCtrlEntry =
        Rmon2UsrHistGetCtrlEntry ((UINT4) i4UsrHistoryControlIndex);

    if (pUsrHistoryCtrlEntry == NULL)
    {
        if (i4TestValUsrHistoryControlStatus == CREATE_AND_WAIT)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "SNMP Failure: Entry is not exist \n");
        return SNMP_FAILURE;
    }
    else
    {
        if ((i4TestValUsrHistoryControlStatus == ACTIVE) ||
            (i4TestValUsrHistoryControlStatus == NOT_IN_SERVICE) ||
            (i4TestValUsrHistoryControlStatus == DESTROY))
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "The given entry is already exist \n");
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhTestv2UsrHistoryControlStatus \n");
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2UsrHistoryControlTable
 Input       :  The Indices
                UsrHistoryControlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2UsrHistoryControlTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : UsrHistoryObjectTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceUsrHistoryObjectTable
 Input       :  The Indices
                UsrHistoryControlIndex
                UsrHistoryObjectIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceUsrHistoryObjectTable (INT4 i4UsrHistoryControlIndex,
                                               INT4 i4UsrHistoryObjectIndex)
{
    tUsrHistoryObject  *pUsrHistoryObjectEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "nmhValidateIndexInstanceUsrHistoryObjectTable \n");
    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4UsrHistoryControlIndex < RMON2_ONE) ||
        (i4UsrHistoryControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid UsrHistoryControlIndex \n");
        return SNMP_FAILURE;
    }

    if ((i4UsrHistoryObjectIndex < RMON2_ONE) ||
        (i4UsrHistoryObjectIndex > RMON2_MAX_USRHIST_OBJECT_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid UsrHistoryObjectIndex \n");
        return SNMP_FAILURE;
    }

    pUsrHistoryObjectEntry =
        Rmon2UsrHistGetObjectEntry ((UINT4) i4UsrHistoryControlIndex,
                                    (UINT4) i4UsrHistoryObjectIndex);

    if (pUsrHistoryObjectEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of User History Object table"
                    " for index : %d is failed \r\n", i4UsrHistoryObjectIndex);
        return SNMP_FAILURE;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "nmhValidateIndexInstanceUsrHistoryObjectTable \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexUsrHistoryObjectTable
 Input       :  The Indices
                UsrHistoryControlIndex
                UsrHistoryObjectIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexUsrHistoryObjectTable (INT4 *pi4UsrHistoryControlIndex,
                                       INT4 *pi4UsrHistoryObjectIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetFirstIndexUsrHistoryObjectTable \n");

    if (nmhGetNextIndexUsrHistoryObjectTable (RMON2_ZERO,
                                              pi4UsrHistoryControlIndex,
                                              RMON2_ZERO,
                                              pi4UsrHistoryObjectIndex) !=
        SNMP_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get first entry of UsrHistory Object table is failed \r\n");
        return SNMP_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetFirstIndexUsrHistoryObjectTable \n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexUsrHistoryObjectTable
 Input       :  The Indices
                UsrHistoryControlIndex
                nextUsrHistoryControlIndex
                UsrHistoryObjectIndex
                nextUsrHistoryObjectIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexUsrHistoryObjectTable (INT4 i4UsrHistoryControlIndex,
                                      INT4 *pi4NextUsrHistoryControlIndex,
                                      INT4 i4UsrHistoryObjectIndex,
                                      INT4 *pi4NextUsrHistoryObjectIndex)
{
    tUsrHistoryObject  *pUsrHistoryObjectEntry = NULL;
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNextIndexUsrHistoryObjectTable \n");

    if (i4UsrHistoryControlIndex == 0)
    {
        pUsrHistoryCtrlEntry = Rmon2UsrHistGetNextCtrlIndex
            ((UINT4) i4UsrHistoryControlIndex);
        if (pUsrHistoryCtrlEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        i4UsrHistoryControlIndex =
            (INT4) pUsrHistoryCtrlEntry->u4UsrHistoryControlIndex;
    }
    pUsrHistoryObjectEntry =
        Rmon2UsrHistGetNextObjectIndex ((UINT4) i4UsrHistoryControlIndex,
                                        (UINT4) i4UsrHistoryObjectIndex);

    if (pUsrHistoryObjectEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of User History Object table"
                    " for index : %d is failed \r\n", i4UsrHistoryObjectIndex);
        pUsrHistoryCtrlEntry = Rmon2UsrHistGetNextCtrlIndex
            ((UINT4) i4UsrHistoryControlIndex);
        if (pUsrHistoryCtrlEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        i4UsrHistoryControlIndex =
            (INT4) pUsrHistoryCtrlEntry->u4UsrHistoryControlIndex;
        pUsrHistoryObjectEntry =
            Rmon2UsrHistGetNextObjectIndex ((UINT4) i4UsrHistoryControlIndex,
                                            RMON2_ZERO);
        if (pUsrHistoryObjectEntry == NULL)
        {
            return SNMP_FAILURE;
        }
    }
    *pi4NextUsrHistoryControlIndex = i4UsrHistoryControlIndex;
    *pi4NextUsrHistoryObjectIndex = (INT4)
        pUsrHistoryObjectEntry->u4UsrHistoryObjectIndex;

    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetNextIndexUsrHistoryObjectTable \n");
    return SNMP_SUCCESS;

}

/* LowoLevel GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetUsrHistoryObjectVariable
 Input       :  The Indices
                UsrHistoryControlIndex
                UsrHistoryObjectIndex

                The Object 
                retValUsrHistoryObjectVariable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsrHistoryObjectVariable (INT4 i4UsrHistoryControlIndex,
                                INT4 i4UsrHistoryObjectIndex,
                                tSNMP_OID_TYPE *
                                pRetValUsrHistoryObjectVariable)
{
    tUsrHistoryObject  *pUsrHistoryObjectEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "nmhGetUsrHistoryObjectVariable \n");

    pUsrHistoryObjectEntry =
        Rmon2UsrHistGetObjectEntry ((UINT4) i4UsrHistoryControlIndex,
                                    (UINT4) i4UsrHistoryObjectIndex);
    if (pUsrHistoryObjectEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of User History Object table"
                    " for index : %d is failed \r\n", i4UsrHistoryObjectIndex);
        return SNMP_FAILURE;
    }

    pRetValUsrHistoryObjectVariable->u4_Length =
        pUsrHistoryObjectEntry->UsrHistoryObjectVariable.u4_Length;
    MEMCPY (pRetValUsrHistoryObjectVariable->pu4_OidList,
            pUsrHistoryObjectEntry->UsrHistoryObjectVariable.pu4_OidList,
            (pUsrHistoryObjectEntry->UsrHistoryObjectVariable.u4_Length *
             sizeof (UINT4)));

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "nmhGetUsrHistoryObjectVariable \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUsrHistoryObjectSampleType
 Input       :  The Indices
                UsrHistoryControlIndex
                UsrHistoryObjectIndex

                The Object 
                retValUsrHistoryObjectSampleType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsrHistoryObjectSampleType (INT4 i4UsrHistoryControlIndex,
                                  INT4 i4UsrHistoryObjectIndex,
                                  INT4 *pi4RetValUsrHistoryObjectSampleType)
{
    tUsrHistoryObject  *pUsrHistoryObjectEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "nmhGetUsrHistoryObjectSampleType \n");

    pUsrHistoryObjectEntry =
        Rmon2UsrHistGetObjectEntry ((UINT4) i4UsrHistoryControlIndex,
                                    (UINT4) i4UsrHistoryObjectIndex);

    if (pUsrHistoryObjectEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of User History Object table"
                    " for index : %d is failed \r\n", i4UsrHistoryObjectIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValUsrHistoryObjectSampleType =
        (INT4) pUsrHistoryObjectEntry->u4UsrHistoryObjectSampleType;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "nmhGetUsrHistoryObjectSampleType \n");
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetUsrHistoryObjectVariable
 Input       :  The Indices
                UsrHistoryControlIndex
                UsrHistoryObjectIndex

                The Object 
                setValUsrHistoryObjectVariable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetUsrHistoryObjectVariable (INT4 i4UsrHistoryControlIndex,
                                INT4 i4UsrHistoryObjectIndex,
                                tSNMP_OID_TYPE *
                                pSetValUsrHistoryObjectVariable)
{
    tUsrHistoryObject  *pUsrHistoryObjectEntry = NULL;
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "nmhSetUsrHistoryObjectVariable \n");

    pUsrHistoryCtrlEntry =
        Rmon2UsrHistGetCtrlEntry ((UINT4) i4UsrHistoryControlIndex);

    if (pUsrHistoryCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of User History Control table"
                    " for index : %d is failed \r\n", i4UsrHistoryControlIndex);
        return SNMP_FAILURE;
    }

    if (pUsrHistoryCtrlEntry->u4UsrHistoryControlStatus == ACTIVE)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of UsrHistory Control table is Active "
                    "for index : %d \r\n", i4UsrHistoryControlIndex);
        return SNMP_FAILURE;

    }

    pUsrHistoryObjectEntry =
        Rmon2UsrHistGetObjectEntry ((UINT4) i4UsrHistoryControlIndex,
                                    (UINT4) i4UsrHistoryObjectIndex);
    if (pUsrHistoryObjectEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of User History Object table"
                    " for index : %d is failed \r\n", i4UsrHistoryObjectIndex);
        return SNMP_FAILURE;
    }

    pUsrHistoryObjectEntry->UsrHistoryObjectVariable.u4_Length =
        pSetValUsrHistoryObjectVariable->u4_Length;
    MEMCPY (pUsrHistoryObjectEntry->UsrHistoryObjectVariable.pu4_OidList,
            pSetValUsrHistoryObjectVariable->pu4_OidList,
            (pSetValUsrHistoryObjectVariable->u4_Length * sizeof (UINT4)));

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "nmhSetUsrHistoryObjectVariable \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetUsrHistoryObjectSampleType
 Input       :  The Indices
                UsrHistoryControlIndex
                UsrHistoryObjectIndex

                The Object 
                setValUsrHistoryObjectSampleType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetUsrHistoryObjectSampleType (INT4 i4UsrHistoryControlIndex,
                                  INT4 i4UsrHistoryObjectIndex,
                                  INT4 i4SetValUsrHistoryObjectSampleType)
{
    tUsrHistoryObject  *pUsrHistoryObjectEntry = NULL;
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY "
               "nmhGetUsrHistoryObjectSampleType \n");
    pUsrHistoryCtrlEntry =
        Rmon2UsrHistGetCtrlEntry ((UINT4) i4UsrHistoryControlIndex);

    if (pUsrHistoryCtrlEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of User History Control table"
                    "for index : %d is failed \r\n", i4UsrHistoryControlIndex);
        return SNMP_FAILURE;
    }

    if (pUsrHistoryCtrlEntry->u4UsrHistoryControlStatus == ACTIVE)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Row Status of UsrHistory Control table is Active "
                    "for index : %d \r\n", i4UsrHistoryControlIndex);
        return SNMP_FAILURE;

    }

    pUsrHistoryObjectEntry =
        Rmon2UsrHistGetObjectEntry ((UINT4) i4UsrHistoryControlIndex,
                                    (UINT4) i4UsrHistoryObjectIndex);

    if (pUsrHistoryObjectEntry == NULL)
    {
        RMON2_TRC1 (RMON2_DEBUG_TRC,
                    "Get entry of User History Object table"
                    " for index : %d is failed \r\n", i4UsrHistoryObjectIndex);
        return SNMP_FAILURE;
    }
    pUsrHistoryObjectEntry->u4UsrHistoryObjectSampleType =
        (UINT4) i4SetValUsrHistoryObjectSampleType;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT "
               "nmhGetUsrHistoryObjectSampleType \n");
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2UsrHistoryObjectVariable
 Input       :  The Indices
                UsrHistoryControlIndex
                UsrHistoryObjectIndex

                The Object 
                testValUsrHistoryObjectVariable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2UsrHistoryObjectVariable (UINT4 *pu4ErrorCode,
                                   INT4 i4UsrHistoryControlIndex,
                                   INT4 i4UsrHistoryObjectIndex,
                                   tSNMP_OID_TYPE *
                                   pTestValUsrHistoryObjectVariable)
{
    tUsrHistoryObject  *pUsrHistoryObjectEntry = NULL;
    tSnmpIndex         *pUsrHistIndexPool = gRmon2Globals.Rmon2IndexPool;
    UINT4               u4Error = RMON2_ZERO;
    tSNMP_MULTI_DATA_TYPE MultiData;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2UsrHistoryObjectVariable \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4UsrHistoryControlIndex < RMON2_ONE) ||
        (i4UsrHistoryControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid UsrHistoryControlIndex \n");
        return SNMP_FAILURE;
    }

    if ((i4UsrHistoryObjectIndex < RMON2_ONE) ||
        (i4UsrHistoryObjectIndex > RMON2_MAX_USRHIST_OBJECT_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid UsrHistoryObjectIndex \n");
        return SNMP_FAILURE;
    }

    MEMSET (&MultiData, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    /*Allocate memory for the MultiData variables */
    MultiData.pOctetStrValue = (tSNMP_OCTET_STRING_TYPE *)
        rmon2_allocmem_octetstring (RMON2_MAX_OCTET_STRING_LEN);

    if (MultiData.pOctetStrValue == NULL)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_MEM_FAIL,
                   " RMON2 Memory Allocation Failure - Multi Data\n");
        return RMON2_FAILURE;
    }

    MultiData.pOidValue = (tSNMP_OID_TYPE *)
        rmon2_alloc_oid (RMON2_MAX_OID_LEN);

    if (MultiData.pOidValue == NULL)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_MEM_FAIL,
                   " RMON2 Memory Allocation Failure - Multi Data\n");
        rmon2_free_octetstring (MultiData.pOctetStrValue);
        return RMON2_FAILURE;
    }

    Rmon2MainUnLock ();
    if (SNMPGet
        (*pTestValUsrHistoryObjectVariable, &MultiData, &u4Error,
         pUsrHistIndexPool, SNMP_DEFAULT_CONTEXT) == SNMP_SUCCESS)
    {
        /* Get the Object variable value from the multi-data type */
        switch (MultiData.i2_DataType)
        {
            case SNMP_DATA_TYPE_INTEGER32:
            case SNMP_DATA_TYPE_TIME_TICKS:
            case SNMP_DATA_TYPE_GAUGE:
            case SNMP_DATA_TYPE_COUNTER:
                rmon2_free_oid (MultiData.pOidValue);
                rmon2_free_octetstring (MultiData.pOctetStrValue);
                Rmon2MainLock ();
                break;
            default:
                rmon2_free_oid (MultiData.pOidValue);
                rmon2_free_octetstring (MultiData.pOctetStrValue);
                *pu4ErrorCode = SNMP_ERR_WRONG_TYPE;
                Rmon2MainLock ();
                return SNMP_FAILURE;
        }
    }

    pUsrHistoryObjectEntry =
        Rmon2UsrHistGetObjectEntry ((UINT4) i4UsrHistoryControlIndex,
                                    (UINT4) i4UsrHistoryObjectIndex);
    if (pUsrHistoryObjectEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "SNMP Failure: Entry is not exist \n");
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhTestv2UsrHistoryObjectVariable \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2UsrHistoryObjectSampleType
 Input       :  The Indices
                UsrHistoryControlIndex
                UsrHistoryObjectIndex

                The Object 
                testValUsrHistoryObjectSampleType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2UsrHistoryObjectSampleType (UINT4 *pu4ErrorCode,
                                     INT4 i4UsrHistoryControlIndex,
                                     INT4 i4UsrHistoryObjectIndex,
                                     INT4 i4TestValUsrHistoryObjectSampleType)
{
    tUsrHistoryObject  *pUsrHistoryObjectEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2UsrHistoryObjectSampleType \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4UsrHistoryControlIndex < RMON2_ONE) ||
        (i4UsrHistoryControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid UsrHistoryControlIndex \n");
        return SNMP_FAILURE;
    }

    if ((i4UsrHistoryObjectIndex < RMON2_ONE) ||
        (i4UsrHistoryObjectIndex > RMON2_MAX_USRHIST_OBJECT_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid UsrHistoryObjectIndex \n");
        return SNMP_FAILURE;
    }

    if ((i4TestValUsrHistoryObjectSampleType <
         RMON2_SAMPLE_TYPE_ABSOLUTE_VALUE) ||
        (i4TestValUsrHistoryObjectSampleType > RMON2_SAMPLE_TYPE_DELTA_VALUE))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Wrong value for UsrHistoryObjectSampleType "
                   "object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pUsrHistoryObjectEntry =
        Rmon2UsrHistGetObjectEntry ((UINT4) i4UsrHistoryControlIndex,
                                    (UINT4) i4UsrHistoryObjectIndex);

    if (pUsrHistoryObjectEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "SNMP Failure: Entry is not exist \n");
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhTestv2UsrHistoryObjectSampleType \n");

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2UsrHistoryObjectTable
 Input       :  The Indices
                UsrHistoryControlIndex
                UsrHistoryObjectIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2UsrHistoryObjectTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : UsrHistoryTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceUsrHistoryTable
 Input       :  The Indices
                UsrHistoryControlIndex
                UsrHistorySampleIndex
                UsrHistoryObjectIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceUsrHistoryTable (INT4 i4UsrHistoryControlIndex,
                                         INT4 i4UsrHistorySampleIndex,
                                         INT4 i4UsrHistoryObjectIndex)
{
    tUsrHistory        *pUsrHistoryEntry = NULL;
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhValidateIndexInstanceUsrHistoryTable \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4UsrHistoryControlIndex < RMON2_ONE) ||
        (i4UsrHistoryControlIndex > RMON2_MAX_CTRL_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid UsrHistoryControlIndex \n");
        return SNMP_FAILURE;
    }

    if ((i4UsrHistoryObjectIndex < RMON2_ONE) ||
        (i4UsrHistoryObjectIndex > RMON2_MAX_USRHIST_OBJECT_INDEX))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid UsrHistoryObjectIndex \n");
        return SNMP_FAILURE;
    }

    /* Testing the input for negative value alone is sufficient, because
     * RMON2 allows this configuration till the maximum value of interger 32 */
    if (i4UsrHistorySampleIndex < RMON2_ONE)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Invalid UsrHistorySampleIndex \n");
        return SNMP_FAILURE;
    }

    pUsrHistoryEntry =
        Rmon2UsrHistGetDataEntry ((UINT4) i4UsrHistoryControlIndex,
                                  (UINT4) i4UsrHistorySampleIndex,
                                  (UINT4) i4UsrHistoryObjectIndex);

    if (pUsrHistoryEntry == NULL)
    {
        RMON2_TRC3 (RMON2_DEBUG_TRC,
                    "Get entry of User History table"
                    " for index  : %d,%d,%d is failed \r\n",
                    i4UsrHistoryControlIndex, i4UsrHistorySampleIndex,
                    i4UsrHistoryObjectIndex);
        return SNMP_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhValidateIndexInstanceUsrHistoryTable \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexUsrHistoryTable
 Input       :  The Indices
                UsrHistoryControlIndex
                UsrHistorySampleIndex
                UsrHistoryObjectIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexUsrHistoryTable (INT4 *pi4UsrHistoryControlIndex,
                                 INT4 *pi4UsrHistorySampleIndex,
                                 INT4 *pi4UsrHistoryObjectIndex)
{
    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetFirstIndexUsrHistoryTable \n");

    if (nmhGetNextIndexUsrHistoryTable (RMON2_ZERO,
                                        pi4UsrHistoryControlIndex, RMON2_ZERO,
                                        pi4UsrHistorySampleIndex, RMON2_ZERO,
                                        pi4UsrHistoryObjectIndex) !=
        SNMP_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get first entry of UsrHistory table is failed \r\n");
        return SNMP_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetFirstIndexUsrHistoryTable \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexUsrHistoryTable
 Input       :  The Indices
                UsrHistoryControlIndex
                nextUsrHistoryControlIndex
                UsrHistorySampleIndex
                nextUsrHistorySampleIndex
                UsrHistoryObjectIndex
                nextUsrHistoryObjectIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexUsrHistoryTable (INT4 i4UsrHistoryControlIndex,
                                INT4 *pi4NextUsrHistoryControlIndex,
                                INT4 i4UsrHistorySampleIndex,
                                INT4 *pi4NextUsrHistorySampleIndex,
                                INT4 i4UsrHistoryObjectIndex,
                                INT4 *pi4NextUsrHistoryObjectIndex)
{
    tUsrHistory        *pUsrHistoryEntry = NULL;
    tUsrHistoryControl *pUsrHistoryCtrlEntry = NULL;
    tUsrHistoryObject  *pUsrHistoryObjectEntry = NULL;
    INT4                i4TempUsrHistoryControlIndex;
    INT4                i4TempUsrHistoryObjectIndex;
    INT4                i4TempUsrHistorySampleIndex;
    UINT4               u4BucketsGranted;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY  nmhGetNextIndexUsrHistoryTable \n");

    if (i4UsrHistoryControlIndex == 0)
    {
        pUsrHistoryCtrlEntry = Rmon2UsrHistGetNextCtrlIndex
            ((UINT4) i4UsrHistoryControlIndex);

        if (pUsrHistoryCtrlEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        i4UsrHistoryControlIndex =
            (INT4) pUsrHistoryCtrlEntry->u4UsrHistoryControlIndex;
    }
    else
    {
        pUsrHistoryCtrlEntry = Rmon2UsrHistGetCtrlEntry
            ((UINT4) i4UsrHistoryControlIndex);

        if (pUsrHistoryCtrlEntry == NULL)
        {
            return SNMP_FAILURE;
        }
    }

    u4BucketsGranted = pUsrHistoryCtrlEntry->u4UsrHistoryControlBucketsGranted;

    i4TempUsrHistoryControlIndex = i4UsrHistoryControlIndex;

    i4TempUsrHistoryObjectIndex = i4UsrHistoryObjectIndex;

    i4TempUsrHistorySampleIndex = i4UsrHistorySampleIndex;

    while (1)
    {
        i4TempUsrHistoryObjectIndex++;
        pUsrHistoryObjectEntry = Rmon2UsrHistGetObjectEntry ((UINT4)
                                                             i4TempUsrHistoryControlIndex,
                                                             (UINT4)
                                                             i4TempUsrHistoryObjectIndex);
        if (pUsrHistoryObjectEntry != NULL)
        {
            pUsrHistoryEntry =
                Rmon2UsrHistGetDataEntry ((UINT4) i4TempUsrHistoryControlIndex,
                                          (UINT4) i4TempUsrHistorySampleIndex,
                                          (UINT4) i4TempUsrHistoryObjectIndex);
            if (pUsrHistoryEntry != NULL)
            {
                *pi4NextUsrHistoryControlIndex =
                    (INT4) i4TempUsrHistoryControlIndex;
                *pi4NextUsrHistoryObjectIndex =
                    (INT4) i4TempUsrHistoryObjectIndex;
                *pi4NextUsrHistorySampleIndex =
                    (INT4) i4TempUsrHistorySampleIndex;
                break;
            }

        }

        i4TempUsrHistorySampleIndex++;

        if (i4TempUsrHistorySampleIndex > (INT4) u4BucketsGranted)
        {

            pUsrHistoryCtrlEntry = Rmon2UsrHistGetNextCtrlIndex
                ((UINT4) i4TempUsrHistoryControlIndex);

            if (pUsrHistoryCtrlEntry == NULL)
            {
                return SNMP_FAILURE;
            }

            i4TempUsrHistoryControlIndex = (INT4) pUsrHistoryCtrlEntry->
                u4UsrHistoryControlIndex;
            i4TempUsrHistoryObjectIndex = 0;
            i4TempUsrHistorySampleIndex = 1;
            continue;
        }
        i4TempUsrHistoryObjectIndex = 0;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNextIndexUsrHistoryTable \n");

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetUsrHistoryIntervalStart
 Input       :  The Indices
                UsrHistoryControlIndex
                UsrHistorySampleIndex
                UsrHistoryObjectIndex

                The Object 
                retValUsrHistoryIntervalStart
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsrHistoryIntervalStart (INT4 i4UsrHistoryControlIndex,
                               INT4 i4UsrHistorySampleIndex,
                               INT4 i4UsrHistoryObjectIndex,
                               UINT4 *pu4RetValUsrHistoryIntervalStart)
{
    tUsrHistory        *pUsrHistoryEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY   nmhGetUsrHistoryIntervalStart \n");

    pUsrHistoryEntry =
        Rmon2UsrHistGetDataEntry ((UINT4) i4UsrHistoryControlIndex,
                                  (UINT4) i4UsrHistorySampleIndex,
                                  (UINT4) i4UsrHistoryObjectIndex);

    if (pUsrHistoryEntry == NULL)
    {
        RMON2_TRC3 (RMON2_DEBUG_TRC,
                    "Get entry of User History table"
                    " for index  : %d,%d,%d is failed \r\n",
                    i4UsrHistoryControlIndex, i4UsrHistorySampleIndex,
                    i4UsrHistoryObjectIndex);
        return SNMP_FAILURE;
    }

    *pu4RetValUsrHistoryIntervalStart =
        pUsrHistoryEntry->u4UsrHistoryIntervalStart;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT  nmhGetUsrHistoryIntervalStart \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetUsrHistoryIntervalEnd
 Input       :  The Indices
                UsrHistoryControlIndex
                UsrHistorySampleIndex
                UsrHistoryObjectIndex

                The Object 
                retValUsrHistoryIntervalEnd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsrHistoryIntervalEnd (INT4 i4UsrHistoryControlIndex,
                             INT4 i4UsrHistorySampleIndex,
                             INT4 i4UsrHistoryObjectIndex,
                             UINT4 *pu4RetValUsrHistoryIntervalEnd)
{
    tUsrHistory        *pUsrHistoryEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY   nmhGetUsrHistoryIntervalEnd \n");

    pUsrHistoryEntry =
        Rmon2UsrHistGetDataEntry ((UINT4) i4UsrHistoryControlIndex,
                                  (UINT4) i4UsrHistorySampleIndex,
                                  (UINT4) i4UsrHistoryObjectIndex);

    if (pUsrHistoryEntry == NULL)
    {
        RMON2_TRC3 (RMON2_DEBUG_TRC,
                    "Get entry of User History table"
                    " for index  : %d,%d,%d is failed \r\n",
                    i4UsrHistoryControlIndex, i4UsrHistorySampleIndex,
                    i4UsrHistoryObjectIndex);
        return SNMP_FAILURE;
    }

    *pu4RetValUsrHistoryIntervalEnd = pUsrHistoryEntry->u4UsrHistoryIntervalEnd;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT  nmhGetUsrHistoryIntervalEnd \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetUsrHistoryAbsValue
 Input       :  The Indices
                UsrHistoryControlIndex
                UsrHistorySampleIndex
                UsrHistoryObjectIndex

                The Object 
                retValUsrHistoryAbsValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsrHistoryAbsValue (INT4 i4UsrHistoryControlIndex,
                          INT4 i4UsrHistorySampleIndex,
                          INT4 i4UsrHistoryObjectIndex,
                          UINT4 *pu4RetValUsrHistoryAbsValue)
{
    tUsrHistory        *pUsrHistoryEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY   nmhGetUsrHistoryAbsValue \n");

    pUsrHistoryEntry =
        Rmon2UsrHistGetDataEntry ((UINT4) i4UsrHistoryControlIndex,
                                  (UINT4) i4UsrHistorySampleIndex,
                                  (UINT4) i4UsrHistoryObjectIndex);

    if (pUsrHistoryEntry == NULL)
    {
        RMON2_TRC3 (RMON2_DEBUG_TRC,
                    "Get entry of User History table"
                    " for index  : %d,%d,%d is failed \r\n",
                    i4UsrHistoryControlIndex, i4UsrHistorySampleIndex,
                    i4UsrHistoryObjectIndex);
        return SNMP_FAILURE;
    }

    *pu4RetValUsrHistoryAbsValue =
        pUsrHistoryEntry->u4UsrHistoryAbsValueCurSample;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT  nmhGetUsrHistoryAbsValue \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetUsrHistoryValStatus
 Input       :  The Indices
                UsrHistoryControlIndex
                UsrHistorySampleIndex
                UsrHistoryObjectIndex

                The Object 
                retValUsrHistoryValStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsrHistoryValStatus (INT4 i4UsrHistoryControlIndex,
                           INT4 i4UsrHistorySampleIndex,
                           INT4 i4UsrHistoryObjectIndex,
                           INT4 *pi4RetValUsrHistoryValStatus)
{
    tUsrHistory        *pUsrHistoryEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY   nmhGetUsrHistoryValStatus \n");

    pUsrHistoryEntry =
        Rmon2UsrHistGetDataEntry ((UINT4) i4UsrHistoryControlIndex,
                                  (UINT4) i4UsrHistorySampleIndex,
                                  (UINT4) i4UsrHistoryObjectIndex);

    if (pUsrHistoryEntry == NULL)
    {
        RMON2_TRC3 (RMON2_DEBUG_TRC,
                    "Get entry of User History table"
                    " for index  : %d,%d,%d is failed \r\n",
                    i4UsrHistoryControlIndex, i4UsrHistorySampleIndex,
                    i4UsrHistoryObjectIndex);
        return SNMP_FAILURE;
    }

    *pi4RetValUsrHistoryValStatus =
        (INT4) pUsrHistoryEntry->u4UsrHistoryValStatus;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT  nmhGetUsrHistoryValStatus \n");
    return SNMP_SUCCESS;
}
