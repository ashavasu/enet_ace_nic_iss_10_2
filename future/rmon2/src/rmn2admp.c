/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * $Id: rmn2admp.c,v 1.11 2013/07/12 12:33:23 siva Exp $ 
 *
 * Description: This file contains the functional routine for
 *              RMON2 Address Map module
 *
 *******************************************************************/
#include "rmn2inc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AddrMapGiveLRUEntry                                   */
/*                                                                           */
/* Description  : Gives the LRU Entry                                        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pAddressMap / NULL                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC tAddressMap *
Rmon2AddrMapGiveLRUEntry (VOID)
{
    tAddressMap        *pAddressMap = NULL, *pLRUEntry = NULL;
    UINT4               u4ProtocolDirLocalIndex = 0;
    tIpAddr             AddressMapNetworkAddress;
    UINT4               u4AddressMapSource = 0;

    MEMSET (&AddressMapNetworkAddress, 0, sizeof (tIpAddr));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AddrMapGiveLRUEntry\r\n");

    /* Get First Entry in Address Map Table */
    pAddressMap = Rmon2AddrMapGetNextIndex (u4ProtocolDirLocalIndex,
                                            &AddressMapNetworkAddress,
                                            u4AddressMapSource);
    pLRUEntry = pAddressMap;

    while (pAddressMap != NULL)
    {
        u4ProtocolDirLocalIndex = pAddressMap->u4ProtocolDirLocalIndex;
        MEMCPY (&AddressMapNetworkAddress,
                &(pAddressMap->AddressMapNetworkAddress), sizeof (tIpAddr));
        u4AddressMapSource = pAddressMap->u4AddressMapSource;

        if (pLRUEntry->u4AddressMapTimeMark > pAddressMap->u4AddressMapTimeMark)
        {
            pLRUEntry = pAddressMap;
        }
        pAddressMap = Rmon2AddrMapGetNextIndex (u4ProtocolDirLocalIndex,
                                                &AddressMapNetworkAddress,
                                                u4AddressMapSource);
    }                            /* End of While */

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AddrMapGiveLRUEntry\r\n");
    return pLRUEntry;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AddrMapProcessPktInfo                                 */
/*                                                                           */
/* Description  : Process Incoming Packet from Control Plane and Update      */
/*                AddrMap Table                                              */
/*                                                                           */
/* Input        : pPktInfo                                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rmon2AddrMapProcessPktInfo (tRmon2PktInfo * pPktInfo)
{
    tAddressMapControl *pAddressMapCtrl = NULL;
    tProtocolDir       *pNlProtDirEntry = NULL;
    tAddressMap        *pCurAddrMap = NULL, *pPrevAddrMap = NULL;
    UINT4               u4AddressMapControlIndex = 0;
    INT4                i4RetVal = OSIX_FAILURE;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AddrMapProcessPktInfo\r\n");

    if (pPktInfo->u4ProtoNLIndex == 0)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Rmon2AddrMapProcessPktInfo Invalid NL Index\n");
        return i4RetVal;
    }

    pNlProtDirEntry = Rmon2PDirGetProtocolNode (pPktInfo->u4ProtoNLIndex);
    if (pNlProtDirEntry == NULL)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Get Protocol Entry failed for NLProtocolLocIndex\r\n");
        return i4RetVal;
    }

    if (pNlProtDirEntry->u4ProtocolDirAddressMapConfig == SUPPORTED_OFF)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Rmon2AddrMapProcessPktInfo AddrMap is in OFF state\n");
        return i4RetVal;
    }

    pAddressMapCtrl = Rmon2AddrMapGetNextCtrlIndex (u4AddressMapControlIndex);

    while (pAddressMapCtrl != NULL)
    {
        if ((pAddressMapCtrl->u4AddressMapControlStatus == ACTIVE) &&
            ((pAddressMapCtrl->u4AddressMapControlDataSource ==
              pPktInfo->PktHdr.u4IfIndex) ||
             (pAddressMapCtrl->u4AddressMapControlDataSource ==
              pPktInfo->PktHdr.u4VlanIfIndex)))
        {
            if (pAddressMapCtrl->u4AddressMapControlDataSource ==
                pPktInfo->PktHdr.u4VlanIfIndex)
            {
                pPktInfo->u1IsL3Vlan = OSIX_TRUE;
            }
            /* Checking for an AddressMap Entry already exist or not */
            pCurAddrMap =
                Rmon2AddrMapGetDataEntry (pPktInfo->u4ProtoNLIndex,
                                          &(pPktInfo->PktHdr.SrcIp),
                                          pPktInfo->PktHdr.u4IfIndex);
            if (pCurAddrMap == NULL)
            {
                if (gRmon2Globals.u4AddressMapMaxDesiredSupported == 0)
                {
                    pAddressMapCtrl->u4AddressMapControlDroppedFrames++;
                    i4RetVal = OSIX_FAILURE;
                    return i4RetVal;
                }

                /* Check whether data entries reached max limit */

                if ((gRmon2Globals.u4AddressMapInserts -
                     gRmon2Globals.u4AddressMapDeletes) >=
                    gRmon2Globals.u4AddressMapMaxDesiredSupported)
                {
                    pCurAddrMap = Rmon2AddrMapGiveLRUEntry ();

                    if (pCurAddrMap != NULL)
                    {
                        RMON2_TRC (RMON2_DEBUG_TRC,
                                   "Deleting Address Map LRU Entry\r\n");

                        /* Remove Inter table references */
                        Rmon2AddrMapDelInterDep (pCurAddrMap);

                        gRmon2Globals.u4AddressMapDeletes++;

                        /* After Reference changes, free AddrMap entry */
                        Rmon2AddrMapDelDataEntry (pCurAddrMap);

                        pCurAddrMap = NULL;
                    }
                }

                /* Add new AddressMap Entry */
                pCurAddrMap = Rmon2AddrMapAddEntry (pPktInfo->u4ProtoNLIndex,
                                                    &(pPktInfo->PktHdr.SrcIp),
                                                    pPktInfo->PktHdr.u4IfIndex);

                if (pCurAddrMap == NULL)
                {
                    pAddressMapCtrl->u4AddressMapControlDroppedFrames++;
                }
                else
                {
                    MEMCPY (&pCurAddrMap->AddressMapPhysicalAddress,
                            &pPktInfo->PktHdr.SrcMACAddress, sizeof (tMacAddr));
                    if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV4)
                    {
                        pCurAddrMap->u4Rmon2AddressMapIpAddrLen =
                            RMON2_IPV4_MAX_LEN;
                    }
                    else if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV6)
                    {
                        pCurAddrMap->u4Rmon2AddressMapIpAddrLen =
                            RMON2_IPV6_MAX_LEN;
                    }
                    pCurAddrMap->u4AddressMapLastChange = OsixGetSysUpTime ();
                    gRmon2Globals.u4AddressMapInserts++;
                }
            }
            else
            {
                pCurAddrMap->u4PktRefCount++;
                /* Even if the Address Map entry already exist,   *
                 * MAC Address may change, so the same is updated */
                MEMCPY (&pCurAddrMap->AddressMapPhysicalAddress,
                        &pPktInfo->PktHdr.SrcMACAddress, sizeof (tMacAddr));
                pCurAddrMap->u4AddressMapLastChange = OsixGetSysUpTime ();
                pCurAddrMap->u4AddressMapTimeMark = OsixGetSysUpTime ();
            }

            if (pPrevAddrMap == NULL)
            {
                pPktInfo->pAddrMap = pCurAddrMap;
            }
            else
            {
                if (pCurAddrMap != NULL)
                {
                    pPrevAddrMap->pNextAddrMap = pCurAddrMap;
                    pCurAddrMap->pPrevAddrMap = pPrevAddrMap;
                }
            }
            pPrevAddrMap = pCurAddrMap;
            i4RetVal = OSIX_SUCCESS;
        }                        /* End of if check */
        pAddressMapCtrl =
            Rmon2AddrMapGetNextCtrlIndex (pAddressMapCtrl->
                                          u4AddressMapControlIndex);
    }                            /* End of while */

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AddrMapProcessPktInfo\r\n");
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AddrMapUpdateIFIndexChgs                              */
/*                                                                           */
/* Description  : If Operstatus is CFA_IF_DOWN, then set appropriate control */
/*                entries to NOT_READY state thus deleting the associated    */
/*                Data Table. If OperStatus is CFA_IF_UP, set NOT_READY      */
/*                entries to ACTIVE state                                    */
/*                                                                           */
/* Input        : u4IfIndex, u1OperStatus                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2AddrMapUpdateIFIndexChgs (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tAddressMapControl *pAddressMapCtrl = NULL;
    UINT4               u4AddressMapControlIndex = 0;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AddrMapUpdateIFIndexChgs\r\n");

    switch (u1OperStatus)
    {
        case CFA_IF_DOWN:
            /* Get 1st Hl Matrix Ctrl Index */
            pAddressMapCtrl =
                Rmon2AddrMapGetNextCtrlIndex (u4AddressMapControlIndex);

            while (pAddressMapCtrl != NULL)
            {
                u4AddressMapControlIndex =
                    pAddressMapCtrl->u4AddressMapControlIndex;

                if ((pAddressMapCtrl->u4AddressMapControlStatus == ACTIVE) &&
                    (pAddressMapCtrl->u4AddressMapControlDataSource ==
                     u4IfIndex))
                {
                    /* Remove Data Entries for this data source 
                     * from AddrMap Table */

                    Rmon2AddrMapDelDataEntries (pAddressMapCtrl->
                                                u4AddressMapControlDataSource);

                    /* Change RowStatus to NOT_READY */
                    pAddressMapCtrl->u4AddressMapControlStatus = NOT_READY;
                }                /* End of if check */

                pAddressMapCtrl =
                    Rmon2AddrMapGetNextCtrlIndex (u4AddressMapControlIndex);
            }                    /* End of while */
            break;
        case CFA_IF_UP:
            /* Get 1st Hl Matrix Ctrl Index */
            pAddressMapCtrl =
                Rmon2AddrMapGetNextCtrlIndex (u4AddressMapControlIndex);
            while (pAddressMapCtrl != NULL)
            {
                u4AddressMapControlIndex =
                    pAddressMapCtrl->u4AddressMapControlIndex;
                if ((pAddressMapCtrl->u4AddressMapControlStatus == NOT_READY)
                    && (pAddressMapCtrl->u4AddressMapControlDataSource ==
                        u4IfIndex))
                {
                    /* Set RowStatus to ACTIVE */
                    nmhSetAddressMapControlStatus ((INT4)
                                                   u4AddressMapControlIndex,
                                                   ACTIVE);
                }                /* End of if check */
                pAddressMapCtrl =
                    Rmon2AddrMapGetNextCtrlIndex (u4AddressMapControlIndex);
            }                    /* End of while */
            break;
        default:
            break;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AddrMapUpdateIFIndexChgs\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AddrMapUpdateProtoChgs                                */
/*                                                                           */
/* Description  : Removes Address Map Data Table Entries for change in       */
/*                Protocol Dir Entry Status or Address Map config support is */
/*                turned off for a Protocol Dir Entry.                       */
/*                                                                           */
/* Input        : u4ProtocolDirLocalIndex                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2AddrMapUpdateProtoChgs (UINT4 u4ProtocolIndex)
{
    tAddressMap        *pAddressMap = NULL;
    UINT4               u4ProtocolDirLocalIndex = 0, u4AddressMapSource = 0;
    tIpAddr             AddressMapNetworkAddress;

    MEMSET (&AddressMapNetworkAddress, 0, sizeof (tIpAddr));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AddrMapUpdateProtoChgs\r\n");

    /* Get First Entry in Address Map Table */

    pAddressMap = Rmon2AddrMapGetNextIndex (u4ProtocolDirLocalIndex,
                                            &AddressMapNetworkAddress,
                                            u4AddressMapSource);

    while (pAddressMap != NULL)
    {
        u4ProtocolDirLocalIndex = pAddressMap->u4ProtocolDirLocalIndex;
        MEMCPY (&AddressMapNetworkAddress,
                &(pAddressMap->AddressMapNetworkAddress), sizeof (tIpAddr));
        u4AddressMapSource = pAddressMap->u4AddressMapSource;

        if (pAddressMap->u4ProtocolDirLocalIndex == u4ProtocolIndex)
        {
            /* Remove Inter table dependencies */
            Rmon2AddrMapDelInterDep (pAddressMap);

            gRmon2Globals.u4AddressMapDeletes++;

            /* After Reference changes, free Address Map entry */
            Rmon2AddrMapDelDataEntry (pAddressMap);
        }
        pAddressMap = Rmon2AddrMapGetNextIndex (u4ProtocolDirLocalIndex,
                                                &AddressMapNetworkAddress,
                                                u4AddressMapSource);
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AddrMapUpdateProtoChgs\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AddrMapDelDataEntries                                 */
/*                                                                           */
/* Description  : Removes Address Map Data Table Entries for given           */
/*                Data Source                                                */
/*                                                                           */
/* Input        : u4AddressMapControlDataSource                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2AddrMapDelDataEntries (UINT4 u4AddressMapControlDataSource)
{
    tAddressMap        *pAddressMap = NULL;
    UINT4               u4ProtocolDirLocalIndex = 0;
    tIpAddr             AddressMapNetworkAddress;
    UINT4               u4AddressMapSource = 0;

    MEMSET (&AddressMapNetworkAddress, 0, sizeof (tIpAddr));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AddrMapDelDataEntries\r\n");

    /* Remove Data Entries for this Data Source from AddrMap Table */
    pAddressMap = Rmon2AddrMapGetNextIndex (u4ProtocolDirLocalIndex,
                                            &AddressMapNetworkAddress,
                                            u4AddressMapSource);

    while (pAddressMap != NULL)
    {
        u4ProtocolDirLocalIndex = pAddressMap->u4ProtocolDirLocalIndex;
        MEMCPY (&AddressMapNetworkAddress,
                &(pAddressMap->AddressMapNetworkAddress), sizeof (tIpAddr));
        u4AddressMapSource = pAddressMap->u4AddressMapSource;

        if (pAddressMap->u4AddressMapSource == u4AddressMapControlDataSource)
        {

            /* Update Inter table references */
            Rmon2AddrMapDelInterDep (pAddressMap);

            gRmon2Globals.u4AddressMapDeletes++;

            /* After Reference changes, free AddrMap entry */
            Rmon2AddrMapDelDataEntry (pAddressMap);

        }
        pAddressMap = Rmon2AddrMapGetNextIndex (u4ProtocolDirLocalIndex,
                                                &AddressMapNetworkAddress,
                                                u4AddressMapSource);
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AddrMapDelDataEntries\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AddrMapGetNextCtrlIndex                               */
/*                                                                           */
/* Description  : Get First / Next Addr Map Ctrl Index                       */
/*                                                                           */
/* Input        : u4AddressMapControlIndex                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tAddressMapControl / NULL Pointer                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC tAddressMapControl *
Rmon2AddrMapGetNextCtrlIndex (UINT4 u4AddressMapControlIndex)
{
    tAddressMapControl  AddrMapCtrl;
    MEMSET (&AddrMapCtrl, 0, sizeof (tAddressMapControl));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AddrMapGetNextCtrlIndex\r\n");

    AddrMapCtrl.u4AddressMapControlIndex = u4AddressMapControlIndex;
    return ((tAddressMapControl *) RBTreeGetNext (RMON2_ADDRMAPCTRL_TREE,
                                                  (tRBElem *) & AddrMapCtrl,
                                                  NULL));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AddrMapAddCtrlEntry                                   */
/*                                                                           */
/* Description  : Allocates memory and Adds Addr Map Control Entry into      */
/*                AddressMapControlRBTree                                    */
/*                                                                           */
/* Input        : pAddrMapCtrl                                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tAddressMapControl / NULL Pointer                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC tAddressMapControl *
Rmon2AddrMapAddCtrlEntry (UINT4 u4AddressMapControlIndex)
{
    tAddressMapControl *pAddrMapCtrl = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AddrMapAddCtrlEntry\r\n");

    if ((pAddrMapCtrl = (tAddressMapControl *)
         (MemAllocMemBlk (RMON2_ADDRMAPCTRL_POOL))) == NULL)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for AddressMapControlEntry\r\n");
        return NULL;
    }
    MEMSET (pAddrMapCtrl, 0, sizeof (tAddressMapControl));

    pAddrMapCtrl->u4AddressMapControlIndex = u4AddressMapControlIndex;
    pAddrMapCtrl->u4AddressMapControlDroppedFrames = RMON2_ZERO;

    if (RBTreeAdd (RMON2_ADDRMAPCTRL_TREE, (tRBElem *) pAddrMapCtrl)
        == RB_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Rmon2AddrMapAddCtrlEntry: AddressMapControlEntry is added\r\n");
        return pAddrMapCtrl;
    }

    RMON2_TRC (RMON2_CRITICAL_TRC,
               "Rmon2AddrMapAddCtrlEntry: Adding AddressMapControlEntry Failed\r\n");

    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (RMON2_ADDRMAPCTRL_POOL, (UINT1 *) pAddrMapCtrl);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AddrMapAddCtrlEntry\r\n");

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AddrMapDelCtrlEntry                                   */
/*                                                                           */
/* Description  : Releases memory and Removes Addr Map Control Entry from    */
/*                AddressMapControlRBTree                                    */
/*                                                                           */
/* Input        : pAddrMapCtrl                                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rmon2AddrMapDelCtrlEntry (tAddressMapControl * pAddrMapCtrl)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AddrMapDelCtrlEntry\r\n");

    /* Remove AddressMap Ctrl node from AddressMapControlRBTree */
    RBTreeRem (RMON2_ADDRMAPCTRL_TREE, (tRBElem *) pAddrMapCtrl);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (RMON2_ADDRMAPCTRL_POOL,
                            (UINT1 *) pAddrMapCtrl) != MEM_SUCCESS)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for AddressMapControlEntry\r\n");
        return OSIX_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AddrMapDelCtrlEntry\r\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AddrMapGetCtrlEntry                                   */
/*                                                                           */
/* Description  : Get Addr Map Ctrl Entry for the given control index        */
/*                                                                           */
/* Input        : u4AddressMapControlIndex                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tAddressMapControl / NULL Pointer                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC tAddressMapControl *
Rmon2AddrMapGetCtrlEntry (UINT4 u4AddressMapControlIndex)
{
    tAddressMapControl  AddrMapCtrl;
    MEMSET (&AddrMapCtrl, 0, sizeof (tAddressMapControl));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AddrMapGetCtrlEntry\r\n");

    AddrMapCtrl.u4AddressMapControlIndex = u4AddressMapControlIndex;

    return ((tAddressMapControl *) RBTreeGet (RMON2_ADDRMAPCTRL_TREE,
                                              (tRBElem *) & AddrMapCtrl));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AddrMapGetNextIndex                                   */
/*                                                                           */
/* Description  : Get First / Next AddrMap Index                             */
/*                                                                           */
/* Input        : u4ProtocolDirLocalIndex                                    */
/*                pAddressMapNetworkAddress, u4AddressMapSource              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tAddressMap / NULL Pointer                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC tAddressMap *
Rmon2AddrMapGetNextIndex (UINT4 u4ProtocolDirLocalIndex,
                          tIpAddr * pAddressMapNetworkAddress,
                          UINT4 u4AddressMapSource)
{
    tAddressMap         AddressMap;
    MEMSET (&AddressMap, 0, sizeof (tAddressMap));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AddrMapGetNextIndex\r\n");

    AddressMap.u4ProtocolDirLocalIndex = u4ProtocolDirLocalIndex;
    MEMCPY (&(AddressMap.AddressMapNetworkAddress),
            pAddressMapNetworkAddress, sizeof (tIpAddr));
    AddressMap.u4AddressMapSource = u4AddressMapSource;

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AddrMapGetNextIndex\r\n");

    return ((tAddressMap *) RBTreeGetNext (RMON2_ADDRMAP_TREE,
                                           (tRBElem *) & AddressMap, NULL));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AddrMapAddEntry                                       */
/*                                                                           */
/* Description  : Allocates memory and Adds AddrMap Entry into               */
/*                AddressMapRBTree                                           */
/*                                                                           */
/* Input        : u4ProtocolDirLocalIndex                                    */
/*                pAddressMapNetworkAddress, u4AddressMapSource              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tAddressMap / NULL Pointer                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC tAddressMap *
Rmon2AddrMapAddEntry (UINT4 u4ProtocolDirLocalIndex,
                      tIpAddr * pAddressMapNetworkAddress,
                      UINT4 u4AddressMapSource)
{
    tAddressMap        *pAddressMap = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AddrMapAddEntry\r\n");

    if ((pAddressMap = (tAddressMap *)
         (MemAllocMemBlk (RMON2_ADDRMAP_POOL))) == NULL)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for AddressMapEntry\r\n");
        return NULL;
    }
    MEMSET (pAddressMap, 0, sizeof (tAddressMap));

    pAddressMap->u4ProtocolDirLocalIndex = u4ProtocolDirLocalIndex;
    MEMCPY (&(pAddressMap->AddressMapNetworkAddress), pAddressMapNetworkAddress,
            sizeof (tIpAddr));
    pAddressMap->u4AddressMapSource = u4AddressMapSource;
    pAddressMap->u4AddressMapTimeMark = OsixGetSysUpTime ();
    pAddressMap->u4PktRefCount++;

    if (RBTreeAdd (RMON2_ADDRMAP_TREE, (tRBElem *) pAddressMap) == RB_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Rmon2AddrMapAddEntry: AddressMapEntry is added\r\n");
        return pAddressMap;
    }
    RMON2_TRC (RMON2_CRITICAL_TRC,
               "Rmon2AddrMapAddEntry: Adding AddressMapEntry Failed\r\n");
    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (RMON2_ADDRMAP_POOL, (UINT1 *) pAddressMap);

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AddrMapAddEntry\r\n");

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AddrMapDelDataEntry                                   */
/*                                                                           */
/* Description  : Releases memory and Removes AddrMap Entry from             */
/*                AddressMapRBTree                                           */
/*                                                                           */
/* Input        : pAddressMap                                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rmon2AddrMapDelDataEntry (tAddressMap * pAddressMap)
{
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AddrMapDelDataEntry\r\n");

    /* Remove PDist Stats node from AddressMapRBTree */
    RBTreeRem (RMON2_ADDRMAP_TREE, (tRBElem *) pAddressMap);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (RMON2_ADDRMAP_POOL,
                            (UINT1 *) pAddressMap) != MEM_SUCCESS)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for AddressMapEntry\r\n");
        return OSIX_FAILURE;
    }

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AddrMapDelDataEntry\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AddrMapGetDataEntry                                   */
/*                                                                           */
/* Description  : Get Addr Map Entry for the given index                     */
/*                                                                           */
/* Input        : u4ProtocolDirLocalIndex, AddressMapNetworkAddress,         */
/*                u4AddressMapSource                                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tAddressMap / NULL Pointer                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC tAddressMap *
Rmon2AddrMapGetDataEntry (UINT4 u4ProtocolDirLocalIndex,
                          tIpAddr * pAddressMapNetworkAddress,
                          UINT4 u4AddressMapSource)
{
    tAddressMap         AddressMap;
    MEMSET (&AddressMap, 0, sizeof (tAddressMap));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AddrMapGetDataEntry\r\n");

    AddressMap.u4ProtocolDirLocalIndex = u4ProtocolDirLocalIndex;
    MEMCPY (&(AddressMap.AddressMapNetworkAddress),
            pAddressMapNetworkAddress, sizeof (tIpAddr));
    AddressMap.u4AddressMapSource = u4AddressMapSource;

    return ((tAddressMap *) RBTreeGet (RMON2_ADDRMAP_TREE,
                                       (tRBElem *) & AddressMap));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AddrMapDelInterDep                                    */
/*                                                                           */
/* Description  : Removes the inter-dependencies and re-order the links      */
/*                                                                           */
/* Input        : pAddrMap                                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2AddrMapDelInterDep (tAddressMap * pAddrMap)
{
    tRmon2PktInfo      *pPktInfo = NULL;
    tPktHeader          PktHdr;

    MEMSET (&PktHdr, 0, sizeof (tPktHeader));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2AddrMapDelInterDep\r\n");

    if (pAddrMap->pPrevAddrMap == NULL)
    {
        /* Update PktInfo nodes to point next pAddrMap Entry */
        pPktInfo = Rmon2PktInfoGetNextIndex (&PktHdr);

        while (pPktInfo != NULL)
        {
            if (pPktInfo->pAddrMap == pAddrMap)
            {
                pPktInfo->pAddrMap = pAddrMap->pNextAddrMap;
                if (pPktInfo->pAddrMap != NULL)
                {
                    pPktInfo->pAddrMap->pPrevAddrMap = NULL;
                }
            }

            pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
        }
    }
    else if (pAddrMap->pNextAddrMap != NULL)
    {
        pAddrMap->pPrevAddrMap->pNextAddrMap = pAddrMap->pNextAddrMap;
    }
    else
    {
        pAddrMap->pPrevAddrMap->pNextAddrMap = NULL;
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2AddrMapPopulateDataEntries                            */
/*                                                                           */
/* Description  : Populates data entries if the same data source is already  */
/*                present and Adds the inter-dependencies b/w other tables   */
/*                                                                           */
/* Input        : pAddressMapCtrl                                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2AddrMapPopulateDataEntries (tAddressMapControl * pAddressMapCtrl)
{
    tRmon2PktInfo      *pPktInfo = NULL;
    tAddressMap        *pAddressMap = NULL, *pNewAddressMap = NULL;
    tPktHeader          PktHdr;

    MEMSET (&PktHdr, 0, sizeof (tPktHeader));

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY Rmon2AddrMapPopulateDataEntries\r\n");

    /* Check for the presence of data source in PktInfo table */
    pPktInfo = Rmon2PktInfoGetNextIndex (&PktHdr);
    while (pPktInfo != NULL)
    {
        if (pPktInfo->PktHdr.u4IfIndex ==
            pAddressMapCtrl->u4AddressMapControlDataSource)
        {
            if (gRmon2Globals.u4AddressMapMaxDesiredSupported == 0)
            {
                pAddressMapCtrl->u4AddressMapControlDroppedFrames++;
                pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
                continue;
            }

            /* Check whether data entries reached max limit */

            if ((gRmon2Globals.u4AddressMapInserts -
                 gRmon2Globals.u4AddressMapDeletes) >=
                gRmon2Globals.u4AddressMapMaxDesiredSupported)
            {
                pNewAddressMap = Rmon2AddrMapGiveLRUEntry ();

                if (pNewAddressMap != NULL)
                {
                    RMON2_TRC (RMON2_DEBUG_TRC,
                               "Deleting Address Map LRU Entry\r\n");

                    /* Remove Inter table references */
                    Rmon2AddrMapDelInterDep (pNewAddressMap);

                    gRmon2Globals.u4AddressMapDeletes++;

                    /* After Reference changes, free AddrMap entry */
                    Rmon2AddrMapDelDataEntry (pNewAddressMap);

                    pNewAddressMap = NULL;
                }
            }

            if (pPktInfo->u4ProtoNLIndex != 0)
            {
                /* Add AddressMap Entry */
                pNewAddressMap =
                    Rmon2AddrMapAddEntry (pPktInfo->u4ProtoNLIndex,
                                          &(pPktInfo->PktHdr.SrcIp),
                                          pPktInfo->PktHdr.u4IfIndex);
                if (pNewAddressMap == NULL)
                {
                    pAddressMapCtrl->u4AddressMapControlDroppedFrames++;
                    pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);
                    continue;
                }
                else
                {
                    MEMCPY (&pNewAddressMap->AddressMapPhysicalAddress,
                            &pPktInfo->PktHdr.SrcMACAddress, sizeof (tMacAddr));
                    pNewAddressMap->u4AddressMapLastChange =
                        OsixGetSysUpTime ();
                    gRmon2Globals.u4AddressMapInserts++;
                }

                if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV4)
                {
                    pNewAddressMap->u4Rmon2AddressMapIpAddrLen =
                        RMON2_IPV4_MAX_LEN;
                }
                else if (pPktInfo->PktHdr.u1IpVersion == RMON2_VER_IPV6)
                {
                    pNewAddressMap->u4Rmon2AddressMapIpAddrLen =
                        RMON2_IPV6_MAX_LEN;
                }

                pAddressMap = pPktInfo->pAddrMap;
                if (pAddressMap == NULL)
                {
                    pPktInfo->pAddrMap = pAddressMap;
                }
                else
                {
                    while (pAddressMap->pNextAddrMap != NULL)
                    {
                        pAddressMap = pAddressMap->pNextAddrMap;
                    }
                    pAddressMap->pNextAddrMap = pNewAddressMap;
                    pNewAddressMap->pPrevAddrMap = pAddressMap;
                }
            }

        }                        /* End of if check */
        pPktInfo = Rmon2PktInfoGetNextIndex (&pPktInfo->PktHdr);

    }                            /* End of While - PktInfo */
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2AddrMapPopulateDataEntries\r\n");
    return;
}
