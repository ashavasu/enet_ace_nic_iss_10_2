/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * $Id: rmn2npapi.c,v 1.4 2015/09/13 10:36:20 siva Exp $
 * Description:
 *       Contains wrapper functions definitions for RMONV2 NP calls.
 *******************************************************************/

/***************************************************************************
 *                                                                          
 *    Function Name       : Rmonv2FsRMONv2AddFlowStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsRMONv2AddFlowStats
 *                                                                          
 *    Input(s)            : Arguments of FsRMONv2AddFlowStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#ifndef __RMN2_NP_API_C
#define __RMN2_NP_API_C

#include "nputil.h"
UINT1
Rmonv2FsRMONv2AddFlowStats (tPktHeader FlowStatsTuple)
{
    tFsHwNp             FsHwNp;
    tRmonv2NpModInfo   *pRmonv2NpModInfo = NULL;
    tRmonv2NpWrFsRMONv2AddFlowStats *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RMONv2_MOD,    /* Module ID */
                         FS_RMONV2_ADD_FLOW_STATS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pRmonv2NpModInfo = &(FsHwNp.Rmonv2NpModInfo);
    pEntry = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2AddFlowStats;

    pEntry->FlowStatsTuple = FlowStatsTuple;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Rmonv2FsRMONv2CollectStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsRMONv2CollectStats
 *                                                                          
 *    Input(s)            : Arguments of FsRMONv2CollectStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Rmonv2FsRMONv2CollectStats (tPktHeader FlowStatsTuple, tRmon2Stats * pStatsCnt)
{
    tFsHwNp             FsHwNp;
    tRmonv2NpModInfo   *pRmonv2NpModInfo = NULL;
    tRmonv2NpWrFsRMONv2CollectStats *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RMONv2_MOD,    /* Module ID */
                         FS_RMONV2_COLLECT_STATS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pRmonv2NpModInfo = &(FsHwNp.Rmonv2NpModInfo);
    pEntry = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2CollectStats;

    pEntry->FlowStatsTuple = FlowStatsTuple;
    pEntry->pStatsCnt = pStatsCnt;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Rmonv2FsRMONv2DisableProbe                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsRMONv2DisableProbe
 *                                                                          
 *    Input(s)            : Arguments of FsRMONv2DisableProbe
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Rmonv2FsRMONv2DisableProbe (UINT4 u4InterfaceIdx, UINT2 u2VlanId,
                            tPortList PortList)
{
    tFsHwNp             FsHwNp;
    tRmonv2NpModInfo   *pRmonv2NpModInfo = NULL;
    tRmonv2NpWrFsRMONv2DisableProbe *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RMONv2_MOD,    /* Module ID */
                         FS_RMONV2_DISABLE_PROBE,    /* Function/OpCode */
                         u4InterfaceIdx,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         1);    /* No. of PortList Parms */
    pRmonv2NpModInfo = &(FsHwNp.Rmonv2NpModInfo);
    pEntry = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2DisableProbe;

    pEntry->u4InterfaceIdx = u4InterfaceIdx;
    pEntry->u2VlanId = u2VlanId;
    pEntry->PortList = PortList;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Rmonv2FsRMONv2DisableProtocol                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsRMONv2DisableProtocol
 *                                                                          
 *    Input(s)            : Arguments of FsRMONv2DisableProtocol
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Rmonv2FsRMONv2DisableProtocol (UINT4 u4ProtocolLclIdx)
{
    tFsHwNp             FsHwNp;
    tRmonv2NpModInfo   *pRmonv2NpModInfo = NULL;
    tRmonv2NpWrFsRMONv2DisableProtocol *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RMONv2_MOD,    /* Module ID */
                         FS_RMONV2_DISABLE_PROTOCOL,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pRmonv2NpModInfo = &(FsHwNp.Rmonv2NpModInfo);
    pEntry = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2DisableProtocol;

    pEntry->u4ProtocolLclIdx = u4ProtocolLclIdx;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Rmonv2FsRMONv2EnableProbe                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsRMONv2EnableProbe
 *                                                                          
 *    Input(s)            : Arguments of FsRMONv2EnableProbe
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Rmonv2FsRMONv2EnableProbe (UINT4 u4InterfaceIdx, UINT2 u2VlanId,
                           tPortList PortList)
{
    tFsHwNp             FsHwNp;
    tRmonv2NpModInfo   *pRmonv2NpModInfo = NULL;
    tRmonv2NpWrFsRMONv2EnableProbe *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RMONv2_MOD,    /* Module ID */
                         FS_RMONV2_ENABLE_PROBE,    /* Function/OpCode */
                         u4InterfaceIdx,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         1);    /* No. of PortList Parms */
    pRmonv2NpModInfo = &(FsHwNp.Rmonv2NpModInfo);
    pEntry = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2EnableProbe;

    pEntry->u4InterfaceIdx = u4InterfaceIdx;
    pEntry->u2VlanId = u2VlanId;
    pEntry->PortList = PortList;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Rmonv2FsRMONv2EnableProtocol                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsRMONv2EnableProtocol
 *                                                                          
 *    Input(s)            : Arguments of FsRMONv2EnableProtocol
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Rmonv2FsRMONv2EnableProtocol (tRmon2ProtocolIdfr Rmon2ProtoIdfr)
{
    tFsHwNp             FsHwNp;
    tRmonv2NpModInfo   *pRmonv2NpModInfo = NULL;
    tRmonv2NpWrFsRMONv2EnableProtocol *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RMONv2_MOD,    /* Module ID */
                         FS_RMONV2_ENABLE_PROTOCOL,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pRmonv2NpModInfo = &(FsHwNp.Rmonv2NpModInfo);
    pEntry = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2EnableProtocol;

    pEntry->Rmon2ProtoIdfr = Rmon2ProtoIdfr;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Rmonv2FsRMONv2HwSetStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsRMONv2HwSetStatus
 *                                                                          
 *    Input(s)            : Arguments of FsRMONv2HwSetStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Rmonv2FsRMONv2HwSetStatus (UINT4 u4RMONv2Status)
{
    tFsHwNp             FsHwNp;
    tRmonv2NpModInfo   *pRmonv2NpModInfo = NULL;
    tRmonv2NpWrFsRMONv2HwSetStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RMONv2_MOD,    /* Module ID */
                         FS_RMONV2_HW_SET_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pRmonv2NpModInfo = &(FsHwNp.Rmonv2NpModInfo);
    pEntry = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2HwSetStatus;

    pEntry->u4RMONv2Status = u4RMONv2Status;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Rmonv2FsRMONv2HwGetStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsRMONv2HwGetStatus
 *                                                                          
 *    Input(s)            : Arguments of FsRMONv2HwGetStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Rmonv2FsRMONv2HwGetStatus ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RMONv2_MOD,    /* Module ID */
                         FS_RMONV2_HW_GET_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Rmonv2FsRMONv2NPDeInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsRMONv2NPDeInit
 *                                                                          
 *    Input(s)            : Arguments of FsRMONv2NPDeInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Rmonv2FsRMONv2NPDeInit ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RMONv2_MOD,    /* Module ID */
                         FS_RMONV2_NP_DE_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Rmonv2FsRMONv2NPInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsRMONv2NPInit
 *                                                                          
 *    Input(s)            : Arguments of FsRMONv2NPInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Rmonv2FsRMONv2NPInit ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RMONv2_MOD,    /* Module ID */
                         FS_RMONV2_NP_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Rmonv2FsRMONv2RemoveFlowStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsRMONv2RemoveFlowStats
 *                                                                          
 *    Input(s)            : Arguments of FsRMONv2RemoveFlowStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Rmonv2FsRMONv2RemoveFlowStats (tPktHeader FlowStatsTuple)
{
    tFsHwNp             FsHwNp;
    tRmonv2NpModInfo   *pRmonv2NpModInfo = NULL;
    tRmonv2NpWrFsRMONv2RemoveFlowStats *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RMONv2_MOD,    /* Module ID */
                         FS_RMONV2_REMOVE_FLOW_STATS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pRmonv2NpModInfo = &(FsHwNp.Rmonv2NpModInfo);
    pEntry = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2RemoveFlowStats;

    pEntry->FlowStatsTuple = FlowStatsTuple;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}


/***************************************************************************
 *                                                                          
 *    Function Name       : Rmonv2FsRMONv2GetPortID 
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 
 *                                                                          
 *    Input(s)            : Arguments of FsRMONv2RemoveFlowStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Rmonv2FsRMONv2GetPortID (UINT1 *au1Mac,UINT4  u4VlanIndex, INT4 *i4Index)
{
    tFsHwNp             FsHwNp;
    tRmonv2NpModInfo   *pRmonv2NpModInfo = NULL;
    tRmonv2NpWrFsRMONv2GetPortID *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RMONv2_MOD,    /* Module ID */
                         FS_RMONV2_GET_PORT_ID,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pRmonv2NpModInfo = &(FsHwNp.Rmonv2NpModInfo);
    pEntry = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2GetPortID;
    pEntry->au1Mac = au1Mac;
    pEntry->u4VlanIndex = u4VlanIndex;
    pEntry->i4Index = i4Index;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}


#endif /* __RMN2_NP_API_C */
