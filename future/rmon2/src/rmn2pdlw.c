/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * $Id: rmn2pdlw.c,v 1.8 2015/10/05 08:01:22 siva Exp $
 *
* Description: This file contains the low level nmh routines for
 *              RMON2 Protocol Directory module
 *
 *******************************************************************/
#include "rmn2inc.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetProtocolDirLastChange
 Input       :  The Indices

                The Object 
                retValProtocolDirLastChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProtocolDirLastChange (UINT4 *pu4RetValProtocolDirLastChange)
{
    *pu4RetValProtocolDirLastChange = gRmon2Globals.u4ProtocolDirLastChange;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : ProtocolDirTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceProtocolDirTable
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceProtocolDirTable (tSNMP_OCTET_STRING_TYPE *
                                          pProtocolDirID,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pProtocolDirParameters)
{
    tProtocolDir       *pProtocolDirEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhValidateIndexInstanceProtocolDirTable \n");

    /* Check whether RMON2 is started. */
    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((pProtocolDirID->i4_Length < RMON2_MIN_PROT_DIR_ID_LEN) ||
        (pProtocolDirID->i4_Length > RMON2_MAX_PROT_DIR_ID_LEN))
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "SNMP Failure: Protocol Dir ID Length is not in \n"
                    "between MIN value %d and MAX value %d \r\n",
                    RMON2_MIN_PROT_DIR_ID_LEN, RMON2_MAX_PROT_DIR_ID_LEN);
        return SNMP_FAILURE;
    }

    if ((pProtocolDirParameters->i4_Length < RMON2_MIN_PROT_DIR_PARAM_LEN) ||
        (pProtocolDirParameters->i4_Length > RMON2_MAX_PROT_DIR_PARAM_LEN))
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "SNMP Failure: Protocol Dir Param Length is not in \n"
                    "between MIN value %d and MAX value %d \r\n",
                    RMON2_MIN_PROT_DIR_PARAM_LEN, RMON2_MAX_PROT_DIR_PARAM_LEN);
        return SNMP_FAILURE;
    }

    if (pProtocolDirID->i4_Length != (pProtocolDirParameters->i4_Length * 4))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Protocol Dir ID Length should be four "
                   "times the Protocol Dir Parameters Length \r\n");
        return SNMP_FAILURE;
    }

    pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                  pProtocolDirParameters);

    if (pProtocolDirEntry == NULL)
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "Get Entry of Protocol Directory table"
                    " for Index %s, %s is failed \r\n",
                    pProtocolDirID->pu1_OctetList,
                    pProtocolDirParameters->pu1_OctetList);
        return SNMP_FAILURE;
    }
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhValidateIndexInstanceProtocolDirTable \n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexProtocolDirTable
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexProtocolDirTable (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pProtocolDirParameters)
{
    UINT1               au1ProtocolDirID[RMON2_MAX_PROT_DIR_ID_LEN + 1];
    UINT1               au1ProtocolDirParameters[RMON2_MAX_PROT_DIR_PARAM_LEN +
                                                 1];
    tSNMP_OCTET_STRING_TYPE ProtocolDirID, ProtocolDirParameters;

    ProtocolDirID.pu1_OctetList = au1ProtocolDirID;
    ProtocolDirID.i4_Length = 0;

    ProtocolDirParameters.pu1_OctetList = au1ProtocolDirParameters;
    ProtocolDirParameters.i4_Length = 0;

    MEMSET (ProtocolDirID.pu1_OctetList, 0, RMON2_MAX_PROT_DIR_ID_LEN);
    MEMSET (ProtocolDirParameters.pu1_OctetList, 0,
            RMON2_MAX_PROT_DIR_PARAM_LEN);

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetFirstIndexProtocolDirTable \n");

    return nmhGetNextIndexProtocolDirTable (&ProtocolDirID,
                                            pProtocolDirID,
                                            &ProtocolDirParameters,
                                            pProtocolDirParameters);

}

/****************************************************************************
 Function    :  nmhGetNextIndexProtocolDirTable
 Input       :  The Indices
                ProtocolDirID
                nextProtocolDirID
                ProtocolDirParameters
                nextProtocolDirParameters
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexProtocolDirTable (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                                 tSNMP_OCTET_STRING_TYPE * pNextProtocolDirID,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pProtocolDirParameters,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNextProtocolDirParameters)
{
    tProtocolDir       *pProtocolDirEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetNextIndexProtocolDirTable \n");

    pProtocolDirEntry = Rmon2PDirGetNextProtDirIndex (pProtocolDirID,
                                                      pProtocolDirParameters);

    if (pProtocolDirEntry == NULL)
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "Get Entry of Protocol Directory table"
                    " for Index %s, %s is failed \r\n",
                    pProtocolDirID->pu1_OctetList,
                    pProtocolDirParameters->pu1_OctetList);
        return SNMP_FAILURE;
    }

    pNextProtocolDirID->i4_Length =
        (INT4) pProtocolDirEntry->u4ProtocolDirIDLen;
    MEMCPY (pNextProtocolDirID->pu1_OctetList,
            pProtocolDirEntry->au1ProtocolDirID, pNextProtocolDirID->i4_Length);

    pNextProtocolDirParameters->i4_Length =
        (INT4) pProtocolDirEntry->u4ProtocolDirParametersLen;
    MEMCPY (pNextProtocolDirParameters->pu1_OctetList,
            pProtocolDirEntry->au1ProtocolDirParameters,
            pNextProtocolDirParameters->i4_Length);

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetNextIndexProtocolDirTable \n");
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetProtocolDirLocalIndex
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters

                The Object 
                retValProtocolDirLocalIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProtocolDirLocalIndex (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                             tSNMP_OCTET_STRING_TYPE * pProtocolDirParameters,
                             INT4 *pi4RetValProtocolDirLocalIndex)
{
    tProtocolDir       *pProtocolDirEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetProtocolDirLocalIndex \n");

    pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                  pProtocolDirParameters);

    if (pProtocolDirEntry == NULL)
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "Get Entry of Protocol Directory table"
                    " for Index %s, %s is failed \r\n",
                    pProtocolDirID->pu1_OctetList,
                    pProtocolDirParameters->pu1_OctetList);
        return SNMP_FAILURE;
    }
    *pi4RetValProtocolDirLocalIndex =
        (INT4) pProtocolDirEntry->u4ProtocolDirLocalIndex;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetProtocolDirLocalIndex \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetProtocolDirDescr
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters

                The Object 
                retValProtocolDirDescr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProtocolDirDescr (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                        tSNMP_OCTET_STRING_TYPE * pProtocolDirParameters,
                        tSNMP_OCTET_STRING_TYPE * pRetValProtocolDirDescr)
{
    tProtocolDir       *pProtocolDirEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetProtocolDirDescr \n");

    pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                  pProtocolDirParameters);

    if (pProtocolDirEntry == NULL)
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "Failed in Get Entry of Protocol Directory table"
                    " for Index %s, %s is failed \r\n",
                    pProtocolDirID->pu1_OctetList,
                    pProtocolDirParameters->pu1_OctetList);
        return SNMP_FAILURE;
    }

    pRetValProtocolDirDescr->i4_Length =
        (INT4) STRLEN (pProtocolDirEntry->au1ProtocolDirDescr);
    MEMCPY (pRetValProtocolDirDescr->pu1_OctetList,
            pProtocolDirEntry->au1ProtocolDirDescr,
            pRetValProtocolDirDescr->i4_Length);

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetProtocolDirDescr \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetProtocolDirType
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters

                The Object 
                retValProtocolDirType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProtocolDirType (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                       tSNMP_OCTET_STRING_TYPE * pProtocolDirParameters,
                       tSNMP_OCTET_STRING_TYPE * pRetValProtocolDirType)
{
    tProtocolDir       *pProtocolDirEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetProtocolDirType \n");

    pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                  pProtocolDirParameters);

    if (pProtocolDirEntry == NULL)
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "Get Entry of Protocol Directory table"
                    " for Index %s, %s is failed \r\n",
                    pProtocolDirID->pu1_OctetList,
                    pProtocolDirParameters->pu1_OctetList);
        return SNMP_FAILURE;
    }
    pRetValProtocolDirType->pu1_OctetList[0] =
        pProtocolDirEntry->u1ProtocolDirType;
    pRetValProtocolDirType->i4_Length =
        sizeof (pProtocolDirEntry->u1ProtocolDirType);

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetProtocolDirType \n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetProtocolDirAddressMapConfig
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters

                The Object 
                retValProtocolDirAddressMapConfig
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProtocolDirAddressMapConfig (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pProtocolDirParameters,
                                   INT4 *pi4RetValProtocolDirAddressMapConfig)
{
    tProtocolDir       *pProtocolDirEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhGetProtocolDirAddressMapConfig \n");

    pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                  pProtocolDirParameters);

    if (pProtocolDirEntry == NULL)
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "Get Entry of Protocol Directory table"
                    " for Index %s, %s is failed \r\n",
                    pProtocolDirID->pu1_OctetList,
                    pProtocolDirParameters->pu1_OctetList);
        return SNMP_FAILURE;
    }
    *pi4RetValProtocolDirAddressMapConfig =
        (INT4) pProtocolDirEntry->u4ProtocolDirAddressMapConfig;

    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhGetProtocolDirAddressMapConfig \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetProtocolDirHostConfig
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters

                The Object 
                retValProtocolDirHostConfig
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProtocolDirHostConfig (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                             tSNMP_OCTET_STRING_TYPE * pProtocolDirParameters,
                             INT4 *pi4RetValProtocolDirHostConfig)
{
    tProtocolDir       *pProtocolDirEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetProtocolDirHostConfig \n");

    pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                  pProtocolDirParameters);

    if (pProtocolDirEntry == NULL)
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "Get Entry of Protocol Directory table"
                    " for Index : %s, %s is failed \r\n",
                    pProtocolDirID->pu1_OctetList,
                    pProtocolDirParameters->pu1_OctetList);
        return SNMP_FAILURE;
    }
    *pi4RetValProtocolDirHostConfig =
        (INT4) pProtocolDirEntry->u4ProtocolDirHostConfig;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetProtocolDirHostConfig \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetProtocolDirMatrixConfig
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters

                The Object 
                retValProtocolDirMatrixConfig
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProtocolDirMatrixConfig (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                               tSNMP_OCTET_STRING_TYPE * pProtocolDirParameters,
                               INT4 *pi4RetValProtocolDirMatrixConfig)
{
    tProtocolDir       *pProtocolDirEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetProtocolDirMatrixConfig \n");

    pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                  pProtocolDirParameters);

    if (pProtocolDirEntry == NULL)
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "Get Entry of Protocol Directory table"
                    " for Index : %s, %s is failed \r\n",
                    pProtocolDirID->pu1_OctetList,
                    pProtocolDirParameters->pu1_OctetList);
        return SNMP_FAILURE;
    }
    *pi4RetValProtocolDirMatrixConfig =
        (INT4) pProtocolDirEntry->u4ProtocolDirMatrixConfig;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetProtocolDirMatrixConfig \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetProtocolDirOwner
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters

                The Object 
                retValProtocolDirOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProtocolDirOwner (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                        tSNMP_OCTET_STRING_TYPE * pProtocolDirParameters,
                        tSNMP_OCTET_STRING_TYPE * pRetValProtocolDirOwner)
{
    tProtocolDir       *pProtocolDirEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetProtocolDirOwner \n");

    pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                  pProtocolDirParameters);

    if (pProtocolDirEntry == NULL)
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "Get Entry of Protocol Directory table"
                    " for Index : %s, %s is failed \r\n",
                    pProtocolDirID->pu1_OctetList,
                    pProtocolDirParameters->pu1_OctetList);
        return SNMP_FAILURE;
    }

    pRetValProtocolDirOwner->i4_Length =
        (INT4) STRLEN (pProtocolDirEntry->au1ProtocolDirOwner);
    MEMCPY (pRetValProtocolDirOwner->pu1_OctetList,
            pProtocolDirEntry->au1ProtocolDirOwner,
            pRetValProtocolDirOwner->i4_Length);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetProtocolDirOwner \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetProtocolDirStatus
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters

                The Object 
                retValProtocolDirStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetProtocolDirStatus (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                         tSNMP_OCTET_STRING_TYPE * pProtocolDirParameters,
                         INT4 *pi4RetValProtocolDirStatus)
{
    tProtocolDir       *pProtocolDirEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhGetProtocolDirStatus \n");

    pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                  pProtocolDirParameters);

    if (pProtocolDirEntry == NULL)
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "Get Entry of Protocol Directory table"
                    " for Index : %s, %s is failed \r\n",
                    pProtocolDirID->pu1_OctetList,
                    pProtocolDirParameters->pu1_OctetList);
        return SNMP_FAILURE;
    }
    *pi4RetValProtocolDirStatus = (INT4) pProtocolDirEntry->u4ProtocolDirStatus;
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhGetProtocolDirStatus \n");
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetProtocolDirDescr
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters

                The Object 
                setValProtocolDirDescr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetProtocolDirDescr (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                        tSNMP_OCTET_STRING_TYPE * pProtocolDirParameters,
                        tSNMP_OCTET_STRING_TYPE * pSetValProtocolDirDescr)
{
    tProtocolDir       *pProtocolDirEntry = NULL;

    UINT4               u4Minimum;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhSetProtocolDirDescr \n");

    u4Minimum =
        (UINT4) ((pSetValProtocolDirDescr->i4_Length <
                  RMON2_PROT_DIR_DESC_LEN) ? pSetValProtocolDirDescr->
                 i4_Length : RMON2_PROT_DIR_DESC_LEN);

    pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                  pProtocolDirParameters);

    if (pProtocolDirEntry == NULL)
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "Get Entry of Protocol Directory table"
                    " for Index : %s, %s is failed \r\n",
                    pProtocolDirID->pu1_OctetList,
                    pProtocolDirParameters->pu1_OctetList);
        return SNMP_FAILURE;
    }

    MEMCPY (pProtocolDirEntry->au1ProtocolDirDescr,
            pSetValProtocolDirDescr->pu1_OctetList, u4Minimum);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhSetProtocolDirDescr \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetProtocolDirAddressMapConfig
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters

                The Object 
                setValProtocolDirAddressMapConfig
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetProtocolDirAddressMapConfig (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pProtocolDirParameters,
                                   INT4 i4SetValProtocolDirAddressMapConfig)
{
    tProtocolDir       *pProtocolDirEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhSetProtocolDirAddressMapConfig \n");

    pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                  pProtocolDirParameters);

    if (pProtocolDirEntry == NULL)
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "Get Entry of Protocol Directory table"
                    " for Index : %s, %s is failed \r\n",
                    pProtocolDirID->pu1_OctetList,
                    pProtocolDirParameters->pu1_OctetList);
        return SNMP_FAILURE;
    }

    if (i4SetValProtocolDirAddressMapConfig == SUPPORTED_OFF)
    {
        Rmon2AddrMapUpdateProtoChgs (pProtocolDirEntry->
                                     u4ProtocolDirLocalIndex);
    }

    pProtocolDirEntry->u4ProtocolDirAddressMapConfig =
        (UINT4) i4SetValProtocolDirAddressMapConfig;
    gRmon2Globals.u4ProtocolDirLastChange = OsixGetSysUpTime ();
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhSetProtocolDirAddressMapConfig \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetProtocolDirHostConfig
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters

                The Object 
                setValProtocolDirHostConfig
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetProtocolDirHostConfig (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                             tSNMP_OCTET_STRING_TYPE * pProtocolDirParameters,
                             INT4 i4SetValProtocolDirHostConfig)
{
    tProtocolDir       *pProtocolDirEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhSetProtocolDirHostConfig \n");

    pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                  pProtocolDirParameters);

    if (pProtocolDirEntry == NULL)
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "Get Entry of Protocol Directory table"
                    " for Index : %s, %s is failed \r\n",
                    pProtocolDirID->pu1_OctetList,
                    pProtocolDirParameters->pu1_OctetList);
        return SNMP_FAILURE;
    }
    if (i4SetValProtocolDirHostConfig == SUPPORTED_OFF)
    {
        Rmon2HostUpdateProtoChgs (pProtocolDirEntry->u4ProtocolDirLocalIndex);
    }

    pProtocolDirEntry->u4ProtocolDirHostConfig =
        (UINT4) i4SetValProtocolDirHostConfig;
    gRmon2Globals.u4ProtocolDirLastChange = OsixGetSysUpTime ();

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhSetProtocolDirHostConfig \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetProtocolDirMatrixConfig
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters

                The Object 
                setValProtocolDirMatrixConfig
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetProtocolDirMatrixConfig (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                               tSNMP_OCTET_STRING_TYPE * pProtocolDirParameters,
                               INT4 i4SetValProtocolDirMatrixConfig)
{
    tProtocolDir       *pProtocolDirEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhSetProtocolDirMatrixConfig \n");

    pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                  pProtocolDirParameters);

    if (pProtocolDirEntry == NULL)
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "Get Entry of Protocol Directory table"
                    " for Index : %s, %s is failed \r\n",
                    pProtocolDirID->pu1_OctetList,
                    pProtocolDirParameters->pu1_OctetList);
        return SNMP_FAILURE;
    }
    if (i4SetValProtocolDirMatrixConfig == SUPPORTED_OFF)
    {
        Rmon2MatrixUpdateProtoChgs (pProtocolDirEntry->u4ProtocolDirLocalIndex);
    }

    pProtocolDirEntry->u4ProtocolDirMatrixConfig =
        (UINT4) i4SetValProtocolDirMatrixConfig;
    gRmon2Globals.u4ProtocolDirLastChange = OsixGetSysUpTime ();

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhSetProtocolDirMatrixConfig \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetProtocolDirOwner
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters

                The Object 
                setValProtocolDirOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetProtocolDirOwner (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                        tSNMP_OCTET_STRING_TYPE * pProtocolDirParameters,
                        tSNMP_OCTET_STRING_TYPE * pSetValProtocolDirOwner)
{
    tProtocolDir       *pProtocolDirEntry = NULL;

    UINT4               u4Minimum;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhSetProtocolDirOwner \n");

    u4Minimum =
        (UINT4) ((pSetValProtocolDirOwner->i4_Length <
                  RMON2_OWNER_LEN) ? pSetValProtocolDirOwner->
                 i4_Length : RMON2_OWNER_LEN);

    pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                  pProtocolDirParameters);

    if (pProtocolDirEntry == NULL)
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "Get Entry of Protocol Directory table"
                    " for Index : %s, %s is failed \r\n",
                    pProtocolDirID->pu1_OctetList,
                    pProtocolDirParameters->pu1_OctetList);
        return SNMP_FAILURE;
    }
    MEMCPY (pProtocolDirEntry->au1ProtocolDirOwner,
            pSetValProtocolDirOwner->pu1_OctetList, u4Minimum);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhSetProtocolDirOwner \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetProtocolDirStatus
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters

                The Object 
                setValProtocolDirStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetProtocolDirStatus (tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                         tSNMP_OCTET_STRING_TYPE * pProtocolDirParameters,
                         INT4 i4SetValProtocolDirStatus)
{
    tProtocolDir       *pProtocolDirEntry = NULL;
    INT4                i4Result;
    INT4                i4ResultValue;
#ifdef NPAPI_WANTED
    tRmon2ProtocolIdfr  NpProtoId;
    UINT4               u4Index = 1;    /*was u4Index = 1; */

    MEMSET (&NpProtoId, 0, sizeof (tRmon2ProtocolIdfr));
#endif

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhSetProtocolDirStatus \n");

    switch (i4SetValProtocolDirStatus)
    {
        case CREATE_AND_WAIT:
            if (Rmon2ChkForPrntEntryAndExFeature (pProtocolDirID,
                                                  pProtocolDirParameters)
                == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }
            pProtocolDirEntry = Rmon2PDirAddProtDirEntry (pProtocolDirID,
                                                          pProtocolDirParameters);
            if (pProtocolDirEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC | RMON2_MEM_FAIL,
                           "Adding to Protocol Directory \
                                table is failed\n");
                return SNMP_FAILURE;
            }
            pProtocolDirEntry->u1ProtocolDirType =
                (UINT1) Rmon2PDirFindProtDirType (pProtocolDirID,
                                                  pProtocolDirParameters);

            pProtocolDirEntry->u4ProtocolDirAddressMapConfig =
                Rmon2PDirGetProtAddrMapConfig (pProtocolDirID,
                                               pProtocolDirParameters);

            pProtocolDirEntry->u4ProtocolDirHostConfig =
                Rmon2PDirGetProtHostConfig (pProtocolDirID,
                                            pProtocolDirParameters);

            pProtocolDirEntry->u4ProtocolDirMatrixConfig =
                Rmon2PDirGetProtMatrixConfig (pProtocolDirID,
                                              pProtocolDirParameters);

            pProtocolDirEntry->u4ProtocolDirStatus = NOT_READY;
            gRmon2Globals.u4ProtocolDirLastChange = OsixGetSysUpTime ();
            break;

        case ACTIVE:
            pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                          pProtocolDirParameters);

            if (pProtocolDirEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC, "Get Entry of Protocol Directory "
                           "table is failed\n");
                return SNMP_FAILURE;
            }
            pProtocolDirEntry->u4ProtocolDirStatus = ACTIVE;
#ifdef NPAPI_WANTED
            NpProtoId.u4ProtoDirLclIdx =
                pProtocolDirEntry->u4ProtocolDirLocalIndex;

            if (u4Index < (UINT4) pProtocolDirParameters->i4_Length)
            {
                MEMCPY (&NpProtoId.u2L3ProtocolId,
                        pProtocolDirID->pu1_OctetList + 6, 2);
                NpProtoId.u2L3ProtocolId =
                    OSIX_NTOHS (NpProtoId.u2L3ProtocolId);
                u4Index++;
            }
            if (u4Index < (UINT4) pProtocolDirParameters->i4_Length)
            {
                MEMCPY (&NpProtoId.u2L4ProtocolId,
                        pProtocolDirID->pu1_OctetList + 10, 2);
                NpProtoId.u2L4ProtocolId =
                    OSIX_NTOHS (NpProtoId.u2L4ProtocolId);
                u4Index++;
            }
            if (u4Index < (UINT4) pProtocolDirParameters->i4_Length)
            {
                MEMCPY (&NpProtoId.u2L5ProtocolId,
                        pProtocolDirID->pu1_OctetList + 14, 2);
                NpProtoId.u2L5ProtocolId =
                    OSIX_NTOHS (NpProtoId.u2L5ProtocolId);
                u4Index++;
            }
            Rmonv2FsRMONv2EnableProtocol (NpProtoId);
#endif
            break;

        case NOT_IN_SERVICE:
            pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                          pProtocolDirParameters);

            if (pProtocolDirEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC, "Get Entry of Protocol Directory "
                           "table is failed \n");
                return SNMP_FAILURE;
            }
            pProtocolDirEntry->u4ProtocolDirStatus = NOT_IN_SERVICE;

#ifdef NPAPI_WANTED
            Rmonv2FsRMONv2DisableProtocol (pProtocolDirEntry->
                                           u4ProtocolDirLocalIndex);
#endif

            Rmon2MainUpdateProtoChgs (pProtocolDirEntry->
                                      u4ProtocolDirLocalIndex);
#ifdef DSMON_WANTED
            DsmonMainUpdateProtoChgs (pProtocolDirEntry->
                                      u4ProtocolDirLocalIndex);
#endif
            break;

        case DESTROY:
            pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                          pProtocolDirParameters);

            if (pProtocolDirEntry == NULL)
            {
                RMON2_TRC (RMON2_DEBUG_TRC, "Get Entry of Protocol Directory "
                           "table is failed \n");
                return SNMP_FAILURE;
            }

#ifdef NPAPI_WANTED
            Rmonv2FsRMONv2DisableProtocol (pProtocolDirEntry->
                                           u4ProtocolDirLocalIndex);
#endif

            Rmon2MainUpdateProtoChgs (pProtocolDirEntry->
                                      u4ProtocolDirLocalIndex);
#ifdef DSMON_WANTED
            DsmonMainUpdateProtoChgs (pProtocolDirEntry->
                                      u4ProtocolDirLocalIndex);
#endif
            i4ResultValue =
                (INT4) Rmon2DeleteChildProtocol (pProtocolDirID,
                                                 pProtocolDirParameters);

            if (i4ResultValue == OSIX_FAILURE)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "RBTreeRemove Failure for Child Protocol"
                           "entry deletion\n");
                return SNMP_FAILURE;
            }

            i4Result = Rmon2PDirDelProtDirEntry (pProtocolDirEntry);
            if (i4Result == OSIX_FAILURE)
            {
                RMON2_TRC (RMON2_DEBUG_TRC,
                           "RBTreeRemove Failure for Protocol Directory "
                           "entry \n");
                return SNMP_FAILURE;
            }
            gRmon2Globals.u4ProtocolDirLastChange = OsixGetSysUpTime ();
            break;
        default:
            RMON2_TRC (RMON2_DEBUG_TRC, "Unknown option \n");
            return SNMP_FAILURE;
            break;

    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhSetProtocolDirStatus \n");
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2ProtocolDirDescr
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters

                The Object 
                testValProtocolDirDescr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ProtocolDirDescr (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                           tSNMP_OCTET_STRING_TYPE * pProtocolDirParameters,
                           tSNMP_OCTET_STRING_TYPE * pTestValProtocolDirDescr)
{
    tProtocolDir       *pProtocolDirEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhTestv2ProtocolDirDescr \n");

    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((pProtocolDirID->i4_Length < RMON2_MIN_PROT_DIR_ID_LEN) ||
        (pProtocolDirID->i4_Length > RMON2_MAX_PROT_DIR_ID_LEN))
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "SNMP Failure: Protocol Dir ID Length is not in \n"
                    "between MIN value %d and MAX value %d \r\n",
                    RMON2_MIN_PROT_DIR_ID_LEN, RMON2_MAX_PROT_DIR_ID_LEN);
        return SNMP_FAILURE;
    }

    if ((pProtocolDirParameters->i4_Length < RMON2_MIN_PROT_DIR_PARAM_LEN) ||
        (pProtocolDirParameters->i4_Length > RMON2_MAX_PROT_DIR_PARAM_LEN))
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "SNMP Failure: Protocol Dir Param Length is not in \n"
                    "between MIN value %d and MAX value %d \r\n",
                    RMON2_MIN_PROT_DIR_PARAM_LEN, RMON2_MAX_PROT_DIR_PARAM_LEN);
        return SNMP_FAILURE;
    }

    if ((pTestValProtocolDirDescr->i4_Length < RMON2_ONE) ||
        (pTestValProtocolDirDescr->i4_Length > RMON2_PROT_DIR_DESC_LEN))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Wrong length for ProtocolDirDescr object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValProtocolDirDescr->pu1_OctetList,
                              pTestValProtocolDirDescr->i4_Length)
        == OSIX_FAILURE)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Invalid Characters for ProtocolDirDescr "
                   "string object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                  pProtocolDirParameters);

    if (pProtocolDirEntry != NULL)
    {
        if (pProtocolDirEntry->u4ProtocolDirStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "ERROR: Rowstatus of Protocol Directory Entry is Active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry is not exist \n");
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhTestv2ProtocolDirDescr \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2ProtocolDirAddressMapConfig
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters

                The Object 
                testValProtocolDirAddressMapConfig
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ProtocolDirAddressMapConfig (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pProtocolDirParameters,
                                      INT4 i4TestValProtocolDirAddressMapConfig)
{
    tProtocolDir       *pProtocolDirEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2ProtocolDirAddressMapConfig \n");

    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((pProtocolDirID->i4_Length < RMON2_MIN_PROT_DIR_ID_LEN) ||
        (pProtocolDirID->i4_Length > RMON2_MAX_PROT_DIR_ID_LEN))
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "SNMP Failure: Protocol Dir ID Length is not in \n"
                    "between MIN value %d and MAX value %d \r\n",
                    RMON2_MIN_PROT_DIR_ID_LEN, RMON2_MAX_PROT_DIR_ID_LEN);
        return SNMP_FAILURE;
    }

    if ((pProtocolDirParameters->i4_Length < RMON2_MIN_PROT_DIR_PARAM_LEN) ||
        (pProtocolDirParameters->i4_Length > RMON2_MAX_PROT_DIR_PARAM_LEN))
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "SNMP Failure: Protocol Dir Param Length is not in \n"
                    "between MIN value %d and MAX value %d \r\n",
                    RMON2_MIN_PROT_DIR_PARAM_LEN, RMON2_MAX_PROT_DIR_PARAM_LEN);
        return SNMP_FAILURE;
    }

    if ((i4TestValProtocolDirAddressMapConfig < NOT_SUPPORTED) ||
        (i4TestValProtocolDirAddressMapConfig > SUPPORTED_ON))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Wrong value for ProtocolDirAddressMapConfig "
                   "object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                  pProtocolDirParameters);

    if (pProtocolDirEntry != NULL)
    {
        if (pProtocolDirEntry->u4ProtocolDirStatus != ACTIVE)
        {
            if ((pProtocolDirEntry->u4ProtocolDirAddressMapConfig
                 == NOT_SUPPORTED
                 && i4TestValProtocolDirAddressMapConfig == NOT_SUPPORTED)
                ||
                (pProtocolDirEntry->u4ProtocolDirAddressMapConfig
                 != NOT_SUPPORTED
                 && i4TestValProtocolDirAddressMapConfig != NOT_SUPPORTED))
            {
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "ERROR:Rowstatus of Protocol Directory Entry is Active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry is not exist \n");
    RMON2_TRC (RMON2_FN_EXIT,
               "FUNC: EXIT nmhTestv2ProtocolDirAddressMapConfig \n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2ProtocolDirHostConfig
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters

                The Object 
                testValProtocolDirHostConfig
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ProtocolDirHostConfig (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                                tSNMP_OCTET_STRING_TYPE *
                                pProtocolDirParameters,
                                INT4 i4TestValProtocolDirHostConfig)
{
    tProtocolDir       *pProtocolDirEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhTestv2ProtocolDirHostConfig \n");

    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((pProtocolDirID->i4_Length < RMON2_MIN_PROT_DIR_ID_LEN) ||
        (pProtocolDirID->i4_Length > RMON2_MAX_PROT_DIR_ID_LEN))
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "SNMP Failure: Protocol Dir ID Length is not in \n"
                    "between MIN value %d and MAX value %d \r\n",
                    RMON2_MIN_PROT_DIR_ID_LEN, RMON2_MAX_PROT_DIR_ID_LEN);
        return SNMP_FAILURE;
    }

    if ((pProtocolDirParameters->i4_Length < RMON2_MIN_PROT_DIR_PARAM_LEN) ||
        (pProtocolDirParameters->i4_Length > RMON2_MAX_PROT_DIR_PARAM_LEN))
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "SNMP Failure: Protocol Dir Param Length is not in \n"
                    "between MIN value %d and MAX value %d \r\n",
                    RMON2_MIN_PROT_DIR_PARAM_LEN, RMON2_MAX_PROT_DIR_PARAM_LEN);
        return SNMP_FAILURE;
    }

    if ((i4TestValProtocolDirHostConfig < NOT_SUPPORTED) ||
        (i4TestValProtocolDirHostConfig > SUPPORTED_ON))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Wrong value for ProtocolDirHostConfig object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                  pProtocolDirParameters);

    if (pProtocolDirEntry != NULL)
    {
        if (pProtocolDirEntry->u4ProtocolDirStatus != ACTIVE)
        {
            if ((pProtocolDirEntry->u4ProtocolDirHostConfig
                 == NOT_SUPPORTED
                 && i4TestValProtocolDirHostConfig == NOT_SUPPORTED)
                ||
                (pProtocolDirEntry->u4ProtocolDirHostConfig
                 != NOT_SUPPORTED
                 && i4TestValProtocolDirHostConfig != NOT_SUPPORTED))
            {
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "ERROR:Rowstatus of Protocol Directory Entry is Active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry is not \
                exist \n");
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhTestv2ProtocolDirHostConfig \n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2ProtocolDirMatrixConfig
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters

                The Object 
                testValProtocolDirMatrixConfig
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ProtocolDirMatrixConfig (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pProtocolDirParameters,
                                  INT4 i4TestValProtocolDirMatrixConfig)
{
    tProtocolDir       *pProtocolDirEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY nmhTestv2ProtocolDirMatrixConfig \n");

    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((pProtocolDirID->i4_Length < RMON2_MIN_PROT_DIR_ID_LEN) ||
        (pProtocolDirID->i4_Length > RMON2_MAX_PROT_DIR_ID_LEN))
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "SNMP Failure: Protocol Dir ID Length is not in \n"
                    "between MIN value %d and MAX value %d \r\n",
                    RMON2_MIN_PROT_DIR_ID_LEN, RMON2_MAX_PROT_DIR_ID_LEN);
        return SNMP_FAILURE;
    }

    if ((pProtocolDirParameters->i4_Length < RMON2_MIN_PROT_DIR_PARAM_LEN) ||
        (pProtocolDirParameters->i4_Length > RMON2_MAX_PROT_DIR_PARAM_LEN))
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "SNMP Failure: Protocol Dir Param Length is not in \n"
                    "between MIN value %d and MAX value %d \r\n",
                    RMON2_MIN_PROT_DIR_PARAM_LEN, RMON2_MAX_PROT_DIR_PARAM_LEN);
        return SNMP_FAILURE;
    }

    if ((i4TestValProtocolDirMatrixConfig < NOT_SUPPORTED) ||
        (i4TestValProtocolDirMatrixConfig > SUPPORTED_ON))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Wrong value for ProtocolDirMatrixConfig "
                   "object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                  pProtocolDirParameters);

    if (pProtocolDirEntry != NULL)
    {
        if (pProtocolDirEntry->u4ProtocolDirStatus != ACTIVE)
        {
            if ((pProtocolDirEntry->u4ProtocolDirHostConfig
                 == NOT_SUPPORTED
                 && i4TestValProtocolDirMatrixConfig == NOT_SUPPORTED)
                ||
                (pProtocolDirEntry->u4ProtocolDirHostConfig
                 != NOT_SUPPORTED
                 && i4TestValProtocolDirMatrixConfig != NOT_SUPPORTED))
            {
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "ERROR:Rowstatus of Protocol Directory Entry is Active \n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry is not exist \n");
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhTestv2ProtocolDirMatrixConfig \n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2ProtocolDirOwner
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters

                The Object 
                testValProtocolDirOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ProtocolDirOwner (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                           tSNMP_OCTET_STRING_TYPE * pProtocolDirParameters,
                           tSNMP_OCTET_STRING_TYPE * pTestValProtocolDirOwner)
{
    tProtocolDir       *pProtocolDirEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhTestv2ProtocolDirOwner \n");

    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((pProtocolDirID->i4_Length < RMON2_MIN_PROT_DIR_ID_LEN) ||
        (pProtocolDirID->i4_Length > RMON2_MAX_PROT_DIR_ID_LEN))
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "SNMP Failure: Protocol Dir ID Length is not in \n"
                    "between MIN value %d and MAX value %d \r\n",
                    RMON2_MIN_PROT_DIR_ID_LEN, RMON2_MAX_PROT_DIR_ID_LEN);
        return SNMP_FAILURE;
    }

    if ((pProtocolDirParameters->i4_Length < RMON2_MIN_PROT_DIR_PARAM_LEN) ||
        (pProtocolDirParameters->i4_Length > RMON2_MAX_PROT_DIR_PARAM_LEN))
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "SNMP Failure: Protocol Dir Param Length is not in \n"
                    "between MIN value %d and MAX value %d \r\n",
                    RMON2_MIN_PROT_DIR_PARAM_LEN, RMON2_MAX_PROT_DIR_PARAM_LEN);
        return SNMP_FAILURE;
    }

    if ((pTestValProtocolDirOwner->i4_Length < RMON2_ZERO) ||
        (pTestValProtocolDirOwner->i4_Length > RMON2_OWNER_LEN))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Wrong length for ProtocolDirOwner object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValProtocolDirOwner->pu1_OctetList,
                              pTestValProtocolDirOwner->i4_Length)
        == OSIX_FAILURE)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Invalid Characters for ProtocolDirOwner string "
                   "object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif
    pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                  pProtocolDirParameters);

    if (pProtocolDirEntry != NULL)
    {
        if (pProtocolDirEntry->u4ProtocolDirStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "ERROR:Rowstatus of Protocol Directory Entry is Active \n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "SNMP Failure: Entry is not exist \n");
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhTestv2ProtocolDirOwner \n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2ProtocolDirStatus
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters

                The Object 
                testValProtocolDirStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ProtocolDirStatus (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pProtocolDirID,
                            tSNMP_OCTET_STRING_TYPE * pProtocolDirParameters,
                            INT4 i4TestValProtocolDirStatus)
{
    tProtocolDir       *pProtocolDirEntry = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY nmhTestv2ProtocolDirStatus \n");

    if (!(RMON2_IS_ENABLED ()))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((pProtocolDirID->i4_Length < RMON2_MIN_PROT_DIR_ID_LEN) ||
        (pProtocolDirID->i4_Length > RMON2_MAX_PROT_DIR_ID_LEN))
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "SNMP Failure: Protocol Dir ID Length is not in \n"
                    "between MIN value %d and MAX value %d \r\n",
                    RMON2_MIN_PROT_DIR_ID_LEN, RMON2_MAX_PROT_DIR_ID_LEN);
        return SNMP_FAILURE;
    }

    if ((pProtocolDirParameters->i4_Length < RMON2_MIN_PROT_DIR_PARAM_LEN) ||
        (pProtocolDirParameters->i4_Length > RMON2_MAX_PROT_DIR_PARAM_LEN))
    {
        RMON2_TRC2 (RMON2_DEBUG_TRC,
                    "SNMP Failure: Protocol Dir Param Length is not in \n"
                    "between MIN value %d and MAX value %d \r\n",
                    RMON2_MIN_PROT_DIR_PARAM_LEN, RMON2_MAX_PROT_DIR_PARAM_LEN);
        return SNMP_FAILURE;
    }

    if (pProtocolDirID->i4_Length != (pProtocolDirParameters->i4_Length * 4))
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "SNMP Failure: Protocol Dir ID Length should be four "
                   "times the Protocol Dir Parameters Length \r\n");
        return SNMP_FAILURE;
    }

    if ((i4TestValProtocolDirStatus < ACTIVE) ||
        (i4TestValProtocolDirStatus > DESTROY))
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "ERROR - Wrong value for ProtocolDirStatus object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pProtocolDirEntry = Rmon2PDirGetProtDirEntry (pProtocolDirID,
                                                  pProtocolDirParameters);

    if (pProtocolDirEntry == NULL)
    {
        if (i4TestValProtocolDirStatus == CREATE_AND_WAIT)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
                   "SNMP Failure: Entry is not exist \n");
        return SNMP_FAILURE;
    }
    if ((i4TestValProtocolDirStatus == ACTIVE) ||
        (i4TestValProtocolDirStatus == NOT_IN_SERVICE) ||
        (i4TestValProtocolDirStatus == DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    RMON2_TRC (RMON2_CRITICAL_TRC | RMON2_DEBUG_TRC,
               "The given Entry is already exist.. \n");

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT nmhTestv2ProtocolDirStatus \n");
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2ProtocolDirTable
 Input       :  The Indices
                ProtocolDirID
                ProtocolDirParameters
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2ProtocolDirTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
