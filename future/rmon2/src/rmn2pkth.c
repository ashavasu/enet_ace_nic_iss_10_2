/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * $Id: rmn2pkth.c,v 1.8 2012/07/10 05:57:10 siva Exp $
 *
 * Description: This file contains the functional routine for
 *              RMON2 PktInfo module
 *
 *******************************************************************/
#include "rmn2inc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PktInfoAddEntry                                   */
/*                                                                           */
/* Description  : Allocates memory and Adds PktInfo Entry into               */
/*                Rmon2PktInfoRBTree                         */
/*                                                                           */
/* Input        : PktInfo                                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tRmon2PktInfo / NULL Pointer                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC tRmon2PktInfo *
Rmon2PktInfoAddEntry (tRmon2PktInfo * pPktInfoParams)
{
    tRmon2PktInfo      *pPktInfo = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PktInfoAddEntry\r\n");

    if ((pPktInfo = (tRmon2PktInfo *)
         (MemAllocMemBlk (RMON2_PKTINFO_POOL))) == NULL)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for PacketInfoEntry\r\n");
        return NULL;
    }

    MEMSET (pPktInfo, 0, sizeof (tRmon2PktInfo));
    MEMCPY (pPktInfo, pPktInfoParams, sizeof (tRmon2PktInfo));

    if (RBTreeAdd (RMON2_PKTINFO_TREE, (tRBElem *) pPktInfo) == RB_SUCCESS)
    {
        RMON2_TRC (RMON2_DEBUG_TRC,
                   "Rmon2PktInfoAddEntry: PacketInfoEntry is added\r\n");
        return pPktInfo;
    }

    RMON2_TRC (RMON2_CRITICAL_TRC,
               "Rmon2PktInfoAddEntry: Adding PacketInfoEntry Failed\r\n");

    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (RMON2_PKTINFO_POOL, (UINT1 *) pPktInfo);

    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2PktInfoAddEntry\r\n");

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PktInfoDelUnusedEntries                              */
/*                                                                           */
/* Description  : Releases memory and Removes PktInfo Entry from             */
/*                Rmon2PktInfoRBTree                                         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2PktInfoDelUnusedEntries ()
{
    tRmon2PktInfo       PktInfo, *pPktInfo = NULL;
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PktInfoDelUnusedEntries\r\n");

    pPktInfo = (tRmon2PktInfo *) RBTreeGetFirst (RMON2_PKTINFO_TREE);

    while (pPktInfo != NULL)
    {
        MEMCPY (&PktInfo, pPktInfo, sizeof (tRmon2PktInfo));
        if ((pPktInfo->pPDist == NULL) &&
            (pPktInfo->pAddrMap == NULL) &&
            (pPktInfo->pInNlHost == NULL) &&
            (pPktInfo->pOutNlHost == NULL) &&
            (pPktInfo->pInAlHost == NULL) &&
            (pPktInfo->pOutAlHost == NULL) &&
            (pPktInfo->pNlMatrixSD == NULL) && (pPktInfo->pAlMatrixSD == NULL))
        {
            Rmon2PktInfoDelEntry (pPktInfo);
        }
        pPktInfo = (tRmon2PktInfo *) RBTreeGetNext (RMON2_PKTINFO_TREE,
                                                    (tRBElem *) & PktInfo,
                                                    NULL);
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2PktInfoDelUnusedEntries\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PktInfoDelEntry                                       */
/*                                                                           */
/* Description  : Releases memory and Removes PktInfo Entry from             */
/*        Rmon2PktInfoRBTree                             */
/*                                                                           */
/* Input        : pPktInfo                                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rmon2PktInfoDelEntry (tRmon2PktInfo * pPktInfo)
{
#ifdef NPAPI_WANTED
    if (pPktInfo->u1IsNPFlowAdded == OSIX_TRUE)
    {
        Rmonv2FsRMONv2RemoveFlowStats (pPktInfo->PktHdr);
    }
#endif
    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PktInfoDelEntry\r\n");
    /* Remove pktinfo node from Rmon2PktInfoRBTree */
    RBTreeRem (RMON2_PKTINFO_TREE, (tRBElem *) pPktInfo);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (RMON2_PKTINFO_POOL,
                            (UINT1 *) pPktInfo) != MEM_SUCCESS)
    {
        RMON2_TRC (RMON2_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for PacketInfoEntry\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PktInfoGetNextIndex                                   */
/*                                                                           */
/* Description  : Gets next PktInfo entry from Rmon2PktInfoRBTree            */
/*                                                                           */
/* Input        : u4IfIndex, u4ProtoNLIndex, u4ProtoALIndex, SrcIp, DstIp    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pPktInfo / NULL                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tRmon2PktInfo *
Rmon2PktInfoGetNextIndex (tPktHeader * pPktHdr)
{
    tRmon2PktInfo      *pPktInfo = NULL;
    tRmon2PktInfo       PktInfo;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PktInfoGetNextIndex\r\n");
    MEMSET (&PktInfo, 0, sizeof (tRmon2PktInfo));

    MEMCPY (&PktInfo.PktHdr, pPktHdr, sizeof (tPktHeader));

    pPktInfo = (tRmon2PktInfo *) RBTreeGetNext (RMON2_PKTINFO_TREE,
                                                (tRBElem *) & PktInfo, NULL);
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2PktInfoGetNextIndex\r\n");

    return pPktInfo;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PktInfoGetEntry                                   */
/*                                                                           */
/* Description  : Gets PktInfo entry from Rmon2PktInfoRBTree                 */
/*                                                                           */
/* Input        : u4IfIndex, u4ProtoNLIndex, u4ProtoALIndex, SrcIp, DstIp    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pPktInfo / NULL                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tRmon2PktInfo *
Rmon2PktInfoGetEntry (tPktHeader * pPktHdr)
{
    tRmon2PktInfo      *pPktInfo = NULL;
    tRmon2PktInfo       PktInfo;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PktInfoGetEntry\r\n");
    MEMSET (&PktInfo, 0, sizeof (tRmon2PktInfo));

    MEMCPY (&PktInfo.PktHdr, pPktHdr, sizeof (tPktHeader));

    pPktInfo = (tRmon2PktInfo *) RBTreeGet (RMON2_PKTINFO_TREE,
                                            (tRBElem *) & PktInfo);
    return pPktInfo;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PktInfoProcessIncomingPkt                                */
/*                                                                           */
/* Description  : Process incoming packet information from NPAPI and add    */
/*        an entry in Rmon2PktInfoRBTree                 */
/*                                                                           */
/* Input        : PktInfo                                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tRmon2PktInfo / NULL Pointer                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC tRmon2PktInfo *
Rmon2PktInfoProcessIncomingPkt (tRmon2PktInfo * pPktInfoParams)
{
    tRmon2PktInfo      *pPktInfo = NULL;

    RMON2_TRC (RMON2_FN_ENTRY,
               "FUNC: ENTRY Rmon2PktInfoProcessIncomingPkt\r\n");
    pPktInfo = Rmon2PktInfoGetEntry (&pPktInfoParams->PktHdr);
    if (pPktInfo == NULL)
    {
        pPktInfo = Rmon2PktInfoAddEntry (pPktInfoParams);
    }
    else
    {
        return NULL;
    }

    return pPktInfo;
}

/************************************************************************/
/*  Function Name   : Rmon2PktInfoUpdateProtoChgs                       */
/*                                  */
/*  Description     : If RMONv2 removes a protocol entry from its       */
/*            ProtocolDir Table, this function will be invoked  */
/*            by RMONv2 to reflect the changes in RMON2     */
/*                                  */
/*  Input(s)        : u4Rmon2ProtDirLocalIndex                      */
/*                                  */
/*  Output(s)       : None.                                             */
/*                                  */
/*  Returns         : None                                              */
/************************************************************************/
PUBLIC VOID
Rmon2PktInfoUpdateProtoChgs (UINT4 u4Rmon2ProtDirLocalIndex)
{
    tRmon2PktInfo      *pPktInfo = NULL, PktInfo;

    MEMSET (&PktInfo, 0, sizeof (tRmon2PktInfo));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PktInfoUpdateProtoChgs\r\n");
    /* Get First PktInfo entry */
    pPktInfo = Rmon2PktInfoGetNextIndex (&PktInfo.PktHdr);

    while (pPktInfo != NULL)
    {
        MEMCPY (&PktInfo, pPktInfo, sizeof (tRmon2PktInfo));
        if ((pPktInfo->u4ProtoNLIndex == u4Rmon2ProtDirLocalIndex) ||
            (pPktInfo->u4ProtoL4ALIndex == u4Rmon2ProtDirLocalIndex) ||
            (pPktInfo->u4ProtoL5ALIndex == u4Rmon2ProtDirLocalIndex))
        {
            Rmon2PktInfoDelIdlePkt (pPktInfo);
        }

        pPktInfo = Rmon2PktInfoGetNextIndex (&PktInfo.PktHdr);
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2PktInfoUpdateProtoChgs\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PktInfoUpdateIFIndexChgs                                */
/*                                                                           */
/* Description  : If Operstatus is CFA_IF_DOWN, then deleting the associated */
/*                Pktinfo Table.                         */
/*                                                                           */
/* Input        : u4IfIndex, u1OperStatus                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2PktInfoUpdateIFIndexChgs (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tRmon2PktInfo      *pPktInfo = NULL, PktInfo;

    MEMSET (&PktInfo, 0, sizeof (tRmon2PktInfo));

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC: ENTRY Rmon2PktInfoUpdateIFIndexChgs\r\n");
    switch (u1OperStatus)
    {
        case CFA_IF_UP:
            break;
        case CFA_IF_DOWN:
            /* Get First PktInfo entry */
            pPktInfo = Rmon2PktInfoGetNextIndex (&PktInfo.PktHdr);

            while (pPktInfo != NULL)
            {
                MEMCPY (&PktInfo, pPktInfo, sizeof (tRmon2PktInfo));
                if (pPktInfo->PktHdr.u4IfIndex == u4IfIndex)
                {
                    Rmon2PktInfoDelEntry (pPktInfo);
                }
                pPktInfo = Rmon2PktInfoGetNextIndex (&PktInfo.PktHdr);
            }
            break;
        default:
            break;
    }
    RMON2_TRC (RMON2_FN_EXIT, "FUNC: EXIT Rmon2PktInfoUpdateIFIndexChgs\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rmon2PktInfoDelIdlePkt                                     */
/*                                                                           */
/* Description  : This routine removes Least recently used pkt entry and its */
/*                associated data entries.                                   */
/*                                                                           */
/* Input        : pPktInfo                                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Rmon2PktInfoDelIdlePkt (tRmon2PktInfo * pPktInfo)
{
    tProtocolDistStats *pPDist = NULL;
    tAddressMap        *pAddrMap = NULL;
    tNlHost            *pNlHost = NULL;
    tAlHost            *pAlHost = NULL;
    tNlMatrixSD        *pNlMatrixSD = NULL;
    tAlMatrixSD        *pAlMatrixSD = NULL;

    RMON2_TRC (RMON2_FN_ENTRY, "FUNC:ENTRY Rmon2PktInfoDelIdlePkt\r\n");

    /* Remove Nl Matrix Entries */
    pNlMatrixSD = pPktInfo->pNlMatrixSD;

    while (pNlMatrixSD != NULL)
    {
        if (pNlMatrixSD->u4PktRefCount <= RMON2_ONE)
        {
            Rmon2NlMatrixDelInterDep (pNlMatrixSD);
            pNlMatrixSD->pHlMatrixCtrl->u4HlMatrixControlNlDeletes += 2;
            Rmon2NlMatrixDelDataEntry (pNlMatrixSD);
            pNlMatrixSD = pPktInfo->pNlMatrixSD;
        }
        else
        {
            pNlMatrixSD->u4PktRefCount--;
            pNlMatrixSD = pNlMatrixSD->pNextNlMatrixSD;
        }
    }

    /* Remove Al Matrix Entries */
    pAlMatrixSD = pPktInfo->pAlMatrixSD;

    while (pAlMatrixSD != NULL)
    {
        if (pAlMatrixSD->u4PktRefCount <= RMON2_ONE)
        {
            Rmon2AlMatrixDelInterDep (pAlMatrixSD);
            pAlMatrixSD->pHlMatrixCtrl->u4HlMatrixControlAlDeletes += 2;
            Rmon2AlMatrixDelDataEntry (pAlMatrixSD);
            pAlMatrixSD = pPktInfo->pAlMatrixSD;
        }
        else
        {
            pAlMatrixSD->u4PktRefCount--;
            pAlMatrixSD = pAlMatrixSD->pNextAlMatrixSD;
        }
    }

    /* Remove Nl Host Entries */
    pNlHost = pPktInfo->pOutNlHost;

    while (pNlHost != NULL)
    {
        if (pNlHost->u4PktRefCount <= RMON2_ONE)
        {
            Rmon2NlHostDelInterDep (pNlHost);
            pNlHost->pHlHostCtrl->u4HlHostControlNlDeletes++;
            Rmon2NlHostDelDataEntry (pNlHost);
            pNlHost = pPktInfo->pOutNlHost;
        }
        else
        {
            pNlHost->u4PktRefCount--;
            pNlHost = pNlHost->pNextNlHost;
        }
    }

    /* Remove Al Host Entries */
    pAlHost = pPktInfo->pOutAlHost;

    while (pAlHost != NULL)
    {
        if (pAlHost->u4PktRefCount <= RMON2_ONE)
        {
            Rmon2AlHostDelInterDep (pAlHost);
            pAlHost->pHlHostCtrl->u4HlHostControlAlDeletes++;
            Rmon2AlHostDelDataEntry (pAlHost);
            pAlHost = pPktInfo->pOutAlHost;
        }
        else
        {
            pAlHost->u4PktRefCount--;
            pAlHost = pAlHost->pNextAlHost;
        }
    }

    /* Remove AddrMap Entries */
    pAddrMap = pPktInfo->pAddrMap;

    while (pAddrMap != NULL)
    {
        if (pAddrMap->u4PktRefCount <= RMON2_ONE)
        {
            Rmon2AddrMapDelInterDep (pAddrMap);
            gRmon2Globals.u4AddressMapDeletes++;
            Rmon2AddrMapDelDataEntry (pAddrMap);
        }
        else
        {
            pAddrMap->u4PktRefCount--;
        }
        pAddrMap = pPktInfo->pAddrMap;
    }

    /* Remove Pdist Entries */
    pPDist = pPktInfo->pPDist;

    while (pPDist != NULL)
    {
        if (pPDist->u4PktRefCount <= RMON2_ONE)
        {
            Rmon2PDistDelInterDep (pPDist);
            Rmon2PDistDelDataEntry (pPDist);
            pPDist = pPktInfo->pPDist;
        }
        else
        {
            pPDist->u4PktRefCount--;
            pPDist = pPDist->pNextPDist;
        }
    }

    /* Remove Packet Info Entry */
    Rmon2PktInfoDelEntry (pPktInfo);

    RMON2_TRC (RMON2_FN_EXIT, "FUNC:EXIT Rmon2PktInfoDelIdlePkt\r\n");
    return;
}
