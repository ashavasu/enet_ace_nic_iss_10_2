/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * $Id: rmn2npwr.c,v 1.3 2015/09/13 10:36:20 siva Exp $
 * Description:
 *       Contains wrapper functions for RMONV2 NP calls.
 *******************************************************************/

/***************************************************************************
 *                                                                          
 *    Function Name       : Rmonv2NpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tRmonv2NpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#ifndef __RMN2_NP_WR_C
#define __RMN2_NP_WR_C

#include "nputil.h"

PUBLIC UINT1
Rmonv2NpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tRmonv2NpModInfo   *pRmonv2NpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pRmonv2NpModInfo = &(pFsHwNp->Rmonv2NpModInfo);

    if (NULL == pRmonv2NpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_RMONV2_ADD_FLOW_STATS:
        {
            tRmonv2NpWrFsRMONv2AddFlowStats *pEntry = NULL;
            pEntry = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2AddFlowStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRMONv2AddFlowStats (pEntry->FlowStatsTuple);
            break;
        }
        case FS_RMONV2_COLLECT_STATS:
        {
            tRmonv2NpWrFsRMONv2CollectStats *pEntry = NULL;
            pEntry = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2CollectStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsRMONv2CollectStats (pEntry->FlowStatsTuple,
                                      pEntry->pStatsCnt);
            break;
        }
        case FS_RMONV2_DISABLE_PROBE:
        {
            tRmonv2NpWrFsRMONv2DisableProbe *pEntry = NULL;
            pEntry = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2DisableProbe;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsRMONv2DisableProbe (pEntry->u4InterfaceIdx, pEntry->u2VlanId,
                                      pEntry->PortList);
            break;
        }
        case FS_RMONV2_DISABLE_PROTOCOL:
        {
            tRmonv2NpWrFsRMONv2DisableProtocol *pEntry = NULL;
            pEntry = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2DisableProtocol;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRMONv2DisableProtocol (pEntry->u4ProtocolLclIdx);
            break;
        }
        case FS_RMONV2_ENABLE_PROBE:
        {
            tRmonv2NpWrFsRMONv2EnableProbe *pEntry = NULL;
            pEntry = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2EnableProbe;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsRMONv2EnableProbe (pEntry->u4InterfaceIdx, pEntry->u2VlanId,
                                     pEntry->PortList);
            break;
        }
        case FS_RMONV2_ENABLE_PROTOCOL:
        {
            tRmonv2NpWrFsRMONv2EnableProtocol *pEntry = NULL;
            pEntry = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2EnableProtocol;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRMONv2EnableProtocol (pEntry->Rmon2ProtoIdfr);
            break;
        }
        case FS_RMONV2_HW_SET_STATUS:
        {
            tRmonv2NpWrFsRMONv2HwSetStatus *pEntry = NULL;
            pEntry = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2HwSetStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRMONv2HwSetStatus (pEntry->u4RMONv2Status);
            break;
        }
        case FS_RMONV2_HW_GET_STATUS:
        {
            u1RetVal = FsRMONv2HwGetStatus ();
            break;
        }
        case FS_RMONV2_NP_DE_INIT:
        {
            FsRMONv2NPDeInit ();
            break;
        }
        case FS_RMONV2_NP_INIT:
        {
            u1RetVal = FsRMONv2NPInit ();
            break;
        }
        case FS_RMONV2_REMOVE_FLOW_STATS:
        {
            tRmonv2NpWrFsRMONv2RemoveFlowStats *pEntry = NULL;
            pEntry = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2RemoveFlowStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRMONv2RemoveFlowStats (pEntry->FlowStatsTuple);
            break;
        }
        case FS_RMONV2_GET_PORT_ID:
        {
            tRmonv2NpWrFsRMONv2GetPortID *pEntry = NULL;

            pEntry = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2GetPortID;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsRMONv2GetPortID (pEntry->au1Mac,pEntry->u4VlanIndex,pEntry->i4Index);
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif /* __RMN2_NP_WR_C */
