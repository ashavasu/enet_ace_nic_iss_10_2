#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : MAKE.H                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                     |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX ( Slackware 1.2.1 )                     |
# |                                                                          |
# |   DATE                   : 07th Mar 2002                                 |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

PNAC_SWITCHES = -DRAD_TEST

TOTAL_OPNS = ${PNAC_SWITCHES} $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

CFA_BASE_DIR   = ${BASE_DIR}/cfa2

PNAC_BASE_DIR      = ${BASE_DIR}/pnac
PNAC_SRC_DIR       = ${PNAC_BASE_DIR}/src
PNAC_INC_DIR       = ${PNAC_BASE_DIR}/inc
PNAC_OBJ_DIR       = ${PNAC_BASE_DIR}/obj
SNMP_INCL_DIR      = ${BASE_DIR}/inc/snmp
CFA_INCD           = ${CFA_BASE_DIR}/inc
CMN_INC_DIR        = ${BASE_DIR}/inc

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${PNAC_INC_DIR} -I${CFA_INCD} -I${CMN_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
