#####################################################################
##########################################################################
# Copyright (C) 2010 Aricent Inc . All Rights Reserved                   #
# ------------------------------------------                             #
# $Id: Makefile,v 1.28 2014/07/15 12:56:55 siva Exp $                  #
##########################################################################
####  MAKEFILE HEADER ::                                         ####
##|###############################################################|##
##|    FILE NAME               ::  Makefile                       |##
##|                                                               |##
##|    PRINCIPAL    AUTHOR     ::  Aricent Inc.      |##
##|                                                               |##
##|    SUBSYSTEM NAME          ::  PNAC                           |##
##|                                                               |##
##|    MODULE NAME             ::  TOP most level Makefile        |##
##|                                                               |##
##|    TARGET ENVIRONMENT      ::  Linux                          |##
##|                                                               |##
##|    DATE OF FIRST RELEASE   ::  07 March 2002                  |##
##|                                                               |##
##|            DESCRIPTION     ::                                 |##
##|                                MAKEFILE for FuturePNAC        |##
##|                                Final Object                   |##
##|                                                               |##
##|###############################################################|##

.PHONY  : clean
include ../LR/make.h
include ../LR/make.rule
include ./make.h



ISS_C_FLAGS           =  ${CC_FLAGS} ${TOTAL_OPNS} ${INCLUDES}
PNAC_FINAL_OBJ = ${PNAC_OBJ_DIR}/FuturePNAC.o

#object module list

PNAC_OBJS = \
       ${PNAC_OBJ_DIR}/pnactmr.o \
       ${PNAC_OBJ_DIR}/pnackey.o \
       ${PNAC_OBJ_DIR}/pnacasif.o \
       ${PNAC_OBJ_DIR}/pnacinit.o \
       ${PNAC_OBJ_DIR}/pnacpae.o \
       ${PNAC_OBJ_DIR}/pnacauth.o \
       ${PNAC_OBJ_DIR}/pnacasm.o \
       ${PNAC_OBJ_DIR}/pnacbsm.o \
       ${PNAC_OBJ_DIR}/pnacssm.o \
       ${PNAC_OBJ_DIR}/pnacsupp.o \
       ${PNAC_OBJ_DIR}/pnacutil.o \
       ${PNAC_OBJ_DIR}/pnacsrv.o \
       ${PNAC_OBJ_DIR}/pnacport.o \
       ${PNAC_OBJ_DIR}/pnacslow.o \
       ${PNAC_OBJ_DIR}/pnacplow.o \
       ${PNAC_OBJ_DIR}/pnacintf.o \
       ${PNAC_OBJ_DIR}/pnacl2wr.o \
       ${PNAC_OBJ_DIR}/pnacapi.o \
       ${PNAC_OBJ_DIR}/pnacsz.o \
       ${PNAC_OBJ_DIR}/dpnacutil.o


ifeq (${SNMP_2}, YES)
PNAC_OBJS += \
       ${PNAC_OBJ_DIR}/stdpnawr.o \
       ${PNAC_OBJ_DIR}/fspnacwr.o
endif

ifeq (${L2RED}, YES)
PNAC_OBJS += \
       ${PNAC_OBJ_DIR}/pnacred.o
else
PNAC_OBJS += \
       ${PNAC_OBJ_DIR}/pnacrdsb.o
endif

ifeq (${CLI}, YES)
PNAC_OBJS += \
       ${PNAC_OBJ_DIR}/pnaccli.o	       	
endif
ifeq (${MBSM}, YES)
PNAC_OBJS += \
     	${PNAC_OBJ_DIR}/pnacmbsm.o
endif

ifeq (${NPAPI}, YES)
PNAC_OBJS += \
       ${PNAC_OBJ_DIR}/pnacnpwr.o \
       ${PNAC_OBJ_DIR}/pnacnpapi.o
endif

#object module dependency
${PNAC_FINAL_OBJ} :obj ${PNAC_OBJS} 
	${LD} ${LD_FLAGS} ${CC_COMMON_OPTIONS} -o ${PNAC_FINAL_OBJ} ${PNAC_OBJS}

obj:
	${MKDIR}  $(MKDIR_FLAGS) $(PNAC_OBJ_DIR) 

EXTERNAL_DEPENDENCIES = \
   ${COMN_INCL_DIR}/lr.h        \
   ${COMN_INCL_DIR}/ip.h        \
   ${COMN_INCL_DIR}/cfa.h       \
   ${COMN_INCL_DIR}/la.h        \
   ${COMN_INCL_DIR}/bridge.h    \
   ${SNMP_INCL_DIR}/snmctdfs.h  \
   ${SNMP_INCL_DIR}/snmccons.h  \
   ${SNMP_INCL_DIR}/snmcdefn.h

ifeq (${NPAPI}, YES)
EXTERNAL_DEPENDENCIES +=$(NPAPI_INCLUDE_DIR)/pnacnp.h
EXTERNAL_DEPENDENCIES +=$(COMN_INCL_DIR)/nputil.h
INTERNAL_DEPENDENCIES +=$(COMN_INCL_DIR)/pnacnpwr.h
endif

ifeq (${MBSM}, YES)
EXTERNAL_DEPENDENCIES +=${COMN_INCL_DIR}/mbsm.h
endif

INTERNAL_DEPENDENCIES = \
        ${PNAC_BASE_DIR}/make.h \
        ${PNAC_BASE_DIR}/Makefile \
        ${COMN_INCL_DIR}/pnac.h \
        ${PNAC_INC_DIR}/pnachdrs.h \
        ${PNAC_INC_DIR}/pnacglob.h \
        ${PNAC_INC_DIR}/pnaccons.h \
        ${PNAC_INC_DIR}/pnacextn.h \
        ${PNAC_INC_DIR}/pnacmacr.h \
        ${PNAC_INC_DIR}/pnacprot.h \
        ${PNAC_INC_DIR}/pnacport.h \
        ${PNAC_INC_DIR}/pnacsem.h \
        ${PNAC_INC_DIR}/pnactrc.h \
        ${PNAC_INC_DIR}/pnacsrv.h \
        ${PNAC_INC_DIR}/pnacasp.h \
        ${PNAC_INC_DIR}/pnacast.h \
        ${PNAC_INC_DIR}/pnacasg.h \
        ${PNAC_INC_DIR}/pnacsrm.h \
        ${PNAC_INC_DIR}/pnactdfs.h \
        ${PNAC_INC_DIR}/pnacred.h \
	${PNAC_INC_DIR}/pnacsz.h 

ifeq (${L2RED}, YES)
INTERNAL_DEPENDENCIES += \
       ${PNAC_INC_DIR}/pnacred.h
else
INTERNAL_DEPENDENCIES += \
       ${PNAC_INC_DIR}/pnacrdsb.h
endif
 
DEPENDENCIES =  \
    ${EXTERNAL_DEPENDENCIES} \
	 ${INTERNAL_DEPENDENCIES} \
	 ${COMMON_DEPENDENCIES}

$(PNAC_OBJ_DIR)/pnactmr.o : \
   $(PNAC_SRC_DIR)/pnactmr.c  \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(PNAC_SRC_DIR)/pnactmr.c -o $@


$(PNAC_OBJ_DIR)/pnackey.o : \
   $(PNAC_SRC_DIR)/pnackey.c \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnackey.c -o $@

$(PNAC_OBJ_DIR)/pnacasif.o : \
   $(PNAC_SRC_DIR)/pnacasif.c  \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES}
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnacasif.c -o $@

$(PNAC_OBJ_DIR)/pnacinit.o : \
   $(PNAC_SRC_DIR)/pnacinit.c  \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES} $(COMN_INCL_DIR)/radius.h $(COMN_INCL_DIR)/rstp.h
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnacinit.c -o $@


$(PNAC_OBJ_DIR)/pnacpae.o : \
   $(PNAC_SRC_DIR)/pnacpae.c  \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES} $(COMN_INCL_DIR)/rstp.h $(COMN_INCL_DIR)/mstp.h
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnacpae.c -o $@

$(PNAC_OBJ_DIR)/pnacauth.o : \
   $(PNAC_SRC_DIR)/pnacauth.c  \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES} $(COMN_INCL_DIR)/rstp.h $(COMN_INCL_DIR)/mstp.h
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnacauth.c -o $@

$(PNAC_OBJ_DIR)/pnacasm.o : \
   $(PNAC_SRC_DIR)/pnacasm.c  \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES}
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnacasm.c -o $@

$(PNAC_OBJ_DIR)/pnacbsm.o : \
   $(PNAC_SRC_DIR)/pnacbsm.c  \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES}
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnacbsm.c -o $@
   
$(PNAC_OBJ_DIR)/pnacssm.o : \
   $(PNAC_SRC_DIR)/pnacssm.c  \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES}
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnacssm.c -o $@

$(PNAC_OBJ_DIR)/pnacsupp.o : \
   $(PNAC_SRC_DIR)/pnacsupp.c  \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES}
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnacsupp.c -o $@

$(PNAC_OBJ_DIR)/pnacutil.o : \
   $(PNAC_SRC_DIR)/pnacutil.c  \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES}
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnacutil.c -o $@

$(PNAC_OBJ_DIR)/pnacsrv.o : \
   $(PNAC_SRC_DIR)/pnacsrv.c  \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES}
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnacsrv.c -o $@

$(PNAC_OBJ_DIR)/pnacport.o : \
   $(PNAC_SRC_DIR)/pnacport.c  \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES} $(COMN_INCL_DIR)/radius.h
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnacport.c -o $@

$(PNAC_OBJ_DIR)/pnacslow.o : \
   $(PNAC_SRC_DIR)/pnacslow.c  \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES}
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnacslow.c -o $@


$(PNAC_OBJ_DIR)/pnacplow.o : \
   $(PNAC_SRC_DIR)/pnacplow.c  \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES}
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnacplow.c -o $@

$(PNAC_OBJ_DIR)/fspnacwr.o : \
   $(PNAC_SRC_DIR)/fspnacwr.c  \
	${DEPENDENCIES}
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/fspnacwr.c -o $@

$(PNAC_OBJ_DIR)/stdpnawr.o : \
   $(PNAC_SRC_DIR)/stdpnawr.c  \
	${DEPENDENCIES}
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/stdpnawr.c -o $@

$(PNAC_OBJ_DIR)/pnacintf.o : \
   $(PNAC_SRC_DIR)/pnacintf.c  \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES}
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnacintf.c -o $@

$(PNAC_OBJ_DIR)/pnacl2wr.o : \
   $(PNAC_SRC_DIR)/pnacl2wr.c  \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES}
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnacl2wr.c -o $@

$(PNAC_OBJ_DIR)/pnacapi.o : \
   $(PNAC_SRC_DIR)/pnacapi.c  \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES}
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnacapi.c -o $@

$(PNAC_OBJ_DIR)/pnaccli.o : \
   $(PNAC_SRC_DIR)/pnaccli.c  \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES}
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnaccli.c -o $@

$(PNAC_OBJ_DIR)/pnacmbsm.o : \
   $(PNAC_SRC_DIR)/pnacmbsm.c  \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES}
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnacmbsm.c -o $@

$(PNAC_OBJ_DIR)/pnacred.o : \
   $(PNAC_SRC_DIR)/pnacred.c \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnacred.c -o $@

$(PNAC_OBJ_DIR)/pnacrdsb.o : \
   $(PNAC_SRC_DIR)/pnacrdsb.c \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnacrdsb.c -o $@

$(PNAC_OBJ_DIR)/pnacsz.o : \
   $(PNAC_SRC_DIR)/pnacsz.c  \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES}
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnacsz.c -o $@

$(PNAC_OBJ_DIR)/pnacnpwr.o : \
   $(PNAC_SRC_DIR)/pnacnpwr.c  \
   $(COMN_INCL_DIR)/pnacnpwr.h  \
	${DEPENDENCIES}
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnacnpwr.c -o $@

$(PNAC_OBJ_DIR)/pnacnpapi.o : \
   $(PNAC_SRC_DIR)/pnacnpapi.c  \
	${DEPENDENCIES}
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/pnacnpapi.c -o $@

$(PNAC_OBJ_DIR)/dpnacutil.o : \
   $(PNAC_SRC_DIR)/dpnacutil.c  \
	${DEPENDENCIES}
	$(CC)  $(ISS_C_FLAGS) $(PNAC_SRC_DIR)/dpnacutil.c -o $@
clean:
	$(RM) $(RM_FLAGS) obj/*.o

pkg:
	CUR_PWD=${PWD};\
        cd ${BASE_DIR}/../..;\
        tar cvzf ${ISS_PKG_CREATE_DIR}/pnac.tgz -T ${BASE_DIR}/pnac/FILES.NEW;\
        cd ${CUR_PWD};

