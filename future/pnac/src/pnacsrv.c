/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pnacsrv.c,v 1.30 2015/12/14 10:59:16 siva Exp $
 *
 * Description     : This file contains Authentication
 *                   functions of PNAC module
 *******************************************************************/

#ifndef _PNACSRV_C
#define _PNACSRV_C

#include "pnachdrs.h"
#ifdef WPS_WANTED
#include "wps.h"
#endif

/*****************************************************************************/
/* Function Name      : PnacServInit                                         */
/*                                                                           */
/* Description        : This function allocates memory pools for Local       */
/*                      Authentication Server of PNAC module                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacServInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacServInfo                                        */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
PnacServInit (VOID)
{
    /* PNAC_MEMSET (&gPnacServInfo, PNAC_INIT_VAL, sizeof (tgPnacServInfo)); */
    gPnacServInfo.cacheTimeout = PNAC_AS_CACHE_TIMEOUT;

    PNAC_SLL_INIT (&(PNAC_SERV_USER_LIST));
    PNAC_SLL_INIT (&(PNAC_SERV_CACHE_LIST));

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacServRemoveCache                                  */
/*                                                                           */
/* Description        : This function removes the cache entries maintained   */
/*                      by the authentication server of PNAC module.         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacServInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacServInfo                                        */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacServRemoveCache (VOID)
{
    tPnacSLLNode       *pNode = NULL;

   /*************************************/
    /* Release the Cache Records.        */
   /*************************************/
    while ((pNode = PNAC_SLL_GET (&PNAC_SERV_CACHE_LIST)) != NULL)
    {
        PNAC_DEBUG (PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                                   " SLL Cache Node deleted : %x\n", pNode);
            );
        if (((tPnacServAuthCache *) pNode)->pTmrNode != NULL)
        {
            if (PnacTmrStopTmr ((tPnacServAuthCache *) pNode,
                                PNAC_SERVERCACHE_TIMER) != PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " AS: Stop Cache Timer Failed... Anyway continuing..\n");
            }
        }
        if (PNAC_RELEASE_SERVCACHE_MEMBLK (pNode) != PNAC_MEM_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " AS: Releasing Cache Node failed\n");
        }
    }
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacServDisable                                      */
/*                                                                           */
/* Description        : This function disables Local Authentication Server   */
/*                      of PNAC module                                       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacServInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacServInfo                                        */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacServDisable (VOID)
{
    tPnacSLLNode       *pNode = NULL;

   /*************************************/
    /* Release the user records.         */
   /*************************************/

    while ((pNode = PNAC_SLL_GET (&PNAC_SERV_USER_LIST)) != NULL)
    {
        PNAC_DEBUG (PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                                   " SLL User Node deleted : %x\n", pNode);
            );
        if (PNAC_RELEASE_SERVUSER_MEMBLK (pNode) != PNAC_MEM_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " AS: Releasing User Info Node failed\n");
        }
    }
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacServInitUserRecord                               */
/*                                                                           */
/* Description        : This function creates a User record for the          */
/*                      inputted User name                                   */
/*                                                                           */
/* Input(s)           : pu1Name - User name of new record                    */
/*                      u2NameLen - length of User name                      */
/*                      pu1Passwd - Password of the User                     */
/*                      u2PasswdLen - length of Password                     */
/*                      u1AuthType - Authentication protocol for the user    */
/*                      pu1PortList - set of ports as User-access-list       */
/*                      bDenyUser - boolean to 'allow' or 'deny' the User    */
/*                                  on the pu1PortList set of ports          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacServInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacServInfo                                        */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacServInitUserRecord (UINT1 *pu1Name,
                        UINT2 u2NameLen,
                        UINT1 *pu1Passwd,
                        UINT2 u2PasswdLen,
                        UINT1 u1AuthType, UINT1 *pu1PortList, UINT1 u1DenyUser)
{
    tPnacServUserInfo  *pUserRecord = NULL;
    UINT2               u2Len = 0;

    if (PNAC_ALLOCATE_SERVUSER_MEMBLK (pUserRecord) == NULL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " AS: Allocate User Info Record Failed \n");
        return PNAC_FAILURE;
    }
    PNAC_MEMSET (pUserRecord, PNAC_INIT_VAL, sizeof (tPnacServUserInfo));
    pUserRecord->bDenyUser = (BOOL1) u1DenyUser;
    PNAC_MEMCPY (pUserRecord->au1PortList, pu1PortList, PNAC_PORT_LIST_SIZE);

    u2Len = (u2NameLen < PNAC_MAX_LEN_USERNAME) ? u2NameLen :
        PNAC_MAX_LEN_USERNAME;
    pUserRecord->u2NameLen = u2Len;
    PNAC_STRNCPY (pUserRecord->au1Name, pu1Name, u2Len);

    u2Len = (u2PasswdLen < PNAC_MAX_LEN_PASSWD) ? u2PasswdLen :
        PNAC_MAX_LEN_PASSWD;
    pUserRecord->u2PasswdLen = u2Len;
    PNAC_MEMCPY (pUserRecord->au1Passwd, pu1Passwd, u2Len);
    pUserRecord->au1Passwd[u2Len] = '\0';
    FsUtlEncryptPasswd ((CHR1 *) pUserRecord->au1Passwd);
    pUserRecord->u1AuthType = u1AuthType;
    pUserRecord->u1RowStatus = PNAC_ACTIVE;

    PNAC_SLL_ADD ((&PNAC_SERV_USER_LIST), &(pUserRecord->nextNode));
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacServGetUserNode                                  */
/*                                                                           */
/* Description        : This function gets the User record for the           */
/*                      inputted User name                                   */
/*                                                                           */
/* Input(s)           : pu1Name - User name of record                        */
/*                                                                           */
/* Output(s)          : ppUserNode - double pointer to User record           */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacServInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacServInfo                                        */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacServGetUserNode (UINT1 *pu1Name, tPnacServUserInfo ** ppUserNode)
{
    tPnacSLLNode       *pSLLNode = NULL;
    tPnacServUserInfo  *pUserNode = NULL;

    pSLLNode = PNAC_SLL_FIRST (&PNAC_SERV_USER_LIST);
    while (pSLLNode != NULL)
    {
        pUserNode = (tPnacServUserInfo *) pSLLNode;
        if ((pUserNode->u2NameLen == PNAC_STRLEN (pu1Name))
            &&
            (PNAC_STRCMP (pUserNode->au1Name, pu1Name) == PNAC_NO_VAL)
            && pUserNode->u1RowStatus == PNAC_ACTIVE)
        {

            *ppUserNode = pUserNode;
            return PNAC_SUCCESS;
        }
        pSLLNode = PNAC_SLL_NEXT (&PNAC_SERV_USER_LIST, pSLLNode);
    }

    return PNAC_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PnacServGetCacheNode                                 */
/*                                                                           */
/* Description        : This function gets the Cache record for the          */
/*                      inputted User name                                   */
/*                                                                           */
/* Input(s)           : pu1Name - User name of record                        */
/*                      macAddr - MAC address of the User                    */
/*                      u2PortNum - Port number the User connected to in     */
/*                                  Authenticator                            */
/*                      u2SessId - Current Session Identifier of the User    */
/*                                                                           */
/* Output(s)          : ppCacheNode - double pointer to User cache record    */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacServInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacServInfo                                        */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacServGetCacheNode (UINT1 *pu1Name,
                      tMacAddr macAddr,
                      UINT2 u2PortNum,
                      UINT2 u2SessId, tPnacServAuthCache ** ppCacheNode)
{
    tPnacSLLNode       *pSLLNode = NULL;
    tPnacServAuthCache *pCacheNode = NULL;

    pSLLNode = PNAC_SLL_FIRST (&PNAC_SERV_CACHE_LIST);
    while (pSLLNode != NULL)
    {
        pCacheNode = (tPnacServAuthCache *) pSLLNode;
        if (pu1Name != NULL)
        {
            if ((pCacheNode->u2SessId == u2SessId)
                &&
                PNAC_COMPARE_MAC_ADDR (pCacheNode->macAddr, macAddr)
                &&
                (pCacheNode->u2RxdPort == u2PortNum)
                &&
                (pCacheNode->u2NameLen == PNAC_STRLEN (pu1Name))
                && (PNAC_STRCMP (pCacheNode->au1Name, pu1Name) == PNAC_NO_VAL))
            {

                *ppCacheNode = pCacheNode;
                return PNAC_SUCCESS;
            }
        }
        else
        {
            if ((pCacheNode->u2SessId == u2SessId)
                &&
                PNAC_COMPARE_MAC_ADDR (pCacheNode->macAddr, macAddr)
                && (pCacheNode->u2RxdPort == u2PortNum))
            {
                *ppCacheNode = pCacheNode;
                return PNAC_SUCCESS;
            }

        }
        pSLLNode = PNAC_SLL_NEXT (&PNAC_SERV_CACHE_LIST, pSLLNode);
    }

    return PNAC_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PnacServValidateEapPkt                               */
/*                                                                           */
/* Description        : This function validates the input Eap pkt            */
/*                                                                           */
/* Input(s)           : pu1EapPkt - pointer to EAP pkt                       */
/*                      u2EapPktLen - length of EAP pkt                      */
/*                                                                           */
/* Output(s)          : pEapMesgPtr - pointer to Eap Message descriptor      */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacServInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacServInfo                                        */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacServValidateEapPkt (UINT1 *pu1EapPkt, UINT2 u2EapPktLen,
                        tPnacEapDesc * pEapMesgPtr)
{
    UINT1              *pu1ReadPtr = NULL;
    UINT1               u1Val = 0;
    UINT2               u2Val = 0;
    UINT2               u2Len = 0;

    /* Copy the pointer locally and use it */
    pu1ReadPtr = pu1EapPkt;

    /* Length can never be less than minimum EAP Header size */
    if (u2EapPktLen < PNAC_EAP_HDR_SIZE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  " AS: Error: EAP Packet Length Less than minumum \n");
        return PNAC_FAILURE;
    }

    PNAC_GET_1BYTE (u1Val, pu1ReadPtr);
    pEapMesgPtr->u1Code = u1Val;
    PNAC_GET_1BYTE (u1Val, pu1ReadPtr);
    pEapMesgPtr->u1Id = u1Val;
    PNAC_GET_2BYTE (u2Val, pu1ReadPtr);
    pEapMesgPtr->u2EapLen = u2Val;

    /* Verify the passed Length and Compare with the length in the packet */
    if (u2EapPktLen != pEapMesgPtr->u2EapLen)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  " AS: Error: Invalid EAP Packet Length\n");
        return PNAC_FAILURE;
    }

    u2Len = (UINT2) (pEapMesgPtr->u2EapLen - PNAC_EAP_HDR_SIZE);

    switch (pEapMesgPtr->u1Code)
    {
        case PNAC_EAP_CODE_RESP:
         /******************************************/
            /* Get the EAP type field and continue    */
            /* as per that.                           */
         /******************************************/
            if (u2Len == PNAC_NO_VAL)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                          " AS: Error: Invalid EAP Response Length\n");
                return PNAC_FAILURE;
            }
            PNAC_GET_1BYTE (u1Val, pu1ReadPtr);
            pEapMesgPtr->u1Type = u1Val;
            u2Len--;
            switch (u1Val)
            {
                case PNAC_EAP_TYPE_IDENTITY:
                    if (u2Len > PNAC_NO_VAL)
                    {
                        pEapMesgPtr->identity.u2Len = u2Len;
                        pEapMesgPtr->identity.pu1Str = pu1ReadPtr;
                    }
                    else
                    {
                        pEapMesgPtr->identity.u2Len = PNAC_NO_VAL;
                    }
                    break;

                case PNAC_EAP_TYPE_NAK:
                    if (u2Len != 1)
                    {
                        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                                  " AS: Error: Invalid EAP Response Nak Length\n");
                        return PNAC_FAILURE;
                    }
                    pEapMesgPtr->nak.u1AuthDesired = *pu1ReadPtr;

                    break;
                case PNAC_EAP_TYPE_MD5:
                    if (u2Len < 1)
                    {
                        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                                  " AS: Error: Invalid EAP MD5 Response Length\n");
                        return PNAC_FAILURE;
                    }
                    PNAC_GET_1BYTE (u1Val, pu1ReadPtr);
                    u2Len--;
                    if (u2Len < u1Val)
                    {
                        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                                  " AS: Error: EAP MD5 Response Value Size Mismatch\n");
                        return PNAC_FAILURE;
                    }
                    pEapMesgPtr->mesgDigest.u1ValSize = u1Val;
                    pEapMesgPtr->mesgDigest.pu1ValPtr = pu1ReadPtr;
                    pu1ReadPtr += u1Val;
                    u2Len = (UINT2) (u2Len - u1Val);
                    pEapMesgPtr->mesgDigest.u2NameLen = u2Len;
                    pEapMesgPtr->mesgDigest.pu1Name = pu1ReadPtr;
                    break;
                case PNAC_EAP_TYPE_TLS:
                    /* In case of EAP TLS no need to validate the EAP data packet,
                     * pass the packet received.
                     */
                    break;
                default:
                    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                              " AS: Error: Invalid EAP Response Type\n");
                    return PNAC_FAILURE;
                    break;

            }
            break;
        case PNAC_EAP_CODE_REQ:
            if (u2Len == PNAC_NO_VAL)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                          " AS: Error: Invalid EAP Request Length\n");
                return PNAC_FAILURE;
            }
            PNAC_GET_1BYTE (u1Val, pu1ReadPtr);
            pEapMesgPtr->u1Type = u1Val;
            u2Len--;
            switch (u1Val)
            {
                case PNAC_EAP_TYPE_IDENTITY:
                    if (u2Len > PNAC_NO_VAL)
                    {
                        pEapMesgPtr->identity.u2Len = u2Len;
                        pEapMesgPtr->identity.pu1Str = pu1ReadPtr;
                    }
                    else
                    {
                        pEapMesgPtr->identity.u2Len = PNAC_NO_VAL;
                    }
                    break;
                case PNAC_EAP_TYPE_NOTIFICATION:
                    if (u2Len > PNAC_NO_VAL)
                    {
                        pEapMesgPtr->notify.u2Len = u2Len;
                        pEapMesgPtr->notify.pu1Str = pu1ReadPtr;
                    }
                    else
                    {
                        pEapMesgPtr->notify.u2Len = PNAC_NO_VAL;
                    }

                    break;
                case PNAC_EAP_TYPE_NAK:
                    if (u2Len != 1)
                    {
                        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                                  " AS: Error: Invalid EAP Request Nak Length\n");
                        return PNAC_FAILURE;
                    }
                    pEapMesgPtr->nak.u1AuthDesired = *pu1ReadPtr;

                    break;
                case PNAC_EAP_TYPE_MD5:
                    if (u2Len < 1)
                    {
                        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                                  " AS: Error: Invalid EAP MD5 Request Length\n");
                        return PNAC_FAILURE;
                    }
                    PNAC_GET_1BYTE (u1Val, pu1ReadPtr);
                    u2Len--;
                    if (u2Len < u1Val)
                    {
                        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                                  " AS: Error: EAP MD5 Request Value Size Mismatch\n");
                        return PNAC_FAILURE;
                    }
                    pEapMesgPtr->mesgDigest.u1ValSize = u1Val;
                    pEapMesgPtr->mesgDigest.pu1ValPtr = pu1ReadPtr;
                    pu1ReadPtr += u1Val;
                    u2Len = (UINT2) (u2Len - u1Val);
                    if (u2Len == PNAC_NO_VAL)
                    {
                        return PNAC_FAILURE;
                    }
                    else
                    {
                        pEapMesgPtr->mesgDigest.u2NameLen = u2Len;
                        pEapMesgPtr->mesgDigest.pu1Name = pu1ReadPtr;
                    }

                    break;
                case PNAC_EAP_TYPE_TLS:
                    /* In case of EAP TLS no need to validate the EAP data packet,
                     * pass the packet received.
                     */
                    break;
                default:
                    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                              " AS: Error: Invalid EAP Request Type\n");
                    return PNAC_FAILURE;
                    break;

            }

            break;
        case PNAC_EAP_CODE_SUCCESS:
        case PNAC_EAP_CODE_FAILURE:
            if (u2Len != PNAC_NO_VAL)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                          " AS: Error: Invalid EAP Success/Failure Packet Length\n");
                return PNAC_FAILURE;
            }
            break;
        default:
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      " AS: Error: Invalid EAP Code in Packet\n");
            return PNAC_FAILURE;
            break;
    }
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacServHandleEapPkt                                 */
/*                                                                           */
/* Description        : This function handles the Eap pkt & sends responses  */
/*                                                                           */
/* Input(s)           : pAuthData - pointer to Server's input data           */
/*                      pCallBackFnPtr - Call back function pointer          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacServInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacServInfo                                        */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacServHandleEapPkt (tPnacAsIfSendData * pAuthData)
{
    tPnacServUserInfo  *pUserNode = NULL;
    tPnacServAuthCache *pCacheNode = NULL;
    tPnacAsIfRecvData  *pAuthReq = NULL;
    tPnacEapDesc        eapDesc;
    tPnacEapDesc        txEapDesc;
    UINT2               u2Len;
    INT4                i4RetVal = PNAC_FAILURE;
    UINT1               au1UserName[PNAC_MAX_LEN_USERNAME + 1];

    if (pAuthData->u2EapPktLength > PNAC_MAX_EAP_PKT_SIZE)
    {
	PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
		" AS: Oversized Eap pkt\n");
	return PNAC_FAILURE;
    }

    PNAC_MEMSET (&eapDesc, PNAC_INIT_VAL, sizeof (tPnacEapDesc));
    if (PnacServValidateEapPkt (pAuthData->au1EapPkt, pAuthData->u2EapPktLength,
		&eapDesc) != PNAC_SUCCESS)
    {
	PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
		" AS: Eap Packet Validation Failed\n");
	return PNAC_SUCCESS;
    }

    if (eapDesc.u1Code == PNAC_EAP_CODE_RESP)
    {
	if (PNAC_ALLOCATE_IF_RECV_DATA_MEMBLK (pAuthReq) == NULL)
	{
	    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
		    "Unable to allocate memory. \r\n");
	    return PNAC_FAILURE;
	}
	PNAC_MEMSET (pAuthReq, PNAC_INIT_VAL, sizeof (tPnacAsIfRecvData));

	switch (eapDesc.u1Type)
	{

	    case PNAC_EAP_TYPE_IDENTITY:
		u2Len = eapDesc.identity.u2Len;
		if (u2Len > PNAC_MAX_LEN_USERNAME)
		{
		    /* User Name With this length is not supported by
		     * this system.
		     */
		    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
			    " AS: Identity Length Too Large\n");
		    i4RetVal = PNAC_FAILURE;
		    break;
		}
#ifdef WPS_WANTED
		if (u2Len ==  WSC_ID_ENROLLEE_LEN)
		{
		    if (STRCMP(eapDesc.identity.pu1Str,WSC_ID_ENROLLEE) == 0)
		    {
			wpsRecvIdentity(pAuthData->u2NasPortNumber,pAuthData->macAddr);	
		    }
		}
#endif
		PNAC_MEMSET (au1UserName, PNAC_INIT_VAL,
			(PNAC_MAX_LEN_USERNAME + 1));
		PNAC_MEMCPY (au1UserName, eapDesc.identity.pu1Str, u2Len);
		au1UserName[u2Len] = '\0';

		PNAC_MEMSET (pAuthReq, PNAC_INIT_VAL,
			sizeof (tPnacAsIfRecvData));

		if (PNAC_IS_LOCAL_AS ())
		{
		    if (PnacServGetUserNode (au1UserName, &pUserNode) !=
			    PNAC_SUCCESS)
		    {
			/*********************************************************/
			/* Silently discard the request since the user name is   */
			/* not valid.                                            */
			/*********************************************************/
			PNAC_TRC (PNAC_ALL_FAILURE_TRC |
				PNAC_CONTROL_PATH_TRC,
				" AS: User name Unknown\n");
			i4RetVal = PNAC_FAILURE;
			break;
		    }
		    /************************************************/
		    /* Validate and Process the request since       */
		    /* the user name is valid                       */
		    /************************************************/
		    if (PnacServValidateResponseId (pAuthData, pUserNode) !=
			    PNAC_SUCCESS)
		    {
			PNAC_TRC (PNAC_ALL_FAILURE_TRC |
				PNAC_CONTROL_PATH_TRC,
				" AS: Validate Response Idenity Failed\n");
			i4RetVal = PNAC_FAILURE;
			break;
		    }
		}
		if (PnacServProcessResponseId
			(pAuthData, au1UserName, u2Len, (UINT2) eapDesc.u1Id,
			 pAuthReq) != PNAC_SUCCESS)
		{
		    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
			    " AS: Process Response Idenity Failed\n");
		    i4RetVal = PNAC_FAILURE;
		    break;
		}
		/* Now call the call back routine send the Authentication
		 * Request structure (authReq).
		 */
		PnacAsIfRecvFromServer (pAuthReq);
		i4RetVal = PNAC_SUCCESS;
		break;

	    case PNAC_EAP_TYPE_MD5:
		PNAC_MEMSET (au1UserName, PNAC_INIT_VAL,
			(PNAC_MAX_LEN_USERNAME + 1));
		PNAC_MEMCPY (au1UserName, pAuthData->au1SuppUserName,
			pAuthData->u2SuppUserNameLen);

		if (PnacServGetCacheNode
			(au1UserName, pAuthData->macAddr,
			 pAuthData->u2NasPortNumber, eapDesc.u1Id,
			 &pCacheNode) != PNAC_SUCCESS)
		{
		    /*********************************************************/
		    /* Silently discard the request since the user name is   */
		    /* not valid.                                            */
		    /*********************************************************/
		    PNAC_TRC_ARG1 (PNAC_ALL_FAILURE_TRC |
			    PNAC_CONTROL_PATH_TRC,
			    " AS: This user didnt Start an Authentication Session: %s \n",
			    au1UserName);
		    i4RetVal = PNAC_FAILURE;
		    break;
		}
		/*********************************************************/
		/* Nothing more to Validate and hence                    */
		/* Process the request since the Cache Node is Available */
		/*********************************************************/
		if (PNAC_IS_LOCAL_AS ())
		{
		    PNAC_MEMSET (pAuthReq, PNAC_INIT_VAL,
			    sizeof (tPnacAsIfRecvData));
		    if (PnacServProcessResponseMd5
			    (pAuthData, pCacheNode, &eapDesc,
			     pAuthReq) != PNAC_SUCCESS)
		    {
			PNAC_TRC (PNAC_ALL_FAILURE_TRC |
				PNAC_CONTROL_PATH_TRC,
				" AS: Process Response MD5 Failed\n");
			i4RetVal = PNAC_FAILURE;
			break;
		    }
		    /* Now call the call back routine send the Authentication
		     * Request structure (authReq).
		     */
		    PnacAsIfRecvFromServer (pAuthReq);
		}
		else
		{
		    if (PnacTacacsplusProcessResponseMd5
			    (pAuthData, pCacheNode, &eapDesc) != PNAC_SUCCESS)
		    {
			PNAC_TRC (PNAC_ALL_FAILURE_TRC |
				PNAC_CONTROL_PATH_TRC,
				" AS: Process Response MD5 Failed\n");
			i4RetVal = PNAC_FAILURE;
			break;
		    }
		}
		i4RetVal = PNAC_SUCCESS;
		break;

	    case PNAC_EAP_TYPE_NAK:
		if (eapDesc.nak.u1AuthDesired != PNAC_EAP_TYPE_MD5)
		{
		    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
			    " AS: Nak Frame Received\n");
		    /* Currently LAS support only MD5,
		     * other Types of auth mechanisms are not supported
		     * send EAP FAILURE response.
		     */
		    PNAC_MEMSET (&txEapDesc, PNAC_INIT_VAL,
			    sizeof (tPnacEapDesc));
		    txEapDesc.u1Code = PNAC_EAP_CODE_FAILURE;
		    txEapDesc.u1Id = (UINT1) (eapDesc.u1Id + 1);
		    pAuthReq->u2NasPortNumber = pAuthData->u2NasPortNumber;
		    PNAC_MEMCPY (pAuthReq->macAddr, pAuthData->macAddr,
			    PNAC_MAC_ADDR_SIZE);
		    if (PnacServGenEap (&txEapDesc, pAuthReq->au1EapPkt,
				&(pAuthReq->u2EapPktLength)) !=
			    PNAC_SUCCESS)
		    {
			PNAC_TRC (PNAC_CONTROL_PATH_TRC |
				PNAC_ALL_FAILURE_TRC,
				" AS: EAP Packet Generation Failed \n");
			i4RetVal = PNAC_FAILURE;
			break;
		    }
		    PnacAsIfRecvFromServer (pAuthReq);
		    i4RetVal = PNAC_SUCCESS;
		}
		else
		{
		    i4RetVal = PNAC_FAILURE;
		}
		break;
	    default:
		/* Other Types of authorization mechanisms are not supported
		 * in this version.
		 */
		PNAC_MEMSET (&txEapDesc, PNAC_INIT_VAL, sizeof (tPnacEapDesc));
		txEapDesc.u1Code = PNAC_EAP_CODE_FAILURE;
		txEapDesc.u1Id = (UINT1) (eapDesc.u1Id + 1);
		pAuthReq->u2NasPortNumber = pAuthData->u2NasPortNumber;
		PNAC_MEMCPY (pAuthReq->macAddr, pAuthData->macAddr,
			PNAC_MAC_ADDR_SIZE);
		if (PnacServGenEap (&txEapDesc, pAuthReq->au1EapPkt,
			    &(pAuthReq->u2EapPktLength)) !=
			PNAC_SUCCESS)
		{
		    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
			    " AS: EAP Packet Generation Failed \n");
		    i4RetVal = PNAC_FAILURE;
		    break;
		}

		PnacAsIfRecvFromServer (pAuthReq);

		i4RetVal = PNAC_SUCCESS;
		break;

	}                        /*switch u1Type end */
	if (PNAC_RELEASE_IF_RECV_DATA_MEMBLK (pAuthReq) == MEM_FAILURE)
	{
	    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
		    "Failed to release memory. \r\n");
	    return PNAC_FAILURE;
	}
	return i4RetVal;
    }
    else
    {
	return PNAC_FAILURE;
    }
}                                /* PnacServHandleEapPkt() */

/*****************************************************************************/
/* Function Name      : PnacServValidateResponseId                           */
/*                                                                           */
/* Description        : This function validates the Eap response Id pkt      */
/*                                                                           */
/* Input(s)           : pAuthData - pointer to Server's input data           */
/*                      pUserNode - User record                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacServInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacServInfo                                        */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacServValidateResponseId (tPnacAsIfSendData * pAuthData,
                            tPnacServUserInfo * pUserNode)
{
    if (pUserNode->u1AuthType != PNAC_EAP_TYPE_MD5)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  " Access With Authentication Mechanism other than MD5 "
                  "not supported at Present\n");
        return PNAC_FAILURE;
    }
    if (PnacServValidatePort (pAuthData->u2NasPortNumber,
                              pUserNode->au1PortList) != PNAC_SUCCESS)
    {
        /* The Port Number is not present in the port List */
        if (pUserNode->bDenyUser == PNAC_FALSE)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      " User Not Allowed on this Port\n");
            return PNAC_FAILURE;
        }
    }
    else
    {
        /* The Port Number is present in the port List */
        if (pUserNode->bDenyUser == PNAC_TRUE)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      " User Access Denied on this Port\n");
            return PNAC_FAILURE;
        }
    }
    /* User is valid on the Port */
    PNAC_DEBUG (PNAC_TRC_ARG2 (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                               " User : %s Access Permitted on this Port (%u) ...\n",
                               pUserNode->au1Name, pAuthData->u2NasPortNumber);
        );
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacServProcessResponseId                            */
/*                                                                           */
/* Description        : This function processes the Eap Response Id pkt      */
/*                      & sends responses                                    */
/*                                                                           */
/* Input(s)           : pAuthData - pointer to Server's input data           */
/*                      pu1Name    - User Name                               */
/*                      u2NameLen  - Length of User Name                     */
/*                      u2SessId - Session Id received by Server             */
/*                      pAuthReply - pointer to Server's transmit data       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacServInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacServInfo                                        */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacServProcessResponseId (tPnacAsIfSendData * pAuthData,
                           UINT1 *pu1Name, UINT2 u2NameLen,
                           UINT2 u2SessId, tPnacAsIfRecvData * pAuthReply)
{
    tPnacServAuthCache *pCacheNode = NULL;
    tPnacEapDesc        txEapDesc;
    UINT2               u2NewSessId = 0;

    if (u2SessId < PNAC_MAX_SESS_ID)
    {
        u2NewSessId = (UINT2) (u2SessId + 1);
    }
    else
    {
        /* If the session id reaches max value ie 255 we need to start from
           0 again */
        u2NewSessId = 0;
    }
    if (PnacServGetCacheNode (pu1Name, pAuthData->macAddr,
                              pAuthData->u2NasPortNumber,
                              u2SessId, &pCacheNode) != PNAC_SUCCESS)
    {
        /* Node not there in the cache list */
        if (PNAC_ALLOCATE_SERVCACHE_MEMBLK (pCacheNode) == NULL)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " AS: Allocate Cache Node Failed \n");
            return PNAC_FAILURE;
        }
        PNAC_MEMSET (pCacheNode, PNAC_INIT_VAL, sizeof (tPnacServAuthCache));

        PNAC_SLL_INIT_NODE (&(pCacheNode->nextNode));
        PNAC_MEMCPY (pCacheNode->au1Name, pu1Name, u2NameLen);
        pCacheNode->u2NameLen = u2NameLen;
        pCacheNode->u1AuthType = PNAC_EAP_TYPE_MD5;
        pCacheNode->u2SessId = u2NewSessId;
        pCacheNode->u2RxdPort = pAuthData->u2NasPortNumber;
        PNAC_MEMCPY (pCacheNode->macAddr, pAuthData->macAddr,
                     PNAC_MAC_ADDR_SIZE);

        /* Now Generate the Challenge and copy that to Cache node.
         * Generate the EAP response frame for the Request Identity.
         */
        PnacServGenerateMd5Chal (pCacheNode->au1Chal, &(pCacheNode->u2ChalLen));

        PNAC_SLL_ADD (&PNAC_SERV_CACHE_LIST, &(pCacheNode->nextNode));
    }
    else
    {
        /* Node is present in the cache list already 
         * so this is a duplicate request.
         * Now Generate A new Challenge and copy that to Cache node.
         * and generate the EAP response frame for the request identity.
         */
        PNAC_MEMSET (pCacheNode->au1Chal, PNAC_INIT_VAL,
                     sizeof (pCacheNode->au1Chal));
        pCacheNode->u2ChalLen = PNAC_INIT_VAL;
        PnacServGenerateMd5Chal (pCacheNode->au1Chal, &(pCacheNode->u2ChalLen));
        pCacheNode->u2SessId = u2NewSessId;
    }
    /* Start a Timer */
    if (pCacheNode->pTmrNode != NULL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " AS: Error: Cache Timer already Running... anyway continuing \n");
        if (PnacTmrStopTmr (pCacheNode, PNAC_SERVERCACHE_TIMER) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " AS: Stop Cache Timer Failed... anyway continuing..\n");
        }
    }
    if (PnacTmrStartTmr (pCacheNode, PNAC_SERVERCACHE_TIMER,
                         PNAC_SERV_CACHE_DURATION) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " AS: Start Cache Timer Failed ... Aborting further Processing \n");
        return PNAC_FAILURE;
    }
    pAuthReply->u4SessionTimeout = PNAC_INIT_VAL;
    pAuthReply->u2NasPortNumber = pAuthData->u2NasPortNumber;

    PNAC_MEMSET (&txEapDesc, PNAC_INIT_VAL, sizeof (tPnacEapDesc));
    txEapDesc.u1Code = PNAC_EAP_CODE_REQ;
    txEapDesc.u1Id = (UINT1) (u2NewSessId);
    PNAC_DEBUG (PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                               " AS: Incrementing Identifier to %u \n",
                               txEapDesc.u1Id););
    txEapDesc.u1Type = PNAC_EAP_TYPE_MD5;
    txEapDesc.mesgDigest.u1ValSize = (UINT1) pCacheNode->u2ChalLen;
    txEapDesc.mesgDigest.pu1ValPtr = pCacheNode->au1Chal;
    txEapDesc.mesgDigest.u2NameLen = gPnacSystemInfo.u2NasIdLen;
    txEapDesc.mesgDigest.pu1Name = gPnacSystemInfo.au1NasId;
    PNAC_MEMCPY (pAuthReply->macAddr, pAuthData->macAddr, PNAC_MAC_ADDR_SIZE);

    if (PnacServGenEap (&txEapDesc, pAuthReply->au1EapPkt,
                        &(pAuthReply->u2EapPktLength)) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " AS: Server Generate EAP Failed... Aborting further Processing \n");
        return PNAC_FAILURE;
    }
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacServGenerateMd5Chal                              */
/*                                                                           */
/* Description        : This function generates Challenge octets value       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1Buf - pointer to Buffer containing Challenge value*/
/*                      pu2Len - pointer to Buffer length                    */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacServInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacServInfo                                        */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacServGenerateMd5Chal (UINT1 *pu1Buf, UINT2 *pu2Len)
{
    UINT1               u1Val1 = PNAC_INIT_VAL;
    UINT1               u1Val2 = PNAC_INIT_VAL;
    UINT2               u2Cnt = PNAC_INIT_VAL;

    for (u2Cnt = PNAC_INIT_VAL; u2Cnt < PNAC_AS_MAX_CHAL_SIZE; u2Cnt++)
    {
        u1Val1 = (UINT1) (1 + (UINT1) (255.0 * rand () / (RAND_MAX + 1.0)));
        u1Val2 = (UINT1) (1 + (UINT1) (255.0 * rand () / (RAND_MAX + 1.0)));
        *pu1Buf++ = (UINT1) ((u1Val1 + u1Val2) / 2);
    }
    *pu2Len = (UINT2) PNAC_AS_MAX_CHAL_SIZE;
    return;
}

/*****************************************************************************/
/* Function Name      : PnacServValidateChallenge                            */
/*                                                                           */
/* Description        : This function validates the Eap challenge responses  */
/*                                                                           */
/* Input(s)           : pu1Chal - pointer to Challenge response              */
/*                      u2ChalLen - length of Challenge response             */
/*                      pu1Passwd - pointer to User password                 */
/*                      u2PasswdLen - length of User password                */
/*                      pEapDesc - descriptor of received EAP challenge      */
/*                                 response pkt                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacServInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacServInfo                                        */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacServValidateChallenge (UINT1 *pu1Chal,
                           UINT2 u2ChalLen,
                           UINT1 *pu1Passwd,
                           UINT2 u2PasswdLen, tPnacEapDesc * pEapDesc)
{
    UINT1               au1Buf[PNAC_AS_SESSID_SIZE + PNAC_MAX_LEN_PASSWD
                               + PNAC_AS_MAX_CHAL_SIZE];
    UINT1              *pu1Ptr = NULL;
    UINT1               au1Digest[PNAC_MD5_DIGEST_SIZE];
    UINT1               au1Password[PNAC_MAX_LEN_PASSWD + 1];
    UINT2               u2Len = 0;

    PNAC_MEMSET (au1Password, PNAC_INIT_VAL, (PNAC_MAX_LEN_PASSWD + 1));

    pu1Ptr = au1Buf;

    *pu1Ptr++ = pEapDesc->u1Id;
    u2Len = (u2PasswdLen < (PNAC_MAX_LEN_PASSWD)) ? u2PasswdLen :
        (PNAC_MAX_LEN_PASSWD);
    PNAC_MEMCPY (au1Password, pu1Passwd, u2Len);
    au1Password[u2Len] = '\0';
    FsUtlDecryptPasswd ((CHR1 *) au1Password);
    PNAC_MEMCPY (pu1Ptr, au1Password, u2PasswdLen);
    pu1Ptr += u2PasswdLen;
    PNAC_MEMCPY (pu1Ptr, pu1Chal, u2ChalLen);
    pu1Ptr += u2ChalLen;

    /* Compute The One Way hash Using MD5 */
    PNAC_GET_MD5_DIGEST (au1Buf, (UINT2) (pu1Ptr - au1Buf), au1Digest);

    if (PNAC_MEMCMP (au1Digest, pEapDesc->mesgDigest.pu1ValPtr,
                     PNAC_MD5_DIGEST_SIZE) != PNAC_NO_VAL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " AS: Invalid Secret From User... Aborting further Processing \n");
        return PNAC_FAILURE;
    }
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacServProcessResponseMd5                           */
/*                                                                           */
/* Description        : This function processes the Eap response challenge   */
/*                      pkt & sends responses                                */
/*                                                                           */
/* Input(s)           : pAuthData - pointer to Server's input data           */
/*                      pCacheNode - cache node for the User                 */
/*                      pEapDesc - descriptor of EAP challenge response pkt  */
/*                                                                           */
/* Output(s)          : pAuthReply - pointer to Server's transmit data       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacServInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacServInfo                                        */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacServProcessResponseMd5 (tPnacAsIfSendData * pAuthData,
                            tPnacServAuthCache * pCacheNode,
                            tPnacEapDesc * pEapDesc,
                            tPnacAsIfRecvData * pAuthReply)
{
    tPnacEapDesc        txEapDesc;
    tPnacServUserInfo  *pUserNode = NULL;
    tPnacSLLNode       *pNode = NULL;

    if (PnacServGetUserNode (pCacheNode->au1Name, &pUserNode) != PNAC_SUCCESS)
    {
        PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                       " AS: User Name Unknown : %s \n", pCacheNode->au1Name);
        return PNAC_FAILURE;
    }
    pAuthReply->u2NasPortNumber = pAuthData->u2NasPortNumber;

    PNAC_MEMSET (&txEapDesc, PNAC_INIT_VAL, sizeof (tPnacEapDesc));
    if (pEapDesc->mesgDigest.u1ValSize != PNAC_MD5_CHAL_RESP_SIZE)
    {
        txEapDesc.u1Code = PNAC_EAP_CODE_FAILURE;
        PNAC_TRC_ARG3 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                       " AS: Sending Failure for user: %s, Id: %d, Port: %d\n",
                       pCacheNode->au1Name, pEapDesc->u1Id,
                       pAuthData->u2NasPortNumber);
    }
    else if (PnacServValidateChallenge (pCacheNode->au1Chal,
                                        pCacheNode->u2ChalLen,
                                        pUserNode->au1Passwd,
                                        pUserNode->u2PasswdLen,
                                        pEapDesc) != PNAC_SUCCESS)
    {
        txEapDesc.u1Code = PNAC_EAP_CODE_FAILURE;
        PNAC_TRC_ARG3 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                       " AS: Sending Failure for user: %s, Id: %d, Port: %d\n",
                       pCacheNode->au1Name, pEapDesc->u1Id,
                       pAuthData->u2NasPortNumber);
    }
    else
    {
        pAuthReply->u4SessionTimeout = pUserNode->u4AuthTimeout;
        txEapDesc.u1Code = PNAC_EAP_CODE_SUCCESS;
        PNAC_TRC_ARG3 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                       " AS: Sending Success for user: %s, Id: %d, Port: %d\n",
                       pCacheNode->au1Name, pEapDesc->u1Id,
                       pAuthData->u2NasPortNumber);
    }
    txEapDesc.u1Id = (UINT1) (pEapDesc->u1Id + 1);
    PNAC_DEBUG (PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                               " AS: Incrementing Identifier to %u \n",
                               txEapDesc.u1Id););

    if (PnacServGenEap (&txEapDesc, pAuthReply->au1EapPkt,
                        &(pAuthReply->u2EapPktLength)) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " AS: EAP Packet Generation Failed \n");
        return PNAC_FAILURE;
    }

    PNAC_MEMCPY (pAuthReply->macAddr, pAuthData->macAddr, PNAC_MAC_ADDR_SIZE);
    pNode = &pCacheNode->nextNode;
    if (pCacheNode->pTmrNode != NULL)
    {
        if (PnacTmrStopTmr (pCacheNode, PNAC_SERVERCACHE_TIMER) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " AS: Stop Cache Timer Failed... anyway continuing..\n");
        }
    }
    PNAC_SLL_DELETE (&PNAC_SERV_CACHE_LIST, pNode);
    PNAC_DEBUG (PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                               " SLL Cache Node deleted : %x\n", pNode);
        );
    if (PNAC_RELEASE_SERVCACHE_MEMBLK (pCacheNode) != PNAC_MEM_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " AS: Releasing Cache Node failed\n");
        return PNAC_FAILURE;
    }
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacServGenEap                                       */
/*                                                                           */
/* Description        : This function generates Eap pkt for transmission     */
/*                                                                           */
/* Input(s)           : pTxEapDesc - descriptor for Server's transmit data   */
/*                                                                           */
/* Output(s)          : pu1EapPkt - pointer to Buffer containing generated   */
/*                                  Eap pkt                                  */
/*                      pu2PktLen - pointer to length of buffer              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacServInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacServInfo                                        */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacServGenEap (tPnacEapDesc * pTxEapDesc, UINT1 *pu1EapPkt, UINT2 *pu2PktLen)
{
    UINT1              *pu1WritePtr = NULL;
    UINT1               u1Val;
    UINT2               u2Val;

    pu1WritePtr = pu1EapPkt;

    u1Val = pTxEapDesc->u1Code;
    PNAC_PUT_1BYTE (pu1WritePtr, u1Val);
    u1Val = pTxEapDesc->u1Id;
    PNAC_PUT_1BYTE (pu1WritePtr, u1Val);

    switch (pTxEapDesc->u1Code)
    {

        case PNAC_EAP_CODE_SUCCESS:

            u2Val = PNAC_EAP_HDR_SIZE;
            PNAC_PUT_2BYTE (pu1WritePtr, u2Val);
            /* Write the total EAP Packet length into the Transmit
             * EAP descriptor's Length Field.
             */
            *pu2PktLen = (UINT2) PNAC_EAP_HDR_SIZE;
            PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC,
                           " AS: Sending Success for Id %d\n",
                           pTxEapDesc->u1Id);
            break;

        case PNAC_EAP_CODE_FAILURE:

            u2Val = PNAC_EAP_HDR_SIZE;
            PNAC_PUT_2BYTE (pu1WritePtr, u2Val);
            /* Write the total EAP Packet length into the Transmit
             * EAP descriptor's Length Field.
             */
            *pu2PktLen = (UINT2) PNAC_EAP_HDR_SIZE;
            PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC,
                           " AS: Sending Failure for Id %d\n",
                           pTxEapDesc->u1Id);
            break;

        case PNAC_EAP_CODE_REQ:

            switch (pTxEapDesc->u1Type)
            {
                case PNAC_EAP_TYPE_MD5:
                    u2Val = (UINT2) (PNAC_EAP_HDR_SIZE + PNAC_EAP_TYPE_SIZE +
                                     PNAC_EAP_VALUE_SIZE
                                     + pTxEapDesc->mesgDigest.u1ValSize
                                     + pTxEapDesc->mesgDigest.u2NameLen);
                    /* Write the total EAP Packet length into the Transmit
                     * EAP descriptor's Length Field.
                     */
                    *pu2PktLen = u2Val;
                    PNAC_PUT_2BYTE (pu1WritePtr, u2Val);
                    u1Val = pTxEapDesc->u1Type;
                    PNAC_PUT_1BYTE (pu1WritePtr, u1Val);
                    u1Val = pTxEapDesc->mesgDigest.u1ValSize;
                    PNAC_PUT_1BYTE (pu1WritePtr, u1Val);
                    PNAC_MEMCPY (pu1WritePtr, pTxEapDesc->mesgDigest.pu1ValPtr,
                                 pTxEapDesc->mesgDigest.u1ValSize);
                    pu1WritePtr += pTxEapDesc->mesgDigest.u1ValSize;
                    PNAC_MEMCPY (pu1WritePtr, pTxEapDesc->mesgDigest.pu1Name,
                                 pTxEapDesc->mesgDigest.u2NameLen);
                    pu1WritePtr += pTxEapDesc->mesgDigest.u2NameLen;

                    PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC,
                                   " AS: Sending MD5 Challenge for Id %d\n",
                                   pTxEapDesc->u1Id);

                    break;
                case PNAC_EAP_TYPE_NOTIFICATION:
                    u2Val = (UINT2) (PNAC_EAP_HDR_SIZE + PNAC_EAP_TYPE_SIZE +
                                     pTxEapDesc->notify.u2Len);
                    /* Write the total EAP Packet length into the Transmit
                     * EAP descriptor's Length Field.
                     */
                    *pu2PktLen = u2Val;
                    PNAC_PUT_2BYTE (pu1WritePtr, u2Val);
                    u1Val = pTxEapDesc->u1Type;
                    PNAC_PUT_1BYTE (pu1WritePtr, u1Val);
                    PNAC_MEMCPY (pu1WritePtr, pTxEapDesc->notify.pu1Str,
                                 pTxEapDesc->notify.u2Len);
                    PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC,
                                   " AS: Sending Notification for Id %d\n",
                                   pTxEapDesc->u1Id);
                    break;
                default:
                    PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC,
                                   " AS: Error: Unknown EAP Type %d\n",
                                   pTxEapDesc->u1Type);
                    return PNAC_FAILURE;
                    break;
            }
            break;

        default:
            PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC,
                           " AS: Error: Unknown EAP Code %d\n",
                           pTxEapDesc->u1Code);
            return PNAC_FAILURE;
            break;

    }                            /*switch end */

    return PNAC_SUCCESS;

}                                /* GenEap end */

/*****************************************************************************/
/* Function Name      : PnacServCacheTmrExpired                              */
/*                                                                           */
/* Description        : This function handles Cache timer expiry             */
/*                                                                           */
/* Input(s)           : pCacheNode - pointer to cache node for the User      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacServInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacServInfo                                        */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacServCacheTmrExpired (tPnacServAuthCache * pCacheNode)
{
    PNAC_SLL_DELETE (&(PNAC_SERV_CACHE_LIST), &pCacheNode->nextNode);
    PNAC_DEBUG (PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                               " SLL Cache Node deleted : %x\n", pCacheNode);
        );
    if (PNAC_RELEASE_SERVCACHE_MEMBLK (pCacheNode) != PNAC_MEM_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " AS: Releasing Cache Node failed\n");
        return PNAC_FAILURE;
    }
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacServValidatePort                                 */
/*                                                                           */
/* Description        : This function verifies the PortList for the presence */
/*                      of bit corresponding to the inputted port            */
/*                                                                           */
/* Input(s)           : u2PortNum - port which is to be verified             */
/*                      pu1PortList - pointer to PortList                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacServInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacServInfo                                        */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacServValidatePort (UINT2 u2PortNum, UINT1 *pu1PortList)
{
    UINT4               u4Index = 0;
    UINT1               u1Power = 0;
    UINT1               u1Mask = 0x01;

    PNAC_GET_INDEX_AND_POWER (u2PortNum, u4Index, u1Power);
    if (pu1PortList[u4Index] & (u1Mask << u1Power))
        return PNAC_SUCCESS;
    return PNAC_FAILURE;

}

#endif /* _PNACSRV_C */
