/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/*                                                                           */
/* $Id: pnacsupp.c,v 1.16 2015/02/10 10:10:30 siva Exp $                     */
/*                                                                           */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : pnacsupp.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : Supplicant Module                              */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Mar 2002                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains Supplicant sub module       */
/*                            functions                                      */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 MAR 2002 / BridgeTeam   Initial Create.                     */
/*---------------------------------------------------------------------------*/

#include "pnachdrs.h"

/*****************************************************************************/
/* Function Name      : PnacInitSupplicant                                   */
/*                                                                           */
/* Description        : This function initialises the Supplicant sub module  */
/*                                                                           */
/* Input(s)           : u2PortNum - Number of the port for which the
 *                                  supplicant is to be initialised.         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.aPnacPaePortInfo                     */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
PnacInitSupplicant (UINT2 u2PortNum)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacSuppSessionNode *pSuppSessNode = NULL;
    tPnacSuppConfigEntry *pSuppConfig = NULL;

    if (PNAC_ALLOCATE_SUPPSESSNODE_MEMBLK (pSuppSessNode) == NULL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                  PNAC_OS_RESOURCE_TRC,
                  "PnacInitSupplicant: Mem alloc failed\n");
        return PNAC_FAILURE;
    }

    PNAC_MEMSET (pSuppSessNode, PNAC_INIT_VAL, sizeof (tPnacSuppSessionNode));

    pPortInfo = &(PNAC_PORT_INFO (u2PortNum));

    /* Attach the allocated supplicant session node to the port entry */
    pPortInfo->pPortSuppSessionNode = pSuppSessNode;

    PnacSuppSessionNodeInit (pSuppSessNode, u2PortNum);
    /* Initialise the Supplicant Configuration Information */
    pSuppConfig = &(pPortInfo->suppConfigEntry);

    pSuppConfig->u4AuthPeriod = PNAC_AUTHPERIOD;
    pSuppConfig->u4HeldPeriod = PNAC_HELDPERIOD;
    pSuppConfig->u4StartPeriod = PNAC_STARTPERIOD;
    pSuppConfig->u4MaxStart = PNAC_MAXSTART;
    pSuppConfig->bKeyTxEnabled = PNAC_FALSE;

    if (PnacSuppStateMachine (PNAC_SSM_EV_INITIALIZE,
                              u2PortNum,
                              pSuppSessNode, NULL, PNAC_NO_VAL) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  "InitSupplicant: SuppStateMachine returned failure\n");
        return PNAC_FAILURE;
    }

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacSuppSessionNodeInit                              */
/*                                                                           */
/* Description        : This function initialises the Supplicant session     */
/*                      Node.                                                */
/*                                                                           */
/* Input(s)           : pSuppSessNode - Pointer to the supplicant session    */
/*                        node.                                              */
/*                                                                           */
/*                      u2PortNum - Port Number.                             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacSuppSessionNodeInit (tPnacSuppSessionNode * pSuppSessNode, UINT2 u2PortNum)
{
    tPnacSuppFsmInfo   *pSuppFsmInfo = NULL;
    tPnacSuppStatsEntry *pSuppStats = NULL;
    tPnacKeyInfo       *pKeyInfo = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;

    /* Initialise the supplicant state machine information */
    pSuppFsmInfo = &(pSuppSessNode->suppFsmInfo);
    pSuppFsmInfo->u4StartCount = PNAC_INIT_VAL;
    pSuppFsmInfo->u2SuppPaeState = PNAC_SSM_STATE_DISCONNECTED;
    pSuppFsmInfo->u2ReceivedId = PNAC_INIT_VAL;
    pSuppFsmInfo->u2PreviousId = PNAC_INIT_VAL;
    pSuppFsmInfo->u2ControlPortStatus = PNAC_PORTSTATUS_UNAUTHORIZED;

    if (PnacGetPortEntry (u2PortNum, &pPortInfo) == PNAC_SUCCESS)
    {
        if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl
            == PNAC_PORTCNTRL_FORCEAUTHORIZED)
        {
            pSuppFsmInfo->u2ControlPortStatus = PNAC_PORTSTATUS_AUTHORIZED;
            if (pPortInfo->u1EntryStatus == PNAC_PORT_OPER_UP)
            {
                /*If Entrystatus is Up then only give Authorized indication up
                 * to L2Iwf*/

                if (PnacCheckIfPortStatusChanged (u2PortNum) == PNAC_SUCCESS)
                {
                    PnacL2IwfSetPortPnacAuthStatus (u2PortNum,
                                                    (UINT2) gPnacSystemInfo.
                                                    aPnacPaePortInfo[u2PortNum].
                                                    u2PortStatus);
                }
            }
        }
    }
    pSuppFsmInfo->bSuppKeyAvailable = PNAC_FALSE;
    pSuppFsmInfo->bUserLogoff = PNAC_FALSE;
    pSuppFsmInfo->bLogoffSent = PNAC_FALSE;
    pSuppFsmInfo->bReqId = PNAC_FALSE;
    pSuppFsmInfo->bReqAuth = PNAC_FALSE;
    pSuppFsmInfo->bEapSuccess = PNAC_FALSE;
    pSuppFsmInfo->bEapFail = PNAC_FALSE;

    /* Initialise key related information */
    pKeyInfo = &(pSuppSessNode->suppKeyInfo);
    pKeyInfo->u2KeyLength = PNAC_INIT_VAL;
    pKeyInfo->u1KeyIndex = PNAC_INIT_VAL;

    /* Initialise the Supplicant Statistics Information */
    pPortInfo = &(PNAC_PORT_INFO (u2PortNum));
    pSuppStats = &(PNAC_SUPP_STATS_ENTRY (pPortInfo));
    PNAC_MEMSET (pSuppStats->lastEapolFrameSource,
                 PNAC_INIT_VAL, sizeof (tMacAddr));
    pSuppStats->u4EapolFramesRx = PNAC_INIT_VAL;
    pSuppStats->u4EapolFramesTx = PNAC_INIT_VAL;
    pSuppStats->u4EapolStartFramesTx = PNAC_INIT_VAL;
    pSuppStats->u4EapolLogoffFramesTx = PNAC_INIT_VAL;
    pSuppStats->u4EapolRespIdFramesTx = PNAC_INIT_VAL;
    pSuppStats->u4EapolRespFramesTx = PNAC_INIT_VAL;
    pSuppStats->u4EapolReqIdFramesRx = PNAC_INIT_VAL;
    pSuppStats->u4EapolReqFramesRx = PNAC_INIT_VAL;
    pSuppStats->u4InvalidEapolFramesRx = PNAC_INIT_VAL;
    pSuppStats->u4EapLengthErrorFramesRx = PNAC_INIT_VAL;
    pSuppStats->u4LastEapolFrameVersion = PNAC_INIT_VAL;

    /* Initialise the timer node pointers */
    pSuppSessNode->pAuthWhileTimer = NULL;
    pSuppSessNode->pHeldWhileTimer = NULL;
    pSuppSessNode->pStartWhenTimer = NULL;
    pSuppSessNode->u2PortNo = u2PortNum;

    /* Set Default user names and passwords */
    pSuppSessNode->u2UserNameLen = (UINT2) PNAC_STRLEN ("guest");
    PNAC_MEMCPY (pSuppSessNode->au1UserName,
                 "guest", pSuppSessNode->u2UserNameLen);

    pSuppSessNode->u2PasswdLen = (UINT2) PNAC_STRLEN ("future");
    PNAC_STRNCPY (pSuppSessNode->au1Passwd, "future", STRLEN("future"));
    pSuppSessNode->au1Passwd[STRLEN("future")] = '\0';
    FsUtlEncryptPasswd ((CHR1 *) pSuppSessNode->au1Passwd);
    return;
}

/*****************************************************************************/
/* Function Name      : PnacSuppHandleInEapFrame                             */
/*                                                                           */
/* Description        : Processes the EAP Frames destined to supplicant      */
/*                      and calls the Supplicant state machine.              */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pu1PktPtr - Pointer to the EAP packet received       */
/*                                  from the port.                           */
/*                      u2PktLen -  EAPOL body (i.e EAP Packet) length       */
/*                      u2PortNum - The Port Number through which this       */
/*                                  packet was received.                     */
/*                      u1EapolType - EAPOL packet's type field value.       */
/*                      pSrcAddr - Received packet's source MAC address.     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
PnacSuppHandleInEapFrame (UINT1 *pu1PktPtr,
                          UINT2 u2PktLen,
                          UINT2 u2PortNum,
                          UINT1 u1EapolType, tMacAddr srcMacAddr)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacSuppSessionNode *pSuppSessNode = NULL;
    UINT2               u2Identifier;
    UINT2               u2EapDataLength;
    UINT1              *pu1ReadPtr = NULL;
    UINT1               u1Code;
    UINT1               u1EapType;
    UINT1               u1Val;

    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  "PnacSuppHandleInEapFrame:Invalid" "Port Number\n");
        return PNAC_FAILURE;
    }
    pu1ReadPtr = pu1PktPtr;

    PNAC_TRC (PNAC_CONTROL_PATH_TRC, " Inside PnacSuppHandleInEapFrame:\n");

    if ((pSuppSessNode = pPortInfo->pPortSuppSessionNode) == NULL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " pPortSuppSessionNode is NULL\n");
        return PNAC_FAILURE;
    }
    /* Supplicant Session Node is available now */
    PNAC_MEMCPY (PNAC_SUPP_STATS_ENTRY (pPortInfo).lastEapolFrameSource,
                 srcMacAddr, PNAC_MAC_ADDR_SIZE);

    if (u1EapolType == PNAC_EAP_PKT)
    {
        /* Read the Eap Hdr Code field */
        PNAC_GET_1BYTE (u1Code, pu1ReadPtr);
        /* Read the Eap Hdr Identifier field */
        PNAC_GET_1BYTE (u1Val, pu1ReadPtr);
        u2Identifier = (UINT2) u1Val;
        pSuppSessNode->suppFsmInfo.u2ReceivedId = u2Identifier;
        /* read the EapBodyLength specified in the Eap pkt received */
        PNAC_GET_2BYTE (u2EapDataLength, pu1ReadPtr);

        /* u2PktLen is EapolBodyLength. Get EapBodyLength from it
         * and validate it.
         */
        if ((u2PktLen != u2EapDataLength) ||
            ((u2PktLen - PNAC_EAP_HDR_SIZE) > (PNAC_MAX_EAP_DATA_SIZE)))
        {

            /* Eap length error had occurred */
            /* update stats */
            PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapLengthErrorFramesRx++;

            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " Eap pkt Length error occurred\n");
            return PNAC_FAILURE;
        }

        switch (u1Code)
        {
            case PNAC_EAP_CODE_REQ:
                PNAC_GET_1BYTE (u1EapType, pu1ReadPtr);
                /* Restore the read Pointer */
                pu1ReadPtr--;
                if (u1EapType == PNAC_EAP_TYPE_IDENTITY)
                {

                    /* update stats */
                    PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolReqIdFramesRx++;
                    PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolFramesRx++;

                    if (PnacSuppStateMachine (PNAC_SSM_EV_REQID_RCVD,
                                              u2PortNum, pSuppSessNode,
                                              pu1ReadPtr,
                                              (UINT2) (u2PktLen -
                                                       PNAC_EAP_HDR_SIZE)) !=
                        PNAC_SUCCESS)
                    {
                        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                                  "PnacSuppHandleInEapFrame or"
                                  "SSM returned Failure\n");
                        return PNAC_FAILURE;
                    }

                    return PNAC_SUCCESS;
                }
                else
                {

                    /* update stats */
                    PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolReqFramesRx++;
                    PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolFramesRx++;
                    if (PnacSuppStateMachine (PNAC_SSM_EV_REQAUTH_RCVD,
                                              u2PortNum, pSuppSessNode,
                                              pu1ReadPtr,
                                              (UINT2) (u2PktLen -
                                                       PNAC_EAP_HDR_SIZE)) !=
                        PNAC_SUCCESS)
                    {
                        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                                  "PnacSuppHandleInEapFrame or"
                                  "SSM returned Failure\n");
                        return PNAC_FAILURE;
                    }
                    return PNAC_SUCCESS;

                }
                break;

            case PNAC_EAP_CODE_SUCCESS:

                if (u2EapDataLength != PNAC_EAP_HDR_SIZE)
                {

                    /* Eap length error had occurred */
                    /* update stats */
                    PNAC_SUPP_STATS_ENTRY (pPortInfo).
                        u4EapLengthErrorFramesRx++;
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              " Eap pkt Length error occurred\n");
                    return PNAC_FAILURE;
                }

                /* update stats */
                PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolFramesRx++;

                if (PnacSuppStateMachine (PNAC_SSM_EV_EAPSUCCESS_RCVD,
                                          u2PortNum, pSuppSessNode,
                                          NULL, PNAC_NO_VAL) != PNAC_SUCCESS)
                {
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              "SuppHandleInEapFrame: SSM returned Failure\n");
                    return PNAC_FAILURE;
                }
                return PNAC_SUCCESS;
                break;
            case PNAC_EAP_CODE_FAILURE:

                if (u2EapDataLength != PNAC_EAP_HDR_SIZE)
                {

                    /* Eap length error had occurred */
                    /* update stats */
                    PNAC_SUPP_STATS_ENTRY (pPortInfo).
                        u4EapLengthErrorFramesRx++;
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              "SuppHandleInEapFrame: Eap Length error\n");
                    return PNAC_FAILURE;
                }

                /* update stats */
                PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolFramesRx++;

                if (PnacSuppStateMachine (PNAC_SSM_EV_EAPFAIL_RCVD,
                                          u2PortNum, pSuppSessNode,
                                          NULL, PNAC_NO_VAL) != PNAC_SUCCESS)
                {
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              "SuppHandleInEapFrame: SSM returned Failure\n");
                    return PNAC_FAILURE;
                }
                return PNAC_SUCCESS;
                break;
            default:
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          "SuppHandleInEapFrame: Illegal EAP pkt for Supplicant\n");
                /* update stats */
                PNAC_SUPP_STATS_ENTRY (pPortInfo).u4InvalidEapolFramesRx++;

                return PNAC_FAILURE;
                break;

        }
    }
    else
    {
        /* EAPOL Pkt type is other than EAP pkt */
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  "SuppHandleInEapFrame: Illegal EAPOL packet for Supplicant\n");
        /* update stats */
        PNAC_SUPP_STATS_ENTRY (pPortInfo).u4InvalidEapolFramesRx++;
        return PNAC_FAILURE;
    }
}                                /* PnacSuppHandleInEapFrame */

/*****************************************************************************/
/* Function Name      : PnacSuppLinkStatusUpdate                             */
/*                                                                           */
/* Description        : This function invokes the Supplicant state machine   */
/*                      to handle the change in port Oper status.            */
/*                                                                           */
/* Input(s)           :  pPortInfo - Port number,                            */
/*                       u1Status - Oper status of the port                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : Supplicant session node in gPnacSystemInfo           */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : Supplicant session node in gPnacSystemInfo           */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacSuppLinkStatusUpdate (tPnacPaePortEntry * pPortInfo)
{
    UINT1               u1Status;
    tPnacSuppSessionNode *pSuppSessionNode = NULL;

    pSuppSessionNode = pPortInfo->pPortSuppSessionNode;
    if (pSuppSessionNode == NULL)
    {
        /* No Supplicant session node available. ignore this event */
        PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                  "SuppHandleInEapFrame:or "
                  "SSM returned because of lack of node\n");
        return PNAC_FAILURE;
    }
    u1Status = pPortInfo->u1EntryStatus;
    if (u1Status == PNAC_PORT_OPER_DOWN)
    {
        if (PnacSuppStateMachine (PNAC_SSM_EV_PORTDISABLED,
                                  pPortInfo->u2PortNo,
                                  pSuppSessionNode,
                                  NULL, PNAC_NO_VAL) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      "SuppHandleInEapFrame:or" "SSM returned Failure\n");
            return PNAC_FAILURE;
        }
        return PNAC_SUCCESS;
    }
    else if (u1Status == PNAC_PORT_OPER_UP)
    {
        if (PNAC_IS_PORT_MODE_PORTBASED (pPortInfo))
        {
            if (PnacSuppStateMachine (PNAC_SSM_EV_INITIALIZE,
                                      pPortInfo->u2PortNo,
                                      pSuppSessionNode,
                                      NULL, PNAC_NO_VAL) != PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          "SuppHandleInEapFrame:or" "SSM returned Failure\n");
                return PNAC_FAILURE;
            }
        }
        return PNAC_SUCCESS;
    }
    else
    {

        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " Invalid Status information\n");
        return PNAC_FAILURE;
    }

}                                /*PnacSuppLinkStatusUpdate ends */

/*****************************************************************************/
/* Function Name      : PnacSuppTxStart                                      */
/*                                                                           */
/* Description        : This function builds Eapol Start frame and transmits */
/*                      to CFA.                                              */
/*                                                                           */
/* Input(s)           : destAddr - Destination MAC address,                  */
/*                      u1Identifier - session identifier,                   */
/*                      u2PortNum - Port Number                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
PnacSuppTxStart (tMacAddr destAddr, UINT2 u2PortNum)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    UINT2               u2FrameLength = PNAC_INIT_VAL;
    UINT1              *pu1StartEapolFrame = NULL;

    if (PNAC_ALLOCATE_ENET_EAP_ARRAY_MEMBLK (pu1StartEapolFrame) == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Unable to allocate memory. \r\n");
        return PNAC_FAILURE;
    }

    PNAC_MEMSET (pu1StartEapolFrame, PNAC_INIT_VAL, PNAC_MAX_ENET_FRAME_SIZE);

    PnacPaeConstructEapolHdr (pu1StartEapolFrame,
                              destAddr, PNAC_EAPOL_START, 0, u2PortNum,
                              OSIX_FALSE);

    u2FrameLength += PNAC_ENET_EAPOL_HDR_SIZE;

    if (PnacPaeTxEnetFrame (pu1StartEapolFrame,
                            u2FrameLength, u2PortNum) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PnacAuthTxEap: or" "PnacPaeTxEnetFrame returned Failure\n");
        if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1StartEapolFrame) ==
            MEM_FAILURE)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      "Failed to release  memory. \r\n");
        }
        return PNAC_FAILURE;
    }

    if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1StartEapolFrame) == MEM_FAILURE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Failed to release  memory. \r\n");
        return PNAC_FAILURE;
    }

    /* update stats */

    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  "PnacSuppTxStart:Invalid Port Number\n");
        return PNAC_FAILURE;
    }
    PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolStartFramesTx++;
    PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolFramesTx++;

    return PNAC_SUCCESS;

}                                /*PnacSuppTxStart ends */

/*****************************************************************************/
/* Function Name      : PnacSuppTxLogoff                                     */
/*                                                                           */
/* Description        : This function builds Eapol Logoff frame and          */
/*                      transmits to CFA.                                    */
/*                                                                           */
/* Input(s)           : destAddr - Destination MAC address,                  */
/*                      u1Identifier - session identifier,                   */
/*                      u2PortNum - Port Number                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT4
PnacSuppTxLogoff (tMacAddr destAddr, UINT2 u2PortNum)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    UINT2               u2FrameLength = PNAC_INIT_VAL;
    UINT1              *pu1LogoffEapolFrame = NULL;

    if (PNAC_ALLOCATE_ENET_EAP_ARRAY_MEMBLK (pu1LogoffEapolFrame) == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Unable to allocate memory. \r\n");
        return PNAC_FAILURE;
    }

    PNAC_MEMSET (pu1LogoffEapolFrame, PNAC_INIT_VAL, PNAC_MAX_ENET_FRAME_SIZE);

    PnacPaeConstructEapolHdr (pu1LogoffEapolFrame,
                              destAddr, PNAC_EAPOL_LOGOFF, 0, u2PortNum,
                              OSIX_FALSE);

    u2FrameLength += PNAC_ENET_EAPOL_HDR_SIZE;

    if (PnacPaeTxEnetFrame (pu1LogoffEapolFrame,
                            u2FrameLength, u2PortNum) != PNAC_SUCCESS)
    {
        if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1LogoffEapolFrame) ==
            MEM_FAILURE)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      "Failed to release  memory. \r\n");
        }
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PnacAuthTxEap:or" "PnacPaeTxEnetFrame returned Failure\n");
        return PNAC_FAILURE;
    }

    /* update stats */
    pPortInfo = &(PNAC_PORT_INFO (u2PortNum));
    PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolLogoffFramesTx++;
    PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolFramesTx++;
    if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1LogoffEapolFrame) == MEM_FAILURE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Failed to release  memory. \r\n");
        return PNAC_FAILURE;
    }

    return PNAC_SUCCESS;

}                                /*PnacSuppTxLogoff ends */

/*****************************************************************************/
/* Function Name      : PnacSuppTxRespId                                     */
/*                                                                           */
/* Description        : This function builds Eap Response Identity frame and */
/*                      transmits to CFA.                                    */
/*                                                                           */
/* Input(s)           : destAddr - Destination MAC address,                  */
/*                      u1Identifier - session identifier,                   */
/*                      u2PortNum - Port Number                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT4
PnacSuppTxRespId (tMacAddr destAddr, UINT1 u1Identifier, UINT2 u2PortNum)
{
    tPnacSuppSessionNode *pSuppSessionNode = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacEapPktHdr      eapPktHdr;
    UINT1               au1Buf[PNAC_USER_NAME_SIZE + 1];
    UINT2               u2UserNameLen = 0;

    PNAC_MEMSET (au1Buf, PNAC_INIT_VAL, PNAC_USER_NAME_SIZE + 1);

    pPortInfo = &(PNAC_PORT_INFO (u2PortNum));
    pSuppSessionNode = pPortInfo->pPortSuppSessionNode;

    u2UserNameLen = pSuppSessionNode->u2UserNameLen;

    /* filling Eap Data, which is 'UserName' */
    eapPktHdr.u2DataLength = (UINT2) (PNAC_EAP_TYPE_SIZE + u2UserNameLen);

    /* Fill the Eap Data type as Identity */
    au1Buf[0] = PNAC_EAP_TYPE_IDENTITY;
    PNAC_STRNCPY (&au1Buf[1], pSuppSessionNode->au1UserName, u2UserNameLen);

    eapPktHdr.pu1Data = au1Buf;

    /* filling Eap Code and Identifier */
    eapPktHdr.u1Code = PNAC_EAP_CODE_RESP;
    eapPktHdr.u1Identifier = u1Identifier;

    /* build Eapol RespID frame and transmit it to CFA through L2IWF */
    if (PnacPaeTxEapPkt (destAddr, u2PortNum, &eapPktHdr, OSIX_FALSE) !=
        PNAC_SUCCESS)
    {

        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PnacSuppTxRespId: or" "PnacPaeTxEapPkt returned Failure\n");
        return PNAC_FAILURE;
    }

    /* update stats */
    PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolRespIdFramesTx++;
    PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolFramesTx++;

    return PNAC_SUCCESS;

}                                /*PnacSuppTxRespId ends */

/*****************************************************************************/
/* Function Name      : PnacSuppTxRespAuth                                   */
/*                                                                           */
/* Description        : This function builds Eap Response Authentication     */
/*                      frame and transmits it to CFA through L2IWF.         */
/*                                                                           */
/* Input(s)           : pEapData - Data field of Eap-Request-Identity,       */
/*                      u2EapDataLen - Length of Eap-Request-Identity,       */
/*                      destAddr - Destination MAC address,                  */
/*                      u1Identifier - session identifier,                   */
/*                      u2PortNum - Port Number                              */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT4
PnacSuppTxRespAuth (UINT1 *pEapData,
                    UINT2 u2EapDataLen,
                    tMacAddr destAddr, UINT1 u1Identifier, UINT2 u2PortNum)
{

    tPnacSuppSessionNode *pSuppSessionNode = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacEapPktHdr      eapPktHdr;
    UINT2               u2ChallengeLen = PNAC_INIT_VAL;
    UINT2               u2Md5InputLen = PNAC_INIT_VAL;
    UINT2               u2ResponseLen = PNAC_INIT_VAL;
    UINT1              *pu1ReadPtr = NULL;
    UINT1              *pu1WritePtr = NULL;
    UINT1              *pu1Md5Input = NULL;
    UINT1               au1Response[PNAC_EAP_TYPE_SIZE +
                                    PNAC_EAP_VALUE_SIZE +
                                    PNAC_MD5_DIGEST_SIZE + PNAC_USER_NAME_SIZE];
    UINT1               au1Password[PNAC_SUPP_PASSWD_SIZE];

    if (PNAC_ALLOCATE_ENET_EAP_ARRAY_MEMBLK (pu1Md5Input) == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Unable to allocate memory. \r\n");
        return PNAC_FAILURE;
    }

    pPortInfo = &(PNAC_PORT_INFO (u2PortNum));
    pSuppSessionNode = pPortInfo->pPortSuppSessionNode;

    /* initialize all data arrays */
    PNAC_MEMSET (au1Password, PNAC_INIT_VAL, PNAC_SUPP_PASSWD_SIZE);
    PNAC_MEMSET (pu1Md5Input, PNAC_INIT_VAL, PNAC_MAX_ENET_FRAME_SIZE);
    PNAC_MEMSET (au1Response, PNAC_INIT_VAL, PNAC_EAP_TYPE_SIZE
                 + PNAC_EAP_VALUE_SIZE + PNAC_MD5_DIGEST_SIZE
                 + PNAC_USER_NAME_SIZE);

    /* validating the Request Type */
    pu1ReadPtr = pEapData;

    if (*pu1ReadPtr != PNAC_EAP_TYPE_MD5CHALLENGE)
    {

        /* send NAK to the Authentication Server */

        /* filling Eap Response Length */
        u2ResponseLen = PNAC_EAP_TYPE_SIZE + PNAC_EAP_TYPE_SIZE;    /* 1 byte each for Eap-Data-Type 
                                                                       and Eap-Data */
        eapPktHdr.u2DataLength = u2ResponseLen;

        /* filling Eap  Response Data Type */
        pu1WritePtr = au1Response;
        *pu1WritePtr = PNAC_EAP_TYPE_NAK;

        /* filling Eap Response Data, which is desired Authentication Type */
        pu1WritePtr += 1;
        *pu1WritePtr = PNAC_EAP_DESIRED_AUTHENTICATION_TYPE;

        eapPktHdr.pu1Data = au1Response;

        /* filling Eap Code and Identifier */
        eapPktHdr.u1Code = PNAC_EAP_CODE_RESP;
        eapPktHdr.u1Identifier = u1Identifier;

        /* build Eapol RespAuth frame and transmit it to CFA through L2IWF */
        if (PnacPaeTxEapPkt (destAddr, u2PortNum, &eapPktHdr, OSIX_FALSE) !=
            PNAC_SUCCESS)
        {
            if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Md5Input) == MEM_FAILURE)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                          "Failed to release  memory. \r\n");
            }
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " PnacSuppTxRespAuth:or"
                      "PnacPaeTxEapPkt returned Failure\n");
            return PNAC_FAILURE;
        }

        /* update stats */
        PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolRespFramesTx++;
        PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolFramesTx++;

        if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Md5Input) == MEM_FAILURE)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      "Failed to release  memory. \r\n");
            return PNAC_FAILURE;
        }
        return PNAC_SUCCESS;

    }
    /*condition for non-MD5 Challenge request, ends */

    /* Request is an MD5 Challenge */

    /* build the input for MD5 Algorithm Calculation */
    /* The input is the concatenation of 'Identifier', 'Passwd' and
     * 'ChallengeValue' 
     */

    pu1WritePtr = pu1Md5Input;
    *(pu1WritePtr) = u1Identifier;
    u2Md5InputLen += sizeof (u1Identifier);

    pu1WritePtr += 1;

    PNAC_STRNCPY (au1Password,
                  pSuppSessionNode->au1Passwd, (sizeof (au1Password) - 1));
    FsUtlDecryptPasswd ((CHR1 *) au1Password);
    PNAC_STRNCPY (pu1WritePtr, au1Password, pSuppSessionNode->u2PasswdLen);
    u2Md5InputLen += pSuppSessionNode->u2PasswdLen;
    pu1ReadPtr += 1;
    u2ChallengeLen = *pu1ReadPtr;
    pu1ReadPtr += 1;
    pu1WritePtr += pSuppSessionNode->u2PasswdLen;
    PNAC_MEMCPY (pu1WritePtr, pu1ReadPtr, u2ChallengeLen);
    u2Md5InputLen += u2ChallengeLen;

    pu1WritePtr = NULL;            /* reinitializing the write pointer */

    /* filling Eap Response Length */

    u2ResponseLen = (UINT2) (PNAC_EAP_TYPE_SIZE +
                             PNAC_EAP_VALUE_SIZE +
                             PNAC_MD5_DIGEST_SIZE +
                             pSuppSessionNode->u2UserNameLen);
    eapPktHdr.u2DataLength = u2ResponseLen;

    /* filling Eap  Response Data Type */
    pu1WritePtr = au1Response;
    *pu1WritePtr = PNAC_EAP_TYPE_MD5CHALLENGE;

    /* filling Eap Response Data, which is MD5 Digest */
    pu1WritePtr += 1;

    *pu1WritePtr = PNAC_MD5_DIGEST_SIZE;
    pu1WritePtr += 1;

    PNAC_GET_MD5_DIGEST (pu1Md5Input, u2Md5InputLen, pu1WritePtr);
    pu1WritePtr += PNAC_MD5_DIGEST_SIZE;

    PNAC_STRNCPY (pu1WritePtr,
                  pSuppSessionNode->au1UserName,
                  pSuppSessionNode->u2UserNameLen);
    eapPktHdr.pu1Data = au1Response;

    /* filling Eap Code and Identifier */
    eapPktHdr.u1Code = PNAC_EAP_CODE_RESP;
    eapPktHdr.u1Identifier = u1Identifier;

    /* Build Eapol RespAuth frame and transmit it */
    if (PnacPaeTxEapPkt (destAddr, u2PortNum, &eapPktHdr, OSIX_FALSE) !=
        PNAC_SUCCESS)
    {
        if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Md5Input) == MEM_FAILURE)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      "Failed to release  memory. \r\n");
        }
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PnacSuppTxRespAuth: or"
                  "PnacPaeTxEapPkt returned Failure\n");
        return PNAC_FAILURE;
    }

    PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolRespFramesTx++;
    PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolFramesTx++;

    PNAC_UNUSED (u2EapDataLen);
    if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Md5Input) == MEM_FAILURE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Failed to release  memory. \r\n");
        return PNAC_FAILURE;
    }
    return PNAC_SUCCESS;

}                                /*PnacSuppTxRespAuth ends */
