/* SOURCE FILE HEADER :
 *
 * $Id: pnacnpapi.c,v 1.6 2015/09/23 12:18:43 siva Exp $
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : pnacnpapi.c                                       |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : Jeeva Sethuraman                                 |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : PNAC                                             |
 * |                                                                           |
 * |  MODULE NAME           : PNAC NP Wrapper configurations                   |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : All  Network Processor API Function              |
 * |                          calls are done here                              |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

#ifndef __PNACNPAPI_C__
#define __PNACNPAPI_C__

#include "pnachdrs.h"

#ifdef L2RED_WANTED
/***************************************************************************
 *                                                                          
 *    Function Name       : PnacFsPnacRedHwUpdateDB                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsPnacRedHwUpdateDB
 *                                                                          
 *    Input(s)            : Arguments of FsPnacRedHwUpdateDB
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
PnacFsPnacRedHwUpdateDB (UINT2 u2Port, UINT1 *pu1SuppAddr, UINT1 u1AuthMode,
                         UINT1 u1AuthStatus, UINT1 u1CtrlDir)
{
    tFsHwNp             FsHwNp;
    tPnacNpModInfo     *pPnacNpModInfo = NULL;
    tPnacNpWrFsPnacRedHwUpdateDB *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_PNAC_MOD,    /* Module ID */
                         FS_PNAC_RED_HW_UPDATE_D_B,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pPnacNpModInfo = &(FsHwNp.PnacNpModInfo);
    pEntry = &pPnacNpModInfo->PnacNpFsPnacRedHwUpdateDB;

    pEntry->u4Port = u2Port;
    pEntry->pu1SuppAddr = pu1SuppAddr;
    pEntry->u1AuthMode = u1AuthMode;
    pEntry->u1AuthStatus = u1AuthStatus;
    pEntry->u1CtrlDir = u1CtrlDir;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* L2RED_WANTED */

/***************************************************************************
 *                                                                          
 *    Function Name       : PnacPnacHwAddOrDelMacSess                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes PnacHwAddOrDelMacSess
 *                                                                          
 *    Input(s)            : Arguments of PnacHwAddOrDelMacSess
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
PnacPnacHwAddOrDelMacSess (UINT2 u2Port, UINT1 *pu1SuppAddr, UINT1 u1SessStatus)
{
    tFsHwNp             FsHwNp;
    tPnacNpModInfo     *pPnacNpModInfo = NULL;
    tPnacNpWrPnacHwAddOrDelMacSess *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_PNAC_MOD,    /* Module ID */
                         PNAC_HW_ADD_OR_DEL_MAC_SESS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pPnacNpModInfo = &(FsHwNp.PnacNpModInfo);
    pEntry = &pPnacNpModInfo->PnacNpPnacHwAddOrDelMacSess;

    pEntry->u4Port = u2Port;
    pEntry->pu1SuppAddr = pu1SuppAddr;
    pEntry->u1SessStatus = u1SessStatus;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : PnacPnacHwDisable                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes PnacHwDisable
 *                                                                          
 *    Input(s)            : Arguments of PnacHwDisable
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
PnacPnacHwDisable ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_PNAC_MOD,    /* Module ID */
                         PNAC_HW_DISABLE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : PnacPnacHwEnable                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes PnacHwEnable
 *                                                                          
 *    Input(s)            : Arguments of PnacHwEnable
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
PnacPnacHwEnable ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_PNAC_MOD,    /* Module ID */
                         PNAC_HW_ENABLE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : PnacPnacHwGetAuthStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes PnacHwGetAuthStatus
 *                                                                          
 *    Input(s)            : Arguments of PnacHwGetAuthStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
PnacPnacHwGetAuthStatus (UINT2 u2PortNum, UINT1 *pu1SuppAddr, UINT1 u1AuthMode,
                         UINT2 *pu2AuthStatus, UINT2 *pu2CtrlDir)
{
    tFsHwNp             FsHwNp;
    tPnacNpModInfo     *pPnacNpModInfo = NULL;
    tPnacNpWrPnacHwGetAuthStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_PNAC_MOD,    /* Module ID */
                         PNAC_HW_GET_AUTH_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pPnacNpModInfo = &(FsHwNp.PnacNpModInfo);
    pEntry = &pPnacNpModInfo->PnacNpPnacHwGetAuthStatus;

    pEntry->u4PortNum = u2PortNum;
    pEntry->pu1SuppAddr = pu1SuppAddr;
    pEntry->u1AuthMode = u1AuthMode;
    pEntry->pu2AuthStatus = pu2AuthStatus;
    pEntry->pu2CtrlDir = pu2CtrlDir;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : PnacPnacHwGetSessionCounter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes PnacHwGetSessionCounter
 *                                                                          
 *    Input(s)            : Arguments of PnacHwGetSessionCounter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
PnacPnacHwGetSessionCounter (UINT2 u2Port, UINT1 *pu1SuppAddr,
                             UINT1 u1CounterType, UINT4 *pu4HiCounter,
                             UINT4 *pu4LoCounter)
{
    tFsHwNp             FsHwNp;
    tPnacNpModInfo     *pPnacNpModInfo = NULL;
    tPnacNpWrPnacHwGetSessionCounter *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_PNAC_MOD,    /* Module ID */
                         PNAC_HW_GET_SESSION_COUNTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pPnacNpModInfo = &(FsHwNp.PnacNpModInfo);
    pEntry = &pPnacNpModInfo->PnacNpPnacHwGetSessionCounter;

    pEntry->u4Port = u2Port;
    pEntry->pu1SuppAddr = pu1SuppAddr;
    pEntry->u1CounterType = u1CounterType;
    pEntry->pu4HiCounter = pu4HiCounter;
    pEntry->pu4LoCounter = pu4LoCounter;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : PnacPnacHwSetAuthStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes PnacHwSetAuthStatus
 *                                                                          
 *    Input(s)            : Arguments of PnacHwSetAuthStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
PnacPnacHwSetAuthStatus (UINT2 u2Port, UINT1 *pu1SuppAddr, UINT1 u1AuthMode,
                         UINT1 u1AuthStatus, UINT1 u1CtrlDir)
{
    tFsHwNp             FsHwNp;
    tPnacNpModInfo     *pPnacNpModInfo = NULL;
    tPnacNpWrPnacHwSetAuthStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_PNAC_MOD,    /* Module ID */
                         PNAC_HW_SET_AUTH_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pPnacNpModInfo = &(FsHwNp.PnacNpModInfo);
    pEntry = &pPnacNpModInfo->PnacNpPnacHwSetAuthStatus;

    pEntry->u4Port = u2Port;
    if(u1AuthMode == PNAC_PORT_AUTHMODE_MACBASED)
    {
        pEntry->pu1SuppAddr = pu1SuppAddr;  
    }
    else
    {
        pEntry->pu1SuppAddr = NULL;  
    }
    pEntry->u1AuthMode = u1AuthMode;
    pEntry->u1AuthStatus = u1AuthStatus;
    pEntry->u1CtrlDir = u1CtrlDir;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : PnacPnacHwStartSessionCounters                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes PnacHwStartSessionCounters
 *                                                                          
 *    Input(s)            : Arguments of PnacHwStartSessionCounters
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
PnacPnacHwStartSessionCounters (UINT2 u2Port, UINT1 *pu1SuppAddr)
{
    tFsHwNp             FsHwNp;
    tPnacNpModInfo     *pPnacNpModInfo = NULL;
    tPnacNpWrPnacHwStartSessionCounters *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_PNAC_MOD,    /* Module ID */
                         PNAC_HW_START_SESSION_COUNTERS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pPnacNpModInfo = &(FsHwNp.PnacNpModInfo);
    pEntry = &pPnacNpModInfo->PnacNpPnacHwStartSessionCounters;

    pEntry->u4Port = u2Port;
    pEntry->pu1SuppAddr = pu1SuppAddr;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : PnacPnacHwStopSessionCounters                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes PnacHwStopSessionCounters
 *                                                                          
 *    Input(s)            : Arguments of PnacHwStopSessionCounters
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
PnacPnacHwStopSessionCounters (UINT2 u2Port, UINT1 *pu1SuppAddr)
{
    tFsHwNp             FsHwNp;
    tPnacNpModInfo     *pPnacNpModInfo = NULL;
    tPnacNpWrPnacHwStopSessionCounters *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_PNAC_MOD,    /* Module ID */
                         PNAC_HW_STOP_SESSION_COUNTERS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pPnacNpModInfo = &(FsHwNp.PnacNpModInfo);
    pEntry = &pPnacNpModInfo->PnacNpPnacHwStopSessionCounters;

    pEntry->u4Port = u2Port;
    pEntry->pu1SuppAddr = pu1SuppAddr;

    return (NpUtilHwProgram (&FsHwNp));
}

#ifdef MBSM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : PnacPnacMbsmHwEnable                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes PnacMbsmHwEnable
 *                                                                          
 *    Input(s)            : Arguments of PnacMbsmHwEnable
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
PnacPnacMbsmHwEnable (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tPnacNpModInfo     *pPnacNpModInfo = NULL;
    tPnacNpWrPnacMbsmHwEnable *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_PNAC_MOD,    /* Module ID */
                         PNAC_MBSM_HW_ENABLE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pPnacNpModInfo = &(FsHwNp.PnacNpModInfo);
    pEntry = &pPnacNpModInfo->PnacNpPnacMbsmHwEnable;

    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : PnacPnacMbsmHwSetAuthStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes PnacMbsmHwSetAuthStatus
 *                                                                          
 *    Input(s)            : Arguments of PnacMbsmHwSetAuthStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
PnacPnacMbsmHwSetAuthStatus (UINT2 u2Port, UINT1 *pu1SuppAddr, UINT1 u1AuthMode,
                             UINT1 u1AuthStatus, UINT1 u1CtrlDir,
                             tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tPnacNpModInfo     *pPnacNpModInfo = NULL;
    tPnacNpWrPnacMbsmHwSetAuthStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_PNAC_MOD,    /* Module ID */
                         PNAC_MBSM_HW_SET_AUTH_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pPnacNpModInfo = &(FsHwNp.PnacNpModInfo);
    pEntry = &pPnacNpModInfo->PnacNpPnacMbsmHwSetAuthStatus;

    pEntry->u4Port = u2Port;
    pEntry->pu1SuppAddr = pu1SuppAddr;
    pEntry->u1AuthMode = u1AuthMode;
    pEntry->u1AuthStatus = u1AuthStatus;
    pEntry->u1CtrlDir = u1CtrlDir;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* MBSM_WANTED */

/***************************************************************************
 *
 *    Function Name       : PnacIssHwSetLearningMode                                                                                                        *
 *    Description         : This function using its arguments populates the                                                                                   *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W                                                                                   *                          parameters invokes IssHwSetLearningMode
 *                                                                                                                                                            *    Input(s)            : Arguments of IssHwSetLearningMode
 *                                                                                                                                                            *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
PnacIssHwSetLearningMode (UINT4 u4IfIndex, INT4 i4Status)
{
#ifdef ISS_WANTED
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetLearningMode *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_LEARNING_MODE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetLearningMode;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4Status = i4Status;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4Status);
#endif
    return (FNP_SUCCESS);
}

#endif /* __PNACNPAPI_C__ */
