#ifndef _PNACREDDUMMY_C_
#define _PNACREDDUMMY_C_

#include "pnachdrs.h"
#include "rstp.h"

#ifdef CLI_WANTED
#include "pnaccli.h"
#endif
/*****************************************************************************/
/* Function Name      : PnacRedInitRedSystemInfo                             */
/*                                                                           */
/* Description        : This function initialized gPnacRedSystemInfo.        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedInitRedSystemInfo (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name      : PnacRegisterWithRM                                   */
/*                                                                           */
/* Description        : Registers PNAC with RM by providing an application   */
/*                      ID for pnac and a call back function to be called    */
/*                      whenever RM needs to send an event to PNAC.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then PNAC_SUCCESS         */
/*                      Otherwise PNAC_FAILURE                               */
/*****************************************************************************/
INT4
PnacRedRegisterWithRM (VOID)
{
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacRedSyncUpDynamicInfo                             */
/*                                                                           */
/* Description        : Construct the dynamic sync up message for the given  */
/*                      and given authentication session and sends it to     */
/*                      standby node.                                        */
/*                                                                           */
/* Input(s)           : pPortInfo - Pointer to the port entry.               */
/*                      pAuthSessNode - Pointer to the authentication        */
/*                                      session node.                        */
/*                      pSuppSessNode - Pointer to supplecant session.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedSyncUpDynamicInfo (tPnacPaePortEntry * pPortInfo,
                          tPnacAuthSessionNode * pAuthSessNode,
                          tPnacSuppSessionNode * pSuppSessNode)
{
    UNUSED_PARAM (pPortInfo);
    UNUSED_PARAM (pAuthSessNode);
    UNUSED_PARAM (pSuppSessNode);
    return;
}

/*****************************************************************************/
/* Function Name      : PnacRedSyncUpModOrPortClearInfo                      */
/*                                                                           */
/* Description        : This function is used to port disable or module      */
/*                      disable information to standby node. If the input    */
/*                      port index is zero, it means module disable message  */
/*                      otherwise it is the port disable message for the     */
/*                      given port.                                          */
/*                                                                           */
/* Input(s)           : u4PortIndex - Port IfIndex.                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.NodeStatus.                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedSyncUpModOrPortClearInfo (UINT4 u4PortIndex)
{
    UNUSED_PARAM (u4PortIndex);
    return;
}

/*****************************************************************************/
/* Function Name      : PnacRedSyncUpPortOperUpStatus                        */
/*                                                                           */
/* Description        : This function is used to sync port enable            */
/*                      information to standby node.                         */
/*                                                                           */
/* Input(s)           : u4PortIndex - Port IfIndex.                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.NodeStatus.                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedSyncUpPortOperUpStatus (UINT4 u4PortIndex)
{
    UNUSED_PARAM (u4PortIndex);
    return;
}

/*****************************************************************************/
/* Function Name      : PnacRedSyncAsynchronousStatus                        */
/*                                                                           */
/* Description        : This function is used to sync Asynchronous Status    */
/*                      information to standby node.                         */
/*                                                                           */
/* Input(s)           : u2Port - Port Number                                 */
/*                      u1PnacPortCtrlDir - Port Control Direction           */
/*                      u1PnacPortAuthStatus - Port Auth Status              */
/*                                                                           */
/* Input(s)           : u4PortIndex - Port IfIndex.                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.NodeStatus.                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedSyncAsynchronousStatus (UINT2 u2Port, UINT1 u1PnacPortCtrlDir,
                               UINT1 u1PnacPortAuthStatus)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u1PnacPortCtrlDir);
    UNUSED_PARAM (u1PnacPortAuthStatus);
    return;
}
#endif /* _PNACREDDUMMY_C_ */
