/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/* $Id: pnacinit.c,v 1.82 2017/12/15 09:34:25 siva Exp $                   */
/*****************************************************************************/
/*    FILE  NAME            : pnacinit.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : PNAC module init/deinit                        */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Mar 2002                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains init, deinit functions      */
/*                            for PNAC module                                */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 MAR 2002 / BridgeTeam   Initial Create.                     */
/* 1.0.0.1    22 APR 2002 / BridgeTeam   Changed for including Radius        */
/*                                       Interface Data Initialization       */
/* 1.0.0.2    29 NOV 2002 / Products     Modifying NP-API calls              */
/*---------------------------------------------------------------------------*/

#ifndef _PNACINIT_C
#define _PNACINIT_C

#include "pnachdrs.h"
#include "radius.h"
#include "pnacport.h"
#include "stdpnacli.h"
#include "fspnaccli.h"
#include "mux.h"

#include "rstp.h"
#ifdef RSNA_WANTED
#include "rsna.h"
#endif
/*****************************************************************************/
/* Function Name      : PnacModuleInit                                       */
/*                                                                           */
/* Description        : This function has to be called only once while       */
/*                      system initialization.                               */
/*                      Currently this is called from SystemStart() lrmain.c */
/*                      in the System Initialization thread.                 */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSemId                                           */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSemId                                           */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

INT4
PnacModuleInit (VOID)
{
    if (PNAC_CREATE_SEM (PNAC_SEM_NAME, PNAC_SEM_COUNT,
                         PNAC_SEM_FLAGS, &gPnacSemId) != OSIX_SUCCESS)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_INIT_SHUT_TRC,
                  "Creation of Semaphore Failed ... \n");
        return PNAC_FAILURE;
    }

    if (PnacInitQueue () != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_INIT_SHUT_TRC,
                  "PNAC Queue initialisation failed ... \n");

        return PNAC_FAILURE;
    }

    if (PnacHandleModuleStart () != PNAC_SUCCESS)
    {
        /* If PnacHandleModuleStart failure could be any reason, we need 
         * not delete the sema4, bcz Pnac Module can be started 
         * from SNMP later. */
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_INIT_SHUT_TRC,
                  "Pnac Module Start Failed ... \n");

        PnacQandSemDeinit ();
        return PNAC_FAILURE;
    }
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacHandleModuleStart                                */
/*                                                                           */
/* Description        : This function allocates memory pools &               */
/*                      initialises data structures used by PNAC             */
/*                      module.  First time this function needs to be called */
/*                      from PnacModuleInit. This function can be called     */
/*                      for the PNAC module to start after the shutdown.     */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSystemInfo                                      */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

INT4
PnacHandleModuleStart (VOID)
{
    INT4                i4Result;
    UINT2               u2PortNum = PNAC_INIT_VAL;
    UINT2               u2Count = PNAC_INIT_VAL;
    UINT2               u2PrevPort = PNAC_INIT_VAL;
    tCfaIfInfo          IfInfo;
    tPnacPaePortEntry  *pPortInfo = NULL;

    /* Initialise the global structure variable */
    PNAC_MEMSET (&gPnacSystemInfo, PNAC_INIT_VAL, sizeof (tgPnacSystemInfo));
    gPnacSystemInfo.u4TraceOption = PNAC_INIT_VAL;
    PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC,
              " Initializing PNAC ... \n");
    gPnacSystemInfo.u2NasIdLen = (UINT2) PNAC_STRLEN (PNAC_DEFAULT_NAS_ID);
    PNAC_MEMCPY (gPnacSystemInfo.au1NasId, PNAC_DEFAULT_NAS_ID,
                 gPnacSystemInfo.u2NasIdLen);
    gPnacSystemInfo.u1AuthenticMethod = PNAC_LOCAL_AUTH_SERVER;
    gPnacSystemInfo.u1RemoteAuthServerType = PNAC_REMOTE_RADIUS_AUTH_SERVER;
    PNAC_MEMSET (&gPnacServInfo, PNAC_INIT_VAL, sizeof (tgPnacServInfo));

    PNAC_SLL_INIT (&(gPnacSystemInfo.PnacSlotInfo));

    gPnacSystemInfo.u4DPnacPeriodicSyncTime = PNAC_DEFAULT_PERIODIC_SYNC_TIME;
    gPnacSystemInfo.u4DPnacMaxAliveCount = PNAC_DEFAULT_MAX_KEEP_ALIVE_COUNT;
    gPnacSystemInfo.u1DPnacRolePlayed = (UINT1) CfaGetRetrieveNodeState ();
    gPnacSystemInfo.DPnacPeriodicSyncTmr = NULL;
    gPnacSystemInfo.u1SystemMode = PNAC_CENTRALIZED;

    if (PnacSizingMemCreateMemPools () != PNAC_SUCCESS)
    {

        PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC |
                  PNAC_ALL_FAILURE_TRC, " Create mem pool for Pnac failed\n");
        return PNAC_FAILURE;
    }
    /* Assign MempoolIds created in sz.c to global MempoolIds */
    PnacAssignMempoolIds ();
    gpu1EapolBuffer =
        (UINT1 *) PNAC_ALLOC_MEM_BLOCK (PNAC_EAPOL_MESG_MEMPOOL_ID);

    if (gpu1EapolBuffer == NULL)
    {
        PnacSizingMemDeleteMemPools ();
        return PNAC_FAILURE;
    }

    /* Initialise the timer module */
    if (PnacTmrInit () != PNAC_SUCCESS)

    {
        PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC |
                  PNAC_ALL_FAILURE_TRC, " Timer Init Failed\n");
        PNAC_RELEASE_MEM_BLOCK (PNAC_EAPOL_MESG_MEMPOOL_ID, gpu1EapolBuffer);
        PnacSizingMemDeleteMemPools ();

        return PNAC_FAILURE;
    }

    /* Create the RBTree to store the Retry count for the Mac sesssions
     * for which the NP programming failed during Mac session deletion. */
    gPnacSystemInfo.PnacNpFailedMacSess =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tPnacNpFailedMacSessNode,
                                              nextNpFailedMacSessNode)),
                              PnacCompareFailedMacAddr);

    if (gPnacSystemInfo.PnacNpFailedMacSess == NULL)
    {
        PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC |
                  PNAC_ALL_FAILURE_TRC, " RB Tree Creation for "
                  " NP Failed MAC Address Failed\n");
        PnacTmrDeinit ();
        PNAC_RELEASE_MEM_BLOCK (PNAC_EAPOL_MESG_MEMPOOL_ID, gpu1EapolBuffer);
        PnacSizingMemDeleteMemPools ();

        return PNAC_FAILURE;
    }

    /* Create a Session Hash Table to store information related to all the 
     * supplicants present in all the port that supports MAC based 
     * Authentication mode.
     */
    if ((gPnacSystemInfo.pPnacAuthSessionTable =
         PNAC_HASH_CREATE_TABLE (PNAC_SESS_TABLE_SIZE, NULL,
                                 PNAC_FALSE)) == NULL)

    {
        PnacTmrDeinit ();
        RBTreeDelete (gPnacSystemInfo.PnacNpFailedMacSess);
        PNAC_RELEASE_MEM_BLOCK (PNAC_EAPOL_MESG_MEMPOOL_ID, gpu1EapolBuffer);
        PnacSizingMemDeleteMemPools ();

        PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC |
                  PNAC_ALL_FAILURE_TRC, " Session Table creation failed\n");
        return PNAC_FAILURE;
    }

    /* Initialise the Authentication In Progress Table */
    for (u2Count = PNAC_MIN_SESS_IN_PROGRESS;
         u2Count < PNAC_MAX_SESS_IN_PROGRESS; u2Count++)

    {
        PNAC_AIP_ENTRY (u2Count) = (tPnacAuthSessionNode *) NULL;
    }

    for (u2PortNum = PNAC_MINPORTS; u2PortNum <= PNAC_MAXPORTS; u2PortNum++)
    {
        pPortInfo = &(PNAC_PORT_INFO (u2PortNum));
        PNAC_MEMSET (pPortInfo, PNAC_INIT_VAL, sizeof (tPnacPaePortEntry));
        pPortInfo->u1EntryStatus = PNAC_PORT_INVALID;
    }

    /* Scan the ports that are known and given by CFA */

    while ((i4Result = PnacL2IwfGetNextValidPort (u2PrevPort, &u2PortNum))
           == L2IWF_SUCCESS)
    {
        if ((CfaGetIfInfo (u2PortNum, &IfInfo) == CFA_SUCCESS) &&
            (IfInfo.u1BridgedIface == CFA_ENABLED))
        {
            if ((IfInfo.u1IfType == CFA_ENET) || (IfInfo.u1IfType == CFA_WLAN)
                || (IfInfo.u1IfType == CFA_CAPWAP_DOT11_BSS))
            {
                if (PnacProcCreatePortEvent (u2PortNum) != PNAC_SUCCESS)
                {
                    PnacTmrDeinit ();
                    RBTreeDelete (gPnacSystemInfo.PnacNpFailedMacSess);
                    PNAC_RELEASE_MEM_BLOCK (PNAC_EAPOL_MESG_MEMPOOL_ID,
                                            gpu1EapolBuffer);
                    PnacSizingMemDeleteMemPools ();

                    PNAC_HASH_DELETE_TABLE (gPnacSystemInfo.
                                            pPnacAuthSessionTable,
                                            PnacDeleteAuthSessionHashNode);
                    PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC |
                              PNAC_ALL_FAILURE_TRC,
                              " PnacInitPort returned failure\n");
                    return PNAC_FAILURE;
                }
            }

            /* Port Oper status not needed */

        }

        u2PrevPort = u2PortNum;
    }

    PNAC_SYSTEM_CONTROL = PNAC_START;
    PnacServInit ();

    PnacInitRadInterface ();

    PnacInitTacInterface ();

    PnacRedInitRedSystemInfo ();

    /* Get the Node Status and Num peers from RM. */
#ifdef L2RED_WANTED
    PnacRedInitRedSystemInfo ();
#else
    PNAC_RM_GET_NODE_STATUS ();
#endif
    PNAC_RM_GET_NUM_STANDBY_NODES_UP ();

    /* By default Pnac module is enabled, hence make the admin status as
     * enabled. */
    PNAC_INIT_PROTOCOL_ADMIN_STATUS ();

    PNAC_MODULE_STATUS = PNAC_DISABLED;
    /* Queue is created and the sem too, hence register with RM now. */
    /* Register with Redundancy manager. */
#ifdef L2RED_WANTED
    if (PnacRedRegisterWithRM () == PNAC_FAILURE)
    {
        PnacTmrDeinit ();
        PNAC_RELEASE_MEM_BLOCK (PNAC_EAPOL_MESG_MEMPOOL_ID, gpu1EapolBuffer);
        PnacSizingMemDeleteMemPools ();

        RBTreeDelete (gPnacSystemInfo.PnacNpFailedMacSess);
        PNAC_HASH_DELETE_TABLE (gPnacSystemInfo.pPnacAuthSessionTable,
                                PnacDeleteAuthSessionHashNode);

        return PNAC_FAILURE;
    }
#endif

    /* Register with packet handler when module Starts */
    if (PnacRegisterWithPacketHandler () != PNAC_SUCCESS)
    {
        PnacTmrDeinit ();
        RBTreeDelete (gPnacSystemInfo.PnacNpFailedMacSess);
        PNAC_HASH_DELETE_TABLE (gPnacSystemInfo.pPnacAuthSessionTable,
                                PnacDeleteAuthSessionHashNode);
        PNAC_RELEASE_MEM_BLOCK (PNAC_EAPOL_MESG_MEMPOOL_ID, gpu1EapolBuffer);
        PnacSizingMemDeleteMemPools ();

        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " Pnac Packet Reg failed !!!\n");
        return PNAC_FAILURE;
    }

    if ((PNAC_NODE_STATUS () != PNAC_IDLE_NODE) &&
        (PNAC_PROTOCOL_ADMIN_STATUS () == PNAC_ENABLED))
    {
        if (PnacModuleEnable () != PNAC_SUCCESS)
        {
            PnacTmrDeinit ();
            RBTreeDelete (gPnacSystemInfo.PnacNpFailedMacSess);
            PNAC_HASH_DELETE_TABLE (gPnacSystemInfo.pPnacAuthSessionTable,
                                    PnacDeleteAuthSessionHashNode);
            PNAC_RELEASE_MEM_BLOCK (PNAC_EAPOL_MESG_MEMPOOL_ID,
                                    gpu1EapolBuffer);
            PnacSizingMemDeleteMemPools ();

            PNAC_TRC (PNAC_ALL_FAILURE_TRC, " PnacModuleEnable failed\n");

            return PNAC_FAILURE;
        }
    }

    PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC,
              " Initialized PNAC ... \n");

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacModuleEnable                                     */
/*                                                                           */
/* Description        : Called by management when PNAC is enabled.           */
/*                      This function initialises all the data structures    */
/*                      used by PNAC module                                  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSystemInfo.apAuthInProgress                     */
/*                      gPnacSystemInfo.pPnacAuthSessionTable                */
/*                      gPnacSystemInfo.semId                                */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
PnacModuleEnable (VOID)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    UINT2               u2PortNum = PNAC_INIT_VAL;
    UINT1               u1OperStatus;

    PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC,
              " Enabling PNAC ... \n");

#ifdef NPAPI_WANTED
    if (PNAC_IS_NP_PROGRAMMING_ALLOWED () == PNAC_TRUE)
    {
        if (PnacPnacHwEnable () != FNP_SUCCESS)

        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC, " PnacHwEnable failed !!!\n");
            return PNAC_FAILURE;
        }
    }
#endif /* NPAPI_WANTED */

    for (u2PortNum = PNAC_MINPORTS; u2PortNum <= PNAC_MAXPORTS; u2PortNum++)
    {

        pPortInfo = &(PNAC_PORT_INFO (u2PortNum));

        if (pPortInfo->u1EntryStatus == PNAC_PORT_INVALID)
        {
            continue;
        }

        PnacL2IwfGetPortOperStatus (PNAC_MODULE, u2PortNum, &u1OperStatus);
        if (u1OperStatus == CFA_IF_UP)
        {
            /* Operational Status is UP */
            pPortInfo->u1EntryStatus = PNAC_PORT_OPER_UP;

            if (pPortInfo->u1PortPaeStatus == PNAC_ENABLED)
            {

#ifdef NPAPI_WANTED
                if (PNAC_IS_NP_PROGRAMMING_ALLOWED () == PNAC_TRUE)
                {
                    if ((PNAC_IS_PORT_MODE_MACBASED (pPortInfo)) &&
                        (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).
                         u2AuthControlPortControl == PNAC_PORTCNTRL_AUTO))
                    {
                        if (PnacPnacHwSetAuthStatus (u2PortNum,
                                                     NULL,
                                                     pPortInfo->u1PortAuthMode,
                                                     PNAC_BLOCK_PORT,
                                                     PNAC_CNTRLD_DIR_BOTH) !=
                            FNP_SUCCESS)

                        {
                            PnacNpFailNotifyAndLog (u2PortNum,
                                                    NULL,
                                                    PNAC_CNTRLD_DIR_BOTH,
                                                    pPortInfo->u1PortAuthMode,
                                                    PNAC_BLOCK_PORT);
                            return PNAC_FAILURE;
                        }

                    }
                }

#endif /* NPAPI_WANTED */

                /* Update the Port status as Un authorized if dot1x is enabled
                   and port-control mode is auto */

                if ((PNAC_IS_PORT_MODE_MACBASED (pPortInfo)) &&
                    (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).
                     u2AuthControlPortControl == PNAC_PORTCNTRL_AUTO))
                {
                    pPortInfo->u2PortStatus = PNAC_PORTSTATUS_UNAUTHORIZED;
                }

                if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))

                {
                    PnacPaeCdStateMachine (PNAC_CDSM_EV_PORTENABLED, u2PortNum);
                    PnacAuthLinkStatusUpdate (pPortInfo);
                }
                if (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))
                {
                    PnacSuppLinkStatusUpdate (pPortInfo);
                }
                if (PnacGetHLPortStatus (u2PortNum, &u1OperStatus) ==
                    PNAC_SUCCESS)
                {
                    if (u1OperStatus == CFA_IF_DOWN)

                    {
                        PnacAuthPortOperIndication (u2PortNum);
                    }
                }
            }
        }

    }
    PNAC_MODULE_STATUS = PNAC_ENABLED;

    PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC,
              " Enabled PNAC ... \n");
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacInitPort                                         */
/*                                                                           */
/* Description        : This function initialises the Port Table Entry       */
/*                      of the specified port.                               */
/*                      Called when port is created or when the port is      */
/*                      created already but pnac module is enabled now only  */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.aPnacPaePortInfo[u2PortNum]          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSystemInfo.aPnacPaePortInfo[u2PortNum]          */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
PnacInitPort (UINT2 u2PortNum)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacAuthConfigEntry *pAuthConfig = NULL;
    tPnacSuppConfigEntry *pSuppConfig = NULL;
    BOOL1               bOperEdge = OSIX_FALSE;

    PNAC_TRC_ARG1 (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC,
                   "Initializing Port %d Information ... \n", u2PortNum);
    pPortInfo = &(PNAC_PORT_INFO (u2PortNum));

    pPortInfo->u2PortNo = u2PortNum;
    pAuthConfig = &(pPortInfo->authConfigEntry);
    pSuppConfig = &(pPortInfo->suppConfigEntry);
    pAuthConfig->u4QuietPeriod = PNAC_QUIETPERIOD;
    pAuthConfig->u4TxPeriod = PNAC_TXPERIOD;
    pAuthConfig->u4SuppTimeout = PNAC_SUPPTIMEOUT;
    pAuthConfig->u4ServerTimeout = PNAC_SERVERTIMEOUT;
    pAuthConfig->u4ReAuthPeriod = PNAC_REAUTHPERIOD;
    pAuthConfig->u4MaxReq = PNAC_MAXREQ;
    pAuthConfig->u4ReAuthMax = PNAC_REAUTHMAX;
    pAuthConfig->bReAuthEnabled = PNAC_FALSE;
    pAuthConfig->bKeyTxEnabled = PNAC_FALSE;
    pAuthConfig->u2AdminControlDir = PNAC_CNTRLD_DIR_BOTH;
    pSuppConfig->u4AuthPeriod = PNAC_AUTHPERIOD;
    pSuppConfig->u4HeldPeriod = PNAC_HELDPERIOD;
    pSuppConfig->u4StartPeriod = PNAC_STARTPERIOD;
    pSuppConfig->u4MaxStart = PNAC_MAXSTART;
    pSuppConfig->bKeyTxEnabled = PNAC_FALSE;
    pSuppConfig->u2SuppAccessCtrlWithAuth = PNAC_SUPP_ACCESS_CTRL_INACTIVE;
    pPortInfo->u4ProtocolVersion = PNAC_EAPOLVERSION;
    pPortInfo->u2SuppCount = PNAC_INIT_VAL;
    pPortInfo->u2SuppSessionCount = PNAC_INIT_VAL;
    pPortInfo->bSendTagged = PNAC_FALSE;
    pPortInfo->u2PortStatus = PNAC_PORTSTATUS_AUTHORIZED;

    pAuthConfig->u2AuthControlPortControl = PNAC_PORTCNTRL_FORCEAUTHORIZED;
    pPortInfo->u1Capabilities = PNAC_PORT_CAPABILITY_BOTH;
    pPortInfo->u1PortAuthMode = PNAC_PORT_AUTHMODE_PORTBASED;
    pPortInfo->u1PortPaeStatus = PNAC_ENABLED;

    PnacL2IwfSetPortPnacAuthMode (u2PortNum, PNAC_PORT_AUTHMODE_PORTBASED);
    PnacL2IwfSetPortPnacAuthControl (u2PortNum, PNAC_PORTCNTRL_FORCEAUTHORIZED);

    PnacL2IwfGetVlanPortPvid (u2PortNum, &(pPortInfo->u2CurrentVlanId));

    if (pPortInfo->u2CurrentVlanId == PNAC_INIT_VAL)
    {
        pPortInfo->u2CurrentVlanId = PNAC_DEF_VLAN_ID;
    }

    PnacL2IwfGetPortOperEdgeStatus (u2PortNum, &bOperEdge);
    if (bOperEdge == OSIX_TRUE)
    {
        pPortInfo->bBridgeDetected = PNAC_FALSE;
    }
    else
    {
        pPortInfo->bBridgeDetected = PNAC_TRUE;
    }

    PNAC_TRC_ARG1 (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC,
                   "Initialized Port %d Information ... \n", u2PortNum);
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacModuleDisable                                    */
/*                                                                           */
/* Description        : This function disables the PNAC functionality. It    */
/*                      passess 'port-disabled' 'initialize' as events to    */
/*                      the session state machines.                          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSystemInfo.aPnacPaePortInfo                     */
/*                      gPnacSystemInfo.u2SystemAuthControl                  */
/*                      gPnacSystemInfo.aPnacAuthSessionId                   */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacModuleDisable (VOID)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    UINT2               u2PortNum;
    UINT1               u1OperStatus;
    PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC,
              " Disabling PNAC ... \n");

    /* for every Bridge port */
    for (u2PortNum = PNAC_MINPORTS; u2PortNum <= PNAC_MAXPORTS; u2PortNum++)

    {

        /*  check  whether port is invalid */
        if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)

        {
            continue;
        }
        if (pPortInfo->u1EntryStatus == PNAC_PORT_OPER_UP)

        {
            if (PnacGetHLPortStatus (u2PortNum, &u1OperStatus) == PNAC_SUCCESS)

            {
                if (u1OperStatus == CFA_IF_DOWN)

                {

                    /* For all the ports which previously PNAC module has 
                     * given indication as port oper DOWN, now give UP */
                    /* Indicate to the Higher Layers through L2IWF */
                    if (PnacL2IwfPnacHLPortOperIndication (u2PortNum,
                                                           CFA_IF_UP) !=
                        L2IWF_SUCCESS)
                    {
                        continue;
                    }

                }
            }
        }

        if (pPortInfo->u1EntryStatus == PNAC_PORT_OPER_UP)

        {
            pPortInfo->u1EntryStatus = PNAC_PORT_OPER_DOWN;
            if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))

            {
                PnacPaeCdStateMachine (PNAC_CDSM_EV_PORTDISABLED, u2PortNum);
                PnacAuthLinkStatusUpdate (pPortInfo);
            }
            if (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))

            {
                PnacSuppLinkStatusUpdate (pPortInfo);
            }
        }

        /* Make all the ports auth state as authorized */
        PnacL2IwfSetPortPnacAuthStatus (u2PortNum, PNAC_PORTSTATUS_AUTHORIZED);

        pPortInfo->u2PortStatus = PNAC_PORTSTATUS_AUTHORIZED;
        if (pPortInfo->pPortAuthSessionNode != NULL)
        {
            pPortInfo->pPortAuthSessionNode->authFsmInfo.u2AuthControlPortStatus
                = PNAC_PORTSTATUS_AUTHORIZED;
        }

#ifdef NPAPI_WANTED
        if (PNAC_IS_NP_PROGRAMMING_ALLOWED () == PNAC_TRUE)
        {
            if ((PNAC_IS_PORT_MODE_MACBASED (pPortInfo) &&
                 (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl !=
                  PNAC_PORTCNTRL_FORCEAUTHORIZED)))
            {
                /* UnBlock the port in mac based mode */

                if (PnacPnacHwSetAuthStatus (u2PortNum,
                                             NULL,
                                             pPortInfo->u1PortAuthMode,
                                             PNAC_UNBLOCK_PORT,
                                             PNAC_CNTRLD_DIR_BOTH) !=
                    FNP_SUCCESS)

                {
                    PnacNpFailNotifyAndLog (u2PortNum,
                                            NULL,
                                            PNAC_CNTRLD_DIR_BOTH,
                                            pPortInfo->u1PortAuthMode,
                                            PNAC_UNBLOCK_PORT);
                    return;
                }
            }
        }

#endif /* NPAPI_WANTED */

    }                            /* End For u2PortNum */

    PnacServRemoveCache ();

#ifdef NPAPI_WANTED
    /* Disable the hardware only when node is active. */
    if (PNAC_IS_NP_PROGRAMMING_ALLOWED () == PNAC_TRUE)
    {
        if (PnacPnacHwDisable () != FNP_SUCCESS)

        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC, " PnacHwDisable failed !!!\n");
        }
    }

#endif /* NPAPI_WANTED */

    /* set the global system variable 'disabled' */
    PNAC_MODULE_STATUS = PNAC_DISABLED;

    PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC,
              " Disabled PNAC ... \n");

    /* Inform Standby node to clean up all dynamic synced up info. */
    PnacRedSyncUpModOrPortClearInfo (0);
    return;
}                                /*PnacModuleDisable() ends */

/*****************************************************************************/
/* Function Name      : PnacInitVirtualPort                                  */
/*                                                                           */
/* Description        : This function initialises the Port Table Entry       */
/*                      of the specified port.                               */
/*                      Called when port is created                          */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.aPnacPaePortInfo[u2PortNum]          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSystemInfo.aPnacPaePortInfo[u2PortNum]          */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacInitVirtualPort (UINT2 u2PortNum)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacAuthConfigEntry *pAuthConfig = NULL;
    tPnacSuppConfigEntry *pSuppConfig = NULL;
    UINT4               u4ErrCode = 0;

    PNAC_TRC_ARG1 (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC,
                   "Initializing Port %d Information ... \n", u2PortNum);
    pPortInfo = &(PNAC_PORT_INFO (u2PortNum));

    /* Initialise the port entry structure for this port */
    PNAC_MEMSET (pPortInfo, PNAC_INIT_VAL, sizeof (tPnacPaePortEntry));
    pPortInfo->u2PortNo = u2PortNum;
    pAuthConfig = &(pPortInfo->authConfigEntry);
    pSuppConfig = &(pPortInfo->suppConfigEntry);
    pAuthConfig->u4QuietPeriod = PNAC_QUIETPERIOD;
    pAuthConfig->u4TxPeriod = PNAC_TXPERIOD;
    pAuthConfig->u4SuppTimeout = PNAC_SUPPTIMEOUT;
    pAuthConfig->u4ServerTimeout = PNAC_SERVERTIMEOUT;
    pAuthConfig->u4ReAuthPeriod = PNAC_REAUTHPERIOD;
    pAuthConfig->u4MaxReq = PNAC_MAXREQ;
    pAuthConfig->u4ReAuthMax = PNAC_REAUTHMAX;
    pAuthConfig->bReAuthEnabled = PNAC_FALSE;
    pAuthConfig->bKeyTxEnabled = PNAC_FALSE;
    pAuthConfig->u2AdminControlDir = PNAC_CNTRLD_DIR_BOTH;
    pSuppConfig->u4AuthPeriod = PNAC_AUTHPERIOD;
    pSuppConfig->u4HeldPeriod = PNAC_HELDPERIOD;
    pSuppConfig->u4StartPeriod = PNAC_STARTPERIOD;
    pSuppConfig->u4MaxStart = PNAC_MAXSTART;
    pSuppConfig->bKeyTxEnabled = PNAC_FALSE;
    pSuppConfig->u2SuppAccessCtrlWithAuth = PNAC_SUPP_ACCESS_CTRL_INACTIVE;
    pPortInfo->u4ProtocolVersion = PNAC_EAPOLVERSION;
    pPortInfo->u2SuppCount = PNAC_INIT_VAL;
    pPortInfo->u1Capabilities = PNAC_PORT_CAPABILITY_AUTH;
    pPortInfo->u1EntryStatus = PNAC_PORT_OPER_UP;
    pAuthConfig->u2AuthControlPortControl = PNAC_PORTCNTRL_AUTO;

    if (nmhTestv2Dot1xAuthAuthControlledPortControl (&u4ErrCode,
                                                     u2PortNum,
                                                     PNAC_PORTCNTRL_AUTO)
        == SNMP_FAILURE)
    {
        return;
    }

    if (nmhSetDot1xAuthAuthControlledPortControl (u2PortNum,
                                                  PNAC_PORTCNTRL_AUTO)
        == SNMP_FAILURE)
    {
        return;
    }

    if (nmhTestv2FsPnacPaePortAuthMode (&u4ErrCode, u2PortNum,
                                        PNAC_PORT_AUTHMODE_MACBASED)
        == SNMP_FAILURE)
    {
        return;
    }

    if (nmhSetFsPnacPaePortAuthMode (u2PortNum,
                                     PNAC_PORT_AUTHMODE_MACBASED) ==
        SNMP_FAILURE)
    {
        return;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : PnacHandleModuleShutDown                             */
/*                                                                           */
/* Description        : This function gets the Timer submodule               */
/*                      deinitialization, deletes the memory pools and shuts */
/*                      down the Global Memory Manager.                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSystemInfo.authSessNodeMemPoolId,               */
/*                      gPnacSystemInfo.suppSessNodeMemPoolId                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacHandleModuleShutDown (VOID)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacIntfMesg      *pMesg = NULL;
    UINT2               u2PortNum = PNAC_INIT_VAL;

    PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC,
              " Releasing Resources and Shutting down PNAC Module ... \n");

    if (PNAC_SYSTEM_CONTROL == PNAC_SHUTDOWN)
    {
        /* PNAC module is already shutdown. Just return success */
        return;
    }

#ifdef L2RED_WANTED
#ifdef NPAPI_WANTED
    if (PNAC_RED_AUDIT_TSKID () != PNAC_INIT_VAL)
    {
        PNAC_DELETE_TASK (PNAC_RED_AUDIT_TSKID ());
        PNAC_RED_AUDIT_TSKID () = PNAC_INIT_VAL;
    }
    PNAC_RED_IS_AUDIT_STOP () = PNAC_TRUE;
#endif
#endif
    PNAC_SYSTEM_CONTROL = PNAC_SHUTDOWN;
    PNAC_NODE_STATUS () = PNAC_SHUT_START_IN_PROGRESS;
    while (PNAC_RECV_FROM_QUEUE (PNAC_INTF_INPUT_QID,
                                 (UINT1 *) &pMesg,
                                 OSIX_DEF_MSG_LEN,
                                 OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        PNAC_TRC (PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
                  " INF: Message received from Queue ... Processing...\n");

        if (pMesg == NULL)
            continue;

        PNAC_RELEASE_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID, pMesg);
    }
    while (PNAC_RECV_FROM_QUEUE (PNAC_INTF_CFG_QID,
                                 (UINT1 *) &pMesg,
                                 OSIX_DEF_MSG_LEN,
                                 OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        PNAC_TRC (PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
                  "CFGQ: Message received from Queue ... Processing...\n");

        if (pMesg == NULL)
            continue;

        PNAC_RELEASE_MEM_BLOCK (PNAC_INTF_CFGQ_MESG_MEMPOOL_ID, pMesg);
    }

    /* Disable the PNAC module */
    if (PNAC_IS_ENABLED ())
    {
        PNAC_PROTOCOL_ADMIN_STATUS () = PNAC_DISABLED;
        PnacModuleDisable ();
    }

    for (u2PortNum = PNAC_MINPORTS; u2PortNum <= PNAC_MAXPORTS; u2PortNum++)
    {

        /*  check  whether port is invalid */
        if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)

        {
            continue;
        }

        /* Port is Authenticator Capable */
        if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))

        {
            if (PNAC_IS_PORT_MODE_PORTBASED (pPortInfo))

            {
                if (pPortInfo->pPortAuthSessionNode != NULL)

                {
                    if (PNAC_RELEASE_AUTHSESSNODE_MEMBLK
                        (pPortInfo->pPortAuthSessionNode) != PNAC_MEM_SUCCESS)

                    {
                        PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_ALL_FAILURE_TRC |
                                  PNAC_OS_RESOURCE_TRC,
                                  " Auth Node release failed, Continuing...\n");
                    }
                    pPortInfo->pPortAuthSessionNode = NULL;
                }

                else

                {
                    PNAC_TRC_ARG1 (PNAC_INIT_SHUT_TRC | PNAC_ALL_FAILURE_TRC |
                                   PNAC_OS_RESOURCE_TRC,
                                   " No Authenticator Node on Port: %u, "
                                   " Continuing...\n", u2PortNum);
                }
            }
        }

        /* Port is Supplicant Capable */
        if (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))

        {
            if (pPortInfo->pPortSuppSessionNode != NULL)

            {
                if (PNAC_RELEASE_SUPPSESSNODE_MEMBLK
                    (pPortInfo->pPortSuppSessionNode) != PNAC_MEM_SUCCESS)

                {
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                              PNAC_OS_RESOURCE_TRC,
                              " Supplicant Node release failed, "
                              " Continuing...\n");
                }
                pPortInfo->pPortSuppSessionNode = NULL;
            }

            else

            {
                PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                               PNAC_OS_RESOURCE_TRC,
                               " No Supplicant Node on Port: %u, "
                               " Continuing ...\n", u2PortNum);
            }
        }
        pPortInfo->u1EntryStatus = PNAC_PORT_INVALID;
    }

    if (gPnacSystemInfo.u1SystemMode == PNAC_DISTRIBUTED)
    {
        DPnacSetModuleStatus (PNAC_CENTRALIZED);
        gPnacSystemInfo.u1SystemMode = PNAC_CENTRALIZED;
    }

    PnacServDisable ();

    /* Call the PNAC timer shutdown routine */
    PnacTmrDeinit ();

    if (gPnacSystemInfo.pPnacAuthSessionTable != NULL)
    {
        PNAC_HASH_DELETE_TABLE (gPnacSystemInfo.pPnacAuthSessionTable,
                                PnacDeleteAuthSessionHashNode);
        gPnacSystemInfo.pPnacAuthSessionTable = NULL;
    }
    if (gPnacSystemInfo.PnacNpFailedMacSess != NULL)
    {
        RBTreeDelete (gPnacSystemInfo.PnacNpFailedMacSess);
        gPnacSystemInfo.PnacNpFailedMacSess = NULL;
    }

    /* Delete the Memory pools */
    PnacSizingMemDeleteMemPools ();
    PnacRedInitRedSystemInfo ();
#ifdef L2RED_WANTED
    if (PnacRedDeRegisterWithRM () == PNAC_FAILURE)
    {
        PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC,
                  " PNAC DeRegister with RM ... failed. \n");
        return;
    }
#endif

    /* Deregister with packet handler when module is shutdown */
    CfaMuxUnRegisterProtocolCallBack (PACKET_HDLR_PROTO_PNAC);

    PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC,
              " PNAC Shutdown ... Done. \n");
    /*PnacModuleShutdown ends */
    return;
}

/*****************************************************************************/
/* Function Name      : PnacAssignMempoolIds                                 */
/*                                                                           */
/* Description        : This function assign mempools from sz.c ie.          */
/*                      PnacMemPoolIds to global mempoolIds.                 */
/*                                                                           */
/* Input(s)           :  None                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
PnacAssignMempoolIds (VOID)
{
    /*tPnacIntfMesg */
    PNAC_INTFMESG_MEMPOOL_ID
        = PNACMemPoolIds[MAX_PNAC_INTF_QUEUE_MESG_SIZING_ID];

    /* tPnacIntfMesg */
    PNAC_INTF_CFGQ_MESG_MEMPOOL_ID
        = PNACMemPoolIds[MAX_PNAC_CONFIG_QUEUE_MESG_SIZING_ID];

    /* UINT1 */
    PNAC_EAPOL_MESG_MEMPOOL_ID
        = PNACMemPoolIds[MAX_PNAC_EAPOL_FRAMES_SIZING_ID];

    /* PNAC_AUTHSESSNODE_MEMBLK_SIZE */
    PNAC_AUTHSESSNODE_MEMPOOL_ID
        = PNACMemPoolIds[MAX_PNAC_AUTH_SESSION_NODE_SIZING_ID];

    /* PNAC_SUPPSESSNODE_MEMBLK_SIZE */
    PNAC_SUPPSESSNODE_MEMPOOL_ID
        = PNACMemPoolIds[MAX_PNAC_SUPP_SESSION_NODE_SIZING_ID];

    /* PNAC_AUTHSESSNODE_FAIL_MEMBLK_SIZE */
    PNAC_AUTHSESSNODE_FAIL_MEMPOOL_ID
        = PNACMemPoolIds[MAX_PNAC_NP_FAILED_MAC_SESSION_NODE_SIZING_ID];

    /* PNAC_TIMERNODE_MEMBLK_SIZE */
    PNAC_TIMERNODE_MEMPOOL_ID = PNACMemPoolIds[MAX_PNAC_TIMER_BLOCKS_SIZING_ID];

    /* PNAC_SERVUSER_MEMBLK_SIZE */
    PNAC_SERVUSER_MEMPOOL_ID
        = PNACMemPoolIds[MAX_PNAC_SERV_USER_INFO_SIZING_ID];

    /* PNAC_SERVCACHE_MEMBLK_SIZE */
    PNAC_SERVCACHE_MEMPOOL_ID
        = PNACMemPoolIds[MAX_PNAC_AUTH_SERVER_CACHE_SIZING_ID];

    /* PNAC_NOTIFY_OID_MEMBLK_SIZE */
    PNAC_NOTIFY_OID_MEMPOOL_ID = PNACMemPoolIds[MAX_PNAC_OID_LENGTH_SIZING_ID];

    /* PNAC_ENET_EAP_ARRAY_MEMBLK_SIZE */
    PNAC_ENET_EAP_ARRAY_MEMPOOL_ID
        = PNACMemPoolIds[MAX_PNAC_ENET_EAP_ARRAY_SIZE_SIZING_ID];

    /* PNAC_RECV_DATA_MEMBLK_SIZE */
    PNAC_IF_RECV_DATA_MEMPOOL_ID
        = PNACMemPoolIds[MAX_PNAC_IF_RECV_DATA_SIZING_ID];

    /* PNAC_SEND_DATA_MEMBLK_SIZE */
    PNAC_IF_SEND_DATA_MEMPOOL_ID
        = PNACMemPoolIds[MAX_PNAC_IF_SEND_DATA_SIZING_ID];

    /* PNAC_MAX_USER_INFO_MEMBLK_SIZE */
    PNAC_USER_INFO_MEMPOOL_ID = PNACMemPoolIds[MAX_PNAC_USER_INFO_SIZING_ID];

    /* MAX_DPNAC_SLOTS_INFO_MEMBLK_SIZE */
    PNAC_DPNAC_SLOTS_INFO_MEMPOOL_ID =
        PNACMemPoolIds[MAX_DPNAC_SLOTS_INFO_SIZING_ID];

    /* MAX_PNAC_DPNAC_PORTS_INFO_MEMBLK_SIZE */
    PNAC_DPNAC_PORTS_INFO_MEMPOOL_ID =
        PNACMemPoolIds[MAX_DPNAC_PORTS_INFO_SIZING_ID];

    return;

}
#endif /* _PNACINIT_C */
