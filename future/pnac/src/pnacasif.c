/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pnacasif.c,v 1.20 2015/02/10 10:10:30 siva Exp $
 *
 * Description     : This file contains Authentication Server 
 *                   Interface Sub module functions of PNAC module 
 *******************************************************************/

#include "pnachdrs.h"
#include "pnacport.h"

/*****************************************************************************/
/* Function Name      : PnacAsIfSendToServer                                 */
/*                                                                           */
/* Description        : This function transfers the Eap pkt to the PNAC      */
/*                      Local Authentication Server or through a RADIUS      */
/*                      Client to the remote Authentication Server.          */
/*                                                                           */
/* Input(s)           : pu1EapPkt - Start address of Eap pkt,                */
/*                      u2EapPktLength - Eap pkt length,                     */
/*                      pSessNode - authenticator session node,              */
/*                      u2PortNum - port number                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
PnacAsIfSendToServer (UINT1 *pu1EapPkt,
                      UINT2 u2EapPktLength,
                      tPnacAuthSessionNode * pSessNode, UINT2 u2PortNum)
{
    tPnacAsIfSendData  *pAsIfData = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PNAC_NODE_STATUS () != PNAC_ACTIVE_NODE)
    {
        return PNAC_SUCCESS;
    }

    /* validate Eap Pkt Length */
    if (u2EapPktLength > PNAC_MAX_EAP_PKT_SIZE)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_DATA_PATH_TRC |
                  PNAC_CONTROL_PATH_TRC, " ASIF: Oversized Eap Pkt received\n");
        return PNAC_FAILURE;
    }
    pPortInfo = &(PNAC_PORT_INFO (u2PortNum));

    if (PNAC_ALLOCATE_IF_SEND_DATA_MEMBLK (pAsIfData) == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Unable to allocate memory. \r\n");
        return PNAC_FAILURE;
    }

    PNAC_MEMSET (pAsIfData, PNAC_INIT_VAL, sizeof (tPnacAsIfSendData));

    /* Fill the data for Server */
    PNAC_MEMCPY (pAsIfData->au1SuppUserName,
                 pSessNode->authSessStats.au1SessUserName,
                 pSessNode->authSessStats.u2SessUserNameLen);
    pAsIfData->au1SuppUserName[pSessNode->authSessStats.u2SessUserNameLen] =
        '\0';
    pAsIfData->u2SuppUserNameLen =
        (UINT2) PNAC_STRLEN (pAsIfData->au1SuppUserName);

    PNAC_MEMCPY (pAsIfData->au1NasId,
                 gPnacSystemInfo.au1NasId, gPnacSystemInfo.u2NasIdLen);
    pAsIfData->u2NasIdLen = gPnacSystemInfo.u2NasIdLen;

    pAsIfData->u2NasPortNumber = u2PortNum;
    pAsIfData->u1NasPortType = pPortInfo->portIfMap.u1IfType;

    /* The Mac Address is to be passed to the Server, only for the
     * ports that support MAC based authentication Mode.
     */
    if (PNAC_IS_PORT_MODE_MACBASED (pPortInfo))
    {
        PNAC_MEMCPY (pAsIfData->macAddr, pSessNode->rmtSuppMacAddr,
                     PNAC_MAC_ADDR_SIZE);
    }

    PNAC_MEMCPY (pAsIfData->au1EapPkt, pu1EapPkt, u2EapPktLength);
    pAsIfData->u2EapPktLength = u2EapPktLength;

    PNAC_TRC_ARG1 (PNAC_DATA_PATH_TRC | PNAC_CONTROL_PATH_TRC,
                   " ASIF: Transferring Server-data to Auth Server for the user, %s.\n",
                   pAsIfData->au1SuppUserName);

    /* Transfer the Eap Data to local or remote Authentication Server */
    if (PNAC_IS_REMOTE_RADIUS_AS ())
    {
        /* Call RADIUS client function to transfer the Eap Data to
         * RADIUS client
         */
        if (pSessNode->u2RadAttrStateLen != PNAC_INIT_VAL)
        {
            pAsIfData->u2StateLen = pSessNode->u2RadAttrStateLen;
            PNAC_MEMCPY (pAsIfData->au1State,
                         pSessNode->au1RadAttrState,
                         pSessNode->u2RadAttrStateLen);
        }

        if (PNAC_TX_EAP_TO_RADIUS (pAsIfData) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      " ASIF: Tx to RADIUS client failed\n");
            if (PNAC_RELEASE_IF_SEND_DATA_MEMBLK (pAsIfData) == MEM_FAILURE)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                          "Failed to release  memory. \r\n");
            }
            return PNAC_FAILURE;
        }
    }
    else
    {
        /* Call PNAC local Authentication Server or Tacacs+ Server */
        if (PnacServHandleEapPkt (pAsIfData) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      " ASIF: Tx to Local or Tacacs+ AuthServer failed\n");
            if (PNAC_RELEASE_IF_SEND_DATA_MEMBLK (pAsIfData) == MEM_FAILURE)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                          "Failed to release  memory. \r\n");
            }
            return PNAC_FAILURE;
        }
    }
    if (PNAC_RELEASE_IF_SEND_DATA_MEMBLK (pAsIfData) == MEM_FAILURE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Failed to release  memory. \r\n");
    }
    PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_CONTROL_PATH_TRC,
              " ASIF: Tx of Server-data to Auth Server succeeded\n");

    return PNAC_SUCCESS;

}                                /* PnacAsIfSendToServer() ends */

/*****************************************************************************/
/* Function Name      : PnacAsIfRecvFromServer                               */
/*                                                                           */
/* Description        : This function transfers the Eap pkt from RADIUS      */
/*                      Client or Tacacs+ Server or Local Authenticatione    */
/*                      Server to the Backend Sub module of PNAC.            */
/*                                                                           */
/* Input(s)           : pSrvEapData - Pointer to Server Data that contains   */
/*                      the Eap pkt                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : Port number & Source MAC address in gPnacSystemInfo  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : KeyInfo of Authenticator session node in             */
/*                      gPnacSystemInfo                                      */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacAsIfRecvFromServer (tPnacAsIfRecvData * pSrvEapData)
{
    UINT1              *pu1EapPkt = NULL;
    UINT1               au1Key[PNAC_MAX_SERVER_KEY_SIZE];
    UINT2               u2PortNo;
    tMacAddr            suppMacAddr;
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PNAC_ALLOCATE_ENET_EAP_ARRAY_MEMBLK (pu1EapPkt) == NULL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PnacAsIfRecvFromServer: Allocate pu1EapPkt returned failure\n");
        return;
    }

    /* Initializing to Zero */
    PNAC_MEMSET (pu1EapPkt, PNAC_INIT_VAL, PNAC_MAX_EAP_PKT_SIZE);
    PNAC_MEMSET (au1Key, PNAC_INIT_VAL, PNAC_MAX_SERVER_KEY_SIZE);

    /* Extracting the EapPkt */
    PNAC_MEMCPY (pu1EapPkt, pSrvEapData->au1EapPkt,
                 pSrvEapData->u2EapPktLength);

    PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_CONTROL_PATH_TRC,
              " ASIF: Handing over Server frame to Authenticator\n");
    /* Hand over the Eap pkt to Authenticator */
    if (PnacAuthHandleInServerFrame (pu1EapPkt, pSrvEapData->u2EapPktLength,
                                     pSrvEapData->u2NasPortNumber,
                                     pSrvEapData->macAddr,
                                     pSrvEapData->u4SessionTimeout)
        != PNAC_SUCCESS)
    {
        if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1EapPkt) == MEM_FAILURE)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      "Failed to release  memory. \r\n");
        }
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " ASIF: Authenticator handling of Server frame failed\n");
        return;
    }

    if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1EapPkt) == MEM_FAILURE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Failed to release  memory. \r\n");
    }

    pPortInfo = &(PNAC_PORT_INFO (pSrvEapData->u2NasPortNumber));
    if (PNAC_IS_PORT_MODE_PORTBASED (pPortInfo))
    {
        pAuthSessNode = pPortInfo->pPortAuthSessionNode;
    }
    else
    {
        /* Do verify the MAC address and Get the Session Node */
        if ((PnacAuthGetAuthInProgressTblEntryByMac (pSrvEapData->macAddr,
                                                     &pAuthSessNode))
            != PNAC_SUCCESS)
        {
            if ((PnacAuthGetSessionTblEntryByMac (pSrvEapData->macAddr,
                                                  &pAuthSessNode))
                != PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                          " ASIF: MAC Address From Server Not Found In the Table\n");
                return;
            }
        }
    }
    if (pSrvEapData->u2StateLen != PNAC_INIT_VAL)
    {
        pAuthSessNode->u2RadAttrStateLen = pSrvEapData->u2StateLen;
        PNAC_MEMCPY (pAuthSessNode->au1RadAttrState, pSrvEapData->au1State,
                     pSrvEapData->u2StateLen);
    }
    else
    {
        pAuthSessNode->u2RadAttrStateLen = PNAC_INIT_VAL;
        PNAC_MEMSET (pAuthSessNode->au1RadAttrState, PNAC_INIT_VAL,
                     PNAC_MAX_STATE_ATTRIB_LEN);
    }

    /* if no Server Key is received, return */
    if (pSrvEapData->u2ServerKeyLength == PNAC_NO_VAL)
    {
        PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_CONTROL_PATH_TRC,
                  " ASIF: Received No Key from Server\n");
        return;
    }

    PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_CONTROL_PATH_TRC,
              " ASIF: Key is received from the Server\n");

    /* setting TRUE the boolean, informing that New Key is Available */
    pAuthSessNode->authFsmInfo.bKeyAvailable = PNAC_TRUE;

    /* getting the Port number & Supplicant's MAC address */
    u2PortNo = pAuthSessNode->u2PortNo;
    PNAC_MEMCPY (suppMacAddr, pAuthSessNode->rmtSuppMacAddr,
                 PNAC_MAC_ADDR_SIZE);

    /* Extracting the Key */
    PNAC_MEMCPY (au1Key, pSrvEapData->au1ServerKey,
                 pSrvEapData->u2ServerKeyLength);

    PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_CONTROL_PATH_TRC,
              " ASIF: Storing the Key in Authenticator's session node...\n");

    /* storing the Key in Authenticator's session node */
    pAuthSessNode->keyInfo.u2KeyLength = pSrvEapData->u2ServerKeyLength;

    PNAC_MEMCPY (pAuthSessNode->keyInfo.au1Key,
                 au1Key, pSrvEapData->u2ServerKeyLength);

    PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_CONTROL_PATH_TRC,
              " ASIF: Transmitting the Key to Supplicant...\n");

    /* Transmit the Key to the Supplicant */
    if (PnacKeyAuthTxKey (au1Key,
                          pSrvEapData->u2ServerKeyLength,
                          u2PortNo, suppMacAddr) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_DATA_PATH_TRC |
                  PNAC_CONTROL_PATH_TRC,
                  " ASIF: Tx of Key to Supplicant failed\n");
        return;
    }

    PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_CONTROL_PATH_TRC,
              " ASIF: Key successfully transmitted to the Supplicant\n");

#ifdef RSNA_WANTED
    RsnaApiNotifyRsnaOfKeyAvailable (u2PortNo, suppMacAddr, au1Key);
#endif

    return;

}                                /* PnacAsIfRecvFromServer() ends */

/*****************************************************************************/
/* Function Name      : PnacRadiusTxEapPkt                                   */
/*                                                                           */
/* Description        : This routine is 'EXPECTED' by PNAC module for its    */
/*                      transmission of the Eap pkt to outside Radius Client */
/*                      which in turn will transmit to remote Authentication */
/*                      Server.                                              */
/*                                                                           */
/* Input(s)           : pSuppData - pointer to Radius Client Send data       */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
PnacRadiusTxEapPkt (tPnacAsIfSendData * pAsData)
{

    tRADIUS_INPUT_AUTH  radInputAuth;
    tUSER_INFO_EAP     *puserInfoEap = NULL;
    INT4		i4Ret = 0;

    /* Call RADIUS client function to transfer the Eap Data to
     * RADIUS client
     */
    if (PNAC_ALLOCATE_USER_INFO_DATA_MEMBLK (puserInfoEap) == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Unable to allocate memory. \r\n");
        return PNAC_FAILURE;
    }
    PNAC_MEMSET (&radInputAuth, PNAC_INIT_VAL, sizeof (tRADIUS_INPUT_AUTH));
    PNAC_MEMSET (puserInfoEap, PNAC_INIT_VAL, sizeof (tUSER_INFO_EAP));

    radInputAuth.u1_ProtocolType = PRO_EAP;
    radInputAuth.p_UserInfoEAP = puserInfoEap;
    radInputAuth.u4_NasPort = (UINT4) pAsData->u2NasPortNumber;
    if (pAsData->u1NasPortType == PNAC_ENET_PORT)
    {
        radInputAuth.u4_NasPortType = PNAC_RAD_ENET_PORT;
    }
    else if (pAsData->u1NasPortType == PNAC_WIRELESSLAN_PORT)
    {
        radInputAuth.u4_NasPortType = PNAC_RAD_WLAN_PORT;
    }
    else
    {
        radInputAuth.u4_NasPortType = NO_ATTRIBUTE;
    }
    PNAC_MEMCPY (radInputAuth.a_u1NasId, pAsData->au1NasId,
                 pAsData->u2NasIdLen);
    if (pAsData->u2StateLen != PNAC_INIT_VAL)
    {
        PNAC_MEMCPY (radInputAuth.a_u1State, pAsData->au1State,
                     pAsData->u2StateLen);
    }
    radInputAuth.u1_StateLen = (UINT1) pAsData->u2StateLen;

    /* Need to send the user name always, it is one of the
     * attributes send by radius client ot radius server */
    PNAC_MEMCPY (puserInfoEap->a_u1UserName,
                 pAsData->au1SuppUserName, pAsData->u2SuppUserNameLen);

    PNAC_MEMCPY (puserInfoEap->a_u1Response,
                 pAsData->au1EapPkt, pAsData->u2EapPktLength);
    if (PNAC_RELEASE_USER_INFO_DATA_MEMBLK (puserInfoEap) == MEM_FAILURE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Failed to release  memory. \r\n");
    }

    if (gu1ReqId < 255)
    {
        gu1ReqId++;
    }
    else
    {
        gu1ReqId = 1;
    }
    gPnacRadReq[gu1ReqId].u1Status = PNAC_TRUE;

    PNAC_MEMCPY (radInputAuth.au1StaMac, pAsData->macAddr,
                 PNAC_MAC_ADDR_SIZE);

    if ( RadApiHandleRadAuthCB ( &radInputAuth, NULL,
			         pAsData->u2NasPortNumber, gu1ReqId, &i4Ret)  == NULL ) 
    {
       if ((PnacRadiusAuthentication (&radInputAuth, NULL,
        		              PnacHandleRadResponse,
				      pAsData->u2NasPortNumber, gu1ReqId)) ==
			              RAD_FAILURE)
       {
         PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
   	          " ASIF: Radius Authentication returned failure\n");
         return PNAC_FAILURE;
       }
    }
    PNAC_MEMCPY (gPnacRadReq[gu1ReqId].macAddr, pAsData->macAddr,
                 PNAC_MAC_ADDR_SIZE);

    PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC,
                   " ASIF: Radius returned REQUEST_ID: %2x", gu1ReqId);
    UNUSED_PARAM (i4Ret);
    return PNAC_SUCCESS;

}                                /*RadiusTxEapPkt */

/*****************************************************************************/
/* Function Name      : PnacTacacsplusProcessResponseMd5                     */
/*                                                                           */
/* Description        : This function processes the Eap response challenge   */
/*                      pkt & sends responses                                */
/*                                                                           */
/* Input(s)           : pAuthData - pointer to Server's input data           */
/*                      pCacheNode - cache node for the User                 */
/*                      pEapDesc - descriptor of EAP challenge response pkt  */
/*                                                                           */
/* Output(s)          : pAuthReply - pointer to Server's transmit data       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacServInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacServInfo                                        */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacTacacsplusProcessResponseMd5 (tPnacAsIfSendData * pAuthData,
                                  tPnacServAuthCache * pCacheNode,
                                  tPnacEapDesc * pEapDesc)
{
    tPnacSLLNode       *pNode = NULL;
    INT4                i4Return = PNAC_SUCCESS;
    UINT1               u1ReqId = 0;

    u1ReqId = pEapDesc->u1Id;

    gPnacTacReq[u1ReqId].u1Status = PNAC_TRUE;

    gPnacTacReq[u1ReqId].u2NasPortNumber = pAuthData->u2NasPortNumber;

    PNAC_MEMCPY (gPnacTacReq[u1ReqId].macAddr, pAuthData->macAddr,
                 PNAC_MAC_ADDR_SIZE);

    if (PnacTacacsplusAuthentication (pCacheNode->au1Chal,
                                      pCacheNode->u2ChalLen,
                                      pEapDesc, u1ReqId) != PNAC_SUCCESS)
    {
        i4Return = PNAC_FAILURE;
        PNAC_TRC_ARG3 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                       " AS: Sending Failure for user: %s, Id: %d, Port: %d\n",
                       pCacheNode->au1Name, pEapDesc->u1Id,
                       pAuthData->u2NasPortNumber);
    }

    pNode = &pCacheNode->nextNode;
    if (pCacheNode->pTmrNode != NULL)
    {
        if (PnacTmrStopTmr (pCacheNode, PNAC_SERVERCACHE_TIMER) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " AS: Stop Cache Timer Failed... anyway continuing..\n");
        }
    }

    PNAC_SLL_DELETE (&PNAC_SERV_CACHE_LIST, pNode);
    PNAC_DEBUG (PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                               " SLL Cache Node deleted : %x\n", pNode);
        );
    if (PNAC_RELEASE_SERVCACHE_MEMBLK (pCacheNode) != PNAC_MEM_SUCCESS)
    {
        i4Return = PNAC_FAILURE;
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " AS: Releasing Cache Node failed\n");
        return PNAC_FAILURE;
    }
    PNAC_TRC_ARG3 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                   " AS: Sending Success for user: %s, Id: %d, Port: %d\n",
                   pCacheNode->au1Name, pEapDesc->u1Id,
                   pAuthData->u2NasPortNumber);
    return i4Return;
}

/*****************************************************************************/
/* Function Name      : PnacTacacsplusAuthentication                         */
/*                                                                           */
/* Description        : This routine send UserName and Challenge to Tacacs+  */
/*                      Server for authentication                            */
/*                                                                           */
/* Input(s)           : ppu1Chal   - pointer to Challenge Value              */
/*                      u2ChalLen  - Challenge Length                        */
/*                      pEapDesc   - pointer to EAP                          */
/*                      u1ReqId    - Request Id                              */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - if authenticated                      */
/*                      PNAC_FAILURE - if fail to authneticate               */
/*****************************************************************************/

INT4
PnacTacacsplusAuthentication (UINT1 *pu1Chal, UINT2 u2ChalLen,
                              tPnacEapDesc * pEapDesc, UINT1 u1ReqId)
{
#ifdef TACACS_WANTED
    tTacMsgInput       *pTacMsgInput;
    tTacAuthenInput    *pTacAuthenInput;
    INT4                i4Ret = 0;
    UINT1               au1Port[] = "login_auth";

    gPnacIntfTaskId = OsixGetCurTaskId ();

    if (pu1Chal == NULL || gPnacIntfTaskId == 0)
    {
        return PNAC_FAILURE;
    }
    if (u2ChalLen > TAC_CHAP_CHALLENGE_LEN)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  " Oversized CHAP pkt. \r\n");
        return PNAC_FAILURE;
    }

    if (PNAC_ALLOCATE_TACMSGINP_MEMBLK (pTacMsgInput) == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Unable to allocate memory. \r\n");
        return PNAC_FAILURE;
    }

    MEMSET (pTacMsgInput, 0, sizeof (tTacMsgInput));

    pTacMsgInput->u4MsgType = TAC_AUTHEN;
    pTacAuthenInput = &(pTacMsgInput->TacInput.AuthenInput);

    pTacAuthenInput->u1Action = TAC_AUTHEN_LOGIN;
    pTacAuthenInput->u1PrivilegeLevel = 15;
    pTacAuthenInput->u1Service = TAC_AUTHEN_SVC_LOGIN;
    pTacAuthenInput->u1AuthenType = TAC_AUTHEN_TYPE_CHAP;
    STRNCPY (pTacAuthenInput->au1Port, au1Port, STRLEN(au1Port));
    pTacAuthenInput->au1Port[STRLEN(au1Port)] = '\0';

    STRNCPY (pTacAuthenInput->au1UserName, pEapDesc->mesgDigest.pu1Name,
             sizeof (pTacAuthenInput->au1UserName));
    pTacAuthenInput->UserInfo.UserCHAP.u1Identifier = pEapDesc->u1Id;

    /* The challenge value is not a null terminated string in the   *
     *  cache node, so while copying the challenge value we are not *
     *  supposed to use STRCPY.                                     */
    MEMCPY (pTacAuthenInput->UserInfo.UserCHAP.au1Challenge,
            pu1Chal, u2ChalLen);
    MEMCPY (pTacAuthenInput->UserInfo.UserCHAP.au1Response,
            pEapDesc->mesgDigest.pu1ValPtr, TAC_CHAP_RESPONSE_LEN);

    pTacAuthenInput->u4AppReqId = u1ReqId;
    pTacAuthenInput->AppInfo.fpAppCallBackFn =
        (VOID *) PnacHandleTacacsPlusResponse;
    pTacAuthenInput->AppInfo.TaskId = gPnacIntfTaskId;

    i4Ret = TacacsPlusProcessPacket (pTacMsgInput);

    if (i4Ret != TAC_CLNT_RES_OK)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  " Unable to send Tacacs pkts. \r\n");
        return PNAC_FAILURE;
    }
#else
    UNUSED_PARAM (pu1Chal);
    UNUSED_PARAM (pEapDesc);
    UNUSED_PARAM (u2ChalLen);
    UNUSED_PARAM (u1ReqId);
#endif /*TACACS_WANTED */

    return PNAC_SUCCESS;
}
