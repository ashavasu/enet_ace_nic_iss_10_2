/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/* $Id: pnacpae.c,v 1.78 2017/09/25 12:15:09 siva Exp $                     */
/*****************************************************************************/
/*    FILE  NAME            : pnacpae.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : PNAC PAE sub module                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Mar 2002                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains PAE Sub module functions    */
/*                            of PNAC module                                 */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 MAR 2002 / BridgeTeam   Initial Create.                     */
/* 1.0.0.1    29 NOV 2002 / Products     Modifying NP-API calls              */
/*---------------------------------------------------------------------------*/

#include "pnachdrs.h"
#ifdef RSNA_WANTED
#include "rsna.h"
#endif

/*****************************************************************************/
/* Function Name      : PnacPaeCdStateMachine                                */
/*                                                                           */
/* Description        : This function performs the controlled directions     */
/*                      state machine                                        */
/*                                                                           */
/* Input(s)           : u1Event - Event occured                              */
/*                                Values : PNAC_CDSM_EV_INITIALISE           */
/*                                         PNAC_CDSM_EV_ADMINCHANGED         */
/*                                         PNAC_CDSM_EV_PORTENABLED          */
/*                                         PNAC_CDSM_EV_PORTDISABLED         */
/*                                         PNAC_CDSM_EV_BRGDETECTED          */
/*                                         PNAC_CDSM_EV_BRGNOTDETECTED       */
/*                      u2PortNum - Port Number                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or                                      */
/*                      PNAC_FAILURE                                         */
/*****************************************************************************/
INT4
PnacPaeCdStateMachine (UINT1 u1Event, UINT2 u2PortNum)
{
    tPnacAuthConfigEntry *pAuthConfig = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;
    UINT2               u2OldOperControlDir;

    UINT1               aau1SemEvents[PNAC_MAX_CDSM_EVENTS][20] =
        { "INITIALIZE", "ADMINCHANGED", "PORTENABLED",
        "PORTDISABLED", "BRGDETECTED", "BRGNOTDETECTED"
    };
    UINT1               aau1SemStates[PNAC_MAX_CDSM_STATES][20] =
        { "FORCEBOTH", "IN_OR_BOTH" };

    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  "PnacPaeCdStateMachine:Invalid" "Port Number\n");
        return PNAC_FAILURE;
    }

    PNAC_TRC_ARG3 (PNAC_CONTROL_PATH_TRC,
                   " CD Machine called for Port: %u with Event: %s, "
                   "State: %s\n",
                   u2PortNum, aau1SemEvents[u1Event],
                   aau1SemStates[pPortInfo->u1CtrlDirState]);

    pAuthConfig = &(PNAC_AUTH_CONFIG_ENTRY (pPortInfo));
    u2OldOperControlDir = pAuthConfig->u2OperControlDir;

    if (u1Event == PNAC_CDSM_EV_INITIALIZE)
    {

        if ((pPortInfo->u1EntryStatus == PNAC_PORT_OPER_UP) &&
            (pPortInfo->bBridgeDetected == PNAC_FALSE))
        {
            pAuthConfig->u2OperControlDir = pAuthConfig->u2AdminControlDir;
            pPortInfo->u1CtrlDirState = PNAC_CDSM_STATE_IN_OR_BOTH;

            PnacL2IwfSetPortPnacControlDir (u2PortNum,
                                            pAuthConfig->u2AdminControlDir);
        }
        else
        {
            pAuthConfig->u2OperControlDir = PNAC_CNTRLD_DIR_BOTH;
            pPortInfo->u1CtrlDirState = PNAC_CDSM_STATE_FORCEBOTH;

            PnacL2IwfSetPortPnacControlDir (u2PortNum, PNAC_CNTRLD_DIR_BOTH);
        }
    }
    else
    {
        switch (pPortInfo->u1CtrlDirState)

        {
            case PNAC_CDSM_STATE_FORCEBOTH:
                if (u1Event == PNAC_CDSM_EV_PORTENABLED)
                {
                    if (pPortInfo->bBridgeDetected == PNAC_FALSE)
                    {
                        pAuthConfig->u2OperControlDir =
                            pAuthConfig->u2AdminControlDir;
                        pPortInfo->u1CtrlDirState = PNAC_CDSM_STATE_IN_OR_BOTH;

                        PnacL2IwfSetPortPnacControlDir (u2PortNum,
                                                        pAuthConfig->
                                                        u2AdminControlDir);
                    }
                    else
                    {

                        /* Bridge is not detected yet - so nothing to do now */
                        PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                                  " Successfully exiting CD Machine...\n");
                        return PNAC_SUCCESS;
                    }
                }
                else if (u1Event == PNAC_CDSM_EV_BRGNOTDETECTED)
                {

                    if (pPortInfo->u1EntryStatus == PNAC_PORT_OPER_UP)
                    {
                        if (pAuthConfig->u2OperControlDir !=
                            pAuthConfig->u2AdminControlDir)
                        {
                            pAuthConfig->u2OperControlDir =
                                pAuthConfig->u2AdminControlDir;
                        }
                        PnacL2IwfSetPortPnacControlDir (u2PortNum,
                                                        pAuthConfig->
                                                        u2AdminControlDir);

                        /* If Control Direction changes from BOTH to IN and 
                         * the port is Unauthorized then it should give UP 
                         * indication to HL
                         */
                        if (PNAC_IS_PORT_MODE_PORTBASED (pPortInfo))
                        {
                            if (pPortInfo->u2PortStatus ==
                                PNAC_PORTSTATUS_UNAUTHORIZED)
                            {
                                /* Indicate to the Higher Layers through 
                                 * L2IWF */
                                PnacAuthPortOperIndication (u2PortNum);
                            }
                        }
                        pPortInfo->u1CtrlDirState = PNAC_CDSM_STATE_IN_OR_BOTH;
                    }
                }

                else
                {

                    /* The event can be Bridge Detected or Admin 
                     * changed - Nothing to do for now
                     */
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                              " Successfully exiting CD Machine...\n");
                    return PNAC_SUCCESS;
                }
                break;
            case PNAC_CDSM_STATE_IN_OR_BOTH:
                if (u1Event == PNAC_CDSM_EV_ADMINCHANGED)
                {
                    if (pAuthConfig->u2OperControlDir !=
                        pAuthConfig->u2AdminControlDir)
                    {
                        if ((pPortInfo->u1EntryStatus == PNAC_PORT_OPER_DOWN) ||
                            (pPortInfo->bBridgeDetected == PNAC_TRUE))
                        {
                            pAuthConfig->u2OperControlDir =
                                PNAC_CNTRLD_DIR_BOTH;
                            pPortInfo->u1CtrlDirState =
                                PNAC_CDSM_STATE_FORCEBOTH;
                            PnacL2IwfSetPortPnacControlDir
                                (u2PortNum, PNAC_CNTRLD_DIR_BOTH);
                        }
                        else
                        {
                            pAuthConfig->u2OperControlDir =
                                pAuthConfig->u2AdminControlDir;
                            PnacL2IwfSetPortPnacControlDir (u2PortNum,
                                                            pAuthConfig->
                                                            u2AdminControlDir);
                        }

                        /* 1. If Control Direction changes from IN to BOTH and 
                         * the port is Unauthorized then it should give DOWN 
                         * indication to HL
                         * 2. If Control Direction changes from BOTH to IN and 
                         * the port is Unauthorized then it should give UP 
                         * indication to HL
                         */
                        if (PNAC_IS_PORT_MODE_PORTBASED (pPortInfo))
                        {
                            if ((pPortInfo->u1EntryStatus ==
                                 PNAC_PORT_OPER_UP) &&
                                (pPortInfo->u2PortStatus ==
                                 PNAC_PORTSTATUS_UNAUTHORIZED))
                            {

                                /* Indicate to the Higher Layers through 
                                 * L2IWF */
                                PnacAuthPortOperIndication (u2PortNum);
                            }
                        }
                    }
                }
                else if (u1Event == PNAC_CDSM_EV_PORTDISABLED)
                {
                    pAuthConfig->u2OperControlDir = PNAC_CNTRLD_DIR_BOTH;
                    pPortInfo->u1CtrlDirState = PNAC_CDSM_STATE_FORCEBOTH;
                    PnacL2IwfSetPortPnacControlDir (u2PortNum,
                                                    PNAC_CNTRLD_DIR_BOTH);

                }
                else if (u1Event == PNAC_CDSM_EV_BRGDETECTED)
                {
                    if (pAuthConfig->u2OperControlDir == PNAC_CNTRLD_DIR_IN)
                    {
                        pAuthConfig->u2OperControlDir = PNAC_CNTRLD_DIR_BOTH;
                        PnacL2IwfSetPortPnacControlDir (u2PortNum,
                                                        PNAC_CNTRLD_DIR_BOTH);
                        if (PNAC_IS_PORT_MODE_PORTBASED (pPortInfo))
                        {
                            if ((pPortInfo->u1EntryStatus ==
                                 PNAC_PORT_OPER_UP) &&
                                (pPortInfo->u2PortStatus ==
                                 PNAC_PORTSTATUS_UNAUTHORIZED))
                            {

                                /* Indicate to the Higher Layers through L2IWF */
                                PnacAuthPortOperIndication (u2PortNum);
                            }
                        }
                    }
                    pPortInfo->u1CtrlDirState = PNAC_CDSM_STATE_FORCEBOTH;
                }
                else
                {
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              " CDSM: Invalid Event\n");
                    return PNAC_FAILURE;
                }
                break;
            default:
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " CDSM: Error: Unknown CDSM State \n");
                return PNAC_FAILURE;
                break;
        }
    }
    if (PNAC_IS_ENABLED () &&
        (u2OldOperControlDir != pAuthConfig->u2OperControlDir))
    {
        if ((gPnacSystemInfo.u1SystemMode == PNAC_DISTRIBUTED) &&
            (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl ==
             PNAC_PORTCNTRL_AUTO))
        {
            PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC,
                           "DPNAC: PnacPaeCdStateMachine triggered DPNAC update"
                           "for Port (%d)\n", u2PortNum);

            if (DPnacSendEventUpdateMsg (u2PortNum) == PNAC_FAILURE)

            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                          "DPNAC:PnacPaeCdStateMachine DPNAC frame transmission failed ");
                return PNAC_FAILURE;
            }
            return PNAC_SUCCESS;
        }

#ifdef NPAPI_WANTED
        if (PNAC_IS_NP_PROGRAMMING_ALLOWED () == PNAC_TRUE)
        {
            if (PNAC_IS_PORT_MODE_PORTBASED (pPortInfo))
            {
                if (PnacPnacHwSetAuthStatus (u2PortNum, NULL,
                                             (UINT1)
                                             PNAC_PORT_AUTHMODE_PORTBASED,
                                             (UINT1) pPortInfo->u2PortStatus,
                                             (UINT1) pAuthConfig->
                                             u2OperControlDir) != FNP_SUCCESS)
                {
                    PnacNpFailNotifyAndLog (u2PortNum, NULL,
                                            (UINT1)
                                            pAuthConfig->u2OperControlDir,
                                            (UINT1)
                                            PNAC_PORT_AUTHMODE_PORTBASED,
                                            (UINT1) pPortInfo->u2PortStatus);

                    return PNAC_FAILURE;
                }
            }
        }                        /* Node Active check. */

#endif /* NPAPI_WANTED */
    }
    PNAC_TRC (PNAC_CONTROL_PATH_TRC, " Successfully exiting CD Machine...\n");
    return PNAC_SUCCESS;
}                                /* PnacPaeCdStateMachine */

/*****************************************************************************/
/* Function Name      : PnacPaeBridgeDetectionStatus                         */
/*                                                                           */
/* Description        : This function is called by an external Bridge        */
/*                      Detection State Machine, whenever it detects another */
/*                      Bridge on the remote end of this port.               */
/*                                                                           */
/* Input(s)           : u1Status - Status: PNAC_BRIDGE_DETECTED              */
/*                                     or PNAC_BRIDGE_NOT_DETECTED           */
/*                      u2PortNum - Port Number                              */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacPaeBridgeDetectionStatus (UINT1 u1Status, UINT2 u2PortNum)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacDPnacPktInfo   DPnacPktInfo;
    PNAC_TRC_ARG2 (PNAC_CONTROL_PATH_TRC,
                   " Updating Bridge Detection Status (%u) for Port %u ... \n",
                   u1Status, u2PortNum);

    PNAC_MEMSET (&DPnacPktInfo, PNAC_INIT_VAL, sizeof (tPnacDPnacPktInfo));

    if ((gPnacSystemInfo.u1SystemMode == PNAC_DISTRIBUTED) &&
        (gPnacSystemInfo.u1DPnacRolePlayed == PNAC_DPNAC_SYSTEM_ROLE_MASTER))
    {
        if (DPnacCheckIsLocalPort ((UINT2) u2PortNum) == PNAC_FAILURE)
        {
            DPnacPktInfo.u1PktType = DPNAC_BRIDGE_STATUS_PDU;
            DPnacPktInfo.u2NasPortNum = u2PortNum;
            DPnacPktInfo.u1BridgeDetectionStatus = u1Status;

            if (DPnacPaeTxEapPkt (&DPnacPktInfo) != PNAC_SUCCESS)
            {
                return PNAC_FAILURE;
            }
        }
    }

    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PAE: Port is not created yet Aborting operation ...\n");

        return PNAC_FAILURE;
    }
    if (!PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                  " Authenticator Role is not supported on this port, Hence "
                  " skiping updation... \n");
        return PNAC_SUCCESS;
    }
    if (u1Status == PNAC_BRIDGE_DETECTED)
    {
        pPortInfo->bBridgeDetected = PNAC_TRUE;
        if (PnacPaeCdStateMachine (PNAC_CDSM_EV_BRGDETECTED, u2PortNum)
            != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " PAE: CDSM returned failure\n");
            return PNAC_FAILURE;
        }
        PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                  " Updated Bridge detection status as BRIDGE_DETECTED... \n");
        return PNAC_SUCCESS;
    }
    else if (u1Status == PNAC_BRIDGE_NOT_DETECTED)
    {
        pPortInfo->bBridgeDetected = PNAC_FALSE;
        if (PnacPaeCdStateMachine (PNAC_CDSM_EV_BRGNOTDETECTED, u2PortNum)
            != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " PAE: CDSM returned failure\n");
            return PNAC_FAILURE;
        }
        PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                  " Updated Bridge detection status as BRIDGE_NOT_DETECTED... \n");
        return PNAC_SUCCESS;
    }
    else
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PAE: Invalid Bridge Status Update value "
                  " Aborting operation ...\n");
        return PNAC_FAILURE;
    }

}                                /* PnacPaeBridgeDetectionStatus */

/*****************************************************************************/
/* Function Name      : PnacHandleBridgeDetected                             */
/*                                                                           */
/* Description        : This function is called whenever it detects another  */
/*                      Bridge on the remote end of this port.               */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number                              */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacHandleBridgeDetected (UINT2 u2PortNum)
{
    if (PnacPaeBridgeDetectionStatus (PNAC_BRIDGE_DETECTED, u2PortNum)
        != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  "PNAC Handle Bridge Detected returned failure\n");
        return PNAC_FAILURE;
    }
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacProcPortStatUpdEvent                             */
/*                                                                           */
/* Description        : Called when PNAC Interface Task receives the port    */
/*                      operational status update event from lower layer.    */
/*                      This routine intimates the authenticator and         */
/*                      supplicant submodules about the operational status   */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index for which this status is */
/*                                  intimated                                */
/*                      u1OperStatus - Operational status of the port        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PnacProcPortStatUpdEvent (UINT2 u2PortNum, UINT1 u1OperStatus)
{
    tPnacDPnacConsPortEntry *pConsPortEntry = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;
    UINT4               u4PortBrgIndex = 0;
    UINT4               u4SlotId = 0;
    INT4                i4Val = PNAC_FAILURE;
    tCfaIfInfo          IfInfo;

    PNAC_MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    /* Handle Port up/down events only when the pnac module is enabled */
    if (!PNAC_IS_ENABLED ())
    {
        return PNAC_FAILURE;
    }

    PNAC_TRC_ARG2 (PNAC_CONTROL_PATH_TRC,
                   " Updating Port Operational Status (%u) for Port %u ... \n",
                   u1OperStatus, u2PortNum);

    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PAE: Port is not created yet, Aborting operation ...\n");
        return PNAC_FAILURE;
    }
    switch (u1OperStatus)

    {
        case PNAC_PORT_OPER_UP:

#ifdef RSNA_WANTED
            /*For CAPWAP interfaces when Rsna is enabled , MAC based
             * authentication Mode should be followed */
            if ((pPortInfo->portIfMap.u1IfType == CFA_CAPWAP_DOT11_BSS)
                && (WssGetIsRsnaAuthentication (u2PortNum) == OSIX_SUCCESS))
            {
                PNAC_AUTH_CONFIG_ENTRY (pPortInfo).
                    u2AuthControlPortControl = PNAC_PORTCNTRL_AUTO;

                pPortInfo->u1Capabilities = PNAC_PORT_CAPABILITY_AUTH;
                pPortInfo->u1PortAuthMode = PNAC_PORT_AUTHMODE_MACBASED;
                PnacL2IwfSetPortPnacAuthControl (u2PortNum,
                                                 PNAC_PORTCNTRL_AUTO);
                PnacL2IwfSetPortPnacAuthMode (u2PortNum,
                                              PNAC_PORT_AUTHMODE_MACBASED);

                /* Getting necessary info from the Common database */
                CfaGetIfInfo (u2PortNum, &IfInfo);
                pPortInfo->portIfMap.u1IfType = IfInfo.u1IfType;

                PNAC_MEMCPY (pPortInfo->portIfMap.au1MacAddr, IfInfo.au1MacAddr,
                             PNAC_MAC_ADDR_SIZE);

            }
#else
            UNUSED_PARAM (IfInfo);
#endif

            /* Operational Status is UP */
            if (pPortInfo->u1EntryStatus == PNAC_PORT_OPER_DOWN)
            {
                pPortInfo->u1EntryStatus = PNAC_PORT_OPER_UP;

                if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).
                    u2AuthControlPortControl == PNAC_PORTCNTRL_FORCEAUTHORIZED)
                {
                    /* PnacIsPortStatusChanged check is not needed
                     * here as its already called when port status is 
                     * changed to AUTHORISED/UNAUTHORISED.*/
                    PnacL2IwfSetPortPnacAuthStatus (u2PortNum,
                                                    pPortInfo->u2PortStatus);
                }
                i4Val = PNAC_SUCCESS;
                PnacRedSyncUpPortOperUpStatus ((UINT4) u2PortNum);
            }

            break;
        case PNAC_PORT_OPER_DOWN:

            /* Operational Status is DOWN */
            if (pPortInfo->u1EntryStatus == PNAC_PORT_OPER_UP)
            {
                /* If Port Auth control is Auto giving Unauthorized 
                 * indication to L2Iwf is taken care by the State machine */
                if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).
                    u2AuthControlPortControl == PNAC_PORTCNTRL_FORCEAUTHORIZED)
                {
                    PnacL2IwfSetPortPnacAuthStatus
                        (u2PortNum, PNAC_PORTSTATUS_UNAUTHORIZED);
                }
                pPortInfo->u1EntryStatus = PNAC_PORT_OPER_DOWN;
                i4Val = PNAC_SUCCESS;

                PnacRedSyncUpModOrPortClearInfo (u2PortNum);
            }
#ifdef RSNA_WANTED
            RsnaHandleBSSIfDeletion ((UINT4) u2PortNum);
#endif
            break;
        default:
            break;
    }
    if (i4Val != PNAC_SUCCESS)
    {

        /* Operational Status is INVALID */
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PAE: Invalid Operational status update\n");
        return PNAC_FAILURE;
    }

    if (pPortInfo->u1PortPaeStatus == PNAC_ENABLED)
    {
        if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
        {
            if (pPortInfo->u1EntryStatus == PNAC_PORT_OPER_UP)
            {
                PnacPaeCdStateMachine (PNAC_CDSM_EV_PORTENABLED, u2PortNum);
            }
            else
            {
                PnacPaeCdStateMachine (PNAC_CDSM_EV_PORTDISABLED, u2PortNum);
            }

            PnacAuthLinkStatusUpdate (pPortInfo);
        }

        if (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))
        {
            PnacSuppLinkStatusUpdate (pPortInfo);
        }
    }

    if (gPnacSystemInfo.u1SystemMode == PNAC_DISTRIBUTED)
    {

        PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC,
                       "DPNAC:PnacProcPortStatUpdEvent triggered DPNAC update for Port=%d\n",
                       u2PortNum);

        if (DPnacSendEventUpdateMsg (u2PortNum) == PNAC_FAILURE)

        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      "DPNAC:PnacProcPortStatUpdEvent DPNAC frame transmission failed ");
            return PNAC_FAILURE;

        }

        DPnacGetBridgeIndexFromIfIndex ((UINT4) u2PortNum, &u4PortBrgIndex);

        DPnacGetSlotIdFromPort (u2PortNum, &u4SlotId);

        if (DPnacGetPortEntry
            (&pConsPortEntry, (UINT2) u4PortBrgIndex, u4SlotId) == PNAC_SUCCESS)
        {

            if ((pConsPortEntry->u1LocalOrRemote == PNAC_DPNAC_REMOTE_PORT) &&
                ((pPortInfo->u2PortStatus !=
                  (UINT2) pConsPortEntry->u1PortAuthStatus)
                 || (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2OperControlDir !=
                     (UINT2) pConsPortEntry->u1CtrlDirection)))
            {
                /* If a remote port  made admin down and up check with consolidated
                   port then update the port status */
                pPortInfo->u2PortStatus =
                    (UINT2) pConsPortEntry->u1PortAuthStatus;
                PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2OperControlDir =
                    (UINT2) pConsPortEntry->u1CtrlDirection;

                PnacL2IwfSetPortPnacAuthStatus (u2PortNum,
                                                (UINT2) pConsPortEntry->
                                                u1PortAuthStatus);

                PnacL2IwfSetPortPnacControlDir (u2PortNum,
                                                (UINT2) pConsPortEntry->
                                                u1CtrlDirection);

            }
        }
    }

    /* Indicate to the higher layers through L2IWF */
    PnacAuthPortOperIndication (u2PortNum);
    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
              " Updated Operational status successfully ...\n");
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacProcCreatePortEvent                              */
/*                                                                           */
/* Description        : This function make entries in PNAC global structure  */
/*                      on creation of Bridge ports.                         */
/*                                                                           */
/* Input(s)           : u2PortNum - Interface index,                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : Port Entry Status in gPnacSystemInfo for the port.   */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : Port Interface Map in gPnacSystemInfo for the port.  */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PnacProcCreatePortEvent (UINT2 u2PortNum)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    UINT1               u1OperStatus;
    UINT1               u1PortState;
    tCfaIfInfo          IfInfo;

    PNAC_MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC, " Creating Port %u ...\n", u2PortNum);
    if ((u2PortNum < PNAC_MINPORTS) || (u2PortNum > PNAC_MAXPORTS))
    {

        /* Port Number is not a valid number */
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " CreatePort: Port Number is invalid \n");
        return PNAC_FAILURE;
    }

    pPortInfo = &(PNAC_PORT_INFO (u2PortNum));
    if (pPortInfo->u1EntryStatus != PNAC_PORT_INVALID)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " CreatePort: Port is already created\n");
        return PNAC_FAILURE;
    }

    /* Initialise the port entry structure for this port */
    PNAC_MEMSET (pPortInfo, PNAC_INIT_VAL, sizeof (tPnacPaePortEntry));

    /* Getting necessary info from the Common database */
    CfaGetIfInfo (u2PortNum, &IfInfo);
    pPortInfo->portIfMap.u1IfType = IfInfo.u1IfType;

    PNAC_MEMCPY (pPortInfo->portIfMap.au1MacAddr, IfInfo.au1MacAddr,
                 PNAC_MAC_ADDR_SIZE);

    if (PnacInitPort (u2PortNum) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " CreatePort: PnacInitPort returned failure\n");
        return PNAC_FAILURE;
    }

    /* Create port indiations are handled even when the module is disabled
     * In this case port entry should be created and the oper status will
     * be down, when the module is enabled it will query the actual status
     * from cfa and update the same.
     * */
    u1OperStatus = PNAC_PORT_OPER_DOWN;

    /* Operational Status is DOWN */
    pPortInfo->u1EntryStatus = u1OperStatus;

    if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
    {
        if (PnacInitAuthenticator (u2PortNum) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC |
                      PNAC_ALL_FAILURE_TRC,
                      " Authenticator Initialization Failed\n");
            return PNAC_FAILURE;
        }
        if (PnacPaeCdStateMachine (PNAC_CDSM_EV_INITIALIZE, u2PortNum)
            != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " CreatePort: CDSM returned Failure\n");
            return PNAC_FAILURE;
        }
        if (PnacPaeCdStateMachine (PNAC_CDSM_EV_PORTDISABLED, u2PortNum)
            != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " CreatePort: CDSM returned Failure\n");
            return PNAC_FAILURE;
        }
        if (pPortInfo->u1EntryStatus == PNAC_PORT_OPER_UP)
        {
            if (PnacGetHLPortStatus (u2PortNum, &u1PortState) == PNAC_SUCCESS)
            {
                if (u1PortState == CFA_IF_DOWN)
                {
                    /* Indicate to the Higher Layers through L2IWF */
                    if (PnacL2IwfPnacHLPortOperIndication (u2PortNum,
                                                           u1PortState) ==
                        L2IWF_SUCCESS)
                    {
                        return PNAC_SUCCESS;
                    }

                }
            }
        }
    }

    /* Now initialise the supplicant for all ports that supports supplicant role
     */
    if (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))
    {
        if (PnacInitSupplicant (u2PortNum) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC |
                      PNAC_ALL_FAILURE_TRC,
                      " Supplicant Initialization Failed\n");
            return PNAC_FAILURE;
        }
    }
    PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC, " Port %u Created ...\n", u2PortNum);
    return PNAC_SUCCESS;
}                                /* end PnacProcCreatePortEvent() */

/*****************************************************************************/
/* Function Name      : PnacProcDeletePortEvent                              */
/*                                                                           */
/* Description        : This function delete entries in PNAC global data     */
/*                      structure on deletion of Bridge ports                */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : Port Entry Status in gPnacSystemInfo for the port.   */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : Port Entry Status in gPnacSystemInfo for the port.   */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PnacProcDeletePortEvent (UINT2 u2PortNum)
{
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tPnacSuppSessionNode *pSuppSessNode = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;
    PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC, " Deleting Port %u ...\n", u2PortNum);

    /* Convert the interface index to port number if needed */
    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " DeletePort: Port is not created yet, Aborting operation...\n");
        return PNAC_FAILURE;
    }
    if (pPortInfo->u1EntryStatus == PNAC_PORT_OPER_UP)
    {
        pPortInfo->u1EntryStatus = PNAC_PORT_OPER_DOWN;
        if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
        {
            PnacAuthLinkStatusUpdate (pPortInfo);
        }
        if (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))
        {
            PnacSuppLinkStatusUpdate (pPortInfo);
        }
    }

    /* Port is Operationally Down */
    if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
    {
        if ((PNAC_IS_PORT_MODE_PORTBASED (pPortInfo)) ||
            ((PNAC_IS_PORT_MODE_MACBASED (pPortInfo)) &&
             (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl !=
              PNAC_PORTCNTRL_AUTO)))
        {
            if ((pAuthSessNode = pPortInfo->pPortAuthSessionNode) != NULL)
            {
                if (PNAC_RELEASE_AUTHSESSNODE_MEMBLK (pAuthSessNode)
                    != PNAC_MEM_SUCCESS)
                {
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                              PNAC_OS_RESOURCE_TRC,
                              " DeletePort: Auth Node release failed, Continuing...\n");
                }
                pPortInfo->pPortAuthSessionNode = NULL;
            }
            else
            {
                PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                               PNAC_OS_RESOURCE_TRC,
                               " DeletePort: No Authenticator Node on Port: %u, "
                               " Continuing...\n", u2PortNum);
            }
        }
    }
    if (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))
    {
        if ((pSuppSessNode = pPortInfo->pPortSuppSessionNode) != NULL)
        {
            if (PNAC_RELEASE_SUPPSESSNODE_MEMBLK (pSuppSessNode)
                != PNAC_MEM_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                          PNAC_OS_RESOURCE_TRC,
                          " DeletePort: Supplicant Node release failed, "
                          " Continuing...\n");
            }
            pPortInfo->pPortSuppSessionNode = NULL;
        }
        else
        {
            PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                           PNAC_OS_RESOURCE_TRC,
                           " DeletePort: No Supplicant Node on Port: %u, "
                           " Continuing ...\n", u2PortNum);
        }
    }
#ifdef NPAPI_WANTED
    /* When the port is deleted from Pnac, retaining the earlier programmed
     * authorization state is not correct. So set the port status 
     * as Authorized. */
    if (PNAC_IS_NP_PROGRAMMING_ALLOWED () == PNAC_TRUE)
    {
        if (PNAC_PROTOCOL_ADMIN_STATUS () == PNAC_ENABLED)
        {
            /* Since the port is getting deleted, no need to check for the auth
             * status and control direction from the port entry. Directly set
             * the auth status as Authorized and the control direction as both. */

            if ((PNAC_IS_PORT_MODE_MACBASED (pPortInfo) &&
                 (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl !=
                  PNAC_PORTCNTRL_FORCEAUTHORIZED)))
            {
                PnacPnacHwSetAuthStatus (u2PortNum, NULL,
                                         PNAC_PORT_AUTHMODE_MACBASED,
                                         PNAC_UNBLOCK_PORT,
                                         PNAC_CNTRLD_DIR_BOTH);
            }
            else
            {
                PnacPnacHwSetAuthStatus (u2PortNum, NULL,
                                         PNAC_PORT_AUTHMODE_PORTBASED,
                                         (UINT1)
                                         PNAC_PORTSTATUS_AUTHORIZED,
                                         PNAC_CNTRLD_DIR_BOTH);
            }
        }
    }
#endif /* NPAPI_WANTED */
    pPortInfo->u1EntryStatus = PNAC_PORT_INVALID;
    PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC, " Port %u Deleted...\n", u2PortNum);
    return PNAC_SUCCESS;
}                                /* PnacProcDeletePortEvent */

/*****************************************************************************/
/* Function Name      : PnacPaeConstructEapolHdr                             */
/*                                                                           */
/* Description        : Called to form the EAPOL packet header fields and    */
/*                      Ethernet header fields.                              */
/*                                                                           */
/* Input(s)           : pu1EapolPktBuf - The buffer pointer from where the   */
/*                                       the EAPOL header is to be formed    */
/*                      DestAddr - Pointer to the destination MAC address    */
/*                                                                           */
/*                      u1EapolBodyType - The Packet type field in the EAPOL */
/*                                       Header.                             */
/*                      u2BodyLength - The Packet Body Length field in the   */
/*                                     EAPOL frame                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : Port Interface Map for the Port.                     */
/*                      Protocol Version for the Port.                       */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
PnacPaeConstructEapolHdr (UINT1 *pu1EapolPktBuf, tMacAddr destAddr,
                          UINT1 u1EapolBodyType, UINT2 u2BodyLength,
                          UINT2 u2PortNum, BOOL1 bIsPreAuthPkt)
{
    tMacAddr            srcAddr;
    tPnacPaePortEntry  *pPortInfo = NULL;
    UINT1              *pu1WritePtr = NULL;
    UINT2               u2Val;
    UINT1               u1Val;
    pu1WritePtr = pu1EapolPktBuf;

    /* Write the Destination address */

    /* Write the Source address as the port's own MAC address */
    pPortInfo = &(PNAC_PORT_INFO (u2PortNum));

    if (pPortInfo != NULL)
    {
        /* AR OS - Incase of Multi-Host, the MAC Address will be set to locally generated
           MAC Address(last 2 bytes holding local port ID). Hence, replacing last 2 bytes
           with the last 2 bytes of Globally Reserved Multicast MAC */
        if ((destAddr[0] == 0x01) && (destAddr[1] == 0x80)
            && (destAddr[2] == 0xC2) && (destAddr[3] == 0x00))
        {
            destAddr[4] = 0x00;
            destAddr[5] = 0x03;
        }
        PNAC_MEMCPY (pu1WritePtr, destAddr, PNAC_MAC_ADDR_SIZE);
        pu1WritePtr += PNAC_MAC_ADDR_SIZE;

        PNAC_MEMCPY (srcAddr, pPortInfo->portIfMap.au1MacAddr,
                     PNAC_MAC_ADDR_SIZE);
        PNAC_MEMCPY (pu1WritePtr, srcAddr, PNAC_MAC_ADDR_SIZE);
        pu1WritePtr += PNAC_MAC_ADDR_SIZE;
    }

    /* Write the Ethernet Pkt type as EAPOL type */
    if (bIsPreAuthPkt == OSIX_TRUE)
    {
        u2Val = PNAC_PAE_RSN_PREAUTH_ENET_TYPE;
    }
    else
    {
        u2Val = PNAC_PAE_ENET_TYPE;
    }

    PNAC_PUT_2BYTE (pu1WritePtr, u2Val);

    /* Write the EAPOL header - Protocol Version */
    u1Val = (UINT1) (pPortInfo->u4ProtocolVersion);
    PNAC_PUT_1BYTE (pu1WritePtr, u1Val);

    /* Write the EAPOL Packet Type */
    PNAC_PUT_1BYTE (pu1WritePtr, u1EapolBodyType);

    /* Write the EAPOL packet body length */
    PNAC_PUT_2BYTE (pu1WritePtr, u2BodyLength);
    return;
}                                /* PnacPaeConstructEapolHdr */

/*****************************************************************************/
/* Function Name      : PnacPaeTxEapPkt                                      */
/*                                                                           */
/* Description        : This function is called whenever an EAP Pkt          */
/*                      (Request, Response, Success, Failure) is to be       */
/*                      transmitted by the authenticator or Supplicant       */
/*                      It forms the Ethernet frame and transmits it out     */
/*                                                                           */
/* Input(s)           : DestAddr - Pointer to the destination MAC address    */
/*                                 to which this packet is to be transmitted */
/*                      u2PortNum - Port number through which this packet is */
/*                                  to be transmitted                        */
/*                      pEapHdrInfo - Pointer to the EAP packet information  */
/*                                  that is to be used to build the Ethernet */
/*                                  frame                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacPaeTxEapPkt (tMacAddr DestAddr, UINT2 u2PortNum,
                 tPnacEapPktHdr * pEapHdrInfo, BOOL1 bIsPreAuthPkt)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    UINT1              *pu1Buf = NULL;
    UINT1              *pu1BufPtr = NULL;
    UINT2               u2NewPktLen;
    UINT2               u2Length;
    UINT1               u1Val;
    UINT2               u2Val;

    if (PNAC_NODE_STATUS () != PNAC_ACTIVE_NODE)
    {
        /*Pkt transmission should be allowed only in Active node */
        return PNAC_SUCCESS;
    }
    if (PNAC_ALLOCATE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Unable to allocate memory. \r\n");
        return PNAC_FAILURE;
    }

    PNAC_MEMSET (pu1Buf, PNAC_INIT_VAL, PNAC_MAX_ENET_FRAME_SIZE);
    pu1BufPtr = pu1Buf;
    pu1BufPtr += PNAC_ENET_EAPOL_HDR_SIZE;

    /* Form the EAP header in the packet */
    u1Val = pEapHdrInfo->u1Code;
    PNAC_PUT_1BYTE (pu1BufPtr, u1Val);
    u1Val = pEapHdrInfo->u1Identifier;
    PNAC_PUT_1BYTE (pu1BufPtr, u1Val);
    u2Length = (UINT2) (pEapHdrInfo->u2DataLength + PNAC_EAP_HDR_SIZE);
    u2Val = u2Length;
    PNAC_PUT_2BYTE (pu1BufPtr, u2Val);

    /* Copy the EAP Packet to the Local Buffer if available */
    if (pEapHdrInfo->u2DataLength > PNAC_NO_VAL)

    {
        PNAC_MEMCPY (pu1BufPtr, pEapHdrInfo->pu1Data,
                     pEapHdrInfo->u2DataLength);
    }

    /* Fill the Ethernet Header and EAPOL Header */
    PnacPaeConstructEapolHdr (pu1Buf, DestAddr, PNAC_EAP_PKT,
                              u2Length, u2PortNum, bIsPreAuthPkt);
    u2NewPktLen = (UINT2) (u2Length + PNAC_ENET_EAPOL_HDR_SIZE);

    /* Fetch the PNAC Port Entry for port u2PortNum, to identify the type of the port */

    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PAE: Port is not created yet, Aborting operation ...\n");
        if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == MEM_FAILURE)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      "Failed to release  memory. \r\n");
        }

        return PNAC_FAILURE;
    }

    if (pPortInfo->portIfMap.u1IfType == CFA_CAPWAP_DOT11_BSS)
    {
#ifdef RSNA_WANTED

        if (RsnaPnacTxFrame (pu1Buf, u2NewPktLen, u2PortNum) != RSNA_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " PAE: PnacPaeTxEnetFrame returned Failure\n");
            if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == MEM_FAILURE)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                          "Failed to release  memory. \r\n");
            }
            return PNAC_FAILURE;

        }
#endif
    }
    else
    {
        /* Transmit the EAPOL Ethernet Frame */
        if (PnacPaeTxEnetFrame (pu1Buf, u2NewPktLen, u2PortNum) != PNAC_SUCCESS)

        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " PAE: PnacPaeTxEnetFrame returned Failure\n");
            if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == MEM_FAILURE)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                          "Failed to release  memory. \r\n");
            }
            return PNAC_FAILURE;
        }
    }

    PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC,
                   " EAP Frame Sent with code: %2x !!!\n", pEapHdrInfo->u1Code);
    if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == MEM_FAILURE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Failed to release  memory. \r\n");
    }
    return PNAC_SUCCESS;
}                                /* PnacPaeTxEapPkt */

/*****************************************************************************/
/* Function Name      : PnacPaeTxEnetFrame                                   */
/*                                                                           */
/* Description        : This function copies the Ethernet frame from flat    */
/*                      buffer into the CRU buffer and passess the CRU       */
/*                      buffer pointer to the PnacHandOverOutFrame().        */
/*                                                                           */
/* Input(s)           : pu1ReadPoint - pointer to Flat buffer,               */
/*                      u2PktLength - packet length,                         */
/*                      u2PortNo - Port number                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
PnacPaeTxEnetFrame (UINT1 *pu1ReadPoint, UINT2 u2PktLength, UINT2 u2PortNo)
{
    tPnacCruBufChainHdr *pEnetMesgBuf = NULL;
    if (PNAC_NODE_STATUS () != PNAC_ACTIVE_NODE)
    {
        /*Pkt transmission should be allowed only in Active node */
        return PNAC_SUCCESS;
    }

    /* Allocate a CRU buffer message chain for this Ethernet frame */
    if ((pEnetMesgBuf =
         PNAC_ALLOC_CRU_BUF (u2PktLength, PNAC_OFFSET_NONE)) == NULL)

    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                  PNAC_BUFFER_TRC, "PAE: Allocate Buffer returned Failure\n");
        return PNAC_FAILURE;
    }

    /* Copy the passed Ethernet frame into the CRU buffer */
    if (PNAC_COPY_OVER_CRU_BUF
        (pEnetMesgBuf, pu1ReadPoint, PNAC_OFFSET_NONE,
         u2PktLength) == PNAC_CRU_FAILURE)

    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                  PNAC_BUFFER_TRC, "PAE: Copy Over Buffer returned Failure\n");

        /* Release the CRU buf chain for Ethernet frame */
        if (PNAC_RELEASE_CRU_BUF (pEnetMesgBuf, PNAC_CRU_FALSE)
            != PNAC_CRU_SUCCESS)

        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                      PNAC_BUFFER_TRC,
                      "PAE: Release Buffer returned Failure\n");
        }
        return PNAC_FAILURE;
    }

   /***************************************************************/
    /* Transmit out to Lower layer, the Ethernet frame -           */
    /* Note: Lower layer should take care of releasing the buffer  */
    /* after transmitting the frame.                               */
   /***************************************************************/
    if (PnacHandOverOutFrame (pEnetMesgBuf,
                              u2PortNo,
                              u2PktLength,
                              PNAC_PAE_ENET_TYPE,
                              PNAC_ENCAP_NONE) != PNAC_SUCCESS)

    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  "PAE: HandOverOutFrame returned Failure\n");
        return PNAC_FAILURE;
    }
    return PNAC_SUCCESS;
}                                /*PnacPaeTxEnetFrame ends */

/*****************************************************************************/
/* Function Name      : PnacHandOverOutFrame                                 */
/*                                                                           */
/* Description        : This function forwards the MAC frame to              */
/*                      the CFA to transmit it out on the specified          */
/*                      interface                                            */
/*                                                                           */
/* Input(s)           : pMesg - pointer to CRU buffer containing MAC frame,  */
/*                      u2PortNum - Interface index,                         */
/*                      u4FrameSize - The size of the frame in pMesg buffer  */
/*                      u2Protocol - Protocol of the data contained in MAC   */
/*                                   frame                                   */
/*                      u1EncapType - Encapsulation type of the frame        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
PnacHandOverOutFrame (tPnacCruBufChainHdr * pMesg, UINT2 u2PortNum,
                      UINT4 u4FrameSize, UINT2 u2Protocol, UINT1 u1EncapType)
{

    UINT2               u2MesgLength =
        (UINT2) PNAC_CRU_BUF_Get_ChainValidByteCount (pMesg);
    PNAC_PKT_DUMP (PNAC_DUMP_TRC, pMesg, u2MesgLength,
                   " Dumping frame to be transmitted :\n");

    PNAC_TRC_ARG1 (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC,
                   " Outgoing Frame Handed Over for Port: %u !!!\n", u2PortNum);

    if (PNAC_CFA_HANDLE_OUT_FRAME (pMesg, u2PortNum, u4FrameSize,
                                   u2Protocol, u1EncapType) == CFA_FAILURE)

    {
        PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC | PNAC_ALL_FAILURE_TRC,
                  " PNAC_CFA_HANDLE_OUT_FRAME() returned Failure\n");
        return PNAC_FAILURE;
    }
    return PNAC_SUCCESS;
}                                /* PnacHandOverOutFrame */

/*****************************************************************************/
/* Function Name      : PnacProcPaeFrame                                     */
/*                                                                           */
/* Description        : This function processes the Eapol frame.It also      */
/*                      validates Eapol fields.                              */
/*                                                                           */
/* Input(s)           : pBuf - PNAC EAPOL Buffer pointer                     */
/*                      u2PortNo - Port number,                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : Eapol protocol version in gPnacSystemInfo            */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
PnacProcPaeFrame (UINT2 u2PortNo, tPnacCruBufChainHdr * pBuf)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tMacAddr            macAddress;
    tMacAddr            SrcMacAddress;
    UINT2               u2EnetType = PNAC_INIT_VAL;

    if (pBuf == NULL)
        return PNAC_FAILURE;

    PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC,
              " Received an EAPOL Frame...\n");

    if (((gPnacSystemInfo.u1SystemMode == PNAC_DISTRIBUTED) &&
         (DPnacIsConnectingPort ((UINT4) u2PortNo) != PNAC_SUCCESS)) ||
        (gPnacSystemInfo.u1SystemMode != PNAC_DISTRIBUTED))
    {
        if (PnacGetPortEntry (u2PortNo, &pPortInfo) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC |
                      PNAC_ALL_FAILURE_TRC,
                      " Frame received on INVALID status port...\n");

            PNAC_RELEASE_CRU_BUF (pBuf, PNAC_CRU_FALSE);

            return PNAC_FAILURE;
        }

        /* checking the PAE enabled status */
        if ((pPortInfo != NULL) &&
            (pPortInfo->u1PortPaeStatus == PNAC_DISABLED))
        {
            /* drop the received Ethernet frame received */
            PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC
                      | PNAC_ALL_FAILURE_TRC, " PNAC is disabled on port\n");

            PNAC_RELEASE_CRU_BUF (pBuf, PNAC_CRU_FALSE);

            return PNAC_FAILURE;
        }

        /* checking the port status */
        if ((pPortInfo != NULL) &&
            (pPortInfo->u1EntryStatus == PNAC_PORT_OPER_DOWN))
        {
            /* drop the invalid Ethernet frame received */
            PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC
                      | PNAC_ALL_FAILURE_TRC,
                      " Frame received on a Operationally down port\n");

            PNAC_RELEASE_CRU_BUF (pBuf, PNAC_CRU_FALSE);

            return PNAC_FAILURE;
        }
    }

    /* read the Ethernet frame type */
    if (PNAC_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) &u2EnetType,
                                PNAC_ENETTYPE_OFFSET, PNAC_ENET_TYPE_OR_LEN)
        == PNAC_CRU_FAILURE)
    {

        PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC
                  | PNAC_ALL_FAILURE_TRC, " Copy From Buffer failed ... \n");

        PNAC_RELEASE_CRU_BUF (pBuf, PNAC_CRU_FALSE);

        return PNAC_FAILURE;
    }
    u2EnetType = (UINT2) PNAC_NTOHS (u2EnetType);

    /* handle if received frame is priority tagged */
    if (u2EnetType == PNAC_TAG_ENET_TYPE)
    {
        PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC,
                  " Frame is tagged ... \n");

        PNAC_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) &u2EnetType,
                                PNAC_VLAN_TAGGED_HEADER_OFFSET,
                                PNAC_ENET_TYPE_OR_LEN);
        u2EnetType = (UINT2) PNAC_NTOHS (u2EnetType);
    }

    if ((u2EnetType != PNAC_PAE_ENET_TYPE) &&
        (u2EnetType != PNAC_PAE_RSN_PREAUTH_ENET_TYPE))
    {
        /* Unknown frame */

        PNAC_RELEASE_CRU_BUF (pBuf, PNAC_CRU_FALSE);

        return PNAC_FAILURE;
    }

    /* read the Destination MAC address */
    if (PNAC_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) macAddress,
                                PNAC_DESTADDR_OFFSET, PNAC_MAC_ADDR_SIZE)
        == PNAC_CRU_FAILURE)
    {

        PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC
                  | PNAC_ALL_FAILURE_TRC, " Copy From Buffer failed ... \n");

        PNAC_RELEASE_CRU_BUF (pBuf, PNAC_CRU_FALSE);

        return PNAC_FAILURE;
    }

    /* read the Source MAC address */
    if (PNAC_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) SrcMacAddress,
                                PNAC_SRCADDR_OFFSET,
                                PNAC_MAC_ADDR_SIZE) == PNAC_CRU_FAILURE)
    {

        PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC
                  | PNAC_ALL_FAILURE_TRC, " Copy From Buffer failed ... \n");

        PNAC_RELEASE_CRU_BUF (pBuf, PNAC_CRU_FALSE);

        return PNAC_FAILURE;
    }

    if (((pPortInfo != NULL)
         &&
         (PNAC_COMPARE_MAC_ADDR (macAddress, pPortInfo->portIfMap.au1MacAddr)))
        || PNAC_COMPARE_MAC_ADDR (macAddress, gPnacEapolGrpMacAddr))
    {

        PNAC_TRC (PNAC_DATA_PATH_TRC,
                  " Frame is targetted to this machine...\n");

        /* received frame is treated as a valid Eapol frame */
        if (PnacPaeProcessEapolFrame
            (pBuf, u2PortNo, SrcMacAddress, u2EnetType) != PNAC_SUCCESS)
        {

            if (PNAC_RELEASE_CRU_BUF (pBuf, PNAC_CRU_FALSE) != PNAC_CRU_SUCCESS)
            {
                PNAC_TRC (PNAC_BUFFER_TRC | PNAC_ALL_FAILURE_TRC |
                          PNAC_DATA_PATH_TRC, " Buffer release failed \n");
            }
            PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC
                      | PNAC_ALL_FAILURE_TRC,
                      " Processing EAPOL frame failed ... \n");
            return PNAC_FAILURE;
        }

        /* releasing CRU buffer, after successfully processing it */
        if (PNAC_RELEASE_CRU_BUF (pBuf, PNAC_CRU_FALSE) != PNAC_CRU_SUCCESS)
        {
            PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC |
                      PNAC_ALL_FAILURE_TRC, " Buffer release failed \n");
            return PNAC_FAILURE;
        }
        return PNAC_SUCCESS;
    }                            /* Frames targetted to this machine, ends */

    else
    {
        /* received Eapol frame is not targetted to this machine */

        /* Note: 
         * ----
         * If Eapol frames received that are not targetted 
         * to this machine is treated as any other Bridge 
         * data frame, the below release and return can 
         * be removed. 
         * Such a situation arises
         * when this machine can forward Eapol frames targetted to different
         * Authenticators
         */

        /* Drop the packet and free the buffer */
        PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " Frame is not targetted to this machine... \n");
        if (PNAC_RELEASE_CRU_BUF (pBuf, PNAC_CRU_FALSE) != PNAC_CRU_SUCCESS)
        {
            PNAC_TRC (PNAC_BUFFER_TRC | PNAC_ALL_FAILURE_TRC |
                      PNAC_DATA_PATH_TRC, " Buffer release failed \n");
            return PNAC_FAILURE;
        }
        return PNAC_SUCCESS;

    }                            /* else, ends */
}

/*****************************************************************************/
/* Function Name      : PnacPaeProcessEapolFrame                             */
/*                                                                           */
/* Description        : This function processes the Eapol frame.It also      */
/*                      validates Eapol fields.                              */
/*                                                                           */
/* Input(s)           : pMesg - Pointer to CRU buffer containing MAC frame   */
/*                      u2PortNum - Port number,                             */
/*                      srcMacAddr - Pointer to Supplicant's MAC address     */
/*                      u2EnetType :- RsnaPreAuth /Pnac Pae Frame            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : Eapol protocol version in gPnacSystemInfo            */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
PnacPaeProcessEapolFrame (tPnacCruBufChainHdr * pMesg, UINT2 u2PortNum,
                          tMacAddr srcMacAddr, UINT2 u2EnetType)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    UINT1               u1ProtVern;
    UINT1               u1EapolType;
    UINT1               u1EapCode;
    UINT1              *pu1ReadPtr = NULL;
    UINT2               u2EapolBodyLen;
    UINT2               u2PktLen;
    UINT2               u2EthernetType = PNAC_INIT_VAL;

    PNAC_TRC_ARG1 (PNAC_BUFFER_TRC | PNAC_CONTROL_PATH_TRC,
                   "EAPOL Frame Received on Port: %u !!!\n", u2PortNum);

    /* Initializing Ethernet frame message array */
    PNAC_MEMSET (gpu1EapolBuffer, PNAC_INIT_VAL,
                 PNAC_MAX_TAGGED_ENET_FRAME_SIZE);
    u2PktLen = (UINT2) PNAC_CRU_BUF_Get_ChainValidByteCount (pMesg);

    /* Copy the frame into the local Buffer 'gpu1EapolBuffer' */
    if (PNAC_COPY_FROM_CRU_BUF
        (pMesg, gpu1EapolBuffer, PNAC_OFFSET_NONE,
         u2PktLen) == PNAC_CRU_FAILURE)

    {
        PNAC_TRC (PNAC_BUFFER_TRC | PNAC_CONTROL_PATH_TRC |
                  PNAC_ALL_FAILURE_TRC, " PAE: Copy from buf failed \n");
        return PNAC_FAILURE;
    }
    pu1ReadPtr = gpu1EapolBuffer;
    /* read the Ethernet frame type */
    if (PNAC_COPY_FROM_CRU_BUF (pMesg, (UINT1 *) &u2EthernetType,
                                PNAC_ENETTYPE_OFFSET, PNAC_ENET_TYPE_OR_LEN)
        == PNAC_CRU_FAILURE)
    {

        PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC
                  | PNAC_ALL_FAILURE_TRC, " Copy From Buffer failed ... \n");

        return PNAC_FAILURE;
    }

    u2EthernetType = (UINT2) PNAC_NTOHS (u2EthernetType);

    /* handle if received frame is priority tagged */
    if (u2EthernetType == PNAC_TAG_ENET_TYPE)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_BUFFER_TRC,
                  " Frame is tagged ... \n");
        /* Read EAP protocol version */
        pu1ReadPtr += (PNAC_VLAN_TAGGED_HEADER_OFFSET + PNAC_ENET_TYPE_OR_LEN);
    }
    else
    {
        /* Read EAP protocol version */
        pu1ReadPtr += PNAC_ENET_HDR_SIZE;
    }

    PNAC_GET_1BYTE (u1ProtVern, pu1ReadPtr);

    if (((gPnacSystemInfo.u1SystemMode == PNAC_DISTRIBUTED) &&
         (DPnacIsConnectingPort ((UINT4) u2PortNum) != PNAC_SUCCESS)) ||
        (gPnacSystemInfo.u1SystemMode != PNAC_DISTRIBUTED))
    {

        if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      "PnacPaeProcessEapolFrame:Invalid" "Port Number\n");
            return PNAC_FAILURE;
        }
    }
    /* read Eapol type */
    PNAC_GET_1BYTE (u1EapolType, pu1ReadPtr);

    /* read Eapol body length */
    PNAC_GET_2BYTE (u2EapolBodyLen, pu1ReadPtr);

    /* Process the Eapol frame, according to its type */
    switch (u1EapolType)

    {
        case PNAC_EAPOL_START:    /* fall through */
        case PNAC_EAPOL_LOGOFF:

            /* Eapol body length should be zero for Start & Logoff frames */
            if (u2EapolBodyLen > PNAC_NO_VAL)

            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                          " PAE: Oversized Start/Logoff frame\n");
                /* If EAPOL Type is Start/Logoff ignore the octets after Packet Type field
                   and process it as per (IEEE-802.1x-(2004) section 7.5.7 */

                PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                          " PAE: Ignoring Octets after the Packet Type field and Processing it \n");
            }

            /* only if Authenticator capable, handle it */
            if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo))

            {

                /* silently discard the frame, memory is released only in 
                 * PnacHandleInFrame()
                 */
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " PAE: Authenticator Role is not supported on this "
                          " port to process Start/Logoff frame\n");
                return PNAC_SUCCESS;
            }

            /* update stats */
            PNAC_AUTH_STATS_ENTRY (pPortInfo).u4LastEapolFrameVersion =
                u1ProtVern;
            PNAC_MEMCPY (PNAC_AUTH_STATS_ENTRY (pPortInfo).
                         lastEapolFrameSource, srcMacAddr, PNAC_MAC_ADDR_SIZE);
            if (PnacAuthHandleInEapFrame
                (pu1ReadPtr, u2EapolBodyLen, u2PortNum, u1EapolType,
                 srcMacAddr, u2EnetType) != PNAC_SUCCESS)

            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " PAE: Authenticator handling of Eap Frame failed\n");
                return PNAC_FAILURE;
            }
            return PNAC_SUCCESS;
            break;
        case PNAC_EAPOL_KEY:
            if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))

            {

                /* update stats */
                PNAC_AUTH_STATS_ENTRY (pPortInfo).u4LastEapolFrameVersion =
                    u1ProtVern;
                PNAC_MEMCPY (PNAC_AUTH_STATS_ENTRY (pPortInfo).
                             lastEapolFrameSource, srcMacAddr,
                             PNAC_MAC_ADDR_SIZE);
                PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                               " Eapol Key Frame Received by Auth Capable Port: %u\n",
                               u2PortNum);
            }
            if (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))

            {

                /* update stats */
                PNAC_SUPP_STATS_ENTRY (pPortInfo).u4LastEapolFrameVersion =
                    u1ProtVern;
                PNAC_MEMCPY (PNAC_SUPP_STATS_ENTRY (pPortInfo).
                             lastEapolFrameSource, srcMacAddr,
                             PNAC_MAC_ADDR_SIZE);
                PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                               " Eapol Key Frame Received by Supp Capable Port: %u\n",
                               u2PortNum);
            }
            if (PNAC_IS_PORT_NONE_CAPABLE (pPortInfo))

            {
                PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                               " Eapol Key Frame Received on None Capable Port: %u "
                               " - Dropping Frame\n", u2PortNum);
                return PNAC_SUCCESS;
            }

            if ((pPortInfo->portIfMap.u1IfType != CFA_WLAN)
                && (pPortInfo->portIfMap.u1IfType != CFA_CAPWAP_DOT11_BSS))
            {
                if (PnacKeyRxKey (pu1ReadPtr, u2PortNum) != PNAC_SUCCESS)

                {
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              " PAE: Processing of Received Key Frame failed\n");
                    return PNAC_FAILURE;
                }
            }
            else
            {
#ifdef RSNA_WANTED
                if (RsnaApiHandleInEapolKeyFrame
                    ((pu1ReadPtr - PNAC_EAP_HDR_SIZE),
                     u2EapolBodyLen + PNAC_EAPOL_HDR_SIZE, u2PortNum,
                     srcMacAddr) == OSIX_FAILURE)
                {
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              " PAE: Processing of Received Key Frame failed\n");
                    return PNAC_FAILURE;
                }

#endif
            }
            return PNAC_SUCCESS;
            break;
        case PNAC_EAP_PKT:
            /* read the Eap pkt code field */
            PNAC_GET_1BYTE (u1EapCode, pu1ReadPtr);

#ifdef RSNA_WANTED
            RsnaEapolRtyDeleteEntry (srcMacAddr);
#endif

            if (u1EapCode == DPNAC_PKT_TYPE)
            {
                if (gPnacSystemInfo.u1SystemMode != PNAC_DISTRIBUTED)
                {
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              " DPNAC: Distributed PNAC is disable.Dropping frame \n");

                    return PNAC_FAILURE;

                }
                DPnacProcessEapPkt (pu1ReadPtr, u2EapolBodyLen, u2PortNum);
                return PNAC_SUCCESS;
            }

            /* move the pointer, to the start of Eap pkt */
            pu1ReadPtr -= PNAC_EAP_CODE_SIZE;
            if ((u1EapCode == PNAC_EAP_CODE_REQ) ||
                (u1EapCode == PNAC_EAP_CODE_SUCCESS) ||
                (u1EapCode == PNAC_EAP_CODE_FAILURE))

            {

                /* only if Supplicant capable, handle it */
                if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))

                {
                    PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC |
                                   PNAC_ALL_FAILURE_TRC,
                                   " PAE: Supplicant Role is not supported on "
                                   " Port %u, Aborting operation\n", u2PortNum);
                    return PNAC_SUCCESS;
                }

                /* update stats */
                PNAC_SUPP_STATS_ENTRY (pPortInfo).u4LastEapolFrameVersion =
                    u1ProtVern;
                PNAC_MEMCPY (PNAC_SUPP_STATS_ENTRY (pPortInfo).
                             lastEapolFrameSource, srcMacAddr,
                             PNAC_MAC_ADDR_SIZE);
                if (PnacSuppHandleInEapFrame
                    (pu1ReadPtr, u2EapolBodyLen, u2PortNum, u1EapolType,
                     srcMacAddr) != PNAC_SUCCESS)

                {
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              " PAE: Supplicant handling of Eap Frame failed\n");
                    return PNAC_FAILURE;
                }
            }

            else if (u1EapCode == PNAC_EAP_CODE_RESP)

            {

                /* only if Authenticator capable, handle it */
                if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo))

                {
                    PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC |
                                   PNAC_ALL_FAILURE_TRC,
                                   " PAE: Authenticator Role is not supported on "
                                   " Port %u to process Response frame\n",
                                   u2PortNum);
                    return PNAC_SUCCESS;
                }

                /* update stats */
                PNAC_AUTH_STATS_ENTRY (pPortInfo).u4LastEapolFrameVersion =
                    u1ProtVern;
                PNAC_MEMCPY (PNAC_AUTH_STATS_ENTRY (pPortInfo).
                             lastEapolFrameSource, srcMacAddr,
                             PNAC_MAC_ADDR_SIZE);
                if (PnacAuthHandleInEapFrame
                    (pu1ReadPtr, u2EapolBodyLen, u2PortNum, u1EapolType,
                     srcMacAddr, u2EnetType) != PNAC_SUCCESS)

                {
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              " PAE: Authenticator handling of Incoming Eap Frame failed\n");
                    return PNAC_FAILURE;
                }
            }

            else

            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " PAE: Invalid Eap pkt received\n");
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " PAE: Dropping the Eapol frame\n");

                /* update stats */
                if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))

                {
                    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4InvalidEapolFramesRx++;
                }
                if (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))

                {
                    PNAC_SUPP_STATS_ENTRY (pPortInfo).u4InvalidEapolFramesRx++;
                }
                return PNAC_FAILURE;
            }
            return PNAC_SUCCESS;
            break;
        case PNAC_EAPOL_ASF_ALERT:
            if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))

            {

                /* update stats */
                PNAC_AUTH_STATS_ENTRY (pPortInfo).u4LastEapolFrameVersion =
                    u1ProtVern;
                PNAC_MEMCPY (PNAC_AUTH_STATS_ENTRY (pPortInfo).
                             lastEapolFrameSource, srcMacAddr,
                             PNAC_MAC_ADDR_SIZE);
                PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                               " Eapol ASF Alert Received by Auth Capable Port: %u\n",
                               u2PortNum);
            }
            if (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))

            {

                /* update stats */
                PNAC_SUPP_STATS_ENTRY (pPortInfo).u4LastEapolFrameVersion =
                    u1ProtVern;
                PNAC_MEMCPY (PNAC_SUPP_STATS_ENTRY (pPortInfo).
                             lastEapolFrameSource, srcMacAddr,
                             PNAC_MAC_ADDR_SIZE);
                PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                               " Eapol ASF Alert Received by Supp Capable Port: %u\n",
                               u2PortNum);
            }
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " PAE: Since ASF alert is not supported in this "
                      " PNAC version, Discarding Frame...\n");
            return PNAC_SUCCESS;
            break;
        default:
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " PAE: Invalid EapolType received\n");
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " PAE: Dropping the Eapol frame\n");

            /* update stats */
            if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))

            {
                PNAC_AUTH_STATS_ENTRY (pPortInfo).u4InvalidEapolFramesRx++;
            }
            if (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))

            {
                PNAC_SUPP_STATS_ENTRY (pPortInfo).u4InvalidEapolFramesRx++;
            }
            return PNAC_FAILURE;
            break;
    }                            /*switch on Eapol type ends */
}                                /*PnacPaeProcessEapolFrame ends */
