/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Id: pnacl2wr.c,v 1.1                                                     */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : pnacl2wr.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : PNAC L2Iwf Wrapper                             */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 31 Oct 2007                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains L2IWF module routines       */
/*                            used in PNAC                                   */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    31 OCT 2007 / L2Team       Initial Create.                     */
/*---------------------------------------------------------------------------*/
#ifndef _PNACL2WR_C
#define _PNACL2WR_C

#include "pnachdrs.h"

/*****************************************************************************/
/* Function Name      : PnacL2IwfSetPortPnacAuthControl                      */
/*                                                                           */
/* Description        : This function calls the L2IWF module to set the      */
/*                      Port AuthControl.                                    */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface Identifier                     */
/*                      u2PortAuthControl - Port AuthControl                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacL2IwfSetPortPnacAuthControl (UINT2 u2IfIndex, UINT2 u2PortAuthControl)
{
    return (L2IwfSetPortPnacAuthControl (u2IfIndex, u2PortAuthControl));
}

/*****************************************************************************/
/* Function Name      : PnacL2IwfSetPortPnacAuthMode                         */
/*                                                                           */
/* Description        : This function calls the L2IWF module to set the      */
/*                      Port AuthMode.                                       */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface Identifier                     */
/*                      u2PortAuthMode - Port AuthMode                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacL2IwfSetPortPnacAuthMode (UINT2 u2IfIndex, UINT2 u2PortAuthMode)
{
    return (L2IwfSetPortPnacAuthMode (u2IfIndex, u2PortAuthMode));
}


/*****************************************************************************/
/* Function Name      : PnacL2IwfSetPortPnacAuthStatus                       */
/*                                                                           */
/* Description        : This function calls the L2IWF module to set the      */
/*                      Per Port Pnac enable/disable status.                 */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface Identifier                     */
/*                      u1PortAuthStatus - PNAC enables/disabled status      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacL2IwfSetPortPnacPaeStatus (UINT2 u2IfIndex, UINT1 u1PortPaeStatus)
{
    return (L2IwfSetPortPnacPaeStatus ((UINT4) u2IfIndex, u1PortPaeStatus));
}

/*****************************************************************************/
/* Function Name      : PnacL2IwfSetPortPnacAuthStatus                       */
/*                                                                           */
/* Description        : This function calls the L2IWF module to set the      */
/*                      Port AuthMode.                                       */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface Identifier                     */
/*                      u2PortAuthStatus - Port AuthStatus                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacL2IwfSetPortPnacAuthStatus (UINT2 u2IfIndex, UINT2 u2PortAuthStatus)
{
    INT4                i4ProtoId = (INT4) L2_PROTO_DOT1X;
    INT4                i4NpapiMode = PNAC_NP_SYNC;

    i4NpapiMode = PnacNpGetNpapiMode (i4ProtoId);

    if ((u2PortAuthStatus == PNAC_PORTSTATUS_AUTHORIZED) &&
        (gPnacSystemInfo.u1PnacCallSequence == PNAC_PROTOCOL_FLOW) &&
        (i4NpapiMode == PNAC_NP_ASYNC))
    {
        return PNAC_SUCCESS;
    }

    return (L2IwfSetPortPnacAuthStatus (u2IfIndex, u2PortAuthStatus));
}

/*****************************************************************************/
/* Function Name      : PnacL2IwfSetPortPnacControlDir                       */
/*                                                                           */
/* Description        : This function calls the L2IWF module to set the      */
/*                      Port control direction.                              */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface Identifier                     */
/*                      u2PortControlDir - Port Control direction            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacL2IwfSetPortPnacControlDir (UINT2 u2IfIndex, UINT2 u2PortControlDir)
{
    INT4                i4ProtoId = (INT4) L2_PROTO_DOT1X;
    INT4                i4NpapiMode = PNAC_NP_SYNC;

    i4NpapiMode = PnacNpGetNpapiMode (i4ProtoId);

    if ((u2PortControlDir == PNAC_CNTRLD_DIR_IN) &&
        (gPnacSystemInfo.u1PnacCallSequence == PNAC_PROTOCOL_FLOW) &&
        (i4NpapiMode == PNAC_NP_ASYNC))
    {
        return PNAC_SUCCESS;
    }

    return (L2IwfSetPortPnacControlDir (u2IfIndex, u2PortControlDir));
}

/*****************************************************************************/
/* Function Name      : PnacL2IwfSetSuppMacAuthStatus                        */
/*                                                                           */
/* Description        : This function calls the L2IWF module to set the      */
/*                      supplicant MacAuth status.                           */
/*                                                                           */
/* Input(s)           : SuppMacAddr - MAC Address                            */
/*                      u2MacAuthStatus - Auth Status                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacL2IwfSetSuppMacAuthStatus (tMacAddr SuppMacAddr, UINT2 u2MacAuthStatus)
{
    INT4                i4ProtoId = (INT4) L2_PROTO_DOT1X;
    INT4                i4NpapiMode = PNAC_NP_SYNC;

    i4NpapiMode = PnacNpGetNpapiMode (i4ProtoId);

    if ((u2MacAuthStatus == PNAC_PORTSTATUS_AUTHORIZED) &&
        (gPnacSystemInfo.u1PnacCallSequence == PNAC_PROTOCOL_FLOW) &&
        (i4NpapiMode == PNAC_NP_ASYNC))
    {
        return PNAC_SUCCESS;
    }

    return (L2IwfSetSuppMacAuthStatus (SuppMacAddr, u2MacAuthStatus));
}

/*****************************************************************************/
/* Function Name      : PnacL2IwfGetPortPnacControlDir                       */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the      */
/*                      port control direction.                              */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pu2PortControlDir - Control direction                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacL2IwfGetPortPnacControlDir (UINT2 u2IfIndex, UINT2 *pu2PortControlDir)
{
    return (L2IwfGetPortPnacControlDir (u2IfIndex, pu2PortControlDir));
}

/*****************************************************************************/
/* Function Name      : PnacL2IwfGetPortPnacAuthStatus                       */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the      */
/*                      Port Authentication Status.                          */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pu2PortAuthStatus - Port Authentication Status       */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacL2IwfGetPortPnacAuthStatus (UINT2 u2IfIndex, UINT2 *pu2PortAuthStatus)
{
    return (L2IwfGetPortPnacAuthStatus (u2IfIndex, pu2PortAuthStatus));
}

/*****************************************************************************/
/* Function Name      : PnacL2IwfGetPortPnacAuthStatus                       */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the      */
/*                      Port Pae Status.                                     */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pu1PortPaeStatus - Port level PNAC enables/disabled  */
/*                      Status                                               */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacL2IwfGetPortPnacPaeStatus (UINT2 u2IfIndex, UINT1 *pu1PortPaeStatus)
{
    return (L2IwfGetPortPnacPaeStatus ((UINT4) u2IfIndex, pu1PortPaeStatus));
}

/*****************************************************************************/
/* Function Name      : PnacL2IwfGetVlanPortPvid                             */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the      */
/*                      Pvid of the port.                                    */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pu2VlanId - Pvid of the port                         */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacL2IwfGetVlanPortPvid (UINT2 u2IfIndex, UINT2 *pu2VlanId)
{
    return (L2IwfGetVlanPortPvid ((UINT4) u2IfIndex, pu2VlanId));
}

#endif
