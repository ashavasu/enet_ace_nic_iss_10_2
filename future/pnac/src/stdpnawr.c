/* $Id: stdpnawr.c,v 1.8 2011/09/08 07:22:09 siva Exp $*/
# include  "lr.h"
# include  "fssnmp.h"
# include  "stdpnawr.h"
# include  "stdpnadb.h"
#include   "pnachdrs.h"

VOID
RegisterSTDPNA ()
{
    SNMPRegisterMibWithLock (&stdpnaOID, &stdpnaEntry, PnacLock, PnacUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&stdpnaOID, (const UINT1 *) "stdpnac");
}

VOID
UnRegisterSTDPNA ()
{
    SNMPUnRegisterMib (&stdpnaOID, &stdpnaEntry);
    SNMPDelSysorEntry (&stdpnaOID, (const UINT1 *) "stdpnac");
}

INT4
Dot1xPaeSystemAuthControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDot1xPaeSystemAuthControl (&(pMultiData->i4_SLongValue)));
}

INT4
Dot1xPaeSystemAuthControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetDot1xPaeSystemAuthControl (pMultiData->i4_SLongValue));
}

INT4
Dot1xPaeSystemAuthControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Dot1xPaeSystemAuthControl
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Dot1xPaeSystemAuthControlDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Dot1xPaeSystemAuthControl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexDot1xPaePortTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1xPaePortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1xPaePortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Dot1xPaePortProtocolVersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xPaePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xPaePortProtocolVersion
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xPaePortCapabilitiesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xPaePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xPaePortCapabilities
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Dot1xPaePortInitializeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xPaePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xPaePortInitialize (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
Dot1xPaePortReauthenticateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xPaePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xPaePortReauthenticate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1xPaePortInitializeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1xPaePortInitialize (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Dot1xPaePortReauthenticateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1xPaePortReauthenticate
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1xPaePortInitializeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Dot1xPaePortInitialize (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
Dot1xPaePortReauthenticateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2Dot1xPaePortReauthenticate (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
Dot1xPaePortTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Dot1xPaePortTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexDot1xAuthConfigTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1xAuthConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1xAuthConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Dot1xAuthPaeStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthPaeState (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
Dot1xAuthBackendAuthStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthBackendAuthState
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1xAuthAdminControlledDirectionsGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthAdminControlledDirections
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1xAuthOperControlledDirectionsGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthOperControlledDirections
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1xAuthAuthControlledPortStatusGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthAuthControlledPortStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1xAuthAuthControlledPortControlGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthAuthControlledPortControl
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1xAuthQuietPeriodGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthQuietPeriod (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthTxPeriodGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthTxPeriod (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthSuppTimeoutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthSuppTimeout (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthServerTimeoutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthServerTimeout (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthMaxReqGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthMaxReq (pMultiIndex->pIndex[0].i4_SLongValue,
                                   &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthReAuthPeriodGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthReAuthPeriod (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthReAuthEnabledGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthReAuthEnabled (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
Dot1xAuthKeyTxEnabledGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthKeyTxEnabled (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
Dot1xAuthAdminControlledDirectionsSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetDot1xAuthAdminControlledDirections
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1xAuthAuthControlledPortControlSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetDot1xAuthAuthControlledPortControl
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1xAuthQuietPeriodSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1xAuthQuietPeriod (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
Dot1xAuthTxPeriodSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1xAuthTxPeriod (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->u4_ULongValue));

}

INT4
Dot1xAuthSuppTimeoutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1xAuthSuppTimeout (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
Dot1xAuthServerTimeoutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1xAuthServerTimeout (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
Dot1xAuthMaxReqSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1xAuthMaxReq (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiData->u4_ULongValue));

}

INT4
Dot1xAuthReAuthPeriodSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1xAuthReAuthPeriod (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
Dot1xAuthReAuthEnabledSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1xAuthReAuthEnabled (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Dot1xAuthKeyTxEnabledSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1xAuthKeyTxEnabled (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
Dot1xAuthAdminControlledDirectionsTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2Dot1xAuthAdminControlledDirections (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
Dot1xAuthAuthControlledPortControlTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2Dot1xAuthAuthControlledPortControl (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
Dot1xAuthQuietPeriodTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2Dot1xAuthQuietPeriod (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
Dot1xAuthTxPeriodTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2Dot1xAuthTxPeriod (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
Dot1xAuthSuppTimeoutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2Dot1xAuthSuppTimeout (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
Dot1xAuthServerTimeoutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Dot1xAuthServerTimeout (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->u4_ULongValue));

}

INT4
Dot1xAuthMaxReqTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2Dot1xAuthMaxReq (pu4Error,
                                      pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->u4_ULongValue));

}

INT4
Dot1xAuthReAuthPeriodTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Dot1xAuthReAuthPeriod (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->u4_ULongValue));

}

INT4
Dot1xAuthReAuthEnabledTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Dot1xAuthReAuthEnabled (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
Dot1xAuthKeyTxEnabledTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Dot1xAuthKeyTxEnabled (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
Dot1xAuthConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Dot1xAuthConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexDot1xAuthStatsTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1xAuthStatsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1xAuthStatsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Dot1xAuthEapolFramesRxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthEapolFramesRx (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthEapolFramesTxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthEapolFramesTx (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthEapolStartFramesRxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthEapolStartFramesRx
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthEapolLogoffFramesRxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthEapolLogoffFramesRx
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthEapolRespIdFramesRxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthEapolRespIdFramesRx
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthEapolRespFramesRxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthEapolRespFramesRx
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthEapolReqIdFramesTxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthEapolReqIdFramesTx
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthEapolReqFramesTxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthEapolReqFramesTx
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthInvalidEapolFramesRxGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthInvalidEapolFramesRx
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthEapLengthErrorFramesRxGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthEapLengthErrorFramesRx
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthLastEapolFrameVersionGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthLastEapolFrameVersion
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthLastEapolFrameSourceGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetDot1xAuthLastEapolFrameSource
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList));

}

INT4
GetNextIndexDot1xAuthDiagTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1xAuthDiagTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1xAuthDiagTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Dot1xAuthEntersConnectingGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthDiagTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthEntersConnecting
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthEapLogoffsWhileConnectingGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthDiagTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthEapLogoffsWhileConnecting
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthEntersAuthenticatingGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthDiagTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthEntersAuthenticating
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthAuthSuccessWhileAuthenticatingGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthDiagTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthAuthSuccessWhileAuthenticating
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthAuthTimeoutsWhileAuthenticatingGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthDiagTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthAuthTimeoutsWhileAuthenticating
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthAuthFailWhileAuthenticatingGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthDiagTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthAuthFailWhileAuthenticating
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthAuthReauthsWhileAuthenticatingGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthDiagTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthAuthReauthsWhileAuthenticating
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthAuthEapStartsWhileAuthenticatingGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthDiagTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthAuthEapStartsWhileAuthenticating
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthAuthEapLogoffWhileAuthenticatingGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthDiagTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthAuthEapLogoffWhileAuthenticating
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthAuthReauthsWhileAuthenticatedGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthDiagTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthAuthReauthsWhileAuthenticated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthAuthEapStartsWhileAuthenticatedGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthDiagTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthAuthEapStartsWhileAuthenticated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthAuthEapLogoffWhileAuthenticatedGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthDiagTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthAuthEapLogoffWhileAuthenticated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthBackendResponsesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthDiagTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthBackendResponses
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthBackendAccessChallengesGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthDiagTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthBackendAccessChallenges
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthBackendOtherRequestsToSupplicantGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthDiagTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthBackendOtherRequestsToSupplicant
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthBackendNonNakResponsesFromSupplicantGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthDiagTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthBackendNonNakResponsesFromSupplicant
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthBackendAuthSuccessesGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthDiagTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthBackendAuthSuccesses
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthBackendAuthFailsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthDiagTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthBackendAuthFails
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexDot1xAuthSessionStatsTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1xAuthSessionStatsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1xAuthSessionStatsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Dot1xAuthSessionOctetsRxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthSessionStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthSessionOctetsRx
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
Dot1xAuthSessionOctetsTxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthSessionStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthSessionOctetsTx
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
Dot1xAuthSessionFramesRxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthSessionStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthSessionFramesRx
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthSessionFramesTxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthSessionStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthSessionFramesTx
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthSessionIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthSessionStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthSessionId (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
Dot1xAuthSessionAuthenticMethodGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthSessionStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthSessionAuthenticMethod
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1xAuthSessionTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthSessionStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthSessionTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xAuthSessionTerminateCauseGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthSessionStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthSessionTerminateCause
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1xAuthSessionUserNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xAuthSessionStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xAuthSessionUserName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
GetNextIndexDot1xSuppConfigTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1xSuppConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1xSuppConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Dot1xSuppPaeStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xSuppConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xSuppPaeState (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
Dot1xSuppHeldPeriodGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xSuppConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xSuppHeldPeriod (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xSuppAuthPeriodGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xSuppConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xSuppAuthPeriod (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xSuppStartPeriodGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xSuppConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xSuppStartPeriod (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xSuppMaxStartGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xSuppConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xSuppMaxStart (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xSuppControlledPortStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xSuppConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xSuppControlledPortStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1xSuppAccessCtrlWithAuthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xSuppConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xSuppAccessCtrlWithAuth
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1xSuppHeldPeriodSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1xSuppHeldPeriod (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
Dot1xSuppAuthPeriodSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1xSuppAuthPeriod (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
Dot1xSuppStartPeriodSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1xSuppStartPeriod (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
Dot1xSuppMaxStartSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1xSuppMaxStart (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->u4_ULongValue));

}

INT4
Dot1xSuppAccessCtrlWithAuthSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1xSuppAccessCtrlWithAuth
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1xSuppHeldPeriodTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2Dot1xSuppHeldPeriod (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
Dot1xSuppAuthPeriodTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2Dot1xSuppAuthPeriod (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
Dot1xSuppStartPeriodTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2Dot1xSuppStartPeriod (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
Dot1xSuppMaxStartTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2Dot1xSuppMaxStart (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
Dot1xSuppAccessCtrlWithAuthTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Dot1xSuppAccessCtrlWithAuth (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Dot1xSuppConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Dot1xSuppConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexDot1xSuppStatsTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1xSuppStatsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1xSuppStatsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Dot1xSuppEapolFramesRxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xSuppStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xSuppEapolFramesRx (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xSuppEapolFramesTxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xSuppStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xSuppEapolFramesTx (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xSuppEapolStartFramesTxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xSuppStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xSuppEapolStartFramesTx
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xSuppEapolLogoffFramesTxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xSuppStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xSuppEapolLogoffFramesTx
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xSuppEapolRespIdFramesTxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xSuppStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xSuppEapolRespIdFramesTx
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xSuppEapolRespFramesTxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xSuppStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xSuppEapolRespFramesTx
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xSuppEapolReqIdFramesRxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xSuppStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xSuppEapolReqIdFramesRx
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xSuppEapolReqFramesRxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xSuppStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xSuppEapolReqFramesRx
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xSuppInvalidEapolFramesRxGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xSuppStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xSuppInvalidEapolFramesRx
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xSuppEapLengthErrorFramesRxGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xSuppStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xSuppEapLengthErrorFramesRx
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xSuppLastEapolFrameVersionGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xSuppStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1xSuppLastEapolFrameVersion
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1xSuppLastEapolFrameSourceGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1xSuppStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetDot1xSuppLastEapolFrameSource
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList));

}
