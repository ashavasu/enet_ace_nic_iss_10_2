/* $Id: pnacport.c,v 1.39 2014/08/13 12:40:34 siva Exp $*/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : pnacport.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : PNAC Stubs                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Mar 2002                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains external module routines    */
/*                            used in PNAC                                   */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 MAR 2002 / BridgeTeam   Initial Create.                     */
/* 1.0.0.1    22 APR 2002 / BridgeTeam   Changed for incorporating Radius    */
/*                                       client interfacing.                 */
/* 1.0.0.2    29 NOV 2002 / Products     Modifying NP-API calls              */
/*---------------------------------------------------------------------------*/
#ifndef _PNACPORT_C
#define _PNACPORT_C

#include "pnachdrs.h"
#include "rstp.h"
#include "mstp.h"
#include "radius.h"
#include "pnacport.h"

#ifdef SNMP_2_WANTED
#include "snmputil.h"
#endif

extern unsigned int EoidGetEnterpriseOid (void);

#ifdef MBSM_WANTED
extern INT4         MbsmGetSlotIdFromConnectingPort (UINT4 u4ConnectingPort,
                                                     UINT4 *pu4SlotId);
extern VOID         MbsmGetDissMasterSlotId (UINT4 *pu4SlotId);
extern VOID         MbsmGetDissRoleFromSlotId (UINT4 u4SlotId,
                                               UINT1 *pu1DissRole);
extern INT4         MbsmIsConnectingPort (UINT4 u4IfIndex);
extern INT4         MbsmGetConnectingPortFromSlotId (UINT4 u4SlotId,
                                                     UINT4 *pu4ConnectingPort);
extern INT4         MbsmGetSlotIdFromRemotePort (UINT4 u4RemotePort,
                                                 UINT4 *pu4SlotId);
#endif

/*****************************************************************************/
/* Function Name      : PnacWlanRxKey                                        */
/*                                                                           */
/* Description        : This routine is called for passing the the Key (got  */
/*                      from remote Authentication Server) to WLAN driver.   */
/*                                                                           */
/* Input(s)           : pCruBuf -  Pointer to CRU buffer having the EAPOL    */
/*                                 key packet.                               */
/*                                                                           */
/*                      u2PortNum  - Port Number.                            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacWlanRxKey (tPnacKeyInfo * pKeyInfo, tMacAddr macAddr, UINT2 u2PortNum)
{
    PNAC_UNUSED (pKeyInfo);
    PNAC_UNUSED (macAddr);
    PNAC_UNUSED (u2PortNum);
}

/*****************************************************************************/
/* Function Name      : PnacWlanCryptStoreKey                                */
/*                                                                           */
/* Description        : This routine is called by PNAC module for storing    */
/*                      the Key received from the remote PNAC machine.       */
/*                                                                           */
/* Input(s)           : pBuf - Pointer to the buffer containing Key          */
/*                                                                           */
/*                      u2KeyLen - Key length                                */
/*                                                                           */
/*                      u2PortNum  - Port Number.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacWlanCryptStoreKey (UINT1 *pBuf, UINT2 u2KeyLen, UINT2 u2PortNum)
{
    PNAC_UNUSED (pBuf);
    PNAC_UNUSED (u2KeyLen);
    PNAC_UNUSED (u2PortNum);
}

/*****************************************************************************/
/* Function Name      : PnacWlanFillKeyDesc                                  */
/*                                                                           */
/* Description        : This routine is called by PNAC module for getting    */
/*                      the Key descriptor filled in.                        */
/*                                                                           */
/* Input(s)           : pu1Key   - Pointer to Key                            */
/*                      u2KeyLen - Key length                                */
/*                      u2PortNum  - Port Number.                            */
/*                      macAddr - Pointer to Mac address of the supplicant  */
/*                                                                           */
/* Output(s)          : pKeyDesc - Pointer to Key Descriptor                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacWlanFillKeyDesc (UINT1 *pu1Key, UINT2 u2KeyLen, UINT2 u2PortNum,
                     tMacAddr macAddr, tPnacKeyDesc * pKeyDesc)
{
    PNAC_UNUSED (u2PortNum);
    PNAC_UNUSED (macAddr);
    PNAC_MEMSET (pKeyDesc, 0, (sizeof (tPnacKeyDesc)));
    /* Temporarily fill the key alone - directly without encryption */
    pKeyDesc->u1DescType = PNAC_RC4_KEY_DESC;
    pKeyDesc->u2KeyLength = u2KeyLen;
    PNAC_MEMCPY (pKeyDesc->au1Key, pu1Key, u2KeyLen);
}

#ifdef WLAN_WANTED
/*****************************************************************************/
/* Function Name      : PnacWlanNtfyAuthFailure                              */
/*                                                                           */
/* Description        : This routine calles the WLAN module to notify the    */
/*                      authentication failure.                              */
/*                                                                           */
/* Input(s)           : pBuf - Pointer to the buffer containing Key          */
/*                                                                           */
/*                      u2KeyLen - Key length                                */
/*                                                                           */
/*                      u2PortNum  - Port Number.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
UINT4
PnacWlanNtfyAuthFailure (tMacAddr SrcMac)
{
    return (WlanNtfyAuthFailure (SrcMac));
}
#endif
/*****************************************************************************/
/* Function Name      : PnacInitRadInterface                                 */
/*                                                                           */
/* Description        : This routine Initialises the radius client related   */
/*                      data                                                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacInitRadInterface (VOID)
{
    UINT2               u2Cnt;

    PNAC_MEMSET (gPnacRadReq, 0,
                 ((sizeof (tPnacRadReq)) * PNAC_MAX_RAD_REQ_SIZE));
    for (u2Cnt = 0; u2Cnt < PNAC_MAX_RAD_REQ_SIZE; u2Cnt++)
        gPnacRadReq[u2Cnt].u1Status = PNAC_FALSE;
    return;
}

/*****************************************************************************/
/* Function Name      : PnacInitTacInterface                                 */
/*                                                                           */
/* Description        : This routine Initialises the tacacs+ client related  */
/*                      data                                                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacInitTacInterface (VOID)
{
    UINT2               u2Cnt;

    PNAC_MEMSET (gPnacTacReq, 0,
                 ((sizeof (tPnacTacReq)) * PNAC_MAX_TAC_REQ_SIZE));
    for (u2Cnt = 0; u2Cnt < PNAC_MAX_TAC_REQ_SIZE; u2Cnt++)
        gPnacTacReq[u2Cnt].u1Status = PNAC_FALSE;
    return;
}

/*****************************************************************************/
/* Function Name      : PnacRadiusAuthentication                             */
/*                                                                           */
/* Description        : This routine calls the Radius module for authenticate*/
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
PnacRadiusAuthentication (tRADIUS_INPUT_AUTH * p_RadiusInputAuth,
                          tRADIUS_SERVER * p_RadiusServerAuth,
                          VOID (*CallBackfPtr) (VOID *),
                          UINT4 ifIndex, UINT1 u1ReqId)
{
    return (radiusAuthentication (p_RadiusInputAuth, p_RadiusServerAuth,
                                  CallBackfPtr, ifIndex, u1ReqId));
}

/*****************************************************************************/
/* Function Name      : PnacAstIsMstEnabledInContext                         */
/*                                                                           */
/* Description        : This function calls the MSTP Module to get the       */
/*                      status.                                              */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacAstIsMstEnabledInContext (UINT4 u4ContextId)
{
    return (AstIsMstEnabledInContext (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : PnacAstIsRstEnabledInContext                         */
/*                                                                           */
/* Description        : This function calls the MSTP Module to get the       */
/*                      status.                                              */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacAstIsRstEnabledInContext (UINT4 u4ContextId)
{
    return (AstIsRstEnabledInContext (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : PnacCfaCliConfGetIfName                              */
/*                                                                           */
/* Description        : This function calls the CFA Module to get the        */
/*                      interface name from the given interface index.       */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pi1IfName - Interface name of the given IfIndex.     */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacCfaCliConfGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
    return (CfaCliConfGetIfName (u4IfIndex, pi1IfName));
}

/*****************************************************************************/
/* Function Name      : PnacCfaCliGetIfName                                  */
/*                                                                           */
/* Description        : This function calls the CFA Module to get the        */
/*                      interface name from the given interface index.       */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pi1IfName - Interface name of the given IfIndex.     */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacCfaCliGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
    return (CfaCliGetIfName (u4IfIndex, pi1IfName));
}

/*****************************************************************************/
/* Function Name      : PnacCfaGetIfInfo                                     */
/*                                                                           */
/* Description        : This function calls the CFA Module to get the        */
/*                      interface name from the given interface index.       */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pIfInfo   - Interface information.                   */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacCfaGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    return (CfaGetIfInfo (u4IfIndex, pIfInfo));
}

/*****************************************************************************/
/* Function Name      : PnacLaIsLaStarted                                    */
/*                                                                           */
/* Description        : This function calls the LA Module to get the status  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacLaIsLaStarted (VOID)
{
    return (LaIsLaStarted ());
}

/*****************************************************************************/
/* Function Name      : PnacVcmGetContextInfoFromIfIndex                     */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      Context-Id and the Localport number.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Identifier.                    */
/*                                                                           */
/* Output(s)          : pu4ContextId - Context Identifier.                   */
/*                      pu2LocalPortId - Local port number.                  */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacVcmGetContextInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId,
                                  UINT2 *pu2LocalPortId)
{
    return (VcmGetContextInfoFromIfIndex (u4IfIndex, pu4ContextId,
                                          pu2LocalPortId));
}

/*****************************************************************************/
/* Function Name      : PnacL2IwfGetNextValidPort                            */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the next */
/*                      Valid port.                                          */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pu2NextPort - Next Interface Index                   */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacL2IwfGetNextValidPort (UINT2 u2IfIndex, UINT2 *pu2NextPort)
{
    return (L2IwfGetNextValidPort (u2IfIndex, pu2NextPort));
}

/*****************************************************************************/
/* Function Name      : PnacL2IwfGetPortOperEdgeStatus                       */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the oper */
/*                      edge status of the port.                             */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pbOperEdge - OperEdge Status                         */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacL2IwfGetPortOperEdgeStatus (UINT2 u2IfIndex, BOOL1 * pbOperEdge)
{
    return (L2IwfGetPortOperEdgeStatus (u2IfIndex, pbOperEdge));
}

/*****************************************************************************/
/* Function Name      : PnacL2IwfGetPortOperStatus                           */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the oper */
/*                      status of the port.                                  */
/*                                                                           */
/* Input(s)           : i4ModuleId- Module Identifier                        */
/*                      u2IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pu1OperStatus - Oper status                          */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacL2IwfGetPortOperStatus (INT4 i4ModuleId, UINT2 u2IfIndex,
                            UINT1 *pu1OperStatus)
{
    return (L2IwfGetPortOperStatus (i4ModuleId, u2IfIndex, pu1OperStatus));
}

/*****************************************************************************/
/* Function Name      : PnacL2IwfGetProtocolTunnelStatusOnPort               */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the      */
/*                      tunnel status for the given port.                    */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                      u2Protocol - Protocol identifier                     */
/*                                                                           */
/* Output(s)          : pu1TunnelStatus - Tunnel status                      */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
PnacL2IwfGetProtocolTunnelStatusOnPort (UINT4 u4IfIndex, UINT2 u2Protocol,
                                        UINT1 *pu1TunnelStatus)
{
    L2IwfGetProtocolTunnelStatusOnPort (u4IfIndex, u2Protocol, pu1TunnelStatus);
}

/*****************************************************************************/
/* Function Name      : PnacL2IwfHandleOutgoingPktOnPort                     */
/*                                                                           */
/* Description        : This function calls the L2Iwf module to send out the */
/*                      PNAC packet.                                         */
/*                                                                           */
/* Input(s)           : pBuf - pointer to the outgoing packet                */
/*                      TaggedPortBitmap - Port list                         */
/*                      u4PktSize        - Packet size                       */
/*                      u2Protocol       - Protocol type                     */
/*                      u1Encap          - Encapsulation Type                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacL2IwfHandleOutgoingPktOnPort (tCRU_BUF_CHAIN_HEADER * pBuf,
                                  UINT2 u2IfIndex, UINT4 u4PktSize,
                                  UINT2 u2Protocol, UINT1 u1Encap)
{
    return (L2IwfHandleOutgoingPktOnPort (pBuf, u2IfIndex, u4PktSize,
                                          u2Protocol, u1Encap));
}

/*****************************************************************************/
/* Function Name      : PnacL2IwfIsPortInPortChannel                         */
/*                                                                           */
/* Description        : This function calls the L2IWF module to check        */
/*                      whether the port is in port-channel.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacL2IwfIsPortInPortChannel (UINT4 u4IfIndex)
{
    return (L2IwfIsPortInPortChannel (u4IfIndex));
}

/*****************************************************************************/
/* Function Name      : PnacL2IwfPnacHLPortOperIndication                    */
/*                                                                           */
/* Description        : This function calls the L2IWF module to check        */
/*                      whether the port is in port-channel.                 */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface Identifier                     */
/*                      u1OperStatus - Oper status                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacL2IwfPnacHLPortOperIndication (UINT2 u2IfIndex, UINT1 u1OperStatus)
{
    return (L2IwfPnacHLPortOperIndication (u2IfIndex, u1OperStatus));
}

/*****************************************************************************/
/* Function Name      : DPnacGetSlotIdFromPort                               */
/*                                                                           */
/* Description        : This function is called to get the Slot Id from port */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface Identifier                     */
/*                      pu4SlotId - Slot Identifier                          */
/*                                                                           */
/* Output(s)          : pu4SlotId - Slot Identifier                          */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
DPnacGetSlotIdFromPort (UINT2 u2IfIndex, UINT4 *pu4SlotId)
{
#ifdef MBSM_WANTED
    if (MbsmGetSlotFromPort ((UINT4) u2IfIndex, (INT4 *) pu4SlotId) ==
        MBSM_FAILURE)
    {
        return PNAC_FAILURE;

    }
#else
    UNUSED_PARAM (u2IfIndex);
    UNUSED_PARAM (pu4SlotId);
#endif

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacGetDissMasterSlotId                             */
/*                                                                           */
/* Description        : This function is called for getting Master Slot Id   */
/*                                                                           */
/* Input(s)           : pu4SlotId - Slot Identifier                          */
/*                                                                           */
/* Output(s)          : pu4SlotId - Slot Identifier                          */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
DPnacGetMasterSlotId (UINT4 *pu4SlotId)
{
#ifdef MBSM_WANTED
    MbsmGetDissMasterSlotId (pu4SlotId);
#else
    UNUSED_PARAM (pu4SlotId);
#endif
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacIsConnectingPort                                */
/*                                                                           */
/* Description        : This function is called to check port is connecting  */
/*                      port or not.                                         */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Identifier                          */
/*                      pHwPortInfo - Port Info                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
DPnacIsConnectingPort (UINT4 u4IfIndex)
{
#ifdef MBSM_WANTED
    if (MbsmIsConnectingPort (u4IfIndex) == MBSM_TRUE)
    {
        return PNAC_SUCCESS;
    }
#else
    UNUSED_PARAM (u4IfIndex);
#endif
    return PNAC_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DPnacGetSlotIdFromConnectingPort                     */
/*                                                                           */
/* Description        : This function is called to get the slot id from      */
/*                      connecting port                                      */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Identifier                          */
/*                      pHwPortInfo - Port Info                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
DPnacGetSlotIdFromConnectingPort (UINT4 u4IfIndex, UINT4 *pu4SlotId)
{
#ifdef MBSM_WANTED
    if (MbsmGetSlotIdFromConnectingPort (u4IfIndex, pu4SlotId) != MBSM_SUCCESS)
    {
        return PNAC_FAILURE;
    }
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4SlotId);
#endif
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacGetDistributionPortFromSlotId                   */
/*                                                                           */
/* Description        : This function is called to get from port from Slot   */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Identifier                          */
/*                      u4SlotId - Slot Identifier                           */
/*                                                                           */
/* Output(s)          : u4IfIndex - Port Identifier                          */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

INT4
DPnacGetDistributionPortFromSlotId (UINT4 u4SlotId, UINT4 *pu4IfIndex)
{
#ifdef MBSM_WANTED
    if (MbsmGetConnectingPortFromSlotId (u4SlotId, pu4IfIndex) != MBSM_SUCCESS)
    {
        return PNAC_FAILURE;
    }
    else
    {
        return PNAC_SUCCESS;
    }
#else
    UNUSED_PARAM (u4SlotId);
    UNUSED_PARAM (pu4IfIndex);
#endif
    return PNAC_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DPnacGetBridgeIndexFromIfIndex                       */
/*                                                                           */
/* Description        : This function converts the CFA Index to              */
/*                      Bridge Index by referring Interface Map Table        */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pu4BrgIndex - pointer to Bridge Index                */
/*                                                                           */
/* Return Value(s)    : PNAC_FAILURE - On Failure                            */
/*                      PNAC_SUCCESS - On Success                            */
/*****************************************************************************/
INT4
DPnacGetBridgeIndexFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4BrgIndex)
{
#ifdef MBSM_WANTED
    if (MbsmGetBridgeIndexFromIfIndex (u4IfIndex, pu4BrgIndex) != MBSM_SUCCESS)
    {
        return PNAC_FAILURE;
    }
#else
    *pu4BrgIndex = u4IfIndex;
#endif
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacGetIfIndexFromBridgeIndex                       */
/*                                                                           */
/* Description        : This function converts the Bridge Index to           */
/*                      CFA Index by referring MBSM Interface Map Table      */
/*                                                                           */
/* Input(s)           : u4BrgIndex - Bridge index                            */
/*                                                                           */
/* Output(s)          : pu4IfIndex - Pointer to Interface Index              */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - On Succes                             */
/*                      PNAC_FAILURE - On Failure                            */
/*****************************************************************************/
INT4
DPnacGetIfIndexFromBridgeIndex (UINT4 u4BrgIndex, UINT4 *pu4IfIndex)
{
#ifdef MBSM_WANTED
    if (MbsmGetIfIndexFromBridgeIndex (u4BrgIndex, pu4IfIndex) != MBSM_SUCCESS)
    {
        return PNAC_FAILURE;
    }
#else
    *pu4IfIndex = u4BrgIndex;
#endif
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacCheckIsLocalPort                                */
/*                                                                           */
/* Description        : This function checks if the port is local or Remote  */
/*                                                                           */
/* Input(s)           : u2PortNumber - Port Number                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_FAILURE- If port is remote port                 */
/*                      PNAC_SUCCESS- If port is Local port                  */
/*****************************************************************************/
INT4
DPnacCheckIsLocalPort (UINT2 u2PortNumber)
{
#ifdef MBSM_WANTED
    UINT4               u4SwitchId = 0;
    UINT4               u4SlotId = 0xff;

    if (IssGetStackingModel () == ISS_DISS_STACKING_MODEL)
    {

        u4SwitchId = (UINT4) IssGetSwitchid ();

        if (MbsmGetSlotIdFromRemotePort ((UINT4) u2PortNumber, &u4SlotId) ==
            MBSM_SUCCESS)
        {
            if (u4SwitchId != u4SlotId)
            {
                return PNAC_FAILURE;
            }
        }
        else
        {
            return PNAC_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u2PortNumber);
#endif
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacGetSlotStatus                                   */
/*                                                                           */
/* Description        : This function returns the Node status(active         */
/*                      inactive) of the given slot                          */
/*                                                                           */
/* Input(s)           : Slot Identifier- Interface index                     */
/*                                                                           */
/* Output(s)          : pu4BrgIndex - pointer to Bridge Index                */
/*                                                                           */
/* Return Value(s)    : PNAC_FAILURE - On Failure                            */
/*                      PNAC_SUCCESS - On Success                            */
/*****************************************************************************/
INT4
DPnacGetSlotStatus (UINT4 u4SlotId)
{
#ifdef MBSM_WANTED
    if (MbsmGetSlotIndexStatus ((INT4) u4SlotId) != MBSM_STATUS_ACTIVE)
    {
        return PNAC_FAILURE;
    }
#else
    UNUSED_PARAM (u4SlotId);
#endif
    return PNAC_SUCCESS;
}

#ifdef L2RED_WANTED
#ifdef LA_WANTED
/*****************************************************************************/
/* Function Name      : PnacLaRedRcvPktFromRm                                */
/*                                                                           */
/* Description        : This function calls the LA Module.                   */
/*                                                                           */
/* Input(s)           : u1Event - Event given by RM module.                  */
/*                      pData   - Msg to be enqueue.                         */
/*                      u2DataLen - Msg size.                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacLaRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    return (LaRedRcvPktFromRm (u1Event, pData, u2DataLen));
}
#else
#if defined(RSTP_WANTED) || defined(MSTP_WANTED)
/*****************************************************************************/
/* Function Name      : PnacAstHandleUpdateEvents                            */
/*                                                                           */
/* Description        : This function calls the STP module to update the     */
/*                      event.                                               */
/*                                                                           */
/* Input(s)           : u1Event - Event given by RM module.                  */
/*                      pData   - Msg to be enqueue.                         */
/*                      u2DataLen - Msg size.                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
PnacAstHandleUpdateEvents (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    return (AstHandleUpdateEvents (u1Event, pData, u2DataLen));
}
#else
#ifdef VLAN_WANTED
/*****************************************************************************/
/* Function Name      : PnacVlanRedHandleUpdateEvents                        */
/*                                                                           */
/* Description        : This function calls the VLAN module to update the    */
/*                      event.                                               */
/*                                                                           */
/* Input(s)           : u1Event - Event given by RM module.                  */
/*                      pData   - Msg to be enqueue.                         */
/*                      u2DataLen - Msg size.                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
PnacVlanRedHandleUpdateEvents (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    return (VlanRedHandleUpdateEvents (u1Event, pData, u2DataLen));
}
#else
#ifdef LLDP_WANTED
/*****************************************************************************/
/* Function Name      : PnacLldpRedRcvPktFromRm                              */
/*                                                                           */
/* Description        : For received events from RM this function generates  */
/*                      the corresponding events and sends the event to LLDP */
/*                      task.                                                */
/*                                                                           */
/* Input(s)           : u1Event   - Event to be sent to LLDP                 */
/*                      pData   - Msg to be enqueue.                         */
/*                      u2DataLen - Msg size.                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : if the event is sent successfully then return        */
/*                      OSIX_SUCCESS otherwise return OSIX_FAILURE           */
/*****************************************************************************/
UINT4
PnacLldpRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    return (LldpRedRcvPktFromRm (u1Event, pData, u2DataLen));
}
#endif
#endif
#endif
#endif

/*****************************************************************************/
/* Function Name      : PnacRmEnqMsgToRmFromAppl                             */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : pRmMsg - msg from appl.                              */
/*                      u2DataLen - Len of msg.                              */
/*                      u4SrcEntId - EntId from which the msg has arrived.   */
/*                      u4DestEntId - EntId to which the msg has to be sent. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
PnacRmEnqMsgToRmFromAppl (tRmMsg * pRmMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                          UINT4 u4DestEntId)
{
    return (RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen, u4SrcEntId, u4DestEntId));
}

/*****************************************************************************/
/* Function Name      : PnacRmGetNodeState                                   */
/*                                                                           */
/* Description        : This function calls the RM module to get the node    */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
PnacRmGetNodeState (VOID)
{
    return (RmGetNodeState ());
}

/*****************************************************************************/
/* Function Name      : PnacRmGetStandbyNodeCount                            */
/*                                                                           */
/* Description        : This function calls the RM module to get the number  */
/*                      of peer nodes that are up.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
PnacRmGetStandbyNodeCount (VOID)
{
    return (RmGetStandbyNodeCount ());
}

/*****************************************************************************/
/* Function Name      : PnacRmGetStaticConfigStatus                          */
/*                                                                           */
/* Description        : This function calls the RM module to get the         */
/*                      status of static configuration restoration.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
PnacRmGetStaticConfigStatus (VOID)
{
    return (RmGetStaticConfigStatus ());
}

/*****************************************************************************/
/* Function Name      : PnacRmHandleProtocolEvent                         */
/*                                                                           */
/* Description        : This function calls the RM module to intimate about  */
/*                      the protocol operations                              */
/*                                                                           */
/* Input(s)           : pEvt->u4AppId - Application Id                       */
/*                      pEvt->u4Event - Event send by protocols to RM        */
/*                                      (RM_PROTOCOL_BULK_UPDT_COMPLETION /  */
/*                                      RM_BULK_UPDT_ABORT /                 */
/*                                      RM_STANDBY_TO_ACTIVE_EVT_PROCESSED / */
/*                                      RM_IDLE_TO_ACTIVE_EVT_PROCESSED /    */
/*                                      RM_STANDBY_EVT_PROCESSED)            */
/*                      pEvt->u4Error - Error code (RM_MEMALLOC_FAIL /       */
/*                                      RM_SENDTO_FAIL / RM_PROCESS_FAIL)    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
PnacRmHandleProtocolEvent (tRmProtoEvt * pEvt)
{
    return (RmApiHandleProtocolEvent (pEvt));
}

/*****************************************************************************/
/* Function Name      : PnacRmRegisterProtocols                              */
/*                                                                           */
/* Description        : This function calls the RM module to register.       */
/*                                                                           */
/* Input(s)           : tRmRegParams - Reg. params to be provided by         */
/*                      protocols.                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
PnacRmRegisterProtocols (tRmRegParams * pRmReg)
{
    return (RmRegisterProtocols (pRmReg));
}

/*****************************************************************************/
/* Function Name      : PnacRmDeRegisterProtocols                            */
/*                                                                           */
/* Description        : This function calls the RM module to deregister.     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
UINT4
PnacRmDeRegisterProtocols (VOID)
{
    return (RmDeRegisterProtocols (RM_PNAC_APP_ID));
}

/*****************************************************************************/
/* Function Name      : PnacRmReleaseMemoryForMsg                            */
/*                                                                           */
/* Description        : This function calls the RM module to release the     */
/*                      memory allocated for sending the Standby node count. */
/*                                                                           */
/* Input(s)           : pu1Block - Mmemory block.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
PnacRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    return (RmReleaseMemoryForMsg (pu1Block));
}

/*****************************************************************************/
/* Function Name      : PnacRmSetBulkUpdatesStatus                           */
/*                                                                           */
/* Description        : This function calls the RM module to set the Status  */
/*                      of the bulk updates.                                 */
/*                                                                           */
/* Input(s)           : u4AppId - Application Identifier.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PnacRmSetBulkUpdatesStatus (UINT4 u4AppId)
{
    return (RmSetBulkUpdatesStatus (u4AppId));
}
#endif

/*****************************************************************************
 * Function Name      : PnacNpFailLog                               
 *                                                                           
 * Description        : This function sends the NP Fail Error  Message to 
 *                      SysLog.                      
 *                                                                           
 * Input(s)           : pPnacNpTrapLogInfo - Pointer to PnacNpTrapLogInfo
 *                                           Structure.
 * 
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : None                                                 
 *                                                                           
 ****************************************************************************/
VOID
PnacNpFailLog (tPnacNpTrapLogInfo * pPnacNpTrapLogInfo)
{
#ifdef SYSLOG_WANTED
    CHR1               *pac1PnacNpCall[] = {
        "PnacHwSetAuthStatus",
        "PnacHwAddOrDelSessStatus"
    };

    CHR1               *pac1PnacCtrlDir[] = {
        "BOTH",
        "IN"
    };

    CHR1               *pac1PnacAuthMode[] = {
        "Port Based",
        "MAC Based"
    };

    CHR1               *pac1PnacAuthStatus[] = {
        "AUTHORISED",
        "UNAUTHORISED",
        "SESSION DELETION"
    };

    UINT4               u4PnacVal = PNAC_NO_VAL;

    CHR1                ac1PnacNpSysLogMsg[130];
    UINT1               au1MacBuf[PNAC_CLI_NAME_SIZE];

    PNAC_MEMSET (ac1PnacNpSysLogMsg, PNAC_NO_VAL, sizeof (ac1PnacNpSysLogMsg));
    PNAC_MEMSET (au1MacBuf, 0, PNAC_CLI_NAME_SIZE);

    PrintMacAddress (pPnacNpTrapLogInfo->SuppMacAddr, (UINT1 *) au1MacBuf);

    if (pPnacNpTrapLogInfo->u1PnacAuthMode == PNAC_PORT_AUTHMODE_PORTBASED)
    {
        SNPRINTF (ac1PnacNpSysLogMsg, sizeof (ac1PnacNpSysLogMsg),
                  "%s; Port No: %d; Ctrl Dir: %s; Mode: %s; %s",
                  pac1PnacNpCall[u4PnacVal],
                  pPnacNpTrapLogInfo->u2Port,
                  pac1PnacCtrlDir[pPnacNpTrapLogInfo->u1PnacPortCtrlDir],
                  pac1PnacAuthMode[pPnacNpTrapLogInfo->u1PnacAuthMode - 1],
                  pac1PnacAuthStatus[pPnacNpTrapLogInfo->u1PnacAuthStatus - 1]);
    }
    else if (pPnacNpTrapLogInfo->u1PnacAuthMode == PNAC_PORT_AUTHMODE_MACBASED)
    {
        if (pPnacNpTrapLogInfo->u1PnacAuthStatus == PNAC_MAC_SESS_DEL)
        {
            u4PnacVal = 1;
        }

        SNPRINTF (ac1PnacNpSysLogMsg, sizeof (ac1PnacNpSysLogMsg),
                  "%s; Port No: %d; Supp MAC Addr: %s; Mode: %s; %s",
                  pac1PnacNpCall[u4PnacVal],
                  pPnacNpTrapLogInfo->u2Port, au1MacBuf,
                  pac1PnacAuthMode[pPnacNpTrapLogInfo->u1PnacAuthMode - 1],
                  pac1PnacAuthStatus[pPnacNpTrapLogInfo->u1PnacAuthStatus - 1]);
    }

    /* Send the Log Message to SysLog */
    SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gPnacSystemInfo.i4SysLogId,
                  "[NP-FAULT] %s", ac1PnacNpSysLogMsg));

#else /* NOT SYSLOG_WANTED */
    UNUSED_PARAM (pPnacNpTrapLogInfo);
#endif /* SYSLOG_WANTED */

    return;
}

/*****************************************************************************
 * Function Name      : PnacNpFailNotify                               
 *                                                                           
 * Description        : This function sends NP Fail Notification to
 *                      SNMP Manager.                      
 *                                                                           
 * Input(s)           : pPnacNpTrapLogInfo - Pointer to PnacNpTrapLogInfo
 *                                           Structure.
 * 
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : None                                                 
 *                                                                           
 ****************************************************************************/
VOID
PnacNpFailNotify (tPnacNpTrapLogInfo * pPnacNpTrapLogInfo)
{
#ifdef SNMP_3_WANTED
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_OID_TYPE      PnacNpNotifyPrefix;
    tSNMP_OID_TYPE      PnacNpNotifySuffix;
    tSNMP_OID_TYPE      PnacNpNotify;
    tSNMP_OID_TYPE      PnacPortBasedPortStatusOidPrefix;
    tSNMP_OID_TYPE      PnacPortBasedPortStatusOidSuffix;
    tSNMP_OID_TYPE      PnacPortBasedPortStatusOid;
    tSNMP_OID_TYPE      PnacMacBasedPortNoOidPrefix;
    tSNMP_OID_TYPE      PnacMacBasedPortNoOidSuffix;
    tSNMP_OID_TYPE      PnacMacBasedPortNoOid;
    tSNMP_OID_TYPE      PnacMacBasedAuthStatusOidPrefix;
    tSNMP_OID_TYPE      PnacMacBasedAuthStatusOidSuffix;
    tSNMP_OID_TYPE      PnacMacBasedAuthStatusOid;
    tSNMP_OID_TYPE      PnacMacBasedSessionStatusOidPrefix;
    tSNMP_OID_TYPE      PnacMacBasedSessionStatusOidSuffix;
    tSNMP_OID_TYPE      PnacMacBasedSessionStatusOid;
    UINT4               au4PnacNpNotifyPrefix[] = { 1, 3, 6, 1, 4, 1 };
    UINT4               au4PnacNpNotifySuffix[] = { 64, 5, 0 };
    UINT4              *pu4PnacNpNotify = NULL;
    UINT4               au4PnacPortBasedPortStatusOidPrefix[] =
        { 1, 3, 6, 1, 4, 1 };
    UINT4               au4PnacPortBasedPortStatusOidSuffix[] =
        { 64, 1, 5, 1, 6, 0 };
    UINT4              *pu4PnacPortBasedPortStatusOid = NULL;
    UINT4               au4PnacMacBasedPortNoOidPrefix[] = { 1, 3, 6, 1, 4, 1 };
    UINT4               au4PnacMacBasedPortNoOidSuffix[] =
        { 64, 2, 1, 1, 6, 0, 0, 0,
        0, 0, 0
    };
    UINT4              *pu4PnacMacBasedPortNoOid = NULL;
    UINT4               au4PnacMacBasedAuthStatusOidPrefix[] =
        { 1, 3, 6, 1, 4, 1 };
    UINT4               au4PnacMacBasedAuthStatusOidSuffix[] =
        { 64, 2, 1, 1, 5, 0, 0, 0,
        0, 0, 0
    };
    UINT4              *pu4PnacMacBasedAuthStatusOid = NULL;
    UINT4               au4PnacMacBasedSessionStatusOidPrefix[] =
        { 1, 3, 6, 1, 4, 1 };
    UINT4               au4PnacMacBasedSessionStatusOidSuffix[] =
        { 64, 4, 1, 1, 1, 0, 0, 0,
        0, 0, 0
    };
    UINT4              *pu4PnacMacBasedSessionStatusOid = NULL;

    UINT4               au4SnmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };

    UINT4               au4PnacPortBasedCtrlDirOid[]
        = { 1, 0, 8802, 1, 1, 1, 1, 2, 1, 1, 4, 0 };

    /* MAC Based OID's */

    UINT4               u4ArrLen = PNAC_NO_VAL;
    UINT4               u4TempArrLen = PNAC_NO_VAL;

    INT4                i4MacAddrLen = PNAC_NO_VAL;
    INT4                i4PnacMacSessStatus = PNAC_NO_VAL;

    INT1                i1RetVal = PNAC_FAILURE;
    INT4                i4AllocationCount = PNAC_NO_VAL;

    if (PNAC_ALLOCATE_NOTIFY_OID_MEMBLK (pu4PnacNpNotify) == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Unable to allocate memory. \r\n");
        return;
    }
    i4AllocationCount++;

    if (PNAC_ALLOCATE_NOTIFY_OID_MEMBLK (pu4PnacPortBasedPortStatusOid) == NULL)
    {
        if (PnacMemRelease (gPnacSystemInfo.npNotifyOidMemPoolId,
                            i4AllocationCount, pu4PnacNpNotify) == PNAC_FAILURE)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      "Unable to Free memory. \r\n");
        }
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacNpNotify));
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Unable to allocate memory. \r\n");
        return;
    }
    i4AllocationCount++;

    if (PNAC_ALLOCATE_NOTIFY_OID_MEMBLK (pu4PnacMacBasedPortNoOid) == NULL)
    {
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacNpNotify));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacPortBasedPortStatusOid));
        return;
    }
    i4AllocationCount++;

    if (PNAC_ALLOCATE_NOTIFY_OID_MEMBLK (pu4PnacMacBasedAuthStatusOid) == NULL)
    {
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacNpNotify));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacPortBasedPortStatusOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedPortNoOid));
        return;
    }
    i4AllocationCount++;

    if (PNAC_ALLOCATE_NOTIFY_OID_MEMBLK (pu4PnacMacBasedSessionStatusOid) ==
        NULL)
    {
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacNpNotify));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacPortBasedPortStatusOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedPortNoOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedAuthStatusOid));
        return;
    }
    i4AllocationCount++;

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    PnacNpNotifyPrefix.pu4_OidList = au4PnacNpNotifyPrefix;
    PnacNpNotifyPrefix.u4_Length =
        sizeof (au4PnacNpNotifyPrefix) / sizeof (UINT4);

    PnacNpNotifySuffix.pu4_OidList = au4PnacNpNotifySuffix;
    PnacNpNotifySuffix.u4_Length =
        sizeof (au4PnacNpNotifySuffix) / sizeof (UINT4);

    MEMSET (pu4PnacNpNotify, 0, SNMP_MAX_OID_LENGTH);

    PnacNpNotify.pu4_OidList = pu4PnacNpNotify;
    PnacNpNotify.u4_Length = 0;

    if (SNMPAddEnterpriseOid (&PnacNpNotifyPrefix, &PnacNpNotifySuffix,
                              &PnacNpNotify) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacNpNotify));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacPortBasedPortStatusOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedPortNoOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedAuthStatusOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedSessionStatusOid));
        return;
    }

    PnacPortBasedPortStatusOidPrefix.pu4_OidList =
        au4PnacPortBasedPortStatusOidPrefix;
    PnacPortBasedPortStatusOidPrefix.u4_Length =
        sizeof (au4PnacPortBasedPortStatusOidPrefix) / sizeof (UINT4);

    PnacPortBasedPortStatusOidSuffix.pu4_OidList =
        au4PnacPortBasedPortStatusOidSuffix;
    PnacPortBasedPortStatusOidSuffix.u4_Length =
        sizeof (au4PnacPortBasedPortStatusOidSuffix) / sizeof (UINT4);

    MEMSET (pu4PnacPortBasedPortStatusOid, 0, SNMP_MAX_OID_LENGTH);

    PnacPortBasedPortStatusOid.pu4_OidList = pu4PnacPortBasedPortStatusOid;
    PnacPortBasedPortStatusOid.u4_Length = 0;

    if (SNMPAddEnterpriseOid
        (&PnacPortBasedPortStatusOidPrefix, &PnacPortBasedPortStatusOidSuffix,
         &PnacPortBasedPortStatusOid) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacNpNotify));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacPortBasedPortStatusOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedPortNoOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedAuthStatusOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedSessionStatusOid));
        return;
    }

    PnacMacBasedPortNoOidPrefix.pu4_OidList = au4PnacMacBasedPortNoOidPrefix;
    PnacMacBasedPortNoOidPrefix.u4_Length =
        sizeof (au4PnacMacBasedPortNoOidPrefix) / sizeof (UINT4);

    PnacMacBasedPortNoOidSuffix.pu4_OidList = au4PnacMacBasedPortNoOidSuffix;
    PnacMacBasedPortNoOidSuffix.u4_Length =
        sizeof (au4PnacMacBasedPortNoOidSuffix) / sizeof (UINT4);

    MEMSET (pu4PnacMacBasedPortNoOid, 0, SNMP_MAX_OID_LENGTH);

    PnacMacBasedPortNoOid.pu4_OidList = pu4PnacMacBasedPortNoOid;
    PnacMacBasedPortNoOid.u4_Length = 0;

    if (SNMPAddEnterpriseOid
        (&PnacMacBasedPortNoOidPrefix, &PnacMacBasedPortNoOidSuffix,
         &PnacMacBasedPortNoOid) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacNpNotify));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacPortBasedPortStatusOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedPortNoOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedAuthStatusOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedSessionStatusOid));
        return;
    }

    PnacMacBasedAuthStatusOidPrefix.pu4_OidList =
        au4PnacMacBasedAuthStatusOidPrefix;
    PnacMacBasedAuthStatusOidPrefix.u4_Length =
        sizeof (au4PnacMacBasedAuthStatusOidPrefix) / sizeof (UINT4);

    PnacMacBasedAuthStatusOidSuffix.pu4_OidList =
        au4PnacMacBasedAuthStatusOidSuffix;
    PnacMacBasedAuthStatusOidSuffix.u4_Length =
        sizeof (au4PnacMacBasedAuthStatusOidSuffix) / sizeof (UINT4);

    MEMSET (pu4PnacMacBasedAuthStatusOid, 0, SNMP_MAX_OID_LENGTH);

    PnacMacBasedAuthStatusOid.pu4_OidList = pu4PnacMacBasedAuthStatusOid;
    PnacMacBasedAuthStatusOid.u4_Length = 0;

    if (SNMPAddEnterpriseOid
        (&PnacMacBasedAuthStatusOidPrefix, &PnacMacBasedAuthStatusOidSuffix,
         &PnacMacBasedAuthStatusOid) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacNpNotify));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacPortBasedPortStatusOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedPortNoOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedAuthStatusOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedSessionStatusOid));
        return;
    }

    PnacMacBasedSessionStatusOidPrefix.pu4_OidList =
        au4PnacMacBasedSessionStatusOidPrefix;
    PnacMacBasedSessionStatusOidPrefix.u4_Length =
        sizeof (au4PnacMacBasedSessionStatusOidPrefix) / sizeof (UINT4);

    PnacMacBasedSessionStatusOidSuffix.pu4_OidList =
        au4PnacMacBasedSessionStatusOidSuffix;
    PnacMacBasedSessionStatusOidSuffix.u4_Length =
        sizeof (au4PnacMacBasedSessionStatusOidSuffix) / sizeof (UINT4);

    MEMSET (pu4PnacMacBasedSessionStatusOid, 0, SNMP_MAX_OID_LENGTH);

    PnacMacBasedSessionStatusOid.pu4_OidList = pu4PnacMacBasedSessionStatusOid;
    PnacMacBasedSessionStatusOid.u4_Length = 0;

    if (SNMPAddEnterpriseOid
        (&PnacMacBasedSessionStatusOidPrefix,
         &PnacMacBasedSessionStatusOidSuffix,
         &PnacMacBasedSessionStatusOid) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacNpNotify));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacPortBasedPortStatusOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedPortNoOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedAuthStatusOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedSessionStatusOid));
        return;
    }

    /* Trap OID construction. Telling Manager that you have received
     * trap for PNAC NP Fail Notifications */
    pOid = alloc_oid (sizeof (au4SnmpTrapOid) / sizeof (UINT4));
    if (pOid == NULL)
    {
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacNpNotify));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacPortBasedPortStatusOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedPortNoOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedAuthStatusOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedSessionStatusOid));
        return;
    }
    MEMCPY (pOid->pu4_OidList, au4SnmpTrapOid, sizeof (au4SnmpTrapOid));
    pOid->u4_Length = sizeof (au4SnmpTrapOid) / sizeof (UINT4);

    pOidValue = alloc_oid (SNMP_MAX_OID_LENGTH);
    if (pOidValue == NULL)
    {
        free_oid (pOid);
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacNpNotify));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacPortBasedPortStatusOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedPortNoOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedAuthStatusOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedSessionStatusOid));
        return;
    }
    MEMCPY (pOidValue->pu4_OidList, PnacNpNotify.pu4_OidList,
            PnacNpNotify.u4_Length * sizeof (UINT4));
    pOidValue->u4_Length = PnacNpNotify.u4_Length;

    pVbList = (tSNMP_VAR_BIND *)
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OBJECT_ID, 0L, 0, NULL,
                              pOidValue, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacNpNotify));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacPortBasedPortStatusOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedPortNoOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedAuthStatusOid));
        MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                            (UINT1 *) (pu4PnacMacBasedSessionStatusOid));
        return;
    }

    pStartVb = pVbList;

    switch (pPnacNpTrapLogInfo->u1PnacAuthMode)
    {
        case PNAC_PORT_AUTHMODE_PORTBASED:
            /* Trap Oid Construction for Port Based Auth Status */
            u4ArrLen = SNMP_MAX_OID_LENGTH;
            pOid = alloc_oid (u4ArrLen);
            if (pOid == NULL)
            {
                free_oid (pOidValue);
                SNMP_free_snmp_vb_list (pStartVb);
                i1RetVal = PNAC_FAILURE;
                break;
            }
            *(pu4PnacPortBasedPortStatusOid +
              (PnacPortBasedPortStatusOid.u4_Length - 1)) =
(UINT4) pPnacNpTrapLogInfo->u2Port;
            MEMCPY (pOid->pu4_OidList, PnacPortBasedPortStatusOid.pu4_OidList,
                    PnacPortBasedPortStatusOid.u4_Length * sizeof (UINT4));
            pOid->u4_Length = PnacPortBasedPortStatusOid.u4_Length;
            pVbList->pNextVarBind =
                ((tSNMP_VAR_BIND *)
                 SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0,
                                       (INT4)
                                       pPnacNpTrapLogInfo->u1PnacAuthStatus,
                                       NULL, NULL, SnmpCnt64Type));
            if (pVbList->pNextVarBind == NULL)
            {
                free_oid (pOid);
                free_oid (pOidValue);
                SNMP_free_snmp_vb_list (pStartVb);
                i1RetVal = PNAC_FAILURE;
                break;
            }

            /* Trap Oid Construction for Port Based Port Control Direction */
            pVbList = pVbList->pNextVarBind;
            u4ArrLen = sizeof (au4PnacPortBasedCtrlDirOid) / sizeof (UINT4);
            pOid = alloc_oid (u4ArrLen);
            if (pOid == NULL)
            {
                free_oid (pOidValue);
                SNMP_free_snmp_vb_list (pStartVb);
                i1RetVal = PNAC_FAILURE;
                break;
            }
            au4PnacPortBasedCtrlDirOid[u4ArrLen - 1]
                = (UINT4) pPnacNpTrapLogInfo->u2Port;
            MEMCPY (pOid->pu4_OidList, au4PnacPortBasedCtrlDirOid,
                    sizeof (au4PnacPortBasedCtrlDirOid));
            pOid->u4_Length = u4ArrLen;
            pVbList->pNextVarBind =
                ((tSNMP_VAR_BIND *)
                 SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0,
                                       (INT4)
                                       pPnacNpTrapLogInfo->u1PnacPortCtrlDir,
                                       NULL, NULL, SnmpCnt64Type));
            if (pVbList->pNextVarBind == NULL)
            {
                free_oid (pOid);
                free_oid (pOidValue);
                SNMP_free_snmp_vb_list (pStartVb);
                i1RetVal = PNAC_FAILURE;
                break;
            }

            i1RetVal = PNAC_SUCCESS;

            break;
        case PNAC_PORT_AUTHMODE_MACBASED:
            /* Trap Oid Construction for MAC Based Port Number */
            u4ArrLen = SNMP_MAX_OID_LENGTH;
            pOid = alloc_oid (u4ArrLen);
            if (pOid == NULL)
            {
                free_oid (pOidValue);
                SNMP_free_snmp_vb_list (pStartVb);
                i1RetVal = PNAC_FAILURE;
                break;
            }
            for (u4TempArrLen = PnacMacBasedPortNoOid.u4_Length - 1,
                 i4MacAddrLen = (INT4) PNAC_MAC_ADDR_SIZE - 1;
                 i4MacAddrLen >= PNAC_NO_VAL; i4MacAddrLen--, u4TempArrLen--)
            {
                *(pu4PnacMacBasedPortNoOid + u4TempArrLen)
                    = (UINT4) pPnacNpTrapLogInfo->SuppMacAddr[i4MacAddrLen];
            }
            MEMCPY (pOid->pu4_OidList, PnacMacBasedPortNoOid.pu4_OidList,
                    PnacMacBasedPortNoOid.u4_Length * sizeof (UINT4));
            pOid->u4_Length = PnacMacBasedPortNoOid.u4_Length;
            pVbList->pNextVarBind =
                ((tSNMP_VAR_BIND *)
                 SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0,
                                       (INT4)
                                       pPnacNpTrapLogInfo->u2Port,
                                       NULL, NULL, SnmpCnt64Type));
            if (pVbList->pNextVarBind == NULL)
            {
                free_oid (pOid);
                free_oid (pOidValue);
                SNMP_free_snmp_vb_list (pStartVb);
                i1RetVal = PNAC_FAILURE;
                break;
            }

            /* Trap Oid Construction for MAC Based Auth Status */
            pVbList = pVbList->pNextVarBind;
            u4ArrLen = SNMP_MAX_OID_LENGTH;
            pOid = alloc_oid (u4ArrLen);
            if (pOid == NULL)
            {
                free_oid (pOidValue);
                SNMP_free_snmp_vb_list (pStartVb);
                i1RetVal = PNAC_FAILURE;
                break;
            }
            for (u4TempArrLen = PnacMacBasedAuthStatusOid.u4_Length - 1,
                 i4MacAddrLen = PNAC_MAC_ADDR_SIZE - 1;
                 i4MacAddrLen >= PNAC_NO_VAL; i4MacAddrLen--, u4TempArrLen--)
            {
                *(pu4PnacMacBasedAuthStatusOid + u4TempArrLen)
                    = (UINT4) pPnacNpTrapLogInfo->SuppMacAddr[i4MacAddrLen];
            }
            MEMCPY (pOid->pu4_OidList, PnacMacBasedAuthStatusOid.pu4_OidList,
                    PnacMacBasedAuthStatusOid.u4_Length * sizeof (UINT4));
            pOid->u4_Length = PnacMacBasedAuthStatusOid.u4_Length;
            pVbList->pNextVarBind =
                ((tSNMP_VAR_BIND *)
                 SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0,
                                       (INT4)
                                       pPnacNpTrapLogInfo->u1PnacAuthStatus,
                                       NULL, NULL, SnmpCnt64Type));
            if (pVbList->pNextVarBind == NULL)
            {
                free_oid (pOid);
                free_oid (pOidValue);
                SNMP_free_snmp_vb_list (pStartVb);
                i1RetVal = PNAC_FAILURE;
                break;
            }

            /* Trap Oid Construction for MAC Based SESSION Status
             * This Trap object is sent when the NPAPI Call 
             * PnacHwAddOrDelMacSess fails during deletion of SESSION. */
            if (pPnacNpTrapLogInfo->u1PnacAuthStatus != PNAC_MAC_SESS_DEL)
            {
                i4PnacMacSessStatus = (INT4) PNAC_MAC_AUTH_SESS_PRESENT;
            }
            else
            {
                i4PnacMacSessStatus = (INT4) PNAC_MAC_AUTH_SESS_DEL_FAILED;
            }

            pVbList = pVbList->pNextVarBind;
            u4ArrLen = SNMP_MAX_OID_LENGTH;
            pOid = alloc_oid (u4ArrLen);
            if (pOid == NULL)
            {
                free_oid (pOidValue);
                SNMP_free_snmp_vb_list (pStartVb);
                i1RetVal = PNAC_FAILURE;
                break;
            }
            for (u4TempArrLen = PnacMacBasedSessionStatusOid.u4_Length - 1,
                 i4MacAddrLen = PNAC_MAC_ADDR_SIZE - 1;
                 i4MacAddrLen >= PNAC_NO_VAL; i4MacAddrLen--, u4TempArrLen--)
            {
                *(pu4PnacMacBasedSessionStatusOid + u4TempArrLen)
                    = (UINT4) pPnacNpTrapLogInfo->SuppMacAddr[i4MacAddrLen];
            }
            MEMCPY (pOid->pu4_OidList, PnacMacBasedSessionStatusOid.pu4_OidList,
                    PnacMacBasedSessionStatusOid.u4_Length * sizeof (UINT4));
            pOid->u4_Length = PnacMacBasedSessionStatusOid.u4_Length;
            pVbList->pNextVarBind =
                ((tSNMP_VAR_BIND *)
                 SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0,
                                       i4PnacMacSessStatus, NULL, NULL,
                                       SnmpCnt64Type));
            if (pVbList->pNextVarBind == NULL)
            {
                free_oid (pOid);
                free_oid (pOidValue);
                SNMP_free_snmp_vb_list (pStartVb);
                i1RetVal = PNAC_FAILURE;
                break;
            }

            i1RetVal = PNAC_SUCCESS;

            break;
        default:
            break;
    }

    MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                        (UINT1 *) (pu4PnacNpNotify));
    MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                        (UINT1 *) (pu4PnacPortBasedPortStatusOid));
    MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                        (UINT1 *) (pu4PnacMacBasedPortNoOid));
    MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                        (UINT1 *) (pu4PnacMacBasedAuthStatusOid));
    MemReleaseMemBlock (gPnacSystemInfo.npNotifyOidMemPoolId,
                        (UINT1 *) (pu4PnacMacBasedSessionStatusOid));
    if (i1RetVal == PNAC_FAILURE)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        pVbList = pVbList->pNextVarBind;
        return;
    }
    else
    {
        pVbList = pVbList->pNextVarBind;
        pVbList->pNextVarBind = NULL;
    }
    /* Sending Trap to SNMP Module */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);

#else /* NOT SNMP_2_WANTED */
    UNUSED_PARAM (pPnacNpTrapLogInfo);
#endif /* SNMP_2_WANTED */

    return;
}

/*****************************************************************************
 * Function Name      : PnacNpGetNpapiMode                               
 *                                                                           
 * Description        : This function returns the NPAPI Mode of Processing.                      
 *                                                                           
 * Input(s)           : i4ProtoId - PNAC Protocol Identifier.
 * 
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : PNAC_NP_ASYNC or PNAC_NP_SYNC                                                 
 *                                                                           
 ****************************************************************************/
INT4
PnacNpGetNpapiMode (INT4 i4ProtoId)
{
    if (IssGetAsyncMode (i4ProtoId) == (INT4) PNAC_NP_ASYNC)
    {
        return ((INT4) PNAC_NP_ASYNC);
    }

    return ((INT4) PNAC_NP_SYNC);
}

#endif /* _PNACPORT_C */
