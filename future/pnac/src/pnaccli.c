
/* SOURCE FILE HEADER :
 *
 * $Id: pnaccli.c,v 1.90 2017/09/04 10:53:17 siva Exp $
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : pnaccli.c                                        |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : Jeeva Sethuraman                                 |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : PNAC                                             |
 * |                                                                           |
 * |  MODULE NAME           : PNAC CLI configurations                          |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : Action routines for set/get objects in           | 
 * |                          stdpnac.mib, fspnac.mib                          |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

#ifndef __PNACCLI_C__
#define __PNACCLI_C__

#include "pnachdrs.h"
#include "pnaccli.h"
#include "stdpnacli.h"
#include "fspnaccli.h"

extern void free_octetstring ARG_LIST ((tSNMP_OCTET_STRING_TYPE *));
extern tSNMP_OCTET_STRING_TYPE *allocmem_octetstring PROTO ((INT4));

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_process_pnac_cmd                               */
/*                                                                           */
/*     DESCRIPTION      : This function takes in variable no. of arguments   */
/*                        and process the commands for the PNAC module.      */
/*                        defined in pnaccli.h                               */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Command -  Command Identifier                    */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
cli_process_pnac_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[PNAC_CLI_MAX_COMMANDS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT4               u4CmdType = 0;
    UINT4               u4AuthTimeOut = 0;
    UINT1               u1DebugSet = PNAC_FALSE;
    tMacAddr            au1InMacAddr;

    /* If PNAC is not Started, then all commands are considered as invalid,
     * except Start/Shutdown command
     */
    if ((u4Command != CLI_PNAC_SYSTEMCONTROL) &&
        (u4Command != CLI_PNAC_NO_SYSTEMCONTROL) &&
        (u4Command != CLI_PNAC_NO_SYSTEMSHUT) &&
        (PNAC_SYSTEM_CONTROL == PNAC_SHUTDOWN))
    {
        if (u4Command != CLI_PNAC_SYSTEMSHUT)
        {
            CliPrintf (CliHandle, "\r%% Dot1x is shutdown\r\n");
        }
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* Third arguement is always interface name/index */

    u4IfIndex = va_arg (ap, UINT4);

    if ((u4Command == CLI_SHOW_D_PNAC) && (u4IfIndex == 0))
    {

    }
    else
    {
        if (!(u4IfIndex))
        {
            u4IfIndex = CLI_GET_IFINDEX ();
        }
    }

    /* Walk through the rest of the arguements and store in args array. 
     * Store PNAC_CLI_MAX_ARGS arguements at the max. 
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == PNAC_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    CliRegisterLock (CliHandle, PnacLock, PnacUnLock);

    PNAC_LOCK ();

    switch (u4Command)
    {
        case CLI_PNAC_SYSTEMSHUT:
            i4RetStatus = PnacSetSystemShut (CliHandle, PNAC_SHUTDOWN);
            break;

        case CLI_PNAC_NO_SYSTEMSHUT:
            i4RetStatus = PnacSetSystemShut (CliHandle, PNAC_START);
            break;

        case CLI_PNAC_SYSTEMCONTROL:
            i4RetStatus = PnacSetSystemControl (CliHandle, PNAC_ENABLED);
            break;

        case CLI_PNAC_NO_SYSTEMCONTROL:
            i4RetStatus = PnacSetSystemControl (CliHandle, PNAC_DISABLED);
            break;

        case CLI_PNAC_AUTH_SERVER:
            i4RetStatus =
                PnacSetAuthServer (CliHandle, CLI_PTR_TO_U4 (args[0]),
                                   CLI_PTR_TO_U4 (args[1]));
            break;

        case CLI_PNAC_NAS:
            i4RetStatus = PnacSetNasId (CliHandle, (UINT1 *) (args[0]));
            break;

        case CLI_PNAC_AUTH_SESSION_REAUTH:
            i4RetStatus = PnacSetReAuthInit (CliHandle, u4IfIndex);
            break;

        case CLI_PNAC_REAUTH:
            i4RetStatus = PnacSetPortReAuth (CliHandle, u4IfIndex,
                                             PNAC_MIB_TRUE);
            break;

        case CLI_PNAC_NO_REAUTH:
            i4RetStatus = PnacSetPortReAuth (CliHandle, u4IfIndex,
                                             PNAC_MIB_FALSE);
            break;

        case CLI_PNAC_TIMEOUT:
            i4RetStatus =
                PnacSetTimers (CliHandle, u4IfIndex, CLI_PTR_TO_U4 (args[0]),
                               *args[1]);
            break;

        case CLI_PNAC_NO_TIMEOUT:
            i4RetStatus =
                PnacSetNoTimers (CliHandle, u4IfIndex, CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_PNAC_AS_SETTING:

            if ((args[3]) != NULL)
            {
                u4AuthTimeOut = *(args[3]);
            }

            i4RetStatus =
                PnacSetASUserInfo (CliHandle, (UINT1 *) args[4],
                                   (UINT1 *) (args[0]),
                                   (UINT1 *) (args[1]),
                                   CLI_PTR_TO_U4 (args[2]), u4AuthTimeOut);
            break;

        case CLI_PNAC_NO_AS_SETTING:
            i4RetStatus = PnacSetNoASUserInfo (CliHandle, (UINT1 *) args[0]);

            break;

        case CLI_PNAC_MAXREQ_COUNT:
            i4RetStatus = PnacSetMaxReq (CliHandle, u4IfIndex, *(args[0]));
            break;

        case CLI_PNAC_NO_MAXREQ_COUNT:
            i4RetStatus = PnacSetMaxReq (CliHandle, u4IfIndex, PNAC_MAXREQ);
            break;
        case CLI_PNAC_MAXSTART_COUNT:
            i4RetStatus = PnacSetMaxStart (CliHandle, u4IfIndex, *(args[0]));
            break;

        case CLI_PNAC_NO_MAXSTART_COUNT:
            i4RetStatus = PnacSetMaxStart (CliHandle, u4IfIndex, PNAC_MAXSTART);
            break;

        case CLI_PNAC_PORT_DEFAULT:
            i4RetStatus = PnacSetDefaults (CliHandle, u4IfIndex);
            break;

        case CLI_PNAC_DEBUG:
            u1DebugSet = PNAC_TRUE;
            i4RetStatus =
                PnacSetDebug (CliHandle, CLI_PTR_TO_U4 (args[0]), u1DebugSet);
            break;

        case CLI_PNAC_NO_DEBUG:
            i4RetStatus =
                PnacSetDebug (CliHandle, CLI_PTR_TO_U4 (args[0]), u1DebugSet);
            break;

        case CLI_PNAC_PORT_CONTROL_SET:
            i4RetStatus = PnacSetPortControl (CliHandle, u4IfIndex,
                                              CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_PNAC_PORT_CONTROL_RESET:
            i4RetStatus = PnacSetPortControl (CliHandle, u4IfIndex,
                                              PNAC_CLI_FORCE_AUTHORIZED);
            break;
        case CLI_PNAC_SUPPACCESS_CONTROL_SET:
            i4RetStatus = PnacSetSuppAccessCtrlWithAuth (CliHandle,
                                                         u4IfIndex,
                                                         CLI_PTR_TO_U4 (args
                                                                        [0]));
            break;
        case CLI_PNAC_SUPPACCESS_CONTROL_RESET:
            i4RetStatus = PnacSetSuppAccessCtrlWithAuth (CliHandle, u4IfIndex,
                                                         PNAC_CLI_INACTIVE);
            break;

        case CLI_PNAC_CONTROL_DIRECTION_SET:
            i4RetStatus = PnacSetPortControlDirection (CliHandle,
                                                       u4IfIndex,
                                                       CLI_PTR_TO_U4 (args[0]));
            break;
        case CLI_PNAC_CONTROL_DIRECTION_RESET:
            i4RetStatus = PnacSetPortControlDirection (CliHandle, u4IfIndex,
                                                       PNAC_CLI_BOTH);
            break;

        case CLI_PNAC_AUTHMODE_SET:
            i4RetStatus = PnacSetPortAuthMode (CliHandle, u4IfIndex,
                                               CLI_PTR_TO_U4 (args[0]));
            break;
        case CLI_PNAC_AUTHMODE_RESET:
            i4RetStatus = PnacSetPortAuthMode (CliHandle, u4IfIndex,
                                               PNAC_CLI_PORTBASED);
            break;
        case CLI_PNAC_SESS_INIT:
            StrToMac ((UINT1 *) args[0], au1InMacAddr);
            i4RetStatus = PnacSessionInitialise (CliHandle, au1InMacAddr,
                                                 PNAC_ENABLED);
            break;
        case CLI_PNAC_SESS_REAUTH:
            StrToMac ((UINT1 *) args[0], au1InMacAddr);
            i4RetStatus = PnacSessionReauth (CliHandle, au1InMacAddr,
                                             PNAC_ENABLED);
            break;
        case CLI_PNAC_CENTRALIZED:
            i4RetStatus = PnacSetSystemMode (CliHandle, PNAC_CENTRALIZED);
            break;
        case CLI_PNAC_DISTRIBUTED:
            i4RetStatus = PnacSetSystemMode (CliHandle, PNAC_DISTRIBUTED);
            break;
        case CLI_DPNAC_SYNC_TIME:
            i4RetStatus = DPnacSetPeriodicSyncTime (CliHandle, *(args[0]));
            break;
        case CLI_DPNAC_SYNC_TIME_RESET:
            i4RetStatus =
                DPnacSetPeriodicSyncTime (CliHandle,
                                          PNAC_DEFAULT_PERIODIC_SYNC_TIME);
            break;
        case CLI_DPNAC_MAX_KEEP_ALIVE_COUNT:
            i4RetStatus = DPnacSetMaxAliveCount (CliHandle, (INT4) *(args[0]));
            break;
        case CLI_DPNAC_MAX_KEEP_ALIVE_COUNT_RESET:
            i4RetStatus =
                DPnacSetMaxAliveCount (CliHandle,
                                       PNAC_DEFAULT_MAX_KEEP_ALIVE_COUNT);
            break;
        case CLI_SHOW_D_PNAC:
            u4CmdType = CLI_PTR_TO_U4 (args[0]);
            switch (u4CmdType)
            {
                case DPNAC_CLI_AUTH_SLOT_INFO:
                    i4RetStatus = DPnacShowSlotDetails (CliHandle, u4IfIndex);
                    break;
                case DPNAC_CLI_AUTH_ALL:
                    i4RetStatus =
                        DPnacShowSlotDetails (CliHandle, MAX_DPNAC_SLOT_ALL);
                    break;
                case DPNAC_CLI_STAT_SLOT_INFO:
                    i4RetStatus = DPnacShowSlotStats (CliHandle, u4IfIndex);
                    break;
                case DPNAC_CLI_STAT_ALL:
                    i4RetStatus =
                        DPnacShowSlotStats (CliHandle, MAX_DPNAC_SLOT_ALL);
                    break;
                case DPNAC_CLI_DETAIL:
                    i4RetStatus = DPnacShowDetail (CliHandle);
                    break;
                default:
                    break;

            }
            break;
        case CLI_SHOW_PNAC:
            u4CmdType = CLI_PTR_TO_U4 (args[0]);
            switch (u4CmdType)
            {
                case PNAC_CLI_IF_INFO:
                    i4RetStatus = PnacShowInterfaceInfo (CliHandle, u4IfIndex);
                    break;

                case PNAC_CLI_STAT_IF_INFO:
                    i4RetStatus =
                        PnacShowAuthInterfaceStats (CliHandle, u4IfIndex);
                    break;

                case PNAC_CLI_SUPPSTAT_IF_INFO:
                    i4RetStatus =
                        PnacShowSuppInterfaceStats (CliHandle, u4IfIndex);
                    break;

                case PNAC_CLI_DATABASE_INFO:
                    i4RetStatus = PnacShowDatabaseInfo (CliHandle);
                    break;

                case PNAC_CLI_ALL_INTF_INFO:
                    i4RetStatus = PnacShowInterfaceInfo (CliHandle, 0);
                    break;

                case PNAC_CLI_ALL_INFO:
                    i4RetStatus = PnacShowGlobalInfo (CliHandle);
                    break;
                case PNAC_CLI_MAC_INFO:
                    StrToMac ((UINT1 *) args[1], au1InMacAddr);
                    i4RetStatus = PnacShowMacInfo (CliHandle, au1InMacAddr);
                    break;
                case PNAC_CLI_ALLMAC_INFO:
                    i4RetStatus = PnacShowAllMacInfo (CliHandle);
                    break;
                case PNAC_CLI_MAC_STATS:
                    StrToMac ((UINT1 *) args[2], au1InMacAddr);
                    i4RetStatus = PnacShowMacStats (CliHandle, au1InMacAddr);
                    break;
                case PNAC_CLI_ALLMAC_STATS:
                    i4RetStatus = PnacShowAllMacStats (CliHandle);
                    break;
            }
            break;
        case CLI_CLEAR_PNAC_STATS:
            i4RetStatus = PnacClearAllStats (CliHandle);
            if (i4RetStatus == CLI_SUCCESS)
            {
                CliPrintf (CliHandle, "\rInterface and MAC Statistics"
                           " cleared successfully\r\n");
            }
            break;
        case CLI_CLEAR_PNAC_STATS_IF_MAC_INFO:
            StrToMac ((UINT1 *) args[0], au1InMacAddr);
            i4RetStatus =
                PnacClearStatsIfMacInfo (CliHandle, u4IfIndex, au1InMacAddr);
            if (i4RetStatus == CLI_SUCCESS)
            {
                CliPrintf (CliHandle, "\rStatistics cleared successfully for "
                           "the MAC and Port Number\r\n");
            }

            break;
        case CLI_CLEAR_PNAC_STATS_IF_INFO:
            i4RetStatus = PnacClearStatsIfInfo (CliHandle, u4IfIndex);
            if (i4RetStatus == CLI_SUCCESS)
            {
                CliPrintf (CliHandle, "\rInterface Statistics cleared"
                           " successfully\r\n");
            }

            break;
        case CLI_CLEAR_PNAC_STATS_MAC_INFO:
            StrToMac ((UINT1 *) args[0], au1InMacAddr);
            i4RetStatus = PnacClearStatsMacInfo (CliHandle, au1InMacAddr);
            if (i4RetStatus == CLI_SUCCESS)
            {
                CliPrintf (CliHandle, "\rMAC Statistics cleared"
                           " successfully\r\n");
            }

            break;
        case CLI_PNAC_NO_AUTH_SERVER:
            i4RetStatus =
                PnacSetAuthServer (CliHandle, PNAC_REMOTE_AUTH_SERVER,
                                   PNAC_REMOTE_RADIUS_AUTH_SERVER);
            break;
        case CLI_PNAC_PORT_AUTHSTATUS_SET:

            i4RetStatus =
                PnacSetPortAuthStatus (CliHandle, u4IfIndex,
                                       CLI_PTR_TO_U4 (args[0]));
            break;
        case CLI_PNAC_MAXREAUTH_COUNT:

            i4RetStatus =
                PnacSetPortAuthReAuthMax (CliHandle, u4IfIndex, *(args[0]));
            break;
        case CLI_PNAC_NO_MAXREAUTH_COUNT:

            i4RetStatus =
                PnacSetPortAuthReAuthMax (CliHandle, u4IfIndex, PNAC_REAUTHMAX);
            break;

    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_PNAC_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", PnacCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);

    PNAC_UNLOCK ();

    return i4RetStatus;
}

/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : PnacSetSystemShut                                 */
/*                                                                          */
/*     DESCRIPTION      : This function starts or shuts the PNAC system     */
/*                                                                          */
/*     INPUT            : i4PnacStatus - PNAC_START/PNAC_SHUTDOWN           */
/*                        CliHandle  - CLI Handler                          */
/*                                                                          */
/*     OUTPUT           : NONE                                              */
/*                                                                          */
/*     RETURNS          : Success/Failure                                   */
/*                                                                          */
/****************************************************************************/
INT4
PnacSetSystemShut (tCliHandle CliHandle, INT4 i4PnacStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsPnacSystemControl (&u4ErrCode, i4PnacStatus) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsPnacSystemControl (i4PnacStatus) != SNMP_SUCCESS)
        /* if i4PnacStatus == PNAC_START,
         * This starts as well as enables PNAC */
        /* if i4PnacStatus == PNAC_SHUT,
         * This shutsdown as well as disables PNAC */
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacSetSystemMode                                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Pnac Mode                   */
/*                                                                           */
/*     INPUT            : i4PnacMode - PNAC_CENTRALIZED/PNAC_DISTRIBUTED   */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSetSystemMode (tCliHandle CliHandle, INT4 i4PnacMode)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsDPnacSystemStatus (&u4ErrCode, i4PnacMode) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsDPnacSystemStatus (i4PnacMode) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DPnacSetPeriodicSyncTime                           */
/*                                                                           */
/*     DESCRIPTION      : This function sets the DPnac Periodic sync timer   */
/*                                                                           */
/*     INPUT            : u4DPnacPeriodicTimer - Periodic Sync timer              */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
DPnacSetPeriodicSyncTime (tCliHandle CliHandle, UINT4 u4DPnacPeriodicSyncTime)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsDPnacPeriodicSyncTime (&u4ErrCode, u4DPnacPeriodicSyncTime)
        != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsDPnacPeriodicSyncTime (u4DPnacPeriodicSyncTime) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DPnacSetMaxAliveCount                              */
/*                                                                           */
/*     DESCRIPTION      : This function sets the DPnac Maximum               */
/*                        Keep alive count                                   */
/*                                                                           */
/*     INPUT            : i4DPnacMaxAliveCount - Maximum keep alive count    */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
DPnacSetMaxAliveCount (tCliHandle CliHandle, INT4 i4DPnacMaxAliveCount)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsDPnacMaxKeepAliveCount (&u4ErrCode, i4DPnacMaxAliveCount)
        != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsDPnacMaxKeepAliveCount (i4DPnacMaxAliveCount) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacSetSystemControl                               */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Pnac System Control         */
/*                                                                           */
/*     INPUT            : i4PnacStatus - PNAC_START/PNAC_SHUTDOWN            */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSetSystemControl (tCliHandle CliHandle, INT4 i4PnacStatus)
{
    UINT4               u4ErrCode = 0;

    /* o When dot1x system auth control is called
     *    just call FsPnacSystemControl enable, which intern 
     *    makes system startup and enables pnac.
     * o When  no dot1x system auth control is called
     *   just disable Pnac by calling Dot1xPaeSystemAuthControl.
     */
    if (i4PnacStatus == PNAC_ENABLED)
    {
        if (nmhTestv2FsPnacSystemControl (&u4ErrCode, PNAC_START) !=
            SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetFsPnacSystemControl (PNAC_START) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else
    {
        if (PNAC_IS_SHUTDOWN ())
        {
            return (CLI_SUCCESS);
        }
    }

    if (nmhTestv2Dot1xPaeSystemAuthControl (&u4ErrCode, i4PnacStatus)
        != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetDot1xPaeSystemAuthControl (i4PnacStatus) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacSetAuthServer                                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Pnac AuthServer Location    */
/*                                                                           */
/*     INPUT            : i4AuthMethod - PNAC_LOCAL_AUTH_SERVER/             */
/*                                       PNAC_REMOTE_AUTH_SERVER             */
/*                        i4RemAuthMethod -PNAC_REMOTE_RADIUS_AUTH_SERVER    */
/*                                         PNAC_REMOTE_TACACSPLUS_AUTH_SERVER*/
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSetAuthServer (tCliHandle CliHandle, INT4 i4AuthMethod,
                   INT4 i4RemAuthMethod)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsPnacAuthenticServer (&u4ErrCode, i4AuthMethod) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetFsPnacAuthenticServer (i4AuthMethod) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (i4AuthMethod == PNAC_REMOTE_AUTH_SERVER)
    {
        /* Remote Authentication */
        if (nmhTestv2FsPnacRemoteAuthServerType (&u4ErrCode, i4RemAuthMethod) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhSetFsPnacRemoteAuthServerType (i4RemAuthMethod) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacSetNasId                                       */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Pnac NAS Identifier         */
/*                                                                           */
/*     INPUT            : pu1Nasid - NAS Identifier                          */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSetNasId (tCliHandle CliHandle, UINT1 *pu1Nasid)
{
    INT4                i4FsPnacAuthenticServer;
    UINT4               u4ErrCode = 0;
    tSNMP_OCTET_STRING_TYPE Name;

    Name.i4_Length = (STRLEN (pu1Nasid)) ?
        STRLEN (pu1Nasid) : PNAC_CLI_NAME_SIZE;

    Name.pu1_OctetList = pu1Nasid;

    nmhGetFsPnacAuthenticServer (&i4FsPnacAuthenticServer);

    if (i4FsPnacAuthenticServer == PNAC_LOCAL_AUTH_SERVER)
    {
        CLI_SET_ERR (CLI_PNAC_NASID_CONFIG_NOT_ALLOWED);
        return (CLI_FAILURE);
    }

    if (nmhTestv2FsPnacNasId (&u4ErrCode, &Name) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsPnacNasId (&Name) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacSetReAuthInit                                  */
/*                                                                           */
/*     DESCRIPTION      : This function starts the ReAuthentication Process  */
/*                                                                           */
/*     INPUT            : u4IfIndex - Interface Index                        */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSetReAuthInit (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4NextIfIndex = 0;
    UINT1               u1ForAllInterface = PNAC_FALSE;

    /* Initiate ReAuthentication process for the given input port */

    if (u4IfIndex)
    {
        u1ForAllInterface = PNAC_TRUE;
        u4NextIfIndex = u4IfIndex;
    }
    /* Initiate ReAuthentication process for all valid ports */
    else
    {
        if (nmhGetFirstIndexDot1xPaePortTable ((INT4 *) &u4NextIfIndex) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    do
    {
        if (nmhTestv2Dot1xPaePortReauthenticate (&u4ErrCode,
                                                 u4NextIfIndex,
                                                 PNAC_MIB_TRUE) != SNMP_SUCCESS)
        {
            CLI_SET_ERR (CLI_PNAC_REAUTH_FAIL);
            return (CLI_FAILURE);
        }

        if (nmhSetDot1xPaePortReauthenticate (u4NextIfIndex,
                                              PNAC_MIB_TRUE) != SNMP_SUCCESS)
        {
            CLI_SET_ERR (CLI_PNAC_REAUTH_FAIL);
            return (CLI_FAILURE);
        }

        u4IfIndex = u4NextIfIndex;

    }
    while ((nmhGetNextIndexDot1xPaePortTable ((INT4) u4IfIndex,
                                              (INT4 *) &u4NextIfIndex)
            != SNMP_FAILURE) && (u1ForAllInterface == PNAC_FALSE));

    UNUSED_PARAM (CliHandle);
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacSetPortReAuth                                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Pnac ReAuthentication State */
/*                                                                           */
/*     INPUT            : u4IfIndex - Interface Index                        */
/*                        u4ReAuthStatus - ENABLE / DISABLE                  */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSetPortReAuth (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4ReAuthStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2Dot1xAuthReAuthEnabled (&u4ErrCode, u4IfIndex,
                                         u4ReAuthStatus) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetDot1xAuthReAuthEnabled (u4IfIndex, u4ReAuthStatus)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacSetTimers                                      */
/*                                                                           */
/*     DESCRIPTION      : This function sets the various Pnac Timers         */
/*                                                                           */
/*     INPUT            : u4IfIndex - Interface Index                        */
/*                        u4TmrType - PNAC_CLI_QUIET_PERIOD /                */
/*                                    PNAC_CLI_REAUTH_PERIOD /               */
/*                                    PNAC_CLI_SERVER_TIMEOUT /              */
/*                                    PNAC_CLI_SUPP_TIMEOUT /                */
/*                                    PNAC_CLI_TX_PERIOD                     */
/*                                    PNAC_CLI_START_PERIOD                  */
/*                                    PNAC_CLI_HELD_PERIOD                   */
/*                                    PNAC_CLI_AUTH_PERIOD                   */
/*                        u4TmrValue - Timer Value                           */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSetTimers (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4TmrType,
               UINT4 u4TmrValue)
{
    UINT4               u4ErrCode = 0;

    switch (u4TmrType)
    {
        case PNAC_CLI_QUIET_PERIOD:

            if (nmhTestv2Dot1xAuthQuietPeriod (&u4ErrCode, u4IfIndex,
                                               u4TmrValue) == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (nmhSetDot1xAuthQuietPeriod (u4IfIndex, u4TmrValue) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
            break;

        case PNAC_CLI_REAUTH_PERIOD:

            if (nmhTestv2Dot1xAuthReAuthPeriod (&u4ErrCode, u4IfIndex,
                                                u4TmrValue) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

            if (nmhSetDot1xAuthReAuthPeriod (u4IfIndex, u4TmrValue)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
            break;

        case PNAC_CLI_SERVER_TIMEOUT:

            if (nmhTestv2Dot1xAuthServerTimeout (&u4ErrCode, u4IfIndex,
                                                 u4TmrValue) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

            if (nmhSetDot1xAuthServerTimeout (u4IfIndex, u4TmrValue)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
            break;

        case PNAC_CLI_SUPP_TIMEOUT:

            if (nmhTestv2Dot1xAuthSuppTimeout (&u4ErrCode, u4IfIndex,
                                               u4TmrValue) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

            if (nmhSetDot1xAuthSuppTimeout (u4IfIndex, u4TmrValue)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
            break;

        case PNAC_CLI_TX_PERIOD:

            if (nmhTestv2Dot1xAuthTxPeriod (&u4ErrCode, u4IfIndex,
                                            u4TmrValue) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

            if (nmhSetDot1xAuthTxPeriod (u4IfIndex, u4TmrValue) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
            break;
        case PNAC_CLI_START_PERIOD:

            if (nmhTestv2Dot1xSuppStartPeriod (&u4ErrCode, u4IfIndex,
                                               u4TmrValue) == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (nmhSetDot1xSuppStartPeriod (u4IfIndex, u4TmrValue) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
            break;
        case PNAC_CLI_HELD_PERIOD:

            if (nmhTestv2Dot1xSuppHeldPeriod (&u4ErrCode, u4IfIndex,
                                              u4TmrValue) == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (nmhSetDot1xSuppHeldPeriod (u4IfIndex, u4TmrValue) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
            break;
        case PNAC_CLI_AUTH_PERIOD:

            if (nmhTestv2Dot1xSuppAuthPeriod (&u4ErrCode, u4IfIndex,
                                              u4TmrValue) == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (nmhSetDot1xSuppAuthPeriod (u4IfIndex, u4TmrValue) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
            break;
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacSetNoTimers                                    */
/*                                                                           */
/*     DESCRIPTION      : This function sets the default values for Timers   */
/*                                                                           */
/*     INPUT            : u4IfIndex - Interface Index                        */
/*                        u4TmrType - PNAC_QUIET_PERIOD /                    */
/*                                    PNAC_REAUTH_PERIOD /                   */
/*                                    PNAC_SERVER_TIMEOUT /                  */
/*                                    PNAC_SUPP_TIMEOUT /                    */
/*                                    PNAC_TX_PERIOD                         */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSetNoTimers (tCliHandle CliHandle, UINT4 i4Interface, INT4 i4TmrType)
{

    switch (i4TmrType)
    {
        case PNAC_CLI_QUIET_PERIOD:
            PnacSetTimers (CliHandle, i4Interface, i4TmrType, PNAC_QUIETPERIOD);
            break;

        case PNAC_CLI_REAUTH_PERIOD:
            PnacSetTimers (CliHandle, i4Interface, i4TmrType,
                           PNAC_REAUTHPERIOD);
            break;

        case PNAC_CLI_SERVER_TIMEOUT:
            PnacSetTimers (CliHandle, i4Interface, i4TmrType,
                           PNAC_SERVERTIMEOUT);
            break;

        case PNAC_CLI_SUPP_TIMEOUT:
            PnacSetTimers (CliHandle, i4Interface, i4TmrType, PNAC_SUPPTIMEOUT);
            break;

        case PNAC_CLI_TX_PERIOD:
            PnacSetTimers (CliHandle, i4Interface, i4TmrType, PNAC_TXPERIOD);
            break;
        case PNAC_CLI_START_PERIOD:
            PnacSetTimers (CliHandle, i4Interface, i4TmrType, PNAC_STARTPERIOD);
            break;
        case PNAC_CLI_HELD_PERIOD:
            PnacSetTimers (CliHandle, i4Interface, i4TmrType, PNAC_HELDPERIOD);
            break;
        case PNAC_CLI_AUTH_PERIOD:
            PnacSetTimers (CliHandle, i4Interface, i4TmrType, PNAC_AUTHPERIOD);
            break;
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacSetASUserInfo                                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Information for Local       */
/*                        Authentication Server.                             */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        pu1PortList - Ports Index                          */
/*                        pu1UserName - User Name                            */
/*                        pu1Password - Password                             */
/*                        u4Permission - ALLOW / DENY                        */
/*                        u4AuthTimeout - Authentication Timeout             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSetASUserInfo (tCliHandle CliHandle, UINT1 *pu1PortList,
                   UINT1 *pu1UserName, UINT1 *pu1Password,
                   UINT4 u4Permission, UINT4 u4AuthTimeout)
{
    UINT4               u4ErrCode = 0;
    UINT1               au1TmpPortList[PNAC_PORTLIST_LEN];
    tSNMP_OCTET_STRING_TYPE Name;
    tSNMP_OCTET_STRING_TYPE Password;
    tSNMP_OCTET_STRING_TYPE PortList;
    INT4                i4RowStatus;
    UINT4               u4RetStatus;    /* Use this to delete the entry created
                                         * here
                                         */

    PNAC_MEMSET (au1TmpPortList, 0, PNAC_PORTLIST_LEN);
    if ((pu1UserName == NULL) || (pu1Password == NULL))
    {
        return (CLI_FAILURE);
    }

    Name.i4_Length = STRLEN (pu1UserName);
    Password.i4_Length = STRLEN (pu1Password);
    PortList.i4_Length = PNAC_PORTLIST_LEN;

    if (Password.i4_Length > PNAC_MAX_LEN_PASSWD)
    {
        return (CLI_FAILURE);
    }

    Name.pu1_OctetList = pu1UserName;
    Password.pu1_OctetList = pu1Password;
    PortList.pu1_OctetList = (UINT1 *) &au1TmpPortList;

    if (pu1PortList != NULL)
    {
        MEMCPY (PortList.pu1_OctetList, pu1PortList, PortList.i4_Length);
    }

    /* Check if the user name already exists in the database. If the Get 
     * function returns Success, for CREATE and WAIT then a user already exists.
     * Otherwise we need to create an entry.
     */

    if ((u4RetStatus = nmhGetFsPnacASUserConfigRowStatus (&Name, &i4RowStatus))
        == SNMP_FAILURE)
    {
        /* No Entry exists with this name */

        if (nmhTestv2FsPnacASUserConfigRowStatus (&u4ErrCode, &Name,
                                                  PNAC_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_PNAC_MAX_USERS_REACHED);
            return (CLI_FAILURE);
        }

        if (nmhSetFsPnacASUserConfigRowStatus (&Name, PNAC_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }
    else
    {
        /* Entry already exists with this name, so make it to not-in-service 
         * and proceed further.
         */
        if (nmhTestv2FsPnacASUserConfigRowStatus (&u4ErrCode, &Name,
                                                  PNAC_NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetFsPnacASUserConfigRowStatus (&Name,
                                               PNAC_NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (nmhTestv2FsPnacASUserConfigPassword (&u4ErrCode, &Name, &Password)
        == SNMP_FAILURE)
    {
        if (u4RetStatus == SNMP_FAILURE)
        {
            nmhSetFsPnacASUserConfigRowStatus (&Name, PNAC_DESTROY);
        }
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2FsPnacASUserConfigPermission (&u4ErrCode,
                                               &Name,
                                               u4Permission) == SNMP_FAILURE)
    {
        if (u4RetStatus == SNMP_FAILURE)
        {
            nmhSetFsPnacASUserConfigRowStatus (&Name, PNAC_DESTROY);
        }
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (pu1PortList != NULL)
    {
        if (nmhTestv2FsPnacASUserConfigPortList (&u4ErrCode,
                                                 &Name,
                                                 &PortList) == SNMP_FAILURE)
        {
            if (u4RetStatus == SNMP_FAILURE)
            {
                nmhSetFsPnacASUserConfigRowStatus (&Name, PNAC_DESTROY);
            }
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (u4AuthTimeout)
    {
        if (nmhTestv2FsPnacASUserConfigAuthTimeout (&u4ErrCode,
                                                    &Name,
                                                    u4AuthTimeout)
            == SNMP_FAILURE)
        {
            if (u4RetStatus == SNMP_FAILURE)
            {
                nmhSetFsPnacASUserConfigRowStatus (&Name, PNAC_DESTROY);
            }
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (nmhSetFsPnacASUserConfigPassword (&Name, &Password) == SNMP_FAILURE)
    {
        if (u4RetStatus == SNMP_FAILURE)
        {
            nmhSetFsPnacASUserConfigRowStatus (&Name, PNAC_DESTROY);
        }
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetFsPnacASUserConfigPermission (&Name,
                                            u4Permission) == SNMP_FAILURE)
    {
        if (u4RetStatus == SNMP_FAILURE)
        {
            nmhSetFsPnacASUserConfigRowStatus (&Name, PNAC_DESTROY);
        }
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (pu1PortList != NULL)
    {
        if (nmhSetFsPnacASUserConfigPortList (&Name, &PortList) == SNMP_FAILURE)
        {
            if (u4RetStatus == SNMP_FAILURE)
            {
                nmhSetFsPnacASUserConfigRowStatus (&Name, PNAC_DESTROY);
            }
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (u4AuthTimeout)
    {
        if (nmhSetFsPnacASUserConfigAuthTimeout (&Name,
                                                 u4AuthTimeout) == SNMP_FAILURE)
        {
            if (u4RetStatus == SNMP_FAILURE)
            {
                nmhSetFsPnacASUserConfigRowStatus (&Name, PNAC_DESTROY);
            }
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (nmhTestv2FsPnacASUserConfigRowStatus (&u4ErrCode, &Name,
                                              PNAC_ACTIVE) == SNMP_FAILURE)
    {

        if (u4RetStatus == SNMP_FAILURE)
        {
            nmhSetFsPnacASUserConfigRowStatus (&Name, PNAC_DESTROY);
        }
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetFsPnacASUserConfigRowStatus (&Name, PNAC_ACTIVE) == SNMP_FAILURE)
    {
        if (u4RetStatus == SNMP_FAILURE)
        {
            nmhSetFsPnacASUserConfigRowStatus (&Name, PNAC_DESTROY);
        }
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacSetNoASUserInfo                                */
/*                                                                           */
/*     DESCRIPTION      : This function deletes the Information from Local   */
/*                        Authenication Server.                              */
/*                                                                           */
/*     INPUT            : pu1UserName - User Name                            */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSetNoASUserInfo (tCliHandle CliHandle, UINT1 *pu1UserName)
{
    UINT4               u4ErrCode = 0;
    tSNMP_OCTET_STRING_TYPE Name;

    Name.i4_Length = STRLEN (pu1UserName);

    Name.pu1_OctetList = pu1UserName;

    if (nmhTestv2FsPnacASUserConfigRowStatus (&u4ErrCode, &Name, PNAC_DESTROY)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_PNAC_USER_NOT_EXISTS);
        return (CLI_FAILURE);
    }

    if (nmhSetFsPnacASUserConfigRowStatus (&Name, PNAC_DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacSetMaxReq                                      */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Maximum Request Period      */
/*                                                                           */
/*     INPUT            : u4IfIndex - Interface Inde                         */
/*                        u4MaxReq  - Maximum Number of Requests             */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSetMaxReq (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4MaxReq)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2Dot1xAuthMaxReq (&u4ErrCode, u4IfIndex, u4MaxReq)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetDot1xAuthMaxReq (u4IfIndex, u4MaxReq) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacSetMaxStart                                    */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Maximum Start Count         */
/*                                                                           */
/*     INPUT            : u4IfIndex - Interface Inde                         */
/*                        u4MaxStart  - Maximum Number of Requests           */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSetMaxStart (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4MaxStart)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2Dot1xSuppMaxStart (&u4ErrCode, u4IfIndex, u4MaxStart)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetDot1xSuppMaxStart (u4IfIndex, u4MaxStart) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacSetDefaults                                    */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Defaults for Pnac           */
/*                        Scalar MIB objects                                 */
/*                                                                           */
/*     INPUT            : u4IfIndex - Interface Inde                         */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSetDefaults (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsPnacPaePortAuthMode (&u4ErrCode, u4IfIndex,
                                        PNAC_PORT_AUTHMODE_PORTBASED)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsPnacPaePortAuthMode (u4IfIndex, PNAC_PORT_AUTHMODE_PORTBASED)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2Dot1xAuthReAuthEnabled (&u4ErrCode, u4IfIndex,
                                         PNAC_MIB_FALSE) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetDot1xAuthReAuthEnabled (u4IfIndex, PNAC_MIB_FALSE)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2Dot1xAuthAuthControlledPortControl (&u4ErrCode,
                                                     u4IfIndex,
                                                     PNAC_PORTCNTRL_FORCEAUTHORIZED)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetDot1xAuthAuthControlledPortControl (u4IfIndex,
                                                  PNAC_PORTCNTRL_FORCEAUTHORIZED)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (nmhTestv2Dot1xAuthAdminControlledDirections (&u4ErrCode, u4IfIndex,
                                                     PNAC_CNTRLD_DIR_BOTH) !=
        SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetDot1xAuthAdminControlledDirections
        (u4IfIndex, PNAC_CNTRLD_DIR_BOTH) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2Dot1xAuthReAuthPeriod (&u4ErrCode, u4IfIndex,
                                        PNAC_REAUTHPERIOD) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetDot1xAuthReAuthPeriod (u4IfIndex, PNAC_REAUTHPERIOD)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2Dot1xAuthQuietPeriod (&u4ErrCode, u4IfIndex,
                                       PNAC_QUIETPERIOD) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetDot1xAuthQuietPeriod (u4IfIndex, PNAC_QUIETPERIOD)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2Dot1xAuthTxPeriod (&u4ErrCode, u4IfIndex,
                                    PNAC_TXPERIOD) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetDot1xAuthTxPeriod (u4IfIndex, PNAC_TXPERIOD) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2Dot1xAuthServerTimeout (&u4ErrCode, u4IfIndex,
                                         PNAC_SERVERTIMEOUT) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetDot1xAuthServerTimeout (u4IfIndex, PNAC_SERVERTIMEOUT)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2Dot1xAuthSuppTimeout (&u4ErrCode, u4IfIndex,
                                       PNAC_SUPPTIMEOUT) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetDot1xAuthSuppTimeout (u4IfIndex, PNAC_SUPPTIMEOUT)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (nmhTestv2Dot1xSuppStartPeriod (&u4ErrCode, u4IfIndex,
                                       PNAC_STARTPERIOD) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetDot1xSuppStartPeriod (u4IfIndex, PNAC_STARTPERIOD) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (nmhTestv2Dot1xSuppHeldPeriod (&u4ErrCode, u4IfIndex,
                                      PNAC_HELDPERIOD) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetDot1xSuppHeldPeriod (u4IfIndex, PNAC_HELDPERIOD) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (nmhTestv2Dot1xSuppAuthPeriod (&u4ErrCode, u4IfIndex,
                                      PNAC_AUTHPERIOD) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetDot1xSuppAuthPeriod (u4IfIndex, PNAC_AUTHPERIOD) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2Dot1xAuthMaxReq (&u4ErrCode, u4IfIndex,
                                  PNAC_MAXREQ) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetDot1xAuthMaxReq (u4IfIndex, PNAC_MAXREQ) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2Dot1xSuppMaxStart (&u4ErrCode, u4IfIndex,
                                    PNAC_MAXSTART) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetDot1xSuppMaxStart (u4IfIndex, PNAC_MAXSTART) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2FsPnacPaeAuthReAuthMax (&u4ErrCode,
                                         (INT4) u4IfIndex,
                                         PNAC_REAUTHMAX) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetFsPnacPaeAuthReAuthMax ((INT4) u4IfIndex, PNAC_REAUTHMAX) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2Dot1xSuppAccessCtrlWithAuth (&u4ErrCode, u4IfIndex,
                                              PNAC_SUPP_ACCESS_CTRL_INACTIVE)
        != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetDot1xSuppAccessCtrlWithAuth
        (u4IfIndex, PNAC_SUPP_ACCESS_CTRL_INACTIVE) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2FsPnacPaePortAuthStatus (&u4ErrCode, (INT4) u4IfIndex,
                                          PNAC_ENABLED) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetFsPnacPaePortAuthStatus (u4IfIndex, PNAC_ENABLED) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    CliPrintf (CliHandle, "\rSetting the Default Configuration for Dot1x on "
               "this interface\r\n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacSetDebug                                       */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Pnac Debug options          */
/*                                                                           */
/*     INPUT            : u4Level -  Debug Level                             */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSetDebug (tCliHandle CliHandle, UINT4 u4Level, UINT1 u1DebugSet)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4TmpLevel = 0;
    UINT4               u4OldLevel = 0;

    switch (u4Level)
    {

        case PNAC_CLI_DEBUG_ALL:
            u4TmpLevel = PNAC_ALL_FAILURE_TRC | PNAC_INIT_SHUT_TRC |
                PNAC_MGMT_TRC | PNAC_CONTROL_PATH_TRC | PNAC_DUMP_TRC |
                PNAC_RED_TRC;
            break;

        case PNAC_CLI_DEBUG_ERRORS:
            u4TmpLevel = PNAC_ALL_FAILURE_TRC;
            break;
        case PNAC_CLI_DEBUG_INIT_SHUT:
            u4TmpLevel = PNAC_INIT_SHUT_TRC;
            break;

        case PNAC_CLI_DEBUG_MGMT:
            u4TmpLevel = PNAC_MGMT_TRC;
            break;

        case PNAC_CLI_DEBUG_PACKETS:
            u4TmpLevel = PNAC_DUMP_TRC;
            break;

        case PNAC_CLI_DEBUG_STATEMC:
            u4TmpLevel = PNAC_CONTROL_PATH_TRC;
            break;

        case PNAC_CLI_DEBUG_RED:
            u4TmpLevel = PNAC_RED_TRC;
            break;

    }

    nmhGetFsPnacTraceOption ((INT4 *) &u4OldLevel);

    if (u1DebugSet == PNAC_TRUE)
    {
        u4TmpLevel = u4OldLevel | u4TmpLevel;
    }

    if (u1DebugSet == PNAC_FALSE)
    {
        u4TmpLevel = u4OldLevel & (~u4TmpLevel);
    }

    if (nmhTestv2FsPnacTraceOption (&u4ErrCode, u4TmpLevel) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (nmhSetFsPnacTraceOption (u4TmpLevel) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacSetPortControl                                 */
/*                                                                           */
/*     DESCRIPTION      : This function sets the authenticator port control  */
/*                                                                           */
/*     INPUT            : i4PortControl - Authenticator port control         */
/*                             AUTO/FORCE-AUTHORIZED/FORCE-UNAUTHORIZED      */
/*                        CliHandle     - CLI Handler                        */
/*                        u4IfIndex     - Interface index                    */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSetPortControl (tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4PortControl)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2Dot1xAuthAuthControlledPortControl (&u4ErrCode, u4IfIndex,
                                                     i4PortControl) !=
        SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetDot1xAuthAuthControlledPortControl (u4IfIndex, i4PortControl)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacSetSuppAccessCtrlWithAuth                      */
/*                                                                           */
/*     DESCRIPTION      : This function sets the supplicant access control   */
/*                        with Auth parameter                                */
/*                                                                           */
/*     INPUT            : CliHandle     - CLI Handler                        */
/*                        u4IfIndex     - Interface index                    */
/*                        u2SuppAccessCtrlWithAuth - ACTIVE/INACTIVE         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
PnacSetSuppAccessCtrlWithAuth (tCliHandle CliHandle, UINT4 u4IfIndex,
                               INT4 i4SuppAccessCtrlWithAuth)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2Dot1xSuppAccessCtrlWithAuth (&u4ErrCode, u4IfIndex,
                                              i4SuppAccessCtrlWithAuth)
        != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetDot1xSuppAccessCtrlWithAuth (u4IfIndex, i4SuppAccessCtrlWithAuth)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacSetPortControlDirection                        */
/*                                                                           */
/*     DESCRIPTION      : This function sets the port Control Direction      */
/*                                                                           */
/*     INPUT            : i4PortControlDir - Port control Direction          */
/*                                           IN/BOTH                         */
/*                        CliHandle     - CLI Handler                        */
/*                        u4IfIndex     - Interface index                    */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSetPortControlDirection (tCliHandle CliHandle, UINT4 u4IfIndex,
                             INT4 i4PortControlDir)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2Dot1xAuthAdminControlledDirections (&u4ErrCode, u4IfIndex,
                                                     i4PortControlDir) !=
        SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetDot1xAuthAdminControlledDirections (u4IfIndex, i4PortControlDir)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacSetPortAuthMode                                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the port authentication mode    */
/*                                                                           */
/*     INPUT            : i4PortAuthMode - Port authentication mode          */
/*                                PORTBASED/MACBASED                         */
/*                        CliHandle     - CLI Handler                        */
/*                        u4IfIndex     - Interface index                    */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSetPortAuthMode (tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4PortAuthMode)
{
    UINT4               u4ErrCode = 0;
    if (nmhTestv2FsPnacPaePortAuthMode (&u4ErrCode, (INT4) u4IfIndex,
                                        i4PortAuthMode) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsPnacPaePortAuthMode (u4IfIndex, i4PortAuthMode) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacSessionInitialise                              */
/*                                                                           */
/*     DESCRIPTION      : This function sets the port authentication mode    */
/*                                                                           */
/*     INPUT            : CliHandle     - CLI Handler                        */
/*                        pu1MacAddr    - Mac address of Supplicant          */
/*                        u1Status      - Flag to set TRUE/FALSE             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSessionInitialise (tCliHandle CliHandle, UINT1 *pu1MacAddr, UINT1 u1Status)
{
    UINT4               u4ErrCode = 0;
    if (nmhTestv2FsPnacAuthSessionInitialize (&u4ErrCode, pu1MacAddr,
                                              u1Status) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsPnacAuthSessionInitialize (pu1MacAddr, u1Status)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacSessionReauth                                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the port authentication mode    */
/*                                                                           */
/*     INPUT            : CliHandle     - CLI Handler                        */
/*                        pu1MacAddr    - Mac address of Supplicant          */
/*                        u1Status      - Flag to set TRUE/FALSE             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSessionReauth (tCliHandle CliHandle, UINT1 *pu1MacAddr, UINT1 u1Status)
{
    UINT4               u4ErrCode = 0;
    if (nmhTestv2FsPnacAuthSessionReauthenticate (&u4ErrCode, pu1MacAddr,
                                                  u1Status) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsPnacAuthSessionReauthenticate (pu1MacAddr, u1Status)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : DPnacShowSlotDetails                               */
/*                                                                           */
/*     DESCRIPTION      : This function shows the ports auth status and      */
/*                        control direction information for slots.           */
/*                                                                           */
/*     INPUT            : u4SlotId -  Slot Identifier                        */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
DPnacShowSlotDetails (tCliHandle CliHandle, UINT4 u4SlotId)
{
    tPnacDPnacConsPortEntry *pConsPortEntry = NULL;
    tPnacDPnacSlotInfo *pDPnacSlotInfo = NULL;
    UINT4               u4NextSlotId = 0;
    INT4                i4IfIndex = 0;
    INT4                i4PrevIfIndex = 0;
    INT4                i4PortAuthStatus = 0;
    INT4                i4PortCtrlDirection = 0;
    UINT1               ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1Flag = 0;

    if (gPnacSystemInfo.u1DPnacRolePlayed == PNAC_DPNAC_SYSTEM_ROLE_SLAVE)
    {
        CliPrintf (CliHandle,
                   "\r\n %%Consolidated information is available in Master D-PNAC Node\r\n ");
        return CLI_SUCCESS;
    }

    if (u4SlotId == MAX_DPNAC_SLOT_ALL)
    {
        u1Flag = 0;
        if (nmhGetFirstIndexFsDPnacSlotPortTable
            ((INT4 *) &u4NextSlotId, (INT4 *) &i4IfIndex) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_DPNAC_SLOT_INFO_NOT_EXISTS);
            return CLI_FAILURE;
        }
    }
    else
    {
        u1Flag = 1;
        DPnacGetSlotInfo (&pDPnacSlotInfo, u4SlotId);

        if (pDPnacSlotInfo == NULL)
        {
            CLI_SET_ERR (CLI_DPNAC_SLOT_INFO_NOT_EXISTS);
            return CLI_FAILURE;
        }
    }
    do
    {
        i4PrevIfIndex = 0;

        if (u1Flag == 0)
        {
            u4SlotId = u4NextSlotId;
            DPnacGetSlotInfo (&pDPnacSlotInfo, u4SlotId);
            if (pDPnacSlotInfo == NULL)
            {
                return CLI_FAILURE;
            }
        }

        CliPrintf (CliHandle,
                   "\r\nDPNAC Authentication Information: Slot %d\r\n",
                   u4SlotId);

        CliPrintf (CliHandle,
                   "\r---------------------------------------------------- \r\n ");
        CliPrintf (CliHandle,
                   "\r           Port         Authentication   Control\r\n ");
        CliPrintf (CliHandle,
                   "\rPort       Property     Status           Direction        \r\n ");
        CliPrintf (CliHandle,
                   "\r--------   ---------    ---------------  ------------");

        while (nmhGetNextIndexFsDPnacSlotPortTable
               ((INT4) u4SlotId, (INT4 *) &u4NextSlotId,
                i4PrevIfIndex, &i4IfIndex) == SNMP_SUCCESS)
        {
            if (u4SlotId != u4NextSlotId)
            {
                break;
            }
            PNAC_MEMSET (ai1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
            PnacCfaCliGetIfName ((UINT4) i4IfIndex, (INT1 *) ai1IfName);
            nmhGetFsDPnacPortAuthStatus ((INT4) u4SlotId, i4IfIndex,
                                         &i4PortAuthStatus);
            nmhGetFsDPnacPortControlledDirection ((INT4) u4SlotId, i4IfIndex,
                                                  &i4PortCtrlDirection);

            if ((DPnacGetPortEntry
                 (&pConsPortEntry, (UINT2) i4IfIndex,
                  u4SlotId) != PNAC_SUCCESS))
            {
                return CLI_FAILURE;
            }

            if (i4PortAuthStatus == PNAC_PORTSTATUS_AUTHORIZED)
            {

                if (pConsPortEntry->u1LocalOrRemote == PNAC_DPNAC_LOCAL_PORT)
                {
                    CliPrintf (CliHandle, "\r\n%-11s%-13s%-17s", ai1IfName,
                               "Local", "Authorized");
                }
                else
                {
                    CliPrintf (CliHandle, "\r\n%-11s%-13s%-17s", ai1IfName,
                               "Remote", "Authorized");
                }

            }
            else
            {
                if (pConsPortEntry->u1LocalOrRemote == PNAC_DPNAC_LOCAL_PORT)
                {

                    CliPrintf (CliHandle, "\r\n%-11s%-13s%-17s", ai1IfName,
                               "Local", "Un-Authorized");
                }
                else
                {
                    CliPrintf (CliHandle, "\r\n%-11s%-13s%-17s", ai1IfName,
                               "Remote", "Un-Authorized");
                }

            }

            if (i4PortCtrlDirection == PNAC_CDSM_STATE_IN_OR_BOTH)
            {
                CliPrintf (CliHandle, "%s", "IN");

            }
            else
            {
                CliPrintf (CliHandle, "%s", "BOTH");

            }

            i4PrevIfIndex = i4IfIndex;
            pConsPortEntry = NULL;
        }

        CliPrintf (CliHandle, "\r\n");

    }
    while ((nmhGetNextIndexFsDPnacStatsTable
            ((INT4) u4SlotId, (INT4 *) &u4NextSlotId) == SNMP_SUCCESS)
           && (u1Flag == 0));

    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DPnacShowSlotStats                                 */
/*                                                                           */
/*     DESCRIPTION      : This function shows the frame statistics for       */
/*                        a Slot                                             */
/*                                                                           */
/*     INPUT            : u4SlotId - Slot Identifier                         */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
DPnacShowSlotStats (tCliHandle CliHandle, UINT4 u4SlotId)
{
    tPnacDPnacSlotInfo *pDPnacSlotInfo = NULL;
    UINT4               u4EventUpdateFramesRx = 0;
    UINT4               u4EventUpdateFramesTx = 0;
    UINT4               u4PeriodicFramesRx = 0;
    UINT4               u4PeriodicFramesTx = 0;
    UINT4               u4PrevSlotId = 0;
    UINT4               u4MasterSlotId = 0;
    UINT1               u1Flag = 0;

    DPnacGetMasterSlotId (&u4MasterSlotId);
    if (u4SlotId == MAX_DPNAC_SLOT_ALL)
    {
        u1Flag = 0;
        if (nmhGetFirstIndexFsDPnacStatsTable ((INT4 *) &u4SlotId) ==
            SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_DPNAC_SLOT_INFO_NOT_EXISTS);
            return CLI_FAILURE;
        }
    }
    else
    {
        u1Flag = 1;

        if (u4SlotId == u4MasterSlotId)
        {
            CliPrintf (CliHandle,
                       "\r\n %%Statistics not present for Master Slot\r\n");
            CliPrintf (CliHandle, "\r\n");
            return CLI_FAILURE;
        }

    }
    do
    {
        if (u4SlotId == u4MasterSlotId)
        {
            continue;
        }

        u4PrevSlotId = u4SlotId;
        DPnacGetSlotInfo (&pDPnacSlotInfo, u4SlotId);
        if (pDPnacSlotInfo == NULL)
        {
            CLI_SET_ERR (CLI_DPNAC_SLOT_INFO_NOT_EXISTS);
            return CLI_FAILURE;
        }

        if (pDPnacSlotInfo != NULL)
        {
            nmhGetFsDPnacEventUpdateFramesRx ((INT4) pDPnacSlotInfo->u4SlotId,
                                              &u4EventUpdateFramesRx);
            nmhGetFsDPnacEventUpdateFramesTx ((INT4) pDPnacSlotInfo->u4SlotId,
                                              &u4EventUpdateFramesTx);
            nmhGetFsDPnacPeriodicFramesRx ((INT4) pDPnacSlotInfo->u4SlotId,
                                           &u4PeriodicFramesRx);
            nmhGetFsDPnacPeriodicFramesTx ((INT4) pDPnacSlotInfo->u4SlotId,
                                           &u4PeriodicFramesTx);

            CliPrintf (CliHandle, "\r\n DPNAC Statistics Info: Slot %d \r\n",
                       pDPnacSlotInfo->u4SlotId);
            CliPrintf (CliHandle,
                       "\r------------------------------------------------- \r\n ");
            CliPrintf (CliHandle,
                       "\rEvent Update Frames received       : %d \r\n",
                       u4EventUpdateFramesRx);
            CliPrintf (CliHandle,
                       "\rEvent Update Frames transmitted    : %d \r\n",
                       u4EventUpdateFramesTx);
            CliPrintf (CliHandle,
                       "\rPeriodic Sync Frames received      : %d \r\n",
                       u4PeriodicFramesRx);
            CliPrintf (CliHandle,
                       "\rPeriodic Sync Frames transmitted   : %d \r\n",
                       u4PeriodicFramesTx);
            CliPrintf (CliHandle,
                       "\rCurrent Keep Alive Count           : %d \r\n",
                       pDPnacSlotInfo->u4KeepAliveCount);
        }

    }
    while ((nmhGetNextIndexFsDPnacStatsTable
            ((INT4) u4PrevSlotId, (INT4 *) &u4SlotId) == SNMP_SUCCESS)
           && (u1Flag == 0));

    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DPnacShowDetail                                    */
/*                                                                           */
/*     DESCRIPTION      : This function shows the DPnac information          */
/*                                                                           */
/*     INPUT            : u4IfIndex -  Interface Index                       */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
DPnacShowDetail (tCliHandle CliHandle)
{

    INT4                i4DPnacSystemStatus = 0;
    INT4                i4DPnacMaxKeepAliveCount = 0;
    UINT4               u4DPnacPeriodicSyncTime = 0;

    nmhGetFsDPnacSystemStatus (&i4DPnacSystemStatus);
    nmhGetFsDPnacPeriodicSyncTime (&u4DPnacPeriodicSyncTime);
    nmhGetFsDPnacMaxKeepAliveCount (&i4DPnacMaxKeepAliveCount);

    CliPrintf (CliHandle, "\r\n DPNAC Detail information \r\n");
    CliPrintf (CliHandle, "--------------------------\r\n");
    if (i4DPnacSystemStatus == PNAC_DISTRIBUTED)
    {
        CliPrintf (CliHandle, "\r\nPNAC Status              : Distributed");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nPNAC Status              : Centralized");
    }

    if (gPnacSystemInfo.u1DPnacRolePlayed == PNAC_DPNAC_SYSTEM_ROLE_MASTER)
    {
        CliPrintf (CliHandle, "\r\nRole-Played              : Master");
    }
    else if (gPnacSystemInfo.u1DPnacRolePlayed == PNAC_DPNAC_SYSTEM_ROLE_SLAVE)
    {
        CliPrintf (CliHandle, "\r\nRole-Played              : Slave");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nRole-Played              : None");
    }

    CliPrintf (CliHandle, "\r\nPeriodic Sync-Timer      : %d Seconds",
               u4DPnacPeriodicSyncTime);
    CliPrintf (CliHandle, "\r\nMaximum Keep Alive Count : %d\r\n",
               i4DPnacMaxKeepAliveCount);
    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacShowInterfaceInfo                              */
/*                                                                           */
/*     DESCRIPTION      : This function shows the Pnac port details          */
/*                                                                           */
/*     INPUT            : u4IfIndex -  Interface Index                       */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacShowInterfaceInfo (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    tCfaIfInfo          IfInfo;
    INT4                i4AuthSMState = 0;
    INT4                i4AuthPaeStatus = 0;
    INT4                i4BendSMState = 0;
    INT4                i4MaxReq = 0;
    INT4                i4MaxStart = 0;
    INT4                i4PortStatus = 0;
    INT4                i4PortControl = 0;
    INT4                i4ReAuth = 0;
    INT4                i4QuietPeriod = 0;
    INT4                i4ReAuthPeriod = 0;
    INT4                i4ServerTimeOut = 0;
    INT4                i4SuppTimeOut = 0;
    INT4                i4TxPeriod = 0;
    INT4                i4StartPeriod = 0;
    INT4                i4HeldPeriod = 0;
    INT4                i4AuthPeriod = 0;
    INT4                i4ReAuthMax = 0;
    UINT1               u1Flag;
    UINT1               u1Quit = CLI_SUCCESS;
    UINT4               u4PrevIndex = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4SuppAccCtrlWithAuth;
    INT4                i4FinalPortStatus = 0;
    INT4                i4SuppSMState = 0;
    INT4                i4SuppPortStatus = 0;
    INT4                i4AdminControlledDirection = 0;
    INT4                i4OperControlledDirection = 0;
    INT4                i4PortAuthMode = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (u4IfIndex == 0)
    {
        u1Flag = 0;
        i1RetVal = nmhGetFirstIndexDot1xPaePortTable ((INT4 *) &u4IfIndex);
        if (i1RetVal == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }
    else
    {
        u1Flag = 1;
    }

    do
    {
        u4PrevIndex = u4IfIndex;

        MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

        if ((PnacCfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS) &&
            (IfInfo.u1BridgedIface == CFA_DISABLED))
        {
            continue;
        }

        MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        PnacCfaCliGetIfName (u4IfIndex, (INT1 *) au1IfName);

        nmhGetDot1xAuthPaeState (u4IfIndex, &i4AuthSMState);
        nmhGetFsPnacPaePortAuthStatus ((INT4) u4IfIndex, &i4AuthPaeStatus);
        nmhGetDot1xAuthBackendAuthState (u4IfIndex, &i4BendSMState);

        nmhGetDot1xAuthAuthControlledPortStatus (u4IfIndex, &i4PortStatus);
        nmhGetDot1xSuppPaeState (u4IfIndex, &i4SuppSMState);
        nmhGetDot1xSuppControlledPortStatus (u4IfIndex, &i4SuppPortStatus);
        nmhGetDot1xAuthAdminControlledDirections (u4IfIndex,
                                                  &i4AdminControlledDirection);
        nmhGetDot1xAuthOperControlledDirections (u4IfIndex,
                                                 &i4OperControlledDirection);
        nmhGetDot1xSuppAccessCtrlWithAuth (u4IfIndex, &i4SuppAccCtrlWithAuth);
        if (i4SuppAccCtrlWithAuth == PNAC_SUPP_ACCESS_CTRL_INACTIVE)
        {
            i4FinalPortStatus = i4PortStatus;
        }
        else
        {
            nmhGetFsPnacPaePortStatus ((INT4) u4IfIndex, &i4FinalPortStatus);
        }
        nmhGetDot1xAuthMaxReq ((INT4) u4IfIndex, (UINT4 *) &i4MaxReq);
        nmhGetDot1xSuppMaxStart ((INT4) u4IfIndex, (UINT4 *) &i4MaxStart);

        nmhGetDot1xAuthAuthControlledPortControl (u4IfIndex, &i4PortControl);
        nmhGetDot1xAuthReAuthEnabled (u4IfIndex, &i4ReAuth);
        nmhGetDot1xAuthQuietPeriod ((INT4) u4IfIndex, (UINT4 *) &i4QuietPeriod);

        nmhGetDot1xAuthReAuthPeriod ((INT4) u4IfIndex,
                                     (UINT4 *) &i4ReAuthPeriod);

        nmhGetDot1xAuthServerTimeout ((INT4) u4IfIndex,
                                      (UINT4 *) &i4ServerTimeOut);
        nmhGetDot1xAuthSuppTimeout ((INT4) u4IfIndex, (UINT4 *) &i4SuppTimeOut);
        nmhGetDot1xAuthTxPeriod ((INT4) u4IfIndex, (UINT4 *) &i4TxPeriod);
        nmhGetDot1xSuppStartPeriod ((INT4) u4IfIndex, (UINT4 *) &i4StartPeriod);
        nmhGetDot1xSuppHeldPeriod ((INT4) u4IfIndex, (UINT4 *) &i4HeldPeriod);
        nmhGetDot1xSuppAuthPeriod ((INT4) u4IfIndex, (UINT4 *) &i4AuthPeriod);
        nmhGetFsPnacPaePortAuthMode ((INT4) u4IfIndex, &i4PortAuthMode);
        nmhGetFsPnacPaeAuthReAuthMax ((INT4) u4IfIndex, (UINT4 *) &i4ReAuthMax);

        /* Do release the Lock here */

        CliPrintf (CliHandle, "\r\nDot1x Info for %-8s\r\n", au1IfName);
        CliPrintf (CliHandle, "\r--------------------- \r\n ");
        if (i4PortAuthMode == PNAC_PORT_AUTHMODE_MACBASED)
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "AuthMode", "MAC-BASED");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "AuthMode", "PORT-BASED");
        }

        if (i4AuthPaeStatus == PNAC_ENABLED)
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "AuthPaeStatus",
                       "ENABLED");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "AuthPaeStatus",
                       "DISABLED");
        }

        if (i4FinalPortStatus == PNAC_PORTSTATUS_UNAUTHORIZED)
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "PortStatus",
                       "UNAUTHORIZED");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "PortStatus",
                       "AUTHORIZED");
        }

        if (i4SuppAccCtrlWithAuth == PNAC_SUPP_ACCESS_CTRL_INACTIVE)
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "AccessControl",
                       "INACTIVE");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "AccessControl", "ACTIVE");
        }
        CliPrintf (CliHandle, "\r\n");
        PnacShowAuthSmState (CliHandle, i4AuthSMState);

        switch (i4SuppSMState)
        {
            case PNAC_SSM_STATE_DISCONNECTED:
                CliPrintf (CliHandle, "\r\n%-20s  = %s", "SuppSM State",
                           "DISCONNECTED");
                break;

            case PNAC_SSM_STATE_CONNECTING:
                CliPrintf (CliHandle, "\r\n%-20s  = %s", "SuppSM State",
                           "CONNECTING");
                break;

            case PNAC_SSM_STATE_AUTHENTICATING:
                CliPrintf (CliHandle, "\r\n%-20s  = %s", "SuppSM State",
                           "AUTHENTICATING");
                break;

            case PNAC_SSM_STATE_AUTHENTICATED:
                CliPrintf (CliHandle, "\r\n%-20s  = %s", "SuppSM State",
                           "AUTHENTICATED");
                break;

            case PNAC_SSM_STATE_ACQUIRED:
                CliPrintf (CliHandle, "\r\n%-20s  = %s", "SuppSM State",
                           "ACQUIRED");
                break;
            case PNAC_SSM_STATE_LOGOFF:
                CliPrintf (CliHandle, "\r\n%-20s  = %s", "SuppSM State",
                           "LOGOFF");
                break;

            case PNAC_SSM_STATE_HELD:
                CliPrintf (CliHandle, "\r\n%-20s  = %s", "SuppSM State",
                           "HELD");
                break;

            case PNAC_SSM_STATE_FORCEAUTH:
                CliPrintf (CliHandle, "\r\n%-20s  = %s", "SuppSM State",
                           "FORCE AUTHORIZED");
                break;

            case PNAC_SSM_STATE_FORCEUNAUTH:
                CliPrintf (CliHandle, "\r\n%-20s  = %s", "SuppSM State",
                           "FORCE UNAUTHORIZED");
                break;

            default:
                break;
        }

        switch (i4BendSMState)
        {
            case PNAC_BSM_STATE_REQUEST:
                CliPrintf (CliHandle, "\r\n%-20s  = %s", "BendSM State",
                           "REQUEST");
                break;

            case PNAC_BSM_STATE_RESPONSE:
                CliPrintf (CliHandle, "\r\n%-20s  = %s", "BendSM State",
                           "RESPONSE");
                break;
            case PNAC_BSM_STATE_SUCCESS:
                CliPrintf (CliHandle, "\r\n%-20s  = %s", "BendSM State",
                           "SUCCESS");
                break;
            case PNAC_BSM_STATE_FAIL:
                CliPrintf (CliHandle, "\r\n%-20s  = %s", "BendSM State",
                           "FAIL");
                break;
            case PNAC_BSM_STATE_TIMEOUT:
                CliPrintf (CliHandle, "\r\n%-20s  = %s", "BendSM State",
                           "TIMEOUT");
                break;
            case PNAC_BSM_STATE_IDLE:
                CliPrintf (CliHandle, "\r\n%-20s  = %s", "BendSM State",
                           "IDLE");
                break;
            case PNAC_BSM_STATE_INITIALIZE:
                CliPrintf (CliHandle, "\r\n%-20s  = %s", "BendSM State",
                           "INITIALIZE");
                break;

            default:
                break;
        }

        if (i4PortStatus == PNAC_PORTSTATUS_UNAUTHORIZED)
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "AuthPortStatus",
                       "UNAUTHORIZED");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "AuthPortStatus",
                       "AUTHORIZED");
        }

        if (i4SuppPortStatus == PNAC_PORTSTATUS_UNAUTHORIZED)
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "SuppPortStatus",
                       "UNAUTHORIZED");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "SuppPortStatus",
                       "AUTHORIZED");
        }

        if (i4AdminControlledDirection == PNAC_CNTRLD_DIR_BOTH)
        {
            CliPrintf (CliHandle, "\r\n%-20s = %s", "AdminControlDirection",
                       "BOTH");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n%-20s = %s", "AdminControlDirection",
                       "IN");
        }

        if (i4OperControlledDirection == PNAC_CNTRLD_DIR_BOTH)
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "OperControlDirection",
                       "BOTH");
        }
        else
        {

            if ((DPnacCheckIsLocalPort ((UINT2) u4IfIndex) == PNAC_SUCCESS) &&
                (gPnacSystemInfo.u1SystemMode == PNAC_DISTRIBUTED))
            {
                CliPrintf (CliHandle, "\r\n%-20s  = %s", "OperControlDirection",
                           "IN");
            }
            else
            {
                CliPrintf (CliHandle, "\r\n%-20s  = %s", "OperControlDirection",
                           "BOTH");
            }
        }

        CliPrintf (CliHandle, "\r\n%-20s  = %d", "MaxReq", i4MaxReq);
        CliPrintf (CliHandle, "\r\n%-20s  = %d", "ReAuthMax", i4ReAuthMax);

        if (i4SuppAccCtrlWithAuth == PNAC_SUPP_ACCESS_CTRL_ACTIVE)
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %d", "MaxStart", i4MaxStart);
        }

        switch (i4PortControl)
        {
            case PNAC_PORTCNTRL_AUTO:
                CliPrintf (CliHandle, "\r\n%-20s  = %s", "Port Control",
                           "Auto");
                break;
            case PNAC_PORTCNTRL_FORCEAUTHORIZED:
                CliPrintf (CliHandle, "\r\n%-20s  = %s", "Port Control",
                           "Force Authorized");
                break;
            case PNAC_PORTCNTRL_FORCEUNAUTHORIZED:
                CliPrintf (CliHandle, "\r\n%-20s  = %s", "Port Control",
                           "Force Un-Authorized");
                break;
        }

        CliPrintf (CliHandle, "\r\n%-20s  = %d Seconds", "QuietPeriod",
                   i4QuietPeriod);

        if (i4ReAuth == PNAC_MIB_FALSE)
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "Re-authentication",
                       "Disabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "Re-authentication",
                       "Enabled");
        }

        CliPrintf (CliHandle, "\r\n%-20s  = %d Seconds", "ReAuthPeriod",
                   i4ReAuthPeriod);

        CliPrintf (CliHandle, "\r\n%-20s  = %d Seconds", "ServerTimeout",
                   i4ServerTimeOut);

        CliPrintf (CliHandle, "\r\n%-20s  = %d Seconds", "SuppTimeout",
                   i4SuppTimeOut);

        CliPrintf (CliHandle, "\r\n%-20s  = %d Seconds", "Tx Period",
                   i4TxPeriod);
        if (i4SuppAccCtrlWithAuth == PNAC_SUPP_ACCESS_CTRL_ACTIVE)
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %d Seconds", "Start Period",
                       i4StartPeriod);
            CliPrintf (CliHandle, "\r\n%-20s  = %d Seconds", "Held Period",
                       i4HeldPeriod);
            CliPrintf (CliHandle, "\r\n%-20s  = %d Seconds", "Auth Period",
                       i4AuthPeriod);
        }

        u1Quit = (UINT1) CliPrintf (CliHandle, "\r\n");

        if (u1Quit == CLI_FAILURE)
        {
            break;
        }
    }
    while ((nmhGetNextIndexDot1xPaePortTable ((INT4) u4PrevIndex,
                                              (INT4 *) &u4IfIndex)
            == SNMP_SUCCESS) && (u1Flag == 0));

    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacShowAuthInterfaceStats                         */
/*                                                                           */
/*     DESCRIPTION      : This function shows the Pnac port statistics       */
/*                                                                           */
/*     INPUT            : u4IfIndex -  Interface Index                       */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacShowAuthInterfaceStats (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    UINT4               u4TxReqId = 0;
    UINT4               u4TxReq = 0;
    UINT4               u4RxStart = 0;
    UINT4               u4RxLogoff = 0;
    UINT4               u4RxRespId = 0;
    UINT4               u4RxResp = 0;
    UINT4               u4RxInvalid = 0;
    UINT4               u4RxLenErr = 0;
    UINT4               u4RxVersion = 0;

    UINT4               u4Total = 0;
    INT1                ai1MacBuf[PNAC_CLI_NAME_SIZE];
    INT1                i1RetVal = SNMP_FAILURE;
    tMacAddr            MacAddress;

    PNAC_MEMSET (ai1MacBuf, 0, PNAC_CLI_NAME_SIZE);

    /* Get all the Mib Objects */

    /* Eapol Request Identity Frames Transmitted */
    i1RetVal = nmhGetDot1xAuthEapolReqIdFramesTx (u4IfIndex, &u4TxReqId);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Eapol Request Challenge Frames Transmitted */
    i1RetVal = nmhGetDot1xAuthEapolReqFramesTx (u4IfIndex, &u4TxReq);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Eapol Start Frames Received */
    i1RetVal = nmhGetDot1xAuthEapolStartFramesRx (u4IfIndex, &u4RxStart);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Eapol Logoff Frames Received */
    i1RetVal = nmhGetDot1xAuthEapolLogoffFramesRx (u4IfIndex, &u4RxLogoff);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Eapol Response Identity Frames Received */
    i1RetVal = nmhGetDot1xAuthEapolRespIdFramesRx (u4IfIndex, &u4RxRespId);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Eapol Response Challenge Frames Received */
    i1RetVal = nmhGetDot1xAuthEapolRespFramesRx (u4IfIndex, &u4RxResp);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Invalid Eapol Frames Received */
    i1RetVal = nmhGetDot1xAuthInvalidEapolFramesRx (u4IfIndex, &u4RxInvalid);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Length Error Eapol Frames Received */
    i1RetVal = nmhGetDot1xAuthEapLengthErrorFramesRx (u4IfIndex, &u4RxLenErr);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Version of the last received Eapol Frame */
    i1RetVal = nmhGetDot1xAuthLastEapolFrameVersion (u4IfIndex, &u4RxVersion);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Source MAC Address of the last received Eapol Frame */
    i1RetVal = nmhGetDot1xAuthLastEapolFrameSource (u4IfIndex, &MacAddress);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Do Release the lock here.. */

    /* Print all the values of the objects */

    CliPrintf (CliHandle, "\r\nPortStatistics Parameters for Dot1x\r\n");
    CliPrintf (CliHandle, "\r------------------------------------\r\n");

    /* Eapol Request Identity Frames Transmitted */
    CliPrintf (CliHandle, "\r\n%-15s = %-5d", "TxReqId", u4TxReqId);
    u4Total += u4TxReqId;

    /* Eapol Request Challenge Frames Transmitted */
    CliPrintf (CliHandle, "\r\n%-15s = %-5d", "TxReq", u4TxReq);
    u4Total += u4TxReq;

    /* Total Eapol Request Frames Transmitted */
    CliPrintf (CliHandle, "\r\n%-15s = %-5d", "TxTotal ", u4Total);
    CliPrintf (CliHandle, "\r\n");
    u4Total = 0;

    /* Eapol Start Frames Received */
    CliPrintf (CliHandle, "\r\n%-15s = %-5d", "RxStart", u4RxStart);
    u4Total += u4RxStart;

    /* Eapol Logoff Frames Received */
    CliPrintf (CliHandle, "\r\n%-15s = %-5d", "RxLogoff", u4RxLogoff);
    u4Total += u4RxLogoff;

    /* Eapol Response Identity Frames Received */
    CliPrintf (CliHandle, "\r\n%-15s = %-5d", "RxRespId", u4RxRespId);
    u4Total += u4RxRespId;

    /* Eapol Response Challenge Frames Received */
    CliPrintf (CliHandle, "\r\n%-15s = %-5d", "RxResp", u4RxResp);
    u4Total += u4RxResp;

    CliPrintf (CliHandle, "\r\n");

    /* Invalid Eapol Frames Received */
    CliPrintf (CliHandle, "\r\n%-15s = %-5d", "RxInvalid", u4RxInvalid);
    u4Total += u4RxInvalid;

    /* Length Error Eapol Frames Received */
    CliPrintf (CliHandle, "\r\n%-15s = %-5d", "RxLenErr", u4RxLenErr);
    u4Total += u4RxLenErr;

    /* Total Eapol Frames Received */
    CliPrintf (CliHandle, "\r\n%-15s = %-5d", "RxTotal", u4Total);
    CliPrintf (CliHandle, "\r\n");

    /* Version of the last received Eapol Frame */
    CliPrintf (CliHandle, "\r\n%-15s = %-5d", "RxVersion", u4RxVersion);

    /* Source MAC Address of the last received Eapol Frame */
    PrintMacAddress (MacAddress, (UINT1 *) ai1MacBuf);

    CliPrintf (CliHandle, "\r\n%-15s = %s\r\n", "LastRxSrcMac", ai1MacBuf);

    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacShowSuppInterfaceStats                         */
/*                                                                           */
/*     DESCRIPTION      : This function shows the Pnac port statistics       */
/*                                                                           */
/*     INPUT            : u4IfIndex -  Interface Index                       */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacShowSuppInterfaceStats (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    UINT4               u4TxStart = 0;
    UINT4               u4TxLogoff = 0;
    UINT4               u4TxRespId = 0;
    UINT4               u4TxResp = 0;
    UINT4               u4RxReqId = 0;
    UINT4               u4RxReq = 0;
    UINT4               u4RxInvalid = 0;
    UINT4               u4RxLenErr = 0;
    UINT4               u4RxVersion = 0;

    UINT4               u4Total = 0;
    INT1                ai1MacBuf[PNAC_CLI_NAME_SIZE];
    INT1                i1RetVal = SNMP_FAILURE;
    tMacAddr            MacAddress;
    tPnacPaePortEntry  *pPortInfo = NULL;

    PNAC_MEMSET (ai1MacBuf, 0, PNAC_CLI_NAME_SIZE);

    /* Get all the Mib Objects */

    if (PnacGetPortEntry ((UINT2) u4IfIndex, &pPortInfo) == PNAC_SUCCESS)
    {
        if (PNAC_IS_PORT_MODE_MACBASED (pPortInfo))
        {
            CLI_SET_ERR (CLI_SUPP_PAE_NOT_SUPPORTED_MAC_BASED);
            return (CLI_FAILURE);
        }
    }

    /* Eapol Request Identity Frames Received */
    i1RetVal = nmhGetDot1xSuppEapolReqIdFramesRx (u4IfIndex, &u4RxReqId);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Eapol Request Challenge Frames Received */
    i1RetVal = nmhGetDot1xSuppEapolReqFramesRx (u4IfIndex, &u4RxReq);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Eapol Start Frames Transmitted */
    i1RetVal = nmhGetDot1xSuppEapolStartFramesTx (u4IfIndex, &u4TxStart);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Eapol Logoff Frames Transmitted */
    i1RetVal = nmhGetDot1xSuppEapolLogoffFramesTx (u4IfIndex, &u4TxLogoff);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Eapol Response Identity Frames Transmitted */
    i1RetVal = nmhGetDot1xSuppEapolRespIdFramesTx (u4IfIndex, &u4TxRespId);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Eapol Response Challenge Frames Transmitted */
    i1RetVal = nmhGetDot1xSuppEapolRespFramesTx (u4IfIndex, &u4TxResp);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Invalid Eapol Frames Received */
    i1RetVal = nmhGetDot1xSuppInvalidEapolFramesRx (u4IfIndex, &u4RxInvalid);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Length Error Eapol Frames Received */
    i1RetVal = nmhGetDot1xSuppEapLengthErrorFramesRx (u4IfIndex, &u4RxLenErr);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Version of the last received Eapol Frame */
    i1RetVal = nmhGetDot1xSuppLastEapolFrameVersion (u4IfIndex, &u4RxVersion);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Source MAC Address of the last received Eapol Frame */
    i1RetVal = nmhGetDot1xSuppLastEapolFrameSource (u4IfIndex, &MacAddress);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Do Release the lock here.. */

    /* Print all the values of the objects */

    CliPrintf (CliHandle,
               "\r\nPortStatistics Parameters for Dot1x-Supplicant\r\n");
    CliPrintf (CliHandle,
               "\r----------------------------------------------\r\n");

    /* Eapol Start Frames Transmitted */
    CliPrintf (CliHandle, "\r\n%-15s = %-5d", "TxStart", u4TxStart);
    u4Total += u4TxStart;

    /* Eapol Response Identity Frames Transmitted */
    CliPrintf (CliHandle, "\r\n%-15s = %-5d", "TxRespId", u4TxRespId);
    u4Total += u4TxRespId;

    /* Eapol Response Challenge Frames Transmitted */
    CliPrintf (CliHandle, "\r\n%-15s = %-5d", "TxResp", u4TxResp);
    u4Total += u4TxResp;

    /* Eapol Logoff Frames Transmitted */
    CliPrintf (CliHandle, "\r\n%-15s = %-5d", "TxLogoff", u4TxLogoff);
    u4Total += u4TxLogoff;

    /* Total Eapol Frames Transmitted by Supplicant */
    CliPrintf (CliHandle, "\r\n%-15s = %-5d", "TxTotal ", u4Total);
    CliPrintf (CliHandle, "\r\n");
    u4Total = 0;

    /* Eapol Request Identity Frames Received */
    CliPrintf (CliHandle, "\r\n%-15s = %-5d", "RxReqId", u4RxReqId);
    u4Total += u4RxReqId;

    /* Eapol Request Challenge Frames Received */
    CliPrintf (CliHandle, "\r\n%-15s = %-5d", "RxReq", u4RxReq);
    u4Total += u4RxReq;

    CliPrintf (CliHandle, "\r\n");

    /* Invalid Eapol Frames Received */
    CliPrintf (CliHandle, "\r\n%-15s = %-5d", "RxInvalid", u4RxInvalid);
    u4Total += u4RxInvalid;

    /* Length Error Eapol Frames Received */
    CliPrintf (CliHandle, "\r\n%-15s = %-5d", "RxLenErr", u4RxLenErr);
    u4Total += u4RxLenErr;

    /* Total Eapol Frames Received */
    CliPrintf (CliHandle, "\r\n%-15s = %-5d", "RxTotal", u4Total);
    CliPrintf (CliHandle, "\r\n");

    /* Version of the last received Eapol Frame */
    CliPrintf (CliHandle, "\r\n%-15s = %-5d", "RxVersion", u4RxVersion);

    /* Source MAC Address of the last received Eapol Frame */
    PrintMacAddress (MacAddress, (UINT1 *) ai1MacBuf);

    CliPrintf (CliHandle, "\r\n%-15s = %s\r\n", "LastRxSrcMac", ai1MacBuf);

    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacShowGlobalInfo                                 */
/*                                                                           */
/*     DESCRIPTION      : This function shows the Pnac global information    */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacShowGlobalInfo (tCliHandle CliHandle)
{
    UINT4               u4Temp = 0;
    UINT4               u4RemAuth = 0;
    UINT4               u4SysControl = 0;
    UINT4               u4ModuleState = 0;
    INT4                i4ModuleOperStatus = 0;
    tSNMP_OCTET_STRING_TYPE *NasId = NULL;

    NasId =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (PNAC_CLI_NAME_SIZE);

    if (NasId == NULL)
    {
        return CLI_FAILURE;
    }

    /* get all the Mib objects */

    nmhGetFsPnacSystemControl ((INT4 *) &u4SysControl);
    nmhGetDot1xPaeSystemAuthControl ((INT4 *) &u4ModuleState);
    nmhGetFsPnacAuthenticServer ((INT4 *) &u4Temp);
    nmhGetFsPnacRemoteAuthServerType ((INT4 *) &u4RemAuth);

    PNAC_MEMSET (NasId->pu1_OctetList, 0, PNAC_CLI_NAME_SIZE);
    nmhGetFsPnacNasId (NasId);

    /* Do Release the Lock here */

    /* Print all the values of the objects */

    if ((u4SysControl == PNAC_START) && (u4ModuleState == PNAC_ENABLED))
    {
        /* Module admin status is enabled. */
        CliPrintf (CliHandle, "\r\n %-35s = %s", "Sysauthcontrol", "Enabled");

        /* Check the module operational status. */
        nmhGetFsPnacModuleOperStatus (&i4ModuleOperStatus);
        if (i4ModuleOperStatus == PNAC_ENABLED)
        {
            CliPrintf (CliHandle, "\r\n %-35s = %s", "Module Oper Status",
                       "Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n %-35s = %s", "Module Oper Status",
                       "Disabled");
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r\n %-35s = %s", "Sysauthcontrol", "Disabled");
        CliPrintf (CliHandle, "\r\n %-35s = %s", "Module Oper Status",
                   "Disabled");
    }

    CliPrintf (CliHandle, "\r\n %-35s = %d", "Dot1x Protocol Version",
               PNAC_EAPOLVERSION);

    /* display the location of Authentication Server */

    if (u4Temp == PNAC_LOCAL_AUTH_SERVER)
    {
        CliPrintf (CliHandle, "\r\n %-35s = %s",
                   "Dot1x Authentication Method", "Local");
    }
    else
    {
        if (u4RemAuth == PNAC_REMOTE_RADIUS_AUTH_SERVER)
        {
            CliPrintf (CliHandle, "\r\n %-35s = %s",
                       "Dot1x Authentication Method", "Radius");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n %-35s = %s",
                       "Dot1x Authentication Method", "TacacsPlus");
        }
    }

    CliPrintf (CliHandle, "\r\n %-35s = %s\r\n", "Nas ID",
               NasId->pu1_OctetList);
    free_octetstring (NasId);
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacShowDatabaseInfo                               */
/*                                                                           */
/*     DESCRIPTION      : This function shows the Local Authentication       */
/*                        Server details                                     */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PnacShowDatabaseInfo (tCliHandle CliHandle)
{
    INT4                i4TimeOut = 0;
    INT4                i4AuthProtocol = 0;
    INT4                i4Permission = 0;
    INT4                i4Result = 0;
    UINT4               u4PagingStatus;
    UINT4               u4Port = 0;
    tSNMP_OCTET_STRING_TYPE *pPortList;
    tSNMP_OCTET_STRING_TYPE *pLocalPortList;
    tSNMP_OCTET_STRING_TYPE CurrentName;
    tSNMP_OCTET_STRING_TYPE NextName;
    tPnacPaePortEntry  *pTmpPortInfo = NULL;
    UINT1               ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1TmpCurrName[PNAC_CLI_NAME_SIZE];
    UINT1               au1TmpNextName[PNAC_CLI_NAME_SIZE];
    UINT1               au1ZeroPorts[PNAC_PORTLIST_LEN];
    UINT1               u1CliRetVal = 0;
    INT1                i1OutCome = SNMP_FAILURE;

    PNAC_MEMSET (au1TmpCurrName, 0, PNAC_CLI_NAME_SIZE);
    PNAC_MEMSET (au1TmpNextName, 0, PNAC_CLI_NAME_SIZE);
    PNAC_MEMSET (au1ZeroPorts, 0, PNAC_PORTLIST_LEN);
    PNAC_MEMSET (ai1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    CurrentName.pu1_OctetList = (UINT1 *) &au1TmpCurrName;
    NextName.pu1_OctetList = (UINT1 *) &au1TmpNextName;

    pPortList =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (PNAC_PORTLIST_LEN);

    if (pPortList == NULL)
    {
        return CLI_FAILURE;
    }

    pLocalPortList =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (PNAC_PORTLIST_LEN);

    if (pLocalPortList == NULL)
    {
        free_octetstring (pPortList);
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nPnac Authentication Users Database\r\n");
    CliPrintf (CliHandle, "-----------------------------------\r\n");

    i1OutCome = nmhGetFirstIndexFsPnacASUserConfigTable (&NextName);

    if (i1OutCome == SNMP_FAILURE)
    {
        free_octetstring (pPortList);
        free_octetstring (pLocalPortList);
        return (CLI_FAILURE);
    }

    do
    {
        /* Get all the Objects */

        /*Protocol */
        nmhGetFsPnacASUserConfigAuthProtocol (&NextName,
                                              (UINT4 *) &i4AuthProtocol);

        /*TimeOut */
        nmhGetFsPnacASUserConfigAuthTimeout (&NextName, (UINT4 *) &i4TimeOut);

        /*PortList */
        if (pPortList->pu1_OctetList == NULL)
        {
            free_octetstring (pPortList);
            free_octetstring (pLocalPortList);
            return CLI_FAILURE;
        }

        PNAC_MEMSET (pPortList->pu1_OctetList, 0, PNAC_PORTLIST_LEN);
        PNAC_MEMSET (pLocalPortList->pu1_OctetList, 0, PNAC_PORTLIST_LEN);

        nmhGetFsPnacASUserConfigPortList (&NextName, pPortList);

        /*Permission */
        nmhGetFsPnacASUserConfigPermission (&NextName, &i4Permission);

        /* Print all their Values */

        /* User Name */
        CliPrintf (CliHandle, "\r%-15s: %s\r\n", "User name",
                   NextName.pu1_OctetList);
        /*Protocol */
        CliPrintf (CliHandle, "\r%-15s: %d\r\n", "Protocol", i4AuthProtocol);
        /*TimeOut */
        CliPrintf (CliHandle, "\r%-15s: %d seconds\r\n", "Timeout", i4TimeOut);

        if (pPortList->pu1_OctetList)
        {
            for (u4Port = PNAC_MINPORTS; u4Port <= PNAC_MAXPORTS; u4Port++)
            {
                /* scan through all the ports present in the pPortList */
                OSIX_BITLIST_IS_BIT_SET (pPortList->pu1_OctetList,
                                         u4Port, pPortList->i4_Length,
                                         i4Result);
                if (i4Result == OSIX_TRUE)
                {
                    /* bit is set. So Port is present in the pPortList
                     * Now check whether this port is Active for PNAC Module*/

                    pTmpPortInfo = &(PNAC_PORT_INFO (u4Port));
                    if (pTmpPortInfo->u1EntryStatus != PNAC_PORT_INVALID)
                    {
                        /* Port is Valid.So update the pLocalPortList Here */
                        OSIX_BITLIST_SET_BIT (pLocalPortList->pu1_OctetList,
                                              u4Port,
                                              pLocalPortList->i4_Length);
                    }

                }                /* if */
            }                    /* for */
        }                        /* if */

        /* PortList */
        if (CliOctetToIfName (CliHandle, "Ports          :",
                              pLocalPortList,
                              SYS_DEF_MAX_PHYSICAL_INTERFACES,
                              PNAC_PORT_LIST_SIZE, 0,
                              &u4PagingStatus, 6) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n");
        }

        if (i4Permission == 1)
        {
            CliPrintf (CliHandle, "\r%-15s: Allow\r\n", "Permission");
        }
        else if (i4Permission == 2)
        {
            CliPrintf (CliHandle, "\r%-15s: Deny\r\n", "Permission");
        }
        else
        {
            CliPrintf (CliHandle, "\r%-15s: Unknown\r\n", "Permission");
        }

        u1CliRetVal = (UINT1)
            CliPrintf (CliHandle, "\r---------------------------------\r\n");

        PNAC_MEMSET (CurrentName.pu1_OctetList, 0, PNAC_CLI_NAME_SIZE);
        PNAC_MEMCPY (CurrentName.pu1_OctetList, NextName.pu1_OctetList,
                     NextName.i4_Length);
        CurrentName.i4_Length = NextName.i4_Length;
        PNAC_MEMSET (NextName.pu1_OctetList, 0, PNAC_CLI_NAME_SIZE);

        i1OutCome = nmhGetNextIndexFsPnacASUserConfigTable (&CurrentName,
                                                            &NextName);
    }
    while ((i1OutCome == SNMP_SUCCESS) && (u1CliRetVal == CLI_SUCCESS));

    CliPrintf (CliHandle, "\r\n");
    free_octetstring (pLocalPortList);
    free_octetstring (pPortList);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacShowAllMacInfo                                 */
/*                                                                           */
/*     DESCRIPTION      : This function shows pnac auth session              */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
PnacShowAllMacInfo (tCliHandle CliHandle)
{

    tMacAddr            NextAddr = "";
    tMacAddr            CurrentAddr = "";
    UINT4               u4PagingStatus = CLI_SUCCESS;

    INT4                i4OutCome;
    INT4                i4RetVal = 0;
    UINT1               u1isShowAll = TRUE;
    INT1                ai1MacBuf[PNAC_CLI_NAME_SIZE];

    i4OutCome = (INT4) (nmhGetFirstIndexFsPnacAuthSessionTable (&NextAddr));

    if (i4OutCome == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\r\nPnac Mac Database\r\n");
    CliPrintf (CliHandle, "-----------------------------------\r\n");
    do
    {

        PNAC_MEMSET (ai1MacBuf, 0, PNAC_CLI_NAME_SIZE);
        /*port number */
        nmhGetFsPnacAuthSessionPortNumber (NextAddr, &i4RetVal);
        /* Source MAC Address of the last received Eapol Frame */
        if (PnacChkSuppMacIsUniqueMacForPort (NextAddr, (UINT2) i4RetVal) !=
            PNAC_SUCCESS)
        {
            PrintMacAddress (NextAddr, (UINT1 *) ai1MacBuf);

            CliPrintf (CliHandle, "\r\n%-15s = %s\r\n", "Supplicant Mac-Addr",
                       ai1MacBuf);
        }
        else
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "Supplicant Mac-Addr",
                       "(unknown)");
        }

        nmhGetFsPnacAuthSessionIdentifier (NextAddr, &i4RetVal);
        CliPrintf (CliHandle, "\r\n%-20s = %d\r\n", "Session Identifier:",
                   i4RetVal);
        /*auth pae state */
        nmhGetFsPnacAuthSessionAuthPaeState (NextAddr, &i4RetVal);
        PnacShowAuthSmState (CliHandle, i4RetVal);
        /*port status */
        nmhGetFsPnacAuthSessionPortStatus (NextAddr, &i4RetVal);
        if (i4RetVal == 1)
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "Auth-Session Status",
                       "AUTHORIZED");
        }
        else if (i4RetVal == 2)
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "Auth-Session Status",
                       "UNAUTHORIZED");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "Auth-Session Status",
                       "UNKNOWN");
        }

        /*port number */
        nmhGetFsPnacAuthSessionPortNumber (NextAddr, &i4RetVal);
        CliPrintf (CliHandle, "\r\n%-20s = %d\r\n", "Port Number", i4RetVal);
        CLI_MEMCPY (CurrentAddr, NextAddr, 6);
        u4PagingStatus = CliPrintf (CliHandle,
                                    "----------------------------------------------------------\r\n");
        i4OutCome =
            (INT4) (nmhGetNextIndexFsPnacAuthSessionTable
                    (CurrentAddr, &NextAddr));

        if (i4OutCome == SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
        }
        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            u1isShowAll = FALSE;
        }

    }
    while (u1isShowAll);
    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacShowMacInfo                                    */
/*                                                                           */
/*     DESCRIPTION      : This function shows pnac auth session              */
/*                                                                           */
/*     INPUT            : CliHandle: Cli Handler                             */
/*                      : pSrcMacAddr: Source Mac Address                    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
PnacShowMacInfo (tCliHandle CliHandle, UINT1 *pSrcMacAddr)
{

    INT4                i4RetVal = 0;
    UINT1               ai1MacBuf[PNAC_CLI_NAME_SIZE];

    CliPrintf (CliHandle, "\r\nPnac Mac Database\r\n");
    CliPrintf (CliHandle, "-----------------------------------\r\n");

    PNAC_MEMSET (ai1MacBuf, 0, PNAC_CLI_NAME_SIZE);
    /* Source MAC Address of the last received Eapol Frame */
    PrintMacAddress (pSrcMacAddr, (UINT1 *) ai1MacBuf);

    CliPrintf (CliHandle, "\r\n%-15s = %s\r\n", "Supplicant Mac-Addr",
               ai1MacBuf);
    if (nmhGetFsPnacAuthSessionIdentifier (pSrcMacAddr, &i4RetVal)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%-20s = %d\r\n", "Session Identifier:",
                   i4RetVal);
    }
    /*auth pae state */
    if (nmhGetFsPnacAuthSessionAuthPaeState (pSrcMacAddr, &i4RetVal)
        == SNMP_SUCCESS)
    {
        PnacShowAuthSmState (CliHandle, i4RetVal);
    }
    /*port status */
    if (nmhGetFsPnacAuthSessionPortStatus (pSrcMacAddr, &i4RetVal) ==
        SNMP_SUCCESS)
    {
        if (i4RetVal == 1)
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "Auth-Session Status",
                       "AUTHORIZED");
        }
        else if (i4RetVal == 2)
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "Auth-Session Status",
                       "UNAUTHORIZED");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "Auth-Session Status",
                       "UNKNOWN");
        }
    }

    /*port number */
    if (nmhGetFsPnacAuthSessionPortNumber (pSrcMacAddr, &i4RetVal) ==
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%-20s = %d\r\n", "Port Number", i4RetVal);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacOctetToIfName                                  */
/*                                                                           */
/*     DESCRIPTION      : This function converts octet to port and prints    */
/*                        the Interface name                                 */
/*                                                                           */
/*     INPUT            : pOctet        - Ports as octet list                */
/*                        u4OctetLen    - Length of port list                */
/*                        u4MaxPorts    - Max ports                          */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PnacOctetToIfName (tCliHandle CliHandle, UINT1 *pu1Octet, UINT4 u4OctetLen,
                   UINT4 u4MaxPorts)
{
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4Port = 0;
    UINT2               u2CommaCount = 0;

    if (pu1Octet)
    {
        for (u4Port = 1; u4Port <= u4MaxPorts; u4Port++)
        {
            if (CliIsMemberPort (pu1Octet, u4OctetLen, u4Port) == OSIX_SUCCESS)
            {
                if (PnacCfaCliGetIfName (u4Port, (INT1 *) au1NameStr) ==
                    CLI_FAILURE)
                {
                    return CLI_FAILURE;
                }

                u2CommaCount++;

                if (u2CommaCount == 1)
                {
                    CliPrintf (CliHandle, "%s", au1NameStr);
                }
                else
                {
                    CliPrintf (CliHandle, ", %s", au1NameStr);
                }
            }
        }
    }
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssPnacShowDebugging                               */
/*                                                                           */
/*     DESCRIPTION      : This function prints the PNAC debug level          */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
IssPnacShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DbgLevel = 0;

    if (PNAC_SYSTEM_CONTROL == PNAC_SHUTDOWN)
    {
        return;
    }

    nmhGetFsPnacTraceOption (&i4DbgLevel);

    if (i4DbgLevel == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "\rDOT1x :");

    if ((i4DbgLevel & PNAC_INIT_SHUT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DOT1x init and shutdown debugging is on");
    }

    if ((i4DbgLevel & PNAC_MGMT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DOT1x management debugging is on");
    }

    if ((i4DbgLevel & PNAC_DATA_PATH_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DOT1x data path debugging is on");
    }

    if ((i4DbgLevel & PNAC_CONTROL_PATH_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DOT1x state machine debugging is on");
    }

    if ((i4DbgLevel & PNAC_DUMP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DOT1x packet dump debugging is on");
    }

    if ((i4DbgLevel & PNAC_OS_RESOURCE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DOT1x resources debugging is on");
    }

    if ((i4DbgLevel & PNAC_ALL_FAILURE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DOT1x error debugging is on");
    }

    if ((i4DbgLevel & PNAC_BUFFER_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DOT1x buffer debugging is on");
    }

    if ((i4DbgLevel & PNAC_RED_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DOT1x redundancy debugging is on");
    }

    CliPrintf (CliHandle, "\r\n");

    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PnacShowRunningConfig                              */
/*                                                                           */
/*     DESCRIPTION      : This function displays current configuration       */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4Module - Specified module (pnac/all), for        */
/*                        configuration                                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
PnacShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{
    CliRegisterLock (CliHandle, PnacLock, PnacUnLock);
    PNAC_LOCK ();
    CliPrintf (CliHandle, "!\r\n");
    if (PnacShowRunningConfigScalars (CliHandle) == CLI_SUCCESS)
    {
        PnacShowRunningConfigTables (CliHandle);

        if (u4Module == ISS_DOT1X_SHOW_RUNNING_CONFIG)
        {
            PnacShowRunningConfigInterface (CliHandle);
        }

    }
    PNAC_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PnacShowRunningConfigInterfaceDetails              */
/*                                                                           */
/*     DESCRIPTION      : This function displays configuration for specified */
/*                        interface                                          */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4Index  - Specified interface for                 */
/*                        configuration                                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
PnacShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index)
{
    INT4                i4AuthMode = 0;
    INT4                i4PortPaeStatus = 0;
    INT4                i4Dot1xAuthReAuthEnabled = 0;
    INT4                i4Dot1xAuthAdminControlledDirections = 0;
    INT4                i4Dot1xSuppAccessCtrlWithAuth = 0;
    INT4                i4Dot1xAuthAuthControlledPortControl = 0;
    UINT4               u4Dot1xSuppHeldPeriod = 0;
    UINT4               u4Dot1xAuthReAuthPeriod = 0;
    UINT4               u4Dot1xAuthQuietPeriod = 0;
    UINT4               u4Dot1xAuthSuppTimeout = 0;
    UINT4               u4Dot1xSuppStartPeriod = 0;
    UINT4               u4Dot1xAuthTxPeriod = 0;
    UINT4               u4Dot1xAuthMaxReq = 0;
    UINT4               u4Dot1xSuppAuthPeriod = 0;
    UINT4               u4Dot1xSuppMaxStart = 0;
    UINT4               u4Dot1xAuthServerTimeout = 0;
    UINT4               u4PortAuthReAuthMax = 0;
    INT1                ai1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               u1Flag = OSIX_FALSE;

    MEMSET (ai1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);

    CliRegisterLock (CliHandle, PnacLock, PnacUnLock);
    PNAC_LOCK ();
    if (nmhValidateIndexInstanceFsPnacPaePortTable (i4Index) == SNMP_SUCCESS)
    {
        nmhGetFsPnacPaePortAuthStatus (i4Index, &i4PortPaeStatus);
        nmhGetFsPnacPaeAuthReAuthMax (i4Index, &u4PortAuthReAuthMax);
        nmhGetFsPnacPaePortAuthMode (i4Index, &i4AuthMode);
    }
    if (nmhValidateIndexInstanceDot1xAuthConfigTable (i4Index) == SNMP_SUCCESS)
    {

        nmhGetDot1xSuppAccessCtrlWithAuth (i4Index,
                                           &i4Dot1xSuppAccessCtrlWithAuth);
        nmhGetDot1xAuthAdminControlledDirections (i4Index,
                                                  &i4Dot1xAuthAdminControlledDirections);
        nmhGetDot1xAuthMaxReq ((INT4) i4Index, &u4Dot1xAuthMaxReq);
        nmhGetDot1xSuppMaxStart ((INT4) i4Index, &u4Dot1xSuppMaxStart);
        nmhGetDot1xAuthAuthControlledPortControl (i4Index,
                                                  &i4Dot1xAuthAuthControlledPortControl);
        nmhGetDot1xAuthReAuthEnabled (i4Index, &i4Dot1xAuthReAuthEnabled);
        nmhGetDot1xAuthQuietPeriod ((INT4) i4Index, &u4Dot1xAuthQuietPeriod);
        nmhGetDot1xAuthReAuthPeriod ((INT4) i4Index, &u4Dot1xAuthReAuthPeriod);
        nmhGetDot1xAuthServerTimeout ((INT4) i4Index,
                                      &u4Dot1xAuthServerTimeout);
        nmhGetDot1xAuthSuppTimeout ((INT4) i4Index, &u4Dot1xAuthSuppTimeout);
        nmhGetDot1xAuthTxPeriod ((INT4) i4Index, &u4Dot1xAuthTxPeriod);
        nmhGetDot1xSuppStartPeriod ((INT4) i4Index, &u4Dot1xSuppStartPeriod);
        nmhGetDot1xSuppHeldPeriod ((INT4) i4Index, &u4Dot1xSuppHeldPeriod);
        nmhGetDot1xSuppAuthPeriod ((INT4) i4Index, &u4Dot1xSuppAuthPeriod);

        if ((i4Dot1xSuppAccessCtrlWithAuth != PNAC_SUPP_ACCESS_CTRL_INACTIVE) ||
            (i4Dot1xAuthAdminControlledDirections != PNAC_CNTRLD_DIR_BOTH) ||
            (u4Dot1xAuthMaxReq != PNAC_MAXREQ) ||
            (u4Dot1xSuppMaxStart != PNAC_MAXSTART) ||
            (i4Dot1xAuthAuthControlledPortControl !=
             PNAC_PORTCNTRL_FORCEAUTHORIZED)
            || (i4AuthMode == PNAC_PORT_AUTHMODE_MACBASED)
            || (i4PortPaeStatus == PNAC_DISABLED)
            || (i4Dot1xAuthReAuthEnabled != PNAC_MIB_FALSE)
            || (u4Dot1xAuthQuietPeriod != PNAC_QUIETPERIOD)
            || (u4Dot1xAuthReAuthPeriod != PNAC_REAUTHPERIOD)
            || (u4Dot1xAuthServerTimeout != PNAC_SERVERTIMEOUT)
            || (u4Dot1xAuthSuppTimeout != PNAC_SUPPTIMEOUT)
            || (u4Dot1xAuthTxPeriod != PNAC_TXPERIOD)
            || (u4Dot1xSuppStartPeriod != PNAC_STARTPERIOD)
            || (u4Dot1xSuppHeldPeriod != PNAC_HELDPERIOD)
            || (u4PortAuthReAuthMax != PNAC_REAUTHMAX)
            || (u4Dot1xSuppAuthPeriod != PNAC_AUTHPERIOD))
        {
            CfaCliConfGetIfName ((UINT4) i4Index, ai1IfName);
            CliPrintf (CliHandle, "interface %s\r\n", ai1IfName);
            u1Flag = OSIX_TRUE;
        }

        if (i4Dot1xSuppAccessCtrlWithAuth != PNAC_SUPP_ACCESS_CTRL_INACTIVE)
        {
            CliPrintf (CliHandle, "dot1x access-control active\r\n");
        }
        if (i4Dot1xAuthAdminControlledDirections != PNAC_CNTRLD_DIR_BOTH)
        {
            CliPrintf (CliHandle, "dot1x control-direction in\r\n");
        }

        if (u4Dot1xAuthMaxReq != PNAC_MAXREQ)
        {
            CliPrintf (CliHandle, "dot1x max-req %d\r\n", u4Dot1xAuthMaxReq);
        }
        if (u4PortAuthReAuthMax != PNAC_REAUTHMAX)
        {
            CliPrintf (CliHandle, "dot1x reauth-max %d\r\n",
                       u4PortAuthReAuthMax);
        }

        if (u4Dot1xSuppMaxStart != PNAC_MAXSTART)
        {
            CliPrintf (CliHandle, "dot1x max-start %d\r\n",
                       u4Dot1xSuppMaxStart);
        }

        if (i4Dot1xAuthAuthControlledPortControl !=
            PNAC_PORTCNTRL_FORCEAUTHORIZED)
        {
            CliPrintf (CliHandle, "dot1x port-control ");

            switch (i4Dot1xAuthAuthControlledPortControl)
            {
                case PNAC_PORTCNTRL_AUTO:
                    CliPrintf (CliHandle, "auto\r\n");
                    break;
                case PNAC_PORTCNTRL_FORCEUNAUTHORIZED:
                    CliPrintf (CliHandle, "force-unauthorized\r\n");
                    break;
                default:
                    break;
            }
        }

        if (i4AuthMode == PNAC_PORT_AUTHMODE_MACBASED)
        {
            CliPrintf (CliHandle, "dot1x auth-mode mac-based\r\n");
        }

        if (i4PortPaeStatus == PNAC_DISABLED)
        {
            CliPrintf (CliHandle, "dot1x disable\r\n");
        }

        if (i4Dot1xAuthReAuthEnabled != PNAC_MIB_FALSE)
        {
            CliPrintf (CliHandle, "dot1x reauthentication\r\n");
        }

        if (u4Dot1xAuthQuietPeriod != PNAC_QUIETPERIOD)
        {
            CliPrintf (CliHandle, "dot1x timeout ");
            CliPrintf (CliHandle, "quiet-period %d\r\n",
                       u4Dot1xAuthQuietPeriod);
        }

        if (u4Dot1xAuthReAuthPeriod != PNAC_REAUTHPERIOD)
        {
            CliPrintf (CliHandle, "dot1x timeout ");
            CliPrintf (CliHandle, "reauth-period %d\r\n",
                       u4Dot1xAuthReAuthPeriod);
        }

        if (u4Dot1xAuthServerTimeout != PNAC_SERVERTIMEOUT)
        {
            CliPrintf (CliHandle, "dot1x timeout ");
            CliPrintf (CliHandle, "server-timeout %d\r\n",
                       u4Dot1xAuthServerTimeout);
        }

        if (u4Dot1xAuthSuppTimeout != PNAC_SUPPTIMEOUT)
        {
            CliPrintf (CliHandle, "dot1x timeout ");
            CliPrintf (CliHandle, "supp-timeout %d\r\n",
                       u4Dot1xAuthSuppTimeout);
        }

        if (u4Dot1xAuthTxPeriod != PNAC_TXPERIOD)
        {
            CliPrintf (CliHandle, "dot1x timeout ");
            CliPrintf (CliHandle, "tx-period %d\r\n", u4Dot1xAuthTxPeriod);
        }

        if (u4Dot1xSuppStartPeriod != PNAC_STARTPERIOD)
        {
            CliPrintf (CliHandle, "dot1x timeout ");
            CliPrintf (CliHandle, "start-period %d\r\n",
                       u4Dot1xSuppStartPeriod);
        }

        if (u4Dot1xSuppHeldPeriod != PNAC_HELDPERIOD)
        {
            CliPrintf (CliHandle, "dot1x timeout ");
            CliPrintf (CliHandle, "held-period %d\r\n", u4Dot1xSuppHeldPeriod);
        }

        if (u4Dot1xSuppAuthPeriod != PNAC_AUTHPERIOD)
        {
            CliPrintf (CliHandle, "dot1x timeout ");
            CliPrintf (CliHandle, "auth-period %d\r\n", u4Dot1xSuppAuthPeriod);
        }
        if (u1Flag == OSIX_TRUE)
        {
            CliPrintf (CliHandle, "! \r\n");
        }
    }

    PNAC_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : PnacShowRunningConfigInterface                     */
/*                                                                           */
/*     DESCRIPTION      : This function displays interface configuration     */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
PnacShowRunningConfigInterface (tCliHandle CliHandle)
{
    UINT4               u4NextIndex;
    UINT4               u4CurrentIndex;
    UINT1               u1isShowAll = TRUE;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];

    if (nmhGetFirstIndexDot1xAuthConfigTable ((INT4 *) &u4NextIndex)
        == SNMP_SUCCESS)
    {
        do
        {
            PnacCfaCliConfGetIfName (u4NextIndex, (INT1 *) au1NameStr);

            PNAC_UNLOCK ();
            CliUnRegisterLock (CliHandle);

            PnacShowRunningConfigInterfaceDetails (CliHandle, u4NextIndex);

            CliRegisterLock (CliHandle, PnacLock, PnacUnLock);
            PNAC_LOCK ();

            u4CurrentIndex = u4NextIndex;
            if (nmhGetNextIndexDot1xAuthConfigTable ((INT4) u4CurrentIndex,
                                                     (INT4 *) &u4NextIndex)
                == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll == TRUE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : PnacShowRunningConfigScalar                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays scalars configuration       */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
PnacShowRunningConfigScalars (tCliHandle CliHandle)
{
    UINT4               u4SysControl = 0;
    UINT4               u4AuthServer = 0;
    UINT4               u4RemoteAuthServer = 0;
    UINT4               u4ModuleState = 0;
    UINT4               u4PnacDPnacPeriodicSyncTime = 0;
    tSNMP_OCTET_STRING_TYPE *NasId = NULL;
    INT4                i4PnacDPnacSystemStatus = 0;
    INT4                i4PnacDPnacMaxKeepAliveCount = 0;

    nmhGetFsPnacSystemControl ((INT4 *) &u4SysControl);

    if (u4SysControl == PNAC_SHUTDOWN)
    {
        CliPrintf (CliHandle, "shutdown dot1x\r\n");

        return (CLI_FAILURE);
    }
    NasId =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (PNAC_CLI_NAME_SIZE);

    if (NasId != NULL)
    {
        PNAC_MEMSET (NasId->pu1_OctetList, 0, PNAC_CLI_NAME_SIZE);
        nmhGetFsPnacNasId (NasId);
    }

    nmhGetFsPnacAuthenticServer ((INT4 *) &u4AuthServer);
    nmhGetFsPnacRemoteAuthServerType ((INT4 *) &u4RemoteAuthServer);
    nmhGetDot1xPaeSystemAuthControl ((INT4 *) &u4ModuleState);

    nmhGetFsDPnacSystemStatus (&i4PnacDPnacSystemStatus);
    nmhGetFsDPnacPeriodicSyncTime (&u4PnacDPnacPeriodicSyncTime);
    nmhGetFsDPnacMaxKeepAliveCount (&i4PnacDPnacMaxKeepAliveCount);

    if (u4ModuleState != PNAC_START)
    {
        CliPrintf (CliHandle, "no dot1x system-auth-control\r\n");
    }
    if (u4AuthServer == PNAC_REMOTE_AUTH_SERVER)
    {
        if (u4RemoteAuthServer == PNAC_REMOTE_RADIUS_AUTH_SERVER)
        {
            CliPrintf (CliHandle,
                       "aaa authentication dot1x default group radius\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       "aaa authentication dot1x default group tacacsplus\r\n");
        }

    }

    if (i4PnacDPnacSystemStatus != PNAC_CENTRALIZED)
    {
        CliPrintf (CliHandle, "dot1x mode distributed\r\n");
    }

    if (u4PnacDPnacPeriodicSyncTime != PNAC_DEFAULT_PERIODIC_SYNC_TIME)
    {

        CliPrintf (CliHandle, "dot1x distributed periodic-sync-time %d\r\n",
                   u4PnacDPnacPeriodicSyncTime);
    }

    if (i4PnacDPnacMaxKeepAliveCount != PNAC_DEFAULT_MAX_KEEP_ALIVE_COUNT)
    {
        CliPrintf (CliHandle, "dot1x distributed max-keep-alive-count %d\r\n",
                   i4PnacDPnacMaxKeepAliveCount);
    }

    if ((NasId != NULL) &&
        (STRCMP (NasId->pu1_OctetList, PNAC_DEFAULT_NAS_ID) != 0))
    {
        CliPrintf (CliHandle, "set nas-id \"%s\"\r\n", NasId->pu1_OctetList);
    }
    if (NasId != NULL)
    {
        free_octetstring (NasId);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : PnacShowRunningConfigTables                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays table configuration         */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
PnacShowRunningConfigTables (tCliHandle CliHandle)
{
    INT4                u4TimeOut = 0;
    INT4                i4Permission = 0;
    INT4                i4RowStatus = 0;
    UINT1               ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1TmpCurrName[PNAC_CLI_NAME_SIZE];
    UINT1               au1TmpNextName[PNAC_CLI_NAME_SIZE];
    UINT1               au1TmpPassword[PNAC_CLI_NAME_SIZE];
    UINT1               au1TmpPortList[PNAC_PORTLIST_LEN];
    UINT1               u1isShowAll = TRUE;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    tSNMP_OCTET_STRING_TYPE CurrentName;
    tSNMP_OCTET_STRING_TYPE NextName;
    tSNMP_OCTET_STRING_TYPE Password;
    tSNMP_OCTET_STRING_TYPE PortList;

    PNAC_MEMSET (au1TmpCurrName, 0, PNAC_CLI_NAME_SIZE);
    PNAC_MEMSET (au1TmpNextName, 0, PNAC_CLI_NAME_SIZE);
    PNAC_MEMSET (ai1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    PNAC_MEMSET (au1TmpPassword, 0, PNAC_CLI_NAME_SIZE);

    CurrentName.pu1_OctetList = (UINT1 *) &au1TmpCurrName;
    NextName.pu1_OctetList = (UINT1 *) &au1TmpNextName;
    PortList.pu1_OctetList = (UINT1 *) &au1TmpPortList;
    Password.pu1_OctetList = (UINT1 *) &au1TmpPassword;

    if (nmhGetFirstIndexFsPnacASUserConfigTable (&NextName) == SNMP_SUCCESS)
    {
        do
        {
            nmhGetFsPnacASUserConfigRowStatus (&NextName, &i4RowStatus);
            if (i4RowStatus == PNAC_ACTIVE)
            {
                nmhGetFsPnacASUserConfigPermission (&NextName, &i4Permission);

                nmhGetFsPnacASUserConfigPassword (&NextName, &Password);

                nmhGetFsPnacASUserConfigAuthTimeout (&NextName,
                                                     (UINT4 *) &u4TimeOut);
                PNAC_MEMSET (PortList.pu1_OctetList, 0, PNAC_PORTLIST_LEN);
                nmhGetFsPnacASUserConfigPortList (&NextName, &PortList);
            }
            CliPrintf (CliHandle, "%s ", "dot1x local-database");

            CliPrintf (CliHandle, "%s ", NextName.pu1_OctetList);

            CliPrintf (CliHandle, "%s", "password");

            CliPrintf (CliHandle, " %s", "******");

            CliPrintf (CliHandle, " %s", "permission");
            if (i4Permission == 1)
            {
                CliPrintf (CliHandle, " %s", "allow");
            }
            else if (i4Permission == 2)
            {
                CliPrintf (CliHandle, " %s", "deny");
            }
            if (u4TimeOut)
            {
                CliPrintf (CliHandle, " %s", "auth-timeout");

                CliPrintf (CliHandle, " %u", u4TimeOut);
            }
            CliConfOctetToIfName (CliHandle,
                                  " interface ", NULL,
                                  &PortList, &u4PagingStatus);

            CliPrintf (CliHandle, "\r\n");

            PNAC_MEMSET (CurrentName.pu1_OctetList, 0, PNAC_CLI_NAME_SIZE);
            PNAC_MEMCPY (CurrentName.pu1_OctetList, NextName.pu1_OctetList,
                         NextName.i4_Length);
            CurrentName.i4_Length = NextName.i4_Length;
            PNAC_MEMSET (NextName.pu1_OctetList, 0, PNAC_CLI_NAME_SIZE);

            if (nmhGetNextIndexFsPnacASUserConfigTable (&CurrentName, &NextName)
                == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            if (u4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt,
                 * no more print required, exit
                 */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : PnacShowAuthSmState                                */
/*                                                                           */
/*     DESCRIPTION      : This function displays authenticator State         */
/*                                                                           */
/*     INPUT            : i4AuthSMState: Authenticator State                 */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
PnacShowAuthSmState (tCliHandle CliHandle, INT4 i4AuthSMState)
{
    switch (i4AuthSMState)
    {
        case PNAC_ASM_STATE_INITIALIZE:
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "AuthSM State",
                       "INITIALIZE");
            break;

        case PNAC_ASM_STATE_DISCONNECTED:
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "AuthSM State",
                       "DISCONNECTED");
            break;

        case PNAC_ASM_STATE_CONNECTING:
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "AuthSM State",
                       "CONNECTING");
            break;

        case PNAC_ASM_STATE_AUTHENTICATING:
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "AuthSM State",
                       "AUTHENTICATING");
            break;

        case PNAC_ASM_STATE_AUTHENTICATED:
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "AuthSM State",
                       "AUTHENTICATED");
            break;

        case PNAC_ASM_STATE_ABORTING:
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "AuthSM State",
                       "ABORTING");
            break;

        case PNAC_ASM_STATE_HELD:
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "AuthSM State", "HELD");
            break;

        case PNAC_ASM_STATE_FORCEAUTH:
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "AuthSM State",
                       "FORCE AUTHORIZED");
            break;

        case PNAC_ASM_STATE_FORCEUNAUTH:
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "AuthSM State",
                       "FORCE UNAUTHORIZED");
            break;

        default:
            break;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacShowMacStats                                   */
/*                                                                           */
/*     DESCRIPTION      : This function shows pnac auth session statistics   */
/*                                                                           */
/*     INPUT            : CliHandle: Cli Handler                             */
/*                      : pSrcMacAddr: Source Mac Address                    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
PnacShowMacStats (tCliHandle CliHandle, UINT1 *pSrcMacAddr)
{

    INT4                i4RetVal = 0;
    INT1                ai1MacBuf[PNAC_CLI_NAME_SIZE];
    tSNMP_OCTET_STRING_TYPE *SessionId = NULL;
    tSNMP_OCTET_STRING_TYPE *Name = NULL;

    SessionId = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (PNAC_SESSION_ID_STRING_SIZE);

    if (SessionId == NULL)
    {
        return CLI_FAILURE;
    }

    Name =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (PNAC_CLI_NAME_SIZE);

    if (Name == NULL)
    {
        free_octetstring (SessionId);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\r\nPnac Mac Session Statistics\r\n");
    CliPrintf (CliHandle, "-----------------------------------\r\n");

    PNAC_MEMSET (ai1MacBuf, 0, PNAC_CLI_NAME_SIZE);
    /* Source MAC Address of the last received Eapol Frame */
    PrintMacAddress (pSrcMacAddr, (UINT1 *) ai1MacBuf);

    CliPrintf (CliHandle, "\r\n%-15s = %s\r\n", "Supplicant Mac-Addr",
               ai1MacBuf);
    /*Frames Rx */
    nmhGetFsPnacAuthSessionFramesRx (pSrcMacAddr, (UINT4 *) &i4RetVal);
    CliPrintf (CliHandle, "\r\n%-15s = %d\r\n", "Frames Received:", i4RetVal);

    /*Frames Tx */
    nmhGetFsPnacAuthSessionFramesTx (pSrcMacAddr, (UINT4 *) &i4RetVal);
    CliPrintf (CliHandle, "\r\n%-15s = %d\r\n", "Frames Transmitted:",
               i4RetVal);

    /*Session id */
    CLI_MEMSET (SessionId->pu1_OctetList, 0, PNAC_SESSION_ID_STRING_SIZE);
    nmhGetFsPnacAuthSessionId (pSrcMacAddr, SessionId);
    CliPrintf (CliHandle, "\r\n%-15s = %s\r\n", "Session Id:",
               SessionId->pu1_OctetList);

    /*Terminate cause */
    nmhGetFsPnacAuthSessionTerminateCause (pSrcMacAddr, &i4RetVal);
    switch (i4RetVal)
    {
        case 1:
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "Session Terminate Cause:",
                       "Supplicant logoff");
            break;
        case 2:
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "Session Terminate Cause:",
                       "Port Failure");
            break;
        case 3:
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "Session Terminate Cause:",
                       "Supplicant Restart");
            break;
        case 4:
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "Session Terminate Cause:",
                       "ReAuth Failed");
            break;
        case 5:
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "Session Terminate Cause:",
                       "Auth Control is Force UnAuthorized");
            break;
        case 6:
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "Session Terminate Cause:",
                       "Port ReInit");
            break;
        case 7:
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "Session Terminate Cause:",
                       "Port Admin Disabled");
            break;
        case 999:
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "Session Terminate Cause:",
                       "Not Terminated Yet");
            break;
        default:
            CliPrintf (CliHandle, "\r\n%-20s  = %s", "Session Terminate Cause:",
                       "Unknown");
            break;
    }
    /*user name */
    CLI_MEMSET (Name->pu1_OctetList, 0, PNAC_CLI_NAME_SIZE);
    nmhGetFsPnacAuthSessionUserName (pSrcMacAddr, Name);
    CliPrintf (CliHandle, "\r\n%-15s = %s\r\n", "User Name:",
               Name->pu1_OctetList);
    free_octetstring (Name);
    free_octetstring (SessionId);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacShowAllMacStats                                */
/*                                                                           */
/*     DESCRIPTION      : This function shows pnac auth session statistics   */
/*                                                                           */
/*     INPUT            : CliHandle: Cli Handler                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
PnacShowAllMacStats (tCliHandle CliHandle)
{

    tMacAddr            NextAddr;
    tMacAddr            CurrentAddr;
    INT4                i4OutCome;
    INT4                i4RetVal = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               u1isShowAll = TRUE;
    INT1                ai1MacBuf[PNAC_CLI_NAME_SIZE];
    tSNMP_OCTET_STRING_TYPE *SessionId = NULL;
    tSNMP_OCTET_STRING_TYPE *Name = NULL;
    MEMSET (&NextAddr, 0, sizeof (tMacAddr));
    i4OutCome =
        (INT4) (nmhGetFirstIndexFsPnacAuthSessionStatsTable (&NextAddr));

    if (i4OutCome == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    SessionId = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (PNAC_SESSION_ID_STRING_SIZE);

    if (SessionId == NULL)
    {
        return CLI_FAILURE;
    }

    Name =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (PNAC_CLI_NAME_SIZE);

    if (Name == NULL)
    {
        free_octetstring (SessionId);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\r\nPnac Mac Session Statistics\r\n");
    CliPrintf (CliHandle, "-----------------------------------\r\n");
    do
    {
        /* supp addr */
        PNAC_MEMSET (ai1MacBuf, 0, PNAC_CLI_NAME_SIZE);
        /* Source MAC Address of the last received Eapol Frame */
        PrintMacAddress (NextAddr, (UINT1 *) ai1MacBuf);

        CliPrintf (CliHandle, "\r\n%-15s = %s\r\n", "Supplicant Mac-Addr:",
                   ai1MacBuf);

        /*Frames Rx */
        nmhGetFsPnacAuthSessionFramesRx (NextAddr, (UINT4 *) &i4RetVal);
        CliPrintf (CliHandle, "\r\n%-15s = %d\r\n", "Frames Received:",
                   i4RetVal);

        /*Frames Tx */
        nmhGetFsPnacAuthSessionFramesTx (NextAddr, (UINT4 *) &i4RetVal);
        CliPrintf (CliHandle, "\r\n%-15s = %d\r\n", "Frames Transmitted:",
                   i4RetVal);

        /*Session id */
        CLI_MEMSET (SessionId->pu1_OctetList, 0, PNAC_SESSION_ID_STRING_SIZE);
        nmhGetFsPnacAuthSessionId (NextAddr, SessionId);
        CliPrintf (CliHandle, "\r\n%-15s = %s\r\n", "Session Id:",
                   SessionId->pu1_OctetList);

        /*Terminate cause */
        nmhGetFsPnacAuthSessionTerminateCause (NextAddr, &i4RetVal);
        switch (i4RetVal)
        {
            case 1:
                CliPrintf (CliHandle, "\r\n%-20s  = %s",
                           "Session Terminate Cause:", "Supplicant logoff");
                break;
            case 2:
                CliPrintf (CliHandle, "\r\n%-20s  = %s",
                           "Session Terminate Cause:", "Port Failure");
                break;
            case 3:
                CliPrintf (CliHandle, "\r\n%-20s  = %s",
                           "Session Terminate Cause:", "Supplicant Restart");
                break;
            case 4:
                CliPrintf (CliHandle, "\r\n%-20s  = %s",
                           "Session Terminate Cause:", "ReAuth Failed");
                break;
            case 5:
                CliPrintf (CliHandle, "\r\n%-20s  = %s",
                           "Session Terminate Cause:",
                           "Auth Control is Force UnAuthorized");
                break;
            case 6:
                CliPrintf (CliHandle, "\r\n%-20s  = %s",
                           "Session Terminate Cause:", "Port ReInit");
                break;
            case 7:
                CliPrintf (CliHandle, "\r\n%-20s  = %s",
                           "Session Terminate Cause:", "Port Admin Disabled");
                break;
            case 999:
                CliPrintf (CliHandle, "\r\n%-20s  = %s",
                           "Session Terminate Cause:", "Not Terminated Yet");
                break;
            default:
                CliPrintf (CliHandle, "\r\n%-20s  = %s",
                           "Session Terminate Cause:", "Unknown");
                break;
        }
        /*user name */
        CLI_MEMSET (Name->pu1_OctetList, 0, PNAC_CLI_NAME_SIZE);
        nmhGetFsPnacAuthSessionUserName (NextAddr, Name);
        CliPrintf (CliHandle, "\r\n%-15s = %s\r\n", "User Name:",
                   Name->pu1_OctetList);
        CLI_MEMCPY (CurrentAddr, NextAddr, 6);
        u4PagingStatus = (UINT4) (CliPrintf (CliHandle,
                                             "----------------------------------------------------------\r\n"));

        i4OutCome =
            (INT4) (nmhGetNextIndexFsPnacAuthSessionStatsTable
                    (CurrentAddr, &NextAddr));

        if (i4OutCome == SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
        }
        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            u1isShowAll = FALSE;
        }

        /* ask the user whether to continue or not */

    }
    while (u1isShowAll);

    free_octetstring (Name);
    free_octetstring (SessionId);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacClearStatsIfInfo                               */
/*                                                                           */
/*     DESCRIPTION      : This function clears Auth Interface statistics     */
/*                                                                           */
/*     INPUT            : CliHandle: Cli Handler                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
PnacClearStatsIfInfo (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode;

    if (SNMP_FAILURE == nmhTestv2FsPnacPaePortStatisticsClear (&u4ErrorCode,
                                                               (INT4) u4IfIndex,
                                                               PNAC_ENABLED))
    {
        CLI_SET_ERR (CLI_PNAC_CLEAR_IF_STATISTICS_FAILED);
        return CLI_FAILURE;
    }
    if (SNMP_FAILURE == nmhSetFsPnacPaePortStatisticsClear (u4IfIndex,
                                                            PNAC_ENABLED))
    {
        CLI_SET_ERR (CLI_PNAC_CLEAR_IF_STATISTICS_FAILED);
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacClearAllStats                                  */
/*                                                                           */
/*     DESCRIPTION      : This function clears both Interface/MAC            */
/*                               statistics                                  */
/*                                                                           */
/*     INPUT            : CliHandle: Cli Handler                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PnacClearAllStats (tCliHandle CliHandle)
{
    INT1                i1OutCome = 0;
    INT4                i4RetVal = 0;
    INT4                i4NextPortNumber = 0;
    INT4                i4CurrentPortNumber = 0;
    tMacAddr            NextAddr;
    tMacAddr            CurrentAddr;

    MEMSET (&NextAddr, 0, sizeof (tMacAddr));
    MEMSET (&CurrentAddr, 0, sizeof (tMacAddr));

    /*clearing the Interface statistics */
    i1OutCome = nmhGetFirstIndexFsPnacPaePortTable (&i4NextPortNumber);
    while (i1OutCome != SNMP_FAILURE)
    {
        i4CurrentPortNumber = i4NextPortNumber;
        i4RetVal =
            PnacClearStatsIfInfo (CliHandle, (UINT4) i4CurrentPortNumber);
        if (i4RetVal == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
        i1OutCome = nmhGetNextIndexFsPnacPaePortTable (i4CurrentPortNumber,
                                                       &i4NextPortNumber);
    }
    /* Clearing the MAC statistics */
    i1OutCome = nmhGetFirstIndexFsPnacAuthSessionStatsTable (&NextAddr);
    while (i1OutCome != SNMP_FAILURE)
    {
        CLI_MEMCPY (CurrentAddr, NextAddr, sizeof (tMacAddr));
        i4RetVal = PnacClearStatsMacInfo (CliHandle, (UINT1 *) &CurrentAddr);
        if (i4RetVal == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
        i1OutCome = nmhGetNextIndexFsPnacAuthSessionStatsTable
            (CurrentAddr, &NextAddr);

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacClearStatsMacInfo                              */
/*                                                                           */
/*     DESCRIPTION      : This function clears MAC statistics                */
/*                                                                           */
/*     INPUT            : CliHandle: Cli Handler                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PnacClearStatsMacInfo (tCliHandle CliHandle, UINT1 *pu1MacAddr)
{
    UINT4               u4ErrorCode;

    if (SNMP_FAILURE == nmhTestv2FsPnacAuthSessionStatisticsClear (&u4ErrorCode,
                                                                   pu1MacAddr,
                                                                   PNAC_ENABLED))
    {
        CLI_SET_ERR (CLI_PNAC_CLEAR_MAC_STATISTICS_FAILED);
        return CLI_FAILURE;
    }

    if (SNMP_FAILURE == nmhSetFsPnacAuthSessionStatisticsClear (pu1MacAddr,
                                                                PNAC_ENABLED))
    {
        CLI_SET_ERR (CLI_PNAC_CLEAR_MAC_STATISTICS_FAILED);
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacClearStatsIfMacInfo                            */
/*                                                                           */
/*     DESCRIPTION      : This function clears MACstatistics for the         */
/*                        particular interface                              */
/*     INPUT            : CliHandle: Cli Handler                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PnacClearStatsIfMacInfo (tCliHandle CliHandle, UINT4 u4IfIndex,
                         UINT1 *pu1MacAddr)
{
    tPnacAuthSessionNode *pSessNode = NULL;

    if (PnacAuthGetSessionTblEntryByMac (pu1MacAddr, &pSessNode) !=
        PNAC_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (pSessNode != NULL)
    {
        if (pSessNode->u2PortNo == u4IfIndex)
        {
            PnacClearStatsMacInfo (CliHandle, pu1MacAddr);
        }
        else
        {
            CLI_SET_ERR (CLI_PNAC_NO_ENTRY_MAC_IF);
            return CLI_FAILURE;
        }
    }
    else
    {
        CLI_SET_ERR (CLI_PNAC_CLEAR_MAC_STATISTICS_FAILED);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacSetPortAuthStatus                              */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables PNAC on a port      */
/*                                                                           */
/*     INPUT            : CliHandle: Cli Handler                             */
/*                        u4IfIndex - Port Index                             */
/*                        i4PortAuthStatus - Authorization Status            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSetPortAuthStatus (tCliHandle CliHandle, UINT4 u4IfIndex,
                       INT4 i4PortAuthStatus)
{
    UINT4               u4ErrorCode = 0;
    if (SNMP_FAILURE == nmhTestv2FsPnacPaePortAuthStatus (&u4ErrorCode,
                                                          u4IfIndex,
                                                          i4PortAuthStatus))
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsPnacPaePortAuthStatus (u4IfIndex, i4PortAuthStatus) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PnacSetPortAuthReAuthMax                           */
/*                                                                           */
/*     DESCRIPTION      : PnacSetPortAuthReAuthMax                           */
/*                                                                           */
/*     INPUT            : CliHandle: Cli Handler                             */
/*                        u4IfIndex - Port Index                             */
/*                        u4PnacPaeAuthReAuthMax - ReAuthMax Value           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSetPortAuthReAuthMax (tCliHandle CliHandle, UINT4 u4IfIndex,
                          UINT4 u4PnacPaeAuthReAuthMax)
{
    UINT4               u4ErrorCode = 0;

    if (SNMP_FAILURE == nmhTestv2FsPnacPaeAuthReAuthMax (&u4ErrorCode,
                                                         (INT4) u4IfIndex,
                                                         u4PnacPaeAuthReAuthMax))
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsPnacPaeAuthReAuthMax
        ((INT4) u4IfIndex, (INT4) u4PnacPaeAuthReAuthMax) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

#endif
