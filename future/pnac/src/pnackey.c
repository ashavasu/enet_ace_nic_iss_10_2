/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/* $Id: pnackey.c,v 1.9 2014/01/31 12:55:33 siva Exp $                       */
/*****************************************************************************/
/*    FILE  NAME            : pnackey.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : PNAC Key sub module                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Mar 2002                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains Key Sub module functions    */
/*                            of PNAC module                                 */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 MAR 2002 / BridgeTeam   Initial Create.                     */
/*---------------------------------------------------------------------------*/

#include "pnachdrs.h"

/*****************************************************************************/
/* Function Name      : PnacKeyAuthTxKey                                     */
/*                                                                           */
/* Description        : This function transfers the Key to the Supplicant.   */
/*                                                                           */
/* Input(s)           : pServerKey - Start address of Key,                   */
/*                      u2KeyLenght - Key length,                            */
/*                      u2PortNumber - Port number,                          */
/*                      DestMacAddr - Supplicant MAC address                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : Port number in gPnacSystemInfo,                      */
/*                      KeyTxEnabled in gPnacSystemInfo,                     */
/*                      InterfaceType in gPnacSytemInfo,                     */
/*                      InterfaceIndex in gPnacSystemInfo.                   */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
PnacKeyAuthTxKey (UINT1 *pServerKey,
                  UINT2 u2KeyLength, UINT2 u2PortNum, tMacAddr DestMacAddr)
{

    tPnacKeyInfo        keyInfo;
    tPnacCruBufChainHdr *pCruBuf = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacKeyDesc        eapKeyDesc;
    UINT1              *pu1KeyEapol = NULL;
    UINT1              *pu1KeyDescBuf = NULL;
    UINT2               u2BodyLength = PNAC_INIT_VAL;
    UINT2               u2PktLen = PNAC_INIT_VAL;

    /* Check if Key Transmission is enabled for this port */

    pPortInfo = &(PNAC_PORT_INFO (u2PortNum));

    if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).bKeyTxEnabled == PNAC_FALSE)
    {
        /* Key transmission is disabled */
        /* Hence returning without the Authenticator transmitting the Key */

        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_DATA_PATH_TRC,
                  " KEY: Key transmission disabled for this Port\n");
        return PNAC_SUCCESS;
    }
    if (PNAC_ALLOCATE_ENET_EAP_ARRAY_MEMBLK (pu1KeyEapol) == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Unable to allocate memory. \r\n");
        return PNAC_FAILURE;
    }

    /* Initializing KeyEapol frame */
    PNAC_MEMSET (pu1KeyEapol, PNAC_INIT_VAL, PNAC_MAX_ENET_FRAME_SIZE);

    /* Give an offset in 'pu1KeyEapol' before filling Key descriptor */
    pu1KeyDescBuf = (pu1KeyEapol + PNAC_ENET_EAPOL_HDR_SIZE);

    /* Form the Key Descriptor in the allocated array :
     * (InitVector, KeySignature, ReplayCounter, KeyDescriptor type, 
     * KeyLength, KeySignature and KeyIndex).
     */
    PNAC_MEMSET (&eapKeyDesc, PNAC_INIT_VAL, (sizeof (eapKeyDesc)));
    PNAC_FILL_KEY_DESC (pServerKey, u2KeyLength, u2PortNum,
                        DestMacAddr, &eapKeyDesc);

    PnacKeyBuildEapKeyPkt (&eapKeyDesc, pu1KeyDescBuf, &u2BodyLength);

    /* Size of Eapol frame's body */

    PnacPaeConstructEapolHdr (pu1KeyEapol, DestMacAddr,
                              PNAC_EAPOL_KEY, u2BodyLength, u2PortNum,
                              OSIX_FALSE);

    /* Total size of built KeyEapol frame */
    u2PktLen = (UINT2) (PNAC_ENET_EAPOL_HDR_SIZE + u2BodyLength);

    /* Allocate a CRU buffer message chain for this KeyEapol frame */
    if ((pCruBuf = PNAC_ALLOC_CRU_BUF (u2PktLen, PNAC_OFFSET_NONE)) == NULL)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_DATA_PATH_TRC,
                  " KEY: CRU buffer allocation for KeyFrame failed\n");
        if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1KeyEapol) == MEM_FAILURE)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      "Failed to release  memory. \r\n");
        }
        return PNAC_FAILURE;
    }

    /* Copy the built KeyEapol frame into the CRU buffer */
    if (PNAC_COPY_OVER_CRU_BUF (pCruBuf, pu1KeyEapol, PNAC_OFFSET_NONE,
                                u2PktLen) == PNAC_CRU_FAILURE)
    {
        PNAC_RELEASE_CRU_BUF (pCruBuf, PNAC_CRU_FALSE);

        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_DATA_PATH_TRC,
                  " KEY: Copy Over CRU Buffer Failed\n");
        if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1KeyEapol) == MEM_FAILURE)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      "Failed to release  memory. \r\n");
        }
        return PNAC_FAILURE;
    }

    if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1KeyEapol) == MEM_FAILURE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Failed to release  memory. \r\n");
    }

    PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_CONTROL_PATH_TRC,
              " KEY: Key to be transmitted on WLAN port\n");
    /* send the keys to wLAN driver */
    PNAC_MEMSET (&keyInfo, PNAC_INIT_VAL, sizeof (tPnacKeyInfo));
    keyInfo.u2KeyLength = u2KeyLength;

    PNAC_MEMCPY (keyInfo.au1Key, pServerKey, u2KeyLength);
    if (pPortInfo->portIfMap.u1IfType == PNAC_WIRELESSLAN_PORT)
    {

        PNAC_TX_KEY_TO_WLAN (&keyInfo, DestMacAddr, u2PortNum);
    }

    /* Transmit out to CFA, the KeyEapol frame */
    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolFramesTx++;

    if (PnacHandOverOutFrame (pCruBuf, u2PortNum,
                              u2PktLen, PNAC_PAE_ENET_TYPE,
                              PNAC_ENCAP_NONE) != PNAC_SUCCESS)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_DATA_PATH_TRC,
                  " KEY: Transmit of KeyEapol frame failed\n");
        return PNAC_FAILURE;
    }

    PNAC_TRC (PNAC_DATA_PATH_TRC,
              " KEY: Authenticator Transmitted Key Successfully\n");
    return PNAC_SUCCESS;

}                                /* PnacKeyAuthTxKey ends */

/*****************************************************************************/
/* Function Name      : PnacKeyRxKey                                         */
/*                                                                           */
/* Description        : This function receives the Eap-Key pkt coming from   */
/*                      remote Authenticator or Supplicant and passes it to  */
/*                      encryption-decryption module.                        */
/*                                                                           */
/* Input(s)           : pEapKeyPkt - Start address of Eap-Key pkt,           */
/*                      u2PortNumber - Port number,                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : Port number in gPnacSystemInfo.                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : Supplicant key info in gPnacSytemInfo.               */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
PnacKeyRxKey (UINT1 *pEapKeyPkt, UINT2 u2PortNumber)
{
    UINT1              *pu1Ptr = NULL;
    UINT1               u1KeyDescType = PNAC_INIT_VAL;
    UINT2               u2KeyLength = PNAC_INIT_VAL;

    /* Extract Key from the EapKey pkt */
    pu1Ptr = (UINT1 *) pEapKeyPkt;
    pu1Ptr += PNAC_ENET_EAPOL_HDR_SIZE;
    PNAC_GET_1BYTE (u1KeyDescType, pu1Ptr);
    PNAC_GET_2BYTE (u2KeyLength, pu1Ptr);
    if (u2KeyLength > PNAC_MAX_SERVER_KEY_SIZE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_DATA_PATH_TRC |
                  PNAC_CONTROL_PATH_TRC,
                  " KEY: Key received is longer than the storeable size\n");
        return PNAC_FAILURE;
    }
    pu1Ptr = ((UINT1 *) pEapKeyPkt) +
        PNAC_ENET_EAPOL_HDR_SIZE + PNAC_KEYDESC_HDR_SIZE;
    /* Now the 'pu1Ptr' points to start of the Key */
    /* pass the key to encrypt-decrypt sub module */
    PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_CONTROL_PATH_TRC,
              " KEY: Forwarding Key to Encrypt-Decrypt Module ...\n");
    PNAC_STORE_KEY (pu1Ptr, u2KeyLength, u2PortNumber);
    PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_CONTROL_PATH_TRC,
              " KEY: Key Forwarded...\n");
    UNUSED_PARAM (u1KeyDescType);
    return PNAC_SUCCESS;
}                                /* PnacKeyRxKey ends */

/*****************************************************************************/
/* Function Name      : PnacKeyBuildEapKeyPkt                                */
/*                                                                           */
/* Description        : This function builds the EAP Key descriptor Pkt      */
/*                                                                           */
/* Input(s)           : pu1KeyDescBuf - Start address of Key descriptor,     */
/*                                                                           */
/* Output(s)          : pu1Buf - Pointer to the buffer in which key pkt is   */
/*                               constructed.                                */
/*                      pu2PktLen - Length of Key packet formed              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
PnacKeyBuildEapKeyPkt (tPnacKeyDesc * pEapKeyDesc,
                       UINT1 *pu1Buf, UINT2 *pu2PktLen)
{
    UINT2               u2Val = 0;
    UINT1               u1Val = 0;
    UINT1              *pu1Ptr = NULL;

    pu1Ptr = pu1Buf;

    /* Form the Key Descriptor as given below:
     * 
     * Descriptor Type (1)
     * Key Length      (2-3)
     * Replay Counter  (4-11)
     * Key IV          (12-27)
     * Key Index       (28)
     * Key Signature   (29-44)
     * Key             (48-Packet Body Length)
     * 
     */

    /* Descriptor type should be PNAC_RC4_KEY_DESC */
    u1Val = pEapKeyDesc->u1DescType;
    PNAC_PUT_1BYTE (pu1Ptr, u1Val);

    /* Key length */
    u2Val = pEapKeyDesc->u2KeyLength;
    PNAC_PUT_2BYTE (pu1Ptr, u2Val);

    PNAC_MEMCPY (pu1Ptr, &(pEapKeyDesc->au1ReplayCounter[0]),
                 PNAC_KEY_REPLAY_LEN);
    pu1Ptr += PNAC_KEY_REPLAY_LEN;

    PNAC_MEMCPY (pu1Ptr, &(pEapKeyDesc->au1InitVector[0]), PNAC_KEY_VECTOR_LEN);
    pu1Ptr += PNAC_KEY_VECTOR_LEN;

    u1Val = pEapKeyDesc->u1KeyIndex;
    PNAC_PUT_1BYTE (pu1Ptr, u1Val);

    PNAC_MEMCPY (pu1Ptr, &(pEapKeyDesc->au1KeySignature[0]),
                 PNAC_KEY_SIGNATURE_LEN);
    pu1Ptr += PNAC_KEY_SIGNATURE_LEN;

    /* copying Key */
    PNAC_MEMCPY (pu1Ptr, &(pEapKeyDesc->au1Key[0]), pEapKeyDesc->u2KeyLength);
    pu1Ptr += pEapKeyDesc->u2KeyLength;

    *pu2PktLen = (UINT2) (pEapKeyDesc->u2KeyLength + PNAC_KEYDESC_HDR_SIZE);

    return PNAC_SUCCESS;

}                                /* PnacKeyBuildEapKeyPkt ends */
