/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/* $Id: pnacintf.c,v 1.68 2015/06/11 10:03:16 siva Exp $                     */
/*****************************************************************************/
/*    FILE  NAME            : pnacintf.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : PNAC Timer sub module                          */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Mar 2002                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains Timer Sub module functions  */
/*                            of PNAC module                                 */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 MAR 2002 / BridgeTeam   Initial Create.                     */
/*---------------------------------------------------------------------------*/

#include "pnachdrs.h"

#ifdef SNMP_2_WANTED
#include "fspnacwr.h"
#include "stdpnawr.h"
#endif
#ifdef RSNA_WANTED
#include "rsna.h"
#endif
#include "mux.h"

/*****************************************************************************/
/* Function Name      : PnacInitQueue                                        */
/*                                                                           */
/* Description        : This function creates the queue and the mempool for  */
/*                      interface message.                                   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacIntfInputQId                                    */
/*                      gPnacIntfMesgMemPoolId                               */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS/ PNAC_FAILURE                           */
/*****************************************************************************/
INT4
PnacInitQueue (VOID)
{
    tPnacQId            inputQId;
    tPnacQId            cfgQId;

    /* Create the arrival Q */
    if (PNAC_CREATE_QUEUE (PNAC_INTF_QUEUE_NAME,
                           OSIX_MAX_Q_MSG_LEN,
                           PNAC_INTF_QUEUE_DEPTH, &inputQId) != OSIX_SUCCESS)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                  PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC,
                  " INF: Queue Creation Failure !!!\n");
        return PNAC_FAILURE;
    }

    PNAC_INTF_INPUT_QID = inputQId;

    /* Create the arrival Q */
    if (PNAC_CREATE_QUEUE (PNAC_INTF_CFGQ_NAME,
                           OSIX_MAX_Q_MSG_LEN,
                           PNAC_INTF_CFGQ_DEPTH, &cfgQId) != OSIX_SUCCESS)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                  PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC,
                  " CFG: Queue Creation Failure !!!\n");
        return PNAC_FAILURE;
    }

    PNAC_INTF_CFG_QID = cfgQId;

    return PNAC_SUCCESS;
}                                /* End of PnacInterfaceTaskStart() */

/*****************************************************************************/
/* Function Name      : PnacQandSemDeinit                              */
/*                                                                           */
/* Description        : This function deletes the Interface Task and a       */
/*                      Queue Created for it                                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.intfTaskId                           */
/*                      gPnacSystemInfo.intfInputQId                         */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSystemInfo.intfTaskId                           */
/*                      gPnacSystemInfo.intfInputQId                         */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacQandSemDeinit (VOID)
{
    tPnacIntfMesg      *pMesg = NULL;
    tPnacCruBufChainHdr *pBuf;

    /* Delete PNAC Interface Task */
    PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
              " INF: Deinitializing Interface Task...\n");

    if (gPnacIntfInputQId != PNAC_INIT_VAL)
    {
        /* Remove all the message from the Interface Queue
         * and then delete it.
         */
        while (PNAC_RECV_FROM_QUEUE (PNAC_INTF_INPUT_QID,
                                     (UINT1 *) &pMesg,
                                     OSIX_DEF_MSG_LEN,
                                     OSIX_NO_WAIT) == OSIX_SUCCESS)
        {
            PNAC_TRC (PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
                      " INF: Message dequeued for Releasing ...\n");

            if (pMesg == NULL)
                continue;

            switch (pMesg->u4MesgType)
            {

                case PNAC_EAPOL_FRAME:
                    pBuf =
                        (tPnacCruBufChainHdr *) pMesg->Mesg.EapolFrame.pFrame;
                    if (pBuf == NULL)
                    {
                        continue;
                    }

                    PNAC_RELEASE_CRU_BUF (pBuf, PNAC_CRU_FALSE);
                    break;

                case PNAC_AUTHSERVER_RESP:

                    PNAC_TRC (PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
                              " INF: Received response from Radius...\n");

                    pBuf =
                        (tPnacCruBufChainHdr *) pMesg->Mesg.AuthSrvResp.pFrame;

                    if (pBuf == NULL)
                    {
                        continue;
                    }
                    PNAC_RELEASE_CRU_BUF (pBuf, PNAC_FALSE);

                    break;
            }

            PNAC_RELEASE_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID, pMesg);

            PNAC_TRC (PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
                      " INF: Message Releasing Done ...\n");
        }

        PNAC_DELETE_QUEUE (PNAC_INTF_INPUT_QID);
        PNAC_INTF_INPUT_QID = PNAC_INIT_VAL;

        PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_OS_RESOURCE_TRC
                  | PNAC_CONTROL_PATH_TRC, " INF: Interface Q deleted...\n");
    }

    if (PNAC_EAPOL_MESG_MEMPOOL_ID != PNAC_INIT_VAL)
    {
        PNAC_RELEASE_MEM_BLOCK (PNAC_EAPOL_MESG_MEMPOOL_ID, gpu1EapolBuffer);
    }

    if (gPnacIntfCfgQId != PNAC_INIT_VAL)
    {

        while (PNAC_RECV_FROM_QUEUE (PNAC_INTF_CFG_QID,
                                     (UINT1 *) &pMesg,
                                     OSIX_DEF_MSG_LEN,
                                     OSIX_NO_WAIT) == OSIX_SUCCESS)
        {
            PNAC_TRC (PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
                      "CFGQ: Message received from Queue ... Release...\n");

            if (pMesg == NULL)
                continue;

            switch (pMesg->u4MesgType)
            {

#ifdef MBSM_WANTED
                case MBSM_MSG_CARD_INSERT:
                case MBSM_MSG_CARD_REMOVE:
                    MEM_FREE (pMesg->Mesg.MbsmCardUpdate.pMbsmProtoMsg);
                    break;
#endif

#ifdef L2RED_WANTED
                case PNAC_RM_MSG:

                    if ((pMesg->Mesg.RmPnacMsg.u1SubMsgType == RM_MESSAGE)
                        && (pMesg->Mesg.RmPnacMsg.pData != NULL))
                    {
                        RM_FREE ((tRmMsg *) (pMesg->Mesg.RmPnacMsg.pData));
                    }
                    else if (((pMesg->Mesg.RmPnacMsg.u1SubMsgType
                               == RM_STANDBY_UP) ||
                              (pMesg->Mesg.RmPnacMsg.u1SubMsgType
                               == RM_STANDBY_DOWN)) &&
                             (pMesg->Mesg.RmPnacMsg.pData != NULL))
                    {
                        PnacRmReleaseMemoryForMsg
                            ((UINT1 *) pMesg->Mesg.RmPnacMsg.pData);
                    }

                    break;
#endif

            }

            PNAC_RELEASE_MEM_BLOCK (PNAC_INTF_CFGQ_MESG_MEMPOOL_ID, pMesg);

            PNAC_TRC (PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
                      " CFG: Message Releasing Done ...\n");
        }

        PNAC_DELETE_QUEUE (PNAC_INTF_CFG_QID);

        PNAC_INTF_CFG_QID = PNAC_INIT_VAL;

        PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_OS_RESOURCE_TRC
                  | PNAC_CONTROL_PATH_TRC, "CFG: Queue deleted...\n");
    }

    if (gPnacSemId != PNAC_INIT_VAL)
    {
        PNAC_DELETE_SEM (PNAC_SEM_ID);
    }

    PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_ALL_FAILURE_TRC |
              PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
              " INF: Interface task deinitialization...Done\n");
    return;
}

/*****************************************************************************/
/* Function Name      : PnacInterfaceTaskMain                                */
/*                                                                           */
/* Description        : This function is the entry point function of the     */
/*                      PNAC Interface Task. This function receives an event */
/*                      from 1) the FSAP Timer Task on expiry of a timer.    */
/*                           2) the Radius Client Task when a response is    */
/*                              received from Radius Server.                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacInterfaceTaskMain (INT1 *pi1DummyArg)
{
    UINT4               u4Event = 0;
#ifdef SYSLOG_WANTED
    INT4                i4SysLogId = 0;
#endif

    PNAC_UNUSED (pi1DummyArg);

    gPnacIntfTaskId = OsixGetCurTaskId ();

    if (PnacModuleInit () != PNAC_SUCCESS)
    {
        /* Indicate the status of initialization to the main routine */
        PNAC_RELEASE_MEM_BLOCK (PNAC_EAPOL_MESG_MEMPOOL_ID, gpu1EapolBuffer);
        PNAC_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    if (PNAC_GET_TASK_ID (SELF, PNAC_INTF_TASK_NAME, &(PNAC_INTF_TASK_ID))
        != OSIX_SUCCESS)
    {
        PNAC_RELEASE_MEM_BLOCK (PNAC_EAPOL_MESG_MEMPOOL_ID, gpu1EapolBuffer);
        PnacQandSemDeinit ();
        PNAC_INIT_COMPLETE (OSIX_FAILURE);
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, "Failed to get PNAC Task Id\n");
        return;
    }

#ifdef SYSLOG_WANTED

    /* Register with SysLog Server to Log Error Messages. */
    i4SysLogId = SYS_LOG_REGISTER ((CONST UINT1 *) "PNAC", SYSLOG_ALERT_LEVEL);

    if (i4SysLogId < 0)
    {
        PNAC_RELEASE_MEM_BLOCK (PNAC_EAPOL_MESG_MEMPOOL_ID, gpu1EapolBuffer);
        PnacQandSemDeinit ();
        PNAC_INIT_COMPLETE (OSIX_FAILURE);
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  "Failed to register with SysLog Server\n");
        return;
    }
    gPnacSystemInfo.i4SysLogId = i4SysLogId;
#endif
    gPnacSystemInfo.u1PnacCallSequence = PNAC_PROTOCOL_FLOW;

    /* Indicate the status of initialization to the main routine */
    PNAC_INIT_COMPLETE (OSIX_SUCCESS);

#ifdef SNMP_2_WANTED
    /* Register the protocol MIBs with SNMP */
    RegisterFSPNAC ();
    RegisterSTDPNA ();
#endif

    /* Always Wait For An Event to Occur */
    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
              PNAC_CONTROL_PATH_TRC,
              " INF: Waiting For Event in interface...\n");
    while (1)
    {
        if (PNAC_RECEIVE_EVENT (PNAC_INTF_TASK_ID, PNAC_TIMER_EXP_EVENT |
                                PNAC_QUEUE_EVENT | PNAC_CFGQ_EVENT |
                                PNAC_RED_BULK_UPD_EVENT |
                                PNAC_RSNA_TIMER_EXP_EVENT, OSIX_WAIT,
                                &u4Event) == PNAC_OSIX_SUCCESS)
        {
            PNAC_TRC (PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
                      " INF: Event Received on the Interface, processing ..\n");
            if ((u4Event & PNAC_TIMER_EXP_EVENT) != 0)
            {
                PNAC_LOCK ();

                if (PNAC_IS_ENABLED ())
                {
                    PNAC_TRC (PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
                              " INF: Received Event is from Timer...\n");
                    if (PnacTmrHandler () != PNAC_SUCCESS)
                    {
                        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                                  PNAC_CONTROL_PATH_TRC,
                                  " INF: Timer handler failed !!!\n");
                    }
                }
                else
                {
                    PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC |
                              PNAC_ALL_FAILURE_TRC, "INF: Timer Expired when"
                              " the module is disabled.\n");
                }

                PNAC_UNLOCK ();
            }
            if ((u4Event & PNAC_QUEUE_EVENT) != 0)
            {
                PnacInterfaceHandleQueueEvent ();
            }

            if ((u4Event & PNAC_CFGQ_EVENT) != 0)
            {
                PnacInterfaceHandleCFGQueueEvent ();
            }
#ifdef RSNA_WANTED
            if ((u4Event & PNAC_RSNA_TIMER_EXP_EVENT) != 0)
            {
                PNAC_LOCK ();
                if (PNAC_IS_ENABLED ())
                {
                    RsnaTmrProcessTimer ();
                }
                else
                {
                    PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC |
                              PNAC_ALL_FAILURE_TRC,
                              "INF: Rsna Timer Expired when the module "
                              "is disabled.\n");
                }
                PNAC_UNLOCK ();
            }
#endif

#ifdef L2RED_WANTED
            if ((u4Event & PNAC_RED_BULK_UPD_EVENT) != 0)
            {
                PNAC_LOCK ();
                PnacRedHandleBulkUpdEvent ();
                PNAC_UNLOCK ();
            }
#endif

            PNAC_TRC (PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
                      " INF: Event Processing done...\n");
        }                        /* End of if (PNAC_RECEIVE_EVENT... */
        PNAC_TRC (PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
                  " INF: Waiting For Next Event ...\n");
    }                            /* End of while(1) */

}                                /* PnacInterfaceTaskMain ends */

/*****************************************************************************/
/* Function Name      : PnacInterfaceHandleQueueEvent                        */
/*                                                                           */
/* Description        : This Initiates the Action for the Queue Event. It    */
/*                      receives the information from the buffer received    */
/*                      from the Queue and passes it to the Radius Server    */
/*                      Interface to Process It.                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacInterfaceHandleQueueEvent (VOID)
{
    tPnacDPnacConsPortEntry *pConsPortEntry = NULL;
    tPnacIntfMesg      *pMesg = NULL;
    tPnacCruBufChainHdr *pBuf;
    tPnacAsIfRecvData  *pRadRecvData = NULL;
    tPnacAuthSessionNode *pPnacAuthSessionNode = NULL;
    UINT2               u2PortIndex;
    UINT2               u2PktLen = 0;
    UINT4               u4Event;
    UINT4               u4SlotId = 0;
    UINT1               au1MacAddr [PNAC_MAC_ADDR_SIZE] = {0x01, 0x80, 0xC2, 0x00, 0x00, 0x00};

    if (PNAC_ALLOCATE_IF_RECV_DATA_MEMBLK (pRadRecvData) == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Unable to allocate memory. \r\n");
        return;
    }
    while (PNAC_RECV_FROM_QUEUE (PNAC_INTF_INPUT_QID,
                                 (UINT1 *) &pMesg,
                                 OSIX_DEF_MSG_LEN,
                                 OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        PNAC_TRC (PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
                  " INF: Message received from Queue ... Processing...\n");

        if (pMesg == NULL)
            continue;

        PNAC_LOCK ();

        switch (pMesg->u4MesgType)
        {

            case PNAC_EAPOL_FRAME:
                u2PortIndex = pMesg->Mesg.EapolFrame.u2PortIndex;
                pBuf = (tPnacCruBufChainHdr *) pMesg->Mesg.EapolFrame.pFrame;

                PnacProcPaeFrame (u2PortIndex, pBuf);
                break;

            case PNAC_AUTHSERVER_RESP:

                PNAC_TRC (PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
                          " INF: Received response from Radius...\n");

                pBuf = (tPnacCruBufChainHdr *) pMesg->Mesg.AuthSrvResp.pFrame;
                u2PktLen = (UINT2) PNAC_CRU_BUF_Get_ChainValidByteCount (pBuf);

                PNAC_MEMSET (pRadRecvData, PNAC_INIT_VAL,
                             sizeof (tPnacAsIfRecvData));

                if (PNAC_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) pRadRecvData,
                                            PNAC_OFFSET_NONE,
                                            u2PktLen) == PNAC_CRU_FAILURE)
                {
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_BUFFER_TRC |
                              PNAC_ALL_FAILURE_TRC,
                              " INF: Copy from CRU Buf returned failure\n");
                    break;
                }
                PNAC_RELEASE_CRU_BUF (pBuf, PNAC_FALSE);

                if (gPnacSystemInfo.u1SystemMode == PNAC_DISTRIBUTED)
                {
                    if (gPnacSystemInfo.u1DPnacRolePlayed ==
                        PNAC_DPNAC_SYSTEM_ROLE_MASTER)
                    {
                        u4SlotId = (UINT4) IssGetSwitchid ();

                        DPnacGetPortEntry (&pConsPortEntry,
                                           pRadRecvData->u2NasPortNumber,
                                           u4SlotId);

                        if (pConsPortEntry != NULL)
                        {
                            PnacAsIfRecvFromServer (pRadRecvData);
                        }
                        else
                        {
                            DPnacSendServRespToSlave (pRadRecvData);
                        }
                    }
                }
                else
                {
                    PnacAsIfRecvFromServer (pRadRecvData);
                }

                break;

            case PNAC_CDSM:
                u4Event = pMesg->Mesg.PnacCdsm.u4Event;
                u2PortIndex = pMesg->Mesg.PnacCdsm.u2PortIndex;

                if (u4Event == PNAC_BRIDGE_DETECTED)
                {

                    PnacPaeBridgeDetectionStatus (PNAC_BRIDGE_DETECTED,
                                                  u2PortIndex);

                }
                else if (u4Event == PNAC_BRIDGE_NOT_DETECTED)
                {
                    PnacPaeBridgeDetectionStatus (PNAC_BRIDGE_NOT_DETECTED,
                                                  u2PortIndex);
                }

                break;
            case PNAC_FIRSTDATA_FRAME:
                PNAC_TRC (PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
                          " INF: Received first data packet ...\n");

                u2PortIndex = pMesg->Mesg.MacSessionFrame.u2PortIndex;

                /* Now check whether Default Session is present and
                   proceed the session by updating the Source Mac.
                 */
                PNAC_DERIVE_UNIQ_MAC(au1MacAddr,u2PortIndex);

                if (PnacAuthGetAuthInProgressTblEntryByMac (au1MacAddr, &pPnacAuthSessionNode)
                    == PNAC_SUCCESS)
                {
                    /* Fill the source mac address of the supplicant as EAPOL Address */
                    PNAC_MEMCPY (pPnacAuthSessionNode->rmtSuppMacAddr,
                                 pMesg->Mesg.MacSessionFrame.MacAddr,
                                 PNAC_MAC_ADDR_SIZE);
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                              " Found and Replaced Group Mac Node\n");
                }
                else
                {
                    if (PnacAuthCreateSession (pMesg->Mesg.MacSessionFrame.MacAddr,
                                               u2PortIndex,
                                               OSIX_FALSE) != PNAC_SUCCESS)

                    {
                        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                                  PNAC_DATA_PATH_TRC,
                                  " HandleInFrame: Create Session failed\n");
                    }
                }
                break;

            case PNAC_CREATE_AUTHORIZE_SESSION:

                u2PortIndex = pMesg->Mesg.MacSessionFrame.u2PortIndex;

                PnacCreateAndAuthorizeMacSesson (u2PortIndex,
                                                 pMesg->Mesg.MacSessionFrame.
                                                 MacAddr);

                break;

            case PNAC_RESTART_MAC_SESSION:
                u2PortIndex = pMesg->Mesg.MacSessionFrame.u2PortIndex;

                PnacReStartMacSession
                    (u2PortIndex, pMesg->Mesg.MacSessionFrame.MacAddr);
                break;

            case PNAC_DEAUTHORIZE_MAC_SESSION:
                u2PortIndex = pMesg->Mesg.MacSessionFrame.u2PortIndex;

                PnacDeAuthorizeMacSession
                    (u2PortIndex, pMesg->Mesg.MacSessionFrame.MacAddr);
                break;

#ifdef NPAPI_WANTED
            case PNAC_NP_CALL_BACK:
                PnacProcNpCallBack (pMesg);
                break;
#endif /* NPAPI_WANTED */

            default:
                PNAC_TRC (PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
                          " ERROR! Unknown message in PNAC..\n");
                break;
        }

        PNAC_UNLOCK ();

        PNAC_RELEASE_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID, pMesg);
        PNAC_TRC (PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
                  " INF: Message Processing Done ...\n");
    }
    if (PNAC_RELEASE_IF_RECV_DATA_MEMBLK (pRadRecvData) == MEM_FAILURE)
    {
        PNAC_RELEASE_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID, pMesg);
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Failed to release  memory. \r\n");
    }
}

/*****************************************************************************/
/* Function Name      : PnacInterfaceHandleCFGQueueEvent                     */
/*                                                                           */
/* Description        : This Initiates the Action for the CFG Queue Event.   */
/*                      It receives the information for the port             */
/*                      configuration information, MBSM related message and  */
/*                      RM related messages.                                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacInterfaceHandleCFGQueueEvent (VOID)
{
    tPnacIntfMesg      *pMesg = NULL;
    tVlanChangeInfo     VlanChangeInfo;

    UINT4               u4ErrorCode;
#ifdef MBSM_WANTED
    INT4                i4ProtoId = 0;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    tMbsmPortInfo      *pPortInfo = NULL;
    INT4                i4RetVal = 0;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;
    UINT4               u4MasterSlotId = 0;
#endif /* MBSM_WANTED */
    UINT2               u2PortIndex;
    UINT2               u2AuthMode;
    UINT2               u2AuthControl;
    UINT1               u1OperStatus;

    while (PNAC_RECV_FROM_QUEUE (PNAC_INTF_CFG_QID,
                                 (UINT1 *) &pMesg,
                                 OSIX_DEF_MSG_LEN,
                                 OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        PNAC_TRC (PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
                  "CFGQ: Message received from Queue ... Processing...\n");

        if (pMesg == NULL)
            continue;

        PNAC_LOCK ();

        switch (pMesg->u4MesgType)
        {

#ifdef MBSM_WANTED
            case MBSM_MSG_CARD_INSERT:
                i4ProtoId =
                    pMesg->Mesg.MbsmCardUpdate.pMbsmProtoMsg->i4ProtoCookie;
                pSlotInfo =
                    &(pMesg->Mesg.MbsmCardUpdate.pMbsmProtoMsg->MbsmSlotInfo);
                pPortInfo =
                    &(pMesg->Mesg.MbsmCardUpdate.pMbsmProtoMsg->MbsmPortInfo);
                i4RetVal = PnacMbsmUpdateCardInsertion (pPortInfo, pSlotInfo);
                MbsmProtoAckMsg.i4ProtoCookie = i4ProtoId;
                MbsmProtoAckMsg.i4SlotId = MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
                MbsmProtoAckMsg.i4RetStatus = i4RetVal;
                MbsmSendAckFromProto (&MbsmProtoAckMsg);

                MEM_FREE (pMesg->Mesg.MbsmCardUpdate.pMbsmProtoMsg);

                DPnacGetMasterSlotId (&u4MasterSlotId);

                if ((MbsmProtoAckMsg.i4SlotId == (INT4) u4MasterSlotId) &&
                    (MbsmProtoAckMsg.i4SlotId != IssGetSwitchid ()) &&
                    (gPnacSystemInfo.u1DPnacRolePlayed ==
                     PNAC_DPNAC_SYSTEM_ROLE_SLAVE))
                {
                    if (gPnacSystemInfo.u1SystemMode == PNAC_DISTRIBUTED)
                    {
                        DPnacMasterUpIndication ();
                        DPnacSendAllPortInfoToMaster ();
                    }
                }

                break;

            case MBSM_MSG_CARD_REMOVE:
                i4ProtoId =
                    pMesg->Mesg.MbsmCardUpdate.pMbsmProtoMsg->i4ProtoCookie;
                pSlotInfo =
                    &(pMesg->Mesg.MbsmCardUpdate.pMbsmProtoMsg->MbsmSlotInfo);
                MbsmProtoAckMsg.i4ProtoCookie = i4ProtoId;
                MbsmProtoAckMsg.i4SlotId = MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
                MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
                MbsmSendAckFromProto (&MbsmProtoAckMsg);

                MEM_FREE (pMesg->Mesg.MbsmCardUpdate.pMbsmProtoMsg);

                DPnacGetMasterSlotId (&u4MasterSlotId);

                if ((gPnacSystemInfo.u1SystemMode == PNAC_DISTRIBUTED) &&
                    (MbsmProtoAckMsg.i4SlotId != IssGetSwitchid ()))
                {
                    if (MbsmProtoAckMsg.i4SlotId == (INT4) u4MasterSlotId)
                    {
                        DPnacMasterDownIndication ();
                    }
                    else
                    {
                        DPnacSlotDetach ((UINT4) MbsmProtoAckMsg.i4SlotId);
                    }
                }

                break;
#endif

            case PNAC_CREATE_PORT:

                u2PortIndex = pMesg->Mesg.PortInfo.u2PortIndex;

                PnacProcCreatePortEvent (u2PortIndex);

                L2_SYNC_GIVE_SEM ();

                break;

            case PNAC_DELETE_PORT:

                u2PortIndex = pMesg->Mesg.PortInfo.u2PortIndex;
                PnacProcDeletePortEvent (u2PortIndex);
                break;

            case PNAC_MAP_PORT:

                u2PortIndex = pMesg->Mesg.PortInfo.u2PortIndex;
                PnacProcCreatePortEvent (u2PortIndex);
                L2MI_SYNC_GIVE_SEM ();
                break;

            case PNAC_UNMAP_PORT:

                u2PortIndex = pMesg->Mesg.PortInfo.u2PortIndex;
                PnacProcDeletePortEvent (u2PortIndex);
                L2MI_SYNC_GIVE_SEM ();
                break;

            case PNAC_UPDATE_PORT_STATUS:

                u2PortIndex = pMesg->Mesg.PortInfo.u2PortIndex;
                u1OperStatus = pMesg->Mesg.PortInfo.u1OperStatus;

                if (!PNAC_IS_SHUTDOWN ())
                {
                    PnacProcPortStatUpdEvent (u2PortIndex, u1OperStatus);
                }
                else
                {
                    PnacL2IwfPnacHLPortOperIndication (u2PortIndex,
                                                       u1OperStatus);
                }

                break;

            case PNAC_AUTH_MODE_CHG:
                u2PortIndex = pMesg->Mesg.PortInfo.u2PortIndex;
                u2AuthMode = pMesg->Mesg.PortInfo.u2AuthMode;
                if (PnacLowTestPortAuthMode (&u4ErrorCode, u2PortIndex,
                                             (INT4) u2AuthMode) == SNMP_FAILURE)
                {
                    break;
                }
                PnacLowSetPortAuthMode (u2PortIndex, (INT4) u2AuthMode);
                break;

            case PNAC_AUTH_CTRL_CHG:
                u2PortIndex = pMesg->Mesg.PortInfo.u2PortIndex;
                u2AuthControl = pMesg->Mesg.PortInfo.u2AuthControl;
                if (PnacLowTestPortAuthControl (&u4ErrorCode, u2PortIndex,
                                                u2AuthControl) == SNMP_FAILURE)
                {
                    break;
                }
                PnacLowSetPortAuthControl (u2PortIndex, u2AuthControl);
                break;
            case PNAC_VLAN_PROP_CHANGE:

                PNAC_MEMSET (&VlanChangeInfo, PNAC_INIT_VAL, sizeof (tVlanChangeInfo));

                VlanChangeInfo.u2CurrentVlanId = pMesg->Mesg.PortInfo.u2CurrentVlanId;
                VlanChangeInfo.u2Port = pMesg->Mesg.PortInfo.u2PortIndex;

                if (PnacReAuthOnVlanPropChange (&VlanChangeInfo) == PNAC_FAILURE)
                {
                    break;
                }
                break;

#ifdef L2RED_WANTED
            case PNAC_RM_MSG:
                PnacRedHandleRmMsg (pMesg);
                break;
#endif

            default:
                PNAC_TRC (PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
                          " ERROR! Unknown message in PNAC..\n");
                break;
        }

        PNAC_UNLOCK ();

        PNAC_RELEASE_MEM_BLOCK (PNAC_INTF_CFGQ_MESG_MEMPOOL_ID, pMesg);

        PNAC_TRC (PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
                  " CFG: Message Processing Done ...\n");
    }
}

/*****************************************************************************/
/* Function Name      : PnacEnQFrameToIntfTask                               */
/*                                                                           */
/* Description        : This enqueues a received frame to Pnac Interface     */
/*                      Queue and sends an event PNAC_QUEUE_EVENT to the     */
/*                      Interface task.                                      */
/*                                                                           */
/* Input(s)           : CRU Buffer, PortIndex, Frame type and protocol       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacEnQFrameToIntfTask (tPnacCruBufChainHdr * pBuf, UINT2 u2PortIndex,
                        UINT1 u1FrameType)
{
    tPnacIntfMesg      *pMesg = NULL;

    if (pBuf == NULL)
        return;

    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PNAC is shutdown\n");
        PNAC_RELEASE_CRU_BUF (pBuf, PNAC_FALSE);
        return;
    }
    if ((pMesg = (tPnacIntfMesg *) PNAC_ALLOC_MEM_BLOCK
         (PNAC_INTFMESG_MEMPOOL_ID)) == NULL)
    {

        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " Allocate mem block for Pnac Interface Message failed\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gPnacSystemInfo.i4SysLogId,
                      " Allocate mem block for Pnac Interface Message failed"));
        PNAC_RELEASE_CRU_BUF (pBuf, PNAC_FALSE);
        return;
    }

    pMesg->u4MesgType = PNAC_EAPOL_FRAME;
    pMesg->Mesg.EapolFrame.u2PortIndex = u2PortIndex;
    pMesg->Mesg.EapolFrame.u1FrameType = u1FrameType;
    pMesg->Mesg.EapolFrame.pFrame = pBuf;

    if (PNAC_SEND_TO_QUEUE (PNAC_INTF_INPUT_QID, (UINT1 *) &pMesg,
                            OSIX_DEF_MSG_LEN) == PNAC_OSIX_SUCCESS)
    {
        if (PNAC_INTF_TASK_ID == PNAC_INIT_VAL)
        {
            if (PNAC_GET_TASK_ID (SELF, PNAC_INTF_TASK_NAME,
                                  &(PNAC_INTF_TASK_ID)) != PNAC_OSIX_SUCCESS)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                          PNAC_CONTROL_PATH_TRC,
                          " INF: Get Task Id failed !!!\n");
                return;
            }
        }

        if (PNAC_SEND_EVENT (PNAC_INTF_TASK_ID, PNAC_QUEUE_EVENT)
            == PNAC_OSIX_SUCCESS)
        {
            /* Success */
            return;
        }

        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                  PNAC_CONTROL_PATH_TRC, " INF: Send Event failed !!!\n");
        return;
    }

    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
              PNAC_CONTROL_PATH_TRC, " INF: Send To Q failed !!!\n");

    PNAC_RELEASE_CRU_BUF (pBuf, PNAC_FALSE);

    PNAC_RELEASE_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID, pMesg);

    return;
}

/*****************************************************************************/
/* Function Name      : PnacEnQFirstDataFrameToIntfTask                      */
/*                                                                           */
/* Description        : This enqueues the first data  frame  received to Pnac*/
/*                      Interface Queue and sends an event PNAC_QUEUE_EVENT  */
/*                      to the Interface task.                               */
/*                                                                           */
/* Input(s)           : CRU Buffer, PortIndex and protocol                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacEnQFirstDataFrameToIntfTask (UINT2 u2PortIndex, tMacAddr SrcMacAddr)
{
    tPnacIntfMesg      *pMesg = NULL;

    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PnacEnQFirstDataFrameToIntfTask: PNAC is shutdown\n");
        return;
    }
    if ((pMesg = (tPnacIntfMesg *)
         PNAC_ALLOC_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID)) == NULL)
    {

        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " Allocate mem block for Pnac Interface Message failed\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gPnacSystemInfo.i4SysLogId,
                      " Allocate mem block for Pnac Interface Message failed"));
        return;
    }

    pMesg->u4MesgType = PNAC_FIRSTDATA_FRAME;
    pMesg->Mesg.MacSessionFrame.u2PortIndex = u2PortIndex;
    MEMCPY (pMesg->Mesg.MacSessionFrame.MacAddr, SrcMacAddr, sizeof (tMacAddr));

    if (PNAC_SEND_TO_QUEUE (PNAC_INTF_INPUT_QID, (UINT1 *) &pMesg,
                            OSIX_DEF_MSG_LEN) == PNAC_OSIX_SUCCESS)
    {
        if (PNAC_INTF_TASK_ID == PNAC_INIT_VAL)
        {
            if (PNAC_GET_TASK_ID (SELF, PNAC_INTF_TASK_NAME,
                                  &(PNAC_INTF_TASK_ID)) != PNAC_OSIX_SUCCESS)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                          PNAC_CONTROL_PATH_TRC,
                          " INF: Get Task Id failed !!!\n");
                return;
            }
        }

        if (PNAC_SEND_EVENT (PNAC_INTF_TASK_ID, PNAC_QUEUE_EVENT)
            == PNAC_OSIX_SUCCESS)
        {
            /* Success */
            return;
        }
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                  PNAC_CONTROL_PATH_TRC, " INF: Send Event failed !!!\n");
        return;
    }

    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
              PNAC_CONTROL_PATH_TRC, " INF: Send To Q failed !!!\n");

    PNAC_RELEASE_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID, pMesg);

    return;
}

/*****************************************************************************/
/* Function Name      : PnacAuthPortOperIndication                           */
/*                                                                           */
/* Description        : This routine is invoked by PNAC module when it wants */
/*                      to indicate the change in port status to the         */
/*                      CFA or similar module. The receiving module should   */
/*                      then inform the appl.s of the status change. This API*/
/*                      should be ported to customer environments.           */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - On successful execution               */
/*                      PNAC_FAILURE - In case of any errors                 */
/*****************************************************************************/
INT4
PnacAuthPortOperIndication (UINT2 u2PortNum)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    UINT1               u1OperStatus;

    INT4                i4NpapiMode = PNAC_NP_SYNC;

    INT4                i4ProtoId = (INT4) L2_PROTO_DOT1X;

    i4NpapiMode = PnacNpGetNpapiMode (i4ProtoId);

    if (PnacGetPortEntry (u2PortNum, &pPortInfo) == PNAC_FAILURE)
    {
        return PNAC_FAILURE;
    }

    if (PnacGetHLPortStatus (u2PortNum, &u1OperStatus) == PNAC_SUCCESS)
    {
        if ((u1OperStatus == PNAC_PORT_OPER_UP) &&
            (gPnacSystemInfo.u1PnacCallSequence == PNAC_PROTOCOL_FLOW) &&
            (i4NpapiMode == PNAC_NP_ASYNC) &&
            (PNAC_IS_PORT_MODE_PORTBASED (pPortInfo)))
        {
            return PNAC_SUCCESS;
        }

        if (PnacL2IwfPnacHLPortOperIndication (u2PortNum, u1OperStatus) ==
            L2IWF_SUCCESS)
        {
            return PNAC_SUCCESS;
        }
    }
    return PNAC_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PnacEnQRadRespToIntfTask                             */
/*                                                                           */
/* Description        : This function sends the response message to PNAC     */
/*                      Interface Task's input queue.                        */
/*                                                                           */
/* Input(s)           : pServEapData - pointer to the response structure.    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS/ PNAC_FAILURE                           */
/*****************************************************************************/
INT4
PnacEnQRadRespToIntfTask (tPnacAsIfRecvData * pServEapData)
{
    tPnacCruBufChainHdr *pBuf = NULL;
    tPnacIntfMesg      *pMesg = NULL;

    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  "INF: Pnac is shutdown\n");
        return PNAC_FAILURE;
    }

    if ((pBuf = PNAC_ALLOC_CRU_BUF ((sizeof (tPnacAsIfRecvData)), 0)) == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                  PNAC_CONTROL_PATH_TRC,
                  " INF: Buffer Allocation failed !!!\n");
        return PNAC_FAILURE;
    }

    if (PNAC_COPY_OVER_CRU_BUF (pBuf, (UINT1 *) pServEapData, PNAC_OFFSET_NONE,
                                (UINT4) (sizeof (tPnacAsIfRecvData)))
        == PNAC_CRU_FAILURE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                  PNAC_CONTROL_PATH_TRC, " INF: Buffer Copying failed !!!\n");
        PNAC_RELEASE_CRU_BUF (pBuf, PNAC_FALSE);
        return PNAC_FAILURE;
    }

    pMesg = (tPnacIntfMesg *) PNAC_ALLOC_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID);

    if (pMesg == NULL)
    {

        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC
                  | PNAC_OS_RESOURCE_TRC,
                  " Allocate mem block for Pnac Interface Message failed\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gPnacSystemInfo.i4SysLogId,
                      "Allocate mem block for Pnac Interface Message failed"));

        PNAC_RELEASE_CRU_BUF (pBuf, PNAC_FALSE);
        return PNAC_FAILURE;
    }

    pMesg->u4MesgType = PNAC_AUTHSERVER_RESP;

    /* pMesg->Mesg.AuthSrvResp.u2RespType -- Unused */
    pMesg->Mesg.AuthSrvResp.u2Length = (UINT2) sizeof (tPnacAsIfRecvData);
    pMesg->Mesg.AuthSrvResp.pFrame = pBuf;

    if (PNAC_SEND_TO_QUEUE (PNAC_INTF_INPUT_QID, (UINT1 *) &pMesg,
                            OSIX_DEF_MSG_LEN) == PNAC_OSIX_SUCCESS)
    {
        if (PNAC_INTF_TASK_ID == PNAC_INIT_VAL)
        {
            if (PNAC_GET_TASK_ID (SELF, PNAC_INTF_TASK_NAME,
                                  &(PNAC_INTF_TASK_ID)) != PNAC_OSIX_SUCCESS)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                          PNAC_CONTROL_PATH_TRC,
                          " INF: Get Task Id failed !!!\n");
                return PNAC_FAILURE;
            }
        }

        if (PNAC_SEND_EVENT (PNAC_INTF_TASK_ID, PNAC_QUEUE_EVENT)
            != PNAC_OSIX_SUCCESS)
        {

            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                      PNAC_CONTROL_PATH_TRC, " INF: Send Event failed !!!\n");

            return PNAC_FAILURE;
        }

        return PNAC_SUCCESS;
    }

    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
              PNAC_CONTROL_PATH_TRC, " INF: Send To Q failed !!!\n");

    PNAC_RELEASE_CRU_BUF (pBuf, PNAC_FALSE);
    PNAC_RELEASE_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID, pMesg);

    return PNAC_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PnacCreateAndAuthorizeMacSesson                      */
/*                                                                           */
/* Description        : This function makes the mac session identified by    */
/*                      given port and mac address, authenticated forcebly.  */
/*                                                                           */
/* Input(s)           : u2PortIndex - Port Index.                            */
/*                      SrcMacAddr  - Source mac of Wlan mac session.        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacCreateAndAuthorizeMacSesson (UINT2 u2PortNum, tMacAddr SrcMacAddr)
{
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC | PNAC_ALL_FAILURE_TRC,
                  "PnacCreateAndAuthorizeMacSesson : Port number not created\n");
        return;
    }

    if (PNAC_IS_PORT_MODE_PORTBASED (pPortInfo))
    {
        PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC | PNAC_ALL_FAILURE_TRC,
                  "PnacCreateAndAuthorizeMacSesson : Port Auth mode is mac"
                  " based.\n");

        return;
    }
    /* received port supports MAC based authentication */
    if (PnacAuthGetSessionTblEntryByMac (SrcMacAddr, &pAuthSessNode)
        == PNAC_SUCCESS)

    {
        /* Session Already authenticated. So nothing to do. */
        return;
    }                            /* GetMacSessionNode ends */

    /*  verify if it is authorizing */
    if (PnacAuthGetAuthInProgressTblEntryByMac
        (SrcMacAddr, &pAuthSessNode) != PNAC_SUCCESS)

    {

        if (PNAC_ALLOCATE_AUTHSESSNODE_MEMBLK (pAuthSessNode) == NULL)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      "PnacCreateAndAuthorizeMacSesson: "
                      "Allocate auth sess node failed \n");
            return;
        }

        PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC,
                       "PnacCreateAndAuthorizeMacSesson: "
                       "Allocated Session Node : %x \n", pAuthSessNode);
        PNAC_MEMSET (pAuthSessNode, PNAC_INIT_VAL,
                     sizeof (tPnacAuthSessionNode));

        PnacAuthSessionNodeInit (pAuthSessNode, u2PortNum);
        PNAC_MEMCPY (pAuthSessNode->rmtSuppMacAddr, SrcMacAddr,
                     PNAC_MAC_ADDR_SIZE);

        /* add to AIP Table */
        if (PnacAuthAddToAuthInProgressTbl (pAuthSessNode) == PNAC_FAILURE)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      "PnacCreateAndAuthorizeMacSesson: "
                      "Adding to Auth in progress table failed \n");
            return;
        }
    }

    /* call MakeAuthenticated() */
    if (PnacAuthAsmMakeAuthenticated (u2PortNum, pAuthSessNode,
                                      NULL, 0) == PNAC_FAILURE)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  "PnacCreateAndAuthorizeMacSesson: "
                  "Unable to make authenticated \n");
        return;
    }

    return;
}

/*****************************************************************************/
/* Function Name      : PnacReStartMacSession                                */
/*                                                                           */
/* Description        : This function restarts the mac session identified by */
/*                      given port and mac address.                          */
/*                                                                           */
/* Input(s)           : u2PortIndex - Port Index.                            */
/*                      SrcMacAddr  - Source mac of Wlan mac session.        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacReStartMacSession (UINT2 u2PortNum, tMacAddr SrcMacAddr)
{
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC | PNAC_ALL_FAILURE_TRC,
                  "PnacReStartMacSession : Port number not created\n");
        return;
    }

    /* Check whether received port supports MAC based authentication */
    if (PNAC_IS_PORT_MODE_PORTBASED (pPortInfo))
    {
        PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC | PNAC_ALL_FAILURE_TRC,
                  "PnacReStartMacSession : Port Auth mode is mac" " based.\n");
        return;
    }

    /* If the mac session is already authorized, then delete the session 
     * and restart the session. */
    if (PnacAuthGetSessionTblEntryByMac (SrcMacAddr, &pAuthSessNode)
        == PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC |
                  PNAC_ALL_FAILURE_TRC,
                  " PnacReStartMacSession : Sta already authenticated");
        PnacAuthDeleteSession (pAuthSessNode);

        /* Start the session for the given mac freshly.
         * Create Session, programs the status of mac session to 
         * unauthorized in L2Iwf as well as in NP. */
        if (PnacAuthCreateSession (SrcMacAddr, u2PortNum, OSIX_FALSE)
            != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                      PNAC_DATA_PATH_TRC,
                      " PnacReStartMacSession : Crt session failed\n");
        }
        return;
    }                            /* GetMacSessionNode ends */
    else
    {
        /* If the mac session is in Auth in progress table, then no need to
         * do anything. If the mac session is not in Auth in progress table
         * then start the mac session freshly. */
        if (PnacAuthGetAuthInProgressTblEntryByMac (SrcMacAddr, &pAuthSessNode)
            != PNAC_SUCCESS)

        {
            /* the Supplicant is not in progress of any authorization */
            if (PnacAuthCreateSession (SrcMacAddr, u2PortNum, OSIX_FALSE)
                != PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                          PNAC_DATA_PATH_TRC,
                          " PnacReStartMacSession : Crt session failed\n");
            }
        }
    }

}

/*****************************************************************************/
/* Function Name      : PnacDeAuthorizeMacSession                            */
/*                                                                           */
/* Description        : This function deauthorizes the mac session identified*/
/*                      by given port and mac address.                       */
/*                                                                           */
/* Input(s)           : u2PortIndex - Port Index.                            */
/*                      SrcMacAddr  - Source mac of Wlan mac session.        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacDeAuthorizeMacSession (UINT2 u2PortNum, tMacAddr SrcMacAddr)
{
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC | PNAC_ALL_FAILURE_TRC,
                  "PnacDeAuthorizeMacSession : Port number not created\n");
        return;
    }

    /* Check whether received port supports MAC based authentication */
    if (PNAC_IS_PORT_MODE_PORTBASED (pPortInfo))
    {
        PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC | PNAC_ALL_FAILURE_TRC,
                  "PnacDeAuthorizeMacSession : Port Auth mode is mac"
                  " based.\n");
        return;
    }

    /* Make this mac session deauthorized and then delete the mac 
     * session. */
    if ((PnacAuthGetSessionTblEntryByMac (SrcMacAddr, &pAuthSessNode)
         == PNAC_SUCCESS) ||
        (PnacAuthGetAuthInProgressTblEntryByMac (SrcMacAddr, &pAuthSessNode)
         == PNAC_SUCCESS))

    {
        PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC |
                  PNAC_ALL_FAILURE_TRC,
                  " PnacDeAuthorizeMacSession : Sta already authenticated");

        PnacL2IwfSetSuppMacAuthStatus (SrcMacAddr,
                                       PNAC_PORTSTATUS_UNAUTHORIZED);

#ifdef NPAPI_WANTED
        if (PNAC_IS_NP_PROGRAMMING_ALLOWED () == PNAC_TRUE)
        {
            if (PnacPnacHwSetAuthStatus (u2PortNum,
                                         SrcMacAddr,
                                         pPortInfo->u1PortAuthMode,
                                         (UINT1) PNAC_PORTSTATUS_UNAUTHORIZED,
                                         PNAC_CNTRLD_DIR_BOTH) != FNP_SUCCESS)

            {
                PnacNpFailNotifyAndLog (u2PortNum,
                                        pAuthSessNode->rmtSuppMacAddr,
                                        (UINT1) PNAC_CNTRLD_DIR_BOTH,
                                        pPortInfo->u1PortAuthMode,
                                        (UINT1) PNAC_PORTSTATUS_UNAUTHORIZED);
                return;
            }
        }
#endif /* NPAPI_WANTED */
        PnacAuthDeleteSession (pAuthSessNode);
    }

    return;
}

/*****************************************************************************/
/* Function Name      : PnacHandleInCtrlFrame                                */
/*                                                                           */
/* Description        : This function processes the incoming PNAC Ctrl frame,*/
/*                      received from CFA. And Handles the received frames   */
/*                      to the Interface task                      */
/*                                                                           */
/* Input(s)           : pMesg - pointer to CRU buffer chain containing       */
/*                              MAC frame,                                   */
/*                      u2IfIndex - Interface index,                         */
/*                      u2Protocol - Protocol of the data inside MAC frame   */
/*                      u1FrameType - Ethernet Frame type                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                              */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - if port is authorized to recv         */
/*                                     this frame                            */
/*                      PNAC_FAIlURE - The port is not authorized,           */
/*                                     discard the frame                     */
/*                      PNAC_IGNORE -  This is a control frame. Ignore       */
/*                                     the frame,dont discard.               */
/*****************************************************************************/

INT4
PnacHandleInCtrlFrame (tPnacCruBufChainHdr * pMesg, UINT2 u2IfIndex,
                       UINT1 u1FrameType)
{
    PNAC_TRC_ARG1 (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC,
                   " Incoming Control Frame Received on Port: %d\n", u2IfIndex);
    /* Eapol frames are Enqueued to Interface Task */
    PnacEnQFrameToIntfTask (pMesg, u2IfIndex, u1FrameType);
    return PNAC_CONTROL;
}

/*****************************************************************************/
/* Function Name      : PnacHandleInDataFrame                                */
/*                                                                           */
/* Description        : This function processes the incoming DATA frame,     */
/*                      received from CFA and verifies the Auth status      */
/*                                                                           */
/* Input(s)           : pMesg - pointer to CRU buffer chain containing       */
/*                              MAC frame,                                   */
/*                      u2IfIndex - Interface Index                          */
/*                      u1FrameType - Ethernet Frame type                    */
/*                      u4ContextId - Context Id of the interface            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : port address of the interface in gPnacSytemInfo,     */
/*                      port's authentication mode in gPnacSystemInfo,       */
/*                      port's authorization status in gPnacSystemInfo       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - if port is authorized to recv         */
/*                                     this frame                            */
/*                      PNAC_FAIlURE - The port is not authorized,           */
/*                                     discard the frame                     */
/*                      PNAC_IGNORE -  This is a control frame. Ignore       */
/*                                     the frame,dont discard.               */
/*****************************************************************************/

INT4
PnacHandleInDataFrame (tPnacCruBufChainHdr * pMesg, UINT2 u2IfIndex,
                       UINT1 u1FrameType, UINT4 u4ContextId)
{
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacAuthSessStats *pAuthSessStats = NULL;
    UINT1               u1RstMstDisabled = PNAC_TRUE;
    UINT2               u2MesgLength = PNAC_INIT_VAL;
    UINT2               u2OperControlDir;
    tMacAddr            SrcMacAddress;
    tMacAddr            DestMacAddress;

    PNAC_TRC_ARG1 (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC,
                   " Incoming Frame Received on Port: %d\n", u2IfIndex);

/* This function does the following:
 * 1) If Rst and Mst are disabled and if this is a BPDU then post 
 *    BRIDGE_DETECTED event to the PNAC queue and return CONSUMED.
 * 2) Else Take PnacLock and 
 * 3) If mode is port based then return PROCESS or DROP based on Authorisation 
 *    status after updating the statistics.
 * 4) Else if mode is MAC based then return PROCESS or DROP based on 
 *    Authorisation status after updating the statistics.
 * 5) In case of mac-based authentication, the first data packet will be
 *    considered as control packet
 */
    UNUSED_PARAM (u1FrameType);

    /* Get the length of first valid bytes in CRU buf */
    u2MesgLength = (UINT2) PNAC_CRU_BUF_Get_ChainValidByteCount (pMesg);
    PNAC_PKT_DUMP (PNAC_DUMP_TRC, pMesg, u2MesgLength,
                   " Dumping received frame :\n");

    /* received frame should be treated as a Data frame only */

    /* Check if both RSTP and MSTP are both disabled and it receives any BPDU
     * then Handle the bridge detection here.
     * If any of these modules are enabled then triggers r provided from the 
     * Bridge Detection State Machine.
     */

    if ((PnacAstIsRstEnabledInContext (u4ContextId)) ||
        (PnacAstIsMstEnabledInContext (u4ContextId)))
    {
        u1RstMstDisabled = PNAC_FALSE;
    }

    if (u1RstMstDisabled == PNAC_TRUE)
    {

        /*******************************************************************/
        /* OperControlledDirections is configured IN and it receives any   */
        /* BPDU on this port then call routine PnacHandleBridgeDetected    */
        /* so that OperControlledDirections is forced to BOTH.             */
        /*******************************************************************/
        PnacL2IwfGetPortPnacControlDir (u2IfIndex, &u2OperControlDir);
        if (u2OperControlDir == PNAC_CNTRLD_DIR_IN)
        {
            if (PNAC_COPY_FROM_CRU_BUF (pMesg, (UINT1 *) DestMacAddress,
                                        PNAC_DESTADDR_OFFSET,
                                        PNAC_MAC_ADDR_SIZE) == PNAC_CRU_FAILURE)
            {

                PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC
                          | PNAC_ALL_FAILURE_TRC,
                          " Copy From Buffer failed ... \n");

                return PNAC_DROP;
            }
            if (PNAC_MEMCMP
                (DestMacAddress, gPnacBrgGrpAddr, PNAC_MAC_ADDR_SIZE) == 0)
            {
                PnacHandleEvent (u2IfIndex, PNAC_BRIDGE_DETECTED);
                return PNAC_DROP;
            }
        }
    }

    PNAC_LOCK ();

    if (PnacGetPortEntry (u2IfIndex, &pPortInfo) != PNAC_SUCCESS)

    {
        PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC | PNAC_ALL_FAILURE_TRC,
                  " Frame received on INVALID status port...\n");
        PNAC_UNLOCK ();
        return PNAC_DROP;
    }

    /* 1. Check if Authenticator role is supported on this port */
    /* 2. Check if Dot1x is disabled on this port */
    if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo) || 
       (pPortInfo->u1PortPaeStatus == PNAC_DISABLED))

    {

      /*******************************************************************/
      /*  Authenticator role not supported on this port (or)             */
      /*  Dot1x is disabled on this port.                                */
      /*  so handover the frame to upper layer                           */
      /*******************************************************************/
        PNAC_UNLOCK ();
        return PNAC_DATA;

    }

   /*******************************************************************/
    /* Authenticator role is supported on this port & has to undergo   */
    /* Authorization checks                                            */
   /*******************************************************************/
    if (PNAC_IS_PORT_MODE_PORTBASED (pPortInfo))

    {
        /* received port supports Port based authentication */
        /* check the authorization status for this port     */
        pAuthSessNode = pPortInfo->pPortAuthSessionNode;
        PNAC_DEBUG (if (pAuthSessNode == NULL)
                    PNAC_TRC (PNAC_BUFFER_TRC | PNAC_ALL_FAILURE_TRC |
                              PNAC_DATA_PATH_TRC,
                              "DBG: Error: PortAuthSessionNode is Null in "
                              "Port Based Authorization Mode\n"););
        if (pAuthSessNode == NULL)
        {
            PNAC_UNLOCK ();
            return PNAC_DROP;
        }
        if (pPortInfo->u2PortStatus == PNAC_PORTSTATUS_AUTHORIZED)

        {

            /* port is authenticated */
            /* Update Statistics     */
            pAuthSessStats = &(pAuthSessNode->authSessStats);
            (pAuthSessStats->u4SessFramesRx)++;
            PNAC_UINT8_ADD (pAuthSessStats->u8SessOctetsRx, u2MesgLength);
            PNAC_DEBUG (PNAC_TRC
                        (PNAC_ALL_FAILURE_TRC | PNAC_DATA_PATH_TRC,
                         " DBG:PAE: Incremented Statistics\n"););
            PNAC_UNLOCK ();
            return PNAC_DATA;

        }

        else

        {
            PNAC_UNLOCK ();
            /* port is unauthenticated, drop the packet */
            return PNAC_DROP;

        }                        /*Port is not Authorized,ends     */
    }                            /*Port based authentication, ends */

    else if (PNAC_IS_PORT_MODE_MACBASED (pPortInfo))

    {

        /* If Port control mode is forceauth classify 
           the frames as data frames */

        if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl ==     
                    PNAC_PORTCNTRL_FORCEAUTHORIZED) 
        {
            PNAC_UNLOCK ();
            return PNAC_DATA;
        }

        /* If Port control mode is forceunauth
           drop the frame */

        if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl == 
            PNAC_PORTCNTRL_FORCEUNAUTHORIZED)
        {
            PNAC_UNLOCK ();
            return PNAC_DROP;
        }

         /* Mode is auto */

        /* read the Source MAC address */
        if (PNAC_COPY_FROM_CRU_BUF (pMesg, (UINT1 *) SrcMacAddress,
                                    PNAC_SRCADDR_OFFSET, PNAC_MAC_ADDR_SIZE)
            == PNAC_CRU_FAILURE)
        {

            PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC
                      | PNAC_ALL_FAILURE_TRC,
                      " Copy From Buffer failed ... \n");

            PNAC_UNLOCK ();
            return PNAC_DROP;
        }
        /* received port supports MAC based authentication */
        if (PnacAuthGetSessionTblEntryByMac (SrcMacAddress, &pAuthSessNode)
            == PNAC_SUCCESS)

        {
            /* This Supplicant is authorized */
            pAuthSessStats = &(pAuthSessNode->authSessStats);
            (pAuthSessStats->u4SessFramesRx)++;
            PNAC_UINT8_ADD (pAuthSessStats->u8SessOctetsRx, u2MesgLength);
            PNAC_UNLOCK ();
            return PNAC_DATA;
        }                        /* GetMacSessionNode ends */

        else
        {

            /*  verify if it is authorizing */
            if (PnacAuthGetAuthInProgressTblEntryByMac
                (SrcMacAddress, &pAuthSessNode) != PNAC_SUCCESS)

            {

                /* the Supplicant is not in progress of any authorization */
                PnacEnQFirstDataFrameToIntfTask (u2IfIndex, SrcMacAddress);
            }
            PNAC_UNLOCK ();
            /* the Supplicant is in progress of authorization */
            /* Hence, return without any further processing for this Supplicant */
            return PNAC_DROP;
        }                        /* else GetMacSessionNode ends */
    }                            /* MAC based authentication, ends */
    else

    {
        PNAC_DEBUG (PNAC_TRC
                    (PNAC_DATA_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                     "  Error: HandleInFrame: Authmode invalid\n"););
        PNAC_UNLOCK ();
        return PNAC_DROP;

    }
}

/*****************************************************************************/
/* Function Name      : PnacRegisterWithPacketHandler                        */
/*                                                                           */
/* Description        : This function registers with the packet handler for  */
/*                      PNAC control packets.                                */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS/PNAC_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PnacRegisterWithPacketHandler ()
{
    tProtocolInfo       PnacProtocolInfo;

    MEMSET (&PnacProtocolInfo, 0, sizeof (tProtocolInfo));

    PnacProtocolInfo.ProtocolFnPtr = NULL;
    PnacProtocolInfo.i4Command = MUX_PROT_REG_HDLR;
    PnacProtocolInfo.u1SendToFlag = MUX_REG_SEND_TO_ISS;

    PnacProtocolInfo.ProtRegTuple.EthInfo.u2Protocol = PNAC_PAE_ENET_TYPE;
    PnacProtocolInfo.ProtRegTuple.EthInfo.u2Mask = 0xFFFF;

    if (CfaMuxRegisterProtocolCallBack
        (PACKET_HDLR_PROTO_PNAC, &PnacProtocolInfo) != CFA_SUCCESS)
    {
        return PNAC_FAILURE;
    }

    return PNAC_SUCCESS;
}

#ifdef NPAPI_WANTED
/*****************************************************************************
 * Function Name      : PnacProcNpCallBack                                   
 *                                                                           
 * Description        : This Function handles the Call back for              
 *                      NPAPI PnacHwSetAuthStatus.                           
 *                                                                           
 * Input(s)           : pMesg - Pointer to the Structure tPnacIntfMesg       
 *                                                                           
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : None                                                 
 *                                                                           
 *****************************************************************************/
VOID
PnacProcNpCallBack (tPnacIntfMesg * pMesg)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tMacAddr            SuppMacAddr;

    UINT4               u4NpCallId = 0;

    INT4                i4NpRetVal = 0;

    UINT2               u2Port = 0;

    UINT1               u1PnacPortAuthMode = 0;
    UINT1               u1PnacPortCtrlDir = 0;
    UINT1               u1PnacPortAuthStatus = 0;

    gPnacSystemInfo.u1PnacCallSequence = PNAC_NP_CALLBACK_FLOW;

    u4NpCallId = pMesg->Mesg.PnacNpCallBackMsg.u4NpCallId;

    u2Port = pMesg->Mesg.PnacNpCallBackMsg.u2Port;

    if (PnacGetPortEntry (u2Port, &pPortInfo) == PNAC_FAILURE)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " INF: Invalid Port ...\n");
        return;
    }

    i4NpRetVal = pMesg->Mesg.PnacNpCallBackMsg.rval;
    u1PnacPortAuthMode = pMesg->Mesg.PnacNpCallBackMsg.u1AuthMode;
    u1PnacPortCtrlDir = pMesg->Mesg.PnacNpCallBackMsg.u1CtrlDir;
    u1PnacPortAuthStatus = pMesg->Mesg.PnacNpCallBackMsg.u1AuthStatus;

    MEMCPY (SuppMacAddr, pMesg->Mesg.PnacNpCallBackMsg.SuppMacAddr,
            sizeof (tMacAddr));

    /* Based on the NP Call Id, do the Call Back processing. */
    if (u4NpCallId == (UINT4) AS_PNAC_HW_SET_AUTH_STATUS)
    {
        /* Based on the Authentication Mode, 
         * Do the Call Back Processing for Port based or Mac Based. */
        if (u1PnacPortAuthMode == PNAC_PORT_AUTHMODE_PORTBASED)
        {
            if (i4NpRetVal == FNP_SUCCESS)
            {
                /* NP Call Has succeeded for Port Based Approach,
                 * Hence, resetting the retry count variable for the Port */
                pPortInfo->u1PnacAsyncNpRetryCount = PNAC_NO_VAL;

                PnacProcNpAuthCbPortBasedSuccess (u2Port, u1PnacPortCtrlDir,
                                                  u1PnacPortAuthStatus);
            }
            else if (i4NpRetVal == FNP_FAILURE)
            {
                PnacProcNpAuthCbPortBasedFailure (u2Port, u1PnacPortCtrlDir,
                                                  u1PnacPortAuthStatus);
            }
        }
        else if (u1PnacPortAuthMode == PNAC_PORT_AUTHMODE_MACBASED)
        {
            PnacProcNpAuthCbMacBased (u2Port, SuppMacAddr,
                                      u1PnacPortAuthStatus, i4NpRetVal);
        }
    }
    else if (u4NpCallId == (UINT4) AS_PNAC_HW_ADD_OR_DEL_MAC_SESS)
    {
        PnacProcNpSessStatusCb
            (u2Port, SuppMacAddr, u1PnacPortAuthStatus, i4NpRetVal);
    }

    gPnacSystemInfo.u1PnacCallSequence = PNAC_PROTOCOL_FLOW;

    return;
}

/*****************************************************************************
 * Function Name      : PnacProcNpSessStatusCb                         
 *                                                                           
 * Description        : This Function handles the Call back for              
 *                      NPAPI PnacHwAddOrDelMacSess.                         
 *                                                                           
 * Input(s)           : u2Port - Port Number.
 *                      SuppMacAddr - Supplicant Mac Addr.
 *                      u1PnacMacSessStatus - Mac Session Status
 *                      i4NpRetVal - Return Value of NPAPI Call
 *                                                                           
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : None                                                 
 *                                                                           
 *****************************************************************************/
VOID
PnacProcNpSessStatusCb (UINT2 u2Port, tMacAddr SuppMacAddr,
                        UINT1 u1PnacMacSessStatus, INT4 i4NpRetVal)
{
    tPnacNpFailedMacSessNode *pNpFailedMacSessNode = NULL;

    if ((u1PnacMacSessStatus == PNAC_PORTSTATUS_UNAUTHORIZED) ||
        (u1PnacMacSessStatus == PNAC_PORTSTATUS_AUTHORIZED))
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " INF: This Case is not Handled Here ...\n");
        return;
    }

    switch (i4NpRetVal)
    {
        case FNP_SUCCESS:
            /* Success Case of PnacHwAddOrDelMacSessStatus need not be 
             * handled. Since, Setting L2IWF is taken care in Normal Flow. */

            if (PnacGetNpFailedMacSessNodeByMac (SuppMacAddr,
                                                 &pNpFailedMacSessNode)
                == PNAC_SUCCESS)
            {
                PnacDelFromNpFailedMacSessTree (pNpFailedMacSessNode);
            }
            break;
        case FNP_FAILURE:
            /* If the Previous Status is AUTHORISED, data traffic flows 
             * through. If the Previous Status is UNAUTHORISED, no new data
             * traffic flows through. So, for both these case 
             * Reprogramming the hardware limited number of times is done. */
            PnacNpSetMacSessStatusRetry (u2Port, SuppMacAddr,
                                         u1PnacMacSessStatus);
            break;
        default:
            /* Not Possible */
            break;
    }
    return;
}

/*****************************************************************************
 * Function Name      : PnacProcNpAuthCbPortBasedSuccess                      
 *                                                                           
 * Description        : This Function handles the Call back for NPAPI        
 *                      PnacHwSetAuthStatus for Port Based Approach.         
 *                      It is called by PnacProcNpCallback on success of the 
 *                      NPAPI Call.                                
 *                                                                           
 * Input(s)           : u2Port - Port Number.
 *                      u1PnacPortCtrlDir - Port Control Direction
 *                      u1PnacPortAuthStatus  - Port Auth Status
 *                                                                           
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : None                                                 
 *                                                                           
 ****************************************************************************/
VOID
PnacProcNpAuthCbPortBasedSuccess (UINT2 u2Port, UINT1 u1PnacPortCtrlDir,
                                  UINT1 u1PnacPortAuthStatus)
{
    UINT1               u1PnacPortOperStatus = PNAC_PORT_OPER_DOWN;
    UINT2               u2PnacPrevPortAuthStatus = PNAC_PORTSTATUS_UNAUTHORIZED;

    PnacL2IwfGetPortOperStatus (LA_MODULE, u2Port, &u1PnacPortOperStatus);

    if (u1PnacPortAuthStatus == PNAC_PORTSTATUS_AUTHORIZED)
    {
        if (u1PnacPortOperStatus == PNAC_PORT_OPER_DOWN)
        {
            /* Port is DOWN in Higher Layers.
             * So, Indicating as UP and Setting L2IWF. */

            PnacAuthPortOperIndication (u2Port);

            PnacL2IwfSetPortPnacAuthStatus (u2Port, PNAC_PORTSTATUS_AUTHORIZED);

            PnacL2IwfSetPortPnacControlDir (u2Port, u1PnacPortCtrlDir);
        }
        else if (u1PnacPortOperStatus == PNAC_PORT_OPER_UP)
        {
            /* Port is Already UP in Higher Layers.
             * So, Not Indicating Again to them. */

            /* So, Setting Only Control Direction in L2IWF. */
            PnacL2IwfSetPortPnacControlDir (u2Port, u1PnacPortCtrlDir);
        }
    }
    else if (u1PnacPortAuthStatus == PNAC_PORTSTATUS_UNAUTHORIZED)
    {
        if (u1PnacPortCtrlDir == PNAC_CNTRLD_DIR_BOTH)
        {
            PnacL2IwfGetPortPnacAuthStatus (u2Port, &u2PnacPrevPortAuthStatus);
            if (u2PnacPrevPortAuthStatus == (UINT2) PNAC_PORTSTATUS_AUTHORIZED)
            {
                PnacL2IwfSetPortPnacAuthStatus (u2Port,
                                                (UINT2) u1PnacPortAuthStatus);
            }
        }
        else if (u1PnacPortCtrlDir == PNAC_CNTRLD_DIR_IN)
        {
            if (u1PnacPortOperStatus == PNAC_PORT_OPER_DOWN)
            {
                /* Port is DOWN in Higher Layers.
                 * So, Indicating as UP and Setting L2IWF. */

                PnacAuthPortOperIndication (u2Port);

                PnacL2IwfSetPortPnacAuthStatus (u2Port,
                                                PNAC_PORTSTATUS_UNAUTHORIZED);

                PnacL2IwfSetPortPnacControlDir (u2Port,
                                                (UINT2) PNAC_CNTRLD_DIR_IN);
            }
            else if (u1PnacPortOperStatus == PNAC_PORT_OPER_UP)
            {
                /* Port is Already UP in Higher Layers.
                 * So, Not Indicating Again to them. */
            }
        }
    }

    return;
}

/*****************************************************************************
 * Function Name      : PnacProcNpAuthCbPortBasedFailure                      
 *                                                                           
 * Description        : This Function handles the Call back for NPAPI        
 *                      PnacHwSetAuthStatus for Port Based Approach.         
 *                      It is called by PnacProcNpCallback on failure of the 
 *                      NPAPI Call.                                
 *                                                                           
 * Input(s)           : u2Port - Port Number.
 *                      u1PnacPortCtrlDir - Port Control Direction
 *                      u1PnacPortAuthStatus  - Port Auth Status
 *                                                                           
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : None                                                 
 *                                                                           
 ****************************************************************************/
VOID
PnacProcNpAuthCbPortBasedFailure (UINT2 u2Port, UINT1 u1PnacPortCtrlDir,
                                  UINT1 u1PnacPortAuthStatus)
{
    tMacAddr            SuppMacAddr = { PNAC_NO_VAL };

    UINT2               u2PnacPrevPortAuthStatus = PNAC_PORTSTATUS_UNAUTHORIZED;
    UINT2               u2PnacPrevPortCtrlDir = PNAC_CNTRLD_DIR_BOTH;

    PnacL2IwfGetPortPnacAuthStatus (u2Port, &u2PnacPrevPortAuthStatus);
    PnacL2IwfGetPortPnacControlDir (u2Port, &u2PnacPrevPortCtrlDir);

    if (u1PnacPortAuthStatus == PNAC_PORTSTATUS_AUTHORIZED)
    {
        /* Failure is not Handled Here, Since there is 
         * a recovery mechanism.  Make the Operational status of Port 
         * DOWN and UP to recover. */
        PnacNpFailNotifyAndLog (u2Port, SuppMacAddr,
                                u1PnacPortCtrlDir,
                                (UINT1) PNAC_PORT_AUTHMODE_PORTBASED,
                                (UINT1) PNAC_PORTSTATUS_AUTHORIZED);
    }
    else if (u1PnacPortAuthStatus == PNAC_PORTSTATUS_UNAUTHORIZED)
    {
        if (u1PnacPortCtrlDir == PNAC_CNTRLD_DIR_BOTH)
        {
            /* Serious Problem Here. New Data traffic will flow through.
             * So, Try to Avoid by Reprogramming the hardware limited
             * number of times. */
            PnacNpSetAuthStatusRetry (u2Port, SuppMacAddr, u1PnacPortCtrlDir,
                                      PNAC_PORT_AUTHMODE_PORTBASED,
                                      u1PnacPortAuthStatus);
        }
        else if (u1PnacPortCtrlDir == PNAC_CNTRLD_DIR_IN)
        {
            if ((u2PnacPrevPortAuthStatus == PNAC_PORTSTATUS_UNAUTHORIZED) &&
                (u2PnacPrevPortCtrlDir == PNAC_CNTRLD_DIR_BOTH))
            {
                /* No Harm Here. No New Data Traffic will flow through.
                 * So, Not Doing Anything here. */
                PnacNpFailNotifyAndLog (u2Port, SuppMacAddr,
                                        u1PnacPortCtrlDir,
                                        (UINT1) PNAC_PORT_AUTHMODE_PORTBASED,
                                        (UINT1) PNAC_PORTSTATUS_AUTHORIZED);
            }
            else
            {
                /* Serious Problem Here. New Data traffic will flow through.
                 * So, Try to Avoid by Reprogramming the hardware limited 
                 * number of times. */
                PnacNpSetAuthStatusRetry (u2Port, SuppMacAddr,
                                          u1PnacPortCtrlDir,
                                          PNAC_PORT_AUTHMODE_PORTBASED,
                                          u1PnacPortAuthStatus);
            }
        }
    }

    return;
}

/*****************************************************************************
 * Function Name      : PnacProcNpAuthCbMacBased
 *                                                                           
 * Description        : This Function handles the Call back for NPAPI        
 *                      PnacHwSetAuthStatus for MAC Based Approach.          
 *                      It is called by PnacProcNpCallback. 
 *                      If the NP Call is  Success, It Sets the Port Status 
 *                      in L2IWF depending on the authorization status
 *                      Else if the NP Call is Failure, it reinitiates the 
 *                      state machine or retries the hardware programming 
 *                      depending on the authorization status.                      
 *                                                                           
 * Input(s)           : u2Port               - Port Number.
 *                      SuppMacAddr          - Supplicant Mac Address.
 *                      u1PnacMacAuthStatus  - Mac Auth Status.
 *                      i4NpRetVal           - Return Value of NP Call.
 *                                                                           
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : None                                                 
 *                                                                           
 ****************************************************************************/
VOID
PnacProcNpAuthCbMacBased (UINT2 u2Port, tMacAddr SuppMacAddr,
                          UINT1 u1PnacMacAuthStatus, INT4 i4NpRetVal)
{
    tPnacAuthSessionNode *pAuthSessNode = NULL;

    UINT1               u1PnacAsyncNpRetryCount = 0;

    if (u1PnacMacAuthStatus == PNAC_PORTSTATUS_AUTHORIZED)
    {
        if (PnacAuthGetSessionTblEntryByMac (SuppMacAddr, &pAuthSessNode)
            == PNAC_FAILURE)
        {
            return;
        }
    }
    else if (u1PnacMacAuthStatus == PNAC_PORTSTATUS_UNAUTHORIZED)
    {
        if (PnacAuthGetAuthInProgressTblEntryByMac (SuppMacAddr,
                                                    &pAuthSessNode)
            == PNAC_FAILURE)
        {
            return;
        }
    }

    if (pAuthSessNode == NULL)
    {
        return;
    }

    switch (i4NpRetVal)
    {
        case FNP_SUCCESS:
            /* NP Call has Succeeded for MAC Based Approach */
            /* Hence, resetting the retry count variable for the Port */

            pAuthSessNode->u1PnacAsyncNpRetryCount = 0;

            /* Check the Authorization Status. */
            if (u1PnacMacAuthStatus == PNAC_PORTSTATUS_AUTHORIZED)
            {
                /* Since the Port Status is Authorized, 
                 * Setting the Port Status Value in L2IWF as AUTHORISED */
                PnacL2IwfSetSuppMacAuthStatus (SuppMacAddr,
                                               PNAC_PORTSTATUS_AUTHORIZED);
            }
            else if (u1PnacMacAuthStatus == PNAC_PORTSTATUS_UNAUTHORIZED)
            {
                /* Since the Port Status is Unauthorized, Not Doing Anything 
                 * here, as L2IWF Port Status is already set. */
            }
            break;
        case FNP_FAILURE:
            /* NP Call has failed for MAC Based Approach */

            /* Check the Authorization Status. */
            if (u1PnacMacAuthStatus == PNAC_PORTSTATUS_AUTHORIZED)
            {
                u1PnacAsyncNpRetryCount =
                    pAuthSessNode->u1PnacAsyncNpRetryCount;

                if (u1PnacAsyncNpRetryCount >= PNAC_ASYNC_NP_RETRY_MAX)
                {
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              " INF: Retry Count Exceeded to Program the NP"
                              " Hence, Logging Error Message and Sending Trap...\n");

                    /* AuthSession is removed from the AIP Table here. */
                    PnacAuthDeleteAuthInProgressTblEntry (pAuthSessNode);

                    PnacNpFailNotifyAndLog (u2Port, SuppMacAddr,
                                            PNAC_CNTRLD_DIR_BOTH,
                                            PNAC_PORT_AUTHMODE_MACBASED,
                                            u1PnacMacAuthStatus);
                    return;
                }

                /* Incrementing the Retry Count */
                pAuthSessNode->u1PnacAsyncNpRetryCount++;

                /* Since the Port Status is Authorised,
                 * ReInitiating the State Machine and Hope the NP Call
                 * will succeed */

                PnacAuthStateMachine (PNAC_ASM_EV_INITIALIZE,
                                      u2Port, pAuthSessNode, NULL, PNAC_NO_VAL);
            }
            else if (u1PnacMacAuthStatus == PNAC_PORTSTATUS_UNAUTHORIZED)
            {
                /* Since the Port Status is UnAuthorised,
                 * Retrying the NP Call Limited Number of Times, to succeed */

                PnacNpSetAuthStatusRetry (u2Port, SuppMacAddr,
                                          PNAC_CNTRLD_DIR_BOTH,
                                          PNAC_PORT_AUTHMODE_MACBASED,
                                          PNAC_PORTSTATUS_UNAUTHORIZED);
            }
            break;
        default:
            /* Not Possible */
            break;
    }
    return;
}

/*****************************************************************************
 * Function Name      : PnacNpSetAuthStatusRetry                       
 *                                                                           
 * Description        : This Function reprogrammes the hardware by calling 
 *                      NPAPI PnacHwSetAuthStatus for Port/MAC Based Approach
 *                      till the retry count does not the exceed the maximum
 *                      retry count. If retry count exceeds the maximum limit,
 *                      this function logs the error message in syslog and 
 *                      sends a notification to the SNMP Manager.    
 *                                                                           
 * Input(s)           : u2Port - Port Number.
 *                      SuppMacAddr - Supplicant Mac Address.
 *                      u1PnacPortCtrlDir - Port Control Direction
 *                      u1PnacAuthMode - Authentication Mode
 *                      u1PnacAuthStatus - Authorization Status
 *                                                                           
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : None                                                 
 *                                                                           
 ****************************************************************************/
VOID
PnacNpSetAuthStatusRetry (UINT2 u2Port, tMacAddr SuppMacAddr,
                          UINT1 u1PnacPortCtrlDir, UINT1 u1PnacAuthMode,
                          UINT1 u1PnacAuthStatus)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    UINT1               u1CurrentAuthStatus = 0;

    if (PnacGetPortEntry (u2Port, &pPortInfo) == PNAC_FAILURE)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " INF: Invalid Port ...\n");
        return;
    }

    if (u1PnacAuthMode == PNAC_PORT_AUTHMODE_PORTBASED)
    {
        if (pPortInfo->u1PnacAsyncNpRetryCount >= PNAC_ASYNC_NP_RETRY_MAX)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " INF: Retry Count Exceeded to Program the NP"
                      " Hence, Logging Error Message and Sending Trap...\n");

            PnacNpFailNotifyAndLog (u2Port,
                                    SuppMacAddr, u1PnacPortCtrlDir,
                                    u1PnacAuthMode, u1PnacAuthStatus);
            return;
        }

        /* Incrementing the Retry Count */
        pPortInfo->u1PnacAsyncNpRetryCount++;
    }
    else if (u1PnacAuthMode == PNAC_PORT_AUTHMODE_MACBASED)
    {
        if (u1PnacAuthStatus == PNAC_PORTSTATUS_AUTHORIZED)
        {
            if (PnacAuthGetSessionTblEntryByMac (SuppMacAddr, &pAuthSessNode)
                == PNAC_FAILURE)
            {
                return;
            }
        }
        else if (u1PnacAuthStatus == PNAC_PORTSTATUS_UNAUTHORIZED)
        {
            if (PnacAuthGetAuthInProgressTblEntryByMac (SuppMacAddr,
                                                        &pAuthSessNode)
                == PNAC_FAILURE)
            {
                return;
            }
        }

        if (pAuthSessNode == NULL)
        {
            return;
        }

        if (pAuthSessNode->u1PnacAsyncNpRetryCount >= PNAC_ASYNC_NP_RETRY_MAX)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " INF: Retry Count Exceeded to Program the NP"
                      " Hence, Logging Error Message and Sending Trap...\n");

            /* AuthSession is removed from the AIP Table here. */
            PnacAuthDeleteAuthInProgressTblEntry (pAuthSessNode);

            PnacNpFailNotifyAndLog (u2Port, SuppMacAddr,
                                    u1PnacPortCtrlDir, u1PnacAuthMode,
                                    u1PnacAuthStatus);
            return;
        }

        /* Incrementing the Retry Count */
        pAuthSessNode->u1PnacAsyncNpRetryCount++;
    }

    if (u1PnacAuthStatus == PNAC_PORTSTATUS_UNAUTHORIZED)
    {
        /* Verifying whether Port Status Transition has happened
         * in the state machine. If it has happened, No need to 
         * Reprogram. */
        if (u1PnacAuthMode == PNAC_PORT_AUTHMODE_PORTBASED)
        {
            u1CurrentAuthStatus = (UINT1) PnacGetCurrentPortStatus (u2Port);
        }
        else if (u1PnacAuthMode == PNAC_PORT_AUTHMODE_MACBASED)
        {
            u1CurrentAuthStatus =
                pAuthSessNode->authFsmInfo.u2AuthControlPortStatus;
        }

        if (u1CurrentAuthStatus == PNAC_PORTSTATUS_AUTHORIZED)
        {
            return;
        }
    }

    /* Reprogramming the Hardware */
#ifdef NPAPI_WANTED
    if (PNAC_IS_NP_PROGRAMMING_ALLOWED () == PNAC_TRUE)
    {
        if (PnacPnacHwSetAuthStatus (u2Port, SuppMacAddr, u1PnacAuthMode,
                                     u1PnacAuthStatus, u1PnacPortCtrlDir)
            != FNP_SUCCESS)
        {
            return;
        }
    }
#endif /* NPAPI_WANTED */

    return;
}

/*****************************************************************************
 * Function Name      : PnacNpSetMacSessStatusRetry                       
 *                                                                           
 * Description        : This Function reprogrammes the hardware by calling 
 *                      NPAPI PnacHwAddOrDelSessStatus for MAC Based Approach
 *                      till the retry count does not the exceed the maximum
 *                      retry count. If retry count exceeds the maximum limit,
 *                      this function logs the error message in syslog and 
 *                      sends a notification to the SNMP Manager.    
 *                                                                           
 * Input(s)           : u2Port           - Port Number.
 *                      SuppMacAddr      - Supplicant Mac Address.
 *                      u1PnacSessStatus - Authorization Status.
 *                                                                           
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : None                                                 
 *                                                                           
 ****************************************************************************/
VOID
PnacNpSetMacSessStatusRetry (UINT2 u2Port, tMacAddr SuppMacAddr,
                             UINT1 u1PnacSessStatus)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacNpFailedMacSessNode *pNpFailedMacSessNode = NULL;

    if (PnacGetPortEntry (u2Port, &pPortInfo) == PNAC_FAILURE)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " INF: Invalid Port ...\n");
        return;
    }

    if (PnacGetNpFailedMacSessNodeByMac (SuppMacAddr, &pNpFailedMacSessNode)
        == PNAC_FAILURE)
    {
        if ((pNpFailedMacSessNode =
             PnacAddToNpFailedMacSessTree (SuppMacAddr, PNAC_NO_VAL)) == NULL)
        {
            return;
        }
    }

    if (pNpFailedMacSessNode->u1PnacAsyncNpRetryCount
        >= PNAC_ASYNC_NP_RETRY_MAX)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " INF: Retry Count Exceeded to Program the NP"
                  " Hence, Logging Error Message and Sending Trap...\n");

        PnacNpFailNotifyAndLog (u2Port, SuppMacAddr,
                                (UINT1) PNAC_CNTRLD_DIR_BOTH,
                                (UINT1) PNAC_PORT_AUTHMODE_MACBASED,
                                u1PnacSessStatus);

        PnacDelFromNpFailedMacSessTree (pNpFailedMacSessNode);

        return;
    }

    /* Incrementing the Retry Count */
    pNpFailedMacSessNode->u1PnacAsyncNpRetryCount++;

    /* Reprogramming the Hardware */
#ifdef NPAPI_WANTED
    if (PNAC_IS_NP_PROGRAMMING_ALLOWED () == PNAC_TRUE)
    {
        if (PnacPnacHwAddOrDelMacSess (u2Port, SuppMacAddr, PNAC_MAC_SESS_DEL)
            != FNP_SUCCESS)
        {
            return;
        }
    }
#endif /* NPAPI_WANTED */

    return;
}

/*****************************************************************************
 * Function Name      : PnacNpFailNotifyAndLog                      
 *                                                                           
 * Description        : This Function sends notification to the SNMP Manager
 *                      and Logs the Failure Happened in hardware.    
 *                                                                           
 * Input(s)           : u2Port            - Port Number.
 *                      SuppMacAddr       - Supplicant Mac Address.
 *                      u1PnacAuthCtrlDir - Port Control Dir.
 *                      u1PnacAuthMode    - Pnac Authentication Mode.
 *                      u1PnacAuthStatus  - Authorization Status.
 *                                                                           
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : None                                                 
 *                                                                           
 ****************************************************************************/
VOID
PnacNpFailNotifyAndLog (UINT2 u2Port, UINT1 *pSuppMacAddr,
                        UINT1 u1PnacAuthCtrlDir, UINT1 u1PnacAuthMode,
                        UINT1 u1PnacAuthStatus)
{
    tPnacNpTrapLogInfo  PnacNpTrapLogInfo;

    MEMSET (&PnacNpTrapLogInfo, PNAC_NO_VAL, sizeof (tPnacNpTrapLogInfo));

    PnacNpTrapLogInfo.u2Port = u2Port;

    if ((u1PnacAuthMode == PNAC_PORT_AUTHMODE_MACBASED) &&
        (pSuppMacAddr != NULL))
    {
        MEMCPY (PnacNpTrapLogInfo.SuppMacAddr, pSuppMacAddr, sizeof (tMacAddr));
    }
    PnacNpTrapLogInfo.u1PnacPortCtrlDir = u1PnacAuthCtrlDir;
    PnacNpTrapLogInfo.u1PnacAuthMode = u1PnacAuthMode;
    PnacNpTrapLogInfo.u1PnacAuthStatus = u1PnacAuthStatus;

    PnacNpFailNotify (&PnacNpTrapLogInfo);

    PnacNpFailLog (&PnacNpTrapLogInfo);

    return;
}
#endif /* NPAPI_WANTED */
