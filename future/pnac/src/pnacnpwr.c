
/* SOURCE FILE HEADER :
 *
 * $Id: pnacnpwr.c,v 1.3 2013/11/14 11:32:29 siva Exp $
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : pnacnpwr.c                                        |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : Jeeva Sethuraman                                 |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : PNAC                                             |
 * |                                                                           |
 * |  MODULE NAME           : PNAC NP Wrapper configurations                   |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : All  Network Processor API Function              |
 * |                          calls are done here                              |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

#ifndef __PNACNPWR_C__
#define __PNACNPWR_C__

#include "pnachdrs.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : PnacNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tPnacNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
PnacNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tPnacNpModInfo     *pPnacNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pPnacNpModInfo = &(pFsHwNp->PnacNpModInfo);

    if (NULL == pPnacNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
#ifdef L2RED_WANTED
        case FS_PNAC_RED_HW_UPDATE_D_B:
        {
            tPnacNpWrFsPnacRedHwUpdateDB *pEntry = NULL;
            pEntry = &pPnacNpModInfo->PnacNpFsPnacRedHwUpdateDB;
            u1RetVal =
                FsPnacRedHwUpdateDB ((UINT2) pEntry->u4Port,
                                     pEntry->pu1SuppAddr, pEntry->u1AuthMode,
                                     pEntry->u1AuthStatus, pEntry->u1CtrlDir);
            break;
        }
#endif /* L2RED_WANTED */
        case PNAC_HW_ADD_OR_DEL_MAC_SESS:
        {
            tPnacNpWrPnacHwAddOrDelMacSess *pEntry = NULL;
            pEntry = &pPnacNpModInfo->PnacNpPnacHwAddOrDelMacSess;
            u1RetVal =
                PnacHwAddOrDelMacSess ((UINT2) pEntry->u4Port,
                                       pEntry->pu1SuppAddr,
                                       pEntry->u1SessStatus);
            break;
        }
        case PNAC_HW_DISABLE:
        {
            u1RetVal = PnacHwDisable ();
            break;
        }
        case PNAC_HW_ENABLE:
        {
            u1RetVal = PnacHwEnable ();
            break;
        }
        case PNAC_HW_GET_AUTH_STATUS:
        {
            tPnacNpWrPnacHwGetAuthStatus *pEntry = NULL;
            pEntry = &pPnacNpModInfo->PnacNpPnacHwGetAuthStatus;
            u1RetVal =
                PnacHwGetAuthStatus ((UINT2) pEntry->u4PortNum,
                                     pEntry->pu1SuppAddr, pEntry->u1AuthMode,
                                     pEntry->pu2AuthStatus, pEntry->pu2CtrlDir);
            break;
        }
        case PNAC_HW_GET_SESSION_COUNTER:
        {
            tPnacNpWrPnacHwGetSessionCounter *pEntry = NULL;
            pEntry = &pPnacNpModInfo->PnacNpPnacHwGetSessionCounter;
            u1RetVal =
                PnacHwGetSessionCounter ((UINT2) pEntry->u4Port,
                                         pEntry->pu1SuppAddr,
                                         pEntry->u1CounterType,
                                         pEntry->pu4HiCounter,
                                         pEntry->pu4LoCounter);
            break;
        }
        case PNAC_HW_SET_AUTH_STATUS:
        {
            tPnacNpWrPnacHwSetAuthStatus *pEntry = NULL;
            pEntry = &pPnacNpModInfo->PnacNpPnacHwSetAuthStatus;
            u1RetVal =
                PnacHwSetAuthStatus ((UINT2) pEntry->u4Port,
                                     pEntry->pu1SuppAddr, pEntry->u1AuthMode,
                                     pEntry->u1AuthStatus, pEntry->u1CtrlDir);
            break;
        }
        case PNAC_HW_START_SESSION_COUNTERS:
        {
            tPnacNpWrPnacHwStartSessionCounters *pEntry = NULL;
            pEntry = &pPnacNpModInfo->PnacNpPnacHwStartSessionCounters;
            u1RetVal =
                PnacHwStartSessionCounters ((UINT2) pEntry->u4Port,
                                            pEntry->pu1SuppAddr);
            break;
        }
        case PNAC_HW_STOP_SESSION_COUNTERS:
        {
            tPnacNpWrPnacHwStopSessionCounters *pEntry = NULL;
            pEntry = &pPnacNpModInfo->PnacNpPnacHwStopSessionCounters;
            u1RetVal =
                PnacHwStopSessionCounters ((UINT2) pEntry->u4Port,
                                           pEntry->pu1SuppAddr);
            break;
        }
#ifdef MBSM_WANTED
        case PNAC_MBSM_HW_ENABLE:
        {
            tPnacNpWrPnacMbsmHwEnable *pEntry = NULL;
            pEntry = &pPnacNpModInfo->PnacNpPnacMbsmHwEnable;
            u1RetVal = PnacMbsmHwEnable (pEntry->pSlotInfo);
            break;
        }
        case PNAC_MBSM_HW_SET_AUTH_STATUS:
        {
            tPnacNpWrPnacMbsmHwSetAuthStatus *pEntry = NULL;
            pEntry = &pPnacNpModInfo->PnacNpPnacMbsmHwSetAuthStatus;
            u1RetVal =
                PnacMbsmHwSetAuthStatus ((UINT2) pEntry->u4Port,
                                         pEntry->pu1SuppAddr,
                                         pEntry->u1AuthMode,
                                         pEntry->u1AuthStatus,
                                         pEntry->u1CtrlDir, pEntry->pSlotInfo);
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

#endif /* __PNACNPWR_C__ */
