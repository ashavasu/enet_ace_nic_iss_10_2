/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pnacauth.c,v 1.45 2015/12/14 10:59:16 siva Exp $
 *
 * Description     : This file contains Authentication 
 *                   functions of PNAC module
 *******************************************************************/

#include "pnachdrs.h"
#include "rstp.h"
#include "mstp.h"
#ifdef WPS_WANTED
#include "wps.h"
#endif

/*****************************************************************************/
/* Function Name      : PnacInitAuthenticator                                */
/*                                                                           */
/* Description        : Initialises the authenticator related functionalities*/
/*                      for the port.                                        */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number for which the Authenticator  */
/*                                  is to be initialised                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS/PNAC_FAILURE                            */
/*****************************************************************************/
INT4
PnacInitAuthenticator (UINT2 u2PortNum)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    UINT1               au1MacAddr[PNAC_MAC_ADDR_SIZE] = {0x01, 0x80, 0xC2, 0x00, 0x00, 0x00};

    if (PNAC_ALLOCATE_AUTHSESSNODE_MEMBLK (pAuthSessNode) == NULL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PNACAUTH: Allocate authsessnode returned failure\n");
        return PNAC_FAILURE;
    }
    PNAC_DEBUG (PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC,
                               " InitAuth: Allocated Session Node : %x ... \n",
                               pAuthSessNode);
        );

    PNAC_MEMSET (pAuthSessNode, PNAC_INIT_VAL, sizeof (tPnacAuthSessionNode));

    /* Initialise the hash node */
    PNAC_HASH_INIT_NODE (&pAuthSessNode->nextAuthSessionNode);

    /* Fill the source mac address of the supplicant as EAPOL Address */
    PNAC_MEMCPY (pAuthSessNode->rmtSuppMacAddr,
                 gPnacEapolGrpMacAddr, PNAC_MAC_ADDR_SIZE);

    PnacAuthSessionNodeInit (pAuthSessNode, u2PortNum);
    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  "PnacInitAuthenticator:Invalid" "Port Number\n");
        return PNAC_FAILURE;
    }
    if (PNAC_IS_PORT_MODE_PORTBASED (pPortInfo))
    {
      /*********************************************************************/
        /* Insert this node in the Port Table entry since this node supports */
        /* Port based authentication mode                                    */
      /*********************************************************************/
        pPortInfo->pPortAuthSessionNode = pAuthSessNode;

    }                            /* end if Authmode port based */
    else
    {
        /* Authentication Mode for the port is MAC based */   

        /* Act as like port-based if port-control is   
           force-authorized/force-unauthorized   
         */   

        if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl !=   
            PNAC_PORTCNTRL_AUTO)   
        {   
            pPortInfo->pPortAuthSessionNode = pAuthSessNode;   
        }   

        /*********************************************************************/   
        /* Insert this node in the Authentication In Progress Table since    */   
        /* this port supports MAC Based Authentication Mode                  */   
        /*********************************************************************/   
        else
        {   
            /* Get a Unique Mac for the table addition.
               Later this Mac will be replaced by the source
               mac which replies for the Request Identity.
               The Mac is derived (01:80:C2:00:) + 2 bytes port num(00:00 to FF:FF).
             */
            
            PNAC_DERIVE_UNIQ_MAC (au1MacAddr, u2PortNum);

            PNAC_MEMCPY (pAuthSessNode->rmtSuppMacAddr, 
                         au1MacAddr, PNAC_MAC_ADDR_SIZE);

            (pPortInfo->u2SuppSessionCount) ++;

            if (PnacAuthAddToAuthInProgressTbl (pAuthSessNode) != PNAC_SUCCESS)   
            {   
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,   
                          " PNACAUTH: PnacAuthAddToAIPTbl returned failure\n");   

                /* Decrement the Session Count */
                if (pPortInfo->u2SuppSessionCount)
                {
                    (pPortInfo->u2SuppSessionCount) --;
                }

                if (PNAC_RELEASE_AUTHSESSNODE_MEMBLK (pAuthSessNode)   
                    != PNAC_MEM_SUCCESS)   
                {   
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,   
                              " PNACAUTH: Releasing Auth Session Node failed\n");   
                }   

                return PNAC_FAILURE;   
            }   
        }
    }                            /* end if Auth mode mac based */

    if (pPortInfo->u1PortPaeStatus == PNAC_ENABLED)
    {
        if (PnacAuthStateMachine (PNAC_ASM_EV_INITIALIZE,
                                  u2PortNum,
                                  pAuthSessNode, NULL, PNAC_NO_VAL) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " InitAuth: ASM returned failure\n");
            return PNAC_FAILURE;
        }

        if (PnacAuthBackendStateMachine (PNAC_BSM_EV_INITIALIZE,
                                         u2PortNum,
                                         pAuthSessNode,
                                         NULL, PNAC_NO_VAL, NULL) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " InitAuth: BSM returned failure\n");
            return PNAC_FAILURE;
        }
        PNAC_DEBUG (PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                              " InitAuth: InitAuth Successful for the Port... \n");
                   );
    }
    return PNAC_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : PnacAuthSessionNodeInit                              */
/*                                                                           */
/* Description        :  Initialises the Authenticator Session               */
/*                       Node except for the Source MAC Address field        */
/*                                                                           */
/* Input(s)           : pAuthSessNode - Pointer to authenticator session     */
/*                                      Node.                                */
/*                      u2PortNum - Port number.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
PnacAuthSessionNodeInit (tPnacAuthSessionNode * pAuthSessNode, UINT2 u2PortNum)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacAuthFsmInfo   *pFsmInfo = NULL;

    /* Initialise the Authenticator Fsm information */

    pFsmInfo = &(pAuthSessNode->authFsmInfo);
    PnacAuthFsmInfoInit (pFsmInfo);

    if (PnacGetPortEntry (u2PortNum, &pPortInfo) == PNAC_SUCCESS)
    {
        if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl
            == PNAC_PORTCNTRL_FORCEAUTHORIZED)
        {
            pFsmInfo->u2AuthControlPortStatus = PNAC_PORTSTATUS_AUTHORIZED;
            if (pPortInfo->u1EntryStatus == PNAC_PORT_OPER_UP)
            {
                /*If Entrystatus is Up then only give Authorized indication up
                 * to L2Iwf*/

                /*If the final port status is changed then only update l2iwf */
                if (PnacCheckIfPortStatusChanged (u2PortNum) == PNAC_SUCCESS)
                {
                    PnacL2IwfSetPortPnacAuthStatus (u2PortNum, gPnacSystemInfo.
                                                    aPnacPaePortInfo[u2PortNum].
                                                    u2PortStatus);
                }
            }
        }
    }

    PnacAuthSessStatsInit (pAuthSessNode, u2PortNum);

    pAuthSessNode->keyInfo.u2KeyLength = (UINT2) PNAC_INIT_VAL;

    PNAC_MEMSET (pAuthSessNode->keyInfo.au1Key, PNAC_INIT_VAL,
                 PNAC_MAX_SERVER_KEY_SIZE);

    pAuthSessNode->pAWhileTimer = (tPnacTimerNode *) NULL;
    pAuthSessNode->pQuietWhileTimer = (tPnacTimerNode *) NULL;
    pAuthSessNode->pReAuthWhenTimer = (tPnacTimerNode *) NULL;
    pAuthSessNode->pTxWhenTimer = (tPnacTimerNode *) NULL;
    pAuthSessNode->pReqMesg = (tPnacCruBufChainHdr *) NULL;
    pAuthSessNode->u2PortNo = u2PortNum;
    pAuthSessNode->u1IsAuthorizedOnce = PNAC_FALSE;    /* Say is the node is not authorized atleast once */

    pAuthSessNode->bIsRsnPreAuthSess = OSIX_FALSE;
#ifdef WPS_WANTED
    pAuthSessNode->bIsWpsPreAuthSess = OSIX_FALSE;
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : PnacAuthSessStatsInit                                */
/*                                                                           */
/* Description        : Initialises the Authenticator Session                */
/*                      Statistics information in the session node           */
/*                                                                           */
/* Input(s)           : pAuthSessNode - Pointer to the authenticator         */
/*                                      session node that is to be           */
/*                                      initialised.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
PnacAuthSessStatsInit (tPnacAuthSessionNode * pAuthSessNode, UINT2 u2PortNum)
{
    tPnacAuthSessStats *pSessStats = NULL;
    UINT1               au1UniqueString[PNAC_SESSION_ID_STRING_SIZE];
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacAuthFsmInfo   *pAuthFsmInfo = NULL;

    pSessStats = &(pAuthSessNode->authSessStats);

    PNAC_UINT8_RESET (pSessStats->u8SessOctetsRx);
    PNAC_UINT8_RESET (pSessStats->u8SessOctetsTx);
    pSessStats->u4SessFramesRx = PNAC_INIT_VAL;
    pSessStats->u4SessFramesTx = PNAC_INIT_VAL;

    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        return;
    }
    pAuthFsmInfo = &(pAuthSessNode->authFsmInfo);
    PNAC_MEMSET (au1UniqueString, PNAC_INIT_VAL, sizeof (au1UniqueString));

    if (PNAC_IS_PORT_MODE_PORTBASED (pPortInfo))
    {
        PNAC_SPRINTF ((CHR1 *) au1UniqueString, "%d-%d", u2PortNum,
                      pAuthFsmInfo->u2CurrentId);
    }
    else
    {
        PNAC_SPRINTF ((CHR1 *) au1UniqueString, "%d-%d-", u2PortNum,
                      pAuthFsmInfo->u2CurrentId);
        PnacCpyMacAddrToStr (&au1UniqueString[PNAC_STRLEN (au1UniqueString)],
                             pAuthSessNode->rmtSuppMacAddr);
    }
    pSessStats->u2SessIdLen = (UINT2) PNAC_STRLEN (au1UniqueString);
    PNAC_STRNCPY (pSessStats->aSessId, au1UniqueString, STRLEN(au1UniqueString));
    pSessStats->aSessId[STRLEN(au1UniqueString)] = '\0';

    pSessStats->u4SessTime = PNAC_INIT_VAL;
    pSessStats->u4SessTerminateCause =
        PNAC_SESS_TERMINATE_CAUSE_PORT_ADMINDISABLED;
    pSessStats->u2SessUserNameLen = (UINT2) PNAC_STRLEN ("No User");
    PNAC_MEMCPY (pSessStats->au1SessUserName, "No User",
                 pSessStats->u2SessUserNameLen);
    return;
}

/*****************************************************************************/
/* Function Name      : PnacAuthFsmInfoInit                                  */
/*                                                                           */
/* Description        : This routine initialises the Authenticator State     */
/*                      machine variables                                    */
/*                                                                           */
/* Input(s)           : pFsmInfo - Pointer to the Authenticator FSM          */
/*                                 information structure.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
PnacAuthFsmInfoInit (tPnacAuthFsmInfo * pFsmInfo)
{

    /* Initialise Authenticator & Backed FSM Informations */

    pFsmInfo->u4ReAuthCount = PNAC_INIT_VAL;
    pFsmInfo->u4ReqCount = PNAC_INIT_VAL;
    pFsmInfo->u2CurrentId = PNAC_INIT_VAL;
    pFsmInfo->u2IdFromServer = PNAC_INIT_VAL;

    pFsmInfo->u2AuthPaeState = PNAC_ASM_STATE_INITIALIZE;
    pFsmInfo->u2AuthBackendAuthState = PNAC_BSM_STATE_INITIALIZE;
    pFsmInfo->u2PortMode = PNAC_PORTCNTRL_AUTO;
    pFsmInfo->u2AuthControlPortStatus = PNAC_PORTSTATUS_UNAUTHORIZED;
    pFsmInfo->bInitialize = PNAC_FALSE;
    pFsmInfo->bAuthStart = PNAC_FALSE;
    pFsmInfo->bAuthAbort = PNAC_FALSE;
    pFsmInfo->bAuthSuccess = PNAC_FALSE;
    pFsmInfo->bAuthFail = PNAC_FALSE;
    pFsmInfo->bAuthTimeout = PNAC_FALSE;
    pFsmInfo->bKeyAvailable = PNAC_FALSE;
    pFsmInfo->bEapLogoff = PNAC_FALSE;
    pFsmInfo->bEapStart = PNAC_FALSE;
    pFsmInfo->bRxRespId = PNAC_FALSE;
    pFsmInfo->bRxResp = PNAC_FALSE;
    pFsmInfo->bASuccess = PNAC_FALSE;
    pFsmInfo->bAFail = PNAC_FALSE;
    pFsmInfo->bAreq = PNAC_FALSE;
    pFsmInfo->bPrevEapLogoff = PNAC_FALSE;
    pFsmInfo->bReAuthenticate = PNAC_FALSE;
    return;

}

/*****************************************************************************/
/* Function Name      : PnacAuthCreateSession                                */
/*                                                                           */
/* Description        : This function creates a new authentication session   */
/*                                                                           */
/* Input(s)           : newAddr - The new MAC address of the source for      */
/*                                 which a new session is to be started      */
/*                      u2PortNum - Port Number.                             */
/*                      u2EnetType :- RsnaPreAuth/Pnac Pae Frame             */
/*                      bIsPreAuthSess:- Is this session a PreAuth Session   */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/

INT4
PnacAuthCreateSession (tMacAddr newAddr, UINT2 u2PortNum, BOOL1 bIsPreAuthSess)
{
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;
    UINT4               u4MacAddr = 0;
    UINT2               u2MacAddr = 0;
    UINT2               u2PreAuthIntf = PNAC_MAXPORTS;

    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " AuthCreateSession: Error: Port Entry Invalid... \n");
        return PNAC_FAILURE;
    }

    if (PNAC_IS_PORT_MODE_MACBASED (pPortInfo))
    {
        if (pPortInfo->u2SuppSessionCount >= PNAC_MAX_SUPP_FOR_PORT)
        {
            PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC," AuthCreateSession: Maximum Supplicant Supported on Port: %u"
                           " Reached... \n",u2PortNum);
            return PNAC_FAILURE;
        }
    }

    PNAC_GET_FOURBYTE (u4MacAddr, newAddr);
    PNAC_GET_TWOBYTE (u2MacAddr, &newAddr[4]);
    PNAC_DEBUG (PNAC_TRC_ARG3 (PNAC_CONTROL_PATH_TRC,
                               " Trying to create a session for MAC: -%x-%x- thro' Port: %d ... \n",
                               u4MacAddr, u2MacAddr, u2PortNum);
        );

    if (PNAC_ALLOCATE_AUTHSESSNODE_MEMBLK (pAuthSessNode) == NULL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " CreateSession: Allocate auth sess node failed \n");
        return PNAC_FAILURE;
    }
    PNAC_DEBUG (PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC,
                               " Allocated Session Node : %x \n",
                               pAuthSessNode);
        );
    PNAC_MEMSET (pAuthSessNode, PNAC_INIT_VAL, sizeof (tPnacAuthSessionNode));

    if (bIsPreAuthSess == OSIX_TRUE)
    {
        /* Attach the PreAuthSession to Virtual Interface */
        /* Virtual Interface will be the last Port */
        /* Store the Actual Port in Physical Port field */
        PnacAuthSessionNodeInit (pAuthSessNode, u2PreAuthIntf);
        pAuthSessNode->u2PreAuthPhyPort = u2PortNum;
#ifdef WPS_WANTED
        pAuthSessNode->bIsWpsPreAuthSess = OSIX_TRUE;
#endif
        pAuthSessNode->bIsRsnPreAuthSess = OSIX_TRUE;
    }
    else
    {
        PnacAuthSessionNodeInit (pAuthSessNode, u2PortNum);
    }

    /* Increment the Session Count */
    if (PNAC_IS_PORT_MODE_MACBASED (pPortInfo))
    {
        (pPortInfo->u2SuppSessionCount) ++;
    }
    
    PNAC_MEMCPY (pAuthSessNode->rmtSuppMacAddr, newAddr, PNAC_MAC_ADDR_SIZE);

    /* Insert this node in the Authentication In Progress Table since 
     * this port supports MAC Based Authentication Mode 
     */

    if (PnacAuthAddToAuthInProgressTbl (pAuthSessNode) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " CreateSession: Adding Node in AIP Table failed\n");
        PnacAuthDeleteSession (pAuthSessNode);
        return PNAC_FAILURE;
    }

    if (bIsPreAuthSess == OSIX_TRUE)
    {
        if (PnacAuthStateMachine (PNAC_ASM_EV_INITIALIZE,
                                  u2PreAuthIntf,
                                  pAuthSessNode, NULL,
                                  PNAC_NO_VAL) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " CreateSession: ASM returned failure\n");
            PnacAuthDeleteSession (pAuthSessNode);
            return PNAC_FAILURE;
        }
        if (PnacAuthBackendStateMachine (PNAC_BSM_EV_INITIALIZE,
                                         u2PreAuthIntf, pAuthSessNode,
                                         NULL, PNAC_NO_VAL,
                                         NULL) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " CreateSession: BSM returned failure\n");
            PnacAuthDeleteSession (pAuthSessNode);
        }

        PNAC_GET_FOURBYTE (u4MacAddr, newAddr);
        PNAC_GET_TWOBYTE (u2MacAddr, &newAddr[4]);
        PNAC_DEBUG (PNAC_TRC_ARG3 (PNAC_CONTROL_PATH_TRC,
                                   " Session Created for MAC: -%x-%x- through Port: %d ... \n",
                                   u4MacAddr, u2MacAddr, u2PreAuthIntf);
            );
    }
    else
    {

        if (PnacAuthStateMachine (PNAC_ASM_EV_INITIALIZE,
                                  u2PortNum,
                                  pAuthSessNode, NULL,
                                  PNAC_NO_VAL) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " CreateSession: ASM returned failure\n");
            PnacAuthDeleteSession (pAuthSessNode);
            return PNAC_FAILURE;
        }
        if (PnacAuthBackendStateMachine (PNAC_BSM_EV_INITIALIZE,
                                         u2PortNum, pAuthSessNode,
                                         NULL, PNAC_NO_VAL,
                                         NULL) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " CreateSession: BSM returned failure\n");
            PnacAuthDeleteSession (pAuthSessNode);
        }

        PNAC_GET_FOURBYTE (u4MacAddr, newAddr);
        PNAC_GET_TWOBYTE (u2MacAddr, &newAddr[4]);
        PNAC_DEBUG (PNAC_TRC_ARG3 (PNAC_CONTROL_PATH_TRC,
                                   " Session Created for MAC: -%x-%x- through Port: %d ... \n",
                                   u4MacAddr, u2MacAddr, u2PortNum);
            );
    }

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacAuthDeleteSession                                */
/*                                                                           */
/* Description        : Deletes an authenticator session related resources   */
/*                      and information.                                     */
/*                                                                           */
/* Input(s)           : pAuthSessNode - Pointer to the authenticator session */
/*                                      Node which is to be deleted.         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
PnacAuthDeleteSession (tPnacAuthSessionNode * pAuthSessNode)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    UINT4               u4Index = PNAC_INIT_VAL;
    UINT2               u2PortNum = PNAC_INIT_VAL;
    UINT4               u4MacAddr = 0;
    UINT2               u2MacAddr = 0;

    PNAC_GET_FOURBYTE (u4MacAddr, pAuthSessNode->rmtSuppMacAddr);
    PNAC_GET_TWOBYTE (u2MacAddr, &(pAuthSessNode->rmtSuppMacAddr[4]));
    PNAC_DEBUG (PNAC_TRC_ARG3 (PNAC_CONTROL_PATH_TRC,
                               " Trying to delete the session for MAC: -%x-%x- on Port: %d ... \n",
                               u4MacAddr, u2MacAddr, pAuthSessNode->u2PortNo);
        );
    PNAC_DEBUG (PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC,
                               " Trying to delete the Session Node : %x ... \n",
                               pAuthSessNode);
        );

    if (pAuthSessNode->pAWhileTimer != NULL)
    {
        PnacTmrStopTmr ((VOID *) pAuthSessNode, PNAC_AWHILE_TIMER);
    }
    if (pAuthSessNode->pQuietWhileTimer != NULL)
    {
        PnacTmrStopTmr ((VOID *) pAuthSessNode, PNAC_QUIETWHILE_TIMER);
    }
    if (pAuthSessNode->pReAuthWhenTimer != NULL)
    {
        PnacTmrStopTmr ((VOID *) pAuthSessNode, PNAC_REAUTHWHEN_TIMER);
    }
    if (pAuthSessNode->pTxWhenTimer != NULL)
    {
        PnacTmrStopTmr ((VOID *) pAuthSessNode, PNAC_TXWHEN_TIMER);
    }
    if (pAuthSessNode->pReqMesg != NULL)
    {
        if (PNAC_RELEASE_CRU_BUF (pAuthSessNode->pReqMesg, PNAC_CRU_FALSE)
            != PNAC_CRU_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_BUFFER_TRC |
                      PNAC_ALL_FAILURE_TRC,
                      " AuthDeleteSession: RELEASE_CRU_BUF Failed\n");
        }
        pAuthSessNode->pReqMesg = NULL;
    }

    u2PortNum = pAuthSessNode->u2PortNo;
    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " AuthDeleteSession: Error: Port Entry Invalid... continuing to Release node still...\n");
        return;
    }
    if (PNAC_IS_PORT_MODE_MACBASED (pPortInfo))
    {
        /* Decrement the Session Count */
        if (pPortInfo->u2SuppSessionCount)
        {
            (pPortInfo->u2SuppSessionCount) --;
        }

        if (pAuthSessNode->authFsmInfo.u2AuthControlPortStatus
            == PNAC_PORTSTATUS_AUTHORIZED)
        {
            PnacAuthCalcSessionTblIndex (pAuthSessNode->rmtSuppMacAddr,
                                         &u4Index);
            PNAC_HASH_DEL_NODE (PNAC_AUTH_SESSION_TABLE,
                                &pAuthSessNode->nextAuthSessionNode, u4Index);
            PnacL2IwfSetSuppMacAuthStatus (pAuthSessNode->rmtSuppMacAddr,
                                           PNAC_PORTSTATUS_UNAUTHORIZED);
        }
        else
        {
            if (PnacAuthDeleteAuthInProgressTblEntry (pAuthSessNode)
                != PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " DeleteSession: Error: Node Not Present in AIP Table...Continuing\n");
            }
        }
#ifdef NPAPI_WANTED
        if (PNAC_IS_NP_PROGRAMMING_ALLOWED () == PNAC_TRUE)
        {
            if (PnacPnacHwAddOrDelMacSess (u2PortNum,
                                           pAuthSessNode->rmtSuppMacAddr,
                                           PNAC_MAC_SESS_DEL) != FNP_SUCCESS)
            {
                return;
            }

            /* Delete the MAC from H/w */
            if (pAuthSessNode->authFsmInfo.u2AuthControlPortStatus == PNAC_PORTSTATUS_AUTHORIZED)
            {
                if ((PNAC_MEMCMP (pAuthSessNode->rmtSuppMacAddr,
                                  gPnacEapolGrpMacAddr,
                                  PNAC_MAC_ADDR_SIZE)) != PNAC_NO_VAL)
                {
                    if (PnacPnacHwSetAuthStatus (u2PortNum,
                                                 pAuthSessNode->rmtSuppMacAddr,
                                                 pPortInfo->u1PortAuthMode,
                                                 PNAC_REMOVE_ALLOWED_MAC,
                                                 PNAC_CNTRLD_DIR_BOTH) != FNP_SUCCESS)

                    {
                        PnacNpFailNotifyAndLog (u2PortNum,
                                                pAuthSessNode->rmtSuppMacAddr,
                                                PNAC_CNTRLD_DIR_BOTH,
                                                pPortInfo->u1PortAuthMode,
                                                PNAC_REMOVE_ALLOWED_MAC);
                        return;
                    }
                }
            }
        }
#endif /* NPAPI_WANTED */
    }

    PNAC_GET_FOURBYTE (u4MacAddr, pAuthSessNode->rmtSuppMacAddr);
    PNAC_GET_TWOBYTE (u2MacAddr, &(pAuthSessNode->rmtSuppMacAddr[4]));
    PNAC_DEBUG (PNAC_TRC_ARG3 (PNAC_CONTROL_PATH_TRC,
                               " Session Deleted for MAC: -%x-%x- on Port: %d ... \n",
                               u4MacAddr, u2MacAddr, pAuthSessNode->u2PortNo);
        );

    PNAC_DEBUG (PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC,
                               " Deleted Session Node : %x ... \n",
                               pAuthSessNode););

    /* Now Free the memory occupied by this node */

    if (PNAC_RELEASE_AUTHSESSNODE_MEMBLK (pAuthSessNode) != PNAC_MEM_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " AuthDeleteSession: Release memblk failed\n");
    }
    return;
}

/*****************************************************************************/
/* Function Name      : PnacAuthTxReqId                                      */
/*                                                                           */
/* Description        : Forms an EAP request identity packet and transmits   */
/*                      it to the supplicant.                                */
/*                                                                           */
/* Input(s)           : destAddr - Pointer to the destination MAC address.   */
/*                      u2Id      - The session identifier to used in the    */
/*                                  EAP packet.                              */
/*                      u2PortNum - Port Number.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PnacAuthTxReqId (tMacAddr destAddr, UINT2 u2Id, UINT2 u2PortNum,
                 BOOL1 bIsPreAuthSess)
{
    tPnacEapPktHdr      eapHdrInfo;
    tPnacPaePortEntry  *pPortInfo = NULL;
    UINT1               u1Val = PNAC_INIT_VAL;
    UINT1              *pu1Buf = NULL;
    UINT1              *pu1BufPtr = NULL;

    if (PNAC_NODE_STATUS () != PNAC_ACTIVE_NODE)
    {
        /*Pkt transmission should be allowed only in Active node */
        return PNAC_SUCCESS;
    }

    if (PNAC_ALLOCATE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Unable to allocate memory. \r\n");
        return PNAC_FAILURE;
    }
    PNAC_MEMSET (pu1Buf, PNAC_INIT_VAL, PNAC_MAX_EAP_DATA_SIZE);
    PNAC_MEMSET (&eapHdrInfo, PNAC_INIT_VAL, sizeof (tPnacEapPktHdr));

    pu1BufPtr = pu1Buf;
    u1Val = PNAC_EAP_TYPE_IDENTITY;
    PNAC_PUT_1BYTE (pu1BufPtr, u1Val);
    PNAC_STRNCPY (pu1BufPtr, "User Name: ", STRLEN("User Name: "));
    pu1BufPtr[STRLEN("User Name: ")] = '\0';

    eapHdrInfo.u2DataLength = (UINT2) PNAC_STRLEN (pu1BufPtr);
    eapHdrInfo.pu1Data = pu1Buf;
    eapHdrInfo.u1Code = PNAC_EAP_CODE_REQ;
    eapHdrInfo.u1Identifier = (UINT1) u2Id;

    if (PnacPaeTxEapPkt (destAddr, u2PortNum, &eapHdrInfo, bIsPreAuthSess) ==
        PNAC_FAILURE)
    {
        if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == MEM_FAILURE)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      "Failed to release  memory. \r\n");
        }

        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PnacAuthTxReqId: PnacPaeTxEap returned Failure\n");
        return PNAC_FAILURE;
    }

    if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == MEM_FAILURE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Failed to release  memory. \r\n");
    }

    /* update stats */
    pPortInfo = &(PNAC_PORT_INFO (u2PortNum));

    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolReqIdFramesTx++;
    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolFramesTx++;
    PNAC_TRC (PNAC_CONTROL_PATH_TRC, " EAP Req Id Frame Sent !!!\n");

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacAuthTxReq                                        */
/*                                                                           */
/* Description        : Forms an EAP request packet and transmits            */
/*                      it to the supplicant.                                */
/*                                                                           */
/* Input(s)           : destAddr - Pointer to the destination MAC address.   */
/*                      u2Id      - The session identifier to used in the    */
/*                                  EAP packet.                              */
/*                      u2PortNum - Port Number.                             */
/*                      pMesg - Pointer to the CRU buffer header which       */
/*                              holds the request that is to be carried      */
/*                              on the EAP packet.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PnacAuthTxReq (tMacAddr destAddr,
               /*    UINT2 u2Id,  */
               UINT2 u2PortNum, tPnacCruBufChainHdr * pMesg,
               BOOL1 bIsPreAuthSess)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    UINT1              *pu1Buf = NULL;
    UINT1              *pu1BufPtr = NULL;
    UINT2               u2PktLen = PNAC_INIT_VAL;

    if (PNAC_NODE_STATUS () != PNAC_ACTIVE_NODE)
    {
        return PNAC_SUCCESS;
    }

    if (PNAC_ALLOCATE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Unable to allocate memory. \r\n");
        return PNAC_FAILURE;
    }

    PNAC_MEMSET (pu1Buf, PNAC_INIT_VAL, PNAC_MAX_ENET_FRAME_SIZE);
    pu1BufPtr = pu1Buf + PNAC_ENET_EAPOL_HDR_SIZE;

    /* Copy the EAP Packet data to the Local Buffer */
    /* Note: The EAP Packet data has EAP type field type data */

    u2PktLen = (UINT2) PNAC_CRU_BUF_Get_ChainValidByteCount (pMesg);
    if (PNAC_COPY_FROM_CRU_BUF (pMesg, pu1BufPtr, PNAC_OFFSET_NONE, u2PktLen)
        == PNAC_CRU_FAILURE)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_BUFFER_TRC |
                  PNAC_ALL_FAILURE_TRC,
                  " AuthTxReq: Copy from CRU Buf returned failure\n");
        if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == MEM_FAILURE)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      "Failed to release  memory. \r\n");
        }
        return PNAC_FAILURE;
    }
    /* Fill the Ethernet Header and EAPOL Header */
    PnacPaeConstructEapolHdr (pu1Buf, destAddr, PNAC_EAP_PKT,
                              u2PktLen, u2PortNum, bIsPreAuthSess);

    u2PktLen = (UINT2) (u2PktLen + PNAC_ENET_EAPOL_HDR_SIZE);

    /* update stats */
    pPortInfo = &(PNAC_PORT_INFO (u2PortNum));

    /* Transmit the EAPOL Ethernet Frame */
    if (pPortInfo->portIfMap.u1IfType == CFA_CAPWAP_DOT11_BSS)
    {
#ifdef RSNA_WANTED

        if (RsnaPnacTxFrame (pu1Buf, u2PktLen, u2PortNum) != RSNA_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " PAE: PnacPaeTxEnetFrame returned Failure\n");
            if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == MEM_FAILURE)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                          "Failed to release  memory. \r\n");
            }
            return PNAC_FAILURE;

        }
#endif
    }
    else
    {
    if (PnacPaeTxEnetFrame (pu1Buf, u2PktLen, u2PortNum) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PnacAuthTxEap: PnacPaeTxEnetFrame returned Failure\n");
        if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == MEM_FAILURE)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      "Failed to release  memory. \r\n");
        }
        return PNAC_FAILURE;
    }
    }

    /* update stats */
    pPortInfo = &(PNAC_PORT_INFO (u2PortNum));

    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolReqFramesTx++;
    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolFramesTx++;
    PNAC_TRC (PNAC_CONTROL_PATH_TRC, " EAP Req Frame Sent !!!\n");

    if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == MEM_FAILURE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Failed to release  memory. \r\n");
    }
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacAuthHandleInPortBasedEapFrame                    */
/*                                                                           */
/* Description        : Processes the EAP frames belonging to the            */
/*                      authenticator module, on port-based-authentication   */
/*                      ports                                                */
/*                                                                           */
/* Input(s)           : pu1PktPtr - Pointer to the EAP packet received       */
/*                                  from the port.                           */
/*                      u2PktLen -  EAPOL body (i.e EAP Packet) length       */
/*                      u1EapolType - EAPOL packet's type field value.       */
/*                      pPortInfo - port entry for the given port.           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PnacAuthHandleInPortBasedEapFrame (UINT1 *pu1PktPtr,
                                   UINT2 u2PktLen,
                                   UINT1 u1EapolType,
                                   tPnacPaePortEntry * pPortInfo)
{
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    UINT1               u1Code;
    UINT2               u2Identifier;
    UINT2               u2EapDataLength;
    UINT2               u2PortNum;
    UINT1               u1EapType;
    UINT1              *pu1ReadPtr = NULL;
    UINT1              *pu1Name = NULL;
    UINT1               u1Val;
    INT4                i4RetVal = PNAC_SUCCESS;
    UINT2               u2Val = 0;

    /* This port supports Port Based Authentication Mode */
    u2PortNum = pPortInfo->u2PortNo;
    pu1ReadPtr = pu1PktPtr;

    /* Get the Authenticator session node from the port table entry */
    if ((pAuthSessNode = pPortInfo->pPortAuthSessionNode) == NULL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PortAuthSessionNode is NULL\n");
        return PNAC_FAILURE;
    }

    /* Session Node is available now */

    if (u1EapolType == PNAC_EAPOL_START)
    {

        /* update stats */
        PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolStartFramesRx++;
        PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolFramesRx++;
        pAuthSessNode->authFsmInfo.bEapStart = PNAC_TRUE;

        if (PnacAuthStateMachine (PNAC_ASM_EV_EAPSTART_RCVD,
                                  u2PortNum,
                                  pAuthSessNode,
                                  NULL, PNAC_NO_VAL) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " HandleInPortBasedEapFrame: ASM returned failure\n");
            i4RetVal = PNAC_FAILURE;
        }
        return (i4RetVal);

    }
    else if (u1EapolType == PNAC_EAPOL_LOGOFF)
    {

        /* update stats */
        PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolLogoffFramesRx++;
        PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolFramesRx++;

        pAuthSessNode->authFsmInfo.bEapLogoff = PNAC_TRUE;

        if (PnacAuthStateMachine (PNAC_ASM_EV_EAPLOGOFF_RCVD,
                                  u2PortNum, pAuthSessNode,
                                  NULL, PNAC_NO_VAL) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " HandleInPortBasedEapFrame: ASM returned failure\n");
            i4RetVal = PNAC_FAILURE;
        }

        return (i4RetVal);
    }
    else if (u1EapolType == PNAC_EAP_PKT)
    {

        /* Read the Eap Hdr Code field */
        PNAC_GET_1BYTE (u1Code, pu1ReadPtr);
        /* Read the Eap Hdr Identifier field */
        PNAC_GET_1BYTE (u1Val, pu1ReadPtr);
        u2Identifier = (UINT2) u1Val;
        /* Read the Eap Hdr Length field */
        PNAC_GET_2BYTE (u2EapDataLength, pu1ReadPtr);
        PNAC_GET_1BYTE (u1EapType, pu1ReadPtr);

        /* u2PktLen is EapolBodyLength. Get EapBodyLength from it
         * and validate it.
         */

        if ((u2PktLen != u2EapDataLength) ||
            ((u2PktLen - PNAC_EAP_HDR_SIZE) > (PNAC_MAX_EAP_DATA_SIZE)))
        {

            /* Eap length error had occurred */
            /* update stats */
            PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapLengthErrorFramesRx++;
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " Eap pkt Length error occurred\n");
            return PNAC_FAILURE;
        }

        if (u1Code == PNAC_EAP_CODE_RESP)
        {
            if (u1EapType == PNAC_EAP_TYPE_IDENTITY)
            {
                /* update stats */
                PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolRespIdFramesRx++;
                PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolFramesRx++;
                if (pAuthSessNode->authFsmInfo.u2CurrentId != u2Identifier)
                {
                    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4InvalidEapolFramesRx++;
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              "Invalid session id for the port, Dropping Eapol Frame \n");
                    return PNAC_FAILURE;
                }
                /* Since This is a response identity,
                 * Copy the User name locally to use it later
                 * thoughout the session.
                 */
                pu1Name = pAuthSessNode->authSessStats.au1SessUserName;
                PNAC_MEMSET (pu1Name, PNAC_INIT_VAL, PNAC_MAX_LEN_USERNAME);
                u2Val = (UINT2) (u2EapDataLength - PNAC_EAP_HDR_SIZE -
                                 PNAC_EAP_TYPE_SIZE);
                pAuthSessNode->authSessStats.u2SessUserNameLen = u2Val;
                PNAC_MEMCPY (pu1Name, pu1ReadPtr, u2Val);
                if (PnacAuthStateMachine (PNAC_ASM_EV_RXRESPID,
                                          u2PortNum, pAuthSessNode,
                                          pu1PktPtr, u2PktLen) != PNAC_SUCCESS)
                {
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              " HandleInPortBasedEapFrame: ASM returned failure\n");
                    i4RetVal = PNAC_FAILURE;
                }
                return (i4RetVal);
            }
            else
            {
                /* The received packet is any other response */

                /* update stats */
                PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolRespFramesRx++;
                PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolFramesRx++;
                if (pAuthSessNode->authFsmInfo.u2CurrentId != u2Identifier)
                {
                    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4InvalidEapolFramesRx++;
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              "Invalid session id for the port, Dropping Eapol Frame \n");
                    return PNAC_FAILURE;
                }
                if (PnacAuthBackendStateMachine (PNAC_BSM_EV_RXRESP,
                                                 u2PortNum, pAuthSessNode,
                                                 pu1PktPtr,
                                                 u2PktLen,
                                                 NULL) != PNAC_SUCCESS)
                {
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              "HandleInPortBasedEapFrame: BSM returned failure\n");
                    i4RetVal = PNAC_FAILURE;
                }
                return (i4RetVal);
            }

        }
        else
        {
            /* These packet types -ie., Request, Success, Failure - 
             * cannot come to authenticator 
             */

            /* update stats */
            PNAC_AUTH_STATS_ENTRY (pPortInfo).u4InvalidEapolFramesRx++;
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " Invalid packet type for authenticator\n");
            return PNAC_FAILURE;
        }

    }
    else
    {
        /* These packet types -ie., EAPOL-key, ASF Alert etc
         * cannot come to authenticator 
         */

        /* update stats */

        PNAC_AUTH_STATS_ENTRY (pPortInfo).u4InvalidEapolFramesRx++;

        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " Invalid packet type for authenticator\n");
        return PNAC_FAILURE;
    }

}

/*****************************************************************************/
/* Function Name      : PnacAuthHandleInMacBasedEapFrame                     */
/*                                                                           */
/* Description        : Processes the EAP frames belonging to the            */
/*                      authenticator module, MAC-based-authentication ports */
/*                                                                           */
/* Input(s)           : pu1PktPtr - Pointer to the EAP packet received       */
/*                                  from the port.                           */
/*                      u2PktLen -  EAPOL body (i.e EAP Packet) length       */
/*                      u1EapolType - EAPOL packet's type field value.       */
/*                      srcAddr - Received packet's source MAC address.      */
/*                      pPortInfo - port entry for the given port.           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PnacAuthHandleInMacBasedEapFrame (UINT1 *pu1PktPtr,
                                  UINT2 u2PktLen,
                                  UINT1 u1EapolType,
                                  tMacAddr srcAddr,
                                  tPnacPaePortEntry * pPortInfo)
{
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    UINT2               u2EapDataLength;
    UINT2               u2PortNum;
    UINT2               u2Event = PNAC_NO_VAL;
    UINT1               u1Code;
    UINT2               u2Identifier;
    UINT1               u1EapType;
    UINT1              *pu1ReadPtr = NULL;
    UINT1              *pu1Name = NULL;
    UINT1               u1Val;
    INT4                i4RetVal = PNAC_SUCCESS;
    UINT4               u4MacAddr = 0;
    UINT2               u2MacAddr = 0;
    UINT1               au1MacAddr [PNAC_MAC_ADDR_SIZE] = {0x01, 0x80, 0xC2, 0x00, 0x00, 0x00};
#ifdef WPS_WANTED
    UINT1 		wpsVendorId[3];
    UINT1	        wpsVendorType[4];
    tPnacAuthFsmInfo   *pFsmInfo = NULL;
#endif

    /* This port supports Mac Based Authentication */
    u2PortNum = pPortInfo->u2PortNo;
    pu1ReadPtr = pu1PktPtr;

    if (PnacAuthGetAuthInProgressTblEntryByMac (srcAddr, &pAuthSessNode)
	    != PNAC_SUCCESS)
    {
	/* Node is not found in the AIP table */

	if (PnacAuthGetSessionTblEntryByMac (srcAddr, &pAuthSessNode)
		!= PNAC_SUCCESS)
	{
	    /* Now node is also not found in the Session hash table.
	     * This packet could be from a new supplicant - so
	     * start a new Session if it is EAPOL Start. 
	     */
	    switch (u1EapolType)
	    {
		case PNAC_EAPOL_START:
		    /* Create session only if auto is configured */

		    if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl ==
			    PNAC_PORTCNTRL_AUTO)
		    {
			/* Now check whether Default Session is present and
			   proceed the session by updating the Source Mac.
			   */
			PNAC_DERIVE_UNIQ_MAC (au1MacAddr,u2PortNum);

			if (PnacAuthGetAuthInProgressTblEntryByMac (au1MacAddr, &pAuthSessNode)
				== PNAC_SUCCESS)
			{
			    /* Fill the source mac address of the supplicant as EAPOL Address */
			    PNAC_MEMCPY (pAuthSessNode->rmtSuppMacAddr,
				    srcAddr,
				    PNAC_MAC_ADDR_SIZE);
			    PNAC_TRC (PNAC_CONTROL_PATH_TRC,
				    " Found and Replaced Group Mac Node\n");
			}
			else
			{
			    /* update stats */
			    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolStartFramesRx++;
			    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolFramesRx++;

			    if (PnacAuthCreateSession
				    (srcAddr, u2PortNum, OSIX_FALSE) !=
				    PNAC_SUCCESS)
			    {
				PNAC_TRC (PNAC_CONTROL_PATH_TRC |
					PNAC_ALL_FAILURE_TRC,
					" AuthCreateSession returned failure\n");
				i4RetVal = PNAC_FAILURE;
			    }
			    return (i4RetVal);
			}
		    }
		    else
		    {
			/* Forceauth/Forceunauth mode. Handle it*/
			pAuthSessNode = pPortInfo->pPortAuthSessionNode;
		    }

		    /* Session is created Successfully */
		    break;
		case PNAC_EAPOL_LOGOFF:
		case PNAC_EAPOL_KEY:
		case PNAC_EAPOL_ASF_ALERT:
		    /* update stats */
		    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
			    " No operations to perform\n");

		    /* pPortInfo->authStatsEntry.u4InvalidEapolFramesRx++; */
		    return PNAC_FAILURE;
		    break;
		case PNAC_EAP_PKT:

		    /* If port control mode is auto do not proces the frame */
		    if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl !=
			    PNAC_PORTCNTRL_AUTO)
		    {
			pAuthSessNode = pPortInfo->pPortAuthSessionNode;
			break;
		    }

		    PNAC_GET_1BYTE (u1Code, pu1ReadPtr);
		    PNAC_GET_1BYTE (u1Val, pu1ReadPtr);
		    u2Identifier = (UINT2) u1Val;
		    PNAC_GET_2BYTE (u2EapDataLength, pu1ReadPtr);

		    /* u2PktLen is EapolBodyLength. Get EapDataLength from it
		     * and validate it.
		     */

		    if ((u2PktLen != u2EapDataLength) ||
			    ((u2PktLen - PNAC_EAP_HDR_SIZE) >
			     (PNAC_MAX_EAP_DATA_SIZE)))
		    {
			/* Eap length error had occurred */
			/* update stats */
			PNAC_AUTH_STATS_ENTRY (pPortInfo).
			    u4EapLengthErrorFramesRx++;
			PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
				"  Eap pkt Length error occurred\n");
			return PNAC_FAILURE;
		    }

		    PNAC_GET_1BYTE (u1EapType, pu1ReadPtr);
		    if (u1Code == PNAC_EAP_CODE_RESP)
		    {
			switch (u1EapType)
			{
			    case PNAC_EAP_TYPE_IDENTITY:

				PNAC_DERIVE_UNIQ_MAC (au1MacAddr,u2PortNum);

				if (PnacAuthGetAuthInProgressTblEntryByMac (au1MacAddr, &pAuthSessNode)
					== PNAC_SUCCESS)
				{
				    /* Fill the source mac address of the supplicant as EAPOL Address */
				    if ((pAuthSessNode->u2PortNo == u2PortNum)
					    && (pAuthSessNode->authFsmInfo.
						u2CurrentId == u2Identifier))
				    {
					PNAC_MEMCPY (pAuthSessNode->rmtSuppMacAddr,
						srcAddr,
						PNAC_MAC_ADDR_SIZE);
					PNAC_TRC (PNAC_CONTROL_PATH_TRC, 
						" Found and Replaced Group Mac Node\n");
					break;
				    }
				}
				else
				{
				    /* update stats */
				    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolRespIdFramesRx++;
				    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolFramesRx++;

				    if (PnacAuthCreateSession
					    (srcAddr, u2PortNum, OSIX_FALSE) !=
					    PNAC_SUCCESS)
				    {
					PNAC_TRC (PNAC_CONTROL_PATH_TRC |
						PNAC_ALL_FAILURE_TRC,
						" AuthCreateSession returned failure\n");
					i4RetVal = PNAC_FAILURE;
				    }
				    return (i4RetVal);
				}
				break;

			    default:
				PNAC_TRC (PNAC_CONTROL_PATH_TRC |
					PNAC_ALL_FAILURE_TRC,
					" EAP Type Other than Identity received  without a Session Node\n");
				return PNAC_FAILURE;
				break;
			}        /* End switch(u1Type) */
		    }
		    else
		    {
			PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
				" EAP Code Other than Response received without "
				" a Session Node\n");
			return PNAC_FAILURE;
		    }
		    break;
		default:
		    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
			    " Unknown EAPOL Type Pkt without a Session Node\n");
		    break;
	    }                    /* End Switch(u1EapolType) */
	}
	else
	{
#ifdef RSNA_WANTED
	    if ((u1EapolType == PNAC_EAPOL_START) && 
		    (pAuthSessNode->pRsnaSessionNode != NULL))
	    {
		RsnaApiFsmStop(pAuthSessNode->pRsnaSessionNode); 
	    }
#endif

	}                        /* end if PnacAuthGetSessionTblEntryByMac */
    }                            /* end if PnacAuthGetAuthInProgressTblEntryByMac */
    /* Node is Present in the AIP table or Session hash
     * table - hence call the Authenticator State Machine.
     */
    /*Handling MAC-Movement*/

    if (u2PortNum != pAuthSessNode->u2PortNo)
    {
	PNAC_TRC_ARG2 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
		" Supplicant MAC Moved from %u to %u ...\n",
		pAuthSessNode->u2PortNo, u2PortNum);

	/*Delete the FDB Entry for supplicant MAC Address*/
	VlanDelFdbEntryByMacAddr(pAuthSessNode->rmtSuppMacAddr);
	/*Delete the previous session running for this supplicant*/
	PnacAuthDeleteSession (pAuthSessNode);
	/* Create the session again*/
	if (PnacAuthCreateSession (srcAddr, u2PortNum, OSIX_FALSE)
		!= PNAC_SUCCESS)
	{
	    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
		    " AuthCreateSession returned failure\n");
	    i4RetVal = PNAC_FAILURE;
	}
	return (i4RetVal);

    }
    PNAC_GET_FOURBYTE (u4MacAddr, pAuthSessNode->rmtSuppMacAddr);
    PNAC_GET_TWOBYTE (u2MacAddr, &(pAuthSessNode->rmtSuppMacAddr[4]));
    PNAC_DEBUG (PNAC_TRC_ARG3 (PNAC_CONTROL_PATH_TRC,
		" Session Node for MAC: -%x-%x- Port: %d obtained ... \n",
		u4MacAddr, u2MacAddr, pAuthSessNode->u2PortNo);
	    );

    /* Reset The pointer again because it might get changed in
     * certain cases.
     */
    pu1ReadPtr = pu1PktPtr;

    switch (u1EapolType)
    {
	case PNAC_EAPOL_START:
	    /* update stats */
	    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolStartFramesRx++;
	    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolFramesRx++;
	    u2Event = PNAC_ASM_EV_EAPSTART_RCVD;
	    pu1PktPtr = NULL;
	    u2PktLen = PNAC_NO_VAL;
	    pAuthSessNode->authFsmInfo.bEapStart = PNAC_TRUE;

	    break;
	case PNAC_EAPOL_LOGOFF:
	    /* update stats */
	    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolLogoffFramesRx++;
	    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolFramesRx++;
	    u2Event = PNAC_ASM_EV_EAPLOGOFF_RCVD;
	    pu1PktPtr = NULL;
	    u2PktLen = PNAC_NO_VAL;
	    pAuthSessNode->authFsmInfo.bEapLogoff = PNAC_TRUE;

	    break;
	case PNAC_EAP_PKT:
	    /* Read the EAP header fields */
	    PNAC_GET_1BYTE (u1Code, pu1ReadPtr);
	    PNAC_GET_1BYTE (u1Val, pu1ReadPtr);
	    u2Identifier = (UINT2) u1Val;
	    PNAC_GET_2BYTE (u2EapDataLength, pu1ReadPtr);

	    /* u2PktLen is EapolBodyLength. Get EapDataLength from it
	     * and validate it.
	     */

	    if ((u2PktLen != u2EapDataLength) ||
		    ((u2PktLen - PNAC_EAP_HDR_SIZE) > (PNAC_MAX_EAP_DATA_SIZE)))
	    {
		/* Eap length error had occurred */
		/* update stats */
		PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapLengthErrorFramesRx++;
		PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
			" Eap pkt Length error occurred\n");
		return PNAC_FAILURE;
	    }
	    /*if (pAuthSessNode->authFsmInfo.u2CurrentId != u2Identifier)
	      {
	      PNAC_AUTH_STATS_ENTRY (pPortInfo).u4InvalidEapolFramesRx++;
	      PNAC_TRC_ARG2 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
	      "Session Id Mismatch - Rxd: %u, Node: %u Dropping Eapol Frame\n",
	      u2Identifier,
	      pAuthSessNode->authFsmInfo.u2CurrentId);
	      return PNAC_FAILURE;
	      }*/

	    PNAC_GET_1BYTE (u1EapType, pu1ReadPtr);

	    if (u1Code == PNAC_EAP_CODE_RESP)
	    {
		if (u1EapType == PNAC_EAP_TYPE_IDENTITY)
		{
		    /* Call the State Machine as per the EAP packet type */
		    /* update stats */
		    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolRespIdFramesRx++;
		    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolFramesRx++;
		    /* Since This is a response identity,
		     * Copy the User name locally to use it later
		     * thoughout the session.
		     */
		    pu1Name = pAuthSessNode->authSessStats.au1SessUserName;
		    PNAC_MEMSET (pu1Name, PNAC_INIT_VAL, PNAC_MAX_LEN_USERNAME);
		    pAuthSessNode->authSessStats.u2SessUserNameLen =
			(UINT2) (u2EapDataLength - PNAC_EAP_HDR_SIZE -
				PNAC_EAP_TYPE_SIZE);
		    PNAC_MEMCPY (pu1Name, pu1ReadPtr,
			    pAuthSessNode->authSessStats.
			    u2SessUserNameLen);
		    u2Event = PNAC_ASM_EV_RXRESPID;
#ifdef WPS_WANTED
		    if (pAuthSessNode->authSessStats.u2SessUserNameLen ==  WSC_ID_ENROLLEE_LEN)
		    {
			if (STRCMP(pu1Name,WSC_ID_ENROLLEE) == 0)
			{
			    wpsRecvIdentity(u2PortNum,srcAddr);	
			}
		    }
#endif
		}                /* End if u1Type == EAP_TYPE_IDENTITY */
#ifdef WPS_WANTED
		else if (u1EapType == PNAC_EAP_TYPE_EXTENDED)
		{
		    UINT1 vendorID[3] = {0x00, 0x37,0x2A};
		    UINT1 vendorType[4] = {0x00,0x00,0x00,0x01};
		    PNAC_MEMCPY(wpsVendorId,pu1ReadPtr,WSC_VENDOR_ID_LEN);
		    pu1ReadPtr += WSC_VENDOR_ID_LEN;
		    if (MEMCMP(wpsVendorId,vendorID,WSC_VENDOR_ID_LEN) == 0)
		    {
			PNAC_MEMCPY(wpsVendorType,pu1ReadPtr,WSC_VENDOR_TYPE_LEN);
			if (MEMCMP(wpsVendorType,vendorType,WSC_VENDOR_TYPE_LEN) == 0)
			{
			    pFsmInfo = &(pAuthSessNode->authFsmInfo);
			    pFsmInfo->bRxRespId = PNAC_TRUE;
			    wpsRecvEapPkt(u2PortNum,srcAddr,pu1PktPtr,u2PktLen);
			}
		    }
		    return PNAC_SUCCESS;
		}
#endif
		else
		{
		    /* update stats */
		    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolRespFramesRx++;
		    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolFramesRx++;
		    if (PnacAuthBackendStateMachine (PNAC_BSM_EV_RXRESP,
				u2PortNum,
				pAuthSessNode,
				pu1PktPtr,
				u2PktLen,
				NULL) != PNAC_SUCCESS)
		    {
			PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
				" HandleInMacBasedEapFrame: BSM returned failure\n");
			i4RetVal = PNAC_FAILURE;
		    }
		    return (i4RetVal);
		}                /* End if u1Type == EAP_TYPE not Identity */
	    }                    /* End if u1Code == EAP_CODE_RESP */

	    break;
	default:
	    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
		    " HandleInMacBasedEapFrame: Invalid Eapol Frame\n");
	    return PNAC_FAILURE;
	    break;
    }                            /* End switch(u1EapolType) */

    if (PnacAuthStateMachine (u2Event,
		u2PortNum,
		pAuthSessNode,
		pu1PktPtr, u2PktLen) != PNAC_SUCCESS)
    {
	PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
		" HandleInMacBasedEapFrame: ASM returned Failure\n");
	i4RetVal = PNAC_FAILURE;
    }
    return (i4RetVal);
}

/*****************************************************************************/
/* Function Name      : PnacAuthHandleInEapFrame                             */
/*                                                                           */
/* Description        : Processes the EAP frames belonging to the            */
/*                      authenticator module                                 */
/*                                                                           */
/* Input(s)           : pu1PktPtr - Pointer to the EAP packet received       */
/*                                  from the port.                           */
/*                      u2PktLen -  EAPOL body (i.e EAP Packet) length       */
/*                      u2PortNum - The Port Number through which this       */
/*                                  packet was received.                     */
/*                      u1EapolType - EAPOL packet's type field value.       */
/*                      srcAddr - Received packet's source MAC address.      */
/*                      u2EnetType :- RsnaPreAuth/Eapol Frame                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PnacAuthHandleInEapFrame (UINT1 *pu1PktPtr,
                          UINT2 u2PktLen,
                          UINT2 u2PortNum, UINT1 u1EapolType, tMacAddr srcAddr,
                          UINT2 u2EnetType)
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  "PnacAuthHandleInEapFrame:Invalid" "Port Number\n");
        return PNAC_FAILURE;
    }
    if (u2EnetType == PNAC_PAE_RSN_PREAUTH_ENET_TYPE)
    {
        if (PnacGetPortEntry (PNAC_MAXPORTS, &pPortInfo) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      "PnacAuthHandleInEapFrame:Invalid" "Port Number\n");
            return PNAC_FAILURE;
        }

        if (PnacAuthHandleInRsnPreAuthEapFrame (pu1PktPtr,
                                                u2PktLen,
                                                u1EapolType, srcAddr,
                                                pPortInfo, u2PortNum))
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " AuthHandleInRsnPreAuthEapFrame: "
                      "MacBasedEapFrame failed\n");
            return PNAC_FAILURE;
        }
    }
    else if (PNAC_IS_PORT_MODE_PORTBASED (pPortInfo))
    {
        if (PnacAuthHandleInPortBasedEapFrame (pu1PktPtr,
                                               u2PktLen,
                                               u1EapolType, pPortInfo))
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " AuthHandleInEapFrame: PortBasedEapFrame failed\n");
            return PNAC_FAILURE;
        }
    }                            /*port-based-authentication, ends */
    else
    {
        if (PnacAuthHandleInMacBasedEapFrame (pu1PktPtr,
                                              u2PktLen,
                                              u1EapolType, srcAddr, pPortInfo))
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " AuthHandleInEapFrame: MacBasedEapFrame failed\n");
            return PNAC_FAILURE;
        }
    }
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacAuthHandleInServerFrame                          */
/*                                                                           */
/* Description        : Processes the EAP frames received from the           */
/*                      authentication server and calles the State Machine.  */
/*                      it to the supplicant.                                */
/*                                                                           */
/* Input(s)           : pu1EapPktBuf - Pointer to the buffer containing the  */
/*                                     EAP packet.                           */
/*                      u2EapPktLen  - Length of the EAP packet.             */
/*                                  EAP packet.                              */
/*                      u2PortNum - Port Number.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PnacAuthHandleInServerFrame (UINT1 *pu1EapPktBuf, UINT2 u2EapPktLen,
                             UINT2 u2PortNum,
                             tMacAddr macAddr, UINT4 u4ServerTimeout)
{
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacCruBufChainHdr *pMesg = NULL;
    UINT1              *pu1PktPtr = NULL;
    UINT1               u1Id;
    UINT1               u1Code;
    UINT2               u2Len;

    PNAC_TRC (PNAC_CONTROL_PATH_TRC, " AUTH: Handling Server Frame\n");

    /* If RADIUS Client is NTOH converting the Eap pkt, the Eap pkt fields are 
     * read directly and following segment is invalid. If RADIUS Client is not 
     * NTOH converting, the following segment for retrieving fields stands 
     * valid 
     */
    /* segment begins */
    pu1PktPtr = (UINT1 *) pu1EapPktBuf;
    PNAC_GET_1BYTE (u1Code, pu1PktPtr);
    PNAC_GET_1BYTE (u1Id, pu1PktPtr);
    PNAC_GET_2BYTE (u2Len, pu1PktPtr);
    /* segment ends */

    /* Get the Authenticator session node from session id table 
     * using the session id received
     */
    pPortInfo = &(PNAC_PORT_INFO (u2PortNum));
    if (PNAC_IS_PORT_MODE_PORTBASED (pPortInfo))
    {
        pAuthSessNode = pPortInfo->pPortAuthSessionNode;
        pAuthSessNode->authFsmInfo.u2IdFromServer = (UINT2) u1Id;
    }
    else
    {
        /* Do verify the MAC address and Get the Session Node */
        if (PnacAuthGetAuthInProgressTblEntryByMac (macAddr, &pAuthSessNode)
            != PNAC_SUCCESS)
        {
            /* Node is not found in the AIP table */
            if (PnacAuthGetSessionTblEntryByMac (macAddr, &pAuthSessNode)
                != PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " No Session Node available - Handling Server Frame failed\n");
                return PNAC_FAILURE;
            }
        }
        pAuthSessNode->authFsmInfo.u2IdFromServer = (UINT2) u1Id;
    }

    PNAC_TRC_ARG3 (PNAC_CONTROL_PATH_TRC,
                   " SessNode got from SessIdtbl for Id %d is : %x, Port: %d  \n",
                   (UINT2) u1Id, pAuthSessNode, u2PortNum);

    switch (u1Code)
    {
        case PNAC_EAP_CODE_REQ:
            if ((pMesg = PNAC_ALLOC_CRU_BUF (u2EapPktLen, PNAC_OFFSET_NONE))
                == NULL)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                          PNAC_BUFFER_TRC,
                          " AuthHandleInServerFrame: Allocate Buffer failed\n");
                return PNAC_FAILURE;
            }

            if (PNAC_COPY_OVER_CRU_BUF (pMesg, pu1EapPktBuf, PNAC_OFFSET_NONE,
                                        u2EapPktLen) == PNAC_CRU_FAILURE)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                          PNAC_BUFFER_TRC,
                          " AuthHandleInServerFrame: Copy over buf failed\n");

                if (PNAC_RELEASE_CRU_BUF (pMesg, PNAC_CRU_FALSE) !=
                    PNAC_CRU_SUCCESS)
                {
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                              PNAC_BUFFER_TRC,
                              " AuthHandleInServerFrame: Release buf failed\n");

                }
                return PNAC_FAILURE;
            }

            if (PnacAuthBackendStateMachine (PNAC_BSM_EV_AREQ_RCVD,
                                             u2PortNum, pAuthSessNode,
                                             NULL, PNAC_NO_VAL,
                                             pMesg) != PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " AuthHandleInServerFrame: BSM returned Failure\n");
                return PNAC_FAILURE;
            }
            if (PNAC_RELEASE_CRU_BUF (pMesg, PNAC_CRU_FALSE) !=
                PNAC_MEM_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                          PNAC_BUFFER_TRC,
                          " AuthHandleInServerFrame: Release buf failed\n");
                return PNAC_FAILURE;
            }
            break;

        case PNAC_EAP_CODE_RESP:
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " AuthHandleInServerFrame: Eap Response Pkt cannot be from server\n");
            return PNAC_FAILURE;
            break;

        case PNAC_EAP_CODE_SUCCESS:
            pPortInfo = &(PNAC_PORT_INFO (u2PortNum));
            if (u4ServerTimeout != PNAC_INIT_VAL)
            {
                pAuthSessNode->authFsmInfo.u4SessReAuthPeriod = u4ServerTimeout;
            }
            else
            {
                pAuthSessNode->authFsmInfo.u4SessReAuthPeriod =
                    pPortInfo->authConfigEntry.u4ReAuthPeriod;
            }
            if (PnacAuthBackendStateMachine (PNAC_BSM_EV_ASUCCESS_RCVD,
                                             u2PortNum,
                                             pAuthSessNode,
                                             NULL,
                                             PNAC_NO_VAL, NULL) != PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " AuthHandleInServerFrame: BSM returned Failure\n");
                return PNAC_FAILURE;
            }
            break;

        case PNAC_EAP_CODE_FAILURE:
            if (PnacAuthBackendStateMachine (PNAC_BSM_EV_AFAIL_RCVD,
                                             u2PortNum,
                                             pAuthSessNode,
                                             NULL,
                                             PNAC_NO_VAL, NULL) != PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " AuthHandleInServerFrame: BSM returned Failure\n");
                return PNAC_FAILURE;
            }

            break;

        default:
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " HandleInServerFrame: Invalid Eap pkt type from Server\n");
            return PNAC_FAILURE;
            break;

    }                            /* switch u1Code */
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacAuthLinkStatusUpdate                             */
/*                                                                           */
/* Description        : This function does the operations necessary on       */
/*                      Supplicant's session due to the change in oper status*/
/*                      of the port.                                         */
/*                                                                           */
/* Input(s)           :  u2PortNo - Port number                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacAuthLinkStatusUpdate (tPnacPaePortEntry * pPortInfo)
{
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    UINT2               u2PortNum;
#ifndef RSNA_WANTED
    UINT1               au1MacAddr [PNAC_MAC_ADDR_SIZE] = {0x01, 0x80, 0xC2, 0x00, 0x00, 0x00};
#endif

    u2PortNum = pPortInfo->u2PortNo;

    switch (pPortInfo->u1EntryStatus)
    {
        case PNAC_PORT_OPER_DOWN:

            if (PNAC_IS_PORT_MODE_MACBASED (pPortInfo))
            {
                /* Delete all the sessions */
                if (PnacAuthDisableMacBasedPort (u2PortNum) != PNAC_SUCCESS)
                {
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              " AuthLinkStatusUpdate: DisableMacBasedPort Failed\n");
                    return PNAC_FAILURE;
                }
            }

            pAuthSessNode = pPortInfo->pPortAuthSessionNode;

            if (pAuthSessNode == NULL)
            {
                /* No session node available, just ignore this event */
                PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                          " AuthLinkStatusUpdate: BSM returned "
                          "since no auth session node available\n");
                return PNAC_FAILURE;
            }

            /* Session node applicable. Port is in force auth/force unauth */

            if (pAuthSessNode->authFsmInfo.u2AuthControlPortStatus ==
                PNAC_PORTSTATUS_AUTHORIZED)
            {
                pAuthSessNode->authSessStats.u4SessTerminateCause =
                    PNAC_SESS_TERMINATE_CAUSE_PORTFAILURE;
            }
            if (PnacAuthStateMachine (PNAC_ASM_EV_PORTDISABLED,
                                      u2PortNum,
                                      pAuthSessNode,
                                      NULL, PNAC_NO_VAL) != PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " AuthLinkStatusUpdate: ASM returned Failure\n");
                return PNAC_FAILURE;
            }

            if (PnacAuthBackendStateMachine (PNAC_BSM_EV_INITIALIZE,
                                             u2PortNum,
                                             pAuthSessNode,
                                             NULL,
                                             PNAC_NO_VAL,
                                             NULL) != PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " AuthLinkStatusUpdate: BSM returned Failure\n");
                return PNAC_FAILURE;
            }
            break;

        case PNAC_PORT_OPER_UP:
#ifdef RSNA_WANTED
	    return PNAC_SUCCESS;
#else
	    pAuthSessNode = pPortInfo->pPortAuthSessionNode;

	    if ((PNAC_IS_PORT_MODE_MACBASED (pPortInfo)) &&
			    (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl ==
                 PNAC_PORTCNTRL_AUTO))
            {

                /* If port-control mode is auto in Mac based.
                   Delete the default session */

                if (pPortInfo->pPortAuthSessionNode != NULL)
                {
                    PnacAuthDeleteSession (pPortInfo->pPortAuthSessionNode);
                    pPortInfo->pPortAuthSessionNode = NULL;
                }

               /* Start the state machine's only for unique mac session
                */

                PNAC_DERIVE_UNIQ_MAC (au1MacAddr, u2PortNum);

                if (PnacAuthGetAuthInProgressTblEntryByMac (au1MacAddr, &pAuthSessNode) 
                    != PNAC_SUCCESS)
                {
                    if (PnacInitAuthenticator (u2PortNum) != PNAC_SUCCESS)
                    {
                        PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC |
                                  PNAC_ALL_FAILURE_TRC,
                                  " Authenticator Initialization Failed\n");
                        return PNAC_FAILURE;
                    }
                    else
                    {   
                        return PNAC_SUCCESS;
                    }
                }
            }

            if (pAuthSessNode == NULL)
            {
                /* No session node available, just ignore this event */
                PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                          " AuthLinkStatusUpdate: BSM returned "
                          "since no auth session node available\n");
                return PNAC_FAILURE;
            }

            /* Session node applicable. */

            pAuthSessNode->u2PortNo = u2PortNum;

            if (PnacAuthStateMachine (PNAC_ASM_EV_PORTENABLED,
                                      u2PortNum,
                                      pAuthSessNode,
                                      NULL, PNAC_NO_VAL) != PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " AuthLinkStatusUpdate: ASM returned Failure\n");
                return PNAC_FAILURE;
            }

            if (PnacAuthBackendStateMachine (PNAC_BSM_EV_INITIALIZE,
                                             u2PortNum,
                                             pAuthSessNode,
                                             NULL,
                                             PNAC_NO_VAL,
                                             NULL) != PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " AuthLinkStatusUpdate: BSM returned Failure\n");
                return PNAC_FAILURE;
            }
#endif
            break;

        default:
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " AuthLinkStatusUpdate: Invalid Operational Status passed\n");
            return PNAC_FAILURE;
    }                        /* end switch operstatus */

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacAuthDisableMacBasedPort                          */
/*                                                                           */
/* Description        : This function does the operations necessary on       */
/*                      Supplicant's session When a port is disabled.        */
/*                                                                           */
/* Input(s)           :  u2PortNum - Port number                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacAuthDisableMacBasedPort (UINT2 u2PortNum)
{
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tPnacAuthSessionNode *pNextAuthSessNode = NULL;
    UINT4               u4Index;
    UINT2               u2Count;

   /***********************************************************/
    /* Get all the session nodes belonging to this port and    */
    /* call the state machine with port disabled event         */
    /* and at the end - the session node should be deleted.    */
   /***********************************************************/

    PNAC_HASH_SCAN_TBL (PNAC_AUTH_SESSION_TABLE, u4Index)
    {
        pAuthSessNode = (tPnacAuthSessionNode *)
            PNAC_HASH_GET_FIRST_BUCKET_NODE (PNAC_AUTH_SESSION_TABLE, u4Index);
        while (pAuthSessNode != NULL)
        {
            pNextAuthSessNode = (tPnacAuthSessionNode *)
                PNAC_HASH_GET_NEXT_BUCKET_NODE
                (PNAC_AUTH_SESSION_TABLE, u4Index,
                 &pAuthSessNode->nextAuthSessionNode);
            if (pAuthSessNode->u2PortNo == u2PortNum)
            {
                /* make the port Unavailable for this Supplicant */
                if (PnacAuthStateMachine (PNAC_ASM_EV_PORTDISABLED,
                                          u2PortNum,
                                          pAuthSessNode,
                                          NULL, PNAC_NO_VAL) != PNAC_SUCCESS)
                {
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              " DisableMacBasedPort: ASM returned Failure\n");
                }

                /* initialize the Backend state machine for this Supplicant */
                if (PnacAuthBackendStateMachine (PNAC_BSM_EV_INITIALIZE,
                                                 u2PortNum,
                                                 pAuthSessNode,
                                                 NULL,
                                                 PNAC_NO_VAL,
                                                 NULL) != PNAC_SUCCESS)
                {
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              " DisableMacBasedPort: BSM returned Failure\n");
                }
            }
            pAuthSessNode = pNextAuthSessNode;
        }
    }
    for (u2Count = PNAC_INIT_VAL; u2Count < PNAC_MAX_SESS_IN_PROGRESS;
         u2Count++)
    {
        if ((pAuthSessNode = PNAC_AIP_ENTRY (u2Count)) == NULL)
        {
            continue;
        }
        if (u2PortNum != pAuthSessNode->u2PortNo)
        {
            continue;
        }
        /* make the port Unavailable for this Supplicant */
        if (PnacAuthStateMachine (PNAC_ASM_EV_PORTDISABLED,
                                  u2PortNum,
                                  pAuthSessNode,
                                  NULL, PNAC_NO_VAL) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " DisableMacBasedPort: ASM returned Failure\n");
        }

        /* initialize the Backend state machine for this Supplicant */
        if (PnacAuthBackendStateMachine (PNAC_BSM_EV_INITIALIZE,
                                         u2PortNum,
                                         pAuthSessNode,
                                         NULL,
                                         PNAC_NO_VAL, NULL) != PNAC_SUCCESS)
        {

            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " DisableMacBasedPort: BSM returned Failure\n");
        }
        PnacAuthDeleteSession (pAuthSessNode);
    }
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacAuthHandleInRsnPreAuthEapFrame                   */
/*                                                                           */
/* Description        : Processes the EAP frames belonging to the            */
/*                      authenticator module, MAC-based-authentication ports */
/*                                                                           */
/* Input(s)           : pu1PktPtr - Pointer to the EAP packet received       */
/*                                  from the port.                           */
/*                      u2PktLen -  EAPOL body (i.e EAP Packet) length       */
/*                      u1EapolType - EAPOL packet's type field value.       */
/*                      srcAddr - Received packet's source MAC address.      */
/*                      pPortInfo - port entry for the given port.           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PnacAuthHandleInRsnPreAuthEapFrame (UINT1 *pu1PktPtr,
                                    UINT2 u2PktLen,
                                    UINT1 u1EapolType,
                                    tMacAddr srcAddr,
                                    tPnacPaePortEntry * pPortInfo,
                                    UINT2 u2PhyPort)
{
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tPnacAuthSessionNode *pNode = NULL;
    tMacAddr            eapolGrpMacAddr;
    UINT2               u2EapDataLength;
    UINT2               u2PortNum;
    UINT2               u2Event = PNAC_NO_VAL;
    UINT1               u1Code;
    UINT2               u2Identifier;
    UINT1               u1EapType;
    UINT1              *pu1ReadPtr = NULL;
    UINT1              *pu1Name = NULL;
    UINT1               u1Val;
    UINT2               u2Count;
    INT4                i4RetVal = PNAC_SUCCESS;
    UINT4               u4MacAddr = 0;
    UINT2               u2MacAddr = 0;
    /* This port supports Mac Based Authentication */
    u2PortNum = pPortInfo->u2PortNo;
    pu1ReadPtr = pu1PktPtr;

    if (PnacAuthGetAuthInProgressTblEntryByMac (srcAddr, &pAuthSessNode)
        != PNAC_SUCCESS)
    {
        /* Node is not found in the AIP table */

        if (PnacAuthGetSessionTblEntryByMac (srcAddr, &pAuthSessNode)
            != PNAC_SUCCESS)
        {
            /* Now node is also not found in the Session hash table.
             * This packet could be from a new supplicant - so
             * start a new Session if it is EAPOL Start. 
             */
            switch (u1EapolType)
            {
                case PNAC_EAPOL_START:
                    /* update stats */
                    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolStartFramesRx++;
                    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolFramesRx++;

                    if (PnacAuthCreateSession (srcAddr,
                                               u2PhyPort, OSIX_TRUE)
                        != PNAC_SUCCESS)
                    {
                        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                                  " AuthCreateSession returned failure\n");
                        i4RetVal = PNAC_FAILURE;
                    }
                    /* Session is created Successfully */
                    return (i4RetVal);
                    break;
                case PNAC_EAPOL_LOGOFF:
                case PNAC_EAPOL_KEY:
                case PNAC_EAPOL_ASF_ALERT:
                    /* update stats */
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              " No operations to perform\n");

                    /* pPortInfo->authStatsEntry.u4InvalidEapolFramesRx++; */
                    return PNAC_FAILURE;
                    break;
                case PNAC_EAP_PKT:
                    PNAC_GET_1BYTE (u1Code, pu1ReadPtr);
                    PNAC_GET_1BYTE (u1Val, pu1ReadPtr);
                    u2Identifier = (UINT2) u1Val;
                    PNAC_GET_2BYTE (u2EapDataLength, pu1ReadPtr);

                    /* u2PktLen is EapolBodyLength. Get EapDataLength from it
                     * and validate it.
                     */

                    if ((u2PktLen != u2EapDataLength) ||
                        ((u2PktLen - PNAC_EAP_HDR_SIZE) >
                         (PNAC_MAX_EAP_DATA_SIZE)))
                    {
                        /* Eap length error had occurred */
                        /* update stats */
                        PNAC_AUTH_STATS_ENTRY (pPortInfo).
                            u4EapLengthErrorFramesRx++;
                        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                                  "  Eap pkt Length error occurred\n");
                        return PNAC_FAILURE;
                    }

                    PNAC_GET_1BYTE (u1EapType, pu1ReadPtr);
                    if (u1Code == PNAC_EAP_CODE_RESP)
                    {
                        switch (u1EapType)
                        {
                            case PNAC_EAP_TYPE_IDENTITY:
                                PNAC_MEMSET (&eapolGrpMacAddr, PNAC_INIT_VAL,
                                             sizeof (tMacAddr));
                                PNAC_MEMCPY (eapolGrpMacAddr,
                                             gPnacEapolGrpMacAddr,
                                             PNAC_MAC_ADDR_SIZE);
                                for (u2Count = PNAC_INIT_VAL;
                                     u2Count < PNAC_MAX_SESS_IN_PROGRESS;
                                     u2Count++)
                                {
                                    if ((pNode =
                                         PNAC_AIP_ENTRY (u2Count)) != NULL)
                                    {
                                        if ((pNode->u2PortNo == u2PortNum)
                                            && (pNode->authFsmInfo.
                                                u2CurrentId == u2Identifier)
                                            &&
                                            (PNAC_MEMCMP (pNode->rmtSuppMacAddr,
                                                          gPnacEapolGrpMacAddr,
                                                          PNAC_MAC_ADDR_SIZE) ==
                                             PNAC_NO_VAL))
                                        {
                                            PNAC_MEMCPY (pNode->rmtSuppMacAddr,
                                                         srcAddr,
                                                         PNAC_MAC_ADDR_SIZE);
                                            pAuthSessNode = pNode;
                                            PNAC_TRC (PNAC_CONTROL_PATH_TRC |
                                                      PNAC_ALL_FAILURE_TRC,
                                                      " Found and Replaced Group Mac Node\n");
                                            break;
                                        }
                                    }
                                }
                                if (pAuthSessNode == NULL)
                                {
                                    if (PnacAuthCreateSession
                                        (srcAddr, u2PhyPort, OSIX_TRUE) !=
                                        PNAC_SUCCESS)
                                    {
                                        PNAC_TRC (PNAC_CONTROL_PATH_TRC |
                                                  PNAC_ALL_FAILURE_TRC,
                                                  " AuthCreateSession"
                                                  "returned failure\n");
                                        i4RetVal = PNAC_FAILURE;
                                    }
                                    return (i4RetVal);
                                }
                                break;
                            default:
                                PNAC_TRC (PNAC_CONTROL_PATH_TRC |
                                          PNAC_ALL_FAILURE_TRC,
                                          " EAP Type Other than Identity "
                                          "received  without a Session Node\n");
                                return PNAC_FAILURE;
                                break;
                        }        /* End switch(u1Type) */
                    }
                    else
                    {
                        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                                  " EAP Code Other than Response received without "
                                  " a Session Node\n");
                        return PNAC_FAILURE;
                    }
                    break;
                default:
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              " Unknown EAPOL Type Pkt without a Session Node\n");
                    break;
            }                    /* End Switch(u1EapolType) */
        }                        /* end if PnacAuthGetSessionTblEntryByMac */
    }                            /* end if PnacAuthGetAuthInProgressTblEntryByMac */
    /* Node is Present in the AIP table or Session hash
     * table - hence call the Authenticator State Machine.
     */
    if (u2PortNum != pAuthSessNode->u2PortNo)
    {
        PNAC_TRC_ARG2 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                       " Supplicant MAC Moved from %u to %u ... Dropping Frame...\n",
                       pAuthSessNode->u2PortNo, u2PortNum);
        return PNAC_FAILURE;
    }
    PNAC_GET_FOURBYTE (u4MacAddr, pAuthSessNode->rmtSuppMacAddr);
    PNAC_GET_TWOBYTE (u2MacAddr, &(pAuthSessNode->rmtSuppMacAddr[4]));
    PNAC_DEBUG (PNAC_TRC_ARG3 (PNAC_CONTROL_PATH_TRC,
                               " Session Node for MAC: -%x-%x- Port: %d obtained ... \n",
                               u4MacAddr, u2MacAddr, pAuthSessNode->u2PortNo);
        );

    /* Reset The pointer again because it might get changed in
     * certain cases.
     */
    pu1ReadPtr = pu1PktPtr;

    switch (u1EapolType)
    {
        case PNAC_EAPOL_START:
            /* update stats */
            PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolStartFramesRx++;
            PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolFramesRx++;
            u2Event = PNAC_ASM_EV_EAPSTART_RCVD;
            pu1PktPtr = NULL;
            u2PktLen = PNAC_NO_VAL;
            pAuthSessNode->authFsmInfo.bEapStart = PNAC_TRUE;
            break;
        case PNAC_EAPOL_LOGOFF:
            /* update stats */
            PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolLogoffFramesRx++;
            PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolFramesRx++;
            u2Event = PNAC_ASM_EV_EAPLOGOFF_RCVD;
            pu1PktPtr = NULL;
            u2PktLen = PNAC_NO_VAL;
            pAuthSessNode->authFsmInfo.bEapLogoff = PNAC_TRUE;
            break;
        case PNAC_EAP_PKT:
            /* Read the EAP header fields */
            PNAC_GET_1BYTE (u1Code, pu1ReadPtr);
            PNAC_GET_1BYTE (u1Val, pu1ReadPtr);
            u2Identifier = (UINT2) u1Val;
            PNAC_GET_2BYTE (u2EapDataLength, pu1ReadPtr);

            /* u2PktLen is EapolBodyLength. Get EapDataLength from it
             * and validate it.
             */

            if ((u2PktLen != u2EapDataLength) ||
                ((u2PktLen - PNAC_EAP_HDR_SIZE) > (PNAC_MAX_EAP_DATA_SIZE)))
            {
                /* Eap length error had occurred */
                /* update stats */
                PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapLengthErrorFramesRx++;
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " Eap pkt Length error occurred\n");
                return PNAC_FAILURE;
            }
            if (pAuthSessNode->authFsmInfo.u2CurrentId != u2Identifier)
            {
                PNAC_AUTH_STATS_ENTRY (pPortInfo).u4InvalidEapolFramesRx++;
                PNAC_TRC_ARG2 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                               "Session Id Mismatch - Rxd: %u, Node: "
                               "%u Dropping Eapol Frame\n",
                               u2Identifier,
                               pAuthSessNode->authFsmInfo.u2CurrentId);
                return PNAC_FAILURE;
            }

            PNAC_GET_1BYTE (u1EapType, pu1ReadPtr);

            if (u1Code == PNAC_EAP_CODE_RESP)
            {
                if (u1EapType == PNAC_EAP_TYPE_IDENTITY)
                {
                    /* Call the State Machine as per the EAP packet type */
                    /* update stats */
                    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolRespIdFramesRx++;
                    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolFramesRx++;
                    /* Since This is a response identity,
                     * Copy the User name locally to use it later
                     * thoughout the session.
                     */
                    pu1Name = pAuthSessNode->authSessStats.au1SessUserName;
                    PNAC_MEMSET (pu1Name, PNAC_INIT_VAL, PNAC_MAX_LEN_USERNAME);
                    pAuthSessNode->authSessStats.u2SessUserNameLen =
                        (UINT2) (u2EapDataLength - PNAC_EAP_HDR_SIZE -
                                 PNAC_EAP_TYPE_SIZE);
                    PNAC_MEMCPY (pu1Name, pu1ReadPtr,
                                 pAuthSessNode->authSessStats.
                                 u2SessUserNameLen);
                    u2Event = PNAC_ASM_EV_RXRESPID;

                }                /* End if u1Type == EAP_TYPE_IDENTITY */
                else
                {
                    /* update stats */
                    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolRespFramesRx++;
                    PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolFramesRx++;
                    if (PnacAuthBackendStateMachine (PNAC_BSM_EV_RXRESP,
                                                     u2PortNum,
                                                     pAuthSessNode,
                                                     pu1PktPtr,
                                                     u2PktLen,
                                                     NULL) != PNAC_SUCCESS)
                    {
                        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                                  " HandleInMacBasedEapFrame: BSM returned failure\n");
                        i4RetVal = PNAC_FAILURE;
                    }
                    return (i4RetVal);
                }                /* End if u1Type == EAP_TYPE not Identity */
            }                    /* End if u1Code == EAP_CODE_RESP */

            break;
        default:
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " HandleInMacBasedEapFrame: Invalid Eapol Frame\n");
            return PNAC_FAILURE;
            break;
    }                            /* End switch(u1EapolType) */

    if (PnacAuthStateMachine (u2Event,
                              u2PortNum,
                              pAuthSessNode,
                              pu1PktPtr, u2PktLen) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " HandleInMacBasedEapFrame: ASM returned Failure\n");
        i4RetVal = PNAC_FAILURE;
    }
    return (i4RetVal);
}
