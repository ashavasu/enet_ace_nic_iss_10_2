/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pnactmr.c,v 1.21 2014/08/13 12:40:34 siva Exp $
 *
 * Description     : This file contains Timer routine 
 *                   for PNAC module
 *******************************************************************/

#include "pnachdrs.h"

/*****************************************************************************/
/* Function Name      : PnacTimerInit                                        */
/*                                                                           */
/* Description        : This function initialises all the timer related      */
/*                      entities such as Timer management library, Timer     */
/*                      Task, Timer mem pool, Timer List and Timer event.    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSystemInfo.tmrTaskId,                           */
/*                      gPnacSystemInfo.tmrMemPoolId,                        */
/*                      gPnacSystemInfo.tmrListId,                           */
/*                      gPnacSystemInfo.tmrTaskId                            */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

INT4
PnacTmrInit (VOID)
{
    tPnacTmrListId      PnacTimerListId;

    PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
              " TMR: Timer submodule is initializing...\n");

    /* Create a PNAC Timer list */
    if (PNAC_CREATE_TIMERLIST (PNAC_INTF_TASK_NAME,
                               PNAC_TIMER_EXP_EVENT,
                               NULL, &PnacTimerListId) != PNAC_TMR_SUCCESS)
    {

        PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_ALL_FAILURE_TRC |
                  PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
                  " TMR: Timer list creation failed\n");

        /* call Timer Deinit */
        PnacTmrDeinit ();
        return PNAC_FAILURE;
    }

    /* store the Timer List ID in global variable */
    gPnacSystemInfo.tmrListId = PnacTimerListId;

    PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
              " TMR: Timer list created\n");

    PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
              " TMR: Timer submodule Initialized\n");
    return PNAC_SUCCESS;

}                                /* PnacTmrInit ends */

/*****************************************************************************/
/* Function Name      : PnacTmrHandler                                       */
/*                                                                           */
/* Description        : This function is called whenever a timer expiry      */
/*                      event is received by the PNAC Timer Task. The        */
/*                      different timer expiry handlers are called according */
/*                      to the timer type                                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.tmrListId                            */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

INT4
PnacTmrHandler (VOID)
{
    tPnacDPnacPktInfo   DPnacPktInfo;
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tPnacSuppSessionNode *pSuppSessNode = NULL;
    tPnacTimerNode     *pTimerNode = NULL;
    tPnacServAuthCache *pCacheNode = NULL;
    tPnacTmrListId      TimerListId;
    INT4                i4RetVal = PNAC_SUCCESS;
    UINT4               u4Duration;
    UINT2               u2PortNum;
    UINT2               u2SessionId;
    UINT1               u1TimerType;

    PNAC_MEMSET (&DPnacPktInfo, PNAC_INIT_VAL, sizeof (tPnacDPnacPktInfo));

    TimerListId = gPnacSystemInfo.tmrListId;
    if ((TimerListId == PNAC_INIT_VAL) ||
        (PNAC_SYSTEM_CONTROL == PNAC_SHUTDOWN))
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  " TMR: Invalid Timer list ID for Handling timer expiries\n");
        return PNAC_FAILURE;
    }

    /* Do for every timer in the Expired-Timer-List */

    while ((pTimerNode =
            (tPnacTimerNode *) PNAC_GET_NEXT_EXPIRED_TIMER (TimerListId))
           != NULL)
    {

        u2PortNum = pTimerNode->u2PortNo;
        u1TimerType = pTimerNode->u1TimerType;
        u2SessionId = pTimerNode->u2SessionId;
        pAuthSessNode = pTimerNode->pAuthSessNode;
        pCacheNode = pTimerNode->pCacheNode;

        PNAC_TRC_ARG3 (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                       " TMR: Timer Type: %2x, Id: %d, Expired for Port: %d\n",
                       u1TimerType, u2SessionId, u2PortNum);

        /* Since reauth timer may get restarted, don't free the block if the
         * timer is of type reauth. */
        if (u1TimerType != PNAC_REAUTHWHEN_TIMER)
        {
            if (PNAC_RELEASE_TIMERNODE_MEMBLK (pTimerNode) != PNAC_MEM_SUCCESS)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                          PNAC_CONTROL_PATH_TRC,
                          " TMR: Releasing MemBlock for Timer Node failed\n");
                return PNAC_FAILURE;
            }
        }

        /* Call respective state machines to handle the timer expiries */
        switch (u1TimerType)
        {

            case PNAC_AUTHWHILE_TIMER:
                pSuppSessNode = PNAC_PORT_INFO (u2PortNum).pPortSuppSessionNode;
                pSuppSessNode->pAuthWhileTimer = NULL;

                if (PnacSuppStateMachine (PNAC_SSM_EV_AUTHWHILE_EXPIRED,
                                          u2PortNum, pSuppSessNode, NULL,
                                          PNAC_NO_VAL) != PNAC_SUCCESS)
                {
                    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                              (" TMR: PnacTmrHandler: SSM returned failure\n"));
                    i4RetVal = PNAC_FAILURE;
                }
                break;

            case PNAC_HELDWHILE_TIMER:
                pSuppSessNode = PNAC_PORT_INFO (u2PortNum).pPortSuppSessionNode;
                pSuppSessNode->pHeldWhileTimer = NULL;

                if (PnacSuppStateMachine (PNAC_SSM_EV_HELDWHILE_EXPIRED,
                                          u2PortNum, pSuppSessNode, NULL,
                                          PNAC_NO_VAL) != PNAC_SUCCESS)
                {

                    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                              (" TMR: PnacTmrHandler: SSM returned failure\n"));
                    i4RetVal = PNAC_FAILURE;
                }
                break;

            case PNAC_STARTWHEN_TIMER:
                pSuppSessNode = PNAC_PORT_INFO (u2PortNum).pPortSuppSessionNode;
                pSuppSessNode->pStartWhenTimer = NULL;
                if (PnacSuppStateMachine (PNAC_SSM_EV_STARTWHEN_EXPIRED,
                                          u2PortNum, pSuppSessNode, NULL,
                                          PNAC_NO_VAL) != PNAC_SUCCESS)
                {

                    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                              (" TMR: PnacTmrHandler: SSM returned failure\n"));
                    i4RetVal = PNAC_FAILURE;
                }
                break;

            case PNAC_QUIETWHILE_TIMER:
                pAuthSessNode->pQuietWhileTimer = NULL;
                if (PnacAuthStateMachine
                    (PNAC_ASM_EV_QUIETWHILE_EXPIRED, u2PortNum, pAuthSessNode,
                     NULL, PNAC_NO_VAL) != PNAC_SUCCESS)
                {

                    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                              (" TMR: PnacTmrHandler: ASM returned failure\n"));
                    i4RetVal = PNAC_FAILURE;
                }

                break;

            case PNAC_TXWHEN_TIMER:
                pAuthSessNode->pTxWhenTimer = NULL;
                if (PnacAuthStateMachine (PNAC_ASM_EV_TXWHEN_EXPIRED, u2PortNum,
                                          pAuthSessNode, NULL,
                                          PNAC_NO_VAL) != PNAC_SUCCESS)
                {

                    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                              (" TMR: PnacTmrHandler: ASM returned failure\n"));
                    i4RetVal = PNAC_FAILURE;
                }
                break;

            case PNAC_REAUTHWHEN_TIMER:

                pAuthSessNode->authFsmInfo.bReAuthenticate = PNAC_TRUE;

                /* 
                 * Once the Session becomes unauthorized, reauth timer will be
                 * stopped. In that case timer expiry will not happen. Hence no 
                 * need to check port authorized state here. The same holds good
                 * for port disabled and port control change. So just restart 
                 * the timer here.
                 */
                pAuthSessNode->pReAuthWhenTimer->u2SessionId =
                    pAuthSessNode->authFsmInfo.u2CurrentId;

                /* ReStart the Timer. Since PnacStartTimer create a new timer 
                 * node we can't use that. Hence directly call PNAC_START_TIMER
                 * here.*/
                u4Duration = pAuthSessNode->authFsmInfo.u4SessReAuthPeriod *
                    PNAC_NUM_OF_TIME_UNITS_IN_A_SEC;

                if (PNAC_START_TIMER
                    (gPnacSystemInfo.tmrListId,
                     &pAuthSessNode->pReAuthWhenTimer->appTimer,
                     u4Duration) != PNAC_TMR_SUCCESS)
                {

                    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                              " TMR: PnacTmrHandler ReStarting ReAuthTimer "
                              "failed\n");
                    i4RetVal = PNAC_FAILURE;
                }

                if (PnacAuthStateMachine (PNAC_ASM_EV_REAUTHENTICATE, u2PortNum,
                                          pAuthSessNode, NULL,
                                          PNAC_NO_VAL) != PNAC_SUCCESS)
                {

                    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                              (" TMR: PnacTmrHandler: ASM returned failure\n"));
                    i4RetVal = PNAC_FAILURE;
                }

                break;

            case PNAC_AWHILE_TIMER:
                pAuthSessNode->pAWhileTimer = NULL;
                if (PnacAuthBackendStateMachine (PNAC_BSM_EV_AWHILE_EXPIRED,
                                                 u2PortNum, pAuthSessNode,
                                                 NULL, PNAC_NO_VAL,
                                                 NULL) != PNAC_SUCCESS)
                {

                    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                              " TMR: PnacTmrHandler: BSM returned Failure\n");
                    i4RetVal = PNAC_FAILURE;
                }
                break;

            case PNAC_SERVERCACHE_TIMER:
                if (PnacServCacheTmrExpired (pCacheNode) != PNAC_SUCCESS)
                {
                    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                              " TMR: ServCacheTmrExpired returned Failure\n");
                    i4RetVal = PNAC_FAILURE;
                }
                break;
            case PNAC_DPNAC_PERIODIC_TIMER:
                if (gPnacSystemInfo.u1DPnacRolePlayed ==
                    PNAC_DPNAC_SYSTEM_ROLE_MASTER)
                {
                    DPnacHandleKeepAlive (PNAC_DPNAC_KEEP_ALIVE_TIMER_EXPIRY);
                }
                else
                {
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                              ("DPNAC: Periodic Sync PDU triggered"));
                    DPnacPktInfo.u1PktType = DPNAC_PERIODIC_SYNC_PDU;

                    if (DPnacPaeTxEapPkt (&DPnacPktInfo) != PNAC_SUCCESS)
                    {
                        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                                  ("TMR: PnacTmrHandler: Packet transmission failed "));
                    }

                }

                PnacTmrStartTmr (&gPnacSystemInfo, PNAC_DPNAC_PERIODIC_TIMER,
                                 gPnacSystemInfo.u4DPnacPeriodicSyncTime);
                break;

            default:
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                          " TMR: PnacTmrHandler: Improper Timer type expiry\n");
                i4RetVal = PNAC_FAILURE;

        }                        /* switch TimerType ends */

        if (i4RetVal == PNAC_FAILURE)
        {
            return PNAC_FAILURE;
        }
    }                            /* while loop for every timer ends */

    return i4RetVal;

}                                /* PnacTmrHandler ends */

/*****************************************************************************/
/* Function Name      : PnacTmrStartTmr                                      */
/*                                                                           */
/* Description        : This function is called whenever a timer needs to    */
/*                      be started by any module/function. The function      */
/*                      allocates a timer node and starts the timer for the  */
/*                      specified duration for that session (u4SessionId)    */
/*                                                                           */
/* Input(s)           : *pSessionNode - Pointer to the session node          */
/*                      u1TimerType - The type of timer in one particular    */
/*                                    session                                */
/*                      u4Duration - duration of the timer to be started     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.tmrListId                            */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
PnacTmrStartTmr (VOID *pSessionNode, UINT1 u1TimerType, UINT4 u4Duration)
{
    tPnacTimerNode     *pTempTimerNode = NULL;
    tPnacTmrListId      TmrListId;

    UINT4               u4MacAddr = 0;
    UINT2               u2MacAddr = 0;

    if (PNAC_NODE_STATUS () != PNAC_ACTIVE_NODE)
    {
        /*Timer should be started only in the Active node */
        return PNAC_SUCCESS;
    }

    if (pSessionNode == NULL)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  " TMR: PnacTmrStartTmr called with EMPTY Session Node\n");
        return PNAC_FAILURE;
    }

    TmrListId = gPnacSystemInfo.tmrListId;

    /* Allocate memory for temporary timer node for Timer MemPool */
    if (PNAC_ALLOCATE_TIMERNODE_MEMBLK (pTempTimerNode) == NULL)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  " TMR: MemBlock allocation for Timer node failed\n");
        return PNAC_FAILURE;
    }
    PNAC_MEMSET (pTempTimerNode, PNAC_INIT_VAL, sizeof (tPnacTimerNode));

    /* Build the temporary timer node */
    pTempTimerNode->u1TimerType = u1TimerType;

    /* Assign a pointer to temporary timer node, in the session node */
    switch (u1TimerType)
    {

        case PNAC_AUTHWHILE_TIMER:
            pTempTimerNode->u2PortNo =
                ((tPnacSuppSessionNode *) pSessionNode)->u2PortNo;
            pTempTimerNode->u2SessionId =
                ((tPnacSuppSessionNode *) pSessionNode)->suppFsmInfo.
                u2ReceivedId;
            ((tPnacSuppSessionNode *) pSessionNode)->pAuthWhileTimer =
                pTempTimerNode;
            break;

        case PNAC_HELDWHILE_TIMER:
            pTempTimerNode->u2PortNo =
                ((tPnacSuppSessionNode *) pSessionNode)->u2PortNo;
            pTempTimerNode->u2SessionId =
                ((tPnacSuppSessionNode *) pSessionNode)->suppFsmInfo.
                u2ReceivedId;
            ((tPnacSuppSessionNode *) pSessionNode)->pHeldWhileTimer =
                pTempTimerNode;
            break;

        case PNAC_STARTWHEN_TIMER:
            pTempTimerNode->u2PortNo =
                ((tPnacSuppSessionNode *) pSessionNode)->u2PortNo;
            pTempTimerNode->u2SessionId =
                ((tPnacSuppSessionNode *) pSessionNode)->suppFsmInfo.
                u2ReceivedId;
            ((tPnacSuppSessionNode *) pSessionNode)->pStartWhenTimer =
                pTempTimerNode;
            break;

        case PNAC_QUIETWHILE_TIMER:
            pTempTimerNode->u2PortNo =
                ((tPnacAuthSessionNode *) pSessionNode)->u2PortNo;
            pTempTimerNode->u2SessionId =
                ((tPnacAuthSessionNode *) pSessionNode)->authFsmInfo.
                u2CurrentId;
            ((tPnacAuthSessionNode *) pSessionNode)->pQuietWhileTimer =
                pTempTimerNode;
            pTempTimerNode->pAuthSessNode =
                (tPnacAuthSessionNode *) pSessionNode;
            PNAC_TRC_ARG2 (PNAC_CONTROL_PATH_TRC,
                           " TMR: Quiet While Timer Started for Id:%d, Duration: %d !!\n",
                           pTempTimerNode->u2SessionId, u4Duration);
            break;

        case PNAC_TXWHEN_TIMER:
            pTempTimerNode->u2PortNo =
                ((tPnacAuthSessionNode *) pSessionNode)->u2PortNo;
            pTempTimerNode->u2SessionId =
                ((tPnacAuthSessionNode *) pSessionNode)->authFsmInfo.
                u2CurrentId;
            ((tPnacAuthSessionNode *) pSessionNode)->pTxWhenTimer =
                pTempTimerNode;
            pTempTimerNode->pAuthSessNode =
                (tPnacAuthSessionNode *) pSessionNode;
            break;

        case PNAC_REAUTHWHEN_TIMER:
            pTempTimerNode->u2PortNo =
                ((tPnacAuthSessionNode *) pSessionNode)->u2PortNo;
            pTempTimerNode->u2SessionId =
                ((tPnacAuthSessionNode *) pSessionNode)->authFsmInfo.
                u2CurrentId;
            ((tPnacAuthSessionNode *) pSessionNode)->pReAuthWhenTimer =
                pTempTimerNode;
            pTempTimerNode->pAuthSessNode =
                (tPnacAuthSessionNode *) pSessionNode;
            PNAC_TRC_ARG2 (PNAC_CONTROL_PATH_TRC,
                           " TMR: Reauth When Timer Started for Id:%d, Duration: %d !!\n",
                           pTempTimerNode->u2SessionId, u4Duration);
            break;

        case PNAC_AWHILE_TIMER:
            pTempTimerNode->u2PortNo =
                ((tPnacAuthSessionNode *) pSessionNode)->u2PortNo;
            pTempTimerNode->u2SessionId =
                ((tPnacAuthSessionNode *) pSessionNode)->authFsmInfo.
                u2CurrentId;
            ((tPnacAuthSessionNode *) pSessionNode)->pAWhileTimer =
                pTempTimerNode;
            pTempTimerNode->pAuthSessNode =
                (tPnacAuthSessionNode *) pSessionNode;
            break;
        case PNAC_SERVERCACHE_TIMER:
            ((tPnacServAuthCache *) pSessionNode)->pTmrNode = pTempTimerNode;
            pTempTimerNode->pCacheNode = (tPnacServAuthCache *) pSessionNode;
            PNAC_GET_FOURBYTE (u4MacAddr, pTempTimerNode->pCacheNode->macAddr);
            PNAC_GET_TWOBYTE (u2MacAddr,
                              &(pTempTimerNode->pCacheNode->macAddr[4]));
            PNAC_TRC_ARG3 (PNAC_CONTROL_PATH_TRC,
                           " TMR:Server Cache Timer Started for MAC:%x-%x, Duration: %u !!\n",
                           u4MacAddr, u2MacAddr, u4Duration);
            break;
        case PNAC_DPNAC_PERIODIC_TIMER:
            ((tgPnacSystemInfo *) pSessionNode)->DPnacPeriodicSyncTmr =
                pTempTimerNode;
            break;

        default:
            MemReleaseMemBlock (PNAC_TIMERNODE_MEMPOOL_ID,
                                (UINT1 *) (pTempTimerNode));
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      " TMR: Improper Timer type to start\n");
            return PNAC_FAILURE;    /* do nothing on default */

    }                            /*switch on TimerType ends */

    /* Start the Timer */
    u4Duration = u4Duration * PNAC_NUM_OF_TIME_UNITS_IN_A_SEC;
    if (PNAC_START_TIMER (TmrListId,
                          &pTempTimerNode->appTimer,
                          u4Duration) != PNAC_TMR_SUCCESS)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  " TMR: Starting FSAP timer failed\n");
        return PNAC_FAILURE;
    }
    PNAC_TRC (PNAC_CONTROL_PATH_TRC, " TMR: Timer is started\n");
    return PNAC_SUCCESS;

}                                /* PnacTmrStartTmr ends */

/*****************************************************************************/
/* Function Name      : PnacTmrStopTmr                                       */
/*                                                                           */
/* Description        : This function is called whenever a timer needs to    */
/*                      be stopped by any module/function. The function      */
/*                      removes the timer node from the Timer List and       */
/*                      frees the timer node memory block to the memory pool.*/
/*                                                                           */
/* Input(s)           : *pSessionNode - Pointer to the session node.         */
/*                      u2PortNo - Port number.                              */
/*                      u1TimerType - type of timer that is to be stopped.   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.tmrListId                            */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

INT4
PnacTmrStopTmr (VOID *pSessionNode, UINT1 u1TimerType)
{
    tPnacTimerNode    **ppTempTimerNode = NULL;
    tPnacTmrListId      TmrListId;

    if ((PNAC_NODE_STATUS () != PNAC_ACTIVE_NODE) &&
        (PNAC_NODE_STATUS () != PNAC_ACTIVE_TO_STANDBY_IN_PROGRESS) &&
        (PNAC_NODE_STATUS () != PNAC_SHUT_START_IN_PROGRESS))
    {
        /*Timer will be started only in the Active node */
        return PNAC_SUCCESS;
    }

    if (pSessionNode == NULL)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  " TMR: PnacTmrStopTmr called with EMPTY Session Node\n");
        return PNAC_FAILURE;
    }

    TmrListId = gPnacSystemInfo.tmrListId;

    /* Get the timer node */
    switch (u1TimerType)
    {

        case PNAC_AUTHWHILE_TIMER:
            ppTempTimerNode =
                &(((tPnacSuppSessionNode *) pSessionNode)->pAuthWhileTimer);
            break;

        case PNAC_HELDWHILE_TIMER:
            ppTempTimerNode =
                &(((tPnacSuppSessionNode *) pSessionNode)->pHeldWhileTimer);
            break;

        case PNAC_STARTWHEN_TIMER:
            ppTempTimerNode =
                &(((tPnacSuppSessionNode *) pSessionNode)->pStartWhenTimer);
            break;

        case PNAC_QUIETWHILE_TIMER:
            ppTempTimerNode =
                &(((tPnacAuthSessionNode *) pSessionNode)->pQuietWhileTimer);
            break;

        case PNAC_TXWHEN_TIMER:
            ppTempTimerNode =
                &(((tPnacAuthSessionNode *) pSessionNode)->pTxWhenTimer);
            break;

        case PNAC_REAUTHWHEN_TIMER:
            ppTempTimerNode =
                &(((tPnacAuthSessionNode *) pSessionNode)->pReAuthWhenTimer);
            break;

        case PNAC_AWHILE_TIMER:
            ppTempTimerNode =
                &(((tPnacAuthSessionNode *) pSessionNode)->pAWhileTimer);
            break;

        case PNAC_SERVERCACHE_TIMER:
            PNAC_TRC (PNAC_CONTROL_PATH_TRC, " TMR: Cache Timer is stopped\n");
            ppTempTimerNode =
                &(((tPnacServAuthCache *) pSessionNode)->pTmrNode);
            break;

        case PNAC_DPNAC_PERIODIC_TIMER:
            PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                      " DPNAC Periodic Timer is stopped\n");
            ppTempTimerNode = &(gPnacSystemInfo.DPnacPeriodicSyncTmr);
            break;

        default:
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      " TMR: Improper Timer type to stop\n");
            return PNAC_FAILURE;

    }                            /* switch on TimerType ends */

    if (*ppTempTimerNode == NULL)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  " TMR: Empty Timer Node, Timer stopping failed \n");
        return PNAC_FAILURE;
    }

    if (PNAC_STOP_TIMER (TmrListId, &(*ppTempTimerNode)->appTimer)
        == PNAC_TMR_FAILURE)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  " TMR: Stopping FSAP Timer failed\n");
        return PNAC_FAILURE;
    }

    /* Free the memory for the timer node, back to Timer MemPool */
    if (PNAC_RELEASE_TIMERNODE_MEMBLK (*ppTempTimerNode) != PNAC_MEM_SUCCESS)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                  PNAC_CONTROL_PATH_TRC,
                  " TMR: Releasing MemBlock for Timer Node failed\n");
        return PNAC_FAILURE;
    }
    *ppTempTimerNode = NULL;
    PNAC_TRC (PNAC_CONTROL_PATH_TRC, " TMR: Timer is stopped\n");
    return PNAC_SUCCESS;

}                                /* PnacTmrStopTmr ends */

/*****************************************************************************/
/* Function Name      : PnacTmrDeInit                                        */
/*                                                                           */
/* Description        : This function stops any timer which is running and   */
/*                      de-initialises all the timer related entities such   */
/*                      as Timer management library, Timer Task, Timer       */
/*                      memory pool and Timer List.                          */
/*                                                                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSystemInfo.tmrTaskId,                           */
/*                      gPnacSystemInfo.tmrPoolId,                           */
/*                      gPnacSystemInfo.tmrListId                            */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

VOID
PnacTmrDeinit (VOID)
{
    PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC |
              PNAC_OS_RESOURCE_TRC, " TMR: Timer is Deinitializing ...\n");

    /* Delete the Timer list, which also stops all the running timers */
    if (gPnacSystemInfo.tmrListId != PNAC_INIT_VAL)
    {
        if (PNAC_DELETE_TIMERLIST (gPnacSystemInfo.tmrListId)
            != PNAC_TMR_SUCCESS)
        {

            PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_ALL_FAILURE_TRC |
                      PNAC_OS_RESOURCE_TRC | PNAC_CONTROL_PATH_TRC,
                      " TMR: Deleting Timer list failed\n");
        }
        gPnacSystemInfo.tmrListId = PNAC_INIT_VAL;
    }

    PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC |
              PNAC_OS_RESOURCE_TRC, " TMR: Timer list deleted \n");

    PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC |
              PNAC_OS_RESOURCE_TRC, " TMR: Timer is Deinitialized\n");

    return;

}                                /* PnacTmrDeinit ends */

/*****************************************************************************/
/* Function Name      : PnacTmrGetRemainingTime                              */
/*                                                                           */
/* Description        : This function is called whenever a timer needs to    */
/*                      be find how much time remaining for time out         */
/*                      by any module/function. The function returns the     */
/*                      remaining time to expire, for the specified timer.   */
/*                                                                           */
/* Input(s)           : *pSessionNode - Pointer to the session node          */
/*                      u1TimerType - The type of timer in one particular    */
/*                                    session                                */
/*                      pu4RemainingTime - The remaining time, to expire in  */
/*                                         secs                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.tmrListId                            */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

INT4
PnacTmrGetRemainingTime (VOID *pSessionNode, UINT1 u1TimerType,
                         UINT4 *pu4RemainingTime)
{
    tPnacTimerNode    **ppTempTimerNode = NULL;
    tPnacTmrListId      TmrListId;

    if (pSessionNode == NULL)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  " TMR: PnacTmrGetRemainingTime called with EMPTY Session Node\n");
        return PNAC_FAILURE;
    }

    TmrListId = gPnacSystemInfo.tmrListId;

    /* Get the timer node */
    switch (u1TimerType)
    {

        case PNAC_AUTHWHILE_TIMER:
            ppTempTimerNode =
                &(((tPnacSuppSessionNode *) pSessionNode)->pAuthWhileTimer);
            break;

        case PNAC_HELDWHILE_TIMER:
            ppTempTimerNode =
                &(((tPnacSuppSessionNode *) pSessionNode)->pHeldWhileTimer);
            break;

        case PNAC_STARTWHEN_TIMER:
            ppTempTimerNode =
                &(((tPnacSuppSessionNode *) pSessionNode)->pStartWhenTimer);
            break;

        case PNAC_QUIETWHILE_TIMER:
            ppTempTimerNode =
                &(((tPnacAuthSessionNode *) pSessionNode)->pQuietWhileTimer);
            break;

        case PNAC_TXWHEN_TIMER:
            ppTempTimerNode =
                &(((tPnacAuthSessionNode *) pSessionNode)->pTxWhenTimer);
            break;

        case PNAC_REAUTHWHEN_TIMER:
            ppTempTimerNode =
                &(((tPnacAuthSessionNode *) pSessionNode)->pReAuthWhenTimer);
            break;

        case PNAC_AWHILE_TIMER:
            ppTempTimerNode =
                &(((tPnacAuthSessionNode *) pSessionNode)->pAWhileTimer);
            break;

        case PNAC_SERVERCACHE_TIMER:
            ppTempTimerNode =
                &(((tPnacServAuthCache *) pSessionNode)->pTmrNode);
            break;

        case PNAC_DPNAC_PERIODIC_TIMER:
            ppTempTimerNode = &(gPnacSystemInfo.DPnacPeriodicSyncTmr);
            break;

        default:
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      " TMR: Improper Timer type to get remaining time\n");
            return PNAC_FAILURE;

    }                            /* switch on TimerType ends */

    if (*ppTempTimerNode == NULL)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  " TMR: Empty Timer Node, PnacTmrGetRemainingTimer failed \n");
        return PNAC_FAILURE;
    }
    if (PNAC_TMR_GET_REMAINING_TIME
        (TmrListId, &(*ppTempTimerNode)->appTimer,
         pu4RemainingTime) != PNAC_TMR_SUCCESS)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  " TMR: GetRemainingTime failed\n");
        return PNAC_FAILURE;
    }
    *pu4RemainingTime = *pu4RemainingTime / PNAC_NUM_OF_TIME_UNITS_IN_A_SEC;
    return PNAC_SUCCESS;
}                                /* PnacTmrGetRemainingTime ends */
