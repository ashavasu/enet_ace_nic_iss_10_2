/* $Id: fspnacwr.c,v 1.13 2015/06/11 10:03:16 siva Exp $*/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fspnacwr.h"
# include  "fspnacdb.h"
#include   "pnachdrs.h"

VOID
RegisterFSPNAC ()
{
    SNMPRegisterMibWithLock (&fspnacOID, &fspnacEntry, PnacLock, PnacUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fspnacOID, (const UINT1 *) "fspnac");
}

VOID
UnRegisterFSPNAC ()
{
    SNMPUnRegisterMib (&fspnacOID, &fspnacEntry);
    SNMPDelSysorEntry (&fspnacOID, (const UINT1 *) "fspnac");
}

INT4
FsDPnacSystemStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsDPnacSystemStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsDPnacPeriodicSyncTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsDPnacPeriodicSyncTime (&(pMultiData->u4_ULongValue)));
}

INT4
FsDPnacMaxKeepAliveCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsDPnacMaxKeepAliveCount (&(pMultiData->i4_SLongValue)));
}

INT4
FsDPnacSystemStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsDPnacSystemStatus (pMultiData->i4_SLongValue));
}

INT4
FsDPnacPeriodicSyncTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsDPnacPeriodicSyncTime (pMultiData->u4_ULongValue));
}

INT4
FsDPnacMaxKeepAliveCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsDPnacMaxKeepAliveCount (pMultiData->i4_SLongValue));
}

INT4
FsDPnacSystemStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsDPnacSystemStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsDPnacPeriodicSyncTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsDPnacPeriodicSyncTime
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsDPnacMaxKeepAliveCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsDPnacMaxKeepAliveCount
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsDPnacSystemStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDPnacSystemStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDPnacPeriodicSyncTimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDPnacPeriodicSyncTime
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDPnacMaxKeepAliveCountDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDPnacMaxKeepAliveCount
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsDPnacStatsTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDPnacStatsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDPnacStatsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsDPnacEventUpdateFramesRxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDPnacStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDPnacEventUpdateFramesRx
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsDPnacEventUpdateFramesTxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDPnacStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDPnacEventUpdateFramesTx
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsDPnacPeriodicFramesTxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDPnacStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDPnacPeriodicFramesTx (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsDPnacPeriodicFramesRxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDPnacStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDPnacPeriodicFramesRx (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexFsDPnacSlotPortTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDPnacSlotPortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDPnacSlotPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsDPnacPortAuthStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDPnacSlotPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDPnacPortAuthStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsDPnacPortControlledDirectionGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDPnacSlotPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDPnacPortControlledDirection
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));
}

INT4
FsPnacSystemControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPnacSystemControl (&(pMultiData->i4_SLongValue)));
}

INT4
FsPnacTraceOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPnacTraceOption (&(pMultiData->i4_SLongValue)));
}

INT4
FsPnacAuthenticServerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPnacAuthenticServer (&(pMultiData->i4_SLongValue)));
}

INT4
FsPnacNasIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPnacNasId (pMultiData->pOctetStrValue));
}

INT4
FsPnacSystemControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPnacSystemControl (pMultiData->i4_SLongValue));
}

INT4
FsPnacTraceOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPnacTraceOption (pMultiData->i4_SLongValue));
}

INT4
FsPnacAuthenticServerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPnacAuthenticServer (pMultiData->i4_SLongValue));
}

INT4
FsPnacNasIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPnacNasId (pMultiData->pOctetStrValue));
}

INT4
FsPnacSystemControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPnacSystemControl (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsPnacTraceOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPnacTraceOption (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsPnacAuthenticServerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPnacAuthenticServer
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsPnacNasIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPnacNasId (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsPnacSystemControlDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPnacSystemControl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPnacTraceOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPnacTraceOption
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPnacAuthenticServerDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPnacAuthenticServer
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPnacNasIdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPnacNasId (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsPnacPaePortTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsPnacPaePortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsPnacPaePortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsPnacPaePortAuthModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacPaePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacPaePortAuthMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsPnacPaePortSupplicantCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacPaePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacPaePortSupplicantCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsPnacPaePortUserNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacPaePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacPaePortUserName (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsPnacPaePortPasswordGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacPaePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacPaePortPassword (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsPnacPaePortStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacPaePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacPaePortStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4 FsPnacPaePortStatisticsClearSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhSetFsPnacPaePortStatisticsClear(
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiData->i4_SLongValue));

}

INT4 FsPnacPaePortStatisticsClearTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhTestv2FsPnacPaePortStatisticsClear(pu4Error,
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiData->i4_SLongValue));

}

INT4 FsPnacPaePortAuthStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsPnacPaePortAuthStatus(pu4Error,
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->i4_SLongValue));

}

INT4 FsPnacPaeAuthReAuthMaxTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsPnacPaeAuthReAuthMax(pu4Error,
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->u4_ULongValue));

}

INT4 FsPnacPaePortStatisticsClearGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        if (nmhValidateIndexInstanceFsPnacPaePortTable(
                pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
        {
                return SNMP_FAILURE;
        }
        return(nmhGetFsPnacPaePortStatisticsClear(
                pMultiIndex->pIndex[0].i4_SLongValue,
                &(pMultiData->i4_SLongValue)));

}

INT4 FsPnacPaePortAuthStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacPaePortTable(
        pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsPnacPaePortAuthStatus(
        pMultiIndex->pIndex[0].i4_SLongValue,
        &(pMultiData->i4_SLongValue)));

}

INT4 FsPnacPaeAuthReAuthMaxGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacPaePortTable(
        pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsPnacPaeAuthReAuthMax(
        pMultiIndex->pIndex[0].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}

INT4
FsPnacPaePortAuthModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPnacPaePortAuthMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsPnacPaePortUserNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPnacPaePortUserName (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsPnacPaePortPasswordSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPnacPaePortPassword (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4 FsPnacPaePortAuthStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPnacPaePortAuthStatus(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->i4_SLongValue));

}

INT4 FsPnacPaeAuthReAuthMaxSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPnacPaeAuthReAuthMax(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->u4_ULongValue));

}

INT4
FsPnacPaePortAuthModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsPnacPaePortAuthMode (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsPnacPaePortUserNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsPnacPaePortUserName (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
FsPnacPaePortPasswordTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsPnacPaePortPassword (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
FsPnacPaePortTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPnacPaePortTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPnacModuleOperStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPnacModuleOperStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsPnacRemoteAuthServerTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPnacRemoteAuthServerType (&(pMultiData->i4_SLongValue)));
}

INT4
FsPnacRemoteAuthServerTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPnacRemoteAuthServerType (pMultiData->i4_SLongValue));
}

INT4
FsPnacRemoteAuthServerTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPnacRemoteAuthServerType
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsPnacRemoteAuthServerTypeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPnacRemoteAuthServerType
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsPnacAuthSessionTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsPnacAuthSessionTable ((tMacAddr *)
                                                    pNextMultiIndex->pIndex[0].
                                                    pOctetStrValue->
                                                    pu1_OctetList) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsPnacAuthSessionTable
            (*(tMacAddr *) pFirstMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
FsPnacAuthSessionIdentifierGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacAuthSessionTable ((*(tMacAddr *)
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacAuthSessionIdentifier ((*(tMacAddr *) pMultiIndex->
                                                pIndex[0].pOctetStrValue->
                                                pu1_OctetList),
                                               &(pMultiData->i4_SLongValue)));

}

INT4
FsPnacAuthSessionAuthPaeStateGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacAuthSessionTable ((*(tMacAddr *)
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacAuthSessionAuthPaeState ((*(tMacAddr *) pMultiIndex->
                                                  pIndex[0].pOctetStrValue->
                                                  pu1_OctetList),
                                                 &(pMultiData->i4_SLongValue)));

}

INT4
FsPnacAuthSessionBackendAuthStateGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacAuthSessionTable ((*(tMacAddr *)
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacAuthSessionBackendAuthState ((*(tMacAddr *)
                                                      pMultiIndex->pIndex[0].
                                                      pOctetStrValue->
                                                      pu1_OctetList),
                                                     &(pMultiData->
                                                       i4_SLongValue)));

}

INT4
FsPnacAuthSessionPortStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacAuthSessionTable ((*(tMacAddr *)
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacAuthSessionPortStatus ((*(tMacAddr *) pMultiIndex->
                                                pIndex[0].pOctetStrValue->
                                                pu1_OctetList),
                                               &(pMultiData->i4_SLongValue)));

}

INT4
FsPnacAuthSessionPortNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacAuthSessionTable ((*(tMacAddr *)
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacAuthSessionPortNumber ((*(tMacAddr *) pMultiIndex->
                                                pIndex[0].pOctetStrValue->
                                                pu1_OctetList),
                                               &(pMultiData->i4_SLongValue)));

}

INT4
FsPnacAuthSessionInitializeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacAuthSessionTable ((*(tMacAddr *)
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacAuthSessionInitialize ((*(tMacAddr *) pMultiIndex->
                                                pIndex[0].pOctetStrValue->
                                                pu1_OctetList),
                                               &(pMultiData->i4_SLongValue)));

}

INT4
FsPnacAuthSessionReauthenticateGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacAuthSessionTable ((*(tMacAddr *)
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacAuthSessionReauthenticate ((*(tMacAddr *) pMultiIndex->
                                                    pIndex[0].pOctetStrValue->
                                                    pu1_OctetList),
                                                   &(pMultiData->
                                                     i4_SLongValue)));

}

INT4
FsPnacAuthSessionInitializeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPnacAuthSessionInitialize ((*(tMacAddr *) pMultiIndex->
                                                pIndex[0].pOctetStrValue->
                                                pu1_OctetList),
                                               pMultiData->i4_SLongValue));

}

INT4
FsPnacAuthSessionReauthenticateSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsPnacAuthSessionReauthenticate ((*(tMacAddr *) pMultiIndex->
                                                    pIndex[0].pOctetStrValue->
                                                    pu1_OctetList),
                                                   pMultiData->i4_SLongValue));

}

INT4
FsPnacAuthSessionInitializeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsPnacAuthSessionInitialize (pu4Error,
                                                  (*(tMacAddr *) pMultiIndex->
                                                   pIndex[0].pOctetStrValue->
                                                   pu1_OctetList),
                                                  pMultiData->i4_SLongValue));

}

INT4
FsPnacAuthSessionReauthenticateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsPnacAuthSessionReauthenticate (pu4Error,
                                                      (*(tMacAddr *)
                                                       pMultiIndex->pIndex[0].
                                                       pOctetStrValue->
                                                       pu1_OctetList),
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
FsPnacAuthSessionTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPnacAuthSessionTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsPnacAuthSessionStatsTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsPnacAuthSessionStatsTable ((tMacAddr *)
                                                         pNextMultiIndex->
                                                         pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsPnacAuthSessionStatsTable
            (*(tMacAddr *) pFirstMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
FsPnacAuthSessionOctetsRxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacAuthSessionStatsTable ((*(tMacAddr *)
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              pOctetStrValue->
                                                              pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacAuthSessionOctetsRx ((*(tMacAddr *) pMultiIndex->
                                              pIndex[0].pOctetStrValue->
                                              pu1_OctetList),
                                             &(pMultiData->u8_Counter64Value)));

}

INT4
FsPnacAuthSessionOctetsTxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacAuthSessionStatsTable ((*(tMacAddr *)
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              pOctetStrValue->
                                                              pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacAuthSessionOctetsTx ((*(tMacAddr *) pMultiIndex->
                                              pIndex[0].pOctetStrValue->
                                              pu1_OctetList),
                                             &(pMultiData->u8_Counter64Value)));

}

INT4
FsPnacAuthSessionFramesRxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacAuthSessionStatsTable ((*(tMacAddr *)
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              pOctetStrValue->
                                                              pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacAuthSessionFramesRx ((*(tMacAddr *) pMultiIndex->
                                              pIndex[0].pOctetStrValue->
                                              pu1_OctetList),
                                             &(pMultiData->u4_ULongValue)));

}

INT4
FsPnacAuthSessionFramesTxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacAuthSessionStatsTable ((*(tMacAddr *)
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              pOctetStrValue->
                                                              pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacAuthSessionFramesTx ((*(tMacAddr *) pMultiIndex->
                                              pIndex[0].pOctetStrValue->
                                              pu1_OctetList),
                                             &(pMultiData->u4_ULongValue)));

}

INT4
FsPnacAuthSessionIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacAuthSessionStatsTable ((*(tMacAddr *)
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              pOctetStrValue->
                                                              pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacAuthSessionId ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                        pOctetStrValue->pu1_OctetList),
                                       pMultiData->pOctetStrValue));

}

INT4
FsPnacAuthSessionAuthenticMethodGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacAuthSessionStatsTable ((*(tMacAddr *)
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              pOctetStrValue->
                                                              pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacAuthSessionAuthenticMethod ((*(tMacAddr *) pMultiIndex->
                                                     pIndex[0].pOctetStrValue->
                                                     pu1_OctetList),
                                                    &(pMultiData->
                                                      i4_SLongValue)));

}

INT4
FsPnacAuthSessionTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacAuthSessionStatsTable ((*(tMacAddr *)
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              pOctetStrValue->
                                                              pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacAuthSessionTime ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                          pOctetStrValue->pu1_OctetList),
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsPnacAuthSessionTerminateCauseGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacAuthSessionStatsTable ((*(tMacAddr *)
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              pOctetStrValue->
                                                              pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacAuthSessionTerminateCause ((*(tMacAddr *) pMultiIndex->
                                                    pIndex[0].pOctetStrValue->
                                                    pu1_OctetList),
                                                   &(pMultiData->
                                                     i4_SLongValue)));

}

INT4
FsPnacAuthSessionUserNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacAuthSessionStatsTable ((*(tMacAddr *)
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              pOctetStrValue->
                                                              pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacAuthSessionUserName ((*(tMacAddr *) pMultiIndex->
                                              pIndex[0].pOctetStrValue->
                                              pu1_OctetList),
                                             pMultiData->pOctetStrValue));

}

INT4 FsPnacAuthSessionStatisticsClearGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        if (nmhValidateIndexInstanceFsPnacAuthSessionStatsTable(
                (*(tMacAddr *)pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
        {
                return SNMP_FAILURE;
        }
        return(nmhGetFsPnacAuthSessionStatisticsClear(
                (*(tMacAddr *)pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList),
                &(pMultiData->i4_SLongValue)));

}
INT4 FsPnacAuthSessionStatisticsClearSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhSetFsPnacAuthSessionStatisticsClear(
                (*(tMacAddr *)pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList),
                pMultiData->i4_SLongValue));

}

INT4 FsPnacAuthSessionStatisticsClearTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhTestv2FsPnacAuthSessionStatisticsClear(pu4Error,
                (*(tMacAddr *)pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList),
                pMultiData->i4_SLongValue));

}

INT4
GetNextIndexFsPnacASUserConfigTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsPnacASUserConfigTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsPnacASUserConfigTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsPnacASUserConfigPasswordGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacASUserConfigTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacASUserConfigPassword
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsPnacASUserConfigAuthProtocolGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacASUserConfigTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacASUserConfigAuthProtocol
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsPnacASUserConfigAuthTimeoutGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacASUserConfigTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacASUserConfigAuthTimeout
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsPnacASUserConfigPortListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacASUserConfigTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacASUserConfigPortList
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsPnacASUserConfigPermissionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacASUserConfigTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacASUserConfigPermission
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsPnacASUserConfigRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacASUserConfigTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacASUserConfigRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsPnacASUserConfigPasswordSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPnacASUserConfigPassword
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsPnacASUserConfigAuthTimeoutSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsPnacASUserConfigAuthTimeout
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
FsPnacASUserConfigPortListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPnacASUserConfigPortList
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsPnacASUserConfigPermissionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPnacASUserConfigPermission
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsPnacASUserConfigRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPnacASUserConfigRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsPnacASUserConfigPasswordTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsPnacASUserConfigPassword (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 pOctetStrValue,
                                                 pMultiData->pOctetStrValue));

}

INT4
FsPnacASUserConfigAuthTimeoutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsPnacASUserConfigAuthTimeout (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    pOctetStrValue,
                                                    pMultiData->u4_ULongValue));

}

INT4
FsPnacASUserConfigPortListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsPnacASUserConfigPortList (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 pOctetStrValue,
                                                 pMultiData->pOctetStrValue));

}

INT4
FsPnacASUserConfigPermissionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsPnacASUserConfigPermission (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsPnacASUserConfigRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsPnacASUserConfigRowStatus (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsPnacASUserConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPnacASUserConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsPnacTrapAuthSessionTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsPnacTrapAuthSessionTable ((tMacAddr *)
                                                        pNextMultiIndex->
                                                        pIndex[0].
                                                        pOctetStrValue->
                                                        pu1_OctetList) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsPnacTrapAuthSessionTable
            (*(tMacAddr *) pFirstMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
FsPnacTrapAuthSessionStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPnacTrapAuthSessionTable ((*(tMacAddr *)
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             pOctetStrValue->
                                                             pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPnacTrapAuthSessionStatus ((*(tMacAddr *) pMultiIndex->
                                                pIndex[0].pOctetStrValue->
                                                pu1_OctetList),
                                               &(pMultiData->i4_SLongValue)));

}
