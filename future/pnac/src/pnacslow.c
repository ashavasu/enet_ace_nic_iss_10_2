/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pnacslow.c,v 1.61 2015/07/01 11:59:29 siva Exp $
 *
 * Description     : This file contains Authentication
 *                   functions of PNAC module
 *******************************************************************/

#include  "pnachdrs.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1xPaeSystemAuthControl
 Input       :  The Indices

                The Object 
                retValDot1xPaeSystemAuthControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xPaeSystemAuthControl ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xPaeSystemAuthControl (INT4 *pi4RetValDot1xPaeSystemAuthControl)
#else /*  */
INT1
nmhGetDot1xPaeSystemAuthControl (pi4RetValDot1xPaeSystemAuthControl)
     INT4               *pi4RetValDot1xPaeSystemAuthControl;

#endif /*  */
{
    if (PNAC_IS_SHUTDOWN ())

    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: Pnac is not started to get enabled/disabled status !!\n");
        *pi4RetValDot1xPaeSystemAuthControl = (INT4) PNAC_DISABLED;
        return SNMP_SUCCESS;
    }
    *pi4RetValDot1xPaeSystemAuthControl = (INT4) PNAC_PROTOCOL_ADMIN_STATUS ();
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1xPaeSystemAuthControl
 Input       :  The Indices

                The Object 
                setValDot1xPaeSystemAuthControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS 
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1xPaeSystemAuthControl ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetDot1xPaeSystemAuthControl (INT4 i4SetValDot1xPaeSystemAuthControl)
#else /*  */
INT1
nmhSetDot1xPaeSystemAuthControl (i4SetValDot1xPaeSystemAuthControl)
     INT4                i4SetValDot1xPaeSystemAuthControl;

#endif /*  */
{
    INT4                i4RetVal;
    /* Compare the existing configured value and given set value. */
    if (i4SetValDot1xPaeSystemAuthControl == PNAC_PROTOCOL_ADMIN_STATUS ())
    {
        PNAC_TRC (PNAC_MGMT_TRC, " SNMPPROP: Value Configured already \n");
        return SNMP_SUCCESS;
    }

    PNAC_PROTOCOL_ADMIN_STATUS () = (UINT1) i4SetValDot1xPaeSystemAuthControl;

    if (i4SetValDot1xPaeSystemAuthControl == (INT4) PNAC_ENABLED)

    {
        /* As the protocol admin status is already set to enabled, there
         * is no need to check admin status here. */
        i4RetVal = PnacModuleEnable ();
        if (i4RetVal != PNAC_SUCCESS)

        {
            /* Revert the protocol module status here. Otherwise, we can't
             * enable the module again. */
            PNAC_PROTOCOL_ADMIN_STATUS () = PNAC_DISABLED;

            CLI_SET_ERR (CLI_PNAC_HW_INIT_FAILED);
            PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                      " SNMPSTD: PnacModuleEnable failed\n");
            return SNMP_FAILURE;
        }
        PNAC_TRC (PNAC_MGMT_TRC, " SNMPSTD: Module Enabled\n");
    }

    else if (i4SetValDot1xPaeSystemAuthControl == PNAC_DISABLED)

    {
        PnacModuleDisable ();
        PNAC_TRC (PNAC_MGMT_TRC, " SNMPSTD: Module Disabled\n");
    }

    else

    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaeSystemAuthControl = %d\n", i4SetValDot1xPaeSystemAuthControl); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1xPaeSystemAuthControl
 Input       :  The Indices

                The Object 
                testValDot1xPaeSystemAuthControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1xPaeSystemAuthControl ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1xPaeSystemAuthControl (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValDot1xPaeSystemAuthControl)
#else /*  */
INT1
nmhTestv2Dot1xPaeSystemAuthControl (pu4ErrorCode,
                                    i4TestValDot1xPaeSystemAuthControl)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValDot1xPaeSystemAuthControl;

#endif /*  */
{
    UINT2               u2PortNum = 0;
    tPnacPaePortEntry  *pPortInfo = NULL;
    UINT1               u1TunnelStatus;

    if (PNAC_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: Pnac is not started to be enabled/disabled !!\n");
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1xPaeSystemAuthControl != PNAC_ENABLED) &&
        (i4TestValDot1xPaeSystemAuthControl != PNAC_DISABLED))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* PNAC should not be enabled if VLAN tunnelling is enabled on any of the 
     * ports */

    if (!PNAC_IS_ENABLED ())
    {
        /* PNAC module is currently disabled */
        if (i4TestValDot1xPaeSystemAuthControl == PNAC_ENABLED)
        {
            for (u2PortNum = PNAC_MINPORTS; u2PortNum <= PNAC_MAXPORTS;
                 u2PortNum++)
            {
                if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
                {
                    continue;
                }

                PnacL2IwfGetProtocolTunnelStatusOnPort (u2PortNum,
                                                        L2_PROTO_DOT1X,
                                                        &u1TunnelStatus);

                if ((u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||
                    (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD))
                {
                    /* Tunnelling is enabled on this port...
                     * dont allow the set */
                    PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                              "SNMPSTD: VLAN tunnelling is enabled on one "
                              "or more ports.PNAC cannot be enabled " "!!\n");
                    *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_PNAC_TUNNEL_ERR);
                    return SNMP_FAILURE;
                }
            }
        }
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaeSystemAuthControl = %d\n", i4TestValDot1xPaeSystemAuthControl); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1xPaeSystemAuthControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1xPaeSystemAuthControl (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1xPaePortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1xPaePortTable
 Input       :  The Indices
                Dot1xPaePortNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceDot1xPaePortTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceDot1xPaePortTable (INT4 i4Dot1xPaePortNumber)
#else /*  */
INT1
nmhValidateIndexInstanceDot1xPaePortTable (i4Dot1xPaePortNumber)
     INT4                i4Dot1xPaePortNumber;

#endif /*  */
{
    if (PNAC_IS_SHUTDOWN ())

    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowValidatePortNumber (i4Dot1xPaePortNumber) == PNAC_SUCCESS)

    {
        return SNMP_SUCCESS;
    }

    else

    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1xPaePortTable
 Input       :  The Indices
                Dot1xPaePortNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstDot1xPaePortTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexDot1xPaePortTable (INT4 *pi4Dot1xPaePortNumber)
#else /*  */
INT1
nmhGetFirstIndexDot1xPaePortTable (pi4Dot1xPaePortNumber)
     INT4               *pi4Dot1xPaePortNumber;

#endif /*  */
{
    if (PNAC_IS_SHUTDOWN ())

    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetFirstValidPortNumber (pi4Dot1xPaePortNumber) ==
        PNAC_SUCCESS)

    {
        return SNMP_SUCCESS;
    }

    else

    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", *pi4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1xPaePortTable
 Input       :  The Indices
                Dot1xPaePortNumber
                nextDot1xPaePortNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextDot1xPaePortTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexDot1xPaePortTable (INT4 i4Dot1xPaePortNumber,
                                  INT4 *pi4NextDot1xPaePortNumber)
#else /*  */
INT1
nmhGetNextIndexDot1xPaePortTable (i4Dot1xPaePortNumber,
                                  pi4NextDot1xPaePortNumber)
     INT4                i4Dot1xPaePortNumber;
     INT4               *pi4NextDot1xPaePortNumber;

#endif /*  */
{
    if (i4Dot1xPaePortNumber < 0)

    {
        return SNMP_FAILURE;
    }
    if (PNAC_IS_SHUTDOWN ())

    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetNextValidPortNumber
        (i4Dot1xPaePortNumber, pi4NextDot1xPaePortNumber) == PNAC_SUCCESS)

    {
        return SNMP_SUCCESS;
    }

    else

    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", *pi4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1xPaePortProtocolVersion
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xPaePortProtocolVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xPaePortProtocolVersion ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xPaePortProtocolVersion (INT4 i4Dot1xPaePortNumber,
                                   UINT4 *pu4RetValDot1xPaePortProtocolVersion)
#else /*  */
INT1
nmhGetDot1xPaePortProtocolVersion (i4Dot1xPaePortNumber,
                                   pu4RetValDot1xPaePortProtocolVersion)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xPaePortProtocolVersion;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        *pu4RetValDot1xPaePortProtocolVersion = pPortInfo->u4ProtocolVersion;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xPaePortCapabilities
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xPaePortCapabilities
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xPaePortCapabilities ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xPaePortCapabilities (INT4 i4Dot1xPaePortNumber,
                                tSNMP_OCTET_STRING_TYPE
                                * pRetValDot1xPaePortCapabilities)
#else /*  */
INT1
nmhGetDot1xPaePortCapabilities (i4Dot1xPaePortNumber,
                                pRetValDot1xPaePortCapabilities)
     INT4                i4Dot1xPaePortNumber;
     tSNMP_OCTET_STRING_TYPE *pRetValDot1xPaePortCapabilities;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        pRetValDot1xPaePortCapabilities->pu1_OctetList[0] =
            pPortInfo->u1Capabilities;
        pRetValDot1xPaePortCapabilities->i4_Length =
            sizeof (pPortInfo->u1Capabilities);
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xPaePortInitialize
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xPaePortInitialize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xPaePortInitialize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xPaePortInitialize (INT4 i4Dot1xPaePortNumber,
                              INT4 *pi4RetValDot1xPaePortInitialize)
#else /*  */
INT1
nmhGetDot1xPaePortInitialize (i4Dot1xPaePortNumber,
                              pi4RetValDot1xPaePortInitialize)
     INT4                i4Dot1xPaePortNumber;
     INT4               *pi4RetValDot1xPaePortInitialize;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    BOOL1               bInitialize = PNAC_FALSE;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
    {
        /* This object is valid only if the port is configured as Port-Based
         * In case of Mac-Based configuration return the default value */
        if (PNAC_PORT_INFO (i4Dot1xPaePortNumber).u1PortAuthMode ==
            PNAC_PORT_AUTHMODE_PORTBASED)

        {
            bInitialize =
                pPortInfo->pPortAuthSessionNode->authFsmInfo.bInitialize;
            if (bInitialize == PNAC_TRUE)

            {
                *pi4RetValDot1xPaePortInitialize = (INT4) PNAC_MIB_TRUE;
            }

            else

            {
                *pi4RetValDot1xPaePortInitialize = (INT4) PNAC_MIB_FALSE;
            }
        }

        else

        {
            *pi4RetValDot1xPaePortInitialize = (INT4) PNAC_MIB_FALSE;
        }
    }
    else if (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))
    {
        *pi4RetValDot1xPaePortInitialize = (INT4) PNAC_MIB_FALSE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xPaePortReauthenticate
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xPaePortReauthenticate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xPaePortReauthenticate ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xPaePortReauthenticate (INT4 i4Dot1xPaePortNumber,
                                  INT4 *pi4RetValDot1xPaePortReauthenticate)
#else /*  */
INT1
nmhGetDot1xPaePortReauthenticate (i4Dot1xPaePortNumber,
                                  pi4RetValDot1xPaePortReauthenticate)
     INT4                i4Dot1xPaePortNumber;
     INT4               *pi4RetValDot1xPaePortReauthenticate;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    if (PNAC_PORT_INFO (i4Dot1xPaePortNumber).u1PortAuthMode ==
        PNAC_PORT_AUTHMODE_PORTBASED)

    {
        *pi4RetValDot1xPaePortReauthenticate = (INT4) PNAC_MIB_FALSE;
    }

    else

    {

        /* This object is valid only if the port is configured as Port-Based
         * In case of Mac-Based configuration return the default value */
        *pi4RetValDot1xPaePortReauthenticate = (INT4) PNAC_MIB_FALSE;
    }

    /* When this object is read, always returned FALSE */
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1xPaePortInitialize
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                setValDot1xPaePortInitialize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1xPaePortInitialize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetDot1xPaePortInitialize (INT4 i4Dot1xPaePortNumber,
                              INT4 i4SetValDot1xPaePortInitialize)
#else /*  */
INT1
nmhSetDot1xPaePortInitialize (i4Dot1xPaePortNumber,
                              i4SetValDot1xPaePortInitialize)
     INT4                i4Dot1xPaePortNumber;
     INT4                i4SetValDot1xPaePortInitialize;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (!PNAC_IS_ENABLED ())

    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Pnac Module is not yet"
                  "ENABLED for setting 'Initialize'\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
        {
            if (i4SetValDot1xPaePortInitialize == (INT4) PNAC_MIB_TRUE)

            {
                pPortInfo->pPortAuthSessionNode->authFsmInfo.bInitialize =
                    PNAC_TRUE;
            }

            else if (i4SetValDot1xPaePortInitialize == (INT4) PNAC_MIB_FALSE)

            {
                pPortInfo->pPortAuthSessionNode->authFsmInfo.bInitialize =
                    PNAC_FALSE;
                return SNMP_SUCCESS;
            }

            /* For Authenticator capable port */

            /*  Call ASM with 'PNAC_ASM_EV_INITIALIZE' as event for this Supp */
            if (PnacAuthStateMachine (PNAC_ASM_EV_INITIALIZE,
                                      (UINT2) i4Dot1xPaePortNumber,
                                      pPortInfo->pPortAuthSessionNode,
                                      NULL, PNAC_NO_VAL) != PNAC_SUCCESS)

            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                          " SNMPSTD: ASM failed to Initialize\n");
                return SNMP_FAILURE;
            }

            /*  Call CDSM with 'PNAC_CDSM_EV_INITIALISE' as event for this port */
            if (PnacPaeCdStateMachine (PNAC_CDSM_EV_INITIALIZE,
                                       (UINT2) i4Dot1xPaePortNumber) !=
                PNAC_SUCCESS)

            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                          " SNMPSTD: CDSM failed to Initialize\n");
                return SNMP_FAILURE;
            }
        }
        if (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))

        {

            /* For Supplicant capable port */
            /*  Call SSM with 'PNAC_SSM_EV_INITIALISE' as event for this port */
            if (PnacSuppStateMachine (PNAC_SSM_EV_INITIALIZE,
                                      (UINT2) i4Dot1xPaePortNumber,
                                      pPortInfo->pPortSuppSessionNode,
                                      NULL, PNAC_NO_VAL) != PNAC_SUCCESS)

            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                          " SNMPSTD: SSM failed to Initialize\n");
                return SNMP_FAILURE;
            }
        }
        PNAC_TRC (PNAC_MGMT_TRC, " SNMPSTD: Port is set to 'Initialize'\n");
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortInitialize = %d\n", i4SetValDot1xPaePortInitialize); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetDot1xPaePortReauthenticate
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                setValDot1xPaePortReauthenticate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1xPaePortReauthenticate ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetDot1xPaePortReauthenticate (INT4 i4Dot1xPaePortNumber,
                                  INT4 i4SetValDot1xPaePortReauthenticate)
#else /*  */
INT1
nmhSetDot1xPaePortReauthenticate (i4Dot1xPaePortNumber,
                                  i4SetValDot1xPaePortReauthenticate)
     INT4                i4Dot1xPaePortNumber;
     INT4                i4SetValDot1xPaePortReauthenticate;

#endif /*  */
{
    tMacAddr            NextAddr = { 0 };
    tMacAddr            CurrentAddr = { 0 };
    INT4                i4OutCome;
    tPnacAuthSessionNode *pSessNode = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (!PNAC_IS_ENABLED ())

    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Pnac Module is not"
                  " yet ENABLED for setting 'Reauthenticate'\n");
        return SNMP_FAILURE;
    }
    else
    {
        if (PNAC_PORT_INFO (i4Dot1xPaePortNumber).u1PortAuthMode ==
                PNAC_PORT_AUTHMODE_PORTBASED)
        {
            if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo) !=
                    PNAC_SUCCESS)

            {
                return SNMP_FAILURE;
            }

            if (i4SetValDot1xPaePortReauthenticate == (INT4) PNAC_MIB_TRUE)

            {
                pPortInfo->pPortAuthSessionNode->authFsmInfo.bReAuthenticate =
                    PNAC_TRUE;
            }

            else if (i4SetValDot1xPaePortReauthenticate == (INT4) PNAC_MIB_FALSE)

            {
                pPortInfo->pPortAuthSessionNode->authFsmInfo.bReAuthenticate =
                    PNAC_FALSE;
                return SNMP_SUCCESS;
            }

            /* Call ASM with 'PNAC_ASM_EV_REAUTHENTICATE' as event for this Supp */
            if (PnacAuthStateMachine (PNAC_ASM_EV_REAUTHENTICATE,
                        (UINT2) i4Dot1xPaePortNumber,
                        pPortInfo->pPortAuthSessionNode, NULL,
                        PNAC_NO_VAL) != PNAC_SUCCESS)

            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                        " SNMPSTD: ASM failed to Reauthenticate\n");
                return SNMP_FAILURE;
            }
            PNAC_TRC (PNAC_MGMT_TRC, " SNMPSTD: Port is set to 'Reauthenticate'\n");
        }
        else
        {
            i4OutCome = (INT4) (nmhGetFirstIndexFsPnacAuthSessionTable (&NextAddr));
            if (i4OutCome == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            do
            {
                if(PnacSnmpLowGetAuthSessionNode (NextAddr,&pSessNode) == PNAC_FAILURE)
                {
                    return SNMP_FAILURE;
                }
                if((pSessNode != NULL) && ((pSessNode->u2PortNo) == i4Dot1xPaePortNumber))
                {
                    if (PnacAuthStateMachine (PNAC_ASM_EV_REAUTHENTICATE,
                                (UINT2) i4Dot1xPaePortNumber,
                                pSessNode, NULL,
                                PNAC_NO_VAL) != PNAC_SUCCESS)

                    {
                        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                                " SNMPSTD: ASM failed to Reauthenticate\n");
                        return SNMP_FAILURE;
                    }
                    PNAC_TRC (PNAC_MGMT_TRC, " SNMPSTD: Port is set to 'Reauthenticate'\n");
                }
                PNAC_MEMCPY (CurrentAddr, NextAddr, PNAC_MAC_ADDR_SIZE);
            }
            while(nmhGetNextIndexFsPnacAuthSessionTable
                    (CurrentAddr, &NextAddr) !=SNMP_FAILURE);
        }

    }
   
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortReauthenticate = %d\n", i4SetValDot1xPaePortReauthenticate); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1xPaePortInitialize
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                testValDot1xPaePortInitialize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1xPaePortInitialize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1xPaePortInitialize (UINT4 *pu4ErrorCode, INT4 i4Dot1xPaePortNumber,
                                 INT4 i4TestValDot1xPaePortInitialize)
#else /*  */
INT1
nmhTestv2Dot1xPaePortInitialize (pu4ErrorCode, i4Dot1xPaePortNumber,
                                 i4TestValDot1xPaePortInitialize)
     UINT4              *pu4ErrorCode;
     INT4                i4Dot1xPaePortNumber;
     INT4                i4TestValDot1xPaePortInitialize;

#endif /*  */
{
    if (PnacSnmpLowValidatePortNumber (i4Dot1xPaePortNumber) != PNAC_SUCCESS)

    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValDot1xPaePortInitialize != PNAC_MIB_TRUE) &&
        (i4TestValDot1xPaePortInitialize != PNAC_MIB_FALSE))

    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Use only '1' or"
                  "  '2' for setting 'Initialize'\n");
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortInitialize = %d\n", i4TestValDot1xPaePortInitialize); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Dot1xPaePortReauthenticate
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                testValDot1xPaePortReauthenticate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1xPaePortReauthenticate ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1xPaePortReauthenticate (UINT4 *pu4ErrorCode,
                                     INT4 i4Dot1xPaePortNumber,
                                     INT4 i4TestValDot1xPaePortReauthenticate)
#else /*  */
INT1
nmhTestv2Dot1xPaePortReauthenticate (pu4ErrorCode, i4Dot1xPaePortNumber,
                                     i4TestValDot1xPaePortReauthenticate)
     UINT4              *pu4ErrorCode;
     INT4                i4Dot1xPaePortNumber;
     INT4                i4TestValDot1xPaePortReauthenticate;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacSnmpLowValidatePortNumber (i4Dot1xPaePortNumber) != PNAC_SUCCESS)

    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    pPortInfo = &(PNAC_PORT_INFO (i4Dot1xPaePortNumber));

    if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo))

    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Cannot Reauthenticate"
                  " on Authenticator-incapable port\n");
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1xPaePortReauthenticate != (INT4) PNAC_MIB_TRUE) &&
        (i4TestValDot1xPaePortReauthenticate != (INT4) PNAC_MIB_FALSE))

    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Use only '1' or"
                  " '2' for setting 'Reauthenticate'\n");
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /*If Reauthenticate is already set to false and again if 
     * Reauthenticate is set to FALSE don't do any processing*/
    if ((i4TestValDot1xPaePortReauthenticate == PNAC_MIB_FALSE) &&
        (pPortInfo->pPortAuthSessionNode->authFsmInfo.bReAuthenticate
         == PNAC_FALSE))
    {
        return SNMP_SUCCESS;
    }

    if (PNAC_PORT_INFO (i4Dot1xPaePortNumber).u1PortAuthMode ==
        PNAC_PORT_AUTHMODE_PORTBASED)
    {
        if ((pPortInfo->pPortAuthSessionNode->authFsmInfo.u2PortMode !=
             (INT4) (PNAC_PORTCNTRL_AUTO)))
        {
            *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
            PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                      "SNMPSTD:Reauthenticate  can be done only if Port Mode is Auto \n");
            return SNMP_FAILURE;
        }

        if (pPortInfo->pPortAuthSessionNode->authFsmInfo.
            u2AuthControlPortStatus != (INT4) (PNAC_PORTSTATUS_AUTHORIZED))
        {
            *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
            PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                      "SNMPSTD:Reauthenticate can be done  only if Port Status is Authorized \n");
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* In Mac based mode, check atleast one Mac is authenticated */
        if (pPortInfo->u2SuppCount == PNAC_INIT_VAL)
        {
            *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
            PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                      "SNMPSTD:Reauthenticate can be done only if atleast one Supplicant is Authorized \n");
            return SNMP_FAILURE;
        }

    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortReauthenticate = %d\n", i4TestValDot1xPaePortReauthenticate); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1xPaePortTable
 Input       :  The Indices
                Dot1xPaePortNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1xPaePortTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1xAuthConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1xAuthConfigTable
 Input       :  The Indices
                Dot1xPaePortNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceDot1xAuthConfigTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceDot1xAuthConfigTable (INT4 i4Dot1xPaePortNumber)
#else /*  */
INT1
nmhValidateIndexInstanceDot1xAuthConfigTable (i4Dot1xPaePortNumber)
     INT4                i4Dot1xPaePortNumber;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PNAC_IS_SHUTDOWN ())

    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1xAuthConfigTable
 Input       :  The Indices
                Dot1xPaePortNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstDot1xAuthConfigTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexDot1xAuthConfigTable (INT4 *pi4Dot1xPaePortNumber)
#else /*  */
INT1
nmhGetFirstIndexDot1xAuthConfigTable (pi4Dot1xPaePortNumber)
     INT4               *pi4Dot1xPaePortNumber;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PNAC_IS_SHUTDOWN ())

    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetFirstValidPortNumber (pi4Dot1xPaePortNumber) ==
        PNAC_SUCCESS)

    {
        if (PnacGetPortEntry ((UINT2) *pi4Dot1xPaePortNumber, &pPortInfo) !=
            PNAC_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }

    else

    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", *pi4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1xAuthConfigTable
 Input       :  The Indices
                Dot1xPaePortNumber
                nextDot1xPaePortNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextDot1xAuthConfigTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexDot1xAuthConfigTable (INT4 i4Dot1xPaePortNumber,
                                     INT4 *pi4NextDot1xPaePortNumber)
#else /*  */
INT1
nmhGetNextIndexDot1xAuthConfigTable (i4Dot1xPaePortNumber,
                                     pi4NextDot1xPaePortNumber)
     INT4                i4Dot1xPaePortNumber;
     INT4               *pi4NextDot1xPaePortNumber;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (i4Dot1xPaePortNumber < 0)

    {
        return SNMP_FAILURE;
    }
    if (PNAC_IS_SHUTDOWN ())

    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetNextValidPortNumber
        (i4Dot1xPaePortNumber, pi4NextDot1xPaePortNumber) == PNAC_SUCCESS)

    {
        if (PnacGetPortEntry ((UINT2) *pi4NextDot1xPaePortNumber, &pPortInfo) !=
            PNAC_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }

    }

    else

    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", *pi4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1xAuthPaeState
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthPaeState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthPaeState ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthPaeState (INT4 i4Dot1xPaePortNumber,
                         INT4 *pi4RetValDot1xAuthPaeState)
#else /*  */
INT1
nmhGetDot1xAuthPaeState (i4Dot1xPaePortNumber, pi4RetValDot1xAuthPaeState)
     INT4                i4Dot1xPaePortNumber;
     INT4               *pi4RetValDot1xAuthPaeState;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    if ((PNAC_PORT_INFO (i4Dot1xPaePortNumber).u1PortAuthMode !=
         PNAC_PORT_AUTHMODE_PORTBASED) &&
        (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl == PNAC_PORTCNTRL_AUTO))
    {

        /* This port is configured as Mac-Based-Auto, in this case this object is
         * invalid. When this object is read it will show the state as
         * Initialize */
        *pi4RetValDot1xAuthPaeState = PNAC_ASM_STATE_INITIALIZE;
        return SNMP_SUCCESS;
    }

    else

    {
        *pi4RetValDot1xAuthPaeState =
            (INT4) pPortInfo->pPortAuthSessionNode->authFsmInfo.u2AuthPaeState;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthBackendAuthState
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthBackendAuthState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthBackendAuthState ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthBackendAuthState (INT4 i4Dot1xPaePortNumber,
                                 INT4 *pi4RetValDot1xAuthBackendAuthState)
#else /*  */
INT1
nmhGetDot1xAuthBackendAuthState (i4Dot1xPaePortNumber,
                                 pi4RetValDot1xAuthBackendAuthState)
     INT4                i4Dot1xPaePortNumber;
     INT4               *pi4RetValDot1xAuthBackendAuthState;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    if ((PNAC_PORT_INFO (i4Dot1xPaePortNumber).u1PortAuthMode !=
         PNAC_PORT_AUTHMODE_PORTBASED) &&
        (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl == PNAC_PORTCNTRL_AUTO))
    {

        /* This port is configured as Mac-Based-Auto, in this case this object is
         * invalid. When this object is read it will show the the state as
         * Initialize */
        *pi4RetValDot1xAuthBackendAuthState = PNAC_BSM_STATE_INITIALIZE;
        return SNMP_SUCCESS;
    }

    else

    {
        *pi4RetValDot1xAuthBackendAuthState =
            (INT4) pPortInfo->pPortAuthSessionNode->authFsmInfo.
            u2AuthBackendAuthState;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthAdminControlledDirections
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthAdminControlledDirections
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthAdminControlledDirections ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthAdminControlledDirections (INT4 i4Dot1xPaePortNumber,
                                          INT4
                                          *pi4RetValDot1xAuthAdminControlledDirections)
#else /*  */
INT1
nmhGetDot1xAuthAdminControlledDirections (i4Dot1xPaePortNumber,
                                          pi4RetValDot1xAuthAdminControlledDirections)
     INT4                i4Dot1xPaePortNumber;
     INT4               *pi4RetValDot1xAuthAdminControlledDirections;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1xAuthAdminControlledDirections =
        (INT4) PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AdminControlDir;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthOperControlledDirections
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthOperControlledDirections
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthOperControlledDirections ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthOperControlledDirections (INT4 i4Dot1xPaePortNumber,
                                         INT4
                                         *pi4RetValDot1xAuthOperControlledDirections)
#else /*  */
INT1
nmhGetDot1xAuthOperControlledDirections (i4Dot1xPaePortNumber,
                                         pi4RetValDot1xAuthOperControlledDirections)
     INT4                i4Dot1xPaePortNumber;
     INT4               *pi4RetValDot1xAuthOperControlledDirections;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        *pi4RetValDot1xAuthOperControlledDirections =
            (INT4) PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2OperControlDir;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthAuthControlledPortStatus
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthAuthControlledPortStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthAuthControlledPortStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthAuthControlledPortStatus (INT4 i4Dot1xPaePortNumber,
                                         INT4
                                         *pi4RetValDot1xAuthAuthControlledPortStatus)
#else /*  */
INT1
nmhGetDot1xAuthAuthControlledPortStatus (i4Dot1xPaePortNumber,
                                         pi4RetValDot1xAuthAuthControlledPortStatus)
     INT4                i4Dot1xPaePortNumber;
     INT4               *pi4RetValDot1xAuthAuthControlledPortStatus;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    if ((PNAC_PORT_INFO (i4Dot1xPaePortNumber).u1PortAuthMode !=
         PNAC_PORT_AUTHMODE_PORTBASED) &&
        (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl == PNAC_PORTCNTRL_AUTO)) 
    {

        /* This port is configured as Mac-Based-Auto, in this case this object is
         * invalid. When this object is read it will show the state as
         * Unauthorized */
        *pi4RetValDot1xAuthAuthControlledPortStatus =
            PNAC_PORTSTATUS_UNAUTHORIZED;
        return SNMP_SUCCESS;
    }

    else
    {
        if (PNAC_PORT_INFO (i4Dot1xPaePortNumber).u1PortPaeStatus == PNAC_DISABLED)
        {
            /* When PNAC is disabled on a port, when this object is read it will
               show the state as Authorized */

            *pi4RetValDot1xAuthAuthControlledPortStatus = PNAC_PORTSTATUS_AUTHORIZED;
        }
        else
        {
            *pi4RetValDot1xAuthAuthControlledPortStatus =
                (INT4) pPortInfo->pPortAuthSessionNode->authFsmInfo.
                u2AuthControlPortStatus;
        }
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthAuthControlledPortControl
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthAuthControlledPortControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthAuthControlledPortControl ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthAuthControlledPortControl (INT4 i4Dot1xPaePortNumber,
                                          INT4
                                          *pi4RetValDot1xAuthAuthControlledPortControl)
#else /*  */
INT1
nmhGetDot1xAuthAuthControlledPortControl (i4Dot1xPaePortNumber,
                                          pi4RetValDot1xAuthAuthControlledPortControl)
     INT4                i4Dot1xPaePortNumber;
     INT4               *pi4RetValDot1xAuthAuthControlledPortControl;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        *pi4RetValDot1xAuthAuthControlledPortControl =
            (INT4) PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthQuietPeriod
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthQuietPeriod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthQuietPeriod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthQuietPeriod (INT4 i4Dot1xPaePortNumber,
                            UINT4 *pu4RetValDot1xAuthQuietPeriod)
#else /*  */
INT1
nmhGetDot1xAuthQuietPeriod (i4Dot1xPaePortNumber, pu4RetValDot1xAuthQuietPeriod)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthQuietPeriod;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        *pu4RetValDot1xAuthQuietPeriod =
            (UINT4) PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4QuietPeriod;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthTxPeriod
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthTxPeriod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthTxPeriod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthTxPeriod (INT4 i4Dot1xPaePortNumber,
                         UINT4 *pu4RetValDot1xAuthTxPeriod)
#else /*  */
INT1
nmhGetDot1xAuthTxPeriod (i4Dot1xPaePortNumber, pu4RetValDot1xAuthTxPeriod)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthTxPeriod;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        *pu4RetValDot1xAuthTxPeriod =
            PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4TxPeriod;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthSuppTimeout
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthSuppTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthSuppTimeout ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthSuppTimeout (INT4 i4Dot1xPaePortNumber,
                            UINT4 *pu4RetValDot1xAuthSuppTimeout)
#else /*  */
INT1
nmhGetDot1xAuthSuppTimeout (i4Dot1xPaePortNumber, pu4RetValDot1xAuthSuppTimeout)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthSuppTimeout;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        *pu4RetValDot1xAuthSuppTimeout =
            PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4SuppTimeout;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************

 Function    :  nmhGetDot1xAuthServerTimeout
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthServerTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthServerTimeout ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthServerTimeout (INT4 i4Dot1xPaePortNumber,
                              UINT4 *pu4RetValDot1xAuthServerTimeout)
#else /*  */
INT1
nmhGetDot1xAuthServerTimeout (i4Dot1xPaePortNumber,
                              pu4RetValDot1xAuthServerTimeout)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthServerTimeout;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        *pu4RetValDot1xAuthServerTimeout =
            PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4ServerTimeout;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthMaxReq
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthMaxReq
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthMaxReq ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthMaxReq (INT4 i4Dot1xPaePortNumber,
                       UINT4 *pu4RetValDot1xAuthMaxReq)
#else /*  */
INT1
nmhGetDot1xAuthMaxReq (i4Dot1xPaePortNumber, pu4RetValDot1xAuthMaxReq)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthMaxReq;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        *pu4RetValDot1xAuthMaxReq = PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4MaxReq;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthReAuthPeriod
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthReAuthPeriod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthReAuthPeriod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthReAuthPeriod (INT4 i4Dot1xPaePortNumber,
                             UINT4 *pu4RetValDot1xAuthReAuthPeriod)
#else /*  */
INT1
nmhGetDot1xAuthReAuthPeriod (i4Dot1xPaePortNumber,
                             pu4RetValDot1xAuthReAuthPeriod)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthReAuthPeriod;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        *pu4RetValDot1xAuthReAuthPeriod =
            PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4ReAuthPeriod;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthReAuthEnabled
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthReAuthEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthReAuthEnabled ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthReAuthEnabled (INT4 i4Dot1xPaePortNumber,
                              INT4 *pi4RetValDot1xAuthReAuthEnabled)
#else /*  */
INT1
nmhGetDot1xAuthReAuthEnabled (i4Dot1xPaePortNumber,
                              pi4RetValDot1xAuthReAuthEnabled)
     INT4                i4Dot1xPaePortNumber;
     INT4               *pi4RetValDot1xAuthReAuthEnabled;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).bReAuthEnabled == PNAC_TRUE)

        {
            *pi4RetValDot1xAuthReAuthEnabled = (INT4) PNAC_MIB_TRUE;
        }

        else if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).bReAuthEnabled ==
                 PNAC_FALSE)

        {
            *pi4RetValDot1xAuthReAuthEnabled = (INT4) PNAC_MIB_FALSE;
        }
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthKeyTxEnabled
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthKeyTxEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthKeyTxEnabled ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthKeyTxEnabled (INT4 i4Dot1xPaePortNumber,
                             INT4 *pi4RetValDot1xAuthKeyTxEnabled)
#else /*  */
INT1
nmhGetDot1xAuthKeyTxEnabled (i4Dot1xPaePortNumber,
                             pi4RetValDot1xAuthKeyTxEnabled)
     INT4                i4Dot1xPaePortNumber;
     INT4               *pi4RetValDot1xAuthKeyTxEnabled;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).bKeyTxEnabled == PNAC_TRUE)

        {
            *pi4RetValDot1xAuthKeyTxEnabled = PNAC_MIB_TRUE;
        }

        else if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).bKeyTxEnabled == PNAC_FALSE)

        {
            *pi4RetValDot1xAuthKeyTxEnabled = (INT4) PNAC_MIB_FALSE;
        }
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1xAuthAdminControlledDirections
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                setValDot1xAuthAdminControlledDirections
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1xAuthAdminControlledDirections ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetDot1xAuthAdminControlledDirections (INT4 i4Dot1xPaePortNumber,
                                          INT4
                                          i4SetValDot1xAuthAdminControlledDirections)
#else /*  */
INT1
nmhSetDot1xAuthAdminControlledDirections (i4Dot1xPaePortNumber,
                                          i4SetValDot1xAuthAdminControlledDirections)
     INT4                i4Dot1xPaePortNumber;
     INT4                i4SetValDot1xAuthAdminControlledDirections;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    if (PNAC_IS_PORT_MODE_MACBASED (pPortInfo) &&
        (i4SetValDot1xAuthAdminControlledDirections == PNAC_CNTRLD_DIR_IN))
    {
        return SNMP_FAILURE;
    }

    if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AdminControlDir ==
        (UINT2) i4SetValDot1xAuthAdminControlledDirections)

    {
        PNAC_TRC (PNAC_MGMT_TRC, " SNMPPROP: Value Configured already \n");
        return SNMP_SUCCESS;
    }
    PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AdminControlDir =
        (UINT2) i4SetValDot1xAuthAdminControlledDirections;

    if (PNAC_IS_ENABLED ())

    {

        /* intimate to CDSM */
        if (PnacPaeCdStateMachine (PNAC_CDSM_EV_ADMINCHANGED,
                                   (UINT2) i4Dot1xPaePortNumber) !=
            PNAC_SUCCESS)

        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                      " SNMPSTD: CDSM returned failure\n");
            return SNMP_FAILURE;
        }

    }

    PNAC_TRC (PNAC_MGMT_TRC, " SNMPSTD: Port Control Direction is Set \n");
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY, "Dot1xAuthAdminControlledDirections = %d\n", i4SetValDot1xAuthAdminControlledDirections); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetDot1xAuthAuthControlledPortControl
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                setValDot1xAuthAuthControlledPortControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1xAuthAuthControlledPortControl ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetDot1xAuthAuthControlledPortControl (INT4 i4Dot1xPaePortNumber,
                                          INT4
                                          i4SetValDot1xAuthAuthControlledPortControl)
#else /*  */
INT1
nmhSetDot1xAuthAuthControlledPortControl (i4Dot1xPaePortNumber,
                                          i4SetValDot1xAuthAuthControlledPortControl)
     INT4                i4Dot1xPaePortNumber;
     INT4                i4SetValDot1xAuthAuthControlledPortControl;

#endif /*  */
{
    INT1                i1Result = SNMP_SUCCESS;

    i1Result = PnacLowSetPortAuthControl ((UINT2) i4Dot1xPaePortNumber,
                                          (UINT2)
                                          i4SetValDot1xAuthAuthControlledPortControl);

    return i1Result;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY, "Dot1xAuthAuthControlledPortControl = %d\n", i4SetValDot1xAuthAuthControlledPortControl); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetDot1xAuthQuietPeriod
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                setValDot1xAuthQuietPeriod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1xAuthQuietPeriod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetDot1xAuthQuietPeriod (INT4 i4Dot1xPaePortNumber,
                            UINT4 u4SetValDot1xAuthQuietPeriod)
#else /*  */
INT1
nmhSetDot1xAuthQuietPeriod (i4Dot1xPaePortNumber, u4SetValDot1xAuthQuietPeriod)
     INT4                i4Dot1xPaePortNumber;
     UINT4               u4SetValDot1xAuthQuietPeriod;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4QuietPeriod ==
            u4SetValDot1xAuthQuietPeriod)

        {
            return SNMP_SUCCESS;
        }
        PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4QuietPeriod =
            u4SetValDot1xAuthQuietPeriod;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY,"Dot1xAuthQuietPeriod = %u\n", u4SetValDot1xAuthQuietPeriod); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetDot1xAuthTxPeriod
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                setValDot1xAuthTxPeriod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1xAuthTxPeriod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetDot1xAuthTxPeriod (INT4 i4Dot1xPaePortNumber,
                         UINT4 u4SetValDot1xAuthTxPeriod)
#else /*  */
INT1
nmhSetDot1xAuthTxPeriod (i4Dot1xPaePortNumber, u4SetValDot1xAuthTxPeriod)
     INT4                i4Dot1xPaePortNumber;
     UINT4               u4SetValDot1xAuthTxPeriod;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4TxPeriod ==
            u4SetValDot1xAuthTxPeriod)

        {
            return SNMP_SUCCESS;
        }
        PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4TxPeriod =
            u4SetValDot1xAuthTxPeriod;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY,"Dot1xAuthTxPeriod = %u\n", u4SetValDot1xAuthTxPeriod); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetDot1xAuthSuppTimeout
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                setValDot1xAuthSuppTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1xAuthSuppTimeout ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetDot1xAuthSuppTimeout (INT4 i4Dot1xPaePortNumber,
                            UINT4 u4SetValDot1xAuthSuppTimeout)
#else /*  */
INT1
nmhSetDot1xAuthSuppTimeout (i4Dot1xPaePortNumber, u4SetValDot1xAuthSuppTimeout)
     INT4                i4Dot1xPaePortNumber;
     UINT4               u4SetValDot1xAuthSuppTimeout;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4SuppTimeout ==
            u4SetValDot1xAuthSuppTimeout)

        {
            return SNMP_SUCCESS;
        }
        PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4SuppTimeout =
            u4SetValDot1xAuthSuppTimeout;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY,"Dot1xAuthSuppTimeout = %u\n", u4SetValDot1xAuthSuppTimeout); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetDot1xAuthServerTimeout
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                setValDot1xAuthServerTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1xAuthServerTimeout ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetDot1xAuthServerTimeout (INT4 i4Dot1xPaePortNumber,
                              UINT4 u4SetValDot1xAuthServerTimeout)
#else /*  */
INT1
nmhSetDot1xAuthServerTimeout (i4Dot1xPaePortNumber,
                              u4SetValDot1xAuthServerTimeout)
     INT4                i4Dot1xPaePortNumber;
     UINT4               u4SetValDot1xAuthServerTimeout;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4ServerTimeout ==
            u4SetValDot1xAuthServerTimeout)

        {
            return SNMP_SUCCESS;
        }
        PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4ServerTimeout =
            u4SetValDot1xAuthServerTimeout;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY,"Dot1xAuthServerTimeout = %u\n", u4SetValDot1xAuthServerTimeout); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetDot1xAuthMaxReq
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                setValDot1xAuthMaxReq
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1xAuthMaxReq ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetDot1xAuthMaxReq (INT4 i4Dot1xPaePortNumber, UINT4 u4SetValDot1xAuthMaxReq)
#else /*  */
INT1
nmhSetDot1xAuthMaxReq (i4Dot1xPaePortNumber, u4SetValDot1xAuthMaxReq)
     INT4                i4Dot1xPaePortNumber;
     UINT4               u4SetValDot1xAuthMaxReq;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4MaxReq ==
            u4SetValDot1xAuthMaxReq)

        {
            return SNMP_SUCCESS;
        }
        PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4MaxReq = u4SetValDot1xAuthMaxReq;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY,"Dot1xAuthMaxReq = %u\n", u4SetValDot1xAuthMaxReq); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetDot1xAuthReAuthPeriod
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                setValDot1xAuthReAuthPeriod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1xAuthReAuthPeriod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetDot1xAuthReAuthPeriod (INT4 i4Dot1xPaePortNumber,
                             UINT4 u4SetValDot1xAuthReAuthPeriod)
#else /*  */
INT1
nmhSetDot1xAuthReAuthPeriod (i4Dot1xPaePortNumber,
                             u4SetValDot1xAuthReAuthPeriod)
     INT4                i4Dot1xPaePortNumber;
     UINT4               u4SetValDot1xAuthReAuthPeriod;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    UINT4               u4PresReAuthPeriod;
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tPnacAuthSessionNode *pNextAuthSessNode = NULL;
    UINT4               u4Index;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        u4PresReAuthPeriod = PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4ReAuthPeriod;
        if (u4PresReAuthPeriod == u4SetValDot1xAuthReAuthPeriod)

        {
            return SNMP_SUCCESS;
        }
        PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4ReAuthPeriod =
            u4SetValDot1xAuthReAuthPeriod;

        if (!PNAC_IS_ENABLED () || (pPortInfo->u1PortPaeStatus == PNAC_DISABLED) ||
            (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl !=
             PNAC_PORTCNTRL_AUTO))
        {
            return SNMP_SUCCESS;
        }

       /*************************************************************/
        /* Check if reauthentication is enabled and reauthwhen timer */
        /* is running - If so stop and start reauthwhen timer        */
       /*************************************************************/
        if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).bReAuthEnabled == PNAC_TRUE)

        {
            if ((PNAC_PORT_INFO (i4Dot1xPaePortNumber)).u1PortAuthMode ==
                (UINT1) PNAC_PORT_AUTHMODE_MACBASED)
            {
                /*****************************************************************/
                /* In case of Mac-Based AuthMode all the current sessions on this */
                /* port will be using the same re-auth period that is currently  */
                /* configured. Restart all the session timers belongs to this port */
                /*****************************************************************/

                PNAC_HASH_SCAN_TBL (PNAC_AUTH_SESSION_TABLE, u4Index)
                {
                /**************************************************************/
                    /* Get all the session nodes belonging to this port and       */
                    /* call the corresponding function to restart the reauth timer */
                /**************************************************************/
                    pAuthSessNode = (tPnacAuthSessionNode *)
                        PNAC_HASH_GET_FIRST_BUCKET_NODE
                        (PNAC_AUTH_SESSION_TABLE, u4Index);
                    while (pAuthSessNode != NULL)
                    {
                        pNextAuthSessNode = (tPnacAuthSessionNode *)
                            PNAC_HASH_GET_NEXT_BUCKET_NODE
                            (PNAC_AUTH_SESSION_TABLE, u4Index,
                             &pAuthSessNode->nextAuthSessionNode);
                        if (pAuthSessNode->u2PortNo ==
                            (UINT2) i4Dot1xPaePortNumber)
                        {
                            if (pAuthSessNode->pReAuthWhenTimer != NULL)
                            {
                                if (PnacRestartReauthTimer (u4PresReAuthPeriod,
                                                            u4SetValDot1xAuthReAuthPeriod,
                                                            pAuthSessNode) !=
                                    PNAC_SUCCESS)
                                {
                                    PNAC_TRC (PNAC_MGMT_TRC |
                                              PNAC_ALL_FAILURE_TRC,
                                              " SNMPSTD: Start ReAuthWhen timer restart fail\n");
                                }
                            }
                        }
                        pAuthSessNode = pNextAuthSessNode;
                    }
                }
            }
            else
            {
                if (pPortInfo->pPortAuthSessionNode->pReAuthWhenTimer != NULL)
                {

                    if (PnacRestartReauthTimer (u4PresReAuthPeriod,
                                                u4SetValDot1xAuthReAuthPeriod,
                                                pPortInfo->
                                                pPortAuthSessionNode) !=
                        PNAC_SUCCESS)
                    {
                        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                                  " SNMPSTD: Start ReAuthWhen timer restart fail\n");
                        return SNMP_FAILURE;
                    }
                }
            }
        }

        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY,"Dot1xAuthReAuthPeriod = %u\n", u4SetValDot1xAuthReAuthPeriod); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetDot1xAuthReAuthEnabled
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                setValDot1xAuthReAuthEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1xAuthReAuthEnabled ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetDot1xAuthReAuthEnabled (INT4 i4Dot1xPaePortNumber,
                              INT4 i4SetValDot1xAuthReAuthEnabled)
#else /*  */
INT1
nmhSetDot1xAuthReAuthEnabled (i4Dot1xPaePortNumber,
                              i4SetValDot1xAuthReAuthEnabled)
     INT4                i4Dot1xPaePortNumber;
     INT4                i4SetValDot1xAuthReAuthEnabled;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tPnacAuthSessionNode *pNextAuthSessNode = NULL;
    UINT4               u4Index;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        if (i4SetValDot1xAuthReAuthEnabled == (INT4) PNAC_MIB_TRUE)

        {
            PNAC_TRC_ARG1 (PNAC_MGMT_TRC,
                           " SNMPSTD: Reauthentication Enabled"
                           " for the port: %d \n", i4Dot1xPaePortNumber);
        }

        else if (i4SetValDot1xAuthReAuthEnabled == (INT4) PNAC_MIB_FALSE)

        {
            PNAC_TRC_ARG1 (PNAC_MGMT_TRC,
                           " SNMPSTD: Reauthentication Disabled"
                           " for the port: %d \n", i4Dot1xPaePortNumber);
        }
        if (((PNAC_AUTH_CONFIG_ENTRY (pPortInfo).bReAuthEnabled == PNAC_TRUE)
             &&
             (i4SetValDot1xAuthReAuthEnabled == PNAC_MIB_TRUE)) ||
            ((PNAC_AUTH_CONFIG_ENTRY (pPortInfo).bReAuthEnabled ==
              PNAC_FALSE) && (i4SetValDot1xAuthReAuthEnabled ==
                              (INT4) PNAC_MIB_FALSE)))

        {
            return SNMP_SUCCESS;
        }
        if (i4SetValDot1xAuthReAuthEnabled == (INT4) PNAC_MIB_TRUE)

        {
            PNAC_AUTH_CONFIG_ENTRY (pPortInfo).bReAuthEnabled = PNAC_TRUE;
        }

        else if (i4SetValDot1xAuthReAuthEnabled == (INT4) PNAC_MIB_FALSE)

        {
            PNAC_AUTH_CONFIG_ENTRY (pPortInfo).bReAuthEnabled = PNAC_FALSE;
        }

        if (!PNAC_IS_ENABLED () || (pPortInfo->u1PortPaeStatus == PNAC_DISABLED) ||
            (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl !=
             PNAC_PORTCNTRL_AUTO))
        {
            return SNMP_SUCCESS;
        }

        if ((PNAC_PORT_INFO (i4Dot1xPaePortNumber)).u1PortAuthMode ==
            (UINT1) PNAC_PORT_AUTHMODE_MACBASED)

        {
            /*****************************************************************/
            /* In case of Mac-Based AuthMode all the current sessions on this */
            /* port needs and start or stop the timers based on the re-auth  */
            /* is enabled or disabled.                                       */
            /*****************************************************************/

            PNAC_HASH_SCAN_TBL (PNAC_AUTH_SESSION_TABLE, u4Index)
            {
                /**************************************************************/
                /* Get all the session nodes belonging to this port.          */
                /**************************************************************/
                pAuthSessNode = (tPnacAuthSessionNode *)
                    PNAC_HASH_GET_FIRST_BUCKET_NODE (PNAC_AUTH_SESSION_TABLE,
                                                     u4Index);
                while (pAuthSessNode != NULL)
                {
                    pNextAuthSessNode = (tPnacAuthSessionNode *)
                        PNAC_HASH_GET_NEXT_BUCKET_NODE
                        (PNAC_AUTH_SESSION_TABLE, u4Index,
                         &pAuthSessNode->nextAuthSessionNode);
                    if (pAuthSessNode->u2PortNo == (UINT2) i4Dot1xPaePortNumber)
                    {
                        if (i4SetValDot1xAuthReAuthEnabled ==
                            (INT4) PNAC_MIB_TRUE)
                        {
                            if (pAuthSessNode->pReAuthWhenTimer == NULL)
                            {
                                /* Start the ReAuthentication Timer */
                                if (PnacTmrStartTmr ((VOID *)
                                                     pAuthSessNode,
                                                     PNAC_REAUTHWHEN_TIMER,
                                                     PNAC_AUTH_CONFIG_ENTRY
                                                     (pPortInfo).
                                                     u4ReAuthPeriod) !=
                                    PNAC_SUCCESS)
                                {
                                    PNAC_TRC (PNAC_MGMT_TRC |
                                              PNAC_ALL_FAILURE_TRC,
                                              " SNMPSTD: Start ReAuthWhen timer returned failure \n");
                                }
                            }
                        }
                        else if (i4SetValDot1xAuthReAuthEnabled ==
                                 (INT4) PNAC_MIB_FALSE)
                        {
                            if (pAuthSessNode->pReAuthWhenTimer != NULL)
                            {
                                /* Stop the ReAuthentication Timer */
                                if (PnacTmrStopTmr ((VOID *)
                                                    pAuthSessNode,
                                                    PNAC_REAUTHWHEN_TIMER) !=
                                    PNAC_SUCCESS)
                                {
                                    PNAC_TRC (PNAC_MGMT_TRC |
                                              PNAC_ALL_FAILURE_TRC,
                                              " SNMPSTD: Stop ReAuthWhen timer returned failure \n");
                                }
                            }
                        }
                    }
                    pAuthSessNode = pNextAuthSessNode;
                }
            }
        }
        else
        {
            if (i4SetValDot1xAuthReAuthEnabled == (INT4) PNAC_MIB_TRUE)
            {
                if (pPortInfo->pPortAuthSessionNode == NULL)
                {
                    return SNMP_FAILURE;
                }

                pAuthSessNode = pPortInfo->pPortAuthSessionNode;

                if ((pAuthSessNode->pReAuthWhenTimer == NULL) &&
                    (pAuthSessNode->authFsmInfo.u2AuthControlPortStatus ==
                     PNAC_PORTSTATUS_AUTHORIZED))
                {
                    /*Start the ReAuthentication Timer */
                    if (PnacTmrStartTmr ((VOID *)
                                         pPortInfo->pPortAuthSessionNode,
                                         PNAC_REAUTHWHEN_TIMER,
                                         PNAC_AUTH_CONFIG_ENTRY (pPortInfo).
                                         u4ReAuthPeriod) != PNAC_SUCCESS)
                    {
                        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                                  " SNMPSTD: Start ReAuthWhen timer returned failure \n");
                        return SNMP_FAILURE;
                    }
                }
            }
            else if (i4SetValDot1xAuthReAuthEnabled == (INT4) PNAC_MIB_FALSE)
            {
                if (pPortInfo->pPortAuthSessionNode == NULL)
                {
                    return SNMP_FAILURE;
                }

                if (pPortInfo->pPortAuthSessionNode->pReAuthWhenTimer != NULL)
                {
                    /*Stop the ReAuthentication Timer */
                    if (PnacTmrStopTmr ((VOID *)
                                        pPortInfo->pPortAuthSessionNode,
                                        PNAC_REAUTHWHEN_TIMER) != PNAC_SUCCESS)
                    {
                        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                                  " SNMPSTD: Stop ReAuthWhen timer returned failure \n");
                        return SNMP_FAILURE;
                    }
                }
            }
        }

        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY, "Dot1xAuthReAuthEnabled = %d\n", i4SetValDot1xAuthReAuthEnabled); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetDot1xAuthKeyTxEnabled
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                setValDot1xAuthKeyTxEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1xAuthKeyTxEnabled ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetDot1xAuthKeyTxEnabled (INT4 i4Dot1xPaePortNumber,
                             INT4 i4SetValDot1xAuthKeyTxEnabled)
#else /*  */
INT1
nmhSetDot1xAuthKeyTxEnabled (i4Dot1xPaePortNumber,
                             i4SetValDot1xAuthKeyTxEnabled)
     INT4                i4Dot1xPaePortNumber;
     INT4                i4SetValDot1xAuthKeyTxEnabled;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        if (i4SetValDot1xAuthKeyTxEnabled == (INT4) PNAC_MIB_TRUE)

        {
            PNAC_TRC_ARG1 (PNAC_MGMT_TRC,
                           " SNMPSTD: Key Transmission Enabled"
                           " for the port: %d \n", i4Dot1xPaePortNumber);
        }

        else if (i4SetValDot1xAuthKeyTxEnabled == (INT4) PNAC_MIB_FALSE)

        {
            PNAC_TRC_ARG1 (PNAC_MGMT_TRC,
                           " SNMPSTD: Key Transmission Disabled"
                           " for the port: %d \n", i4Dot1xPaePortNumber);
        }
        if (((PNAC_AUTH_CONFIG_ENTRY (pPortInfo).bKeyTxEnabled == PNAC_TRUE)
             && (i4SetValDot1xAuthKeyTxEnabled == (INT4) PNAC_MIB_TRUE)) ||
            ((PNAC_AUTH_CONFIG_ENTRY (pPortInfo).bKeyTxEnabled == PNAC_FALSE)
             && (i4SetValDot1xAuthKeyTxEnabled == (INT4) PNAC_MIB_FALSE)))

        {
            return SNMP_SUCCESS;
        }
        if (i4SetValDot1xAuthKeyTxEnabled == (INT4) PNAC_MIB_TRUE)

        {
            PNAC_AUTH_CONFIG_ENTRY (pPortInfo).bKeyTxEnabled = PNAC_TRUE;
        }

        else if (i4SetValDot1xAuthKeyTxEnabled == (INT4) PNAC_MIB_FALSE)

        {
            PNAC_AUTH_CONFIG_ENTRY (pPortInfo).bKeyTxEnabled = PNAC_FALSE;
        }
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY, "Dot1xAuthKeyTxEnabled = %d\n", i4SetValDot1xAuthKeyTxEnabled); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1xAuthAdminControlledDirections
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                testValDot1xAuthAdminControlledDirections
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1xAuthAdminControlledDirections ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1xAuthAdminControlledDirections (UINT4 *pu4ErrorCode,
                                             INT4 i4Dot1xPaePortNumber,
                                             INT4
                                             i4TestValDot1xAuthAdminControlledDirections)
#else /*  */
INT1
nmhTestv2Dot1xAuthAdminControlledDirections (pu4ErrorCode, i4Dot1xPaePortNumber,
                                             i4TestValDot1xAuthAdminControlledDirections)
     UINT4              *pu4ErrorCode;
     INT4                i4Dot1xPaePortNumber;
     INT4                i4TestValDot1xAuthAdminControlledDirections;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    else if ((i4TestValDot1xAuthAdminControlledDirections !=
              (INT4) PNAC_CNTRLD_DIR_BOTH) &&
             (i4TestValDot1xAuthAdminControlledDirections !=
              (INT4) PNAC_CNTRLD_DIR_IN))

    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: Use only PNAC_CNTRLD_DIR_BOTH or"
                  " PNAC_CNTRLD_DIR_IN for setting"
                  "'AdminControlledDirections'\n");
        return SNMP_FAILURE;
    }

    if (PNAC_IS_PORT_MODE_MACBASED (pPortInfo) &&
        (i4TestValDot1xAuthAdminControlledDirections == PNAC_CNTRLD_DIR_IN))
    {
        CLI_SET_ERR (CLI_CNTRL_DIR_IN_NOT_ALLOWED);
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY, "Dot1xAuthAdminControlledDirections = %d\n", i4TestValDot1xAuthAdminControlledDirections); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Dot1xAuthAuthControlledPortControl
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                testValDot1xAuthAuthControlledPortControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1xAuthAuthControlledPortControl ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1xAuthAuthControlledPortControl (UINT4 *pu4ErrorCode,
                                             INT4 i4Dot1xPaePortNumber,
                                             INT4
                                             i4TestValDot1xAuthAuthControlledPortControl)
#else /*  */
INT1
nmhTestv2Dot1xAuthAuthControlledPortControl (pu4ErrorCode, i4Dot1xPaePortNumber,
                                             i4TestValDot1xAuthAuthControlledPortControl)
     UINT4              *pu4ErrorCode;
     INT4                i4Dot1xPaePortNumber;
     INT4                i4TestValDot1xAuthAuthControlledPortControl;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (DPnacCheckIsLocalPort ((UINT2) i4Dot1xPaePortNumber) == PNAC_FAILURE)
    {
        CLI_SET_ERR (CLI_DPNAC_REMOTE_PORT_CONFIG_ERR);
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (PnacLowTestPortAuthControl
        (pu4ErrorCode, (UINT2) i4Dot1xPaePortNumber,
         (UINT2) i4TestValDot1xAuthAuthControlledPortControl) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY, "Dot1xAuthAuthControlledPortControl = %d\n", i4TestValDot1xAuthAuthControlledPortControl); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Dot1xAuthQuietPeriod
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                testValDot1xAuthQuietPeriod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1xAuthQuietPeriod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1xAuthQuietPeriod (UINT4 *pu4ErrorCode, INT4 i4Dot1xPaePortNumber,
                               UINT4 u4TestValDot1xAuthQuietPeriod)
#else /*  */
INT1
nmhTestv2Dot1xAuthQuietPeriod (pu4ErrorCode, i4Dot1xPaePortNumber,
                               u4TestValDot1xAuthQuietPeriod)
     UINT4              *pu4ErrorCode;
     INT4                i4Dot1xPaePortNumber;
     UINT4               u4TestValDot1xAuthQuietPeriod;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    else if (u4TestValDot1xAuthQuietPeriod > PNAC_QUIETPERIOD_MAX)

    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: QuietPeriod cannot be"
                  " less than 0 timetick or greater than 65535\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY,"Dot1xAuthQuietPeriod = %u\n", u4TestValDot1xAuthQuietPeriod); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Dot1xAuthTxPeriod
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                testValDot1xAuthTxPeriod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1xAuthTxPeriod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1xAuthTxPeriod (UINT4 *pu4ErrorCode, INT4 i4Dot1xPaePortNumber,
                            UINT4 u4TestValDot1xAuthTxPeriod)
#else /*  */
INT1
nmhTestv2Dot1xAuthTxPeriod (pu4ErrorCode, i4Dot1xPaePortNumber,
                            u4TestValDot1xAuthTxPeriod)
     UINT4              *pu4ErrorCode;
     INT4                i4Dot1xPaePortNumber;
     UINT4               u4TestValDot1xAuthTxPeriod;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    else if ((u4TestValDot1xAuthTxPeriod < (UINT4) PNAC_TXPERIOD_MIN) ||
             (u4TestValDot1xAuthTxPeriod > (UINT4) PNAC_TXPERIOD_MAX))

    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: TxPeriod cannot be"
                  " less than 1 timetick or greater than 65535\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY,"Dot1xAuthTxPeriod = %u\n", u4TestValDot1xAuthTxPeriod); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Dot1xAuthSuppTimeout
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                testValDot1xAuthSuppTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1xAuthSuppTimeout ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1xAuthSuppTimeout (UINT4 *pu4ErrorCode, INT4 i4Dot1xPaePortNumber,
                               UINT4 u4TestValDot1xAuthSuppTimeout)
#else /*  */
INT1
nmhTestv2Dot1xAuthSuppTimeout (pu4ErrorCode, i4Dot1xPaePortNumber,
                               u4TestValDot1xAuthSuppTimeout)
     UINT4              *pu4ErrorCode;
     INT4                i4Dot1xPaePortNumber;
     UINT4               u4TestValDot1xAuthSuppTimeout;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Check  for range of values - Supplicant Timeout Maximum is not
     * specified in the standard - It is implementation 
     * specific and can change in pnacmacr.h file 
     */
    else if ((u4TestValDot1xAuthSuppTimeout < (UINT4) PNAC_SUPPTIMEOUT_MIN)
             || (u4TestValDot1xAuthSuppTimeout > (UINT4) PNAC_SUPPTIMEOUT_MAX))

    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: SuppTimeout cannot be"
                  " less than 1 time tick \n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY,"Dot1xAuthSuppTimeout = %u\n", u4TestValDot1xAuthSuppTimeout); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Dot1xAuthServerTimeout
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                testValDot1xAuthServerTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1xAuthServerTimeout ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1xAuthServerTimeout (UINT4 *pu4ErrorCode, INT4 i4Dot1xPaePortNumber,
                                 UINT4 u4TestValDot1xAuthServerTimeout)
#else /*  */
INT1
nmhTestv2Dot1xAuthServerTimeout (pu4ErrorCode, i4Dot1xPaePortNumber,
                                 u4TestValDot1xAuthServerTimeout)
     UINT4              *pu4ErrorCode;
     INT4                i4Dot1xPaePortNumber;
     UINT4               u4TestValDot1xAuthServerTimeout;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Check  for range of values - Supplicant Timeout Maximum is not
     * specified in the standard - It is implementation 
     * specific and can change in pnacmacr.h file 
     */
    else if ((u4TestValDot1xAuthServerTimeout <
              (UINT4) PNAC_SERVERTIMEOUT_MIN)
             || (u4TestValDot1xAuthServerTimeout >
                 (UINT4) PNAC_SERVERTIMEOUT_MAX))

    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: ServerTimeout cannot be"
                  " less than 1 time tick \n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY,"Dot1xAuthServerTimeout = %u\n", u4TestValDot1xAuthServerTimeout); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Dot1xAuthMaxReq
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                testValDot1xAuthMaxReq
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1xAuthMaxReq ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1xAuthMaxReq (UINT4 *pu4ErrorCode, INT4 i4Dot1xPaePortNumber,
                          UINT4 u4TestValDot1xAuthMaxReq)
#else /*  */
INT1
nmhTestv2Dot1xAuthMaxReq (pu4ErrorCode, i4Dot1xPaePortNumber,
                          u4TestValDot1xAuthMaxReq)
     UINT4              *pu4ErrorCode;
     INT4                i4Dot1xPaePortNumber;
     UINT4               u4TestValDot1xAuthMaxReq;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else if ((u4TestValDot1xAuthMaxReq < (UINT4) PNAC_MAXREQ_MIN) ||
             (u4TestValDot1xAuthMaxReq > (UINT4) PNAC_MAXREQ_MAX))

    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: MaxReq for Authenticator"
                  " cannot be less than 1  \n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY,"Dot1xAuthMaxReq = %u\n", u4TestValDot1xAuthMaxReq); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Dot1xAuthReAuthPeriod
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                testValDot1xAuthReAuthPeriod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1xAuthReAuthPeriod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1xAuthReAuthPeriod (UINT4 *pu4ErrorCode, INT4 i4Dot1xPaePortNumber,
                                UINT4 u4TestValDot1xAuthReAuthPeriod)
#else /*  */
INT1
nmhTestv2Dot1xAuthReAuthPeriod (pu4ErrorCode, i4Dot1xPaePortNumber,
                                u4TestValDot1xAuthReAuthPeriod)
     UINT4              *pu4ErrorCode;
     INT4                i4Dot1xPaePortNumber;
     UINT4               u4TestValDot1xAuthReAuthPeriod;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    else if ((u4TestValDot1xAuthReAuthPeriod < (UINT4) PNAC_REAUTHPERIOD_MIN) ||
             (u4TestValDot1xAuthReAuthPeriod > (UINT4) PNAC_REAUTHPERIOD_MAX))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: ReAuthPeriod cannot be"
                  " less than 1 or greater than 65535.\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY,"Dot1xAuthReAuthPeriod = %u\n", u4TestValDot1xAuthReAuthPeriod); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Dot1xAuthReAuthEnabled
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                testValDot1xAuthReAuthEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1xAuthReAuthEnabled ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1xAuthReAuthEnabled (UINT4 *pu4ErrorCode, INT4 i4Dot1xPaePortNumber,
                                 INT4 i4TestValDot1xAuthReAuthEnabled)
#else /*  */
INT1
nmhTestv2Dot1xAuthReAuthEnabled (pu4ErrorCode, i4Dot1xPaePortNumber,
                                 i4TestValDot1xAuthReAuthEnabled)
     UINT4              *pu4ErrorCode;
     INT4                i4Dot1xPaePortNumber;
     INT4                i4TestValDot1xAuthReAuthEnabled;
#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    else if ((i4TestValDot1xAuthReAuthEnabled != PNAC_MIB_TRUE) &&
             (i4TestValDot1xAuthReAuthEnabled != PNAC_MIB_FALSE))

    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Use '1' or"
                  " '2' for setting 'ReAuthEnabled' \n");
        return SNMP_FAILURE;
    }

    /* Check if the re-auth value is same as what has already been
     * configured. If so no further processing is done.
     */

    if ((i4TestValDot1xAuthReAuthEnabled == PNAC_MIB_FALSE) &&
        (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).bReAuthEnabled == PNAC_FALSE))
    {
        return SNMP_SUCCESS;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY, "Dot1xAuthReAuthEnabled = %d\n", i4TestValDot1xAuthReAuthEnabled); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Dot1xAuthKeyTxEnabled
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                testValDot1xAuthKeyTxEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1xAuthKeyTxEnabled ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1xAuthKeyTxEnabled (UINT4 *pu4ErrorCode, INT4 i4Dot1xPaePortNumber,
                                INT4 i4TestValDot1xAuthKeyTxEnabled)
#else /*  */
INT1
nmhTestv2Dot1xAuthKeyTxEnabled (pu4ErrorCode, i4Dot1xPaePortNumber,
                                i4TestValDot1xAuthKeyTxEnabled)
     UINT4              *pu4ErrorCode;
     INT4                i4Dot1xPaePortNumber;
     INT4                i4TestValDot1xAuthKeyTxEnabled;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    else if ((i4TestValDot1xAuthKeyTxEnabled != PNAC_MIB_TRUE) &&
             (i4TestValDot1xAuthKeyTxEnabled != PNAC_MIB_FALSE))

    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Use only '1' or"
                  " '2' for setting 'AuthKeyTxEnabled' \n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY, "Dot1xAuthKeyTxEnabled = %d\n", i4TestValDot1xAuthKeyTxEnabled); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1xAuthConfigTable
 Input       :  The Indices
                Dot1xPaePortNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1xAuthConfigTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1xAuthStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1xAuthStatsTable
 Input       :  The Indices
                Dot1xPaePortNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceDot1xAuthStatsTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceDot1xAuthStatsTable (INT4 i4Dot1xPaePortNumber)
#else /*  */
INT1
nmhValidateIndexInstanceDot1xAuthStatsTable (i4Dot1xPaePortNumber)
     INT4                i4Dot1xPaePortNumber;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PNAC_IS_SHUTDOWN ())

    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1xAuthStatsTable
 Input       :  The Indices
                Dot1xPaePortNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstDot1xAuthStatsTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexDot1xAuthStatsTable (INT4 *pi4Dot1xPaePortNumber)
#else /*  */
INT1
nmhGetFirstIndexDot1xAuthStatsTable (pi4Dot1xPaePortNumber)
     INT4               *pi4Dot1xPaePortNumber;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PNAC_IS_SHUTDOWN ())

    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetFirstValidPortNumber (pi4Dot1xPaePortNumber) !=
        PNAC_SUCCESS)

    {
        if (PnacGetPortEntry ((UINT2) *pi4Dot1xPaePortNumber, &pPortInfo) !=
            PNAC_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }

    else

    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", *pi4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1xAuthStatsTable
 Input       :  The Indices
                Dot1xPaePortNumber
                nextDot1xPaePortNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextDot1xAuthStatsTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexDot1xAuthStatsTable (INT4 i4Dot1xPaePortNumber,
                                    INT4 *pi4NextDot1xPaePortNumber)
#else /*  */
INT1
nmhGetNextIndexDot1xAuthStatsTable (i4Dot1xPaePortNumber,
                                    pi4NextDot1xPaePortNumber)
     INT4                i4Dot1xPaePortNumber;
     INT4               *pi4NextDot1xPaePortNumber;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (i4Dot1xPaePortNumber < 0)

    {
        return SNMP_FAILURE;
    }
    if (PNAC_IS_SHUTDOWN ())

    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetNextValidPortNumber
        (i4Dot1xPaePortNumber, pi4NextDot1xPaePortNumber) == PNAC_SUCCESS)

    {
        if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
            PNAC_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (!PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
        {
            return SNMP_FAILURE;
        }
        else
        {
            return SNMP_SUCCESS;
        }

    }

    else

    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", *pi4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1xAuthEapolFramesRx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthEapolFramesRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthEapolFramesRx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthEapolFramesRx (INT4 i4Dot1xPaePortNumber,
                              UINT4 *pu4RetValDot1xAuthEapolFramesRx)
#else /*  */
INT1
nmhGetDot1xAuthEapolFramesRx (i4Dot1xPaePortNumber,
                              pu4RetValDot1xAuthEapolFramesRx)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthEapolFramesRx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        *pu4RetValDot1xAuthEapolFramesRx =
            PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolFramesRx;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthEapolFramesTx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthEapolFramesTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthEapolFramesTx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthEapolFramesTx (INT4 i4Dot1xPaePortNumber,
                              UINT4 *pu4RetValDot1xAuthEapolFramesTx)
#else /*  */
INT1
nmhGetDot1xAuthEapolFramesTx (i4Dot1xPaePortNumber,
                              pu4RetValDot1xAuthEapolFramesTx)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthEapolFramesTx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        *pu4RetValDot1xAuthEapolFramesTx =
            PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolFramesTx;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthEapolStartFramesRx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthEapolStartFramesRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthEapolStartFramesRx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthEapolStartFramesRx (INT4 i4Dot1xPaePortNumber,
                                   UINT4 *pu4RetValDot1xAuthEapolStartFramesRx)
#else /*  */
INT1
nmhGetDot1xAuthEapolStartFramesRx (i4Dot1xPaePortNumber,
                                   pu4RetValDot1xAuthEapolStartFramesRx)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthEapolStartFramesRx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        *pu4RetValDot1xAuthEapolStartFramesRx =
            PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolStartFramesRx;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthEapolLogoffFramesRx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthEapolLogoffFramesRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthEapolLogoffFramesRx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthEapolLogoffFramesRx (INT4 i4Dot1xPaePortNumber,
                                    UINT4
                                    *pu4RetValDot1xAuthEapolLogoffFramesRx)
#else /*  */
INT1
nmhGetDot1xAuthEapolLogoffFramesRx (i4Dot1xPaePortNumber,
                                    pu4RetValDot1xAuthEapolLogoffFramesRx)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthEapolLogoffFramesRx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        *pu4RetValDot1xAuthEapolLogoffFramesRx =
            PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolLogoffFramesRx;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthEapolRespIdFramesRx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthEapolRespIdFramesRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthEapolRespIdFramesRx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthEapolRespIdFramesRx (INT4 i4Dot1xPaePortNumber,
                                    UINT4
                                    *pu4RetValDot1xAuthEapolRespIdFramesRx)
#else /*  */
INT1
nmhGetDot1xAuthEapolRespIdFramesRx (i4Dot1xPaePortNumber,
                                    pu4RetValDot1xAuthEapolRespIdFramesRx)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthEapolRespIdFramesRx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        *pu4RetValDot1xAuthEapolRespIdFramesRx =
            PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolRespIdFramesRx;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthEapolRespFramesRx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthEapolRespFramesRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthEapolRespFramesRx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthEapolRespFramesRx (INT4 i4Dot1xPaePortNumber,
                                  UINT4 *pu4RetValDot1xAuthEapolRespFramesRx)
#else /*  */
INT1
nmhGetDot1xAuthEapolRespFramesRx (i4Dot1xPaePortNumber,
                                  pu4RetValDot1xAuthEapolRespFramesRx)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthEapolRespFramesRx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        *pu4RetValDot1xAuthEapolRespFramesRx =
            PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolRespFramesRx;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthEapolReqIdFramesTx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthEapolReqIdFramesTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthEapolReqIdFramesTx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthEapolReqIdFramesTx (INT4 i4Dot1xPaePortNumber,
                                   UINT4 *pu4RetValDot1xAuthEapolReqIdFramesTx)
#else /*  */
INT1
nmhGetDot1xAuthEapolReqIdFramesTx (i4Dot1xPaePortNumber,
                                   pu4RetValDot1xAuthEapolReqIdFramesTx)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthEapolReqIdFramesTx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        *pu4RetValDot1xAuthEapolReqIdFramesTx =
            PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolReqIdFramesTx;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthEapolReqFramesTx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthEapolReqFramesTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthEapolReqFramesTx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthEapolReqFramesTx (INT4 i4Dot1xPaePortNumber,
                                 UINT4 *pu4RetValDot1xAuthEapolReqFramesTx)
#else /*  */
INT1
nmhGetDot1xAuthEapolReqFramesTx (i4Dot1xPaePortNumber,
                                 pu4RetValDot1xAuthEapolReqFramesTx)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthEapolReqFramesTx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        *pu4RetValDot1xAuthEapolReqFramesTx =
            PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolReqFramesTx;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthInvalidEapolFramesRx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthInvalidEapolFramesRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthInvalidEapolFramesRx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthInvalidEapolFramesRx (INT4 i4Dot1xPaePortNumber,
                                     UINT4
                                     *pu4RetValDot1xAuthInvalidEapolFramesRx)
#else /*  */
INT1
nmhGetDot1xAuthInvalidEapolFramesRx (i4Dot1xPaePortNumber,
                                     pu4RetValDot1xAuthInvalidEapolFramesRx)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthInvalidEapolFramesRx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        *pu4RetValDot1xAuthInvalidEapolFramesRx =
            PNAC_AUTH_STATS_ENTRY (pPortInfo).u4InvalidEapolFramesRx;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthEapLengthErrorFramesRx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthEapLengthErrorFramesRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthEapLengthErrorFramesRx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthEapLengthErrorFramesRx (INT4 i4Dot1xPaePortNumber,
                                       UINT4
                                       *pu4RetValDot1xAuthEapLengthErrorFramesRx)
#else /*  */
INT1
nmhGetDot1xAuthEapLengthErrorFramesRx (i4Dot1xPaePortNumber,
                                       pu4RetValDot1xAuthEapLengthErrorFramesRx)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthEapLengthErrorFramesRx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        *pu4RetValDot1xAuthEapLengthErrorFramesRx =
            PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapLengthErrorFramesRx;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthLastEapolFrameVersion
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthLastEapolFrameVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthLastEapolFrameVersion ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthLastEapolFrameVersion (INT4 i4Dot1xPaePortNumber,
                                      UINT4
                                      *pu4RetValDot1xAuthLastEapolFrameVersion)
#else /*  */
INT1
nmhGetDot1xAuthLastEapolFrameVersion (i4Dot1xPaePortNumber,
                                      pu4RetValDot1xAuthLastEapolFrameVersion)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthLastEapolFrameVersion;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        *pu4RetValDot1xAuthLastEapolFrameVersion =
            PNAC_AUTH_STATS_ENTRY (pPortInfo).u4LastEapolFrameVersion;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthLastEapolFrameSource
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthLastEapolFrameSource
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthLastEapolFrameSource ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthLastEapolFrameSource (INT4 i4Dot1xPaePortNumber,
                                     tMacAddr *
                                     pRetValDot1xAuthLastEapolFrameSource)
#else /*  */
INT1
nmhGetDot1xAuthLastEapolFrameSource (i4Dot1xPaePortNumber,
                                     pRetValDot1xAuthLastEapolFrameSource)
     INT4                i4Dot1xPaePortNumber;
     tMacAddr           *pRetValDot1xAuthLastEapolFrameSource;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        PNAC_MEMCPY ((UINT1 *) *pRetValDot1xAuthLastEapolFrameSource,
                     PNAC_AUTH_STATS_ENTRY (pPortInfo).lastEapolFrameSource,
                     PNAC_MAC_ADDR_SIZE);
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* LOW LEVEL Routines for Table : Dot1xAuthDiagTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1xAuthDiagTable
 Input       :  The Indices
                Dot1xPaePortNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceDot1xAuthDiagTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceDot1xAuthDiagTable (INT4 i4Dot1xPaePortNumber)
#else /*  */
INT1
nmhValidateIndexInstanceDot1xAuthDiagTable (i4Dot1xPaePortNumber)
     INT4                i4Dot1xPaePortNumber;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PNAC_IS_SHUTDOWN ())

    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1xAuthDiagTable
 Input       :  The Indices
                Dot1xPaePortNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstDot1xAuthDiagTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexDot1xAuthDiagTable (INT4 *pi4Dot1xPaePortNumber)
#else /*  */
INT1
nmhGetFirstIndexDot1xAuthDiagTable (pi4Dot1xPaePortNumber)
     INT4               *pi4Dot1xPaePortNumber;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PNAC_IS_SHUTDOWN ())

    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetFirstValidPortNumber (pi4Dot1xPaePortNumber) !=
        PNAC_SUCCESS)

    {
        if (PnacGetPortEntry ((UINT2) *pi4Dot1xPaePortNumber, &pPortInfo) !=
            PNAC_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }

    }

    else

    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", *pi4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1xAuthDiagTable
 Input       :  The Indices
                Dot1xPaePortNumber
                nextDot1xPaePortNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextDot1xAuthDiagTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexDot1xAuthDiagTable (INT4 i4Dot1xPaePortNumber,
                                   INT4 *pi4NextDot1xPaePortNumber)
#else /*  */
INT1
nmhGetNextIndexDot1xAuthDiagTable (i4Dot1xPaePortNumber,
                                   pi4NextDot1xPaePortNumber)
     INT4                i4Dot1xPaePortNumber;
     INT4               *pi4NextDot1xPaePortNumber;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (i4Dot1xPaePortNumber < 0)

    {
        return SNMP_FAILURE;
    }
    if (PNAC_IS_SHUTDOWN ())

    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetNextValidPortNumber
        (i4Dot1xPaePortNumber, pi4NextDot1xPaePortNumber) == PNAC_SUCCESS)

    {
        if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
            PNAC_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }

    }

    else

    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", *pi4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1xAuthEntersConnecting
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthEntersConnecting
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthEntersConnecting ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthEntersConnecting (INT4 i4Dot1xPaePortNumber,
                                 UINT4 *pu4RetValDot1xAuthEntersConnecting)
#else /*  */
INT1
nmhGetDot1xAuthEntersConnecting (i4Dot1xPaePortNumber,
                                 pu4RetValDot1xAuthEntersConnecting)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthEntersConnecting;

#endif /*  */
{
    *pu4RetValDot1xAuthEntersConnecting = (UINT4) PNAC_NO_VAL;
    PNAC_UNUSED (i4Dot1xPaePortNumber);
    PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Authenticator Diagnostics"
              " Entries are not supported in this implementation\n");
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthEapLogoffsWhileConnecting
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthEapLogoffsWhileConnecting
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthEapLogoffsWhileConnecting ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthEapLogoffsWhileConnecting (INT4 i4Dot1xPaePortNumber,
                                          UINT4
                                          *pu4RetValDot1xAuthEapLogoffsWhileConnecting)
#else /*  */
INT1
nmhGetDot1xAuthEapLogoffsWhileConnecting (i4Dot1xPaePortNumber,
                                          pu4RetValDot1xAuthEapLogoffsWhileConnecting)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthEapLogoffsWhileConnecting;

#endif /*  */
{
    *pu4RetValDot1xAuthEapLogoffsWhileConnecting = (UINT4) PNAC_NO_VAL;
    PNAC_UNUSED (i4Dot1xPaePortNumber);
    PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Authenticator Diagnostics"
              " Entries are not supported in this implementation\n");
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthEntersAuthenticating
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthEntersAuthenticating
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthEntersAuthenticating ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthEntersAuthenticating (INT4 i4Dot1xPaePortNumber,
                                     UINT4
                                     *pu4RetValDot1xAuthEntersAuthenticating)
#else /*  */
INT1
nmhGetDot1xAuthEntersAuthenticating (i4Dot1xPaePortNumber,
                                     pu4RetValDot1xAuthEntersAuthenticating)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthEntersAuthenticating;

#endif /*  */
{
    *pu4RetValDot1xAuthEntersAuthenticating = (UINT4) PNAC_NO_VAL;
    PNAC_UNUSED (i4Dot1xPaePortNumber);
    PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Authenticator Diagnostics"
              " Entries are not supported in this implementation\n");
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthAuthSuccessWhileAuthenticating
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthAuthSuccessWhileAuthenticating
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthAuthSuccessWhileAuthenticating ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthAuthSuccessWhileAuthenticating (INT4 i4Dot1xPaePortNumber,
                                               UINT4
                                               *pu4RetValDot1xAuthAuthSuccessWhileAuthenticating)
#else /*  */
INT1
nmhGetDot1xAuthAuthSuccessWhileAuthenticating (i4Dot1xPaePortNumber,
                                               pu4RetValDot1xAuthAuthSuccessWhileAuthenticating)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthAuthSuccessWhileAuthenticating;

#endif /*  */
{
    *pu4RetValDot1xAuthAuthSuccessWhileAuthenticating = (UINT4) PNAC_NO_VAL;
    PNAC_UNUSED (i4Dot1xPaePortNumber);
    PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Authenticator Diagnostics"
              " Entries are not supported in this implementation\n");
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthAuthTimeoutsWhileAuthenticating
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthAuthTimeoutsWhileAuthenticating
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthAuthTimeoutsWhileAuthenticating ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthAuthTimeoutsWhileAuthenticating (INT4 i4Dot1xPaePortNumber,
                                                UINT4
                                                *pu4RetValDot1xAuthAuthTimeoutsWhileAuthenticating)
#else /*  */
INT1
nmhGetDot1xAuthAuthTimeoutsWhileAuthenticating (i4Dot1xPaePortNumber,
                                                pu4RetValDot1xAuthAuthTimeoutsWhileAuthenticating)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthAuthTimeoutsWhileAuthenticating;

#endif /*  */
{
    *pu4RetValDot1xAuthAuthTimeoutsWhileAuthenticating = (UINT4) PNAC_NO_VAL;
    PNAC_UNUSED (i4Dot1xPaePortNumber);
    PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Authenticator Diagnostics"
              " Entries are not supported in this implementation\n");
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthAuthFailWhileAuthenticating
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthAuthFailWhileAuthenticating
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthAuthFailWhileAuthenticating ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthAuthFailWhileAuthenticating (INT4 i4Dot1xPaePortNumber,
                                            UINT4
                                            *pu4RetValDot1xAuthAuthFailWhileAuthenticating)
#else /*  */
INT1
nmhGetDot1xAuthAuthFailWhileAuthenticating (i4Dot1xPaePortNumber,
                                            pu4RetValDot1xAuthAuthFailWhileAuthenticating)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthAuthFailWhileAuthenticating;

#endif /*  */
{
    *pu4RetValDot1xAuthAuthFailWhileAuthenticating = (UINT4) PNAC_NO_VAL;
    PNAC_UNUSED (i4Dot1xPaePortNumber);
    PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Authenticator Diagnostics"
              " Entries are not supported in this implementation\n");
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthAuthReauthsWhileAuthenticating
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthAuthReauthsWhileAuthenticating
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthAuthReauthsWhileAuthenticating ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthAuthReauthsWhileAuthenticating (INT4 i4Dot1xPaePortNumber,
                                               UINT4
                                               *pu4RetValDot1xAuthAuthReauthsWhileAuthenticating)
#else /*  */
INT1
nmhGetDot1xAuthAuthReauthsWhileAuthenticating (i4Dot1xPaePortNumber,
                                               pu4RetValDot1xAuthAuthReauthsWhileAuthenticating)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthAuthReauthsWhileAuthenticating;

#endif /*  */
{
    *pu4RetValDot1xAuthAuthReauthsWhileAuthenticating = (UINT4) PNAC_NO_VAL;
    PNAC_UNUSED (i4Dot1xPaePortNumber);
    PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Authenticator Diagnostics"
              " Entries are not supported in this implementation\n");
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthAuthEapStartsWhileAuthenticating
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthAuthEapStartsWhileAuthenticating
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthAuthEapStartsWhileAuthenticating ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthAuthEapStartsWhileAuthenticating (INT4 i4Dot1xPaePortNumber,
                                                 UINT4
                                                 *pu4RetValDot1xAuthAuthEapStartsWhileAuthenticating)
#else /*  */
INT1
nmhGetDot1xAuthAuthEapStartsWhileAuthenticating (i4Dot1xPaePortNumber,
                                                 pu4RetValDot1xAuthAuthEapStartsWhileAuthenticating)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthAuthEapStartsWhileAuthenticating;

#endif /*  */
{
    *pu4RetValDot1xAuthAuthEapStartsWhileAuthenticating = (UINT4) PNAC_NO_VAL;
    PNAC_UNUSED (i4Dot1xPaePortNumber);
    PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Authenticator Diagnostics"
              " Entries are not supported in this implementation\n");
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthAuthEapLogoffWhileAuthenticating
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthAuthEapLogoffWhileAuthenticating
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthAuthEapLogoffWhileAuthenticating ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthAuthEapLogoffWhileAuthenticating (INT4 i4Dot1xPaePortNumber,
                                                 UINT4
                                                 *pu4RetValDot1xAuthAuthEapLogoffWhileAuthenticating)
#else /*  */
INT1
nmhGetDot1xAuthAuthEapLogoffWhileAuthenticating (i4Dot1xPaePortNumber,
                                                 pu4RetValDot1xAuthAuthEapLogoffWhileAuthenticating)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthAuthEapLogoffWhileAuthenticating;

#endif /*  */
{
    *pu4RetValDot1xAuthAuthEapLogoffWhileAuthenticating = (UINT4) PNAC_NO_VAL;
    PNAC_UNUSED (i4Dot1xPaePortNumber);
    PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Authenticator Diagnostics"
              " Entries are not supported in this implementation\n");
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthAuthReauthsWhileAuthenticated
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthAuthReauthsWhileAuthenticated
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthAuthReauthsWhileAuthenticated ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthAuthReauthsWhileAuthenticated (INT4 i4Dot1xPaePortNumber,
                                              UINT4
                                              *pu4RetValDot1xAuthAuthReauthsWhileAuthenticated)
#else /*  */
INT1
nmhGetDot1xAuthAuthReauthsWhileAuthenticated (i4Dot1xPaePortNumber,
                                              pu4RetValDot1xAuthAuthReauthsWhileAuthenticated)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthAuthReauthsWhileAuthenticated;

#endif /*  */
{
    *pu4RetValDot1xAuthAuthReauthsWhileAuthenticated = (UINT4) PNAC_NO_VAL;
    PNAC_UNUSED (i4Dot1xPaePortNumber);
    PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Authenticator Diagnostics"
              " Entries are not supported in this implementation\n");
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthAuthEapStartsWhileAuthenticated
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthAuthEapStartsWhileAuthenticated
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthAuthEapStartsWhileAuthenticated ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthAuthEapStartsWhileAuthenticated (INT4 i4Dot1xPaePortNumber,
                                                UINT4
                                                *pu4RetValDot1xAuthAuthEapStartsWhileAuthenticated)
#else /*  */
INT1
nmhGetDot1xAuthAuthEapStartsWhileAuthenticated (i4Dot1xPaePortNumber,
                                                pu4RetValDot1xAuthAuthEapStartsWhileAuthenticated)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthAuthEapStartsWhileAuthenticated;

#endif /*  */
{
    *pu4RetValDot1xAuthAuthEapStartsWhileAuthenticated = (UINT4) PNAC_NO_VAL;
    PNAC_UNUSED (i4Dot1xPaePortNumber);
    PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Authenticator Diagnostics"
              " Entries are not supported in this implementation\n");
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthAuthEapLogoffWhileAuthenticated
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthAuthEapLogoffWhileAuthenticated
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthAuthEapLogoffWhileAuthenticated ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthAuthEapLogoffWhileAuthenticated (INT4 i4Dot1xPaePortNumber,
                                                UINT4
                                                *pu4RetValDot1xAuthAuthEapLogoffWhileAuthenticated)
#else /*  */
INT1
nmhGetDot1xAuthAuthEapLogoffWhileAuthenticated (i4Dot1xPaePortNumber,
                                                pu4RetValDot1xAuthAuthEapLogoffWhileAuthenticated)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthAuthEapLogoffWhileAuthenticated;

#endif /*  */
{
    *pu4RetValDot1xAuthAuthEapLogoffWhileAuthenticated = (UINT4) PNAC_NO_VAL;
    PNAC_UNUSED (i4Dot1xPaePortNumber);
    PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Authenticator Diagnostics"
              " Entries are not supported in this implementation\n");
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthBackendResponses
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthBackendResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthBackendResponses ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthBackendResponses (INT4 i4Dot1xPaePortNumber,
                                 UINT4 *pu4RetValDot1xAuthBackendResponses)
#else /*  */
INT1
nmhGetDot1xAuthBackendResponses (i4Dot1xPaePortNumber,
                                 pu4RetValDot1xAuthBackendResponses)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthBackendResponses;

#endif /*  */
{
    *pu4RetValDot1xAuthBackendResponses = (UINT4) PNAC_NO_VAL;
    PNAC_UNUSED (i4Dot1xPaePortNumber);
    PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Authenticator Diagnostics"
              " Entries are not supported in this implementation\n");
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthBackendAccessChallenges
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthBackendAccessChallenges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthBackendAccessChallenges ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthBackendAccessChallenges (INT4 i4Dot1xPaePortNumber,
                                        UINT4
                                        *pu4RetValDot1xAuthBackendAccessChallenges)
#else /*  */
INT1
nmhGetDot1xAuthBackendAccessChallenges (i4Dot1xPaePortNumber,
                                        pu4RetValDot1xAuthBackendAccessChallenges)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthBackendAccessChallenges;

#endif /*  */
{
    *pu4RetValDot1xAuthBackendAccessChallenges = (UINT4) PNAC_NO_VAL;
    PNAC_UNUSED (i4Dot1xPaePortNumber);
    PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Authenticator Diagnostics"
              " Entries are not supported in this implementation\n");
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthBackendOtherRequestsToSupplicant
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthBackendOtherRequestsToSupplicant
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthBackendOtherRequestsToSupplicant ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthBackendOtherRequestsToSupplicant (INT4 i4Dot1xPaePortNumber,
                                                 UINT4
                                                 *pu4RetValDot1xAuthBackendOtherRequestsToSupplicant)
#else /*  */
INT1
nmhGetDot1xAuthBackendOtherRequestsToSupplicant (i4Dot1xPaePortNumber,
                                                 pu4RetValDot1xAuthBackendOtherRequestsToSupplicant)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthBackendOtherRequestsToSupplicant;

#endif /*  */
{
    *pu4RetValDot1xAuthBackendOtherRequestsToSupplicant = (UINT4) PNAC_NO_VAL;
    PNAC_UNUSED (i4Dot1xPaePortNumber);
    PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Authenticator Diagnostics"
              " Entries are not supported in this implementation\n");
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthBackendNonNakResponsesFromSupplicant
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthBackendNonNakResponsesFromSupplicant
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthBackendNonNakResponsesFromSupplicant ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthBackendNonNakResponsesFromSupplicant (INT4 i4Dot1xPaePortNumber,
                                                     UINT4
                                                     *pu4RetValDot1xAuthBackendNonNakResponsesFromSupplicant)
#else /*  */
INT1
nmhGetDot1xAuthBackendNonNakResponsesFromSupplicant (i4Dot1xPaePortNumber,
                                                     pu4RetValDot1xAuthBackendNonNakResponsesFromSupplicant)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthBackendNonNakResponsesFromSupplicant;

#endif /*  */
{
    *pu4RetValDot1xAuthBackendNonNakResponsesFromSupplicant =
        (UINT4) PNAC_NO_VAL;
    PNAC_UNUSED (i4Dot1xPaePortNumber);
    PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Authenticator Diagnostics"
              " Entries are not supported in this implementation\n");
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthBackendAuthSuccesses
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthBackendAuthSuccesses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthBackendAuthSuccesses ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthBackendAuthSuccesses (INT4 i4Dot1xPaePortNumber,
                                     UINT4
                                     *pu4RetValDot1xAuthBackendAuthSuccesses)
#else /*  */
INT1
nmhGetDot1xAuthBackendAuthSuccesses (i4Dot1xPaePortNumber,
                                     pu4RetValDot1xAuthBackendAuthSuccesses)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthBackendAuthSuccesses;

#endif /*  */
{
    *pu4RetValDot1xAuthBackendAuthSuccesses = (UINT4) PNAC_NO_VAL;
    PNAC_UNUSED (i4Dot1xPaePortNumber);
    PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Authenticator Diagnostics"
              " Entries are not supported in this implementation\n");
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthBackendAuthFails
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthBackendAuthFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthBackendAuthFails ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthBackendAuthFails (INT4 i4Dot1xPaePortNumber,
                                 UINT4 *pu4RetValDot1xAuthBackendAuthFails)
#else /*  */
INT1
nmhGetDot1xAuthBackendAuthFails (i4Dot1xPaePortNumber,
                                 pu4RetValDot1xAuthBackendAuthFails)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthBackendAuthFails;

#endif /*  */
{
    *pu4RetValDot1xAuthBackendAuthFails = (UINT4) PNAC_NO_VAL;
    PNAC_UNUSED (i4Dot1xPaePortNumber);
    PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Authenticator Diagnostics"
              " Entries are not supported in this implementation\n");
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* LOW LEVEL Routines for Table : Dot1xAuthSessionStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1xAuthSessionStatsTable
 Input       :  The Indices
                Dot1xPaePortNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceDot1xAuthSessionStatsTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceDot1xAuthSessionStatsTable (INT4 i4Dot1xPaePortNumber)
#else /*  */
INT1
nmhValidateIndexInstanceDot1xAuthSessionStatsTable (i4Dot1xPaePortNumber)
     INT4                i4Dot1xPaePortNumber;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PNAC_IS_SHUTDOWN ())

    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
    {
        return SNMP_FAILURE;
    }

    if (PnacSnmpLowValidatePortNumber (i4Dot1xPaePortNumber) == PNAC_SUCCESS)

    {
        if (PNAC_PORT_INFO (i4Dot1xPaePortNumber).u1PortAuthMode ==
            PNAC_PORT_AUTHMODE_PORTBASED)

        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1xAuthSessionStatsTable
 Input       :  The Indices
                Dot1xPaePortNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstDot1xAuthSessionStatsTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexDot1xAuthSessionStatsTable (INT4 *pi4Dot1xPaePortNumber)
#else /*  */
INT1
nmhGetFirstIndexDot1xAuthSessionStatsTable (pi4Dot1xPaePortNumber)
     INT4               *pi4Dot1xPaePortNumber;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PNAC_IS_SHUTDOWN ())

    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetNextPortBasedValidPortNumber (0, pi4Dot1xPaePortNumber)
        == PNAC_SUCCESS)

    {
        if (PnacGetPortEntry ((UINT2) *pi4Dot1xPaePortNumber, &pPortInfo) !=
            PNAC_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }

    }

    else

    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", *pi4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1xAuthSessionStatsTable
 Input       :  The Indices
                Dot1xPaePortNumber
                nextDot1xPaePortNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextDot1xAuthSessionStatsTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexDot1xAuthSessionStatsTable (INT4 i4Dot1xPaePortNumber,
                                           INT4 *pi4NextDot1xPaePortNumber)
#else /*  */
INT1
nmhGetNextIndexDot1xAuthSessionStatsTable (i4Dot1xPaePortNumber,
                                           pi4NextDot1xPaePortNumber)
     INT4                i4Dot1xPaePortNumber;
     INT4               *pi4NextDot1xPaePortNumber;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (i4Dot1xPaePortNumber < 0)

    {
        return SNMP_FAILURE;
    }
    if (PNAC_IS_SHUTDOWN ())

    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetNextPortBasedValidPortNumber
        (i4Dot1xPaePortNumber, pi4NextDot1xPaePortNumber) == PNAC_SUCCESS)

    {
        if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
            PNAC_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }

    }

    else

    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", *pi4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1xAuthSessionOctetsRx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthSessionOctetsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthSessionOctetsRx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthSessionOctetsRx (INT4 i4Dot1xPaePortNumber,
                                tSNMP_COUNTER64_TYPE *
                                pu8RetValDot1xAuthSessionOctetsRx)
#else /*  */
INT1
nmhGetDot1xAuthSessionOctetsRx (i4Dot1xPaePortNumber,
                                pu8RetValDot1xAuthSessionOctetsRx)
     INT4                i4Dot1xPaePortNumber;
     tSNMP_COUNTER64_TYPE *pu8RetValDot1xAuthSessionOctetsRx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    /* If 64 bytes Counter not supported in the hardware, 
     * the hardware API PnacPnacHwGetSessionCounter() should return '0' in 
     * its 4th argument
     */
    if (PnacPnacHwGetSessionCounter ((UINT2) i4Dot1xPaePortNumber,
                                     (UINT1 *) &(pPortInfo->
                                                 pPortAuthSessionNode->
                                                 rmtSuppMacAddr),
                                     (UINT1) PNAC_COUNT_SESS_OCTETS_RX,
                                     (UINT4 *)
                                     &(pu8RetValDot1xAuthSessionOctetsRx->
                                       msn),
                                     (UINT4 *)
                                     &(pu8RetValDot1xAuthSessionOctetsRx->
                                       lsn)) != FNP_SUCCESS)

    {
        return SNMP_FAILURE;
    }

#else /*  */
    pu8RetValDot1xAuthSessionOctetsRx->msn =
        pPortInfo->pPortAuthSessionNode->authSessStats.u8SessOctetsRx.u4HiWord;
    pu8RetValDot1xAuthSessionOctetsRx->lsn =
        pPortInfo->pPortAuthSessionNode->authSessStats.u8SessOctetsRx.u4LoWord;

#endif /* NPAPI_WANTED */
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthSessionOctetsTx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthSessionOctetsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthSessionOctetsTx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthSessionOctetsTx (INT4 i4Dot1xPaePortNumber,
                                tSNMP_COUNTER64_TYPE *
                                pu8RetValDot1xAuthSessionOctetsTx)
#else /*  */
INT1
nmhGetDot1xAuthSessionOctetsTx (i4Dot1xPaePortNumber,
                                pu8RetValDot1xAuthSessionOctetsTx)
     INT4                i4Dot1xPaePortNumber;
     tSNMP_COUNTER64_TYPE *pu8RetValDot1xAuthSessionOctetsTx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    /* If 64 bytes Counter not supported in the hardware, 
     * the hardware API PnacPnacHwGetSessionCounter() should return '0' in 
     * its 4th argument
     */
    if (PnacPnacHwGetSessionCounter ((UINT2) i4Dot1xPaePortNumber,
                                     (UINT1 *) &(pPortInfo->
                                                 pPortAuthSessionNode->
                                                 rmtSuppMacAddr),
                                     (UINT1) PNAC_COUNT_SESS_OCTETS_TX,
                                     (UINT4 *)
                                     &(pu8RetValDot1xAuthSessionOctetsTx->
                                       msn),
                                     (UINT4 *)
                                     &(pu8RetValDot1xAuthSessionOctetsTx->
                                       lsn)) != FNP_SUCCESS)

    {
        return SNMP_FAILURE;
    }

#else /*  */
    pu8RetValDot1xAuthSessionOctetsTx->msn =
        pPortInfo->pPortAuthSessionNode->authSessStats.u8SessOctetsTx.u4HiWord;
    pu8RetValDot1xAuthSessionOctetsTx->lsn =
        pPortInfo->pPortAuthSessionNode->authSessStats.u8SessOctetsTx.u4LoWord;

#endif /* NPAPI_WANTED */
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthSessionFramesRx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthSessionFramesRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthSessionFramesRx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthSessionFramesRx (INT4 i4Dot1xPaePortNumber,
                                UINT4 *pu4RetValDot1xAuthSessionFramesRx)
#else /*  */
INT1
nmhGetDot1xAuthSessionFramesRx (i4Dot1xPaePortNumber,
                                pu4RetValDot1xAuthSessionFramesRx)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthSessionFramesRx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;

#ifdef NPAPI_WANTED
    UINT4               u4Dummy = 0;

#endif /* NPAPI_WANTED */
    if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (PnacPnacHwGetSessionCounter ((UINT2) i4Dot1xPaePortNumber,
                                     (UINT1 *) &(pPortInfo->
                                                 pPortAuthSessionNode->
                                                 rmtSuppMacAddr),
                                     (UINT1) PNAC_COUNT_SESS_FRAMES_RX,
                                     &u4Dummy,
                                     pu4RetValDot1xAuthSessionFramesRx) !=
        FNP_SUCCESS)

    {
        return SNMP_FAILURE;
    }

#else /*  */
    *pu4RetValDot1xAuthSessionFramesRx =
        pPortInfo->pPortAuthSessionNode->authSessStats.u4SessFramesRx;

#endif /* NPAPI_WANTED */
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthSessionFramesTx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthSessionFramesTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthSessionFramesTx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthSessionFramesTx (INT4 i4Dot1xPaePortNumber,
                                UINT4 *pu4RetValDot1xAuthSessionFramesTx)
#else /*  */
INT1
nmhGetDot1xAuthSessionFramesTx (i4Dot1xPaePortNumber,
                                pu4RetValDot1xAuthSessionFramesTx)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthSessionFramesTx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;

#ifdef NPAPI_WANTED
    UINT4               u4Dummy = 0;

#endif /* NPAPI_WANTED */
    if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (PnacPnacHwGetSessionCounter ((UINT2) i4Dot1xPaePortNumber,
                                     (UINT1 *) &(pPortInfo->
                                                 pPortAuthSessionNode->
                                                 rmtSuppMacAddr),
                                     (UINT1) PNAC_COUNT_SESS_FRAMES_TX,
                                     &u4Dummy,
                                     pu4RetValDot1xAuthSessionFramesTx) !=
        FNP_SUCCESS)

    {
        return SNMP_FAILURE;
    }

#else /*  */
    *pu4RetValDot1xAuthSessionFramesTx =
        pPortInfo->pPortAuthSessionNode->authSessStats.u4SessFramesTx;

#endif /* NPAPI_WANTED */
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthSessionId
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthSessionId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthSessionId ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthSessionId (INT4 i4Dot1xPaePortNumber,
                          tSNMP_OCTET_STRING_TYPE * pRetValDot1xAuthSessionId)
#else /*  */
INT1
nmhGetDot1xAuthSessionId (i4Dot1xPaePortNumber, pRetValDot1xAuthSessionId)
     INT4                i4Dot1xPaePortNumber;
     tSNMP_OCTET_STRING_TYPE *pRetValDot1xAuthSessionId;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacAuthSessStats *pSessStatsEntry = NULL;
    if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    pSessStatsEntry = &(pPortInfo->pPortAuthSessionNode->authSessStats);
    MEMCPY (pRetValDot1xAuthSessionId->pu1_OctetList,
            pSessStatsEntry->aSessId, pSessStatsEntry->u2SessIdLen);
    pRetValDot1xAuthSessionId->i4_Length = pSessStatsEntry->u2SessIdLen;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthSessionAuthenticMethod
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthSessionAuthenticMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthSessionAuthenticMethod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthSessionAuthenticMethod (INT4 i4Dot1xPaePortNumber,
                                       INT4
                                       *pi4RetValDot1xAuthSessionAuthenticMethod)
#else /*  */
INT1
nmhGetDot1xAuthSessionAuthenticMethod (i4Dot1xPaePortNumber,
                                       pi4RetValDot1xAuthSessionAuthenticMethod)
     INT4                i4Dot1xPaePortNumber;
     INT4               *pi4RetValDot1xAuthSessionAuthenticMethod;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    *pi4RetValDot1xAuthSessionAuthenticMethod =
        gPnacSystemInfo.u1AuthenticMethod;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthSessionTime
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthSessionTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthSessionTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthSessionTime (INT4 i4Dot1xPaePortNumber,
                            UINT4 *pu4RetValDot1xAuthSessionTime)
#else /*  */
INT1
nmhGetDot1xAuthSessionTime (i4Dot1xPaePortNumber, pu4RetValDot1xAuthSessionTime)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xAuthSessionTime;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacSysTime        sysTime = PNAC_NO_VAL;
    if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }

    /* Session Time should be returned as follows.
     * a) when a port is not currently authorized - 
     the time duration of the last authorized session
     * b) when a port is currently authorized - 
     the time duration since the session got authorized.
     */
    if (pPortInfo->pPortAuthSessionNode->authFsmInfo.
        u2AuthControlPortStatus == PNAC_PORTSTATUS_AUTHORIZED)

    {

        /* get current System time */
        PNAC_GET_SYSTEM_TIME (&sysTime);

        /* get the time elapsed since the Session got Authenticated */
        *pu4RetValDot1xAuthSessionTime =
            (UINT4) ((sysTime -
                      ((tPnacSysTime) pPortInfo->pPortAuthSessionNode->
                       authSessStats.u4SessTime)) / SYS_TIME_TICKS_IN_A_SEC);
        *pu4RetValDot1xAuthSessionTime =
            PNAC_SYS_TO_MGMT (*pu4RetValDot1xAuthSessionTime);
    }

    else

    {

        /* get the time duration of the last authorized session */
        if (pPortInfo->pPortAuthSessionNode->u1IsAuthorizedOnce != PNAC_TRUE)

        {

            /* Say is the node is authorized atleast once */
            *pu4RetValDot1xAuthSessionTime = 0;    /* No need for sys to mgmt conversion */
            return SNMP_SUCCESS;
        }
        *pu4RetValDot1xAuthSessionTime =
            (pPortInfo->pPortAuthSessionNode->authSessStats.u4SessTime /
             SYS_TIME_TICKS_IN_A_SEC);
        *pu4RetValDot1xAuthSessionTime =
            PNAC_SYS_TO_MGMT (*pu4RetValDot1xAuthSessionTime);
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthSessionTerminateCause
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthSessionTerminateCause
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthSessionTerminateCause ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthSessionTerminateCause (INT4 i4Dot1xPaePortNumber,
                                      INT4
                                      *pi4RetValDot1xAuthSessionTerminateCause)
#else /*  */
INT1
nmhGetDot1xAuthSessionTerminateCause (i4Dot1xPaePortNumber,
                                      pi4RetValDot1xAuthSessionTerminateCause)
     INT4                i4Dot1xPaePortNumber;
     INT4               *pi4RetValDot1xAuthSessionTerminateCause;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    *pi4RetValDot1xAuthSessionTerminateCause =
        pPortInfo->pPortAuthSessionNode->authSessStats.u4SessTerminateCause;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xAuthSessionUserName
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xAuthSessionUserName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xAuthSessionUserName ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xAuthSessionUserName (INT4 i4Dot1xPaePortNumber,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValDot1xAuthSessionUserName)
#else /*  */
INT1
nmhGetDot1xAuthSessionUserName (i4Dot1xPaePortNumber,
                                pRetValDot1xAuthSessionUserName)
     INT4                i4Dot1xPaePortNumber;
     tSNMP_OCTET_STRING_TYPE *pRetValDot1xAuthSessionUserName;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacAuthSessStats *pSessStatsEntry = NULL;
    if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    pSessStatsEntry = &(pPortInfo->pPortAuthSessionNode->authSessStats);
    pRetValDot1xAuthSessionUserName->i4_Length =
        pSessStatsEntry->u2SessUserNameLen;
    PNAC_MEMCPY (pRetValDot1xAuthSessionUserName->pu1_OctetList,
                 pSessStatsEntry->au1SessUserName,
                 pSessStatsEntry->u2SessUserNameLen);
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* LOW LEVEL Routines for Table : Dot1xSuppConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1xSuppConfigTable
 Input       :  The Indices
                Dot1xPaePortNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceDot1xSuppConfigTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceDot1xSuppConfigTable (INT4 i4Dot1xPaePortNumber)
#else /*  */
INT1
nmhValidateIndexInstanceDot1xSuppConfigTable (i4Dot1xPaePortNumber)
     INT4                i4Dot1xPaePortNumber;

#endif /*  */
{
    if (PNAC_IS_SHUTDOWN ())

    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowValidateSuppCapPortNumber (i4Dot1xPaePortNumber) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1xSuppConfigTable
 Input       :  The Indices
                Dot1xPaePortNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstDot1xSuppConfigTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexDot1xSuppConfigTable (INT4 *pi4Dot1xPaePortNumber)
#else /*  */
INT1
nmhGetFirstIndexDot1xSuppConfigTable (pi4Dot1xPaePortNumber)
     INT4               *pi4Dot1xPaePortNumber;

#endif /*  */
{
    if (PNAC_IS_SHUTDOWN ())

    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetFirstSuppCapValidPortNumber (pi4Dot1xPaePortNumber)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", *pi4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1xSuppConfigTable
 Input       :  The Indices
                Dot1xPaePortNumber
                nextDot1xPaePortNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextDot1xSuppConfigTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexDot1xSuppConfigTable (INT4 i4Dot1xPaePortNumber,
                                     INT4 *pi4NextDot1xPaePortNumber)
#else /*  */
INT1
nmhGetNextIndexDot1xSuppConfigTable (i4Dot1xPaePortNumber,
                                     pi4NextDot1xPaePortNumber)
     INT4                i4Dot1xPaePortNumber;
     INT4               *pi4NextDot1xPaePortNumber;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (i4Dot1xPaePortNumber < 0)

    {
        return SNMP_FAILURE;
    }
    if (PNAC_IS_SHUTDOWN ())

    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetNextSuppCapValidPortNumber
        (i4Dot1xPaePortNumber, pi4NextDot1xPaePortNumber) == PNAC_SUCCESS)

    {
        if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
            PNAC_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }

    }

    else

    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", *pi4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1xSuppPaeState
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xSuppPaeState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xSuppPaeState ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xSuppPaeState (INT4 i4Dot1xPaePortNumber,
                         INT4 *pi4RetValDot1xSuppPaeState)
#else /*  */
INT1
nmhGetDot1xSuppPaeState (i4Dot1xPaePortNumber, pi4RetValDot1xSuppPaeState)
     INT4                i4Dot1xPaePortNumber;
     INT4               *pi4RetValDot1xSuppPaeState;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    *pi4RetValDot1xSuppPaeState =
        pPortInfo->pPortSuppSessionNode->suppFsmInfo.u2SuppPaeState;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xSuppHeldPeriod
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xSuppHeldPeriod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xSuppHeldPeriod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xSuppHeldPeriod (INT4 i4Dot1xPaePortNumber,
                           UINT4 *pu4RetValDot1xSuppHeldPeriod)
#else /*  */
INT1
nmhGetDot1xSuppHeldPeriod (i4Dot1xPaePortNumber, pu4RetValDot1xSuppHeldPeriod)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xSuppHeldPeriod;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    *pu4RetValDot1xSuppHeldPeriod = pPortInfo->suppConfigEntry.u4HeldPeriod;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xSuppAuthPeriod
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xSuppAuthPeriod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xSuppAuthPeriod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xSuppAuthPeriod (INT4 i4Dot1xPaePortNumber,
                           UINT4 *pu4RetValDot1xSuppAuthPeriod)
#else /*  */
INT1
nmhGetDot1xSuppAuthPeriod (i4Dot1xPaePortNumber, pu4RetValDot1xSuppAuthPeriod)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xSuppAuthPeriod;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    *pu4RetValDot1xSuppAuthPeriod = pPortInfo->suppConfigEntry.u4AuthPeriod;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xSuppStartPeriod
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xSuppStartPeriod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xSuppStartPeriod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xSuppStartPeriod (INT4 i4Dot1xPaePortNumber,
                            UINT4 *pu4RetValDot1xSuppStartPeriod)
#else /*  */
INT1
nmhGetDot1xSuppStartPeriod (i4Dot1xPaePortNumber, pu4RetValDot1xSuppStartPeriod)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xSuppStartPeriod;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    *pu4RetValDot1xSuppStartPeriod = pPortInfo->suppConfigEntry.u4StartPeriod;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xSuppMaxStart
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xSuppMaxStart
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xSuppMaxStart ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xSuppMaxStart (INT4 i4Dot1xPaePortNumber,
                         UINT4 *pu4RetValDot1xSuppMaxStart)
#else /*  */
INT1
nmhGetDot1xSuppMaxStart (i4Dot1xPaePortNumber, pu4RetValDot1xSuppMaxStart)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xSuppMaxStart;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    *pu4RetValDot1xSuppMaxStart = pPortInfo->suppConfigEntry.u4MaxStart;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xSuppControlledPortStatus
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xSuppControlledPortStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xSuppControlledPortStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xSuppControlledPortStatus (INT4 i4Dot1xPaePortNumber,
                                     INT4
                                     *pi4RetValDot1xSuppControlledPortStatus)
#else /*  */
INT1
nmhGetDot1xSuppControlledPortStatus (i4Dot1xPaePortNumber,
                                     pi4RetValDot1xSuppControlledPortStatus)
     INT4                i4Dot1xPaePortNumber;
     INT4               *pi4RetValDot1xSuppControlledPortStatus;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1xSuppControlledPortStatus =
        (INT4) pPortInfo->pPortSuppSessionNode->suppFsmInfo.u2ControlPortStatus;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xSuppAccessCtrlWithAuth
 Input       :  The Indices
                Dot1xPaePortNumber
                The Object 
                retValDot1xSuppAccessCtrlWithAuth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xSuppAccessCtrlWithAuth ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xSuppAccessCtrlWithAuth (INT4 i4Dot1xPaePortNumber,
                                   INT4 *pi4RetValDot1xSuppAccessCtrlWithAuth)
#else /*  */
INT1
nmhGetDot1xSuppAccessCtrlWithAuth (i4Dot1xPaePortNumber,
                                   pi4RetValDot1xSuppAccessCtrlWithAuth)
     INT4                i4Dot1xPaePortNumber;
     INT4               *pi4RetValDot1xSuppAccessCtrlWithAuth;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    if (PNAC_IS_PORT_NOT_BOTH_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    *pi4RetValDot1xSuppAccessCtrlWithAuth =
        pPortInfo->suppConfigEntry.u2SuppAccessCtrlWithAuth;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1xSuppHeldPeriod
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                setValDot1xSuppHeldPeriod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1xSuppHeldPeriod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetDot1xSuppHeldPeriod (INT4 i4Dot1xPaePortNumber,
                           UINT4 u4SetValDot1xSuppHeldPeriod)
#else /*  */
INT1
nmhSetDot1xSuppHeldPeriod (i4Dot1xPaePortNumber, u4SetValDot1xSuppHeldPeriod)
     INT4                i4Dot1xPaePortNumber;
     UINT4               u4SetValDot1xSuppHeldPeriod;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    if (pPortInfo->suppConfigEntry.u4HeldPeriod == u4SetValDot1xSuppHeldPeriod)

    {
        return SNMP_SUCCESS;
    }
    pPortInfo->suppConfigEntry.u4HeldPeriod = u4SetValDot1xSuppHeldPeriod;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY,"Dot1xSuppHeldPeriod = %u\n", u4SetValDot1xSuppHeldPeriod); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetDot1xSuppAuthPeriod
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                setValDot1xSuppAuthPeriod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1xSuppAuthPeriod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetDot1xSuppAuthPeriod (INT4 i4Dot1xPaePortNumber,
                           UINT4 u4SetValDot1xSuppAuthPeriod)
#else /*  */
INT1
nmhSetDot1xSuppAuthPeriod (i4Dot1xPaePortNumber, u4SetValDot1xSuppAuthPeriod)
     INT4                i4Dot1xPaePortNumber;
     UINT4               u4SetValDot1xSuppAuthPeriod;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    if (pPortInfo->suppConfigEntry.u4AuthPeriod == u4SetValDot1xSuppAuthPeriod)

    {
        return SNMP_SUCCESS;
    }
    pPortInfo->suppConfigEntry.u4AuthPeriod = u4SetValDot1xSuppAuthPeriod;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY,"Dot1xSuppAuthPeriod = %u\n", u4SetValDot1xSuppAuthPeriod); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetDot1xSuppStartPeriod
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                setValDot1xSuppStartPeriod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1xSuppStartPeriod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetDot1xSuppStartPeriod (INT4 i4Dot1xPaePortNumber,
                            UINT4 u4SetValDot1xSuppStartPeriod)
#else /*  */
INT1
nmhSetDot1xSuppStartPeriod (i4Dot1xPaePortNumber, u4SetValDot1xSuppStartPeriod)
     INT4                i4Dot1xPaePortNumber;
     UINT4               u4SetValDot1xSuppStartPeriod;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    if (pPortInfo->suppConfigEntry.u4StartPeriod ==
        u4SetValDot1xSuppStartPeriod)

    {
        return SNMP_SUCCESS;
    }
    pPortInfo->suppConfigEntry.u4StartPeriod = u4SetValDot1xSuppStartPeriod;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY,"Dot1xSuppStartPeriod = %u\n", u4SetValDot1xSuppStartPeriod); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetDot1xSuppMaxStart
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                setValDot1xSuppAccessCtrlWithAuth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1xSuppAccessCtrlWithAuth ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetDot1xSuppAccessCtrlWithAuth (INT4 i4Dot1xPaePortNumber,
                                   INT4 i4SetValDot1xSuppAccessCtrlWithAuth)
#else /*  */
INT1
nmhSetDot1xSuppAccessCtrlWithAuth (i4Dot1xPaePortNumber,
                                   i4SetValDot1xSuppAccessCtrlWithAuth)
     INT4                i4Dot1xPaePortNumber;
     INT4                i4SetValDot1xSuppAccessCtrlWithAuth;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    UINT1               i1RetVal;

    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (PNAC_IS_PORT_NOT_BOTH_CAPABLE (pPortInfo))
    {
        return SNMP_FAILURE;
    }

    if (PNAC_IS_PORT_MODE_MACBASED (pPortInfo) &&
        (i4SetValDot1xSuppAccessCtrlWithAuth == PNAC_SUPP_ACCESS_CTRL_ACTIVE))
    {
        return SNMP_FAILURE;
    }

    if (pPortInfo->suppConfigEntry.u2SuppAccessCtrlWithAuth ==
        (UINT2) i4SetValDot1xSuppAccessCtrlWithAuth)

    {
        return SNMP_SUCCESS;
    }

    pPortInfo->suppConfigEntry.u2SuppAccessCtrlWithAuth =
        (UINT2) i4SetValDot1xSuppAccessCtrlWithAuth;
    i1RetVal = PnacCheckIfPortStatusChanged ((UINT2) i4Dot1xPaePortNumber);
    UNUSED_PARAM (i1RetVal);
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY,"Dot1xSuppStartPeriod = %u\n", u2SetValDot1xSuppAccessCtrlWithAuth); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetDot1xSuppMaxStart
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                setValDot1xSuppMaxStart
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1xSuppMaxStart ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetDot1xSuppMaxStart (INT4 i4Dot1xPaePortNumber,
                         UINT4 u4SetValDot1xSuppMaxStart)
#else /*  */
INT1
nmhSetDot1xSuppMaxStart (i4Dot1xPaePortNumber, u4SetValDot1xSuppMaxStart)
     INT4                i4Dot1xPaePortNumber;
     UINT4               u4SetValDot1xSuppMaxStart;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    if (pPortInfo->suppConfigEntry.u4MaxStart == u4SetValDot1xSuppMaxStart)

    {
        return SNMP_SUCCESS;
    }
    pPortInfo->suppConfigEntry.u4MaxStart = u4SetValDot1xSuppMaxStart;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY,"Dot1xSuppMaxStart = %u\n", u4SetValDot1xSuppMaxStart); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1xSuppHeldPeriod
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                testValDot1xSuppHeldPeriod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1xSuppHeldPeriod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1xSuppHeldPeriod (UINT4 *pu4ErrorCode, INT4 i4Dot1xPaePortNumber,
                              UINT4 u4TestValDot1xSuppHeldPeriod)
#else /*  */
INT1
nmhTestv2Dot1xSuppHeldPeriod (pu4ErrorCode, i4Dot1xPaePortNumber,
                              u4TestValDot1xSuppHeldPeriod)
     UINT4              *pu4ErrorCode;
     INT4                i4Dot1xPaePortNumber;
     UINT4               u4TestValDot1xSuppHeldPeriod;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValDot1xSuppHeldPeriod < (UINT4) PNAC_HELDPERIOD_MIN) ||
        (u4TestValDot1xSuppHeldPeriod > (UINT4) PNAC_HELDPERIOD_MAX))
    {

        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: HeldPeriod cannot be"
                  " less than 1 time tick or greater than 65535 \n");
        return SNMP_FAILURE;
    }

    if (PNAC_IS_PORT_MODE_MACBASED (pPortInfo) &&
        (u4TestValDot1xSuppHeldPeriod != PNAC_HELDPERIOD))
    {
        CLI_SET_ERR (CLI_HELD_PERIOD_CONFIG_NOT_ALLOWED);
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY,"Dot1xSuppHeldPeriod = %u\n", u4TestValDot1xSuppHeldPeriod); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Dot1xSuppAuthPeriod
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                testValDot1xSuppAuthPeriod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1xSuppAuthPeriod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1xSuppAuthPeriod (UINT4 *pu4ErrorCode, INT4 i4Dot1xPaePortNumber,
                              UINT4 u4TestValDot1xSuppAuthPeriod)
#else /*  */
INT1
nmhTestv2Dot1xSuppAuthPeriod (pu4ErrorCode, i4Dot1xPaePortNumber,
                              u4TestValDot1xSuppAuthPeriod)
     UINT4              *pu4ErrorCode;
     INT4                i4Dot1xPaePortNumber;
     UINT4               u4TestValDot1xSuppAuthPeriod;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValDot1xSuppAuthPeriod < (UINT4) PNAC_AUTHPERIOD_MIN) ||
        (u4TestValDot1xSuppAuthPeriod > (UINT4) PNAC_AUTHPERIOD_MAX))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: AuthPeriod cannot be"
                  " less than 1 time tick or greater than 65535 \n");
        return SNMP_FAILURE;
    }

    if (PNAC_IS_PORT_MODE_MACBASED (pPortInfo) &&
        (u4TestValDot1xSuppAuthPeriod != PNAC_AUTHPERIOD))
    {
        CLI_SET_ERR (CLI_AUTH_PERIOD_CONFIG_NOT_ALLOWED);
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY,"Dot1xSuppAuthPeriod = %u\n", u4TestValDot1xSuppAuthPeriod); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Dot1xSuppStartPeriod
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                testValDot1xSuppStartPeriod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1xSuppStartPeriod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1xSuppStartPeriod (UINT4 *pu4ErrorCode, INT4 i4Dot1xPaePortNumber,
                               UINT4 u4TestValDot1xSuppStartPeriod)
#else /*  */
INT1
nmhTestv2Dot1xSuppStartPeriod (pu4ErrorCode, i4Dot1xPaePortNumber,
                               u4TestValDot1xSuppStartPeriod)
     UINT4              *pu4ErrorCode;
     INT4                i4Dot1xPaePortNumber;
     UINT4               u4TestValDot1xSuppStartPeriod;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (!PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((u4TestValDot1xSuppStartPeriod < (UINT4) PNAC_STARTPERIOD_MIN) ||
        (u4TestValDot1xSuppStartPeriod > (UINT4) PNAC_STARTPERIOD_MAX))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: StartPeriod cannot be"
                  " less than 1 time tick or greater than 65535 \n");
        return SNMP_FAILURE;
    }
    if (PNAC_IS_PORT_MODE_MACBASED (pPortInfo) &&
        (u4TestValDot1xSuppStartPeriod != PNAC_STARTPERIOD))
    {
        CLI_SET_ERR (CLI_START_PERIOD_CONFIG_NOT_ALLOWED);
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY,"Dot1xSuppStartPeriod = %u\n", u4TestValDot1xSuppStartPeriod); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Dot1xSuppAccessCtrlWithAuth
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                testValDot1xSuppAccessCtrlWithAuth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1xSuppAccessCtrlWithAuth ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1xSuppAccessCtrlWithAuth (UINT4 *pu4ErrorCode,
                                      INT4 i4Dot1xPaePortNumber,
                                      INT4 i4TestValDot1xSuppAccessCtrlWithAuth)
#else /*  */
INT1
nmhTestv2Dot1xSuppAccessCtrlWithAuth (pu4ErrorCode, i4Dot1xPaePortNumber,
                                      i4TestValDot1xSuppAccessCtrlWithAuth)
     UINT4              *pu4ErrorCode;
     INT4                i4Dot1xPaePortNumber;
     INT4                i4TestValDot1xSuppAccessCtrlWithAuth;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (PNAC_IS_PORT_NOT_BOTH_CAPABLE (pPortInfo))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else if ((i4TestValDot1xSuppAccessCtrlWithAuth <
              PNAC_SUPP_ACCESS_CTRL_INACTIVE)
             || (i4TestValDot1xSuppAccessCtrlWithAuth >
                 PNAC_SUPP_ACCESS_CTRL_ACTIVE))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: Value of SuppAccessCtrlWith cannot be"
                  " less than 1 or greater than 2 \n");
        return SNMP_FAILURE;
    }

    if (PNAC_IS_PORT_MODE_MACBASED (pPortInfo) &&
        (i4TestValDot1xSuppAccessCtrlWithAuth == PNAC_SUPP_ACCESS_CTRL_ACTIVE))
    {
        CLI_SET_ERR (CLI_ACCESS_ACTIVE_NOT_ALLOWED);
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1xSuppAccessCtrlWithAuth == PNAC_SUPP_ACCESS_CTRL_ACTIVE)
        && (gPnacSystemInfo.u1SystemMode == PNAC_DISTRIBUTED))
    {
        CLI_SET_ERR (CLI_DPNAC_ACCESS_CTRL_NOT_ALLOWED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY,"Dot1xSuppAccessCtrlWithAuth = 
                     %u\n", u2TestValDot1xSuppAccessCtrlWithAuth); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Dot1xSuppMaxStart
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                testValDot1xSuppMaxStart
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1xSuppMaxStart ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1xSuppMaxStart (UINT4 *pu4ErrorCode, INT4 i4Dot1xPaePortNumber,
                            UINT4 u4TestValDot1xSuppMaxStart)
#else /*  */
INT1
nmhTestv2Dot1xSuppMaxStart (pu4ErrorCode, i4Dot1xPaePortNumber,
                            u4TestValDot1xSuppMaxStart)
     UINT4              *pu4ErrorCode;
     INT4                i4Dot1xPaePortNumber;
     UINT4               u4TestValDot1xSuppMaxStart;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else if ((u4TestValDot1xSuppMaxStart < (UINT4) PNAC_MAXSTART_MIN) ||
             (u4TestValDot1xSuppMaxStart > (UINT4) PNAC_MAXSTART_MAX))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: MaxStart cannot be"
                  " less than 1 or greater than 65535 \n");
        return SNMP_FAILURE;
    }
    if (PNAC_IS_PORT_MODE_MACBASED (pPortInfo) &&
        (u4TestValDot1xSuppMaxStart != PNAC_MAXSTART))
    {
        CLI_SET_ERR (CLI_MAX_START_CONFIG_NOT_ALLOWED);
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY,"Dot1xSuppMaxStart = %u\n", u4TestValDot1xSuppMaxStart); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1xSuppConfigTable
 Input       :  The Indices
                Dot1xPaePortNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1xSuppConfigTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1xSuppStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1xSuppStatsTable
 Input       :  The Indices
                Dot1xPaePortNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceDot1xSuppStatsTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceDot1xSuppStatsTable (INT4 i4Dot1xPaePortNumber)
#else /*  */
INT1
nmhValidateIndexInstanceDot1xSuppStatsTable (i4Dot1xPaePortNumber)
     INT4                i4Dot1xPaePortNumber;

#endif /*  */
{
    if (PNAC_IS_SHUTDOWN ())

    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowValidateSuppCapPortNumber (i4Dot1xPaePortNumber) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1xSuppStatsTable
 Input       :  The Indices
                Dot1xPaePortNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstDot1xSuppStatsTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexDot1xSuppStatsTable (INT4 *pi4Dot1xPaePortNumber)
#else /*  */
INT1
nmhGetFirstIndexDot1xSuppStatsTable (pi4Dot1xPaePortNumber)
     INT4               *pi4Dot1xPaePortNumber;

#endif /*  */
{
    if (PNAC_IS_SHUTDOWN ())

    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetFirstSuppCapValidPortNumber (pi4Dot1xPaePortNumber) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", *pi4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1xSuppStatsTable
 Input       :  The Indices
                Dot1xPaePortNumber
                nextDot1xPaePortNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextDot1xSuppStatsTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexDot1xSuppStatsTable (INT4 i4Dot1xPaePortNumber,
                                    INT4 *pi4NextDot1xPaePortNumber)
#else /*  */
INT1
nmhGetNextIndexDot1xSuppStatsTable (i4Dot1xPaePortNumber,
                                    pi4NextDot1xPaePortNumber)
     INT4                i4Dot1xPaePortNumber;
     INT4               *pi4NextDot1xPaePortNumber;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (i4Dot1xPaePortNumber < 0)

    {
        return SNMP_FAILURE;
    }
    if (PNAC_IS_SHUTDOWN ())

    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetNextSuppCapValidPortNumber
        (i4Dot1xPaePortNumber, pi4NextDot1xPaePortNumber) == PNAC_SUCCESS)

    {
        if (PnacGetPortEntry ((UINT2) *pi4NextDot1xPaePortNumber, &pPortInfo) !=
            PNAC_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }

    else

    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", *pi4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1xSuppEapolFramesRx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xSuppEapolFramesRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xSuppEapolFramesRx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xSuppEapolFramesRx (INT4 i4Dot1xPaePortNumber,
                              UINT4 *pu4RetValDot1xSuppEapolFramesRx)
#else /*  */
INT1
nmhGetDot1xSuppEapolFramesRx (i4Dot1xPaePortNumber,
                              pu4RetValDot1xSuppEapolFramesRx)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xSuppEapolFramesRx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    *pu4RetValDot1xSuppEapolFramesRx =
        PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolFramesRx;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xSuppEapolFramesTx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xSuppEapolFramesTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xSuppEapolFramesTx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xSuppEapolFramesTx (INT4 i4Dot1xPaePortNumber,
                              UINT4 *pu4RetValDot1xSuppEapolFramesTx)
#else /*  */
INT1
nmhGetDot1xSuppEapolFramesTx (i4Dot1xPaePortNumber,
                              pu4RetValDot1xSuppEapolFramesTx)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xSuppEapolFramesTx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    *pu4RetValDot1xSuppEapolFramesTx =
        PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolFramesTx;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xSuppEapolStartFramesTx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xSuppEapolStartFramesTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xSuppEapolStartFramesTx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xSuppEapolStartFramesTx (INT4 i4Dot1xPaePortNumber,
                                   UINT4 *pu4RetValDot1xSuppEapolStartFramesTx)
#else /*  */
INT1
nmhGetDot1xSuppEapolStartFramesTx (i4Dot1xPaePortNumber,
                                   pu4RetValDot1xSuppEapolStartFramesTx)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xSuppEapolStartFramesTx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    *pu4RetValDot1xSuppEapolStartFramesTx =
        PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolStartFramesTx;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xSuppEapolLogoffFramesTx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xSuppEapolLogoffFramesTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xSuppEapolLogoffFramesTx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xSuppEapolLogoffFramesTx (INT4 i4Dot1xPaePortNumber,
                                    UINT4
                                    *pu4RetValDot1xSuppEapolLogoffFramesTx)
#else /*  */
INT1
nmhGetDot1xSuppEapolLogoffFramesTx (i4Dot1xPaePortNumber,
                                    pu4RetValDot1xSuppEapolLogoffFramesTx)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xSuppEapolLogoffFramesTx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    *pu4RetValDot1xSuppEapolLogoffFramesTx =
        PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolLogoffFramesTx;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xSuppEapolRespIdFramesTx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xSuppEapolRespIdFramesTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xSuppEapolRespIdFramesTx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xSuppEapolRespIdFramesTx (INT4 i4Dot1xPaePortNumber,
                                    UINT4
                                    *pu4RetValDot1xSuppEapolRespIdFramesTx)
#else /*  */
INT1
nmhGetDot1xSuppEapolRespIdFramesTx (i4Dot1xPaePortNumber,
                                    pu4RetValDot1xSuppEapolRespIdFramesTx)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xSuppEapolRespIdFramesTx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    *pu4RetValDot1xSuppEapolRespIdFramesTx =
        PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolRespIdFramesTx;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xSuppEapolRespFramesTx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xSuppEapolRespFramesTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xSuppEapolRespFramesTx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xSuppEapolRespFramesTx (INT4 i4Dot1xPaePortNumber,
                                  UINT4 *pu4RetValDot1xSuppEapolRespFramesTx)
#else /*  */
INT1
nmhGetDot1xSuppEapolRespFramesTx (i4Dot1xPaePortNumber,
                                  pu4RetValDot1xSuppEapolRespFramesTx)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xSuppEapolRespFramesTx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    *pu4RetValDot1xSuppEapolRespFramesTx =
        PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolRespFramesTx;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xSuppEapolReqIdFramesRx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xSuppEapolReqIdFramesRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xSuppEapolReqIdFramesRx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xSuppEapolReqIdFramesRx (INT4 i4Dot1xPaePortNumber,
                                   UINT4 *pu4RetValDot1xSuppEapolReqIdFramesRx)
#else /*  */
INT1
nmhGetDot1xSuppEapolReqIdFramesRx (i4Dot1xPaePortNumber,
                                   pu4RetValDot1xSuppEapolReqIdFramesRx)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xSuppEapolReqIdFramesRx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    *pu4RetValDot1xSuppEapolReqIdFramesRx =
        PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolReqIdFramesRx;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xSuppEapolReqFramesRx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xSuppEapolReqFramesRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xSuppEapolReqFramesRx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xSuppEapolReqFramesRx (INT4 i4Dot1xPaePortNumber,
                                 UINT4 *pu4RetValDot1xSuppEapolReqFramesRx)
#else /*  */
INT1
nmhGetDot1xSuppEapolReqFramesRx (i4Dot1xPaePortNumber,
                                 pu4RetValDot1xSuppEapolReqFramesRx)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xSuppEapolReqFramesRx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    *pu4RetValDot1xSuppEapolReqFramesRx =
        PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapolReqFramesRx;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xSuppInvalidEapolFramesRx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xSuppInvalidEapolFramesRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xSuppInvalidEapolFramesRx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xSuppInvalidEapolFramesRx (INT4 i4Dot1xPaePortNumber,
                                     UINT4
                                     *pu4RetValDot1xSuppInvalidEapolFramesRx)
#else /*  */
INT1
nmhGetDot1xSuppInvalidEapolFramesRx (i4Dot1xPaePortNumber,
                                     pu4RetValDot1xSuppInvalidEapolFramesRx)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xSuppInvalidEapolFramesRx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    *pu4RetValDot1xSuppInvalidEapolFramesRx =
        PNAC_SUPP_STATS_ENTRY (pPortInfo).u4InvalidEapolFramesRx;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xSuppEapLengthErrorFramesRx
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xSuppEapLengthErrorFramesRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xSuppEapLengthErrorFramesRx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xSuppEapLengthErrorFramesRx (INT4 i4Dot1xPaePortNumber,
                                       UINT4
                                       *pu4RetValDot1xSuppEapLengthErrorFramesRx)
#else /*  */
INT1
nmhGetDot1xSuppEapLengthErrorFramesRx (i4Dot1xPaePortNumber,
                                       pu4RetValDot1xSuppEapLengthErrorFramesRx)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xSuppEapLengthErrorFramesRx;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    *pu4RetValDot1xSuppEapLengthErrorFramesRx =
        PNAC_SUPP_STATS_ENTRY (pPortInfo).u4EapLengthErrorFramesRx;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xSuppLastEapolFrameVersion
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xSuppLastEapolFrameVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xSuppLastEapolFrameVersion ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xSuppLastEapolFrameVersion (INT4 i4Dot1xPaePortNumber,
                                      UINT4
                                      *pu4RetValDot1xSuppLastEapolFrameVersion)
#else /*  */
INT1
nmhGetDot1xSuppLastEapolFrameVersion (i4Dot1xPaePortNumber,
                                      pu4RetValDot1xSuppLastEapolFrameVersion)
     INT4                i4Dot1xPaePortNumber;
     UINT4              *pu4RetValDot1xSuppLastEapolFrameVersion;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    *pu4RetValDot1xSuppLastEapolFrameVersion =
        PNAC_SUPP_STATS_ENTRY (pPortInfo).u4LastEapolFrameVersion;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1xSuppLastEapolFrameSource
 Input       :  The Indices
                Dot1xPaePortNumber

                The Object 
                retValDot1xSuppLastEapolFrameSource
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1xSuppLastEapolFrameSource ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1xSuppLastEapolFrameSource (INT4 i4Dot1xPaePortNumber,
                                     tMacAddr *
                                     pRetValDot1xSuppLastEapolFrameSource)
#else /*  */
INT1
nmhGetDot1xSuppLastEapolFrameSource (i4Dot1xPaePortNumber,
                                     pRetValDot1xSuppLastEapolFrameSource)
     INT4                i4Dot1xPaePortNumber;
     tMacAddr           *pRetValDot1xSuppLastEapolFrameSource;

#endif /*  */
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PnacSnmpLowGetPortInfo (i4Dot1xPaePortNumber, &pPortInfo)
        != PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))

    {
        return SNMP_FAILURE;
    }
    PNAC_MEMCPY ((UINT1 *) *pRetValDot1xSuppLastEapolFrameSource,
                 PNAC_SUPP_STATS_ENTRY (pPortInfo).lastEapolFrameSource,
                 PNAC_MAC_ADDR_SIZE);
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "Dot1xPaePortNumber = %d\n", i4Dot1xPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Utility Routines for All Tables */

/****************************************************************************
 Function    :  PnacSnmpLowValidatePortNumber 
 Input       :  i4Dot1xPaePortNumber - Port number

 Output      :  None
 Returns     :  PNAC_SUCCESS or PNAC_FAILURE
****************************************************************************/
INT4
PnacSnmpLowValidatePortNumber (INT4 i4Dot1xPaePortNumber)
{
    if ((i4Dot1xPaePortNumber < PNAC_MINPORTS) ||
        (i4Dot1xPaePortNumber > PNAC_MAXPORTS))

    {
        return PNAC_FAILURE;
    }
    if (PNAC_PORT_INFO (i4Dot1xPaePortNumber).u1EntryStatus
        == PNAC_PORT_INVALID)

    {
        return PNAC_FAILURE;
    }
    return PNAC_SUCCESS;
}

/****************************************************************************
 Function    :  PnacSnmpLowGetFirstValidPortNumber
 Input       :  None
 Output      :  This Routine Takes the argument pi4Dot1xPaePortNumber &
                stores the first Valid Port Number of the PaePortEntry Table
                in the argument.
 Returns     :  PNAC_SUCCESS or PNAC_FAILURE
****************************************************************************/
INT4
PnacSnmpLowGetFirstValidPortNumber (INT4 *pi4Dot1xPaePortNumber)
{
    if ((PnacSnmpLowGetNextValidPortNumber (0, pi4Dot1xPaePortNumber))
        == PNAC_SUCCESS)

    {
        return PNAC_SUCCESS;
    }

    else

    {
        return PNAC_FAILURE;
    }
}

/****************************************************************************
 Function    :  PnacSnmpLowGetNextValidPortNumber
 Input       :  i4Dot1xPaePortNumber - Port number
 
 Output      :  This Routine Takes the argument pi4Dot1xPaePortNumber &
                stores in it, the first Valid Port Number that occurs next to
                the PaePortEntry Table.
 Returns     :  PNAC_SUCCESS or PNAC_FAILURE
****************************************************************************/
INT4
PnacSnmpLowGetNextValidPortNumber (INT4 i4Dot1xPaePortNumber,
                                   INT4 *pi4Dot1xPaePortNumber)
{
    INT4                i4Count;

    if (i4Dot1xPaePortNumber > PNAC_MAXPORTS)
    {
        return PNAC_FAILURE;
    }

    for (i4Count = i4Dot1xPaePortNumber + 1; i4Count <= PNAC_MAXPORTS;
         i4Count++)

    {
        if (PNAC_PORT_INFO (i4Count).u1EntryStatus != PNAC_PORT_INVALID)

        {
            *pi4Dot1xPaePortNumber = i4Count;
            return PNAC_SUCCESS;
        }
    }
    PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: No other Port is created\n");
    return PNAC_FAILURE;
}

/****************************************************************************
 Function    :  PnacSnmpLowGetPortInfo
 Input       :  i4Dot1xPaePortNumber - Port number 
                ppPortInfo - double pointer to Port Entry

 Output      :  This Routine Takes the argument pPortInfo &
                stores in it, the port table entry corresponding to that port.
 Returns     :  PNAC_SUCCESS or PNAC_FAILURE
****************************************************************************/
INT4
PnacSnmpLowGetPortInfo (INT4 i4Dot1xPaePortNumber,
                        tPnacPaePortEntry ** ppPortInfo)
{
    if ((PNAC_PORT_INFO (i4Dot1xPaePortNumber).
         u1Capabilities & PNAC_PORT_CAPABILITY_AUTH))

    {

        /* The port is Authenticator capable */
        if (PNAC_PORT_INFO (i4Dot1xPaePortNumber).u1PortAuthMode !=
            PNAC_PORT_AUTHMODE_PORTBASED)

        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: Only objects"
                      " for Port-based-authentication is accessible"
                      " on this Port-based-authentication port\n");
            return PNAC_FAILURE;
        }
    }
    return (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, ppPortInfo));
}

/****************************************************************************
 Function    :  PnacSnmpLowValidateSuppCapPortNumber 
 Input       :  i4Dot1xPaePortNumber - Port number

 Output      :  None
 Returns     :  PNAC_SUCCESS or PNAC_FAILURE
****************************************************************************/
INT4
PnacSnmpLowValidateSuppCapPortNumber (INT4 i4Dot1xPaePortNumber)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if ((i4Dot1xPaePortNumber < PNAC_MINPORTS) ||
        (i4Dot1xPaePortNumber > PNAC_MAXPORTS))

    {
        return PNAC_FAILURE;
    }
    if (PnacGetPortEntry ((UINT2) i4Dot1xPaePortNumber, &pPortInfo) ==
        PNAC_SUCCESS)

    {
        if (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))

        {
            return PNAC_SUCCESS;
        }
    }
    return PNAC_FAILURE;
}

/****************************************************************************
 Function    :  PnacSnmpLowGetFirstSuppCapValidPortNumber
 Input       :  None
 Output      :  This Routine Takes the argument pi4Dot1xPaePortNumber &
                stores the first Valid Suppliant Capable Port Number in 
                the PaePortEntry Table in this argument.
 Returns     :  PNAC_SUCCESS or PNAC_FAILURE
****************************************************************************/
INT4
PnacSnmpLowGetFirstSuppCapValidPortNumber (INT4 *pi4Dot1xPaePortNumber)
{
    if ((PnacSnmpLowGetNextSuppCapValidPortNumber (0, pi4Dot1xPaePortNumber))
        == PNAC_SUCCESS)

    {
        return PNAC_SUCCESS;
    }

    else

    {
        return PNAC_FAILURE;
    }
}

/****************************************************************************
 Function    :  PnacSnmpLowGetNextSuppCapValidPortNumber
 Input       :  i4Dot1xPaePortNumber - Port number
 
 Output      :  This Routine Takes the argument pi4Dot1xPaePortNumber &
                stores the next Valid Supplicant Capable Port Number 
                that occurs next in the PaePortEntry Table.
 Returns     :  PNAC_SUCCESS or PNAC_FAILURE
****************************************************************************/
INT4
PnacSnmpLowGetNextSuppCapValidPortNumber (INT4 i4Dot1xPaePortNumber,
                                          INT4 *pi4Dot1xPaePortNumber)
{
    INT4                i4Count;
    tPnacPaePortEntry  *pPortInfo = NULL;
    for (i4Count = i4Dot1xPaePortNumber + 1; i4Count <= PNAC_MAXPORTS;
         i4Count++)

    {
        if (i4Count < 0)
        {
            return PNAC_FAILURE;
        }

        if (PnacGetPortEntry ((UINT2) i4Count, &pPortInfo) == PNAC_SUCCESS)

        {
            if (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))

            {
                *pi4Dot1xPaePortNumber = i4Count;
                return PNAC_SUCCESS;
            }
        }
    }
    PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: No other Supp Capable Port\n");
    return PNAC_FAILURE;
}

/****************************************************************************
 Function    :  PnacSnmpLowGetNextPortBasedValidPortNumber
 Input       :  i4Dot1xPaePortNumber - Port number
 
 Output      :  This Routine Takes the argument pi4Dot1xPaePortNumber &
                stores the next Valid Port Number that occurs next in the 
        PaePortEntry Table, configuread as port based.
 Returns     :  PNAC_SUCCESS or PNAC_FAILURE
****************************************************************************/
INT4
PnacSnmpLowGetNextPortBasedValidPortNumber (INT4 i4Dot1xPaePortNumber,
                                            INT4 *pi4Dot1xPaePortNumber)
{
    INT4                i4Count;
    tPnacPaePortEntry  *pPortInfo = NULL;
    for (i4Count = i4Dot1xPaePortNumber + 1; i4Count <= PNAC_MAXPORTS;
         i4Count++)

    {
        if (i4Count < 0)
        {
            return PNAC_FAILURE;
        }

        if (PnacGetPortEntry ((UINT2) i4Count, &pPortInfo) == PNAC_SUCCESS)

        {
            if (PNAC_PORT_INFO (i4Count).u1PortAuthMode ==
                PNAC_PORT_AUTHMODE_PORTBASED)

            {
                *pi4Dot1xPaePortNumber = i4Count;
                return PNAC_SUCCESS;
            }
        }
    }
    PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: No other Port-Based Port\n");
    return PNAC_FAILURE;
}
