/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/*                                                                           */
/* $Id: pnacapi.c,v 1.27 2017/09/04 10:53:17 siva Exp $                      */
/*                                                                           */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : pnacapi.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : PNAC sub module                                */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Mar 2002                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains exported API of PNAC module.*/
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/

#include "pnachdrs.h"
#include "pnacport.h"

/*****************************************************************************/
/* Function Name      : PnacGetPnacEnableStatus                              */
/*                                                                           */
/* Description        : This function gets the PNAC Module Status - i.e      */
/*                      Whether it is enabled or disabled                    */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.u2SystemAuthControl                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - if enabled                            */
/*                      PNAC_FAILURE - if disabled                           */
/*****************************************************************************/
INT4
PnacGetPnacEnableStatus (VOID)
{
    if (gPnacSystemInfo.u2SystemAuthControl == PNAC_ENABLED)
    {
        return PNAC_SUCCESS;
    }
    return PNAC_FAILURE;
}                                /* PnacGetPnacEnableStatus */

/*****************************************************************************/
/* Function Name      : PnacCreatePort                                       */
/*                                                                           */
/* Description        : This function posts a message to  PNAC task to create*/
/*                       entries in PNAC global structure                    */
/*                      on creation of Bridge ports.                         */
/*                                                                           */
/* Input(s)           : u2PortNum - Interface index,                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : Port Entry Status in gPnacSystemInfo for the port.   */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : Port Interface Map in gPnacSystemInfo for the port.  */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PnacCreatePort (UINT2 u2PortNum)
{
    tPnacIntfMesg      *pMesg = NULL;
    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " INF: CreatePort Event When module is shutdown ... \n");
        return PNAC_FAILURE;
    }

    if ((u2PortNum < PNAC_MINPORTS) || (u2PortNum > PNAC_MAXPORTS))
    {
        /* Port Number is not a valid number */
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " CreatePort: Port Number is invalid \n");
        return PNAC_FAILURE;
    }

    pMesg =
        (tPnacIntfMesg *) PNAC_ALLOC_MEM_BLOCK (PNAC_INTF_CFGQ_MESG_MEMPOOL_ID);

    if (pMesg == NULL)
    {

        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " Allocate mem block for Pnac Configuration Message failed\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gPnacSystemInfo.i4SysLogId,
                      "Allocate mem block for Pnac Configuration Message failed"));

        return PNAC_FAILURE;
    }

    pMesg->u4MesgType = PNAC_CREATE_PORT;

    pMesg->Mesg.PortInfo.u2PortIndex = u2PortNum;

    if (PNAC_SEND_TO_QUEUE (PNAC_INTF_CFG_QID, (UINT1 *) &pMesg,
                            OSIX_DEF_MSG_LEN) == PNAC_OSIX_SUCCESS)
    {

        if (PNAC_SEND_EVENT (PNAC_INTF_TASK_ID,
                             PNAC_CFGQ_EVENT) == PNAC_OSIX_SUCCESS)
        {
            L2_SYNC_TAKE_SEM ();
            /* Success */
            return PNAC_SUCCESS;
        }
        else
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                      PNAC_CONTROL_PATH_TRC, " INF: Send Event failed !!!\n");

            /* Failure */
            return PNAC_FAILURE;
        }

    }

    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
              PNAC_CONTROL_PATH_TRC, " INF: Send To Q failed !!!\n");

    PNAC_RELEASE_MEM_BLOCK (PNAC_INTF_CFGQ_MESG_MEMPOOL_ID, pMesg);

    return PNAC_FAILURE;

}                                /* end PnacCreatePort() */

/*****************************************************************************/
/* Function Name      : PnacDeletePort                                       */
/*                                                                           */
/* Description        : This function delete entries in PNAC global data     */
/*                      structure on deletion of Bridge ports                */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : Port Entry Status in gPnacSystemInfo for the port.   */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : Port Entry Status in gPnacSystemInfo for the port.   */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PnacDeletePort (UINT2 u2PortNum)
{

    tPnacIntfMesg      *pMesg = NULL;

    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " INF: DeletePort Event When module is shutdown ... \n");
        return PNAC_FAILURE;
    }

    if ((u2PortNum < PNAC_MINPORTS) || (u2PortNum > PNAC_MAXPORTS))
    {
        /* Port Number is not a valid number */
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " DeletePort: Port Number is invalid \n");
        return PNAC_FAILURE;
    }

    pMesg =
        (tPnacIntfMesg *) PNAC_ALLOC_MEM_BLOCK (PNAC_INTF_CFGQ_MESG_MEMPOOL_ID);

    if (pMesg == NULL)
    {

        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " Allocate mem block for Pnac Configuration Msg failed\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gPnacSystemInfo.i4SysLogId,
                      " Allocate mem block for Pnac Configuration Msg failed"));
        return PNAC_FAILURE;
    }

    pMesg->u4MesgType = PNAC_DELETE_PORT;

    pMesg->Mesg.PortInfo.u2PortIndex = u2PortNum;

    if (PNAC_SEND_TO_QUEUE (PNAC_INTF_CFG_QID, (UINT1 *) &pMesg,
                            OSIX_DEF_MSG_LEN) == PNAC_OSIX_SUCCESS)
    {

        if (PNAC_SEND_EVENT (PNAC_INTF_TASK_ID, PNAC_CFGQ_EVENT)
            == PNAC_OSIX_SUCCESS)
        {

            /* Success */
            return PNAC_SUCCESS;
        }
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                  PNAC_CONTROL_PATH_TRC, " CFGQ: Send Event failed !!!\n");
        return PNAC_FAILURE;
    }

    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
              PNAC_CONTROL_PATH_TRC, " CFGQ: Send To Q failed !!!\n");

    PNAC_RELEASE_MEM_BLOCK (PNAC_INTF_CFGQ_MESG_MEMPOOL_ID, pMesg);

    return PNAC_FAILURE;

}                                /* PnacDeletePort() */

/*****************************************************************************/
/* Function Name      : PnacMapPort                                          */
/*                                                                           */
/* Description        : This function posts a message to  PNAC task to create*/
/*                       entries in PNAC global structure                    */
/*                      on creation of Bridge ports.                         */
/*                                                                           */
/* Input(s)           : u2PortNum - Interface index,                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : Port Entry Status in gPnacSystemInfo for the port.   */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : Port Interface Map in gPnacSystemInfo for the port.  */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PnacMapPort (UINT2 u2PortNum)
{
    tPnacIntfMesg      *pMesg = NULL;

    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " INF: CreatePort Event When module is shutdown ... \n");
        return PNAC_FAILURE;
    }

    if ((u2PortNum < PNAC_MINPORTS) || (u2PortNum > PNAC_MAXPORTS))
    {
        /* Port Number is not a valid number */
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " CreatePort: Port Number is invalid \n");
        return PNAC_FAILURE;
    }

    pMesg =
        (tPnacIntfMesg *) PNAC_ALLOC_MEM_BLOCK (PNAC_INTF_CFGQ_MESG_MEMPOOL_ID);

    if (pMesg == NULL)
    {

        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " Allocate mem block for Pnac Configuration Message failed\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gPnacSystemInfo.i4SysLogId,
                      " Allocate mem block for Pnac Configuration Message failed"));
        return PNAC_FAILURE;
    }

    pMesg->u4MesgType = PNAC_MAP_PORT;

    pMesg->Mesg.PortInfo.u2PortIndex = u2PortNum;

    if (PNAC_SEND_TO_QUEUE (PNAC_INTF_CFG_QID, (UINT1 *) &pMesg,
                            OSIX_DEF_MSG_LEN) == PNAC_OSIX_SUCCESS)
    {

        if (PNAC_SEND_EVENT (PNAC_INTF_TASK_ID,
                             PNAC_CFGQ_EVENT) == PNAC_OSIX_SUCCESS)
        {
            L2MI_SYNC_TAKE_SEM ();
            /* Success */
            return PNAC_SUCCESS;
        }
        else
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                      PNAC_CONTROL_PATH_TRC, " INF: Send Event failed !!!\n");

            /* Failure */
            return PNAC_FAILURE;
        }

    }

    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
              PNAC_CONTROL_PATH_TRC, " INF: Send To Q failed !!!\n");

    PNAC_RELEASE_MEM_BLOCK (PNAC_INTF_CFGQ_MESG_MEMPOOL_ID, pMesg);

    return PNAC_FAILURE;

}                                /* end PnacCreatePort() */

/*****************************************************************************/
/* Function Name      : PnacUnmapPort                                        */
/*                                                                           */
/* Description        : This function delete entries in PNAC global data     */
/*                      structure on unmapping of ports from a context       */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : Port Entry Status in gPnacSystemInfo for the port.   */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : Port Entry Status in gPnacSystemInfo for the port.   */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PnacUnmapPort (UINT2 u2PortNum)
{
    tPnacIntfMesg      *pMesg = NULL;

    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " INF: DeletePort Event When module is shutdown ... \n");
        return PNAC_FAILURE;
    }

    if ((u2PortNum < PNAC_MINPORTS) || (u2PortNum > PNAC_MAXPORTS))
    {
        /* Port Number is not a valid number */
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " DeletePort: Port Number is invalid \n");
        return PNAC_FAILURE;
    }

    pMesg =
        (tPnacIntfMesg *) PNAC_ALLOC_MEM_BLOCK (PNAC_INTF_CFGQ_MESG_MEMPOOL_ID);

    if (pMesg == NULL)
    {

        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " Allocate mem block for Pnac Configuration Msg failed\n");

        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gPnacSystemInfo.i4SysLogId,
                      " Allocate mem block for Pnac Configuration Msg failed"));
        return PNAC_FAILURE;
    }

    pMesg->u4MesgType = PNAC_UNMAP_PORT;

    pMesg->Mesg.PortInfo.u2PortIndex = u2PortNum;

    if (PNAC_SEND_TO_QUEUE (PNAC_INTF_CFG_QID, (UINT1 *) &pMesg,
                            OSIX_DEF_MSG_LEN) == PNAC_OSIX_SUCCESS)
    {
        if (PNAC_SEND_EVENT (PNAC_INTF_TASK_ID, PNAC_CFGQ_EVENT)
            == PNAC_OSIX_SUCCESS)
        {
            L2MI_SYNC_TAKE_SEM ();
            /* Success */
            return PNAC_SUCCESS;
        }

        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                  PNAC_CONTROL_PATH_TRC, " CFGQ: Send Event failed !!!\n");
        return PNAC_FAILURE;
    }

    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
              PNAC_CONTROL_PATH_TRC, " CFGQ: Send To Q failed !!!\n");

    PNAC_RELEASE_MEM_BLOCK (PNAC_INTF_CFGQ_MESG_MEMPOOL_ID, pMesg);

    return PNAC_FAILURE;

}                                /* PnacDeletePort() */

/*****************************************************************************/
/* Function Name      : PnacPortStatusUpdate                                 */
/*                                                                           */
/* Description        : Called when the port status is to be intimated to    */
/*                      the PNAC module about the operation status of the    */
/*                      port. This routine intimates the authenticator and   */
/*                      supplicant submodules about the operational status   */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index for which this status is */
/*                                  intimated                                */
/*                      u1OperStatus - The operational status of the         */
/*                                  interface, values can be PNAC_OPER_UP or */
/*                                  PNAC_OPER_DOWN.                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PnacPortStatusUpdate (UINT2 u2PortNum, UINT1 u1OperStatus)
{

    tPnacIntfMesg      *pMesg = NULL;

    PNAC_TRC_ARG2 (PNAC_CONTROL_PATH_TRC,
                   " Updating Port Operational Status (%u) for Port %u ... \n",
                   u1OperStatus, u2PortNum);

    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  "INF: Pnac is shutdown\n");
        return PNAC_FAILURE;
    }

    if ((u2PortNum < PNAC_MINPORTS) || (u2PortNum > PNAC_MAXPORTS))
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PAE: Port Number out of range Aborting operation ...\n");
        return PNAC_FAILURE;
    }

    pMesg =
        (tPnacIntfMesg *) PNAC_ALLOC_MEM_BLOCK (PNAC_INTF_CFGQ_MESG_MEMPOOL_ID);

    if (pMesg == NULL)
    {

        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " Allocate mem block for Pnac Interface Message failed\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gPnacSystemInfo.i4SysLogId,
                      " Allocate mem block for Pnac Interface Message failed"));
        return PNAC_FAILURE;
    }

    pMesg->u4MesgType = PNAC_UPDATE_PORT_STATUS;

    pMesg->Mesg.PortInfo.u2PortIndex = u2PortNum;

    pMesg->Mesg.PortInfo.u1OperStatus = u1OperStatus;

    if (PNAC_SEND_TO_QUEUE (PNAC_INTF_CFG_QID, (UINT1 *) &pMesg,
                            OSIX_DEF_MSG_LEN) == PNAC_OSIX_SUCCESS)
    {

        if (PNAC_SEND_EVENT (PNAC_INTF_TASK_ID, PNAC_CFGQ_EVENT)
            == PNAC_OSIX_SUCCESS)
        {
            /* Success */
            return PNAC_SUCCESS;
        }

        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                  PNAC_CONTROL_PATH_TRC, " CFGQ : Send Event failed !!!\n");
        return PNAC_FAILURE;
    }

    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
              PNAC_CONTROL_PATH_TRC, " CFGQ : Send To Q failed !!!\n");

    PNAC_RELEASE_MEM_BLOCK (PNAC_INTF_CFGQ_MESG_MEMPOOL_ID, pMesg);

    return PNAC_FAILURE;

}

/*****************************************************************************/
/* Function Name      : PnacHandleInFrame                                    */
/*                                                                           */
/* Description        : This function processes the incoming MAC frame,      */
/*                      received from CFA. In this function, incoming Frame's*/
/*                      frameType is  classified as PNAC_CTRL or PNAC_DATA.  */
/*                      After the frameType classification it handovers the  */
/*                      packet to proper function                            */
/*                                                                           */
/* Input(s)           : pMesg - pointer to CRU buffer chain containing       */
/*                              MAC frame,                                   */
/*                      u2IfIndex - Interface index,                         */
/*                      u2Protocol - Protocol of the data inside MAC frame   */
/*                      u1FrameType - Ethernet Frame type                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : NOME                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - if port is authorized to recv         */
/*                                     this frame                            */
/*                      PNAC_FAIlURE - The port is not authorized,           */
/*                                     discard the frame                     */
/*                      PNAC_IGNORE -  This is a control frame. Ignore       */
/*                                     the frame,dont discard.               */
/*****************************************************************************/

INT4
PnacHandleInFrame (tPnacCruBufChainHdr * pMesg, UINT2 u2IfIndex,
                   UINT1 u1FrameType)
{
    if ((PnacIsPnacCtrlFrame (pMesg)) == PNAC_TRUE)
    {
        return (PnacHandleReceivedFrame (pMesg, u2IfIndex, L2_PNAC_CTRL_FRAME));
    }
    else
    {
        return (PnacHandleReceivedFrame (pMesg, u2IfIndex, u1FrameType));
    }
}

/*****************************************************************************/
/* Function Name      : PnacHandleReceivedFrame                              */
/*                                                                           */
/* Description        : This API is used as Protocol Entry point function    */
/*                      that processes the incoming MAC frame, received from */
/*                      CFA. This function expects the frameType of the      */
/*                      received frame from the caller function, then        */
/*                      handovers the frame to appropriate function          */
/*                                                                           */
/* Input(s)           : pMesg - pointer to CRU buffer chain containing       */
/*                              MAC frame,                                   */
/*                      u2IfIndex - Interface index                          */
/*                      u1FrameType - Ethernet Frame type                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : NOME                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - if port is authorized to recv         */
/*                                     this frame                            */
/*                      PNAC_FAIlURE - The port is not authorized,           */
/*                                     discard the frame                     */
/*                      PNAC_IGNORE -  This is a control frame. Ignore       */
/*                                     the frame,dont discard.               */
/*****************************************************************************/
INT4
PnacHandleReceivedFrame (tPnacCruBufChainHdr * pMesg, UINT2 u2IfIndex,
                         UINT1 u1FrameType)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    if (((gPnacSystemInfo.u1SystemMode == PNAC_DISTRIBUTED) &&
         (DPnacIsConnectingPort ((UINT4) u2IfIndex) != PNAC_SUCCESS)) ||
        (gPnacSystemInfo.u1SystemMode != PNAC_DISTRIBUTED))
    {
        /* Get the Context Information before calling PnacWrHandleReceivedFrame ()
         * for backward compatibility */
        if (VcmGetContextInfoFromIfIndex ((UINT4) u2IfIndex, &u4ContextId,
                                          &u2LocalPortId) == VCM_FAILURE)
        {
            return PNAC_FAILURE;
        }
    }

    return (PnacWrHandleReceivedFrame
            (pMesg, u2IfIndex, u1FrameType, u4ContextId));
}

/*****************************************************************************/
/* Function Name      : PnacWrHandleReceivedFrame                            */
/*                                                                           */
/* Description        : This wrapper function processes incoming MAC frame,  */
/*                      received from CFA.This function expects the frameType*/
/*                      of the received frame from the caller function, then */
/*                      handovers the frame to appropriate function          */
/*                                                                           */
/* Input(s)           : pMesg - pointer to CRU buffer chain containing       */
/*                              MAC frame,                                   */
/*                      u2IfIndex - Interface index                          */
/*                      u1FrameType - Ethernet Frame type                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : NOME                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - if port is authorized to recv         */
/*                                     this frame                            */
/*                      PNAC_FAIlURE - The port is not authorized,           */
/*                                     discard the frame                     */
/*                      PNAC_IGNORE -  This is a control frame. Ignore       */
/*                                     the frame,dont discard.               */
/*****************************************************************************/
INT4
PnacWrHandleReceivedFrame (tPnacCruBufChainHdr * pMesg, UINT2 u2IfIndex,
                           UINT1 u1FrameType, UINT4 u4ContextId)
{
    if (PnacGetPnacEnableStatus () != PNAC_SUCCESS)
    {
        return PNAC_DATA;        /* FRAME_VALIDATED */
    }

    if (u1FrameType == L2_PNAC_CTRL_FRAME)
    {
        return (PnacHandleInCtrlFrame (pMesg, u2IfIndex, u1FrameType));
    }
    else
    {
        return (PnacHandleInDataFrame
                (pMesg, u2IfIndex, u1FrameType, u4ContextId));
    }
}

/*****************************************************************************/
/* Function Name      : PnacHandleOutFrame                                   */
/*                                                                           */
/* Description        : This function processes the outgoing MAC frame,      */
/*                      received from CFA. PNAC checks its database to see if*/
/*                      this port is authorized to transmit the frame or not */
/*                                                                           */
/* Input(s)           : pBuf - pointer to CRU buffer chain containing frame, */
/*                      u2IfIndex - Interface index,                         */
/*                      pu1DestMacAddr - Destination MAC Address of the frame*/
/*                      u4FrameSize- Length of the frame                     */
/*                      u2Protocol = Protocol Type                           */
/*                      u1EncapType - Encapsulation type                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : port address of the interface in gPnacSytemInfo,     */
/*                      port's authentication mode in gPnacSystemInfo,       */
/*                      port's authorization status in gPnacSystemInfo       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    :PNAC_SUCCESS - if port is authorized to Tx this frame */
/*****************************************************************************/
INT4
PnacHandleOutFrame (tPnacCruBufChainHdr * pMesg, UINT2 u2IfIndex,
                    UINT1 *pu1DestHwAddr, UINT4 u4FrameSize, UINT2 u2Protocol,
                    UINT1 u1EncapType)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tPnacAuthSessStats *pAuthSessStats = NULL;
    tPnacAuthFsmInfo   *pAuthFsmInfo = NULL;
    tMacAddr            destMacAddr;
    UINT1               au1TempBuf[PNAC_MAC_ADDR_SIZE];
    UINT1              *pu1ReadPtr = NULL;
    UINT1               u1PortStatus;

    PNAC_TRC_ARG1 (PNAC_DATA_PATH_TRC | PNAC_BUFFER_TRC,
                   " Handling Outgoing Frame for Port: %d !!!\n", u2IfIndex);

    PNAC_LOCK ();
    if (PnacGetPnacEnableStatus () != PNAC_SUCCESS)
    {
        /* 
         * When PNAC is disabled, the packet is also considered
         * to be a valid packet and has to be transmitted.
         */
        PNAC_UNLOCK ();
        return PNAC_SUCCESS;
    }

    if (PnacGetPortEntry (u2IfIndex, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_DATA_PATH_TRC,
                  " Frame received on INVALID status port\n");

        PNAC_UNLOCK ();
        return PNAC_FAILURE;
    }

    /* Send Pnac Control Frames thru Green channel */
    if (u2Protocol == PNAC_PAE_ENET_TYPE)
    {
        /* If the node is active then transmit pnac packet. */
        if (PNAC_NODE_STATUS () != PNAC_ACTIVE_NODE)
        {
            /* Node is not active. */
            PNAC_UNLOCK ();
            return PNAC_FAILURE;
        }
        /* Node is active. */
        PNAC_UNLOCK ();
        return PNAC_SUCCESS;
    }

    /* checking the port status */
    if (pPortInfo->u1EntryStatus == PNAC_PORT_OPER_DOWN)
    {
        /* drop the invalid Ethernet frame received */
        /* releasing CRU buffer */
        PNAC_TRC_ARG1 (PNAC_ALL_FAILURE_TRC | PNAC_DATA_PATH_TRC,
                       " HandleOutFrame: Port %u Operationally down\n",
                       u2IfIndex);
        PNAC_UNLOCK ();
        return PNAC_FAILURE;
    }

    /* 1. Check if Authenticator role is supported on this port */
    /* 2. Check if Dot1x is disabled on this port */
    if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo) ||
        (pPortInfo->u1PortPaeStatus == PNAC_DISABLED))
    {
        /*******************************************************************/
        /*  Authenticator role not supported on this port (or)             */
        /*  Dot1x is disabled on this port.                                */
        /*  so handover the frame to upper layer                           */
        /*******************************************************************/

        PNAC_UNLOCK ();
        return PNAC_SUCCESS;
    }

    switch (pPortInfo->u1PortAuthMode)
    {
        case PNAC_PORT_AUTHMODE_PORTBASED:

            /* Check whether the port is authorized or not. */
            if ((PnacGetHLPortStatus (u2IfIndex, &u1PortStatus) !=
                 PNAC_SUCCESS) || (u1PortStatus != CFA_IF_UP))
            {
                PNAC_TRC_ARG1 (PNAC_ALL_FAILURE_TRC |
                               PNAC_CONTROL_PATH_TRC,
                               " PAE: supplicant port %u not authorized\n",
                               u2IfIndex);
                PNAC_UNLOCK ();
                return PNAC_FAILURE;
            }

            if (pPortInfo->pPortAuthSessionNode == NULL)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_DATA_PATH_TRC,
                          "No Auth Session Node found\n");

                PNAC_UNLOCK ();
                return PNAC_FAILURE;
            }

            pAuthSessNode = pPortInfo->pPortAuthSessionNode;
            pAuthFsmInfo = &(pAuthSessNode->authFsmInfo);
            if (pAuthFsmInfo->u2AuthControlPortStatus
                == PNAC_PORTSTATUS_AUTHORIZED)
            {
                pAuthSessStats = &(pAuthSessNode->authSessStats);
                (pAuthSessStats->u4SessFramesTx)++;
                PNAC_UINT8_ADD (pAuthSessStats->u8SessOctetsTx, u4FrameSize);
            }
            break;

        case PNAC_PORT_AUTHMODE_MACBASED:
            /**********************************************/
            /* If encapsulation type is none get the      */
            /* destination MAC addr from the CRU buffer,  */
            /* else get from the destination              */
            /* hardware address argument.                 */
            /**********************************************/
            if (u1EncapType == PNAC_ENCAP_NONE)

            {
                PNAC_MEMSET (au1TempBuf, PNAC_INIT_VAL, PNAC_MAC_ADDR_SIZE);
                if (PNAC_COPY_FROM_CRU_BUF
                    (pMesg, au1TempBuf, PNAC_OFFSET_NONE,
                     PNAC_MAC_ADDR_SIZE) == PNAC_CRU_FAILURE)

                {
                    PNAC_TRC (PNAC_BUFFER_TRC | PNAC_ALL_FAILURE_TRC |
                              PNAC_DATA_PATH_TRC,
                              " PAE: Copy from buf failed\n");
                    PNAC_UNLOCK ();
                    return PNAC_FAILURE;
                }
                pu1ReadPtr = au1TempBuf;
            }

            else

            {
                pu1ReadPtr = pu1DestHwAddr;
            }

            /* read the Destination MAC address */
            PNAC_MEMCPY (destMacAddr, pu1ReadPtr, PNAC_MAC_ADDR_SIZE);
            pu1ReadPtr += PNAC_MAC_ADDR_SIZE;
            if (PNAC_IS_BROADCAST_ADDR (destMacAddr)
                || PNAC_IS_MULTICAST_ADDR (destMacAddr[0]))

            {

                /*****************************************/
                /* Since the dest address is Broadcast   */
                /* Multicast address, if                 */
                /* atleast one supplicant is authorized  */
                /* in the port, then                     */
                /* forward the packet out, else drop the */
                /* frame.                                */
                /*****************************************/
                if ((pPortInfo->u2SuppCount >= PNAC_MINSUPP) ||
                    (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).
                     u2AuthControlPortControl ==
                     PNAC_PORTCNTRL_FORCEAUTHORIZED))
                {
                    PNAC_UNLOCK ();
                    return PNAC_SUCCESS;
                }

                else

                {
                    PNAC_TRC_ARG1 (PNAC_ALL_FAILURE_TRC |
                                   PNAC_DATA_PATH_TRC,
                                   " PAE: Port %u not authorized since no "
                                   " supplicant is authorized\n", u2IfIndex);
                    PNAC_UNLOCK ();
                    return PNAC_FAILURE;
                }                /* End else SuppCount >= PNAC_MINSUPP */
            }                    /* End if dest address B'cast or M'cast ends */

            /**********************************************/
            /* The packet is not a broadcast or multicast */
            /* destination mac addressed  packet          */
            /**********************************************/
            if (PnacAuthGetSessionTblEntryByMac
                (destMacAddr, &pAuthSessNode) != PNAC_SUCCESS)

            {

                /**************************************************/
                /* The destination MAC address is not authorized. */
                /**************************************************/
                PNAC_TRC_ARG1 (PNAC_ALL_FAILURE_TRC | PNAC_DATA_PATH_TRC,
                               "PAE: Supplicant port %u not authorized \n",
                               u2IfIndex);
                PNAC_UNLOCK ();
                return PNAC_FAILURE;
            }
            pAuthSessStats = &(pAuthSessNode->authSessStats);
            (pAuthSessStats->u4SessFramesTx)++;
            PNAC_UINT8_ADD (pAuthSessStats->u8SessOctetsTx, u4FrameSize);

            /***************************************************/
            /* The MAC addr is authorized the control switches */
            /* to end of this function where the packet is     */
            /* forwarded to lower layer                        */
            /***************************************************/
            break;                /* end case MACBASED */

        default:
            break;
    }

   /*****************************************************************/
    /* The Port or MAC address is authorized - Hence forward the     */
    /* frame to the lower layer                                      */
   /*****************************************************************/
    PNAC_UNLOCK ();
    return PNAC_SUCCESS;
}                                /* PnacHandleOutFrame() */

/*****************************************************************************/
/* Function           : PnacGetLocAuthServPassword                           */
/*                                                                           */
/* Description        : This function is used to get the password that is    */
/*                      configured for the User in the Local Authentication  */
/*                      Server.  This function is called from the MSR.       */
/*                                                                           */
/* Input              : The UserName for the entry the password is requested.*/
/*                                                                           */
/* Output             : Returns the Password configured for the User.        */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Returns            : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacGetLocAuthServPassword (UINT1 *pu1UserName, UINT1 *pu1RetValPassword)
{
    tPnacServUserInfo  *pUserNode = NULL;
    tSNMP_OCTET_STRING_TYPE Name;
    UINT1               au1TempBuf[PNAC_USER_NAME_SIZE];

    Name.i4_Length = STRLEN (pu1UserName);

    if ((Name.i4_Length < 1) || (Name.i4_Length > PNAC_USER_NAME_SIZE))
    {
        return PNAC_FAILURE;
    }

    PNAC_MEMSET (au1TempBuf, PNAC_INIT_VAL, PNAC_USER_NAME_SIZE);
    Name.pu1_OctetList = au1TempBuf;

    if (Name.pu1_OctetList == NULL)
    {
        return PNAC_FAILURE;
    }
    PNAC_MEMCPY (Name.pu1_OctetList, pu1UserName, Name.i4_Length);
    PNAC_LOCK ();
    if (PnacSnmpLowServGetNodeByName (&Name, &pUserNode) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " User node could not be obtained! \n");
        PNAC_MEMSET (au1TempBuf, PNAC_INIT_VAL, PNAC_USER_NAME_SIZE);
        PNAC_UNLOCK ();
        return PNAC_FAILURE;
    }

    PNAC_UNLOCK ();

    if (pUserNode == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " User node could not be obtained! \n");
        PNAC_MEMSET (au1TempBuf, PNAC_INIT_VAL, PNAC_USER_NAME_SIZE);
        return PNAC_FAILURE;
    }

    PNAC_STRNCPY (pu1RetValPassword, pUserNode->au1Passwd,
                  STRLEN (pUserNode->au1Passwd));
    pu1RetValPassword[STRLEN (pUserNode->au1Passwd)] = '\0';
    FsUtlDecryptPasswd ((CHR1 *) pu1RetValPassword);
    PNAC_MEMSET (au1TempBuf, PNAC_INIT_VAL, PNAC_USER_NAME_SIZE);

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PnacGetStartedStatus                             */
/*                                                                           */
/*    Description         : This function returns whether Pnac is started or */
/*                          shutdown                                         */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gPnacSystemInfo.u2SystemControl            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PNAC_TRUE or PNAC_FALSE                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
PnacGetStartedStatus ()
{

    if (PNAC_SYSTEM_CONTROL == PNAC_START)
    {
        return PNAC_TRUE;
    }
    else
    {
        return PNAC_FALSE;
    }
}

/*****************************************************************************/
/* Function Name      : PnacHandleEvent                                      */
/*                                                                           */
/* Description        : This function can be called by external modules to   */
/*                      pass any event to PNAC module.                       */
/*                      Currently this fucntion is called by Bridge Detection*/
/*                      State Machine(RSTP/MSTP), whenever edge port         */
/*                      parameter changes value.                             */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number.                             */
/*                      u4Event   - Event Passed                             */
/*                      1) PNAC_BRIDGE_DETECTED - Edge Port is FALSE         */
/*                      2) PNAC_BRIDGE_NOT_DETECTED - Edge Port is TRUE      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
PnacHandleEvent (UINT2 u2PortNum, UINT4 u4Event)
{
    tPnacIntfMesg      *pMesg = NULL;

    if ((PNAC_IS_SHUTDOWN ()) || (!PNAC_IS_ENABLED ()))
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  "INF: PnacHandleEvent called when PNAC Module is not enabled.\n");
        return PNAC_SUCCESS;
    }

    pMesg = (tPnacIntfMesg *) PNAC_ALLOC_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID);

    if (pMesg == NULL)
    {

        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " Allocate mem block for Pnac Interface Message failed\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gPnacSystemInfo.i4SysLogId,
                      " Allocate mem block for Pnac Interface Message failed"));
        return PNAC_FAILURE;
    }
    pMesg->u4MesgType = PNAC_CDSM;

    pMesg->Mesg.PnacCdsm.u4Event = u4Event;
    pMesg->Mesg.PnacCdsm.u2PortIndex = u2PortNum;

    if (PNAC_SEND_TO_QUEUE (PNAC_INTF_INPUT_QID, (UINT1 *) &pMesg,
                            OSIX_DEF_MSG_LEN) == PNAC_OSIX_SUCCESS)
    {

        if (PNAC_INTF_TASK_ID == PNAC_INIT_VAL)
        {
            if (PNAC_GET_TASK_ID (SELF, PNAC_INTF_TASK_NAME,
                                  &(PNAC_INTF_TASK_ID)) != PNAC_OSIX_SUCCESS)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                          PNAC_CONTROL_PATH_TRC,
                          " INF: Get Task Id failed !!!\n");
                return PNAC_FAILURE;
            }
        }

        if (PNAC_SEND_EVENT (PNAC_INTF_TASK_ID, PNAC_QUEUE_EVENT)
            == PNAC_OSIX_SUCCESS)
        {

            /* Success */
            return PNAC_SUCCESS;
        }

        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                  PNAC_CONTROL_PATH_TRC, " INF: Send Event failed !!!\n");
        return PNAC_FAILURE;
    }

    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
              PNAC_CONTROL_PATH_TRC, " INF: Send To Q failed !!!\n");

    PNAC_RELEASE_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID, pMesg);

    return PNAC_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PnacLock                                             */
/*                                                                           */
/* Description        : This function is used to take the PNAC mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/*****************************************************************************/
INT4
PnacLock (VOID)
{
    if (OsixSemTake (gPnacSemId) != OSIX_SUCCESS)
    {
        PNAC_TRC_ARG1 (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC,
                       "TakeSem failure for %s \n", PNAC_SEM_NAME);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacUnLock                                           */
/*                                                                           */
/* Description        : This function is used to give the PNAC mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/*****************************************************************************/
INT4
PnacUnLock (VOID)
{
    OsixSemGive (gPnacSemId);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacModuleStart                                      */
/*                                                                           */
/* Description        : This function is an API to start the PNAC module     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

INT4
PnacModuleStart (VOID)
{
    PNAC_LOCK ();
    if (PnacHandleModuleStart () == PNAC_FAILURE)
    {
        PNAC_UNLOCK ();
        return PNAC_FAILURE;
    }
    PNAC_UNLOCK ();
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacModuleShutDown                                   */
/*                                                                           */
/* Description        : This function is an API to shutdown                  */
/*                      the PNAC module.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
PnacModuleShutDown (VOID)
{
    PNAC_LOCK ();
    PnacHandleModuleShutDown ();
    PNAC_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : PnacIsPnacCtrlFrame                                  */
/*                                                                           */
/* Description        : This function checks whether the given frame is a    */
/*                      PNAC control frame.                                  */
/*                      Whether it is enabled or disabled                    */
/* Input(s)           : pFrame - Incoming fram                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_TRUE                                            */
/*                      PNAC_FALASE                                          */
/*****************************************************************************/
INT4
PnacIsPnacCtrlFrame (tCRU_BUF_CHAIN_DESC * pMesg)
{
    if (L2IwfIsPnacCtrlFrame (pMesg) == L2IWF_TRUE)
    {
        return PNAC_TRUE;
    }

    return PNAC_FALSE;
}

/*****************************************************************************/
/* Function Name      : PnacHandleMacSession                                 */
/*                                                                           */
/* Description        : This function posts different types of messages to   */
/*                      modify the mac session identified by given port      */
/*                      number and mac address. This function will be called */
/*                      by WLAN module.                                      */
/*                                                                           */
/* Input(s)           : u2PortIndex - Port Index.                            */
/*                      SrcMacAddr  - Source mac of Wlan mac session.        */
/*                      u2MsgType   - Type of message to be enqueued.        */
/*                                     - PNAC_CREATE_AUTHORIZE_SESSION       */
/*                                     - PNAC_RESTART_MAC_SESSION            */
/*                                     - PNAC_DEAUTHORIZE_MAC_SESSION        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu2PnacSystemControl                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacHandleMacSession (UINT2 u2PortIndex, tMacAddr SrcMacAddr, UINT2 u2MsgType)
{
    tPnacIntfMesg      *pMesg = NULL;

    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PAE: HandleMacSession called When module is "
                  "shutdown ... \n");
        return;
    }

    if ((u2PortIndex < PNAC_MINPORTS) || (u2PortIndex > PNAC_MAXPORTS))
    {
        /* Port Number is not a valid number */
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " HandleMacSession: Port Number is invalid \n");
        return;
    }

    pMesg = (tPnacIntfMesg *) PNAC_ALLOC_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID);

    if (pMesg == NULL)
    {

        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " Allocate mem block for Pnac Interface Message failed\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gPnacSystemInfo.i4SysLogId,
                      " Allocate mem block for Pnac Interface Message failed"));
        return;
    }

    pMesg->Mesg.MacSessionFrame.u2PortIndex = u2PortIndex;
    MEMCPY (pMesg->Mesg.MacSessionFrame.MacAddr, SrcMacAddr, sizeof (tMacAddr));
    pMesg->u4MesgType = u2MsgType;

    if (PNAC_SEND_TO_QUEUE (PNAC_INTF_INPUT_QID, (UINT1 *) &pMesg,
                            OSIX_DEF_MSG_LEN) == PNAC_OSIX_SUCCESS)
    {

        if (PNAC_SEND_EVENT (PNAC_INTF_TASK_ID, PNAC_QUEUE_EVENT)
            == PNAC_OSIX_SUCCESS)
        {
            /* Success */
            return;
        }
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                  PNAC_CONTROL_PATH_TRC, " PAE: Send Event failed !!!\n");
        return;
    }

    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
              PNAC_CONTROL_PATH_TRC, " PAE: Send To Q failed !!!\n");

    PNAC_RELEASE_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID, pMesg);

    return;
}

/*****************************************************************************/
/* Function Name      : PnacSetPnacPortAuthMode                              */
/*                                                                           */
/* Description        : This function posts a message to  PNAC task to set   */
/*                      auth mode for the given port.                        */
/*                                                                           */
/* Input(s)           : u2Port - Interface index,                            */
/*                    : u2AuthMode - Auth Mode to be changed.                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSetPnacPortAuthMode (UINT2 u2PortNum, UINT2 u2AuthMode)
{
    tPnacIntfMesg      *pMesg = NULL;

    if ((PNAC_IS_SHUTDOWN ()) || (!PNAC_IS_ENABLED ()))
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  "PAE: PnacSetPnacPortAuthMode called when PNAC"
                  "Module is not enabled\n");
        return PNAC_SUCCESS;
    }

    pMesg = (tPnacIntfMesg *) PNAC_ALLOC_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID);

    if (pMesg == NULL)
    {

        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PAE: PnacSetPnacPortAuthMode Allocate mem block for"
                  "Pnac Interface Message failed\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gPnacSystemInfo.i4SysLogId,
                      "PAE: PnacSetPnacPortAuthMode Allocate mem block for Pnac Interface Message failed"));
        return PNAC_FAILURE;
    }
    pMesg->u4MesgType = PNAC_AUTH_MODE_CHG;

    pMesg->Mesg.PortInfo.u2PortIndex = u2PortNum;
    pMesg->Mesg.PortInfo.u2AuthMode = u2AuthMode;

    if (PNAC_SEND_TO_QUEUE (PNAC_INTF_INPUT_QID, (UINT1 *) &pMesg,
                            OSIX_DEF_MSG_LEN) == PNAC_OSIX_SUCCESS)
    {

        if (PNAC_SEND_EVENT (PNAC_INTF_TASK_ID, PNAC_CFGQ_EVENT)
            == PNAC_OSIX_SUCCESS)
        {

            /* Success */
            return PNAC_SUCCESS;
        }

        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                  PNAC_CONTROL_PATH_TRC, " PAE: Send Event failed !!!\n");
        return PNAC_FAILURE;
    }

    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
              PNAC_CONTROL_PATH_TRC, " PAE: Send To Q failed !!!\n");

    PNAC_RELEASE_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID, pMesg);

    return PNAC_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PnacSetPnacPortAuthControl                           */
/*                                                                           */
/* Description        : This function posts a message to  PNAC task to set   */
/*                      auth control for the given port.                     */
/*                                                                           */
/* Input(s)           : u2Port - Interface index,                            */
/*                    : u2AuthControl - Auth Control to be changed.          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PnacSetPnacPortAuthControl (UINT2 u2Port, UINT2 u2AuthControl)
{
    tPnacIntfMesg      *pMesg = NULL;

    if ((PNAC_IS_SHUTDOWN ()) || (!PNAC_IS_ENABLED ()))
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  "PAE: PnacSetPnacPortAuthControl called when PNAC Module"
                  "is not enabled\n");
        return PNAC_SUCCESS;
    }

    pMesg = (tPnacIntfMesg *) PNAC_ALLOC_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID);

    if (pMesg == NULL)
    {

        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PAE: PnacSetPnacPortAuthControl Allocate mem block"
                  "for Pnac Interface Message failed\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gPnacSystemInfo.i4SysLogId,
                      "PAE: PnacSetPnacPortAuthControl Allocate mem block for Pnac Interface Message failed"));
        return PNAC_FAILURE;
    }
    pMesg->u4MesgType = PNAC_AUTH_CTRL_CHG;

    pMesg->Mesg.PortInfo.u2PortIndex = u2Port;
    pMesg->Mesg.PortInfo.u2AuthControl = u2AuthControl;

    if (PNAC_SEND_TO_QUEUE (PNAC_INTF_INPUT_QID, (UINT1 *) &pMesg,
                            OSIX_DEF_MSG_LEN) == PNAC_OSIX_SUCCESS)
    {

        if (PNAC_SEND_EVENT (PNAC_INTF_TASK_ID, PNAC_CFGQ_EVENT)
            == PNAC_OSIX_SUCCESS)
        {

            /* Success */
            return PNAC_SUCCESS;
        }

        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                  PNAC_CONTROL_PATH_TRC, " PAE: PnacSetPnacPortAuthControl"
                  "Send Event failed !!!\n");
        return PNAC_FAILURE;
    }

    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
              PNAC_CONTROL_PATH_TRC, " PAE: PnacSetPnacPortAuthControl"
              "Send To Q failed !!!\n");

    PNAC_RELEASE_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID, pMesg);

    return PNAC_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PnacHandleRadResponse                                */
/*                                                                           */
/* Description        : This function is called when Radius Client receives  */
/*                      a response from the Radius server. It sends an event */
/*                      to the PNAC Interface Task for the Response to get   */
/*                      processed by the PNAC module.                        */
/*                                                                           */
/* Input(s)           : pRadResp - Pointer to the Response data received.    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacHandleRadResponse (VOID *pRadRespRcvd)
{
    tRadInterface      *pRadResp = NULL;
    tPnacAsIfRecvData  *pServEapData = NULL;
    UINT1              *pu1EapPtr = NULL;
    UINT1               u1Val = 0;
    UINT2               u2Val = 0;
    UINT4               u4MacAddr = 0;
    UINT2               u2MacAddr = 0;
    INT4                i4RetVal = PNAC_TRUE;

    if (!PNAC_IS_ENABLED ())
    {
        PnacFreeMemForInterface ((tRadInterface *) pRadRespRcvd);
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " INF: Radius Response Received, When module is disabled\n");
        return;
    }
    pRadResp = (tRadInterface *) pRadRespRcvd;

    if (PNAC_ALLOCATE_IF_RECV_DATA_MEMBLK (pServEapData) == NULL)
    {
        PnacFreeMemForInterface ((tRadInterface *) pRadRespRcvd);
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Unable to allocate memory. \r\n");
        return;
    }
    PNAC_MEMSET (pServEapData, PNAC_INIT_VAL, sizeof (tPnacAsIfRecvData));
    if ((pRadResp->Session_Timeout != (UINT4) (-1)) &&
        (pRadResp->Session_Timeout != 0))
    {
        pServEapData->u4SessionTimeout = pRadResp->Session_Timeout;
    }
    pServEapData->u2NasPortNumber = (UINT2) pRadResp->ifIndex;
    pu1EapPtr = pServEapData->au1EapPkt;
    switch (pRadResp->Access)
    {
        case ACCESS_ACCEPT:
            if (gPnacRadReq[pRadResp->RequestId].u1Status != PNAC_TRUE)
            {
                PnacFreeMemForInterface ((tRadInterface *) pRadRespRcvd);
                i4RetVal = PNAC_FALSE;
                break;
            }
            u1Val = PNAC_EAP_CODE_SUCCESS;
            PNAC_PUT_1BYTE (pu1EapPtr, u1Val);
            u1Val = pRadResp->au1EapPkt[PNAC_EAP_CODE_SIZE];
            PNAC_PUT_1BYTE (pu1EapPtr, u1Val);
            u2Val = PNAC_EAP_HDR_SIZE;
            PNAC_PUT_2BYTE (pu1EapPtr, u2Val);
            pServEapData->u2EapPktLength = PNAC_EAP_HDR_SIZE;
#ifdef RSNA_WANTED
            /* MPPE Keys are used to generate PMK for AP and STA */
            if (pRadResp->MsMppeKeys.pu1MppeRecvKey != NULL)
            {
                UINT2               u2KeyLen = 0;

                u2KeyLen = pRadResp->MsMppeKeys.u2MppeRecvKeyLen;

                if (u2KeyLen > PNAC_MAX_SERVER_KEY_SIZE)
                {
                    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                              " INF: MS-MPPE-Recv-Key length invalid!!!\n");
                }
                else
                {
                    /* Update MS-MPPE-Recv-Key value */
                    MEMCPY (pServEapData->au1ServerKey,
                            pRadResp->MsMppeKeys.pu1MppeRecvKey, u2KeyLen);

                    /* Update MS-MPPE-Recv-Key length */
                    pServEapData->u2ServerKeyLength = u2KeyLen;
                }

                UtilRadKeyReleaseBufferSize (pRadResp->MsMppeKeys.
                                             pu1MppeRecvKey);
            }
#endif
            break;
        case ACCESS_REJECT:
            if (gPnacRadReq[pRadResp->RequestId].u1Status != PNAC_TRUE)
            {
                PnacFreeMemForInterface ((tRadInterface *) pRadRespRcvd);
                i4RetVal = PNAC_FALSE;
                break;
            }
            u1Val = PNAC_EAP_CODE_FAILURE;
            PNAC_PUT_1BYTE (pu1EapPtr, u1Val);
            u1Val = pRadResp->au1EapPkt[PNAC_EAP_CODE_SIZE];
            PNAC_PUT_1BYTE (pu1EapPtr, u1Val);
            u2Val = PNAC_EAP_HDR_SIZE;
            PNAC_PUT_2BYTE (pu1EapPtr, u2Val);
            pServEapData->u2EapPktLength = PNAC_EAP_HDR_SIZE;

            break;

        case ACCESS_CHALLENGE:
            if (gPnacRadReq[pRadResp->RequestId].u1Status != PNAC_TRUE)
            {
                PnacFreeMemForInterface ((tRadInterface *) pRadRespRcvd);
                i4RetVal = PNAC_FALSE;
                break;
            }
            MEMCPY (&(pServEapData->u2EapPktLength),
                    &pRadResp->au1EapPkt[EAP_CODE_FIELD_LEN +
                                         EAP_ID_FIELD_LEN], 2);

            pServEapData->u2EapPktLength =
                OSIX_NTOHS (pServEapData->u2EapPktLength);

            MEMCPY (pu1EapPtr, pRadResp->au1EapPkt,
                    pServEapData->u2EapPktLength);

            pServEapData->u2StateLen = (UINT2) (pRadResp->u1StateLen);
            PNAC_MEMCPY (pServEapData->au1State, pRadResp->State,
                         pServEapData->u2StateLen);

            break;
        case RAD_TIMEOUT:
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      " INF: Access Request to Server Timed Out !!!\n");
            gPnacRadReq[pRadResp->RequestId].u1Status = PNAC_FALSE;
            PnacFreeMemForInterface ((tRadInterface *) pRadRespRcvd);
            i4RetVal = PNAC_FALSE;
            break;
        default:
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      " INF: Unknown Packet Type from Server !!!\n");
            PnacFreeMemForInterface ((tRadInterface *) pRadRespRcvd);
            i4RetVal = PNAC_FALSE;
            break;
    }

    if (i4RetVal == PNAC_TRUE)
    {
        PNAC_MEMCPY (pServEapData->macAddr, gPnacRadReq[pRadResp->RequestId].
                     macAddr, PNAC_MAC_ADDR_SIZE);
        gPnacRadReq[pRadResp->RequestId].u1Status = PNAC_FALSE;

        PNAC_GET_FOURBYTE (u4MacAddr, pServEapData->macAddr);
        PNAC_GET_TWOBYTE (u2MacAddr, &(pServEapData->macAddr[4]));
        PNAC_TRC_ARG3 (PNAC_CONTROL_PATH_TRC,
                       " INF: Reply Mesg From Server : %s For MAC: %x-%x",
                       pRadResp->Reply_Message, u4MacAddr, u2MacAddr);
        /* Done with this -- free it */
        PnacFreeMemForInterface ((tRadInterface *) pRadRespRcvd);

        PnacEnQRadRespToIntfTask (pServEapData);
    }
    if (PNAC_RELEASE_IF_RECV_DATA_MEMBLK (pServEapData) == MEM_FAILURE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Failed to release  memory. \r\n");
    }
    return;
}

/*****************************************************************************/
/* Function Name      : PnacL2IwfProcPortStatUpdEvent                        */
/*                                                                           */
/* Description        : API Called from l2Iwf during link status change      */
/* Input(s)           : u2IfIndex - Interface index for which this status is */
/*                                  intimated                                */
/*                      u1OperStatus - Operational status of the port        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PnacL2IwfProcPortStatUpdEvent (UINT2 u2PortNum, UINT1 u1OperStatus)
{
    PNAC_LOCK ();

    if (PnacProcPortStatUpdEvent (u2PortNum, u1OperStatus) == PNAC_FAILURE)
    {
        PNAC_UNLOCK ();
        return PNAC_FAILURE;
    }

    PNAC_UNLOCK ();
    return PNAC_SUCCESS;
}

#ifdef TACACS_WANTED
/*****************************************************************************/
/* Function Name      : PnacHandleTacacsPlusResponse                         */
/*                                                                           */
/* Description        : This function is called when Tacacs+ Client receives */
/*                      a response from the Tacacs+ server.                  */
/*                                                                           */
/* Input(s)           : pTacRespRcvd - Pointer to the Response data received.*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - if authenticated                      */
/*                      PNAC_FAILURE - if fail to authneticate               */
/*****************************************************************************/
VOID
PnacHandleTacacsPlusResponse (VOID *pTacRespRcvd)
{
    tTacMsgOutput      *pTacResp = NULL;
    tTacAuthenOutput   *pTacAuthenOutput;
    tPnacAsIfRecvData  *pServEapData = NULL;
    UINT4               u4AppReqId;
    UINT1              *pu1EapPtr = NULL;
    UINT2               u2Val = 0;
    UINT1               u1Val = 0;

    if (!PNAC_IS_ENABLED ())
    {
        UtlShMemFreeTacMsgOutput ((UINT1 *) pTacRespRcvd);
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " INF: Tacacsplus Response Received, When module is disabled\n");
        return;
    }
    if (PNAC_ALLOCATE_IF_RECV_DATA_MEMBLK (pServEapData) == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Unable to allocate memory. \r\n");
        return;
    }

    pTacResp = (tTacMsgOutput *) pTacRespRcvd;

    pTacAuthenOutput = &(pTacResp->TacOutput.AuthenOutput);

    u4AppReqId = pTacAuthenOutput->u4AppReqId;

    PNAC_MEMSET (pServEapData, PNAC_INIT_VAL, sizeof (tPnacAsIfRecvData));

    pServEapData->u4SessionTimeout = PNAC_AS_CACHE_TIMEOUT;

    pServEapData->u2NasPortNumber = gPnacTacReq[u4AppReqId].u2NasPortNumber;

    pu1EapPtr = pServEapData->au1EapPkt;

    switch (pTacAuthenOutput->i4AuthenStatus)
    {
        case TAC_AUTHEN_STATUS_PASS:
            if (gPnacTacReq[u4AppReqId].u1Status != PNAC_TRUE)
            {
                UtlShMemFreeTacMsgOutput ((UINT1 *) pTacRespRcvd);
                if (PNAC_RELEASE_IF_RECV_DATA_MEMBLK (pServEapData)
                    == MEM_FAILURE)
                {
                    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                              "Failed to release  memory. \r\n");
                }
                return;
            }
            u1Val = PNAC_EAP_CODE_SUCCESS;
            PNAC_PUT_1BYTE (pu1EapPtr, u1Val);
            u1Val = (UINT1) (u4AppReqId + 1);
            PNAC_PUT_1BYTE (pu1EapPtr, u1Val);
            u2Val = PNAC_EAP_HDR_SIZE;
            PNAC_PUT_2BYTE (pu1EapPtr, u2Val);
            pServEapData->u2EapPktLength = PNAC_EAP_HDR_SIZE;
            break;
        case TAC_AUTHEN_STATUS_FAIL:
            if (gPnacTacReq[u4AppReqId].u1Status != PNAC_TRUE)
            {
                UtlShMemFreeTacMsgOutput ((UINT1 *) pTacRespRcvd);
                if (PNAC_RELEASE_IF_RECV_DATA_MEMBLK (pServEapData)
                    == MEM_FAILURE)
                {
                    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                              "Failed to release  memory. \r\n");
                }
                return;
            }
            u1Val = PNAC_EAP_CODE_FAILURE;
            PNAC_PUT_1BYTE (pu1EapPtr, u1Val);
            u1Val = (UINT1) (u4AppReqId + 1);
            PNAC_PUT_1BYTE (pu1EapPtr, u1Val);
            u2Val = PNAC_EAP_HDR_SIZE;
            PNAC_PUT_2BYTE (pu1EapPtr, u2Val);
            pServEapData->u2EapPktLength = PNAC_EAP_HDR_SIZE;
            break;
        default:
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      " INF: Unknown Packet Type from Server !!!\n");
            gPnacTacReq[u4AppReqId].u1Status = PNAC_FALSE;
            UtlShMemFreeTacMsgOutput ((UINT1 *) pTacRespRcvd);
            if (PNAC_RELEASE_IF_RECV_DATA_MEMBLK (pServEapData) == MEM_FAILURE)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                          "Failed to release  memory. \r\n");
            }
            return;
            break;
    }

    PNAC_MEMCPY (pServEapData->macAddr, gPnacTacReq[u4AppReqId].macAddr,
                 PNAC_MAC_ADDR_SIZE);

    gPnacTacReq[u4AppReqId].u1Status = PNAC_FALSE;

    /* Done with this -- free it */
    UtlShMemFreeTacMsgOutput ((UINT1 *) pTacRespRcvd);

    PnacEnQRadRespToIntfTask (pServEapData);
    if (PNAC_RELEASE_IF_RECV_DATA_MEMBLK (pServEapData) == MEM_FAILURE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Failed to release  memory. \r\n");
    }
    return;
}
#endif

#ifdef NPAPI_WANTED

/*****************************************************************************/
/* Function Name      : PnacNpCallBack                                       */
/*                                                                           */
/* Description        : This is the Callback function registered with        */
/*                      NPAPI task. It is called from NPAPI task after       */
/*                      making the NP Call with the result of the NP Call.   */
/*                      This API places the received information in lv to    */
/*                      a Self Queue and Sends an event to process the       */
/*                      NPAPI Call back.                                     */
/*                      of the bulk updates.                                 */
/*                                                                           */
/* Input(s)           : u4NpCallId     - NPAPI Call Identifier.              */
/*                      lv             - Union of NPAPI Call Structures      */
/*                                       tAsNpwPnacHwSetAuthStatus and       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
PnacNpCallBack (UINT4 u4NpCallId, unAsyncNpapi * lv)
{
    tPnacIntfMesg      *pMesg = NULL;

    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " INF: Call back Event when Module is ShutDown ... \n");
        return;
    }

    pMesg = (tPnacIntfMesg *) PNAC_ALLOC_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID);

    if (pMesg == NULL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " Memory Allocation for PnacCallBackEvent Message failed\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gPnacSystemInfo.i4SysLogId,
                      " Memory Allocation for PnacCallBackEvent Message failed"));
        return;
    }

    pMesg->u4MesgType = PNAC_NP_CALL_BACK;

    pMesg->Mesg.PnacNpCallBackMsg.u4NpCallId = u4NpCallId;

    /* Copying the Arguments to the structure corresponding to the 
     * NPAPI Call */
    if (u4NpCallId == AS_PNAC_HW_SET_AUTH_STATUS)
    {
        pMesg->Mesg.PnacNpCallBackMsg.u2Port = lv->PnacHwSetAuthStatus.u2Port;
        MEMCPY (pMesg->Mesg.PnacNpCallBackMsg.SuppMacAddr,
                lv->PnacHwSetAuthStatus.SuppAddr, sizeof (tMacAddr));
        pMesg->Mesg.PnacNpCallBackMsg.u1AuthMode =
            lv->PnacHwSetAuthStatus.u1AuthMode;
        pMesg->Mesg.PnacNpCallBackMsg.u1AuthStatus =
            lv->PnacHwSetAuthStatus.u1AuthStatus;
        pMesg->Mesg.PnacNpCallBackMsg.u1CtrlDir =
            lv->PnacHwSetAuthStatus.u1CtrlDir;
        pMesg->Mesg.PnacNpCallBackMsg.rval = lv->PnacHwSetAuthStatus.rval;
    }
    else if (u4NpCallId == AS_PNAC_HW_ADD_OR_DEL_MAC_SESS)
    {
        pMesg->Mesg.PnacNpCallBackMsg.u2Port = lv->PnacHwAddOrDelMacSess.u2Port;
        MEMCPY (pMesg->Mesg.PnacNpCallBackMsg.SuppMacAddr,
                lv->PnacHwAddOrDelMacSess.SuppAddr, sizeof (tMacAddr));
        pMesg->Mesg.PnacNpCallBackMsg.u1AuthStatus =
            lv->PnacHwAddOrDelMacSess.u1SessStatus;
        pMesg->Mesg.PnacNpCallBackMsg.rval = lv->PnacHwAddOrDelMacSess.rval;
    }

    /* Sending the Message to the PNAC Interface Queue */
    if (PNAC_SEND_TO_QUEUE (PNAC_INTF_INPUT_QID, (UINT1 *) &pMesg,
                            OSIX_DEF_MSG_LEN) == PNAC_OSIX_FAILURE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                  PNAC_CONTROL_PATH_TRC, " INF: Send To Q failed !!!\n");
        PNAC_RELEASE_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID, pMesg);
        return;
    }

    if (PNAC_SEND_EVENT (PNAC_INTF_TASK_ID,
                         PNAC_QUEUE_EVENT) == PNAC_OSIX_FAILURE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                  PNAC_CONTROL_PATH_TRC, " INF: Send Event failed !!!\n");
        return;
    }

    return;
}

#endif

/*****************************************************************************/
/* Function Name      : PnacSetPnacTraceLevel                                */
/*                                                                           */
/* Description        : This function is used to set the trace level for     */
/*                      PNAC module                                          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS                                         */
/*                                                                           */
/*****************************************************************************/
INT1
PnacSetPnacTraceLevel (UINT4 u4PnacTrcLevel)
{
    gPnacSystemInfo.u4TraceOption = u4PnacTrcLevel;
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacVlanPropChange                                   */
/*                                                                           */
/* Description        : This function posts a message to  PNAC task to take  */
/*                      neccessary action on vlan membership change          */
/*                                                                           */
/* Input(s)           : pVlanChangeInfo -(Contains the change specific info) */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PnacVlanPropChange (tVlanChangeInfo * pVlanChangeInfo)
{
    tPnacIntfMesg      *pMesg = NULL;
    UINT2               u2PortNum;

    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " INF: PnacVlanPropChange Event When module is shutdown ... \n");
        return PNAC_FAILURE;
    }

    if (pVlanChangeInfo == NULL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " INF: PnacVlanPropChange Vlan related Information not available ... \n");
        return PNAC_FAILURE;
    }

    u2PortNum = pVlanChangeInfo->u2Port;

    if ((u2PortNum < PNAC_MINPORTS) || (u2PortNum > PNAC_MAXPORTS))
    {
        /* Port Number is not a valid number */
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PnacVlanPropChange: Port Number is invalid \n");
        return PNAC_FAILURE;
    }

    pMesg =
        (tPnacIntfMesg *) PNAC_ALLOC_MEM_BLOCK (PNAC_INTF_CFGQ_MESG_MEMPOOL_ID);

    if (pMesg == NULL)
    {

        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " Allocate mem block for Pnac Configuration Message failed\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, (UINT4) gPnacSystemInfo.i4SysLogId,
                      "Allocate mem block for Pnac Configuration Message failed"));

        return PNAC_FAILURE;
    }

    pMesg->u4MesgType = PNAC_VLAN_PROP_CHANGE;

    pMesg->Mesg.PortInfo.u2PortIndex = u2PortNum;
    pMesg->Mesg.PortInfo.u2CurrentVlanId = pVlanChangeInfo->u2CurrentVlanId;

    if (PNAC_SEND_TO_QUEUE (PNAC_INTF_CFG_QID, (UINT1 *) &pMesg,
                            OSIX_DEF_MSG_LEN) == PNAC_OSIX_SUCCESS)
    {

        if (PNAC_SEND_EVENT (PNAC_INTF_TASK_ID,
                             PNAC_CFGQ_EVENT) == PNAC_OSIX_SUCCESS)
        {
            /* Success */
            return PNAC_SUCCESS;
        }
        else
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                      PNAC_CONTROL_PATH_TRC, " INF: Send Event failed !!!\n");

            /* Failure */
            return PNAC_FAILURE;
        }

    }

    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
              PNAC_CONTROL_PATH_TRC, " INF: Send To Q failed !!!\n");

    PNAC_RELEASE_MEM_BLOCK (PNAC_INTF_CFGQ_MESG_MEMPOOL_ID, pMesg);

    return PNAC_FAILURE;
}
