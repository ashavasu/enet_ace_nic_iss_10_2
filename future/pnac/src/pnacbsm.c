/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* $Id: pnacbsm.c,v 1.9 2014/07/15 12:56:58 siva Exp $                       */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : pnacbsm.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : PNAC Backend State machine                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Mar 2002                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains Backend State Machine       */
/*                            functions for PNAC module                      */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 MAR 2002 / BridgeTeam   Initial Create.                     */
/*---------------------------------------------------------------------------*/

#include "pnachdrs.h"

/*****************************************************************************/
/* Function Name      : PnacAuthBackendStateMachine                          */
/*                                                                           */
/* Description        : This routines performs the backend state machine     */
/*                      functionalities of the IEEE 802.1x protocol.         */
/*                                                                           */
/* Input(s)           : u2Event - The event that occured related to this     */
/*                                state machine.                             */
/*                                                                           */
/*                      u2PortNum - The port number corresponding to which   */
/*                                this event occured.                        */
/*                                                                           */
/*                      pAuthSessNode- Pointer to the authenticator session  */
/*                                node that holds the information related    */
/*                                to this session.                           */
/*                                                                           */
/*                      pu1Pkt -  Pointer to the packet buffer if this       */
/*                                state machine is called because of the     */
/*                                reception of the packet.                   */
/*                                                                           */
/*                      u2PktLen - The length of the packet in the packet    */
/*                                 buffer.                                   */
/*                                                                           */
/*                      pMesg -    Pointer to the CRU buffer when the event  */
/*                                 is because of packet arrival, and this    */
/*                                 buffer should be held by this state       */
/*                                 machine for retransmissions until a       */
/*                                 response to this packet is received.      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacBkendStateMachine[][]                           */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

INT4
PnacAuthBackendStateMachine (UINT2 u2Event, UINT2 u2PortNum,
                             tPnacAuthSessionNode * pAuthSessNode,
                             UINT1 *pu1Pkt,
                             UINT2 u2PktLen, tPnacCruBufChainHdr * pMesg)
{
    UINT2               u2State;
    INT4                i4RetVal;
    UINT4               u4MacAddr = 0;
    UINT2               u2MacAddr = 0;

    UINT1               aau1SemEvent[PNAC_MAX_BSM_EVENTS][20] = {
        "INITIALIZE", "RXRESP", "AWHILE_EXPIRED", "ASUCCESS_RCVD",
        "AFAIL_RCVD", "AREQ_RCVD", "AUTHSTART", "AUTHABORT"
    };
    UINT1               aau1SemState[PNAC_MAX_BSM_STATES][20] = {
        "Request", "Response", "Success", "Fail",
        "Timeout", "Idle", "Initialize"
    };

    if (pAuthSessNode == NULL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  "Invalid pointer passed to BSM event handler routine\n");
        return PNAC_FAILURE;
    }

    u2State = (UINT2) (pAuthSessNode->authFsmInfo.u2AuthBackendAuthState - 1);

    PNAC_TRC_ARG2 (PNAC_CONTROL_PATH_TRC,
                   " BSM Called with Event: %s, and State: %s\n",
                   aau1SemEvent[u2Event], aau1SemState[u2State]);

    PNAC_GET_FOURBYTE (u4MacAddr, pAuthSessNode->rmtSuppMacAddr);
    PNAC_GET_TWOBYTE (u2MacAddr, &(pAuthSessNode->rmtSuppMacAddr[4]));
    PNAC_TRC_ARG3 (PNAC_CONTROL_PATH_TRC,
                   " for Port: %d, MAC: %x-%x \n",
                   pAuthSessNode->u2PortNo, u4MacAddr, u2MacAddr);
    PNAC_TRC_ARG2 (PNAC_CONTROL_PATH_TRC,
                   " Id: %d, SessionNode: %x \n",
                   pAuthSessNode->authFsmInfo.u2CurrentId, pAuthSessNode);

    if (gPnacBkendStateMachine[u2Event][u2State].pAction == NULL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC, " BSM: No Operations to perform \n");
        return PNAC_SUCCESS;
    }

    i4RetVal = (*gPnacBkendStateMachine[u2Event][u2State].pAction)
        (u2PortNum, pAuthSessNode, pu1Pkt, u2PktLen, pMesg);
    if (i4RetVal != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " BSM: Event Function returned Failure \n");
        return PNAC_FAILURE;
    }
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacAuthBsmMakeInitialize                            */
/*                                                                           */
/* Description        : Called when the INITIALIZE event occurs.             */
/*                      Changes the Backend state machine state to           */
/*                      INITIALIZE.                                          */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u2PortNum - The port number corresponding to which   */
/*                                this event occured.                        */
/*                                                                           */
/*                      pAuthSessNode- Pointer to the authenticator session  */
/*                                node that holds the information related    */
/*                                to this session.                           */
/*                                                                           */
/*                      pu1Pkt -  Pointer to the packet buffer if this       */
/*                                state machine is called because of the     */
/*                                reception of the packet.                   */
/*                                                                           */
/*                      u2PktLen - The length of the packet in the packet    */
/*                                 buffer.                                   */
/*                                                                           */
/*                      pMesg -    Pointer to the CRU buffer when the event  */
/*                                 is because of packet arrival, and this    */
/*                                 buffer should be held by this state       */
/*                                 machine for retransmissions until a       */
/*                                 response to this packet is received.      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
PnacAuthBsmMakeInitialize (UINT2 u2PortNum,
                           tPnacAuthSessionNode * pAuthSessNode,
                           UINT1 *pu1Pkt,
                           UINT2 u2PktLen, tPnacCruBufChainHdr * pMesg)
{
    tPnacAuthFsmInfo   *pAuthFsmInfo = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;

    pAuthFsmInfo = &(pAuthSessNode->authFsmInfo);
    if (pAuthSessNode->pAWhileTimer != NULL)
    {
        if (PnacTmrStopTmr ((VOID *) pAuthSessNode, PNAC_AWHILE_TIMER)
            != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " BSM: Stop Awhile timer returned failure \n");
            return PNAC_FAILURE;
        }
    }

    if (pAuthSessNode->pReqMesg != NULL)
    {
        if (PNAC_RELEASE_CRU_BUF (pAuthSessNode->pReqMesg, PNAC_CRU_FALSE)
            != PNAC_CRU_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                      PNAC_BUFFER_TRC, " BSM: Release CRU Buffer Failed\n");
            return PNAC_FAILURE;
        }
        pAuthSessNode->pReqMesg = NULL;
    }
    pAuthFsmInfo->bAuthAbort = PNAC_FALSE;
    pAuthFsmInfo->u2AuthBackendAuthState = PNAC_BSM_STATE_INITIALIZE;
    PNAC_TRC (PNAC_CONTROL_PATH_TRC, " BSM moved to state: INITIALIZE !!\n");

    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  "PnacAuthBsmMakeInitialize:Invalid Port Number\n");
        return PNAC_FAILURE;
    }

    if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl
        == PNAC_PORTCNTRL_AUTO)
    {
        pAuthFsmInfo->bAuthStart = PNAC_FALSE;
        pAuthFsmInfo->u4ReqCount = PNAC_INIT_VAL;
        pAuthFsmInfo->u2AuthBackendAuthState = PNAC_BSM_STATE_IDLE;
        PNAC_TRC (PNAC_CONTROL_PATH_TRC, " BSM moved to state: IDLE !!\n");
    }

    /* Use the unused variables to avoid compilation warnings */
    PNAC_UNUSED (pu1Pkt);
    PNAC_UNUSED (u2PktLen);
    PNAC_UNUSED (pMesg);

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacAuthBsmAwhileExpiredRequest                      */
/*                                                                           */
/* Description        : Called when the awhile timer expired event occurs    */
/*                      when the present state is request.                   */
/*                      Checks the retransmit Count for the EAP request      */
/*                      packet transmitted and if it is greater than the     */
/*                      maximum retry count, then the state is moved to      */
/*                      TIMEOUT. Else the state is made REQUEST again.       */
/*                                                                           */
/* Input(s)           : u2PortNum - The port number corresponding to which   */
/*                                this event occured.                        */
/*                                                                           */
/*                      pAuthSessNode- Pointer to the authenticator session  */
/*                                node that holds the information related    */
/*                                to this session.                           */
/*                                                                           */
/*                      pu1Pkt -  Pointer to the packet buffer if this       */
/*                                state machine is called because of the     */
/*                                reception of the packet.                   */
/*                                                                           */
/*                      u2PktLen - The length of the packet in the packet    */
/*                                 buffer.                                   */
/*                                                                           */
/*                      pMesg -    Pointer to the CRU buffer when the event  */
/*                                 is because of packet arrival, and this    */
/*                                 buffer should be held by this state       */
/*                                 machine for retransmissions until a       */
/*                                 response to this packet is received.      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
PnacAuthBsmAwhileExpiredRequest (UINT2 u2PortNum,
                                 tPnacAuthSessionNode * pAuthSessNode,
                                 UINT1 *pu1Pkt,
                                 UINT2 u2PktLen, tPnacCruBufChainHdr * pMesg)
{
    tPnacAuthFsmInfo   *pAuthFsmInfo = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;

    pAuthFsmInfo = &(pAuthSessNode->authFsmInfo);
    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  "PnacAuthBsmAwhileExpiredRequest:Invalid Port Number\n");
        return PNAC_FAILURE;
    }

    if (pAuthFsmInfo->u4ReqCount >= PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4MaxReq)
    {
        /* Release the Buffer holding the Request */
        if (pAuthSessNode->pReqMesg != NULL)
        {
            if (PNAC_RELEASE_CRU_BUF (pAuthSessNode->pReqMesg, PNAC_CRU_FALSE)
                != PNAC_CRU_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                          PNAC_BUFFER_TRC, " BSM: Release CRU Buffer Failed\n");
                return PNAC_FAILURE;
            }
            pAuthSessNode->pReqMesg = NULL;
        }
        return (PnacAuthBsmMakeTimeout (u2PortNum, pAuthSessNode,
                                        pu1Pkt, u2PktLen, pMesg));
    }
    return (PnacAuthBsmMakeRequest (u2PortNum, pAuthSessNode,
                                    pu1Pkt, u2PktLen, pMesg));
}

/*****************************************************************************/
/* Function Name      : PnacAuthBsmMakeSuccess                               */
/*                                                                           */
/* Description        : This routine performs the operations needed for      */
/*                      changing the Back End State Machine state as         */
/*                      SUCCESS and calls the Authenticator State Machine.   */
/*                                                                           */
/* Input(s)           : u2PortNum - The port number corresponding to which   */
/*                                this event occured.                        */
/*                                                                           */
/*                      pAuthSessNode- Pointer to the authenticator session  */
/*                                node that holds the information related    */
/*                                to this session.                           */
/*                                                                           */
/*                      pu1Pkt -  Pointer to the packet buffer if this       */
/*                                state machine is called because of the     */
/*                                reception of the packet.                   */
/*                                                                           */
/*                      u2PktLen - The length of the packet in the packet    */
/*                                 buffer.                                   */
/*                                                                           */
/*                      pMesg -    Pointer to the CRU buffer when the event  */
/*                                 is because of packet arrival, and this    */
/*                                 buffer should be held by this state       */
/*                                 machine for retransmissions until a       */
/*                                 response to this packet is received.      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
PnacAuthBsmMakeSuccess (UINT2 u2PortNum,
                        tPnacAuthSessionNode * pAuthSessNode,
                        UINT1 *pu1Pkt,
                        UINT2 u2PktLen, tPnacCruBufChainHdr * pMesg)
{
    tPnacAuthFsmInfo   *pAuthFsmInfo = NULL;
    tPnacEapPktHdr      eapHdrInfo;

    pAuthFsmInfo = &(pAuthSessNode->authFsmInfo);

    if (pAuthSessNode->pAWhileTimer != NULL)
    {
        if (PnacTmrStopTmr ((VOID *) pAuthSessNode, PNAC_AWHILE_TIMER)
            != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " BSM: Stop Awhile timer returned failure \n");
            return PNAC_FAILURE;
        }
    }
    pAuthFsmInfo->u2CurrentId = pAuthFsmInfo->u2IdFromServer;

    PNAC_MEMSET (&eapHdrInfo, PNAC_INIT_VAL, sizeof (tPnacEapPktHdr));
    eapHdrInfo.u1Identifier = (UINT1) pAuthFsmInfo->u2CurrentId;
    eapHdrInfo.u1Code = PNAC_EAP_CODE_SUCCESS;
    eapHdrInfo.u2DataLength = PNAC_NO_VAL;
    eapHdrInfo.pu1Data = NULL;

    if (pAuthSessNode->bIsRsnPreAuthSess == OSIX_TRUE)
    {
        if (PnacPaeTxEapPkt (pAuthSessNode->rmtSuppMacAddr,
                             pAuthSessNode->u2PreAuthPhyPort, &eapHdrInfo,
                             pAuthSessNode->bIsRsnPreAuthSess) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_DATA_PATH_TRC |
                      PNAC_ALL_FAILURE_TRC,
                      " BSM: TxEapPkt returned failure \n");
            return PNAC_FAILURE;
        }
    }
    else
    {
        if (PnacPaeTxEapPkt (pAuthSessNode->rmtSuppMacAddr,
                             u2PortNum, &eapHdrInfo,
                             pAuthSessNode->bIsRsnPreAuthSess) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_DATA_PATH_TRC |
                      PNAC_ALL_FAILURE_TRC,
                      " BSM: TxEapPkt returned failure \n");
            return PNAC_FAILURE;
        }
    }

    pAuthFsmInfo->bAuthSuccess = PNAC_TRUE;
    pAuthFsmInfo->u2AuthBackendAuthState = PNAC_BSM_STATE_SUCCESS;
    PNAC_TRC (PNAC_CONTROL_PATH_TRC, " BSM moved to state: SUCCESS !!\n");

    if (PnacAuthStateMachine (PNAC_ASM_EV_BKEND_AUTHSUCCESS,
                              u2PortNum,
                              pAuthSessNode, NULL, PNAC_NO_VAL) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " BSM: ASM returned failure\n");
        return PNAC_FAILURE;
    }

    /* Now uncontrolled transition to Idle state */
    pAuthFsmInfo->bAuthStart = PNAC_FALSE;
    pAuthFsmInfo->u4ReqCount = PNAC_INIT_VAL;
    pAuthFsmInfo->u2AuthBackendAuthState = PNAC_BSM_STATE_IDLE;
    PNAC_TRC (PNAC_CONTROL_PATH_TRC, " BSM moved to state: IDLE !!\n");

    /* Use the unused variables to avoid compilation warnings */
    PNAC_UNUSED (pu1Pkt);
    PNAC_UNUSED (u2PktLen);
    PNAC_UNUSED (pMesg);

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacAuthBsmMakeFail                                  */
/*                                                                           */
/* Description        : This routine performs the operations needed for      */
/*                      changing the Back End State Machine state as         */
/*                      FAIL and calls the Authenticator State Machine.      */
/*                                                                           */
/* Input(s)           : u2PortNum - The port number corresponding to which   */
/*                                this event occured.                        */
/*                                                                           */
/*                      pAuthSessNode- Pointer to the authenticator session  */
/*                                node that holds the information related    */
/*                                to this session.                           */
/*                                                                           */
/*                      pu1Pkt -  Pointer to the packet buffer if this       */
/*                                state machine is called because of the     */
/*                                reception of the packet.                   */
/*                                                                           */
/*                      u2PktLen - The length of the packet in the packet    */
/*                                 buffer.                                   */
/*                                                                           */
/*                      pMesg -    Pointer to the CRU buffer when the event  */
/*                                 is because of packet arrival, and this    */
/*                                 buffer should be held by this state       */
/*                                 machine for retransmissions until a       */
/*                                 response to this packet is received.      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
PnacAuthBsmMakeFail (UINT2 u2PortNum,
                     tPnacAuthSessionNode * pAuthSessNode,
                     UINT1 *pu1Pkt, UINT2 u2PktLen, tPnacCruBufChainHdr * pMesg)
{
    tPnacAuthFsmInfo   *pAuthFsmInfo = NULL;
    tPnacEapPktHdr      eapHdrInfo;

    pAuthFsmInfo = &(pAuthSessNode->authFsmInfo);
    if (pAuthSessNode->pAWhileTimer != NULL)
    {
        if (PnacTmrStopTmr ((VOID *) pAuthSessNode, PNAC_AWHILE_TIMER)
            != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " BSM: Stop Awhile timer returned failure \n");
            return PNAC_FAILURE;
        }
    }

    pAuthFsmInfo->u2CurrentId = pAuthFsmInfo->u2IdFromServer;

    PNAC_MEMSET (&eapHdrInfo, PNAC_INIT_VAL, sizeof (tPnacEapPktHdr));
    eapHdrInfo.u1Identifier = (UINT1) pAuthFsmInfo->u2CurrentId;
    eapHdrInfo.u1Code = PNAC_EAP_CODE_FAILURE;
    eapHdrInfo.u2DataLength = PNAC_NO_VAL;
    eapHdrInfo.pu1Data = NULL;
    if (pAuthSessNode->bIsRsnPreAuthSess == OSIX_TRUE)
    {
        if (PnacPaeTxEapPkt (pAuthSessNode->rmtSuppMacAddr,
                             pAuthSessNode->u2PreAuthPhyPort, &eapHdrInfo,
                             pAuthSessNode->bIsRsnPreAuthSess) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " BSM: TxEapPkt returned Failure\n");
            return PNAC_FAILURE;
        }
    }
    else
    {
        if (PnacPaeTxEapPkt (pAuthSessNode->rmtSuppMacAddr,
                             u2PortNum, &eapHdrInfo,
                             pAuthSessNode->bIsRsnPreAuthSess) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " BSM: TxEapPkt returned Failure\n");
            return PNAC_FAILURE;
        }
    }

    pAuthFsmInfo->bAuthFail = PNAC_TRUE;
    pAuthFsmInfo->u2AuthBackendAuthState = PNAC_BSM_STATE_FAIL;
    PNAC_TRC (PNAC_CONTROL_PATH_TRC, " BSM moved to state: FAIL !!\n");

    if (PnacAuthStateMachine (PNAC_ASM_EV_BKEND_AUTHFAIL,
                              u2PortNum,
                              pAuthSessNode, NULL, PNAC_NO_VAL) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " BSM: ASM returned failure\n");
        return PNAC_FAILURE;
    }

    /* Now uncontrolled transition to Idle state */
    pAuthFsmInfo->bAuthStart = PNAC_FALSE;
    pAuthFsmInfo->u4ReqCount = PNAC_INIT_VAL;
    pAuthFsmInfo->u2AuthBackendAuthState = PNAC_BSM_STATE_IDLE;
    PNAC_TRC (PNAC_CONTROL_PATH_TRC, " BSM moved to state: IDLE !!\n");

    /* Use the unused variables to avoid compilation warnings */
    PNAC_UNUSED (pu1Pkt);
    PNAC_UNUSED (u2PktLen);
    PNAC_UNUSED (pMesg);

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacAuthBsmMakeTimeout                               */
/*                                                                           */
/* Description        : This routine performs the operations needed for      */
/*                      changing the Back End State Machine state as         */
/*                      TIMEOUT and calls the Authenticator State Machine.   */
/*                                                                           */
/* Input(s)           : u2PortNum - The port number corresponding to which   */
/*                                this event occured.                        */
/*                                                                           */
/*                      pAuthSessNode- Pointer to the authenticator session  */
/*                                node that holds the information related    */
/*                                to this session.                           */
/*                                                                           */
/*                      pu1Pkt -  Pointer to the packet buffer if this       */
/*                                state machine is called because of the     */
/*                                reception of the packet.                   */
/*                                                                           */
/*                      u2PktLen - The length of the packet in the packet    */
/*                                 buffer.                                   */
/*                                                                           */
/*                      pMesg -    Pointer to the CRU buffer when the event  */
/*                                 is because of packet arrival, and this    */
/*                                 buffer should be held by this state       */
/*                                 machine for retransmissions until a       */
/*                                 response to this packet is received.      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
PnacAuthBsmMakeTimeout (UINT2 u2PortNum,
                        tPnacAuthSessionNode * pAuthSessNode,
                        UINT1 *pu1Pkt,
                        UINT2 u2PktLen, tPnacCruBufChainHdr * pMesg)
{
    tPnacAuthFsmInfo   *pAuthFsmInfo = NULL;
    tPnacEapPktHdr      eapHdrInfo;

    pAuthFsmInfo = &(pAuthSessNode->authFsmInfo);

    if (pAuthFsmInfo->u2AuthControlPortStatus == PNAC_PORTSTATUS_UNAUTHORIZED)
    {
        PNAC_MEMSET (&eapHdrInfo, PNAC_INIT_VAL, sizeof (tPnacEapPktHdr));
        eapHdrInfo.u1Identifier = (UINT1) pAuthFsmInfo->u2CurrentId;
        eapHdrInfo.u1Code = PNAC_EAP_CODE_FAILURE;
        eapHdrInfo.u2DataLength = PNAC_NO_VAL;
        eapHdrInfo.pu1Data = NULL;
        if (pAuthSessNode->bIsRsnPreAuthSess == OSIX_TRUE)
        {
            if (PnacPaeTxEapPkt (pAuthSessNode->rmtSuppMacAddr,
                                 pAuthSessNode->u2PreAuthPhyPort, &eapHdrInfo,
                                 pAuthSessNode->bIsRsnPreAuthSess) !=
                PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " BSM: TxEapPkt returned Failure\n");
                return PNAC_FAILURE;
            }
        }
        else
        {
            if (PnacPaeTxEapPkt (pAuthSessNode->rmtSuppMacAddr,
                                 u2PortNum, &eapHdrInfo,
                                 pAuthSessNode->bIsRsnPreAuthSess) !=
                PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " BSM: TxEapPkt returned Failure\n");
                return PNAC_FAILURE;
            }
        }

    }
    pAuthFsmInfo->bAuthTimeout = PNAC_TRUE;
    pAuthFsmInfo->u2AuthBackendAuthState = PNAC_BSM_STATE_TIMEOUT;
    PNAC_TRC (PNAC_CONTROL_PATH_TRC, " BSM moved to state: TIMEOUT !!\n");
    if (PnacAuthStateMachine (PNAC_ASM_EV_BKEND_AUTHTIMEOUT,
                              u2PortNum,
                              pAuthSessNode, NULL, PNAC_NO_VAL) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " BSM: ASM returned failure\n");
        return PNAC_FAILURE;
    }

    /* Now ASM would have moved to aborting state and then to connecting 
     * state - so now move to initialise and idle state here itself 
     */

    pAuthFsmInfo->bAuthAbort = PNAC_FALSE;
    pAuthFsmInfo->u4ReqCount = PNAC_INIT_VAL;
    pAuthFsmInfo->u2AuthBackendAuthState = PNAC_BSM_STATE_IDLE;
    PNAC_TRC (PNAC_CONTROL_PATH_TRC, " BSM moved to state: IDLE !!\n");

    /* Use the unused variables to avoid compilation warnings */
    PNAC_UNUSED (pu1Pkt);
    PNAC_UNUSED (u2PktLen);
    PNAC_UNUSED (pMesg);

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacAuthBsmMakeResponse                              */
/*                                                                           */
/* Description        : This routine performs the operations needed for      */
/*                      changing the Back End State Machine state as         */
/*                      RESPONSE.                                            */
/*                                                                           */
/* Input(s)           : u2PortNum - The port number corresponding to which   */
/*                                this event occured.                        */
/*                                                                           */
/*                      pAuthSessNode- Pointer to the authenticator session  */
/*                                node that holds the information related    */
/*                                to this session.                           */
/*                                                                           */
/*                      pu1Pkt -  Pointer to the packet buffer if this       */
/*                                state machine is called because of the     */
/*                                reception of the packet.                   */
/*                                                                           */
/*                      u2PktLen - The length of the packet in the packet    */
/*                                 buffer.                                   */
/*                                                                           */
/*                      pMesg -    Pointer to the CRU buffer when the event  */
/*                                 is because of packet arrival, and this    */
/*                                 buffer should be held by this state       */
/*                                 machine for retransmissions until a       */
/*                                 response to this packet is received.      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

INT4
PnacAuthBsmMakeResponse (UINT2 u2PortNum,
                         tPnacAuthSessionNode * pAuthSessNode,
                         UINT1 *pu1Pkt,
                         UINT2 u2PktLen, tPnacCruBufChainHdr * pMesg)
{
    tPnacDPnacPktInfo   DPnacPktInfo;
    tPnacAuthFsmInfo   *pAuthFsmInfo = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;

    PNAC_MEMSET (&DPnacPktInfo, PNAC_INIT_VAL, sizeof (tPnacDPnacPktInfo));

    pAuthFsmInfo = &(pAuthSessNode->authFsmInfo);
    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  "PnacAuthBsmMakeResponse:Invalid Port Number\n");
        return PNAC_FAILURE;
    }

    /* Release the Buffer with Request if held because Response is received */
    if (pAuthSessNode->pReqMesg != NULL)
    {
        if (PNAC_RELEASE_CRU_BUF (pAuthSessNode->pReqMesg, PNAC_CRU_FALSE)
            != PNAC_CRU_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                      PNAC_BUFFER_TRC, " BSM: Release CRU Buffer Failed\n");
            return PNAC_FAILURE;
        }
        pAuthSessNode->pReqMesg = NULL;
    }

    if (pAuthSessNode->pAWhileTimer != NULL)
    {
        if (PnacTmrStopTmr ((VOID *) pAuthSessNode, PNAC_AWHILE_TIMER)
            != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " BSM: Stop Awhile timer returned failure \n");
            return PNAC_FAILURE;
        }
    }
    pAuthFsmInfo->bAreq = PNAC_FALSE;
    pAuthFsmInfo->bASuccess = PNAC_FALSE;
    pAuthFsmInfo->bAuthTimeout = PNAC_FALSE;
    pAuthFsmInfo->bRxResp = PNAC_FALSE;
    pAuthFsmInfo->bAFail = PNAC_FALSE;

    if (PnacTmrStartTmr ((VOID *) pAuthSessNode,
                         PNAC_AWHILE_TIMER,
                         PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4ServerTimeout)
        != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " BSM: Start Awhile timer returned failure \n");
        return PNAC_FAILURE;
    }
    pAuthFsmInfo->u4ReqCount = PNAC_INIT_VAL;

    pAuthFsmInfo->u2AuthBackendAuthState = PNAC_BSM_STATE_RESPONSE;
    PNAC_TRC (PNAC_CONTROL_PATH_TRC, " BSM moved to state: RESPONSE !!\n");

    if ((gPnacSystemInfo.u1SystemMode == PNAC_DISTRIBUTED) &&
        (PNAC_IS_REMOTE_RADIUS_AS ()))
    {
        /* Construct DPNAC packet and send it to PNAC queue */
        if (gPnacSystemInfo.u1DPnacRolePlayed == PNAC_DPNAC_SYSTEM_ROLE_SLAVE)
        {
            PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC,
                           "DPNAC:PnacAuthBsmMakeResponse DPNAC EAP frame triggered for Port=%d\n",
                           u2PortNum);

            DPnacPktInfo.u1PktType = PNAC_EAP_CODE_RESP;
            DPnacPktInfo.pu1EapPktBuf = pu1Pkt;
            DPnacPktInfo.u2EapPktLen = u2PktLen;
            DPnacPktInfo.u2NasPortNum = u2PortNum;

            if (DPnacPaeTxEapPkt (&DPnacPktInfo) != PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                          "DPNAC:PnacAuthBsmMakeResponse: DPNAC frame transmission failed ");
                return PNAC_FAILURE;
            }

            return PNAC_SUCCESS;
        }
    }

    if (PnacAsIfSendToServer (pu1Pkt, u2PktLen,
                              pAuthSessNode, u2PortNum) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " BSM: Handling of Frame by Server returned failure\n");
        return PNAC_FAILURE;
    }

    /* Use the unused variables to avoid compilation warnings */

    PNAC_UNUSED (pMesg);

    return PNAC_SUCCESS;
}                                /* PnacAuthBsmMakeResponse */

/*****************************************************************************/
/* Function Name      : PnacAuthBsmAreqRcvdResponse                          */
/*                                                                           */
/* Description        : This routine is called when an EAP request is        */
/*                      received is received from the Authentication         */
/*                      Server.                                              */
/*                      This routine stores the EAP frame in a CRU buffer    */
/*                      and attaches it to the authenticator session node for*/
/*                      it to be used later during retransmissions.          */
/*                                                                           */
/* Input(s)           : u2PortNum - The port number corresponding to which   */
/*                                this event occured.                        */
/*                                                                           */
/*                      pAuthSessNode- Pointer to the authenticator session  */
/*                                node that holds the information related    */
/*                                to this session.                           */
/*                                                                           */
/*                      pu1Pkt -  Pointer to the packet buffer if this       */
/*                                state machine is called because of the     */
/*                                reception of the packet.                   */
/*                                                                           */
/*                      u2PktLen - The length of the packet in the packet    */
/*                                 buffer.                                   */
/*                                                                           */
/*                      pMesg -    Pointer to the CRU buffer when the event  */
/*                                 is because of packet arrival, and this    */
/*                                 buffer should be held by this state       */
/*                                 machine for retransmissions until a       */
/*                                 response to this packet is received.      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

INT4
PnacAuthBsmAreqRcvdResponse (UINT2 u2PortNum,
                             tPnacAuthSessionNode * pAuthSessNode,
                             UINT1 *pu1Pkt,
                             UINT2 u2PktLen, tPnacCruBufChainHdr * pMesg)
{
    tPnacCruBufChainHdr **ppMesg = NULL;

    ppMesg = &(pAuthSessNode->pReqMesg);
    if (*ppMesg != NULL)
    {
        /* This should not happen */
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                  PNAC_BUFFER_TRC,
                  " BSM: Error: Buffer not released when changing from request state previously..\n");
        if (PNAC_RELEASE_CRU_BUF (*ppMesg, PNAC_CRU_FALSE) != PNAC_CRU_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                      PNAC_BUFFER_TRC, " BSM: Release CRU Buffer Failed\n");
            return PNAC_FAILURE;
        }
        *ppMesg = NULL;

    }
   /*************************************************************************
    * Duplicate the buffer and store the reference in Authenticator         *
    * session node.                                                         *
    *************************************************************************/
    if ((*ppMesg = PNAC_DUPLICATE_CRU_BUF (pMesg)) == NULL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                  PNAC_BUFFER_TRC, " BSM: Duplicate cru buf failed\n");
        return PNAC_FAILURE;
    }
    if (PnacAuthBsmMakeRequest (u2PortNum, pAuthSessNode, pu1Pkt, u2PktLen,
                                NULL) == PNAC_FAILURE)
    {
        if (PNAC_RELEASE_CRU_BUF (*ppMesg, PNAC_CRU_FALSE) != PNAC_CRU_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC |
                      PNAC_BUFFER_TRC, " BSM: Release CRU Buffer Failed\n");
            return PNAC_FAILURE;
        }
        *ppMesg = NULL;
        return PNAC_FAILURE;

    }
    return PNAC_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : PnacAuthBsmMakeRequest                               */
/*                                                                           */
/* Description        : This routine performs the operations needed for      */
/*                      changing the Back End State Machine state as         */
/*                      SUCCESS.                                             */
/*                                                                           */
/* Input(s)           : u2PortNum - The port number corresponding to which   */
/*                                this event occured.                        */
/*                                                                           */
/*                      pAuthSessNode- Pointer to the authenticator session  */
/*                                node that holds the information related    */
/*                                to this session.                           */
/*                                                                           */
/*                      pu1Pkt -  Pointer to the packet buffer if this       */
/*                                state machine is called because of the     */
/*                                reception of the packet.                   */
/*                                                                           */
/*                      u2PktLen - The length of the packet in the packet    */
/*                                 buffer.                                   */
/*                                                                           */
/*                      pMesg -    Pointer to the CRU buffer when the event  */
/*                                 is because of packet arrival, and this    */
/*                                 buffer should be held by this state       */
/*                                 machine for retransmissions until a       */
/*                                 response to this packet is received.      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

INT4
PnacAuthBsmMakeRequest (UINT2 u2PortNum,
                        tPnacAuthSessionNode * pAuthSessNode,
                        UINT1 *pu1Pkt,
                        UINT2 u2PktLen, tPnacCruBufChainHdr * pMesg)
{
    tPnacAuthFsmInfo   *pAuthFsmInfo = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacCruBufChainHdr *pDupCruMesg = NULL;

    pAuthFsmInfo = &(pAuthSessNode->authFsmInfo);
    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  "PnacAuthBsmMakeRequest:Invalid Port Number\n");
        return PNAC_FAILURE;
    }

    if (pAuthSessNode->pAWhileTimer != NULL)
    {
        if (PnacTmrStopTmr ((VOID *) pAuthSessNode, PNAC_AWHILE_TIMER)
            != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " BSM: Stop Awhile timer returned failure \n");
            return PNAC_FAILURE;
        }
    }
    pDupCruMesg = pAuthSessNode->pReqMesg;
    pAuthFsmInfo->u2CurrentId = pAuthFsmInfo->u2IdFromServer;

    if (pAuthSessNode->bIsRsnPreAuthSess == OSIX_TRUE)
    {
        if (PnacAuthTxReq (pAuthSessNode->rmtSuppMacAddr,
                           pAuthSessNode->u2PreAuthPhyPort,
                           pDupCruMesg, pAuthSessNode->bIsRsnPreAuthSess)
            != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      "BSM: Transmit Request Frame returned failure\n");
        }
    }
    else
    {
        if (PnacAuthTxReq (pAuthSessNode->rmtSuppMacAddr,
                           u2PortNum, pDupCruMesg,
                           pAuthSessNode->bIsRsnPreAuthSess) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      "BSM: Transmit Request Frame returned failure\n");
        }
    }
    if (PnacTmrStartTmr ((VOID *) pAuthSessNode,
                         PNAC_AWHILE_TIMER,
                         PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4SuppTimeout)
        != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " BSM: Start Awhile timer returned failure \n");
        return PNAC_FAILURE;
    }
    pAuthFsmInfo->u4ReqCount++;
    pAuthFsmInfo->u2AuthBackendAuthState = PNAC_BSM_STATE_REQUEST;
    PNAC_TRC (PNAC_CONTROL_PATH_TRC, " BSM moved to state: REQUEST !!\n");

    /* Use the unused variables to avoid compilation warnings */

    PNAC_UNUSED (pu1Pkt);
    PNAC_UNUSED (u2PktLen);
    PNAC_UNUSED (pMesg);

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacAuthBsmEventImpossible                           */
/*                                                                           */
/* Description        : This routine is called when the event and state      */
/*                      combinations are not possible.                       */
/*                                                                           */
/* Input(s)           : u2PortNum - The port number corresponding to which   */
/*                                this event occured.                        */
/*                                                                           */
/*                      pAuthSessNode- Pointer to the authenticator session  */
/*                                node that holds the information related    */
/*                                to this session.                           */
/*                                                                           */
/*                      pu1Pkt -  Pointer to the packet buffer if this       */
/*                                state machine is called because of the     */
/*                                reception of the packet.                   */
/*                                                                           */
/*                      u2PktLen - The length of the packet in the packet    */
/*                                 buffer.                                   */
/*                                                                           */
/*                      pMesg -    Pointer to the CRU buffer when the event  */
/*                                 is because of packet arrival, and this    */
/*                                 buffer should be held by this state       */
/*                                 machine for retransmissions until a       */
/*                                 response to this packet is received.      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

INT4
PnacAuthBsmEventImpossible (UINT2 u2PortNum,
                            tPnacAuthSessionNode * pAuthSessNode,
                            UINT1 *pu1Pkt,
                            UINT2 u2PktLen, tPnacCruBufChainHdr * pMesg)
{
    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
              " BSM: Event State Combination impossible !!!\n");

    /* Use the unused variables to avoid compilation warnings */

    PNAC_UNUSED (u2PortNum);
    PNAC_UNUSED (pAuthSessNode);
    PNAC_UNUSED (pu1Pkt);
    PNAC_UNUSED (u2PktLen);
    PNAC_UNUSED (pMesg);

    return PNAC_SUCCESS;
}
