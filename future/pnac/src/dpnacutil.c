/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2014                                          */
/* $Id: dpnacutil.c,v 1.5 2014/09/11 09:45:34 siva Exp $                     */
/*****************************************************************************/
/*    FILE  NAME            : dpnacutil.c                                    */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : DPNAC module                                   */
/*    MODULE NAME           : DPNAC PAE sub module                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 1 May 2014                                     */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains functions                   */
/*                            of DPNAC module                                */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    1 May 2014 / BridgeTeam   Initial Create.                      */
/*---------------------------------------------------------------------------*/

#include "pnachdrs.h"

/*****************************************************************************/
/* Function Name      : DPnacPaeConstructEapolHdr                             */
/*                                                                           */
/* Description        : Called to form the EAPOL packet header fields and    */
/*                      Ethernet header fields.                              */
/*                                                                           */
/* Input(s)           : pu1EapolPktBuf - The buffer pointer from where the   */
/*                                       the EAPOL header is to be formed    */
/*                                                                           */
/*                      u2PortNum - Port number of the PNAC frame            */
/*                                  transmitted                              */
/*                                                                           */
/*                      u1PktType -  Frame type                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : Port Interface Map for the Port.                     */
/*                      Protocol Version for the Port.                       */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
DPnacPaeConstructEapolHdr (UINT1 *pu1EapolPktBuf)
{
    tMacAddr            srcAddr = { 0 };
    UINT1              *pu1WritePtr = NULL;
    UINT2               u2Val = 0;
    UINT1               u1Val = 0;
    pu1WritePtr = pu1EapolPktBuf;

    if (pu1EapolPktBuf == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "DPNAC: DPnacPaeConstructEapolHdr NULL Pointer passed\r\n");
        return;
    }

    /* Write the Destination address as PNAC destination mac address */
    PNAC_MEMCPY (pu1WritePtr, gPnacEapolGrpMacAddr, PNAC_MAC_ADDR_SIZE);
    pu1WritePtr += PNAC_MAC_ADDR_SIZE;

    CfaGetSysMacAddress (srcAddr);
    PNAC_MEMCPY (pu1WritePtr, srcAddr, PNAC_MAC_ADDR_SIZE);

    pu1WritePtr += PNAC_MAC_ADDR_SIZE;

    /* Write the Ethernet Pkt type as EAPOL type */
    u2Val = PNAC_PAE_ENET_TYPE;

    PNAC_PUT_2BYTE (pu1WritePtr, u2Val);
    /* Write the EAPOL header - Protocol Version */
    u1Val = (UINT1) PNAC_EAPOLVERSION;
    PNAC_PUT_1BYTE (pu1WritePtr, u1Val);

    /* Write the EAPOL Packet Type */
    PNAC_PUT_1BYTE (pu1WritePtr, PNAC_EAP_PKT);
    return;
}

/*****************************************************************************/
/* Function Name      : DPnacPaeTxEapPkt                                     */
/*                                                                           */
/* Description        : This function is called to transmit an DPNAC frame   */
/*                      to Master or Slave/ by Master or Slave.              */
/*                      This will form                                       */
/*                      1) Event/Periodic Sync messges for sending port      */
/*                         information to Master by Slave.                   */
/*                      2) EAP Response frames from Slaves to Master.        */
/*                      3) EAP Request frames from Master to Slaves.         */
/*                                                                           */
/* Input(s)           : pDPnacPktInfo - Pointer to the data to be sent       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
DPnacPaeTxEapPkt (tPnacDPnacPktInfo * pDPnacPktInfo)
{
    tPnacDPnacSlotInfo *pDPnacSlotInfo = NULL;
    tPnacPaePortEntry  *pTmpPortInfo = NULL;
    UINT1              *pu1Buf = NULL;
    UINT1              *pu1BufPtr = NULL;
    UINT1              *pu1TmpBufPtr = NULL;
    UINT1              *pu1PortInfoLenField = NULL;
    UINT4               u4Val = 0;
    UINT2               u2NewPktLen = 0;
    UINT2               u2TmpPktLen = 0;
    UINT4               u4DistPortNum = 0;
    UINT2               u2Port = 0;
    UINT4               u4BrgPort = 0;
    UINT2               u2Val = 0;
    UINT2               u2Count = 0;
    UINT4               u4SlotId = 0;
    UINT1               u1PktSubType = 0;

    if (pDPnacPktInfo == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "DPNAC: DPNAC frame information is NULL\r\n");
        return PNAC_FAILURE;

    }
    if ((pDPnacPktInfo->u1PktType == PNAC_EAP_CODE_RESP)
        || (pDPnacPktInfo->u1PktType == PNAC_EAP_CODE_REQ))
    {
        /* If Packet type is Request/Response frame and EAP frame is NULL
           return FAILURE */

        if (pDPnacPktInfo->pu1EapPktBuf == NULL)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      "DPNAC: EAP frame is NULL\r\n");
            return PNAC_FAILURE;
        }

        /* If Packet type if Request and Server data is NULL 
           return FAILURE */

        if ((pDPnacPktInfo->u1PktType == PNAC_EAP_CODE_REQ)
            && (pDPnacPktInfo->pSrvEapData == NULL))
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      "DPNAC: EAP server frame is NULL\r\n");
            return PNAC_FAILURE;

        }
    }

    /* Distribution port */
    if (DPnacGetDistributionPortFromSlotId (u4SlotId, &u4DistPortNum)
        == PNAC_FAILURE)
    {
        /* Currently Connecting port from MBSM module is used as
           Distribution port in PNAC. Incase MBSM is not defined
           above function should be customized to return the
           required port number for connection with slave nodes.
         */

        return PNAC_FAILURE;
    }

    /* Get the Slot Identifier of the switch */
    u4SlotId = (UINT4) IssGetSwitchid ();

    if (DPnacGetSlotInfo (&pDPnacSlotInfo, u4SlotId) == PNAC_FAILURE)
    {

        if (DPnacGetSlotStatus (u4SlotId) == PNAC_FAILURE)
        {
            PNAC_TRC_ARG1 (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                           "DPNAC: Slot = %d is not ACTIVE \r\n", u4SlotId);
            return PNAC_FAILURE;
        }

        /* If Slot is not available create it for statistics updation */
        if (DPnacCreateSlotInfo (&pDPnacSlotInfo, u4SlotId) == PNAC_FAILURE)
        {
            return PNAC_FAILURE;
        }
        DPnacAddSlotToTable (pDPnacSlotInfo);
    }

    if (PNAC_ALLOCATE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Unable to allocate memory. \r\n");
        return PNAC_FAILURE;
    }

    PNAC_MEMSET (pu1Buf, PNAC_INIT_VAL, PNAC_MAX_ENET_FRAME_SIZE);
    pu1BufPtr = pu1Buf;

    /* Fill the Ethernet Header and EAPOL Header */
    DPnacPaeConstructEapolHdr (pu1BufPtr);

    /* Move the pointer to end of EAP type filed */
    pu1BufPtr += PNAC_ENET_HDR_SIZE + PNAC_EAP_TYPE_SIZE + PNAC_EAP_CODE_SIZE;

    /* Mark the pointer for filling length incase of
       the total EAP length */

    pu1TmpBufPtr = pu1BufPtr;
    u2TmpPktLen = pDPnacPktInfo->u2EapPktLen;

    /* Fill the EAP frame length which will 
       be filled later */
    PNAC_PUT_2BYTE (pu1BufPtr, u2TmpPktLen);

    /* Write the DPNAC Type */
    PNAC_PUT_1BYTE (pu1BufPtr, DPNAC_PKT_TYPE);

    /* Fill the Packet type */
    PNAC_PUT_1BYTE (pu1BufPtr, pDPnacPktInfo->u1PktType);

    if (pDPnacPktInfo->u1PktType == DPNAC_PERIODIC_SYNC_PDU)
    {
        /* Marker Pointer for Number of Remote Port Info field *
         * This field is filled in the end of this loop */

        pu1PortInfoLenField = pu1BufPtr;
        pu1BufPtr += DPNAC_PORT_NUMS_FIELD_LENGTH;

        for (u2Port = PNAC_MINPORTS; u2Port <= PNAC_MAXPORTS; u2Port++)
        {
            /* scan through all the ports present in the pPortList */
            pTmpPortInfo = &(PNAC_PORT_INFO (u2Port));

            if (DPnacCheckIsLocalPort (u2Port) == PNAC_FAILURE)
            {
                continue;
            }

            if (pTmpPortInfo == NULL)
            {
                if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == MEM_FAILURE)
                {
                    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                              "Failed to release  memory. \r\n");
                }
                pu1Buf = NULL;
                return PNAC_FAILURE;
            }

            if ((PNAC_AUTH_CONFIG_ENTRY (pTmpPortInfo).
                 u2AuthControlPortControl != PNAC_PORTCNTRL_FORCEAUTHORIZED)
                || (pTmpPortInfo->u1PortCtrlChanged == PNAC_TRUE))
            {
                /* Port Number */
                u2Val = 0;
                DPnacGetBridgeIndexFromIfIndex ((UINT4) u2Port, &u4BrgPort);
                u2Val = (UINT2) u4BrgPort;
                PNAC_PUT_2BYTE (pu1BufPtr, u2Val);
                /* Port status */
                PNAC_PUT_1BYTE (pu1BufPtr, (UINT1) pTmpPortInfo->u2PortStatus);
                /* Control Direction */
                PNAC_PUT_1BYTE (pu1BufPtr,
                                (UINT1) pTmpPortInfo->authConfigEntry.
                                u2OperControlDir);
                /* port control mode */
                PNAC_PUT_1BYTE (pu1BufPtr,
                                (UINT1) pTmpPortInfo->authConfigEntry.
                                u2AuthControlPortControl);
                /* Reserved - 1 byte */
                pu1BufPtr++;
                u2Count++;

                /* If any of the port's mode is changed send that information
                   in periodic sync frame. This is done so that port info
                   is synced between master and slave even Event Pdu is 
                   missed.
                 */
                if (pTmpPortInfo->u1PortCtrlChanged == PNAC_TRUE)
                {
                    pTmpPortInfo->u1PortCtrlChanged = PNAC_FALSE;
                }
            }

        }

        if (u2Count == 0)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                      "DPNAC: No Ports are Dot1x Enabled for Periodic Sync frame \r\n");

            if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == MEM_FAILURE)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                          "Failed to release  memory. \r\n");
            }

            pu1Buf = NULL;
            return PNAC_SUCCESS;
        }
        /* Number of ports filled in PDU */
        PNAC_PUT_2BYTE (pu1PortInfoLenField, u2Count);
        /* Length of the filled in PDU of all the port properties */
        u2TmpPktLen = (UINT2) (u2Count * DPNAC_PORT_INFO_LENGTH);
        PNAC_PUT_2BYTE (pu1TmpBufPtr, u2TmpPktLen);
        /* Increment the periodic frame tx counter */
        pDPnacSlotInfo->u4PeriodicFramesTx++;
    }

    if (pDPnacPktInfo->u1PktType == DPNAC_EVENT_UPDATE_PDU)
    {
        pTmpPortInfo = &(PNAC_PORT_INFO (pDPnacPktInfo->u2NasPortNum));

        /* Marker Pointer for Number of Remote Port Info field *
         * This field is filled in the end of this loop */

        pu1PortInfoLenField = pu1BufPtr;
        pu1BufPtr += DPNAC_PORT_NUMS_FIELD_LENGTH;

        if (pTmpPortInfo == NULL)
        {
            if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == MEM_FAILURE)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                          "Failed to release  memory. \r\n");
            }
            pu1Buf = NULL;
            return PNAC_FAILURE;
        }

        u2TmpPktLen = DPNAC_PORT_INFO_LENGTH;
        /* Port Number */
        DPnacGetBridgeIndexFromIfIndex ((UINT4) pDPnacPktInfo->u2NasPortNum,
                                        &u4BrgPort);
        u2Val = 0;
        u2Val = (UINT2) u4BrgPort;
        PNAC_PUT_2BYTE (pu1BufPtr, u2Val);
        /* Port status */
        PNAC_PUT_1BYTE (pu1BufPtr, (UINT1) pTmpPortInfo->u2PortStatus);
        /* Control Direction */
        PNAC_PUT_1BYTE (pu1BufPtr,
                        (UINT1) pTmpPortInfo->authConfigEntry.u2OperControlDir);
        /* port control mode */
        PNAC_PUT_1BYTE (pu1BufPtr,
                        (UINT1) pTmpPortInfo->authConfigEntry.
                        u2AuthControlPortControl);
        /* Reserved - 1 byte */
        pu1BufPtr++;

        u2Val = 1;
        /* Fill no of ports as 1 */
        PNAC_PUT_2BYTE (pu1PortInfoLenField, u2Val);

        PNAC_PUT_2BYTE (pu1TmpBufPtr, u2TmpPktLen);
        /* Increment the event update frame tx count */
        pDPnacSlotInfo->u4EventUpdateFramesTx++;

    }
    else if ((pDPnacPktInfo->u1PktType == PNAC_EAP_CODE_RESP)
             || (pDPnacPktInfo->u1PktType == PNAC_EAP_CODE_REQ))
    {
        pTmpPortInfo = &(PNAC_PORT_INFO (pDPnacPktInfo->u2NasPortNum));

        if (pTmpPortInfo == NULL)
        {
            if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == MEM_FAILURE)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                          "Failed to release  memory. \r\n");
            }
            pu1Buf = NULL;
            return PNAC_FAILURE;
        }

        /* Get the EAP packet subtype */
        pDPnacPktInfo->pu1EapPktBuf += PNAC_EAP_HDR_SIZE;

        PNAC_GET_1BYTE (u1PktSubType, pDPnacPktInfo->pu1EapPktBuf);

        /* Move back the pointer to the EAP code type */
        pDPnacPktInfo->pu1EapPktBuf -= (PNAC_EAP_HDR_SIZE + PNAC_EAP_TYPE_SIZE);

        /* Port Number */
        DPnacGetBridgeIndexFromIfIndex ((UINT4) pDPnacPktInfo->u2NasPortNum,
                                        &u4BrgPort);
        u2Val = 0;
        u2Val = (UINT2) u4BrgPort;
        /* Fill the NAS port number */
        PNAC_PUT_2BYTE (pu1BufPtr, u2Val);
        /* Fill the NAS port type */
        PNAC_PUT_1BYTE (pu1BufPtr, (pTmpPortInfo->portIfMap.u1IfType));
        /* Copy the EAP packet into the D-PNAC packet */
        PNAC_MEMCPY (pu1BufPtr, pDPnacPktInfo->pu1EapPktBuf,
                     pDPnacPktInfo->u2EapPktLen);
        /* Move the pointer */
        pu1BufPtr += pDPnacPktInfo->u2EapPktLen;

        if (u1PktSubType == PNAC_EAP_TYPE_MD5CHALLENGE)
        {
            /* If frames is MD5 response from Supplicant fill the State Attribute 
               Value in the D-PNAC packet */

            if (pDPnacPktInfo->u1PktType == PNAC_EAP_CODE_RESP)
            {
                if (pTmpPortInfo->pPortAuthSessionNode == NULL)
                {
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              " PortAuthSessionNode is NULL\n");
                    if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) ==
                        MEM_FAILURE)
                    {
                        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                                  "Failed to release  memory. \r\n");
                    }
                    pu1Buf = NULL;
                    return PNAC_FAILURE;
                }

                if (pTmpPortInfo->pPortAuthSessionNode->u2RadAttrStateLen !=
                    PNAC_INIT_VAL)
                {
                    u2Val = 0;
                    u2Val =
                        pTmpPortInfo->pPortAuthSessionNode->u2RadAttrStateLen;
                    /* Fill the State Attribute Length */
                    PNAC_PUT_2BYTE (pu1BufPtr, u2Val);
                    /* Fill the State Attribute Value */
                    PNAC_MEMCPY (pu1BufPtr,
                                 pTmpPortInfo->pPortAuthSessionNode->
                                 au1RadAttrState,
                                 pTmpPortInfo->pPortAuthSessionNode->
                                 u2RadAttrStateLen);
                    pu1BufPtr +=
                        pTmpPortInfo->pPortAuthSessionNode->u2RadAttrStateLen;
                }
            }
            /* If frames is MD5 request from Master Radius Server fill
               1) Session timeout Attribute value.
               2) State Attribute Length
               3) State Attribute Value
             */

            if (pDPnacPktInfo->u1PktType == PNAC_EAP_CODE_REQ)
            {
                u4Val = 0;

                if (pDPnacPktInfo->pSrvEapData != NULL)
                {
                    u4Val = pDPnacPktInfo->pSrvEapData->u4SessionTimeout;

                    PNAC_PUT_4BYTE (pu1BufPtr, u4Val);

                    u2Val = 0;
                    u2Val = pDPnacPktInfo->pSrvEapData->u2StateLen;
                    PNAC_PUT_2BYTE (pu1BufPtr, u2Val);
                    PNAC_MEMCPY (pu1BufPtr,
                                 pDPnacPktInfo->pSrvEapData->au1State,
                                 pDPnacPktInfo->pSrvEapData->u2StateLen);
                    pu1BufPtr += pDPnacPktInfo->pSrvEapData->u2StateLen;
                }

            }

        }
        if (pDPnacPktInfo->u1PktType == PNAC_EAP_CODE_REQ)
        {
            /* if no Server Key is received */
            if (pDPnacPktInfo->pSrvEapData != NULL)
            {
                if (pDPnacPktInfo->pSrvEapData->u2ServerKeyLength ==
                    PNAC_NO_VAL)
                {
                    PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_CONTROL_PATH_TRC,
                              " DPNAC: Received No Key from Server\n");

                    pu1BufPtr++;
                }
                else
                {
                    u2Val = 0;
                    u2Val = pDPnacPktInfo->pSrvEapData->u2ServerKeyLength;
                    PNAC_PUT_2BYTE (pu1BufPtr, u2Val);
                    PNAC_MEMCPY (pu1BufPtr,
                                 pDPnacPktInfo->pSrvEapData->au1ServerKey,
                                 pDPnacPktInfo->pSrvEapData->u2ServerKeyLength);
                    pu1BufPtr += pDPnacPktInfo->pSrvEapData->u2ServerKeyLength;

                }
            }

        }

        if ((u1PktSubType == PNAC_EAP_TYPE_NAK) ||
            (u1PktSubType == PNAC_EAP_TYPE_NOTIFICATION))
        {
            /* Other frames such as Notification, NAK are not supported. Drop it
             */
            if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == MEM_FAILURE)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                          "Failed to release  memory. \r\n");
            }
            pu1Buf = NULL;
            return PNAC_FAILURE;
        }
    }
    else if (pDPnacPktInfo->u1PktType == DPNAC_BRIDGE_STATUS_PDU)
    {
        /* EAP - Bridge detection status frames */

        u2Val = 0;
        u2Port = pDPnacPktInfo->u2NasPortNum;
        DPnacGetBridgeIndexFromIfIndex ((UINT4) u2Port, &u4BrgPort);
        u2Val = (UINT2) u4BrgPort;
        /* Port Number */
        PNAC_PUT_2BYTE (pu1BufPtr, u2Val);
        /* Bridge Detection status */
        PNAC_PUT_1BYTE (pu1BufPtr,
                        (UINT1) pDPnacPktInfo->u1BridgeDetectionStatus);
        /* Fill EAP length as (Port info length + Bridge detection status length) */
        u2TmpPktLen = (UINT2) (DPNAC_PORT_INFO_LENGTH + 1);
        PNAC_PUT_2BYTE (pu1TmpBufPtr, u2TmpPktLen);

    }

    /* Get the total length of the frame */
    u2NewPktLen = (UINT2) (pu1BufPtr - pu1Buf);

    /* validate Eap Pkt Length */
    if (u2NewPktLen > PNAC_MAX_EAP_PKT_SIZE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_DATA_PATH_TRC |
                  PNAC_CONTROL_PATH_TRC,
                  " DPNAC: Oversized Eap Pkt received\n");
        if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == MEM_FAILURE)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      "Failed to release  memory. \r\n");
        }
        pu1Buf = NULL;
        return PNAC_FAILURE;
    }

    /* Transmit the EAPOL Ethernet Frame */
#ifdef  NPAPI_WANTED

    /* Replace the pointers of new frame with the existing one */

    pDPnacPktInfo->pu1EapPktBuf = pu1Buf;
    pDPnacPktInfo->u2EapPktLen = u2NewPktLen;

    if (DPnacTxEnetFrame (pDPnacPktInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PAE: DPnacTxEnetFrame returned Failure\n");
        if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == MEM_FAILURE)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      "Failed to release  memory. \r\n");
        }
        pu1Buf = NULL;

        return PNAC_FAILURE;
    }
#else
    if (PnacPaeTxEnetFrame (pu1Buf, u2NewPktLen, (UINT2) u4DistPortNum) !=
        PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PAE: PnacPaeTxEnetFrame returned Failure\n");
        if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == MEM_FAILURE)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      "Failed to release  memory. \r\n");
        }
        pu1Buf = NULL;

        return PNAC_FAILURE;

    }
#endif
    if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1Buf) == MEM_FAILURE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Failed to release  memory. \r\n");
        pu1Buf = NULL;
        return PNAC_FAILURE;
    }

    pu1Buf = NULL;

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacProcessEapPkt                                   */
/*                                                                           */
/* Description        : This function is called whenever an EAP-DPNAC Packet */
/*                      with type (Request, Response, Periodic, Event) is to */
/*                      be processed by Master PNAC Node.                    */
/*                                                                           */
/* Input(s)           : pu1PktPtr    - DPNAC packet                          */
/*                      u2PktLength  - Packet length                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
DPnacProcessEapPkt (UINT1 *pu1PktPtr, UINT2 u2PktLength, UINT2 u2PortNum)
{
    UINT1              *pu1ReadPtr = NULL;
    UINT1               u1PktType = 0;

    if (pu1PktPtr == NULL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " DPNAC:DPnacProcessEapPkt Frame is NULL\n");
        return PNAC_FAILURE;
    }

    pu1ReadPtr = pu1PktPtr;

    /* Get the Packet type */
    PNAC_GET_1BYTE (u1PktType, pu1ReadPtr);

    switch (u1PktType)
    {
        case PNAC_EAP_CODE_RESP:

            /* If frame is Response it will be sent to           
               Radius client. */
            if (DPnacSendSlaveRespToServer (pu1ReadPtr, u2PktLength) !=
                PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " DPNAC: Send Response to Server returned failure\n");

                return PNAC_FAILURE;
            }
            break;
        case PNAC_EAP_CODE_REQ:

            /*  If frame is Request it will be processed by Slaves */
            if (DPnacHandleServResponse (pu1ReadPtr, u2PktLength) !=
                PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " DPNAC: Send Request to Supplicant returned failure\n");
                return PNAC_FAILURE;
            }

            break;
        case DPNAC_PERIODIC_SYNC_PDU:
        case DPNAC_EVENT_UPDATE_PDU:
            if (gPnacSystemInfo.u1DPnacRolePlayed ==
                PNAC_DPNAC_SYSTEM_ROLE_MASTER)
            {
                if (DPnacHandlePeriodicSyncOrEventPdu
                    (pu1ReadPtr, u1PktType, u2PortNum) != PNAC_SUCCESS)
                {
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              " DPNAC: Periodic Sync message handler returned failure\n");
                    return PNAC_FAILURE;

                }
            }
            break;

        case DPNAC_BRIDGE_STATUS_PDU:

            if (DPnacHandleBridgeDetectionFrame (pu1ReadPtr) != PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " DPNAC: Bridge Detection frame handling returned failure\n");
                return PNAC_FAILURE;
            }

            break;
        default:
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " DPNAC: Unknown D-PNAC packet \n");

            break;
    }

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacHandlePeriodicSyncOrEventPdu                    */
/*                                                                           */
/* Description        : This function process the Event Or Periodic sync     */
/*                      PDU.                                                 */
/*                                                                           */
/* Input(s)           : pu1PktPtr - Periodic Sync/Event packet               */
/*                      u1PktType - Periodic Sync/Event frame                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - On Success                            */
/*                      PNAC_FAILURE - On failure                            */
/*****************************************************************************/

INT4
DPnacHandlePeriodicSyncOrEventPdu (UINT1 *pu1PktPtr, UINT1 u1PktType,
                                   UINT2 u2PortNum)
{
    tPnacDPnacConsPortEntry *pConsPortEntry = NULL;
    tPnacDPnacSlotInfo *pDPnacSlotInfo = NULL;
    UINT4               u4SlotId = 1;
    UINT2               u2RemotePortIndex = 0;
    UINT2               u2NoOfRemotePortsEntry = 0;
    UINT2               u2Count = 0;
    UINT1               u1CtrlDirection = 0;
    UINT1               u1PortCtrlMode = 0;
    UINT1               u1RemotePortStatus = 0;
    UINT1               u1Reserved = 0;

    if (pu1PktPtr == NULL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " DPNAC:DPnacProcessEapPkt Frame is NULL\n");
        return PNAC_FAILURE;
    }

    if (DPnacGetSlotIdFromConnectingPort (u2PortNum, &u4SlotId) == PNAC_FAILURE)
    {
        return PNAC_FAILURE;
    }

    if (DPnacGetSlotInfo (&pDPnacSlotInfo, u4SlotId) == PNAC_FAILURE)
    {
        if (DPnacGetSlotStatus (u4SlotId) == PNAC_FAILURE)
        {
            PNAC_TRC_ARG1 (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                           "DPNAC: Slot = %d is not ACTIVE \r\n", u4SlotId);
            return PNAC_FAILURE;
        }

        /* If Slot entry not exists create and add */
        if (DPnacCreateSlotInfo (&pDPnacSlotInfo, u4SlotId) == PNAC_FAILURE)
        {
            return PNAC_FAILURE;

        }
        DPnacAddSlotToTable (pDPnacSlotInfo);
    }
    else
    {
        pDPnacSlotInfo->u4KeepAliveCount = 0;
    }

    PNAC_GET_2BYTE (u2NoOfRemotePortsEntry, pu1PktPtr);

    for (u2Count = 0; u2Count < u2NoOfRemotePortsEntry; u2Count++)
    {
        /* Get the port Index */
        PNAC_GET_2BYTE (u2RemotePortIndex, pu1PktPtr);

        /* Get the port status */
        PNAC_GET_1BYTE (u1RemotePortStatus, pu1PktPtr);

        /* Get the port ctrl direction */
        PNAC_GET_1BYTE (u1CtrlDirection, pu1PktPtr);

        /* Get the port ctrl mode */
        PNAC_GET_1BYTE (u1PortCtrlMode, pu1PktPtr);

        /* Get the reserved byte */
        PNAC_GET_1BYTE (u1Reserved, pu1PktPtr);

        /* Get the port entry from Slot */
        DPnacGetPortEntry (&pConsPortEntry, u2RemotePortIndex, u4SlotId);

        if (pConsPortEntry != NULL)
        {
            /* Port Ctrl is changed to force-authorized
               Need to Delete the port from table. */

            if (u1PortCtrlMode == PNAC_PORTCNTRL_FORCEAUTHORIZED)
            {
                DPnacUpdateHlAndHw (u2RemotePortIndex, u1RemotePortStatus,
                                    u1CtrlDirection, u1PortCtrlMode);
                DPnacRemovePortEntryFromConsTree (pConsPortEntry,
                                                  pDPnacSlotInfo->u4SlotId);
                DPnacDeleteConsPortEntry (pConsPortEntry);
                pConsPortEntry = NULL;
                continue;
            }

            if ((pConsPortEntry->u1PortAuthStatus != u1RemotePortStatus) ||
                (pConsPortEntry->u1CtrlDirection != u1CtrlDirection))
            {
                pConsPortEntry->u1PortAuthStatus = u1RemotePortStatus;
                pConsPortEntry->u1CtrlDirection = u1CtrlDirection;
                DPnacUpdateHlAndHw (u2RemotePortIndex, u1RemotePortStatus,
                                    u1CtrlDirection, u1PortCtrlMode);
            }

        }
        else
        {

            if (u1PortCtrlMode == PNAC_PORTCNTRL_FORCEAUTHORIZED)
            {
                /* If Port is force-authorized dont create port-entry
                   Just update Higher Layer. */
                DPnacUpdateHlAndHw (u2RemotePortIndex, u1RemotePortStatus,
                                    u1CtrlDirection, u1PortCtrlMode);
                continue;
            }

            DPnacCreateConsPortEntry (&pConsPortEntry,
                                      u2RemotePortIndex, u1RemotePortStatus);

            if (pConsPortEntry != NULL)
            {
                pConsPortEntry->u1LocalOrRemote = PNAC_DPNAC_REMOTE_PORT;
                DPnacAddToConsPortListTree (pConsPortEntry, u4SlotId);
                pConsPortEntry->u1CtrlDirection = u1CtrlDirection;
                pConsPortEntry->u1PortAuthStatus = u1RemotePortStatus;
                DPnacUpdateHlAndHw (u2RemotePortIndex, u1RemotePortStatus,
                                    u1CtrlDirection, u1PortCtrlMode);
            }

        }

    }

    if (u1PktType == DPNAC_PERIODIC_SYNC_PDU)
    {
        pDPnacSlotInfo->u4PeriodicFramesRx++;
        PNAC_TRC (CONTROL_PLANE_TRC, "DPNAC_PERIODIC_SYNC_PDU Received\n");
    }
    else
    {
        pDPnacSlotInfo->u4EventUpdateFramesRx++;
        PNAC_TRC (CONTROL_PLANE_TRC, "DPNAC_EVENT_UPDATE_PDU Received \n");

    }

    UNUSED_PARAM (u1Reserved);
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacHandleBridgeDetectionFrame                      */
/*                                                                           */
/* Description        : This function process the Event Or Periodic sync     */
/*                      PDU.                                                 */
/*                                                                           */
/* Input(s)           : pu1PktPtr - Periodic Sync/Event packet               */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - On Success                            */
/*                      PNAC_FAILURE - On failure                            */
/*****************************************************************************/
INT4
DPnacHandleBridgeDetectionFrame (UINT1 *pu1PktPtr)
{
    UINT2               u2PortNum = 0;
    UINT1               u1BridgeDetectStatus = 0;

    if (pu1PktPtr == NULL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " DPNAC:DPnacProcessEapPkt Frame is NULL\n");
        return PNAC_FAILURE;
    }

    /* Get the port Index */
    PNAC_GET_2BYTE (u2PortNum, pu1PktPtr);

    /* Get the bridge detection status */
    PNAC_GET_1BYTE (u1BridgeDetectStatus, pu1PktPtr);

    PnacPaeBridgeDetectionStatus (u1BridgeDetectStatus, u2PortNum);

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacSendSlaveRespToServer                           */
/*                                                                           */
/* Description        : This function transfers the Eap pkt to the remote    */
/*                      Authentication Server through a RADIUS client        */
/*                                                                           */
/* Input(s)           : pu1EapPkt - Start address of Eap pkt,                */
/*                      u2EapPktLength - Eap pkt length.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.au1NasId                             */
/*                      gPnacSystemInfo.u2NasIdLen                           */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

INT4
DPnacSendSlaveRespToServer (UINT1 *pu1EapPkt, UINT2 u2EapPktLength)
{
    tPnacAsIfSendData  *pAsIfData = NULL;
    UINT1              *pu1ReadPtr = NULL;
    UINT2               u2PortNum = 0;
    UINT2               u2Val = 0;
    UINT1               u1NasPortType = 0;
    UINT1               u1EapSubType = 0;
    UINT1               u1Val = 0;

    if (pu1EapPkt == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_DATA_PATH_TRC |
                  PNAC_CONTROL_PATH_TRC, " DPNAC: Empty EAP frame\n");
        return PNAC_FAILURE;
    }

    /* validate Eap Pkt Length */
    if (u2EapPktLength > PNAC_MAX_EAP_PKT_SIZE)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_DATA_PATH_TRC |
                  PNAC_CONTROL_PATH_TRC,
                  " DPNAC: Oversized Eap Pkt received\n");
        return PNAC_FAILURE;
    }

    if (PNAC_ALLOCATE_IF_SEND_DATA_MEMBLK (pAsIfData) == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Unable to allocate memory. \r\n");
        return PNAC_FAILURE;
    }

    PNAC_MEMSET (pAsIfData, PNAC_INIT_VAL, sizeof (tPnacAsIfSendData));

    /* Get the Nas port no from frame */
    PNAC_GET_2BYTE (u2PortNum, pu1EapPkt);

    /* Get the Nas port type from frame */
    PNAC_GET_1BYTE (u1NasPortType, pu1EapPkt);

    pu1ReadPtr = pu1EapPkt;

    pu1ReadPtr += PNAC_EAP_HDR_SIZE;

    /* Get the EAP sub type */
    PNAC_GET_1BYTE (u1EapSubType, pu1ReadPtr);

    if (u1EapSubType == PNAC_EAP_TYPE_MD5CHALLENGE)
    {
        /* If the frame is Response MD5 challenge , Username 
           length will be (EAP frame length - EAP header length -
           length of MD5 response value - MD5 response length field) 
         */
        PNAC_GET_1BYTE (u1Val, pu1ReadPtr);
        u2Val = (UINT2) (u2EapPktLength - u1Val - PNAC_EAP_HDR_SIZE -
                         PNAC_EAP_TYPE_SIZE - PNAC_EAP_SUBTYPE_SIZE);
        pu1ReadPtr += u1Val;
    }
    else if (u1EapSubType == PNAC_EAP_TYPE_IDENTITY)
    {
        /* If the frame is Response identity, Username 
           length will be (EAP frame length - EAP header length) 
         */
        u2Val = (UINT2) (u2EapPktLength - PNAC_EAP_HDR_SIZE -
                         PNAC_EAP_TYPE_SIZE);
    }
    else
    {
        /* Other frames such as Notification, NAK . Drop it */
        if (PNAC_RELEASE_IF_SEND_DATA_MEMBLK (pAsIfData) == MEM_FAILURE)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      "Failed to release  memory. \r\n");
        }
        pAsIfData = NULL;
        return PNAC_FAILURE;
    }

    if (u2Val > PNAC_USER_NAME_SIZE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  " DPNAC: User name size exceeds maximum user name length\n");
        /* Other frames such as Notification, NAK . Drop it */
        if (PNAC_RELEASE_IF_SEND_DATA_MEMBLK (pAsIfData) == MEM_FAILURE)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      "Failed to release  memory. \r\n");
        }
        pAsIfData = NULL;
        return PNAC_FAILURE;
    }

    /* Copy the Username of Supplicant */
    PNAC_MEMCPY (pAsIfData->au1SuppUserName, pu1ReadPtr, u2Val);
    pAsIfData->au1SuppUserName[u2Val] = '\0';

    /* Copy the Username length of Supplicant */
    pAsIfData->u2SuppUserNameLen =
        (UINT2) PNAC_STRLEN (pAsIfData->au1SuppUserName);

    /* Move the pointer to the end of Username */
    pu1ReadPtr += u2Val;

    /* Copy the Nas Id and Nas Id length */
    PNAC_MEMCPY (pAsIfData->au1NasId,
                 gPnacSystemInfo.au1NasId, gPnacSystemInfo.u2NasIdLen);
    pAsIfData->u2NasIdLen = gPnacSystemInfo.u2NasIdLen;

    /* Copy the Nas port number and Nas port type */
    pAsIfData->u2NasPortNumber = u2PortNum;
    pAsIfData->u1NasPortType = u1NasPortType;

    /* Copy the entire EAP packet */
    PNAC_MEMCPY (pAsIfData->au1EapPkt, pu1EapPkt, u2EapPktLength);
    pAsIfData->u2EapPktLength = u2EapPktLength;

    PNAC_TRC_ARG1 (PNAC_DATA_PATH_TRC | PNAC_CONTROL_PATH_TRC,
                   " DPNAC: Transferring Server-data to Auth Server for the user, %s.\n",
                   pAsIfData->au1SuppUserName);

    /* Copy the State Attribute Value from frame */
    PNAC_GET_2BYTE (u2Val, pu1ReadPtr);

    if (u2Val != PNAC_INIT_VAL)
    {
        pAsIfData->u2StateLen = u2Val;
        PNAC_MEMCPY (pAsIfData->au1State, pu1ReadPtr, u2Val);
    }
    /* Call RADIUS client function to transfer the Eap Data to
     * RADIUS client
     */
    if (PNAC_TX_EAP_TO_RADIUS (pAsIfData) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  " DPNAC: Tx to RADIUS client failed\n");
        if (PNAC_RELEASE_IF_SEND_DATA_MEMBLK (pAsIfData) == MEM_FAILURE)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      "Failed to release  memory. \r\n");
        }

        pAsIfData = NULL;
        return PNAC_FAILURE;
    }

    if (PNAC_RELEASE_IF_SEND_DATA_MEMBLK (pAsIfData) == MEM_FAILURE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Failed to release  memory. \r\n");
    }

    pAsIfData = NULL;

    PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_CONTROL_PATH_TRC,
              " DPNAC: Tx of Server-data to Auth Server succeeded\n");

    return PNAC_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : DPnacSendServRespToSlave                             */
/*                                                                           */
/* Description        : This function transfers the Eap pkt from RADIUS      */
/*                      Server to the Slave Nodes by forming DPNAC frame     */
/*                                                                           */
/* Input(s)           : pSrvEapData - Pointer to Server Data that contains   */
/*                      the Eap pkt                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
DPnacSendServRespToSlave (tPnacAsIfRecvData * pSrvEapData)
{
    tPnacDPnacPktInfo   DPnacPktInfo;

    PNAC_MEMSET (&DPnacPktInfo, PNAC_INIT_VAL, sizeof (tPnacDPnacPktInfo));

    if (pSrvEapData != NULL)
    {
        DPnacPktInfo.u1PktType = PNAC_EAP_CODE_REQ;
        DPnacPktInfo.pSrvEapData = pSrvEapData;
        DPnacPktInfo.pu1EapPktBuf = pSrvEapData->au1EapPkt;
        DPnacPktInfo.u2EapPktLen = pSrvEapData->u2EapPktLength;
        DPnacPktInfo.u2NasPortNum = pSrvEapData->u2NasPortNumber;
        /* Transmit the frame to all slave nodes by forming a DPNAC frame */
        if (DPnacPaeTxEapPkt (&DPnacPktInfo) == PNAC_FAILURE)
        {
            PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_CONTROL_PATH_TRC,
                      "DPnacPaeTxEapPkt returned FAILURE \n");
            return;
        }
    }
}

/*****************************************************************************/
/* Function Name      : DPnacHandleServResponse                              */
/*                                                                           */
/* Description        : This function is called by DPNAC module to process   */
/*                      the Radius Server response frame from Master Node.   */
/*                      EAP frame is extraced from the frame and send to the */
/*                      PNAC Back end submodule for processing.              */
/*                      Attribute value pairs are extracted from the frame   */
/*                      and stored in the Data base.                         */
/*                                                                           */
/* Input(s)           : pu1EapPkt - Start address of Eap pkt,                */
/*                      u2EapPktLength - Eap pkt length.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
DPnacHandleServResponse (UINT1 *pu1EapPkt, UINT2 u2EapPktLength)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tMacAddr            macAddr = { 0 };
    tMacAddr            suppMacAddr = { 0 };
    UINT1              *pu1PktPtr = NULL;
    UINT4               u4PortNo = 0;
    UINT4               u4PortBrgIndex = 0;
    UINT4               u4ServerTimeoutVal = 0;
    UINT2               u2Val = 0;
    UINT1               u1Val = 0;

    if (pu1EapPkt == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_DATA_PATH_TRC |
                  PNAC_CONTROL_PATH_TRC,
                  " DPnacHandleServResponse: Empty EAP frame\n");
        return PNAC_FAILURE;
    }

    if (PNAC_ALLOCATE_ENET_EAP_ARRAY_MEMBLK (pu1PktPtr) == NULL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " DPnacHandleServResponse: Allocate pu1EapPkt returned failure\n");
        return PNAC_FAILURE;
    }

    /* Initializing to Zero */
    PNAC_MEMSET (pu1PktPtr, PNAC_INIT_VAL, PNAC_MAX_EAP_PKT_SIZE);

    /* Get the port Number */
    PNAC_GET_2BYTE (u4PortBrgIndex, pu1EapPkt);

    DPnacGetIfIndexFromBridgeIndex (u4PortBrgIndex, &u4PortNo);

    /* Get port type - not used */
    PNAC_GET_1BYTE (u1Val, pu1EapPkt);

    /* Extracting the EapPkt */
    PNAC_MEMCPY (pu1PktPtr, pu1EapPkt, u2EapPktLength);

    pu1EapPkt += u2EapPktLength;

    /* Get the u4ServerTimeout value from the EAP frame */
    PNAC_GET_4BYTE (u4ServerTimeoutVal, pu1EapPkt);

    PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_CONTROL_PATH_TRC,
              " DPNAC: Handing over Server frame to Authenticator\n");
    /* Hand over the Eap pkt to Authenticator */
    if (PnacAuthHandleInServerFrame (pu1PktPtr, u2EapPktLength,
                                     (UINT2) u4PortNo,
                                     macAddr,
                                     u4ServerTimeoutVal) != PNAC_SUCCESS)
    {
        if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1PktPtr) == MEM_FAILURE)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      "Failed to release  memory. \r\n");
        }
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " DPNAC: Authenticator handling of Server frame failed\n");
        pu1PktPtr = NULL;
        return PNAC_FAILURE;
    }

    if (PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK (pu1PktPtr) == MEM_FAILURE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  "Failed to release  memory. \r\n");
    }

    pu1PktPtr = NULL;

    pPortInfo = &(PNAC_PORT_INFO ((UINT2) u4PortNo));

    if ((pPortInfo == NULL) || (pPortInfo->pPortAuthSessionNode == NULL))
    {
        return PNAC_FAILURE;

    }

    /* Get State attribute Length from frame */
    PNAC_GET_2BYTE (u2Val, pu1EapPkt);

    if (u2Val != PNAC_INIT_VAL)
    {
        /* Copy the Radius State Attribute into port info */
        pPortInfo->pPortAuthSessionNode->u2RadAttrStateLen = u2Val;
        PNAC_MEMCPY (pPortInfo->pPortAuthSessionNode->au1RadAttrState,
                     pu1EapPkt, u2Val);
        pu1EapPkt += u2Val;
    }
    else
    {
        pPortInfo->pPortAuthSessionNode->u2RadAttrStateLen = PNAC_INIT_VAL;
        PNAC_MEMSET (pPortInfo->pPortAuthSessionNode->au1RadAttrState,
                     PNAC_INIT_VAL, PNAC_MAX_STATE_ATTRIB_LEN);
    }

    /* Get Key Length from frame */
    PNAC_GET_2BYTE (u2Val, pu1EapPkt);

    /* if no Server Key is received, return */
    if (u2Val == PNAC_NO_VAL)
    {
        PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_CONTROL_PATH_TRC,
                  " DPNAC: Received No Key from Server\n");
        return PNAC_SUCCESS;
    }
    else
    {
        /* storing the Key in Authenticator's session node */
        pPortInfo->pPortAuthSessionNode->keyInfo.u2KeyLength = u2Val;

    }

    /* setting TRUE the boolean, informing that New Key is Available */
    pPortInfo->pPortAuthSessionNode->authFsmInfo.bKeyAvailable = PNAC_TRUE;

    PNAC_MEMCPY (suppMacAddr, pPortInfo->pPortAuthSessionNode->rmtSuppMacAddr,
                 PNAC_MAC_ADDR_SIZE);

    /* Extracting the Key */
    PNAC_MEMCPY (pPortInfo->pPortAuthSessionNode->keyInfo.au1Key, pu1EapPkt,
                 pPortInfo->pPortAuthSessionNode->keyInfo.u2KeyLength);

    PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_CONTROL_PATH_TRC,
              " DPNAC: Transmitting the Key to Supplicant...\n");

    /* Transmit the Key to the Supplicant */
    if (PnacKeyAuthTxKey (pPortInfo->pPortAuthSessionNode->keyInfo.au1Key,
                          u2Val, (UINT2) u4PortNo, suppMacAddr) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_DATA_PATH_TRC |
                  PNAC_CONTROL_PATH_TRC,
                  " DPNAC: Tx of Key to Supplicant failed\n");
        return PNAC_FAILURE;
    }

    PNAC_TRC (PNAC_DATA_PATH_TRC | PNAC_CONTROL_PATH_TRC,
              " DPNAC: Key successfully transmitted to the Supplicant\n");

    UNUSED_PARAM (u1Val);
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacCreateConsPortEntry                             */
/*                                                                           */
/* Description        : This function creates consolidated port entry for    */
/*                      a given port  by allocating memory for it.           */
/*                                                                           */
/* Input(s)           : ppConsPortEntry - Pointer to Cons port entry         */
/*                      u2PortIndex - Port Index of the port                 */
/*                      u1PortStatus - Authentication port status of port    */
/*                                                                           */
/* Output(s)          : ppConsPortEntry - Pointer to the Cons port entry     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS- On success                             */
/*                      PNAC_FAILURE - if memory allocation fails            */
/*****************************************************************************/

INT4
DPnacCreateConsPortEntry (tPnacDPnacConsPortEntry ** ppConsPortEntry,
                          UINT2 u2PortIndex, UINT1 u1PortStatus)
{

    tPnacDPnacConsPortEntry *pTmpConsPortEntry = NULL;

    if (PNAC_ALLOCATE_DPNAC_PORTS_INFO_MEM_BLOCK (pTmpConsPortEntry) == NULL)
    {
        PNAC_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PNAC_ALLOCATE_DPNAC_PORTS_INFO_MEM_BLOCK FAILED \n");
        return PNAC_FAILURE;
    }
    PNAC_MEMSET (pTmpConsPortEntry, 0, sizeof (tPnacDPnacConsPortEntry));

    pTmpConsPortEntry->u2PortIndex = u2PortIndex;
    pTmpConsPortEntry->u1PortAuthStatus = u1PortStatus;

    *ppConsPortEntry = pTmpConsPortEntry;
    return PNAC_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : DPnacAddToConsPortListTree                           */
/*                                                                           */
/* Description        : This function adds a consolidated port entry         */
/*                      to the given slot struture                           */
/*                                                                           */
/* Input(s)           : pConsPortEntry - pointer to Consolidated             */
/*                                       port entry that to added            */
/*                    : u4SlotId    - Slot Identifier                        */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - On Success                            */
/*                      PNAC_FAILURE - On failure                            */
/*****************************************************************************/

INT4
DPnacAddToConsPortListTree (tPnacDPnacConsPortEntry * pConsPortEntry,
                            UINT4 u4SlotId)
{
    tPnacDPnacConsPortEntry *pTmpConsPortEntry = NULL;
    tPnacDPnacSlotInfo *pDPnacSlotInfo = NULL;

    if (pConsPortEntry == NULL)
    {
        PNAC_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "DPnacAddToConsPortListTree" "NULL POINTER.\n");
        return PNAC_FAILURE;
    }

    if (DPnacGetSlotInfo (&pDPnacSlotInfo, u4SlotId) == PNAC_FAILURE)
    {
        /* If Slot entry not exists return */
        return PNAC_FAILURE;
    }

    if (DPnacGetPortEntry
        (&pTmpConsPortEntry, pConsPortEntry->u2PortIndex,
         u4SlotId) == PNAC_FAILURE)

    {
        if (RBTreeAdd
            (pDPnacSlotInfo->PnacDPnacPortList, pConsPortEntry) == RB_FAILURE)
        {
            PNAC_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                           "\n  Add to CONS LIST FAILURE: Port=%d\n",
                           pConsPortEntry->u2PortIndex);
            return PNAC_FAILURE;

        }

    }

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacGetPortEntry                                    */
/*                                                                           */
/* Description        : This function is called to get the port entry from   */
/*                      the given slot info for a port no.                   */
/*                                                                           */
/* Input(s)           : ppConsPortEntry - Pointer to the Port entry          */
/*                    : u2PortIndex     - Port number                        */
/*                    : u4SlotId        - Slot Identifier                    */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Output(s)          : ppConsPortEntry                                      */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - On success                            */
/*                      PNAC_FAILURE - On failure.                           */
/*****************************************************************************/

INT4
DPnacGetPortEntry (tPnacDPnacConsPortEntry ** ppConsPortEntry,
                   UINT2 u2PortIndex, UINT4 u4SlotId)
{
    tPnacDPnacConsPortEntry ConsPortEntry;
    tPnacDPnacSlotInfo *pDPnacSlotInfo = NULL;

    if (DPnacGetSlotInfo (&pDPnacSlotInfo, u4SlotId) == PNAC_FAILURE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " DPNAC: DPnacGetPortEntry failed\n");
        return PNAC_FAILURE;

    }

    PNAC_MEMSET (&ConsPortEntry, 0, sizeof (tPnacDPnacConsPortEntry));

    ConsPortEntry.u2PortIndex = u2PortIndex;
    *ppConsPortEntry =
        RBTreeGet (pDPnacSlotInfo->PnacDPnacPortList,
                   (tRBElem *) & ConsPortEntry);

    if (*ppConsPortEntry != NULL)
    {
        return PNAC_SUCCESS;
    }

    *ppConsPortEntry = NULL;

    PNAC_TRC_ARG1 (PNAC_ALL_FAILURE_TRC,
                   " DPNAC: DPnacGetPortEntry failed for port = %d\n",
                   u2PortIndex);

    return PNAC_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DPnacGetNextPortEntry                                */
/*                                                                           */
/* Description        : This function is called to Get the next port entry   */
/*                      from the given slot info.                            */
/*                                                                           */
/* Input(s)           : pConsPortEntry - Pointer to the Port entry           */
/*                    : ppConsPortEntry - Pointer to the next Port entry     */
/*                    : u4SlotId    - Slot Identifier                        */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Output(s)          : ppNextConsPortEntry                                  */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - On success                            */
/*                      PNAC_FAILURE - On failure.                           */
/*****************************************************************************/

INT4
DPnacGetNextPortEntry (tPnacDPnacConsPortEntry * pConsPortEntry,
                       tPnacDPnacConsPortEntry ** ppNextConsPortEntry,
                       UINT4 u4SlotId)
{
    tPnacDPnacConsPortEntry *pTmpConsPortEntry = NULL;
    tPnacDPnacSlotInfo *pDPnacSlotInfo = NULL;

    /* Get the Slot Info from Slot Id */
    if (DPnacGetSlotInfo (&pDPnacSlotInfo, u4SlotId) == PNAC_FAILURE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  " DPNAC: DPnacGetNextPortEntry failed\n");
        return PNAC_FAILURE;

    }

    if (pConsPortEntry == NULL)
    {
        pTmpConsPortEntry = RBTreeGetFirst (pDPnacSlotInfo->PnacDPnacPortList);
    }
    else
    {
        pTmpConsPortEntry =
            RBTreeGetNext (pDPnacSlotInfo->PnacDPnacPortList,
                           (tRBElem *) pConsPortEntry, NULL);
    }

    if (pTmpConsPortEntry != NULL)
    {
        *ppNextConsPortEntry = pTmpConsPortEntry;
        return PNAC_SUCCESS;
    }

    *ppNextConsPortEntry = NULL;

    PNAC_TRC (PNAC_ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
              " DPNAC: DPnacGetNextPortEntry failed\n");

    return PNAC_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DPnacRemovePortEntryFromConsTree                     */
/*                                                                           */
/* Description        : This function removes a port entry from              */
/*                      Consolidated port list RBTree for the given Slot     */
/*                                                                           */
/* Input(s)           : pConsPortEntry - pointer to Consolidated             */
/*                                       port entry that to be removed.      */
/*                    : u4SlotId        - Slot Identifier                    */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - On Success                            */
/*                      PNAC_FAILURE - On failure                            */
/*****************************************************************************/

INT4
DPnacRemovePortEntryFromConsTree (tPnacDPnacConsPortEntry * pConsPortEntry,
                                  UINT4 u4SlotId)
{
    tPnacDPnacSlotInfo *pDPnacSlotInfo = NULL;

    if (pConsPortEntry == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "PnacActiveDPnacRemovePortEntryFromConsTree: "
                  "NULL POINTER.\n");
        return PNAC_FAILURE;
    }

    if (DPnacGetSlotInfo (&pDPnacSlotInfo, u4SlotId) == PNAC_FAILURE)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  " DPNAC: DPnacRemovePortEntryFromConsTree failed\n");
        return PNAC_FAILURE;
    }

    if (RBTreeGet (pDPnacSlotInfo->PnacDPnacPortList, pConsPortEntry) == NULL)
    {
        /* Entry not present */
        PNAC_TRC_ARG1 (PNAC_ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                       " DPNAC: DPnacGetPortEntry failed for port = %d\n",
                       pConsPortEntry->u2PortIndex);
        return PNAC_FAILURE;

    }

    if (RBTreeRem
        (pDPnacSlotInfo->PnacDPnacPortList, (tRBElem *) pConsPortEntry) == 0)
    {
        PNAC_TRC_ARG1 (PNAC_ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                       "\nRemove port entry FAILED port = %d\n",
                       pConsPortEntry->u2PortIndex);
        return PNAC_FAILURE;
    }

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacDeleteConsPortEntry                             */
/*                                                                           */
/* Description        : This function Deletes consolidated port entry for    */
/*                      a given port  by releasing memory for it.            */
/*                                                                           */
/* Input(s)           : pConsPortEntry - Pointer to Cons port entry          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - On Success                            */
/*                      PNAC_FAILURE - On failure/memory release failure     */
/*****************************************************************************/
INT4
DPnacDeleteConsPortEntry (tPnacDPnacConsPortEntry * pConsPortEntry)
{

    if (pConsPortEntry == NULL)
    {
        PNAC_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "DPnacDeleteConsPortEntry" "NULL POINTER.\n");
        return PNAC_FAILURE;
    }

    if (PNAC_RELEASE_DPNAC_PORTS_INFO_MEM_BLOCK (pConsPortEntry) ==
        PNAC_MEM_FAILURE)
    {
        PNAC_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PNAC_RELEASE_DPNAC_PORTS_INFO_MEM_BLOCK FAILED \n");
        return PNAC_FAILURE;
    }

    pConsPortEntry = NULL;

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacCreateSlotInfo                                  */
/*                                                                           */
/* Description        : This function creates slot info entry for            */
/*                      a given slot  by allocating memory for it.           */
/*                                                                           */
/* Input(s)           : ppPnacDPnacSlotInfo - Pointer to Slot Info           */
/*                      u4SlotId - Slot Identifier                           */
/*                                                                           */
/* Output(s)          : ppPnacDPnacSlotInfo - pointer to slot info           */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - On Success                            */
/*                      PNAC_FAILURE - if memory allocation fails            */
/*                                                                           */
/*****************************************************************************/

INT4
DPnacCreateSlotInfo (tPnacDPnacSlotInfo ** ppPnacDPnacSlotInfo, UINT4 u4SlotId)
{

    tPnacDPnacSlotInfo *pTmpPnacDPnacSlotInfo = NULL;

    if (PNAC_ALLOCATE_SLOTS_INFO_MEM_BLOCK (pTmpPnacDPnacSlotInfo) == NULL)
    {
        PNAC_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PNAC_ALLOCATE_SLOTS_INFO_MEM_BLOCK FAILED \n");
        return PNAC_FAILURE;
    }

    PNAC_MEMSET (pTmpPnacDPnacSlotInfo, 0, sizeof (tPnacDPnacSlotInfo));

    PNAC_SLL_INIT_NODE (&(pTmpPnacDPnacSlotInfo->NextNode));

    pTmpPnacDPnacSlotInfo->u4SlotId = u4SlotId;
    pTmpPnacDPnacSlotInfo->PnacDPnacPortList =
        PnacRBTreeCreateEmbedded (FSAP_OFFSETOF (tPnacDPnacConsPortEntry,
                                                 PnacDPnacConsPortInfo),
                                  DPnacCmpPort);
    *ppPnacDPnacSlotInfo = pTmpPnacDPnacSlotInfo;

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacAddSlotToTable                                  */
/*                                                                           */
/* Description        : This function adds the slot entry to global table    */
/*                                                                           */
/* Input(s)           : pPnacDPnacSlotInfo - Pointer to Slot Info            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.PnacSlotInfo                         */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSystemInfo.PnacSlotInfo                         */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - On Success                            */
/*                      PNAC_FAILURE - On Failure                            */
/*****************************************************************************/

INT4
DPnacAddSlotToTable (tPnacDPnacSlotInfo * pPnacDPnacSlotInfo)
{

    tPnacDPnacSlotInfo *pTmpPnacDPnacSlotInfo = NULL;
    tPnacDPnacSlotInfo *pPrevPnacDPnacSlotInfo = NULL;

    if (pPnacDPnacSlotInfo == NULL)
    {
        PNAC_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC, "DPnacAddSlotToTable"
                  "NULL POINTER.\n");
        return PNAC_FAILURE;
    }

    PNAC_SLL_SCAN (&(gPnacSystemInfo.PnacSlotInfo),
                   pTmpPnacDPnacSlotInfo, tPnacDPnacSlotInfo *)
    {
        if (pTmpPnacDPnacSlotInfo->u4SlotId < pPnacDPnacSlotInfo->u4SlotId)
        {
            pPrevPnacDPnacSlotInfo = pTmpPnacDPnacSlotInfo;
            continue;
        }
        else
        {
            break;
        }
    }

    PNAC_SLL_INSERT (&(gPnacSystemInfo.PnacSlotInfo),
                     &pPrevPnacDPnacSlotInfo->NextNode,
                     &pPnacDPnacSlotInfo->NextNode);

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacGetSlotInfo                                     */
/*                                                                           */
/* Description        : This function gets the Slot entry for the given Slot */
/*                                                                           */
/* Input(s)           : ppDPnacSlotInfo - Pointer to Slot info               */
/*                    : u4SlotId        - Slot Identifier                    */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.PnacSlotInfo                         */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Output(s)          : ppDPnacSlotInfo                                      */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - On Success                            */
/*                      PNAC_FAILURE - On failure                            */
/*****************************************************************************/

INT4
DPnacGetSlotInfo (tPnacDPnacSlotInfo ** ppDPnacSlotInfo, UINT4 u4SlotId)
{
    tPnacDPnacSlotInfo *pTmpDPnacSlotInfo = NULL;

    /* Scan through the list and get the slot info */
    PNAC_SLL_SCAN (&(gPnacSystemInfo.PnacSlotInfo),
                   pTmpDPnacSlotInfo, tPnacDPnacSlotInfo *)
    {
        if (pTmpDPnacSlotInfo->u4SlotId == u4SlotId)
        {
            *ppDPnacSlotInfo = pTmpDPnacSlotInfo;
            return PNAC_SUCCESS;
        }
    }

    *ppDPnacSlotInfo = NULL;

    PNAC_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                   " DPNAC: PnacDPnacGetSlotInfo failed for Slot = %d\n",
                   u4SlotId);
    return PNAC_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DPnacGetNextSlotInfo                                 */
/*                                                                           */
/* Description        : This function is called to Get the next slot from    */
/*                      the given slot info.                                 */
/*                                                                           */
/* Input(s)           : ppDPnacSlotInfo - Pointer to Slot info               */
/*                    : ppNextDPnacSlotInfo - Pointer to the next Slot Info  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.PnacSlotInfo                         */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Output(s)          : ppNextDPnacSlotInfo                                  */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - On success                            */
/*                      PNAC_FAILURE - On failure.                           */
/*****************************************************************************/

INT4
DPnacGetNextSlotInfo (tPnacDPnacSlotInfo * pDPnacSlotInfo,
                      tPnacDPnacSlotInfo ** ppNextDPnacSlotInfo)
{
    tPnacDPnacSlotInfo *pTmpDPnacSlotInfo = NULL;

    if (pDPnacSlotInfo == NULL)
    {
        pTmpDPnacSlotInfo =
            (tPnacDPnacSlotInfo *)
            PNAC_SLL_FIRST (&(gPnacSystemInfo.PnacSlotInfo));
    }
    else
    {
        pTmpDPnacSlotInfo =
            (tPnacDPnacSlotInfo *)
            PNAC_SLL_NEXT (&(gPnacSystemInfo.PnacSlotInfo),
                           &pDPnacSlotInfo->NextNode);
    }

    if (pTmpDPnacSlotInfo != NULL)
    {
        *ppNextDPnacSlotInfo = pTmpDPnacSlotInfo;

        return PNAC_SUCCESS;
    }

    *ppNextDPnacSlotInfo = NULL;

    return PNAC_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DPnacDeleteSlotInfo                                  */
/*                                                                           */
/* Description        : This function deletes the slot info's port entries   */
/*                      and corresponsing slot info from global table and    */
/*                      free's the memory allocated for it                   */
/*                                                                           */
/* Input(s)           : pDPnacSlotInfo - Pointer to Slot info entry          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.PnacSlotInfo                         */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSystemInfo.PnacSlotInfo                         */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - On success                            */
/*                      PNAC_FAILURE - On failure.                           */
/*****************************************************************************/

INT4
DPnacDeleteSlotInfo (tPnacDPnacSlotInfo * pDPnacSlotInfo)
{
    tPnacDPnacConsPortEntry *pConsPortEntry = NULL;

    if (pDPnacSlotInfo == NULL)
    {
        PNAC_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC, "DPnacDeleteSlotInfo"
                  "NULL POINTER.\n");
        return PNAC_FAILURE;
    }

    DPnacGetNextPortEntry (NULL, &pConsPortEntry, pDPnacSlotInfo->u4SlotId);
    while (pConsPortEntry != NULL)
    {
        DPnacRemovePortEntryFromConsTree (pConsPortEntry,
                                          pDPnacSlotInfo->u4SlotId);
        DPnacDeleteConsPortEntry (pConsPortEntry);
        pConsPortEntry = NULL;
        DPnacGetNextPortEntry (NULL, &pConsPortEntry, pDPnacSlotInfo->u4SlotId);
    }

    DPnacInfoTreeDestroy (pDPnacSlotInfo->PnacDPnacPortList, NULL, 0);
    pDPnacSlotInfo->PnacDPnacPortList = NULL;

    PNAC_SLL_DELETE (&(gPnacSystemInfo.PnacSlotInfo),
                     &pDPnacSlotInfo->NextNode);

    if (PNAC_RELEASE_DPNAC_SLOTS_INFO_MEM_BLOCK (pDPnacSlotInfo) ==
        PNAC_MEM_FAILURE)
    {
        PNAC_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PNAC_RELEASE_DPNAC_SLOTS_INFO_MEM_BLOCK FAILED\r\n");
        return PNAC_FAILURE;
    }

    pDPnacSlotInfo = NULL;

    return PNAC_SUCCESS;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      : DPnacTxEnetFrame                                     */
/*                                                                           */
/* Description        : This function copies the frame into ICCH stucture    */
/*                      and transfers the frame to ICCH module for           */
/*                                                                           */
/* Input(s)           : pu1PktBuf - Start address of frame                   */
/*                      u2PktLength - pkt length.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - On success                            */
/*                      PNAC_FAILURE - On failure.                           */
/*****************************************************************************/
INT4
DPnacTxEnetFrame (tPnacDPnacPktInfo * pDPnacPktInfo)
{
    tIcchInfo           IcchInfo;
    UINT4               u4DestSlotId = 0;

    if (pDPnacPktInfo == NULL)
    {
        PNAC_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC, "DPnacTxEnetFrame"
                  "NULL POINTER.\n");
        return PNAC_FAILURE;
    }

    PNAC_MEMSET (&IcchInfo, 0, sizeof (tIcchInfo));

    if (gPnacSystemInfo.u1DPnacRolePlayed == PNAC_DPNAC_SYSTEM_ROLE_MASTER)
    {
        if (DPnacGetSlotIdFromPort (pDPnacPktInfo->u2NasPortNum, &u4DestSlotId)
            != PNAC_SUCCESS)
        {
            return PNAC_FAILURE;
        }
        else
        {
            IcchInfo.u4SlotId = u4DestSlotId;
        }
    }
    else
    {
        /* Get the Master Slot identifier */
        DPnacGetMasterSlotId (&u4DestSlotId);
        IcchInfo.u4SlotId = u4DestSlotId;    /* Master SlotId */
    }

    /* Fill the frame and and length */
    IcchInfo.pu1Data = pDPnacPktInfo->pu1EapPktBuf;
    IcchInfo.u2PktLength = pDPnacPktInfo->u2EapPktLen;
    IcchInfo.u2AppId = ICCH_PNAC;

    /*Need to call API call for Handle Out frame */
    ICCHHandOverTxFrame (&IcchInfo);
    return PNAC_SUCCESS;
}
#endif
/*****************************************************************************/
/* Function Name      : DPnacHandleKeepAlive                                 */
/*                                                                           */
/* Description        : This function increments keep alive counter for each */
/*                      slot after Master periodic sync timer expiry.        */
/*                      If the keep alive counter has reached maximum keep   */
/*                      alive count configured for the system, then that     */
/*                      remote slot will be deleted from global table.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
DPnacHandleKeepAlive (UINT1 u1Trigger)
{
    tPnacDPnacSlotInfo *pDPnacSlotInfo = NULL;
    tPnacDPnacSlotInfo *pDelDPnacSlotInfo = NULL;
    UINT4               u4MasterSlotId = 0;

    DPnacGetNextSlotInfo (NULL, &pDPnacSlotInfo);

    while (pDPnacSlotInfo != NULL)
    {
        /* Increment the Keep Alive count for other slots
           other than Master Slot */

        DPnacGetMasterSlotId (&u4MasterSlotId);

        if ((pDPnacSlotInfo->u4SlotId != u4MasterSlotId) &&
            (u1Trigger == PNAC_DPNAC_KEEP_ALIVE_TIMER_EXPIRY))
        {
            pDPnacSlotInfo->u4KeepAliveCount++;
        }

        /* Delete the slots which exceeds maximum */
        if (pDPnacSlotInfo->u4KeepAliveCount >=
            gPnacSystemInfo.u4DPnacMaxAliveCount)
        {
            PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC,
                           "DPNAC: Keep Alive : Node=%d is Down\n",
                           pDPnacSlotInfo->u4SlotId);
            pDelDPnacSlotInfo = pDPnacSlotInfo;
            DPnacGetNextSlotInfo (pDPnacSlotInfo, &pDPnacSlotInfo);
            DPnacDeleteSlotInfo (pDelDPnacSlotInfo);
            pDelDPnacSlotInfo = NULL;
            continue;
        }

        DPnacGetNextSlotInfo (pDPnacSlotInfo, &pDPnacSlotInfo);
    }

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacUpdateHlAndHw                                   */
/*                                                                           */
/* Description        : This function updates H/w and L2IWF when there is a  */
/*                      change in port status or port control direction      */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Index of the port                   */
/*                      u1PortStatus - Authentication port status of port    */
/*                      u1PortControlDir - Oper control direction of port    */
/*                      u1PortCtrlMode - Port control mode of port           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

INT4
DPnacUpdateHlAndHw (UINT2 u2PortNum, UINT1 u1PortStatus, UINT1 u1PortControlDir,
                    UINT1 u1PortCtrlMode)
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        return PNAC_FAILURE;
    }

    if (pPortInfo != NULL)
    {
        pPortInfo->u2PortStatus = (UINT2) u1PortStatus;
        PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2OperControlDir =
            (UINT2) u1PortControlDir;
    }
    PnacL2IwfSetPortPnacAuthStatus (u2PortNum, (UINT2) u1PortStatus);

    PnacL2IwfSetPortPnacControlDir (u2PortNum, (UINT2) u1PortControlDir);

    PnacL2IwfSetPortPnacAuthControl (u2PortNum, (UINT2) u1PortCtrlMode);

    PnacAuthPortOperIndication (u2PortNum);

#ifdef NPAPI_WANTED
    if (PNAC_IS_NP_PROGRAMMING_ALLOWED () == PNAC_TRUE)
    {
        if (PnacPnacHwSetAuthStatus (u2PortNum,
                                     NULL,
                                     PNAC_PORT_AUTHMODE_PORTBASED,
                                     u1PortStatus,
                                     u1PortControlDir) != FNP_SUCCESS)
        {
            if (u1PortStatus == PNAC_PORTSTATUS_AUTHORIZED)
            {
                PnacNpFailNotifyAndLog (u2PortNum,
                                        NULL,
                                        u1PortControlDir,
                                        PNAC_PORT_AUTHMODE_PORTBASED,
                                        PNAC_PORTSTATUS_AUTHORIZED);
            }
            else
            {

                PnacNpFailNotifyAndLog (u2PortNum,
                                        NULL,
                                        u1PortControlDir,
                                        PNAC_PORT_AUTHMODE_PORTBASED,
                                        PNAC_PORTSTATUS_UNAUTHORIZED);
            }
            return PNAC_FAILURE;
        }
    }
#endif /* NPAPI_WANTED */

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacMasterUpdateEntries                             */
/*                                                                           */
/* Description        : This function is called to update the Master node    */
/*                      ports in the consolidated database                   */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Index of the port                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

INT4
DPnacMasterUpdateEntries (UINT2 u2PortNum)
{
    tPnacDPnacSlotInfo *pDPnacSlotInfo = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacDPnacConsPortEntry *pConsPortEntry = NULL;
    UINT4               u4SlotId = 0;
    UINT4               u4PortBrgIndex = 0;

    PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC,
                   "DPNAC: DPnacMasterUpdateEntries for Port=%d\n", u2PortNum);

    u4SlotId = (UINT4) IssGetSwitchid ();

    if (DPnacGetSlotInfo (&pDPnacSlotInfo, u4SlotId) == PNAC_FAILURE)
    {
        /* If Slot entry not exists create and add */
        if (DPnacCreateSlotInfo (&pDPnacSlotInfo, u4SlotId) == PNAC_FAILURE)
        {
            return PNAC_FAILURE;

        }

        DPnacAddSlotToTable (pDPnacSlotInfo);
    }

    DPnacGetBridgeIndexFromIfIndex ((UINT4) u2PortNum, &u4PortBrgIndex);

    DPnacGetPortEntry (&pConsPortEntry, (UINT2) u4PortBrgIndex, u4SlotId);

    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        return PNAC_FAILURE;
    }
    else
    {
        if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl ==
            PNAC_PORTCNTRL_FORCEAUTHORIZED)
        {
            DPnacUpdateHlAndHw ((UINT2) u4PortBrgIndex,
                                PNAC_PORTSTATUS_AUTHORIZED,
                                (UINT1) PNAC_AUTH_CONFIG_ENTRY (pPortInfo).
                                u2OperControlDir,
                                PNAC_PORTCNTRL_FORCEAUTHORIZED);

            return PNAC_SUCCESS;
        }

    }

    if (pConsPortEntry != NULL)
    {
        if ((pConsPortEntry->u1PortAuthStatus != pPortInfo->u2PortStatus) ||
            (pConsPortEntry->u1CtrlDirection !=
             pPortInfo->authConfigEntry.u2OperControlDir))
        {
            pConsPortEntry->u1PortAuthStatus = (UINT1) pPortInfo->u2PortStatus;
            pConsPortEntry->u1CtrlDirection =
                (UINT1) pPortInfo->authConfigEntry.u2OperControlDir;
            DPnacUpdateHlAndHw ((UINT2) u4PortBrgIndex,
                                pConsPortEntry->u1PortAuthStatus,
                                pConsPortEntry->u1CtrlDirection,
                                (UINT1) pPortInfo->authConfigEntry.
                                u2AuthControlPortControl);
        }
    }
    else
    {
        if (DPnacCreateConsPortEntry
            (&pConsPortEntry, (UINT2) u4PortBrgIndex,
             (UINT1) pPortInfo->u2PortStatus) == PNAC_FAILURE)
        {
            PNAC_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                           "DPnacCreateConsPortEntry FAILED \n", u2PortNum);
            return PNAC_FAILURE;
        }

        if (pConsPortEntry != NULL)
        {
            pConsPortEntry->u1LocalOrRemote = PNAC_DPNAC_LOCAL_PORT;
            pConsPortEntry->u1PortAuthStatus = (UINT1) pPortInfo->u2PortStatus;
            pConsPortEntry->u1CtrlDirection =
                (UINT1) pPortInfo->authConfigEntry.u2OperControlDir;
            DPnacAddToConsPortListTree (pConsPortEntry, u4SlotId);
            DPnacUpdateHlAndHw ((UINT2) u4PortBrgIndex,
                                pConsPortEntry->u1PortAuthStatus,
                                pConsPortEntry->u1CtrlDirection,
                                (UINT1) pPortInfo->authConfigEntry.
                                u2AuthControlPortControl);
        }

    }

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacMasterDownIndication                            */
/*                                                                           */
/* Description        : This function is called when Master Node is down     */
/*                      to enable normal PNAC on all ports                   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

INT4
DPnacMasterDownIndication ()
{

    tPnacPaePortEntry  *pPortInfo = NULL;
    UINT2               u2PortNum = 0;

    /* for every Bridge port */
    for (u2PortNum = PNAC_MINPORTS; u2PortNum <= PNAC_MAXPORTS; u2PortNum++)
    {
        /*  check  whether port is invalid */
        if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
        {
            continue;
        }

        if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl ==
            PNAC_PORTCNTRL_AUTO)
        {
            PnacAuthAsmMakeForceUnauthorized
                (u2PortNum, pPortInfo->pPortAuthSessionNode, NULL, PNAC_NO_VAL);
        }
    }

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacMasterUpIndication                              */
/*                                                                           */
/* Description        : This function is called when Master Node comes up    */
/*                      and to enable DPNAC on all the ports.                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

INT4
DPnacMasterUpIndication ()
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    UINT2               u2PortNum = 0;

    /* for every Bridge port */
    for (u2PortNum = PNAC_MINPORTS; u2PortNum <= PNAC_MAXPORTS; u2PortNum++)
    {
        /*  check whether port is invalid */
        if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)

        {
            continue;
        }

        if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl ==
            PNAC_PORTCNTRL_AUTO)
        {
            PnacAuthAsmPortCntrlChanged (u2PortNum,
                                         pPortInfo->pPortAuthSessionNode,
                                         NULL, PNAC_NO_VAL);
        }
    }

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacSlotDetach                                      */
/*                                                                           */
/* Description        : This function is called when a slot is detached to   */
/*                      remove the entry from Database.                      */
/*                                                                           */
/* Input(s)           : u4SlotId - Slot Identifier that is detached          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

INT4
DPnacSlotDetach (UINT4 u4SlotId)
{
    tPnacDPnacSlotInfo *pDPnacSlotInfo = NULL;
    tPnacDPnacConsPortEntry *pConsPortEntry = NULL;
    if (DPnacGetSlotInfo (&pDPnacSlotInfo, u4SlotId) == PNAC_FAILURE)
    {
        return PNAC_FAILURE;
    }
    if (pDPnacSlotInfo != NULL)
    {
        DPnacGetNextPortEntry (NULL, &pConsPortEntry, pDPnacSlotInfo->u4SlotId);
        while (pConsPortEntry != NULL)
        {

            DPnacUpdateHlAndHw (pConsPortEntry->u2PortIndex,
                                PNAC_PORTSTATUS_UNAUTHORIZED,
                                PNAC_CNTRLD_DIR_BOTH,
                                PNAC_PORTCNTRL_FORCEUNAUTHORIZED);

            DPnacRemovePortEntryFromConsTree (pConsPortEntry,
                                              pDPnacSlotInfo->u4SlotId);
            DPnacDeleteConsPortEntry (pConsPortEntry);
            pConsPortEntry = NULL;
            DPnacGetNextPortEntry (NULL, &pConsPortEntry,
                                   pDPnacSlotInfo->u4SlotId);
        }

        DPnacDeleteSlotInfo (pDPnacSlotInfo);
        pDPnacSlotInfo = NULL;
    }
    else
    {
        PNAC_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Slot Info= %d not exists \n",
                       u4SlotId);
    }

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacSetModuleStatus                                 */
/*                                                                           */
/* Description        : This function is called to enable/disable DPNAC      */
/*                                                                           */
/* Input(s)           : u1ModuleStatus - Centralized / Distributed           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.u1DPnacRolePlayed                    */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

INT4
DPnacSetModuleStatus (UINT1 u1ModuleStatus)
{
    tPnacDPnacSlotInfo *pDPnacSlotInfo = NULL;
    tPnacDPnacSlotInfo *pDelDPnacSlotInfo = NULL;

    if (u1ModuleStatus == PNAC_CENTRALIZED)
    {
        DPnacGetNextSlotInfo (NULL, &pDPnacSlotInfo);
        while (pDPnacSlotInfo != NULL)
        {
            pDelDPnacSlotInfo = pDPnacSlotInfo;
            DPnacGetNextSlotInfo (pDPnacSlotInfo, &pDPnacSlotInfo);
            DPnacDeleteSlotInfo (pDelDPnacSlotInfo);
            pDelDPnacSlotInfo = NULL;
        }

        PNAC_SLL_INIT (&(gPnacSystemInfo.PnacSlotInfo));

        if (gPnacSystemInfo.DPnacPeriodicSyncTmr != NULL)
        {

            if (PnacTmrStopTmr (&gPnacSystemInfo, PNAC_DPNAC_PERIODIC_TIMER)
                != PNAC_SUCCESS)

            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " DPNAC: Stop periodic timer returned failure \n");
                return PNAC_FAILURE;
            }
            gPnacSystemInfo.DPnacPeriodicSyncTmr = (tPnacTimerNode *) NULL;
        }
    }
    else
    {
        gPnacSystemInfo.u1DPnacRolePlayed = (UINT1) CfaGetRetrieveNodeState ();

        if (gPnacSystemInfo.DPnacPeriodicSyncTmr == NULL)
        {
            if (PnacTmrStartTmr (&gPnacSystemInfo, PNAC_DPNAC_PERIODIC_TIMER,
                                 gPnacSystemInfo.u4DPnacPeriodicSyncTime) !=
                PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC, "PnacTmrStartTmr failed \n");
                return PNAC_FAILURE;
            }
        }
    }
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacSendEventUpdateMsg                              */
/*                                                                           */
/* Description        : This function is used to updated the Master database */
/*                      and to send to event update incase of port           */
/*                      properties change                                    */
/*                                                                           */
/* Input(s)           : u2PortNum - Port number of the port                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.u1SystemMode                         */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
DPnacSendEventUpdateMsg (UINT2 u2PortNum)
{
    tPnacDPnacPktInfo   DPnacPktInfo;
    PNAC_MEMSET (&DPnacPktInfo, PNAC_INIT_VAL, sizeof (tPnacDPnacPktInfo));

    if (gPnacSystemInfo.u1SystemMode == PNAC_DISTRIBUTED)
    {
        if (gPnacSystemInfo.u1DPnacRolePlayed == PNAC_DPNAC_SYSTEM_ROLE_MASTER)
        {
            DPnacMasterUpdateEntries (u2PortNum);
        }
        else
        {
            DPnacPktInfo.u1PktType = DPNAC_EVENT_UPDATE_PDU;
            DPnacPktInfo.u2NasPortNum = u2PortNum;

            if (DPnacCheckIsLocalPort (u2PortNum) == PNAC_FAILURE)
            {
                return PNAC_FAILURE;
            }

            if (DPnacPaeTxEapPkt (&DPnacPktInfo) != PNAC_SUCCESS)
            {
                return PNAC_FAILURE;
            }

        }
    }
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DPnacSendAllPortInfoToMaster                         */
/*                                                                           */
/* Description        : This function is used to send the complete info      */
/*                      for all the ports                                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
DPnacSendAllPortInfoToMaster ()
{
    tPnacDPnacPktInfo   DPnacPktInfo;
    tPnacPaePortEntry  *pPortInfo = NULL;
    UINT2               u2PortNum = 0;

    PNAC_MEMSET (&DPnacPktInfo, PNAC_INIT_VAL, sizeof (tPnacDPnacPktInfo));

    /* for every Bridge port */
    for (u2PortNum = PNAC_MINPORTS; u2PortNum <= PNAC_MAXPORTS; u2PortNum++)
    {
        /*  check whether port is invalid */
        if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)

        {
            continue;
        }

        if (pPortInfo != NULL)
        {
            /* Set the variable as True so that in next periodic
               all the port status will be synced in master.
             */
            pPortInfo->u1PortCtrlChanged = PNAC_TRUE;
        }
    }

    DPnacPktInfo.u1PktType = DPNAC_PERIODIC_SYNC_PDU;

    if (DPnacPaeTxEapPkt (&DPnacPktInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  ("TMR: PnacTmrHandler: Packet transmission failed "));
        return PNAC_FAILURE;
    }

    return PNAC_SUCCESS;
}
