/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pnacmbsm.c,v 1.17 2013/06/12 10:30:55 siva Exp $
 *
 * Description     : 
 *                   
 *******************************************************************/

/*****************************************************************************/

/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2004-2005                         */
/*****************************************************************************/
/*    FILE  NAME            : pnacmbsm.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : PNAC module Card Updation                      */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 21 Jan 2005                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains Card Insertion functions for*/
/*                            the PNAC module.                               */
/*---------------------------------------------------------------------------*/

#include "pnachdrs.h"
/*****************************************************************************/
/* Function Name      : PnacMbsmUpdateCardInsertion                          */
/*                                                                           */
/* Description        : This function programs the HW with the PNAC software */
/*                      configuration.                                       */
/*                                                                           */
/*                      This function will be called from the PNAC module    */
/*                      when an indication for the Card Insertion is         */
/*                      received from the MBSM.                              */
/*                                                                           */
/* Input(s)           :  tMbsmPortInfo - Structure containing the PortList,  */
/*                                       Starting Index and the port count.  */
/*                       tMbsmSlotInfo - Structure containing the SlotId     */
/*                                       info.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCCESS/MBSM_FAILURE                            */
/*****************************************************************************/

INT4
PnacMbsmUpdateCardInsertion (tMbsmPortInfo * pPortInfo,
                             tMbsmSlotInfo * pSlotInfo)
{
    tPnacPaePortEntry  *pPnacPortInfo = NULL;
    UINT4               u4PortNo = 0;
    UINT4               u4PortCount = 0;
    UINT1               u1AuthMode = 0;
    UINT1               u1AuthStatus = 0;
    UINT1               u1CtrlDir = 0;
    UINT1               u1IsSetInPortList = OSIX_FALSE;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;
    if (PNAC_IS_DISABLED ())
    {
        /* Return without configuring the H/W if PNAC is disabled */
        return MBSM_SUCCESS;
    }
    if (!MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
    {
        if (PnacPnacMbsmHwEnable (pSlotInfo) == FNP_FAILURE)
        {
            return MBSM_FAILURE;
        }
    }

    u4PortCount = MBSM_PORT_INFO_PORTCOUNT (pPortInfo);
    u4PortNo = MBSM_PORT_INFO_STARTINDEX (pPortInfo);

    while (u4PortCount != 0)
    {
        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST (pPortInfo), u4PortNo,
                                 sizeof (tPortList), u1IsSetInPortList);
        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST_STATUS (pPortInfo),
                                 u4PortNo, sizeof (tPortList),
                                 u1IsSetInPortListStatus);

        /* PORT INSERT for the corresponding bit in portList */
        if ((u1IsSetInPortList == OSIX_TRUE)
            && (OSIX_FALSE == u1IsSetInPortListStatus))
        {
            /* Check whether there is an entry in the SW for the ports that
             * were added. */
            if (PnacGetPortEntry ((UINT2) u4PortNo, &pPnacPortInfo)
                != PNAC_SUCCESS)
            {
                PNAC_TRC (CONTROL_PLANE_TRC | PNAC_ALL_FAILURE_TRC,
                          "Such a port does not exist!! ... \n");
                u4PortCount--;
                u4PortNo++;
                continue;
            }

            /* Find the Authentication Mode, Status and the Control 
             * Direction from the Port Entry. */
            u1AuthStatus = gPnacSystemInfo.aPnacPaePortInfo[u4PortNo].
                u2PortStatus;
            u1CtrlDir =
                (UINT1) pPnacPortInfo->authConfigEntry.u2AdminControlDir;

            u1AuthMode = pPnacPortInfo->u1PortAuthMode;

            /* Supplicant address is always passed as NULL. */

            if (PnacPnacMbsmHwSetAuthStatus ((UINT2) u4PortNo, NULL, u1AuthMode,
                                             u1AuthStatus, u1CtrlDir, pSlotInfo)
                == FNP_FAILURE)
            {
                return MBSM_FAILURE;
            }
        }
        u4PortCount--;
        u4PortNo++;
    }
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacMbsmProcessUpdateMessage                         */
/*                                                                           */
/* Description        : This function constructs the PNAC Q Mesg and calls   */
/*                      the PNAC CONTROL event to process this Mesg.         */
/*                                                                           */
/*                      This function will be called from the MBSM module    */
/*                      when an indication for the Card Insertion is         */
/*                      received.                                            */
/*                                                                           */
/* Input(s)           :  tMbsmProtoMsg - Structure containing the SlotInfo,  */
/*                                       PortInfo and the Protocol Id.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
PnacMbsmProcessUpdateMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{
    tPnacIntfMesg      *pMesg = NULL;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;

    if (pProtoMsg == NULL)
        return MBSM_FAILURE;

    if (PNAC_IS_SHUTDOWN ())
    {
        MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
        MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
        MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
        MbsmSendAckFromProto (&MbsmProtoAckMsg);
        return MBSM_SUCCESS;
    }

    if ((pMesg = (tPnacIntfMesg *)
         PNAC_ALLOC_MEM_BLOCK (PNAC_INTF_CFGQ_MESG_MEMPOOL_ID)) == NULL)
    {

        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " Allocate mem block for Pnac Interface Message failed\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gPnacSystemInfo.i4SysLogId,
                      " Allocate mem block for Pnac Interface Message failed"));
        return MBSM_FAILURE;
    }

    if (!(pMesg->Mesg.MbsmCardUpdate.pMbsmProtoMsg =
          MEM_MALLOC (sizeof (tMbsmProtoMsg), tMbsmProtoMsg)))
    {
        PNAC_RELEASE_MEM_BLOCK (PNAC_INTF_CFGQ_MESG_MEMPOOL_ID, pMesg);
        return MBSM_FAILURE;
    }
    pMesg->u4MesgType = (UINT4) i4Event;
    MEMCPY (pMesg->Mesg.MbsmCardUpdate.pMbsmProtoMsg, pProtoMsg,
            sizeof (tMbsmProtoMsg));

    if (PNAC_SEND_TO_QUEUE (PNAC_INTF_CFG_QID, (UINT1 *) &pMesg,
                            OSIX_DEF_MSG_LEN) == PNAC_OSIX_SUCCESS)
    {
        if (PNAC_SEND_EVENT (PNAC_INTF_TASK_ID, PNAC_CFGQ_EVENT)
            != PNAC_OSIX_SUCCESS)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
                      PNAC_CONTROL_PATH_TRC, " INF: Send Event failed !!!\n");
        }
        return MBSM_SUCCESS;
    }

    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_OS_RESOURCE_TRC |
              PNAC_CONTROL_PATH_TRC, " INF: Send To Q failed !!!\n");

    MEM_FREE (pMesg->Mesg.MbsmCardUpdate.pMbsmProtoMsg);
    PNAC_RELEASE_MEM_BLOCK (PNAC_INTF_CFGQ_MESG_MEMPOOL_ID, pMesg);

    return MBSM_FAILURE;
}
