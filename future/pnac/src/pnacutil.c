/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pnacutil.c,v 1.43 2017/09/04 10:53:17 siva Exp $
 *
 * Description     : This file contains utillity 
 *                   routine for PNAC module
 *******************************************************************/

#include "pnachdrs.h"

/*****************************************************************************/
/* Function Name      : PnacAuthCalcSessionTblIndex                          */
/*                                                                           */
/* Description        : This function calculates Session Hash table index    */
/*                                                                           */
/* Input(s)           : pAddr - MAC address to be searched                   */
/*                                                                           */
/* Output(s)          : pu4HashIdx - Pointer to Hash index                   */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacAuthCalcSessionTblIndex (tMacAddr macAddr, UINT4 *pu4HashIdx)
{
    UINT4               u2MacAddr = 0;
    /*  Computation of Hash Index can be modified here */

    PNAC_GET_TWOBYTE (u2MacAddr, &(macAddr[4]));
    if (pu4HashIdx == NULL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  "Empty hashindex pointer passed to the function"
                  "calculating session table Index");
        return;
    }
    *pu4HashIdx = (UINT4) (u2MacAddr % PNAC_SESS_TABLE_SIZE);
    return;
}

/*****************************************************************************/
/* Function Name      : PnacAuthGetSessionTblEntryByMac                      */
/*                                                                           */
/* Description        : This function gets Session table entry by MAC addr   */
/*                                                                           */
/* Input(s)           : pMacAddr - MAC address                               */
/*                                                                           */
/* Output(s)          : ppAuthSessNode - Pointer to the Pointer to Session   */
/*                                 table entry to be got                     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

INT4
PnacAuthGetSessionTblEntryByMac (tMacAddr macAddr,
                                 tPnacAuthSessionNode ** ppAuthSessNode)
{
    tPnacAuthSessionNode *pNode = NULL;
    UINT4               u4Index = PNAC_INIT_VAL;

    PnacAuthCalcSessionTblIndex (macAddr, &u4Index);
    pNode = (tPnacAuthSessionNode *) PNAC_HASH_GET_FIRST_BUCKET_NODE
        (PNAC_AUTH_SESSION_TABLE, u4Index);
    while (pNode != NULL)
    {
        if (PNAC_COMPARE_MAC_ADDR (pNode->rmtSuppMacAddr, macAddr))
        {
            *ppAuthSessNode = pNode;
            return PNAC_SUCCESS;
        }
        pNode = (tPnacAuthSessionNode *) PNAC_HASH_GET_NEXT_BUCKET_NODE
            (PNAC_AUTH_SESSION_TABLE, u4Index, &pNode->nextAuthSessionNode);
    }

    return PNAC_FAILURE;

}

/*****************************************************************************/
/* Function Name      : PnacCompareFailedMacAddr                             */
/*                                                                           */
/* Description        : This function compares the two NpFailedMacSessNode   */
/*                      based on the MAC Addresses                           */
/*                                                                           */
/* Input(s)           : e1        Pointer to First NpFailedMacSessNode       */
/* Input(s)           : e2        Pointer to Second NpFailedMacSessNode      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1. PNAC_GREATER - If key of first element is greater */
/*                                        than key of second element         */
/*                      2. PNAC_EQUAL   - If key of first element is equal   */
/*                                        to key of second element           */
/*                      3. PNAC_LESSER  - If key of first element is lesser  */
/*                                        than key of second element         */
/*****************************************************************************/
INT4
PnacCompareFailedMacAddr (tRBElem * e1, tRBElem * e2)
{
    tPnacNpFailedMacSessNode *pNode1 = e1;
    tPnacNpFailedMacSessNode *pNode2 = e2;

    INT4                i4RetVal = PNAC_NO_VAL;

    i4RetVal = MEMCMP (pNode1->rmtSuppMacAddr, pNode2->rmtSuppMacAddr,
                       sizeof (tMacAddr));

    if (i4RetVal > PNAC_NO_VAL)
    {
        return PNAC_GREATER;
    }
    else if (i4RetVal < PNAC_NO_VAL)
    {
        return PNAC_LESSER;
    }

    return PNAC_EQUAL;
}

/*****************************************************************************/
/* Function Name      : PnacGetNpFailedMacSessNodeByMac                      */
/*                                                                           */
/* Description        : This function gets Mac Session Node failed in        */
/*                      NP Programming by Mac Address                        */
/*                                                                           */
/* Input(s)           : macAddr - MAC address                                */
/*                                                                           */
/* Output(s)          : ppNpFailedMacSessNode - Pointer to the Pointer to    */
/*                                              NpFailedMacSessNode          */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
PnacGetNpFailedMacSessNodeByMac (tMacAddr macAddr,
                                 tPnacNpFailedMacSessNode
                                 ** ppNpFailedMacSessNode)
{
    tPnacNpFailedMacSessNode *pNpFailedMacSessNode = NULL;
    tPnacNpFailedMacSessNode NpFailedMacSessNode;

    MEMSET (&NpFailedMacSessNode, PNAC_NO_VAL,
            sizeof (tPnacNpFailedMacSessNode));

    MEMCPY (NpFailedMacSessNode.rmtSuppMacAddr, macAddr, sizeof (tMacAddr));

    pNpFailedMacSessNode = (tPnacNpFailedMacSessNode *)
        RBTreeGet (gPnacSystemInfo.PnacNpFailedMacSess, &NpFailedMacSessNode);

    if (pNpFailedMacSessNode == NULL)
    {
        return PNAC_FAILURE;
    }

    *ppNpFailedMacSessNode = pNpFailedMacSessNode;
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacAddToNpFailedMacSessTree                         */
/*                                                                           */
/* Description        : This function gets Mac Session Node failed in        */
/*                      NP Programming by Mac Address                        */
/*                                                                           */
/* Input(s)           : macAddr - MAC address                                */
/*                      u1PnacAsyncNpRetryCount - Retry Count                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
tPnacNpFailedMacSessNode *
PnacAddToNpFailedMacSessTree (tMacAddr macAddr, UINT1 u1PnacAsyncNpRetryCount)
{
    tPnacNpFailedMacSessNode *pNpFailedMacSessNode = NULL;

    pNpFailedMacSessNode = (tPnacNpFailedMacSessNode *)
        MemAllocMemBlk (PNAC_AUTHSESSNODE_FAIL_MEMPOOL_ID);

    if (pNpFailedMacSessNode == NULL)
    {
        return NULL;
    }

    MEMSET (pNpFailedMacSessNode, 0, sizeof (tPnacNpFailedMacSessNode));

    MEMCPY (pNpFailedMacSessNode->rmtSuppMacAddr, macAddr, sizeof (tMacAddr));
    pNpFailedMacSessNode->u1PnacAsyncNpRetryCount = u1PnacAsyncNpRetryCount;

    if (RBTreeAdd (gPnacSystemInfo.PnacNpFailedMacSess, pNpFailedMacSessNode)
        == RB_FAILURE)
    {
        MemReleaseMemBlock (PNAC_AUTHSESSNODE_FAIL_MEMPOOL_ID,
                            (UINT1 *) pNpFailedMacSessNode);
        return NULL;
    }

    return (pNpFailedMacSessNode);
}

/*****************************************************************************/
/* Function Name      : PnacDelFromNpFailedMacSessTree                       */
/*                                                                           */
/* Description        : This function gets Mac Session Node failed in        */
/*                      NP Programming by Mac Address                        */
/*                                                                           */
/* Input(s)           : pNpFailedMacSessNode - Pointer to                    */
/*                                             tPnacNpFailedMacSessNode      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
PnacDelFromNpFailedMacSessTree (tPnacNpFailedMacSessNode * pNpFailedMacSessNode)
{
    if (RBTreeRemove (gPnacSystemInfo.PnacNpFailedMacSess,
                      pNpFailedMacSessNode) == RB_FAILURE)
    {
        return PNAC_FAILURE;
    }

    MemReleaseMemBlock (PNAC_AUTHSESSNODE_FAIL_MEMPOOL_ID,
                        (UINT1 *) pNpFailedMacSessNode);
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacAuthGetFirstValidSuppAddress                     */
/*                                                                           */
/* Description        : This function gets the first Valid Supplicant MAC    */
/*                      address in Session hash table                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : macAddr - MAC address                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/

INT4
PnacAuthGetFirstValidSuppAddress (tMacAddr macAddr)
{
    UINT4               u4Index = PNAC_INIT_VAL;
    UINT2               u2Count = PNAC_INIT_VAL;
    UINT4               u4MacAddr = 0;
    UINT2               u2MacAddr = 0;

    tPnacAuthSessionNode *pNode = NULL;
    PNAC_HASH_SCAN_TBL (PNAC_AUTH_SESSION_TABLE, u4Index)
    {
        pNode = (tPnacAuthSessionNode *) PNAC_HASH_GET_FIRST_BUCKET_NODE
            (PNAC_AUTH_SESSION_TABLE, u4Index);
        while (pNode != NULL)
        {
            PNAC_MEMCPY (macAddr, pNode->rmtSuppMacAddr, PNAC_MAC_ADDR_SIZE);
            return PNAC_SUCCESS;
        }

    }

    /* Get From AIP Table after trying to get
     * from session hash table.
     */
    PNAC_DEBUG (PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                          " UTIL: Trying to Find First Node from AIP Tbl\n");
        );
    pNode = PNAC_AIP_ENTRY (u2Count);
    while (pNode == NULL)
    {
        if (++u2Count >= PNAC_MAX_SESS_IN_PROGRESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                      " UTIL: Unable to Find First Node in AIP Tbl\n");
            return PNAC_FAILURE;
        }
        pNode = PNAC_AIP_ENTRY (u2Count);
    }
    PNAC_MEMCPY (macAddr, pNode->rmtSuppMacAddr, PNAC_MAC_ADDR_SIZE);
    PNAC_GET_FOURBYTE (u4MacAddr, pNode->rmtSuppMacAddr);
    PNAC_GET_TWOBYTE (u2MacAddr, &(pNode->rmtSuppMacAddr[4]));
    PNAC_DEBUG (PNAC_TRC_ARG2 (PNAC_CONTROL_PATH_TRC,
                               " UTIL: Found First Node from AIP Tbl With Mac: %x-%x \n",
                               u4MacAddr, u2MacAddr);
        );
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacAuthGetNextValidSuppAddress                      */
/*                                                                           */
/* Description        : This function gets the next valid Supplicant's MAC   */
/*                      address occurring to a reference MAC address         */
/*                                                                           */
/* Input(s)           : refMacAddr - reference Supp address                  */
/*                                                                           */
/* Output(s)          : nextMacAddr - next Supp's MAC address                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/

INT4
PnacAuthGetNextValidSuppAddress (tMacAddr refMacAddr, tMacAddr nextMacAddr)
{
    tPnacAuthSessionNode *pNode = NULL;
    BOOL1               bRefNodeFound = PNAC_FALSE;
    UINT4               u4Index = PNAC_INIT_VAL;
    UINT2               u2Count = PNAC_INIT_VAL;
    UINT4               u4MacAddr = 0;
    UINT2               u2MacAddr = 0;
    UINT1               au1MacAddr[PNAC_MAC_ADDR_SIZE] =
        { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x03 };

    PnacAuthCalcSessionTblIndex (refMacAddr, &u4Index);
    pNode = (tPnacAuthSessionNode *) PNAC_HASH_GET_FIRST_BUCKET_NODE
        (PNAC_AUTH_SESSION_TABLE, u4Index);
    while (pNode != NULL)
    {
        if (PNAC_COMPARE_MAC_ADDR (pNode->rmtSuppMacAddr, refMacAddr))
        {
            /* Reference Node is found now - So now get the next node */
            pNode = (tPnacAuthSessionNode *) PNAC_HASH_GET_NEXT_BUCKET_NODE
                (PNAC_AUTH_SESSION_TABLE, u4Index, &pNode->nextAuthSessionNode);
            while (pNode == NULL)
            {
                /* Since the next node is null get the first node
                 * in the next bucket 
                 */
                if (++u4Index >= PNAC_SESS_TABLE_SIZE)
                {
                    /* Get From AIP Table after trying to get
                     * from session hash table.
                     */
                    PNAC_DEBUG (PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                                          " UTIL: Trying to get the Next Valid "
                                          " Node from AIP Tbl(after STBL)\n");
                        );
                    while ((pNode = PNAC_AIP_ENTRY (u2Count)) == NULL)
                    {
                        if (++u2Count >= PNAC_MAX_SESS_IN_PROGRESS)
                        {
                            PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                                      " UTIL: Unable to Find Node in AIP Tbl(after STBL)\n");
                            return PNAC_FAILURE;
                        }
                    }
                    PNAC_MEMCPY (nextMacAddr, pNode->rmtSuppMacAddr,
                                 PNAC_MAC_ADDR_SIZE);
                    PNAC_GET_FOURBYTE (u4MacAddr, pNode->rmtSuppMacAddr);
                    PNAC_GET_TWOBYTE (u2MacAddr, &(pNode->rmtSuppMacAddr[4]));
                    PNAC_DEBUG (PNAC_TRC_ARG2 (PNAC_CONTROL_PATH_TRC,
                                               " UTIL: Found Next Node with Mac(after STBL): %x-%x \n",
                                               u4MacAddr, u2MacAddr);
                        );
                    return PNAC_SUCCESS;
                }
                pNode = (tPnacAuthSessionNode *)
                    PNAC_HASH_GET_FIRST_BUCKET_NODE (PNAC_AUTH_SESSION_TABLE,
                                                     u4Index);

            }                    /* While pNode == NULL */

            /******************************/
            /* Now the node is available. */
            /* Copy the mac address       */
            /******************************/

            PNAC_MEMCPY (nextMacAddr, pNode->rmtSuppMacAddr,
                         PNAC_MAC_ADDR_SIZE);
            return PNAC_SUCCESS;
        }                        /* end if */
        pNode = (tPnacAuthSessionNode *) PNAC_HASH_GET_NEXT_BUCKET_NODE
            (PNAC_AUTH_SESSION_TABLE, u4Index, &pNode->nextAuthSessionNode);
    }
    /* Get From AIP Table after trying to get
     * from session hash table.
     */
    /* Reference node is not found yet */
    PNAC_DEBUG (PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                          " UTIL: Trying to get the Reference Node from "
                          " AIP Tbl\n");
        );
    for (; u2Count < PNAC_MAX_SESS_IN_PROGRESS; u2Count++)
    {
        if ((pNode = PNAC_AIP_ENTRY (u2Count)) == NULL)
        {
            continue;
        }
        if (bRefNodeFound != PNAC_TRUE)
        {
            if (PNAC_COMPARE_MAC_ADDR (pNode->rmtSuppMacAddr, refMacAddr))
            {
                /* Reference Node is found now - So now get the next node */
                bRefNodeFound = PNAC_TRUE;
                PNAC_DEBUG (PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                                      " UTIL: Found Reference Node from AIP Tbl\n");
                    );
            }
            continue;
        }
        else
        {
            PNAC_GET_FOURBYTE (u4MacAddr, pNode->rmtSuppMacAddr);
            PNAC_GET_TWOBYTE (u2MacAddr, &(pNode->rmtSuppMacAddr[4]));
            PNAC_DEBUG (PNAC_TRC_ARG2 (PNAC_CONTROL_PATH_TRC,
                                       " UTIL: Found Next Node with Mac: %x-%x \n",
                                       u4MacAddr, u2MacAddr);
                );
            PNAC_MEMCPY (nextMacAddr, pNode->rmtSuppMacAddr,
                         PNAC_MAC_ADDR_SIZE);
            if (PNAC_COMPARE_MAC_ADDR (nextMacAddr, au1MacAddr))
            {
                return PNAC_FAILURE;
            }
            return PNAC_SUCCESS;
        }
    }
    PNAC_DEBUG (PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                          " UTIL: Unable to Find Next Node ");
        );
    return PNAC_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PnacAuthAddToAuthInProgressTbl                       */
/*                                                                           */
/* Description        : This function initialises the Session ID table       */
/*                                                                           */
/* Input(s)           : pAuthSessNode - Session node to be added in AIP table*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/

INT4
PnacAuthAddToAuthInProgressTbl (tPnacAuthSessionNode * pAuthSessNode)
{
    UINT2               u2Count;

    for (u2Count = PNAC_MIN_SESS_IN_PROGRESS;
         u2Count < PNAC_MAX_SESS_IN_PROGRESS; u2Count++)
    {
        if (PNAC_AIP_ENTRY (u2Count) != NULL)
        {
            continue;
        }
        else
        {
            PNAC_AIP_ENTRY (u2Count) = pAuthSessNode;
            return PNAC_SUCCESS;
        }
    }

    return PNAC_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PnacAuthDeleteAuthInProgressTblEntry                 */
/*                                                                           */
/* Description        : This function deletes the entry in AIP table         */
/*                                                                           */
/* Input(s)           : pAuthSessNode - the entry to be deleted              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/

INT4
PnacAuthDeleteAuthInProgressTblEntry (tPnacAuthSessionNode * pAuthSessNode)
{
    UINT2               u2Count;

    for (u2Count = PNAC_MIN_SESS_IN_PROGRESS;
         u2Count < PNAC_MAX_SESS_IN_PROGRESS; u2Count++)
    {
        if (PNAC_AIP_ENTRY (u2Count) == pAuthSessNode)
        {
            PNAC_AIP_ENTRY (u2Count) = (tPnacAuthSessionNode *) NULL;
            return PNAC_SUCCESS;
        }
    }

    return PNAC_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PnacAuthGetAuthInProgressTblEntryByMac               */
/*                                                                           */
/* Description        : This function gets the AIP table entry by MAC addr   */
/*                                                                           */
/* Input(s)           : macAddr - MAC address                                */
/*                                                                           */
/* Output(s)          : pNode - AIP table entry                              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacAuthGetAuthInProgressTblEntryByMac (tMacAddr macAddr,
                                        tPnacAuthSessionNode ** ppNode)
{
    tPnacAuthSessionNode *pAipNode = NULL;
    UINT2               u2Count;

    for (u2Count = PNAC_MIN_SESS_IN_PROGRESS;
         u2Count < PNAC_MAX_SESS_IN_PROGRESS; u2Count++)
    {
        if ((pAipNode = PNAC_AIP_ENTRY (u2Count)) != NULL)
        {
            if (PNAC_COMPARE_MAC_ADDR (pAipNode->rmtSuppMacAddr, macAddr))
            {
                *ppNode = pAipNode;
                return PNAC_SUCCESS;
            }
        }
    }
    return PNAC_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PnacCpyMacAddrToStr                                  */
/*                                                                           */
/* Description        : Copies the MAC Address to the buffer as a string     */
/*                                                                           */
/*                                                                           */
/* Input(s)           : macAddr - Pointer to the MAC address which is to be  */
/*                      converted to the string.                             */
/*                                                                           */
/* Output(s)          : pu1Buf - Pointer to the buffer in which the string   */
/*                               is to be copied                             */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
PnacCpyMacAddrToStr (UINT1 *pu1Buf, tMacAddr macAddr)
{
    UINT4               u4MacAddr = 0;
    UINT2               u2MacAddr = 0;

    PNAC_GET_FOURBYTE (u4MacAddr, macAddr);
    PNAC_GET_TWOBYTE (u2MacAddr, &(macAddr[4]));
    PNAC_SPRINTF ((char *) pu1Buf, "%x-%x-%x-%x-%x-%x",
                  (UINT1) ((u4MacAddr >> 24) & PNAC_LSBYTE0_MASK),
                  (UINT1) ((u4MacAddr >> 16) & PNAC_LSBYTE0_MASK),
                  (UINT1) ((u4MacAddr >> 8) & PNAC_LSBYTE0_MASK),
                  (UINT1) ((u4MacAddr) & PNAC_LSBYTE0_MASK),
                  (UINT1) ((u2MacAddr >> 8) & PNAC_LSBYTE0_MASK),
                  (UINT1) ((u2MacAddr) & PNAC_LSBYTE0_MASK));
    return;
}

/*****************************************************************************/
/* Function Name      : PnacChkSuppMacIsUniqueMacForPort                     */
/*                                                                           */
/* Description        : This function will check whether the given source    */
/*                      mac is equivalent to the unique MAC derived for      */
/*                      MAC based mode.                                      */
/*                                                                           */
/* Input(s)           : macAddr - Mac which to be compared with unique Mac   */
/*                      u2PortNum - Port Num used for unique mac generation  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - If equal                              */
/*                      PNAC_FAILURE - If not equal                          */
/*****************************************************************************/

INT4
PnacChkSuppMacIsUniqueMacForPort (tMacAddr macAddr, UINT2 u2PortNum)
{
    UINT1               au1MacAddr[PNAC_MAC_ADDR_SIZE] =
        { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x00 };

    PNAC_DERIVE_UNIQ_MAC (au1MacAddr, u2PortNum);

    if (PNAC_COMPARE_MAC_ADDR (au1MacAddr, macAddr) == PNAC_TRUE)
    {
        return PNAC_SUCCESS;
    }

    return PNAC_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PnacGetPortEntry                                     */
/*                                                                           */
/* Description        : Gets the pointer to the port table entry             */
/*                                                                           */
/* Input              : u2PortNum - Port Number                              */
/*                                                                           */
/* Output             : ppPortInfo - double Pointer to the port table entry  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.aPnacPaePortInfo                     */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

INT4
PnacGetPortEntry (UINT2 u2PortNum, tPnacPaePortEntry ** ppPortInfo)
{
    tPnacPaePortEntry  *pTmpPortInfo = NULL;

    if ((u2PortNum < PNAC_MINPORTS) || (u2PortNum > PNAC_MAXPORTS))
    {
        PNAC_TRC_ARG2 (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                       " There are only %d-%d range of ports !! \n",
                       PNAC_MINPORTS, PNAC_MAXPORTS);
        return PNAC_FAILURE;
    }

    pTmpPortInfo = &(PNAC_PORT_INFO (u2PortNum));
    if (pTmpPortInfo->u1EntryStatus == PNAC_PORT_INVALID)
    {
        return PNAC_FAILURE;
    }
    *ppPortInfo = pTmpPortInfo;
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacGetHLPortStatus                                  */
/*                                                                           */
/* Description        : This function returns the port operational           */
/*                      status as updated by the PNAC module.                */
/*                      The port oper status will be UP/DOWN will be given   */
/*                      to the higher layers as follows:                     */
/*                      ControlDir / portStatus                              */
/*                      a. BOTH    / UNAUTH   --> oper down                  */
/*                      b. BOTH    / AUTH     --> oper up                    */
/*                      c. IN      / UNAUTH   --> oper up                    */
/*                      d. IN      / AUTH     --> oper up                    */
/*                                                                           */
/* Input(s)           : u2PortIndex - Port Number for which, the status is   */
/*                                    needed.                                */
/*                                                                           */
/* Output(s)          : pu1PortState - status of the link as known to higher */
/*                                     layer CFA_IF_UP/CFA_IF_DOWN           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_FAILURE                                         */
/*                      PNAC_SUCCESS                                         */
/*****************************************************************************/
INT4
PnacGetHLPortStatus (UINT2 u2PortIndex, UINT1 *pu1PortState)
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry (u2PortIndex, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (CONTROL_PLANE_TRC | PNAC_ALL_FAILURE_TRC,
                  "Such a port does not exist!! ... \n");
        return PNAC_FAILURE;
    }

    if (pPortInfo->u1EntryStatus == PNAC_PORT_OPER_UP)
    {
        if (!PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
        {
            *pu1PortState = CFA_IF_UP;
        }
        /* Check whether PNAC is disabled on the port */
        else if (pPortInfo->u1PortPaeStatus == PNAC_DISABLED)
        {
            *pu1PortState = CFA_IF_UP;
        }
        else if (PNAC_PORT_INFO (u2PortIndex).u1PortAuthMode !=
                 PNAC_PORT_AUTHMODE_PORTBASED)
        {

            /* Give Oper up if
               1. Atleast one supplicant is authorized
               2. Mode is ForceAuthorized */
            if (pPortInfo->portIfMap.u1IfType != CFA_CAPWAP_DOT11_BSS)
            {
                if ((pPortInfo->u2SuppCount >= PNAC_MINSUPP) ||
                    (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).
                     u2AuthControlPortControl ==
                     PNAC_PORTCNTRL_FORCEAUTHORIZED))
                {
                    *pu1PortState = CFA_IF_UP;
                }
                else
                {
                    *pu1PortState = CFA_IF_DOWN;
                }
            }
            else
            {
                *pu1PortState = CFA_IF_UP;
            }
        }
        else
        {
            if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2OperControlDir
                == PNAC_CNTRLD_DIR_IN)
            {
                *pu1PortState = CFA_IF_UP;
            }
            else
            {
                if (gPnacSystemInfo.aPnacPaePortInfo[u2PortIndex].u2PortStatus
                    == PNAC_PORTSTATUS_AUTHORIZED)

                {
                    *pu1PortState = CFA_IF_UP;
                }
                else
                {
                    *pu1PortState = CFA_IF_DOWN;
                }
            }
        }
    }
    else if (pPortInfo->u1EntryStatus == PNAC_PORT_OPER_DOWN)
    {
        if (!PNAC_IS_ENABLED ())
        {
            return PNAC_FAILURE;
        }
        else
        {
            *pu1PortState = CFA_IF_DOWN;
        }

    }
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacRestartReauthTimer                               */
/*                                                                           */
/* Description        : This function is called when user modified the       */
/*                      reauth period value.                                 */
/* Input(s)           : u4PresReAuthVal - Present value of the re-auth period*/
/*                      u4NewReAuthVal - New value configured for            */
/*                                       re-auth period                      */
/*                      pPortAuthSessNode - Authenticator session node which */
/*                                      stores the information related this  */
/*                                      session.                             */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacRestartReauthTimer (UINT4 u4PresReAuthVal, UINT4 u4NewReAuthVal,
                        tPnacAuthSessionNode * pPortAuthSessNode)
{
    UINT4               u4TmrPeriod = 0;
    UINT4               u4ElapsedPeriod;
    UINT4               u4RemainingPeriod;

    if (PnacTmrGetRemainingTime ((VOID *) pPortAuthSessNode,
                                 PNAC_REAUTHWHEN_TIMER,
                                 &u4RemainingPeriod) != PNAC_SUCCESS)
    {
        PNAC_TRC_ARG1 (PNAC_MGMT_TRC,
                       " UTIL: GetRemainingTime ReAuthWhen timer failed"
                       " for the port: %d \n", pPortAuthSessNode->u2PortNo);
        return PNAC_FAILURE;
    }

    if (u4RemainingPeriod == 0)
    {
        /* In this case need not stop and start the timer, the 
           timer will expire automatically and take the new value 
           configured upon timer expiry */
        return PNAC_SUCCESS;
    }

    u4ElapsedPeriod = u4PresReAuthVal - u4RemainingPeriod;

    if (u4NewReAuthVal > u4PresReAuthVal)
    {
        /* New value is greater than the old value */
        u4TmrPeriod = (u4NewReAuthVal - u4PresReAuthVal) + u4RemainingPeriod;
    }
    else
    {
        /* New value is less than the old value */
        if (u4ElapsedPeriod > u4NewReAuthVal)
        {
            /* Already the time is elapsed, expire immediately */
            u4TmrPeriod = 1;
        }
        else
        {
            /* Start the timer for remaining time */
            u4TmrPeriod = u4NewReAuthVal - u4ElapsedPeriod;
        }
    }
    if (u4TmrPeriod == 0)
    {
        /* In this case need not stop and start the timer, the 
           timer will expire automatically and take the new value 
           configured upon timer expiry */
        return PNAC_SUCCESS;
    }
    if (PnacTmrStopTmr ((VOID *) pPortAuthSessNode,
                        PNAC_REAUTHWHEN_TIMER) != PNAC_SUCCESS)
    {
        PNAC_TRC_ARG1 (PNAC_MGMT_TRC,
                       "UTIL: Stop ReAuthWhen timer returned failure \n"
                       " for the port: %d \n", pPortAuthSessNode->u2PortNo);
        return PNAC_FAILURE;
    }

    if (PnacTmrStartTmr ((VOID *) pPortAuthSessNode,
                         PNAC_REAUTHWHEN_TIMER, u4TmrPeriod) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " UTIL: Start ReAuthWhen timer returned failure \n");
        return PNAC_FAILURE;
    }

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacCheckIfPortStatusChanged                         */
/*                                                                           */
/* Description        : This function compares the port status and returns   */
/*                      Success if there is a change otherwise returns       */
/*                      Failure.                                             */
/* Input(s)           : u2PortNum - Port Number                              */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
PnacCheckIfPortStatusChanged (UINT2 u2PortNum)
{
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tPnacSuppSessionNode *pSuppSessNode = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacSuppConfigEntry *pSuppConfig = NULL;
    UINT2               u2CurrentPortStatus = PNAC_PORTSTATUS_UNAUTHORIZED;

    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (CONTROL_PLANE_TRC | PNAC_ALL_FAILURE_TRC,
                  "Such a port does not exist!! ... \n");
        return PNAC_FAILURE;
    }

    if (PNAC_IS_PORT_MODE_MACBASED (pPortInfo))
    {
        PNAC_TRC (CONTROL_PLANE_TRC | PNAC_ALL_FAILURE_TRC,
                  "PnacCheckIfPortStatusChanged: "
                  "Authentication mode is MacBased!! ... \n");

        /* Return Success for Mac based. Based on the Supplicant count
           port oper indication will be given. */

        if ((PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl !=
             PNAC_PORTCNTRL_AUTO) && (pPortInfo->pPortAuthSessionNode != NULL))
        {
            pPortInfo->u2PortStatus = pPortInfo->pPortAuthSessionNode->
                authFsmInfo.u2AuthControlPortStatus;
        }
        else
        {
            if (pPortInfo->u2SuppCount >= PNAC_MINSUPP)
            {
                pPortInfo->u2PortStatus = PNAC_PORTSTATUS_AUTHORIZED;
            }
            else
            {
                pPortInfo->u2PortStatus = PNAC_PORTSTATUS_UNAUTHORIZED;
            }
        }

        return PNAC_SUCCESS;
    }

    if (pPortInfo->pPortAuthSessionNode == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_DATA_PATH_TRC,
                  "No Auth Session Node found\n");
        return PNAC_FAILURE;
    }

    pAuthSessNode = pPortInfo->pPortAuthSessionNode;
    pSuppSessNode = pPortInfo->pPortSuppSessionNode;
    pSuppConfig = &(pPortInfo->suppConfigEntry);

    if (PNAC_IS_PORT_BOTH_CAPABLE (pPortInfo))
    {
        if (pSuppConfig->u2SuppAccessCtrlWithAuth ==
            PNAC_SUPP_ACCESS_CTRL_ACTIVE)
        {
            if ((pAuthSessNode->authFsmInfo.u2AuthControlPortStatus ==
                 PNAC_PORTSTATUS_AUTHORIZED) &&
                (pSuppSessNode->suppFsmInfo.u2ControlPortStatus ==
                 PNAC_PORTSTATUS_AUTHORIZED))
            {
                u2CurrentPortStatus = PNAC_PORTSTATUS_AUTHORIZED;
            }
            else
            {
                u2CurrentPortStatus = PNAC_PORTSTATUS_UNAUTHORIZED;
            }
        }
        else
        {
            u2CurrentPortStatus = pAuthSessNode->authFsmInfo.
                u2AuthControlPortStatus;
        }

    }
    else
    {
        if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
        {
            /* This port is only auth capable, hence the port
             * status depends upon port authenticator's status. */
            u2CurrentPortStatus =
                pAuthSessNode->authFsmInfo.u2AuthControlPortStatus;

        }
        else
        {
            /* This port is only supplicant capable. Hence consider
             * the port as authorized. */
            u2CurrentPortStatus = PNAC_PORTSTATUS_AUTHORIZED;
        }
    }

    if (pPortInfo->u2PortStatus != u2CurrentPortStatus)
    {
        pPortInfo->u2PortStatus = u2CurrentPortStatus;
        return PNAC_SUCCESS;
    }
    else
    {
        return PNAC_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name      : PnacGetCurrentPortStatus                             */
/*                                                                           */
/* Description        : This function gets the port status                   */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : u2CurrentPortStatus - Current Port Status            */
/*****************************************************************************/
INT1
PnacGetCurrentPortStatus (UINT2 u2PortNum)
{
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tPnacSuppSessionNode *pSuppSessNode = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacSuppConfigEntry *pSuppConfig = NULL;
    INT1                i1CurrentPortStatus = PNAC_PORTSTATUS_UNAUTHORIZED;

    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (CONTROL_PLANE_TRC | PNAC_ALL_FAILURE_TRC,
                  "Such a port does not exist!! ... \n");
        return PNAC_FAILURE;
    }

    if (PNAC_IS_PORT_MODE_MACBASED (pPortInfo))
    {
        PNAC_TRC (CONTROL_PLANE_TRC | PNAC_ALL_FAILURE_TRC,
                  "PnacGetCurrentPortStatus: "
                  "Authentication mode is MacBased!! ... \n");
        return PNAC_FAILURE;
    }

    pAuthSessNode = pPortInfo->pPortAuthSessionNode;
    pSuppSessNode = pPortInfo->pPortSuppSessionNode;
    pSuppConfig = &(pPortInfo->suppConfigEntry);

    if (PNAC_IS_PORT_BOTH_CAPABLE (pPortInfo))
    {
        if (pSuppConfig->u2SuppAccessCtrlWithAuth ==
            PNAC_SUPP_ACCESS_CTRL_ACTIVE)
        {
            if ((pAuthSessNode->authFsmInfo.u2AuthControlPortStatus ==
                 PNAC_PORTSTATUS_AUTHORIZED) &&
                (pSuppSessNode->suppFsmInfo.u2ControlPortStatus ==
                 PNAC_PORTSTATUS_AUTHORIZED))
            {
                i1CurrentPortStatus = PNAC_PORTSTATUS_AUTHORIZED;
            }
            else
            {
                i1CurrentPortStatus = PNAC_PORTSTATUS_UNAUTHORIZED;
            }
        }
        else
        {
            i1CurrentPortStatus = (INT1) pAuthSessNode->authFsmInfo.
                u2AuthControlPortStatus;
        }

    }
    else
    {
        if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
        {
            /* This port is only auth capable, hence the port
             * status depends upon port authenticator's status. */
            i1CurrentPortStatus =
                (INT1) pAuthSessNode->authFsmInfo.u2AuthControlPortStatus;

        }
        else
        {
            /* This port is only supplicant capable. Hence consider
             * the port as authorized. */
            i1CurrentPortStatus = PNAC_PORTSTATUS_AUTHORIZED;
        }
    }

    return i1CurrentPortStatus;
}

/*****************************************************************************/
/* Function Name      : PnacLowTestPortAuthMode                              */
/*                                                                           */
/* Description        : This function tests whether the given auth mode      */
/*                      can be set for the given port.                       */
/*                                                                           */
/* Input(s)           : u2Port - Port Number                                 */
/*                    : i4AuthMode - Auth mode to be set.                    */
/*                                                                           */
/* Output(s)          : pu4ErrorCode - Error code returned.                  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS / SNMP_FAILURE                          */
/*****************************************************************************/
INT1
PnacLowTestPortAuthMode (UINT4 *pu4ErrorCode, UINT2 u2Port, INT4 i4AuthMode)
{

    tPnacPaePortEntry  *pPortInfo = NULL;

    if ((i4AuthMode != PNAC_PORT_AUTHMODE_PORTBASED) &&
        (i4AuthMode != PNAC_PORT_AUTHMODE_MACBASED))
    {

        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPPROP: Use only"
                  " PNAC_PORT_AUTHMODE_PORTBASED or PNAC_PORT_AUTHMODE_MACBASED "
                  "for setting PortAuth mode \n");

        return SNMP_FAILURE;
    }

    if (PnacGetPortEntry (u2Port, &pPortInfo) != PNAC_SUCCESS)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  "Such a port does not exist!! ... \n");
        return SNMP_FAILURE;
    }

    if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_NO_CREATION;
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: This object exist only for Authenticator-capable "
                  "port !!\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacLowSetPortAuthMode                               */
/*                                                                           */
/* Description        : This function sets the given auth mode to the given  */
/*                      port if the required conditions are met.             */
/*                                                                           */
/* Input(s)           : u2Port - Port Number                                 */
/*                    : i4AuthMode - Auth mode to be set.                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS / SNMP_FAILURE                          */
/*****************************************************************************/
INT1
PnacLowSetPortAuthMode (UINT2 u2Port, INT4 i4PortAuthMode)
{

    tPnacPaePortEntry  *pPortInfo = NULL;
    UINT1               u1PortStatusFlag = PNAC_FALSE;

    if (PnacGetPortEntry (u2Port, &pPortInfo) != PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (pPortInfo->u1PortAuthMode == (UINT1) i4PortAuthMode)
    {
        PNAC_TRC (PNAC_MGMT_TRC, " SNMPPROP: Value Configured already \n");
        return SNMP_SUCCESS;
    }

    if ((PNAC_IS_ENABLED ()) && (pPortInfo->u1PortPaeStatus == PNAC_ENABLED))
    {
        if (PNAC_PORT_INFO (u2Port).u1EntryStatus == PNAC_PORT_OPER_UP)
        {
            if (PnacProcPortStatUpdEvent (u2Port, PNAC_PORT_OPER_DOWN) !=
                PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                          " PAE: Not able to change the Port Operational "
                          "status\n");
                return SNMP_FAILURE;
            }
            u1PortStatusFlag = PNAC_TRUE;
        }
        else if (i4PortAuthMode == PNAC_PORT_AUTHMODE_PORTBASED)
        {
            /* Delete all the sessions */
            if (PnacAuthDisableMacBasedPort (u2Port) != PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " AuthLinkStatusUpdate: DisableMacBasedPort Failed\n");
                return PNAC_FAILURE;
            }
        }
#ifdef NPAPI_WANTED
        if (PNAC_IS_NP_PROGRAMMING_ALLOWED () == PNAC_TRUE)
        {
            /* Indicate Authentication mode change to NPAPI */

            if ((PNAC_IS_PORT_MODE_MACBASED (pPortInfo) &&
                 (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl !=
                  PNAC_PORTCNTRL_FORCEAUTHORIZED)))
            {
                if (PnacPnacHwSetAuthStatus (u2Port,
                                             NULL,
                                             pPortInfo->u1PortAuthMode,
                                             PNAC_UNBLOCK_PORT,
                                             PNAC_CNTRLD_DIR_BOTH) !=
                    FNP_SUCCESS)
                {
                    PnacNpFailNotifyAndLog (u2Port,
                                            NULL,
                                            PNAC_CNTRLD_DIR_BOTH,
                                            pPortInfo->u1PortAuthMode,
                                            PNAC_UNBLOCK_PORT);
                    return PNAC_FAILURE;

                }                /* Hw call */
            }                    /* Mac based mode */
        }                        /* NP PROG Allowed */
#endif /* NPAPI_WANTED */

    }

    if (pPortInfo->pPortAuthSessionNode != NULL)
    {
        PnacAuthDeleteSession (pPortInfo->pPortAuthSessionNode);
        pPortInfo->pPortAuthSessionNode = NULL;
    }

    pPortInfo->u1PortAuthMode = (UINT1) i4PortAuthMode;

    if (pPortInfo->pPortAuthSessionNode != NULL)
    {
        PnacAuthDeleteSession (pPortInfo->pPortAuthSessionNode);
        pPortInfo->pPortAuthSessionNode = NULL;
    }
    if (PnacInitAuthenticator (u2Port) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_MGMT_TRC,
                  " SNMPPROP: Initialize Authenticator returned failure\n");
    }
    if (i4PortAuthMode == PNAC_PORT_AUTHMODE_MACBASED)
    {
        PNAC_TRC (PNAC_MGMT_TRC,
                  " SNMPPROP: Port Auth mode is set MAC-based\n");
        if ((PNAC_IS_ENABLED ())
            && (pPortInfo->u1PortPaeStatus == PNAC_ENABLED))
        {
#ifdef NPAPI_WANTED
            if (PNAC_IS_NP_PROGRAMMING_ALLOWED () == PNAC_TRUE)
            {
                if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).
                    u2AuthControlPortControl == PNAC_PORTCNTRL_AUTO)
                {
                    /* Indicate Authentication mode change to NPAPI */
                    if (PnacPnacHwSetAuthStatus (u2Port,
                                                 NULL,
                                                 pPortInfo->u1PortAuthMode,
                                                 PNAC_BLOCK_PORT,
                                                 PNAC_CNTRLD_DIR_BOTH) !=
                        FNP_SUCCESS)
                    {
                        PnacNpFailNotifyAndLog (u2Port,
                                                NULL,
                                                PNAC_CNTRLD_DIR_BOTH,
                                                pPortInfo->u1PortAuthMode,
                                                PNAC_BLOCK_PORT);
                        return PNAC_FAILURE;

                    }            /* Hw call */
                }                /* Auto mode */
            }                    /* NP PROG Allowed */
#endif /* NPAPI_WANTED */
        }

        /* Update the Port status as Un authorized if dot1x is enabled
           and port-control mode is auto */

        if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl ==
            PNAC_PORTCNTRL_AUTO)
        {
            pPortInfo->u2PortStatus = PNAC_PORTSTATUS_UNAUTHORIZED;
        }
    }
    else if (i4PortAuthMode == PNAC_PORT_AUTHMODE_PORTBASED)
    {
        /**************************************************************/
        /* Get all the session nodes belonging to this port and       */
        /* call delete the existing Mac-Sessions on this port         */
        /**************************************************************/
#ifdef NPAPI_WANTED
        if (PNAC_IS_NP_PROGRAMMING_ALLOWED () == PNAC_TRUE)
        {
            if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl ==
                PNAC_PORTCNTRL_AUTO)
            {
                /* Indicate Authentication mode change to NPAPI */
                if (PnacPnacHwSetAuthStatus (u2Port,
                                             NULL,
                                             pPortInfo->u1PortAuthMode,
                                             PNAC_PORTSTATUS_UNAUTHORIZED,
                                             PNAC_CNTRLD_DIR_BOTH) !=
                    FNP_SUCCESS)
                {
                    PnacNpFailNotifyAndLog (u2Port,
                                            NULL,
                                            PNAC_CNTRLD_DIR_BOTH,
                                            pPortInfo->u1PortAuthMode,
                                            PNAC_PORTSTATUS_UNAUTHORIZED);
                    return PNAC_FAILURE;

                }                /* Hw call */
            }                    /* Auto mode */
        }                        /* NP PROG Allowed */
#endif /* NPAPI_WANTED */

        PNAC_TRC (PNAC_MGMT_TRC,
                  " SNMPPROP: Port Auth mode is set Port-based\n");
    }

    /* Auth Mode is getting changed, so set the admin control direction
     * Supplicant Access Control, supplicant timers and 
     * Max-Start to default values. */
    PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AdminControlDir =
        (UINT2) PNAC_CNTRLD_DIR_BOTH;
    pPortInfo->suppConfigEntry.u2SuppAccessCtrlWithAuth =
        (UINT2) PNAC_SUPP_ACCESS_CTRL_INACTIVE;
    pPortInfo->suppConfigEntry.u4AuthPeriod = PNAC_AUTHPERIOD;
    pPortInfo->suppConfigEntry.u4HeldPeriod = PNAC_HELDPERIOD;
    pPortInfo->suppConfigEntry.u4StartPeriod = PNAC_STARTPERIOD;
    pPortInfo->suppConfigEntry.u4MaxStart = PNAC_MAXSTART;

    if ((PNAC_IS_ENABLED ()) && (pPortInfo->u1PortPaeStatus == PNAC_ENABLED))
    {

        /* intimate to CDSM */
        if (PnacPaeCdStateMachine (PNAC_CDSM_EV_ADMINCHANGED,
                                   u2Port) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                      " SNMPSTD: CDSM returned failure\n");
            return SNMP_FAILURE;
        }

        /* If the port is oper up, then update that. */
        if (u1PortStatusFlag == PNAC_TRUE)
        {
            if (PnacProcPortStatUpdEvent (u2Port, PNAC_PORT_OPER_UP) !=
                PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                          " PAE: Not able to change the Port Operational "
                          "status\n");
                return SNMP_FAILURE;
            }
        }
    }
    /* Set the auth mode in L2Iwf. */
    if (PnacL2IwfSetPortPnacAuthMode (u2Port, (UINT2) i4PortAuthMode)
        == L2IWF_FAILURE)
    {
        return SNMP_FAILURE;

    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacLowTestPortAuthControl                           */
/*                                                                           */
/* Description        : This function tests whether the given auth control   */
/*                      can be set for the given port.                       */
/*                                                                           */
/* Input(s)           : u2Port - Port Number                                 */
/*                    : u2AuthControl - Auth control to be set.              */
/*                                                                           */
/* Output(s)          : pu4ErrorCode - Error code returned.                  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS / SNMP_FAILURE                          */
/*****************************************************************************/
INT1
PnacLowTestPortAuthControl (UINT4 *pu4ErrorCode, UINT2 u2Port,
                            UINT2 u2AuthControl)
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry (u2Port, &pPortInfo) != PNAC_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /*Port Based Auth Mode */
    if ((u2AuthControl != (INT4) PNAC_PORTCNTRL_FORCEUNAUTHORIZED) &&
        (u2AuthControl != (INT4) PNAC_PORTCNTRL_AUTO) &&
        (u2AuthControl != (INT4) PNAC_PORTCNTRL_FORCEAUTHORIZED))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: Use only PNAC_PORTCNTRL_AUTO or"
                  " PNAC_PORTCNTRL_FORCEUNAUTHORIZED or"
                  " PNAC_PORTCNTRL_FORCEAUTHORIZED for setting"
                  " 'AuthControlledPortControl\n");
        return SNMP_FAILURE;
    }

    if (PnacLaIsLaStarted () == LA_TRUE)
    {
        if (u2AuthControl == (INT4) PNAC_PORTCNTRL_FORCEAUTHORIZED)
        {
            /* No need to do trunk related check 
             * Since Force authorise does not block any port
             */
            return SNMP_SUCCESS;
        }

        if (PnacL2IwfIsPortInPortChannel ((UINT4) u2Port) == L2IWF_SUCCESS)
        {

            /* If LACP is enabled and dot1x is not
             * force authorized return failure
             */
            CLI_SET_ERR (CLI_PNAC_LACP_ENABLED);
            *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacLowSetPortAuthControl                            */
/*                                                                           */
/* Description        : This function sets the given auth control to the     */
/*                      given port if the required conditions are met.       */
/*                                                                           */
/* Input(s)           : u2Port - Port Number                                 */
/*                    : u2AuthControl - Auth control to be set.              */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS / SNMP_FAILURE                          */
/*****************************************************************************/
INT1
PnacLowSetPortAuthControl (UINT2 u2Port, UINT2 u2AuthControl)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacDPnacConsPortEntry *pConsPortEntry = NULL;
    UINT4               u4SlotId = 0;
    UINT2               u2PrevAuthControl;
    UINT1               u1PortStatusFlag = PNAC_FALSE;

    if (PnacGetPortEntry (u2Port, &pPortInfo) != PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl ==
        u2AuthControl)
    {
        PNAC_TRC (PNAC_MGMT_TRC, " SNMPPROP: Value Configured already \n");
        return SNMP_SUCCESS;
    }

    /* If the AuthControlPortControl changes from Auto to ForceAuthorized / 
     * ForceUnAuthorized, then syncup the information to standby. 
     * If this information is not synced then the following problem may 
     * arise:
     *    -- Port 1 - Auth Contol set to AUTO
     *    -- Port 1 - Becomes Authorized and this info is synced up.
     *    -- Port 1 - Auth Control set to ForceUnAuthorized.
     *    -- Port 1 - Auth Control set to AUTO and before the sync up
     *                message reaches standby node, switch over happens.
     *    In this case after switch over port 1 will be come Authorized 
     *    (based on the very old info synced up when the port 1 was in 
     *    AUTO and authorized state). But Port 1 should be unauthorized. 
     *  Hence syncup this info. After getting this informatin standby will 
     *  flush the dynamic info present for this port.*/

    if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl ==
        PNAC_PORTCNTRL_AUTO)
    {
        PnacRedSyncUpModOrPortClearInfo ((UINT4) u2Port);
        if (pPortInfo->u1EntryStatus == PNAC_PORT_OPER_UP)
        {
            PnacRedSyncUpPortOperUpStatus ((UINT4) u2Port);
        }
    }

    u2PrevAuthControl =
        PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl;

    PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl = u2AuthControl;

    PnacL2IwfSetPortPnacAuthControl (u2Port, u2AuthControl);

    if (PNAC_IS_ENABLED ())
    {
        if (PNAC_IS_PORT_MODE_MACBASED (pPortInfo))
        {
            /* --------------------------------------------------------------------|
               |  | Previous Mode  | Current Mode  | Action                        |
               |  |----------------------------------------------------------------|
               |  |  ForceAuth/    |  Auto         | Delete and create 802.1x mac  |
               |  |                |               | default session and Init.     |
               |1 |  ForceUnAuth   |               | Session will be deleted on    |
               |  |                |               | EAP-start receive.            |
               |--|----------------|---------------|-------------------------------|
               |  |  Auto          |  Force Auth/  | Delete all sessions.          |
               |2 |                |  ForceUnAuth  | Create 802.1x Destination Mac |
               |  |                |               | session and init              |
               |--|----------------|---------------|-------------------------------|
               |  |                |               |                               |
               |3 |  ForceAuth/    | ForceAuth/    | Change only port control mode |
               |  |  ForceUnAuth   | ForceUnAuth   |                               |
               |------------------------------------------------------------------ */

            /* Case 1 */

            if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl ==
                PNAC_PORTCNTRL_AUTO)
            {

                /* Update the Port status as Un authorized if dot1x is enabled
                   and port-control mode is auto */

                if (pPortInfo->u1PortPaeStatus == PNAC_ENABLED)
                {
                    pPortInfo->u2PortStatus = PNAC_PORTSTATUS_UNAUTHORIZED;
                }

                /* Delete the session with 802.1x mac
                   if current mode is auto and previous mode is forceauth/forceunauth */

                if (u2PrevAuthControl != PNAC_PORTCNTRL_AUTO)
                {
#ifdef NPAPI_WANTED
                    if (PNAC_IS_NP_PROGRAMMING_ALLOWED () == PNAC_TRUE)
                    {
                        /* Update NP to block port only if port it was previously 
                           force-authorized. 
                         */
                        if ((PNAC_IS_PORT_MODE_MACBASED (pPortInfo) &&
                             (pPortInfo->u1PortPaeStatus == PNAC_ENABLED) &&
                             (u2PrevAuthControl ==
                              PNAC_PORTCNTRL_FORCEAUTHORIZED)))
                        {
                            if (PnacPnacHwSetAuthStatus (u2Port,
                                                         NULL,
                                                         pPortInfo->
                                                         u1PortAuthMode,
                                                         PNAC_BLOCK_PORT,
                                                         PNAC_CNTRLD_DIR_BOTH)
                                != FNP_SUCCESS)

                            {
                                PnacNpFailNotifyAndLog (u2Port,
                                                        NULL,
                                                        PNAC_CNTRLD_DIR_BOTH,
                                                        pPortInfo->
                                                        u1PortAuthMode,
                                                        PNAC_BLOCK_PORT);
                                return SNMP_FAILURE;
                            }

                        }
                    }

#endif /* NPAPI_WANTED */

                    if (pPortInfo->pPortAuthSessionNode != NULL)
                    {
                        PnacAuthDeleteSession (pPortInfo->pPortAuthSessionNode);
                        pPortInfo->pPortAuthSessionNode = NULL;

                        if (PnacInitAuthenticator (u2Port) != PNAC_SUCCESS)
                        {
                            PNAC_TRC (PNAC_MGMT_TRC,
                                      " SNMPPROP: Initialize Authenticator returned failure\n");
                            return SNMP_FAILURE;
                        }
                    }

                    /*  Give Oper state indication to HL */
                    if (pPortInfo->u1PortPaeStatus == PNAC_ENABLED)
                    {
                        PnacAuthPortOperIndication (u2Port);
                    }

                    return SNMP_SUCCESS;
                }
            }
            /* Case 2 */
            else if ((PNAC_AUTH_CONFIG_ENTRY (pPortInfo).
                      u2AuthControlPortControl != PNAC_PORTCNTRL_AUTO)
                     && (u2PrevAuthControl == PNAC_PORTCNTRL_AUTO))
            {
                /* Delete all the sessions */
                if ((PNAC_PORT_INFO (u2Port).u1EntryStatus == PNAC_PORT_OPER_UP)
                    && (pPortInfo->u1PortPaeStatus == PNAC_ENABLED))
                {
                    if (PnacProcPortStatUpdEvent (u2Port, PNAC_PORT_OPER_DOWN)
                        != PNAC_SUCCESS)
                    {
                        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                                  " PAE: Not able to change the Port Operational "
                                  "status\n");
                        return SNMP_FAILURE;
                    }

                    u1PortStatusFlag = PNAC_TRUE;
                }

                /* If Auth session node is not initialized 
                   then initialize
                 */
                if (pPortInfo->pPortAuthSessionNode != NULL)
                {
                    PnacAuthDeleteSession (pPortInfo->pPortAuthSessionNode);
                    pPortInfo->pPortAuthSessionNode = NULL;
                }

                if (PnacInitAuthenticator (u2Port) != PNAC_SUCCESS)
                {
                    PNAC_TRC (PNAC_INIT_SHUT_TRC | PNAC_CONTROL_PATH_TRC |
                              PNAC_ALL_FAILURE_TRC,
                              " Authenticator Initialization Failed\n");
                    return SNMP_FAILURE;
                }

                if (u1PortStatusFlag == PNAC_TRUE)
                {
                    /* If the port is oper up, then update that. */
                    if (PnacProcPortStatUpdEvent (u2Port, PNAC_PORT_OPER_UP) !=
                        PNAC_SUCCESS)
                    {
                        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                                  " PAE: Not able to change the Port Operational "
                                  "status\n");
                        return SNMP_FAILURE;
                    }
                }

                return SNMP_SUCCESS;
            }

            /* ForceAuth   --> ForceUnauth
               ForceUnAuth  --> ForceAuth 
               Force-Auth/
               Force-UnAuth --> Auto  
             */

            /* intimate to ASM */

            if (pPortInfo->u1PortPaeStatus == PNAC_ENABLED)
            {
                if (PnacAuthStateMachine (PNAC_ASM_EV_PORTCNTRL_CHANGED,
                                          u2Port,
                                          pPortInfo->pPortAuthSessionNode,
                                          NULL, PNAC_NO_VAL) != PNAC_SUCCESS)

                {
                    PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                              " SNMPSTD: ASM returned failure for "
                              "PortControl-Change event\n");
                    return SNMP_FAILURE;
                }
            }

            return SNMP_SUCCESS;
        }

        if (pPortInfo->u1PortPaeStatus == PNAC_ENABLED)
        {
            if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
            {

                /* intimate to ASM */
                if (PnacAuthStateMachine (PNAC_ASM_EV_PORTCNTRL_CHANGED,
                                          u2Port,
                                          pPortInfo->pPortAuthSessionNode,
                                          NULL, PNAC_NO_VAL) != PNAC_SUCCESS)

                {
                    PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                              " SNMPSTD: ASM returned failure for "
                              "PortControl-Change event\n");
                    return SNMP_FAILURE;
                }
            }

            /* intimate to SSM */
            if (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))
            {
                if (PnacSuppStateMachine (PNAC_SSM_EV_PORTCNTRL_CHANGED,
                                          u2Port,
                                          pPortInfo->pPortSuppSessionNode,
                                          NULL, PNAC_NO_VAL) != PNAC_SUCCESS)

                {
                    PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                              " SNMPSTD: SSM returned failure for "
                              "PortControl-Change event\n");
                    return SNMP_FAILURE;
                }
            }
        }

        if ((gPnacSystemInfo.u1DPnacRolePlayed ==
             PNAC_DPNAC_SYSTEM_ROLE_MASTER) &&
            (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl ==
             PNAC_PORTCNTRL_FORCEAUTHORIZED))
        {

            u4SlotId = (UINT4) IssGetSwitchid ();
            DPnacGetPortEntry (&pConsPortEntry, u2Port, u4SlotId);
            if (pConsPortEntry != NULL)
            {
                DPnacRemovePortEntryFromConsTree (pConsPortEntry, u4SlotId);
                DPnacDeleteConsPortEntry (pConsPortEntry);
                pConsPortEntry = NULL;
            }

        }

    }

    switch ((INT4) u2AuthControl)
    {
        case PNAC_PORTCNTRL_FORCEUNAUTHORIZED:
            PNAC_TRC (PNAC_MGMT_TRC,
                      " SNMPSTD: Auth Cntrl set to 'Force-Unauthorized'\n");
            break;
        case PNAC_PORTCNTRL_FORCEAUTHORIZED:
            PNAC_TRC (PNAC_MGMT_TRC,
                      " SNMPSTD: Auth Cntrl set to 'Force-Authorized'\n");
            break;
        case PNAC_PORTCNTRL_AUTO:
            PNAC_TRC (PNAC_MGMT_TRC, " SNMPSTD: Auth Cntrl set to 'Auto'\n");
            break;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacLowSetPortPaeStatus                              */
/*                                                                           */
/* Description        : This function sets the given auth status to the      */
/*                      given port if the required conditions are met.       */
/*                                                                           */
/* Input(s)           : u2Port - Port Number                                 */
/*                    : u2PortPaeStatus  -                                   */
/*                                      Pnac enabled/disable state to be set */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS / SNMP_FAILURE                          */
/*****************************************************************************/
INT1
PnacLowSetPortPaeStatus (UINT2 u2Port, UINT2 u2PortPaeStatus)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
#ifdef NPAPI_WANTED
    UINT1               u1NpPortStatus = PNAC_PORTSTATUS_AUTHORIZED;
    UINT1              *pu1SuppMacAddr = NULL;
#endif

    if (PnacGetPortEntry (u2Port, &pPortInfo) != PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (pPortInfo->u1PortPaeStatus == (UINT1) u2PortPaeStatus)
    {
        PNAC_TRC (PNAC_MGMT_TRC,
                  "SNMPPROP: Port Auth Status value configured already\n");
        return SNMP_SUCCESS;
    }

    /* Update the Port status as Un authorized if dot1x is enabled
       and port-control mode is auto */

    if ((u2PortPaeStatus == PNAC_ENABLED) &&
        (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl ==
         PNAC_PORTCNTRL_AUTO))
    {
        pPortInfo->u2PortStatus = PNAC_PORTSTATUS_UNAUTHORIZED;
    }

    /* ----------------------------------------------------------------------|
       |  | Status         | Current Mode  |  Action                         |
       |  |------------------------------------------------------------------|
       |1 |  Disable       |   Auto        |  Delete all sessions(Mac based) |
       |  |                |               |  Oper Up Indication to HL       |
       |--|----------------|---------------|---------------------------------|
       |  |  Disable       |  Force Auth/  |  Update NP                      |
       |2 |                |  ForceUnAuth  |  Oper Up Indication to HL       |
       |--|----------------|---------------|---------------------------------|  
       |  |  Enable        |  Force Auth/  |  Update NP                      |
       |3 |                |  ForceUnAuth/ |  Oper up/down Indication        |
       |  |                |Auto(Mac based)|                                 |
       |--|----------------|---------------|---------------------------------- */

    if (PNAC_PORT_INFO (u2Port).u1EntryStatus == PNAC_PORT_OPER_UP)
    {
        if (PnacProcPortStatUpdEvent (u2Port, PNAC_PORT_OPER_DOWN) !=
            PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                      " PAE: Not able to change the Port Operational "
                      "status\n");
            return SNMP_FAILURE;
        }

        /* This is done here for disabling the state machines via oper down call */

        pPortInfo->u1PortPaeStatus = (UINT1) u2PortPaeStatus;

        PnacL2IwfSetPortPnacPaeStatus (u2Port, (UINT1) u2PortPaeStatus);

        if (PnacProcPortStatUpdEvent (u2Port, PNAC_PORT_OPER_UP) !=
            PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                      " PAE: Not able to change the Port Operational "
                      "status\n");
            return SNMP_FAILURE;
        }
    }
    else
    {
        pPortInfo->u1PortPaeStatus = (UINT1) u2PortPaeStatus;
    }

#ifdef NPAPI_WANTED
    if (PNAC_IS_NP_PROGRAMMING_ALLOWED () == PNAC_TRUE)
    {
        if (u2PortPaeStatus == PNAC_DISABLED)
        {
            if (PNAC_IS_PORT_MODE_MACBASED (pPortInfo))
            {
                if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).
                    u2AuthControlPortControl != PNAC_PORTCNTRL_FORCEAUTHORIZED)
                {
                    u1NpPortStatus = PNAC_UNBLOCK_PORT;
                }
                else
                {
                    PnacL2IwfSetPortPnacAuthStatus (u2Port,
                                                    PNAC_PORTSTATUS_AUTHORIZED);
                    pPortInfo->u2PortStatus = PNAC_PORTSTATUS_AUTHORIZED;
                    return SNMP_SUCCESS;
                }
            }
            else
            {
                u1NpPortStatus = PNAC_PORTSTATUS_AUTHORIZED;
                pu1SuppMacAddr = gPnacEapolGrpMacAddr;
            }
        }

        if (u2PortPaeStatus == PNAC_ENABLED)
        {
            if ((PNAC_IS_PORT_MODE_MACBASED (pPortInfo) &&
                 (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl ==
                  PNAC_PORTCNTRL_AUTO)))
            {
                u1NpPortStatus = PNAC_BLOCK_PORT;
            }
            else
            {
                return SNMP_SUCCESS;
            }
        }

        if (PnacPnacHwSetAuthStatus (u2Port,
                                     pu1SuppMacAddr,
                                     pPortInfo->u1PortAuthMode,
                                     u1NpPortStatus,
                                     PNAC_CNTRLD_DIR_BOTH) != FNP_SUCCESS)
        {
            PnacNpFailNotifyAndLog (u2Port,
                                    pu1SuppMacAddr,
                                    PNAC_CNTRLD_DIR_BOTH,
                                    pPortInfo->u1PortAuthMode, u1NpPortStatus);
            return SNMP_FAILURE;
        }

    }

#endif /* NPAPI_WANTED */

    /* Update L2IWF state as AUTHORIZED irrespective of current state
       because dot1x is disabled on this port */

    if ((u2PortPaeStatus == PNAC_DISABLED) &&
        (PNAC_PORT_INFO (u2Port).u1EntryStatus == PNAC_PORT_OPER_UP))
    {

        PnacL2IwfSetPortPnacAuthStatus (u2Port, PNAC_PORTSTATUS_AUTHORIZED);
        pPortInfo->u2PortStatus = PNAC_PORTSTATUS_AUTHORIZED;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacLowTestPortPaeStatus                             */
/*                                                                           */
/* Description        : This function tests if the given auth status can be  */
/*                      set for the given port.                              */
/*                                                                           */
/* Input(s)           : u2Port - Port Number                                 */
/*                    : u2PortPaeStatus  -                                   */
/*                                      Pnac enabled/disable state to be set */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS / SNMP_FAILURE                          */
/*****************************************************************************/
INT1
PnacLowTestPortPaeStatus (UINT4 *pu4ErrorCode, UINT2 u2Port,
                          UINT2 u2PortPaeStatus)
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry (u2Port, &pPortInfo) != PNAC_SUCCESS)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (PnacSnmpLowValidatePortNumber ((INT4) u2Port) != PNAC_SUCCESS)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((u2PortPaeStatus != PNAC_DISABLED) && (u2PortPaeStatus != PNAC_ENABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacFreeMemForInterface                              */
/*                                                                           */
/* Description        : This routine frees the Radius interface data         */
/*                      structure memory.                                    */
/*                                                                           */
/* Input(s)           : pIface - pointer to Radius Client Interface data     */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacFreeMemForInterface (tRadInterface * pIface)
{
    UtlShMemFreeRadInterface ((UINT1 *) pIface);
}

/*****************************************************************************/
/* Function Name      : PnacNotifyAuthFailure                                */
/*                                                                           */
/* Description        : This routine notifies the authentication failure     */
/*                      to WLAN if present else it just returns.             */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number                              */
/*                      SrcMacAddr - Mac address of the source station       */
/*                      u2Status - Mac authorization status                  */
/*                                 - PNAC_PORTSTATUS_AUTHORIZED              */
/*                                 - PNAC_PORTSTATUS_UNAUTHORIZED            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
PnacNotifyAuthFailure (UINT2 u2PortNum, tMacAddr SrcMacAddr, UINT2 u2Status)
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    pPortInfo = &(PNAC_PORT_INFO (u2PortNum));

    if (pPortInfo->portIfMap.u1IfType == CFA_WLAN)
    {
#ifdef WLAN_WANTED
        if (u2Status == PNAC_PORTSTATUS_UNAUTHORIZED)
        {
            PnacWlanNtfyAuthFailure (SrcMacAddr);
        }
#else
        UNUSED_PARAM (SrcMacAddr);
        UNUSED_PARAM (u2Status);
#endif /*WLAN_WANTED */
    }
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacMemRelease                                       */
/* Description        : This routine Release the memory for the specified    */
/*                      Pool Id.                                             */
/*                                                                           */
/* Input(s)           : PoolIdInfo - Memory Pool Id                          */
/*                      i4RelCount - No of Count to release                  */
/*                      ...        - Variable arguments.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

INT4
PnacMemRelease (tMemPoolId PoolIdInfo, INT4 i4RelCount, ...)
{
    va_list             toRelease;
    INT4                i4Index = 0;
    INT4                i4FailureCount = 0;
    UINT1              *pRelease = NULL;

    va_start (toRelease, i4RelCount);
    for (i4Index = 0; i4Index < i4RelCount; i4Index++)
    {
        pRelease = va_arg (toRelease, UINT1 *);
        if (MemReleaseMemBlock (PoolIdInfo, pRelease) == MEM_FAILURE)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      "Memory Release Failed");
            i4FailureCount++;
        }
    }
    va_end (toRelease);
    if (i4FailureCount > 0)
    {
        return PNAC_FAILURE;
    }
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacDeleteAuthSessionHashNode                        */
/* Description        : This routine Release the memory for the specified    */
/*                      Hash Node.                                           */
/*                                                                           */
/* Input(s)           : pPnacHashNode - Hash Node that is to be freed        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PnacDeleteAuthSessionHashNode (tTMO_HASH_NODE * pPnacHashNode)
{
    PNAC_RELEASE_AUTHSESSNODE_MEMBLK (pPnacHashNode);
    return;
}

/*****************************************************************************/
/* Function Name      : PnacRBTreeCreateEmbedded                             */
/* Description        : This routine create RBTree                           */
/*                                                                           */
/* Input(s)           : u4Offset - Offset                                    */
/*                      Cmp      - Compare Function required for RBTree      */
/*                                 Creation                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    :  Pointer to RBTree                                   */
/*****************************************************************************/
tRBTree
PnacRBTreeCreateEmbedded (UINT4 u4Offset, tRBCompareFn Cmp)
{
    tRBTree             RBTree;

    RBTree = RBTreeCreateEmbedded (u4Offset, Cmp);
    if (RBTree == NULL)
    {
        return NULL;
    }
    RBTreeDisableMutualExclusion (RBTree);
    return RBTree;
}

/***************************************************************************************
 *
 *    FUNCTION NAME    :DPnacCmpPort
 *
 *    DESCRIPTION      : This routine is used to compare nodes of RBTree
 *                       Ports (indexed by port no)
 *                       in a particular PnacDPnacPortEntry node.
 *
 *    INPUT            : *pPnacDPnacPortEntryNode1 - pointer to Consolidated port entry Node1
 *                       *pPnacDPnacPortEntryNode2 - pointer to Consolidated port entry Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If equal returns 0, returns 1 if pE1 > pE2
 *                       else returns -1
 *
 *****************************************************************************************/

INT4
DPnacCmpPort (tRBElem * pPnacDPnacConsPortEntryNode1,
              tRBElem * pPnacDPnacConsPortEntryNode2)
{
    tPnacDPnacConsPortEntry *pEntryA = NULL;
    tPnacDPnacConsPortEntry *pEntryB = NULL;

    pEntryA = (tPnacDPnacConsPortEntry *) pPnacDPnacConsPortEntryNode1;
    pEntryB = (tPnacDPnacConsPortEntry *) pPnacDPnacConsPortEntryNode2;

    /* RBTree Index is PortIndex */
    if (pEntryA->u2PortIndex < pEntryB->u2PortIndex)
    {
        return (-1);
    }
    if (pEntryA->u2PortIndex > pEntryB->u2PortIndex)
    {
        return (1);
    }

    return (0);

}

/******************************************************************************
 * Function Name      : DPnacInfoTreeDestroy
 *
 * Description        : This routine is used to destroy the RBTree
 *
 * Input(s)           : u4Arg    - Offset for free function
 *                      freeFn   - Free Function required for RBTree Destruction
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/

VOID
DPnacInfoTreeDestroy (tRBTree Tree, tRBKeyFreeFn FreeFn, UINT4 u4Arg)
{
    if (Tree != NULL)
    {
        RBTreeDestroy (Tree, FreeFn, u4Arg);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : PnacReAuthOnVlanPropChange                           */
/*                                                                           */
/* Description        : This routine will initiate the new authentication    */
/*                      incase of Events that requires reauthentication      */
/*                                                                           */
/* Input(s)           : pVlanChangeInfo -(Contains the change specific info) */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - On Success                            */
/*                      PNAC_FAILURE - On Failure                            */
/*****************************************************************************/

INT4
PnacReAuthOnVlanPropChange (tVlanChangeInfo * pVlanChangeInfo)
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (pVlanChangeInfo == NULL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  "PnacReAuthOnVlanPropChange Vlan related Information not available ... \n");
        return PNAC_FAILURE;
    }
    /*  check  whether port is invalid */
    if (PnacGetPortEntry (pVlanChangeInfo->u2Port, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PnacReAuthOnVlanPropChange: Port is not created yet, Aborting operation...\n");
        return PNAC_FAILURE;
    }

    /* Check whether Previous and Current value's between VLAN & PNAC is same */

    if ((pPortInfo->u2CurrentVlanId == pVlanChangeInfo->u2CurrentVlanId) ||
        (pVlanChangeInfo->u2CurrentVlanId == PNAC_INIT_VAL))
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                  " PnacReAuthOnVlanPropChange: No change in Pvid. Ignoring Event \n");
        return PNAC_SUCCESS;
    }

    if (PNAC_PORT_INFO (pVlanChangeInfo->u2Port).u1EntryStatus ==
        PNAC_PORT_OPER_UP)
    {
        if (PnacProcPortStatUpdEvent
            ((pVlanChangeInfo->u2Port), PNAC_PORT_OPER_DOWN) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                      " PAE: Not able to change the Port Operational "
                      "status\n");
            return PNAC_FAILURE;
        }

        /* Update the Vlan information for future use */

        pPortInfo->u2CurrentVlanId = pVlanChangeInfo->u2CurrentVlanId;

        if (PnacProcPortStatUpdEvent
            (pVlanChangeInfo->u2Port, PNAC_PORT_OPER_UP) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                      " PAE: Not able to change the Port Operational "
                      "status\n");
            return PNAC_FAILURE;
        }
    }
    return PNAC_SUCCESS;
}
