/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pnacsz.c,v 1.3 2013/11/29 11:04:15 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _PNACSZ_C
#include "pnachdrs.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
PnacSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < PNAC_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsPNACSizingParams[i4SizingId].u4StructSize,
                              FsPNACSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(PNACMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            PnacSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
PnacSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsPNACSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, PNACMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
PnacSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < PNAC_MAX_SIZING_ID; i4SizingId++)
    {
        if (PNACMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (PNACMemPoolIds[i4SizingId]);
            PNACMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
