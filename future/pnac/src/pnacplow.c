/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pnacplow.c,v 1.72 2015/06/11 10:03:16 siva Exp $
 *
 * Description     : This file contains Authentication
 *                   functions of PNAC module
 *******************************************************************/
#include  "pnachdrs.h"

extern UINT4        stdpna[6];
extern UINT4        fspnac[8];
extern UINT4        FsPnacSystemControl[10];
PRIVATE VOID        PnacNotifyProtocolShutdownStatus (VOID);

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDPnacSystemStatus
 Input       :  The Indices

                The Object 
                retValFsDPnacSystemStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDPnacSystemStatus (INT4 *pi4RetValFsDPnacSystemStatus)
{
    *pi4RetValFsDPnacSystemStatus = (INT4) gPnacSystemInfo.u1SystemMode;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDPnacPeriodicSyncTime
 Input       :  The Indices

                The Object 
                retValFsDPnacPeriodicSyncTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDPnacPeriodicSyncTime (UINT4 *pu4RetValFsDPnacPeriodicSyncTime)
{
    *pu4RetValFsDPnacPeriodicSyncTime = gPnacSystemInfo.u4DPnacPeriodicSyncTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDPnacMaxKeepAliveCount
 Input       :  The Indices

                The Object 
                retValFsDPnacMaxKeepAliveCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDPnacMaxKeepAliveCount (INT4 *pi4RetValFsDPnacMaxKeepAliveCount)
{
    *pi4RetValFsDPnacMaxKeepAliveCount =
        (INT4) gPnacSystemInfo.u4DPnacMaxAliveCount;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDPnacSystemStatus
 Input       :  The Indices

                The Object 
                setValFsDPnacSystemStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDPnacSystemStatus (INT4 i4SetValFsDPnacSystemStatus)
{
    if (gPnacSystemInfo.u1SystemMode == (UINT1) i4SetValFsDPnacSystemStatus)
    {
        return SNMP_SUCCESS;
    }

    PnacModuleDisable ();

    gPnacSystemInfo.u1SystemMode = (UINT1) i4SetValFsDPnacSystemStatus;

    DPnacSetModuleStatus (gPnacSystemInfo.u1SystemMode);

    if (PnacModuleEnable () != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_INIT_SHUT_TRC,
                  "Pnac Module Start Failed ... \n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDPnacPeriodicSyncTime
 Input       :  The Indices

                The Object 
                setValFsDPnacPeriodicSyncTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDPnacPeriodicSyncTime (UINT4 u4SetValFsDPnacPeriodicSyncTime)
{
    gPnacSystemInfo.u4DPnacPeriodicSyncTime = u4SetValFsDPnacPeriodicSyncTime;

    if (gPnacSystemInfo.u1SystemMode == PNAC_CENTRALIZED)
    {
        return SNMP_SUCCESS;

    }

    if (gPnacSystemInfo.u4DPnacPeriodicSyncTime == 0)
    {
        if (gPnacSystemInfo.DPnacPeriodicSyncTmr != NULL)
        {
            if (PnacTmrStopTmr (&gPnacSystemInfo, PNAC_DPNAC_PERIODIC_TIMER)
                != PNAC_SUCCESS)

            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " DPNAC: Stop periodic timer returned failure \n");
                return PNAC_FAILURE;
            }

            gPnacSystemInfo.DPnacPeriodicSyncTmr = NULL;
        }
    }
    else
    {
        if (gPnacSystemInfo.DPnacPeriodicSyncTmr == NULL)
        {
            if (PnacTmrStartTmr (&gPnacSystemInfo, PNAC_DPNAC_PERIODIC_TIMER,
                                 gPnacSystemInfo.u4DPnacPeriodicSyncTime) !=
                PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC, "PnacTmrStartTmr failed \n");
                return PNAC_FAILURE;
            }
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDPnacMaxKeepAliveCount
 Input       :  The Indices

                The Object 
                setValFsDPnacMaxKeepAliveCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDPnacMaxKeepAliveCount (INT4 i4SetValFsDPnacMaxKeepAliveCount)
{
    gPnacSystemInfo.u4DPnacMaxAliveCount =
        (UINT4) i4SetValFsDPnacMaxKeepAliveCount;

    if (gPnacSystemInfo.u1SystemMode == PNAC_CENTRALIZED)
    {
        return SNMP_SUCCESS;
    }

    if (gPnacSystemInfo.u1DPnacRolePlayed == PNAC_DPNAC_SYSTEM_ROLE_MASTER)
    {
        DPnacHandleKeepAlive (PNAC_DPNAC_KEEP_ALIVE_CONFIG);
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsDPnacSystemStatus
 Input       :  The Indices

                The Object 
                testValFsDPnacSystemStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDPnacSystemStatus (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsDPnacSystemStatus)
{
    if ((i4TestValFsDPnacSystemStatus != PNAC_CENTRALIZED) &&
        (i4TestValFsDPnacSystemStatus != PNAC_DISTRIBUTED))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: Use only PNAC_CENTRALIZED or"
                  " PNAC_CENTRALIZED for setting 'PNAC system status'\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDPnacPeriodicSyncTime
 Input       :  The Indices

                The Object 
                testValFsDPnacPeriodicSyncTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDPnacPeriodicSyncTime (UINT4 *pu4ErrorCode,
                                  UINT4 u4TestValFsDPnacPeriodicSyncTime)
{
    if (u4TestValFsDPnacPeriodicSyncTime > 300)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  "SNMPPROP: Use range in beteen  0 to 300 for setting Periodic sync timer\n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDPnacMaxKeepAliveCount
 Input       :  The Indices

                The Object 
                testValFsDPnacMaxKeepAliveCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDPnacMaxKeepAliveCount (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsDPnacMaxKeepAliveCount)
{
    if ((i4TestValFsDPnacMaxKeepAliveCount < 1) ||
        (i4TestValFsDPnacMaxKeepAliveCount > 5))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  "SNMPPROP: Use range in beteen  1 to 5 for setting Maximum Keep Alive Count\n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDPnacSystemStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDPnacSystemStatus (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDPnacPeriodicSyncTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDPnacPeriodicSyncTime (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDPnacMaxKeepAliveCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDPnacMaxKeepAliveCount (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDPnacStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDPnacStatsTable
 Input       :  The Indices
                FsDPnacSlotNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDPnacStatsTable (INT4 i4FsDPnacSlotNumber)
{
    tPnacDPnacSlotInfo *pDPnacSlotInfo = NULL;

    DPnacGetSlotInfo (&pDPnacSlotInfo, (UINT4) i4FsDPnacSlotNumber);

    if (pDPnacSlotInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDPnacStatsTable
 Input       :  The Indices
                FsDPnacSlotNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDPnacStatsTable (INT4 *pi4FsDPnacSlotNumber)
{
    tPnacDPnacSlotInfo *pDPnacSlotInfo = NULL;

    DPnacGetNextSlotInfo (NULL, &pDPnacSlotInfo);

    if (pDPnacSlotInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4FsDPnacSlotNumber = (INT4) pDPnacSlotInfo->u4SlotId;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDPnacStatsTable
 Input       :  The Indices
                FsDPnacSlotNumber
                nextFsDPnacSlotNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDPnacStatsTable (INT4 i4FsDPnacSlotNumber,
                                  INT4 *pi4NextFsDPnacSlotNumber)
{
    tPnacDPnacSlotInfo *pDPnacSlotInfo = NULL;
    tPnacDPnacSlotInfo *pDPnacNextSlotInfo = NULL;

    DPnacGetSlotInfo (&pDPnacSlotInfo, (UINT4) i4FsDPnacSlotNumber);
    if (pDPnacSlotInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    else
    {
        DPnacGetNextSlotInfo (pDPnacSlotInfo, &pDPnacNextSlotInfo);

        if (pDPnacNextSlotInfo == NULL)
        {
            return SNMP_FAILURE;
        }
        else
        {
            *pi4NextFsDPnacSlotNumber = (INT4) pDPnacNextSlotInfo->u4SlotId;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDPnacEventUpdateFramesRx
 Input       :  The Indices
                FsDPnacSlotNumber

                The Object
                retValFsDPnacEventUpdateFramesRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDPnacEventUpdateFramesRx (INT4 i4FsDPnacSlotNumber,
                                  UINT4 *pu4RetValFsDPnacEventUpdateFramesRx)
{
    tPnacDPnacSlotInfo *pDPnacSlotInfo = NULL;

    if (DPnacGetSlotInfo (&pDPnacSlotInfo, (UINT4) i4FsDPnacSlotNumber) ==
        PNAC_FAILURE)
    {
        return SNMP_FAILURE;

    }

    *pu4RetValFsDPnacEventUpdateFramesRx =
        pDPnacSlotInfo->u4EventUpdateFramesRx;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDPnacEventUpdateFramesTx
 Input       :  The Indices
                FsDPnacSlotNumber

                The Object
                retValFsDPnacEventUpdateFramesTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDPnacEventUpdateFramesTx (INT4 i4FsDPnacSlotNumber,
                                  UINT4 *pu4RetValFsDPnacEventUpdateFramesTx)
{
    tPnacDPnacSlotInfo *pDPnacSlotInfo = NULL;

    if (DPnacGetSlotInfo (&pDPnacSlotInfo, (UINT4) i4FsDPnacSlotNumber) ==
        PNAC_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsDPnacEventUpdateFramesTx =
        pDPnacSlotInfo->u4EventUpdateFramesTx;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDPnacPeriodicFramesTx
 Input       :  The Indices
                FsDPnacSlotNumber

                The Object
                retValFsDPnacPeriodicFramesTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDPnacPeriodicFramesTx (INT4 i4FsDPnacSlotNumber,
                               UINT4 *pu4RetValFsDPnacPeriodicFramesTx)
{
    tPnacDPnacSlotInfo *pDPnacSlotInfo = NULL;

    if (DPnacGetSlotInfo (&pDPnacSlotInfo, (UINT4) i4FsDPnacSlotNumber) ==
        PNAC_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsDPnacPeriodicFramesTx = pDPnacSlotInfo->u4PeriodicFramesTx;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDPnacPeriodicFramesRx
 Input       :  The Indices
                FsDPnacSlotNumber

                The Object
                retValFsDPnacPeriodicFramesRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDPnacPeriodicFramesRx (INT4 i4FsDPnacSlotNumber,
                               UINT4 *pu4RetValFsDPnacPeriodicFramesRx)
{
    tPnacDPnacSlotInfo *pDPnacSlotInfo = NULL;

    if (DPnacGetSlotInfo (&pDPnacSlotInfo, (UINT4) i4FsDPnacSlotNumber) ==
        PNAC_FAILURE)
    {
        return SNMP_FAILURE;

    }
    *pu4RetValFsDPnacPeriodicFramesRx = pDPnacSlotInfo->u4PeriodicFramesRx;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDPnacSlotPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDPnacSlotPortTable
 Input       :  The Indices
                FsDPnacSlotNumber
                FsDPnacPortIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDPnacSlotPortTable (INT4 i4FsDPnacSlotNumber,
                                              INT4 i4FsDPnacPortIndex)
{
    tPnacDPnacConsPortEntry *pConsPortEntry = NULL;

    if ((DPnacGetPortEntry
         (&pConsPortEntry, (UINT2) i4FsDPnacPortIndex,
          (UINT4) i4FsDPnacSlotNumber) != PNAC_SUCCESS))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDPnacSlotPortTable
 Input       :  The Indices
                FsDPnacSlotNumber
                FsDPnacPortIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDPnacSlotPortTable (INT4 *pi4FsDPnacSlotNumber,
                                      INT4 *pi4FsDPnacPortIndex)
{
    tPnacDPnacSlotInfo *pNextDPnacSlotInfo = NULL;
    tPnacDPnacConsPortEntry *pConsPortEntry = NULL;
    UINT4               u4Count = 0;

    if (DPnacGetNextSlotInfo (NULL, &pNextDPnacSlotInfo) == PNAC_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (pNextDPnacSlotInfo == NULL)
    {
        return PNAC_FAILURE;

    }
    RBTreeCount (pNextDPnacSlotInfo->PnacDPnacPortList, &u4Count);
    if (u4Count == 0)
    {
        do
        {
            DPnacGetNextSlotInfo (pNextDPnacSlotInfo, &pNextDPnacSlotInfo);
            if (pNextDPnacSlotInfo == NULL)
            {
                return PNAC_FAILURE;

            }
            RBTreeCount (pNextDPnacSlotInfo->PnacDPnacPortList, &u4Count);
        }
        while (u4Count == 0);
    }

    if (pNextDPnacSlotInfo == NULL)
    {
        return PNAC_FAILURE;

    }
    if ((DPnacGetNextPortEntry
         (pConsPortEntry, &pConsPortEntry,
          pNextDPnacSlotInfo->u4SlotId) != PNAC_SUCCESS))
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4FsDPnacSlotNumber = (INT4) pNextDPnacSlotInfo->u4SlotId;
        *pi4FsDPnacPortIndex = (INT4) pConsPortEntry->u2PortIndex;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDPnacSlotPortTable
 Input       :  The Indices
                FsDPnacSlotNumber
                nextFsDPnacSlotNumber
                FsDPnacPortIndex
                nextFsDPnacPortIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDPnacSlotPortTable (INT4 i4FsDPnacSlotNumber,
                                     INT4 *pi4NextFsDPnacSlotNumber,
                                     INT4 i4FsDPnacPortIndex,
                                     INT4 *pi4NextFsDPnacPortIndex)
{
    tPnacDPnacConsPortEntry *pConsPortEntry = NULL;
    tPnacDPnacConsPortEntry *pNextConsPortEntry = NULL;
    tPnacDPnacSlotInfo *pDPnacSlotInfo = NULL;
    tPnacDPnacSlotInfo *pNextDPnacSlotInfo = NULL;

    if (DPnacGetSlotInfo (&pDPnacSlotInfo, (UINT4) i4FsDPnacSlotNumber) ==
        PNAC_FAILURE)
    {
        return SNMP_FAILURE;
    }

    DPnacGetPortEntry (&pConsPortEntry, (UINT2) i4FsDPnacPortIndex,
                       (UINT4) i4FsDPnacSlotNumber);

    if ((DPnacGetNextPortEntry
         (pConsPortEntry, &pNextConsPortEntry,
          (UINT4) pDPnacSlotInfo->u4SlotId) == PNAC_SUCCESS))
    {
        *pi4NextFsDPnacSlotNumber = (INT4) pDPnacSlotInfo->u4SlotId;
        *pi4NextFsDPnacPortIndex = (INT4) pNextConsPortEntry->u2PortIndex;
        return SNMP_SUCCESS;
    }
    else
    {
        if (DPnacGetNextSlotInfo (pDPnacSlotInfo, &pNextDPnacSlotInfo) ==
            PNAC_FAILURE)
        {
            return SNMP_FAILURE;
        }
        else
        {
            pConsPortEntry = NULL;
            if ((DPnacGetNextPortEntry
                 (pConsPortEntry, &pNextConsPortEntry,
                  (UINT4) pNextDPnacSlotInfo->u4SlotId) == PNAC_SUCCESS))
            {
                *pi4NextFsDPnacSlotNumber = (INT4) pNextDPnacSlotInfo->u4SlotId;
                *pi4NextFsDPnacPortIndex =
                    (INT4) pNextConsPortEntry->u2PortIndex;
                return SNMP_SUCCESS;
            }
            else
            {
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDPnacPortAuthStatus
 Input       :  The Indices
                FsDPnacSlotNumber
                FsDPnacPortIndex

                The Object
                retValFsDPnacPortAuthStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDPnacPortAuthStatus (INT4 i4FsDPnacSlotNumber, INT4 i4FsDPnacPortIndex,
                             INT4 *pi4RetValFsDPnacPortAuthStatus)
{
    tPnacDPnacConsPortEntry *pConsPortEntry = NULL;

    if ((DPnacGetPortEntry
         (&pConsPortEntry, (UINT2) i4FsDPnacPortIndex,
          (UINT4) i4FsDPnacSlotNumber) != PNAC_SUCCESS))
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsDPnacPortAuthStatus =
            (INT4) pConsPortEntry->u1PortAuthStatus;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDPnacPortControlledDirection
 Input       :  The Indices
                FsDPnacSlotNumber
                FsDPnacPortIndex

                The Object
                retValFsDPnacPortControlledDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDPnacPortControlledDirection (INT4 i4FsDPnacSlotNumber,
                                      INT4 i4FsDPnacPortIndex,
                                      INT4
                                      *pi4RetValFsDPnacPortControlledDirection)
{
    tPnacDPnacConsPortEntry *pConsPortEntry = NULL;

    if ((DPnacGetPortEntry
         (&pConsPortEntry, (UINT2) i4FsDPnacPortIndex,
          (UINT4) i4FsDPnacSlotNumber) != PNAC_SUCCESS))
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsDPnacPortControlledDirection =
            (INT4) pConsPortEntry->u1CtrlDirection;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPnacSystemControl
 Input       :  The Indices

                The Object
                retValFsPnacSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacSystemControl ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacSystemControl (INT4 *pi4RetValFsPnacSystemControl)
#else
INT1
nmhGetFsPnacSystemControl (pi4RetValFsPnacSystemControl)
     INT4               *pi4RetValFsPnacSystemControl;
#endif
{
    *pi4RetValFsPnacSystemControl = (INT4) PNAC_SYSTEM_CONTROL;

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacModuleOperStatus
 Input       :  The Indices

                The Object 
                retValFsPnacModuleOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPnacModuleOperStatus (INT4 *pi4RetValFsPnacModuleOperStatus)
{
    *pi4RetValFsPnacModuleOperStatus = (INT4) PNAC_MODULE_STATUS;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPnacTraceOption
 Input       :  The Indices

                The Object 
                retValFsPnacTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacTraceOption ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacTraceOption (INT4 *pi4RetValFsPnacTraceOption)
#else
INT1
nmhGetFsPnacTraceOption (pi4RetValFsPnacTraceOption)
     INT4               *pi4RetValFsPnacTraceOption;
#endif
{
    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");

        *pi4RetValFsPnacTraceOption = PNAC_INIT_VAL;
        return SNMP_SUCCESS;
    }
    *pi4RetValFsPnacTraceOption = (INT4) gPnacSystemInfo.u4TraceOption;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacAuthenticServer
 Input       :  The Indices

                The Object 
                retValFsPnacAuthenticServer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacAuthenticServer ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacAuthenticServer (INT4 *pi4RetValFsPnacAuthenticServer)
#else
INT1
nmhGetFsPnacAuthenticServer (pi4RetValFsPnacAuthenticServer)
     INT4               *pi4RetValFsPnacAuthenticServer;
#endif
{
    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");

        *pi4RetValFsPnacAuthenticServer = PNAC_LOCAL_AUTH_SERVER;
        return SNMP_SUCCESS;
    }

    *pi4RetValFsPnacAuthenticServer = (INT4) gPnacSystemInfo.u1AuthenticMethod;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacRemoteAuthServerType
 Input       :  The Indices

                The Object
                retValFsPnacRemoteAuthServerType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacRemoteAuthServerType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacRemoteAuthServerType (INT4 *pi4RetValFsPnacRemoteAuthServerType)
#else
INT1
nmhGetFsPnacRemoteAuthServerType (pi4RetValFsPnacRemoteAuthServerType)
     INT4               *pi4RetValFsPnacRemoteAuthServerType;
#endif
{
    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");

        *pi4RetValFsPnacRemoteAuthServerType = PNAC_REMOTE_RADIUS_AUTH_SERVER;
        return SNMP_SUCCESS;
    }

    *pi4RetValFsPnacRemoteAuthServerType =
        (INT4) gPnacSystemInfo.u1RemoteAuthServerType;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacNasId
 Input       :  The Indices

                The Object 
                retValFsPnacNasId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacNasId ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacNasId (tSNMP_OCTET_STRING_TYPE * pRetValFsPnacNasId)
#else
INT1
nmhGetFsPnacNasId (pRetValFsPnacNasId)
     tSNMP_OCTET_STRING_TYPE *pRetValFsPnacNasId;
#endif
{
    if (PNAC_IS_SHUTDOWN ())
    {

        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");

        pRetValFsPnacNasId->i4_Length = PNAC_STRLEN (PNAC_DEFAULT_NAS_ID);
        PNAC_MEMCPY (pRetValFsPnacNasId->pu1_OctetList, PNAC_DEFAULT_NAS_ID,
                     PNAC_STRLEN (PNAC_DEFAULT_NAS_ID));
        return SNMP_SUCCESS;
    }

    pRetValFsPnacNasId->i4_Length = (INT4) gPnacSystemInfo.u2NasIdLen;

    PNAC_MEMCPY (pRetValFsPnacNasId->pu1_OctetList,
                 gPnacSystemInfo.au1NasId, gPnacSystemInfo.u2NasIdLen);

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPnacSystemControl
 Input       :  The Indices

                The Object
                setValFsPnacSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsPnacSystemControl ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsPnacSystemControl (INT4 i4SetValFsPnacSystemControl)
#else
INT1
nmhSetFsPnacSystemControl (i4SetValFsPnacSystemControl)
     INT4                i4SetValFsPnacSystemControl;

#endif
{
    if (i4SetValFsPnacSystemControl == (INT4) PNAC_SYSTEM_CONTROL)
    {
        PNAC_TRC (PNAC_MGMT_TRC, " SNMPPROP: Value Configured already \n");
        return SNMP_SUCCESS;
    }

    if (i4SetValFsPnacSystemControl == (INT4) PNAC_START)
    {
        if (PnacHandleModuleStart () != PNAC_SUCCESS)
        {
            CLI_SET_ERR (CLI_PNAC_SW_INIT_FAILED);
            PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                      " PnacModuleStart failed\n");
            return SNMP_FAILURE;
        }

        PNAC_TRC (PNAC_MGMT_TRC, " Module started\n");
    }
    else
    {
        PnacHandleModuleShutDown ();
        PNAC_TRC (PNAC_MGMT_TRC, " Shutdown...Done\n");
        /* Notify MSR with the pnac oids */
        PnacNotifyProtocolShutdownStatus ();
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "FsPnacSystemControl = %d\n", i4SetValFsPnacSystemControl);
 ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsPnacTraceOption
 Input       :  The Indices

                The Object 
                setValFsPnacTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsPnacTraceOption ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsPnacTraceOption (INT4 i4SetValFsPnacTraceOption)
#else
INT1
nmhSetFsPnacTraceOption (i4SetValFsPnacTraceOption)
     INT4                i4SetValFsPnacTraceOption;

#endif
{
    gPnacSystemInfo.u4TraceOption = (UINT4) i4SetValFsPnacTraceOption;
    PNAC_TRC_ARG1 (PNAC_MGMT_TRC,
                   " SNMPPROP: Trace Option is set with value: %d \n",
                   i4SetValFsPnacTraceOption);
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "FsPnacTraceOption = %d\n", i4SetValFsPnacTraceOption); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsPnacAuthenticServer
 Input       :  The Indices

                The Object 
                setValFsPnacAuthenticServer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsPnacAuthenticServer ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsPnacAuthenticServer (INT4 i4SetValFsPnacAuthenticServer)
#else
INT1
nmhSetFsPnacAuthenticServer (i4SetValFsPnacAuthenticServer)
     INT4                i4SetValFsPnacAuthenticServer;

#endif
{
    if (i4SetValFsPnacAuthenticServer ==
        (INT4) gPnacSystemInfo.u1AuthenticMethod)
    {
        PNAC_TRC (PNAC_MGMT_TRC, " SNMPPROP: Value Configured already \n");
        return SNMP_SUCCESS;
    }

    gPnacSystemInfo.u1AuthenticMethod = (UINT1) i4SetValFsPnacAuthenticServer;

    if (i4SetValFsPnacAuthenticServer == (INT4) PNAC_LOCAL_AUTH_SERVER)
    {
        PNAC_TRC (PNAC_MGMT_TRC,
                  " SNMPPROP: Pnac module is now configured for Local-Auth-Server\n");
    }
    else
    {
        PNAC_TRC (PNAC_MGMT_TRC,
                  " SNMPPROP: Pnac module is now configured for Remote-Auth-Server\n");
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "FsPnacAuthenticServer = %d\n", i4SetValFsPnacAuthenticServer); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsPnacRemoteAuthServerType
 Input       :  The Indices

                The Object
                setValFsPnacRemoteAuthServerType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsPnacRemoteAuthServerType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsPnacRemoteAuthServerType (INT4 i4SetValFsPnacRemoteAuthServerType)
#else
INT1
nmhSetFsPnacRemoteAuthServerType (i4SetValFsPnacRemoteAuthServerType)
     INT4                i4SetValFsPnacRemoteAuthServerType;

#endif
{
    if (i4SetValFsPnacRemoteAuthServerType ==
        (INT4) gPnacSystemInfo.u1RemoteAuthServerType)
    {
        PNAC_TRC (PNAC_MGMT_TRC, " SNMPPROP: Value Configured already \n");
        return SNMP_SUCCESS;
    }

    gPnacSystemInfo.u1RemoteAuthServerType =
        (UINT1) i4SetValFsPnacRemoteAuthServerType;

    if (i4SetValFsPnacRemoteAuthServerType ==
        (INT4) PNAC_REMOTE_RADIUS_AUTH_SERVER)
    {
        PNAC_TRC (PNAC_MGMT_TRC,
                  " SNMPPROP: Pnac module is now configured for RADIUS-Server\n");
    }
    else
    {
        PNAC_TRC (PNAC_MGMT_TRC,
                  " SNMPPROP: Pnac module is now configured for TACACS-Plus-Server\n");
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "FsPnacRemoteAuthServerType = %d\n", i4SetValFsPnacRemoteAuthServerType); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsPnacNasId
 Input       :  The Indices

                The Object 
                setValFsPnacNasId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsPnacNasId ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsPnacNasId (tSNMP_OCTET_STRING_TYPE * pSetValFsPnacNasId)
#else
INT1
nmhSetFsPnacNasId (pSetValFsPnacNasId)
     tSNMP_OCTET_STRING_TYPE *pSetValFsPnacNasId;

#endif
{
    gPnacSystemInfo.u2NasIdLen = (UINT2) pSetValFsPnacNasId->i4_Length;

    PNAC_MEMCPY (gPnacSystemInfo.au1NasId,
                 pSetValFsPnacNasId->pu1_OctetList,
                 pSetValFsPnacNasId->i4_Length);

    PNAC_TRC_ARG1 (PNAC_MGMT_TRC,
                   " SNMPPROP: Nas Id is set as %s \n",
                   gPnacSystemInfo.au1NasId);
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacPaePortStatisticsClear
 Input       :  The Indices
                FsPnacPaePortNumber

                The Object
                retValFsPnacPaePortStatisticsClear
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPnacPaePortStatisticsClear(INT4 i4FsPnacPaePortNumber , INT4 *pi4RetValFsPnacPaePortStatisticsClear)
{

    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    *pi4RetValFsPnacPaePortStatisticsClear = PNAC_DISABLED;

    UNUSED_PARAM(i4FsPnacPaePortNumber);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsPnacPaePortAuthStatus
 Input       :  The Indices
                FsPnacPaePortNumber

                The Object
                retValFsPnacPaePortAuthStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPnacPaePortAuthStatus(INT4 i4FsPnacPaePortNumber , INT4 *pi4RetValFsPnacPaePortAuthStatus)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PNAC_IS_SHUTDOWN ())
    {

        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        *pi4RetValFsPnacPaePortAuthStatus = PNAC_DISABLED;
        return SNMP_SUCCESS;
    }
    if (PnacGetPortEntry ((UINT2) i4FsPnacPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPnacPaePortAuthStatus = (INT4) pPortInfo->u1PortPaeStatus;
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFsPnacPaeAuthReAuthMax
 Input       :  The Indices
                FsPnacPaePortNumber

                The Object
                retValFsPnacPaeAuthReAuthMax
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPnacPaeAuthReAuthMax(INT4 i4FsPnacPaePortNumber , UINT4 *pu4RetValFsPnacPaeAuthReAuthMax)
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PNAC_IS_SHUTDOWN ())
    {

        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacGetPortEntry ((UINT2) i4FsPnacPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsPnacPaeAuthReAuthMax = PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4ReAuthMax;
        return SNMP_SUCCESS;
    }
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPnacSystemControl
 Input       :  The Indices

                The Object
                testValFsPnacSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsPnacSystemControl ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsPnacSystemControl (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsPnacSystemControl)
#else
INT1
nmhTestv2FsPnacSystemControl (pu4ErrorCode, i4TestValFsPnacSystemControl)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValFsPnacSystemControl;
#endif
{
    if ((i4TestValFsPnacSystemControl != PNAC_START) &&
        (i4TestValFsPnacSystemControl != PNAC_SHUTDOWN))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: Use only PNAC_START or"
                  " PNAC_SHUTDOWN for setting 'SystemControl'\n");
        return SNMP_FAILURE;
    }

    if (VlanGetBaseBridgeMode () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BASE_BRIDGE_PNAC_ENABLED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "FsPnacSystemControl = %d\n", i4TestValFsPnacSystemControl)
; ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsPnacTraceOption
 Input       :  The Indices

                The Object 
                testValFsPnacTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsPnacTraceOption ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsPnacTraceOption (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsPnacTraceOption)
#else
INT1
nmhTestv2FsPnacTraceOption (pu4ErrorCode, i4TestValFsPnacTraceOption)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValFsPnacTraceOption;
#endif
{
   /*** $$TRACE_LOG (ENTRY, "FsPnacTraceOption = %d\n", i4TestValFsPnacTraceOption); ***/
    if (PNAC_IS_SHUTDOWN ())
    {

        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPnacTraceOption < 0) ||
        (i4TestValFsPnacTraceOption > 0x01ff))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsPnacAuthenticServer
 Input       :  The Indices

                The Object 
                testValFsPnacAuthenticServer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsPnacAuthenticServer ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsPnacAuthenticServer (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsPnacAuthenticServer)
#else
INT1
nmhTestv2FsPnacAuthenticServer (pu4ErrorCode, i4TestValFsPnacAuthenticServer)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValFsPnacAuthenticServer;
#endif
{
    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPnacAuthenticServer != PNAC_LOCAL_AUTH_SERVER) &&
        (i4TestValFsPnacAuthenticServer != PNAC_REMOTE_AUTH_SERVER))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_MGMT_TRC,
                  " SNMPPROP: Use only '1' for Remote-Auth-Server and '2' for Local-Auth-Server configuration !!\n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "FsPnacAuthenticServer = %d\n", i4TestValFsPnacAuthenticServer); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsPnacRemoteAuthServerType
 Input       :  The Indices

                The Object
                testValFsPnacRemoteAuthServerType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsPnacRemoteAuthServerType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsPnacRemoteAuthServerType (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsPnacRemoteAuthServerType)
#else
INT1
nmhTestv2FsPnacRemoteAuthServerType (pu4ErrorCode,
                                     i4TestValFsPnacRemoteAuthServerType)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValFsPnacRemoteAuthServerType;
#endif
{
    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPnacRemoteAuthServerType != PNAC_REMOTE_RADIUS_AUTH_SERVER)
        && (i4TestValFsPnacRemoteAuthServerType !=
            PNAC_REMOTE_TACACSPLUS_AUTH_SERVER))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_MGMT_TRC,
                  " SNMPPROP: Use only '1' for RADIUS-Server and '2' for TACACS-PLUS-Server configuration !!\n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "FsPnacRemoteAuthServerType = %d\n", i4TestValFsPnacRemoteAuthServerType); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhDepv2FsPnacRemoteAuthServerType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPnacRemoteAuthServerType (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPnacNasId
 Input       :  The Indices

                The Object 
                testValFsPnacNasId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsPnacNasId ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsPnacNasId (UINT4 *pu4ErrorCode,
                      tSNMP_OCTET_STRING_TYPE * pTestValFsPnacNasId)
#else
INT1
nmhTestv2FsPnacNasId (pu4ErrorCode, pTestValFsPnacNasId)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pTestValFsPnacNasId;
#endif
{
    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if ((pTestValFsPnacNasId->i4_Length < 0) ||
        (pTestValFsPnacNasId->i4_Length > PNAC_MAX_NAS_ID_SIZE))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_LENGTH;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPPROP: Invalid Nas Id to set\n");

        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValFsPnacNasId->pu1_OctetList,
                              pTestValFsPnacNasId->i4_Length) == OSIX_FAILURE)
    {
        CLI_SET_ERR (CLI_PNAC_NON_PRINTABLE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPnacSystemControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPnacSystemControl (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPnacTraceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPnacTraceOption (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPnacAuthenticServer
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPnacAuthenticServer (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPnacNasId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPnacNasId (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPnacPaePortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPnacPaePortTable
 Input       :  The Indices
                FsPnacPaePortNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsPnacPaePortTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsPnacPaePortTable (INT4 i4FsPnacPaePortNumber)
#else
INT1
nmhValidateIndexInstanceFsPnacPaePortTable (i4FsPnacPaePortNumber)
     INT4                i4FsPnacPaePortNumber;
#endif
{
    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if (PnacSnmpLowValidatePortNumber (i4FsPnacPaePortNumber) != PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
   /*** $$TRACE_LOG (ENTRY, "FsPnacPaePortNumber = %d\n", i4FsPnacPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsPnacPaePortStatisticsClear
 Input       :  The Indices
                FsPnacPaePortNumber

                The Object
                testValFsPnacPaePortStatisticsClear
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsPnacPaePortStatisticsClear(UINT4 *pu4ErrorCode , INT4 i4FsPnacPaePortNumber , INT4 i4TestValFsPnacPaePortStatisticsClear)
{

    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (PnacSnmpLowValidatePortNumber (i4FsPnacPaePortNumber) != PNAC_SUCCESS)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsPnacPaePortStatisticsClear != PNAC_DISABLED) &&
	(i4TestValFsPnacPaePortStatisticsClear != PNAC_ENABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPnacAuthSessionStatisticsClear
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object
                testValFsPnacAuthSessionStatisticsClear
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsPnacAuthSessionStatisticsClear(UINT4 *pu4ErrorCode , tMacAddr FsPnacAuthSessionSuppAddress , INT4 i4TestValFsPnacAuthSessionStatisticsClear)
{

    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPnacAuthSessionStatisticsClear != PNAC_DISABLED) &&
        (i4TestValFsPnacAuthSessionStatisticsClear != PNAC_ENABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM(FsPnacAuthSessionSuppAddress);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsPnacAuthSessionStatisticsClear
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress                                                     
                The Object
                retValFsPnacAuthSessionStatisticsClear
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPnacAuthSessionStatisticsClear(tMacAddr FsPnacAuthSessionSuppAddress , INT4 *pi4RetValFsPnacAuthSessionStatisticsClear)
{

    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    *pi4RetValFsPnacAuthSessionStatisticsClear = PNAC_DISABLED;

    UNUSED_PARAM(FsPnacAuthSessionSuppAddress);
    return SNMP_SUCCESS;

}
/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsPnacAuthSessionStatisticsClear                                            Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object
                setValFsPnacAuthSessionStatisticsClear
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsPnacAuthSessionStatisticsClear(tMacAddr FsPnacAuthSessionSuppAddress , INT4 i4SetValFsPnacAuthSessionStatisticsClear)
{

    tPnacAuthSessionNode *pSessNode = NULL;
    tPnacAuthSessStats *pSessStatsEntry = NULL;

    if(i4SetValFsPnacAuthSessionStatisticsClear == PNAC_ENABLED)
    {

        if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
	{

		return SNMP_FAILURE;

	}
	if(pSessNode!= NULL)
	{
		pSessStatsEntry = &(pSessNode->authSessStats);
		pSessStatsEntry->u4SessFramesRx = PNAC_INIT_VAL;
		/*Also Clearing Octets Received*/
		PNAC_UINT8_RESET (pSessStatsEntry->u8SessOctetsRx);
                pSessStatsEntry->u4SessFramesTx = PNAC_INIT_VAL;
                /*Also Clearing Octets Transmitted*/
                PNAC_UINT8_RESET (pSessStatsEntry->u8SessOctetsTx);

	}
    }
    
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPnacPaePortTable
 Input       :  The Indices
                FsPnacPaePortNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstFsPnacPaePortTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsPnacPaePortTable (INT4 *pi4FsPnacPaePortNumber)
#else
INT1
nmhGetFirstIndexFsPnacPaePortTable (pi4FsPnacPaePortNumber)
     INT4               *pi4FsPnacPaePortNumber;
#endif
{
    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if (PnacSnmpLowGetFirstValidPortNumber (pi4FsPnacPaePortNumber)
        != PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
   /*** $$TRACE_LOG (ENTRY, "FsPnacPaePortNumber = %d\n", *pi4FsPnacPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPnacPaePortTable
 Input       :  The Indices
                FsPnacPaePortNumber
                nextFsPnacPaePortNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextFsPnacPaePortTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexFsPnacPaePortTable (INT4 i4FsPnacPaePortNumber,
                                   INT4 *pi4NextFsPnacPaePortNumber)
#else
INT1
nmhGetNextIndexFsPnacPaePortTable (i4FsPnacPaePortNumber,
                                   pi4NextFsPnacPaePortNumber)
     INT4                i4FsPnacPaePortNumber;
     INT4               *pi4NextFsPnacPaePortNumber;
#endif
{
    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if (i4FsPnacPaePortNumber < 0)
    {
        return SNMP_FAILURE;
    }

    if (PnacSnmpLowGetNextValidPortNumber (i4FsPnacPaePortNumber,
                                           pi4NextFsPnacPaePortNumber)
        != PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
   /*** $$TRACE_LOG (ENTRY, "FsPnacPaePortNumber = %d\n", *pi4FsPnacPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPnacPaePortAuthMode
 Input       :  The Indices
                FsPnacPaePortNumber

                The Object 
                retValFsPnacPaePortAuthMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacPaePortAuthMode ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacPaePortAuthMode (INT4 i4FsPnacPaePortNumber,
                             INT4 *pi4RetValFsPnacPaePortAuthMode)
#else
INT1
nmhGetFsPnacPaePortAuthMode (i4FsPnacPaePortNumber,
                             pi4RetValFsPnacPaePortAuthMode)
     INT4                i4FsPnacPaePortNumber;
     INT4               *pi4RetValFsPnacPaePortAuthMode;
#endif
{

    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4FsPnacPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPnacPaePortAuthMode = (INT4) pPortInfo->u1PortAuthMode;
        return SNMP_SUCCESS;
    }
   /*** $$TRACE_LOG (ENTRY, "FsPnacPaePortNumber = %d\n", i4FsPnacPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacPaePortSupplicantCount
 Input       :  The Indices
                FsPnacPaePortNumber

                The Object 
                retValFsPnacPaePortSupplicantCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacPaePortSupplicantCount ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacPaePortSupplicantCount (INT4 i4FsPnacPaePortNumber,
                                    UINT4
                                    *pu4RetValFsPnacPaePortSupplicantCount)
#else
INT1
nmhGetFsPnacPaePortSupplicantCount (i4FsPnacPaePortNumber,
                                    pu4RetValFsPnacPaePortSupplicantCount)
     INT4                i4FsPnacPaePortNumber;
     UINT4              *pu4RetValFsPnacPaePortSupplicantCount;
#endif
{

    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4FsPnacPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsPnacPaePortSupplicantCount = (UINT4) pPortInfo->u2SuppCount;
        return SNMP_SUCCESS;
    }
   /*** $$TRACE_LOG (ENTRY, "FsPnacPaePortNumber = %d\n", i4FsPnacPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacPaePortUserName
 Input       :  The Indices
                FsPnacPaePortNumber

                The Object 
                retValFsPnacPaePortUserName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacPaePortUserName ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacPaePortUserName (INT4 i4FsPnacPaePortNumber,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsPnacPaePortUserName)
#else
INT1
nmhGetFsPnacPaePortUserName (i4FsPnacPaePortNumber,
                             pRetValFsPnacPaePortUserName)
     INT4                i4FsPnacPaePortNumber;
     tSNMP_OCTET_STRING_TYPE *pRetValFsPnacPaePortUserName;
#endif
{

    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacSuppSessionNode *pSessionNode = NULL;

    if (PnacGetPortEntry ((UINT2) i4FsPnacPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {

        return SNMP_FAILURE;

    }
    else if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: This object does not exist for Authenticator-only-capable port !!\n");
        /* User name valid in this port, while operating as a Supplicant. */
        PNAC_MEMSET (pRetValFsPnacPaePortUserName->pu1_OctetList,
                     PNAC_INIT_VAL, pRetValFsPnacPaePortUserName->i4_Length);
        pRetValFsPnacPaePortUserName->i4_Length = 0;
        return SNMP_SUCCESS;
    }

    pSessionNode = pPortInfo->pPortSuppSessionNode;

    pRetValFsPnacPaePortUserName->i4_Length =
        (INT4) pSessionNode->u2UserNameLen;

    PNAC_MEMCPY (pRetValFsPnacPaePortUserName->pu1_OctetList,
                 pSessionNode->au1UserName, pSessionNode->u2UserNameLen);

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "FsPnacPaePortNumber = %d\n", i4FsPnacPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacPaePortPassword
 Input       :  The Indices
                FsPnacPaePortNumber

                The Object 
                retValFsPnacPaePortPassword
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacPaePortPassword ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacPaePortPassword (INT4 i4FsPnacPaePortNumber,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsPnacPaePortPassword)
#else
INT1
nmhGetFsPnacPaePortPassword (i4FsPnacPaePortNumber,
                             pRetValFsPnacPaePortPassword)
     INT4                i4FsPnacPaePortNumber;
     tSNMP_OCTET_STRING_TYPE *pRetValFsPnacPaePortPassword;
#endif
{

    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacSuppSessionNode *pSessionNode = NULL;
    UINT1               au1Password[PNAC_MAX_LEN_PASSWD];
    UINT2               u2Count;

    PNAC_MEMSET (au1Password, PNAC_INIT_VAL, PNAC_MAX_LEN_PASSWD);

    if (PnacGetPortEntry ((UINT2) i4FsPnacPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: This object does not exist for Authenticator-only-capable port !!\n");
        /* Password valid in this port, while operating as a Supplicant. */
        PNAC_MEMSET (pRetValFsPnacPaePortPassword->pu1_OctetList,
                     PNAC_INIT_VAL, pRetValFsPnacPaePortPassword->i4_Length);
        pRetValFsPnacPaePortPassword->i4_Length = 0;
        return SNMP_SUCCESS;
    }

    pSessionNode = pPortInfo->pPortSuppSessionNode;

    pRetValFsPnacPaePortPassword->i4_Length = pSessionNode->u2PasswdLen;

    /* If Mib save is in progress either by write startup or by incremental save
       copy and store the encrypted password in iss.conf file
     */
    if ((MsrGetSaveStatus () == ISS_TRUE) || (MsrIsSaveComplete () != ISS_TRUE))
    {
        PNAC_MEMCPY (pRetValFsPnacPaePortPassword->pu1_OctetList,
                     pSessionNode->au1Passwd, pSessionNode->u2PasswdLen);
        return SNMP_SUCCESS;

    }

    for (u2Count = 0; u2Count < (pSessionNode->u2PasswdLen); u2Count++)
    {
        au1Password[u2Count] = '*';
    }

    PNAC_MEMCPY (pRetValFsPnacPaePortPassword->pu1_OctetList,
                 au1Password, pSessionNode->u2PasswdLen);

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "FsPnacPaePortNumber = %d\n", i4FsPnacPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacPaePortStatus
 Input       :  The Indices
                FsPnacPaePortNumber

                The Object 
                retValFsPnacPaePortStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacPaePortStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacPaePortStatus (INT4 i4FsPnacPaePortNumber,
                           INT4 *pi4RetValFsPnacPaePortStatus)
#else
INT1
nmhGetFsPnacPaePortStatus (i4FsPnacPaePortNumber, pi4RetValFsPnacPaePortStatus)
     INT4                i4FsPnacPaePortNumber;
     INT4               *pi4RetValFsPnacPaePortStatus;
#endif
{

    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4FsPnacPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPnacPaePortStatus = (INT4) gPnacSystemInfo.
            aPnacPaePortInfo[i4FsPnacPaePortNumber].u2PortStatus;
        return SNMP_SUCCESS;
    }
   /*** $$TRACE_LOG (ENTRY, "FsPnacPaePortNumber = %d\n", i4FsPnacPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsPnacPaePortStatisticsClear
 Input       :  The Indices
                FsPnacPaePortNumber

                The Object
                setValFsPnacPaePortStatisticsClear
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsPnacPaePortStatisticsClear(INT4 i4FsPnacPaePortNumber , INT4 i4SetValFsPnacPaePortStatisticsClear)
{

    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4FsPnacPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)

    {
        return SNMP_FAILURE;
    }
   
    if (i4SetValFsPnacPaePortStatisticsClear == PNAC_ENABLED)
    {
        PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolFramesRx = PNAC_INIT_VAL;
	PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolFramesTx = PNAC_INIT_VAL;
	PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolStartFramesRx = PNAC_INIT_VAL;
	PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolLogoffFramesRx = PNAC_INIT_VAL;
	PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolRespIdFramesRx = PNAC_INIT_VAL;
	PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolRespFramesRx = PNAC_INIT_VAL;
	PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolReqIdFramesTx = PNAC_INIT_VAL;
	PNAC_AUTH_STATS_ENTRY (pPortInfo).u4LastEapolFrameVersion = PNAC_INIT_VAL;
	PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapolReqFramesTx = PNAC_INIT_VAL;
	PNAC_AUTH_STATS_ENTRY (pPortInfo).u4InvalidEapolFramesRx = PNAC_INIT_VAL;
	PNAC_AUTH_STATS_ENTRY (pPortInfo).u4EapLengthErrorFramesRx = PNAC_INIT_VAL;
	MEMSET((PNAC_AUTH_STATS_ENTRY (pPortInfo).lastEapolFrameSource),0,MAC_ADDR_LEN);
    }

    return SNMP_SUCCESS;


}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPnacPaePortAuthMode
 Input       :  The Indices
                FsPnacPaePortNumber

                The Object 
                setValFsPnacPaePortAuthMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsPnacPaePortAuthMode ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsPnacPaePortAuthMode (INT4 i4FsPnacPaePortNumber,
                             INT4 i4SetValFsPnacPaePortAuthMode)
#else
INT1
nmhSetFsPnacPaePortAuthMode (i4FsPnacPaePortNumber,
                             i4SetValFsPnacPaePortAuthMode)
     INT4                i4FsPnacPaePortNumber;
     INT4                i4SetValFsPnacPaePortAuthMode;

#endif
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    INT1                i1Result = SNMP_SUCCESS;
    INT1                i1CurrAuthMode = 0;

    if (PnacGetPortEntry ((UINT2) i4FsPnacPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1CurrAuthMode = pPortInfo->u1PortAuthMode;

    i1Result = PnacLowSetPortAuthMode ((UINT2) i4FsPnacPaePortNumber,
                                       (UINT2) i4SetValFsPnacPaePortAuthMode);
    if (SNMP_SUCCESS == i1Result)
    {
        /* clearing the mac entries if mac based pnac or disabled, if the
         * current mode is not same */
        if (i4SetValFsPnacPaePortAuthMode != i1CurrAuthMode)
        {
#ifdef NPAPI_WANTED
            PnacIssHwSetLearningMode (i4FsPnacPaePortNumber, 0);
#endif
        }
    }

    return i1Result;
   /*** $$TRACE_LOG (ENTRY, "FsPnacPaePortNumber = %d\n", i4FsPnacPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY, "FsPnacPaePortAuthMode = %d\n", i4SetValFsPnacPaePortAuthMode); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsPnacPaePortUserName
 Input       :  The Indices
                FsPnacPaePortNumber

                The Object 
                setValFsPnacPaePortUserName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsPnacPaePortUserName ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsPnacPaePortUserName (INT4 i4FsPnacPaePortNumber,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsPnacPaePortUserName)
#else
INT1
nmhSetFsPnacPaePortUserName (i4FsPnacPaePortNumber,
                             pSetValFsPnacPaePortUserName)
     INT4                i4FsPnacPaePortNumber;
     tSNMP_OCTET_STRING_TYPE *pSetValFsPnacPaePortUserName;

#endif
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacSuppSessionNode *pSessionNode = NULL;

    if (PnacGetPortEntry ((UINT2) i4FsPnacPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {

        return SNMP_FAILURE;

    }
    else if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: This object does not exist for Authenticator-only-capable port !!\n");
        return SNMP_FAILURE;
    }

    pSessionNode = pPortInfo->pPortSuppSessionNode;
    PNAC_MEMSET (pSessionNode->au1UserName, PNAC_INIT_VAL, PNAC_USER_NAME_SIZE);

    PNAC_MEMCPY (pSessionNode->au1UserName,
                 pSetValFsPnacPaePortUserName->pu1_OctetList,
                 pSetValFsPnacPaePortUserName->i4_Length);

    pSessionNode->u2UserNameLen =
        (UINT2) pSetValFsPnacPaePortUserName->i4_Length;

    return SNMP_SUCCESS;

    /*** $$TRACE_LOG (ENTRY, "FsPnacPaePortNumber = %d\n", i4FsPnacPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsPnacPaePortPassword
 Input       :  The Indices
                FsPnacPaePortNumber

                The Object 
                setValFsPnacPaePortPassword
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsPnacPaePortPassword ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsPnacPaePortPassword (INT4 i4FsPnacPaePortNumber,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsPnacPaePortPassword)
#else
INT1
nmhSetFsPnacPaePortPassword (i4FsPnacPaePortNumber,
                             pSetValFsPnacPaePortPassword)
     INT4                i4FsPnacPaePortNumber;
     tSNMP_OCTET_STRING_TYPE *pSetValFsPnacPaePortPassword;

#endif
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacSuppSessionNode *pSessionNode = NULL;
    UINT4               u4PassLength = 0;

    if (PnacGetPortEntry ((UINT2) i4FsPnacPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {

        return SNMP_FAILURE;

    }
    else if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: This object does not exist for Authenticator-only-capable port !!\n");
        return SNMP_FAILURE;
    }

    pSessionNode = pPortInfo->pPortSuppSessionNode;
    PNAC_MEMSET (pSessionNode->au1Passwd, PNAC_INIT_VAL, PNAC_SUPP_PASSWD_SIZE);
    u4PassLength = (pSetValFsPnacPaePortPassword->i4_Length);
    if (MsrIsMibRestoreInProgress () == MSR_TRUE)
    {
        PNAC_MEMCPY (pSessionNode->au1Passwd,
                     pSetValFsPnacPaePortPassword->pu1_OctetList, u4PassLength);
        pSessionNode->au1Passwd[u4PassLength] = '\0';

        pSessionNode->u2PasswdLen = (UINT2) u4PassLength;
        return SNMP_SUCCESS;
    }

    PNAC_MEMCPY (pSessionNode->au1Passwd,
                 pSetValFsPnacPaePortPassword->pu1_OctetList, u4PassLength);
    pSessionNode->au1Passwd[u4PassLength] = '\0';

    pSessionNode->u2PasswdLen = (UINT2) u4PassLength;

    FsUtlEncryptPasswd ((CHR1 *) pSessionNode->au1Passwd);

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "FsPnacPaePortNumber = %d\n", i4FsPnacPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}


/****************************************************************************
 Function    :  nmhSetFsPnacPaePortAuthStatus
 Input       :  The Indices
                FsPnacPaePortNumber
 
                The Object
                setValFsPnacPaePortAuthStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhSetFsPnacPaePortAuthStatus(INT4 i4FsPnacPaePortNumber , INT4 i4SetValFsPnacPaePortAuthStatus)
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4FsPnacPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (PnacLowSetPortPaeStatus ((UINT2) i4FsPnacPaePortNumber,(UINT2) i4SetValFsPnacPaePortAuthStatus)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPnacPaeAuthReAuthMax
 Input       :  The Indices
                FsPnacPaePortNumber

                The Object
                setValFsPnacPaeAuthReAuthMax
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsPnacPaeAuthReAuthMax(INT4 i4FsPnacPaePortNumber , UINT4 u4SetValFsPnacPaeAuthReAuthMax)
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4FsPnacPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4ReAuthMax ==
            u4SetValFsPnacPaeAuthReAuthMax)
        {
            return SNMP_SUCCESS;
        }

        PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4ReAuthMax = u4SetValFsPnacPaeAuthReAuthMax;
        return SNMP_SUCCESS;
    }
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPnacPaePortAuthMode
 Input       :  The Indices
                FsPnacPaePortNumber

                The Object 
                testValFsPnacPaePortAuthMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsPnacPaePortAuthMode ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsPnacPaePortAuthMode (UINT4 *pu4ErrorCode, INT4 i4FsPnacPaePortNumber,
                                INT4 i4TestValFsPnacPaePortAuthMode)
#else
INT1
nmhTestv2FsPnacPaePortAuthMode (pu4ErrorCode, i4FsPnacPaePortNumber,
                                i4TestValFsPnacPaePortAuthMode)
     UINT4              *pu4ErrorCode;
     INT4                i4FsPnacPaePortNumber;
     INT4                i4TestValFsPnacPaePortAuthMode;
#endif
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4FsPnacPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (PnacLowTestPortAuthMode
        (pu4ErrorCode, (UINT2) i4FsPnacPaePortNumber,
         (UINT2) i4TestValFsPnacPaePortAuthMode) == SNMP_FAILURE)
    {
        if ((i4TestValFsPnacPaePortAuthMode == PNAC_PORT_AUTHMODE_MACBASED) &&
            (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl !=
             PNAC_PORTCNTRL_AUTO))
        {
            CLI_SET_ERR (CLI_PNAC_NOT_AUTO);
        }
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPnacPaePortAuthMode == PNAC_PORT_AUTHMODE_MACBASED) &&
        (gPnacSystemInfo.u1SystemMode == PNAC_DISTRIBUTED))
    {
        CLI_SET_ERR (CLI_DPNAC_MAC_MODE_NOT_ALLOWED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (ENTRY, "FsPnacPaePortNumber = %d\n", i4FsPnacPaePortNumber); ***/
   /*** $$TRACE_LOG (ENTRY, "FsPnacPaePortAuthMode = %d\n", i4TestValFsPnacPaePortAuthMode); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsPnacPaePortUserName
 Input       :  The Indices
                FsPnacPaePortNumber

                The Object 
                testValFsPnacPaePortUserName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsPnacPaePortUserName ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsPnacPaePortUserName (UINT4 *pu4ErrorCode, INT4 i4FsPnacPaePortNumber,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsPnacPaePortUserName)
#else
INT1
nmhTestv2FsPnacPaePortUserName (pu4ErrorCode, i4FsPnacPaePortNumber,
                                pTestValFsPnacPaePortUserName)
     UINT4              *pu4ErrorCode;
     INT4                i4FsPnacPaePortNumber;
     tSNMP_OCTET_STRING_TYPE *pTestValFsPnacPaePortUserName;
#endif
{
    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4FsPnacPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))
    {

        *pu4ErrorCode = (UINT4) SNMP_ERR_NO_CREATION;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: This object does not exist for Authenticator-only-capable port !!\n");
        return SNMP_FAILURE;
    }

    if (PnacSnmpLowValidatePortNumber (i4FsPnacPaePortNumber) != PNAC_SUCCESS)
    {

        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else if ((pTestValFsPnacPaePortUserName->i4_Length <
              PNAC_MIN_LEN_USERNAME) ||
             (pTestValFsPnacPaePortUserName->i4_Length > PNAC_MAX_LEN_USERNAME))
    {

        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_LENGTH;
        PNAC_TRC_ARG2 (PNAC_ALL_FAILURE_TRC, " SNMPPROP: Use only %d to"
                       " %d number of characters for setting 'UserName'",
                       PNAC_MIN_LEN_USERNAME, PNAC_MAX_LEN_USERNAME);

        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValFsPnacPaePortUserName->pu1_OctetList,
                              pTestValFsPnacPaePortUserName->i4_Length)
        == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (ENTRY, "FsPnacPaePortNumber = %d\n", i4FsPnacPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsPnacPaePortPassword
 Input       :  The Indices
                FsPnacPaePortNumber

                The Object 
                testValFsPnacPaePortPassword
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsPnacPaePortPassword ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsPnacPaePortPassword (UINT4 *pu4ErrorCode, INT4 i4FsPnacPaePortNumber,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsPnacPaePortPassword)
#else
INT1
nmhTestv2FsPnacPaePortPassword (pu4ErrorCode, i4FsPnacPaePortNumber,
                                pTestValFsPnacPaePortPassword)
     UINT4              *pu4ErrorCode;
     INT4                i4FsPnacPaePortNumber;
     tSNMP_OCTET_STRING_TYPE *pTestValFsPnacPaePortPassword;
#endif
{

    tPnacPaePortEntry  *pPortInfo = NULL;

    if (PnacGetPortEntry ((UINT2) i4FsPnacPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else if (PNAC_IS_PORT_NOT_SUPP_CAPABLE (pPortInfo))
    {

        *pu4ErrorCode = (UINT4) SNMP_ERR_NO_CREATION;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: This object does not exist for Authenticator-only-capable port !!\n");
        return SNMP_FAILURE;
    }

    if (PnacSnmpLowValidatePortNumber (i4FsPnacPaePortNumber) != PNAC_SUCCESS)
    {

        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else if ((pTestValFsPnacPaePortPassword->i4_Length < PNAC_MIN_LEN_PASSWD)
             ||
             (pTestValFsPnacPaePortPassword->i4_Length > PNAC_MAX_LEN_PASSWD))
    {

        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_LENGTH;
        PNAC_TRC_ARG2 (PNAC_ALL_FAILURE_TRC, " SNMPPROP: Use only %d"
                       " to %d number of characters for setting 'Passwd'",
                       PNAC_MIN_LEN_PASSWD, PNAC_MAX_LEN_PASSWD);

        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValFsPnacPaePortPassword->pu1_OctetList,
                              pTestValFsPnacPaePortPassword->i4_Length)
        == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (ENTRY, "FsPnacPaePortNumber = %d\n", i4FsPnacPaePortNumber); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsPnacPaePortAuthStatus
 Input       :  The Indices
                FsPnacPaePortNumber

                The Object
                testValFsPnacPaePortAuthStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsPnacPaePortAuthStatus (UINT4 *pu4ErrorCode , INT4 i4FsPnacPaePortNumber , INT4 i4TestValFsPnacPaePortAuthStatus)
{
    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (PnacLowTestPortPaeStatus (pu4ErrorCode,(UINT2) i4FsPnacPaePortNumber,
                                  (UINT2) i4TestValFsPnacPaePortAuthStatus) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}


/****************************************************************************
 Function    :  nmhTestv2FsPnacPaeAuthReAuthMax
 Input       :  The Indices
                FsPnacPaePortNumber

                The Object
                testValFsPnacPaeAuthReAuthMax
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsPnacPaeAuthReAuthMax(UINT4 *pu4ErrorCode , INT4 i4FsPnacPaePortNumber , UINT4 u4TestValFsPnacPaeAuthReAuthMax)
{                                                                                                                     tPnacPaePortEntry  *pPortInfo = NULL;
    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (PnacGetPortEntry ((UINT2) i4FsPnacPaePortNumber, &pPortInfo) !=
        PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (!PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else if ((u4TestValFsPnacPaeAuthReAuthMax < (UINT4) PNAC_MAXREQ_MIN) ||
             (u4TestValFsPnacPaeAuthReAuthMax > (UINT4) PNAC_MAXREQ_MAX))

    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPSTD: ReAuthMax for Authenticator"
                  " cannot be less than 1  \n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}


/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPnacPaePortTable
 Input       :  The Indices
                FsPnacPaePortNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPnacPaePortTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/* Low Level GET Routine for All Objects  */

/* LOW LEVEL Routines for Table : FsPnacAuthSessionTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPnacAuthSessionTable
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsPnacAuthSessionTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsPnacAuthSessionTable (tMacAddr
                                                FsPnacAuthSessionSuppAddress)
#else
INT1
nmhValidateIndexInstanceFsPnacAuthSessionTable (FsPnacAuthSessionSuppAddress)
     tMacAddr            FsPnacAuthSessionSuppAddress;
#endif
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacAuthSessionNode *pSessNode = NULL;
    if (PNAC_IS_SHUTDOWN ())
    {

        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowValidateSuppAddress (FsPnacAuthSessionSuppAddress) !=
        PNAC_SUCCESS)
    {

        return SNMP_FAILURE;
    }

    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pPortInfo = &(PNAC_PORT_INFO (pSessNode->u2PortNo));
    if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPnacAuthSessionTable
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstFsPnacAuthSessionTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsPnacAuthSessionTable (tMacAddr *
                                        pFsPnacAuthSessionSuppAddress)
#else
INT1
nmhGetFirstIndexFsPnacAuthSessionTable (pFsPnacAuthSessionSuppAddress)
     tMacAddr           *pFsPnacAuthSessionSuppAddress;
#endif
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacAuthSessionNode *pSessNode = NULL;
    if (PNAC_IS_SHUTDOWN ())
    {

        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetFirstValidSuppAddress
        ((UINT1 *) *pFsPnacAuthSessionSuppAddress) != PNAC_SUCCESS)
    {

        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetAuthSessionNode ((UINT1 *) *pFsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pPortInfo = &(PNAC_PORT_INFO (pSessNode->u2PortNo));
    if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPnacAuthSessionTable
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress
                nextFsPnacAuthSessionSuppAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextFsPnacAuthSessionTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexFsPnacAuthSessionTable (tMacAddr FsPnacAuthSessionSuppAddress,
                                       tMacAddr *
                                       pNextFsPnacAuthSessionSuppAddress)
#else
INT1
nmhGetNextIndexFsPnacAuthSessionTable (FsPnacAuthSessionSuppAddress,
                                       pNextFsPnacAuthSessionSuppAddress)
     tMacAddr            FsPnacAuthSessionSuppAddress;
     tMacAddr           *pNextFsPnacAuthSessionSuppAddress;
#endif
{
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacAuthSessionNode *pSessNode = NULL;
    if (PNAC_IS_SHUTDOWN ())
    {

        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetNextValidSuppAddress (FsPnacAuthSessionSuppAddress,
                                            pNextFsPnacAuthSessionSuppAddress)
        != PNAC_SUCCESS)
    {

        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pPortInfo = &(PNAC_PORT_INFO (pSessNode->u2PortNo));
    if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsPnacAuthSessionIdentifier
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object 
                retValFsPnacAuthSessionIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacAuthSessionIdentifier ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacAuthSessionIdentifier (tMacAddr FsPnacAuthSessionSuppAddress,
                                   INT4 *pi4RetValFsPnacAuthSessionIdentifier)
#else
INT1
nmhGetFsPnacAuthSessionIdentifier (FsPnacAuthSessionSuppAddress,
                                   pi4RetValFsPnacAuthSessionIdentifier)
     tMacAddr            FsPnacAuthSessionSuppAddress;
     INT4               *pi4RetValFsPnacAuthSessionIdentifier;
#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;

    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {
        return SNMP_FAILURE;

    }
    else
    {

        *pi4RetValFsPnacAuthSessionIdentifier =
            pSessNode->authFsmInfo.u2CurrentId;
        return SNMP_SUCCESS;

    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacAuthSessionAuthPaeState
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object 
                retValFsPnacAuthSessionAuthPaeState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacAuthSessionAuthPaeState ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacAuthSessionAuthPaeState (tMacAddr FsPnacAuthSessionSuppAddress,
                                     INT4
                                     *pi4RetValFsPnacAuthSessionAuthPaeState)
#else
INT1
nmhGetFsPnacAuthSessionAuthPaeState (FsPnacAuthSessionSuppAddress,
                                     pi4RetValFsPnacAuthSessionAuthPaeState)
     tMacAddr            FsPnacAuthSessionSuppAddress;
     INT4               *pi4RetValFsPnacAuthSessionAuthPaeState;
#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;

    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {

        return SNMP_FAILURE;

    }
    else
    {

        *pi4RetValFsPnacAuthSessionAuthPaeState =
            pSessNode->authFsmInfo.u2AuthPaeState;
        return SNMP_SUCCESS;

    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacAuthSessionBackendAuthState
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object 
                retValFsPnacAuthSessionBackendAuthState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacAuthSessionBackendAuthState ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacAuthSessionBackendAuthState (tMacAddr
                                         FsPnacAuthSessionSuppAddress,
                                         INT4
                                         *pi4RetValFsPnacAuthSessionBackendAuthState)
#else
INT1
nmhGetFsPnacAuthSessionBackendAuthState (FsPnacAuthSessionSuppAddress,
                                         pi4RetValFsPnacAuthSessionBackendAuthState)
     tMacAddr            FsPnacAuthSessionSuppAddress;
     INT4               *pi4RetValFsPnacAuthSessionBackendAuthState;
#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;

    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {
        return SNMP_FAILURE;

    }
    else
    {

        *pi4RetValFsPnacAuthSessionBackendAuthState =
            pSessNode->authFsmInfo.u2AuthBackendAuthState;
        return SNMP_SUCCESS;

    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacAuthSessionPortStatus
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object 
                retValFsPnacAuthSessionPortStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacAuthSessionPortStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacAuthSessionPortStatus (tMacAddr FsPnacAuthSessionSuppAddress,
                                   INT4 *pi4RetValFsPnacAuthSessionPortStatus)
#else
INT1
nmhGetFsPnacAuthSessionPortStatus (FsPnacAuthSessionSuppAddress,
                                   pi4RetValFsPnacAuthSessionPortStatus)
     tMacAddr            FsPnacAuthSessionSuppAddress;
     INT4               *pi4RetValFsPnacAuthSessionPortStatus;
#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;

    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {

        return SNMP_FAILURE;

    }
    else
    {

        *pi4RetValFsPnacAuthSessionPortStatus =
            pSessNode->authFsmInfo.u2AuthControlPortStatus;
        return SNMP_SUCCESS;

    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacAuthSessionPortNumber
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object 
                retValFsPnacAuthSessionPortNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacAuthSessionPortNumber ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacAuthSessionPortNumber (tMacAddr FsPnacAuthSessionSuppAddress,
                                   INT4 *pi4RetValFsPnacAuthSessionPortNumber)
#else
INT1
nmhGetFsPnacAuthSessionPortNumber (FsPnacAuthSessionSuppAddress,
                                   pi4RetValFsPnacAuthSessionPortNumber)
     tMacAddr            FsPnacAuthSessionSuppAddress;
     INT4               *pi4RetValFsPnacAuthSessionPortNumber;
#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;

    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {

        return SNMP_FAILURE;

    }
    else
    {

        *pi4RetValFsPnacAuthSessionPortNumber = pSessNode->u2PortNo;
        return SNMP_SUCCESS;

    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacAuthSessionInitialize
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object 
                retValFsPnacAuthSessionInitialize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacAuthSessionInitialize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacAuthSessionInitialize (tMacAddr FsPnacAuthSessionSuppAddress,
                                   INT4 *pi4RetValFsPnacAuthSessionInitialize)
#else
INT1
nmhGetFsPnacAuthSessionInitialize (FsPnacAuthSessionSuppAddress,
                                   pi4RetValFsPnacAuthSessionInitialize)
     tMacAddr            FsPnacAuthSessionSuppAddress;
     INT4               *pi4RetValFsPnacAuthSessionInitialize;
#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;

    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {

        return SNMP_FAILURE;

    }
    else
    {

        if (pSessNode->authFsmInfo.bInitialize == PNAC_TRUE)
        {
            *pi4RetValFsPnacAuthSessionInitialize = PNAC_MIB_TRUE;
        }
        else if (pSessNode->authFsmInfo.bInitialize == PNAC_FALSE)
        {
            *pi4RetValFsPnacAuthSessionInitialize = PNAC_MIB_FALSE;
        }
        return SNMP_SUCCESS;

    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacAuthSessionReauthenticate
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object 
                retValFsPnacAuthSessionReauthenticate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacAuthSessionReauthenticate ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacAuthSessionReauthenticate (tMacAddr FsPnacAuthSessionSuppAddress,
                                       INT4
                                       *pi4RetValFsPnacAuthSessionReauthenticate)
#else
INT1
nmhGetFsPnacAuthSessionReauthenticate (FsPnacAuthSessionSuppAddress,
                                       pi4RetValFsPnacAuthSessionReauthenticate)
     tMacAddr            FsPnacAuthSessionSuppAddress;
     INT4               *pi4RetValFsPnacAuthSessionReauthenticate;
#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;

    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {
        *pi4RetValFsPnacAuthSessionReauthenticate = PNAC_MIB_FALSE;

        return SNMP_FAILURE;

    }
    else
    {

        *pi4RetValFsPnacAuthSessionReauthenticate = PNAC_MIB_TRUE;
        return SNMP_SUCCESS;

    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPnacAuthSessionInitialize
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object 
                setValFsPnacAuthSessionInitialize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsPnacAuthSessionInitialize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsPnacAuthSessionInitialize (tMacAddr FsPnacAuthSessionSuppAddress,
                                   INT4 i4SetValFsPnacAuthSessionInitialize)
#else
INT1
nmhSetFsPnacAuthSessionInitialize (FsPnacAuthSessionSuppAddress,
                                   i4SetValFsPnacAuthSessionInitialize)
     tMacAddr            FsPnacAuthSessionSuppAddress;
     INT4                i4SetValFsPnacAuthSessionInitialize;

#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;
    UINT4               u4MacAddr = 0;
    UINT2               u2MacAddr = 0;

    if (!PNAC_IS_ENABLED ())
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPPROP: Pnac Module is not"
                  " yet ENABLED for setting 'Initialize'\n");
        return SNMP_FAILURE;
    }

    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {

        return SNMP_FAILURE;

    }
    else
    {
        if (i4SetValFsPnacAuthSessionInitialize == PNAC_MIB_TRUE)
        {
            pSessNode->authFsmInfo.bInitialize = PNAC_TRUE;
        }
        else if (i4SetValFsPnacAuthSessionInitialize == PNAC_MIB_FALSE)
        {
            pSessNode->authFsmInfo.bInitialize = PNAC_FALSE;
        }

        /*  Call ASM with 'PNAC_ASM_EV_INITIALIZE' as event for this Supp */
        if (PnacAuthStateMachine (PNAC_ASM_EV_INITIALIZE,
                                  pSessNode->u2PortNo,
                                  pSessNode, NULL, PNAC_NO_VAL) != PNAC_SUCCESS)
        {

            PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                      " SNMPPROP: ASM failed to Initialize\n");
            return SNMP_FAILURE;
        }
        PNAC_GET_FOURBYTE (u4MacAddr, FsPnacAuthSessionSuppAddress);
        PNAC_GET_TWOBYTE (u2MacAddr, &(FsPnacAuthSessionSuppAddress[4]));
        PNAC_TRC_ARG2 (PNAC_MGMT_TRC,
                       " SNMPPROP: Supplicant %x-%x is set to 'Initialize'\n",
                       u4MacAddr, u2MacAddr);

        return SNMP_SUCCESS;

    }

   /*** $$TRACE_LOG (ENTRY, "FsPnacAuthSessionInitialize = %d\n", i4SetValFsPnacAuthSessionInitialize); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsPnacAuthSessionReauthenticate
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object 
                setValFsPnacAuthSessionReauthenticate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsPnacAuthSessionReauthenticate ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsPnacAuthSessionReauthenticate (tMacAddr FsPnacAuthSessionSuppAddress,
                                       INT4
                                       i4SetValFsPnacAuthSessionReauthenticate)
#else
INT1
nmhSetFsPnacAuthSessionReauthenticate (FsPnacAuthSessionSuppAddress,
                                       i4SetValFsPnacAuthSessionReauthenticate)
     tMacAddr            FsPnacAuthSessionSuppAddress;
     INT4                i4SetValFsPnacAuthSessionReauthenticate;

#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;
    UINT4               u4MacAddr = 0;
    UINT2               u2MacAddr = 0;

    if (!PNAC_IS_ENABLED ())
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPPROP: Pnac Module is not"
                  " yet ENABLED for setting 'Initialize'\n");
        return SNMP_FAILURE;
    }

    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {

        return SNMP_FAILURE;

    }
    else
    {

        if (i4SetValFsPnacAuthSessionReauthenticate == PNAC_MIB_TRUE)
        {
            pSessNode->authFsmInfo.bReAuthenticate = PNAC_TRUE;
        }
        else if (i4SetValFsPnacAuthSessionReauthenticate == PNAC_MIB_FALSE)
        {
            /* User configures FALSE, nothing to do and no need
             * to call the state m/c  */
            return SNMP_SUCCESS;
        }

        /* Call ASM with 'PNAC_ASM_EV_REAUTHENTICATE' as event for this Supp */

        if (PnacAuthStateMachine (PNAC_ASM_EV_REAUTHENTICATE,
                                  pSessNode->u2PortNo,
                                  pSessNode, NULL, PNAC_NO_VAL) != PNAC_SUCCESS)
        {

            PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                      " SNMPPROP: ASM failed to Reauthenticate\n");
            return SNMP_FAILURE;
        }

        PNAC_GET_FOURBYTE (u4MacAddr, FsPnacAuthSessionSuppAddress);
        PNAC_GET_TWOBYTE (u2MacAddr, &(FsPnacAuthSessionSuppAddress[4]));
        PNAC_TRC_ARG2 (PNAC_MGMT_TRC,
                       " SNMPPROP: Supplicant %x-%x is set to 'Reauthenticate'\n",
                       u4MacAddr, u2MacAddr);

        return SNMP_SUCCESS;

    }

   /*** $$TRACE_LOG (ENTRY, "FsPnacAuthSessionReauthenticate = %d\n", i4SetValFsPnacAuthSessionReauthenticate); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPnacAuthSessionInitialize
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object 
                testValFsPnacAuthSessionInitialize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsPnacAuthSessionInitialize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsPnacAuthSessionInitialize (UINT4 *pu4ErrorCode,
                                      tMacAddr FsPnacAuthSessionSuppAddress,
                                      INT4 i4TestValFsPnacAuthSessionInitialize)
#else
INT1
nmhTestv2FsPnacAuthSessionInitialize (pu4ErrorCode,
                                      FsPnacAuthSessionSuppAddress,
                                      i4TestValFsPnacAuthSessionInitialize)
     UINT4              *pu4ErrorCode;
     tMacAddr            FsPnacAuthSessionSuppAddress;
     INT4                i4TestValFsPnacAuthSessionInitialize;
#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;
    if ((i4TestValFsPnacAuthSessionInitialize != PNAC_MIB_TRUE) &&
        (i4TestValFsPnacAuthSessionInitialize != PNAC_MIB_FALSE))
    {

        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPPROP: Use only '1' or"
                  " '2' for setting 'Initialize'\n");
        return SNMP_FAILURE;
    }
    else if (PnacSnmpLowValidateSuppAddress (FsPnacAuthSessionSuppAddress) !=
             PNAC_SUCCESS)
    {

        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: Use valid Supplicant address\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {
        CLI_SET_ERR (CLI_SUPP_MAC_NOT_EXISTS);
        return SNMP_FAILURE;
    }

    pPortInfo = &(PNAC_PORT_INFO (pSessNode->u2PortNo));
    if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "FsPnacAuthSessionInitialize = %d\n", i4TestValFsPnacAuthSessionInitialize); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsPnacAuthSessionReauthenticate
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object 
                testValFsPnacAuthSessionReauthenticate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsPnacAuthSessionReauthenticate ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsPnacAuthSessionReauthenticate (UINT4 *pu4ErrorCode,
                                          tMacAddr
                                          FsPnacAuthSessionSuppAddress,
                                          INT4
                                          i4TestValFsPnacAuthSessionReauthenticate)
#else
INT1
nmhTestv2FsPnacAuthSessionReauthenticate (pu4ErrorCode,
                                          FsPnacAuthSessionSuppAddress,
                                          i4TestValFsPnacAuthSessionReauthenticate)
     UINT4              *pu4ErrorCode;
     tMacAddr            FsPnacAuthSessionSuppAddress;
     INT4                i4TestValFsPnacAuthSessionReauthenticate;
#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;

    if ((i4TestValFsPnacAuthSessionReauthenticate != PNAC_MIB_TRUE) &&
        (i4TestValFsPnacAuthSessionReauthenticate != PNAC_MIB_FALSE))
    {

        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPPROP: Use only '1' or"
                  " '2' for setting 'Reauthenticate'\n");
        return SNMP_FAILURE;
    }

    else if (PnacSnmpLowValidateSuppAddress (FsPnacAuthSessionSuppAddress) !=
             PNAC_SUCCESS)
    {

        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: Use valid Supplicant address\n");
        return SNMP_FAILURE;
    }

    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {
        CLI_SET_ERR (CLI_SUPP_MAC_NOT_EXISTS);
        return SNMP_FAILURE;
    }

    pPortInfo = &(PNAC_PORT_INFO (pSessNode->u2PortNo));
    if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPPROP: Can't Reauthenticate"
                  " on Authenticator-incapable port\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (ENTRY, "FsPnacAuthSessionReauthenticate = %d\n", i4TestValFsPnacAuthSessionReauthenticate); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPnacAuthSessionTable
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPnacAuthSessionTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPnacAuthSessionStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPnacAuthSessionStatsTable
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsPnacAuthSessionStatsTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsPnacAuthSessionStatsTable (tMacAddr
                                                     FsPnacAuthSessionSuppAddress)
#else
INT1                nmhValidateIndexInstanceFsPnacAuthSessionStatsTable
    (FsPnacAuthSessionSuppAddress)
     tMacAddr            FsPnacAuthSessionSuppAddress;
#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PNAC_IS_SHUTDOWN ())
    {

        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowValidateSuppAddress (FsPnacAuthSessionSuppAddress) !=
        PNAC_SUCCESS)
    {

        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pPortInfo = &(PNAC_PORT_INFO (pSessNode->u2PortNo));
    if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPnacAuthSessionStatsTable
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstFsPnacAuthSessionStatsTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsPnacAuthSessionStatsTable (tMacAddr *
                                             pFsPnacAuthSessionSuppAddress)
#else
INT1
nmhGetFirstIndexFsPnacAuthSessionStatsTable (pFsPnacAuthSessionSuppAddress)
     tMacAddr           *pFsPnacAuthSessionSuppAddress;
#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PNAC_IS_SHUTDOWN ())
    {

        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetFirstValidSuppAddress
        ((UINT1 *) *pFsPnacAuthSessionSuppAddress) != PNAC_SUCCESS)
    {

        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetAuthSessionNode ((UINT1 *) *pFsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pPortInfo = &(PNAC_PORT_INFO (pSessNode->u2PortNo));
    if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPnacAuthSessionStatsTable
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress
                nextFsPnacAuthSessionSuppAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextFsPnacAuthSessionStatsTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexFsPnacAuthSessionStatsTable (tMacAddr
                                            FsPnacAuthSessionSuppAddress,
                                            tMacAddr *
                                            pNextFsPnacAuthSessionSuppAddress)
#else
INT1
nmhGetNextIndexFsPnacAuthSessionStatsTable (FsPnacAuthSessionSuppAddress,
                                            pNextFsPnacAuthSessionSuppAddress)
     tMacAddr            FsPnacAuthSessionSuppAddress;
     tMacAddr           *pNextFsPnacAuthSessionSuppAddress;
#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;
    if (PNAC_IS_SHUTDOWN ())
    {

        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetNextValidSuppAddress (FsPnacAuthSessionSuppAddress,
                                            pNextFsPnacAuthSessionSuppAddress)
        != PNAC_SUCCESS)
    {

        return SNMP_FAILURE;
    }
    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pPortInfo = &(PNAC_PORT_INFO (pSessNode->u2PortNo));
    if (PNAC_IS_PORT_NOT_AUTH_CAPABLE (pPortInfo))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPnacAuthSessionOctetsRx
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object 
                retValFsPnacAuthSessionOctetsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacAuthSessionOctetsRx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacAuthSessionOctetsRx (tMacAddr FsPnacAuthSessionSuppAddress,
                                 tSNMP_COUNTER64_TYPE *
                                 pu8RetValFsPnacAuthSessionOctetsRx)
#else
INT1
nmhGetFsPnacAuthSessionOctetsRx (FsPnacAuthSessionSuppAddress,
                                 pu8RetValFsPnacAuthSessionOctetsRx)
     tMacAddr            FsPnacAuthSessionSuppAddress;
     tSNMP_COUNTER64_TYPE *pu8RetValFsPnacAuthSessionOctetsRx;
#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;
    tPnacAuthSessStats *pSessStatsEntry = NULL;

    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {

        return SNMP_FAILURE;

    }
    else
    {

        pSessStatsEntry = &(pSessNode->authSessStats);
        pu8RetValFsPnacAuthSessionOctetsRx->msn =
            pSessStatsEntry->u8SessOctetsRx.u4HiWord;
        pu8RetValFsPnacAuthSessionOctetsRx->lsn =
            pSessStatsEntry->u8SessOctetsRx.u4LoWord;

        return SNMP_SUCCESS;

    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacAuthSessionOctetsTx
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object 
                retValFsPnacAuthSessionOctetsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacAuthSessionOctetsTx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacAuthSessionOctetsTx (tMacAddr FsPnacAuthSessionSuppAddress,
                                 tSNMP_COUNTER64_TYPE *
                                 pu8RetValFsPnacAuthSessionOctetsTx)
#else
INT1
nmhGetFsPnacAuthSessionOctetsTx (FsPnacAuthSessionSuppAddress,
                                 pu8RetValFsPnacAuthSessionOctetsTx)
     tMacAddr            FsPnacAuthSessionSuppAddress;
     tSNMP_COUNTER64_TYPE *pu8RetValFsPnacAuthSessionOctetsTx;
#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;
    tPnacAuthSessStats *pSessStatsEntry = NULL;

    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {

        return SNMP_FAILURE;

    }
    else
    {

        pSessStatsEntry = &(pSessNode->authSessStats);
        pu8RetValFsPnacAuthSessionOctetsTx->msn =
            pSessStatsEntry->u8SessOctetsTx.u4HiWord;
        pu8RetValFsPnacAuthSessionOctetsTx->lsn =
            pSessStatsEntry->u8SessOctetsTx.u4LoWord;

        return SNMP_SUCCESS;

    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacAuthSessionFramesRx
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object 
                retValFsPnacAuthSessionFramesRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacAuthSessionFramesRx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacAuthSessionFramesRx (tMacAddr FsPnacAuthSessionSuppAddress,
                                 UINT4 *pu4RetValFsPnacAuthSessionFramesRx)
#else
INT1
nmhGetFsPnacAuthSessionFramesRx (FsPnacAuthSessionSuppAddress,
                                 pu4RetValFsPnacAuthSessionFramesRx)
     tMacAddr            FsPnacAuthSessionSuppAddress;
     UINT4              *pu4RetValFsPnacAuthSessionFramesRx;
#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;
    tPnacAuthSessStats *pSessStatsEntry = NULL;

    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {

        return SNMP_FAILURE;

    }
    else
    {

        pSessStatsEntry = &(pSessNode->authSessStats);
        *pu4RetValFsPnacAuthSessionFramesRx = pSessStatsEntry->u4SessFramesRx;
        return SNMP_SUCCESS;

    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacAuthSessionFramesTx
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object 
                retValFsPnacAuthSessionFramesTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacAuthSessionFramesTx ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacAuthSessionFramesTx (tMacAddr FsPnacAuthSessionSuppAddress,
                                 UINT4 *pu4RetValFsPnacAuthSessionFramesTx)
#else
INT1
nmhGetFsPnacAuthSessionFramesTx (FsPnacAuthSessionSuppAddress,
                                 pu4RetValFsPnacAuthSessionFramesTx)
     tMacAddr            FsPnacAuthSessionSuppAddress;
     UINT4              *pu4RetValFsPnacAuthSessionFramesTx;
#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;
    tPnacAuthSessStats *pSessStatsEntry = NULL;

    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {

        return SNMP_FAILURE;

    }
    else
    {

        pSessStatsEntry = &(pSessNode->authSessStats);
        *pu4RetValFsPnacAuthSessionFramesTx = pSessStatsEntry->u4SessFramesTx;
        return SNMP_SUCCESS;

    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacAuthSessionId
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object 
                retValFsPnacAuthSessionId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacAuthSessionId ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacAuthSessionId (tMacAddr FsPnacAuthSessionSuppAddress,
                           tSNMP_OCTET_STRING_TYPE * pRetValFsPnacAuthSessionId)
#else
INT1
nmhGetFsPnacAuthSessionId (FsPnacAuthSessionSuppAddress,
                           pRetValFsPnacAuthSessionId)
     tMacAddr            FsPnacAuthSessionSuppAddress;
     tSNMP_OCTET_STRING_TYPE *pRetValFsPnacAuthSessionId;
#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;
    tPnacAuthSessStats *pSessStatsEntry = NULL;

    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {

        return SNMP_FAILURE;

    }
    else
    {
        pSessStatsEntry = &(pSessNode->authSessStats);
        MEMCPY (pRetValFsPnacAuthSessionId->pu1_OctetList,
                pSessStatsEntry->aSessId, pSessStatsEntry->u2SessIdLen);
        pRetValFsPnacAuthSessionId->i4_Length = pSessStatsEntry->u2SessIdLen;
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacAuthSessionAuthenticMethod
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object 
                retValFsPnacAuthSessionAuthenticMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacAuthSessionAuthenticMethod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacAuthSessionAuthenticMethod (tMacAddr
                                        FsPnacAuthSessionSuppAddress,
                                        INT4
                                        *pi4RetValFsPnacAuthSessionAuthenticMethod)
#else
INT1
nmhGetFsPnacAuthSessionAuthenticMethod (FsPnacAuthSessionSuppAddress,
                                        pi4RetValFsPnacAuthSessionAuthenticMethod)
     tMacAddr            FsPnacAuthSessionSuppAddress;
     INT4               *pi4RetValFsPnacAuthSessionAuthenticMethod;
#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;

    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {

        return SNMP_FAILURE;

    }
    else
    {

        *pi4RetValFsPnacAuthSessionAuthenticMethod =
            gPnacSystemInfo.u1AuthenticMethod;
        return SNMP_SUCCESS;

    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacAuthSessionTime
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object 
                retValFsPnacAuthSessionTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacAuthSessionTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacAuthSessionTime (tMacAddr FsPnacAuthSessionSuppAddress,
                             UINT4 *pu4RetValFsPnacAuthSessionTime)
#else
INT1
nmhGetFsPnacAuthSessionTime (FsPnacAuthSessionSuppAddress,
                             pu4RetValFsPnacAuthSessionTime)
     tMacAddr            FsPnacAuthSessionSuppAddress;
     UINT4              *pu4RetValFsPnacAuthSessionTime;
#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;
    tPnacAuthSessStats *pSessStatsEntry = NULL;
    tPnacSysTime        sysTime = PNAC_INIT_VAL;

    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {

        return SNMP_FAILURE;

    }
    else
    {
        pSessStatsEntry = &(pSessNode->authSessStats);

        if (pSessNode->authFsmInfo.u2AuthControlPortStatus
            == PNAC_PORTSTATUS_AUTHORIZED)
        {
            /* get current System time */
            PNAC_GET_SYSTEM_TIME (&sysTime);

            /* get the time elapsed since the Session got Authenticated */
            *pu4RetValFsPnacAuthSessionTime =
                (UINT4) ((sysTime -
                          (tPnacSysTime) pSessStatsEntry->u4SessTime) /
                         PNAC_NUM_OF_TIME_UNITS_IN_A_SEC);
            /* Convert time in seconds to TimeTicks */
            *pu4RetValFsPnacAuthSessionTime =
                PNAC_SYS_TO_MGMT (*pu4RetValFsPnacAuthSessionTime);

            return SNMP_SUCCESS;
        }
        else
        {
            if (pSessNode->u1IsAuthorizedOnce != PNAC_TRUE)
            {
                /* Say is the node is authorized atleast once */
                *pu4RetValFsPnacAuthSessionTime = 0;
                return SNMP_SUCCESS;
            }

            *pu4RetValFsPnacAuthSessionTime =
                (UINT4) (pSessStatsEntry->u4SessTime /
                         PNAC_NUM_OF_TIME_UNITS_IN_A_SEC);
            /* Convert time in seconds to TimeTicks */
            *pu4RetValFsPnacAuthSessionTime =
                PNAC_SYS_TO_MGMT (*pu4RetValFsPnacAuthSessionTime);

            return SNMP_SUCCESS;
        }
    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacAuthSessionTerminateCause
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object 
                retValFsPnacAuthSessionTerminateCause
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacAuthSessionTerminateCause ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacAuthSessionTerminateCause (tMacAddr FsPnacAuthSessionSuppAddress,
                                       INT4
                                       *pi4RetValFsPnacAuthSessionTerminateCause)
#else
INT1
nmhGetFsPnacAuthSessionTerminateCause (FsPnacAuthSessionSuppAddress,
                                       pi4RetValFsPnacAuthSessionTerminateCause)
     tMacAddr            FsPnacAuthSessionSuppAddress;
     INT4               *pi4RetValFsPnacAuthSessionTerminateCause;
#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;
    tPnacAuthSessStats *pSessStatsEntry = NULL;

    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {

        return SNMP_FAILURE;

    }
    else
    {

        pSessStatsEntry = &(pSessNode->authSessStats);
        *pi4RetValFsPnacAuthSessionTerminateCause =
            pSessStatsEntry->u4SessTerminateCause;
        return SNMP_SUCCESS;

    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacAuthSessionUserName
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object 
                retValFsPnacAuthSessionUserName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacAuthSessionUserName ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacAuthSessionUserName (tMacAddr FsPnacAuthSessionSuppAddress,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsPnacAuthSessionUserName)
#else
INT1
nmhGetFsPnacAuthSessionUserName (FsPnacAuthSessionSuppAddress,
                                 pRetValFsPnacAuthSessionUserName)
     tMacAddr            FsPnacAuthSessionSuppAddress;
     tSNMP_OCTET_STRING_TYPE *pRetValFsPnacAuthSessionUserName;
#endif
{
    tPnacAuthSessionNode *pSessNode = NULL;
    tPnacAuthSessStats *pSessStatsEntry = NULL;

    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {

        return SNMP_FAILURE;

    }
    else
    {

        pSessStatsEntry = &(pSessNode->authSessStats);
        pRetValFsPnacAuthSessionUserName->i4_Length =
            pSessStatsEntry->u2SessUserNameLen;
        PNAC_MEMCPY (pRetValFsPnacAuthSessionUserName->pu1_OctetList,
                     pSessStatsEntry->au1SessUserName,
                     pSessStatsEntry->u2SessUserNameLen);
        return SNMP_SUCCESS;

    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* LOW LEVEL Routines for Table : FsPnacASUserConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPnacASUserConfigTable
 Input       :  The Indices
                FsPnacASUserConfigUserName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsPnacASUserConfigTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsPnacASUserConfigTable (tSNMP_OCTET_STRING_TYPE *
                                                 pFsPnacASUserConfigUserName)
#else
INT1
nmhValidateIndexInstanceFsPnacASUserConfigTable (pFsPnacASUserConfigUserName)
     tSNMP_OCTET_STRING_TYPE *pFsPnacASUserConfigUserName;
#endif
{
    if (PNAC_IS_SHUTDOWN ())
    {

        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if ((pFsPnacASUserConfigUserName->i4_Length < 1) ||
        (pFsPnacASUserConfigUserName->i4_Length > PNAC_MAX_LEN_USERNAME))
    {

        PNAC_TRC_ARG1 (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                       " SNMPPROP: ASUserConfig table index must be only a valid UserName of size range 1-%d !!\n",
                       PNAC_MAX_LEN_USERNAME);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPnacASUserConfigTable
 Input       :  The Indices
                FsPnacASUserConfigUserName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstFsPnacASUserConfigTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsPnacASUserConfigTable (tSNMP_OCTET_STRING_TYPE *
                                         pFsPnacASUserConfigUserName)
#else
INT1
nmhGetFirstIndexFsPnacASUserConfigTable (pFsPnacASUserConfigUserName)
     tSNMP_OCTET_STRING_TYPE *pFsPnacASUserConfigUserName;
#endif
{
    tSNMP_OCTET_STRING_TYPE TmpFsPnacASUserConfigUserName;
    UINT1               pu1Name[] = { 0x00 };

    TmpFsPnacASUserConfigUserName.i4_Length = 1;
    TmpFsPnacASUserConfigUserName.pu1_OctetList = pu1Name;

    return (nmhGetNextIndexFsPnacASUserConfigTable
            (&TmpFsPnacASUserConfigUserName, pFsPnacASUserConfigUserName));

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPnacASUserConfigTable
 Input       :  The Indices
                FsPnacASUserConfigUserName
                nextFsPnacASUserConfigUserName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextFsPnacASUserConfigTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexFsPnacASUserConfigTable (tSNMP_OCTET_STRING_TYPE *
                                        pFsPnacASUserConfigUserName,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextFsPnacASUserConfigUserName)
#else
INT1
nmhGetNextIndexFsPnacASUserConfigTable (pFsPnacASUserConfigUserName,
                                        pNextFsPnacASUserConfigUserName)
     tSNMP_OCTET_STRING_TYPE *pFsPnacASUserConfigUserName;
     tSNMP_OCTET_STRING_TYPE *pNextFsPnacASUserConfigUserName;
#endif
{
    tPnacServUserInfo  *pUserNode = NULL;
    tPnacSLLNode       *pSLLNode = NULL;
    UINT1               pu1Name[PNAC_MAX_LEN_USERNAME];
    UINT1               u1EntryFound = PNAC_FALSE;
    tSNMP_OCTET_STRING_TYPE TmpFsPnacASUserConfigUserName;

    if (PNAC_IS_SHUTDOWN ())
    {

        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if ((pFsPnacASUserConfigUserName->i4_Length <= 0) ||
        (pFsPnacASUserConfigUserName->i4_Length > PNAC_MAX_LEN_USERNAME))
    {
        return SNMP_FAILURE;
    }

    PNAC_MEMSET (&pu1Name, 0xff, PNAC_MAX_LEN_USERNAME);
    TmpFsPnacASUserConfigUserName.pu1_OctetList = pu1Name;
    TmpFsPnacASUserConfigUserName.i4_Length = PNAC_MAX_LEN_USERNAME;

    pSLLNode = PNAC_SLL_FIRST (&(PNAC_SERV_USER_LIST));
    pUserNode = (tPnacServUserInfo *) pSLLNode;

    while (pSLLNode != NULL)
    {
        if ((pUserNode->u2NameLen < TmpFsPnacASUserConfigUserName.i4_Length)
            && (pUserNode->u2NameLen > pFsPnacASUserConfigUserName->i4_Length))
        {
            TmpFsPnacASUserConfigUserName.i4_Length = pUserNode->u2NameLen;
            TmpFsPnacASUserConfigUserName.pu1_OctetList = pUserNode->au1Name;
            u1EntryFound = PNAC_TRUE;
        }
        else if (pUserNode->u2NameLen ==
                 TmpFsPnacASUserConfigUserName.i4_Length)
        {
            if (pUserNode->u2NameLen > pFsPnacASUserConfigUserName->i4_Length)
            {

                if ((PNAC_MEMCMP (pUserNode->au1Name,
                                  TmpFsPnacASUserConfigUserName.pu1_OctetList,
                                  pUserNode->u2NameLen) < 0))
                {
                    TmpFsPnacASUserConfigUserName.i4_Length =
                        pUserNode->u2NameLen;
                    TmpFsPnacASUserConfigUserName.pu1_OctetList =
                        pUserNode->au1Name;
                    u1EntryFound = PNAC_TRUE;
                }
            }
            else
            {
                if ((PNAC_MEMCMP (pUserNode->au1Name,
                                  pFsPnacASUserConfigUserName->pu1_OctetList,
                                  pUserNode->u2NameLen) > 0)
                    &&
                    (PNAC_MEMCMP (pUserNode->au1Name,
                                  TmpFsPnacASUserConfigUserName.pu1_OctetList,
                                  pUserNode->u2NameLen) < 0))
                {
                    TmpFsPnacASUserConfigUserName.i4_Length =
                        pUserNode->u2NameLen;
                    TmpFsPnacASUserConfigUserName.pu1_OctetList =
                        pUserNode->au1Name;
                    u1EntryFound = PNAC_TRUE;
                }
            }
        }
        else if (pUserNode->u2NameLen == pFsPnacASUserConfigUserName->i4_Length)
        {
            if (pUserNode->u2NameLen == TmpFsPnacASUserConfigUserName.i4_Length)
            {
                if ((PNAC_MEMCMP (pUserNode->au1Name,
                                  pFsPnacASUserConfigUserName->pu1_OctetList,
                                  pUserNode->u2NameLen) > 0)
                    &&
                    (PNAC_MEMCMP (pUserNode->au1Name,
                                  TmpFsPnacASUserConfigUserName.pu1_OctetList,
                                  pUserNode->u2NameLen) < 0))
                {
                    TmpFsPnacASUserConfigUserName.i4_Length =
                        pUserNode->u2NameLen;
                    TmpFsPnacASUserConfigUserName.pu1_OctetList =
                        pUserNode->au1Name;
                    u1EntryFound = PNAC_TRUE;
                }
            }
            else if ((PNAC_MEMCMP (pUserNode->au1Name,
                                   pFsPnacASUserConfigUserName->pu1_OctetList,
                                   pUserNode->u2NameLen) > 0))
            {

                TmpFsPnacASUserConfigUserName.i4_Length = pUserNode->u2NameLen;
                TmpFsPnacASUserConfigUserName.pu1_OctetList =
                    pUserNode->au1Name;
                u1EntryFound = PNAC_TRUE;
            }
        }
        pSLLNode = PNAC_SLL_NEXT (&PNAC_SERV_USER_LIST, pSLLNode);
        pUserNode = (tPnacServUserInfo *) pSLLNode;
    }

    if (u1EntryFound == PNAC_TRUE)
    {
        pNextFsPnacASUserConfigUserName->i4_Length =
            TmpFsPnacASUserConfigUserName.i4_Length;
        PNAC_MEMCPY (pNextFsPnacASUserConfigUserName->pu1_OctetList,
                     TmpFsPnacASUserConfigUserName.pu1_OctetList,
                     TmpFsPnacASUserConfigUserName.i4_Length);
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPnacASUserConfigPassword
 Input       :  The Indices
                FsPnacASUserConfigUserName

                The Object 
                retValFsPnacASUserConfigPassword
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacASUserConfigPassword ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacASUserConfigPassword (tSNMP_OCTET_STRING_TYPE *
                                  pFsPnacASUserConfigUserName,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsPnacASUserConfigPassword)
#else
INT1
nmhGetFsPnacASUserConfigPassword (pFsPnacASUserConfigUserName,
                                  pRetValFsPnacASUserConfigPassword)
     tSNMP_OCTET_STRING_TYPE *pFsPnacASUserConfigUserName;
     tSNMP_OCTET_STRING_TYPE *pRetValFsPnacASUserConfigPassword;
#endif
{
    tPnacServUserInfo  *pUserNode = NULL;

    if (PnacSnmpLowServGetNodeByName (pFsPnacASUserConfigUserName,
                                      &pUserNode) != PNAC_SUCCESS)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: User node could not be obtained! \n");
        return SNMP_FAILURE;
    }

    if (pUserNode == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: User node could not be obtained! \n");
        return SNMP_FAILURE;
    }

    /*
     * The User Password when read should return an Octet String of length 0.
     */
    pRetValFsPnacASUserConfigPassword->i4_Length = 0;

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacASUserConfigAuthProtocol
 Input       :  The Indices
                FsPnacASUserConfigUserName

                The Object 
                retValFsPnacASUserConfigAuthProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacASUserConfigAuthProtocol ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacASUserConfigAuthProtocol (tSNMP_OCTET_STRING_TYPE *
                                      pFsPnacASUserConfigUserName,
                                      UINT4
                                      *pu4RetValFsPnacASUserConfigAuthProtocol)
#else
INT1
nmhGetFsPnacASUserConfigAuthProtocol (pFsPnacASUserConfigUserName,
                                      pu4RetValFsPnacASUserConfigAuthProtocol)
     tSNMP_OCTET_STRING_TYPE *pFsPnacASUserConfigUserName;
     UINT4              *pu4RetValFsPnacASUserConfigAuthProtocol;
#endif
{
    tPnacServUserInfo  *pUserNode = NULL;

    if (PnacSnmpLowServGetNodeByName (pFsPnacASUserConfigUserName,
                                      &pUserNode) != PNAC_SUCCESS)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: User node could not be obtained! \n");
        return SNMP_FAILURE;
    }

    *pu4RetValFsPnacASUserConfigAuthProtocol = (UINT4) pUserNode->u1AuthType;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacASUserConfigAuthTimeout
 Input       :  The Indices
                FsPnacASUserConfigUserName

                The Object 
                retValFsPnacASUserConfigAuthTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacASUserConfigAuthTimeout ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacASUserConfigAuthTimeout (tSNMP_OCTET_STRING_TYPE *
                                     pFsPnacASUserConfigUserName,
                                     UINT4
                                     *pu4RetValFsPnacASUserConfigAuthTimeout)
#else
INT1
nmhGetFsPnacASUserConfigAuthTimeout (pFsPnacASUserConfigUserName,
                                     pu4RetValFsPnacASUserConfigAuthTimeout)
     tSNMP_OCTET_STRING_TYPE *pFsPnacASUserConfigUserName;
     UINT4              *pu4RetValFsPnacASUserConfigAuthTimeout;
#endif
{
    tPnacServUserInfo  *pUserNode = NULL;

    if (PnacSnmpLowServGetNodeByName (pFsPnacASUserConfigUserName,
                                      &pUserNode) != PNAC_SUCCESS)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: User node could not be obtained! \n");
        return SNMP_FAILURE;
    }

    *pu4RetValFsPnacASUserConfigAuthTimeout = pUserNode->u4AuthTimeout;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacASUserConfigPortList
 Input       :  The Indices
                FsPnacASUserConfigUserName

                The Object 
                retValFsPnacASUserConfigPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacASUserConfigPortList ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacASUserConfigPortList (tSNMP_OCTET_STRING_TYPE *
                                  pFsPnacASUserConfigUserName,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsPnacASUserConfigPortList)
#else
INT1
nmhGetFsPnacASUserConfigPortList (pFsPnacASUserConfigUserName,
                                  pRetValFsPnacASUserConfigPortList)
     tSNMP_OCTET_STRING_TYPE *pFsPnacASUserConfigUserName;
     tSNMP_OCTET_STRING_TYPE *pRetValFsPnacASUserConfigPortList;
#endif
{
    tPnacServUserInfo  *pUserNode = NULL;

    if (PnacSnmpLowServGetNodeByName (pFsPnacASUserConfigUserName,
                                      &pUserNode) != PNAC_SUCCESS)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: User node could not be obtained! \n");
        return SNMP_FAILURE;
    }
    pRetValFsPnacASUserConfigPortList->i4_Length = PNAC_PORT_LIST_SIZE;
    PNAC_MEMCPY (pRetValFsPnacASUserConfigPortList->pu1_OctetList,
                 &(pUserNode->au1PortList), PNAC_PORT_LIST_SIZE);
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacASUserConfigPermission
 Input       :  The Indices
                FsPnacASUserConfigUserName

                The Object 
                retValFsPnacASUserConfigPermission
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacASUserConfigPermission ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacASUserConfigPermission (tSNMP_OCTET_STRING_TYPE *
                                    pFsPnacASUserConfigUserName,
                                    INT4 *pi4RetValFsPnacASUserConfigPermission)
#else
INT1
nmhGetFsPnacASUserConfigPermission (pFsPnacASUserConfigUserName,
                                    pi4RetValFsPnacASUserConfigPermission)
     tSNMP_OCTET_STRING_TYPE *pFsPnacASUserConfigUserName;
     INT4               *pi4RetValFsPnacASUserConfigPermission;
#endif
{
    tPnacServUserInfo  *pUserNode = NULL;

    if (PnacSnmpLowServGetNodeByName (pFsPnacASUserConfigUserName,
                                      &pUserNode) != PNAC_SUCCESS)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: User node could not be obtained! \n");
        return SNMP_FAILURE;
    }

    if (pUserNode->bDenyUser == PNAC_FALSE)
    {

        *pi4RetValFsPnacASUserConfigPermission = 1;
    }
    else if (pUserNode->bDenyUser == PNAC_TRUE)
    {

        *pi4RetValFsPnacASUserConfigPermission = 2;
    }
    else
    {

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsPnacASUserConfigRowStatus
 Input       :  The Indices
                FsPnacASUserConfigUserName

                The Object 
                retValFsPnacASUserConfigRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsPnacASUserConfigRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsPnacASUserConfigRowStatus (tSNMP_OCTET_STRING_TYPE *
                                   pFsPnacASUserConfigUserName,
                                   INT4 *pi4RetValFsPnacASUserConfigRowStatus)
#else
INT1
nmhGetFsPnacASUserConfigRowStatus (pFsPnacASUserConfigUserName,
                                   pi4RetValFsPnacASUserConfigRowStatus)
     tSNMP_OCTET_STRING_TYPE *pFsPnacASUserConfigUserName;
     INT4               *pi4RetValFsPnacASUserConfigRowStatus;
#endif
{
    tPnacServUserInfo  *pUserNode = NULL;

    if (PnacSnmpLowServGetNodeByName (pFsPnacASUserConfigUserName,
                                      &pUserNode) != PNAC_SUCCESS)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: User node could not be obtained! \n");
        return SNMP_FAILURE;
    }

    *pi4RetValFsPnacASUserConfigRowStatus = (INT4) pUserNode->u1RowStatus;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPnacASUserConfigPassword
 Input       :  The Indices
                FsPnacASUserConfigUserName

                The Object 
                setValFsPnacASUserConfigPassword
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsPnacASUserConfigPassword ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsPnacASUserConfigPassword (tSNMP_OCTET_STRING_TYPE *
                                  pFsPnacASUserConfigUserName,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValFsPnacASUserConfigPassword)
#else
INT1
nmhSetFsPnacASUserConfigPassword (pFsPnacASUserConfigUserName,
                                  pSetValFsPnacASUserConfigPassword)
     tSNMP_OCTET_STRING_TYPE *pFsPnacASUserConfigUserName;
     tSNMP_OCTET_STRING_TYPE *pSetValFsPnacASUserConfigPassword;

#endif
{
    tPnacServUserInfo  *pUserNode = NULL;
    UINT4               u4PassLength = 0;

    if (PnacSnmpLowServGetNodeByName (pFsPnacASUserConfigUserName, &pUserNode)
        != PNAC_SUCCESS)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: User node could not be obtained! \n");
        return SNMP_FAILURE;
    }
    u4PassLength = (pSetValFsPnacASUserConfigPassword->i4_Length);

    PNAC_MEMCPY (pUserNode->au1Passwd,
                 pSetValFsPnacASUserConfigPassword->pu1_OctetList,
                 u4PassLength);
    pUserNode->au1Passwd[u4PassLength] = '\0';

    pUserNode->u2PasswdLen = (UINT2) u4PassLength;

    FsUtlEncryptPasswd ((CHR1 *) pUserNode->au1Passwd);
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsPnacASUserConfigAuthTimeout
 Input       :  The Indices
                FsPnacASUserConfigUserName

                The Object 
                setValFsPnacASUserConfigAuthTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsPnacASUserConfigAuthTimeout ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsPnacASUserConfigAuthTimeout (tSNMP_OCTET_STRING_TYPE *
                                     pFsPnacASUserConfigUserName,
                                     UINT4
                                     u4SetValFsPnacASUserConfigAuthTimeout)
#else
INT1
nmhSetFsPnacASUserConfigAuthTimeout (pFsPnacASUserConfigUserName,
                                     u4SetValFsPnacASUserConfigAuthTimeout)
     tSNMP_OCTET_STRING_TYPE *pFsPnacASUserConfigUserName;
     UINT4               u4SetValFsPnacASUserConfigAuthTimeout;

#endif
{
    tPnacServUserInfo  *pUserNode = NULL;

    if (PnacSnmpLowServGetNodeByName (pFsPnacASUserConfigUserName, &pUserNode)
        != PNAC_SUCCESS)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: User node could not be obtained! \n");
        return SNMP_FAILURE;
    }

    pUserNode->u4AuthTimeout = u4SetValFsPnacASUserConfigAuthTimeout;

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY,"FsPnacASUserConfigAuthTimeout = %u\n", u4SetValFsPnacASUserConfigAuthTimeout); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsPnacASUserConfigPortList
 Input       :  The Indices
                FsPnacASUserConfigUserName

                The Object 
                setValFsPnacASUserConfigPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsPnacASUserConfigPortList ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsPnacASUserConfigPortList (tSNMP_OCTET_STRING_TYPE *
                                  pFsPnacASUserConfigUserName,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValFsPnacASUserConfigPortList)
#else
INT1
nmhSetFsPnacASUserConfigPortList (pFsPnacASUserConfigUserName,
                                  pSetValFsPnacASUserConfigPortList)
     tSNMP_OCTET_STRING_TYPE *pFsPnacASUserConfigUserName;
     tSNMP_OCTET_STRING_TYPE *pSetValFsPnacASUserConfigPortList;

#endif
{
    tPnacServUserInfo  *pUserNode = NULL;

    if (PnacSnmpLowServGetNodeByName (pFsPnacASUserConfigUserName, &pUserNode)
        != PNAC_SUCCESS)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: User node could not be obtained! \n");
        return SNMP_FAILURE;
    }
    PNAC_MEMCPY (pUserNode->au1PortList,
                 pSetValFsPnacASUserConfigPortList->pu1_OctetList,
                 pSetValFsPnacASUserConfigPortList->i4_Length);
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsPnacASUserConfigPermission
 Input       :  The Indices
                FsPnacASUserConfigUserName

                The Object 
                setValFsPnacASUserConfigPermission
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsPnacASUserConfigPermission ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsPnacASUserConfigPermission (tSNMP_OCTET_STRING_TYPE *
                                    pFsPnacASUserConfigUserName,
                                    INT4 i4SetValFsPnacASUserConfigPermission)
#else
INT1
nmhSetFsPnacASUserConfigPermission (pFsPnacASUserConfigUserName,
                                    i4SetValFsPnacASUserConfigPermission)
     tSNMP_OCTET_STRING_TYPE *pFsPnacASUserConfigUserName;
     INT4                i4SetValFsPnacASUserConfigPermission;

#endif
{
    tPnacServUserInfo  *pUserNode = NULL;

    if (PnacSnmpLowServGetNodeByName (pFsPnacASUserConfigUserName, &pUserNode)
        != PNAC_SUCCESS)
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: User node could not be obtained! \n");
        return SNMP_FAILURE;
    }

    if (i4SetValFsPnacASUserConfigPermission == 1)
    {

        pUserNode->bDenyUser = PNAC_FALSE;
    }
    else if (i4SetValFsPnacASUserConfigPermission == 2)
    {

        pUserNode->bDenyUser = PNAC_TRUE;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "FsPnacASUserConfigPermission = %d\n", i4SetValFsPnacASUserConfigPermission); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsPnacASUserConfigRowStatus
 Input       :  The Indices
                FsPnacASUserConfigUserName

                The Object 
                setValFsPnacASUserConfigRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsPnacASUserConfigRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsPnacASUserConfigRowStatus (tSNMP_OCTET_STRING_TYPE *
                                   pFsPnacASUserConfigUserName,
                                   INT4 i4SetValFsPnacASUserConfigRowStatus)
#else
INT1
nmhSetFsPnacASUserConfigRowStatus (pFsPnacASUserConfigUserName,
                                   i4SetValFsPnacASUserConfigRowStatus)
     tSNMP_OCTET_STRING_TYPE *pFsPnacASUserConfigUserName;
     INT4                i4SetValFsPnacASUserConfigRowStatus;

#endif
{
    tPnacServUserInfo  *pUserRecord = NULL;

    switch (i4SetValFsPnacASUserConfigRowStatus)
    {

        case PNAC_CREATE_AND_GO:
        case PNAC_CREATE_AND_WAIT:
            if (PnacSnmpLowServGetNodeByName (pFsPnacASUserConfigUserName,
                                              &pUserRecord) == PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " SNMPPROP: User Record Already present Aborting operation ...\n");
                return SNMP_FAILURE;
            }
            if (PnacSnmpLowServCreateUserRecord (pFsPnacASUserConfigUserName,
                                                 &pUserRecord) != PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " SNMPPROP: User Record creation Failed- Aborting operation ...\n");
                return SNMP_FAILURE;
            }
            pUserRecord->u1RowStatus =
                (UINT1) i4SetValFsPnacASUserConfigRowStatus;
            break;

        case PNAC_ACTIVE:
            if (PnacSnmpLowServGetNodeByName (pFsPnacASUserConfigUserName,
                                              &pUserRecord) != PNAC_SUCCESS)
            {

                PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                          " SNMPPROP: User node could not be obtained! \n");
                return SNMP_FAILURE;
            }
            if (pUserRecord->u2PasswdLen == PNAC_INIT_VAL)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                          " SNMPPROP: Password is not Entered to Make the Entry Active! \n");
                return SNMP_FAILURE;
            }
            pUserRecord->u1RowStatus
                = (UINT1) i4SetValFsPnacASUserConfigRowStatus;
            return SNMP_SUCCESS;

            break;
        case PNAC_NOT_READY:
        case PNAC_NOT_IN_SERVICE:
            if (PnacSnmpLowServGetNodeByName (pFsPnacASUserConfigUserName,
                                              &pUserRecord) != PNAC_SUCCESS)
            {

                PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                          " SNMPPROP: User node could not be obtained! \n");
                return SNMP_FAILURE;
            }
            pUserRecord->u1RowStatus
                = (UINT1) i4SetValFsPnacASUserConfigRowStatus;
            break;
        case PNAC_DESTROY:
            if (PnacSnmpLowServGetNodeByName (pFsPnacASUserConfigUserName,
                                              &pUserRecord) != PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " SNMPPROP: User Record not present Aborting operation ...\n");
                return SNMP_FAILURE;
            }
            PNAC_SLL_DELETE (&(PNAC_SERV_USER_LIST), &pUserRecord->nextNode);
            PNAC_DEBUG (PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC,
                                       " SLL User Node deleted : %x",
                                       pUserRecord););
            if (PNAC_RELEASE_SERVUSER_MEMBLK (pUserRecord) != PNAC_MEM_SUCCESS)
            {
                PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                          " AS: Releasing User Node failed\n");
                return SNMP_FAILURE;
            }
            PNAC_TRC (PNAC_CONTROL_PATH_TRC, " User Record destroyed...");
            break;
        default:
            PNAC_TRC (PNAC_CONTROL_PATH_TRC,
                      " Trying to set unknown Row Status Value ...");
            return SNMP_FAILURE;
            break;
    }                            /* switch ends */

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "FsPnacASUserConfigRowStatus = %d\n", i4SetValFsPnacASUserConfigRowStatus); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPnacASUserConfigPassword
 Input       :  The Indices
                FsPnacASUserConfigUserName

                The Object 
                testValFsPnacASUserConfigPassword
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsPnacASUserConfigPassword ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsPnacASUserConfigPassword (UINT4 *pu4ErrorCode,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsPnacASUserConfigUserName,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValFsPnacASUserConfigPassword)
#else
INT1
nmhTestv2FsPnacASUserConfigPassword (pu4ErrorCode, pFsPnacASUserConfigUserName,
                                     pTestValFsPnacASUserConfigPassword)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsPnacASUserConfigUserName;
     tSNMP_OCTET_STRING_TYPE *pTestValFsPnacASUserConfigPassword;
#endif
{
    tPnacServUserInfo  *pUserNode = NULL;

    if (PNAC_IS_SHUTDOWN ())
    {

        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowServGetNodeByName (pFsPnacASUserConfigUserName, &pUserNode)
        != PNAC_SUCCESS)
    {

        *pu4ErrorCode = (UINT4) SNMP_ERR_NO_CREATION;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: User node could not be obtained! \n");
        return SNMP_FAILURE;
    }
    if (pUserNode->u1RowStatus == PNAC_ACTIVE)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: Row Status is active Cannot Set Value !!!\n");
        return SNMP_FAILURE;
    }

    if ((pTestValFsPnacASUserConfigPassword->i4_Length < 1) ||
        (pTestValFsPnacASUserConfigPassword->i4_Length > PNAC_MAX_LEN_PASSWD))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        PNAC_TRC_ARG1 (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                       " SNMPPROP: Set only valid Passwords of size range 1-%d !!\n",
                       PNAC_MAX_LEN_PASSWD);
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValFsPnacASUserConfigPassword->pu1_OctetList,
                              pTestValFsPnacASUserConfigPassword->i4_Length)
        == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsPnacASUserConfigAuthTimeout
 Input       :  The Indices
                FsPnacASUserConfigUserName

                The Object 
                testValFsPnacASUserConfigAuthTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsPnacASUserConfigAuthTimeout ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsPnacASUserConfigAuthTimeout (UINT4 *pu4ErrorCode,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsPnacASUserConfigUserName,
                                        UINT4
                                        u4TestValFsPnacASUserConfigAuthTimeout)
#else
INT1
nmhTestv2FsPnacASUserConfigAuthTimeout (pu4ErrorCode,
                                        pFsPnacASUserConfigUserName,
                                        u4TestValFsPnacASUserConfigAuthTimeout)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsPnacASUserConfigUserName;
     UINT4               u4TestValFsPnacASUserConfigAuthTimeout;
#endif
{
    tPnacServUserInfo  *pUserNode = NULL;

    if (PNAC_IS_SHUTDOWN ())
    {

        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowServGetNodeByName (pFsPnacASUserConfigUserName, &pUserNode)
        != PNAC_SUCCESS)
    {

        *pu4ErrorCode = (UINT4) SNMP_ERR_NO_CREATION;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: User node could not be obtained! \n");
        return SNMP_FAILURE;
    }
    if (pUserNode->u1RowStatus == PNAC_ACTIVE)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_NO_CREATION;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: Row Status is active Cannot Set Value !!!\n");
        return SNMP_FAILURE;
    }

    if ((u4TestValFsPnacASUserConfigAuthTimeout < 1) ||
        (u4TestValFsPnacASUserConfigAuthTimeout > PNAC_MAX_AS_AUTH_TIMEOUT))
    {

        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        PNAC_TRC_ARG1 (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                       " SNMPPROP: Set only Auth-Timeouts of range 1-%d seconds !!\n",
                       PNAC_MAX_AS_AUTH_TIMEOUT);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY,"FsPnacASUserConfigAuthTimeout = %u\n", u4TestValFsPnacASUserConfigAuthTimeout); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsPnacASUserConfigPortList
 Input       :  The Indices
                FsPnacASUserConfigUserName

                The Object 
                testValFsPnacASUserConfigPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsPnacASUserConfigPortList ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsPnacASUserConfigPortList (UINT4 *pu4ErrorCode,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsPnacASUserConfigUserName,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValFsPnacASUserConfigPortList)
#else
INT1
nmhTestv2FsPnacASUserConfigPortList (pu4ErrorCode, pFsPnacASUserConfigUserName,
                                     pTestValFsPnacASUserConfigPortList)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsPnacASUserConfigUserName;
     tSNMP_OCTET_STRING_TYPE *pTestValFsPnacASUserConfigPortList;
#endif
{
    tPnacServUserInfo  *pUserNode = NULL;

    if (PNAC_IS_SHUTDOWN ())
    {

        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowServGetNodeByName (pFsPnacASUserConfigUserName, &pUserNode)
        != PNAC_SUCCESS)
    {

        *pu4ErrorCode = (UINT4) SNMP_ERR_NO_CREATION;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: User node could not be obtained! \n");
        return SNMP_FAILURE;
    }
    if (pUserNode->u1RowStatus == PNAC_ACTIVE)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_NO_CREATION;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: Row Status is active Cannot Set Value !!!\n");
        return SNMP_FAILURE;
    }

    if ((pTestValFsPnacASUserConfigPortList->i4_Length <= 0) ||
        (pTestValFsPnacASUserConfigPortList->i4_Length > PNAC_PORT_LIST_SIZE))
    {

        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (PnacIsPortListValid (pTestValFsPnacASUserConfigPortList->pu1_OctetList,
                             pTestValFsPnacASUserConfigPortList->i4_Length) ==
        SNMP_FAILURE)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPPROP: Invalid Port No !!!\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
Function    :  PnacIsPortListValid
Input       :  The PortList
Length of the portList
Output      :  This function takes the list of ports &
Test whether the port is valid ot not.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/

INT1
PnacIsPortListValid (UINT1 *pu1Ports, INT4 i4Len)
{
    UINT4               u4Port = 0;
    UINT4               u2LocalPort = 0;
    UINT2               u2Index = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;
    UINT2               u2IsPortPresent = FALSE;

    if (pu1Ports == NULL)
    {
        return SNMP_SUCCESS;
    }

    for (u2Index = 0; u2Index < i4Len; u2Index++)
    {
        if (pu1Ports[u2Index] != 0)
        {
            u2IsPortPresent = TRUE;
            u1PortFlag = pu1Ports[u2Index];

            for (u2BitIndex = 0;
                 ((u2BitIndex < PNAC_PORTS_PER_BYTE) && (u1PortFlag != 0));
                 u2BitIndex++)
            {
                if ((u1PortFlag & 0x80) != 0)
                {
                    u2LocalPort = (UINT4) ((u2Index * PNAC_PORTS_PER_BYTE) +
                                           u2BitIndex + 1);

                    u4Port =
                        gPnacSystemInfo.aPnacPaePortInfo[u2LocalPort].u2PortNo;

                    if (u4Port == 0)
                    {
                        return SNMP_FAILURE;
                    }

                    if (L2IwfIsPortInPortChannel (u4Port) == L2IWF_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }

                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }                    /* for each bit in the given byte */
        }
    }
    if ((pu1Ports[0] == 0) && (u2IsPortPresent == FALSE))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPnacASUserConfigPermission
 Input       :  The Indices
                FsPnacASUserConfigUserName

                The Object 
                testValFsPnacASUserConfigPermission
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsPnacASUserConfigPermission ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsPnacASUserConfigPermission (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsPnacASUserConfigUserName,
                                       INT4
                                       i4TestValFsPnacASUserConfigPermission)
#else
INT1
nmhTestv2FsPnacASUserConfigPermission (pu4ErrorCode,
                                       pFsPnacASUserConfigUserName,
                                       i4TestValFsPnacASUserConfigPermission)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsPnacASUserConfigUserName;
     INT4                i4TestValFsPnacASUserConfigPermission;
#endif
{
    tPnacServUserInfo  *pUserNode = NULL;

    if ((i4TestValFsPnacASUserConfigPermission != 1) &&
        (i4TestValFsPnacASUserConfigPermission != 2))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                  " SNMPPROP: Set only '1' for 'allow' or '2' for 'deny' as Permissions !!\n");
        return SNMP_FAILURE;
    }

    if (PNAC_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;

        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (PnacSnmpLowServGetNodeByName (pFsPnacASUserConfigUserName, &pUserNode)
        != PNAC_SUCCESS)
    {

        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_NAME;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: User node could not be obtained! \n");
        return SNMP_FAILURE;
    }
    if (pUserNode->u1RowStatus == PNAC_ACTIVE)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: Row Status is active Cannot Set Value !!!\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "FsPnacASUserConfigPermission = %d\n", i4TestValFsPnacASUserConfigPermission); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsPnacASUserConfigRowStatus
 Input       :  The Indices
                FsPnacASUserConfigUserName

                The Object 
                testValFsPnacASUserConfigRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsPnacASUserConfigRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsPnacASUserConfigRowStatus (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsPnacASUserConfigUserName,
                                      INT4 i4TestValFsPnacASUserConfigRowStatus)
#else
INT1
nmhTestv2FsPnacASUserConfigRowStatus (pu4ErrorCode, pFsPnacASUserConfigUserName,
                                      i4TestValFsPnacASUserConfigRowStatus)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsPnacASUserConfigUserName;
     INT4                i4TestValFsPnacASUserConfigRowStatus;
#endif
{
    tPnacServUserInfo  *pUserNode = NULL;

    if (PNAC_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;

        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    switch (i4TestValFsPnacASUserConfigRowStatus)
    {

        case PNAC_ACTIVE:
        case PNAC_NOT_IN_SERVICE:
        case PNAC_NOT_READY:
        case PNAC_DESTROY:
            if (PnacSnmpLowServGetNodeByName (pFsPnacASUserConfigUserName,
                                              &pUserNode) != PNAC_SUCCESS)
            {

                *pu4ErrorCode = (UINT4) SNMP_ERR_NO_CREATION;
                PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                          " SNMPPROP: User node could not be obtained! \n");
                return SNMP_FAILURE;
            }
            break;
        case PNAC_CREATE_AND_GO:
        case PNAC_CREATE_AND_WAIT:

            if (MemGetFreeUnits (PNAC_SERVUSER_MEMPOOL_ID) == 0)
            {
                *pu4ErrorCode = (UINT4) SNMP_ERR_RESOURCE_UNAVAILABLE;
                PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                          " SNMPPROP: Max user Reached!\n");
                return SNMP_FAILURE;
            }

            if (PnacSnmpLowServGetNodeByName (pFsPnacASUserConfigUserName,
                                              &pUserNode) == PNAC_SUCCESS)
            {
                *pu4ErrorCode = (UINT4) SNMP_ERR_NO_CREATION;
                PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                          " SNMPPROP: Entry already existing with this name! \n");
                return SNMP_FAILURE;
            }
            break;

        default:

            *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
            PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                      " SNMPPROP: Improper RowStatus value !!\n");
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "FsPnacASUserConfigRowStatus = %d\n", i4TestValFsPnacASUserConfigRowStatus); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPnacASUserConfigTable
 Input       :  The Indices
                FsPnacASUserConfigUserName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPnacASUserConfigTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Utility Routines for All Tables */

/****************************************************************************
 Function    :  PnacSnmpLowValidateSuppAddress 
 Input       :  FsPnacAuthSessionSuppAddress - Supplicant's MAC address

 Output      :  None
 Returns     :  PNAC_SUCCESS or PNAC_FAILURE
****************************************************************************/

INT4
PnacSnmpLowValidateSuppAddress (tMacAddr AuthSessionSuppAddress)
{
    tMacAddr            testAddr;

    if (PNAC_IS_SHUTDOWN ())
    {

        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return PNAC_FAILURE;
    }

    PNAC_MEMCPY (testAddr, AuthSessionSuppAddress, PNAC_MAC_ADDR_SIZE);

    if ((PNAC_IS_BROADCAST_ADDR ((testAddr))))
    {

        PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: Broadcast address is not supported\n");
        return PNAC_FAILURE;
    }

    return PNAC_SUCCESS;

}

/****************************************************************************
 Function    :  PnacSnmpLowGetFirstValidSuppAddress
 Input       :  None

 Output      :  This Routine Takes the argument FsPnacAuthSessionSuppAddress 
                & stores the first Valid Supplicant's MAC address in the 
                argument.
 Returns     :  PNAC_SUCCESS or PNAC_FAILURE
****************************************************************************/

INT4
PnacSnmpLowGetFirstValidSuppAddress (tMacAddr FsPnacAuthSessionSuppAddress)
{
    tMacAddr            macAddr;

    if (PNAC_IS_SHUTDOWN ())
    {

        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return PNAC_FAILURE;
    }
    if (PnacAuthGetFirstValidSuppAddress (macAddr) == PNAC_SUCCESS)
    {
        PNAC_MEMCPY (FsPnacAuthSessionSuppAddress, macAddr, PNAC_MAC_ADDR_SIZE);
        return PNAC_SUCCESS;
    }

    return PNAC_FAILURE;
}

/****************************************************************************
 Function    :  PnacSnmpLowGetNextValidSuppAddress 
 Input       :  FsPnacAuthSessionSuppAddress - Supplicant's MAC address

 Output      :  This Routine Takes the argument *pFsPnacAuthSessionSuppAddress
                & stores in it, the Valid Supplicant's MAC address that occurs
                next to FsPnacAuthSessionSuppAddress.
 Returns     :  PNAC_SUCCESS or PNAC_FAILURE
****************************************************************************/

INT4
PnacSnmpLowGetNextValidSuppAddress (tMacAddr AuthSessionSuppAddress,
                                    tMacAddr * pFsPnacAuthSessionSuppAddress)
{
    tMacAddr            refAddr;
    tMacAddr            nextAddr;

    if (PNAC_IS_SHUTDOWN ())
    {

        PNAC_TRC (PNAC_MGMT_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPSTD: PNAC should be started before accessing its MIB objects !!\n");
        return PNAC_FAILURE;
    }
    PNAC_MEMCPY (refAddr, AuthSessionSuppAddress, PNAC_MAC_ADDR_SIZE);

    if (PnacAuthGetNextValidSuppAddress (refAddr, nextAddr) == PNAC_SUCCESS)
    {

        PNAC_MEMCPY (pFsPnacAuthSessionSuppAddress, nextAddr,
                     PNAC_MAC_ADDR_SIZE);
        return PNAC_SUCCESS;
    }

    return PNAC_FAILURE;

}

/****************************************************************************
 Function    :  PnacSnmpLowGetAuthSessionNode 
 Input       :  FsPnacAuthSessionSuppAddress - Supplicant's MAC address

 Output      :  This Routine Takes the argument *pSessNode & stores in it, 
                the Authenticator session node corresponding to the inputted
                Supplicant's MAC address.
 Returns     :  PNAC_SUCCESS or PNAC_FAILURE
****************************************************************************/

INT4
PnacSnmpLowGetAuthSessionNode (tMacAddr AuthSessionSuppAddress,
                               tPnacAuthSessionNode ** ppSessNode)
{
    tMacAddr            refAddr;

    PNAC_MEMCPY (refAddr, AuthSessionSuppAddress, PNAC_MAC_ADDR_SIZE);

    if (PnacAuthGetSessionTblEntryByMac (refAddr, ppSessNode) != PNAC_SUCCESS)
    {
        if (PnacAuthGetAuthInProgressTblEntryByMac (refAddr, ppSessNode)
            != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_ALL_FAILURE_TRC, " SNMPPROP: Getting AuthSessNode"
                      " by Supplicant's MAC address failed\n");
            return PNAC_FAILURE;
        }
    }

    return PNAC_SUCCESS;

}

/****************************************************************************
 Function    :  PnacSnmpLowServGetNode
 Input       :  pOctets - ASUserConfig table index as an Octet String 

 Output      :  This Routine Takes the argument *ppUserNode & stores in it, 
                the User node corresponding to the inputted User Name.
 Returns     :  PNAC_SUCCESS or PNAC_FAILURE
****************************************************************************/

INT4
PnacSnmpLowServGetNode (tSNMP_OCTET_STRING_TYPE * pOctets,
                        tPnacServUserInfo ** ppUserNode)
{

    UINT1               au1UserName[PNAC_MAX_LEN_USERNAME + 1];

    PNAC_MEMSET (au1UserName, PNAC_INIT_VAL, (PNAC_MAX_LEN_USERNAME + 1));
    PNAC_MEMCPY (au1UserName, pOctets->pu1_OctetList, pOctets->i4_Length);
    /* Putting the NULL character termination for the string */
    au1UserName[pOctets->i4_Length] = '\0';

    return (PnacServGetUserNode ((UINT1 *) au1UserName, ppUserNode));
}

/*****************************************************************************/
/* Function Name      : PnacSnmpLowServGetNodeByName                         */
/*                                                                           */
/* Description        : This function gets the User record for the           */
/*                      input User name                                      */
/*                                                                           */
/* Input(s)           : pOctets - Pointer to User name string and length     */
/*                                                                           */
/* Output(s)          : ppUserNode - double pointer to User record           */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacSnmpLowServGetNodeByName (tSNMP_OCTET_STRING_TYPE * pOctets,
                              tPnacServUserInfo ** ppUserNode)
{
    tPnacSLLNode       *pSLLNode = NULL;
    tPnacServUserInfo  *pUserNode = NULL;
    UINT1              *pu1Name = NULL;
    UINT2               u2NameLen = 0;

    pu1Name = pOctets->pu1_OctetList;
    u2NameLen = (UINT2) pOctets->i4_Length;

    pSLLNode = PNAC_SLL_FIRST (&PNAC_SERV_USER_LIST);
    while (pSLLNode != NULL)
    {
        pUserNode = (tPnacServUserInfo *) pSLLNode;
        if ((pUserNode->u2NameLen == u2NameLen)
            &&
            (PNAC_MEMCMP (pUserNode->au1Name, pu1Name, u2NameLen)
             == PNAC_NO_VAL))
        {
            *ppUserNode = pUserNode;
            return PNAC_SUCCESS;
        }

        pSLLNode = PNAC_SLL_NEXT (&PNAC_SERV_USER_LIST, pSLLNode);
    }
    return PNAC_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PnacSnmpLowServCreateUserRecord                      */
/*                                                                           */
/* Description        : This function Creates a new user Record              */
/*                                                                           */
/* Input(s)           : pu1Name - User name of record                        */
/*                                                                           */
/* Output(s)          : ppUserNode - double pointer to User record           */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacServInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacServInfo                                        */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS or PNAC_FAILURE                         */
/*****************************************************************************/
INT4
PnacSnmpLowServCreateUserRecord (tSNMP_OCTET_STRING_TYPE *
                                 pFsPnacASUserConfigUserName,
                                 tPnacServUserInfo ** ppUserRecord)
{
    tPnacServUserInfo  *pUserRecord = NULL;
    UINT2               u2NameLen = 0;
    UINT2               u2Len = 0;
    if (PNAC_ALLOCATE_SERVUSER_MEMBLK (pUserRecord) == NULL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " SNMPPROP: Allocate User Info Record Failed \n");
        return PNAC_FAILURE;
    }
    PNAC_MEMSET (pUserRecord, PNAC_INIT_VAL, sizeof (tPnacServUserInfo));

    u2NameLen = (UINT2) pFsPnacASUserConfigUserName->i4_Length;
    u2Len = (u2NameLen < PNAC_MAX_LEN_USERNAME) ? u2NameLen :
        PNAC_MAX_LEN_USERNAME;
    pUserRecord->u2NameLen = u2Len;
    PNAC_MEMCPY (pUserRecord->au1Name,
                 pFsPnacASUserConfigUserName->pu1_OctetList, u2Len);
    pUserRecord->u1AuthType = PNAC_EAP_TYPE_MD5;
    pUserRecord->bDenyUser = PNAC_FALSE;
    PNAC_MEMSET (&(pUserRecord->au1PortList[0]), 0xff, PNAC_PORT_LIST_SIZE);

    pUserRecord->u1RowStatus = (UINT1) PNAC_CREATE_AND_WAIT;
    PNAC_SLL_ADD (&(PNAC_SERV_USER_LIST), &(pUserRecord->nextNode));
    *ppUserRecord = pUserRecord;
    return PNAC_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPnacTrapAuthSessionTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPnacTrapAuthSessionTable
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsPnacTrapAuthSessionTable
    (tMacAddr FsPnacAuthSessionSuppAddress)
{
    if (nmhValidateIndexInstanceFsPnacAuthSessionTable
        (FsPnacAuthSessionSuppAddress) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPnacTrapAuthSessionTable
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsPnacTrapAuthSessionTable
    (tMacAddr * pFsPnacAuthSessionSuppAddress)
{
    if (nmhGetFirstIndexFsPnacAuthSessionTable
        (pFsPnacAuthSessionSuppAddress) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPnacTrapAuthSessionTable
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress
                nextFsPnacAuthSessionSuppAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsPnacTrapAuthSessionTable
    (tMacAddr FsPnacAuthSessionSuppAddress,
     tMacAddr * pNextFsPnacAuthSessionSuppAddress)
{
    if (nmhGetNextIndexFsPnacAuthSessionTable
        (FsPnacAuthSessionSuppAddress, pNextFsPnacAuthSessionSuppAddress)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPnacTrapAuthSessionStatus
 Input       :  The Indices
                FsPnacAuthSessionSuppAddress

                The Object 
                retValFsPnacTrapAuthSessionStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPnacTrapAuthSessionStatus (tMacAddr FsPnacAuthSessionSuppAddress,
                                   INT4 *pi4RetValFsPnacTrapAuthSessionStatus)
{
    tPnacAuthSessionNode *pSessNode = NULL;

    if (PnacSnmpLowGetAuthSessionNode (FsPnacAuthSessionSuppAddress,
                                       &pSessNode) != PNAC_SUCCESS)
    {
        return SNMP_FAILURE;

    }

    *pi4RetValFsPnacTrapAuthSessionStatus = PNAC_MAC_AUTH_SESS_PRESENT;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  PnacNotifyProtocolShutdownStatus
 Input       :  None
 Output      :  None
 Returns     :  None
****************************************************************************/

VOID
PnacNotifyProtocolShutdownStatus (VOID)
{
#ifdef ISS_WANTED
    UINT1               au1ObjectOid[3][SNMP_MAX_OID_LENGTH];

    /* The oid list contains the list of Oids that has been registered 
     * for the protocol + the oid of the SystemControl object. */
    SNMPGetOidString (fspnac, (sizeof (fspnac) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    SNMPGetOidString (stdpna, (sizeof (stdpna) / sizeof (UINT4)),
                      au1ObjectOid[1]);
    SNMPGetOidString (FsPnacSystemControl,
                      (sizeof (FsPnacSystemControl) / sizeof (UINT4)),
                      au1ObjectOid[2]);

    /* Send a notification to MSR to process the pnac shutdown, with 
     * pnac oids and its system control object 
     * As the protocol does not have MI support, invalid context id is sent */
    MSR_NOTIFY_PROTOCOL_SHUT (au1ObjectOid, 3, MSR_INVALID_CNTXT);
#endif
    return;
}
