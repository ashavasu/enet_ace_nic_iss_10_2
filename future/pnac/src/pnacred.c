/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pnacred.c,v 1.48 2014/03/18 11:58:47 siva Exp $
 *
 * Description     : This file contains RED related 
 *                   routine for PNAC module
 *******************************************************************/

#ifndef _PNACRED_C_
#define _PNACRED_C_

#include "pnachdrs.h"
#include "rstp.h"
/*****************************************************************************/
/* Function Name      : PnacRedInitRedSystemInfo                             */
/*                                                                           */
/* Description        : This function initialized gPnacRedSystemInfo.        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedInitRedSystemInfo (VOID)
{
    PNAC_MEMSET (&(gPnacRedSystemInfo), 0, sizeof (tgPnacRedSystemInfo));
    PNAC_BULK_REQ_RECD () = PNAC_FALSE;
    PNAC_NODE_STATUS () = PNAC_IDLE_NODE;
    gPnacRedSystemInfo.u4BulkUpdNextPort = PNAC_MINPORTS;
    PNAC_NUM_STANDBY_NODES () = 0;
}

/*****************************************************************************/
/* Function Name      : PnacRedRcvPktFromRm                                  */
/*                                                                           */
/* Description        : This function constructs a message containing the    */
/*                      given RM event and RM message and post it to the PNAC*/
/*                      queue.                                               */
/*                                                                           */
/* Input(s)           : u1Event - Event given by RM module                   */
/*                      pData   - Msg to be enqueue                          */
/*                      u2DataLen - Msg size                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if msg is enqueued and event sent then PNAC_SUCCESS  */
/*                      Otherwise PNAC_FAILURE                               */
/*****************************************************************************/
INT4
PnacRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{

    tPnacIntfMesg      *pMesg = NULL;

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        PNAC_TRC (PNAC_RED_TRC, "PNAC RED: Rcvd valid message with "
                  " pointer to buffer as NULL. \n");
        /* Message absent and hence no need to process and no need
         * to send anything to LA task. */

        return RM_FAILURE;

    }
    if (PNAC_IS_SHUTDOWN ())
    {
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            PnacRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " PNAC is shutdown\n");
        return RM_FAILURE;
    }

    if ((pMesg = (tPnacIntfMesg *)
         PNAC_ALLOC_MEM_BLOCK (PNAC_INTF_CFGQ_MESG_MEMPOOL_ID)) == NULL)
    {
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, "Rm to Pnac - Allocate mem "
                  "block for Pnac Interface Message failed\n");

        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            PnacRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        return RM_FAILURE;
    }

    pMesg->u4MesgType = PNAC_RM_MSG;

    pMesg->Mesg.RmPnacMsg.u1SubMsgType = u1Event;
    pMesg->Mesg.RmPnacMsg.pData = pData;
    pMesg->Mesg.RmPnacMsg.u2DataLen = u2DataLen;

    if (PNAC_SEND_TO_QUEUE (PNAC_INTF_CFG_QID,
                            (UINT1 *) &pMesg,
                            OSIX_DEF_MSG_LEN) == PNAC_OSIX_SUCCESS)
    {

        if (PNAC_SEND_EVENT (PNAC_INTF_TASK_ID, PNAC_CFGQ_EVENT)
            != PNAC_OSIX_SUCCESS)
        {

            PNAC_TRC (PNAC_ALL_FAILURE_TRC, "INF: RM to PNAC: Send Event"
                      " failed !!! \n");
            /* Freeing pData here is not advisable, as the pdata is already 
             * queued and pnac may process this at any time. */

            return RM_FAILURE;

        }

        return RM_SUCCESS;
    }

    PNAC_TRC (PNAC_ALL_FAILURE_TRC, " Pnac-RmInf: Send To Q failed !!!\n");

    if (u1Event == RM_MESSAGE)
    {
        RM_FREE (pData);
    }
    else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
    {
        PnacRmReleaseMemoryForMsg ((UINT1 *) pData);
    }

    PNAC_RELEASE_MEM_BLOCK (PNAC_INTF_CFGQ_MESG_MEMPOOL_ID, pMesg);

    return RM_FAILURE;

}

/*****************************************************************************/
/* Function Name      : PnacRedRegisterWithRM                                */
/*                                                                           */
/* Description        : Registers PNAC with RM by providing an application   */
/*                      ID for pnac and a call back function to be called    */
/*                      whenever RM needs to send an event to PNAC.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then PNAC_SUCCESS         */
/*                      Otherwise PNAC_FAILURE                               */
/*****************************************************************************/
INT4
PnacRedRegisterWithRM (VOID)
{
    tRmRegParams        RmRegParams;

    RmRegParams.u4EntId = RM_PNAC_APP_ID;
    RmRegParams.pFnRcvPkt = PnacRedRcvPktFromRm;
    if (PnacRmRegisterProtocols (&RmRegParams) == RM_FAILURE)
    {
        PNAC_TRC (INIT_SHUT_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                  "PnacRedRegisterWithRM: Registration with RM FAILED\n");
        return PNAC_FAILURE;
    }

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacRedDeRegisterWithRM                              */
/*                                                                           */
/* Description        : Deregisters PNAC with RM                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if deregistration is success then PNAC_SUCCESS       */
/*                      Otherwise PNAC_FAILURE                               */
/*****************************************************************************/
INT4
PnacRedDeRegisterWithRM (VOID)
{
    if (PnacRmDeRegisterProtocols () == RM_FAILURE)
    {
        PNAC_TRC (INIT_SHUT_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                  "PnacRedRegisterWithRM: Registration with RM FAILED\n");
        return PNAC_FAILURE;
    }
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacRedGetNodeStateFromRm.                           */
/*                                                                           */
/* Description        : This function gets Node status from RM and stores in */
/*                      gPnacSystemInfo.NodeStatus.                          */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSystemInfo.NodeStatus.                          */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedGetNodeStateFromRm (VOID)
{
    UINT4               u4NodeState;
    u4NodeState = PnacRmGetNodeState ();

    switch (u4NodeState)
    {
        case RM_ACTIVE:
            PNAC_NODE_STATUS () = PNAC_ACTIVE_NODE;
            break;
        case RM_STANDBY:
            PNAC_NODE_STATUS () = PNAC_STANDBY_NODE;
            break;
        default:
            PNAC_NODE_STATUS () = PNAC_IDLE_NODE;
            break;
    }
}

/*****************************************************************************/
/* Function Name      : PnacRedConstructMsgHeader                            */
/*                                                                           */
/* Description        : Allocate RM buffer and fill the message header       */
/*                      required for syncup message.                         */
/*                                                                           */
/* Input(s)           : u4Size - Size of the Sync Up message                 */
/*                      UpdateMsgType - Type of the sync Up message          */
/*                                                                           */
/* Output(s)          : ppMsg - Pointer to the allocated RM buffer.          */
/*                      pu4Offset - Update offset value from where the next  */
/*                                  write operation in RM buffer should be   */
/*                                  started.                                 */
/*                      pu4LenFieldOffset - Offset of the length filed       */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedConstructMsgHeader (UINT4 u4Size, tPnacRedUpdateMsgType UpdateMsgType,
                           tRmMsg ** ppMsg, UINT4 *pu4Offset,
                           UINT4 *pu4LenFieldOffset)
{

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((*ppMsg = RM_ALLOC_TX_BUF (u4Size)) == NULL)
    {
        return;
    }
    *pu4Offset = PNAC_RM_OFFSET;    /* Start filling from PNAC_RM_OFFSET. */

    /* Fill the message type. u4Offset will be incremented by 4 after this 
     * put.*/
    PNAC_RM_PUT_4_BYTE (*ppMsg, pu4Offset, (UINT4) UpdateMsgType);

    /* Previous put_4_byte would have updated the u4Offset. Set the len 
     * field offset. */
    *pu4LenFieldOffset = *pu4Offset;

    /* Put the length as zero now. Can be filled later. This is basically to
     * move the offset 2 bytes.*/
    PNAC_RM_PUT_2_BYTE (*ppMsg, pu4Offset, 0);
}

/*****************************************************************************/
/* Function Name      : PnacRedFillPortBasedSyncUpInfo                       */
/*                                                                           */
/* Description        : Fill the authentication session related information  */
/*                      for the given port and given session in the given    */
/*                      RM buffer.                                           */
/*                                                                           */
/* Input(s)           : pMsg - RM buffer                                     */
/*                      pPortInfo - Pointer to the port entry.               */
/*                      pAuthSessNode - Pointer to Authentication session    */
/*                      pu4Offset - Offset in the given RM buffer from where */
/*                                  the information needs to be filled.      */
/*                                                                           */
/* Output(s)          : pu4Offset - Update offset value from where the next  */
/*                                  write operation in RM buffer should be   */
/*                                  started.                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedFillPortBasedSyncUpInfo (tRmMsg * pMsg, tPnacPaePortEntry * pPortInfo,
                                tPnacAuthSessionNode * pAuthSessNode,
                                tPnacSuppSessionNode * pSuppSessNode,
                                UINT4 *pu4Offset)
{

    UINT4               u4FlagOffSet;
    UINT1               u1SessionFlag = 0;
    UINT4               u4PortNo;

    /* Fill the data related to the given port. Fill u4PortNum, u4ReAuthPeriod, 
     * u2AuthStatus, u2AuthState, u2OperCtrlDir. */

    u4FlagOffSet = *pu4Offset;

    /* Leave one byte for filling session flag. */
    (*pu4Offset)++;
    u4PortNo = (UINT4) pPortInfo->u2PortNo;

    PNAC_RM_PUT_4_BYTE (pMsg, pu4Offset, u4PortNo);

    if (pAuthSessNode != NULL)
    {
        /* Sync Up Authentication session information. */
        u1SessionFlag |= PNAC_RED_AUTH_UPDATE_MSG;

        PNAC_RM_PUT_4_BYTE (pMsg, pu4Offset,
                            (pAuthSessNode->authFsmInfo.u4SessReAuthPeriod));
        PNAC_RM_PUT_2_BYTE (pMsg, pu4Offset,
                            pAuthSessNode->authFsmInfo.u2AuthControlPortStatus);
        PNAC_RM_PUT_2_BYTE (pMsg, pu4Offset,
                            pAuthSessNode->authFsmInfo.u2AuthPaeState);
    }

    if (pSuppSessNode != NULL)
    {
        /* Sync Up Supplecant session information. */
        u1SessionFlag |= PNAC_RED_SUPP_UPDATE_MSG;
        PNAC_RM_PUT_2_BYTE (pMsg, pu4Offset,
                            pSuppSessNode->suppFsmInfo.u2ControlPortStatus);
        PNAC_RM_PUT_2_BYTE (pMsg, pu4Offset,
                            pSuppSessNode->suppFsmInfo.u2SuppPaeState);
    }

    PNAC_RM_PUT_1_BYTE (pMsg, &u4FlagOffSet, u1SessionFlag);
}

/*****************************************************************************/
/* Function Name      : PnacRedSendMsgToRm                                   */
/*                                                                           */
/* Description        : This function constructs the RM message from the     */
/*                      given linear buf and sends it to Redundancy Manager. */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to the RM input buffer.               */
/*                      u2BufSize  - Size of the given input buffer.         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if sync msg is sent then PNAC_SUCCESS                */
/*                      Otherwise PNAC_FAILURE                               */
/*****************************************************************************/
INT4
PnacRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2BufSize)
{

    /* Call the API provided by RM to send the data to RM */
    if (PnacRmEnqMsgToRmFromAppl (pMsg, u2BufSize, RM_PNAC_APP_ID,
                                  RM_PNAC_APP_ID) == RM_FAILURE)
    {
        PNAC_TRC (PNAC_TRC_FLAG, "Enq to RM from appl failed\n");
        /* pMsg is freed only in failure case. RM will free the buf
         * in success case. */
        RM_FREE (pMsg);

        return PNAC_FAILURE;
    }

    return (PNAC_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : PnacRedSyncUpDynamicInfo                             */
/*                                                                           */
/* Description        : Construct the dynamic sync up message for the given  */
/*                      and given authentication session and sends it to     */
/*                      standby node.                                        */
/*                                                                           */
/* Input(s)           : pPortInfo - Pointer to the port entry.               */
/*                      pAuthSessNode - Pointer to the authentication        */
/*                                      session node.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedSyncUpDynamicInfo (tPnacPaePortEntry * pPortInfo,
                          tPnacAuthSessionNode * pAuthSessNode,
                          tPnacSuppSessionNode * pSuppSessNode)
{
    tRmMsg             *pMsg = NULL;
    UINT2               u4BufSize;
    UINT4               u4Offset = 0;
    UINT4               u4LenFieldOffset = 0;

    if (PNAC_NODE_STATUS () == PNAC_STANDBY_NODE)
    {
        /* When the node is acting as standby, no sync. Just return. */
        PNAC_TRC (PNAC_RED_TRC, "RED: Dynamic sync up information "
                  "cannot be sent from Standby node. \n");
        return;
    }

    if (PNAC_IS_STANDBY_UP () == PNAC_FALSE)
    {
        /* When there are no standby nodes, just return. */
        PNAC_TRC (PNAC_RED_TRC, "RED: Dynamic sync up information "
                  "cannot be sent if Standby node is down. \n");
        return;
    }

    u4BufSize = PNAC_SYNCUP_PORTBASED_DATA_SIZE;

    /* Allocate RM buffer and fill the message type and return the following:
     * RM buffer pointer, offset to the length and NumPorts fields in the 
     * buffer. */
    PnacRedConstructMsgHeader (u4BufSize, PNAC_PORTBASED_UPDATE_MSG, &pMsg,
                               &u4Offset, &u4LenFieldOffset);

    if (NULL == pMsg)
    {
        /* RM msg allocation failed. Cannot send bulk update. */
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, "RED: RM msg allocation failed. "
                  "Cannot send dynamic update!!!!!!!!\n");
        return;
    }

    PnacRedFillPortBasedSyncUpInfo (pMsg, pPortInfo, pAuthSessNode,
                                    pSuppSessNode, &u4Offset);

    /* Fill the correct len at the offset initially set. */
    PNAC_RM_PUT_2_BYTE (pMsg, &u4LenFieldOffset, (u4Offset - PNAC_RM_OFFSET));

    PNAC_TRC_ARG1 (PNAC_RED_TRC, "RED: PnacRedSyncUpDynamicInfo : "
                   "Sending %d bytes to RM.\n", u4Offset);

    /* Send the constructed message to Standby node. */
    PnacRedSendMsgToRm (pMsg, (UINT2) (u4Offset - PNAC_RM_OFFSET));

    return;
}

/*****************************************************************************/
/* Function Name      : PnacRedSyncUpPortOperUpStatus                        */
/*                                                                           */
/* Description        : This function is used to sync port enable            */
/*                      information to standby node.                         */
/*                                                                           */
/* Input(s)           : u4PortIndex - Port IfIndex.                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.NodeStatus.                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedSyncUpPortOperUpStatus (UINT4 u4PortIndex)
{
    tRmMsg             *pMsg = NULL;
    UINT2               u4BufSize;
    UINT4               u4Offset = 0;
    UINT4               u4LenFieldOffset = 0;

    if (PNAC_NODE_STATUS () == PNAC_STANDBY_NODE)
    {
        /* When the node is acting as standby, no sync. Just return. */
        return;
    }

    if (PNAC_IS_STANDBY_UP () == PNAC_FALSE)
    {
        /* When there are no standby nodes, just return. */
        return;
    }

    u4BufSize = (PNAC_SYNCUP_HDR_LEN + PNAC_PORT_OPER_UP_MSG_SIZE);

    /* Allocate RM buffer and fill the message type and return the following:
     * RM buffer pointer, offset to the length and NumPorts fields in the 
     * buffer. */
    PnacRedConstructMsgHeader (u4BufSize, PNAC_PORT_OPER_UP_MSG, &pMsg,
                               &u4Offset, &u4LenFieldOffset);

    if (NULL == pMsg)
    {
        /* RM msg allocation failed. Cannot send bulk update. */
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, "RED: RM msg allocation failed. Cannot "
                  "send dynamic update!!!!!!!!\n");
        return;
    }

    /* Insert the given port id into the message. */
    PNAC_RM_PUT_4_BYTE (pMsg, &u4Offset, u4PortIndex);

    /* Fill the correct len at the offset initially set. */
    PNAC_RM_PUT_2_BYTE (pMsg, &u4LenFieldOffset, (u4Offset - PNAC_RM_OFFSET));

    PNAC_TRC_ARG1 (PNAC_RED_TRC, "RED: PnacRedSyncUpModOrPortClearInfo : "
                   "Sending %d bytes to RM.\n", u4Offset);

    /* Send the constructed message to Standby node. */
    PnacRedSendMsgToRm (pMsg, u4BufSize);

    return;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      : PnacRedSyncAsynchronousStatus                        */
/*                                                                           */
/* Description        : This function is used to sync Asynchronous Status    */
/*                      information to standby node.                         */
/*                                                                           */
/* Input(s)           : u2Port - Port Number                                 */
/*                      u1PnacPortCtrlDir - Port Control Direction           */
/*                      u1PnacPortAuthStatus - Port Auth Status              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.NodeStatus.                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedSyncAsynchronousStatus (UINT2 u2Port, UINT1 u1PnacPortCtrlDir,
                               UINT1 u1PnacPortAuthStatus)
{
    tRmMsg             *pMsg = NULL;
    UINT2               u4BufSize;
    UINT4               u4Offset = 0;
    UINT4               u4LenFieldOffset = 0;

    if (PNAC_NODE_STATUS () == PNAC_STANDBY_NODE)
    {
        /* When the node is acting as standby, no sync. Just return. */
        PNAC_TRC (PNAC_RED_TRC, "RED: Dynamic sync up information "
                  "cannot be sent from Standby node. \n");

        return;
    }

    if (PNAC_IS_STANDBY_UP () == PNAC_FALSE)
    {
        /* When there are no standby nodes, just return. */
        PNAC_TRC (PNAC_RED_TRC, "RED: Dynamic sync up information "
                  "cannot be sent if Standby node is down. \n");
        return;
    }

    u4BufSize = (PNAC_SYNCUP_HDR_LEN + PNAC_PORT_ID_SIZE);

    /* Allocate RM buffer and fill the message type and return the following:
     * RM buffer pointer, offset to the length and NumPorts fields in the 
     * buffer. */
    PnacRedConstructMsgHeader (u4BufSize, PNAC_PORT_ASYNC_STATUS_MSG, &pMsg,
                               &u4Offset, &u4LenFieldOffset);

    if (NULL == pMsg)
    {
        /* RM msg allocation failed. Cannot send bulk update. */
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, "RED: RM msg allocation failed. Cannot "
                  "send Asynchronous update!!!!!!!!\n");
        return;
    }

    /* Insert the given port into the message. */
    PNAC_RM_PUT_2_BYTE (pMsg, &u4Offset, u2Port);

    /* Insert the given port control direction into the message. */
    PNAC_RM_PUT_1_BYTE (pMsg, &u4Offset, u1PnacPortCtrlDir);

    /* Insert the given port authstatus into the message. */
    PNAC_RM_PUT_1_BYTE (pMsg, &u4Offset, u1PnacPortAuthStatus);

    /* Fill the correct len at the offset initially set. */
    PNAC_RM_PUT_2_BYTE (pMsg, &u4LenFieldOffset, (u4Offset - PNAC_RM_OFFSET));

    PNAC_TRC_ARG1 (PNAC_RED_TRC, "RED: PnacRedSyncUpModOrPortClearInfo : "
                   "Sending %d bytes to RM.\n", u4Offset);

    /* Send the constructed message to Standby node. */
    PnacRedSendMsgToRm (pMsg, u4BufSize);

    return;
}
#endif

/*****************************************************************************/
/* Function Name      : PnacRedSyncUpModOrPortClearInfo                      */
/*                                                                           */
/* Description        : This function is used to sync port disable or module */
/*                      disable information to standby node. If the input    */
/*                      port index is zero, it means module disable message  */
/*                      otherwise it is the port disable message for the     */
/*                      given port.                                          */
/*                                                                           */
/* Input(s)           : u4PortIndex - Port IfIndex.                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.NodeStatus.                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedSyncUpModOrPortClearInfo (UINT4 u4PortIndex)
{
    tRmMsg             *pMsg = NULL;
    UINT2               u4BufSize;
    UINT4               u4Offset = 0;
    UINT4               u4LenFieldOffset = 0;

    if (PNAC_NODE_STATUS () == PNAC_STANDBY_NODE)
    {
        /* When the node is acting as standby, no sync. Just return. */
        return;
    }

    if (PNAC_IS_STANDBY_UP () == PNAC_FALSE)
    {
        /* When there are no standby nodes, just return. */
        return;
    }

    u4BufSize = (PNAC_SYNCUP_HDR_LEN + PNAC_MOD_OR_PORT_CLR_INFO_SIZE);

    /* Allocate RM buffer and fill the message type and return the following:
     * RM buffer pointer, offset to the length and NumPorts fields in the 
     * buffer. */
    PnacRedConstructMsgHeader (u4BufSize, PNAC_MODULE_OR_PORT_CLEAR_MSG, &pMsg,
                               &u4Offset, &u4LenFieldOffset);

    if (NULL == pMsg)
    {
        /* RM msg allocation failed. Cannot send bulk update. */
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, "RED: RM msg allocation failed. Cannot "
                  "send dynamic update!!!!!!!!\n");
        return;
    }

    /* Insert the given port id into the message. */
    PNAC_RM_PUT_4_BYTE (pMsg, &u4Offset, u4PortIndex);

    /* Fill the correct len at the offset initially set. */
    PNAC_RM_PUT_2_BYTE (pMsg, &u4LenFieldOffset, (u4Offset - PNAC_RM_OFFSET));

    PNAC_TRC_ARG1 (PNAC_RED_TRC, "RED: PnacRedSyncUpModOrPortClearInfo : "
                   "Sending %d bytes to RM.\n", u4Offset);

    /* Send the constructed message to Standby node. */
    PnacRedSendMsgToRm (pMsg, u4BufSize);

    return;
}

/*****************************************************************************/
/* Function Name      : PnacRedSendBulkReq                                   */
/*                                                                           */
/* Description        : This function is sends a bulk request to the active  */
/*                      node of the system.                                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : NodeStatus                                           */
/*                      gPnacRedSystemInfo.u1NumPeersUp                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedSendBulkReq (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT2               u4BufSize;
    UINT4               u4Offset = 0;
    UINT4               u4LenFieldOffset = 0;
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_PNAC_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u4BufSize = PNAC_RM_MSG_TYPE_SIZE + PNAC_RM_LEN_SIZE;

    /* Allocate RM buffer and fill the message type and return the following:
     * RM buffer pointer, offset to the length and NumPorts fields in the 
     * buffer. */
    PnacRedConstructMsgHeader (u4BufSize, PNAC_BULK_REQ_MSG, &pMsg,
                               &u4Offset, &u4LenFieldOffset);

    if (NULL == pMsg)
    {
        /* RM msg allocation failed. Cannot send bulk update. */
        PNAC_TRC (PNAC_ALL_FAILURE_TRC, "RED: RM msg allocation failed. "
                  "Cannot send dynamic update!!!!!!!!\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        PnacRmHandleProtocolEvent (&ProtoEvt);
        return;
    }

    /* Fill the correct len at the offset initially set. */
    PNAC_RM_PUT_2_BYTE (pMsg, &u4LenFieldOffset, (u4Offset - PNAC_RM_OFFSET));

    PNAC_TRC_ARG1 (PNAC_RED_TRC, "RED: PnacRedSyncUpDynamicInfo : "
                   "Sending %d bytes to RM.\n", u4Offset);

    /* Send the constructed message to Standby node. */
    if (PnacRedSendMsgToRm (pMsg, u4BufSize) != PNAC_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        PnacRmHandleProtocolEvent (&ProtoEvt);
    }

    return;
}

/*****************************************************************************/
/* Function Name      : PnacRedHandlePortBasedBulkReqEvent.                  */
/*                                                                           */
/* Description        : This function gets the dynamic informatin for port   */
/*                      based authentication for all ports that are auth     */
/*                      capable, port-control set to AUTO.                   */
/*                                                                           */
/* Input(s)           : pvPeerId - Pointer to the Standby node Id to which   */
/*                                 the bulk update needs to be sent.         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.aPnacPaePortInfo                     */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedHandlePortBasedBulkReqEvent (VOID *pvPeerId)
{
    UINT4               u4PortNum;
    UINT4               u4StartIfIndex;
    UINT4               u4BulkUpdPortCnt = 0;
    tRmProtoEvt         ProtoEvt;

    UNUSED_PARAM (pvPeerId);

    ProtoEvt.u4AppId = RM_PNAC_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if ((PNAC_MODULE_STATUS == PNAC_DISABLED) || (PNAC_IS_SHUTDOWN ()))
    {
        /* PNAC completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR
         */
        PnacRmSetBulkUpdatesStatus (RM_PNAC_APP_ID);

        PnacRedSendBulkUpdateTailMsg ();

        return;
    }

    PNAC_TRC (PNAC_RED_TRC, "RED: Rcvd Bulk Req from new standby node\n");

    u4BulkUpdPortCnt = PNAC_RED_NO_OF_PORTS_PER_SUB_UPDATE;
    u4StartIfIndex = gPnacRedSystemInfo.u4BulkUpdNextPort;

    for (u4PortNum = gPnacRedSystemInfo.u4BulkUpdNextPort;
         u4PortNum <= PNAC_MAXPORTS && u4BulkUpdPortCnt > 0;
         u4PortNum++, u4BulkUpdPortCnt--)
    {
        /* Update u4BulkUpdNextPort, to resume the next sub bulk update from
         * where the previous sub bulk update left it out.
         */
        gPnacRedSystemInfo.u4BulkUpdNextPort++;
    }

    PnacRedPortOperStatusBulkSyncup (u4StartIfIndex);

    PnacRedPortBasedBulkSyncup (u4StartIfIndex);

    if (gPnacRedSystemInfo.u4BulkUpdNextPort <= PNAC_MAXPORTS)
    {
        /* Send an event to start the next sub bulk update.
         */
        if (PNAC_SEND_EVENT (PNAC_INTF_TASK_ID,
                             PNAC_RED_BULK_UPD_EVENT) != PNAC_OSIX_SUCCESS)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            PnacRmHandleProtocolEvent (&ProtoEvt);
            PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                      "L2RED Bulk Update: Send Event failed !!!\n");
        }
    }
    else
    {
        /* PNAC completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR
         */
        PnacRmSetBulkUpdatesStatus (RM_PNAC_APP_ID);

        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        PnacRedSendBulkUpdateTailMsg ();
    }

    return;
}

/*****************************************************************************/
/* Function Name      : PnacRedHandleSyncUpData.                             */
/*                                                                           */
/* Description        : This function reads the dynamic information present  */
/*                      in pData and takes neccessary action.                */
/*                                                                           */
/* Input(s)           : pData - Pointer to the RM buffer.                    */
/*                      u2DataLen - Size of the buffer given by RM.          */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSystemInfo.NodeStatus.                          */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedHandleSyncUpData (VOID *pData, UINT2 u2DataLen)
{
    UINT4               u4Offset = 0;
    UINT4               u4MsgType = 0;
    UINT2               u2Len = 0;
    tRmProtoEvt         ProtoEvt;
    tRmProtoAck         ProtoAck;
    UINT4               u4SeqNum = 0;

    ProtoEvt.u4AppId = RM_PNAC_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* Read the sequence number from the RM Header */
    RM_PKT_GET_SEQNUM (pData, &u4SeqNum);
    /* Remove the RM Header */
    RM_STRIP_OFF_RM_HDR (pData, u2DataLen);

    ProtoAck.u4AppId = RM_PNAC_APP_ID;
    ProtoAck.u4SeqNumber = u4SeqNum;

    PNAC_TRC_ARG1 (PNAC_RED_TRC, "RED: Rcvd %d bytes from RM.\n", u2DataLen);

    /* No need to do null pointer check for pMesg as it is done
     * in PnacRedRcvPktFromRm. */
    if (u2DataLen < (PNAC_RM_MSG_TYPE_SIZE + PNAC_RM_LEN_SIZE))
    {
        /* Message size is very less to process. */
        /* Sending ACK to RM */
        RmApiSendProtoAckToRM (&ProtoAck);
        return;
    }

    /* Read message type from pData. */
    PNAC_RM_GET_4_BYTE (pData, &u4Offset, u4MsgType);

    /* Read length from pData. */
    PNAC_RM_GET_2_BYTE (pData, &u4Offset, u2Len);
    if (u2Len != u2DataLen)
    {
        /* Length field present in data and the len given by RM differs. 
         * Something wrong!!!*/
        PNAC_TRC (PNAC_RED_TRC, "RED: Rcvd VALID MESSAGE from RM "
                  "Length given by RM and the length of Pnac mesg does not "
                  "mactch.\n");
        /* Sending ACK to RM */
        RmApiSendProtoAckToRM (&ProtoAck);
        return;
    }

    /* Process the message based on message types. */
    switch (u4MsgType)
    {
        case PNAC_PORTBASED_UPDATE_MSG:
            PnacRedHandlePortBasedUpdtMsg (pData, &u4Offset, u2Len);
            break;

        case PNAC_MODULE_OR_PORT_CLEAR_MSG:
            PnacRedHandleModOrPortClearMsg (pData, &u4Offset, u2Len);
            break;

        case PNAC_PORT_OPER_UP_MSG:
            PnacRedHandlePortOperUpMsg (pData, &u4Offset, u2Len);
            break;

        case PNAC_PORT_ASYNC_STATUS_MSG:
            PnacRedHandleAsynchronousStatus (pData, &u4Offset, u2Len);
            break;

        case PNAC_BULK_REQ_MSG:

            if (PNAC_IS_STANDBY_UP () == PNAC_FALSE)
            {
                /* This is a special case, where bulk request msg from
                 * standby is coming before RM_STANDBY_UP. So no need to
                 * process the bulk request now. Bulk updates will be send
                 * on RM_STANDBY_UP event.
                 */
                PNAC_BULK_REQ_RECD () = PNAC_TRUE;
                break;
            }

            PNAC_BULK_REQ_RECD () = PNAC_FALSE;
            /* On recieving PNAC_BULK_REQ_MSG, we should restart the
             * bulk updation process.
             */
            gPnacRedSystemInfo.u4BulkUpdNextPort = PNAC_MINPORTS;
            PnacRedHandleBulkUpdEvent ();

            break;

        case PNAC_BULK_UPD_TAIL_MSG:
            PNAC_TRC (PNAC_RED_TRC, "Received Bulk Update tail msg. \n");
            /* On receiving PNAC_BULK_UPD_TAIL_MSG, give indication to RM. 
             * RM gives the trigger to higher
             * layers (LA/STP/VLAN) inorder to send the Bulk Req msg. 
             */
            ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
            PnacRmHandleProtocolEvent (&ProtoEvt);

            break;

        case PNAC_MACBASED_UPDATE_MSG:
            /* Delibrate fall through as redundancy is not implemented for i
             * Mac based authentications. */
        default:
            break;
    }

    /* Sending ACK to RM */
    RmApiSendProtoAckToRM (&ProtoAck);

}

/*****************************************************************************/
/* Function Name      : PnacRedPortOperStatusBulkSyncup                      */
/*                                                                           */
/* Description        : This function sync  up the port oper status  on      */
/*                      getting the Bulk Request message.                    */
/*                                                                           */
/* Input(s)           : u4StartIfIndex                                       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedPortOperStatusBulkSyncup (UINT4 u4StartIfIndex)
{
    /* Send Bulk Update messages to the standby. */
    tRmMsg             *pMsg = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = 0;
    UINT4               u4SyncLengthOffset;
    UINT2               u2BufSize = 0;
    UINT4               u4PortIndex = 0;
    UINT4               u4BulkUpdPortCnt;

    ProtoEvt.u4AppId = RM_PNAC_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u4BulkUpdPortCnt = PNAC_RED_NO_OF_PORTS_PER_SUB_UPDATE;

    for (u4PortIndex = u4StartIfIndex;
         u4PortIndex <= PNAC_MAXPORTS && u4BulkUpdPortCnt > 0;
         u4PortIndex++, u4BulkUpdPortCnt--)
    {
        pPortInfo = &(PNAC_PORT_INFO (u4PortIndex));

        if (pPortInfo->u1EntryStatus != PNAC_PORT_OPER_UP)
        {
            /* Bulk update will be done only for ports that are
             * operationally up.*/
            continue;
        }

        if (u2BufSize - u4Offset < PNAC_PORT_OPER_UP_MSG_SIZE)
        {
            /* There is no enough space to fill this port information
             * in to the buffer. Hence send the existing message and
             * construct new message to fill the information of other 
             * ports.*/
            if (pMsg != NULL)
            {
                /* Send the existing message to RM. */
                /* Offset of the Length field for update information is
                 * stored in u4SyncLengthOffset.
                 */
                PNAC_RM_PUT_2_BYTE (pMsg, &u4SyncLengthOffset,
                                    (u4Offset - PNAC_RM_OFFSET));

                /* Note u4Offset contains the number of bytes written in to
                 * the buffer. Hence can be used a buffer size here. */
                if (PnacRedSendMsgToRm (pMsg, (UINT2) u4Offset) == PNAC_FAILURE)
                {
                    ProtoEvt.u4Error = RM_SENDTO_FAIL;
                    PnacRmHandleProtocolEvent (&ProtoEvt);
                }
            }

            /* Reset the offset, size fields so that they can be used in the
             * new buffer. */
            u4Offset = PNAC_RM_OFFSET;

            u2BufSize = PNAC_BULK_SPLIT_MSG_SIZE;

            /* Allocate RM buf and fill the message type, and get the pointer 
             * to RM buffer, offset of Lenght field. */
            PnacRedConstructMsgHeader (u2BufSize, PNAC_PORT_OPER_UP_MSG,
                                       &pMsg, &u4Offset, &u4SyncLengthOffset);

            if (pMsg == NULL)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                          "Bulk Update: Rm alloc failed\n");
                ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                PnacRmHandleProtocolEvent (&ProtoEvt);
                return;
            }
        }

        /* Insert the given port id into the message. */
        PNAC_RM_PUT_4_BYTE (pMsg, &u4Offset, u4PortIndex);
    }

    /* Send the last message. */
    if (pMsg != NULL)
    {
        /* Fill the message length at proper offset. */
        PNAC_RM_PUT_2_BYTE (pMsg, &u4SyncLengthOffset,
                            (u4Offset - PNAC_RM_OFFSET));

        /* Note u4Offset contains the number of bytes written in to
         * the buffer. Hence can be used a buffer size here. */
        if (PnacRedSendMsgToRm (pMsg, (UINT2) u4Offset) == PNAC_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            PnacRmHandleProtocolEvent (&ProtoEvt);
        }
    }
}

/*****************************************************************************/
/* Function Name      : PnacRedPortBasedBulkSyncup                           */
/*                                                                           */
/* Description        : This function sync  up the port based information on */
/*                      getting the Bulk Request message.                    */
/*                                                                           */
/* Input(s)           : u4StartIfIndex                                       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedPortBasedBulkSyncup (UINT4 u4StartIfIndex)
{
    /* Send Bulk Update messages to the standby. */
    tRmMsg             *pMsg = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = 0;
    UINT4               u4SyncLengthOffset;
    UINT2               u2BufSize = 0;
    UINT4               u4PortIndex = 0;
    UINT4               u4BulkUpdPortCnt;

    ProtoEvt.u4AppId = RM_PNAC_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u4BulkUpdPortCnt = PNAC_RED_NO_OF_PORTS_PER_SUB_UPDATE;

    for (u4PortIndex = u4StartIfIndex;
         u4PortIndex <= PNAC_MAXPORTS && u4BulkUpdPortCnt > 0;
         u4PortIndex++, u4BulkUpdPortCnt--)
    {
        pPortInfo = &(PNAC_PORT_INFO (u4PortIndex));

        if (pPortInfo->u1EntryStatus != PNAC_PORT_OPER_UP)
        {
            /* Bulk update will be done only for ports that are
             * operationally up.*/
            continue;
        }

        if ((!PNAC_IS_PORT_MODE_PORTBASED (pPortInfo)) ||
            (PNAC_IS_PORT_NONE_CAPABLE (pPortInfo)))
        {
            /* Bulk update will be done only for ports that supports
             * port based authentications.*/
            continue;
        }

        /* For ports whose port control is set to Auto, asm state info
         * needs to be synced up. */
        if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl
            != PNAC_PORTCNTRL_AUTO)
        {
            /* For ports that are either ForceAuthorized or 
             * ForceUnAuthorized, the sem states are constructed based on 
             * config and port oper status and hence nothing to do here.*/
            continue;
        }

        if (u2BufSize - u4Offset < PNAC_PORTINFO_SIZE)
        {
            /* There is no enough space to fill this port information
             * in to the buffer. Hence send the existing message and
             * construct new message to fill the information of other 
             * ports.*/
            if (pMsg != NULL)
            {
                /* Send the existing message to RM. */
                /* Offset of the Length field for update information is
                 * stored in u4SyncLengthOffset.
                 */
                PNAC_RM_PUT_2_BYTE (pMsg, &u4SyncLengthOffset,
                                    (u4Offset - PNAC_RM_OFFSET));

                /* Note u4Offset contains the number of bytes written in to
                 * the buffer. Hence can be used a buffer size here. */
                if (PnacRedSendMsgToRm (pMsg, (UINT2) u4Offset) == PNAC_FAILURE)
                {
                    ProtoEvt.u4Error = RM_SENDTO_FAIL;
                    PnacRmHandleProtocolEvent (&ProtoEvt);
                }
            }

            /* Reset the offset, size fields so that they can be used in the
             * new buffer. */
            u4Offset = PNAC_RM_OFFSET;

            u2BufSize = PNAC_BULK_SPLIT_MSG_SIZE;

            /* Allocate RM buf and fill the message type, and get the pointer 
             * to RM buffer, offset of Lenght field. */
            PnacRedConstructMsgHeader (u2BufSize, PNAC_PORTBASED_UPDATE_MSG,
                                       &pMsg, &u4Offset, &u4SyncLengthOffset);

            if (pMsg == NULL)
            {
                PNAC_TRC (PNAC_ALL_FAILURE_TRC,
                          "Bulk Update: Rm alloc failed\n");
                ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                PnacRmHandleProtocolEvent (&ProtoEvt);
                return;
            }

        }

        /* 
         * Copy the Port's ID, AuthStatus, AuthState,
         * AuthSessionReAuthPeriod, SuppStatus, SuppState into the RM buf.
         */
        PnacRedFillPortBasedSyncUpInfo (pMsg, pPortInfo,
                                        pPortInfo->pPortAuthSessionNode,
                                        pPortInfo->pPortSuppSessionNode,
                                        &u4Offset);
    }

    /* Send the last message. */
    if (pMsg != NULL)
    {
        /* Fill the message length at proper offset. */
        PNAC_RM_PUT_2_BYTE (pMsg, &u4SyncLengthOffset,
                            (u4Offset - PNAC_RM_OFFSET));

        /* Note u4Offset contains the number of bytes written in to
         * the buffer. Hence can be used a buffer size here. */
        if (PnacRedSendMsgToRm (pMsg, (UINT2) u4Offset) == PNAC_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            PnacRmHandleProtocolEvent (&ProtoEvt);
        }
    }
}

/*****************************************************************************/
/* Function Name      : PnacRedHandlePortBasedUpdtMsg.                       */
/*                                                                           */
/* Description        : Get the sync up information for the given port and   */
/*                      apply it immediately.                                */
/*                                                                           */
/* Input(s)           : pData - Pointer to the RM buffer.                    */
/*                      pu4Offset - Offset from where the read should start. */
/*                      u2Len - Length of the information (includes msgtype  */
/*                              msglen).                                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacRedSystemInfo.aPnacSyncUpData.                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacRedSystemInfo.aPnacSyncUpData.                  */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedHandlePortBasedUpdtMsg (tRmMsg * pData, UINT4 *pu4Offset, UINT2 u2Len)
{
    UINT4               u4PortNum = 0;

    PNAC_TRC (PNAC_RED_TRC, "RED: Rcvd Port based sync up msg from RM \n");

    if (PNAC_NODE_STATUS () != PNAC_STANDBY_NODE)
    {
        PNAC_TRC (PNAC_RED_TRC, "RED: Rcvd sync up msg from RM "
                  "when pnac node is not standby.\n");
        return;
    }

    /* Port based PNAC data. Read the portnum, status, state and cause. */
    /*
       ---------------------------------------------------------------------
       | MsgType | Len | SessFlag | PortNum |  ReAuth | Auth | Auth| Oper  |
       | pb_upd  |     |          |         |  Period |Status|State|CtrlDir|
       ---------------------------------------------------------------------
     */

    if (PNAC_MIN_PORTBASED_SYNCDATA_SIZE > u2Len)
    {
        /* Length field present in data is less than one port info. 
         * Something wrong!!!*/
        PNAC_TRC (PNAC_RED_TRC, "Given len is less than info size for"
                  "sync up info.  Something wrong!!!!\n");
        return;
    }

    while (PnacRedReadPortSessInfo (pData, u2Len, pu4Offset, &u4PortNum)
           != PNAC_FAILURE)
    {
        /* Read the synced up data from the message and apply it to the 
         * state machine immediately. Store the timestamp in tPnacRedSyncUpTime.
         */
    }
}

/*****************************************************************************/
/* Function Name      : PnacRedReadPortSessInfo.                             */
/*                                                                           */
/* Description        : This function reads one dynamic port based           */
/*                      information from pMsg and applies immediately.       */
/*                      Also stores the time at which the sync up message    */
/*                      arrived in the standby node to start the timers.     */
/*                                                                           */
/* Input(s)           : pMsg     - Pointer to the RM buffer.                 */
/*                      u2PktLen - Size of the buffer given by RM.           */
/*                      pu4Offset - Offset from where the read has to start. */
/*                                                                           */
/* Output(s)          : pu4PortNum - Port number read.                       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacRedSystemInfo.aPnacSyncUpData                   */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : ggPnacRedSystemInfo.aPnacSyncUpData                  */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - if further read can be made in the    */
/*                                     given buffer.                         */
/*                      PNAC_FAILURE - if further read is not possible on the*/
/*                                     given buffer.                         */
/*****************************************************************************/
INT4
PnacRedReadPortSessInfo (tRmMsg * pMsg, UINT2 u2PktLen, UINT4 *pu4Offset,
                         UINT4 *pu4PortNum)
{
    UINT1               u1SessionFlag = 0;
    tPnacRedSyncUpData  SyncUp;
    tPnacRedSyncUpTime *pSyncUpTime = NULL;
    tPnacPaePortEntry  *pPortInfo;
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_PNAC_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2PktLen <= *pu4Offset)
    {
        /* Pkt is fully read, so just return failure here. */
        return PNAC_FAILURE;
    }

    /* Read the session Flag. */
    PNAC_RM_GET_1_BYTE (pMsg, pu4Offset, u1SessionFlag);

    switch (u1SessionFlag)
    {
        case PNAC_RED_AUTH_UPDATE_MSG:
            if ((u2PktLen - *pu4Offset) <
                (PNAC_PORT_ID_SIZE + PNAC_AUTH_PORTINFO_SIZE))
            {
                /* Pkt doesn't not contains the full information. */
                ProtoEvt.u4Error = RM_PROCESS_FAIL;
                PnacRmHandleProtocolEvent (&ProtoEvt);
                return PNAC_FAILURE;
            }

            /* Read port ID. */
            PNAC_RM_GET_4_BYTE (pMsg, pu4Offset, *pu4PortNum);

            if ((*pu4PortNum < PNAC_MINPORTS) || (*pu4PortNum > PNAC_MAXPORTS))
            {
                *pu4PortNum = PNAC_PORT_INVALID;
                /* Invalid port, hence move the offset, so that it points to 
                 * the next port sync message.*/
                *pu4Offset += PNAC_AUTH_PORTINFO_SIZE;
                return PNAC_SUCCESS;
            }

            break;

        case PNAC_RED_SUPP_UPDATE_MSG:
            if ((u2PktLen - *pu4Offset) <
                (PNAC_PORT_ID_SIZE + PNAC_SUPP_PORTINFO_SIZE))
            {
                /* Pkt doesn't not contains the full information. */
                ProtoEvt.u4Error = RM_PROCESS_FAIL;
                PnacRmHandleProtocolEvent (&ProtoEvt);
                return PNAC_FAILURE;
            }

            /* Read port ID. */
            PNAC_RM_GET_4_BYTE (pMsg, pu4Offset, *pu4PortNum);
            if ((*pu4PortNum < PNAC_MINPORTS) || (*pu4PortNum > PNAC_MAXPORTS))
            {
                *pu4PortNum = PNAC_PORT_INVALID;
                /* Invalid port, hence move the offset, so that it points to 
                 * the next port sync message.*/
                *pu4Offset += PNAC_SUPP_PORTINFO_SIZE;
                return PNAC_SUCCESS;
            }

            break;

        case PNAC_RED_BOTH_UPDATE_MSG:
            /* This case is possible only when bulk update message comes. */

            if ((u2PktLen - *pu4Offset) <
                (PNAC_PORT_ID_SIZE + PNAC_AUTH_PORTINFO_SIZE +
                 PNAC_SUPP_PORTINFO_SIZE))
            {
                /* Pkt doesn't not contains the full information. */
                ProtoEvt.u4Error = RM_PROCESS_FAIL;
                PnacRmHandleProtocolEvent (&ProtoEvt);
                return PNAC_FAILURE;
            }

            /* Read port ID. */
            PNAC_RM_GET_4_BYTE (pMsg, pu4Offset, *pu4PortNum);
            if ((*pu4PortNum < PNAC_MINPORTS) || (*pu4PortNum > PNAC_MAXPORTS))
            {
                *pu4PortNum = PNAC_PORT_INVALID;
                /* Invalid port, hence move the offset, so that it points to 
                 * the next port sync message.*/
                *pu4Offset +=
                    (PNAC_AUTH_PORTINFO_SIZE + PNAC_SUPP_PORTINFO_SIZE);
                return PNAC_SUCCESS;
            }
            break;

        default:
            ProtoEvt.u4Error = RM_PROCESS_FAIL;
            PnacRmHandleProtocolEvent (&ProtoEvt);
            return PNAC_FAILURE;
    }

    pPortInfo = &(PNAC_PORT_INFO (*pu4PortNum));

    if (pPortInfo->u1EntryStatus != PNAC_PORT_OPER_UP)
    {
        /* Apply is possible only for valid ports that are created. */
        return PNAC_SUCCESS;
    }

    if (!PNAC_IS_PORT_MODE_PORTBASED (pPortInfo))
    {
        /* Apply is possible only for port based authentication
         * sessions.
         */
        return PNAC_SUCCESS;
    }

    if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl
        != PNAC_PORTCNTRL_AUTO)
    {
        /* For ports that are either ForceAuthorized or
         * ForceUnAuthorized, the sem states are constructed based on
         * config and port oper status and hence nothing to do here.
         */
        return PNAC_SUCCESS;
    }

    if (u1SessionFlag == PNAC_RED_AUTH_UPDATE_MSG)
    {
        /* Read Authentation session information. */
        PNAC_RM_GET_4_BYTE (pMsg, pu4Offset, SyncUp.u4SessReAuthPeriod);
        PNAC_RM_GET_2_BYTE (pMsg, pu4Offset, SyncUp.u2AuthStatus);
        PNAC_RM_GET_2_BYTE (pMsg, pu4Offset, SyncUp.u2AuthPaeState);

        /* When the ASM moves to HELD state heldtimer needs to be started and 
         * when the ASM moves to authenticated state reauthtimer needs to be  
         * started / restarted. So store the time at which the synced up data
         * arrived.
         */

        pSyncUpTime = PNAC_SYNCUP_ENTRY (*pu4PortNum);

        if ((SyncUp.u2AuthPaeState == PNAC_ASM_STATE_HELD) ||
            (SyncUp.u2AuthPaeState == PNAC_ASM_STATE_AUTHENTICATED))
        {
            PNAC_GET_SYSTEM_TIME (&(pSyncUpTime->SyncedUpTime));
        }
        else
        {
            pSyncUpTime->SyncedUpTime = 0;
        }

        PnacRedApplyAuthDynamicData (pPortInfo,
                                     pPortInfo->pPortAuthSessionNode, &SyncUp);
    }
    else if (u1SessionFlag == PNAC_RED_SUPP_UPDATE_MSG)
    {

        /* Read Supplicant session information. */
        PNAC_RM_GET_2_BYTE (pMsg, pu4Offset, SyncUp.u2SuppPortStatus);
        PNAC_RM_GET_2_BYTE (pMsg, pu4Offset, SyncUp.u2SuppPaeState);
        pSyncUpTime = PNAC_SYNCUP_ENTRY (*pu4PortNum);
        PNAC_GET_SYSTEM_TIME (&(pSyncUpTime->SuppSyncedUpTime));
        PnacRedApplySuppDynamicData (pPortInfo,
                                     pPortInfo->pPortSuppSessionNode, &SyncUp);
    }
    else if (u1SessionFlag == PNAC_RED_BOTH_UPDATE_MSG)
    {

        /* Read Authentation session information. */
        PNAC_RM_GET_4_BYTE (pMsg, pu4Offset, SyncUp.u4SessReAuthPeriod);
        PNAC_RM_GET_2_BYTE (pMsg, pu4Offset, SyncUp.u2AuthStatus);
        PNAC_RM_GET_2_BYTE (pMsg, pu4Offset, SyncUp.u2AuthPaeState);
        pSyncUpTime = PNAC_SYNCUP_ENTRY (*pu4PortNum);
        PNAC_GET_SYSTEM_TIME (&(pSyncUpTime->SyncedUpTime));
        PnacRedApplyAuthDynamicData (pPortInfo,
                                     pPortInfo->pPortAuthSessionNode, &SyncUp);

        /* Read Supplicant session information. */
        PNAC_RM_GET_2_BYTE (pMsg, pu4Offset, SyncUp.u2SuppPortStatus);
        PNAC_RM_GET_2_BYTE (pMsg, pu4Offset, SyncUp.u2SuppPaeState);
        PNAC_GET_SYSTEM_TIME (&(pSyncUpTime->SuppSyncedUpTime));
        /* If supplicant access control is active then apply the 
         * sync up data for supplicant sem. Else don't apply. */
        if ((pPortInfo->suppConfigEntry.u2SuppAccessCtrlWithAuth ==
             PNAC_SUPP_ACCESS_CTRL_ACTIVE) &&
            (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo)))
        {
            PnacRedApplySuppDynamicData (pPortInfo,
                                         pPortInfo->pPortSuppSessionNode,
                                         &SyncUp);
        }

    }

    /* Update the combined port status of port. */
    if (PnacCheckIfPortStatusChanged ((UINT2) *pu4PortNum) != PNAC_SUCCESS)
    {
        return PNAC_FAILURE;
    }

    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacRedHandlePortOperUpMsg.                          */
/*                                                                           */
/* Description        : If the port number present in message is zero, then  */
/*                      clear all dynamic syncup information. If valid port  */
/*                      number present in the message then clean the dynamic */
/*                      information for that port.                           */
/*                                                                           */
/* Input(s)           : pData - Pointer to the RM buffer.                    */
/*                      pu4Offset - Offset from where the read should start. */
/*                      u2Len - Length of the information (includes msgtype  */
/*                              msglen).                                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacRedSystemInfo.aPnacSyncUpData.                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacRedSystemInfo.aPnacSyncUpData.                  */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedHandlePortOperUpMsg (tRmMsg * pData, UINT4 *pu4Offset, UINT2 u2Len)
{

    tPnacPaePortEntry  *pPortInfo;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4PortNum;
    UINT2               u2PnacPortStatus;

    ProtoEvt.u4AppId = RM_PNAC_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    PNAC_TRC (PNAC_RED_TRC, "RED: Rcvd Port oper up msg from RM \n");
    if (PNAC_NODE_STATUS () != PNAC_STANDBY_NODE)
    {
        PNAC_TRC (PNAC_RED_TRC, "RED: Rcvd Sync up msg from RM "
                  "when pnac node is not standby.\n");
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        PnacRmHandleProtocolEvent (&ProtoEvt);
        return;
    }

    /* Minimum length of port oper up sync message */
    if (u2Len < (PNAC_SYNCUP_HDR_LEN + PNAC_PORT_OPER_UP_MSG_SIZE))
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        PnacRmHandleProtocolEvent (&ProtoEvt);
        return;
    }

    u2Len -= PNAC_SYNCUP_HDR_LEN;

    while (u2Len >= PNAC_PORT_OPER_UP_MSG_SIZE)
    {
        /* Read port number. */
        PNAC_RM_GET_4_BYTE (pData, pu4Offset, u4PortNum);
        u2Len -= PNAC_PORT_OPER_UP_MSG_SIZE;

        if ((u4PortNum < PNAC_MINPORTS) || (u4PortNum > PNAC_MAXPORTS))
        {
            /* Wrong port number. */
            PNAC_TRC (PNAC_RED_TRC, "Syncup data for wrong port num. "
                      "Something wrong!!!!\n");
            ProtoEvt.u4Error = RM_PROCESS_FAIL;
            PnacRmHandleProtocolEvent (&ProtoEvt);
            continue;
        }

        pPortInfo = &(PNAC_PORT_INFO (u4PortNum));
        L2IwfGetPortPnacAuthStatus (pPortInfo->u2PortNo, &u2PnacPortStatus);

        if ((pPortInfo->u1EntryStatus == PNAC_PORT_OPER_DOWN) ||
            (u2PnacPortStatus == PNAC_PORTSTATUS_UNAUTHORIZED))
        {
            pPortInfo->u1EntryStatus = PNAC_PORT_OPER_UP;

            if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).
                u2AuthControlPortControl == PNAC_PORTCNTRL_FORCEAUTHORIZED)
            {
                /* PnacIsPortStatusChanged check is not needed
                 * here as its already called when port status is 
                 * changed to AUTHORISED/UNAUTHORISED.*/
                pPortInfo->u2PortStatus = PNAC_PORTSTATUS_AUTHORIZED;
                PnacL2IwfSetPortPnacAuthStatus (pPortInfo->u2PortNo,
                                                pPortInfo->u2PortStatus);
            }

            if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
            {
                PnacPaeCdStateMachine (PNAC_CDSM_EV_PORTENABLED,
                                       pPortInfo->u2PortNo);
                PnacAuthLinkStatusUpdate (pPortInfo);
            }
            if (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))
            {
                PnacSuppLinkStatusUpdate (pPortInfo);
            }

            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " Updated Operational status successfully ...\n");
        }
        L2PnacRedSetBridgePortOperStatus (u4PortNum, PNAC_PORT_OPER_UP);
    }
}

/*****************************************************************************/
/* Function Name      : PnacRedHandleAsynchronousStatus                      */
/*                                                                           */
/* Description        : If the port number present in message is zero, then  */
/*                      clear all dynamic syncup information. If valid port  */
/*                      number present in the message then clean the dynamic */
/*                      information for that port.                           */
/*                                                                           */
/* Input(s)           : pData - Pointer to the RM buffer.                    */
/*                      pu4Offset - Offset from where the read should start. */
/*                      u2Len - Length of the information (includes msgtype  */
/*                              msglen).                                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacRedSystemInfo.aPnacSyncUpData.                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacRedSystemInfo.aPnacSyncUpData.                  */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedHandleAsynchronousStatus (tRmMsg * pData, UINT4 *pu4Offset, UINT2 u2Len)
{
#ifdef NPAPI_WANTED

    UINT2               u2Port;
    UINT1               u1PnacPortCtrlDir;
    UINT1               u1PnacPortAuthStatus;

    if (PNAC_NODE_STATUS () != PNAC_STANDBY_NODE)
    {
        PNAC_TRC (PNAC_RED_TRC, "RED: Rcvd Sync up msg from RM "
                  "when pnac node is not standby.\n");
        return;
    }

    /* Minimum length of port oper up sync message */
    if (u2Len < (PNAC_SYNCUP_HDR_LEN + PNAC_PORT_ID_SIZE))
    {
        return;
    }

    u2Len -= PNAC_SYNCUP_HDR_LEN;

    while (u2Len >= PNAC_PORT_OPER_UP_MSG_SIZE)
    {
        /* Read port number. */
        PNAC_RM_GET_2_BYTE (pData, pu4Offset, u2Port);

        /* Read port control direction. */
        PNAC_RM_GET_1_BYTE (pData, pu4Offset, u1PnacPortCtrlDir);

        /* Read port Auth Status. */
        PNAC_RM_GET_1_BYTE (pData, pu4Offset, u1PnacPortAuthStatus);

        u2Len -= PNAC_PORT_ID_SIZE;
    }

    PnacProcNpAuthCbPortBasedSuccess (u2Port, u1PnacPortCtrlDir,
                                      u1PnacPortAuthStatus);

    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
              " Asynchronous Updation status successfully ...\n");
#else
    UNUSED_PARAM (pData);
    UNUSED_PARAM (pu4Offset);
    UNUSED_PARAM (u2Len);
#endif /* NPAPI_WANTED */
}

/*****************************************************************************/
/* Function Name      : PnacRedHandleModOrPortClearMsg.                      */
/*                                                                           */
/* Description        : If the port number present in message is zero, then  */
/*                      clear all dynamic syncup information. If valid port  */
/*                      number present in the message then clean the dynamic */
/*                      information for that port.                           */
/*                                                                           */
/* Input(s)           : pData - Pointer to the RM buffer.                    */
/*                      pu4Offset - Offset from where the read should start. */
/*                      u2Len - Length of the information (includes msgtype  */
/*                              msglen).                                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacRedSystemInfo.aPnacSyncUpData.                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacRedSystemInfo.aPnacSyncUpData.                  */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedHandleModOrPortClearMsg (tRmMsg * pData, UINT4 *pu4Offset, UINT2 u2Len)
{
    tPnacRedSyncUpTime *pSyncUpEntry;
    tPnacPaePortEntry  *pPortInfo;
    UINT4               u4PortNum;
    UINT4               u4Index;

    PNAC_TRC (PNAC_RED_TRC, "RED: Rcvd Mod or Port clear up msg from RM \n");

    if (PNAC_NODE_STATUS () != PNAC_STANDBY_NODE)
    {
        PNAC_TRC (PNAC_RED_TRC, "RED: Rcvd Sync up msg from RM "
                  "when pnac node is not standby.\n");
        return;
    }

    /* Check whether the given length is enough to carry the module or port
     * clear message. */
    if (u2Len < (PNAC_SYNCUP_HDR_LEN + PNAC_MOD_OR_PORT_CLR_INFO_SIZE))
    {
        return;
    }

    /* Read port number. */
    PNAC_RM_GET_4_BYTE (pData, pu4Offset, u4PortNum);

    if (u4PortNum == 0)
    {
        /* Pnac Module Disable message, hence clean up all dynamic syncup
         * information. */
        for (u4Index = 1; u4Index < PNAC_MAXPORT_INFO; u4Index++)
        {
            pSyncUpEntry = PNAC_SYNCUP_ENTRY (u4Index);
            MEMSET (pSyncUpEntry, 0, sizeof (tPnacRedSyncUpTime));
        }
    }
    else
    {
        /* Port disable message, hence clean up dynamic syncup message for 
         * this port. */
        if ((u4PortNum < PNAC_MINPORTS) || (u4PortNum > PNAC_MAXPORTS))
        {
            /* Wrong port number. */
            PNAC_TRC (PNAC_RED_TRC, "Syncup data for wrong port num. "
                      "Something wrong!!!!\n");
            return;
        }

        pSyncUpEntry = PNAC_SYNCUP_ENTRY (u4PortNum);
        MEMSET (pSyncUpEntry, 0, sizeof (tPnacRedSyncUpTime));
        pPortInfo = &(PNAC_PORT_INFO (u4PortNum));

        if (pPortInfo->u1EntryStatus == PNAC_PORT_OPER_UP)
        {
            /* If Port Auth control is Auto giving Unauthorized 
             * indication to L2Iwf is taken care by the State machine */
            if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).
                u2AuthControlPortControl == PNAC_PORTCNTRL_FORCEAUTHORIZED)
            {
                PnacL2IwfSetPortPnacAuthStatus (pPortInfo->u2PortNo,
                                                PNAC_PORTSTATUS_UNAUTHORIZED);
            }
            pPortInfo->u1EntryStatus = PNAC_PORT_OPER_DOWN;
            if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
            {
                PnacPaeCdStateMachine (PNAC_CDSM_EV_PORTDISABLED,
                                       pPortInfo->u2PortNo);
                PnacAuthLinkStatusUpdate (pPortInfo);
            }
            if (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo))
            {
                PnacSuppLinkStatusUpdate (pPortInfo);
            }
            L2PnacRedSetBridgePortOperStatus (u4PortNum, PNAC_PORT_OPER_DOWN);

            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " Updated Operational status successfully ...\n");
        }
    }
}

/*****************************************************************************/
/* Function Name      : PnacRedMakeNodeStandbyFromIdle.                      */
/*                                                                           */
/* Description        : This function makes node standby from idle state.    */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSystemInfo.NodeStatus.                          */
/*                      gPnacRedSystemInfo.u1NumPeersUp.                     */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedMakeNodeStandbyFromIdle (VOID)
{
    PNAC_NODE_STATUS () = PNAC_STANDBY_NODE;
    PNAC_NUM_STANDBY_NODES () = 0;
    if (PNAC_PROTOCOL_ADMIN_STATUS () == PNAC_ENABLED)
    {
        PnacModuleEnable ();
        PNAC_TRC (PNAC_RED_TRC, "PNAC RED: Enabled module.\n");
    }
}

/*****************************************************************************/
/* Function Name      : PnacRedMakeNodeActiveFromStandby.                    */
/*                                                                           */
/* Description        : This function makes node active from standby state.  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSystemInfo.NodeStatus.                          */
/*                      gPnacRedSystemInfo.u1NumPeersUp.                     */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedMakeNodeActiveFromStandby (VOID)
{
    UINT4               u4PortNum;
    tPnacPaePortEntry  *pPortInfo;
    tPnacAuthFsmInfo   *pAuthFsmInfo = NULL;
    tPnacSuppFsmInfo   *pSuppFsmInfo = NULL;
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tPnacSuppSessionNode *pSuppSessNode = NULL;
    tPnacRedSyncUpTime *pSyncUpTime = NULL;

    PNAC_NODE_STATUS () = PNAC_ACTIVE_NODE;

    if (PNAC_IS_SHUTDOWN ())
    {
        PNAC_RM_GET_NUM_STANDBY_NODES_UP ();
        PNAC_TRC (PNAC_RED_TRC, "PNAC Node made active and"
                  "PNAC is not started.\n");
        return;
    }

    /* Start the timers based on the states of the
     * state machine and start the hardware audit to ensure that the 
     * hardware and the software are in sync
     */

    for (u4PortNum = PNAC_MINPORTS; u4PortNum <= PNAC_MAXPORTS; u4PortNum++)
    {
        pPortInfo = &(PNAC_PORT_INFO (u4PortNum));

        if (pPortInfo->u1EntryStatus == PNAC_PORT_INVALID)
        {
            continue;
        }

        pAuthSessNode = pPortInfo->pPortAuthSessionNode;
        pSuppSessNode = pPortInfo->pPortSuppSessionNode;
        pAuthFsmInfo = &(pAuthSessNode->authFsmInfo);
        pSuppFsmInfo = &(pSuppSessNode->suppFsmInfo);
        pSyncUpTime = PNAC_SYNCUP_ENTRY (u4PortNum);

#ifdef NPAPI_WANTED
        if (PnacFsPnacRedHwUpdateDB ((UINT2) u4PortNum, NULL,
                                     pPortInfo->u1PortAuthMode,
                                     (UINT1) pPortInfo->u2PortStatus,
                                     (UINT1)
                                     pPortInfo->authConfigEntry.
                                     u2OperControlDir) != FNP_SUCCESS)
        {
            PNAC_TRC_ARG1 (PNAC_RED_TRC, "PNAC RED: Failed to update NPAPI "
                           "DB during switchover (Standby -> Active) for "
                           "port %d", u4PortNum);
        }
#endif

        if (pPortInfo->u1EntryStatus != PNAC_PORT_OPER_UP)
        {
            /* Apply is possible only for valid ports that are created. */
            continue;
        }

        if (!PNAC_IS_PORT_MODE_PORTBASED (pPortInfo))
        {
            /* Apply is possible only for port based authentication 
             * sessions.*/
            continue;
        }

        if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u2AuthControlPortControl
            != PNAC_PORTCNTRL_AUTO)
        {
            /* For ports that are either ForceAuthorized or 
             * ForceUnAuthorized, the sem states are constructed based on 
             * config and port oper status and hence nothing to do here.*/
            continue;
        }

        if (PNAC_IS_PORT_AUTH_CAPABLE (pPortInfo))
        {
            if (pAuthFsmInfo->u2AuthPaeState == PNAC_ASM_STATE_CONNECTING)
            {
                /* Start the TxWhenTimer for ports that are in Connecting state
                 * and transmit EAP Req/Identity packet to the supplicant
                 * to restart the authentication process for those ports that 
                 * were in CONNECTING/AUTHENTICATING/ABORTING state in the 
                 * previous Active node*/
                if (pAuthSessNode->pTxWhenTimer != NULL)
                {
                    if (PnacTmrStopTmr ((VOID *) pAuthSessNode,
                                        PNAC_TXWHEN_TIMER) != PNAC_SUCCESS)

                    {
                        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                                  " ASM: Stop txwhen timer returned failure\n");
                        return;
                    }
                }
                if (PnacTmrStartTmr
                    ((VOID *) pAuthSessNode, PNAC_TXWHEN_TIMER,
                     PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4TxPeriod)
                    != PNAC_SUCCESS)

                {
                    PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                              " ASM: Start TxWhen timer returned failure \n");
                    return;
                }
                if (pAuthSessNode->bIsRsnPreAuthSess == OSIX_TRUE)
                {
                    if (PnacAuthTxReqId
                        (pAuthSessNode->rmtSuppMacAddr,
                         pAuthFsmInfo->u2CurrentId,
                         pAuthSessNode->u2PreAuthPhyPort,
                         pAuthSessNode->bIsRsnPreAuthSess) != PNAC_SUCCESS)

                    {
                        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                                  "ASM: Transmit Request/Identity returned"
                                  " failure\n");
                        return;
                    }
                }
                else
                {
                    if (PnacAuthTxReqId
                        (pAuthSessNode->rmtSuppMacAddr,
                         pAuthFsmInfo->u2CurrentId,
                         pPortInfo->u2PortNo, pAuthSessNode->bIsRsnPreAuthSess)
                        != PNAC_SUCCESS)
                    {
                        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                                  "ASM: Transmit Request/Identity returned"
                                  " failure\n");
                        return;
                    }
                }

                if ((pAuthSessNode)->authFsmInfo.u2AuthControlPortStatus ==
                    PNAC_PORTSTATUS_AUTHORIZED)
                {
                    if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).bReAuthEnabled ==
                        PNAC_TRUE)

                    {
                        /* Since the session is authorized and reauthentication is
                         * enabled, start the reauthentication timer. */
                        PnacRedReStartTimer
                            (pPortInfo, pAuthSessNode, PNAC_REAUTHWHEN_TIMER,
                             (pAuthSessNode->authFsmInfo.u4SessReAuthPeriod),
                             (pSyncUpTime->SyncedUpTime));
                    }
                }
            }
            else if (pAuthFsmInfo->u2AuthPaeState ==
                     PNAC_ASM_STATE_AUTHENTICATED)
            {
                if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).bReAuthEnabled
                    == PNAC_TRUE)

                {
                    /* If reauth while timer is already started in active node, 
                     * then start the timer only for the exact left out time. */
                    PnacRedReStartTimer
                        (pPortInfo, pAuthSessNode, PNAC_REAUTHWHEN_TIMER,
                         (pAuthSessNode->authFsmInfo.u4SessReAuthPeriod),
                         (pSyncUpTime->SyncedUpTime));
                }

            }
            else if (pAuthFsmInfo->u2AuthPaeState == PNAC_ASM_STATE_HELD)
            {
                PnacRedReStartTimer
                    (pPortInfo, pAuthSessNode, PNAC_QUIETWHILE_TIMER,
                     PNAC_AUTH_CONFIG_ENTRY (pPortInfo).u4QuietPeriod,
                     pSyncUpTime->SyncedUpTime);
            }
        }

        /* If supplicant access control is active then apply the 
         * sync up data for supplicant sem. Else don't apply. */
        if ((pPortInfo->suppConfigEntry.u2SuppAccessCtrlWithAuth ==
             PNAC_SUPP_ACCESS_CTRL_ACTIVE) &&
            (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo)))
        {

            if (pSuppFsmInfo->u2SuppPaeState == PNAC_SSM_STATE_CONNECTING)
            {
                if (pSuppSessNode->pStartWhenTimer == NULL)
                {
                    PnacTmrStartTmr ((VOID *) pSuppSessNode,
                                     PNAC_STARTWHEN_TIMER,
                                     pPortInfo->suppConfigEntry.u4StartPeriod);
                }
                /* Now Form an EAPOL Start frame and transmit it to the
                 * authenticator */
                PnacSuppTxStart (gPnacEapolGrpMacAddr, pPortInfo->u2PortNo);

            }
            else if (pSuppFsmInfo->u2SuppPaeState == PNAC_SSM_STATE_HELD)
            {
                PnacRedReStartTimer (pPortInfo, (VOID *) pSuppSessNode,
                                     PNAC_HELDWHILE_TIMER,
                                     pPortInfo->suppConfigEntry.u4HeldPeriod,
                                     pSyncUpTime->SuppSyncedUpTime);
            }
        }
        if ((pPortInfo->suppConfigEntry.u2SuppAccessCtrlWithAuth !=
             PNAC_SUPP_ACCESS_CTRL_ACTIVE) &&
            (PNAC_IS_PORT_SUPP_CAPABLE (pPortInfo)))
        {
            if (pSuppFsmInfo->u2SuppPaeState == PNAC_SSM_STATE_CONNECTING)
            {
                if (pSuppSessNode->pStartWhenTimer == NULL)
                {
                    PnacTmrStartTmr ((VOID *) pSuppSessNode,
                                     PNAC_STARTWHEN_TIMER,
                                     pPortInfo->suppConfigEntry.u4StartPeriod);
                }
                /* Now Form an EAPOL Start frame and transmit it to the authenticator */
                PnacSuppTxStart (gPnacEapolGrpMacAddr, pPortInfo->u2PortNo);
            }
        }

    }

    /* Clear all dynamic informations. */
    for (u4PortNum = PNAC_MINPORTS; u4PortNum < PNAC_MAXPORT_INFO; u4PortNum++)
    {
        pSyncUpTime = PNAC_SYNCUP_ENTRY (u4PortNum);
        MEMSET (pSyncUpTime, 0, sizeof (tPnacRedSyncUpTime));
    }

    PNAC_RM_GET_NUM_STANDBY_NODES_UP ();

    PNAC_TRC (PNAC_RED_TRC, "PNAC RED: Switch over complete.\n");
}

/*****************************************************************************/
/* Function Name      : PnacRedMakeNodeActiveFromIdle.                       */
/*                                                                           */
/* Description        : This function makes node active from idle state.     */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSystemInfo.NodeStatus.                          */
/*                      gPnacRedSystemInfo.u1NumPeersUp.                     */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedMakeNodeActiveFromIdle (VOID)
{
    PNAC_NODE_STATUS () = PNAC_ACTIVE_NODE;
    PNAC_RM_GET_NUM_STANDBY_NODES_UP ();

    PNAC_TRC (PNAC_RED_TRC, "PNAC RED: Made node Active from IDLE " "state.\n");

    /* Node is Active, now enable the module only if the module is started 
     * and protocol admin status is enabled. */
    if ((PNAC_IS_STARTED ()) && (PNAC_PROTOCOL_ADMIN_STATUS () == PNAC_ENABLED))
    {
        PnacModuleEnable ();
        PNAC_TRC (PNAC_RED_TRC, "PNAC RED: Enabled module.\n");
    }
}

/*****************************************************************************/
/* Function Name      : PnacRedHandleStandByUpEvent.                         */
/*                                                                           */
/* Description        : This function handles the STANDBY UP event from RM.  */
/*                      If node is active, then it increments the            */
/*                      u1NumPeersUp.                                        */
/*                                                                           */
/* Input(s)           : pvPeerId - PeerId to which the BulkUpdate has to be  */
/*                      sent.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedHandleStandByUpEvent (VOID *pvPeerId)
{
    UNUSED_PARAM (pvPeerId);

#if !defined (CFA_WANTED) && !defined (EOAM_WANTED)
    if (PNAC_BULK_REQ_RECD () == PNAC_TRUE)
    {
        PNAC_BULK_REQ_RECD () = PNAC_FALSE;
        gPnacRedSystemInfo.u4BulkUpdNextPort = PNAC_MINPORTS;
        PnacRedHandleBulkUpdEvent ();
    }
#endif
}

/*****************************************************************************/
/* Function Name      : PnacRedHandleGoStandbyEvent                          */
/*                                                                           */
/* Description        : This function handles the GO_STANDBY event from RM.  */
/*                      It restarts the module and makes the node as standby.*/
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.NodeStatus                           */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSystemInfo.NodeStatus                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedHandleGoStandbyEvent (VOID)
{

    if (PNAC_NODE_STATUS () == PNAC_ACTIVE_NODE)
    {
        /* Make the node status as standby. */
        PNAC_NODE_STATUS () = PNAC_ACTIVE_TO_STANDBY_IN_PROGRESS;
        PNAC_NUM_STANDBY_NODES () = 0;

#ifdef NPAPI_WANTED
        if (PNAC_RED_AUDIT_TSKID () != PNAC_INIT_VAL)
        {
            PNAC_DELETE_TASK (PNAC_RED_AUDIT_TSKID ());
            PNAC_RED_AUDIT_TSKID () = PNAC_INIT_VAL;
        }
        /* Stop the audit process if it is running. */
        PNAC_RED_IS_AUDIT_STOP () = PNAC_TRUE;
#endif
        /*Disable the module inorder to flush the dynamic information */
        if (PNAC_PROTOCOL_ADMIN_STATUS () == PNAC_ENABLED)
        {
            PnacModuleDisable ();
            PNAC_NODE_STATUS () = PNAC_STANDBY_NODE;
            if (PnacModuleEnable () != PNAC_SUCCESS)
            {
                PNAC_TRC (PNAC_RED_TRC, "RED: Not able to enable PNAC "
                          " while making node standby from active state.\n");
            }
        }
    }
    else if (PNAC_NODE_STATUS () == PNAC_STANDBY_NODE)
    {
        /* Node is already a standby node. This should not happen. Assert if 
         * it happens. */
        PNAC_TRC (PNAC_RED_TRC, "RED: Node is already standby. "
                  "Getting GO_ STANDBY is wrong. Assert.\n");
    }
}

/*****************************************************************************/
/* Function Name      : PnacRedMakeNodeActive                                */
/*                                                                           */
/* Description        : This function makes the node active. If the node is  */
/*                      is in IDLE state then it enables the module if the   */
/*                      protocol admin status is enable. If the node is      */
/*                      standby, then depending on the states it starts the  */
/*                      timers and triggers the hardware audit               */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.NodeStatus                           */
/*                      gPnacSystemInfo.aPnacPaePortInfo                     */
/*                      gPnacRedSystemInfo.u1ProtocolAdminStatus             */
/*                      gPnacRedSystemInfo.aPnacSyncUpData                   */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSystemInfo.NodeStatus                           */
/*                      gPnacSystemInfo.aPnacPaePortInfo                     */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedMakeNodeActive (VOID)
{
    if (PNAC_NODE_STATUS () == PNAC_ACTIVE_NODE)
    {
        /* Node is already active. Nothing to do. */
        PNAC_TRC (PNAC_RED_TRC, "RED: Make active trigger received for "
                  "already active node. Something wrong in RM.\n");

        return;
    }

    if (PNAC_NODE_STATUS () == PNAC_IDLE_NODE)
    {
        /* Node is coming up for the first time. Move from IDLE state to active 
         * state.*/
        PnacRedMakeNodeActiveFromIdle ();
    }
    else
    {
        PnacRedMakeNodeActiveFromStandby ();
    }

    if (PNAC_BULK_REQ_RECD () == PNAC_TRUE)
    {
        PNAC_BULK_REQ_RECD () = PNAC_FALSE;
        gPnacRedSystemInfo.u4BulkUpdNextPort = PNAC_MINPORTS;
        PnacRedHandleBulkUpdEvent ();
    }
}

/*****************************************************************************/
/* Function Name      : PnacRedTrigHigherLayer                               */
/*                                                                           */
/* Description        : This function will send the given event to the next  */
/*                      higher layer (can be LA or STP or VLAN).             */
/*                                                                           */
/* Input(s)           : u1TrigType - Trigger Type.                           */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedTrigHigherLayer (UINT1 u1TrigType)
{
#ifdef LA_WANTED
    PnacLaRedRcvPktFromRm (u1TrigType, NULL, 0);
#else
#if defined(RSTP_WANTED) || defined(MSTP_WANTED)
    PnacAstHandleUpdateEvents (u1TrigType, NULL, 0);
#else
#ifdef VLAN_WANTED
    PnacVlanRedHandleUpdateEvents (u1TrigType, NULL, 0);
#else
#ifdef LLDP_WANTED
    PnacLldpRedRcvPktFromRm (u1TrigType, NULL, 0);
#else
    if ((PNAC_NODE_STATUS () == PNAC_ACTIVE_NODE) &&
        ((u1TrigType == RM_COMPLETE_SYNC_UP) ||
         (u1TrigType == L2_COMPLETE_SYNC_UP)))
    {
        /* Send an event to RM to notify that sync up is completed */
        if (RmSendEventToRmTask (L2_SYNC_UP_COMPLETED) != RM_SUCCESS)
        {
            PNAC_TRC (PNAC_RED_TRC, "PNAC send event L2_SYNC_UP_COMPLETED to "
                      "RM failed\n");
        }
    }
#endif /* LLDP_WANTED */
#endif
#endif
#endif
}

/*****************************************************************************/
/* Function Name      : PnacRedApplyAuthDynamicData                          */
/*                                                                           */
/* Description        : This function will be called if the node is standby. */
/*                      This function applies the dynamic syncup data to     */
/*                      authenticator sems.                                  */
/*                                                                           */
/* Input(s)           : pPortInfo - Pointer to the port entry.               */
/*                      pAuthSessNode - Pointer to the authentication        */
/*                                      session.                             */
/*                      pSyncUp - Pointer to the dynamic sync up data for    */
/*                                the given authentication session.          */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.aPnacPaePortInfo                     */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSystemInfo.aPnacPaePortInfo                     */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedApplyAuthDynamicData (tPnacPaePortEntry * pPortInfo,
                             tPnacAuthSessionNode * pAuthSessNode,
                             tPnacRedSyncUpData * pSyncUp)
{

    switch (pSyncUp->u2AuthPaeState)
    {
        case PNAC_ASM_STATE_INITIALIZE:
            /* Fall Through. */

        case PNAC_ASM_STATE_DISCONNECTED:
            /* Port would have been to connecting state. Bring it back to 
             * Disconnected state.*/
            PNAC_TRC (PNAC_RED_TRC, "PNAC RM: Synced up state is "
                      "Disconnected.\n");

            PnacAuthAsmMakeDisconnected (pPortInfo->u2PortNo,
                                         pAuthSessNode, NULL, PNAC_NO_VAL);
            break;

        case PNAC_ASM_STATE_AUTHENTICATING:
            /* Delibrate Fall through */
        case PNAC_ASM_STATE_ABORTING:
            /* Delibrate Fall through */
        case PNAC_ASM_STATE_CONNECTING:
            PNAC_TRC (PNAC_RED_TRC, "PNAC RM: Synced up state is "
                      "Connecting.\n");
            PnacAuthAsmMakeConnecting (pPortInfo->u2PortNo,
                                       pAuthSessNode, NULL, PNAC_NO_VAL);

            if (pSyncUp->u2AuthStatus == PNAC_PORTSTATUS_AUTHORIZED)
            {
                /* Either EAPOL Start packet has been received in the Active
                 * Node or the Reauthentication timer has expired.
                 * Session is authorized, hence make the 
                 * authcontrolportstatus as authorized. */
                (pAuthSessNode)->authFsmInfo.u2AuthControlPortStatus =
                    PNAC_PORTSTATUS_AUTHORIZED;

                /* Update the reauthenticatin period from the synced up date. */
                pPortInfo->pPortAuthSessionNode->authFsmInfo.u4SessReAuthPeriod
                    = pSyncUp->u4SessReAuthPeriod;
            }

            break;

        case PNAC_ASM_STATE_AUTHENTICATED:
            PNAC_TRC (PNAC_RED_TRC, "PNAC RM: Synced up state is "
                      "Authenticated.\n");
            /* In normal process, the SEM goes through Authenticating and then 
             * to Authenticated.  Here, it goes directly to Authenticated State.
             * The job done in Authenticating state is not reqd now since 
             * bAuthSuccess, bAuthFail, bAuthTimeout, bAuthStart variables 
             * already hold the FALSE values which is fine for Authenticated 
             * state. Hence calling MakeAuthenticated directly is fine.*/

            /* Update the reauthenticatin period from the synced up date. */
            pPortInfo->pPortAuthSessionNode->authFsmInfo.u4SessReAuthPeriod =
                pSyncUp->u4SessReAuthPeriod;

            PnacAuthAsmMakeAuthenticated (pPortInfo->u2PortNo, pAuthSessNode,
                                          NULL, PNAC_NO_VAL);

            break;

        case PNAC_ASM_STATE_HELD:

            PNAC_TRC (PNAC_RED_TRC, "PNAC RM: Synced up state is HELD.\n");

            PnacAuthAsmMakeHeld (pPortInfo->u2PortNo,
                                 pPortInfo->pPortAuthSessionNode, NULL,
                                 PNAC_NO_VAL);
            break;

        default:
            PNAC_TRC (PNAC_RED_TRC | PNAC_RED_TRC, "PNAC RM : "
                      "Invalid sycned up state. Something wrong!!!!!!!!!!!\n");
            break;
    }
}

/*****************************************************************************/
/* Function Name      : PnacRedApplySuppDynamicData                          */
/*                                                                           */
/* Description        : This function will be called if the node is standby. */
/*                      This function applies the dynamic syncup data to     */
/*                      supplicant sems.                                     */
/*                                                                           */
/* Input(s)           : pPortInfo - Pointer to the port entry.               */
/*                      pSuppSessNode - Pointer to the Supplecant session.   */
/*                      pSyncUp - Pointer to the dynamic sync up data for    */
/*                                the given authentication session.          */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.aPnacPaePortInfo                     */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSystemInfo.aPnacPaePortInfo                     */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedApplySuppDynamicData (tPnacPaePortEntry * pPortInfo,
                             tPnacSuppSessionNode * pSuppSessNode,
                             tPnacRedSyncUpData * pSyncUp)
{
    switch (pSyncUp->u2SuppPaeState)
    {
        case PNAC_SSM_STATE_DISCONNECTED:
            pSuppSessNode->suppFsmInfo.u4StartCount = 0;
            PnacSuppSmMakeConnecting (pPortInfo->u2PortNo, pSuppSessNode,
                                      NULL, 0);
            break;

        case PNAC_SSM_STATE_LOGOFF:
            /* This state is not possible. */
            break;

        case PNAC_SSM_STATE_AUTHENTICATING:
            /* Delibrate fall through. */
        case PNAC_SSM_STATE_ACQUIRED:
            /* Delibrate fall through. */
        case PNAC_SSM_STATE_CONNECTING:
            if (pSyncUp->u2SuppPortStatus == PNAC_PORTSTATUS_AUTHORIZED)
            {
                pSuppSessNode->suppFsmInfo.u2ControlPortStatus =
                    PNAC_PORTSTATUS_AUTHORIZED;
            }
            PnacSuppSmMakeConnecting (pPortInfo->u2PortNo, pSuppSessNode,
                                      NULL, 0);
            break;

        case PNAC_SSM_STATE_AUTHENTICATED:

            PnacSuppSmMakeAuthenticated (pPortInfo->u2PortNo, pSuppSessNode,
                                         NULL, 0);
            break;

        case PNAC_SSM_STATE_HELD:
            PNAC_TRC_ARG1 (PNAC_RED_TRC,
                           "PNAC RM: Supp Sync up state is HELD for port %d.\n",
                           pPortInfo->u2PortNo);
            PnacSuppSmMakeHeld (pPortInfo->u2PortNo, pSuppSessNode, NULL, 0);
            break;

        default:
            break;
    }
}

/*****************************************************************************/
/* Function Name      : PnacRedReStartTimer                                  */
/*                                                                           */
/* Description        : This function finds out the elapsed timer            */
/*                      duration for the givne timer and restarts the timer  */
/*                      only for the left out period. If the timer duration  */
/*                      is given as zero then it gives the timer expiry      */
/*                      event to the state event machines.                   */
/*                                                                           */
/* Input(s)           : pPortInfo - Pointer to port entry.                   */
/*                      pSessNode - Pointer to the auth / Supp session.      */
/*                      u1TmrType - Timer type (reauth tmr/ quiet while tmr) */
/*                      u4TimerDuration - Duration of the timer.             */
/*                      SyncedUpTime - Time when the timer was started in    */
/*                                     active node.                          */
/*                                                                           */
/* Output(s)          : pu1PortState - Pointer to status of given port.      */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedReStartTimer (tPnacPaePortEntry * pPortInfo,
                     VOID *pSessNode,
                     UINT1 u1TmrType, UINT4 u4TimerDuration,
                     tPnacSysTime SyncedUpTime)
{
    tPnacSysTime        CurrentTime;
    INT4                i4LeftOutDuration = 0;

    if ((u1TmrType != PNAC_REAUTHWHEN_TIMER) ||
        (((tPnacAuthSessionNode *) pSessNode)->pReAuthWhenTimer != NULL))
    {
        if (PnacTmrStopTmr (pSessNode, u1TmrType) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " ASM: Stop timer returned failure \n");
        }
    }

    PNAC_GET_SYSTEM_TIME (&CurrentTime);

    /* Find out the period left for the given timer to expire. First find
     * out diff betweent the current and synceduptime. This difference will be
     * in centiseconds. Convert this to seconds. This value % u4TimerDuration 
     * gives the number of seconds passed after reauth timer is started in the
     * active node.*/

    if (u4TimerDuration != 0)
    {
        if (SyncedUpTime == 0)
        {
            /* This case can come if switch over happens immediately after bulk
             * update
             */
            i4LeftOutDuration = u4TimerDuration;
        }
        else
        {
            i4LeftOutDuration =
                ((CurrentTime -
                  SyncedUpTime) / PNAC_NUM_OF_TIME_UNITS_IN_A_SEC);

            if (i4LeftOutDuration == (INT4) u4TimerDuration)
            {
                /* It is the time for timer expiry, so make LeftOutTime to 
                 * zero. */
                i4LeftOutDuration = 0;
            }
            else
            {
                i4LeftOutDuration =
                    u4TimerDuration - (i4LeftOutDuration % u4TimerDuration);
            }
        }
    }

    if ((u4TimerDuration == 0) || (i4LeftOutDuration == 0))
    {
        /* Timer has expired, so give the timer expiry event to auth sem. */
        switch (u1TmrType)
        {
            case PNAC_REAUTHWHEN_TIMER:
                ((tPnacAuthSessionNode *) pSessNode)->pReAuthWhenTimer = NULL;
                if (PnacAuthStateMachine (PNAC_ASM_EV_REAUTHENTICATE,
                                          pPortInfo->u2PortNo,
                                          (tPnacAuthSessionNode *) pSessNode,
                                          NULL, PNAC_NO_VAL) != PNAC_SUCCESS)
                {

                    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                              (" TMR: PnacTmrHandler: ASM returned failure\n"));
                }
                break;

            case PNAC_QUIETWHILE_TIMER:
                ((tPnacAuthSessionNode *) pSessNode)->pQuietWhileTimer = NULL;
                if (PnacAuthStateMachine
                    (PNAC_ASM_EV_QUIETWHILE_EXPIRED,
                     pPortInfo->u2PortNo,
                     (tPnacAuthSessionNode *) pSessNode,
                     NULL, PNAC_NO_VAL) != PNAC_SUCCESS)
                {

                    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                              (" TMR: PnacTmrHandler: ASM returned failure\n"));
                }
                break;

            case PNAC_HELDWHILE_TIMER:
                ((tPnacSuppSessionNode *) pSessNode)->pHeldWhileTimer = NULL;
                if (PnacSuppStateMachine (PNAC_SSM_EV_HELDWHILE_EXPIRED,
                                          pPortInfo->u2PortNo,
                                          (tPnacSuppSessionNode *) pSessNode,
                                          NULL, PNAC_NO_VAL) != PNAC_SUCCESS)
                {

                    PNAC_TRC (PNAC_ALL_FAILURE_TRC | PNAC_CONTROL_PATH_TRC,
                              (" PNAC ReStartTimer: RM Apply Data: SSM "
                               "returned failure\n"));
                }
                break;

            default:
                break;
        }
        return;
    }

    if (PnacTmrStartTmr (pSessNode, u1TmrType, i4LeftOutDuration)
        != PNAC_SUCCESS)

    {
        PNAC_TRC_ARG1 (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                       " ASM: Start TimerType: timer returned failure \n",
                       u1TmrType);
        return;
    }

}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      : PnacRedPerformAudit                                  */
/*                                                                           */
/* Description        : This function spawns an audit task. Audit task       */
/*                      audits the information present in sw and the         */
/*                      information present in hw and syncs hw with that of  */
/*                      sw.                                                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacRedSystemInfo.AuditTaskId                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacRedSystemInfo.AuditTaskId                       */
/*                      gPnacRedSystemInfo.u1IsAuditStopped                  */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedPerformAudit (VOID)
{
    INT4                i4RetVal;

    /* Set the audit stop flag to false. Based on this flag audit task will 
     * decide wheather to run or not. */
    PNAC_RED_IS_AUDIT_STOP () = PNAC_FALSE;

    if (PNAC_RED_AUDIT_TSKID () == PNAC_INIT_VAL)
    {
        i4RetVal = PNAC_CREATE_TASK (PNAC_AUDIT_TASK,
                                     PNAC_AUDIT_TASK_PRIORITY,
                                     OSIX_DEFAULT_STACK_SIZE,
                                     (OsixTskEntry) PnacRedAuditTaskMain,
                                     0, &PNAC_RED_AUDIT_TSKID ());

        if (i4RetVal != OSIX_SUCCESS)
        {
            PNAC_TRC (ALL_FAILURE_TRC, "PNAC AUDIT Task creation failed. \n");

            PNAC_RED_IS_AUDIT_STOP () = PNAC_TRUE;
            return;
        }

    }
}

/*****************************************************************************/
/* Function Name      : PnacRedAuditTaskMain                                 */
/*                                                                           */
/* Description        : This function audits the sw data (calculted by PNAC) */
/*                      and the hw data (infor programmed in chipset. It     */
/*                      takes proper action to make the sw and hw in sync    */
/*                      with each other.                                     */
/*                                                                           */
/* Input(s)           : pi1Param - unused parameter.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.aPnacPaePortInfo.                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedAuditTaskMain (INT1 *pi1Param)
{
    tPnacPaePortEntry  *pPortInfo;
    tPnacAuthConfigEntry *pAuthConfig;
    UINT4               u4PortNum;
    INT4                i4RetVal;
    UINT2               u2HwPortAuthStatus;
    UINT2               u2CtrlDir;
    UINT2               u2AuthDirChange = 0;

    UNUSED_PARAM (pi1Param);

    PNAC_TRC (PNAC_RED_TRC, "PNAC RED: Auditing sw and hw data.\n");

    /* For every comparison take lock before starting comparison and release 
     * lock once the comparison and the corresponding action is over. */
    PnacLock ();
    if (PNAC_RED_IS_AUDIT_STOP () == PNAC_TRUE)
    {
        PnacUnLock ();
        return;
    }

    for (u4PortNum = PNAC_MINPORTS; u4PortNum <= PNAC_MAXPORTS; u4PortNum++)
    {
        pPortInfo = &(PNAC_PORT_INFO (u4PortNum));

        if (pPortInfo->u1EntryStatus == PNAC_PORT_INVALID)
        {
            PNAC_TRC_ARG1 (PNAC_RED_TRC, "Pnac RED Audit: "
                           "Invalid port %d for Audit\n", u4PortNum);
            continue;
        }

        if ((PNAC_IS_PORT_NONE_CAPABLE (pPortInfo)) ||
            (!PNAC_IS_PORT_MODE_PORTBASED (pPortInfo)))
        {
            /* Audit is possible only for port based authentication sessions. */
            PnacUnLock ();
            PnacLock ();
            if (PNAC_RED_IS_AUDIT_STOP () == PNAC_TRUE)
            {
                PnacUnLock ();
                return;
            }
            continue;
        }

        i4RetVal = PnacRedGetHwAuthStatus (pPortInfo->u2PortNo,
                                           pPortInfo->u1PortAuthMode,
                                           &u2HwPortAuthStatus, &u2CtrlDir);

        if (i4RetVal == PNAC_SUCCESS)
        {
            u2AuthDirChange = 0;
            if (pPortInfo->u2PortStatus != u2HwPortAuthStatus)
            {
                if (u2HwPortAuthStatus == PNAC_PORTSTATUS_UNAUTHORIZED)
                {
                    PNAC_TRC_ARG1 (PNAC_RED_TRC, "Pnac RED Audit: Sw and Hw "
                                   "port status differ for port :%d with "
                                   "hw port status as Unauthorized.\n",
                                   pPortInfo->u2PortNo);
                    u2AuthDirChange |= PNAC_RED_AUTH_CHANGE;
                }
                else
                {
                    PNAC_TRC_ARG1 (PNAC_RED_TRC, "Pnac RED Audit: Sw and Hw "
                                   "port status differ for port :%d with "
                                   "sw port status as Unauthorized.\n",
                                   pPortInfo->u2PortNo);
                    /* Sw port status is Unauthorized, but the hw port
                     * state is authorized. */

                    /* Make hw unauthorized and then reauthenticate. */
                    /* Since we are programming the sw information here,
                     * there is no need to call PnacRedMakeSwInSyncWithHw
                     * function. */
                    PnacPnacHwSetAuthStatus
                        (pPortInfo->u2PortNo,
                         pPortInfo->pPortAuthSessionNode->
                         rmtSuppMacAddr,
                         pPortInfo->u1PortAuthMode,
                         (UINT1) pPortInfo->u2PortStatus,
                         (UINT1) pPortInfo->authConfigEntry.u2OperControlDir);

                    /* Now reauthenticate this port. */
                    if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).bReAuthEnabled ==
                        PNAC_TRUE)

                    {
                        PnacRedReStartTimer
                            (pPortInfo, pPortInfo->pPortAuthSessionNode,
                             PNAC_REAUTHWHEN_TIMER, 0, 0);
                    }

                    /* Indicate the oper status to higher layers. */
                    PnacAuthPortOperIndication (pPortInfo->u2PortNo);
                    PnacUnLock ();
                    PnacLock ();
                    if (PNAC_RED_IS_AUDIT_STOP () == PNAC_TRUE)
                    {
                        PnacUnLock ();
                        return;
                    }
                    continue;
                }

            }
            pAuthConfig = &(PNAC_AUTH_CONFIG_ENTRY (pPortInfo));
            if (pAuthConfig->u2OperControlDir != u2CtrlDir)
            {
                PNAC_TRC_ARG1 (PNAC_RED_TRC, "Pnac RED Audit: Port Ctrl"
                               "Direction differ between hw and sw for "
                               "port :%d\n", pPortInfo->u2PortNo);
                u2AuthDirChange |= PNAC_RED_DIR_CHANGE;
            }

            /* Now make the neccessary things to make the sw and hw
             * in sync. */
            PnacRedMakeSwInSyncWithHw (pPortInfo, u2AuthDirChange);
        }
        else
        {
            /* Directly program the sw status in to hw. */
            PNAC_TRC_ARG2 (PNAC_RED_TRC, "Pnac RED Audit: Get Hw Port"
                           "Status failed. Programming the status as:"
                           "%d in hw\n", pPortInfo->u2PortNo,
                           pPortInfo->u2PortStatus);
            PnacPnacHwSetAuthStatus
                (pPortInfo->u2PortNo,
                 pPortInfo->pPortAuthSessionNode->
                 rmtSuppMacAddr,
                 pPortInfo->u1PortAuthMode,
                 (UINT1) (pPortInfo->u2PortStatus),
                 (UINT1) pPortInfo->authConfigEntry.u2OperControlDir);

        }

        /* Indicate the oper status to higher layers. */
        PnacAuthPortOperIndication (pPortInfo->u2PortNo);
        PnacUnLock ();
        PnacLock ();
        if (PNAC_RED_IS_AUDIT_STOP () == PNAC_TRUE)
        {
            PnacUnLock ();
            return;
        }
    }
    PNAC_RED_AUDIT_TSKID () = PNAC_INIT_VAL;
    PnacUnLock ();
    return;
}

/*****************************************************************************/
/* Function Name      : PnacRedMakeSwInSyncWithHw                            */
/*                                                                           */
/* Description        : This function makes the sw and hw pnac auth status   */
/*                      in sync with each other. Based on the change         */
/*                      (authstatus or control dir or both it calls the      */
/*                      routines to carry out its task.                      */
/*                                                                           */
/* Input(s)           : pPortInfo - Pointer to the port.                     */
/*                      u2AuthDirChange - Auth/Dir change indication flag.   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedMakeSwInSyncWithHw (tPnacPaePortEntry * pPortInfo, UINT2 u2AuthDirChange)
{
    INT4                i4PnacProceed = PNAC_TRUE;

    switch (u2AuthDirChange)
    {
        case PNAC_RED_AUTH_CHANGE:
            /* Only auth status differs for this port between hw and sw. */
            i4PnacProceed = PnacRedHandleAuthStatusChange (pPortInfo);
            if (i4PnacProceed == PNAC_FALSE)
            {
                return;
            }
            break;

        case PNAC_RED_DIR_CHANGE:
            /* Only Oper Ctrldir differs for this port between hw and sw. 
             * Program the sw port status  and ctrl dir in to hw. */
            PnacRedHandleCtrlDirChange (pPortInfo, pPortInfo->u2PortStatus);
            break;

        case PNAC_RED_BOTH_CHANGE:
            /* Both auth status and oper ctrl dir differs for this port
             * between hw and sw. */
            i4PnacProceed = PnacRedHandleAuthStatusChange (pPortInfo);
            if (i4PnacProceed == PNAC_FALSE)
            {
                return;
            }
            /* Sw and Hw port status are equal now. Now Program the sw port 
             * status  and ctrl dir in to hw. */
            PnacRedHandleCtrlDirChange (pPortInfo, pPortInfo->u2PortStatus);
            break;
        default:
            break;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : PnacRedHandleAuthStatusChange                        */
/*                                                                           */
/* Description        : This function will be called if during audit it      */
/*                      auth status between the sw and hw for a port was     */
/*                      found to be different and the hw auth status is      */
/*                      unauthorized.                                        */
/*                                                                           */
/* Input(s)           : pPortInfo - Pointer to the port.                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : PNAC_TRUE- if control dir change between hw and sw   */
/*                                 needs to be handled.                      */
/*                      PNAC_FALSE - if control dir change between hw and sw */
/*                                   need not be handled.                    */
/*****************************************************************************/
INT4
PnacRedHandleAuthStatusChange (tPnacPaePortEntry * pPortInfo)
{
    tPnacAuthFsmInfo   *pAuthFsmInfo;
    tPnacSuppSessionNode *pSuppSessNode = NULL;
    tPnacSuppConfigEntry *pSuppConfig = NULL;

    INT4                i4NpapiMode = PNAC_NP_SYNC;
    INT4                i4ProtoId = (INT4) L2_PROTO_DOT1X;
    UINT1               u1PnacPortOperStatus = PNAC_PORT_OPER_DOWN;

    i4NpapiMode = PnacNpGetNpapiMode (i4ProtoId);

    PnacL2IwfGetPortOperStatus (LA_MODULE, pPortInfo->u2PortNo,
                                &u1PnacPortOperStatus);

    if (pPortInfo->u1EntryStatus == PNAC_PORT_OPER_DOWN)
    {
        /* This is the case if the port control is force authorized and
         * the port is oper down and the hw status is unauthorized.*/
        return PNAC_FALSE;
    }

    pAuthFsmInfo = &(pPortInfo->pPortAuthSessionNode->authFsmInfo);

    if (pAuthFsmInfo->u2AuthControlPortStatus == PNAC_PORTSTATUS_AUTHORIZED)
    {
        switch (pAuthFsmInfo->u2AuthPaeState)
        {
            case PNAC_ASM_STATE_AUTHENTICATED:
                if (i4NpapiMode == PNAC_NP_ASYNC &&
                    u1PnacPortOperStatus == PNAC_PORT_OPER_UP)
                {
                    /* Port is UP in Higher Layers.
                       So, Indicating as Down and Setting L2IWF. */
                    PnacAuthPortOperIndication (pPortInfo->u2PortNo);

                    PnacL2IwfSetPortPnacAuthStatus (pPortInfo->u2PortNo,
                                                    PNAC_PORTSTATUS_UNAUTHORIZED);
                }
                pAuthFsmInfo->u2AuthControlPortStatus =
                    PNAC_PORTSTATUS_UNAUTHORIZED;
                PnacAuthAsmMakeConnecting (pPortInfo->u2PortNo,
                                           pPortInfo->pPortAuthSessionNode,
                                           NULL, 0);
                break;

            case PNAC_ASM_STATE_FORCEAUTH:
#ifdef NPAPI_WANTED
                PnacPnacHwSetAuthStatus
                    (pPortInfo->u2PortNo,
                     pPortInfo->pPortAuthSessionNode->
                     rmtSuppMacAddr,
                     pPortInfo->u1PortAuthMode,
                     (UINT1) (pAuthFsmInfo->u2AuthControlPortStatus),
                     (UINT1) pPortInfo->authConfigEntry.u2OperControlDir);
#endif
                if (i4NpapiMode == PNAC_NP_ASYNC &&
                    u1PnacPortOperStatus == PNAC_PORT_OPER_DOWN)
                {
                    /* Port is Down in Higher Layers.
                       So, Indicating as Up and Setting L2IWF. */
                    PnacAuthPortOperIndication (pPortInfo->u2PortNo);

                    PnacL2IwfSetPortPnacAuthStatus (pPortInfo->u2PortNo,
                                                    PNAC_PORTSTATUS_AUTHORIZED);
                }
                /* No need to check for dir ctrl change as that is also
                 * over written in to the hw, hence return true here. */
                return PNAC_FALSE;
                break;

            default:
                /* Possible ASM states: CONNECTING/AUTHENTICATING/
                 *                      DISCONNECTED/HELD
                 * Hence just make auth status as unauthorized and
                 * reauthenticate. */
                pAuthFsmInfo->u2AuthControlPortStatus =
                    PNAC_PORTSTATUS_UNAUTHORIZED;

                if (i4NpapiMode == PNAC_NP_ASYNC &&
                    u1PnacPortOperStatus == PNAC_PORT_OPER_UP)
                {
                    /* Port is UP in Higher Layers.
                       So, Indicating as Down and Setting L2IWF. */
                    PnacAuthPortOperIndication (pPortInfo->u2PortNo);

                    PnacL2IwfSetPortPnacAuthStatus (pPortInfo->u2PortNo,
                                                    PNAC_PORTSTATUS_UNAUTHORIZED);
                }

                break;
        }
    }

    /* Now reauthenticate this port. */
    if (PNAC_AUTH_CONFIG_ENTRY (pPortInfo).bReAuthEnabled == PNAC_TRUE)

    {
        PnacRedReStartTimer (pPortInfo,
                             pPortInfo->pPortAuthSessionNode,
                             PNAC_REAUTHWHEN_TIMER, 0, 0);
    }

    pSuppSessNode = pPortInfo->pPortSuppSessionNode;
    pSuppConfig = &(pPortInfo->suppConfigEntry);

    /* If the SuppAccessCtrlWithAuth is active and if the supplicant port
     * status is authorized, then
     *  - make the supplicant status as unauthorized
     *  - Move the connecting state.
     */
    if ((PNAC_IS_PORT_BOTH_CAPABLE (pPortInfo)) &&
        (pSuppConfig->u2SuppAccessCtrlWithAuth ==
         PNAC_SUPP_ACCESS_CTRL_ACTIVE) &&
        (pSuppSessNode->suppFsmInfo.u2ControlPortStatus ==
         PNAC_PORTSTATUS_AUTHORIZED))
    {
        pSuppSessNode->suppFsmInfo.u2ControlPortStatus =
            PNAC_PORTSTATUS_UNAUTHORIZED;
        PnacSuppSmMakeConnecting (pPortInfo->u2PortNo, pSuppSessNode, NULL, 0);
    }

    /* Update the combined port status of port. */
    PnacCheckIfPortStatusChanged (pPortInfo->u2PortNo);

    return PNAC_TRUE;
}

/*****************************************************************************/
/* Function Name      : PnacRedHandleCtrlDirChange                           */
/*                                                                           */
/* Description        : This function will be called if during audit it      */
/*                      control dir between the sw and hw for a port was     */
/*                      found to be different and the hw auth status is      */
/*                      unauthorized. This function programs the given port  */
/*                      authorization status in sw and the ctrl dir of the   */
/*                      port in sw in to the hw.                             */
/*                                                                           */
/* Input(s)           : pPortInfo - Pointer to the port.                     */
/*                      u2PortStaus - Port Authorization status.             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedHandleCtrlDirChange (tPnacPaePortEntry * pPortInfo, UINT2 u2PortStatus)
{
    PnacPnacHwSetAuthStatus (pPortInfo->u2PortNo,
                             pPortInfo->pPortAuthSessionNode->rmtSuppMacAddr,
                             pPortInfo->u1PortAuthMode, (UINT1) (u2PortStatus),
                             (UINT1) (pPortInfo->authConfigEntry.
                                      u2OperControlDir));
}

/*****************************************************************************/
/* Function Name      : PnacRedGetHwAuthStatus                               */
/*                                                                           */
/* Description        : This function gets the authorization status of the   */
/*                      given port from hw. If NPAPI is not defined or if the*/
/*                      hw doesn't support port based authentication, then   */
/*                      this function returns failure.                       */
/*                                                                           */
/* Input(s)           : u2PortNun - IfIndex of the port.                     */
/*                                                                           */
/* Output(s)          : pu2HwPortAuthStatus - Hw Port auth status.           */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE.                         */
/*****************************************************************************/
INT4
PnacRedGetHwAuthStatus (UINT2 u2PortNum, UINT1 u1AuthMode,
                        UINT2 *pu2HwPortAuthStatus, UINT2 *pu2CtrlDir)
{
    INT4                i4RetVal = FNP_FAILURE;
    /* For boards that doesn't have port authorization support,  just return
     * FNP_FAILURE. 
     * For boards that support port authorization, get the authorizaton
     * status of port from hw and return.*/
    i4RetVal = PnacPnacHwGetAuthStatus (u2PortNum, NULL, u1AuthMode,
                                        pu2HwPortAuthStatus, pu2CtrlDir);
    if (i4RetVal == FNP_SUCCESS)
    {
        return PNAC_SUCCESS;
    }
    else
    {
        return PNAC_FAILURE;
    }
}
#endif

/*****************************************************************************/
/* Function Name      : PnacRedHandleRmMsg.                                  */
/*                                                                           */
/* Description        : This function  based on the RM event present in the  */
/*                      given input buffer, takes proper action.             */
/*                                                                           */
/* Input(s)           : pMesg - Pointer to the structure containing RM buffer*/
/*                              and the RM event.                            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSystemInfo.NodeStatus.                          */
/*                      gPnacRedSystemInfo.u1NumPeersUp.                     */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedHandleRmMsg (tPnacIntfMesg * pMesg)
{
    tRmNodeInfo        *pData = NULL;
    tRmProtoEvt         ProtoEvt;
    tPnacNodeStatus     PnacPrevNodeState = PNAC_IDLE_NODE;

    ProtoEvt.u4AppId = RM_PNAC_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    switch (pMesg->Mesg.RmPnacMsg.u1SubMsgType)
    {
        case RM_MESSAGE:
            PNAC_TRC (PNAC_RED_TRC, "RED: Rcvd VALID MESSAGE from RM.\n");

            if (pMesg->Mesg.RmPnacMsg.pData != NULL)
            {
                PnacRedHandleSyncUpData (pMesg->Mesg.RmPnacMsg.pData,
                                         pMesg->Mesg.RmPnacMsg.u2DataLen);

                /* Processing of message is over, hence free the RM message. */
                RM_FREE ((tRmMsg *) (pMesg->Mesg.RmPnacMsg.pData));
            }
            else
            {
                PNAC_TRC (PNAC_RED_TRC, "RED: Rcvd VALID MESSAGE from RM."
                          "But the pData given from RM is NULL.\n");
            }

            break;

        case GO_ACTIVE:
            if (PNAC_NODE_STATUS () == PNAC_ACTIVE_NODE)
            {
                break;
            }
            PnacPrevNodeState = PNAC_NODE_STATUS ();

            /* Expecting no data field and hence no need to do RM_FREE. */
            PNAC_TRC (PNAC_RED_TRC, "RED: Rcvd GO ACTIVE from RM.\n");
            PnacRedMakeNodeActive ();
            if (PnacPrevNodeState == PNAC_IDLE_NODE)
            {
                ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
            }
            else
            {
                ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
            }
            PnacRmHandleProtocolEvent (&ProtoEvt);
            break;

        case GO_STANDBY:
            /* Expecting no data field and hence no need to do RM_FREE. */
            PNAC_TRC (PNAC_RED_TRC, "RED: Rcvd GO STANDBY from RM.\n");
            if (PNAC_NODE_STATUS () == PNAC_STANDBY_NODE)
            {
                break;
            }

            if (PNAC_NODE_STATUS () == PNAC_IDLE_NODE)
            {
                return;
            }
            else if (PNAC_NODE_STATUS () == PNAC_ACTIVE_NODE)
            {
                PnacRedHandleGoStandbyEvent ();
                ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                PnacRmHandleProtocolEvent (&ProtoEvt);
            }
            PNAC_BULK_REQ_RECD () = PNAC_FALSE;
            break;

        case RM_INIT_HW_AUDIT:
#ifdef NPAPI_WANTED
            if (PNAC_NODE_STATUS () == PNAC_ACTIVE_NODE)
            {
                PnacRedPerformAudit ();
            }
#endif
            break;

        case RM_CONFIG_RESTORE_COMPLETE:
            /* Expecting no data field and hence no need to do RM_FREE. */
            PNAC_TRC (PNAC_RED_TRC, "RED: Rcvd Config restore "
                      "complete from RM.\n");
            if (PNAC_NODE_STATUS () == PNAC_IDLE_NODE)
            {
                if (PnacRmGetNodeState () == RM_STANDBY)
                {
                    PnacRedMakeNodeStandbyFromIdle ();
                }

                ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                PnacRmHandleProtocolEvent (&ProtoEvt);
            }
            break;

        case RM_STANDBY_UP:
            PNAC_TRC (PNAC_RED_TRC, "RED: Standby Up event from RM\n");
            pData = (tRmNodeInfo *) pMesg->Mesg.RmPnacMsg.pData;
            PNAC_NUM_STANDBY_NODES () = pData->u1NumStandby;
            PnacRmReleaseMemoryForMsg ((UINT1 *) pData);
            PnacRedHandleStandByUpEvent (NULL);
            break;

        case RM_STANDBY_DOWN:
            PNAC_TRC (PNAC_RED_TRC, "RED: Standby Down event from RM\n");
            pData = (tRmNodeInfo *) pMesg->Mesg.RmPnacMsg.pData;
            PNAC_NUM_STANDBY_NODES () = pData->u1NumStandby;
            PnacRmReleaseMemoryForMsg ((UINT1 *) pData);
            break;

        case RM_COMPLETE_SYNC_UP:
#if !defined (CFA_WANTED) && !defined (EOAM_WANTED)
            PnacRedTrigHigherLayer (L2_COMPLETE_SYNC_UP);
#endif
            break;

        case L2_COMPLETE_SYNC_UP:
            PnacRedTrigHigherLayer (L2_COMPLETE_SYNC_UP);
            break;

        case L2_INITIATE_BULK_UPDATES:
            PNAC_TRC (PNAC_RED_TRC, "Receieved L2_INITIATE_BULK_UPDATES event "
                      "from RM\n");
            PnacRedSendBulkReq ();
            break;

        default:
            /* Expecting no data field and hence no need to do RM_FREE. */
            PNAC_TRC (PNAC_RED_TRC | PNAC_RED_TRC, "RED: Rcvd "
                      "unknown msg from RM.\n");
            break;

    }
}

/*****************************************************************************/
/* Function Name      : PnacRedHandleBulkUpdEvent.                           */
/*                                                                           */
/* Description        : It Handles the bulk update event. This event is used */
/*                      to start the next sub bulk update. So                */
/*                      PnacRedHandlePortBasedBulkReqEvent is triggered.     */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gPnacSystemInfo.NodeStatus.                          */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PnacRedHandleBulkUpdEvent (VOID)
{
    if (PNAC_NODE_STATUS () == PNAC_ACTIVE_NODE)
    {
        PnacRedHandlePortBasedBulkReqEvent (NULL);

        /* Once Mac based redundancy is implemented, then use the
         * following prototype to sync mac based authentications.  
         * PnacRedHandleMacBasedBulkReqEvent (pvPeerId);
         */
    }
    else
    {
        /* Only Active node can process bulk request and can
         * send the bulk update.*/
        PNAC_BULK_REQ_RECD () = PNAC_TRUE;
        return;
    }
}

/*****************************************************************************/
/* Function Name      : PnacRedSendBulkUpdateTailMsg                         */
/*                                                                           */
/* Description        : This function will send the tail msg to the standy   */
/*                      node, which indicates the completion of Bulk update  */
/*                      process.                                             */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE.                         */
/*****************************************************************************/
INT4
PnacRedSendBulkUpdateTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = PNAC_RM_OFFSET;
    UINT2               u2BufSize;

    ProtoEvt.u4AppId = RM_PNAC_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (PNAC_NODE_STATUS () != PNAC_ACTIVE_NODE)
    {
        PNAC_TRC (PNAC_RED_TRC, "PNAC: Node is not active."
                  "Bulk update tail msg not sent.\n");
        return PNAC_SUCCESS;
    }

    /* Form a bulk update tail message. 

     *        <---------4 Bytes--------><---2 Bytes-->
     ************************************************* 
     *        *                        *             *
     * RM Hdr * PNAC_BULK_UPD_TAIL_MSG * Msg Length  *
     *        *                        *             *
     *************************************************

     * The RM Hdr shall be included by RM.
     */

    u2BufSize = PNAC_RM_MSG_TYPE_SIZE + PNAC_RM_LEN_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        PNAC_TRC (ALL_FAILURE_TRC, "Rm alloc failed\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        PnacRmHandleProtocolEvent (&ProtoEvt);
        return (PNAC_FAILURE);
    }

    /* Fill the message type. */
    PNAC_RM_PUT_4_BYTE (pMsg, &u4Offset, PNAC_BULK_UPD_TAIL_MSG);
    /* This length field will not be used. It is just to keep the
     * this message structure similar to that of other sync msgs. */
    PNAC_RM_PUT_2_BYTE (pMsg, &u4Offset, u2BufSize);
    if (PnacRedSendMsgToRm (pMsg, u2BufSize) == PNAC_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        PnacRmHandleProtocolEvent (&ProtoEvt);
    }

    return PNAC_SUCCESS;
}

#endif /* _PNACRED_C_ */
