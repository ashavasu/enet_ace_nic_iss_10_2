/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/* $Id: pnachdrs.h,v 1.29 2014/07/21 11:35:35 siva Exp $                   */
/*****************************************************************************/
/*    FILE  NAME            : pnachdrs.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : PNAC module header files list                  */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Mar 2002                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file includes all header files            */
/*                            used in the PNAC module.                       */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 MAR 2002 / BridgeTeam   Initial Create.                     */
/*---------------------------------------------------------------------------*/

#ifndef _PNACHDRS_H
#define _PNACHDRS_H

#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "msr.h"
#include "rmgr.h"
#include "la.h"
#include "utilipvx.h"
#include "radius.h"

#include "snmccons.h"
#include "fssnmp.h" 

#include "vcm.h"

#include "bridge.h"
#include "fsvlan.h"
#ifdef PBB_WANTED
#include "pbb.h"
#endif
#include "pnac.h"
#include "l2iwf.h"
#ifdef WLAN_WANTED
#include "wlan.h"
#endif

#ifdef MBSM_WANTED
#include "mbsm.h"
#endif

#ifdef TACACS_WANTED
#include "tpcli.h"
#include "tacacs.h"
#endif

#include "pnaccons.h"
#include "pnacmacr.h"
#include "pnactdfs.h"
#include "pnacextn.h"
#include "pnacsrv.h"
#include "pnactsk.h"
#include "pnacsz.h"
#include "pnacprot.h"
#include "pnacsem.h"
#include "pnacplow.h"
#include "pnacslow.h"
#include "pnactrc.h"

#ifdef L2RED_WANTED
#include "pnacred.h"
#else
#include "pnacrdsb.h"
#endif

#include "pnacglob.h"
#ifdef NPAPI_WANTED
#include "npapi.h"
#include "pnacnp.h"
#include "issnp.h"
#include "nputil.h"
#endif
#include "pnaccli.h"
#include "fspnacwr.h"
#include "stdpnawr.h"

#include "fssyslog.h"
#include "fsutil.h"

#ifdef WLC_WANTED
#include "wsswlan.h"
#endif

#include "icch.h"

#endif /* _PNACHDRS_H */
