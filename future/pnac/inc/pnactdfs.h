/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                 */
/* $Id: pnactdfs.h,v 1.30 2016/01/22 11:25:39 siva Exp $ */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : pnactdfs.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : PNAC module PAE sub module                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Mar 2002                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains the type definitions and    */
/*                            enumerated data structures in PNAC module.     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 MAR 2002 / BridgeTeam   Initial Create.                     */
/* 1.0.0.1    22 APR 2002 / BridgeTeam   Changed for incorporating Radius    */
/*                                       included state attribute variables  */
/*                                       in Authenticator Session Node.      */
/*---------------------------------------------------------------------------*/

#ifndef _PNACTDFS_H
#define _PNACTDFS_H

typedef tRBTree             tPnacRBTree;
typedef tRBNodeEmbd         tPnacRBNodeEmbd;

#define tPnacSLL tTMO_SLL
#define tPnacSLLNode tTMO_SLL_NODE

/* MAC Address type */

typedef struct PnacUint8 {
    UINT4 u4HiWord;
    UINT4 u4LoWord;
}tPnacUint8; 


/* Key Information */

typedef struct PnacKeyInfo {
    UINT2        u2KeyLength;
    UINT1  u1KeyIndex;
    UINT1        u1Reserved;
    UINT1        au1Key[PNAC_MAX_SERVER_KEY_SIZE];
}tPnacKeyInfo;


/* Timer Information */

typedef struct PnacTimerNode {
   tPnacAppTimer appTimer;
   struct PnacAuthSessionNode *pAuthSessNode;
   struct PnacServAuthCache   *pCacheNode;
   UINT2        u2SessionId;
   UINT2        u2PortNo;
   UINT1        u1TimerType;
   UINT1          u1Reserved[3];
}tPnacTimerNode;

/* PNAC Authenticator Configuration Information */

typedef struct PnacAuthConfigEntry {
   UINT4     u4QuietPeriod;
   UINT4     u4TxPeriod;
   UINT4     u4SuppTimeout;
   UINT4     u4ServerTimeout;
   UINT4     u4ReAuthPeriod;
   UINT4     u4ReAuthMax;
   UINT4     u4MaxReq;
   UINT2     u2AdminControlDir;
   UINT2     u2OperControlDir;
   UINT2     u2AuthControlPortControl;
   BOOL1     bReAuthEnabled;
   BOOL1     bKeyTxEnabled;
}tPnacAuthConfigEntry;


/* PNAC Authenticator FSM  Information */

typedef struct PnacAuthFsmInfo {
   UINT4    u4ReAuthCount;
   UINT4    u4ReqCount;
   UINT4    u4SessReAuthPeriod;
   UINT2    u2CurrentId;        /* used by Authenticator to identify its current 
                                   authentication session */
   UINT2    u2AuthPaeState;
   UINT2    u2AuthBackendAuthState;
   UINT2    u2PortMode;         /* if AUTO or FORCE AUTHORIZED or 
                                   FORCE UNAUTHORIZED */
   UINT2    u2IdFromServer;
   UINT2    u2AuthControlPortStatus; /* if AUTHORIZED    or UNAUTHORIZED    */
   BOOL1      bInitialize;         /* when set TRUE forces all EAPOL state 
                                    machines to their initial state */ 
   BOOL1      bAuthStart;
   BOOL1      bAuthAbort;
   BOOL1      bAuthSuccess;
   BOOL1      bAuthFail;
   BOOL1      bAuthTimeout;
   BOOL1      bKeyAvailable;
   BOOL1      bEapLogoff;
   BOOL1      bEapStart;
   BOOL1      bRxRespId;
   BOOL1      bRxResp;
   BOOL1      bASuccess;
   BOOL1      bAFail;
   BOOL1      bAreq;
   BOOL1      bReAuthenticate;
   BOOL1      bPrevEapLogoff;
   BOOL1      bMacEapLogoff;
   BOOL1      bKeyRun;
   BOOL1      bKeyDone; 
   UINT1      u1Reserved;
}tPnacAuthFsmInfo; 


/* PNAC Authenticator Authentication Statistics Information */

typedef struct PnacAuthStatsEntry {
    tMacAddr    lastEapolFrameSource;
    UINT1    u1Reserved[2];
    UINT4    u4EapolFramesRx;
    UINT4    u4EapolFramesTx;
    UINT4    u4EapolStartFramesRx;
    UINT4    u4EapolLogoffFramesRx;
    UINT4    u4EapolRespIdFramesRx;
    UINT4    u4EapolRespFramesRx;
    UINT4    u4EapolReqIdFramesTx;
    UINT4    u4EapolReqFramesTx;
    UINT4    u4InvalidEapolFramesRx;
    UINT4    u4EapLengthErrorFramesRx;
    UINT4    u4LastEapolFrameVersion;
} tPnacAuthStatsEntry;

/* PNAC Authenticator Session Statistics Information */

typedef struct PnacAuthSessStats {
 tPnacUint8  u8SessOctetsRx;
 tPnacUint8 u8SessOctetsTx;
 UINT4       u4SessFramesRx;
 UINT4       u4SessFramesTx;
 UINT4       u4SessTime;
 UINT4       u4SessTerminateCause;
 UINT2       u2SessUserNameLen;
 UINT2     u2SessIdLen;
 UINT1       au1SessUserName[PNAC_USER_NAME_SIZE];
 UINT1     aSessId[PNAC_SESSION_ID_STRING_SIZE];
} tPnacAuthSessStats;



/* PNAC Authenticator Session Node Information */

typedef struct PnacAuthSessionNode {
    tPnacHashNode     nextAuthSessionNode;
    tPnacAuthFsmInfo       authFsmInfo;
    tPnacAuthSessStats     authSessStats; 
    tPnacKeyInfo           keyInfo;
    tPnacTimerNode         *pAWhileTimer;
    tPnacTimerNode         *pQuietWhileTimer;
    tPnacTimerNode         *pReAuthWhenTimer;
    tPnacTimerNode         *pTxWhenTimer;
    tPnacCruBufChainHdr    *pReqMesg;
    VOID                   *pRsnaSessionNode;      
    UINT2                  u2PortNo;
    UINT2                  u2RadAttrStateLen;
    UINT2                  u2PreAuthPhyPort; /* Physical Port Num for PreAuth
      Session. Used for PreAuthSess
      Only */
    UINT1                  u1PnacAsyncNpRetryCount;
    UINT1                  u1Pad;            /* Alignment Purpose */
    UINT1                  au1RadAttrState[PNAC_MAX_STATE_ATTRIB_LEN+1];
    tMacAddr               rmtSuppMacAddr;
    UINT1        u1IsAuthorizedOnce; /* if the node is
                                      * authorized atleast
                                      * once from initialisation */
#ifdef WPS_WANTED
    BOOL1                  bIsWpsPreAuthSess;      
    UINT1                  au1Pad[3];
#endif
    BOOL1                  bIsRsnPreAuthSess;      /* Flag to indicate if the
                                                   session belongs to 
                                                   preauth */

}tPnacAuthSessionNode;

/* PNAC Auth Session NP Deletion Failed Mac Address Information */
typedef struct _PnacNpFailedMacSessNode {
 tRBNodeEmbd            nextNpFailedMacSessNode;
 tMacAddr               rmtSuppMacAddr;
 UINT1                  u1PnacAsyncNpRetryCount;
 UINT1                  u1Pad;
}tPnacNpFailedMacSessNode;


/* PNAC Session Id Table */

typedef struct PnacAuthSessionId {
 tPnacAuthSessionNode *pNode;
 UINT2            u2PortNo;
 UINT2            u2Reserved;
}tPnacAuthSessionId;



/* PNAC Supplicant FSM Information */

typedef struct PnacSuppFsmInfo {
 UINT4   u4StartCount;
 UINT2       u2SuppPaeState;
 UINT2   u2ReceivedId; /* session id received by the Supplicant, 
                                from the Authenticator, for the 
                                current authentication session */
 UINT2   u2PreviousId;
 UINT2      u2PortMode;   /*whether it is FORCE-AUTHORIZED,AUTO or 
                              FORCE-UNAUTHORIZED*/
 UINT2      u2ControlPortStatus; /*authorized or unauthorized*/
 BOOL1      bSuppKeyAvailable;
 BOOL1      bUserLogoff;
 BOOL1      bLogoffSent;
 BOOL1      bReqId;
 BOOL1      bReqAuth;
 BOOL1      bEapSuccess;
 BOOL1      bEapFail;
   UINT1      au1Reserved[3];
}tPnacSuppFsmInfo;      


/* Supplicant Configuration Information */

typedef struct PnacSuppConfigEntry {
 UINT4     u4AuthPeriod;
 UINT4     u4HeldPeriod;
 UINT4     u4StartPeriod;
 UINT4     u4MaxStart; 
 UINT2       u2SuppAccessCtrlWithAuth; /*active or inactive*/
 BOOL1       bKeyTxEnabled;
 UINT1       au1Reserved[1];
} tPnacSuppConfigEntry;



/* Supplicant Authentication Statistics Information */

typedef struct PnacSuppStatsEntry {
    tMacAddr    lastEapolFrameSource ;
    UINT1    u1Reserved[2];
    UINT4    u4EapolFramesRx ;
    UINT4    u4EapolFramesTx ;
    UINT4    u4EapolStartFramesTx ;
    UINT4    u4EapolLogoffFramesTx ;
    UINT4    u4EapolRespIdFramesTx ;
    UINT4    u4EapolRespFramesTx ;
    UINT4    u4EapolReqIdFramesRx ;
    UINT4    u4EapolReqFramesRx ; 
    UINT4    u4InvalidEapolFramesRx ;
    UINT4    u4EapLengthErrorFramesRx ;
    UINT4    u4LastEapolFrameVersion;
}tPnacSuppStatsEntry;

/* PNAC Supplicant Session Node */

typedef struct PnacSuppSessionNode  {
 tPnacSuppFsmInfo    suppFsmInfo; 
 tPnacKeyInfo     suppKeyInfo;
 tPnacTimerNode   *pAuthWhileTimer;
 tPnacTimerNode   *pHeldWhileTimer;
 tPnacTimerNode   *pStartWhenTimer;
 UINT2          u2PortNo;
 UINT2          u2UserNameLen;
 UINT2          u2PasswdLen;
 UINT2                u2Reserved;
 UINT1          au1UserName[PNAC_USER_NAME_SIZE];
 UINT1          au1Passwd[PNAC_SUPP_PASSWD_SIZE]; 
} tPnacSuppSessionNode; 


/* Port - Interface Index Mapping Information */
typedef struct {
         UINT2     u2IfIndex;
         UINT1     au1MacAddr[PNAC_MAC_ADDR_SIZE]; 
         UINT1     u1IfType;
         UINT1     u1Reserved[3];
}tPortInterfaceMap;

/* PNAC Pae Port Entry */

typedef struct PnacPaePortEntry {
 tPnacAuthConfigEntry  authConfigEntry;
 tPnacAuthStatsEntry authStatsEntry;
 tPnacSuppConfigEntry    suppConfigEntry;
        tPnacSuppStatsEntry     suppStatsEntry;
   tPortInterfaceMap       portIfMap;  
 tPnacAuthSessionNode *pPortAuthSessionNode;
 tPnacSuppSessionNode    *pPortSuppSessionNode;
 VOID                    *pRsnaPaePortInfo;
 UINT4          u4ProtocolVersion;
 UINT2          u2CurrentVlanId;
 UINT2           u2PortNo;
 UINT2          u2SuppCount;
 UINT2          u2SuppSessionCount; /* Maintains the number of sessions created in
                                              mac-based authentication */

 UINT2           u2PortStatus; /*Indicates cumulative port status if
                                         access control is active else only 
                                         authenticator port status*/
 UINT1          u1EntryStatus;
 UINT1          u1Capabilities;
 UINT1          u1PortAuthMode;
 UINT1          u1CtrlDirState;
 UINT1          u1PortPaeStatus; /* Maintains the dot1x enable/disable status of the port */
 UINT1                   u1PnacAsyncNpRetryCount; /* Pnac Async NP Retry Count */
 BOOL1                   bBridgeDetected;
 BOOL1                   bSendTagged;
 BOOL1                   u1PortCtrlChanged; /* When port control changed from one
                                               to different this is set TRUE */
 UINT1                   au1Pad[1];
    }tPnacPaePortEntry;

typedef UINT4 tPnacNodeStatus;

/* PNAC System Information */

typedef struct PnacSystemInfo {
    tPnacSLL        PnacSlotInfo;
 tPnacPaePortEntry  aPnacPaePortInfo[PNAC_MAXPORT_INFO];
 tPnacMemPoolId     authSessNodeMemPoolId;
    tPnacMemPoolId      suppSessNodeMemPoolId;
    tPnacMemPoolId      tmrMemPoolId;
    tPnacMemPoolId      npFailedMacAddrMemPoolId;
    tPnacMemPoolId      npNotifyOidMemPoolId; 
    tPnacMemPoolId      enetEapArrayMemPoolId; 
    tPnacMemPoolId      pnacAsIfRecvDataMemPoolId;
    tPnacMemPoolId      pnacAsIfSendDataMemPoolId;
    tPnacMemPoolId      userInfoMemPoolId;
    tPnacMemPoolId      PnacDPnacPortsInfoMemPoolId;
    tPnacMemPoolId      PnacDPnacSlotsInfoMemPoolId;
    tPnacTmrListId     tmrListId;
    tPnacAuthSessionNode    *apAuthInProgress [PNAC_MAX_SESS_IN_PROGRESS];
    tPnacHashTable  *pPnacAuthSessionTable;

    tPnacTimerNode     *DPnacPeriodicSyncTmr;
                           /* Timer entry for Global D-PNAC periodic timer which
                            * used to control the Global D-PNAC periodic sync
                            * PDU's trasnmission interval
                            */
    tRBTree             PnacNpFailedMacSess;
    UINT4        u4TraceOption;
    UINT4                 u4DPnacPeriodicSyncTime;
                       /* Global Periodic sync time */
    UINT4                  u4DPnacMaxAliveCount;
    INT4                i4SysLogId;
    UINT2       u2SystemAuthControl;
    UINT2               u2NasIdLen;
    UINT1               au1NasId [PNAC_MAX_NAS_ID_SIZE];
    UINT1             u1AuthenticMethod;
    UINT1             u1RemoteAuthServerType;
    UINT1               u1ProtocolAdminStatus;
    UINT1               u1PnacCallSequence;         
 UINT1    u1DPnacRolePlayed;
 UINT1    u1SystemMode;
    UINT1     au1Reserved[2];
                     /* PNAC System mode either Centralized or Distributed */
}tgPnacSystemInfo;

/* Entry creation is based on Slot Identifier in 
   Master Node for which SLL used. Assumption 
   is Slot Identifier is not scaled to larger number.
*/
typedef struct PnacDpnacSlotInfo {
    tPnacSLLNode  NextNode;
    tPnacRBTree   PnacDPnacPortList;
    UINT4         u4SlotId;
    UINT4         u4KeepAliveCount;
    UINT4         u4EventUpdateFramesRx;
    UINT4         u4EventUpdateFramesTx;
    UINT4         u4PeriodicFramesRx;
    UINT4         u4PeriodicFramesTx;

} tPnacDPnacSlotInfo;

typedef struct PnacDPnacConsPortEntry {

    tPnacRBNodeEmbd       PnacDPnacConsPortInfo;
                            /* RB tree Node for Port Entry*/
 UINT2    u2PortIndex;   /* Port Index */

    UINT1               u1LocalOrRemote;

    UINT1               u1PortAuthStatus;         /* Port authentication status */

    UINT1               u1CtrlDirection;         /* Port control direction */

    UINT1     au1Reserved[3];

} tPnacDPnacConsPortEntry;

typedef struct PnacDPnacPktInfo {

    tPnacAsIfRecvData *pSrvEapData; /* Server Data received from Radius */

    UINT1             *pu1EapPktBuf; /* EAP frame to be trasmitted to Master Node */

    UINT2             u2EapPktLen;   /* Length of the EAP frame */

    UINT2             u2NasPortNum;  /* Port Identifier */

    UINT1             u1PktType;    /* Frame Type
                                       1) EAP Request ( For Slave to Master)
                                       2) EAP Response ( For Master to Slave)
                                       3) Periodic Sync ( From Slave to Master 
                                                         for every periodic timer expiry)
                                       4) Event Pdu (From Slave to Master);
                                     */
    UINT1             u1BridgeDetectionStatus;  /* Bridge detection status from STP */
    UINT1             au1Reserved[2];

} tPnacDPnacPktInfo;

/* Other Packet header Informations */

/* EAP Packet Header Information */

typedef struct PnacEapPktHdr {
 UINT2 u2DataLength;
 UINT1 u1Code;
 UINT1 u1Identifier;
 UINT1 *pu1Data;
}tPnacEapPktHdr;


/* Timer Queue Message Information */
typedef struct PnacQMsg {
 UINT4 u4MsgType;
 UINT4 u4DestModId;
 UINT4 u4Data;
}tPnacQMsg;


/* State Machine Entry type definitions */

typedef INT4 (* PNAC_ASMFUNC)(UINT2 , tPnacAuthSessionNode *, UINT1 *, UINT2);
typedef INT4 (* PNAC_BSMFUNC)(UINT2 , tPnacAuthSessionNode *, UINT1 *, UINT2, 
                              tPnacCruBufChainHdr *);
typedef INT4 (* PNAC_SSMFUNC)(UINT2 , tPnacSuppSessionNode *, UINT1 *, UINT2);

/* Authenticator State Machine Structure */

typedef struct PnacAsmEntry {
   PNAC_ASMFUNC pAction;
} tPnacAsmEntry;

/* Back End State Machine Structure */
typedef struct PnacBsmEntry {
   PNAC_BSMFUNC pAction;
} tPnacBsmEntry;

/* Supplicant State Machine Structure */
typedef struct PnacSsmEntry {
   PNAC_SSMFUNC pAction;
} tPnacSsmEntry;

typedef struct _PnacNpTrapLogInfo {
  tMacAddr SuppMacAddr;
  UINT2 u2Port;
  UINT4 u4NpCallId;
  UINT1 u1PnacPortCtrlDir;
  UINT1 u1PnacAuthMode;
  UINT1 u1PnacAuthStatus;
  UINT1 u1Reserved; 
} tPnacNpTrapLogInfo;

/* sizing Params enum. */
enum
{
   PNAC_AUTHSESS_SIZING_ID,
   PNAC_SUPPSESS_SIZING_ID,
   PNAC_AUTHSESS_FAIL_SIZING_ID,
   PNAC_SERVUSER_SIZING_ID,
   PNAC_SERVCACHE_SIZING_ID,
   PNAC_INTFMESG_SIZING_ID,
   PNAC_INTF_CFGQ_SIZING_ID,
   PNAC_MAX_TMR_BLOCK_SIZING_ID,
   PNAC_EAPOL_SIZING_ID,
   PNAC_NO_SIZING_ID
};

#endif /* _PNACTDFS_H */

