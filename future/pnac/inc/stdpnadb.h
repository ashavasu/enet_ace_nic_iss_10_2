/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpnadb.h,v 1.7 2008/08/20 15:23:10 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDPNADB_H
#define _STDPNADB_H

UINT1 Dot1xPaePortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1xAuthConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1xAuthStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1xAuthDiagTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1xAuthSessionStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1xSuppConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1xSuppStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 stdpna [] ={1,0,8802,1,1,1};
tSNMP_OID_TYPE stdpnaOID = {6, stdpna};


UINT4 Dot1xPaeSystemAuthControl [ ] ={1,0,8802,1,1,1,1,1,1};
UINT4 Dot1xPaePortNumber [ ] ={1,0,8802,1,1,1,1,1,2,1,1};
UINT4 Dot1xPaePortProtocolVersion [ ] ={1,0,8802,1,1,1,1,1,2,1,2};
UINT4 Dot1xPaePortCapabilities [ ] ={1,0,8802,1,1,1,1,1,2,1,3};
UINT4 Dot1xPaePortInitialize [ ] ={1,0,8802,1,1,1,1,1,2,1,4};
UINT4 Dot1xPaePortReauthenticate [ ] ={1,0,8802,1,1,1,1,1,2,1,5};
UINT4 Dot1xAuthPaeState [ ] ={1,0,8802,1,1,1,1,2,1,1,1};
UINT4 Dot1xAuthBackendAuthState [ ] ={1,0,8802,1,1,1,1,2,1,1,2};
UINT4 Dot1xAuthAdminControlledDirections [ ] ={1,0,8802,1,1,1,1,2,1,1,3};
UINT4 Dot1xAuthOperControlledDirections [ ] ={1,0,8802,1,1,1,1,2,1,1,4};
UINT4 Dot1xAuthAuthControlledPortStatus [ ] ={1,0,8802,1,1,1,1,2,1,1,5};
UINT4 Dot1xAuthAuthControlledPortControl [ ] ={1,0,8802,1,1,1,1,2,1,1,6};
UINT4 Dot1xAuthQuietPeriod [ ] ={1,0,8802,1,1,1,1,2,1,1,7};
UINT4 Dot1xAuthTxPeriod [ ] ={1,0,8802,1,1,1,1,2,1,1,8};
UINT4 Dot1xAuthSuppTimeout [ ] ={1,0,8802,1,1,1,1,2,1,1,9};
UINT4 Dot1xAuthServerTimeout [ ] ={1,0,8802,1,1,1,1,2,1,1,10};
UINT4 Dot1xAuthMaxReq [ ] ={1,0,8802,1,1,1,1,2,1,1,11};
UINT4 Dot1xAuthReAuthPeriod [ ] ={1,0,8802,1,1,1,1,2,1,1,12};
UINT4 Dot1xAuthReAuthEnabled [ ] ={1,0,8802,1,1,1,1,2,1,1,13};
UINT4 Dot1xAuthKeyTxEnabled [ ] ={1,0,8802,1,1,1,1,2,1,1,14};
UINT4 Dot1xAuthEapolFramesRx [ ] ={1,0,8802,1,1,1,1,2,2,1,1};
UINT4 Dot1xAuthEapolFramesTx [ ] ={1,0,8802,1,1,1,1,2,2,1,2};
UINT4 Dot1xAuthEapolStartFramesRx [ ] ={1,0,8802,1,1,1,1,2,2,1,3};
UINT4 Dot1xAuthEapolLogoffFramesRx [ ] ={1,0,8802,1,1,1,1,2,2,1,4};
UINT4 Dot1xAuthEapolRespIdFramesRx [ ] ={1,0,8802,1,1,1,1,2,2,1,5};
UINT4 Dot1xAuthEapolRespFramesRx [ ] ={1,0,8802,1,1,1,1,2,2,1,6};
UINT4 Dot1xAuthEapolReqIdFramesTx [ ] ={1,0,8802,1,1,1,1,2,2,1,7};
UINT4 Dot1xAuthEapolReqFramesTx [ ] ={1,0,8802,1,1,1,1,2,2,1,8};
UINT4 Dot1xAuthInvalidEapolFramesRx [ ] ={1,0,8802,1,1,1,1,2,2,1,9};
UINT4 Dot1xAuthEapLengthErrorFramesRx [ ] ={1,0,8802,1,1,1,1,2,2,1,10};
UINT4 Dot1xAuthLastEapolFrameVersion [ ] ={1,0,8802,1,1,1,1,2,2,1,11};
UINT4 Dot1xAuthLastEapolFrameSource [ ] ={1,0,8802,1,1,1,1,2,2,1,12};
UINT4 Dot1xAuthEntersConnecting [ ] ={1,0,8802,1,1,1,1,2,3,1,1};
UINT4 Dot1xAuthEapLogoffsWhileConnecting [ ] ={1,0,8802,1,1,1,1,2,3,1,2};
UINT4 Dot1xAuthEntersAuthenticating [ ] ={1,0,8802,1,1,1,1,2,3,1,3};
UINT4 Dot1xAuthAuthSuccessWhileAuthenticating [ ] ={1,0,8802,1,1,1,1,2,3,1,4};
UINT4 Dot1xAuthAuthTimeoutsWhileAuthenticating [ ] ={1,0,8802,1,1,1,1,2,3,1,5};
UINT4 Dot1xAuthAuthFailWhileAuthenticating [ ] ={1,0,8802,1,1,1,1,2,3,1,6};
UINT4 Dot1xAuthAuthReauthsWhileAuthenticating [ ] ={1,0,8802,1,1,1,1,2,3,1,7};
UINT4 Dot1xAuthAuthEapStartsWhileAuthenticating [ ] ={1,0,8802,1,1,1,1,2,3,1,8};
UINT4 Dot1xAuthAuthEapLogoffWhileAuthenticating [ ] ={1,0,8802,1,1,1,1,2,3,1,9};
UINT4 Dot1xAuthAuthReauthsWhileAuthenticated [ ] ={1,0,8802,1,1,1,1,2,3,1,10};
UINT4 Dot1xAuthAuthEapStartsWhileAuthenticated [ ] ={1,0,8802,1,1,1,1,2,3,1,11};
UINT4 Dot1xAuthAuthEapLogoffWhileAuthenticated [ ] ={1,0,8802,1,1,1,1,2,3,1,12};
UINT4 Dot1xAuthBackendResponses [ ] ={1,0,8802,1,1,1,1,2,3,1,13};
UINT4 Dot1xAuthBackendAccessChallenges [ ] ={1,0,8802,1,1,1,1,2,3,1,14};
UINT4 Dot1xAuthBackendOtherRequestsToSupplicant [ ] ={1,0,8802,1,1,1,1,2,3,1,15};
UINT4 Dot1xAuthBackendNonNakResponsesFromSupplicant [ ] ={1,0,8802,1,1,1,1,2,3,1,16};
UINT4 Dot1xAuthBackendAuthSuccesses [ ] ={1,0,8802,1,1,1,1,2,3,1,17};
UINT4 Dot1xAuthBackendAuthFails [ ] ={1,0,8802,1,1,1,1,2,3,1,18};
UINT4 Dot1xAuthSessionOctetsRx [ ] ={1,0,8802,1,1,1,1,2,4,1,1};
UINT4 Dot1xAuthSessionOctetsTx [ ] ={1,0,8802,1,1,1,1,2,4,1,2};
UINT4 Dot1xAuthSessionFramesRx [ ] ={1,0,8802,1,1,1,1,2,4,1,3};
UINT4 Dot1xAuthSessionFramesTx [ ] ={1,0,8802,1,1,1,1,2,4,1,4};
UINT4 Dot1xAuthSessionId [ ] ={1,0,8802,1,1,1,1,2,4,1,5};
UINT4 Dot1xAuthSessionAuthenticMethod [ ] ={1,0,8802,1,1,1,1,2,4,1,6};
UINT4 Dot1xAuthSessionTime [ ] ={1,0,8802,1,1,1,1,2,4,1,7};
UINT4 Dot1xAuthSessionTerminateCause [ ] ={1,0,8802,1,1,1,1,2,4,1,8};
UINT4 Dot1xAuthSessionUserName [ ] ={1,0,8802,1,1,1,1,2,4,1,9};
UINT4 Dot1xSuppPaeState [ ] ={1,0,8802,1,1,1,1,3,1,1,1};
UINT4 Dot1xSuppHeldPeriod [ ] ={1,0,8802,1,1,1,1,3,1,1,2};
UINT4 Dot1xSuppAuthPeriod [ ] ={1,0,8802,1,1,1,1,3,1,1,3};
UINT4 Dot1xSuppStartPeriod [ ] ={1,0,8802,1,1,1,1,3,1,1,4};
UINT4 Dot1xSuppMaxStart [ ] ={1,0,8802,1,1,1,1,3,1,1,5};
UINT4 Dot1xSuppControlledPortStatus [ ] ={1,0,8802,1,1,1,1,3,1,1,6};
UINT4 Dot1xSuppAccessCtrlWithAuth [ ] ={1,0,8802,1,1,1,1,3,1,1,7};
UINT4 Dot1xSuppEapolFramesRx [ ] ={1,0,8802,1,1,1,1,3,2,1,1};
UINT4 Dot1xSuppEapolFramesTx [ ] ={1,0,8802,1,1,1,1,3,2,1,2};
UINT4 Dot1xSuppEapolStartFramesTx [ ] ={1,0,8802,1,1,1,1,3,2,1,3};
UINT4 Dot1xSuppEapolLogoffFramesTx [ ] ={1,0,8802,1,1,1,1,3,2,1,4};
UINT4 Dot1xSuppEapolRespIdFramesTx [ ] ={1,0,8802,1,1,1,1,3,2,1,5};
UINT4 Dot1xSuppEapolRespFramesTx [ ] ={1,0,8802,1,1,1,1,3,2,1,6};
UINT4 Dot1xSuppEapolReqIdFramesRx [ ] ={1,0,8802,1,1,1,1,3,2,1,7};
UINT4 Dot1xSuppEapolReqFramesRx [ ] ={1,0,8802,1,1,1,1,3,2,1,8};
UINT4 Dot1xSuppInvalidEapolFramesRx [ ] ={1,0,8802,1,1,1,1,3,2,1,9};
UINT4 Dot1xSuppEapLengthErrorFramesRx [ ] ={1,0,8802,1,1,1,1,3,2,1,10};
UINT4 Dot1xSuppLastEapolFrameVersion [ ] ={1,0,8802,1,1,1,1,3,2,1,11};
UINT4 Dot1xSuppLastEapolFrameSource [ ] ={1,0,8802,1,1,1,1,3,2,1,12};


tMbDbEntry stdpnaMibEntry[]= {

{{9,Dot1xPaeSystemAuthControl}, NULL, Dot1xPaeSystemAuthControlGet, Dot1xPaeSystemAuthControlSet, Dot1xPaeSystemAuthControlTest, Dot1xPaeSystemAuthControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,Dot1xPaePortNumber}, GetNextIndexDot1xPaePortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1xPaePortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xPaePortProtocolVersion}, GetNextIndexDot1xPaePortTable, Dot1xPaePortProtocolVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot1xPaePortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xPaePortCapabilities}, GetNextIndexDot1xPaePortTable, Dot1xPaePortCapabilitiesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1xPaePortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xPaePortInitialize}, GetNextIndexDot1xPaePortTable, Dot1xPaePortInitializeGet, Dot1xPaePortInitializeSet, Dot1xPaePortInitializeTest, Dot1xPaePortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1xPaePortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xPaePortReauthenticate}, GetNextIndexDot1xPaePortTable, Dot1xPaePortReauthenticateGet, Dot1xPaePortReauthenticateSet, Dot1xPaePortReauthenticateTest, Dot1xPaePortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1xPaePortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthPaeState}, GetNextIndexDot1xAuthConfigTable, Dot1xAuthPaeStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1xAuthConfigTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthBackendAuthState}, GetNextIndexDot1xAuthConfigTable, Dot1xAuthBackendAuthStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1xAuthConfigTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthAdminControlledDirections}, GetNextIndexDot1xAuthConfigTable, Dot1xAuthAdminControlledDirectionsGet, Dot1xAuthAdminControlledDirectionsSet, Dot1xAuthAdminControlledDirectionsTest, Dot1xAuthConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1xAuthConfigTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthOperControlledDirections}, GetNextIndexDot1xAuthConfigTable, Dot1xAuthOperControlledDirectionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1xAuthConfigTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthAuthControlledPortStatus}, GetNextIndexDot1xAuthConfigTable, Dot1xAuthAuthControlledPortStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1xAuthConfigTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthAuthControlledPortControl}, GetNextIndexDot1xAuthConfigTable, Dot1xAuthAuthControlledPortControlGet, Dot1xAuthAuthControlledPortControlSet, Dot1xAuthAuthControlledPortControlTest, Dot1xAuthConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1xAuthConfigTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthQuietPeriod}, GetNextIndexDot1xAuthConfigTable, Dot1xAuthQuietPeriodGet, Dot1xAuthQuietPeriodSet, Dot1xAuthQuietPeriodTest, Dot1xAuthConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot1xAuthConfigTableINDEX, 1, 0, 0, "60"},

{{11,Dot1xAuthTxPeriod}, GetNextIndexDot1xAuthConfigTable, Dot1xAuthTxPeriodGet, Dot1xAuthTxPeriodSet, Dot1xAuthTxPeriodTest, Dot1xAuthConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot1xAuthConfigTableINDEX, 1, 0, 0, "30"},

{{11,Dot1xAuthSuppTimeout}, GetNextIndexDot1xAuthConfigTable, Dot1xAuthSuppTimeoutGet, Dot1xAuthSuppTimeoutSet, Dot1xAuthSuppTimeoutTest, Dot1xAuthConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot1xAuthConfigTableINDEX, 1, 0, 0, "30"},

{{11,Dot1xAuthServerTimeout}, GetNextIndexDot1xAuthConfigTable, Dot1xAuthServerTimeoutGet, Dot1xAuthServerTimeoutSet, Dot1xAuthServerTimeoutTest, Dot1xAuthConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot1xAuthConfigTableINDEX, 1, 0, 0, "30"},

{{11,Dot1xAuthMaxReq}, GetNextIndexDot1xAuthConfigTable, Dot1xAuthMaxReqGet, Dot1xAuthMaxReqSet, Dot1xAuthMaxReqTest, Dot1xAuthConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot1xAuthConfigTableINDEX, 1, 0, 0, "2"},

{{11,Dot1xAuthReAuthPeriod}, GetNextIndexDot1xAuthConfigTable, Dot1xAuthReAuthPeriodGet, Dot1xAuthReAuthPeriodSet, Dot1xAuthReAuthPeriodTest, Dot1xAuthConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot1xAuthConfigTableINDEX, 1, 0, 0, "3600"},

{{11,Dot1xAuthReAuthEnabled}, GetNextIndexDot1xAuthConfigTable, Dot1xAuthReAuthEnabledGet, Dot1xAuthReAuthEnabledSet, Dot1xAuthReAuthEnabledTest, Dot1xAuthConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1xAuthConfigTableINDEX, 1, 0, 0, "2"},

{{11,Dot1xAuthKeyTxEnabled}, GetNextIndexDot1xAuthConfigTable, Dot1xAuthKeyTxEnabledGet, Dot1xAuthKeyTxEnabledSet, Dot1xAuthKeyTxEnabledTest, Dot1xAuthConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1xAuthConfigTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthEapolFramesRx}, GetNextIndexDot1xAuthStatsTable, Dot1xAuthEapolFramesRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthEapolFramesTx}, GetNextIndexDot1xAuthStatsTable, Dot1xAuthEapolFramesTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthEapolStartFramesRx}, GetNextIndexDot1xAuthStatsTable, Dot1xAuthEapolStartFramesRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthEapolLogoffFramesRx}, GetNextIndexDot1xAuthStatsTable, Dot1xAuthEapolLogoffFramesRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthEapolRespIdFramesRx}, GetNextIndexDot1xAuthStatsTable, Dot1xAuthEapolRespIdFramesRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthEapolRespFramesRx}, GetNextIndexDot1xAuthStatsTable, Dot1xAuthEapolRespFramesRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthEapolReqIdFramesTx}, GetNextIndexDot1xAuthStatsTable, Dot1xAuthEapolReqIdFramesTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthEapolReqFramesTx}, GetNextIndexDot1xAuthStatsTable, Dot1xAuthEapolReqFramesTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthInvalidEapolFramesRx}, GetNextIndexDot1xAuthStatsTable, Dot1xAuthInvalidEapolFramesRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthEapLengthErrorFramesRx}, GetNextIndexDot1xAuthStatsTable, Dot1xAuthEapLengthErrorFramesRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthLastEapolFrameVersion}, GetNextIndexDot1xAuthStatsTable, Dot1xAuthLastEapolFrameVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot1xAuthStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthLastEapolFrameSource}, GetNextIndexDot1xAuthStatsTable, Dot1xAuthLastEapolFrameSourceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot1xAuthStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthEntersConnecting}, GetNextIndexDot1xAuthDiagTable, Dot1xAuthEntersConnectingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthDiagTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthEapLogoffsWhileConnecting}, GetNextIndexDot1xAuthDiagTable, Dot1xAuthEapLogoffsWhileConnectingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthDiagTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthEntersAuthenticating}, GetNextIndexDot1xAuthDiagTable, Dot1xAuthEntersAuthenticatingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthDiagTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthAuthSuccessWhileAuthenticating}, GetNextIndexDot1xAuthDiagTable, Dot1xAuthAuthSuccessWhileAuthenticatingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthDiagTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthAuthTimeoutsWhileAuthenticating}, GetNextIndexDot1xAuthDiagTable, Dot1xAuthAuthTimeoutsWhileAuthenticatingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthDiagTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthAuthFailWhileAuthenticating}, GetNextIndexDot1xAuthDiagTable, Dot1xAuthAuthFailWhileAuthenticatingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthDiagTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthAuthReauthsWhileAuthenticating}, GetNextIndexDot1xAuthDiagTable, Dot1xAuthAuthReauthsWhileAuthenticatingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthDiagTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthAuthEapStartsWhileAuthenticating}, GetNextIndexDot1xAuthDiagTable, Dot1xAuthAuthEapStartsWhileAuthenticatingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthDiagTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthAuthEapLogoffWhileAuthenticating}, GetNextIndexDot1xAuthDiagTable, Dot1xAuthAuthEapLogoffWhileAuthenticatingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthDiagTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthAuthReauthsWhileAuthenticated}, GetNextIndexDot1xAuthDiagTable, Dot1xAuthAuthReauthsWhileAuthenticatedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthDiagTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthAuthEapStartsWhileAuthenticated}, GetNextIndexDot1xAuthDiagTable, Dot1xAuthAuthEapStartsWhileAuthenticatedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthDiagTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthAuthEapLogoffWhileAuthenticated}, GetNextIndexDot1xAuthDiagTable, Dot1xAuthAuthEapLogoffWhileAuthenticatedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthDiagTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthBackendResponses}, GetNextIndexDot1xAuthDiagTable, Dot1xAuthBackendResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthDiagTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthBackendAccessChallenges}, GetNextIndexDot1xAuthDiagTable, Dot1xAuthBackendAccessChallengesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthDiagTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthBackendOtherRequestsToSupplicant}, GetNextIndexDot1xAuthDiagTable, Dot1xAuthBackendOtherRequestsToSupplicantGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthDiagTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthBackendNonNakResponsesFromSupplicant}, GetNextIndexDot1xAuthDiagTable, Dot1xAuthBackendNonNakResponsesFromSupplicantGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthDiagTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthBackendAuthSuccesses}, GetNextIndexDot1xAuthDiagTable, Dot1xAuthBackendAuthSuccessesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthDiagTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthBackendAuthFails}, GetNextIndexDot1xAuthDiagTable, Dot1xAuthBackendAuthFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthDiagTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthSessionOctetsRx}, GetNextIndexDot1xAuthSessionStatsTable, Dot1xAuthSessionOctetsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot1xAuthSessionStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthSessionOctetsTx}, GetNextIndexDot1xAuthSessionStatsTable, Dot1xAuthSessionOctetsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot1xAuthSessionStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthSessionFramesRx}, GetNextIndexDot1xAuthSessionStatsTable, Dot1xAuthSessionFramesRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthSessionStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthSessionFramesTx}, GetNextIndexDot1xAuthSessionStatsTable, Dot1xAuthSessionFramesTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xAuthSessionStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthSessionId}, GetNextIndexDot1xAuthSessionStatsTable, Dot1xAuthSessionIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1xAuthSessionStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthSessionAuthenticMethod}, GetNextIndexDot1xAuthSessionStatsTable, Dot1xAuthSessionAuthenticMethodGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1xAuthSessionStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthSessionTime}, GetNextIndexDot1xAuthSessionStatsTable, Dot1xAuthSessionTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Dot1xAuthSessionStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthSessionTerminateCause}, GetNextIndexDot1xAuthSessionStatsTable, Dot1xAuthSessionTerminateCauseGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1xAuthSessionStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xAuthSessionUserName}, GetNextIndexDot1xAuthSessionStatsTable, Dot1xAuthSessionUserNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1xAuthSessionStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xSuppPaeState}, GetNextIndexDot1xSuppConfigTable, Dot1xSuppPaeStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1xSuppConfigTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xSuppHeldPeriod}, GetNextIndexDot1xSuppConfigTable, Dot1xSuppHeldPeriodGet, Dot1xSuppHeldPeriodSet, Dot1xSuppHeldPeriodTest, Dot1xSuppConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot1xSuppConfigTableINDEX, 1, 0, 0, "60"},

{{11,Dot1xSuppAuthPeriod}, GetNextIndexDot1xSuppConfigTable, Dot1xSuppAuthPeriodGet, Dot1xSuppAuthPeriodSet, Dot1xSuppAuthPeriodTest, Dot1xSuppConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot1xSuppConfigTableINDEX, 1, 0, 0, "30"},

{{11,Dot1xSuppStartPeriod}, GetNextIndexDot1xSuppConfigTable, Dot1xSuppStartPeriodGet, Dot1xSuppStartPeriodSet, Dot1xSuppStartPeriodTest, Dot1xSuppConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot1xSuppConfigTableINDEX, 1, 0, 0, "30"},

{{11,Dot1xSuppMaxStart}, GetNextIndexDot1xSuppConfigTable, Dot1xSuppMaxStartGet, Dot1xSuppMaxStartSet, Dot1xSuppMaxStartTest, Dot1xSuppConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot1xSuppConfigTableINDEX, 1, 0, 0, "3"},

{{11,Dot1xSuppControlledPortStatus}, GetNextIndexDot1xSuppConfigTable, Dot1xSuppControlledPortStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1xSuppConfigTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xSuppAccessCtrlWithAuth}, GetNextIndexDot1xSuppConfigTable, Dot1xSuppAccessCtrlWithAuthGet, Dot1xSuppAccessCtrlWithAuthSet, Dot1xSuppAccessCtrlWithAuthTest, Dot1xSuppConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1xSuppConfigTableINDEX, 1, 0, 0, "1"},

{{11,Dot1xSuppEapolFramesRx}, GetNextIndexDot1xSuppStatsTable, Dot1xSuppEapolFramesRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xSuppStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xSuppEapolFramesTx}, GetNextIndexDot1xSuppStatsTable, Dot1xSuppEapolFramesTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xSuppStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xSuppEapolStartFramesTx}, GetNextIndexDot1xSuppStatsTable, Dot1xSuppEapolStartFramesTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xSuppStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xSuppEapolLogoffFramesTx}, GetNextIndexDot1xSuppStatsTable, Dot1xSuppEapolLogoffFramesTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xSuppStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xSuppEapolRespIdFramesTx}, GetNextIndexDot1xSuppStatsTable, Dot1xSuppEapolRespIdFramesTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xSuppStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xSuppEapolRespFramesTx}, GetNextIndexDot1xSuppStatsTable, Dot1xSuppEapolRespFramesTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xSuppStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xSuppEapolReqIdFramesRx}, GetNextIndexDot1xSuppStatsTable, Dot1xSuppEapolReqIdFramesRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xSuppStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xSuppEapolReqFramesRx}, GetNextIndexDot1xSuppStatsTable, Dot1xSuppEapolReqFramesRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xSuppStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xSuppInvalidEapolFramesRx}, GetNextIndexDot1xSuppStatsTable, Dot1xSuppInvalidEapolFramesRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xSuppStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xSuppEapLengthErrorFramesRx}, GetNextIndexDot1xSuppStatsTable, Dot1xSuppEapLengthErrorFramesRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1xSuppStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xSuppLastEapolFrameVersion}, GetNextIndexDot1xSuppStatsTable, Dot1xSuppLastEapolFrameVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot1xSuppStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot1xSuppLastEapolFrameSource}, GetNextIndexDot1xSuppStatsTable, Dot1xSuppLastEapolFrameSourceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot1xSuppStatsTableINDEX, 1, 0, 0, NULL},
};
tMibData stdpnaEntry = { 78, stdpnaMibEntry };
#endif /* _STDPNADB_H */

