/* $Id: fspnacwr.h,v 1.12 2015/06/11 10:03:14 siva Exp $*/

#ifndef _FSPNACWR_H
#define _FSPNACWR_H

VOID RegisterFSPNAC(VOID);

VOID UnRegisterFSPNAC(VOID);
INT4 FsDPnacSystemStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDPnacPeriodicSyncTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsDPnacMaxKeepAliveCountGet(tSnmpIndex *, tRetVal *);
INT4 FsDPnacSystemStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDPnacPeriodicSyncTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsDPnacMaxKeepAliveCountSet(tSnmpIndex *, tRetVal *);
INT4 FsDPnacSystemStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDPnacPeriodicSyncTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDPnacMaxKeepAliveCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDPnacSystemStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsDPnacPeriodicSyncTimeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsDPnacMaxKeepAliveCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsDPnacStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDPnacEventUpdateFramesRxGet(tSnmpIndex *, tRetVal *);
INT4 FsDPnacEventUpdateFramesTxGet(tSnmpIndex *, tRetVal *);
INT4 FsDPnacPeriodicFramesTxGet(tSnmpIndex *, tRetVal *);
INT4 FsDPnacPeriodicFramesRxGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsDPnacSlotPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDPnacPortAuthStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDPnacPortControlledDirectionGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacModuleOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthenticServerGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacNasIdGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsPnacTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthenticServerSet(tSnmpIndex *, tRetVal *);
INT4 FsPnacNasIdSet(tSnmpIndex *, tRetVal *);
INT4 FsPnacSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPnacTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthenticServerTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPnacNasIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPnacSystemControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPnacTraceOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPnacAuthenticServerDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPnacNasIdDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);




INT4 GetNextIndexFsPnacPaePortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPnacPaePortAuthModeGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacPaePortSupplicantCountGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacPaePortUserNameGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacPaePortPasswordGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacPaePortStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacPaePortStatisticsClearGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacPaePortAuthStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacPaeAuthReAuthMaxGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacPaePortAuthModeSet(tSnmpIndex *, tRetVal *);
INT4 FsPnacPaePortUserNameSet(tSnmpIndex *, tRetVal *);
INT4 FsPnacPaePortPasswordSet(tSnmpIndex *, tRetVal *);
INT4 FsPnacPaePortStatisticsClearSet(tSnmpIndex *, tRetVal *);
INT4 FsPnacPaePortAuthStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPnacPaeAuthReAuthMaxSet(tSnmpIndex *, tRetVal *);
INT4 FsPnacPaePortAuthModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPnacPaePortUserNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPnacPaePortPasswordTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPnacPaePortStatisticsClearTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPnacPaePortAuthStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPnacPaeAuthReAuthMaxTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPnacPaePortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 FsPnacRemoteAuthServerTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacRemoteAuthServerTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsPnacRemoteAuthServerTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPnacRemoteAuthServerTypeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 GetNextIndexFsPnacAuthSessionTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPnacAuthSessionIdentifierGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthSessionAuthPaeStateGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthSessionBackendAuthStateGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthSessionPortStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthSessionPortNumberGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthSessionInitializeGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthSessionReauthenticateGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthSessionInitializeSet(tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthSessionReauthenticateSet(tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthSessionInitializeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthSessionReauthenticateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthSessionTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsPnacAuthSessionStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPnacAuthSessionOctetsRxGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthSessionOctetsTxGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthSessionFramesRxGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthSessionFramesTxGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthSessionIdGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthSessionAuthenticMethodGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthSessionTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthSessionTerminateCauseGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthSessionUserNameGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthSessionStatisticsClearGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthSessionStatisticsClearSet(tSnmpIndex *, tRetVal *);
INT4 FsPnacAuthSessionStatisticsClearTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsPnacASUserConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPnacASUserConfigPasswordGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacASUserConfigAuthProtocolGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacASUserConfigAuthTimeoutGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacASUserConfigPortListGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacASUserConfigPermissionGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacASUserConfigRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPnacASUserConfigPasswordSet(tSnmpIndex *, tRetVal *);
INT4 FsPnacASUserConfigAuthTimeoutSet(tSnmpIndex *, tRetVal *);
INT4 FsPnacASUserConfigPortListSet(tSnmpIndex *, tRetVal *);
INT4 FsPnacASUserConfigPermissionSet(tSnmpIndex *, tRetVal *);
INT4 FsPnacASUserConfigRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPnacASUserConfigPasswordTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPnacASUserConfigAuthTimeoutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPnacASUserConfigPortListTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPnacASUserConfigPermissionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPnacASUserConfigRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPnacASUserConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);





INT4 GetNextIndexFsPnacTrapAuthSessionTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPnacTrapAuthSessionStatusGet(tSnmpIndex *, tRetVal *);
#endif /* _FSPNACWR_H */
