/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspnacdb.h,v 1.15 2015/06/11 10:03:14 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSPNACDB_H
#define _FSPNACDB_H

UINT1 FsDPnacStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDPnacSlotPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsPnacPaePortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsPnacAuthSessionTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsPnacAuthSessionStatsTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsPnacASUserConfigTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsPnacTrapAuthSessionTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,6};

UINT4 fspnac [] ={1,3,6,1,4,1,2076,64};
tSNMP_OID_TYPE fspnacOID = {8, fspnac};


UINT4 FsDPnacSystemStatus [ ] ={1,3,6,1,4,1,2076,64,6,1};
UINT4 FsDPnacPeriodicSyncTime [ ] ={1,3,6,1,4,1,2076,64,6,2};
UINT4 FsDPnacMaxKeepAliveCount [ ] ={1,3,6,1,4,1,2076,64,6,3};
UINT4 FsDPnacSlotNumber [ ] ={1,3,6,1,4,1,2076,64,6,4,1,1};
UINT4 FsDPnacEventUpdateFramesRx [ ] ={1,3,6,1,4,1,2076,64,6,4,1,2};
UINT4 FsDPnacEventUpdateFramesTx [ ] ={1,3,6,1,4,1,2076,64,6,4,1,3};
UINT4 FsDPnacPeriodicFramesTx [ ] ={1,3,6,1,4,1,2076,64,6,4,1,4};
UINT4 FsDPnacPeriodicFramesRx [ ] ={1,3,6,1,4,1,2076,64,6,4,1,5};
UINT4 FsDPnacPortIndex [ ] ={1,3,6,1,4,1,2076,64,6,5,1,1};
UINT4 FsDPnacPortAuthStatus [ ] ={1,3,6,1,4,1,2076,64,6,5,1,2};
UINT4 FsDPnacPortControlledDirection [ ] ={1,3,6,1,4,1,2076,64,6,5,1,3};
UINT4 FsPnacSystemControl [ ] ={1,3,6,1,4,1,2076,64,1,1};
UINT4 FsPnacTraceOption [ ] ={1,3,6,1,4,1,2076,64,1,2};
UINT4 FsPnacAuthenticServer [ ] ={1,3,6,1,4,1,2076,64,1,3};
UINT4 FsPnacNasId [ ] ={1,3,6,1,4,1,2076,64,1,4};
UINT4 FsPnacPaePortNumber [ ] ={1,3,6,1,4,1,2076,64,1,5,1,1};
UINT4 FsPnacPaePortAuthMode [ ] ={1,3,6,1,4,1,2076,64,1,5,1,2};
UINT4 FsPnacPaePortSupplicantCount [ ] ={1,3,6,1,4,1,2076,64,1,5,1,3};
UINT4 FsPnacPaePortUserName [ ] ={1,3,6,1,4,1,2076,64,1,5,1,4};
UINT4 FsPnacPaePortPassword [ ] ={1,3,6,1,4,1,2076,64,1,5,1,5};
UINT4 FsPnacPaePortStatus [ ] ={1,3,6,1,4,1,2076,64,1,5,1,6};
UINT4 FsPnacPaePortStatisticsClear [ ] ={1,3,6,1,4,1,2076,64,1,5,1,7};
UINT4 FsPnacPaePortAuthStatus [ ] ={1,3,6,1,4,1,2076,64,1,5,1,8};
UINT4 FsPnacPaeAuthReAuthMax [ ] ={1,3,6,1,4,1,2076,64,1,5,1,9};
UINT4 FsPnacModuleOperStatus [ ] ={1,3,6,1,4,1,2076,64,1,6};
UINT4 FsPnacRemoteAuthServerType [ ] ={1,3,6,1,4,1,2076,64,1,7};
UINT4 FsPnacAuthSessionSuppAddress [ ] ={1,3,6,1,4,1,2076,64,2,1,1,1};
UINT4 FsPnacAuthSessionIdentifier [ ] ={1,3,6,1,4,1,2076,64,2,1,1,2};
UINT4 FsPnacAuthSessionAuthPaeState [ ] ={1,3,6,1,4,1,2076,64,2,1,1,3};
UINT4 FsPnacAuthSessionBackendAuthState [ ] ={1,3,6,1,4,1,2076,64,2,1,1,4};
UINT4 FsPnacAuthSessionPortStatus [ ] ={1,3,6,1,4,1,2076,64,2,1,1,5};
UINT4 FsPnacAuthSessionPortNumber [ ] ={1,3,6,1,4,1,2076,64,2,1,1,6};
UINT4 FsPnacAuthSessionInitialize [ ] ={1,3,6,1,4,1,2076,64,2,1,1,7};
UINT4 FsPnacAuthSessionReauthenticate [ ] ={1,3,6,1,4,1,2076,64,2,1,1,8};
UINT4 FsPnacAuthSessionOctetsRx [ ] ={1,3,6,1,4,1,2076,64,2,2,1,1};
UINT4 FsPnacAuthSessionOctetsTx [ ] ={1,3,6,1,4,1,2076,64,2,2,1,2};
UINT4 FsPnacAuthSessionFramesRx [ ] ={1,3,6,1,4,1,2076,64,2,2,1,3};
UINT4 FsPnacAuthSessionFramesTx [ ] ={1,3,6,1,4,1,2076,64,2,2,1,4};
UINT4 FsPnacAuthSessionId [ ] ={1,3,6,1,4,1,2076,64,2,2,1,5};
UINT4 FsPnacAuthSessionAuthenticMethod [ ] ={1,3,6,1,4,1,2076,64,2,2,1,6};
UINT4 FsPnacAuthSessionTime [ ] ={1,3,6,1,4,1,2076,64,2,2,1,7};
UINT4 FsPnacAuthSessionTerminateCause [ ] ={1,3,6,1,4,1,2076,64,2,2,1,8};
UINT4 FsPnacAuthSessionUserName [ ] ={1,3,6,1,4,1,2076,64,2,2,1,9};
UINT4 FsPnacAuthSessionStatisticsClear [ ] ={1,3,6,1,4,1,2076,64,2,2,1,10};
UINT4 FsPnacASUserConfigUserName [ ] ={1,3,6,1,4,1,2076,64,3,1,1,1};
UINT4 FsPnacASUserConfigPassword [ ] ={1,3,6,1,4,1,2076,64,3,1,1,2};
UINT4 FsPnacASUserConfigAuthProtocol [ ] ={1,3,6,1,4,1,2076,64,3,1,1,3};
UINT4 FsPnacASUserConfigAuthTimeout [ ] ={1,3,6,1,4,1,2076,64,3,1,1,4};
UINT4 FsPnacASUserConfigPortList [ ] ={1,3,6,1,4,1,2076,64,3,1,1,5};
UINT4 FsPnacASUserConfigPermission [ ] ={1,3,6,1,4,1,2076,64,3,1,1,6};
UINT4 FsPnacASUserConfigRowStatus [ ] ={1,3,6,1,4,1,2076,64,3,1,1,7};
UINT4 FsPnacTrapAuthSessionStatus [ ] ={1,3,6,1,4,1,2076,64,4,1,1,1};




tMbDbEntry fspnacMibEntry[]= {

{{10,FsPnacSystemControl}, NULL, FsPnacSystemControlGet, FsPnacSystemControlSet, FsPnacSystemControlTest, FsPnacSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsPnacTraceOption}, NULL, FsPnacTraceOptionGet, FsPnacTraceOptionSet, FsPnacTraceOptionTest, FsPnacTraceOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FsPnacAuthenticServer}, NULL, FsPnacAuthenticServerGet, FsPnacAuthenticServerSet, FsPnacAuthenticServerTest, FsPnacAuthenticServerDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsPnacNasId}, NULL, FsPnacNasIdGet, FsPnacNasIdSet, FsPnacNasIdTest, FsPnacNasIdDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsPnacPaePortNumber}, GetNextIndexFsPnacPaePortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPnacPaePortTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacPaePortAuthMode}, GetNextIndexFsPnacPaePortTable, FsPnacPaePortAuthModeGet, FsPnacPaePortAuthModeSet, FsPnacPaePortAuthModeTest, FsPnacPaePortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPnacPaePortTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacPaePortSupplicantCount}, GetNextIndexFsPnacPaePortTable, FsPnacPaePortSupplicantCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPnacPaePortTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacPaePortUserName}, GetNextIndexFsPnacPaePortTable, FsPnacPaePortUserNameGet, FsPnacPaePortUserNameSet, FsPnacPaePortUserNameTest, FsPnacPaePortTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPnacPaePortTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacPaePortPassword}, GetNextIndexFsPnacPaePortTable, FsPnacPaePortPasswordGet, FsPnacPaePortPasswordSet, FsPnacPaePortPasswordTest, FsPnacPaePortTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPnacPaePortTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacPaePortStatus}, GetNextIndexFsPnacPaePortTable, FsPnacPaePortStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPnacPaePortTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacPaePortStatisticsClear}, GetNextIndexFsPnacPaePortTable, FsPnacPaePortStatisticsClearGet, FsPnacPaePortStatisticsClearSet, FsPnacPaePortStatisticsClearTest, FsPnacPaePortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPnacPaePortTableINDEX, 1, 0, 0, "2"},

{{12,FsPnacPaePortAuthStatus}, GetNextIndexFsPnacPaePortTable, FsPnacPaePortAuthStatusGet, FsPnacPaePortAuthStatusSet, FsPnacPaePortAuthStatusTest, FsPnacPaePortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPnacPaePortTableINDEX, 1, 0, 0, "1"},
{{12,FsPnacPaeAuthReAuthMax}, GetNextIndexFsPnacPaePortTable, FsPnacPaeAuthReAuthMaxGet, FsPnacPaeAuthReAuthMaxSet, FsPnacPaeAuthReAuthMaxTest, FsPnacPaePortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsPnacPaePortTableINDEX, 1, 0, 0, "2"},

{{10,FsPnacModuleOperStatus}, NULL, FsPnacModuleOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsPnacRemoteAuthServerType}, NULL, FsPnacRemoteAuthServerTypeGet, FsPnacRemoteAuthServerTypeSet, FsPnacRemoteAuthServerTypeTest, FsPnacRemoteAuthServerTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{12,FsPnacAuthSessionSuppAddress}, GetNextIndexFsPnacAuthSessionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsPnacAuthSessionTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacAuthSessionIdentifier}, GetNextIndexFsPnacAuthSessionTable, FsPnacAuthSessionIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPnacAuthSessionTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacAuthSessionAuthPaeState}, GetNextIndexFsPnacAuthSessionTable, FsPnacAuthSessionAuthPaeStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPnacAuthSessionTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacAuthSessionBackendAuthState}, GetNextIndexFsPnacAuthSessionTable, FsPnacAuthSessionBackendAuthStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPnacAuthSessionTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacAuthSessionPortStatus}, GetNextIndexFsPnacAuthSessionTable, FsPnacAuthSessionPortStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPnacAuthSessionTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacAuthSessionPortNumber}, GetNextIndexFsPnacAuthSessionTable, FsPnacAuthSessionPortNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPnacAuthSessionTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacAuthSessionInitialize}, GetNextIndexFsPnacAuthSessionTable, FsPnacAuthSessionInitializeGet, FsPnacAuthSessionInitializeSet, FsPnacAuthSessionInitializeTest, FsPnacAuthSessionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPnacAuthSessionTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacAuthSessionReauthenticate}, GetNextIndexFsPnacAuthSessionTable, FsPnacAuthSessionReauthenticateGet, FsPnacAuthSessionReauthenticateSet, FsPnacAuthSessionReauthenticateTest, FsPnacAuthSessionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPnacAuthSessionTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacAuthSessionOctetsRx}, GetNextIndexFsPnacAuthSessionStatsTable, FsPnacAuthSessionOctetsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsPnacAuthSessionStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacAuthSessionOctetsTx}, GetNextIndexFsPnacAuthSessionStatsTable, FsPnacAuthSessionOctetsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsPnacAuthSessionStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacAuthSessionFramesRx}, GetNextIndexFsPnacAuthSessionStatsTable, FsPnacAuthSessionFramesRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPnacAuthSessionStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacAuthSessionFramesTx}, GetNextIndexFsPnacAuthSessionStatsTable, FsPnacAuthSessionFramesTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPnacAuthSessionStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacAuthSessionId}, GetNextIndexFsPnacAuthSessionStatsTable, FsPnacAuthSessionIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPnacAuthSessionStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacAuthSessionAuthenticMethod}, GetNextIndexFsPnacAuthSessionStatsTable, FsPnacAuthSessionAuthenticMethodGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPnacAuthSessionStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacAuthSessionTime}, GetNextIndexFsPnacAuthSessionStatsTable, FsPnacAuthSessionTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPnacAuthSessionStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacAuthSessionTerminateCause}, GetNextIndexFsPnacAuthSessionStatsTable, FsPnacAuthSessionTerminateCauseGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPnacAuthSessionStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacAuthSessionUserName}, GetNextIndexFsPnacAuthSessionStatsTable, FsPnacAuthSessionUserNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPnacAuthSessionStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacAuthSessionStatisticsClear}, GetNextIndexFsPnacAuthSessionStatsTable, FsPnacAuthSessionStatisticsClearGet, FsPnacAuthSessionStatisticsClearSet, FsPnacAuthSessionStatisticsClearTest, NULL , SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPnacAuthSessionStatsTableINDEX, 1, 0, 0, "2"},

{{12,FsPnacASUserConfigUserName}, GetNextIndexFsPnacASUserConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPnacASUserConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacASUserConfigPassword}, GetNextIndexFsPnacASUserConfigTable, FsPnacASUserConfigPasswordGet, FsPnacASUserConfigPasswordSet, FsPnacASUserConfigPasswordTest, FsPnacASUserConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPnacASUserConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacASUserConfigAuthProtocol}, GetNextIndexFsPnacASUserConfigTable, FsPnacASUserConfigAuthProtocolGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPnacASUserConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacASUserConfigAuthTimeout}, GetNextIndexFsPnacASUserConfigTable, FsPnacASUserConfigAuthTimeoutGet, FsPnacASUserConfigAuthTimeoutSet, FsPnacASUserConfigAuthTimeoutTest, FsPnacASUserConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsPnacASUserConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacASUserConfigPortList}, GetNextIndexFsPnacASUserConfigTable, FsPnacASUserConfigPortListGet, FsPnacASUserConfigPortListSet, FsPnacASUserConfigPortListTest, FsPnacASUserConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPnacASUserConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacASUserConfigPermission}, GetNextIndexFsPnacASUserConfigTable, FsPnacASUserConfigPermissionGet, FsPnacASUserConfigPermissionSet, FsPnacASUserConfigPermissionTest, FsPnacASUserConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPnacASUserConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsPnacASUserConfigRowStatus}, GetNextIndexFsPnacASUserConfigTable, FsPnacASUserConfigRowStatusGet, FsPnacASUserConfigRowStatusSet, FsPnacASUserConfigRowStatusTest, FsPnacASUserConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPnacASUserConfigTableINDEX, 1, 0, 1, NULL},

{{12,FsPnacTrapAuthSessionStatus}, GetNextIndexFsPnacTrapAuthSessionTable, FsPnacTrapAuthSessionStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPnacTrapAuthSessionTableINDEX, 1, 0, 0, NULL},

{{10,FsDPnacSystemStatus}, NULL, FsDPnacSystemStatusGet, FsDPnacSystemStatusSet, FsDPnacSystemStatusTest, FsDPnacSystemStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsDPnacPeriodicSyncTime}, NULL, FsDPnacPeriodicSyncTimeGet, FsDPnacPeriodicSyncTimeSet, FsDPnacPeriodicSyncTimeTest, FsDPnacPeriodicSyncTimeDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "60"},

{{10,FsDPnacMaxKeepAliveCount}, NULL, FsDPnacMaxKeepAliveCountGet, FsDPnacMaxKeepAliveCountSet, FsDPnacMaxKeepAliveCountTest, FsDPnacMaxKeepAliveCountDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{12,FsDPnacSlotNumber}, GetNextIndexFsDPnacStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDPnacStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsDPnacEventUpdateFramesRx}, GetNextIndexFsDPnacStatsTable, FsDPnacEventUpdateFramesRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDPnacStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsDPnacEventUpdateFramesTx}, GetNextIndexFsDPnacStatsTable, FsDPnacEventUpdateFramesTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDPnacStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsDPnacPeriodicFramesTx}, GetNextIndexFsDPnacStatsTable, FsDPnacPeriodicFramesTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDPnacStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsDPnacPeriodicFramesRx}, GetNextIndexFsDPnacStatsTable, FsDPnacPeriodicFramesRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDPnacStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsDPnacPortIndex}, GetNextIndexFsDPnacSlotPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsDPnacSlotPortTableINDEX, 2, 0, 0, NULL},

{{12,FsDPnacPortAuthStatus}, GetNextIndexFsDPnacSlotPortTable, FsDPnacPortAuthStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDPnacSlotPortTableINDEX, 2, 0, 0, NULL},

{{12,FsDPnacPortControlledDirection}, GetNextIndexFsDPnacSlotPortTable, FsDPnacPortControlledDirectionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDPnacSlotPortTableINDEX, 2, 0, 0, NULL},
};
tMibData fspnacEntry = { 52, fspnacMibEntry };

#endif /* _FSPNACDB_H */

