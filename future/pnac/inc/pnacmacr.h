/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                 */
/* $Id: pnacmacr.h,v 1.31 2015/06/11 10:03:14 siva Exp $ */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : pnacmacr.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : None                                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Mar 2002                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains the macros used by          */
/*                            PNAC module.                                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 MAR 2002 / BridgeTeam   Initial Create.                     */
/*---------------------------------------------------------------------------*/ 

#ifndef _PNACMACR_H
#define _PNACMACR_H

/* FSAP related defines */
#define   tPnacHashTable       tTMO_HASH_TABLE
#define   tPnacHashNode        tTMO_HASH_NODE
#define   tPnacAppTimer        tTmrAppTimer
#define   tPnacTimerCfg        tTimerCfg
#define   tPnacTmrListId       tTimerListId
#define   tPnacTaskId          tOsixTaskId
#define   tPnacSysTime         tOsixSysTime
#define   tPnacMemPoolId       tMemPoolId
#define   tPnacOsixSemId       tOsixSemId
#define   tPnacQId             tOsixQId


/* Memory Pool related macros */
#define   PNAC_INTFMESG_MEMPOOL_ID      gPnacIntfMesgMemPoolId
#define   PNAC_INTF_CFGQ_MESG_MEMPOOL_ID   gPnacIntfCfgQMesgMemPoolId
#define   PNAC_EAPOL_MESG_MEMPOOL_ID     gPnacEapolMesgMemPoolId
#define   PNAC_AUTHSESSNODE_MEMBLK_SIZE (sizeof(tPnacAuthSessionNode))
#define   PNAC_AUTHSESSNODE_FAIL_MEMBLK_SIZE (sizeof(tPnacNpFailedMacSessNode))
#define   PNAC_SUPPSESSNODE_MEMBLK_SIZE (sizeof(tPnacSuppSessionNode))
#define   PNAC_TIMERNODE_MEMBLK_SIZE    (sizeof( tPnacTimerNode ))

#define   PNAC_MEMORY_TYPE      MEM_DEFAULT_MEMORY_TYPE

#define   PNAC_MAXMEMPOOL_SIZE \
   ((PNAC_MAX_AUTHSESS * PNAC_AUTHSESSNODE_MEMBLK_SIZE) + \
    (PNAC_MAXPORTS * PNAC_SUPPSESSNODE_MEMBLK_SIZE))

    
/* Count of Number of Memory Blocks per Memory Pool */

#define   PNAC_AUTHSESSNODE_MEMBLK_COUNT   PNAC_MAX_AUTHSESS
#define   PNAC_SUPPSESSNODE_MEMBLK_COUNT   PNAC_MAXPORTS


/* MD5 related defines */

   /* Function that performs MD5 Algorithm */
#define   PNAC_GET_MD5_DIGEST                  Md5GetKeyDigest 

#define   PNAC_MD5_DIGEST_SIZE                 16        /* Octets */
#define   PNAC_EAP_DESIRED_AUTHENTICATION_TYPE PNAC_EAP_TYPE_MD5CHALLENGE /* MD5 */

/* Bridge related macros */
#define   PNAC_BRIDGE_SUCCESS           BRIDGE_SUCCESS
#define   tPnacPhyMediaDataRate         tLaPhyMediaDataRate

/* PNAC Module related definitions */
#define   PNAC_MODULE_STATUS            gPnacSystemInfo.u2SystemAuthControl
#define   PNAC_SYSTEM_CONTROL           gu2PnacSystemControl

#define   PNAC_IS_STARTED() \
   (PNAC_SYSTEM_CONTROL == PNAC_START)

#define   PNAC_IS_SHUTDOWN() \
   (PNAC_SYSTEM_CONTROL == PNAC_SHUTDOWN)

#define   PNAC_IS_ENABLED() \
   (PNAC_MODULE_STATUS == PNAC_ENABLED)

#define   PNAC_IS_DISABLED() \
   (PNAC_MODULE_STATUS == PNAC_DISABLED)


/* Memory Pool Ids definitions */
   
#define   PNAC_AUTHSESSNODE_MEMPOOL_ID  gPnacSystemInfo.authSessNodeMemPoolId
#define   PNAC_SUPPSESSNODE_MEMPOOL_ID  gPnacSystemInfo.suppSessNodeMemPoolId
#define   PNAC_AUTHSESSNODE_FAIL_MEMPOOL_ID  gPnacSystemInfo.npFailedMacAddrMemPoolId
#define   PNAC_TIMERNODE_MEMPOOL_ID     gPnacSystemInfo.tmrMemPoolId

#define   PNAC_AUTH_SESSION_TABLE       gPnacSystemInfo.pPnacAuthSessionTable
#define   PNAC_TACMSGINPUT_MEMPOOL_ID   gPnacSystemInfo.tacMsgInpMemPoolId
#define   PNAC_NOTIFY_OID_MEMPOOL_ID    gPnacSystemInfo.npNotifyOidMemPoolId
#define   PNAC_ENET_EAP_ARRAY_MEMPOOL_ID gPnacSystemInfo.enetEapArrayMemPoolId
#define   PNAC_IF_RECV_DATA_MEMPOOL_ID  gPnacSystemInfo.pnacAsIfRecvDataMemPoolId
#define   PNAC_IF_SEND_DATA_MEMPOOL_ID  gPnacSystemInfo.pnacAsIfSendDataMemPoolId
#define   PNAC_USER_INFO_MEMPOOL_ID     gPnacSystemInfo.userInfoMemPoolId
#define   PNAC_DPNAC_PORTS_INFO_MEMPOOL_ID     gPnacSystemInfo.PnacDPnacPortsInfoMemPoolId
#define   PNAC_DPNAC_SLOTS_INFO_MEMPOOL_ID     gPnacSystemInfo.PnacDPnacSlotsInfoMemPoolId

/* Return Values */

#define   PNAC_MEM_SUCCESS         MEM_SUCCESS
#define   PNAC_MEM_FAILURE         MEM_FAILURE

#define   PNAC_CRU_FALSE           FALSE
#define   PNAC_CRU_TRUE            TRUE

#define   PNAC_MIB_TRUE            1
#define   PNAC_MIB_FALSE           2

/* Conversion Macros for TimeTicks for SNMP to/from seconds */
#define PNAC_SYS_TO_MGMT(x)            ((x)*100)
#define PNAC_MGMT_TO_SYS(x)            ((x)/100)

   

/* Defines of Library functions used */
   
#define   PNAC_MEMCPY              MEMCPY
#define   PNAC_MEMCMP              MEMCMP
#define   PNAC_MEMSET              MEMSET

#define   PNAC_STRCPY              STRCPY
#define   PNAC_STRNCPY             STRNCPY
#define   PNAC_STRLEN              STRLEN
#define   PNAC_SPRINTF             SPRINTF 


/* Macros for creating memory pools */
   
#define   PNAC_CREATE_MEM_POOL(x,y,pPoolId)\
             MemCreateMemPool(x,y,PNAC_MEMORY_TYPE,(tPnacMemPoolId*)pPoolId)


/* Macros for deleting Memory Pools */

#define   PNAC_DELETE_MEM_POOL(pPoolId)\
             MemDeleteMemPool((tPnacMemPoolId) pPoolId)


/* Macros for allocating memory block */

#define   PNAC_ALLOC_MEM_BLOCK(poolid) \
             MemAllocMemBlk(poolid)
#define   PNAC_ALLOCATE_AUTHSESSNODE_MEMBLK(pNode)  \
             (pNode = \
              (tPnacAuthSessionNode *)MemAllocMemBlk \
              ( PNAC_AUTHSESSNODE_MEMPOOL_ID))
                                

#define   PNAC_ALLOCATE_SUPPSESSNODE_MEMBLK(pNode)  \
     (pNode = \
     (tPnacSuppSessionNode *) MemAllocMemBlk \
     (PNAC_SUPPSESSNODE_MEMPOOL_ID))

#define   PNAC_ALLOCATE_TIMERNODE_MEMBLK(pNode) \
             (pNode = \
              (tPnacTimerNode *)MemAllocMemBlk(PNAC_TIMERNODE_MEMPOOL_ID))

#define   PNAC_ALLOCATE_TACMSGINP_MEMBLK(pNode) \
          (pNode = \
           (tTacMsgInput *)(VOID *)UtlShMemAllocTacMsgInput())

#define   PNAC_ALLOCATE_NOTIFY_OID_MEMBLK(pNode) \
          (pNode = \
           (UINT4 *)MemAllocMemBlk(PNAC_NOTIFY_OID_MEMPOOL_ID))

#define   PNAC_ALLOCATE_ENET_EAP_ARRAY_MEMBLK(pNode) \
          (pNode = \
           (UINT1 *)MemAllocMemBlk(PNAC_ENET_EAP_ARRAY_MEMPOOL_ID))

#define   PNAC_ALLOCATE_IF_RECV_DATA_MEMBLK(pNode) \
          (pNode = \
           (tPnacAsIfRecvData *)MemAllocMemBlk(PNAC_IF_RECV_DATA_MEMPOOL_ID))

#define   PNAC_ALLOCATE_IF_SEND_DATA_MEMBLK(pNode) \
          (pNode = \
           (tPnacAsIfSendData *)MemAllocMemBlk(PNAC_IF_SEND_DATA_MEMPOOL_ID))


#define   PNAC_ALLOCATE_USER_INFO_DATA_MEMBLK(pNode) \
          (pNode = \
           (tUSER_INFO_EAP *)MemAllocMemBlk(PNAC_USER_INFO_MEMPOOL_ID))

#define   PNAC_ALLOCATE_DPNAC_PORTS_INFO_MEM_BLOCK(ppu1Msg)\
          (ppu1Msg = (tPnacDPnacConsPortEntry *) MemAllocMemBlk(PNAC_DPNAC_PORTS_INFO_MEMPOOL_ID))


#define   PNAC_ALLOCATE_SLOTS_INFO_MEM_BLOCK(ppu1Msg)\
          (ppu1Msg = (tPnacDPnacSlotInfo*) MemAllocMemBlk(PNAC_DPNAC_SLOTS_INFO_MEMPOOL_ID))

   /* Macros for freeing memory block */
#define   PNAC_RELEASE_MEM_BLOCK(poolid, pNode) \
              MemReleaseMemBlock((poolid), (UINT1 *)(pNode))

#define   PNAC_RELEASE_AUTHSESSNODE_MEMBLK(pNode) \
           MemReleaseMemBlock ( PNAC_AUTHSESSNODE_MEMPOOL_ID, \
                              (UINT1 *)pNode)
                              
#define   PNAC_RELEASE_SUPPSESSNODE_MEMBLK(pNode) \
           MemReleaseMemBlock (PNAC_SUPPSESSNODE_MEMPOOL_ID,  \
                                (UINT1 *)pNode) 

#define   PNAC_RELEASE_TIMERNODE_MEMBLK(pNode) \
             MemReleaseMemBlock( PNAC_TIMERNODE_MEMPOOL_ID, \
                               (UINT1 *)pNode)

#define   PNAC_RELEASE_TACMSGINP_MEMBLK(pNode) \
             UtlShMemFreeTacMsgInput ((UINT1 *)pNode)

#define   PNAC_RELEASE_NOTIFY_OID_MEMBLK(pNode) \
             MemReleaseMemBlock( PNAC_NOTIFY_OID_MEMPOOL_ID, \
                               (UINT1 *)pNode)

#define   PNAC_RELEASE_ENET_EAP_ARRAY_MEMBLK(pNode) \
             MemReleaseMemBlock(PNAC_ENET_EAP_ARRAY_MEMPOOL_ID, \
                               (UINT1 *)pNode)

#define   PNAC_RELEASE_IF_RECV_DATA_MEMBLK(pNode) \
             MemReleaseMemBlock( PNAC_IF_RECV_DATA_MEMPOOL_ID, \
                               (UINT1 *)pNode)

#define   PNAC_RELEASE_IF_SEND_DATA_MEMBLK(pNode) \
             MemReleaseMemBlock( PNAC_IF_SEND_DATA_MEMPOOL_ID, \
                               (UINT1 *)pNode)

#define   PNAC_RELEASE_USER_INFO_DATA_MEMBLK(pNode) \
             MemReleaseMemBlock( PNAC_USER_INFO_MEMPOOL_ID, \
                               (UINT1 *)pNode)

#define   PNAC_RELEASE_DPNAC_PORTS_INFO_MEM_BLOCK(pNode) \
             MemReleaseMemBlock(PNAC_DPNAC_PORTS_INFO_MEMPOOL_ID, \
                               (UINT1 *)pNode)

#define   PNAC_RELEASE_DPNAC_SLOTS_INFO_MEM_BLOCK(pNode) \
             MemReleaseMemBlock(PNAC_DPNAC_SLOTS_INFO_MEMPOOL_ID, \
                               (UINT1 *)pNode)

/* Hash Table related macros */

#define   PNAC_HASH_CREATE_TABLE              TMO_HASH_Create_Table

#define   PNAC_HASH_GET_FIRST_BUCKET_NODE     TMO_HASH_Get_First_Bucket_Node

#define   PNAC_HASH_GET_NEXT_BUCKET_NODE      TMO_HASH_Get_Next_Bucket_Node

#define   PNAC_HASH_DELETE_TABLE              TMO_HASH_Delete_Table

#define   PNAC_HASH_FREE(ptr)                 TMO_FREE((VOID *)ptr)

#define   PNAC_HASH_INIT_NODE(pNode)\
             TMO_HASH_INIT_NODE((tPnacHashNode *)pNode)

#define   PNAC_HASH_ADD_NODE(pHashTab, pNode, u4Index,FuncParam)\
             TMO_HASH_Add_Node((tPnacHashTable *)pHashTab,\
             (tPnacHashNode *)pNode,(UINT4)u4Index, (UINT1 *)FuncParam)

#define   PNAC_HASH_DEL_NODE(pHashTab, pNode, u4Index)\
             TMO_HASH_Delete_Node((tPnacHashTable *)pHashTab, \
             (tPnacHashNode *)pNode, u4Index)

#define   PNAC_HASH_SCAN_BKT(pHashTab, u4Index, pNode, Typecast)\
             TMO_HASH_Scan_Bucket((tPnacHashTable *)pHashTab, u4Index,\
             (tPnacHashNode *)pNode, Typecast)

#define   PNAC_HASH_SCAN_TBL(pHashTab, u4Index)\
             TMO_HASH_Scan_Table((tPnacHashTable *)pHashTab, u4Index)


/* Timer related macros */

#define   PNAC_NUM_OF_TIME_UNITS_IN_A_SEC      SYS_NUM_OF_TIME_UNITS_IN_A_SEC
#define   PNAC_CREATE_TIMERLIST        TmrCreateTimerList
#define   PNAC_DELETE_TIMERLIST        TmrDeleteTimerList
#define   PNAC_START_TIMER             TmrStartTimer
#define   PNAC_STOP_TIMER              TmrStopTimer
#define   PNAC_GET_NEXT_EXPIRED_TIMER  TmrGetNextExpiredTimer
#define   PNAC_TMR_SUCCESS             TMR_SUCCESS
#define   PNAC_TMR_FAILURE             TMR_FAILURE
#define   PNAC_TMR_GET_REMAINING_TIME  TmrGetRemainingTime


/* Task and Event related macros */

#define   PNAC_CREATE_TASK       OsixTskCrt
#define   PNAC_DELETE_TASK       OsixTskDel
#define   PNAC_CREATE_QUEUE      OsixQueCrt
#define   PNAC_DELETE_QUEUE      OsixQueDel
#define   PNAC_SELF              SELF
#define   PNAC_OSIX_EV_ANY       OSIX_EV_ANY
#define   PNAC_STACK_SIZE        (2*OSIX_DEFAULT_STACK_SIZE)
#define   PNAC_TASK_MODE         OSIX_LOCAL
#define   PNAC_RECEIVE_EVENT     OsixEvtRecv
#define   PNAC_SEND_EVENT        OsixEvtSend
#define   PNAC_GET_TASK_ID       OsixGetTaskId

#define   PNAC_SEND_TO_QUEUE    OsixQueSend

#define   PNAC_RECV_FROM_QUEUE  OsixQueRecv

#define   PNAC_OSIX_NO_WAIT      OSIX_NO_WAIT
#define   PNAC_OSIX_MSG_NORMAL   OSIX_MSG_NORMAL


#define   PNAC_INTF_TASK_ID      gPnacIntfTaskId
#define   PNAC_SEM_ID            gPnacSemId
#define   PNAC_INTF_INPUT_QID    gPnacIntfInputQId  
#define   PNAC_INTF_CFG_QID      gPnacIntfCfgQId
    
#define   PNAC_OSIX_SUCCESS      OSIX_SUCCESS
#define   PNAC_OSIX_FAILURE      OSIX_FAILURE
#define   PNAC_HTONS             OSIX_HTONS
#define   PNAC_HTONL             OSIX_HTONL
#define   PNAC_NTOHS             OSIX_NTOHS
#define   PNAC_NTOHL             OSIX_NTOHL
#define   PNAC_GET_SYSTEM_TIME   OsixGetSysTime

/* SEM Related Macros */

#define PNAC_CREATE_SEM(pu1Sem, u4Count, u4Flags, pSemId) \
        OsixCreateSem ((pu1Sem), (u4Count), (u4Flags), (pSemId))

#define PNAC_DELETE_SEM          OsixSemDel
   


/* CFA related macros */

#define   PNAC_ENCAP_NONE        CFA_ENCAP_NONE
   
#define   PNAC_CFA_GET_PORT_INFO(u2PortNum, pCfaIfInfo) \
   CfaGetIfInfo(u2PortNum, pCfaIfInfo)
   

#define   PNAC_CFA_HANDLE_OUT_FRAME(pCruBufHdr,u2IfIndex,u4FrameSize,\
                                    u2Protocol,u1EncapType) \
          PnacL2IwfHandleOutgoingPktOnPort (pCruBufHdr, u2IfIndex, \
                                            u4FrameSize, u2Protocol, \
                                            u1EncapType)

/* CRU Buffer related definitions */

#define   PNAC_ALLOC_CRU_BUF                    CRU_BUF_Allocate_MsgBufChain
#define   PNAC_RELEASE_CRU_BUF                  CRU_BUF_Release_MsgBufChain
#define   PNAC_COPY_OVER_CRU_BUF                CRU_BUF_Copy_OverBufChain
#define   PNAC_COPY_FROM_CRU_BUF                CRU_BUF_Copy_FromBufChain
#define   PNAC_CRU_BUF_Get_ChainValidByteCount  CRU_BUF_Get_ChainValidByteCount
#define   PNAC_IS_CRU_BUF_LINEAR                CRU_BUF_Get_DataPtr_IfLinear
#define   PNAC_DUPLICATE_CRU_BUF                CRU_BUF_Duplicate_BufChain
#define   PNAC_CRU_SUCCESS                      CRU_SUCCESS
#define   PNAC_CRU_FAILURE                      CRU_FAILURE

#define   PNAC_UNUSED(x)  UNUSED_PARAM(x)

#define   PNAC_IS_BROADCAST_ADDR(MacAddr) \
        ((PNAC_MEMCMP(MacAddr,gPnacBCastAddr,PNAC_MAC_ADDR_SIZE)) ? \
               PNAC_FALSE : PNAC_TRUE)
#define   PNAC_IS_MULTICAST_ADDR(p) (p & 0x01)
   

/* port capability checks */

#define   PNAC_IS_PORT_AUTH_CAPABLE(pPortEntry)   \
                ((pPortEntry->u1Capabilities & PNAC_PORT_CAPABILITY_AUTH) == PNAC_PORT_CAPABILITY_AUTH) 

#define   PNAC_IS_PORT_SUPP_CAPABLE(pPortEntry)   \
                ((pPortEntry->u1Capabilities & PNAC_PORT_CAPABILITY_SUPP) == PNAC_PORT_CAPABILITY_SUPP) 
#define   PNAC_IS_PORT_BOTH_CAPABLE(pPortEntry)   \
                ((pPortEntry->u1Capabilities & PNAC_PORT_CAPABILITY_BOTH) == PNAC_PORT_CAPABILITY_BOTH) 

#define   PNAC_IS_PORT_NONE_CAPABLE(pPortEntry)   \
                (pPortEntry->u1Capabilities == PNAC_PORT_CAPABILITY_NONE) 
   
#define   PNAC_IS_PORT_NOT_AUTH_CAPABLE(pPortEntry)   \
   ((pPortEntry->u1Capabilities & PNAC_PORT_CAPABILITY_AUTH) != PNAC_PORT_CAPABILITY_AUTH) 

#define   PNAC_IS_PORT_NOT_SUPP_CAPABLE(pPortEntry)   \
   ((pPortEntry->u1Capabilities & PNAC_PORT_CAPABILITY_SUPP) != PNAC_PORT_CAPABILITY_SUPP) 

#define   PNAC_IS_PORT_NOT_BOTH_CAPABLE(pPortEntry)   \
   ((pPortEntry->u1Capabilities & PNAC_PORT_CAPABILITY_BOTH) != PNAC_PORT_CAPABILITY_BOTH) 

   /* Port Authentication Mode checks */
#define  PNAC_IS_PORT_MODE_PORTBASED(pPortInfo) \
   (pPortInfo->u1PortAuthMode == PNAC_PORT_AUTHMODE_PORTBASED)
   
#define  PNAC_IS_PORT_MODE_MACBASED(pPortInfo) \
   (pPortInfo->u1PortAuthMode == PNAC_PORT_AUTHMODE_MACBASED)

#define  PNAC_IS_PORT_UP(pPortInfo) \
   (pPortInfo->u1EntryStatus == PNAC_PORT_OPER_UP)

#define  PNAC_IS_PORT_DOWN(pPortInfo) \
   (pPortInfo->u1EntryStatus == PNAC_PORT_OPER_DOWN)

#define  PNAC_IS_LOCAL_AS() \
   (gPnacSystemInfo.u1AuthenticMethod == PNAC_LOCAL_AUTH_SERVER)

#define  PNAC_IS_REMOTE_RADIUS_AS() \
   ((gPnacSystemInfo.u1AuthenticMethod == PNAC_REMOTE_AUTH_SERVER) \
    && (gPnacSystemInfo.u1RemoteAuthServerType == PNAC_REMOTE_RADIUS_AUTH_SERVER))

#define  PNAC_PORT_INFO(u2PortNum) \
   gPnacSystemInfo.aPnacPaePortInfo[u2PortNum]

#define  PNAC_AUTH_STATS_ENTRY(pPortInfo) \
   pPortInfo->authStatsEntry
   
#define  PNAC_SUPP_STATS_ENTRY(pPortInfo) \
   pPortInfo->suppStatsEntry

#define  PNAC_AUTH_CONFIG_ENTRY(pPortInfo) \
      pPortInfo->authConfigEntry

#define  PNAC_SUPP_CONFIG_ENTRY(pPortInfo) \
      pPortInfo->suppConfigEntry

#define  PNAC_SESSION_ID_ENTRY(u2PortNum,u2Id) \
   gPnacSystemInfo.aPnacAuthSessionId[u2PortNum][u2Id]

#define  PNAC_AIP_ENTRY(u2Num) \
   gPnacSystemInfo.apAuthInProgress[u2Num]


/* WLAN related definition */
                
#define   PNAC_TX_KEY_TO_WLAN          PnacWlanRxKey


/* encrypt-decrypt submodule related definition */
#define   PNAC_STORE_KEY        PnacWlanCryptStoreKey
#define   PNAC_FILL_KEY_DESC    PnacWlanFillKeyDesc 

/* RADIUS Client related definition */
#define   PNAC_TX_EAP_TO_RADIUS    PnacRadiusTxEapPkt

/* Macros for getting data from linear buffer */
#define PNAC_GET_1BYTE(u1Val, pu1PktBuf) \
        do {                             \
           u1Val = *pu1PktBuf;           \
           pu1PktBuf += 1;        \
        } while(0)

#define PNAC_GET_2BYTE(u2Val, pu1PktBuf)            \
        do {                                        \
           PNAC_MEMCPY (&u2Val, pu1PktBuf, 2);\
           u2Val = (UINT2)PNAC_NTOHS(u2Val);               \
           pu1PktBuf += 2;                   \
        } while(0)

#define PNAC_GET_4BYTE(u4Val, pu1PktBuf)              \
        do {                                          \
           PNAC_MEMCPY (&u4Val, pu1PktBuf, 4); \
           u4Val = PNAC_NTOHL(u4Val);                 \
           pu1PktBuf += 4;                    \
        } while(0)


/* Encode Module Related Macros used to assign fields to buffer */
#define PNAC_PUT_1BYTE(pu1PktBuf, u1Val) \
        do {                             \
           *pu1PktBuf = u1Val;           \
           pu1PktBuf += 1;        \
        } while(0)

#define PNAC_PUT_2BYTE(pu1PktBuf, u2Val)        \
        do {                                    \
            u2Val = (UINT2)PNAC_HTONS(u2Val);                  \
            PNAC_MEMCPY (pu1PktBuf, &u2Val, 2); \
            pu1PktBuf += 2;              \
        } while(0)
#define PNAC_PUT_4BYTE(pu1PktBuf, u4Val)       \
        do {                                   \
           u4Val = PNAC_HTONL(u4Val);   \
           PNAC_MEMCPY (pu1PktBuf, &u4Val, 4); \
           pu1PktBuf += 4;             \
        }while(0)

#define PNAC_COMPARE_MAC_ADDR(MacAddr1, MacAddr2) \
         ((PNAC_MEMCMP(MacAddr1, MacAddr2, PNAC_MAC_ADDR_SIZE) == 0) ? \
            PNAC_TRUE : PNAC_FALSE)

#define PNAC_DERIVE_UNIQ_MAC(DestMacAddr, u2Port)                   \
        {                                                           \
          UINT1               u1MacAddr = 0;                        \
          u1MacAddr = (UINT1)((u2Port >> 8) & PNAC_LSBYTE0_MASK);   \
          DestMacAddr[4] = u1MacAddr;                               \
          u1MacAddr = (UINT1)((u2Port) & PNAC_LSBYTE0_MASK);        \
          DestMacAddr[5] = u1MacAddr;                               \
        }
        
/* Macro for UINT8 counters */

#define   PNAC_UINT8_ADD(varA, value) \
   if ((0xffffffff - varA.u4LoWord) >= value){ \
      (varA.u4LoWord)+= value; \
   }                           \
   else {                      \
      varA.u4LoWord = value - (0xffffffff - varA.u4LoWord) - 1;\
      (varA.u4HiWord)++; \
   } 

#define   PNAC_UINT8_INC(varA) \
   if (varA.u4HiWord == 0xffffffff) \
      (varA.u4LoWord)++; \
   (varA.u4HiWord)++; 
   
#define  PNAC_UINT8_RESET(varA) \
   varA.u4HiWord = 0; \
   varA.u4LoWord = 0;


/* Trace related macro */


#define PNAC_TRACE(u4TraceType, Msg)




#define PNAC_MAC_ADDR_TO_ARRAY(pu1Array, pMacAddr) \
        { \
          tMacAddress TmpMacAddr; \
          \
          TmpMacAddr.u4Dword = (pMacAddr)->u4Dword; \
          TmpMacAddr.u2Word = (pMacAddr)->u2Word;   \
          \
          TmpMacAddr.u4Dword = OSIX_HTONL(TmpMacAddr.u4Dword); \
          TmpMacAddr.u2Word = (UINT2)(OSIX_HTONS(TmpMacAddr.u2Word));   \
          \
          MEMCPY ((pu1Array), (&TmpMacAddr.u4Dword), (sizeof(TmpMacAddr.u4Dword))); \
          MEMCPY ((pu1Array+(sizeof(TmpMacAddr.u4Dword))), (&TmpMacAddr.u2Word), \
                   (sizeof(TmpMacAddr.u2Word))); \
        }

#define PNAC_GET_TWOBYTE(u2Val, MacAddr)            \
        do {                                        \
           PNAC_MEMCPY (&u2Val, MacAddr, 2);\
           u2Val = (UINT2)PNAC_NTOHS(u2Val);               \
        } while(0)

#define PNAC_GET_FOURBYTE(u4Val, MacAddr)              \
        do {                                          \
           PNAC_MEMCPY (&u4Val, MacAddr, 4); \
           u4Val = PNAC_NTOHL(u4Val);                 \
        } while(0)

#define PNAC_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)

#endif /* _PNACMACR_H */


