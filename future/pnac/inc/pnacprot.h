/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/* $Id: pnacprot.h,v 1.44 2015/06/19 06:22:03 siva Exp $                   */
/*****************************************************************************/
/*    FILE  NAME            : pnacprot.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : PNAC module PAE sub module                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Mar 2002                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the function prototypes for */
/*                            all the functions in PNAC module.              */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 MAR 2002 / BridgeTeam   Initial Create.                     */
/* 1.0.0.1    29 NOV 2002 / Products     Modifying NP-API calls              */
/*---------------------------------------------------------------------------*/ 

#ifndef _PNACPROT_H
#define _PNACPROT_H

#include "iss.h"
/* Port related and PAE functions */

INT4 PnacModuleInit (VOID);

INT4 PnacHandleModuleStart (VOID);
VOID PnacAssignMempoolIds(VOID);
VOID PnacHandleModuleShutDown (VOID);

VOID PnacDeleteAuthSessionHashNode (tTMO_HASH_NODE *);

INT4 PnacModuleEnable(VOID);

VOID PnacModuleDisable (VOID);

INT4 PnacPaeCdStateMachine (UINT1 , UINT2 );

INT4 PnacPaeBridgeDetectionStatus (UINT1 , UINT2 );

INT4 PnacInitPort (UINT2 );

INT4 PnacRegisterWithPacketHandler (VOID);

INT4 PnacHandleInCtrlFrame (tPnacCruBufChainHdr * pMesg, 
          UINT2 u2IfIndex,
                     UINT1 u1FrameType);
INT4 PnacHandleInDataFrame (tPnacCruBufChainHdr * pMesg, UINT2 u2IfIndex,
                            UINT1 u1FrameType, UINT4 u4ContextId);



UINT4
PnacWlanStartAuth(tMacAddr, UINT4);
VOID PnacInitVirtualPort (UINT2 );

INT4 PnacPaeGetIfStatus (UINT2 , UINT1 *);

INT4 PnacAuthLinkStatusUpdate(tPnacPaePortEntry *);


INT4 PnacProcPortStatUpdEvent (UINT2, UINT1 );

INT4 PnacProcCreatePortEvent (UINT2);

INT4 PnacProcDeletePortEvent (UINT2);
INT4 PnacCheckIfPortStatusChanged (UINT2);

INT1 PnacLowTestPortAuthMode (UINT4 *pu4ErrorCode, UINT2 u2Port,
                              INT4 i4AuthMode);
INT1 PnacLowSetPortAuthMode (UINT2 u2Port, INT4 i4PortAuthMode);

INT1 PnacLowTestPortAuthControl (UINT4 *pu4ErrorCode, UINT2 u2Port,
                                 UINT2 u2AuthControl);
INT1 PnacLowSetPortAuthControl (UINT2 u2Port, UINT2 u2AuthControl);

INT1
PnacLowTestPortPaeStatus (UINT4 *pu4ErrorCode, UINT2 u2Port, UINT2 u2PortPaeStatus);

INT1
PnacLowSetPortPaeStatus (UINT2 u2Port, UINT2 u2PortPaeStatus);

INT4 PnacHandleBridgeDetected (UINT2);

/* Table Data Structures Access related functions */

INT4 PnacInitSessionHashTbl (VOID);

VOID PnacAuthCalcSessionTblIndex (tMacAddr, UINT4 *);

INT4 PnacAuthGetSessionTblEntryByMac (tMacAddr, tPnacAuthSessionNode **);

INT4 PnacAuthGetFirstValidSuppAddress (tMacAddr);

INT4 PnacAuthGetNextValidSuppAddress (tMacAddr, tMacAddr);

INT4 PnacAuthAddToAuthInProgressTbl(tPnacAuthSessionNode *);

INT4 PnacAuthDeleteAuthInProgressTblEntry (tPnacAuthSessionNode *);

INT4 PnacAuthGetAuthInProgressTblEntryByMac (tMacAddr,
                                             tPnacAuthSessionNode **);

INT4 PnacGetPortEntry (UINT2 , tPnacPaePortEntry **);

/* Incoming Packet Handling related functions */
    
INT4 PnacProcPaeFrame (UINT2, tPnacCruBufChainHdr *);
INT4 PnacPaeProcessEapolFrame (tPnacCruBufChainHdr *,
                               UINT2 ,
                               tMacAddr,
                        UINT2);

VOID PnacHandOverInFrame ( tPnacCruBufChainHdr *,
                           UINT2 ,
                           UINT2 ,
                           UINT1 );

INT4 PnacHandOverOutFrame( tPnacCruBufChainHdr *,
                           UINT2  ,
                           UINT4 ,
                           UINT2 ,
                           UINT1 );


/* Outgoing Packet Handling related functions */

VOID PnacPaeConstructEapolHdr ( UINT1 *, 
                         tMacAddr, 
                         UINT1 , 
                         UINT2 ,
                            UINT2,
                         BOOL1 );

INT4 PnacPaeTxEapPkt (tMacAddr, UINT2 , tPnacEapPktHdr *,BOOL1);

INT4 PnacPaeTxEnetFrame (UINT1 *, UINT2 , UINT2 );




/* Key Handling related functions */


INT4 PnacKeyRxKey ( UINT1 *, UINT2 );

INT4 PnacKeyAuthTxKey ( UINT1 *,
                        UINT2 ,
                        UINT2 ,
                        tMacAddr );

INT4 PnacKeyBuildEapKeyPkt ( tPnacKeyDesc *pEapKeyDesc,
                             UINT1    *pu1Buf, 
                             UINT2     *pu2PktLen);

/* Authentication Server Interface functions */
INT4 PnacInitQueue (VOID);
VOID PnacQandSemDeinit (VOID);
VOID PnacInterfaceHandleQueueEvent(VOID);

VOID PnacInterfaceHandleCFGQueueEvent(VOID);

INT4 PnacAsIfSendToServer ( UINT1 *,
                            UINT2 ,
                            tPnacAuthSessionNode  *,
                            UINT2 );

VOID PnacEnQFrameToIntfTask (tPnacCruBufChainHdr *pBuf, 
                             UINT2 u2PortIndex,                                                              UINT1 u1FrameType);
VOID PnacEnQFirstDataFrameToIntfTask (UINT2 u2PortIndex, tMacAddr SrcMac);
INT4 PnacEnQRadRespToIntfTask (tPnacAsIfRecvData *pServEapData);

/* Pnac Tacacs+ Functions */

INT4 PnacTacacsplusProcessResponseMd5 (tPnacAsIfSendData *,
                                       tPnacServAuthCache *,
                                       tPnacEapDesc *);

INT4 PnacTacacsplusAuthentication (UINT1 *, 
                                   UINT2 ,
                                   tPnacEapDesc *,
                                   UINT1);

VOID PnacHandleTacacsPlusResponse (VOID *pTacRespRcvd);

/* PNAC Timer functions */

INT4 PnacTmrInit (VOID);

VOID PnacTmrTaskMain (INT1 *);

INT4 PnacTmrHandler ( VOID );

INT4 PnacTmrStartTmr ( VOID *,
                       UINT1 ,
                       UINT4 );

INT4 PnacTmrStopTmr ( VOID *,
                      UINT1 );
                      
VOID PnacTmrDeinit (VOID);

INT4 PnacTmrGetRemainingTime (VOID *, UINT1, UINT4*);
INT4 PnacRestartReauthTimer(UINT4,UINT4,tPnacAuthSessionNode *);

/* Authenticator Functionality functions */

INT4 PnacInitAuthenticator (UINT2 );

VOID PnacAuthSessionNodeInit (tPnacAuthSessionNode *, UINT2 );

VOID PnacAuthSessStatsInit (tPnacAuthSessionNode *, UINT2 );

VOID PnacAuthFsmInfoInit (tPnacAuthFsmInfo *);

INT4 PnacAuthCreateSession (tMacAddr, UINT2,BOOL1);

VOID PnacAuthDeleteSession (tPnacAuthSessionNode *); 

INT4 PnacAuthTxReqId (tMacAddr, UINT2 , UINT2 ,BOOL1);

INT4 PnacAuthTxReq (tMacAddr, 
                    UINT2 , tPnacCruBufChainHdr *,BOOL1);

INT4 PnacAuthHandleInEapFrame ( UINT1 *,
                                UINT2  ,
                                UINT2  ,
                                UINT1  ,
                                tMacAddr,
    UINT2);


INT4 PnacAuthHandleInPortBasedEapFrame ( UINT1 *,
                                         UINT2  ,
                                         UINT1  ,
                                         tPnacPaePortEntry *);


INT4 PnacAuthHandleInMacBasedEapFrame ( UINT1 *,
                                        UINT2  ,
                                        UINT1  ,
                                        tMacAddr,
                                        tPnacPaePortEntry *);

INT4 PnacAuthHandleInRsnPreAuthEapFrame ( UINT1 *,
                                        UINT2  ,
                                        UINT1  ,
                                        tMacAddr,
                                        tPnacPaePortEntry *,
     UINT2);

INT4 PnacAuthHandleInServerFrame (UINT1 *, UINT2, 
                                  UINT2, tMacAddr,
                                  UINT4);

INT4 PnacAuthDisableMacBasedPort(UINT2 u2PortNum);


/* ASM functions*/
INT4 PnacAuthStateMachine ( UINT2 , 
                            UINT2 ,
                            tPnacAuthSessionNode *, 
                            UINT1 *,
                            UINT2 );

INT4 PnacAuthAsmInitInitialize ( UINT2 , 
                                 tPnacAuthSessionNode *,
                                 UINT1 *,
                                 UINT2 );

INT4 PnacAuthAsmPortEnabledInitialize (UINT2 , 
                                       tPnacAuthSessionNode *,
                                       UINT1 *,
                                       UINT2 );

INT4 PnacAuthAsmEapLogoffConnecting ( UINT2 , 
                                      tPnacAuthSessionNode *,
                                      UINT1 *,
                                      UINT2 );

INT4 PnacAuthAsmRxrespConnecting ( UINT2 , 
                                   tPnacAuthSessionNode *,
                                   UINT1 *,
                                   UINT2 );

INT4 PnacAuthAsmEapLogoffAuthenticated ( UINT2 , 
                                         tPnacAuthSessionNode *,
                                         UINT1 *,
                                         UINT2 );

INT4 PnacAuthAsmEapStartAuthenticating ( UINT2 , 
                                         tPnacAuthSessionNode *,
                                         UINT1 *,
                                         UINT2 );

INT4 PnacAuthAsmEapLogoffAuthenticating ( UINT2 , 
                                          tPnacAuthSessionNode *,
                                          UINT1 *,
                                          UINT2 );

INT4 PnacAuthAsmBkendAuthTimeoutAuthenticating ( UINT2 , 
                                                 tPnacAuthSessionNode *,
                                                 UINT1 *,
                                                 UINT2 );
INT4 PnacAuthAsmBkendAuthAbortedAborting ( UINT2 , 
                                           tPnacAuthSessionNode *,
                                           UINT1 *,
                                           UINT2 );

INT4 PnacAuthAsmQwhileExpiredHeld( UINT2 , 
                                   tPnacAuthSessionNode *,
                                   UINT1 *,
                                   UINT2 );

INT4 PnacAuthAsmMakeDisconnected ( UINT2 , 
                                   tPnacAuthSessionNode *,
                                   UINT1 *,
                                   UINT2 );

INT4 PnacAuthAsmMakeAuthenticated ( UINT2 , 
                                    tPnacAuthSessionNode *,
                                    UINT1 *,
                                    UINT2 );

INT4 PnacAuthAsmMakeAuthenticating ( UINT2 , 
                                     tPnacAuthSessionNode *,
                                     UINT1 *,
                                     UINT2 );
INT4 PnacAuthAsmMakeAborting ( UINT2 , 
                               tPnacAuthSessionNode *,
                               UINT1 *,
                               UINT2 );

INT4 PnacAuthAsmMakeConnecting ( UINT2 , 
                                 tPnacAuthSessionNode *,
                                 UINT1 *,
                                 UINT2 );

INT4 PnacAuthAsmIfRemakeConnecting ( UINT2 , 
                                     tPnacAuthSessionNode *,
                                     UINT1 *,
                                     UINT2 );

INT4 PnacAuthAsmMakeHeld ( UINT2 , 
                           tPnacAuthSessionNode *,
                           UINT1 *,
                           UINT2 );

INT4 PnacAuthAsmMakeForceAuthorized ( UINT2 , 
                                      tPnacAuthSessionNode *,
                                      UINT1 *,
                                      UINT2 );

INT4 PnacAuthAsmMakeForceUnauthorized ( UINT2 , 
                                        tPnacAuthSessionNode *,
                                        UINT1 *,
                                        UINT2 );

INT4 PnacAuthAsmPortCntrlChanged ( UINT2 , 
                                   tPnacAuthSessionNode *,
                                   UINT1 *,
                                   UINT2 );

INT4 PnacAuthAsmEventImpossible ( UINT2 , 
                                  tPnacAuthSessionNode *,
                                  UINT1 *,
                                  UINT2 );


/* BSM functions */
INT4 PnacAuthBackendStateMachine ( UINT2 , 
                                   UINT2 ,
                                   tPnacAuthSessionNode *,
                                   UINT1 *,
                                   UINT2,
                                   tPnacCruBufChainHdr *);

INT4 PnacAuthBsmAwhileExpiredRequest (UINT2 , 
                                      tPnacAuthSessionNode *,
                                      UINT1 *,
                                      UINT2,
                                      tPnacCruBufChainHdr *);

INT4 PnacAuthBsmMakeInitialize (UINT2 , 
                                tPnacAuthSessionNode *,
                                UINT1 *,
                                UINT2,
                                tPnacCruBufChainHdr *);
INT4 PnacAuthBsmMakeSuccess (UINT2 , 
                             tPnacAuthSessionNode *,
                             UINT1 *,
                             UINT2,
                             tPnacCruBufChainHdr *);
INT4 PnacAuthBsmMakeFail (UINT2 , 
                          tPnacAuthSessionNode *,
                          UINT1 *,
                          UINT2,
                          tPnacCruBufChainHdr *);
INT4 PnacAuthBsmMakeTimeout (UINT2 , 
                             tPnacAuthSessionNode *,
                             UINT1 *,
                             UINT2,
                             tPnacCruBufChainHdr *);

INT4 PnacAuthBsmMakeResponse (UINT2 , 
                              tPnacAuthSessionNode *,
                              UINT1 *,
                              UINT2,
                              tPnacCruBufChainHdr *);

INT4 PnacAuthBsmMakeRequest ( UINT2 , 
                              tPnacAuthSessionNode *,
                              UINT1 *,
                              UINT2,
                              tPnacCruBufChainHdr *);

INT4 PnacAuthBsmAreqRcvdResponse ( UINT2 , 
                              tPnacAuthSessionNode *,
                              UINT1 *,
                              UINT2,
                              tPnacCruBufChainHdr *);

INT4 PnacAuthBsmEventImpossible (UINT2 , 
                                 tPnacAuthSessionNode *,
                                 UINT1 *,
                                 UINT2,
                                 tPnacCruBufChainHdr *);

/* Server Interface Functions */
VOID PnacAsIfRecvFromServer (tPnacAsIfRecvData   *);

INT4 PnacRadiusTxEapPkt (tPnacAsIfSendData *);

/* Supplicant Functionality functions */

INT4 PnacInitSupplicant (UINT2 ); 

VOID PnacSuppSessionNodeInit(tPnacSuppSessionNode *, UINT2 );

INT4 PnacSuppHandleInEapFrame (UINT1 *, 
                               UINT2  ,
                               UINT2  , 
                               UINT1  ,
                               tMacAddr);

INT4 PnacSuppTxStart (tMacAddr, UINT2 );

INT4 PnacSuppTxLogoff (tMacAddr, UINT2 );

INT4 PnacSuppTxRespId (tMacAddr, UINT1 , UINT2 );

INT4 PnacSuppTxRespAuth (UINT1 *,
                         UINT2 ,
                         tMacAddr, 
                         UINT1 , 
                         UINT2 );

INT4 PnacSuppLinkStatusUpdate (tPnacPaePortEntry *);


/* SSM functions */
INT4 PnacSuppStateMachine (UINT2 , 
                           UINT2 ,
                           tPnacSuppSessionNode *, 
                           UINT1 *, 
                           UINT2 );

INT4 PnacSuppSmInitialize(UINT2 , tPnacSuppSessionNode *,
                          UINT1 *, UINT2 );

INT4 PnacSuppSmPortEnabled (UINT2 , tPnacSuppSessionNode *,
                          UINT1 *, UINT2 );

INT4 PnacSuppSmUserLoggedOff (UINT2 , tPnacSuppSessionNode *,
                          UINT1 *, UINT2 );

INT4 PnacSuppSmUserLoggedIn (UINT2 , tPnacSuppSessionNode *, 
                          UINT1 *, UINT2 );
                          
INT4 PnacSuppSmStartWhenExpiredConnecting (UINT2 , tPnacSuppSessionNode *, 
                          UINT1 *, UINT2 );
                                       
INT4 PnacSuppSmMakeDisconnected (UINT2 , tPnacSuppSessionNode *,
                          UINT1 *, UINT2 );

INT4 PnacSuppSmMakeConnecting (UINT2 , tPnacSuppSessionNode *,
                          UINT1 *, UINT2 );


INT4 PnacSuppSmMakeAuthenticated (UINT2 , tPnacSuppSessionNode *,
                          UINT1 *, UINT2 );
                               
INT4 PnacSuppSmMakeAcquired (UINT2 , tPnacSuppSessionNode *,
                          UINT1 *, UINT2 );

INT4 PnacSuppSmMakeAuthenticating (UINT2 , tPnacSuppSessionNode *,
                          UINT1 *, UINT2 );

INT4 PnacSuppSmMakeHeld (UINT2 , tPnacSuppSessionNode *,
                          UINT1 *, UINT2 );

INT4 PnacSuppSmMakeLogoff (UINT2 , tPnacSuppSessionNode *,
                          UINT1 *, UINT2 );
                        
INT4 PnacSuppSmEventImpossible (UINT2 , tPnacSuppSessionNode *,
                          UINT1 *, UINT2 );
INT4 PnacSuppSmMakeForceAuthorized (UINT2 ,tPnacSuppSessionNode *, UINT1 *,UINT2);
INT4 PnacSuppSmMakeForceUnauthorized (UINT2 ,tPnacSuppSessionNode *, UINT1 *,UINT2);
INT4 PnacSuppSmPortCntrlChanged (UINT2 , tPnacSuppSessionNode *, UINT1 *, UINT2 );
/* Utility functions */

VOID PnacCpyMacAddrToStr (UINT1 *, tMacAddr);

INT4
PnacChkSuppMacIsUniqueMacForPort (tMacAddr macAddr, UINT2 u2PortNum);

VOID PnacFreeMemForInterface PROTO ((tRadInterface *));

/* Pnacslow.c utility routines */
INT4 PnacSnmpLowValidatePortNumber  PROTO ((INT4 i4Dot1xPaePortNumber));

INT4 PnacSnmpLowGetFirstValidPortNumber  PROTO ((INT4 *pi4Dot1xPaePortNumber));

INT4 PnacSnmpLowGetNextValidPortNumber  PROTO (( INT4  i4Dot1xPaePortNumber,
                                                 INT4 *pi4Dot1xPaePortNumber ));

INT4 PnacSnmpLowGetPortInfo  PROTO (( INT4 i4Dot1xPaePortNumber,
                                       tPnacPaePortEntry  **ppPortEntry ));

INT4 PnacSnmpLowValidateSuppCapPortNumber PROTO ((INT4 i4Dot1xPaePortNumber));

INT4 PnacSnmpLowGetFirstSuppCapValidPortNumber PROTO ((INT4 *pi4Dot1xPaePortNumber));

INT4 PnacSnmpLowGetNextSuppCapValidPortNumber PROTO ((
                                         INT4 i4Dot1xPaePortNumber,
                                         INT4 *pi4Dot1xPaePortNumber));

INT4
PnacSnmpLowGetNextPortBasedValidPortNumber PROTO ((INT4 i4Dot1xPaePortNumber,
                                   INT4 *pi4Dot1xPaePortNumber));
/* Pnacplow.c utility routines */
INT4 PnacSnmpLowValidateSuppAddress PROTO ((tMacAddr AuthSessionSuppAddress));

INT4 PnacSnmpLowGetFirstValidSuppAddress  PROTO ((tMacAddr pFsPnacAuthSessionSuppAddress));

INT4 PnacSnmpLowGetNextValidSuppAddress  PROTO ((
                                  tMacAddr AuthSessionSuppAddress,
                                  tMacAddr* pFsPnacAuthSessionSuppAddress ));


INT4 PnacSnmpLowGetAuthSessionNode PROTO (( tMacAddr AuthSessionSuppAddress,
                                    tPnacAuthSessionNode  **ppSessNode ));
INT4 PnacSnmpLowServGetNode PROTO ((tSNMP_OCTET_STRING_TYPE *pOctets, 
                                    tPnacServUserInfo **ppUserNode));

INT4 PnacSnmpLowServGetNodeByName PROTO ((tSNMP_OCTET_STRING_TYPE *pOctets,
                                   tPnacServUserInfo **ppUserNode));

INT4 PnacSnmpLowServCreateUserRecord PROTO ((tSNMP_OCTET_STRING_TYPE *
                                      pFsPnacASUserConfigUserName,
                                      tPnacServUserInfo **ppUserRecord));
INT4 PnacAuthPortOperIndication PROTO ((UINT2 u2PortNum));

VOID 
PnacRedSyncUpDynamicInfo (tPnacPaePortEntry *pPortInfo, 
                          tPnacAuthSessionNode *pAuthSessNode,
                          tPnacSuppSessionNode *pSuppSessNode);
INT4 PnacGetHLPortStatus (UINT2 u2PortIndex, UINT1 *pu1PortState);

/* Handling of Mac session for WLAN requirements. */
VOID PnacCreateAndAuthorizeMacSesson (UINT2 u2PortNum, tMacAddr SrcMacAddr);
VOID PnacReStartMacSession (UINT2 u2PortNum, tMacAddr SrcMacAddr);
VOID PnacDeAuthorizeMacSession (UINT2 u2PortNum, tMacAddr SrcMacAddr);
#ifdef MBSM_WANTED
/* MBSM Functions. */

INT4 PnacMbsmUpdateCardInsertion PROTO (( tMbsmPortInfo *, tMbsmSlotInfo *));

#endif

/* pnacl2wr.c */
INT4 PnacL2IwfSetPortPnacAuthControl PROTO ((UINT2 u2IfIndex, 
                                             UINT2 u2PortAuthControl));

INT4 PnacL2IwfSetPortPnacAuthMode PROTO ((UINT2 u2IfIndex, 
                                          UINT2 u2PortAuthMode));

INT4 PnacL2IwfSetPortPnacPaeStatus PROTO ((UINT2 u2IfIndex, 
                                          UINT1 u1PortPaeStatus));

INT4 PnacL2IwfSetPortPnacAuthStatus PROTO ((UINT2 u2IfIndex, 
                                            UINT2 u2PortAuthStatus));

INT4 PnacL2IwfSetPortPnacControlDir PROTO ((UINT2 u2IfIndex, 
                                            UINT2 u2PortControlDir));

INT4 PnacL2IwfSetSuppMacAuthStatus PROTO ((tMacAddr SuppMacAddr, 
                                           UINT2 u2MacAuthStatus));

INT4 PnacL2IwfGetPortPnacControlDir PROTO ((UINT2 u2IfIndex, 
                                            UINT2 *pu2PortControlDir));

INT4 PnacL2IwfGetPortPnacAuthStatus PROTO ((UINT2 u2IfIndex, 
                                            UINT2 *pu2PortAuthStatus));

INT4 PnacL2IwfGetPortPnacPaeStatus PROTO ((UINT2 u2IfIndex, 
                                            UINT1 *pu1PortPaeStatus));
INT4
PnacL2IwfGetVlanPortPvid  PROTO ((UINT2 u2IfIndex, UINT2 *pu2VlanId));

/* pnacport.c */
VOID PnacWlanRxKey PROTO ((tPnacKeyInfo *, tMacAddr, UINT2));

VOID PnacWlanCryptStoreKey PROTO ((UINT1 *, UINT2, UINT2));

VOID PnacWlanFillKeyDesc PROTO ((UINT1 *pu1Key, UINT2 u2KeyLen, UINT2 u2PortNum,
                          tMacAddr pMacAddr, tPnacKeyDesc *pKeyDesc));

UINT4 PnacWlanNtfyAuthFailure PROTO ((tMacAddr SrcMac));

INT4 PnacRadiusAuthentication PROTO ((tRADIUS_INPUT_AUTH * p_RadiusInputAuth,
                                      tRADIUS_SERVER * p_RadiusServerAuth,
                                      VOID (*CallBackfPtr) (VOID *),
                                      UINT4 ifIndex, UINT1 u1ReqId));

INT4 PnacAstIsMstEnabledInContext PROTO ((UINT4 u4ContextId));

INT4 PnacAstIsRstEnabledInContext PROTO ((UINT4 u4ContextId));

INT4 PnacCfaCliConfGetIfName PROTO ((UINT4 u4IfIndex, INT1 *pi1IfName));

INT4 PnacCfaCliGetIfName PROTO ((UINT4 u4IfIndex, INT1 *pi1IfName));

INT4 PnacCfaGetIfInfo PROTO ((UINT4 u4IfIndex, tCfaIfInfo * pIfInfo));

INT4 PnacLaIsLaStarted PROTO ((VOID));

INT4 PnacVcmGetContextInfoFromIfIndex PROTO ((UINT4 u4IfIndex, 
                                              UINT4 *pu4ContextId,
                                              UINT2 *pu2LocalPortId));

INT4 PnacL2IwfGetNextValidPort PROTO ((UINT2 u2IfIndex, UINT2 *pu2NextPort));

INT4 PnacL2IwfGetPortOperEdgeStatus PROTO ((UINT2 u2IfIndex, 
                                            BOOL1 * pbOperEdge));

INT4 PnacL2IwfGetPortOperStatus PROTO ((INT4 i4ModuleId, UINT2 u2IfIndex,
                                        UINT1 *pu1OperStatus));

VOID PnacL2IwfGetProtocolTunnelStatusOnPort PROTO ((UINT4 u4IfIndex, 
                                                    UINT2 u2Protocol,
                                                    UINT1 *pu1TunnelStatus));

INT4 PnacL2IwfHandleOutgoingPktOnPort PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                              UINT2 u2IfIndex, UINT4 u4PktSize,
                                              UINT2 u2Protocol, UINT1 u1Encap));

INT4 PnacL2IwfIsPortInPortChannel PROTO ((UINT4 u4IfIndex));

INT4 PnacL2IwfPnacHLPortOperIndication PROTO ((UINT2 u2IfIndex, 
                                               UINT1 u1OperStatus));

#ifdef L2RED_WANTED
#ifdef LA_WANTED
INT4 PnacLaRedRcvPktFromRm PROTO ((UINT1 u1Event, tRmMsg * pData, 
                                   UINT2 u2DataLen));

#else
#if defined(RSTP_WANTED) || defined(MSTP_WANTED)
UINT4 PnacAstHandleUpdateEvents PROTO ((UINT1 u1Event, tRmMsg * pData, 
                                        UINT2 u2DataLen));

#else
#ifdef VLAN_WANTED
UINT4 PnacVlanRedHandleUpdateEvents PROTO ((UINT1 u1Event, tRmMsg * pData, 
                                            UINT2 u2DataLen));
#else
#ifdef LLDP_WANTED
UINT4 PnacLldpRedRcvPktFromRm PROTO ((UINT1 u1Event, tRmMsg * pData, 
                                      UINT2 u2DataLen));
#endif
#endif
#endif
#endif

UINT4
PnacRmEnqMsgToRmFromAppl PROTO ((tRmMsg * pRmMsg, UINT2 u2DataLen, 
                                 UINT4 u4SrcEntId, UINT4 u4DestEntId));

UINT4
PnacRmGetNodeState PROTO ((VOID));

UINT1
PnacRmGetStandbyNodeCount PROTO ((VOID));

UINT1
PnacRmGetStaticConfigStatus PROTO ((VOID));

UINT1
PnacRmHandleProtocolEvent PROTO ((tRmProtoEvt *pEvt));

UINT4
PnacRmRegisterProtocols PROTO ((tRmRegParams * pRmReg));

UINT4
PnacRmDeRegisterProtocols PROTO ((VOID));

UINT4
PnacRmReleaseMemoryForMsg PROTO ((UINT1 *pu1Block));

INT4
PnacRmSetBulkUpdatesStatus PROTO ((UINT4 u4AppId));
#endif
VOID
PnacProcNpCallBack PROTO ((tPnacIntfMesg *pMesg));
VOID 
PnacProcNpSessStatusCb PROTO ((UINT2 u2Port, tMacAddr SuppMacAddr,
           UINT1 u1PnacMacSessStatus, INT4 i4NpRetVal));
VOID
PnacProcNpAuthCbPortBasedSuccess PROTO ((UINT2 u2Port, UINT1 u1PnacPortCtrlDir,
                 UINT1 u1PnacPortAuthStatus));
VOID
PnacProcNpAuthCbPortBasedFailure PROTO ((UINT2 u2Port, UINT1 u1PnacPortCtrlDir,
           UINT1 u1PnacPortAuthStatus));
VOID
PnacProcNpAuthCbMacBased PROTO ((UINT2 u2Port, tMacAddr SuppMacAddr,
         UINT1 u1PnacMacAuthStatus, INT4 i4NpRetVal));
VOID 
PnacNpSetAuthStatusRetry PROTO ((UINT2 u2Port, tMacAddr SuppMacAddr,
         UINT1 u1PnacPortCtrlDir, UINT1 u1PnacAuthMode,
         UINT1 u1PnacAuthStatus));
VOID
PnacNpSetMacSessStatusRetry PROTO ((UINT2 u2Port, tMacAddr SuppMacAddr,
         UINT1 u1PnacSessStatus));

INT4
PnacCompareFailedMacAddr PROTO ((tRBElem * e1, tRBElem * e2));
INT4
PnacGetNpFailedMacSessNodeByMac PROTO ((tMacAddr macAddr, 
              tPnacNpFailedMacSessNode 
            **ppNpFailedMacSessNode));
tPnacNpFailedMacSessNode * 
PnacAddToNpFailedMacSessTree PROTO ((tMacAddr macAddr, UINT1 u1PnacAsyncNpRetryCount));
INT4
PnacDelFromNpFailedMacSessTree PROTO ((tPnacNpFailedMacSessNode *pNpFailedMacSessNode));

VOID
PnacNpFailLog PROTO ((tPnacNpTrapLogInfo *pPnacNpTrapLogInfo));
VOID
PnacNpFailNotify PROTO ((tPnacNpTrapLogInfo *pPnacNpTrapLogInfo));

VOID
PnacNpFailNotifyAndLog PROTO ((UINT2 u2Port, UINT1 *pSuppMacAddr,
          UINT1 u1PnacAuthCtrlDir, UINT1 u1PnacAuthMode,
          UINT1 u1PnacAuthStatus));

INT4
PnacNpGetNpapiMode PROTO ((INT4 i4ProtoId));

INT1
PnacGetCurrentPortStatus PROTO ((UINT2 u2PortNum));
 
INT4
PnacReAuthOnVlanPropChange PROTO ((tVlanChangeInfo * pVlanChangeInfo));

/* DPNAC releated functions */

INT4
DPnacPaeTxEapPkt (tPnacDPnacPktInfo *pDPnacPktInfo);

INT4
DPnacSendSlaveRespToServer (UINT1 *pu1EapPkt,
                      UINT2 u2EapPktLength);

VOID
DPnacPaeConstructEapolHdr (UINT1 *pu1EapolPktBuf);

INT4
DPnacEventUpdatePaeTxEapPkt  PROTO ((UINT2 u2PortNum));

INT4
DPnacProcessEapPkt(UINT1 *pu1ReadPtr,UINT2 u2EapolBodyLen,UINT2 u2PortNum);

INT4
DPnacCmpPort (tRBElem * pPnacDPnacConsPortEntryNode1,
                   tRBElem * pPnacDPnacConsPortEntryNode2);

tRBTree
PnacRBTreeCreateEmbedded (UINT4 u4Offset, tRBCompareFn Cmp);

VOID
DPnacInfoTreeDestroy (tRBTree Tree, tRBKeyFreeFn FreeFn, UINT4 u4Arg);

INT4 DPnacHandlePeriodicSyncOrEventPdu(UINT1* pu1PktPtr,UINT1 u1PktType,UINT2 u2PortNum);

INT4
DPnacHandleBridgeDetectionFrame (UINT1 *pu1PktPtr);

VOID
DPnacSendServRespToSlave(tPnacAsIfRecvData   *);

INT4 
DPnacHandleServResponse(UINT1 *pu1EapPkt,UINT2 u2EapPktLength);

INT4
DPnacCreateConsPortEntry(tPnacDPnacConsPortEntry ** ppConsPortEntry,
                                 UINT2 u2PortIndex,UINT1 u1RemotePortStatus);

INT4
DPnacAddToConsPortListTree (tPnacDPnacConsPortEntry * pConsPortEntry,UINT4 u4SlotId);


INT4
DPnacGetPortEntry (tPnacDPnacConsPortEntry **ppConsPortEntry,UINT2 u2RemotPortIndex,
                             UINT4 u4SlotId);
INT4
DPnacGetNextPortEntry (tPnacDPnacConsPortEntry *pConsPortEntry,tPnacDPnacConsPortEntry **ppNextConsPortEntry,UINT4 u4SlotId);

INT4
DPnacRemovePortEntryFromConsTree (tPnacDPnacConsPortEntry * pConsPortEntry,UINT4 u4SlotId);

INT4
DPnacDeleteConsPortEntry(tPnacDPnacConsPortEntry *pConsPortEntry);

INT4
DPnacCreateSlotInfo( tPnacDPnacSlotInfo **ppPnacDPnacSlotInfo,UINT4 u4SlotId);

INT4
DPnacAddSlotToTable(tPnacDPnacSlotInfo *pPnacDPnacSlotInfo);

INT4 DPnacGetSlotInfo(tPnacDPnacSlotInfo **ppDPnacSlotInfo,UINT4 u4SlotId);

INT4 DPnacGetNextSlotInfo(tPnacDPnacSlotInfo *pDPnacSlotInfo,tPnacDPnacSlotInfo **ppNextDPnacSlotInfo);

INT4 DPnacDeleteSlotInfo (tPnacDPnacSlotInfo *pDPnacSlotInfo);

INT4
DPnacTxEnetFrame (tPnacDPnacPktInfo *pDPnacPktInfo);

INT4 DPnacHandleKeepAlive (UINT1 u1Trigger);

INT4 DPnacUpdateHlAndHw(UINT2 u2PortNum,UINT1 u1PortStatus,UINT1 u1PortControlDir,UINT1 u1PortCtrlMode);

INT4 DPnacMasterUpdateEntries(UINT2 u2PortNum);

INT4 DPnacMasterDownIndication PROTO ((VOID));

INT4 DPnacMasterUpIndication PROTO ((VOID));

INT4 DPnacSlotDetach (UINT4 u4SlotId);

INT4
DPnacGetSlotIdFromPort(UINT2 u2IfIndex, UINT4 *pu4SlotId);

INT4 
DPnacSetModuleStatus(UINT1 u1ModuleStatus);

INT4 
DPnacGetDistributionPort(UINT2 *pDistPortNum);

INT4
DPnacGetMasterSlotId(UINT4 *pu4SlotId);

INT4
DPnacIsConnectingPort (UINT4 u4IfIndex);

INT4 
DPnacGetSlotIdFromConnectingPort(UINT4 u4IfIndex, UINT4 *pu4SlotId);

INT4 
DPnacGetDistributionPortFromSlotId(UINT4 u4SlotId, UINT4 *pu4IfIndex);

INT4
DPnacGetIfIndexFromBridgeIndex (UINT4 u4BrgIndex, UINT4 *pu4IfIndex);

INT4
DPnacGetBridgeIndexFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4BrgIndex);

INT4
DPnacCheckIsLocalPort(UINT2 u2PortNum);

INT4 DPnacSendEventUpdateMsg(UINT2 u2PortNum);

INT4
DPnacGetSlotStatus(UINT4 u4SlotId);

INT4 DPnacSendAllPortInfoToMaster (VOID);

#endif /* _PNACPROT_H */
