#ifndef _STDPNAWR_H
#define _STDPNAWR_H

VOID RegisterSTDPNA(VOID);

VOID UnRegisterSTDPNA(VOID);
INT4 Dot1xPaeSystemAuthControlGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xPaeSystemAuthControlSet(tSnmpIndex *, tRetVal *);
INT4 Dot1xPaeSystemAuthControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1xPaeSystemAuthControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 GetNextIndexDot1xPaePortTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1xPaePortProtocolVersionGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xPaePortCapabilitiesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xPaePortInitializeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xPaePortReauthenticateGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xPaePortInitializeSet(tSnmpIndex *, tRetVal *);
INT4 Dot1xPaePortReauthenticateSet(tSnmpIndex *, tRetVal *);
INT4 Dot1xPaePortInitializeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1xPaePortReauthenticateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1xPaePortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexDot1xAuthConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1xAuthPaeStateGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthBackendAuthStateGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthAdminControlledDirectionsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthOperControlledDirectionsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthAuthControlledPortStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthAuthControlledPortControlGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthQuietPeriodGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthTxPeriodGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthSuppTimeoutGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthServerTimeoutGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthMaxReqGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthReAuthPeriodGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthReAuthEnabledGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthKeyTxEnabledGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthAdminControlledDirectionsSet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthAuthControlledPortControlSet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthQuietPeriodSet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthTxPeriodSet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthSuppTimeoutSet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthServerTimeoutSet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthMaxReqSet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthReAuthPeriodSet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthReAuthEnabledSet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthKeyTxEnabledSet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthAdminControlledDirectionsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthAuthControlledPortControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthQuietPeriodTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthTxPeriodTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthSuppTimeoutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthServerTimeoutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthMaxReqTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthReAuthPeriodTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthReAuthEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthKeyTxEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);










INT4 GetNextIndexDot1xAuthStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1xAuthEapolFramesRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthEapolFramesTxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthEapolStartFramesRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthEapolLogoffFramesRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthEapolRespIdFramesRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthEapolRespFramesRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthEapolReqIdFramesTxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthEapolReqFramesTxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthInvalidEapolFramesRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthEapLengthErrorFramesRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthLastEapolFrameVersionGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthLastEapolFrameSourceGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot1xAuthDiagTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1xAuthEntersConnectingGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthEapLogoffsWhileConnectingGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthEntersAuthenticatingGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthAuthSuccessWhileAuthenticatingGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthAuthTimeoutsWhileAuthenticatingGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthAuthFailWhileAuthenticatingGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthAuthReauthsWhileAuthenticatingGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthAuthEapStartsWhileAuthenticatingGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthAuthEapLogoffWhileAuthenticatingGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthAuthReauthsWhileAuthenticatedGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthAuthEapStartsWhileAuthenticatedGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthAuthEapLogoffWhileAuthenticatedGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthBackendResponsesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthBackendAccessChallengesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthBackendOtherRequestsToSupplicantGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthBackendNonNakResponsesFromSupplicantGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthBackendAuthSuccessesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthBackendAuthFailsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot1xAuthSessionStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1xAuthSessionOctetsRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthSessionOctetsTxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthSessionFramesRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthSessionFramesTxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthSessionIdGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthSessionAuthenticMethodGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthSessionTimeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthSessionTerminateCauseGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xAuthSessionUserNameGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot1xSuppConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1xSuppPaeStateGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppHeldPeriodGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppAuthPeriodGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppStartPeriodGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppMaxStartGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppControlledPortStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppAccessCtrlWithAuthGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppHeldPeriodSet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppAuthPeriodSet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppStartPeriodSet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppMaxStartSet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppAccessCtrlWithAuthSet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppHeldPeriodTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppAuthPeriodTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppStartPeriodTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppMaxStartTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppAccessCtrlWithAuthTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);





INT4 GetNextIndexDot1xSuppStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1xSuppEapolFramesRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppEapolFramesTxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppEapolStartFramesTxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppEapolLogoffFramesTxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppEapolRespIdFramesTxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppEapolRespFramesTxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppEapolReqIdFramesRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppEapolReqFramesRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppInvalidEapolFramesRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppEapLengthErrorFramesRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppLastEapolFrameVersionGet(tSnmpIndex *, tRetVal *);
INT4 Dot1xSuppLastEapolFrameSourceGet(tSnmpIndex *, tRetVal *);
#endif /* _STDPNAWR_H */
