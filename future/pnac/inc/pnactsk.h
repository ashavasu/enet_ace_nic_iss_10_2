/* $Id: pnactsk.h,v 1.14 2015/06/11 10:03:14 siva Exp $ */
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : pnactsk.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : PNAC Stubs                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 22 May 2003                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains type definitions and enums  */
/*                            used for communication with PNAC interface task*/
/*                            and external modules.                          */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    22 APR 2002 / BridgeTeam   Initial Create.                     */
/*---------------------------------------------------------------------------*/
#ifndef _PNAC_TASK_H
#define _PNAC_TASK_H

/* Interface Mesage Types */



/* Common Interface Message Type definition */

typedef struct PnacIntfMesg {
    
    UINT4       u4MesgType; /* enums tPnacIntfMesgTypes */

    union {

        struct PortInformation {
            UINT2   u2PortIndex;
            UINT2   u2AuthControl;
            UINT2   u2AuthMode;
            UINT2   u2CurrentVlanId;
            UINT1   u1OperStatus;
            UINT1   au1Reserved[3];
        }PortInfo;

        
        struct ProtocolFrame {

            void    *pFrame;
            UINT2   u2Length;
            UINT2   u2PortIndex;
            UINT1   u1FrameType;
            UINT1   u1Reserved[3];
        }EapolFrame;

        struct ServerResponse {

            void  *pFrame;
            UINT2 u2RespType;
            UINT2 u2Length;
        }AuthSrvResp;

        struct PnacControldirectionSm {
            UINT4 u4Event;
            UINT2 u2PortIndex;
            UINT2 u2Pad;
 }PnacCdsm;

        struct _MacSessionFrame {
            UINT2    u2PortIndex;
            tMacAddr MacAddr;
        }MacSessionFrame;

#ifdef MBSM_WANTED
        struct MbsmIndication {
            tMbsmProtoMsg *pMbsmProtoMsg;
        }MbsmCardUpdate;
#endif
        struct RmToPnacMsg {
            VOID    *pData;
            UINT2   u2DataLen;
            UINT1   u1SubMsgType;
            UINT1   au1Reserved[1];
        } RmPnacMsg;

        struct PnacKeyAvail {                 
           VOID  *pPaePortInfo;                   
           VOID  *pPaeAuthSessNode;
           UINT1 au1Key[PNAC_MAX_SERVER_KEY_SIZE];
        } PnacKeyAvailable;

        VOID *pApmeToRsnaMsg;
        VOID *pApmeToRsnaMlmeMsg;
 VOID *pApmeToRsnaConfMsg;
 VOID *pApmeToRsnaChgConfMsg;
     
        struct _PnacNpCallBack {
        UINT4      u4NpCallId;
        INT4       rval;
        UINT2      u2Port;
               tMacAddr   SuppMacAddr;
        UINT1      u1AuthMode;
        UINT1      u1AuthStatus;
        UINT1      u1CtrlDir;
        UINT1      au1Reserved[1];
 } PnacNpCallBackMsg;
     }Mesg;

}tPnacIntfMesg;

#endif /* _PNAC_TASK_H */
