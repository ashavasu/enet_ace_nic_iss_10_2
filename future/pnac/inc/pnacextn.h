/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/*$Id: pnacextn.h,v 1.14 2011/09/08 07:22:07 siva Exp $                       */
/*****************************************************************************/
/*    FILE  NAME            : pnacextn.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : PNAC module                                    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Mar 2002                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains external declarations used  */
/*                            in PNAC module.                                */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 MAR 2002 / BridgeTeam   Initial Create.                     */
/* 1.0.0.1    29 NOV 2002 / Products     Modifying NP-API calls              */
/*---------------------------------------------------------------------------*/
#ifndef _PNACEXTN_H
#define _PNACEXTN_H

extern VOID Md5GetKeyDigest(UINT1 *, UINT2, UINT1 * );


#endif /*_PNACEXTN_H */
