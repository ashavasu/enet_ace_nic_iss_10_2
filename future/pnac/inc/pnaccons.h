/* $Id: pnaccons.h,v 1.34 2015/12/14 10:59:16 siva Exp $*/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                 */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : pnaccons.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : PNAC module                                    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Mar 2002                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains defined constants used      */
/*                            by PNAC Module.                                */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 MAR 2002 / BridgeTeam   Initial Create.                     */
/* 1.0.0.1    22 APR 2002 / BridgeTeam   Changed for including Radius        */
/*                                       Port Type Attributes                */
/*---------------------------------------------------------------------------*/

#ifndef _PNACCONST_H
#define _PNACCONST_H


/* Bridge related defines */

#define   PNAC_START          1
#define   PNAC_SHUTDOWN       2

#define   PNAC_ENABLED        1
#define   PNAC_DISABLED       2

#define   PNAC_CENTRALIZED    1 
#define   PNAC_DISTRIBUTED    2 

#define   PNAC_MAX_MEMPOOLS   3
#define   PNAC_NUM_MEMTYPES   0

#define   PNAC_MINPORTS             1
#define   PNAC_MAXPORT_INFO         PNAC_MAXPORTS + 1
#define   PNAC_MINSUPP                        1

#define   PNAC_MIN_SESS_IN_PROGRESS 0
#define   PNAC_DEF_VLAN_ID          1


#define   PNAC_MAX_SESSION_TMRS    7
#define   PNAC_MAX_TMR_BLOCKS      (PNAC_MAX_SESSION_TMRS * PNAC_MAX_AUTHSESS)
#define   PNAC_SESS_TABLE_SIZE      256

#define   PNAC_MAX_SESS_ID          255
#define   PNAC_MIN_SESS_ID          0
#define   PNAC_MAX_SESS_ID_COUNT    PNAC_MAX_SESS_ID + 1

#define   PNAC_INIT_VAL             0
#define   PNAC_NO_VAL               0
#define   PNAC_OFFSET_NONE          0
#define   PNAC_PROTOCOL_TYPE        0

#define   PNAC_DEFAULT_PERIODIC_SYNC_TIME 60
#define   PNAC_DEFAULT_MAX_KEEP_ALIVE_COUNT    3


#define   PNAC_DPNAC_SYSTEM_ROLE_NONE    0
#define   PNAC_DPNAC_SYSTEM_ROLE_MASTER  1
#define   PNAC_DPNAC_SYSTEM_ROLE_SLAVE   2

#define  PNAC_DPNAC_LOCAL_PORT 1
#define  PNAC_DPNAC_REMOTE_PORT 2

#define PNAC_DPNAC_KEEP_ALIVE_TIMER_EXPIRY 1
#define PNAC_DPNAC_KEEP_ALIVE_CONFIG 2

/* Ethernet Related defines */
#define   PNAC_RAD_ENET_PORT        15
#define   PNAC_RAD_WLAN_PORT        19

#define   PNAC_MAX_ENET_FRAME_SIZE         1518 /* Bytes */
#define   PNAC_MAX_TAGGED_ENET_FRAME_SIZE  1522 /* Bytes */
#define   PNAC_DESTADDR_OFFSET             0
#define   PNAC_SRCADDR_OFFSET              6
#define   PNAC_ENET_HDR_SIZE               14 /* Bytes */
#define   PNAC_VLAN_TAGGED_HEADER_OFFSET   16 /* Bytes */

#define   PNAC_TAG_ENET_TYPE        0x8100
#define   PNAC_TAG_CFI_MASK         0x1000

#define   PNAC_EAPOL_HI_ADDR        0x0180c200
#define   PNAC_EAPOL_LOW_ADDR       0x0003


#define   PNAC_MAC_ADDR_SIZE        6 /* Bytes */
#define   PNAC_MAC_HI_ADDR_SIZE     4 /* Bytes */
#define   PNAC_MAC_LOW_ADDR_SIZE    2 /* Bytes */


#define   PNAC_MAC_BCAST_HI_ADDR    0xffffffff
#define   PNAC_MAC_BCAST_LOW_ADDR   0xffff

#define   PNAC_MAC_MCAST_MASK       0x01000000


#define   PNAC_EAPOL_HDR_SIZE       4 /* Bytes */

#define   PNAC_ENET_EAPOL_HDR_SIZE (PNAC_ENET_HDR_SIZE+PNAC_EAPOL_HDR_SIZE)

#define   PNAC_MAX_EAP_DATA_SIZE (PNAC_MAX_ENET_FRAME_SIZE - \
                                  PNAC_ENET_EAPOL_HDR_SIZE - \
                                  PNAC_EAP_HDR_SIZE)

#define PNAC_IDLE_NODE                             RM_INIT
#define PNAC_ACTIVE_NODE                           RM_ACTIVE
#define PNAC_STANDBY_NODE                          RM_STANDBY
#define PNAC_ACTIVE_TO_STANDBY_IN_PROGRESS         4
#define PNAC_SHUT_START_IN_PROGRESS                5

#define   PNAC_EAP_CODE_SIZE           1
#define   PNAC_EAP_TYPE_SIZE           1

/* Constants for EAP_AKA and EAP_SIM */
#define   PNAC_EAP_SUBTYPE_SIZE      1
#define   PNAC_EAP_RESERVED_SIZE  2
#define   PNAC_EAP_AT_IDENTITY_SIZE   1
#define   PNAC_EAP_AT_LENGTH_SIZE     1
#define   PNAC_EAP_AT_ACTUAL_LEN_SIZE  2

/* EAPOL packet type related macros */

#define   PNAC_EAP_PKT                 0x0
#define   PNAC_EAPOL_START             0x1
#define   PNAC_EAPOL_LOGOFF            0x2
#define   PNAC_EAPOL_KEY               0x3
#define   PNAC_EAPOL_ASF_ALERT         0x4


#define   DPNAC_PKT_TYPE                 0xFF

#define   DPNAC_PORT_INFO_LENGTH   6
#define   DPNAC_PORT_NUMS_FIELD_LENGTH    2

/* EAP Pkt Code macros */

#define   PNAC_EAP_CODE_REQ             0x1
#define   PNAC_EAP_CODE_RESP            0x2
#define   PNAC_EAP_CODE_SUCCESS         0x3
#define   PNAC_EAP_CODE_FAILURE         0x4

#define   DPNAC_PERIODIC_SYNC_PDU        0x11
#define   DPNAC_EVENT_UPDATE_PDU         0x12
#define   DPNAC_BRIDGE_STATUS_PDU        0x13

/* EAP Pkt Type macros *
 *
 */
#ifdef WPS_WANTED
#define WSC_ID_ENROLLEE "WFA-SimpleConfig-Enrollee-1-0"
#define WSC_ID_ENROLLEE_LEN 29
#define WSC_VENDOR_ID      0x00372A
#define WSC_VENDOR_ID_LEN   3
#define WSC_VENDOR_TYPE     0x00000001
#define WSC_VENDOR_TYPE_LEN 4
#define   PNAC_EAP_TYPE_EXTENDED        254
#endif


#define   PNAC_EAP_TYPE_IDENTITY        1
#define   PNAC_EAP_TYPE_NOTIFICATION    2
#define   PNAC_EAP_TYPE_NAK             3
#define   PNAC_EAP_TYPE_MD5CHALLENGE    4
/* EAP_AKA and EAP_SIM */
#define   PNAC_EAP_TYPE_AKA_IDENTITY    23
#define   PNAC_EAP_TYPE_SIM_IDENTITY    18

/* EAP_AKA and EAP_SIM SUBTYPES */
#define   PNAC_EAP_AKA_SUBTYPE_CHALLENGE 1
#define   PNAC_EAP_AKA_SUBTYPE_IDENTITY  5
#define   PNAC_EAP_SIM_SUBTYPE_IDENTITY  10


/* Defines used by authenticator and supplicant */
/* Note : These defines must be multiples of word size (i.e. 4 bytes) */
#define   PNAC_SUPP_PASSWD_SIZE         20


#define   PNAC_MIN_LEN_USERNAME         4
#define   PNAC_MIN_LEN_PASSWD           4
#define   PNAC_MAX_LEN_USERNAME         PNAC_USER_NAME_SIZE
#define   PNAC_MAX_LEN_PASSWD           PNAC_SUPP_PASSWD_SIZE


/* PNAC  Protocol Default value macros 
 */

#define   PNAC_QUIETPERIOD              60
#define   PNAC_REAUTHMAX                2
#define   PNAC_TXPERIOD                 30
#define   PNAC_REAUTHPERIOD             3600
#define   PNAC_SUPPTIMEOUT              30
#define   PNAC_SERVERTIMEOUT            30
#define   PNAC_MAXREQ                   2
#define   PNAC_AUTHPERIOD               30
#define   PNAC_HELDPERIOD               60
#define   PNAC_STARTPERIOD              30
#define   PNAC_MAXSTART                 3
#define   PNAC_EAPREQIDLEN              15


/*PNAC Supplicant Access Ctrl With Auth Related Macros*/
#define  PNAC_SUPP_ACCESS_CTRL_INACTIVE                 1
#define  PNAC_SUPP_ACCESS_CTRL_ACTIVE                   2



#define   PNAC_MAXREQ_MIN               1
#define   PNAC_MAXREQ_MAX               10
#define   PNAC_QUIETPERIOD_MIN          0
#define   PNAC_QUIETPERIOD_MAX          65535
#define   PNAC_TXPERIOD_MIN             1
#define   PNAC_TXPERIOD_MAX             65535

#define   PNAC_HELDPERIOD_MIN           1
#define   PNAC_HELDPERIOD_MAX          65535
#define   PNAC_STARTPERIOD_MIN          1
#define   PNAC_STARTPERIOD_MAX         65535
#define   PNAC_AUTHPERIOD_MIN           1
#define   PNAC_AUTHPERIOD_MAX          65535
#define   PNAC_MAXSTART_MIN             1
#define   PNAC_MAXSTART_MAX             65535
/* This range is not specified in the standard for SuppTimeOut and ServerTimeOut-
 * It is implementation specific and can change as required
 */
#define   PNAC_SUPPTIMEOUT_MIN          1
#define   PNAC_SUPPTIMEOUT_MAX          65535
#define   PNAC_SERVERTIMEOUT_MIN        1
#define   PNAC_SERVERTIMEOUT_MAX        65535
#define   PNAC_REAUTHPERIOD_MIN         1
#define   PNAC_REAUTHPERIOD_MAX         65535


#define   PNAC_EAPOLVERSION             2


/* Key related macros */

#define   PNAC_KEYDESC_HDR_SIZE  44
#define   PNAC_RC4_KEY_DESC      1

/* Capabilities of the port */
#define   PNAC_PORT_CAPABILITY_NONE  0x0
#define   PNAC_PORT_CAPABILITY_AUTH  0x80
#define   PNAC_PORT_CAPABILITY_SUPP  0x40
#define   PNAC_PORT_CAPABILITY_BOTH  0xC0

#define   PNAC_WIRELESSLAN_PORT   11

/*********************************/
/* Implementation related macros */
/*********************************/

/* Generic Macros */

#define   PNAC_LSBYTE0_MASK        0x00ff


/* Port Operational State related macros */

#define   PNAC_PORT_INVALID   0xff
#define   PNAC_PORT_OPER_UP     CFA_IF_UP
#define   PNAC_PORT_OPER_DOWN CFA_IF_DOWN

/* Port  Status related macros */

#define PNAC_PORTSTATUS_NOT_CHECKED 0
#define PNAC_PORTSTATUS_CHANGED     1
#define PNAC_PORTSTATUS_NOT_CHANGED 2

/* Session Terminate Cause defines */

#define PNAC_SESS_TERMINATE_CAUSE_SUPPLOGOFF            1
#define PNAC_SESS_TERMINATE_CAUSE_PORTFAILURE           2 
#define PNAC_SESS_TERMINATE_CAUSE_SUPPRESTART           3
#define PNAC_SESS_TERMINATE_CAUSE_REAUTHFAILED          4
#define PNAC_SESS_TERMINATE_CAUSE_AUTHCNTRL_FORCEUNAUTH 5
#define PNAC_SESS_TERMINATE_CAUSE_PORT_REINIT           6
#define PNAC_SESS_TERMINATE_CAUSE_PORT_ADMINDISABLED    7
#define PNAC_SESS_NOT_TERMINATED_YET                    999


/* EAPOL packet related macros */

#define   PNAC_PAE_EAPPKT           1
#define   PNAC_PAE_EAPOLSTART    2
#define   PNAC_PAE_EAPOLLOGOFF 3
#define   PNAC_PAE_EAPOLKEY      4

#define   PNAC_EAP_HDR_SIZE    4


/* Authenticator State Machine States */

#define   PNAC_MAX_ASM_STATES           9
/* Authenticator State Machine Events */

#define   PNAC_MAX_ASM_EVENTS         14

#define   PNAC_ASM_EV_INITIALIZE                 0
#define   PNAC_ASM_EV_PORTDISABLED            1
#define   PNAC_ASM_EV_PORTENABLED             2
#define   PNAC_ASM_EV_PORTCNTRL_CHANGED   3
#define   PNAC_ASM_EV_QUIETWHILE_EXPIRED        4
#define   PNAC_ASM_EV_TXWHEN_EXPIRED       5
#define   PNAC_ASM_EV_EAPSTART_RCVD           6
#define   PNAC_ASM_EV_REAUTHENTICATE       7
#define   PNAC_ASM_EV_EAPLOGOFF_RCVD       8
#define   PNAC_ASM_EV_BKEND_AUTHSUCCESS   9
#define   PNAC_ASM_EV_BKEND_AUTHFAIL       10
#define   PNAC_ASM_EV_BKEND_AUTHTIMEOUT   11
#define   PNAC_ASM_EV_BKEND_AUTHABORTED   12
#define   PNAC_ASM_EV_RXRESPID                13


/* Backend State Machine States */

#define   PNAC_MAX_BSM_STATES       7

#define   PNAC_BSM_STATE_REQUEST           1
#define   PNAC_BSM_STATE_RESPONSE       2
#define   PNAC_BSM_STATE_SUCCESS           3
#define   PNAC_BSM_STATE_FAIL              4
#define   PNAC_BSM_STATE_TIMEOUT           5
#define   PNAC_BSM_STATE_IDLE              6
#define   PNAC_BSM_STATE_INITIALIZE     7


/* Backend State Machine Events */

#define  PNAC_MAX_BSM_EVENTS          8

#define   PNAC_BSM_EV_INITIALIZE          0
#define   PNAC_BSM_EV_RXRESP                 1
#define   PNAC_BSM_EV_AWHILE_EXPIRED  2
#define   PNAC_BSM_EV_ASUCCESS_RCVD       3
#define   PNAC_BSM_EV_AFAIL_RCVD             4
#define   PNAC_BSM_EV_AREQ_RCVD           5
#define   PNAC_BSM_EV_AUTHSTART           6
#define   PNAC_BSM_EV_AUTHABORT           7


/* Supplicant State Machine States */
#define   PNAC_MAX_SSM_STATES            9

#define   PNAC_SSM_STATE_DISCONNECTED    1
#define   PNAC_SSM_STATE_LOGOFF             2
#define   PNAC_SSM_STATE_CONNECTING         3
#define   PNAC_SSM_STATE_AUTHENTICATING 4
#define   PNAC_SSM_STATE_AUTHENTICATED   5
#define   PNAC_SSM_STATE_ACQUIRED           6
#define   PNAC_SSM_STATE_HELD                  7
#define   PNAC_SSM_STATE_FORCEAUTH       8
#define   PNAC_SSM_STATE_FORCEUNAUTH     9


/* Supplicant State Machine Events */

#define   PNAC_MAX_SSM_EVENTS           13

#define   PNAC_SSM_EV_INITIALIZE                0
#define   PNAC_SSM_EV_PORTDISABLED           1
#define   PNAC_SSM_EV_PORTENABLED        2
#define   PNAC_SSM_EV_USERLOGGEDOFF          3
#define   PNAC_SSM_EV_USERLOGGEDIN           4
#define   PNAC_SSM_EV_EAPFAIL_RCVD           5
#define   PNAC_SSM_EV_EAPSUCCESS_RCVD    6
#define   PNAC_SSM_EV_HELDWHILE_EXPIRED  7
#define   PNAC_SSM_EV_STARTWHEN_EXPIRED  8
#define   PNAC_SSM_EV_AUTHWHILE_EXPIRED  9
#define   PNAC_SSM_EV_REQID_RCVD                10
#define   PNAC_SSM_EV_REQAUTH_RCVD           11
#define   PNAC_SSM_EV_PORTCNTRL_CHANGED  12


/* Controlled Direction State Machine States */

#define   PNAC_MAX_CDSM_STATES        2 

#define   PNAC_CDSM_STATE_FORCEBOTH       0
#define   PNAC_CDSM_STATE_IN_OR_BOTH  1


/* Controlled Directions State Machine Events */

#define   PNAC_MAX_CDSM_EVENTS          6

#define   PNAC_CDSM_EV_INITIALIZE       0
#define   PNAC_CDSM_EV_ADMINCHANGED     1
#define   PNAC_CDSM_EV_PORTENABLED      2
#define   PNAC_CDSM_EV_PORTDISABLED     3
#define   PNAC_CDSM_EV_BRGDETECTED      4
#define   PNAC_CDSM_EV_BRGNOTDETECTED   5

#define PNAC_QUEUE_EVENT                0x1 
#define PNAC_TIMER_EXP_EVENT            0x2
#define PNAC_CFGQ_EVENT                 0x3
#define PNAC_RED_BULK_UPD_EVENT         0x4
#define PNAC_RSNA_TIMER_EXP_EVENT       0x5

#define PNAC_SEM_NAME         ((const UINT1 *)"PNAC")
#define PNAC_SEM_COUNT        1
#define PNAC_SEM_FLAGS        OSIX_DEFAULT_SEM_MODE
#define PNAC_SEM_NODE_ID      SELF

/* Interface related defines */
#define   PNAC_INTF_TASK_PRIORITY         30
#define   PNAC_INTF_TASK_NUMBER_OF_ARGS   0
#define   PNAC_INTF_TASK_MODE             OSIX_DEFAULT_TASK_MODE
#define   PNAC_INTF_TASK_NAME             ((const UINT1 *)"PIf")
#define   PNAC_INTF_QUEUE_NAME            ((UINT1 *)"PIfQ")
#define   PNAC_INTF_QUEUE_DEPTH           50
#define   PNAC_INTF_CFGQ_NAME            ((UINT1 *)"PCfQ")
#define   PNAC_INTF_CFGQ_DEPTH           PNAC_MAXPORTS * 4


/* Timer related defines */
#define   PNAC_NUMBER_OF_TIMERLISTS       1

#define   PNAC_AUTHWHILE_TIMER  1
#define   PNAC_AWHILE_TIMER        2
#define   PNAC_HELDWHILE_TIMER  3
#define   PNAC_QUIETWHILE_TIMER 4
#define   PNAC_REAUTHWHEN_TIMER 5
#define   PNAC_STARTWHEN_TIMER  6
#define   PNAC_TXWHEN_TIMER        7
#define   PNAC_SERVERCACHE_TIMER           8
#define   PNAC_DPNAC_PERIODIC_TIMER           9

 
/* Debug levels for PNAC_TRACE */
   
#define   PNAC_SYS_TRACE        0x01
#define   PNAC_IF_TRACE         0x02
#define   PNAC_PROT_TRACE       0x04
#define   PNAC_TRACE_ALL        0xFF

#define   PNAC_ASYNC_NP_RETRY_MAX  10

#define   PNAC_PROTOCOL_FLOW       1
#define   PNAC_NP_CALLBACK_FLOW    2

#define   PNAC_NP_SYNC             1
#define   PNAC_NP_ASYNC            2

#define   PNAC_MAC_AUTH_SESS_CREATE_FAILED   1
#define   PNAC_MAC_AUTH_SESS_DEL_FAILED      2
#define   PNAC_MAC_AUTH_SESS_PRESENT         3

#endif /* _PNACCONST_H */
