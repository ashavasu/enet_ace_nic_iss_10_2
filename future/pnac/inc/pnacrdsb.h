/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/*$Id: pnacrdsb.h,v 1.9 2011/09/08 07:22:07 siva Exp $                       */
/* Licensee Aricent Inc., 2005-2006                         */
/*****************************************************************************/
/*    FILE  NAME            : pnacrdsb.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : PNAC                                           */
/*    MODULE NAME           : All modules in Pnac.                           */
/*                                                                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : L2RED TEAM                                     */
/*    DESCRIPTION           : This file contains all dummy macros and        */
/*                            functions and constants required if            */
/*                            redundacny feature is not used.                */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    5th July 2005/          Initial Create.                        */
/*            ISS Team                                                       */
/*---------------------------------------------------------------------------*/


/* Constants for PNAC redundancy feature. */

#define PNAC_INIT_NUM_STANDBY_NODES() 

#define PNAC_NODE_STATUS()  gPnacNodeStatus

#define PNAC_IS_NP_PROGRAMMING_ALLOWED()  PNAC_TRUE

#define PNAC_PROTOCOL_ADMIN_STATUS() gPnacSystemInfo.u1ProtocolAdminStatus

#define PNAC_INIT_PROTOCOL_ADMIN_STATUS() (gPnacSystemInfo.u1ProtocolAdminStatus = PNAC_ENABLED)

#define PNAC_RM_GET_NODE_STATUS()  \
         (gPnacNodeStatus = PNAC_ACTIVE_NODE);

#define PNAC_RM_GET_NUM_STANDBY_NODES_UP() 

#define PNAC_NUM_STANDBY_NODES() 

/* Prototypes used for PNAC redundancy module. */
VOID PnacRedInitRedSystemInfo (VOID);

INT4 PnacRedRegisterWithRM (VOID);


/* SyncUp module */

VOID PnacRedSyncUpModOrPortClearInfo (UINT4 u4PortIndex);

VOID PnacRedHandleRmMsg (tPnacIntfMesg *pMesg);

INT4 PnacRedGetSyncUpPortStatus (tPnacPaePortEntry *pPortInfo, 
                                 UINT1 *pu1PortState);

VOID PnacRedSyncUpPortOperUpStatus (UINT4 u4PortIndex);
VOID PnacRedSyncAsynchronousStatus (UINT2 u2Port,
         UINT1 u1PnacPortCtrlDir,
         UINT1 u1PnacPortAuthStatus);
