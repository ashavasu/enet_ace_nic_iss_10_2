/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2005-2006                                          */
/*****************************************************************************/
/* $Id: pnacred.h,v 1.20 2012/06/04 12:55:11 siva Exp $                      */
/*****************************************************************************/
/*    FILE  NAME            : pnacred.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : PNAC                                           */
/*    MODULE NAME           : All modules in Pnac.                           */
/*                                                                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : L2RED TEAM                                     */
/*    DESCRIPTION           : This file contains all constants used in the   */
/*                            Pnac redundancy module.                        */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    5th July 2005/          Initial Create.                        */
/*            ISS Team                                                       */
/*---------------------------------------------------------------------------*/
#ifndef _PNAC_RED_H_
#define _PNAC_RED_H_

/* Pnac Redundancy related data structures. */
typedef struct sPnacRedSyncUpTime
{
    tPnacSysTime   SyncedUpTime;   /* Time at which the synced up data is 
                                      received. */
    tPnacSysTime   SuppSyncedUpTime;   /* Time at which the Supp synced up 
                                          data is received. */
} tPnacRedSyncUpTime;

typedef struct sPnacRedSyncUpData
{
    tPnacSysTime   SyncedUpTime;   /* Time at which the synced up data is 
                                      received. */
    tPnacSysTime   SuppSyncedUpTime;   /* Time at which the Supp synced up 
                                          data is received. */
    UINT4          u4SessReAuthPeriod; /* The SessionTimeout value given by 
                                             radius server during eap success. 
                                           */
    UINT2          u2AuthStatus;   /* Auth Status : Authorized / UnAuthorized.*/
    UINT2          u2AuthPaeState; /* ASM state. */
    UINT2          u2SuppPaeState;
    UINT2          u2SuppPortStatus;
} tPnacRedSyncUpData;

typedef enum
{
    PNAC_PORTBASED_UPDATE_MSG = 1,
    PNAC_MACBASED_UPDATE_MSG,
    PNAC_MODULE_OR_PORT_CLEAR_MSG,
    PNAC_BULK_REQ_MSG,
    PNAC_BULK_UPD_TAIL_MSG,
    PNAC_PORT_OPER_UP_MSG,
    PNAC_PORT_ASYNC_STATUS_MSG
} tPnacRedUpdateMsgType;

typedef struct {
    tPnacRedSyncUpTime    aPnacSyncUpTime[PNAC_MAXPORT_INFO];
    UINT4                 u4BulkUpdNextPort; /* Next port for which
                                                Bulk update has to be
                                                generated. */
    UINT1                 u1NumPeersUp;
    BOOL1                 bBulkReqRcvd;
#ifdef NPAPI_WANTED
    UINT1                 u1IsAuditStopped;
    UINT1                 au1Reserved[1];
    tPnacTaskId           AuditTaskId;
#else
    UINT1                 au1Reserved[2];
#endif
}tgPnacRedSystemInfo;

/* Constants for PNAC redundancy feature. */
#define  PNAC_PORT_ID_SIZE                 4

#define  PNAC_RM_OFFSET                    0


/* Auth port info size is sum of u4ReAuthPeriod, u2AuthStatus, 
 * u2AuthPaeState and u2OperControlDir. */
#define PNAC_AUTH_PORTINFO_SIZE                 8

/* Supp port info size is sum of u2SuppStatus, u2SuppPaeState. */
#define PNAC_SUPP_PORTINFO_SIZE                  4

/* Size of session flag. */
#define PNAC_SESSFLG_SIZE                        1

/* MAX size of sync up information per port -- 19. */
#define PNAC_PORTINFO_SIZE                 \
       (PNAC_SESSFLG_SIZE + PNAC_PORT_ID_SIZE + PNAC_AUTH_PORTINFO_SIZE + PNAC_SUPP_PORTINFO_SIZE)


/* Size of Module or Port Clear message. */
#define PNAC_MOD_OR_PORT_CLR_INFO_SIZE     PNAC_PORT_ID_SIZE

/* Size of port oper up message. */
#define PNAC_PORT_OPER_UP_MSG_SIZE             PNAC_PORT_ID_SIZE

/* mac info size is sum of u4PortNum, MacAddr, u4ReAuthPeriod, u2Status, 
 * u2State */
#define PNAC_MACINFO_SIZE                  18

#define PNAC_BULK_SPLIT_MSG_SIZE            1500

#define PNAC_RM_MSG_TYPE_SIZE              4
#define PNAC_RM_LEN_SIZE                   2

/* Maximum no of sub updates in bulk updation process. */
#define PNAC_RED_NO_OF_PORTS_PER_SUB_UPDATE  10
/* Trace related constants. */

/* Constants to indicate auth or dir change for auth session between 
 * hw and sw. */
#define   PNAC_RED_AUTH_CHANGE  0x00000001
#define   PNAC_RED_DIR_CHANGE   0x00000002
#define   PNAC_RED_BOTH_CHANGE  0x00000003
    
#define PNAC_RED_AUTH_UPDATE_MSG  0x0001
#define PNAC_RED_SUPP_UPDATE_MSG  0x0002
#define PNAC_RED_BOTH_UPDATE_MSG  0x0003

#ifdef NPAPI_WANTED
#define PNAC_AUDIT_TASK           ((UINT1 *)"PnAd")

#define PNAC_AUDIT_TASK_PRIORITY   220
#endif

/* Macros for Pnac redundacny feature. */

/* pnac Sync up header includes 4 bytes of type, 2 bytes of len.*/
#define PNAC_SYNCUP_HDR_LEN                 \
              (PNAC_RM_MSG_TYPE_SIZE + PNAC_RM_LEN_SIZE)

/* Size including msg type, len and port info of 12 bytes.*/
#define PNAC_SYNCUP_PORTBASED_DATA_SIZE       \
                      (PNAC_SYNCUP_HDR_LEN + PNAC_PORTINFO_SIZE) 
/* Size including msg type, len and port info of 12 bytes.*/
#define PNAC_MIN_PORTBASED_SYNCDATA_SIZE       \
                      (PNAC_SYNCUP_HDR_LEN + PNAC_SESSFLG_SIZE) 

#define PNAC_SYNCUP_ENTRY(u4Port) \
         &(gPnacRedSystemInfo.aPnacSyncUpTime[(u4Port)])

#define PNAC_IS_STANDBY_UP() \
          ((gPnacRedSystemInfo.u1NumPeersUp > 0) ? PNAC_TRUE : PNAC_FALSE)

#define PNAC_GET_NUM_STANDBY_NODES() gPnacRedSystemInfo.u1NumPeersUp

#define PNAC_INIT_NUM_STANDBY_NODES() \
                      gPnacRedSystemInfo.u1NumPeersUp = 0;

#define PNAC_BULK_REQ_RECD() gPnacRedSystemInfo.bBulkReqRcvd

#define PNAC_NODE_STATUS()  gPnacNodeStatus

#ifdef RM_WANTED
#define  PNAC_IS_NP_PROGRAMMING_ALLOWED() \
        ((L2RED_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE) ? PNAC_TRUE: PNAC_FALSE)
#else
#define  PNAC_IS_NP_PROGRAMMING_ALLOWED() PNAC_TRUE
#endif


#define PNAC_PROTOCOL_ADMIN_STATUS() gPnacSystemInfo.u1ProtocolAdminStatus

#define PNAC_INIT_PROTOCOL_ADMIN_STATUS() \
      PNAC_PROTOCOL_ADMIN_STATUS () = PNAC_ENABLED;

#define PNAC_RM_GET_NODE_STATUS()  PnacRedGetNodeStateFromRm ()

#define PNAC_RM_GET_NUM_STANDBY_NODES_UP() \
      gPnacRedSystemInfo.u1NumPeersUp = PnacRmGetStandbyNodeCount ()

#define PNAC_NUM_STANDBY_NODES() gPnacRedSystemInfo.u1NumPeersUp

#define PNAC_RM_GET_STATIC_CONFIG_STATUS()  PnacRmGetStaticConfigStatus ()

/* Macros to get and put info from/to RM buffers. */
#define PNAC_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), (u4MesgType)); \
        *(pu4Offset) += 4;\
}while (0)

#define PNAC_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), (u2MesgType)); \
        *(pu4Offset) += 2;\
}while (0)

#define PNAC_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), (u1MesgType)); \
        *(pu4Offset) += 1;\
}while (0)

#define PNAC_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
    RM_COPY_TO_OFFSET((pdest), (psrc), *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

#define PNAC_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define PNAC_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define PNAC_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define PNAC_RM_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
do { \
    RM_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

#ifdef NPAPI_WANTED
#define PNAC_RED_AUDIT_TSKID()    (gPnacRedSystemInfo.AuditTaskId)

#define PNAC_RED_IS_AUDIT_STOP() \
        (gPnacRedSystemInfo.u1IsAuditStopped) 
#endif


/* Prototypes used for PNAC redundancy module. */

/* Redundancy init sequence */
VOID PnacRedInitRedSystemInfo (VOID);

INT4 PnacRedRegisterWithRM (VOID);
INT4 PnacRedDeRegisterWithRM (VOID);
VOID PnacRedGetNodeStateFromRm (VOID);

VOID PnacRedHandleRmMsg (tPnacIntfMesg *pMesg);

VOID 
PnacRedFillPortBasedSyncUpInfo (tRmMsg *pMsg, tPnacPaePortEntry *pPortInfo, 
                                tPnacAuthSessionNode *pAuthSessNode, 
                                tPnacSuppSessionNode *pSuppSessNode,
                                UINT4 *pu4Offset);

VOID
PnacRedConstructMsgHeader (UINT4 u4Size, tPnacRedUpdateMsgType UpdateMsgType,
                           tRmMsg **ppMsg, UINT4 *pu4Offset,
                           UINT4 *pu4LenFieldOffset);

INT4 PnacRedSendMsgToRm (tRmMsg *pMsg, UINT2 u2BufSize);

VOID PnacRedSyncUpModOrPortClearInfo (UINT4 u4PortIndex);

VOID PnacRedSyncUpPortOperUpStatus (UINT4 u4PortIndex);

VOID PnacRedSyncAsynchronousStatus (UINT2 u2Port, UINT1 u1PnacPortCtrlDir,
                                    UINT1 u1PnacPortAuthStatus);

VOID PnacRedHandleSyncUpData (VOID *pData, UINT2 u2DataLen);

VOID PnacRedHandlePortBasedUpdtMsg (tRmMsg  *pData, UINT4  *pu4Offset, 
                                    UINT2 u2Len);

INT4
PnacRedReadPortSessInfo (tRmMsg *pMsg, UINT2 u2PktLen, UINT4 *pu4OffSet,
                         UINT4  *pu4PortNum);

VOID PnacRedHandleModOrPortClearMsg (tRmMsg  *pData, UINT4  *pu4Offset, 
                                     UINT2 u2Len);

VOID PnacRedHandlePortOperUpMsg (tRmMsg  *pData, UINT4  *pu4Offset, 
                                     UINT2 u2Len);

VOID PnacRedHandleAsynchronousStatus (tRmMsg  *pData, UINT4  *pu4Offset, 
                                     UINT2 u2Len);

/* Pnac redundancy : Standby bootup module */

VOID PnacRedHandleStandByUpEvent (VOID *pvPeerId);

VOID PnacRedHandlePortBasedBulkReqEvent (VOID *pvPeerId);

VOID PnacRedHandleGoStandbyEvent (VOID);

VOID PnacRedMakeNodeActiveFromIdle (VOID);

VOID PnacRedMakeNodeStandbyFromIdle (VOID);

VOID PnacRedMakeNodeActiveFromStandby (VOID);

VOID PnacRedMakeNodeActive (VOID);

VOID 
PnacRedReStartTimer (tPnacPaePortEntry *pPortInfo, 
                     VOID *pSessNode, 
                     UINT1 u1TmrType, UINT4 u4TimerDuration, 
                     tPnacSysTime SyncedUpTime);

VOID
PnacRedApplyAuthDynamicData (tPnacPaePortEntry *pPortInfo, 
                             tPnacAuthSessionNode * pAuthSessNode,
                             tPnacRedSyncUpData * pSyncUp);
VOID 
PnacRedApplySuppDynamicData (tPnacPaePortEntry *pPortInfo, 
                             tPnacSuppSessionNode *pSuppSessNode, 
                             tPnacRedSyncUpData *pSyncUp);
VOID PnacRedSendBulkReq (VOID);
VOID PnacRedTrigHigherLayer (UINT1 u1TrigType);
VOID PnacRedHandleBulkUpdEvent (VOID);
INT4 PnacRedSendBulkUpdateTailMsg (VOID);
VOID PnacRedPortOperStatusBulkSyncup (UINT4 u4StartIfIndex);
VOID PnacRedPortBasedBulkSyncup (UINT4 u4StartIfIndex);
#ifdef NPAPI_WANTED
VOID PnacRedPerformAudit (VOID);
VOID PnacRedAuditTaskMain (INT1 *pi1Param);
VOID PnacRedMakeSwInSyncWithHw(tPnacPaePortEntry *pPortInfo, 
  UINT2 u2AuthDirChange);
VOID PnacRedHandleCtrlDirChange (tPnacPaePortEntry *pPortInfo, UINT2 u2PortStatus);
INT4 PnacRedHandleAuthStatusChange (tPnacPaePortEntry *pPortInfo);
INT4 PnacRedGetHwAuthStatus (UINT2  u2PortNum, UINT1 u1AuthMode,
               UINT2  *pu2HwPortAuthStatus,
               UINT2  *pu2CtrlDir);
#endif

#endif
