/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/* $Id: pnacsem.h,v 1.5 2014/01/13 11:22:57 siva Exp $                   */
/*****************************************************************************/
/*    FILE  NAME            : pnacsem.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : PNAC module header files list                  */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Mar 2002                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file includes all header files            */
/*                            used in the PNAC module.                       */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 MAR 2002 / BridgeTeam   Initial Create.                     */
/*---------------------------------------------------------------------------*/

/*
 * This file contains type definitions used in PNAC State Machines
 * 
 */
#ifndef _PNACSEM_H
#define _PNACSEM_H
#include "pnaccons.h"
#include "pnactdfs.h"
#ifdef _PNACINIT_C

tPnacAsmEntry  gPnacAuthStateMachine[PNAC_MAX_ASM_EVENTS][PNAC_MAX_ASM_STATES] 
= {
   /* Event 0 - PNAC_ASM_EV_INITIALIZE *
    *
    */

   {  {(PNAC_ASMFUNC)PnacAuthAsmInitInitialize},  /* Initialise st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /* Disconnected St */
      {(PNAC_ASMFUNC)PnacAuthAsmInitInitialize},  /* Connecting st */
      {(PNAC_ASMFUNC)PnacAuthAsmInitInitialize},  /* Authenticating St */
      {(PNAC_ASMFUNC)PnacAuthAsmInitInitialize},  /* Authenticated St */
      {(PNAC_ASMFUNC)PnacAuthAsmInitInitialize},   /* Aborting st */
      {(PNAC_ASMFUNC)PnacAuthAsmInitInitialize},  /* Held st */
      {(PNAC_ASMFUNC)PnacAuthAsmInitInitialize},   /* Force Auth state */
      {(PNAC_ASMFUNC)PnacAuthAsmInitInitialize}  /* Force Unauth state */
   },
   /* Event 1 - PNAC_ASM_EV_PORTDISABLED *
    *
    */

   {  {(PNAC_ASMFUNC)NULL},      /* Initialise st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /*Disconnected St */
      {(PNAC_ASMFUNC)PnacAuthAsmInitInitialize},   /* Connecting st.. */
      {(PNAC_ASMFUNC)PnacAuthAsmInitInitialize},  /* Authenticating St ...*/
      {(PNAC_ASMFUNC)PnacAuthAsmInitInitialize},  /* Authenticated St ... */
      {(PNAC_ASMFUNC)PnacAuthAsmInitInitialize},     /* Aborting st .. */
      {(PNAC_ASMFUNC)PnacAuthAsmInitInitialize},        /* Held st */
      {(PNAC_ASMFUNC)PnacAuthAsmInitInitialize},    /* Force Auth state */
      {(PNAC_ASMFUNC)PnacAuthAsmInitInitialize} /* Force Unauth state */
   },
   /* Event 2 - PNAC_ASM_EV_PORTENABLED *
    *
    */

   {  {(PNAC_ASMFUNC)PnacAuthAsmPortEnabledInitialize},  /* Initialise st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /* Disconnected St */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Connecting st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Authenticating St */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Authenticated St */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /* Aborting st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Held st */
      {(PNAC_ASMFUNC)NULL},   /* Force Auth state */
      {(PNAC_ASMFUNC)NULL}  /* Force Unauth state */
   },

   /* Event 3 - PNAC_ASM_EV_PORTCNTRL_CHANGED *
    *
    */
   { { (PNAC_ASMFUNC)PnacAuthAsmPortCntrlChanged},   /* Initialise st */
      {(PNAC_ASMFUNC)PnacAuthAsmPortCntrlChanged},   /*Disconnected St */
      {(PNAC_ASMFUNC)PnacAuthAsmPortCntrlChanged},   /* Connecting st */
      {(PNAC_ASMFUNC)PnacAuthAsmPortCntrlChanged},  /* Authenticating St */
      {(PNAC_ASMFUNC)PnacAuthAsmPortCntrlChanged},  /* Authenticated St */
      {(PNAC_ASMFUNC)PnacAuthAsmPortCntrlChanged},      /* Aborting st .. */
      {(PNAC_ASMFUNC)PnacAuthAsmPortCntrlChanged},        /* Held st */
      {(PNAC_ASMFUNC)PnacAuthAsmPortCntrlChanged},    /* Force Auth state ..*/
      {(PNAC_ASMFUNC)PnacAuthAsmPortCntrlChanged} /* Force Unauth state ..*/
   },

   /* Event 4 - PNAC_ASM_EV_QUIETWHILE_EXPIRED *
    *
    */

   { { (PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /* Initialise st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /*Disconnected St */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Connecting st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Authenticating St */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Authenticated St */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Aborting st */
      {(PNAC_ASMFUNC)PnacAuthAsmQwhileExpiredHeld},  /* Held st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Force Auth state */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible} /* Force Unauth state */
   },

   /* Event 5 - PNAC_ASM_EV_TXWHEN_EXPIRED *
    *
    */

   { { (PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /* Initialise st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /*Disconnected St */
      {(PNAC_ASMFUNC)PnacAuthAsmIfRemakeConnecting},  /* Connecting st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Authenticating St */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /* Authenticated St */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Aborting st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /* Held st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Force Auth state */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible}  /* Force Unauth state */
   },

   /* Event 6 - PNAC_ASM_EV_EAPSTART_RCVD *
    *
    */

   { { (PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /* Initialise st */
      {(PNAC_ASMFUNC)NULL},   /*Disconnected St */
      {(PNAC_ASMFUNC)PnacAuthAsmIfRemakeConnecting}, /* Connecting st */
      {(PNAC_ASMFUNC)PnacAuthAsmEapStartAuthenticating}, /* Authenticating state */
      {(PNAC_ASMFUNC)PnacAuthAsmMakeConnecting}, /* Authenticated St */
      {(PNAC_ASMFUNC)NULL}, /* Aborting st */ /* no operation */
      {(PNAC_ASMFUNC)NULL},  /* Held st *//* no operation */
      
 
      {(PNAC_ASMFUNC)PnacAuthAsmMakeForceAuthorized},   /* Force Auth state */
      {(PNAC_ASMFUNC)PnacAuthAsmMakeForceUnauthorized} /* Force Unauth state .. */
   },

   /* Event 7 - PNAC_ASM_EV_REAUTHENTICATE *
    *
    */

   { { (PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /* Initialise st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /*Disconnected St */
      {(PNAC_ASMFUNC)PnacAuthAsmIfRemakeConnecting},  /* Connecting st */
      {(PNAC_ASMFUNC)PnacAuthAsmMakeAborting},      /* Authenticating State */
      {(PNAC_ASMFUNC)PnacAuthAsmMakeConnecting},  /* Authenticated St ..  */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Aborting st */
      {(PNAC_ASMFUNC)NULL},         /* Held st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /* Force Auth state */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible}  /* Force Unauth state */
   },

   /* Event 8 - PNAC_ASM_EV_EAPLOGOFF_RCVD *
    *
    */

   { { (PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /* Initialise st */  
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /*Disconnected St */
      {(PNAC_ASMFUNC)PnacAuthAsmEapLogoffConnecting},   /* Connecting st */
      {(PNAC_ASMFUNC)PnacAuthAsmEapLogoffAuthenticating}, /* Authenticating 
                                                 state */
      {(PNAC_ASMFUNC)PnacAuthAsmEapLogoffAuthenticated}, /* Authenticated St */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /* Aborting st .. */
      {(PNAC_ASMFUNC)NULL},  /* no operation */ /* Held st .. */
      {(PNAC_ASMFUNC)NULL}, /* no operation .. *//* Force Auth state */
      {(PNAC_ASMFUNC)NULL} /* no operation *//* Force Unauth state */
   },

   /* Event 9 - PNAC_ASM_EV_BKEND_AUTHSUCCESS *
    *
    */

   { { (PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /* Initialise st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /*Disconnected St */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Connecting st */
      {(PNAC_ASMFUNC)PnacAuthAsmMakeAuthenticated}, /* Authenticating state */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Authenticated St */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /* Aborting st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Held st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /* Force Auth state */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible}  /* Force Unauth state */
   },

   /* Event 10 - PNAC_ASM_EV_BKEND_AUTHFAIL *
    *
    */

   { { (PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /* Initialise st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /*Disconnected St */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Connecting st */
      {(PNAC_ASMFUNC)PnacAuthAsmMakeHeld},       /* Authenticating State */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Authenticated St */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Aborting st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Held st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Force Auth state */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible} /* Force Unauth state */
   },
   /* Event 11 - PNAC_ASM_EV_BKEND_AUTHTIMEOUT *
    *
    */

   { { (PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Initialise st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},   /*Disconnected St */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Connecting st */
      {(PNAC_ASMFUNC)PnacAuthAsmBkendAuthTimeoutAuthenticating}, 
                                              /* Authenticating State */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Authenticated St */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Aborting st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Held st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible}, /* Force Auth state */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible} /* Force Unauth state */

   },
   /* Event 12- PNAC_ASM_EV_BKEND_AUTHABORTED *
    *
    */

   { { (PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Initialise st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /*Disconnected St */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Connecting st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Authenticating St */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Authenticated St */
      {(PNAC_ASMFUNC)PnacAuthAsmBkendAuthAbortedAborting}, /* Aborting st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Held st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible}, /* Force Auth state */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible} /* Force Unauth state */
   },
   /* Event 13 - PNAC_ASM_EV_RXRESPID *
    *
    */

   { { (PNAC_ASMFUNC)NULL},  /* Initialise st */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /*Disconnected St */
      {(PNAC_ASMFUNC)PnacAuthAsmRxrespConnecting},  /* Connecting st */
      {(PNAC_ASMFUNC)NULL}, /* no operation .. *//* Authenticating St */
      {(PNAC_ASMFUNC)NULL}, /* no operation .. */
                                                     /* Authenticated St */
      {(PNAC_ASMFUNC)NULL}, /* Aborting st .. */ /* no operation */
      {(PNAC_ASMFUNC)PnacAuthAsmEventImpossible},  /* Held st */
      {(PNAC_ASMFUNC)NULL}, /* Force Auth state .. */ 
                                 /*no operation */
      {(PNAC_ASMFUNC)NULL} /* Force Unauth state .. */
                                /* no operation */
   }
}; /* End of gPnacAuthStateMachine */

/* Back end State Machine Entry */
tPnacBsmEntry  gPnacBkendStateMachine[PNAC_MAX_BSM_EVENTS][PNAC_MAX_BSM_STATES]
= {
   /* Event 0 - PNAC_BSM_EV_INITIALIZE *
    *
    */
    
    {  {(PNAC_BSMFUNC)PnacAuthBsmMakeInitialize},  /* Request state */
       {(PNAC_BSMFUNC)PnacAuthBsmMakeInitialize},  /* Response state */
       {(PNAC_BSMFUNC)PnacAuthBsmMakeInitialize},  /* Success state */
       {(PNAC_BSMFUNC)PnacAuthBsmMakeInitialize},  /* Fail state */
       {(PNAC_BSMFUNC)PnacAuthBsmMakeInitialize},  /* Timeout state */
       {(PNAC_BSMFUNC)PnacAuthBsmMakeInitialize},  /* Idle state */
       {(PNAC_BSMFUNC)PnacAuthBsmMakeInitialize}  /* Initialise st */
    },
       
       
   /* Event 1 - PNAC_BSM_EV_RXRESP *
    *
    */

    {    {(PNAC_BSMFUNC)PnacAuthBsmMakeResponse},  /* Request state */
       {(PNAC_BSMFUNC)NULL},  /* Response state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Success state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Fail state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Timeout state */
       {(PNAC_BSMFUNC)NULL},  /* Idle state */
         {(PNAC_BSMFUNC)NULL}   /* Initialise st */
    },
   /* Event 2 - PNAC_BSM_EV_AWHILE_EXPIRED *
    *
    */
    
    {    {(PNAC_BSMFUNC)PnacAuthBsmAwhileExpiredRequest},  /* Request state */
       {(PNAC_BSMFUNC)PnacAuthBsmMakeTimeout},  /* Response state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Success state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Fail state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Timeout state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Idle state */
         {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible}   /* Initialise st */
    },
   
   /* Event 3 - PNAC_BSM_EV_ASUCCESS_RCVD *
    *
    */
    {    {(PNAC_BSMFUNC)NULL},  /* Request state */
       {(PNAC_BSMFUNC)PnacAuthBsmMakeSuccess},  /* Response state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Success state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Fail state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Timeout state */
       {(PNAC_BSMFUNC)NULL},  /* Idle state */
         {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible}   /* Initialise st */
    },

   /* Event 4 - PNAC_BSM_EV_AFAIL_RCVD *
    *
    */

    {{(PNAC_BSMFUNC)NULL},  /* Request state */
       {(PNAC_BSMFUNC)PnacAuthBsmMakeFail},  /* Response state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Success state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Fail state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Timeout state */
       {(PNAC_BSMFUNC)NULL},  /* Idle state */
         {(PNAC_BSMFUNC)NULL}  /* Initialise st */
    },
   /* Event 5 - PNAC_BSM_EV_AREQ_RCVD *
    *
    */
    {    {(PNAC_BSMFUNC)NULL},  /* Request state */
       {(PNAC_BSMFUNC)PnacAuthBsmAreqRcvdResponse},  /* Response state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Success state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Fail state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Timeout state */
       {(PNAC_BSMFUNC)NULL},  /* Idle state */
         {(PNAC_BSMFUNC)NULL},  /* Initialise st */
    },

   /* Event 6 - PNAC_BSM_EV_AUTHSTART *
    *
    */

    {    {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Request state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Response state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Success state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Fail state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Timeout state */
       {(PNAC_BSMFUNC)PnacAuthBsmMakeResponse},  /* Idle state */
         {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible}   /* Initialise st */
    },
   
   /* Event 7 - PNAC_BSM_EV_AUTHABORT *
    *
    */
    {    {(PNAC_BSMFUNC)PnacAuthBsmMakeInitialize},  /* Request state */
       {(PNAC_BSMFUNC)PnacAuthBsmMakeInitialize},  /* Response state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Success state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Fail state */
       {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible},  /* Timeout state */
       {(PNAC_BSMFUNC)PnacAuthBsmMakeInitialize},  /* Idle state */
         {(PNAC_BSMFUNC)PnacAuthBsmEventImpossible}  /* Initialise st */
    }
}; /* End of gPnacBkendStateMachine */ 

tPnacSsmEntry  gPnacSuppStateMachine[PNAC_MAX_SSM_EVENTS][PNAC_MAX_SSM_STATES] 
= {
   /* Event 0 - PNAC_SSM_EV_INITIALIZE *
    *
    */
    
    { {  PnacSuppSmInitialize}, /* Disconnected st */
      {  PnacSuppSmInitialize}, /* Logoff St */
      {  PnacSuppSmInitialize}, /* Connecting st */
      {  PnacSuppSmInitialize}, /* Authenticating st */
      {  PnacSuppSmInitialize}, /* Authenticated st */
      {  PnacSuppSmInitialize}, /* Acquired st */
      {  PnacSuppSmInitialize}, /* Held st */
      {  PnacSuppSmInitialize}, /* ForceAuth */
      {  PnacSuppSmInitialize}  /* ForceUnAuth*/
    },
   /* Event 1 - PNAC_SSM_EV_PORTDISABLED *
    *
    */
    { {  NULL}, /* Disconnected st */
      {  PnacSuppSmMakeDisconnected}, /* Logoff St */
      {  PnacSuppSmMakeDisconnected}, /* Connecting st */
      {  PnacSuppSmMakeDisconnected}, /* Authenticating st */
      {  PnacSuppSmMakeDisconnected}, /* Authenticated st */
      {  PnacSuppSmMakeDisconnected}, /* Acquired st */
      {  PnacSuppSmMakeDisconnected}, /* Held st */
      {  PnacSuppSmMakeDisconnected}, /* ForceAuth */
      {  PnacSuppSmMakeDisconnected}  /* ForceUnAuth*/
    },
    
   /* Event 2 - PNAC_SSM_EV_PORTENABLED *
    *
    */
    { {  PnacSuppSmPortEnabled}, /* Disconnected st */
      {  PnacSuppSmEventImpossible}, /* Logoff St */
      {  PnacSuppSmEventImpossible}, /* Connecting st */
      {  PnacSuppSmEventImpossible}, /* Authenticating st */
      {  PnacSuppSmEventImpossible}, /* Authenticated st */
      {  PnacSuppSmEventImpossible}, /* Acquired st */
      {  PnacSuppSmEventImpossible}, /* Held st */
          {  NULL},                      /*ForceAuth*/
          {  NULL}                       /*ForceUnAuth*/
    },

   
    
   /* Event 3 - PNAC_SSM_EV_USERLOGGEDOFF *
    *
    */
    { {  NULL}, /* Disconnected st */
      {  NULL}, /* Logoff St */
      {  PnacSuppSmUserLoggedOff}, /* Connecting st */
      {  PnacSuppSmUserLoggedOff},  /* Authenticating st */
      {  PnacSuppSmUserLoggedOff}, /* Authenticated st */
      {  PnacSuppSmUserLoggedOff}, /* Acquired st */
      {  PnacSuppSmUserLoggedOff},  /* Held st */
      {  PnacSuppSmUserLoggedOff},  /* ForceAuth */
      {  PnacSuppSmUserLoggedOff}   /* ForceUnAuth */
    },
   /* Event 4 - PNAC_SSM_EV_USERLOGGEDIN *
    *
    */

    { {  NULL}, /* Disconnected st */
      {  PnacSuppSmUserLoggedIn}, /* Logoff St */
      {  PnacSuppSmEventImpossible}, /* Connecting st */
      {  PnacSuppSmEventImpossible}, /* Authenticating st */
      {  PnacSuppSmEventImpossible}, /* Authenticated st */
      {  PnacSuppSmEventImpossible}, /* Acquired st */
      {  PnacSuppSmEventImpossible},  /* Held st */
      {  PnacSuppSmEventImpossible},  /* ForceAuth */
      {  PnacSuppSmEventImpossible}  /* ForceUnAuth */
    },
   /* Event 5 - PNAC_SSM_EV_EAPFAIL_RCVD *
    *
    */

    { {  NULL}, /* Disconnected st */
      {  NULL}, /* Logoff St */
      {  PnacSuppSmMakeHeld}, /* Connecting st */
      {  PnacSuppSmMakeHeld}, /* Authenticating st */
      {  PnacSuppSmMakeHeld}, /* Authenticated st */
      {  PnacSuppSmMakeHeld}, /* Acquired st */
      {  NULL}, /* Held st */
      {  PnacSuppSmEventImpossible},  /* ForceAuth */
      {  PnacSuppSmEventImpossible}  /* ForceUnAuth */
    },
   /* Event 6 - PNAC_SSM_EV_EAPSUCCESS_RCVD *
    *
    */

    { {  NULL}, /* Disconnected st */
      {  NULL}, /* Logoff St */
      {  PnacSuppSmMakeAuthenticated}, /* Connecting st */
      {  PnacSuppSmMakeAuthenticated}, /* Authenticating st */
      {  PnacSuppSmMakeAuthenticated}, /* Authenticated st */
      {  PnacSuppSmMakeAuthenticated}, /* Acquired st */
      {  PnacSuppSmMakeAuthenticated}, /* Held st */
      {  PnacSuppSmEventImpossible},  /* ForceAuth */
      {  PnacSuppSmEventImpossible}  /* ForceUnAuth */
    },
   /* Event 7 - PNAC_SSM_EV_HELDWHILE_EXPIRED *
    *
    */

    { {  PnacSuppSmEventImpossible}, /* Disconnected st */
      {  PnacSuppSmEventImpossible}, /* Logoff St */
      {  PnacSuppSmEventImpossible}, /* Connecting st */
      {  PnacSuppSmEventImpossible}, /* Authenticating st */
      {  PnacSuppSmEventImpossible}, /* Authenticated st */
      {  PnacSuppSmEventImpossible}, /* Acquired st */
      {  PnacSuppSmMakeConnecting},   /* Held st */
      {  PnacSuppSmEventImpossible},  /* ForceAuth */
      {  PnacSuppSmEventImpossible}  /* ForceUnAuth */
    },
   /* Event 8 - PNAC_SSM_EV_STARTWHEN_EXPIRED *
    *
    */

    { {  PnacSuppSmEventImpossible}, /* Disconnected st */
      {  PnacSuppSmEventImpossible}, /* Logoff St */
      {  PnacSuppSmStartWhenExpiredConnecting}, /* Connecting st */
      {  PnacSuppSmEventImpossible}, /* Authenticating st */
      {  PnacSuppSmEventImpossible}, /* Authenticated st */
      {  PnacSuppSmEventImpossible}, /* Acquired st */
      {  PnacSuppSmEventImpossible}, /* Held st */
      {  PnacSuppSmEventImpossible},  /* ForceAuth */
      {  PnacSuppSmEventImpossible}  /* ForceUnAuth */
    },
   /* Event 9 - PNAC_SSM_EV_AUTHWHILE_EXPIRED *
    *
    */

    { {  PnacSuppSmEventImpossible}, /* Disconnected st */
      {  PnacSuppSmEventImpossible}, /* Logoff St */
      {  PnacSuppSmEventImpossible}, /* Connecting st */
      {  PnacSuppSmMakeConnecting},  /* Authenticating st */
      {  PnacSuppSmEventImpossible}, /* Authenticated st */
      {  PnacSuppSmMakeConnecting}, /* Acquired st */
      {  PnacSuppSmEventImpossible}, /* Held st */
      {  PnacSuppSmEventImpossible},  /* ForceAuth */
      {  PnacSuppSmEventImpossible}  /* ForceUnAuth */
    },
   /* Event 10 - PNAC_SSM_EV_REQID_RCVD *
    *
    */

    { {  NULL}, /* Disconnected st */
      {  NULL}, /* Logoff St */
      {  PnacSuppSmMakeAcquired}, /* Connecting st */
      {  PnacSuppSmMakeAcquired}, /* Authenticating st */
      {  PnacSuppSmMakeAcquired}, /* Authenticated st */
      {  PnacSuppSmMakeAcquired}, /* Acquired st */
      {  PnacSuppSmMakeAcquired}, /* Held st */
      {  NULL}, /* ForceAuth */
      {  NULL} /* ForceUnAuth */
    },
   /* Event 11 - PNAC_SSM_EV_REQAUTH_RCVD *
    *
    */

    { {  NULL}, /* Disconnected st */
      {  NULL}, /* Logoff St */
      {  NULL}, /* Connecting st */
      {  PnacSuppSmMakeAuthenticating}, /* Authenticating st */
      {  NULL}, /* Authenticated st */
      {  PnacSuppSmMakeAuthenticating}, /* Acquired st */
      {  NULL},  /* Held st */
      {  NULL}, /* ForceAuth */
      {  NULL} /* ForceUnAuth */
    },
   /* Event 12 - PNAC_SSM_EV_PORTCNTRL_CHANGED *
    *
    */
   { { PnacSuppSmPortCntrlChanged},   /* Disconnected st*/
      {PnacSuppSmPortCntrlChanged},   /*Logoff St */
      {PnacSuppSmPortCntrlChanged},   /* Connecting st */
      {PnacSuppSmPortCntrlChanged},  /* Authenticating St */
      {PnacSuppSmPortCntrlChanged},  /* Authenticated St */
      {PnacSuppSmPortCntrlChanged},      /* Acquired st  */
      {PnacSuppSmPortCntrlChanged},        /* Held st */
      {PnacSuppSmPortCntrlChanged},    /* Force Auth state ..*/
      {PnacSuppSmPortCntrlChanged} /* Force Unauth state ..*/
    }
};

#else /* _PNACINIT_C */

extern tPnacAsmEntry  gPnacAuthStateMachine[PNAC_MAX_ASM_EVENTS][PNAC_MAX_ASM_STATES];
extern tPnacBsmEntry  gPnacBkendStateMachine[PNAC_MAX_BSM_EVENTS][PNAC_MAX_BSM_STATES];
extern tPnacSsmEntry  gPnacSuppStateMachine[PNAC_MAX_SSM_EVENTS][PNAC_MAX_SSM_STATES];

#endif /* _PNACINIT_C */

#endif /* _PNACSEM_H */
