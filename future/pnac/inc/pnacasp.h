/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                 */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : pnacasp.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : PNAC module                                    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Mar 2002                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains prototypes of PNAC server   */
/*                            Module Functions.                              */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                      DESCRIPTION OF CHANGE/              */
/*            MODIFIED BY                FAULT REPORT NO                     */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 MAR 2002 / BridgeTeam   Initial Create.                     */
/*---------------------------------------------------------------------------*/
#ifndef _PNACASP_H
#define _PNACASP_H

INT4 PnacServInit (VOID);

INT4 PnacServDisable (VOID);

INT4 PnacServRemoveCache (VOID);

INT4 PnacServInitUserRecord (UINT1 *,
                             UINT2 ,
                             UINT1 *,
                             UINT2 ,
                             UINT1 ,
                             UINT1 *,
                             UINT1 );

         
INT4 PnacServGetUserNode (UINT1 *, tPnacServUserInfo **);

INT4
PnacServGetAnyUserNode (UINT1 *, tPnacServUserInfo ** );

INT4 PnacServGetCacheNode (UINT1 *,
                           tMacAddr,
                           UINT2 ,
                           UINT2 ,
                           tPnacServAuthCache **);


           
INT4 PnacServValidateEapPkt (UINT1 *, UINT2 ,
                             tPnacEapDesc *);

INT4 PnacServHandleEapPkt (tPnacAsIfSendData *);


INT4 PnacServValidateResponseId(tPnacAsIfSendData *,
                                tPnacServUserInfo *);


INT4 PnacServProcessResponseId(tPnacAsIfSendData *,
                                UINT1 *, UINT2 , UINT2 ,
                               tPnacAsIfRecvData *);

INT4 PnacServProcessResponseMd5(tPnacAsIfSendData *,
                               tPnacServAuthCache  *,
                               tPnacEapDesc *,
                               tPnacAsIfRecvData *);

INT4 PnacServGenEap (tPnacEapDesc *,
                     UINT1 *, UINT2 *);

VOID PnacServGenerateMd5Chal (UINT1 *pu1Buf, UINT2 *pu2Len);


INT4 PnacServValidateChallenge(UINT1 *pu1Chal,
                           UINT2 u2ChalLen,
                           UINT1 *pu1Passwd,
                           UINT2 u2PasswdLen,
                           tPnacEapDesc *pEapDesc);

INT4 PnacServCacheTmrExpired(tPnacServAuthCache *);

INT4 PnacServValidatePort(UINT2 u2PortNum, UINT1 *pu1PortList);
#endif /* _PNACASP_H */
