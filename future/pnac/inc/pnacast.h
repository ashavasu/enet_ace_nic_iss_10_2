/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                 */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : pnacast.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : PNAC module                                    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Mar 2002                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains type definitions used by    */
/*                            PNAC Server Module Functions.                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                      DESCRIPTION OF CHANGE/              */
/*            MODIFIED BY                FAULT REPORT NO                     */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 MAR 2002 / BridgeTeam   Initial Create.                     */
/*---------------------------------------------------------------------------*/
#ifndef _PNACAST_H
#define _PNACAST_H
typedef struct PnacServUserInfo {
   tPnacSLLNode nextNode;
   UINT4    u4AuthTimeout;
   UINT2    u2NameLen;
   UINT2    u2PasswdLen;
   UINT1    au1PortList[PNAC_PORT_LIST_SIZE];
   UINT1    au1Name[PNAC_MAX_LEN_USERNAME+1];
   UINT1    au1Passwd[PNAC_MAX_LEN_PASSWD+1];
   UINT1    u1AuthType;
   UINT1    u1RowStatus;
   BOOL1    bDenyUser;
   UINT1    au1Reserved[3];
}tPnacServUserInfo;

typedef struct PnacServAuthCache {
   tPnacSLLNode   nextNode;
   tPnacTimerNode *pTmrNode;
   tMacAddr macAddr;
   UINT2    u2ChalLen;
   UINT2    u2NameLen;
   UINT2    u2RxdPort;
   UINT2    u2SessId;
   UINT1    au1Chal[PNAC_AS_MAX_CHAL_SIZE];
   UINT1    au1Name[PNAC_MAX_LEN_USERNAME+1];
   UINT1    u1AuthType;
}tPnacServAuthCache;

typedef struct PnacServInfo {
   tPnacMemPoolId      userMemPoolId;
   tPnacMemPoolId      authCacheMemPoolId;
   tPnacSLL            userListHead;
   tPnacSLL            cacheListHead;
   UINT4               cacheTimeout;
}tgPnacServInfo;

/* EAP Packet Descriptor Structure */

typedef struct PnacEapReqId {
   UINT2 u2Len;
   UINT2 u2Reserved;
   UINT1 *pu1Str;
}tPnacEapReqId;

typedef struct PnacEapNtfcn {
   UINT2 u2Len;
   UINT2 u2Reserved;
   UINT1 *pu1Str;
}tPnacEapNtfcn;

typedef struct PnacEapNak {
   UINT1  u1AuthDesired;
   UINT1  au1Reserved[3];
}tPnacEapNak;

typedef struct PnacEapMesgDigest {
   UINT1 *pu1Name;
   UINT1 *pu1ValPtr;
   UINT2 u2NameLen;
   UINT1 u1ValSize;
   UINT1 u1Reserved;
}tPnacEapMesgDigest;


typedef struct PnacEapDesc {
   tPnacEapReqId      identity;
   tPnacEapNtfcn      notify;
   tPnacEapNak        nak;
   tPnacEapMesgDigest mesgDigest;
   UINT2 u2EapLen;
   UINT1 u1Code;
   UINT1 u1Id;
   UINT1 u1Type;
   UINT1 au1Reserved[3];
}tPnacEapDesc;
#endif /* _PNACAST_H */
