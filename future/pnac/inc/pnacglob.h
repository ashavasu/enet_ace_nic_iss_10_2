/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/* $Id: pnacglob.h,v 1.12 2010/10/29 09:44:48 prabuc Exp $                   */
/*****************************************************************************/
/*    FILE  NAME            : pnacglob.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : PNAC module                                    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Mar 2002                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the global variables        */
/*                            declaration used in PNAC module.               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 MAR 2002 / BridgeTeam   Initial Create.                     */
/*---------------------------------------------------------------------------*/
#ifndef _PNACGLOB_H
#define _PNACGLOB_H


#ifdef _PNACINIT_C

      /* Global PNAC System Information */
      tgPnacSystemInfo	gPnacSystemInfo;
      UINT1             *gpu1EapolBuffer;
      UINT2             gu2PnacSystemControl = PNAC_SHUTDOWN;
      tPnacOsixSemId    gPnacSemId = PNAC_INIT_VAL;
      tPnacMemPoolId    gPnacIntfMesgMemPoolId = PNAC_INIT_VAL;
      tPnacMemPoolId    gPnacIntfCfgQMesgMemPoolId = PNAC_INIT_VAL;
      tPnacMemPoolId    gPnacEapolMesgMemPoolId = PNAC_INIT_VAL;
      tPnacTaskId		gPnacIntfTaskId  = PNAC_INIT_VAL;
      tPnacQId          gPnacIntfInputQId  = PNAC_INIT_VAL;
      tPnacQId          gPnacIntfCfgQId = PNAC_INIT_VAL;

      
      tMacAddr gPnacEapolGrpMacAddr = {0x01, 0x80, 0xC2, 0x00, 0x00, 0x03}; 
      tMacAddr gPnacBCastAddr = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
      tMacAddr gPnacBrgGrpAddr = {0x01 , 0x80 ,0xc2 ,0x00 ,0x00 ,0x00};

      tPnacNodeStatus   gPnacNodeStatus;
#ifdef L2RED_WANTED
      tgPnacRedSystemInfo  gPnacRedSystemInfo;
#endif

#else /* _PNACINIT_C */
      /* Global PNAC System Information */
      extern tgPnacSystemInfo	gPnacSystemInfo;
      extern tMacAddr gPnacEapolGrpMacAddr;
      extern tMacAddr gPnacBCastAddr;
      extern tMacAddr gPnacBrgGrpAddr;
      extern UINT1    *gpu1EapolBuffer;
      extern tPnacOsixSemId     gPnacSemId;       
      extern UINT2    gu2PnacSystemControl;
      extern tPnacMemPoolId   gPnacIntfMesgMemPoolId;
	  extern tPnacTaskId      gPnacIntfTaskId;
      extern tPnacQId         gPnacIntfInputQId;
      extern tPnacQId         gPnacIntfCfgQId;
      extern tPnacMemPoolId   gPnacIntfCfgQMesgMemPoolId;
      extern tPnacMemPoolId   gPnacEapolMesgMemPoolId;
      extern tPnacNodeStatus  gPnacNodeStatus;
#ifdef L2RED_WANTED
      extern tgPnacRedSystemInfo  gPnacRedSystemInfo;
#endif

#endif /* _PNACINIT_C */

#endif /* _PNACGLOB_H */

