/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : pnacport.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : PNAC Stubs                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 22 Apr 2002                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains external module headers     */
/*                            for routines  in PNAC                          */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    22 APR 2002 / BridgeTeam   Initial Create.                     */
/*---------------------------------------------------------------------------*/
typedef struct PnacRadReq {
   tMacAddr         macAddr;
   UINT1            u1Status;
   UINT1            au1Reserved;
}tPnacRadReq;

typedef struct PnacTacReq {
   tMacAddr         macAddr;
   UINT2            u2NasPortNumber;
   UINT1            u1Status;
   UINT1            au1Reserved[3];
}tPnacTacReq;

#define PNAC_MAX_RAD_REQ_SIZE  256

#define PNAC_MAX_TAC_REQ_SIZE  256

#ifdef _PNACPORT_C
tPnacRadReq gPnacRadReq[PNAC_MAX_RAD_REQ_SIZE];
UINT1        gu1ReqId;
tPnacTacReq gPnacTacReq[PNAC_MAX_TAC_REQ_SIZE];
#else /* _PNACPORT_C */
extern tPnacRadReq gPnacRadReq[PNAC_MAX_RAD_REQ_SIZE];
extern UINT1        gu1ReqId;
extern tPnacTacReq gPnacTacReq[PNAC_MAX_TAC_REQ_SIZE];
#endif /* _PNACPORT_C */

VOID PnacInitRadInterface (VOID);

VOID PnacInitTacInterface (VOID);

