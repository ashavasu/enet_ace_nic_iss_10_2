/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* $Id: pnacsrm.h,v 1.9 2014/07/15 12:56:56 siva Exp $            */
/* Licensee Aricent Inc., 2001-2002                                       */
/*****************************************************************************/
/*    FILE  NAME            : pnacsrm.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : PNAC module                                    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Mar 2002                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains macros and constants used   */
/*                            in PNAC server Module.                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                      DESCRIPTION OF CHANGE/              */
/*            MODIFIED BY                FAULT REPORT NO                     */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 MAR 2002 / BridgeTeam   Initial Create.                     */
/*---------------------------------------------------------------------------*/
#ifndef  _PNACSRM_H
#define _PNACSRM_H
#define tPnacSLL tTMO_SLL 
#define tPnacSLLNode tTMO_SLL_NODE

#define PNAC_STRCMP  STRCMP

#define PNAC_AS_MAX_CHAL_SIZE     16
#define PNAC_MD5_CHAL_RESP_SIZE   16
#define PNAC_EAP_VALUE_SIZE       1

#define PNAC_AS_CACHE_TIMEOUT     (PNAC_TXPERIOD * 3) 
#define PNAC_MAX_AS_AUTH_TIMEOUT  7200
#define PNAC_MAX_SERV_USERS       64 
#define PNAC_MIN_SERV_USERS       1
#define PNAC_EAP_TYPE_MD5         PNAC_EAP_TYPE_MD5CHALLENGE
#define PNAC_EAP_TYPE_TLS         13

#define PNAC_AS_SESSID_SIZE       1

#define PNAC_PORTS_PER_BYTE       8

#define PNAC_PORT_LIST_SIZE      BRG_PHY_PORT_LIST_SIZE 

#define PNAC_SERVUSER_MEMPOOL_ID    gPnacServInfo.userMemPoolId
#define PNAC_SERVUSER_MEMBLK_SIZE   sizeof(tPnacServUserInfo)
#define PNAC_SERVUSER_MEMBLK_COUNT  PNAC_MAX_SERV_USERS

#define PNAC_SERVCACHE_MEMPOOL_ID    gPnacServInfo.authCacheMemPoolId
#define PNAC_SERVCACHE_MEMBLK_SIZE   sizeof(tPnacServAuthCache)
#define PNAC_SERVCACHE_MEMBLK_COUNT  PNAC_MAX_AUTHSESS

#define PNAC_SERV_USER_LIST          gPnacServInfo.userListHead
#define PNAC_SERV_CACHE_LIST         gPnacServInfo.cacheListHead
#define PNAC_SERV_CACHE_DURATION    gPnacServInfo.cacheTimeout
/* RowStatus values */
#define   PNAC_ACTIVE             ACTIVE
#define   PNAC_NOT_IN_SERVICE     NOT_IN_SERVICE 
#define   PNAC_NOT_READY          NOT_READY
#define   PNAC_CREATE_AND_GO      CREATE_AND_GO
#define   PNAC_CREATE_AND_WAIT    CREATE_AND_WAIT
#define   PNAC_DESTROY            DESTROY
 

#define PNAC_ALLOCATE_SERVUSER_MEMBLK(pNode) \
              (pNode = \
               (tPnacServUserInfo  *)MemAllocMemBlk(PNAC_SERVUSER_MEMPOOL_ID))

#define PNAC_ALLOCATE_SERVCACHE_MEMBLK(pNode) \
             (pNode = \
              (tPnacServAuthCache *)MemAllocMemBlk(PNAC_SERVCACHE_MEMPOOL_ID)) 

#define   PNAC_RELEASE_SERVUSER_MEMBLK(pNode) \
           MemReleaseMemBlock ( PNAC_SERVUSER_MEMPOOL_ID, \
                              (UINT1 *)pNode)
   
#define   PNAC_RELEASE_SERVCACHE_MEMBLK(pNode) \
           MemReleaseMemBlock ( PNAC_SERVCACHE_MEMPOOL_ID, \
                              (UINT1 *)pNode)
   
#define PNAC_SLL_INIT(pList) \
              TMO_SLL_Init(pList)
   
#define PNAC_SLL_INIT_NODE(pNode) \
              TMO_SLL_Init_Node(pNode)

#define PNAC_SLL_ADD(pList, pNode) \
              TMO_SLL_Add(pList, pNode)
   
#define PNAC_SLL_DELETE(pList, pNode) \
   TMO_SLL_Delete (pList, pNode)


#define PNAC_SLL_COUNT(pList) \
              TMO_SLL_Count(pList)

#define PNAC_SLL_NEXT(pList, pNode) \
              TMO_SLL_Next(pList,pNode)

#define PNAC_SLL_FIRST(pList) \
              TMO_SLL_First(pList)

#define PNAC_SLL_GET(pList) \
              TMO_SLL_Get(pList)

#define PNAC_SLL_LAST(pList) \
              TMO_SLL_Last(pList)

#define PNAC_SLL_IS_NODE_IN_LIST(pNode) \
             TMO_SLL_Is_Node_In_List(pNode)

#define PNAC_SLL_SCAN(pList,pNode,TypeCast) \
             TMO_SLL_Scan(pList,pNode,TypeCast)
   
#define PNAC_SLL_INSERT(pList, pPrev, pNode)\
          TMO_SLL_Insert((tPnacSLL*)pList, (tPnacSLLNode*)pPrev, (tPnacSLLNode*)pNode)

#define PNAC_GET_INDEX_AND_POWER(u2Value, u4Index, u1Power)              \
   do {                                                                  \
      if ((u2Value % PNAC_PORTS_PER_BYTE) == 0) {                        \
         u4Index = (u2Value / PNAC_PORTS_PER_BYTE) -1 ;                  \
         u1Power = (UINT1)0;                                                    \
      }                                                                  \
      else {                                                             \
         u4Index = u2Value / PNAC_PORTS_PER_BYTE ;                        \
         u1Power = (UINT1)(PNAC_PORTS_PER_BYTE - (u2Value % PNAC_PORTS_PER_BYTE));  \
      }                                                                  \
   }                                                                     \
   while(0)

#endif /* _PNACSRM_H */
