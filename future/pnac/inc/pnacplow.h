/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: pnacplow.h,v 1.13 2015/06/11 10:03:14 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDPnacSystemStatus ARG_LIST((INT4 *));

INT1
nmhGetFsDPnacPeriodicSyncTime ARG_LIST((UINT4 *));

INT1
nmhGetFsDPnacMaxKeepAliveCount ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDPnacSystemStatus ARG_LIST((INT4 ));

INT1
nmhSetFsDPnacPeriodicSyncTime ARG_LIST((UINT4 ));

INT1
nmhSetFsDPnacMaxKeepAliveCount ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDPnacSystemStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsDPnacPeriodicSyncTime ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsDPnacMaxKeepAliveCount ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDPnacSystemStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDPnacPeriodicSyncTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDPnacMaxKeepAliveCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDPnacStatsTable. */
INT1
nmhValidateIndexInstanceFsDPnacStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDPnacStatsTable  */

INT1
nmhGetFirstIndexFsDPnacStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDPnacStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDPnacEventUpdateFramesRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDPnacEventUpdateFramesTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDPnacPeriodicFramesTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDPnacPeriodicFramesRx ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsDPnacSlotPortTable. */
INT1
nmhValidateIndexInstanceFsDPnacSlotPortTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDPnacSlotPortTable  */

INT1
nmhGetFirstIndexFsDPnacSlotPortTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDPnacSlotPortTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDPnacPortAuthStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsDPnacPortControlledDirection ARG_LIST((INT4  , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPnacSystemControl ARG_LIST((INT4 *));

INT1
nmhGetFsPnacTraceOption ARG_LIST((INT4 *));

INT1
nmhGetFsPnacAuthenticServer ARG_LIST((INT4 *));

INT1
nmhGetFsPnacNasId ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPnacSystemControl ARG_LIST((INT4 ));

INT1
nmhSetFsPnacTraceOption ARG_LIST((INT4 ));

INT1
nmhSetFsPnacAuthenticServer ARG_LIST((INT4 ));

INT1
nmhSetFsPnacNasId ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPnacSystemControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPnacTraceOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPnacAuthenticServer ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPnacNasId ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPnacSystemControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPnacTraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPnacAuthenticServer ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPnacNasId ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPnacPaePortTable. */
INT1
nmhValidateIndexInstanceFsPnacPaePortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPnacPaePortTable  */

INT1
nmhGetFirstIndexFsPnacPaePortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPnacPaePortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPnacPaePortAuthMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPnacPaePortSupplicantCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPnacPaePortUserName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPnacPaePortPassword ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPnacPaePortStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPnacPaePortStatisticsClear ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPnacPaePortAuthStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPnacPaeAuthReAuthMax ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPnacPaePortAuthMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPnacPaePortUserName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPnacPaePortPassword ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPnacPaePortStatisticsClear ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPnacPaePortAuthStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPnacPaeAuthReAuthMax ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPnacPaePortAuthMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPnacPaePortUserName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPnacPaePortPassword ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPnacPaePortStatisticsClear ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPnacPaePortAuthStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPnacPaeAuthReAuthMax ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPnacPaePortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPnacModuleOperStatus ARG_LIST((INT4 *));

INT1
nmhGetFsPnacRemoteAuthServerType ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPnacRemoteAuthServerType ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPnacRemoteAuthServerType ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPnacRemoteAuthServerType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPnacAuthSessionTable. */
INT1
nmhValidateIndexInstanceFsPnacAuthSessionTable ARG_LIST((tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsPnacAuthSessionTable  */

INT1
nmhGetFirstIndexFsPnacAuthSessionTable ARG_LIST((tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPnacAuthSessionTable ARG_LIST((tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPnacAuthSessionIdentifier ARG_LIST((tMacAddr ,INT4 *));

INT1
nmhGetFsPnacAuthSessionAuthPaeState ARG_LIST((tMacAddr ,INT4 *));

INT1
nmhGetFsPnacAuthSessionBackendAuthState ARG_LIST((tMacAddr ,INT4 *));

INT1
nmhGetFsPnacAuthSessionPortStatus ARG_LIST((tMacAddr ,INT4 *));

INT1
nmhGetFsPnacAuthSessionPortNumber ARG_LIST((tMacAddr ,INT4 *));

INT1
nmhGetFsPnacAuthSessionInitialize ARG_LIST((tMacAddr ,INT4 *));

INT1
nmhGetFsPnacAuthSessionReauthenticate ARG_LIST((tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPnacAuthSessionInitialize ARG_LIST((tMacAddr  ,INT4 ));

INT1
nmhSetFsPnacAuthSessionReauthenticate ARG_LIST((tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPnacAuthSessionInitialize ARG_LIST((UINT4 *  ,tMacAddr  ,INT4 ));

INT1
nmhTestv2FsPnacAuthSessionReauthenticate ARG_LIST((UINT4 *  ,tMacAddr  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPnacAuthSessionTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPnacAuthSessionStatsTable. */
INT1
nmhValidateIndexInstanceFsPnacAuthSessionStatsTable ARG_LIST((tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsPnacAuthSessionStatsTable  */

INT1
nmhGetFirstIndexFsPnacAuthSessionStatsTable ARG_LIST((tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPnacAuthSessionStatsTable ARG_LIST((tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPnacAuthSessionOctetsRx ARG_LIST((tMacAddr ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsPnacAuthSessionOctetsTx ARG_LIST((tMacAddr ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsPnacAuthSessionFramesRx ARG_LIST((tMacAddr ,UINT4 *));

INT1
nmhGetFsPnacAuthSessionFramesTx ARG_LIST((tMacAddr ,UINT4 *));

INT1
nmhGetFsPnacAuthSessionId ARG_LIST((tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPnacAuthSessionAuthenticMethod ARG_LIST((tMacAddr ,INT4 *));

INT1
nmhGetFsPnacAuthSessionTime ARG_LIST((tMacAddr ,UINT4 *));

INT1
nmhGetFsPnacAuthSessionTerminateCause ARG_LIST((tMacAddr ,INT4 *));

INT1
nmhGetFsPnacAuthSessionUserName ARG_LIST((tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPnacAuthSessionStatisticsClear ARG_LIST((tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPnacAuthSessionStatisticsClear ARG_LIST((tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPnacAuthSessionStatisticsClear ARG_LIST((UINT4 *  ,tMacAddr  ,INT4 ));

/* Proto Validate Index Instance for FsPnacASUserConfigTable. */
INT1
nmhValidateIndexInstanceFsPnacASUserConfigTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsPnacASUserConfigTable  */

INT1
nmhGetFirstIndexFsPnacASUserConfigTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPnacASUserConfigTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPnacASUserConfigPassword ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPnacASUserConfigAuthProtocol ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsPnacASUserConfigAuthTimeout ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsPnacASUserConfigPortList ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPnacASUserConfigPermission ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPnacASUserConfigRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPnacASUserConfigPassword ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPnacASUserConfigAuthTimeout ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsPnacASUserConfigPortList ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPnacASUserConfigPermission ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsPnacASUserConfigRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPnacASUserConfigPassword ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPnacASUserConfigAuthTimeout ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2FsPnacASUserConfigPortList ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
PnacIsPortListValid ARG_LIST((UINT1 * ,INT4 ));

INT1
nmhTestv2FsPnacASUserConfigPermission ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsPnacASUserConfigRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPnacASUserConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPnacTrapAuthSessionTable. */
INT1
nmhValidateIndexInstanceFsPnacTrapAuthSessionTable ARG_LIST((tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsPnacTrapAuthSessionTable  */

INT1
nmhGetFirstIndexFsPnacTrapAuthSessionTable ARG_LIST((tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPnacTrapAuthSessionTable ARG_LIST((tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPnacTrapAuthSessionStatus ARG_LIST((tMacAddr ,INT4 *));
