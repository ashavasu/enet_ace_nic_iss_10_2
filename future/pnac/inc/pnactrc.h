/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                 */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : pnactrc.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : PNAC module                                    */
/*    MODULE NAME           : PNAC module                                    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 Mar 2002                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains declarations of traces used */
/*                            in PNAC Module.                                */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                      DESCRIPTION OF CHANGE/              */
/*            MODIFIED BY                FAULT REPORT NO                     */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 MAR 2002 / BridgeTeam   Initial Create.                     */
/*---------------------------------------------------------------------------*/
#ifndef _PNACTRC_H_
#define _PNACTRC_H_

/* Trace and debug flags */
#define   PNAC_TRC_FLAG            gPnacSystemInfo.u4TraceOption

/* Module names */
#define   PNAC_MOD_NAME           ((const char *)"PNAC")

#define   PNAC_INIT_SHUT_TRC      INIT_SHUT_TRC     
#define   PNAC_MGMT_TRC           MGMT_TRC          
#define   PNAC_DATA_PATH_TRC      DATA_PATH_TRC     
#define   PNAC_CONTROL_PATH_TRC   CONTROL_PLANE_TRC 
#define   PNAC_DUMP_TRC           DUMP_TRC          
#define   PNAC_OS_RESOURCE_TRC    OS_RESOURCE_TRC   
#define   PNAC_ALL_FAILURE_TRC    ALL_FAILURE_TRC   
#define   PNAC_BUFFER_TRC         BUFFER_TRC        

#define   PNAC_RED_TRC            0x00000100 

/* Trace definitions */
#ifdef  TRACE_WANTED

#define PNAC_PKT_DUMP(TraceType, pBuf, Length, Str)                           \
        MOD_PKT_DUMP(PNAC_TRC_FLAG, TraceType, PNAC_MOD_NAME, pBuf, Length, (const char *)Str)

#define PNAC_TRC(TraceType, Str)                                              \
        MOD_TRC(PNAC_TRC_FLAG, TraceType, PNAC_MOD_NAME, (const char *)Str)

#define PNAC_TRC_ARG1(TraceType, Str, Arg1)                                   \
        MOD_TRC_ARG1(PNAC_TRC_FLAG, TraceType, PNAC_MOD_NAME, (const char *)Str, Arg1)

#define PNAC_TRC_ARG2(TraceType, Str, Arg1, Arg2)                             \
        MOD_TRC_ARG2(PNAC_TRC_FLAG, TraceType, PNAC_MOD_NAME, (const char *)Str, Arg1, Arg2)

#define PNAC_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)                       \
        MOD_TRC_ARG3(PNAC_TRC_FLAG, TraceType, PNAC_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3)

#define   PNAC_DEBUG(x) {x}

#else  /* TRACE_WANTED */

#define PNAC_PKT_DUMP(TraceType, pBuf, Length, Str)                           \
         {\
             UNUSED_PARAM(Str); \
             UNUSED_PARAM(TraceType); \
             UNUSED_PARAM(Length); \
         }
#define PNAC_TRC(TraceType, Str)
#define PNAC_TRC_ARG1(TraceType, Str, Arg1) \
         {\
             UNUSED_PARAM(Arg1); \
         }

#define PNAC_TRC_ARG2(TraceType, Str, Arg1, Arg2)                             \
         {\
             UNUSED_PARAM(Arg1); \
             UNUSED_PARAM(Arg2); \
         }
  
#define PNAC_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)                       \
         {\
             UNUSED_PARAM(Arg1); \
             UNUSED_PARAM(Arg2); \
             UNUSED_PARAM(Arg3); \
         }

#define   PNAC_DEBUG(x)

#endif /* TRACE_WANTED */

#endif/* _PNACTRC_H_ */

