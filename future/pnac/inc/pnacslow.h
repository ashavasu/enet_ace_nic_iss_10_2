
/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1xPaeSystemAuthControl ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1xPaeSystemAuthControl ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1xPaeSystemAuthControl ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1xPaeSystemAuthControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1xPaePortTable. */
INT1
nmhValidateIndexInstanceDot1xPaePortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1xPaePortTable  */

INT1
nmhGetFirstIndexDot1xPaePortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1xPaePortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1xPaePortProtocolVersion ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xPaePortCapabilities ARG_LIST((INT4 , tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetDot1xPaePortInitialize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1xPaePortReauthenticate ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1xPaePortInitialize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1xPaePortReauthenticate ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1xPaePortInitialize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1xPaePortReauthenticate ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1xPaePortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1xAuthConfigTable. */
INT1
nmhValidateIndexInstanceDot1xAuthConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1xAuthConfigTable  */

INT1
nmhGetFirstIndexDot1xAuthConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1xAuthConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1xAuthPaeState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1xAuthBackendAuthState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1xAuthAdminControlledDirections ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1xAuthOperControlledDirections ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1xAuthAuthControlledPortStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1xAuthAuthControlledPortControl ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1xAuthQuietPeriod ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthTxPeriod ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthSuppTimeout ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthServerTimeout ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthMaxReq ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthReAuthPeriod ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthReAuthEnabled ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1xAuthKeyTxEnabled ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1xAuthAdminControlledDirections ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1xAuthAuthControlledPortControl ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1xAuthQuietPeriod ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot1xAuthTxPeriod ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot1xAuthSuppTimeout ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot1xAuthServerTimeout ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot1xAuthMaxReq ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot1xAuthReAuthPeriod ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot1xAuthReAuthEnabled ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1xAuthKeyTxEnabled ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1xAuthAdminControlledDirections ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1xAuthAuthControlledPortControl ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1xAuthQuietPeriod ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot1xAuthTxPeriod ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot1xAuthSuppTimeout ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot1xAuthServerTimeout ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot1xAuthMaxReq ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot1xAuthReAuthPeriod ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot1xAuthReAuthEnabled ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1xAuthKeyTxEnabled ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1xAuthConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1xAuthStatsTable. */
INT1
nmhValidateIndexInstanceDot1xAuthStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1xAuthStatsTable  */

INT1
nmhGetFirstIndexDot1xAuthStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1xAuthStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1xAuthEapolFramesRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthEapolFramesTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthEapolStartFramesRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthEapolLogoffFramesRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthEapolRespIdFramesRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthEapolRespFramesRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthEapolReqIdFramesTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthEapolReqFramesTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthInvalidEapolFramesRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthEapLengthErrorFramesRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthLastEapolFrameVersion ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthLastEapolFrameSource ARG_LIST((INT4 ,tMacAddr * ));

/* Proto Validate Index Instance for Dot1xAuthDiagTable. */
INT1
nmhValidateIndexInstanceDot1xAuthDiagTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1xAuthDiagTable  */

INT1
nmhGetFirstIndexDot1xAuthDiagTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1xAuthDiagTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1xAuthEntersConnecting ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthEapLogoffsWhileConnecting ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthEntersAuthenticating ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthAuthSuccessWhileAuthenticating ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthAuthTimeoutsWhileAuthenticating ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthAuthFailWhileAuthenticating ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthAuthReauthsWhileAuthenticating ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthAuthEapStartsWhileAuthenticating ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthAuthEapLogoffWhileAuthenticating ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthAuthReauthsWhileAuthenticated ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthAuthEapStartsWhileAuthenticated ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthAuthEapLogoffWhileAuthenticated ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthBackendResponses ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthBackendAccessChallenges ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthBackendOtherRequestsToSupplicant ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthBackendNonNakResponsesFromSupplicant ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthBackendAuthSuccesses ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthBackendAuthFails ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for Dot1xAuthSessionStatsTable. */
INT1
nmhValidateIndexInstanceDot1xAuthSessionStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1xAuthSessionStatsTable  */

INT1
nmhGetFirstIndexDot1xAuthSessionStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1xAuthSessionStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1xAuthSessionOctetsRx ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDot1xAuthSessionOctetsTx ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDot1xAuthSessionFramesRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthSessionFramesTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthSessionId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1xAuthSessionAuthenticMethod ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1xAuthSessionTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xAuthSessionTerminateCause ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1xAuthSessionUserName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for Dot1xSuppConfigTable. */
INT1
nmhValidateIndexInstanceDot1xSuppConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1xSuppConfigTable  */

INT1
nmhGetFirstIndexDot1xSuppConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1xSuppConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1xSuppPaeState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1xSuppHeldPeriod ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xSuppAuthPeriod ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xSuppStartPeriod ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xSuppAccessCtrlWithAuth ARG_LIST((INT4 ,INT4 *));
INT1
nmhGetDot1xSuppControlledPortStatus ARG_LIST((INT4,INT4 *));

INT1
nmhGetDot1xSuppMaxStart ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1xSuppHeldPeriod ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot1xSuppAuthPeriod ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot1xSuppStartPeriod ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot1xSuppAccessCtrlWithAuth ARG_LIST((INT4 ,INT4 ));
INT1
nmhSetDot1xSuppMaxStart ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1xSuppHeldPeriod ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot1xSuppAuthPeriod ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot1xSuppStartPeriod ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot1xSuppAccessCtrlWithAuth ARG_LIST((UINT4 *, INT4, INT4));
INT1
nmhTestv2Dot1xSuppMaxStart ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1xSuppConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1xSuppStatsTable. */
INT1
nmhValidateIndexInstanceDot1xSuppStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1xSuppStatsTable  */

INT1
nmhGetFirstIndexDot1xSuppStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1xSuppStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1xSuppEapolFramesRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xSuppEapolFramesTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xSuppEapolStartFramesTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xSuppEapolLogoffFramesTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xSuppEapolRespIdFramesTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xSuppEapolRespFramesTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xSuppEapolReqIdFramesRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xSuppEapolReqFramesRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xSuppInvalidEapolFramesRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xSuppEapLengthErrorFramesRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xSuppLastEapolFrameVersion ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1xSuppLastEapolFrameSource ARG_LIST((INT4 ,tMacAddr * ));
