/* FILE HEADER
 *
 * ----------------------------------------------------------------------------
 *  FILE NAME             : tcpfsm.h
 * 
 *  PRINCIPAL AUTHOR      : Ramesh Kumar
 *
 *  SUBSYSTEM NAME        : TCP
 *
 *  MODULE NAME           : High Level Interface Module
 *
 *  LANGUAGE              : C
 *
 *  TARGET ENVIRONMENT    : Any
 *
 *  DATE OF FIRST RELEASE :
 * 
 *  DESCRIPTION           : This file contains values for FSM states.
 *
 * ---------------------------------------------------------------------------
 *
 *
 *  CHANGE RECORD :
 *
 * ---------------------------------------------------------------------------
 *   VERSION        AUTHOR/DATE           DESCRIPTION OF CHANGE
 * ---------------------------------------------------------------------------
 *
 *
 * ---------------------------------------------------------------------------
 */
#ifndef  __TCP_FSM_H__
#define  __TCP_FSM_H__

/* tcpfsm.h */

#define  MAX_TCP_STATES      13 
#define  MAX_TCP_OUT_STATES  4  

/* TCP states */

#define  TCPS_FREE           0  
#define  TCPS_CLOSED         1  
#define  TCPS_LISTEN         2  
#define  TCPS_SYNSENT        3  
#define  TCPS_SYNRCVD        4  
#define  TCPS_ESTABLISHED    5  
#define  TCPS_FINWAIT1       6  
#define  TCPS_FINWAIT2       7  
#define  TCPS_CLOSEWAIT      8  
#define  TCPS_LASTACK        9  
#define  TCPS_CLOSING        10 
#define  TCPS_ABORT          12 

/* TCP type */

#define  TCP_SERVER          0  
#define  TCP_CLIENT          1  

#endif   /*TCP_FSM_H*/

