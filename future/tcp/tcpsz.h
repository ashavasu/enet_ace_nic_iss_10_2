/********************************************************************
 Copyright (C) 2012 Aricent Inc . All Rights Reserved

 $Id: tcpsz.h,v 1.3 2013/01/23 11:16:17 siva Exp $

 Description: Protocol Wrapper Routines
*********************************************************************/
enum {
    MAX_TCP_CXT_STRUCTS_SIZING_ID,
    MAX_TCP_ICMP_PARAMS_SIZING_ID,
    MAX_TCP_IP_HL_PARMS_SIZING_ID,
    MAX_TCP_NUM_CONTEXT_SIZING_ID,
    MAX_TCP_NUM_OF_FRAG_SIZING_ID,
    MAX_TCP_NUM_OF_RXMT_SIZING_ID,
    MAX_TCP_NUM_OF_SACK_SIZING_ID,
    MAX_TCP_NUM_PARAMS_SIZING_ID,
    MAX_TCP_RCV_BUFF_BLOCKS_SIZING_ID,
    MAX_TCP_SND_BUFF_BLOCKS_SIZING_ID,
    MAX_TCP_TCB_TBL_BLOCKS_SIZING_ID,
    MAX_TCP_TO_APP_BLOCKS_SIZING_ID,
    TCP_MAX_SIZING_ID
};


#ifdef  _TCPSZ_C
tMemPoolId TCPMemPoolIds[ TCP_MAX_SIZING_ID];
INT4  TcpSizingMemCreateMemPools(VOID);
VOID  TcpSizingMemDeleteMemPools(VOID);
INT4  TcpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _TCPSZ_C  */
extern tMemPoolId TCPMemPoolIds[ ];
extern INT4  TcpSizingMemCreateMemPools(VOID);
extern VOID  TcpSizingMemDeleteMemPools(VOID);
extern INT4  TcpSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _TCPSZ_C  */


#ifdef  _TCPSZ_C
tFsModSizingParams FsTCPSizingParams [] = {
{ "UINT4*", "MAX_TCP_CXT_STRUCTS", sizeof(UINT4*),MAX_TCP_CXT_STRUCTS, MAX_TCP_CXT_STRUCTS,0 },
{ "tTcpIcmpParams", "MAX_TCP_ICMP_PARAMS", sizeof(tTcpIcmpParams),MAX_TCP_ICMP_PARAMS, MAX_TCP_ICMP_PARAMS,0 },
{ "t_IP_TO_HLMS_PARMS", "MAX_TCP_IP_HL_PARMS", sizeof(t_IP_TO_HLMS_PARMS),MAX_TCP_IP_HL_PARMS, MAX_TCP_IP_HL_PARMS,0 },
{ "tTcpContext", "MAX_TCP_NUM_CONTEXT", sizeof(tTcpContext),MAX_TCP_NUM_CONTEXT, MAX_TCP_NUM_CONTEXT,0 },
{ "tFrag", "MAX_TCP_NUM_OF_FRAG", sizeof(tFrag),MAX_TCP_NUM_OF_FRAG, MAX_TCP_NUM_OF_FRAG,0 },
{ "tRxmt", "MAX_TCP_NUM_OF_RXMT", sizeof(tRxmt),MAX_TCP_NUM_OF_RXMT, MAX_TCP_NUM_OF_RXMT,0 },
{ "tSack", "MAX_TCP_NUM_OF_SACK", sizeof(tSack),MAX_TCP_NUM_OF_SACK, MAX_TCP_NUM_OF_SACK,0 },
{ "tTcpVcmParams", "MAX_TCP_NUM_PARAMS", sizeof(tTcpVcmParams),MAX_TCP_NUM_PARAMS, MAX_TCP_NUM_PARAMS,0 },
{ "tTcpRcvBuffBlock", "MAX_TCP_RCV_BUFF_BLOCKS", sizeof(tTcpRcvBuffBlock),MAX_TCP_RCV_BUFF_BLOCKS, MAX_TCP_RCV_BUFF_BLOCKS,0 },
{ "tTcpSendBuffBlock", "MAX_TCP_SND_BUFF_BLOCKS", sizeof(tTcpSendBuffBlock),MAX_TCP_SND_BUFF_BLOCKS, MAX_TCP_SND_BUFF_BLOCKS,0 },
{ "tTcpTblBlock", "MAX_TCP_TCB_TBL_BLOCKS", sizeof(tTcpTblBlock),MAX_TCP_TCB_TBL_BLOCKS, MAX_TCP_TCB_TBL_BLOCKS,0 },
{ "tTcpToApplicationParms", "MAX_TCP_TO_APP_BLOCKS", sizeof(tTcpToApplicationParms),MAX_TCP_TO_APP_BLOCKS, MAX_TCP_TO_APP_BLOCKS,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _TCPSZ_C  */
extern tFsModSizingParams FsTCPSizingParams [];
#endif /*  _TCPSZ_C  */


