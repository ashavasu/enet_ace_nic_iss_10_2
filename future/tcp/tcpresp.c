/** $Id: tcpresp.c,v 1.22 2014/03/18 12:11:57 siva Exp $ */

/* SOURCE FILE HEADER
 *
 * ----------------------------------------------------------------------------
 *  FILE NAME             : tcpresp.c
 * 
 *  PRINCIPAL AUTHOR      : Ramesh Kumar
 *
 *  SUBSYSTEM NAME        : TCP
 *
 *  MODULE NAME           : High Level Interface Module
 *
 *  LANGUAGE              : C
 *
 *  TARGET ENVIRONMENT    : Any
 *
 *  DATE OF FIRST RELEASE :
 * 
 *  DESCRIPTION           : This module sends back messages to the appln
 *                          via the TCP_TO_APP_Q
 *
 * ---------------------------------------------------------------------------
 *
 *
 *  CHANGE RECORD :
 *
 * ---------------------------------------------------------------------------
 *   VERSION        AUTHOR/DATE           DESCRIPTION OF CHANGE
 * ---------------------------------------------------------------------------
 *                  Vinay/29.11.99       +Modified HliReplyForStatus() by 
 *                                        reflecting the changes made to the 
 *                                        tRetstatus structure in tcpparms.h
 *                 Ramakrishnan/1/3/2000 +Added support for the select call 
 *                                        under the ST_ENHLISTINTF switch.
 * ------------------------------------------------------------------------*
 *     1.3      Saravanan.M    Implementation of                           *
 *              Purush            - Non Blocking Connect                   * 
 *                                - Non Blocking Accept                    *
 *                                - IfMsg Interface between TCP and IP     *
 *                                - One Queue per Socket                   *
 * ------------------------------------------------------------------------*/

#include "tcpinc.h"
/*************************************************************************/
/*  Function Name   : HliReplyForOpen                                    */
/*  Input(s)        : u4Index - Index to Tcb table                       */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK/SYSERR/ERR                                  */
/*  Description     : Send the reply for Open request to the application */
/*  CallingCondition: HliOpen                                            */
/*  GLOBAL VARIABLES AFFECTED : None                                     */
/*************************************************************************/
INT1
HliReplyForOpen (UINT4 u4Index)
{
    tTcpToApplicationParms *pParms;
    UINT4               u4Context;
    INT1                i1RetVal = ERR;

    u4Context = GetTCBContext (u4Index);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering HliReplyForOpen for CONN ID - %d\n", u4Index);
    if ((pParms = ALLOCATE_TCP_TO_APPLICATION_PARMS ()) == NULL)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC |
                     TCP_BUF_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from HliReplyForOpen. Memory allocation failed. Returned ERR.\n");
        return ERR;
    }
    pParms->u1Cmd = OPEN_CMD;
    pParms->u4ConnId = u4Index;
    pParms->cmdtype.conn.LocalIp = GetTCBlip (u4Index);
    pParms->cmdtype.conn.RemoteIp = GetTCBrip (u4Index);
    pParms->cmdtype.conn.u2LocalPort = GetTCBlport (u4Index);
    pParms->cmdtype.conn.u2RemotePort = GetTCBrport (u4Index);
    pParms->cmdtype.conn.u4IfIndex = GetTCBIfIndex (u4Index);
#ifdef SLI_WANTED
    if (GetTCBtype (u4Index) == TCP_CLIENT)
    {
        SetTCBhlicmd (u4Index, ZERO);
        i1RetVal = SliReply (pParms);
        if ((i1RetVal == FAILURE) && (pParms != NULL))
        {
            FREE_TCP_TO_APPLICATION_PARMS (pParms);
        }
    }
    else
    {
        i1RetVal = SliReplyForListen (pParms);
        if ((i1RetVal == FAILURE) && (pParms != NULL))
        {
            FREE_TCP_TO_APPLICATION_PARMS (pParms);
        }
    }
#endif
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from HliReplyForOpen. Returned %d\n", i1RetVal);
    return i1RetVal;
}

/*************************************************************************/
/*  Function Name   : pHliReplyForClose                               */
/*  Input(s)        : u4Index - Index to the Tcb table                   */
/*  Output(s)       : None                                               */
/*  Returns         : Tcp To App                                         */
/*  Description     : SEnd the reply for Close requestfrom Application   */
/*  CallingCondition: HliClose                                           */
/*  GLOBAL VARIABLES AFFECTED : None                                     */
/*************************************************************************/
tTcpToApplicationParms *
pHliReplyForClose (UINT4 u4Index)
{
    tTcpToApplicationParms *pParms;
    UINT4               u4Context;

    u4Context = GetTCBContext (u4Index);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering pHliReplyForClose for CONN ID - %d\n", u4Index);
    if ((pParms = ALLOCATE_TCP_TO_APPLICATION_PARMS ()) == NULL)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC |
                     TCP_BUF_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from pHliReplyForClose. Memory allocation failed. Returned ERR.\n");
        return (tTcpToApplicationParms *) NULL;
    }
    pParms->u1Cmd = CLOSE_CMD;
    pParms->u4ConnId = u4Index;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from pHliReplyForClose. Returned %x.\n", pParms);
    return (pParms);
}

/*************************************************************************/
/*  Function Name   : HliReplyForSend                                    */
/*  Input(s)        : u4Index - Index to the Tcb table                   */
/*                    pBuf - Pointer to the data buffer                  */
/*  Output(s)       : None.                                              */
/*  Returns         : Tcp To App                                         */
/*  Description     : Send the Reply for Write request from Application  */
/*  CallingCondition: HliWrite                                           */
/*  GLOBAL VARIABLES AFFECTED : None                                     */
/*************************************************************************/
tTcpToApplicationParms *
HliReplyForSend (UINT4 u4Index, UINT1 *pBuf, UINT4 u4BufLen)
{
    tTcpToApplicationParms *pParms;
    UINT4               u4Context;

    u4Context = GetTCBContext (u4Index);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering HliReplyForSend for CONN ID - %d\n", u4Index);
    if ((pParms = ALLOCATE_TCP_TO_APPLICATION_PARMS ()) == NULL)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC |
                     TCP_BUF_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from HliReplyForSend. Memory allocation failed. Returned ERR.\n");
        return (tTcpToApplicationParms *) NULL;
    }
    pParms->u1Cmd = SEND_CMD;
    pParms->u4ConnId = u4Index;
    pParms->cmdtype.send.pBuf = pBuf;
    pParms->cmdtype.send.u4BufLen = u4BufLen;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from HliReplyForSend. Returned %x.\n", pParms);
    return pParms;

}

/*************************************************************************/
/*  Function Name   : HliReplyForStatus                                  */
/*  Input(s)        : u4Index - Index to the Tcb Table                   */
/*  Output(s)       : None.                                              */
/*  Returns         : tcp To App params                                  */
/*  Description     : Send Reply for the status request from Application */
/*  CallingCondition: HliStatus                                          */
/*  GLOBAL VARIABLES AFFECTED : None                                     */
/*************************************************************************/
tTcpToApplicationParms *
HliReplyForStatus (UINT4 u4Index)
{
    tTcpToApplicationParms *pParms;
    UINT4               u4ParentConnId;
    UINT4               u4Context;

    u4Context = GetTCBContext (u4Index);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering HliReplyForStatus for CONN ID - %d\n", u4Index);

    if ((pParms = ALLOCATE_TCP_TO_APPLICATION_PARMS ()) == NULL)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC |
                     TCP_BUF_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from HliReplyForStatus. Memory allocation failed. Returned ERR.\n");
        return (tTcpToApplicationParms *) NULL;
    }

    pParms->u1Cmd = STATUS_CMD;
    pParms->u4ConnId = u4Index;
    pParms->cmdtype.status.LocalIp = GetTCBlip (u4Index);
    pParms->cmdtype.status.RemoteIp = GetTCBrip (u4Index);
    pParms->cmdtype.status.u2LocalPort = GetTCBlport (u4Index);
    pParms->cmdtype.status.u2RemotePort = GetTCBrport (u4Index);
    pParms->cmdtype.status.u4RcvWin = FarGetLwsize (&TCBtable[u4Index]);
    pParms->cmdtype.status.u4SendWin = GetTCBswindow (u4Index);
    pParms->cmdtype.status.u1State = GetTCBstate (u4Index);
    pParms->cmdtype.status.u2BufWaitAck = (UINT2) GetTCBsbcount (u4Index);
    pParms->cmdtype.status.u1Urg = (UINT1) ((GetTCBflags ((UINT2) u4Index)
                                             & TCBF_RUPOK) == TCBF_RUPOK);
    pParms->cmdtype.status.u1Tos = GetTCBtos (u4Index);
    pParms->cmdtype.status.u2Security = (GetTCBvalopts (u4Index)
                                         & TCPO_SECURITY) ?
        GetTCBsecurity (u4Index) : ZERO;
    pParms->cmdtype.status.u2TxmtTmout = (UINT2) GetTCBrexmt (u4Index);
    pParms->cmdtype.status.u2BufPendingReceipt =
        (UINT2) GetTCBrbcount (u4Index);

    if ((GetTCBpptcb (u4Index)) == NULL)
    {
        u4ParentConnId = u4Index;
    }
    else
    {
        u4ParentConnId = GET_TCB_INDEX (GetTCBpptcb (u4Index));
    }

    pParms->cmdtype.status.u4ParentConnId = u4ParentConnId;
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from HliReplyForStatus. Returned %x.\n", pParms);
    return pParms;
}

/*************************************************************************/
/*  Function Name   : HliReplyForAbort                                   */
/*  Input(s)        : u4Index - Index to the Tcb table                   */
/*  Output(s)       : None.                                              */
/*  Returns         : Tcp To App params                                  */
/*  Description     : Send Reply for the abort request from Application  */
/*  CallingCondition: HliAbort                                           */
/*  GLOBAL VARIABLES AFFECTED : None                                     */
/*************************************************************************/
tTcpToApplicationParms *
HliReplyForAbort (UINT4 u4Index)
{
    tTcpToApplicationParms *pParms;
    UINT4               u4Context;

    u4Context = GetTCBContext (u4Index);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering HliReplyForAbort for CONN ID - %d\n", u4Index);
    if ((pParms = ALLOCATE_TCP_TO_APPLICATION_PARMS ()) == NULL)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC |
                     TCP_BUF_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from HliReplyForAbort. Memory allocation failed. Returned ERR.\n");
        return (tTcpToApplicationParms *) NULL;
    }
    pParms->u1Cmd = ABORT_CMD;
    pParms->u4ConnId = u4Index;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from HliReplyForAbort. Returned %x.\n", pParms);
    return pParms;
}

/*************************************************************************/
/*  Function Name   : HliSendAsyMsg                                      */
/*  Input(s)        : u4Index - Index to the Tcb Table                   */
/*                    u1Message - Message text                           */
/*                    u1IcmpType - Icmp message type                     */
/*                    u1IcmpCode - ICMP message Code                     */
/*  Output(s)       : None                                               */
/*  Returns         : TCP_OK/SYSERR/ERR                                  */
/*  Description     : Sends asynchronous messages to the user.           */
/*  CallingCondition:                                                    */
/*  GLOBAL VARIABLES AFFECTED : None                                     */
/*************************************************************************/
INT1
HliSendAsyMsg (UINT4 u4Index, UINT1 u1Message, UINT1 u1IcmpType,
               UINT1 u1IcmpCode)
{
    tTcpToApplicationParms *pRetParms;
    INT4                i4TempSockDesc;
    UINT4               u4Context;

    u4Context = GetTCBContext (u4Index);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering HliSendAsyMsg for CONN ID - %d\n", u4Index);

    if (!(GetTCBvalopts (u4Index) & TCPO_ASY_REPORT))
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from HliSendAsyMsg. Returned OK.\n");
        return TCP_OK;
    }

/*    NOTE : As currently no applications has registered with TCP via function pointer to receive the
             async message.So for removing the malloc used for sending the async message to appln, that 
             piece of code is removed from version 1.17 on wards. When a application is willing to register
             through function pointer that code can be brought in*/
    if (TCBtable[u4Index].fpTcpHandleAsyncMesg == NULL)
    {
        if (GetTCBSockDesc (u4Index) == -1)
        {
            return TCP_OK;
        }
        if ((pRetParms = ALLOCATE_TCP_TO_APPLICATION_PARMS ()) == NULL)
        {
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC |
                         TCP_BUF_TRC |
                         TCP_ALL_FAIL_TRC, "TCP",
                         "Exit from HliSendAsyMsg. Memory allocation failed. Returned ERR.\n");
            return ERR;
        }
        pRetParms->u1Cmd = CONN_REPORT;
        pRetParms->cmdtype.message.u1Message = u1Message;
        pRetParms->cmdtype.message.u1IcmpType = u1IcmpType;
        pRetParms->cmdtype.message.u1IcmpCode = u1IcmpCode;

        i4TempSockDesc = GetTCBSockDesc (u4Index);
        switch (GetTCBstate (u4Index))
        {
            case TCPS_SYNSENT:
#ifdef SLI_WANTED
                if (SliReplyForAsyncParams (pRetParms,
                                            i4TempSockDesc,
                                            TCPS_SYNSENT) == FAILURE)
                {
                    if (pRetParms != NULL)
                        FREE_TCP_TO_APPLICATION_PARMS (pRetParms);
                }
#endif
                break;

            case TCPS_ESTABLISHED:
                if (GetTCBReadWaiting (u4Index))
                {
                    SetTCBReadWaiting (u4Index, FALSE);
                    GiveTCBBlockSem (u4Index);
                }
                FREE_TCP_TO_APPLICATION_PARMS (pRetParms);
                break;

            default:
                if (GetTCBReadWaiting (u4Index))
                {
                    SetTCBReadWaiting (u4Index, FALSE);
                    GiveTCBBlockSem (u4Index);
                }
                FREE_TCP_TO_APPLICATION_PARMS (pRetParms);
                break;
        }
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from HliSendAsyMsg. Returned OK.\n");
        return TCP_OK;
    }
    /* AsyncHandler pointer = NULL ? */

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from HliSendAsyMsg. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : HliReplyForNewConn                                 */
/*  Input(s)        : u4Index - Index to the Tcb Table                   */
/*                    u4NewConn - Connection Identifier for new conn     */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK/SYSERR/ERR                                  */
/*  Description     : Sends information about a connection arrived       */
/*                    to HLI.                                            */
/*  CallingCondition: FsmSynRcvd                                         */
/*  GLOBAL VARIABLES AFFECTED : None                                     */
/*************************************************************************/
INT1
HliReplyForNewConn (UINT4 u4Index, UINT4 u4NewConn)
{
    tTcpToApplicationParms *pParms;
    INT1                i1RetVal = ERR;
    UINT4               u4Context;

    u4Context = GetTCBContext (u4Index);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering HliReplyForNewConn for CONN ID - %d\n", u4Index);
    if ((pParms = ALLOCATE_TCP_TO_APPLICATION_PARMS ()) == NULL)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC |
                     TCP_BUF_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from HliReplyForNewConn. Memory allocation failed. Returned ERR.\n");
        return ERR;
    }

    pParms->u1Cmd = NEW_CONN;
    pParms->u4ConnId = u4Index;
    pParms->cmdtype.conn.u4NewConn = u4NewConn;
    pParms->cmdtype.conn.LocalIp = GetTCBlip (u4NewConn);
    pParms->cmdtype.conn.RemoteIp = GetTCBrip (u4NewConn);
    pParms->cmdtype.conn.u2LocalPort = GetTCBlport (u4NewConn);
    pParms->cmdtype.conn.u2RemotePort = GetTCBrport (u4NewConn);
    pParms->cmdtype.conn.u4IfIndex = GetTCBIfIndex (u4NewConn);
#ifdef SLI_WANTED
    i1RetVal = SliReply (pParms);
    if ((i1RetVal == FAILURE) && (pParms != NULL))
    {
        FREE_TCP_TO_APPLICATION_PARMS (pParms);
    }
#endif
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from HliReplyForNewConn. Returned %d\n", i1RetVal);
    return i1RetVal;
}

/*************************************************************************/
/*  Function Name   : HliReplyForOptions                                 */
/*  Input(s)        : u4Index - index to the Tcb table                   */
/*                    u4Option - TCP Options                             */
/*                    u4Value - Option value                             */
/*                    u1Optlen - Option length                           */
/*  Output(s)       : None.                                              */
/*  Returns         : Tcp To App params                                  */
/*  Description     : Send the reply for Optins request from Application */
/*  CallingCondition: HliOptions                                         */
/*  GLOBAL VARIABLES AFFECTED : None                                     */
/*************************************************************************/
tTcpToApplicationParms *
HliReplyForOptions (UINT4 u4Index, UINT4 u4Option, UINT4 u4Value,
                    UINT1 u1Optlen)
{
    tTcpToApplicationParms *pParms;
    UINT4               u4Context;

    u4Context = GetTCBContext (u4Index);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering HliReplyForOptions for CONN ID - %d\n", u4Index);
    if ((pParms = ALLOCATE_TCP_TO_APPLICATION_PARMS ()) == NULL)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC |
                     TCP_BUF_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from HliReplyForOptions. Memory allocation failed. Returned ERR.\n");
        return (tTcpToApplicationParms *) NULL;
    }

    pParms->u1Cmd = OPTIONS_CMD;
    pParms->u4ConnId = u4Index;
    pParms->cmdtype.option.u4Option = u4Option;
    pParms->cmdtype.option.u4Value = u4Value;
    pParms->cmdtype.option.u1Optlen = u1Optlen;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from HliReplyForOptions. Returned %x.\n", pParms);
    return (pParms);
}

/*************************************************************************/
/*  Function Name   : TcpCheckLocalAddrPort                              */
/*  Input(s)        : IpAddr / u4IpAddr - Local Ip address               */
/*                    u2Port - Tcp Port                                  */
/*                    u1State - State of connection for given Addr&port  */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_CONN_NOT_EXIST /TCP_CONN_EXIST                 */
/*  Description     : Check if there exists a connection with the given  */
/*                    Ip address and port                                */
/*  CallingCondition:                                                    */
/*  GLOBAL VARIABLES AFFECTED : None                                     */
/*************************************************************************/
INT4
TcpCheckLocalAddrPort (tIpAddr IpAddr, UINT2 u2Port, UINT1 *u1State)
{
    return (TcpCheckLocalAddrPortInCxt (VCM_DEFAULT_CONTEXT, IpAddr,
                                        u2Port, u1State));
}

/*************************************************************************/
/*  Function Name   : TcpCheckLocalAddrPortInCxt                         */
/*  Input(s)        : u4Context Id - VR context                          */
/*                    IpAddr / u4IpAddr - Local Ip address               */
/*                    u2Port - Tcp Port                                  */
/*                    u1State - State of connection for given Addr&port  */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_CONN_NOT_EXIST /TCP_CONN_EXIST                 */
/*  Description     : Check if there exists a connection with the given  */
/*                    Ip address and port in the given context           */
/*  CallingCondition:                                                    */
/*  GLOBAL VARIABLES AFFECTED : None                                     */
/*************************************************************************/
INT4
TcpCheckLocalAddrPortInCxt (UINT4 u4Context, tIpAddr IpAddr,
                            UINT2 u2Port, UINT1 *u1State)
{
    UINT4               u4TcbIndex;
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TcpCheckLocalAddrPortInCxt");

    for (u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
    {
        if (GetTCBstate (u4TcbIndex) == TCPS_FREE)
        {
            continue;
        }
        if ((u2Port != ZERO) && (GetTCBlport (u4TcbIndex) == u2Port) &&
            ((V6_ADDR_CMP (&(GetTCBlip (u4TcbIndex)), &IpAddr)) == 0) &&
            (GetTCBContext (u4TcbIndex) == u4Context))
        {
            *u1State = GetTCBstate (u4TcbIndex);
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from TcpCheckLocalAddrPortInCxt. Returned %d\n",
                         TCP_CONN_EXIST);
            return TCP_CONN_EXIST;
        }
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                 "TCP",
                 "Exit from TcpCheckLocalAddrPortInCxt. Returned %d\n",
                 TCP_CONN_NOT_EXIST);
    return TCP_CONN_NOT_EXIST;
}

/*************************************************************************/
/*  Function Name   : TcpChangeRecvBufSize                               */
/*  Input(s)        : u4TcbIndex - Index of TCB                          */
/*                    u4Value - size of receive buffer                   */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK / TCP_NOT_OK                                */
/*  Description     : This function will increase the size of TCP        */
/*                    receive window                                     */
/*************************************************************************/
INT4
TcpChangeRecvBufSize (UINT4 u4TcbIndex, UINT4 u4Value)
{
    /* We will not allow the user to reduce the 
     * receive buffer size */
    if (u4Value < GetTCBrbsize (u4TcbIndex))
    {
        return TCP_NOT_OK;
    }

    if ((u4Value >> GetTCBRecvWindowScale (u4TcbIndex)) > TCP_MAX_2BYTE_VAL)
    {
        /* We cannot hold the value in 2 bytes even after
         * window scaling. When window scaling is not present
         * in the connection, 65535 is the maximmum window size
         * since Window Scale = 0 */
        return TCP_NOT_OK;
    }

    SetTCBrbsize (u4TcbIndex, u4Value);
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : TcpChangeSendBufSize                               */
/*  Input(s)        : u4TcbIndex - Index of TCB                          */
/*                    u4Value - size of Sender  buffer                   */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK / TCP_NOT_OK                                */
/*  Description     : This function will increase the size of TCP        */
/*                    Sender  window                                     */
/*************************************************************************/
INT4
TcpChangeSendBufSize (UINT4 u4TcbIndex, UINT4 u4Value)
{

    /* We will not allow the user to reduce the 
     * sending buffer size */
    if (u4Value < GetTCBsbsize (u4TcbIndex))
    {
        return TCP_NOT_OK;
    }

    if (GetTCBsndbuf (u4TcbIndex) == NULL)
    {
        return TCP_NOT_OK;
    }
    SetTCBsbsize (u4TcbIndex, u4Value);
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : TcpCheckListenConn                                 */
/*  Input(s)        : u4TcbIndex - Index of TCB                          */
/*  Output(s)       : None.                                              */
/*  Returns         : TRUE / FALSE                                       */
/*  Description     : This function checks if the given TCB index is for */
/*                    a passively opened connection                      */
/*************************************************************************/
INT1
TcpCheckListenConn (UINT4 u4TcbIndex)
{
    if ((GetTCBstate (u4TcbIndex) == TCPS_LISTEN) &&
        (GetTCBtype (u4TcbIndex) == TCP_SERVER))
    {
        return (TRUE);
    }
    else
    {
        return (FALSE);
    }
}
