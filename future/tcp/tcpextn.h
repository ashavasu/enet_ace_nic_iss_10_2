/** $Id: tcpextn.h,v 1.15 2013/06/07 13:32:13 siva Exp $ */

#ifndef __TCPEXTN_H__
#define __TCPEXTN_H__
#if defined (IP_WANTED) || defined (IP6_WANTED)
/* external definitons */
extern INT4             gu4TcpDbgMap; 
extern INT4             gi4TcpGlobalTraceDebug; 
extern tTimerListId     gTcpTimerListId;
extern tOsixQId         gTcpQId;
extern tTcpContext    **gpTcpContext;
extern tTcpContext        *gpTcpCurrContext;
#ifndef LNXIP4_WANTED
extern tTcpSystemSize   gTcpSysSizingParams;
#endif
extern FILE             *trace_ptr;

/* */
extern INT1  (*fsm_switch[])  PROTO ((tTcb *, tSegment *, tIpTcpHdr *));
extern INT1  (*osm_switch[])  PROTO ((UINT4, UINT1));
extern INT1   HliSendAsyMsg   PROTO  (( UINT4, UINT1, UINT1, UINT1));

/* Imported from SLI */
extern VOID   GenerateSemName   PROTO ((char,char* ));
extern INT4   SliTcpFindSockDesc PROTO((UINT4));
extern INT1   SliReply              PROTO ((tTcpToApplicationParms *));
extern INT4   AcceptMesgCount       PROTO ((UINT4, UINT4*));
extern INT4   SliTcpRegisterConnid   PROTO((INT4, UINT4));
extern INT1   SliReplyForListen      PROTO (( tTcpToApplicationParms * ));
extern INT1   SliReplyForAsyncParams PROTO (( tTcpToApplicationParms *, 
                                              INT4 , UINT1));
extern tSdtEntry *SliGetSockEntry PROTO ((INT4 i4SockDesc));
extern INT1   SliTcpFindMd5Key  PROTO ((INT4, tIpAddr, UINT1 *, UINT1 *));
#ifndef SLI_TCPAO_GET
#define SLI_TCPAO_GET
tsliTcpAoMktListNode * SliGetTcpAo PROTO ((INT4 i4SockDesc, tIpAddr IpAddr));
UINT1 * SliGetTcpAoNghMktCfg PROTO((INT4 , tIpAddr ));
UINT1 * SliGetTcpAoNghIcmpCfg PROTO((INT4 , tIpAddr ));
#endif

/* imported from IPv6 */
extern tOsixSemId          gTcbAllocSemId;
extern tOsixTaskId         gTcpTaskId;

extern tMemPoolId          gTcbTblPoolId;
extern tMemPoolId          gTcbSendBufPoolId;
extern tMemPoolId          gTcbRcvBufPoolId;
extern tMemPoolId          gTcpFragPoolId;
extern tMemPoolId          gTcpSackPoolId;
extern tMemPoolId          gTcpRxmtPoolId;
extern tMemPoolId          gTcpIcmpParamsPoolId;
extern tMemPoolId          gTcpHlParamsPoolId;

#endif
#endif

