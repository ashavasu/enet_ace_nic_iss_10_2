/** $Id: tcprxmt.c,v 1.22 2017/09/13 13:34:08 siva Exp $ */

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                       */
/*                                                                           */
/*    FILE NAME        : tcprxmt.c                                                */
/*    PRINCIPAL AUTHOR    : Aricent Inc.                               */
/*      SUBSYSTEM NAME        : FAR Module                                      */
/*      MODULE NAME        :                                                    */
/*    LANGUAGE        : ANSI-C                                                      */
/*    TARGET ENVIRONMENT    : Any                                                */
/*    DATE OF FIRST RELEASE    :                                                 */
/*    DESCRIPTION        : This file contains routines related to                   */
/*                                the maintenance of retransmission queue    */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*    CHANGE RECORD:                                                                     */
/*    Version        Author        Description of change                               */
/*****************************************************************************/
/*       1.0           Vinay/15.12.99      Created                           */
/*****************************************************************************/

#include "tcpinc.h"

/*****************************************************************************/
/* Function     : FarAddToRxmtQ                                              */
/* Description  : This routine is called from OsmSendSegment() when data   */
/*                segment is transmitted to the peer TCP. RxmtQ is maintained*/
/*          in ascending order ie. the 1st node is the 1st segment that          */
/*          is transmitted. This routine does not take care of                   */
/*          overlapping transmitted segments.                                    */
/* Input        : pTcb         : ptr to the corresponding TCB                */
/*                i4Seq        : starting sequence Number of the Tx segment  */
/*                u4StartIndex : starting index in the send buffer           */
/*                u4DataLen    : datalength of the Tx segment                */
/* Output       : Modifies pTcbRxmtQHdr with the new retransmission node, if */
/*                rxmtQ is empty.                                            */
/* Returns      : TCP_OK     if segment is successfully added to the rxmtQ   */
/*                ERR    otherwise                                           */
/*****************************************************************************/

INT1
FarAddToRxmtQ (tTcb * pTcb, INT4 i4Seq, UINT4 u4StartIndex, UINT4 u4DataLength)
{
    tRxmt              *pRxmt = NULL;
    tRxmt              *pPrev = NULL, *pTemp = NULL;
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4TcbIndex = GET_TCB_INDEX (pTcb);
    u4Context = GetTCBContext (u4TcbIndex);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering FarAddToRxmtQ. \n");

    pPrev = pTemp = NULL;
    if ((pRxmt = (tRxmt *) MemAllocMemBlk (gTcpRxmtPoolId)) == NULL)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC |
                     TCP_BUF_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from FarAddToRxmtQ. Memory allocation failure. Returned ERR.\n");
        return ERR;
    }
    pRxmt->i4Seq = i4Seq;
    pRxmt->u4StartIndex = u4StartIndex;
    if ((u4StartIndex + u4DataLength) <= GetTCBsbsize (u4TcbIndex))
    {
        pRxmt->u4EndIndex = u4StartIndex + u4DataLength - TCP_ONE;
    }
    else
    {
        pRxmt->u4EndIndex = (u4StartIndex + u4DataLength) -
            (GetTCBsbsize (u4TcbIndex)) - TCP_ONE;
    }

    pRxmt->u1SackFlag = ZERO;
    pRxmt->pNext = NULL;

    /* this block is appended to the end */
    if (pTcb->pTcbRxmtQHdr == NULL)
    {
        pTcb->pTcbRxmtQHdr = pRxmt;
    }
    else
    {
        pTemp = pTcb->pTcbRxmtQHdr;
        while (pTemp != NULL)
        {
            pPrev = pTemp;
            pTemp = (tRxmt *) pTemp->pNext;
        }
        pPrev->pNext = pRxmt;
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FarAddToRxmtQ. Returned OK.\n");
    return TCP_OK;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FarUpdateRxmtQ                                             */
/*                                                                           */
/* Description  : This routine is called from LINFarHandleAck() as a part */
/*                of ACK processing. It traverses through the rxmtQ and      */
/*                deletes all the nodes which have been completely ACKed by  */
/*          the peer TCP.                                              */
/*                                                                           */
/* Input        : pTcb   : ptr to the corresponding TCB                      */
/*                i4Ack  : Ack No from the peer TCP                          */
/*                                                                           */
/* Output       : pTcbRxmtQHdr , if any node is ACKed                        */
/*                                                                           */
/* Returns      : TCP_OK                                                         */
/*                                                                           */
/*****************************************************************************/

INT1
FarUpdateRxmtQ (tTcb * pTcb, INT4 i4Ack)
{
    tRxmt              *pNode, *pPrev, *pTemp;
    UINT4               u4TcbIndex;
    UINT4               u4Context;
    UINT1               u1FullyAcked;

    u4TcbIndex = GET_TCB_INDEX (pTcb);
    u4Context = GetTCBContext (u4TcbIndex);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering FarUpdateRxmtQ. \n");
    u1FullyAcked = FALSE;
    if ((pNode =
         FarTraverseRxmtQ (pTcb, (i4Ack - TCP_ONE), &u1FullyAcked)) != NULL)
    {

        /* all the node upto pNode need to be deleted */
        pTemp = pTcb->pTcbRxmtQHdr;
        while (pTemp != pNode)
        {
            pPrev = pTemp;
            pTemp = (tRxmt *) pTemp->pNext;
            MemReleaseMemBlock (gTcpRxmtPoolId, (UINT1 *) pPrev);
        }

        if (u1FullyAcked)
        {
            pTcb->pTcbRxmtQHdr = (tRxmt *) pNode->pNext;
            MemReleaseMemBlock (gTcpRxmtPoolId, (UINT1 *) pNode);
        }
        else
        {
            pTcb->pTcbRxmtQHdr = pNode;
        }

    }                            /* end if */
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FarUpdateRxmtQ. Returned OK.\n");
    return TCP_OK;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FarTraverseRxmtQ                                           */
/*                                                                           */
/* Description  : This routine traverses the rxmtQ and returns the ptr to the*/
/*                node which subsumes within it the ACK No. It also indicates*/
/*                if the node is completely subsumed.                        */
/*                                                                           */
/* Input        : pTcb          : ptr to the corresponding TCB               */
/*                i4Ack         : Ack No from the peer TCP                   */
/*                pu1FullyAcked : ptr to indicate if the node is completely  */
/*                          subsumed                                   */
/*                                                                           */
/* Output       : pu1FullyAcked                                              */
/*                                                                           */
/* Returns      : pNode , ptr to the node which subsumes ACK No              */
/*                NULL  , otherwise                                          */
/*                                                                           */
/*****************************************************************************/

tRxmt              *
FarTraverseRxmtQ (tTcb * pTcb, INT4 i4Ack, UINT1 *pu1FullyAcked)
{
    tRxmt              *pNode;
    UINT2               u2DataLength;
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4TcbIndex = GET_TCB_INDEX (pTcb);
    u4Context = GetTCBContext (u4TcbIndex);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering FarTraverseRxmtQ.\n");
    u2DataLength = ZERO;

    pNode = pTcb->pTcbRxmtQHdr;
    while (pNode != NULL)
    {

        if (pNode->u4StartIndex <= pNode->u4EndIndex)
        {
            u2DataLength =
                (UINT2) ((pNode->u4EndIndex - pNode->u4StartIndex) + TCP_ONE);
        }
        else
        {
            u2DataLength =
                (UINT2) ((GetTCBsbsize (u4TcbIndex) - pNode->u4StartIndex +
                          pNode->u4EndIndex) + TCP_ONE);
        }

        if ((SEQCMP (i4Ack, pNode->i4Seq) >= ZERO) &&
            (SEQCMP (i4Ack, (pNode->i4Seq + u2DataLength - TCP_ONE)) <= ZERO))
        {
            /* found the right node */

            if (SEQCMP (i4Ack, (pNode->i4Seq + u2DataLength - TCP_ONE)) == ZERO)
            {
                *pu1FullyAcked = TRUE;
            }
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from FarTraverseRxmtQ. Returned %x\n", pNode);
            return pNode;
        }

        pNode = (tRxmt *) pNode->pNext;

    }                            /* end of while */
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                 "Exit from FarTraverseRxmtQ. Returned NULL.\n");
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OsmSendSegFromRxmtQ                                        */
/*                                                                           */
/* Description  : This routine retransmits the specified node from the rxmtQ.*/
/*                It is called from OsmRetransmit() upon expiry of the      */
/*          retransmission timer. If SACK option is enabled,                     */
/*          OsmRetransmit() calls it a number of times to retransmit            */
/*          all the non-SACKed segments from the rxmtQ. Is is also               */
/*          called from FsmProcessOption(). If NAK option is configured          */
/*          & the incoming segment contains NAK option,                          */
/*          FsmProcessOption() immediately invokes this routine to               */
/*          retransmit the missing segments.                                     */
/*                                                                           */
/* Input        : pTcb          : ptr to the corresponding TCB               */
/*                u1NthNode     : node to be retransmitted                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TCP_OK   , if the segment is transmitted successfully      */
/*                ERR  , otherwise                                           */
/*                                                                           */
/*****************************************************************************/

INT1
OsmSendSegFromRxmtQ (tTcb * pTcb, UINT1 u1NthNode)
{
    tCRU_BUF_CHAIN_HEADER *pSegOut = NULL;
    tIpTcpHdr           SendSeg;
    tIpTcpHdr          *pSendSeg = NULL;
    tIpHdr             *pIpHdr = NULL;
    tTcpHdr            *pTcpHdr = NULL;
    tRxmt              *pRxmtQNthNode = NULL, *pRxmtQTmpNode = NULL;
    UINT1              *pBuf = NULL;
    UINT1               au1IpTcpHdr[sizeof (tIpHdr) + sizeof (tTcpHdr)];
    UINT4               u4DataLen, u4CruBufSize, u4BufLen;
    UINT4               u4RcvWnd;
    UINT2               u2LowerWindow;
    UINT2               u2UpperWindow = 0;
    UINT2               u2DataOffset;
    UINT1               u1CodeValue, u1IpOptLen;
    UINT1               u1IpHdrSize;
    INT1                i1TCPOptLen;
    UINT1               u1IPTCPHdrSize, u1TCPHdrSize;
    UINT1               u1NodeCnt;
    UINT4               u4TcbIndex;
    UINT4               u4StartIndex, u4EndIndex;
    tIpAddr             SrcAddr, DestAddr;
    UINT1               u1IpToSend;
    UINT1               u1TcpHdrStart;
#ifdef IP6_WANTED
    tHlToIp6Params      Ip6SendParms;
#endif
    UINT1               au1Md5Digest[MD5_DIGEST_LEN];
    UINT1              *pTcpOpt = NULL;
    UINT4               u4TcpOptIndex = TCP_ZERO;
    UINT4               u4Context = TCP_ZERO;
    UINT1               u1TCPHdrSizePrev = TCP_ZERO;

    MEMSET (au1IpTcpHdr, TCP_ZERO, (sizeof (tIpHdr) + sizeof (tTcpHdr)));

    u1CodeValue = ZERO;
    u4TcbIndex = GET_TCB_INDEX (pTcb);
    u4Context = GetTCBContext (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering OsmSendSegFromRxmtQ. \n");

    pRxmtQTmpNode = pTcb->pTcbRxmtQHdr;
    u1NodeCnt = TCP_ONE;
    if (u1NthNode < u1NodeCnt)
    {
        return ERR;
    }
    while (u1NodeCnt != u1NthNode)
    {
        u1NodeCnt++;
        pRxmtQTmpNode = (tRxmt *) pRxmtQTmpNode->pNext;
        if (pRxmtQTmpNode == NULL)
        {
            return ERR;
        }
    }
    pRxmtQNthNode = pRxmtQTmpNode;

    u4StartIndex = pRxmtQNthNode->u4StartIndex;
    u4EndIndex = pRxmtQNthNode->u4EndIndex;

    if (u4StartIndex < u4EndIndex)
    {
        u4DataLen = (u4EndIndex - u4StartIndex) + TCP_ONE;
    }
    else
    {
        u4DataLen =
            (GetTCBsbsize (u4TcbIndex) - u4StartIndex) + u4EndIndex + TCP_ONE;
    }

    i1TCPOptLen = OsmGetTCBOptLen (u4TcbIndex);
    if (i1TCPOptLen == ERR)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from OsmSendSegFromRxmtQ. Returned ERR.\n");
        return ERR;
    }
    SrcAddr = GetTCBlip (u4TcbIndex);
    DestAddr = GetTCBrip (u4TcbIndex);
    if ((IsIP6Addr (SrcAddr)) || (IsIP6Addr (DestAddr)))
    {
        u1IpToSend = (UINT1) IP6;
        u1IpOptLen = ZERO;
        u1IpHdrSize = ZERO;
    }
    else
    {
        u1IpToSend = (UINT1) IP4;
        u1IpOptLen = OsmGetIpOptlen (u4TcbIndex);
        u1IpHdrSize = (UINT1) (IP_HEADER_LENGTH + u1IpOptLen);
    }
    u1TcpHdrStart = (UINT1) ((IP6_HEADER_LEN) + u1IpOptLen);
    if (u1TcpHdrStart >= (sizeof (tIpHdr)) + (sizeof (tTcpHdr)))
    {
        return ERR;
    }
    u1IPTCPHdrSize = (UINT1) (u1IpHdrSize + TCP_HEADER_LENGTH + i1TCPOptLen);
    u4CruBufSize = u1IPTCPHdrSize + u4DataLen;

    if ((pSegOut = CRU_BUF_Allocate_MsgBufChain (u4CruBufSize, ZERO)) == NULL)
    {
        TCP_MOD_TRC (u4Context,
                     TCP_ENTRY_EXIT_TRC | TCP_BUF_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from OsmSendSegFromRxmtQ. Allocation of buffer chain failed. Returned ERR.\n");
        return ERR;
    }
    MEMSET (pSegOut->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pSegOut, "OsmSndSegFrm");

    TCP_MOD_TRC (u4Context, TCP_BUF_TRC, "TCP",
                 "Memory allocation for buffer chain successful.\n");
    pSendSeg = &SendSeg;
    pIpHdr = (tIpHdr *) (VOID *) au1IpTcpHdr;
    pTcpHdr = (tTcpHdr *) (VOID *) (&au1IpTcpHdr[u1TcpHdrStart]);
    pSendSeg->ip = pIpHdr;
    pSendSeg->tcp = pTcpHdr;
    u4RcvWnd = FarGetLwsize (pTcb);
    if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_BW)
    {
        u2LowerWindow = (UINT2) (u4RcvWnd & TCP_LOWER_WINDOW_MASK);
        /* Upper Window is set only when BIg Window is supported */
        u2UpperWindow =
            (UINT2) ((u4RcvWnd & TCP_UPPER_WINDOW_MASK) >> TCP_BIT_SHIFT_16);
    }
    else                        /* HSTCP */
    {
        /* We do not allow u4RcvWnd >> GetTCBRecvWindowScale(u4TcbIndex) to be
         * set more than 65535. So the value will fit into UINT2 */
        u2LowerWindow =
            (UINT2) (u4RcvWnd >> GetTCBRecvWindowScale (u4TcbIndex));
    }

    /* The fields in the packet are being put in network byte order */
    V6_HTONL (&(pIpHdr->SrcAddr), &SrcAddr);
    V6_HTONL (&(pIpHdr->DestAddr), &DestAddr);
    pIpHdr->u1Proto = TCP_PROTOCOL;
    pTcpHdr->u2Sport = OSIX_HTONS (pTcb->u2TcbLocalPort);
    pTcpHdr->u2Dport = OSIX_HTONS (pTcb->u2TcbRemotePort);
    pTcpHdr->i4Ack = OSIX_HTONL (pTcb->i4TcbRnext);
    pTcpHdr->u2Window = OSIX_HTONS (u2LowerWindow);
    pTcpHdr->i4Seq = OSIX_HTONL (pRxmtQNthNode->i4Seq);    /* clarify 
                                                           about typecasting */
    pTcpHdr->u1Hlen =
        (TCP_HEADER_LENGTH >> TCP_BYTES_FOR_SHORT) << TCP_VER_SHIFT;

    if ((GetTCBflags (u4TcbIndex) & TCBF_FIRSTSEND) == ZERO)
    {
        u1CodeValue = TCPF_ACK;
    }

    /* Add the enabled options and modify the header length */
    OsmAddOption (pTcb, pSendSeg, u2UpperWindow);

    pTcpHdr->u2Urgptr = ZERO;
    pTcpHdr->u1Code = u1CodeValue;

    u1TCPHdrSize =
        (UINT1) (((pTcpHdr->
                   u1Hlen & TCP_IPVER_MASK) >> TCP_VER_SHIFT) <<
                 TCP_BYTES_FOR_SHORT);

    /* If TCP-AO/MD5 option is enabled, final TCP header size updated
     * with TCP-AO/MD5 option length. Required for correct offset in CRU buffer
     * before copy of segment payload. Current header length taken in temp
     * variable for getting offset in TCP header where digest will be
     * copied before sending segment to IP*/
    if (GetTCBTcpAoEnabled (u4TcbIndex))
    {
        u1TCPHdrSizePrev = u1TCPHdrSize;
        u1TCPHdrSize = (UINT1) (u1TCPHdrSize + TCPAO_OPT_LEN);
        pTcpHdr->u1Hlen = (UINT1)
            ((u1TCPHdrSize >> TCP_BYTES_FOR_SHORT) << TCP_VER_SHIFT);
    }
    else if (GetTCBmd5keylen (u4TcbIndex) != TCP_ZERO)
    {
        u1TCPHdrSizePrev = u1TCPHdrSize;
        u1TCPHdrSize += MD5_OPT_LEN + TCP_TWO;
        pTcpHdr->u1Hlen = (UINT1)
            ((u1TCPHdrSize >> TCP_BYTES_FOR_SHORT) << TCP_VER_SHIFT);
    }

    u4BufLen = GetTCBsbsize (u4TcbIndex) - u4StartIndex;
    u4BufLen = (u4DataLen < u4BufLen) ? u4DataLen : u4BufLen;
    pBuf = (UINT1 *) &(pTcb->pTcbSndbuf[u4StartIndex]);
    u2DataOffset = (UINT2) (u1IpHdrSize + u1TCPHdrSize);
    if ((CRU_BUF_Copy_OverBufChain (pSegOut, pBuf, u2DataOffset, u4BufLen)) !=
        OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pSegOut, FALSE);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from OsmSendSegFromRxmtQ. Returned ERR.\n");
        return ERR;
    }
    u2DataOffset += (UINT2) u4BufLen;

    /* if all data has not been copied , u4DataLen will be greater than
       u4BufLen. So wrap arround the send buffer and copy the remaining */

    if (u4DataLen > u4BufLen)
    {
        pBuf = (UINT1 *) &(pTcb->pTcbSndbuf[ZERO]);
        u4BufLen = u4DataLen - u4BufLen;
        if ((CRU_BUF_Copy_OverBufChain (pSegOut, pBuf, u2DataOffset, u4BufLen))
            != OSIX_SUCCESS)
        {
            CRU_BUF_Release_MsgBufChain (pSegOut, FALSE);
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from OsmSendSegFromRxmtQ. Returned ERR.\n");
            return ERR;
        }
    }

    if (u1IpToSend == IP6)
    {
        pIpHdr->u2IpOptLen = ZERO;
    }
    else if (u1IpOptLen < MAX_IP_OPT_LEN)
    {
        pIpHdr->u2IpOptLen = u1IpOptLen;
        LINOsmCopyIpOptions (pIpHdr->au1IpOpts, u4TcbIndex);
    }
    pIpHdr->u2Totlen = (UINT2) (u4CruBufSize - u1IpHdrSize);
    pIpHdr->u2Totlen = OSIX_HTONS (pIpHdr->u2Totlen);
    pTcpHdr->u2Cksum = ZERO;

    /* 
     * Last option to be added to the TCP header is TCP-AO
     * if tcp-AO is enabled add TCP-AO MAC , TCP-AO & MD5 will not be
     * allowed on same packet
     */
    if (GetTCBTcpAoEnabled (u4TcbIndex))
    {

        /* MAC is calculated including the TCP-AO option also */
        /* MAC field in the TCP-AO option will be set to zero */
        pTcpOpt = pSendSeg->tcp->au1TcpOpts;
        u4TcpOptIndex = (UINT4) (u1TCPHdrSizePrev - TCP_HEADER_LENGTH);

        if (TcpOptFillTcpAoFields (u4TcbIndex, pSendSeg, pSegOut,
                                   &pTcpOpt[u4TcpOptIndex]) != TCP_OK)
        {
            TCP_MOD_TRC (u4Context,
                         TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC | TCP_BUF_TRC, "TCP", "Exit from OsmSendSegFromRxmtQ. TCP-AO MAC generation failed for \
                Conn - %d.\n", u4TcbIndex);
            CRU_BUF_Release_MsgBufChain (pSegOut, FALSE);
            return ERR;
        }
    }
    else if (GetTCBmd5keylen (u4TcbIndex) != TCP_ZERO)
    {
        MEMSET (au1Md5Digest, TCP_ZERO, sizeof (au1Md5Digest));
        if (TcpOptGenerateMD5Digest
            (u4Context, pSendSeg, pSegOut, au1Md5Digest,
             (UINT1 *) GetTCBpmd5key (u4TcbIndex),
             GetTCBmd5keylen (u4TcbIndex)) == TCP_OK)
        {
            pTcpOpt = pSendSeg->tcp->au1TcpOpts;
            u4TcpOptIndex = u1TCPHdrSizePrev - TCP_HEADER_LENGTH;
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;
            pTcpOpt[u4TcpOptIndex++] = TCP_MD5;
            pTcpOpt[u4TcpOptIndex++] = MD5_OPT_LEN;
            MEMCPY ((pTcpOpt + u4TcpOptIndex), au1Md5Digest, MD5_DIGEST_LEN);
        }
        else
        {
            TCP_MOD_TRC (u4Context,
                         TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC | TCP_BUF_TRC, "TCP", "Exit from OsmSendSegFromRxmtQ.MD5 digest generation failed\
                for Conn - %d.\n", u4TcbIndex);
            CRU_BUF_Release_MsgBufChain (pSegOut, FALSE);
            return ERR;
        }
    }

    pTcpHdr->u2Cksum = TcpPktCksum (u4Context, pSegOut, pSendSeg);
    pTcpHdr->u2Cksum = OSIX_HTONS (pTcpHdr->u2Cksum);

    if ((CRU_BUF_Copy_OverBufChain (pSegOut, au1IpTcpHdr,
                                    IP6_HEADER_LEN,
                                    u1TCPHdrSize + u1IpOptLen)) != OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pSegOut, FALSE);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from OsmSendSegFromRxmtQ. Returned ERR.\n");
        return ERR;
    }
    if (u1IpToSend == IP6)
    {
#ifdef IP6_WANTED
        MEMSET (&Ip6SendParms, 0, sizeof (Ip6SendParms));
        LliGetIp6SendParms (pSegOut, u4TcbIndex, (UINT2) u4CruBufSize,
                            &Ip6SendParms);

        if (LliSendSegmentToIP6 (pSegOut, u4CruBufSize, &Ip6SendParms) ==
            TCP_OK)
        {
            IncTCPstat (u4Context, RetransSegs);
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from OsmSendSegFromRxmtQ. Returned OK.\n");
            return TCP_OK;
        }
#endif
    }
#ifdef IP_WANTED
    else if (LliSendSegmentToIP (pSegOut, u4CruBufSize,
                                 LliGetIpSendParms (pSegOut, u4TcbIndex,
                                                    (UINT2) (u4CruBufSize
                                                             -
                                                             IP_HEADER_LENGTH -
                                                             u1IpOptLen))) ==
             TCP_OK)
    {
        IncTCPstat (u4Context, RetransSegs);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from OsmSendSegFromRxmtQ. Returned OK.\n");
        return TCP_OK;
    }
#endif
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                 "Exit from OsmSendSegFromRxmtQ. Returned ERR.\n");
    return ERR;
}
