/** $Id: tcpinp.c,v 1.33 2015/02/28 12:15:10 siva Exp $ */

/* SOURCE FILE HEADER
 *
 * ----------------------------------------------------------------------------
 *  FILE NAME             : tcpinp.c
 * 
 *  PRINCIPAL AUTHOR      : Ramesh Kumar
 *
 *  SUBSYSTEM NAME        : TCP
 *
 *  MODULE NAME           : Input Module
 *
 *  LANGUAGE              : C
 *
 *  TARGET ENVIRONMENT    : Any
 *
 *  DATE OF FIRST RELEASE :
 * 
 *  DESCRIPTION           : This file contains functions that handles incoming
 *                          segments from IP.
 *
 * ---------------------------------------------------------------------------
 *
 *
 *  CHANGE RECORD :
 *
 * ---------------------------------------------------------------------------
 *   VERSION        AUTHOR/DATE           DESCRIPTION OF CHANGE
 * ---------------------------------------------------------------------------
 *                  Sri. Chellapa        Added code for handling ICMP messages.
 *     1.6          Rebecca Rufus        Enabling of ICMP checked before       
 *                  13-Oct-'98.          sending ICMP error messages.Refer     
 *                                       Problem Id 14.                        
 * ---------------------------------------------------------------------------
 *      -           Ramakrishnan         Changed the way option length is
 *                                       handled under ST_ENH switch. Ref:RFC1122
* ------------------------------------------------------------------------*/
/*     1.8      Saravanan.M    Implementation of                           */
/*              Purush            - Non Blocking Connect                   */
/*                                - Non Blocking Accept                    */
/*                                - IfMsg Interface between TCP and IP     */
/*                                - One Queue per Socket                   */
/* ------------------------------------------------------------------------*/

#include "tcpinc.h"
#include "ip.h"
#include "vcm.h"
#include "fssyslog.h"

extern INT4         gi4TcpSysLogId;
/************************************************************************/
/* Function Name  : IptaskTcpInput                                      */
/* Input(s)       : pSeg - The segment received                         */
/* Output(s)      : None                                                */
/* Returns        : TCP_OK/ERR                                          */
/* Description    : Every time a segment comes from IP, this routine    */
/*                  handles that segment.  The expected buffer          */
/*   structure is 
     +----------+-----------+-----------------+
     |  IP HDR  |  TCP HDR  |       DATA      |
     +----------+-----------+-----------------+

CALLING CONDITION
Whenever a segment comes onto the queue IP_TO_TCP_Q

GLOBAL VARIABLES AFFECTED none
*************************************************************************/
INT1
LliHandlePacket (UINT1 *pSeg)
{
    tTcb               *ptcb = NULL;
    tIpTcpHdr           RcvSeg;
    tIpTcpHdr          *pRcvSeg = NULL;
    tIpHdr             *pRcvIpHdr = NULL;
    tTcpHdr            *pRcvTcpHdr = NULL;
    UINT1               u1IpVer = 0;
    UINT2               u2Len = 0;
#ifdef IP6_WANTED
    tIcmp6Params        Icmp6Params;
    tIpAddr             v6RecvAddr;
#endif
    UINT1               au1IpTcpHdr[sizeof (tIpHdr) + sizeof (tTcpHdr)];
    UINT4               u4Index = 0;
    UINT1               u1TCPCode = 0;
    INT1                i1SegAfRwin = 0;
    t_IP_TO_HLMS_PARMS *pIP2TCPparms = NULL;
#ifdef CERT_WANTED
    UINT4               u4SrcAddr = 0;
#endif
#ifdef IP_WANTED
    t_ICMP              IcmpParms;
#endif
    tOsixSemId          MainSemId = 0;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4Context = 0;
    INT4                i4TCPSeq = 0;
    UINT4               u4RWlast = 0;
    INT4                i4TCPAck = 0;
    tOsixSysTime        CurrentTime = 0;
    UINT2               u2SegDataLen = 0;
    UINT1               u1TCPHlen = 0;
    tUtlInAddr          InAddr;
#ifdef IP6_WANTED
    tUtlIn6Addr         In6Addr;
#endif
    UINT1               au1SrcAddr[SYSLOG_MAX_HOSTNAME];
    UINT1               au1DestAddr[SYSLOG_MAX_HOSTNAME];

    MEMSET (au1SrcAddr, 0, sizeof (au1SrcAddr));
    MEMSET (au1DestAddr, 0, sizeof (au1DestAddr));

    pIP2TCPparms = (t_IP_TO_HLMS_PARMS *) (VOID *) pSeg;
    if (pIP2TCPparms == NULL)
    {
        return ERR;

    }
    u4Context = pIP2TCPparms->u4ContextId;
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering LliHandlePacket.\n");
    pBuf = pIP2TCPparms->pBuf;

    IncTCPstat (u4Context, InSegs);
    IncTCPHCstat (u4Context, u8HcInSegs);

    if ((pIP2TCPparms->u1Cmd != IP_TCP_DATA) &&
        (pIP2TCPparms->u1Cmd != IP_ICMP_DATA))
    {
        CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
        TcpReleaseParams (u4Context, pSeg, pIP2TCPparms->u1Cmd);
        TCP_MOD_TRC (u4Context,
                     TCP_ENTRY_EXIT_TRC | TCP_BUF_TRC | TCP_ALL_FAIL_TRC,
                     "TCP",
                     "Exit from LliHandlePacket. Buffer released. Returned ERR");
        return ERR;
    }
    CRU_BUF_Copy_FromBufChain (pBuf, &u1IpVer, ZERO, TCP_ONE);
    u1IpVer = (UINT1) ((u1IpVer & TCP_IPVER_MASK) >> TCP_VER_SHIFT);
    pRcvSeg = &RcvSeg;
    MEMSET (au1IpTcpHdr, 0, sizeof (au1IpTcpHdr));
    pRcvSeg->ip = (tIpHdr *) (VOID *) (au1IpTcpHdr);
    if (u1IpVer == IP6)
    {
#ifdef IP6_WANTED
        if (LliUpdateIp6TcpHdr (u4Context, pSeg, pRcvSeg) == SYSERR)
        {
            IncTCPstat (u4Context, InErrs);
            CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
            TcpReleaseParams (u4Context, pSeg, pIP2TCPparms->u1Cmd);
            TCP_MOD_TRC (u4Context,
                         TCP_ENTRY_EXIT_TRC | TCP_BUF_TRC | TCP_ALL_FAIL_TRC,
                         "TCP",
                         "Exit from lli_handle_pkt. Buffer released. Returned ERR.\n");
            return ERR;
        }
#endif
    }
    else
    {
#ifdef IP_WANTED
        if (LliUpdateIp4TcpHdr (u4Context, pSeg, pRcvSeg) == SYSERR)
        {
            IncTCPstat (u4Context, InErrs);
            CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
            TcpReleaseParams (u4Context, pSeg, pIP2TCPparms->u1Cmd);
            TCP_MOD_TRC (u4Context,
                         TCP_ENTRY_EXIT_TRC | TCP_BUF_TRC | TCP_ALL_FAIL_TRC,
                         "TCP",
                         "Exit from lli_handle_pkt. Buffer released. Returned ERR.\n");
            return ERR;
        }

#endif
    }
    pRcvIpHdr = pRcvSeg->ip;
    pRcvTcpHdr = pRcvSeg->tcp;

    if (pIP2TCPparms->u1Cmd == IP_ICMP_DATA)
    {

        if ((ptcb = LINInpFindConnection (pSeg, pRcvIpHdr, pRcvTcpHdr))
            != (tTcb *) ERR)
        {
            /*
             * Ignore Icmp messages on TCP-AO enabled connection unless
             * configured to accept 
             */
            tIpAddr             TempAddr;
            INT4                i4SockDesc = TCP_ZERO;
            UINT1              *pu1IcmpCfg = NULL;
            tsliTcpAoMktListNode *ptcpAoMktNd = NULL;
            u4Index = ((UINT4) (GET_TCB_INDEX (ptcb)));
            if (GetTCBtype (u4Index) == TCP_SERVER)
            {
                i4SockDesc = SliTcpFindSockDesc (u4Index);
                V6_NTOHL (&TempAddr, &(pRcvSeg->ip->SrcAddr));
                if (NULL != (ptcpAoMktNd = SliGetTcpAo (i4SockDesc, TempAddr)))
                {
                    pu1IcmpCfg = &ptcpAoMktNd->u1IcmpAccpt;
                }
            }
            else if (GetTCBTcpAoEnabled (u4Index))
            {
                pu1IcmpCfg = &(GetTCBTcpAoIcmpAcc (u4Index));
            }
            if ((gpTcpContext[u4Context]->i2IcmpMessagesEnabled) &&
                ((NULL == pu1IcmpCfg) || (*pu1IcmpCfg == TCP_ONE)))
            {
                IcmpHandleMessage (pSeg, u4Index);
            }
            else
            {
                IncrTcpAoIcmpIgnCtr (u4Index);

            }
        }

        CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
        TcpReleaseParams (u4Context, pSeg, pIP2TCPparms->u1Cmd);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_BUF_TRC, "TCP",
                     "Exit from lli_handle_pkt. Buffer released. Returned OK.\n");
        return TCP_OK;
    }

    /*Do the checksum, to see validity of data */
    if (TcpPktCksum (u4Context, pBuf, pRcvSeg) != TCP_VALID_CKSUM)
    {
        IncTCPstat (u4Context, InErrs);
        CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
        TcpReleaseParams (u4Context, pSeg, pIP2TCPparms->u1Cmd);
        TCP_MOD_TRC (u4Context,
                     TCP_ENTRY_EXIT_TRC | TCP_BUF_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from lli_handle_pkt. Buffer released. Returned ERR.\n");
        return ERR;
    }

    /* Segment will be processed in network byte order */

    /* Check if the packet is addressed correctly */

    u1TCPCode = pRcvTcpHdr->u1Code;

#ifdef IP6_WANTED
    V6_NTOHL (&v6RecvAddr, &(pRcvIpHdr->DestAddr));
#endif

    if ((u1IpVer == IP4) &&
        (((pIP2TCPparms->u1Flag == IP_MCAST
           || pIP2TCPparms->u1Flag == IP_BCAST)) && (u1TCPCode & TCPF_SYN)))
    {
        IncTCPstat (u4Context, InErrs);
        CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
        TcpReleaseParams (u4Context, pSeg, pIP2TCPparms->u1Cmd);
        TCP_MOD_TRC (u4Context,
                     TCP_ENTRY_EXIT_TRC | TCP_BUF_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from lli_handle_pkt. Buffer released. Returned ERR.\n");
        return ERR;
    }
#ifdef IP6_WANTED
    else if ((u1IpVer == IP6) && (IS_ADDR_MULTI (v6RecvAddr))
             && (u1TCPCode & TCPF_SYN))
    {
        IncTCPstat (u4Context, InErrs);
        CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
        TcpReleaseParams (u4Context, pSeg, pIP2TCPparms->u1Cmd);
        TCP_MOD_TRC (u4Context,
                     TCP_ENTRY_EXIT_TRC | TCP_BUF_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from lli_handle_pkt. Buffer released. Returned ERR.\n");
        return ERR;
    }
#endif
#ifdef CERT_WANTED
    /* ANVL BUG FIX : The following IP Addresses are reserved and so 
     * they are filtered. This is according to the CERT Advisory 
     * CA-96.21. Reference: www.cert.org/advisories. 
     *
     *  10.0.0.0 - 10.255.255.255
     *  127.0.0.0 - 127.255.255.255 
     *  172.16.0.0 - 172.31.255.255 
     *  192.168.0.0 - 192.168.255.255
     */
    u4SrcAddr = OSIX_NTOHL (V4_FROM_V6_ADDR (pRcvIpHdr->SrcAddr));
    if (((u4SrcAddr > 0x0a000000) && (u4SrcAddr < 0x0affffff)) ||
        ((u4SrcAddr > 0x7f000000) && (u4SrcAddr < 0x7fffffff)) ||
        ((u4SrcAddr > 0xac100000) && (u4SrcAddr < 0xac1fffff)) ||
        ((u4SrcAddr > 0xc0a80000) && (u4SrcAddr < 0xc0a8ffff)))
    {
        IncTCPstat (u4Context, InErrs);
        CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
        TcpReleaseParams (u4Context, pSeg, pIP2TCPparms->u1Cmd);
        TCP_MOD_TRC (u4Context,
                     TCP_ENTRY_EXIT_TRC | TCP_BUF_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from lli_handle_pkt. Buffer released. Returned ERR.\n");
        return ERR;
    }
#endif /* CERT_WANTED */

    /* find corresponding TCB */
    if ((ptcb = LINInpFindConnection (pSeg, pRcvIpHdr, pRcvTcpHdr))
        == (tTcb *) ERR)
    {
        /* ANVL BUG FIX : When there is no TCB for the connection, 
         * the TCP is in the closed or free state. Therefore, the 
         * corresponding action has to be taken. */

        FsmClosed1 (u4Context, pRcvSeg);
        if (u1IpVer == IP4)
        {
#ifdef IP_WANTED
            MEMSET (&IcmpParms, 0, sizeof (IcmpParms));
            IcmpParms.i1Type = ICMP_DEST_UNREACH;
            IcmpParms.i1Code = ICMP_PORT_UNREACH;
            IcmpParms.u4ContextId = u4Context;

            InAddr.u4Addr = (V4_FROM_V6_ADDR (pRcvIpHdr->SrcAddr));
            u2Len = (UINT2) (STRLEN (INET_NTOA (InAddr)) < SYSLOG_MAX_HOSTNAME ?
                             STRLEN (INET_NTOA (InAddr)) : SYSLOG_MAX_HOSTNAME -
                             1);
            STRNCPY (au1SrcAddr, INET_NTOA (InAddr), u2Len);
            au1SrcAddr[u2Len] = '\0';
            InAddr.u4Addr = (V4_FROM_V6_ADDR (pRcvIpHdr->DestAddr));
            STRNCPY (au1DestAddr, INET_NTOA (InAddr), u2Len);
            au1DestAddr[u2Len] = '\0';
            SYS_LOG_MSG ((SYSLOG_WARN_LEVEL, (UINT4) gi4TcpSysLogId,
                          "Connection attempt to closed/non-active TCP port."
                          " Possible Replay attack."
                          "Source %s(%d) - Destination %s(%d)",
                          au1SrcAddr,
                          OSIX_NTOHS (pRcvSeg->tcp->u2Sport),
                          au1DestAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Dport)));
            IpGenerateIcmpErrorMesg (pBuf, IcmpParms, FALSE);
#endif
        }
        else
        {
#ifdef IP6_WANTED
            Icmp6Params.u1Type = ICMP6_DEST_UNREACHABLE;
            Icmp6Params.u1Code = ICMP6_PORT_UNREACHABLE;
            Icmp6Params.u2Len =
                (UINT2) (pRcvIpHdr->u2Totlen + (IP6_HEADER_LEN));
            /*icmp6  datalength */
            Icmp6Params.u2Id = TCP_ZERO;
            Icmp6Params.u2Seq = TCP_ZERO;
            Icmp6Params.u4ContextId = u4Context;
            V6_ADDR_COPY (&Icmp6Params.ip6Src, &pRcvIpHdr->DestAddr);
            V6_ADDR_COPY (&Icmp6Params.ip6Dst, &pRcvIpHdr->SrcAddr);
            Icmp6Params.u4Index = TCP_IF_FOR_ICMPV6;
            MEMCPY (&In6Addr.u1addr, &((pRcvSeg->ip->SrcAddr).u1_addr),
                    IPVX_IPV6_ADDR_LEN);
            u2Len =
                (UINT2) (STRLEN (INET_NTOA6 (In6Addr)) <
                         SYSLOG_MAX_HOSTNAME ? STRLEN (INET_NTOA6 (In6Addr)) :
                         SYSLOG_MAX_HOSTNAME - 1);
            STRNCPY (au1SrcAddr, INET_NTOA6 (In6Addr), u2Len);
            au1SrcAddr[u2Len] = '\0';
            MEMCPY (&In6Addr.u1addr, &((pRcvSeg->ip->DestAddr).u1_addr),
                    IPVX_IPV6_ADDR_LEN);
            STRNCPY (au1DestAddr, INET_NTOA6 (In6Addr), u2Len);
            au1DestAddr[u2Len] = '\0';
            SYS_LOG_MSG ((SYSLOG_WARN_LEVEL, (UINT4) gi4TcpSysLogId,
                          "Connection attempt to closed/non-active TCP port."
                          " Possible Replay attack."
                          "Source %s(%d) - Destination %s(%d)",
                          au1SrcAddr,
                          OSIX_NTOHS (pRcvSeg->tcp->u2Sport),
                          au1DestAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Dport)));
            Ip6RcvIcmpErrorPkt (pBuf, &Icmp6Params);
#endif
        }
        IncTCPstat (u4Context, InErrs);
        TcpReleaseParams (u4Context, pSeg, pIP2TCPparms->u1Cmd);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from lli_handle_pkt. Returned ERR.\n");
        return ERR;
    }
    /* get tcp index */
    u4Index = ((UINT4) (GET_TCB_INDEX (ptcb)));

    /* Check TCP-AO option present, if so validate it */
    if (LINInpCheckTCPAOOption (pRcvSeg, pBuf, u4Index) != TCP_OK)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
        TcpReleaseParams (u4Context, pSeg, pIP2TCPparms->u1Cmd);
        TCP_MOD_TRC (u4Context,
                     TCP_ENTRY_EXIT_TRC | TCP_BUF_TRC | TCP_ALL_FAIL_TRC,
                     "TCP", "Exit from lli_handle_pkt. Incoming TCP-AO failure.\
                     Returned ERR.\n");
        return ERR;
    }
    /* After connection is found, perform MD5 related verification
     * Drop segment if verification fails */
    if (LINInpCheckMd5Option (pRcvSeg, pBuf, u4Index) != TCP_OK)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
        TcpReleaseParams (u4Context, pSeg, pIP2TCPparms->u1Cmd);
        TCP_MOD_TRC (u4Context,
                     TCP_ENTRY_EXIT_TRC | TCP_BUF_TRC | TCP_ALL_FAIL_TRC,
                     "TCP", "Exit from lli_handle_pkt. Incoming MD5 failure.\
                     Returned ERR.\n");
        return ERR;
    }

    /* tcp vulnerability - Blind reset attack using the SYN bit */
    /* If the SYN bit is set */
    if (((u1TCPCode & TCPF_SYN) == TCPF_SYN) &&
        (!((ptcb->u1TcbState == TCPS_LISTEN)
           || (ptcb->u1TcbState == TCPS_SYNSENT))))
    {
        /* get sys(curr) time */
        OsixGetSysTime (&CurrentTime);
        /* time diff of prev timestamp and curr time
           is less than max allowed */
        if (((CurrentTime - gpTcpContext[u4Context]->u4TcpLstChaAckTimStmp)
             <= TCP_CHA_ACK_MAX_TIME) &&
            (gpTcpContext[u4Context]->u4TcpChaAckCounter >=
             TCP_CHA_ACK_MAX_COUNT))
        {
            /* Do not Send ChaAck */
            IncTCPstat (u4Context, InErrs);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            TcpReleaseParams (u4Context, pSeg, pIP2TCPparms->u1Cmd);
            TCP_MOD_TRC (u4Context,
                         TCP_ENTRY_EXIT_TRC | TCP_BUF_TRC | TCP_ALL_FAIL_TRC,
                         "TCP ",
                         "Exit from lli_handle_pkt:SYN Challenge Ack not sent "
                         "because count reached MAX. "
                         "Buffer released. Returned TCP_NOT_OK. \n");
            return TCP_NOT_OK;
        }
        else if ((CurrentTime -
                  gpTcpContext[u4Context]->u4TcpLstChaAckTimStmp) >
                 TCP_CHA_ACK_MAX_TIME)
        {
            gpTcpContext[u4Context]->u4TcpLstChaAckTimStmp = CurrentTime;    /* change timestamp value to curr time */
            gpTcpContext[u4Context]->u4TcpChaAckCounter = 0;
        }

        LINFarSendAck (ptcb, pRcvSeg);
        gpTcpContext[u4Context]->u4TcpChaAckCounter++;    /* after sending challenge ack increment the counter */
        IncTCPstat (u4Context, InErrs);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        TcpReleaseParams (u4Context, pSeg, pIP2TCPparms->u1Cmd);
        TCP_MOD_TRC (u4Context,
                     TCP_ENTRY_EXIT_TRC | TCP_BUF_TRC | TCP_ALL_FAIL_TRC,
                     "TCP ",
                     "Exit from lli_handle_pkt:SYN Challenge Ack sent "
                     "Buffer released. Returned TCP_OK.\n ");
        return TCP_OK;
    }

    /* tcp vulnerability - Blind reset attack using the RST bit */
    /* If the RST bit is set */
    i4TCPSeq = OSIX_NTOHL (pRcvTcpHdr->i4Seq);
    if (((u1TCPCode & TCPF_RST) == TCPF_RST) &&
        (SEQCMP (i4TCPSeq, GetTCBrnext (u4Index)) != ZERO))
    {
        /* the sequence number within the
         * current receive window */
        /* highest possible sequence number that lies in window */
        /* Compute receiver window size: reference line 252 tcpinpfn.c */
        u4RWlast = (((UINT4) GetTCBrnext (u4Index)) +
                    (GetTCBrbsize (u4Index) - GetTCBrbcount (u4Index)) -
                    TCP_ONE);
        /* (RCV.NXT < SEG.SEQ < RCV.NXT+RCV.WND) */
        if ((SEQCMP (i4TCPSeq, GetTCBrnext (u4Index)) > ZERO) &&
            (SEQCMP (i4TCPSeq, (INT4) u4RWlast) <= ZERO))
        {
            /* get sys(curr) time */
            OsixGetSysTime (&CurrentTime);
            /* time diff of prev timestamp and curr time
             *            is less than max allowed */
            if (((CurrentTime -
                  gpTcpContext[u4Context]->u4TcpLstChaAckTimStmp) <=
                 TCP_CHA_ACK_MAX_TIME)
                && (gpTcpContext[u4Context]->u4TcpChaAckCounter >=
                    TCP_CHA_ACK_MAX_COUNT))
            {
                /* Do not Send ChaAck */
                IncTCPstat (u4Context, InErrs);
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                TcpReleaseParams (u4Context, pSeg, pIP2TCPparms->u1Cmd);
                TCP_MOD_TRC (u4Context,
                             TCP_ENTRY_EXIT_TRC | TCP_BUF_TRC |
                             TCP_ALL_FAIL_TRC, "TCP ",
                             "Exit from lli_handle_pkt:RST Challenge Ack not sent "
                             "because count reached MAX. "
                             "Buffer released. Returned TCP_NOT_OK.\n ");
                return TCP_NOT_OK;
            }
            else if ((CurrentTime -
                      gpTcpContext[u4Context]->u4TcpLstChaAckTimStmp) >
                     TCP_CHA_ACK_MAX_TIME)
            {
                gpTcpContext[u4Context]->u4TcpLstChaAckTimStmp = CurrentTime;    /* change timestamp value to curr time */
                gpTcpContext[u4Context]->u4TcpChaAckCounter = 0;
            }

            LINFarSendAck (ptcb, pRcvSeg);
            gpTcpContext[u4Context]->u4TcpChaAckCounter++;    /* after sending challenge ack increment the counter */
            IncTCPstat (u4Context, InErrs);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            TcpReleaseParams (u4Context, pSeg, pIP2TCPparms->u1Cmd);
            TCP_MOD_TRC (u4Context,
                         TCP_ENTRY_EXIT_TRC | TCP_BUF_TRC | TCP_ALL_FAIL_TRC,
                         "TCP ",
                         "Exit from lli_handle_pkt:RST Challenge Ack sent "
                         "Buffer released. Returned TCP_OK.\n ");
            return TCP_OK;
        }

        /* If segment is beyond the receive window */
        /* (SEG.SEQ < RCV.NXT || SEG.SEQ > RCV.NXT+RCV.WND) */
        /* drop the segment */
        IncTCPstat (u4Context, InErrs);
        CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
        TcpReleaseParams (u4Context, pSeg, pIP2TCPparms->u1Cmd);
        TCP_MOD_TRC (u4Context,
                     TCP_ENTRY_EXIT_TRC | TCP_BUF_TRC | TCP_ALL_FAIL_TRC,
                     "TCP ",
                     "Exit from lli_handle_pkt.Dropped the segment "
                     "because seq.no is beyond the receive window "
                     "Buffer released. Returned ERR.\n ");
        return ERR;
    }

    /* tcp vulnerability - Blind data injection attack */
    i4TCPAck = OSIX_NTOHL (pRcvTcpHdr->i4Ack);

    u1TCPHlen =
        (UINT1) (((pRcvTcpHdr->u1Hlen) >> TCP_VER_SHIFT) <<
                 TCP_SHIFT_BYTE_TO_WORD);
    u2SegDataLen = (UINT2) OSIX_NTOHS (pRcvIpHdr->u2Totlen);
    u2SegDataLen = (UINT2) (u2SegDataLen - u1TCPHlen);

    /* (((SND.UNA - MAX.SND.WND) <= SEG.ACK <= SND.NXT) != TRUE) */
    if ((u2SegDataLen != 0) && (!
                                (((GetTCBsuna (u4Index) -
                                   (INT4) GetTCBMaxswnd (u4Index)) <=
                                  i4TCPAck)
                                 && (i4TCPAck <= (GetTCBsnext (u4Index))))))
    {
        /* drop the segment */
        IncTCPstat (u4Context, InErrs);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        TcpReleaseParams (u4Context, pSeg, pIP2TCPparms->u1Cmd);
        TCP_MOD_TRC (u4Context,
                     TCP_ENTRY_EXIT_TRC | TCP_BUF_TRC | TCP_ALL_FAIL_TRC,
                     "TCP ",
                     "Exit from lli_handle_pkt.Dropped the segment "
                     "because ack is invalid "
                     "Buffer released. Returned ERR.\n ");
        return ERR;

    }

    /* else */
    /* the segment is valid */
    i1SegAfRwin = FALSE;
    if (!LINInpIsSegInRcvWin (ptcb, pRcvIpHdr, pRcvTcpHdr, &i1SegAfRwin))
    {
        /*segment is NOT inside the window */

        if (!i1SegAfRwin)
        {
            /* Fix for LAND Attack* If we are in TCP_SYN_RECEIVED and 
               ACK Bit is not set in the incoming segment,We Should not 
               Jump directly to dropafterack. */

            if (!((ptcb->u1TcbState == TCPS_SYNRCVD) &&
                  (u1TCPCode != TCPF_ACK)))
            {
                if (!(u1TCPCode & TCPF_RST))
                {
                    /* ANVL BUG FIX : Refer RFC 793, page 69, under SEGMENT 
                       ARRIVES. "If an incoming segment is not acceptable, 
                       an ACK should be sent in reply unless the RST bit 
                       is set. If the RST bit is   set drop the segment." 
                       So the segment is going to be dropped if the 
                       RST bit is set. */
                    if (u1IpVer == IP4)
                    {
                        InAddr.u4Addr = (V4_FROM_V6_ADDR (pRcvIpHdr->SrcAddr));
                        u2Len =
                            (UINT2) (STRLEN (INET_NTOA (InAddr)) <
                                     SYSLOG_MAX_HOSTNAME ?
                                     STRLEN (INET_NTOA (InAddr)) :
                                     SYSLOG_MAX_HOSTNAME - 1);
                        STRNCPY (au1SrcAddr, INET_NTOA (InAddr), u2Len);
                        au1SrcAddr[u2Len] = '\0';
                        InAddr.u4Addr = (V4_FROM_V6_ADDR (pRcvIpHdr->DestAddr));
                        STRNCPY (au1DestAddr, INET_NTOA (InAddr), u2Len);
                        au1DestAddr[u2Len] = '\0';
                        SYS_LOG_MSG ((SYSLOG_WARN_LEVEL, (UINT4) gi4TcpSysLogId,
                                      "Invalid segment of data received."
                                      " Possible Replay attack."
                                      "Source %s(%d) - Destination %s(%d)",
                                      au1SrcAddr,
                                      OSIX_NTOHS (pRcvSeg->tcp->u2Sport),
                                      au1DestAddr,
                                      OSIX_NTOHS (pRcvSeg->tcp->u2Dport)));
                    }
                    else
                    {
#ifdef IP6_WANTED
                        MEMCPY (&In6Addr.u1addr,
                                &((pRcvSeg->ip->SrcAddr).u1_addr),
                                IPVX_IPV6_ADDR_LEN);
                        u2Len =
                            (UINT2) (STRLEN (INET_NTOA6 (In6Addr)) <
                                     SYSLOG_MAX_HOSTNAME ?
                                     STRLEN (INET_NTOA6 (In6Addr)) :
                                     SYSLOG_MAX_HOSTNAME - 1);
                        STRNCPY (au1SrcAddr, INET_NTOA6 (In6Addr), u2Len);
                        au1SrcAddr[u2Len] = '\0';
                        MEMCPY (&In6Addr.u1addr,
                                &((pRcvSeg->ip->DestAddr).u1_addr),
                                IPVX_IPV6_ADDR_LEN);
                        STRNCPY (au1DestAddr, INET_NTOA6 (In6Addr), u2Len);
                        au1DestAddr[u2Len] = '\0';
                        SYS_LOG_MSG ((SYSLOG_WARN_LEVEL, (UINT4) gi4TcpSysLogId,
                                      "Connection attempt to closed/non-active TCP "
                                      "port. Possible Replay attack."
                                      "Source %s(%d) - Destination %s(%d)",
                                      au1SrcAddr,
                                      OSIX_NTOHS (pRcvSeg->tcp->u2Sport),
                                      au1DestAddr,
                                      OSIX_NTOHS (pRcvSeg->tcp->u2Dport)));
#endif
                    }
                    LINFarSendAck (ptcb, pRcvSeg);
                }
            }

            /* If segment is beyond the receive window, then drop it */
        }
    }
    /*segment is inside the window */
    else
    {
        /* Check TCP Options */
        LINInpCheckOptions (ptcb, pRcvTcpHdr);

        if (GetTCBvalopts (u4Index) & TCPO_KEEPALIVE)
        {
            TcpTmrRestartTimer (&(ptcb->TcbKaTimer),
                                GetTCBKaMainTmOut (u4Index));
            SetTCBKaRetransOverCnt (u4Index, 0);
            TCP_MOD_TRC (u4Context, TCP_OS_RES_TRC, "TCP",
                         "Keepalive main timer restarted.\n");
        }

        MainSemId = TCBtable[u4Index].MainSemId;
        /*inititate fsm */
        if ((TakeTCBmainsem (u4Index)) == OSIX_SUCCESS)
        {
            (fsm_switch[GetTCBstate (u4Index)]) (ptcb, pBuf, pRcvSeg);
            if (MainSemId == TCBtable[u4Index].MainSemId)
            {
                GiveTCBmainsem (u4Index);
            }
        }
    }

    CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
    TcpReleaseParams (u4Context, pSeg, pIP2TCPparms->u1Cmd);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_BUF_TRC, "TCP",
                 "Exit from lli_handle_pkt. Returned NOT_OK.\n");
    return TCP_NOT_OK;
}

/************************************************************************/
/* Function           : IptaskTcpInput                                  */
/* Input(s)           : pBuf - Buffer from lowerlayer                   */
/*                      u2Len - IP data Len                             */
/*                      u2Port - Inface the Data has arrived from       */
/*                      InterfaceId - Actual interface structure        */
/*                      u1Flag - Mode of receive                        */
/* Output(s)          : None                                            */
/* Returns            : SUCCESS, FAILURE                                */
/* Action :                                                             */
/* Sets up parameters and queues msg to TCP                             */
/************************************************************************/
VOID
IptaskTcpInput (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len, UINT4 u4Port,
                tCRU_INTERFACE InterfaceId, UINT1 u1Flag)
{
    t_IP_TO_HLMS_PARMS *pParms = NULL;
    UINT4               u4Context = 0;

    UNUSED_PARAM (InterfaceId.u1_InterfaceNum);
    VcmGetContextInfoFromIpIfIndex (u4Port, &u4Context);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering IptaskTcpInput.\n");

    pParms = (t_IP_TO_HLMS_PARMS *) MemAllocMemBlk (gTcpHlParamsPoolId);
    if (pParms == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exiting IptaskTcpInput. Unable to allocate TCP_HL_Parms.\n");
        return;
    }
    pParms->pBuf = pBuf;
    pParms->u1Cmd = IP_TCP_DATA;
    pParms->u2Port = (UINT2) u4Port;
    pParms->u1Flag = u1Flag;
    pParms->u2Len = u2Len;
    pParms->u4ContextId = u4Context;

    if (OsixQueSend (gTcpQId, (UINT1 *) &pParms, OSIX_DEF_MSG_LEN))
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MemReleaseMemBlock (gTcpHlParamsPoolId, (UINT1 *) pParms);
        TCP_MOD_TRC (u4Context,
                     TCP_ENTRY_EXIT_TRC | TCP_OS_RES_TRC | TCP_BUF_TRC |
                     TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from IptaskTcpInput. Sent to queue failed. Released buffer. Returned FAILURE.\n");
        return;
    }
    OsixEvtSend (gTcpTaskId, TCP_IP_IF_EVENT);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from IptaskTcpInput. Returned SUCCESS.\n");
    return;
}

/************************************************************************/
/* Function           : Ip6taskTcpInput                                 */
/* Input(s)           : pBuf - Buffer from lowerlayer                   */
/*                      u2Len - IP data Len                             */
/*                      u2Port - Inface the Data has arrived from       */
/*                      InterfaceId - Actual interface structure        */
/*                      u1Flag - Mode of receive                        */
/* Output(s)          : None                                            */
/* Returns            : SUCCESS, FAILURE                                */
/* Action :                                                             */
/* Sets up parameters and queues msg to TCP                             */
/************************************************************************/
VOID
Ip6taskTcpInput (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len, UINT4 u4Port,
                 tCRU_INTERFACE InterfaceId, UINT1 u1Flag)
{
    t_IP_TO_HLMS_PARMS *pParms = NULL;
    UINT4               u4Context = 0;

    UNUSED_PARAM (InterfaceId.u1_InterfaceNum);

    VcmGetContextIdFromCfaIfIndex (u4Port, &u4Context);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering Ip6taskTcpInput\n");

    pParms = (t_IP_TO_HLMS_PARMS *) MemAllocMemBlk (gTcpHlParamsPoolId);
    if (pParms == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exiting Ip6taskTcpInput Unable to allocate TCP_HL_Parms.\n");
        return;
    }
    pParms->pBuf = pBuf;
    pParms->u1Cmd = IP_TCP_DATA;
    pParms->u2Port = (UINT2) u4Port;
    pParms->u1Flag = u1Flag;
    pParms->u2Len = u2Len;
    pParms->u4ContextId = u4Context;

    if (OsixQueSend (gTcpQId, (UINT1 *) &pParms, OSIX_DEF_MSG_LEN))
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MemReleaseMemBlock (gTcpHlParamsPoolId, (UINT1 *) pParms);
        TCP_MOD_TRC (u4Context,
                     TCP_ENTRY_EXIT_TRC | TCP_OS_RES_TRC | TCP_BUF_TRC |
                     TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from Ip6taskTcpInput Sent to queue failed. Released buffer. Returned FAILURE.\n");
        return;
    }
    OsixEvtSend (gTcpTaskId, TCP_IP_IF_EVENT);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from Ip6taskTcpInput Returned SUCCESS.\n");
    return;
}

/************************************************************************/
/* Function           : TcpReleaseParams                                */
/* Input(s)           : u4Context - Context Id                          */
/*                      pu1Params - Params to be released               */
/*                      u1Cmd - Params command type                     */
/* Output(s)          : None                                            */
/* Returns            : None                                            */
/* Action :                                                             */
/* Releases the given params based on the command type to the HL Params */
/*  pool or to the ICMP params pool                                     */
/************************************************************************/
VOID
TcpReleaseParams (UINT4 u4Context, UINT1 *pu1Params, UINT1 u1Cmd)
{
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TcpReleaseParams\n");
    if (u1Cmd == IP_ICMP_DATA)
    {
        MemReleaseMemBlock (gTcpIcmpParamsPoolId, pu1Params);
    }
    else if (u1Cmd == IP_TCP_DATA)
    {
        MemReleaseMemBlock (gTcpHlParamsPoolId, pu1Params);
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exiting TcpReleaseParams\n");
    return;
}
