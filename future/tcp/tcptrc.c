/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tcptrc.c,v 1.4 2013/05/06 12:00:42 siva Exp $
 *
 * Description: This file contains debugging related functions
 *****************************************************************************/

#include "tcpinc.h"

/****************************************************************************
 * Function     : TcpLogMsg
 *
 * Description  : Converts variable argument in to Trace depending on flag
 *
 * Input        : u4ContextId - Context Identifier
 *                u4TrcFlag   - Trace flag
 *
 * Output       : None
 *
 * Returns      : VOID
 *****************************************************************************/
VOID
TcpLogMsg (UINT4 u4ContextId, UINT4 u4TrcFlag, const char *pu1ModName,
           const char *pu1Fmt, ...)
{
    va_list             vTrcArgs;
    UINT4               u4DbgMap = 0;
    UINT4               u4OffSet = 0;
    CHR1                ac1Buf[TCP_MAX_LOG_MSG_LEN];

    MEMSET (ac1Buf, 0, TCP_MAX_LOG_MSG_LEN);

    if (u4ContextId == TCP_INVALID_CONTEXT_ID)
    {
        u4DbgMap = gu4TcpDbgMap;
    }
    else
    {
        if ((u4ContextId <
             FsTCPSizingParams[MAX_TCP_NUM_CONTEXT_SIZING_ID].
             u4PreAllocatedUnits) && (gpTcpContext != NULL)
            && (gpTcpContext[u4ContextId] != NULL))
        {
            u4DbgMap = gpTcpContext[u4ContextId]->u4TcpDbgMap;
            /* Appending the Context Id in the log message */
            SPRINTF (ac1Buf, "Context Id : %d ", u4ContextId);
        }
        else
        {
            u4DbgMap = gu4TcpDbgMap;
        }
    }

    u4OffSet = STRLEN (ac1Buf);

    va_start (vTrcArgs, pu1Fmt);
    vsprintf (&ac1Buf[u4OffSet], pu1Fmt, vTrcArgs);
    va_end (vTrcArgs);

    UtlTrcLog (u4DbgMap, u4TrcFlag, pu1ModName, ac1Buf);
}
