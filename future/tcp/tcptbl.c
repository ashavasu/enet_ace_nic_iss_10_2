/* SOURCE FILE HEADER
 *
 * ----------------------------------------------------------------------------
 *  FILE NAME             : tcptbl.c
 *
 *  PRINCIPAL AUTHOR      : Ramesh Kumar
 *
 *  SUBSYSTEM NAME        : TCP
 *
 *  MODULE NAME           : Finite State Machine & Output State Machine
 *
 *  LANGUAGE              : C
 *
 *  TARGET ENVIRONMENT    : Any
 *
 *  DATE OF FIRST RELEASE :
 *
 *  DESCRIPTION           : This file contains the array declaration for the 
 *                          OSM and FSM modules.
 *
 * ---------------------------------------------------------------------------
 *
 *
 *  CHANGE RECORD :
 *
 * ---------------------------------------------------------------------------
 *   VERSION        AUTHOR/DATE           DESCRIPTION OF CHANGE
 * ---------------------------------------------------------------------------
 *
 *
 * ---------------------------------------------------------------------------
 */

#include "tcpinc.h"

      /*INPUT FINITE STATE MACHINE */
INT1                (*fsm_switch[MAX_TCP_STATES]) (tTcb *, tSegment *,
                                                   tIpTcpHdr *) =
{

    FsmFree,                    /*0 */
        FsmClosed,                /*1 */
        FsmListen,                /*2 */
        FsmSynsent,                /*3 */
        FsmSynrcvd,                /*4 */
        FsmEstablished,            /*5 */
        FsmFin1,                /*6 */
        FsmFin2,                /*7 */
        FsmClosewait,            /*8 */
        FsmLastack,                /*9 */
        FsmClosing,                /*10 */
        FsmTimewait,            /*11 */
        FsmAbort,                /*12 */
};

      /*OUTPUT STATE MACHINE */
INT1                (*osm_switch[MAX_TCP_OUT_STATES]) (UINT4, UINT1) =
{
    OsmIdle,                    /*0 */
        OsmPersist,                /*1 */
        OsmTransmit,            /*2 */
        OsmRetransmit,            /*3 */
};
