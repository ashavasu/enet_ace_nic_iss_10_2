/*##########################################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved                   #
# ----------------------------------------------------                   #
# $Id: tcptrace.h,v 1.4 2013/03/08 13:30:12 siva Exp $                    #
##########################################################################
*/
/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 * /
+--------------------------------------------------------------------------+
 * |   FILE  NAME             :  tcptrace.h                                   |
 * |                                                                          |
 * |   PRINCIPAL AUTHOR       :  Aricent Inc.                      |
 * |                                                                          |
 * |   SUBSYSTEM NAME         :  Linux Router                                 |
 * |                                                                          |
 * |   MODULE NAME            :  UtlTrcLog module                             |
 * |                                                                          |
 * |   LANGUAGE               :  C                                            |
 * |                                                                          |
 * |   TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                          |
 * |   DATE OF FIRST RELEASE  :                                               |
 * |                                                                          |
 * |   DESCRIPTION            :  Provides general purpose macros for tracing  |
 * |                                                                          |
 * |   Author                 :  Bhuvaneswari                                 |
 * |                                                                          |
 * +--------------------------------------------------------------------------+
 */

#ifndef __TCPTRACE_H__
#define __TCPTRACE_H__


 /* Common bitmasks for various events. */
#define  TCP_INIT_SHUT_TRC   INIT_SHUT_TRC     
#define  TCP_MGMT_TRC        MGMT_TRC          
#define  TCP_DATA_PATH_TRC   DATA_PATH_TRC     
#define  TCP_CTRL_PLANE_TRC  CONTROL_PLANE_TRC 
#define  TCP_DUMP_TRC        DUMP_TRC          
#define  TCP_OS_RES_TRC      OS_RESOURCE_TRC   
#define  TCP_ALL_FAIL_TRC    ALL_FAILURE_TRC   
#define  TCP_BUF_TRC         BUFFER_TRC        
#define  TCP_ENTRY_EXIT_TRC  0x00000100        
#define  TCP_FSM_TRC         0x00000200        
#define  TCP_OSM_TRC         0x00000400        

#define TCP_MAX_LOG_MSG_LEN  300
 /* General Macros */
#define  TCP_PKT_DUMP      MOD_PKT_DUMP 
#define  TCP_MOD_TRC       TcpLogMsg

#endif /* _TCPTRACE_H_ */

