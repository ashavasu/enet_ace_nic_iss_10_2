/* FILE HEADER
 *
 * ----------------------------------------------------------------------------
 *  FILE NAME             : tcposm.h
 * 
 *  PRINCIPAL AUTHOR      : Ramesh Kumar
 *
 *  SUBSYSTEM NAME        : TCP
 *
 *  MODULE NAME           : High Level Interface Module
 *
 *  LANGUAGE              : C
 *
 *  TARGET ENVIRONMENT    : Any
 *
 *  DATE OF FIRST RELEASE :
 * 
 *  DESCRIPTION           : This file contains values for the different 
 *                          states of the Output State Machine.
 *
 * ---------------------------------------------------------------------------
 *
 *
 *  CHANGE RECORD :
 *
 * ---------------------------------------------------------------------------
 *   VERSION        AUTHOR/DATE           DESCRIPTION OF CHANGE
 * ---------------------------------------------------------------------------
 *
 *
 * ---------------------------------------------------------------------------
 */
#ifndef  __TCP_OSM_H__
#define  __TCP_OSM_H__

/* tcposm.h */

/* output states */
#define  TCPO_IDLE        0    
#define  TCPO_PERSIST     1    
#define  TCPO_TRANSMIT    2    
#define  TCPO_RETRANSMIT  3    

/* event processing */
#define  TCPE_SEND             0x01 
#define  TCPE_PERSIST          0x02 
#define  TCPE_RETRANSMIT       0x03 
#define  TCPE_DELETE           0x04 

/* used in tcpsend */
#define  TSF_NEWDATA      0        /* Send new data                  */
#define  TSF_REXMT        1        /* Retransmit 1st pending segment */

#endif   /*TCP_OSM_H*/

