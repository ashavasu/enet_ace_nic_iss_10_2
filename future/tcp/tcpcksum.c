/*##########################################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved                   #
# ----------------------------------------------------                   #
# $Id: tcpcksum.c,v 1.7 2013/03/08 13:30:11 siva Exp $                    #
##########################################################################
*/
/* SOURCE FILE HEADER
 *
 * ----------------------------------------------------------------------------
 *  FILE NAME             : tcpcksum.c
 * 
 *  PRINCIPAL AUTHOR      : Ramesh Kumar
 *
 *  SUBSYSTEM NAME        : TCP
 *
 *  MODULE NAME           : High Level Interface Module
 *
 *  LANGUAGE              : C
 *
 *  TARGET ENVIRONMENT    : Any
 *
 *  DATE OF FIRST RELEASE :
 * 
 *  DESCRIPTION           : This file contain the function that  calculates
 *                          checksum.
 *
 * ---------------------------------------------------------------------------
 *
 *
 *  CHANGE RECORD :
 *
 * ---------------------------------------------------------------------------
 *   VERSION        AUTHOR/DATE           DESCRIPTION OF CHANGE
 * ---------------------------------------------------------------------------
 *    1.6           Rebecca Rufus         Host to Network Conversion of the 
 *                                        Checksum to be done.Refer Problem Id 2
 * ---------------------------------------------------------------------------
 */
#include "tcpinc.h"
/*************************************************************************/
/*  Function Name   : TcpPktCksum                                        */
/*  Input(s)        : u4Context - Context Id                             */
/*                    pBuf - The buffer holding to bytes to calc check   */
/*                    sum for                                            */
/*                    pHdr - Pointer to IpTcp structure                  */
/*  Output(s)       : None.                                              */
/*  Returns         : Checksum value                                     */
/*  Description     : This function calculates the checksum of the       */
/*                    received segment.                                  */
/*  CallingCondition: Whenever checksum is to calculated                 */
/*************************************************************************/
UINT2
TcpPktCksum (UINT4 u4Context, tSegment * pBuf, tIpTcpHdr * pHdr)
{
    UINT4               u4Sum;
    UINT1               u1AddrOff;
    UINT2               u2IpOptLen, u2Temp;
    tIpAddr             SrcAddr;
    tIpAddr             DestAddr;

    INT2                u2Len;
    UINT2               u2TCPHdrStart;
    UINT1               u1TcpHlen, u1Count, *pBuffer;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TcpPktCksum\n.");
    V6_NTOHL (&SrcAddr, &(pHdr->ip->SrcAddr));
    V6_NTOHL (&DestAddr, &(pHdr->ip->DestAddr));

    u2IpOptLen = pHdr->ip->u2IpOptLen;
    u2Len = OSIX_NTOHS (pHdr->ip->u2Totlen);

    /* u2TotLen  Is Total IpData Length i.e TcpHeader + Tcpdata ; */

    u4Sum = ZERO;
    if ((IsIP6Addr (SrcAddr)) || (IsIP6Addr (DestAddr)))
    {
        for (u1AddrOff = TCP_ZERO; u1AddrOff < IP6_ADDR_SIZE_IN_WORDS;
             u1AddrOff++)
        {
            u4Sum += (SrcAddr.u4_addr[u1AddrOff]) & TCP_MAX_UINT2;
            u4Sum +=
                ((SrcAddr.
                  u4_addr[u1AddrOff]) >> TCP_BIT_SHIFT_16) & TCP_MAX_UINT2;
            u4Sum += (DestAddr.u4_addr[u1AddrOff]) & TCP_MAX_UINT2;
            u4Sum +=
                ((DestAddr.
                  u4_addr[u1AddrOff]) >> TCP_BIT_SHIFT_16) & TCP_MAX_UINT2;
        }
        u2TCPHdrStart = pHdr->ip->u1VHlen;    /* No IP Header in the OutPut Send Segment */
    }
    else
    {
        u4Sum += (V4_FROM_V6_ADDR (SrcAddr)) & TCP_MAX_UINT2;
        u4Sum +=
            ((V4_FROM_V6_ADDR (SrcAddr)) >> TCP_BIT_SHIFT_16) & TCP_MAX_UINT2;
        u4Sum += (V4_FROM_V6_ADDR (DestAddr)) & TCP_MAX_UINT2;
        u4Sum +=
            ((V4_FROM_V6_ADDR (DestAddr)) >> TCP_BIT_SHIFT_16) & TCP_MAX_UINT2;
        u2TCPHdrStart = (UINT2) (IP_HEADER_LEN + u2IpOptLen);
        /* Fixed IPv4 Header + IP Options */
    }
    u4Sum += u2Len;
    u4Sum += (UINT2) (pHdr->ip->u1Proto);
    u1TcpHlen = (UINT1) (((pHdr->tcp->u1Hlen) >> TCP_VER_SHIFT)
                         << TCP_BYTES_FOR_SHORT);
    pBuffer = (UINT1 *) (pHdr->tcp);

    for (u1Count = INIT_COUNT; u1Count < u1TcpHlen;
         u1Count += TCP_BYTES_FOR_SHORT, pBuffer += TCP_BYTES_FOR_SHORT)
    {
        u2Temp = (*((UINT2 *) (VOID *) pBuffer)) & TCP_MAX_UINT2;
        u4Sum += OSIX_NTOHS (u2Temp);
    }
    u2Len -= u1TcpHlen;

    /* Data in the segment */
    if (u2Len > ZERO)
    {
        u2Len = (UINT2) (~(OSIX_HTONS (CalcDataCksum (u4Context, pBuf, u2Len,
                                                      u2TCPHdrStart +
                                                      u1TcpHlen))));
    }
    else
    {
        u2Len = ZERO;
    }
    u4Sum += (UINT2) (OSIX_NTOHS (u2Len));

    u4Sum = (u4Sum >> TCP_BIT_SHIFT_16) + (u4Sum & TCP_MAX_UINT2);
    u4Sum = (u4Sum >> TCP_BIT_SHIFT_16) + (u4Sum & TCP_MAX_UINT2);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from TcpPktCksum. Returned %d\n",
                 ((~((UINT2) u4Sum)) & TCP_MAX_UINT2));

    return ((UINT2) (~((UINT2) u4Sum)) & TCP_MAX_UINT2);
}

/*************************************************************************/
/*  Function Name   : CalcDataCksum                                      */
/*  Input(s)        : u4Context - Context Id                             */
/*                    pBuf - Pointer to the buffer holding the string of */
/*                    bytes.                                             */
/*                    u4Size - No of bytes                               */
/*                    u4ReadOffset - Where to start from in buffer       */
/*  Output(s)       : None.                                              */
/*  Returns         : Checksum value                                     */
/*  Description     : This function calculates the checksum of the       */
/*                    data portion                                       */
/*  CallingCondition: From TcpPktCksum                                   */
/*************************************************************************/
UINT2
CalcDataCksum (UINT4 u4Context, tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Size,
               UINT4 u4ReadOffset)
{
    tCRU_BUF_DATA_DESC *pDataDesc;
    UINT4               u4Sum = ZERO;
    UINT4               u4Len = ZERO;
    UINT2               u2Tmp = ZERO;
    UINT1              *pu1Buf;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering CalcDataCksum.\n");

    pDataDesc = CRU_BUF_GetFirstDataDesc (pBuf);

    while (pDataDesc &&
           (u4ReadOffset > CRU_BUF_GetBlockValidByteCount (pDataDesc)))
    {
        u4ReadOffset -= CRU_BUF_GetBlockValidByteCount (pDataDesc);
        pDataDesc = CRU_BUF_GetNextDataDesc (pDataDesc);
    }

    if (pDataDesc == NULL)
    {
        TCP_MOD_TRC (u4Context, TCP_ALL_FAIL_TRC, "TCP",
                     "CalcDataCksum: Read Offset is more than buffer size %d\n",
                     u4ReadOffset);
        return ((UINT2) u4Sum);
    }

    pu1Buf = CRU_BUF_GetDataPtr (pDataDesc) + u4ReadOffset;

    while (u4Size > ZERO)
    {
        u4Len = (CRU_BUF_GetBlockValidByteCount (pDataDesc) - u4ReadOffset);
        u4ReadOffset = ZERO;

        u4Len = (u4Len < u4Size) ? u4Len : u4Size;
        u4Size -= u4Len;

        for (; u4Len > TCP_ONE; pu1Buf += TCP_BYTES_FOR_SHORT,
             u4Len -= TCP_BYTES_FOR_SHORT)
        {
            u4Sum += (OSIX_NTOHS (*(UINT2 *) (VOID *) pu1Buf));
        }

/* Add the last byte if number of bytes is odd */
        if (u4Len == TCP_ONE)
        {
            u2Tmp = (UINT2) (*pu1Buf);
            u2Tmp = (UINT2) ((u2Tmp << TCP_BIT_SHIFT_8) &
                             TCP_MAX_UINT2_FOR_BYTE);
            u4Sum += u2Tmp;
        }

        pDataDesc = CRU_BUF_GetNextDataDesc (pDataDesc);
        if (!pDataDesc)
        {
            break;
        }
        pu1Buf = CRU_BUF_GetDataPtr (pDataDesc);
    }                            /* end of while (u4Size > ZERO) */

    u4Sum = (u4Sum >> TCP_BIT_SHIFT_16) + (u4Sum & TCP_MAX_UINT2);
    u4Sum = (u4Sum >> TCP_BIT_SHIFT_16) + (u4Sum & TCP_MAX_UINT2);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from CalcDataCksum. Returned %d\n", (~((UINT2) u4Sum)));

    return ((UINT2) (~((UINT2) u4Sum)));
}
