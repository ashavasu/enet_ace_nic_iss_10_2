/** $Id: tcptdfs.h,v 1.13 2013/06/07 13:32:13 siva Exp $ */


#ifndef __TCPTDFS_H__
#define __TCPTDFS_H__

/* IP header as defined by IP */
#ifndef PACK_REQUIRED
#pragma pack(1)
#endif
typedef struct {
    UINT1    u1VHlen;
    UINT1    u1TOS;
    UINT2    u2Totlen;                   /* Total length  IP header + DATA */
    UINT2    u2IpOptLen;
    UINT2    u2DataLen;
    UINT1    u1TTL;                      /* Time to live */
    UINT1    u1Proto;                    /* Protocol */
    UINT2    u2Cksum;                    /* Checksum value */
    tIpAddr  SrcAddr;
    tIpAddr  DestAddr;
    UINT1    au1IpOpts[MAX_IP_OPT_LEN];  /* IP Options */
} tIpHdr;

/* IPv6 headre for local usage */
typedef struct {
    UINT2    u2VerTClass;
    UINT2    u2FlowLabel;
    UINT2    u2PayLoadLen;
    UINT1    u1NextHdr;
    UINT1    u1HopLimit;
    tIpAddr  SrcAddr;
    tIpAddr  DestAddr;
}tTcpIp6Hdr;

/* IPv4 headre for local usage */
typedef struct {
    UINT1  u1VHlen;
    UINT1  u1TOS;
    UINT2  u2Totlen;  /* Total length  IP header + DATA */
    UINT2  u2Id;      /* Identification */
    UINT2  u2FlOffs;  /* Flags + fragment offset */
    UINT1  u1TTL;     /* Time to live */
    UINT1  u1Proto;   /* Protocol */
    UINT2  u2Cksum;   /* Checksum value */
    UINT4  u4Src;     /* Source address */
    UINT4  u4Dst;     /* Destination address */
}tIp4Hdr;

/* Tcp Header structure */
typedef struct {
    UINT2  u2Sport;                      /* source port */ 
    UINT2  u2Dport;                      /* destination port */
    INT4   i4Seq;                        /* sequence number */
    INT4   i4Ack;                        /* acknowledgement number */
    UINT1  u1Hlen;                       /* header length in word size including                                            options */
    UINT1  u1Code;                       /* control flags */
    UINT2  u2Window;                     /* window advertisement */
    UINT2  u2Cksum;                      /* checksum */
    UINT2  u2Urgptr;                     /* urgent pointer */
    UINT1  au1TcpOpts[MAX_TCP_OPT_LEN];  /* TCP Options */
} tTcpHdr;
#ifndef PACK_REQUIRED
#pragma pack()
#endif 

/* IP & TCP Header */
typedef struct {
    tIpHdr   *ip;
    tTcpHdr  *tcp;
} tIpTcpHdr;


/* TCP options configuration */
typedef struct CONFIGPARMS {
    INT4  i4TcpAckOpt;
    INT4  i4TcpTSOpt;
    INT4  i4TcpBWOpt;
    INT4  i4TcpWSOpt;
    INT4  i4TcpIncrIniWnd;
    UINT2 u2TcpMaxRetries;
    UINT1 au1Padding[2];
} tTcpConfigParms;


/* structure definiton to store statistics */
typedef struct tTcpStats {
    UINT4  u4RtoAlgorithm;
    UINT4  u4RtoMin;
    UINT4  u4RtoMax;
    UINT4  u4MaxConn;
    UINT4  u4ActiveOpens;
    UINT4  u4PassiveOpens;
    UINT4  u4AttemptFails;
    UINT4  u4EstabResets;
    UINT4  u4CurrEstab;
    UINT4  u4InSegs;
    UINT4  u4OutSegs;
    UINT4  u4RetransSegs;
    UINT4  u4InErrs;
    UINT4  u4OutRsts;
    FS_UINT8  u8HcInSegs;   
    FS_UINT8  u8HcOutSegs;    
} tTcpStats;

typedef struct 
{ 
    tCRU_BUF_CHAIN_HEADER * pBuf;
       UINT1    u1Cmd; 
       UINT1    u1Type; 
       UINT1    u1Code; 
       UINT1    u1Reserved; 
    UINT4    u4Reserved1;
       UINT4    u4ContextId;
       tIp6Addr IpSrc; 
} tTcpIcmpParams;

/* TCB Table Mem-Block */
typedef struct _tTcpTblBlock 
{
    tTcb       TcpTblBlock;
}tTcpTblBlock;

/* Send Buffer Mem-Block */
typedef struct _tTcpSendBuffBlock
{
    UINT1     au1SendBuffBlock [MAX_SEND_BUF_SIZE];   
}tTcpSendBuffBlock;

/* Receive Buffer Mem-Block */
typedef struct _tTcpRcvBuffBlock
{
    UINT1     au1RcvBuffBlock [TCP_MAX_RECV_BUF_SIZE];   
}tTcpRcvBuffBlock;

typedef struct
{
    UINT4   u4SrcAddr;
    UINT4   u4DestAddr;
    UINT1   u1Padding;
    UINT1   u1Proto;
    UINT2   u2SegLen;
}tTcp4PseudoHdr;

typedef struct
{
    tIpAddr SrcAddr;
    tIpAddr DestAddr;
    UINT4   u4SegLen;
    UINT1   u1Padding[3];
    UINT1   u1Proto;
}tTcp6PseudoHdr;

typedef struct
{
    UINT2  u2Sport;
    UINT2  u2Dport;
    INT4   i4Seq;
    INT4   i4Ack;
    UINT1  u1Hlen;
    UINT1  u1Code;
    UINT2  u2Window;
    UINT2  u2Cksum;
    UINT2  u2Urgptr;
}tTcpFixedHdr;

/* TCP Context information */
typedef struct TcpContext {
    tTcpConfigParms   TcpConfigParms;
    tTcpStats         TcpStatParms;
    UINT4             u4TcpDbgMap;    /* Tcp trace enabler */
    UINT4             u4TcpLstChaAckTimStmp;
    UINT4             u4TcpChaAckCounter;
    UINT4             u4TcpMinCwnd;    /* Minimum value for compression window for HsTCP */
    UINT4             u4TcpMaxCwnd;    /* Max value for compression windows for HsTCP */
    UINT4             u4ContextId;
    INT2              i2IcmpMessagesEnabled;    /* if ICMP error be given to appn  */
    UINT1             u1TcpTrapAdmnSts; /* Trap Admin Status */
    INT1              i1Padding;
}tTcpContext;

typedef struct TcpVcmParams {
    UINT4     u4ContextId;
    UINT1     u1Event;
    UINT1     au1Padding[3];
}tTcpVcmParams;

#endif
