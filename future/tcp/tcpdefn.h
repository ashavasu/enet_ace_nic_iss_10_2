/** $Id: tcpdefn.h,v 1.18 2016/03/04 11:02:13 siva Exp $ */

#ifndef __TCP_DEFN_H__
#define __TCP_DEFN_H__

#include "size.h"


/*As this macro is only used in Trace*/
#define TCP_INVALID_CONTEXT_ID              (SYS_DEF_MAX_NUM_CONTEXTS + 1) 

/* event received by TCP */
#define  TCP_IP_IF_EVENT                    0x00000001
#define  TCP_TIMER_EVENT                    0x00000002                      
#define  TCP_VCM_EVENT                      0x00000004                      

#define  TCP_PROCESS_Q_MSG_CURR_DEPTH       100

/* TCP task realted definitions */
#define  TCP_TASK_NAME                      (UINT1 *)"TCP"
#define  TCP_TASK_PRIORITY                  40                              
#define  TCP_TASK_STACK_SIZE                20000                           
#define  TCP_TASK_MAIN_FUNCTION             (TcpTaskMain())                 
#define  TCP_TASK_MODE                      OSIX_DEFAULT_TASK_MODE          

/* Q used by LL(IP/IPv6) to send packet to TCP */
#define  TCP_Q_NAME                         (UINT1 *)"ITCQ"
#define  TCP_Q_DEPTH                        50                              
#define  TCP_Q_MODE                         OSIX_GLOBAL                     
#define  TCP_Q_TIMEOUT                      0                               
#define  TCP_CTRL_Q_NAME                    (UINT1 *)"CTLQ"

/* IPv6 task related definitons */
#define  IP6_TASK_NODE_ID                   SELF
#define  IP6_TASK_NAME                      SELF

/* Q used by tcp to post packets to IPv6 */
#define  TCP_TO_IP6_Q_NODE_ID               SELF                            
#define  TCP_TO_IP6_Q_NAME                  SELF                            

/* indicate of packte is normal data or Icmp packet */
#define  IP_TCP_DATA                        1                               
#define  ICMP_TCP_DATA                      2                               

/* TCp task wait time */
#define  TCP_DEQUEUE_TIMEOUT                0                               

#define  TCP_GET_SYS_TIME                   OsixGetSysTime
#define  TCP_INVALID_IF_INDEX               0xffff

/* Ip & Tcp options length */
#define  MAX_TCP_OPT_LEN  64

#define TCP_DEF_MIN_CWND                    38     /* Min and Max cwnd as specified */
#define TCP_DEF_MAX_CWND                    83000  /* by RFC 3649*/
#define TCP_DEF_BW_AT_MAX_CWND              0.1    /* This is the value of bw when 
                                                    * Congestion Window = 38*MSS */
#define TCP_DEF_BW_AT_MIN_CWND              0.5    /* This is the value of bw when
                                                    * Congestion Window = 83000 * MSS */
#define HSTCP_CONST_1                       0.078  /* These 2 are constants specified */
#define HSTCP_CONST_2                       1.2    /* by RFC 3649 */ 


#define TCP_MAX_2BYTE_VAL                   65535

/* for specifying SACK flag in the rxmtQ */
#define  TCP_TRUE          1                                                 
#define  TCP_FALSE         0                                                 

/* return values of functions */
#define  TCP_OK           (0)                   
#define  TCP_NOT_OK       (-1)                  
#define  SYSERR           (-1)                 


#define  tSegment         tCRU_BUF_CHAIN_HEADER 
#define  INIT_COUNT        0                                                 
#define  TCP_PROTOCOL      6


#define  INIT_SSTHRESH     65535                                             

/* TCP HLI return error codes */
#define TCPE_INVALID_OPERATION      SLI_EPERM
#define TCPE_TOO_MANY_CONN          SLI_EMFILE
#define TCPE_ALREADY_CLOSED         SLI_ENOTCONN
#define TCPE_INSUF_RESOURCES        SLI_ENOBUFS
#define TCPE_CONN_CLOSING           SLI_ECLOSINGCONN
#define TCPE_AGAIN                  SLI_EAGAIN
#define TCPE_WOULDBLOCK             SLI_EWOULDBLOCK
#define TCPE_BUF_OVERFLOW           SLI_EMSGSIZE
#define TCPE_CONN_EXISTS            SLI_EADDRINUSE 
#define TCPE_CONN_NOT_EXIST         SLI_ENOTCONN 
#define TCPE_ILL_SRC_ADDRESS        SLI_EADDRNOTAVAIL
#define TCPE_ILL_DST_ADDRESS        SLI_EADDRNOTAVAIL
#define TCPE_KA_TIMEDOUT            SLI_EKATIMEDOUT
#define TCPE_CANNOT_REACH           SLI_ENETUNREACH

/* connection is already there or not */
#define  TCP_CONN_EXIST          (1)
#define  TCP_CONN_NOT_EXIST      (0)

#define  TCP_VALID_CKSUM  (0)                   

#define  MAXIMUM(a,b)     (a > b ? a : b)       
#define  MINIMUM(a,b)     (a < b ? a : b)       

#define  GREATEST_INTEGER(a,b,c,d) \
         if ( SEQCMP((a),(b)) > 0) { \
            if ( SEQCMP((a),(b)) % (c) ) \
               d = ( SEQCMP((a),(b)) / (c)) + 1; \
            else \
               d = ( SEQCMP((a),(b)) / (c)); \
         }

#define TCPIN6_IS_ADDR_LLOCAL(a)      \
   (((a).u4_addr[0] & OSIX_HTONL(0xFFC00000)) == OSIX_HTONL(0xFE800000))

#define ALLOCATE_TCP_TO_APP_ASYNC_PARMS() \
(MEM_MALLOC(sizeof(tTcpToAppAsyncParms), tTcpToAppAsyncParms))

#define FREE_TCP_TO_APP_ASYNC_PARMS(ptr) \
(MEM_FREE((tTcpToAppAsyncParms *) ptr))

#define TCP_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)
 
#define TCP_CHA_ACK_MAX_COUNT             3
#define TCP_CHA_ACK_MAX_TIME              (10 * SYS_NUM_OF_TIME_UNITS_IN_A_SEC)

#define ALPHA 0.125
#define BETA  0.125
#define k     4
    
/* TCP-AO MAC length for TCP-AO option */
#define TCPAO_MAC_LEN  (12)   
#define TCPAO_OPT_HDR_LEN (4)
#define BGP4_TCPAO_PWD_SIZE (80) 
/* TCP-AO Traffic Key Flags */
#define TCPAO_RCV_SYN_TRFKEY  (0x1)
#define TCPAO_SND_SYN_TRFKEY  (0x2)
#define TCPAO_RCV_TRFKEY      (0x4)
#define TCPAO_SND_TRFKEY      (0x8)
/* TCP TRAP Admin Status */
#define TCP_TRAP_ENABLE  (1)
#define TCP_TRAP_DISABLE (2)
/* MD5 Digest length for MD5 option */
#define MD5_DIGEST_LEN 16
#define    TCP_AF_IPV4_TYPE        TCP_ONE
#define    TCP_AF_IPV6_TYPE        TCP_TWO
#define    IPVX_MAX_INET_ADR_LEN   16
#define    TCP_MI                  1
#define    TCP_SI                  2

typedef struct TcpAoErrLogEntry
{ 
    INT1        i1AdType;
    UINT1        au1Reserved[3];
    tUtlIn6Addr *In6SAdr;
    tUtlIn6Addr *In6DAdr;
    tUtlInAddr  *InSAdr;
    tUtlInAddr  *InDAdr;
    INT4         i4Context;
    UINT4        u4TcpAoErCtr;
    UINT2        u2SPort;
    UINT2        u2DPort;
} tTcpAoErrLogEntry;

#ifdef _TCPMAIN_C
UINT4 gu4MaxTcbLimit = 0;
#else
extern UINT4 gu4MaxTcbLimit;
#endif
#define MAX_NUM_OF_TCB gu4MaxTcbLimit
#endif

