/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tcpipif.c,v 1.21 2013/09/13 14:26:59 siva Exp $
 ********************************************************************/

/* SOURCE FILE HEADER
 *
 * ----------------------------------------------------------------------------
 *  FILE NAME             : tcpipif.c
 * 
 *  PRINCIPAL AUTHOR      : Ramesh Kumar
 *
 *  SUBSYSTEM NAME        : TCP
 *
 *  MODULE NAME           : Low Level Interface Module
 *
 *  LANGUAGE              : C
 *
 *  TARGET ENVIRONMENT    : Any
 *
 *  DATE OF FIRST RELEASE :
 * 
 *  DESCRIPTION           : This file contains functions that are related to 
 *                          IP layer. It Enqueues the segment to IP and IP6.
 *
 * ---------------------------------------------------------------------------
 *
 *
 *  CHANGE RECORD :
 *
 * ---------------------------------------------------------------------------
 *   VERSION        AUTHOR/DATE           DESCRIPTION OF CHANGE
 * ---------------------------------------------------------------------------
 *             Sri. Chellapa        Added Function  lli_Is
 *                               Destination_On_Same_Network
 * ------------------------------------------------------------------------*
 *     1.2      Saravanan.M    Implementation of                           *
 *              Purush            - Non Blocking Connect                   * 
 *                                - Non Blocking Accept                    *
 *                                - IfMsg Interface between TCP and IP     *
 *                                - One Queue per Socket                   *
 * ------------------------------------------------------------------------*/

#include "tcpinc.h"
#include "vcm.h"

/*************************************************************************/
/*  Function Name   : LliGetIpSendParms                                  */
/*  Input(s)        : pBuf - Pointer to the buffer to send               */
/*                    u4Index - Index to Tcb table                       */
/*                    u2TotLen - Length of the buffer                    */
/*  Output(s)       : None.                                              */
/*  Returns         : Pointer to t_IP_SEND_PARMS                         */
/*  Description     : Make lli specific structure to be sent.            */
/*  CallingCondition:                                                    */
/*  GLOBAL VARIABLES AFFECTED : None                                     */
/*************************************************************************/
#ifdef IP_WANTED
t_IP_SEND_PARMS    *
LliGetIpSendParms (tSegment * pBuf, UINT4 u4Index, UINT2 u2TotLen)
{
    t_IP_SEND_PARMS    *IpSendParms = NULL;
    tIpAddr             SrcAddr, DestAddr;
    UINT1               u1Ttl = TCP_ZERO;

    TCP_MOD_TRC (GetTCBContext (u4Index), TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering LliGetIpSendParms. \n");

    IpSendParms = (t_IP_SEND_PARMS *) CRU_BUF_Get_ModuleData (pBuf);
    IpSendParms->u1Cmd = IP_LAYER4_DATA;
    SrcAddr = GetTCBlip (u4Index);
    DestAddr = GetTCBrip (u4Index);
    IpSendParms->u4Src = V4_FROM_V6_ADDR (SrcAddr);
    IpSendParms->u4Dest = V4_FROM_V6_ADDR (DestAddr);
    IpSendParms->u2Id = TCP_ZERO;    /* Optional ID field for IP  */
    IpSendParms->u1Proto = TCP_PROTOCOL;
    IpSendParms->u1Tos = GetTCBtos (u4Index);    /* Type of service requested */
    IpSendParms->u4ContextId = GetTCBContext (u4Index);
    u1Ttl = GetTCBTtl (u4Index);
    if (u1Ttl == TCP_ONE)
    {
        IpSendParms->u1Ttl = TCP_DEF_TTL;
    }
    else
    {
        IpSendParms->u1Ttl = u1Ttl;    /* Time to live = 1 min     */
    }
    IpSendParms->u1Df = FALSE;    /* Fragmentation Control    */
    IpSendParms->u2Port = (UINT2) GetTCBIfIndex (u4Index);
    IpSendParms->u1Olen = OsmGetIpOptlen (u4Index);

    IpSendParms->u2Len = u2TotLen;
    TCP_MOD_TRC (GetTCBContext (u4Index), TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from LliGetIpSendParms. Returned %x\n", IpSendParms);
    return IpSendParms;
}

/*************************************************************************/
/*  Function Name   : LliSendSegmentToIP                                 */
/*  Input(s)        : pSegmentToIP - Segment to be send to Ip            */
/*                    u4Length - Length of the segment                   */
/*                    IpSendParms - Ipsend params                        */
/*  Output(s)       : None                                               */
/*  Returns         : TCP_OK                                             */
/*  Description     : Enqueue a TCP pkt to IP                            */
/*  CallingCondition:                                                    */
/*  GLOBAL VARIABLES AFFECTED None                                       */
/*************************************************************************/
/* Got from udp.c
 * This procedure is called from an application whenever an TCP packet
 * is to be sent. Enough buffer should be allocated to accomodate
 * TCP header, IP header and data. u2Olen should contain 0 if there are no
 * options specified.
 *
 * Format of pBuf expected is 
 *                    <---- u2Olen ---->                      <-- i2Len --->
 * +------------------+----------------+----------------------+------------+
 * | Space for IP HDR | Options Filled | Space for TCP header | Appln Data |
 * +------------------+----------------+----------------------+------------+
 */
INT1
LliSendSegmentToIP (tSegment * pSegmentToIP, UINT4 u4Length,
                    t_IP_SEND_PARMS * IpSendParms)
{
    UINT4               u4Context = IpSendParms->u4ContextId;
    UNUSED_PARAM (u4Length);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering LliSendSegmentToIP. \n");

    /* Consider that pSegmentToIP is already in network byte order */
    CRU_BUF_Move_ValidOffset (pSegmentToIP, (IP_HEADER_LENGTH
                                             + IpSendParms->u1Olen));
    if (IpEnquePktToIpFromHLWithCxtId (pSegmentToIP) == IP_FAILURE)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC |
                     TCP_OS_RES_TRC | TCP_BUF_TRC | TCP_ALL_FAIL_TRC,
                     "TCP",
                     "Exit from LliSendSegmentToIP. Send to queue failed.  Released buffer. Returned SYSERR.\n");
        return SYSERR;
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from LliSendSegmentToIP. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : GetIpParmsForReset                                 */
/*  Input(s)        : pIpSendParms - pointer to IP send params           */
/*                    pIpHdr - Pointer to IPHdr                          */
/*                    pRcvIpHdr - Pointer to Ip Hdr                      */
/*  Output(s)       : None.                                              */
/*  Returns         : None                                               */
/*  Description     : Make lli specific structure to be sent.            */
/*  CallingCondition:                                                    */
/*  GLOBAL VARIABLES AFFECTED : None                                     */
/*************************************************************************/
VOID
GetIpParmsForReset (t_IP_SEND_PARMS * pIpSendParms, tIpHdr * pIpHdr,
                    tIpHdr * pRcvIpHdr)
{

    TCP_MOD_TRC (pIpSendParms->u4ContextId, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering GettIp6SendParmsForReset. \n");
    pIpSendParms->u1Cmd = IP_LAYER4_DATA;

    V6_ADDR_COPY (&(pIpHdr->SrcAddr), &(pRcvIpHdr->DestAddr));
    pIpSendParms->u4Src = OSIX_NTOHL (V4_FROM_V6_ADDR (pRcvIpHdr->DestAddr));
    V6_ADDR_COPY (&(pIpHdr->DestAddr), &(pRcvIpHdr->SrcAddr));
    pIpSendParms->u4Dest = OSIX_NTOHL (V4_FROM_V6_ADDR (pRcvIpHdr->SrcAddr));

    pIpSendParms->u2Id = ZERO;    /* Optional ID field for IP  */
    pIpSendParms->u1Proto = TCP_PROTOCOL;

    pIpSendParms->u1Tos = ZERO;    /* Type of service requested */
    pIpSendParms->u1Ttl = TCP_DEF_TTL;    /* Time to live = 1 min     */
    pIpSendParms->u1Df = FALSE;    /* Fragmentation Control    */
    pIpSendParms->u2Port = TCP_INVALID_IF_INDEX;

    pIpSendParms->u1Olen = ZERO;
    pIpSendParms->u2Len = (UINT2) TCP_HEADER_LENGTH;
    TCP_MOD_TRC (pIpSendParms->u4ContextId, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from GettIpSendParmsForReset. Returned %x\n",
                 pIpSendParms);
    return;
}

/*************************************************************************/
/*  Function Name   : LliUpdateIp4TcpHdr                                 */
/*  Input(s)        : u4Context - Context Id                             */
/*                    pSeg - Pointer to the Segment                      */
/*                    pRcvSeg - Pointer to the IpTcp header              */
/*  Output(s)       : None.                                              */
/*  Returns         : Pointer to tIp6SendParms                           */
/*  Description     : Make lli specific structure to be sent.            */
/*  CallingCondition:                                                    */
/*  GLOBAL VARIABLES AFFECTED : None                                     */
/*************************************************************************/
INT1
LliUpdateIp4TcpHdr (UINT4 u4Context, UINT1 *pu1Parms, tIpTcpHdr * pRcvSeg)
{
    UINT1               u1TCPHdrStart;
    tIp4Hdr             RcvIp4Hdr;
    tIpHdr             *pRcvIpHdr = NULL;
    tTcpHdr            *pRcvTcpHdr = NULL;
    UINT1               u1IpOptLen;
    INT4                i4TcpOptLen;
    UINT2               u2TcpLen;
    t_IP_TO_HLMS_PARMS *pParms = NULL;
    tCRU_BUF_CHAIN_HEADER *pSeg = NULL;

    pParms = (t_IP_TO_HLMS_PARMS *) (VOID *) pu1Parms;
    pSeg = pParms->pBuf;

    /* Copy Fixed Header IPv4 Header */
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering LliUpdateIp4TcpHdr. Returned %x\n");
    CRU_BUF_Copy_FromBufChain (pSeg, (UINT1 *) (&RcvIp4Hdr),
                               IP_HLEN_OFFSET, IP_HEADER_LENGTH);

    u1TCPHdrStart =
        (UINT1) (((RcvIp4Hdr.u1VHlen) & TCP_IPHDRLEN_MASK)
                 << TCP_BYTES_FOR_SHORT);
    u1IpOptLen = (UINT1) (u1TCPHdrStart - IP_HEADER_LENGTH);

    /* Convert the IP4 Header To a common IP Header */
    pRcvIpHdr = pRcvSeg->ip;

    pRcvIpHdr->u1VHlen = IP_HEADER_LENGTH;    /* IP HeaderLen 4 */
    pRcvIpHdr->u1TOS = RcvIp4Hdr.u1TOS;
    pRcvIpHdr->u1TTL = RcvIp4Hdr.u1TTL;
    pRcvIpHdr->u1Proto = RcvIp4Hdr.u1Proto;
    V4_MAPPED_ADDR_COPY (&(pRcvIpHdr->SrcAddr), RcvIp4Hdr.u4Src);
    V4_MAPPED_ADDR_COPY (&(pRcvIpHdr->DestAddr), RcvIp4Hdr.u4Dst);
    pRcvIpHdr->u2IpOptLen = u1IpOptLen;
    u2TcpLen = (UINT2) (OSIX_NTOHS (RcvIp4Hdr.u2Totlen) - u1TCPHdrStart);

    /* Copy IP Options */
    CRU_BUF_Copy_FromBufChain (pSeg, (((UINT1 *) pRcvIpHdr) + IP6_HEADER_LEN),
                               IP_HEADER_LENGTH, u1IpOptLen);

    u1TCPHdrStart = (UINT1) (IP_HEADER_LEN + u1IpOptLen);

    pRcvSeg->tcp = (tTcpHdr *) (VOID *)
        (((UINT1 *) (pRcvSeg->ip)) + IP6_HEADER_LEN);
    pRcvTcpHdr = pRcvSeg->tcp;
    /* Copy TCP Header */
    CRU_BUF_Copy_FromBufChain (pSeg, (UINT1 *) (pRcvTcpHdr),
                               u1TCPHdrStart, TCP_HEADER_LENGTH);

    i4TcpOptLen =
        ((((pRcvTcpHdr->
            u1Hlen) >> TCP_VER_SHIFT) << TCP_BYTES_FOR_SHORT) -
         TCP_HEADER_LENGTH);

    if (i4TcpOptLen < TCP_ZERO)
    {
        return SYSERR;
    }
    /* Copy TCP Options */
    CRU_BUF_Copy_FromBufChain (pSeg,
                               (((UINT1 *) pRcvTcpHdr) + TCP_HEADER_LENGTH),
                               u1TCPHdrStart + TCP_HEADER_LENGTH,
                               (UINT4) i4TcpOptLen);
    pRcvIpHdr->u2Totlen = OSIX_HTONS (u2TcpLen);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from LliUpdateIp4TcpHdr. Returned %x\n");
    return TCP_OK;
}
#endif

/*************************************************************************/
/*  Function Name   : LliGetIp6SendParms                                 */
/*  Input(s)        : pBuf - Pointer to the buffer to send               */
/*                    u4Index - Index to Tcb table                       */
/*                    u2TotLen - Length of the buffer                    */
/*  Output(s)       : Pointer to tHlToIp6Params                          */
/*  Returns         : TCP_OK on Success and ERR on failure               */
/*  Description     : Make lli specific structure to be sent.            */
/*  CallingCondition:                                                    */
/*  GLOBAL VARIABLES AFFECTED : None                                     */
/*************************************************************************/
#ifdef IP6_WANTED
INT1
LliGetIp6SendParms (tSegment * pBuf, UINT4 u4Index, UINT2 u2TotLen,
                    tHlToIp6Params * pIp6SendParms)
{

    UNUSED_PARAM (pBuf);
    TCP_MOD_TRC (GetTCBContext (u4Index), TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering LliGetIp6SendParms. \n");

    if (pIp6SendParms == NULL)
    {
        TCP_MOD_TRC (GetTCBContext (u4Index), TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit LliGetIp6SendParms. Allocation of Sendparms failed \n");
        return ERR;
    }

    pIp6SendParms->u1Cmd = IP6_LAYER4_DATA;
    pIp6SendParms->u1Proto = TCP_PROTOCOL;
    pIp6SendParms->u1Hlim = GetTCBTtl (u4Index);
    pIp6SendParms->u4Index = (UINT4) GetTCBIfIndex (u4Index);
    pIp6SendParms->u4Len = u2TotLen;
    pIp6SendParms->u4ContextId = GetTCBContext (u4Index);
    V6_HTONL ((tIpAddr *) & (pIp6SendParms->Ip6SrcAddr), &GetTCBlip (u4Index));
    V6_HTONL ((tIpAddr *) & (pIp6SendParms->Ip6DstAddr), &GetTCBrip (u4Index));

    TCP_MOD_TRC (GetTCBContext (u4Index), TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from LliGettIp6SendParms. Returned %x\n", pIp6SendParms);
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : LliSendSegmentToIP6                                */
/*  Input(s)        : pSegmentToIP6 - Segment to be send to Ip6          */
/*                    u4Length - Length of the segment                   */
/*                    Ip6SendParms - Ip6send params                      */
/*  Output(s)       : None                                               */
/*  Returns         : TCP_OK                                             */
/*  Description     : Enqueue a TCP pkt to IP6                           */
/*  CallingCondition:                                                    */
/*  GLOBAL VARIABLES AFFECTED None                                       */
/*************************************************************************/
/* Got from udp.c
 * This procedure is called from an application whenever an TCP packet
 * is to be sent. Enough buffer should be allocated to accomodate
 * TCP header, IP header and data. u2Olen should contain 0 if there are no
 * options specified.
 *
 * Format of pBuf expected is 
 *                    <---- u2Olen ---->                      <-- i2Len --->
 * +------------------+----------------+----------------------+------------+
 * |Space for IP6 HDR | Options Filled | Space for TCP header | Appln Data |
 * +------------------+----------------+----------------------+------------+
 */

INT1
LliSendSegmentToIP6 (tSegment * pSegmentToIP6, UINT4 u4Length,
                     tHlToIp6Params * pIp6SendParms)
{
    UINT4               u4Context = 0;
    UNUSED_PARAM (u4Length);

    if (pIp6SendParms == NULL)
    {
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from LliSendSegmentToIP6. Returned ERR.\n");
        CRU_BUF_Release_MsgBufChain (pSegmentToIP6, FALSE);
        return ERR;
    }
    u4Context = pIp6SendParms->u4ContextId;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering LliSendSegmentToIP6. \n");

    if (Ip6RcvFromHl (pSegmentToIP6, pIp6SendParms) == FAILURE)
    {
        return ERR;
    }

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from LliSendSegmentToIP6. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : GetIp6ParmsForReset                                */
/*  Input(s)        : pIp6SendParms - pointer to Ip6 send params         */
/*                    pIpHdr - Pointer to IPHdr                          */
/*                    pRcvIpHdr - pointer to Ip Hdr                      */
/*  Output(s)       : None.                                              */
/*  Returns         : None                                               */
/*  Description     : Make lli specific structure to be sent.            */
/*  CallingCondition:                                                    */
/*  GLOBAL VARIABLES AFFECTED : None                                     */
/*************************************************************************/
VOID
GetIp6ParmsForReset (tHlToIp6Params * pIp6SendParms, tIpHdr * pIpHdr,
                     tIpHdr * pRcvIpHdr)
{
    TCP_MOD_TRC (pIp6SendParms->u4ContextId, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering GettIp6SendParmsForReset. \n");
    pIp6SendParms->u1Cmd = IP6_LAYER4_DATA;
    pIp6SendParms->u1Proto = TCP_PROTOCOL;
    pIp6SendParms->u1Hlim = (UINT1) TCP_MINUS_ONE;    /* Time to live = 1 min     */
    pIp6SendParms->u4Index = TCP_INVALID_IF_INDEX;
    pIp6SendParms->u4Len = (UINT4) (TCP_HEADER_LENGTH);

    V6_ADDR_COPY (&(pIpHdr->SrcAddr), &(pRcvIpHdr->DestAddr));
    V6_ADDR_COPY (&(pIpHdr->DestAddr), &(pRcvIpHdr->SrcAddr));
    V6_ADDR_COPY ((tIpAddr *) & (pIp6SendParms->Ip6SrcAddr),
                  &(pRcvIpHdr->DestAddr));
    V6_ADDR_COPY ((tIpAddr *) & (pIp6SendParms->Ip6DstAddr),
                  &(pRcvIpHdr->SrcAddr));

    TCP_MOD_TRC (pIp6SendParms->u4ContextId, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from LliGettIp6SendParms. Returned %x\n", pIp6SendParms);
    return;
}

/*************************************************************************/
/*  Function Name   : LliUpdateIp6TcpHdr                                 */
/*  Input(s)        : u4Context - Context Id                             */
/*                    pSeg - Pointer to the Segment                      */
/*                    pRcvSeg - Pointer to the IpTcp header              */
/*  Output(s)       : None.                                              */
/*  Returns         : Pointer to tIp6SendParms                           */
/*  Description     : Make lli specific structure to be sent.            */
/*  CallingCondition:                                                    */
/*  GLOBAL VARIABLES AFFECTED : None                                     */
/*************************************************************************/
INT1
LliUpdateIp6TcpHdr (UINT4 u4Context, UINT1 *pSeg, tIpTcpHdr * pRcvSeg)
{
    INT4                i4TcpOptLen;
    tTcpIp6Hdr          RcvIp6Hdr;
    tTcpIp6Hdr         *pRcvIp6Hdr = NULL;
    tIpHdr             *pRcvIpHdr = NULL;
    tTcpHdr            *pRcvTcpHdr = NULL;
    t_IP_TO_HLMS_PARMS *pParms = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    pParms = (t_IP_TO_HLMS_PARMS *) (VOID *) pSeg;
    pBuf = pParms->pBuf;
    /* Copy IPv6 From the Buffer */
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering from LliUpdateIp6TcpHdr. Returned %x\n");
    pRcvIp6Hdr = &RcvIp6Hdr;
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pRcvIp6Hdr,
                               IPv6_VERSION_OFFSET, IPv6_HEADER_LEN);

    /* Convert the IPv6 Header To a common IP Header */
    pRcvIpHdr = pRcvSeg->ip;

    pRcvIpHdr->u1VHlen = IPv6_HEADER_LEN;    /* IP Header Len */
    pRcvIpHdr->u1TOS = (UINT1) (pRcvIp6Hdr->u2VerTClass);
    pRcvIpHdr->u1TTL = pRcvIp6Hdr->u1HopLimit;
    pRcvIpHdr->u1Proto = TCP_PROTOCOL;
    V6_ADDR_COPY (&(pRcvIpHdr->SrcAddr), &(pRcvIp6Hdr->SrcAddr));
    V6_ADDR_COPY (&(pRcvIpHdr->DestAddr), &(pRcvIp6Hdr->DestAddr));
    pRcvIpHdr->u2IpOptLen = TCP_ZERO;

    pRcvSeg->tcp = (tTcpHdr *) (VOID *)
        (((UINT1 *) (pRcvSeg->ip)) + IP6_HEADER_LEN);
    pRcvTcpHdr = pRcvSeg->tcp;

    /* Get the TCP Header length from the Buffer */
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pRcvTcpHdr,
                               CB_READ_OFFSET (pBuf), TCP_HEADER_LENGTH);
    i4TcpOptLen =
        (((pRcvTcpHdr->
           u1Hlen >> TCP_VER_SHIFT) << TCP_BYTES_FOR_SHORT) -
         TCP_HEADER_LENGTH);

    if (i4TcpOptLen < TCP_ZERO)
    {
        return SYSERR;
    }

    /* Copy TCP Options */
    CRU_BUF_Copy_FromBufChain (pBuf,
                               (((UINT1 *) pRcvTcpHdr) + TCP_HEADER_LENGTH),
                               CB_READ_OFFSET (pBuf) + TCP_HEADER_LENGTH,
                               (UINT4) i4TcpOptLen);

    pRcvIpHdr->u2Totlen = OSIX_HTONS (pParms->u2Len);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from LliUpdateIp6TcpHdr. Returned %x\n");
    return TCP_OK;
}
#endif
