/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: tcpdata.c,v 1.14 2016/07/05 08:22:23 siva Exp $
 *
 **********************************************************************/
/* SOURCE FILE HEADER
 *
 * ----------------------------------------------------------------------------
 *  FILE NAME             : tcpdata.c
 * 
 *  PRINCIPAL AUTHOR      : Ramesh Kumar
 *
 *  SUBSYSTEM NAME        : TCP
 *
 *  MODULE NAME           : Finite State Machine
 *
 *  LANGUAGE              : C
 *
 *  TARGET ENVIRONMENT    : Any
 *
 *  DATE OF FIRST RELEASE :
 * 
 *  DESCRIPTION           : This file contains routines corresponding to 
 *                          handle data portion of the incoming segment.
 *
 * ---------------------------------------------------------------------------
 *
 *
 *  CHANGE RECORD :
 *
 * ---------------------------------------------------------------------------
 *   VERSION        AUTHOR/DATE           DESCRIPTION OF CHANGE
 * ---------------------------------------------------------------------------
 *                   Sri. Chellapa        Changed all    
 *                   13/05/96             sequence no.Comparisons to use 
 *                                        SEQCMP macro.   
 *    1.6            Rebecca Rufus        HliRead() invoked when data arrives 
 *                     16-Sep-'98.        for a connection Refer Problem Id 5.  
 * ---------------------------------------------------------------------------
 *                   Vinay/28.11.99      +Modified LINFsmProcessData() by 
 *                                        calling fsmProcessOption() to process
 *                                        the options in the incoming segment.
 *                                        It is placed under the compilation 
 *                                        switch ST_ENH.
 *                                       +Modied LINFsmUpdateTcb() for SACK 
 *                                        processing under the compilation 
 *                                        switch ST_ENH.
 *                                       +Modied LINFsmUpdateTcb() for NAK 
 *                                        processing under the compilation 
 *                                        switch ST_ENH.
 *                 Ramakrishnan/1.3.2000 +Added code to LINFsmProcessData()
 *                                        for supporting select call under the
 *                                        ST_ENHLISTINTF switch
 *                 Ramakrishnan/5.6.2000 +Changed ST_ENHLISTINTF to ST_ENH
 * ---------------------------------------------------------------------------
 */

#include "tcpinc.h"
/*************************************************************************/
/*  Function Name   : LINFsmProcessData                                  */
/*  Input(s)        : ptcb - Pointer to the TCB                          */
/*                    pSeg - Segment to process                          */
/*                    pRcvSeg - IpTcp Header of the segment              */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK /TCP_NOT_OK                                 */
/*  Description     : A segment is judged to occupy a portion of valid   */
/*                    receive sequence space if                          */
/*                    RCV.NXT =< SEG.SEQ < RCV.NXT+RCV.WND      or       */
/*                    RCV.NXT =< SEG.SEQ+SEG.LEN-1 < RCV.NXT+RCV.WNDi    */
/*                    The first part of this test checks to see if the   */
/*                    beginning of the segment falls in the window, the  */
/*                    second part of the test checks to see if the end   */
/*                    of the segment falls in the window; if the segment */
/*                    passes either part of the test it contains data    */
/*                    in the window.                                     */
/* CALLING CONDITION: Called from FSM.                                   */
/* GLOBAL VARIABLES AFFECTED : TCB table.                                */
/*************************************************************************/

INT1
LINFsmProcessData (tTcb * ptcb, tSegment * pSeg, tIpTcpHdr * pRcvSeg)
{
    UINT4               u4Rup, u4ReadOffset, u4WriteOffset;
    UINT4               u4Datalen, u4Buflen;
    INT4                i4First, i4SeqNo;
    tIpHdr             *pIpHdr;
    tTcpHdr            *pTcpHdr;
    UINT1              *pBuf;
    UINT2               u2TotLen, u2UrgPtr;
    UINT1               u1TCPHdrStart, u1Code, u1TCPHdrlen;
    UINT4               u4TcbIndex;

    u4TcbIndex = GET_TCB_INDEX (ptcb);

    TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering LINFsmProcessData.\n");

    pIpHdr = (pRcvSeg->ip);
    u1TCPHdrStart = (UINT1) ((pIpHdr->u1VHlen) + (UINT1) pIpHdr->u2IpOptLen);
    pTcpHdr = pRcvSeg->tcp;
    u1TCPHdrlen = (UINT1) (((pTcpHdr->u1Hlen) >> TCP_VER_SHIFT)
                           << TCP_BYTES_FOR_SHORT);
    u1Code = pTcpHdr->u1Code;
    i4SeqNo = OSIX_NTOHL (pTcpHdr->i4Seq);
    u2UrgPtr = OSIX_NTOHS (pTcpHdr->u2Urgptr);

    if (u1Code & TCPF_URG)
    {

        TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_CTRL_PLANE_TRC, "TCP",
                     "Got packet with URG flag set.\n");

        TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_CTRL_PLANE_TRC, "TCP",
                     "CONN ID - %d \nSEQ - %d \n",
                     GET_TCB_INDEX (ptcb), i4SeqNo);

        TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_CTRL_PLANE_TRC, "TCP",
                     "ACK - %d \nURG PTR - %d \n ", pTcpHdr->i4Ack, u2UrgPtr);

        u4Rup = (UINT4) i4SeqNo + (UINT4) u2UrgPtr;
        /*
         * Although the standard says that the urgent pointer should
         * point to the end of urgent data, BSD implementations use
         * a value of one beyond that specified. Hence '--' rup.
         * Refer Comer Vol II page 230 for more details.
         */
        u4Rup--;

        if (!(GetTCBflags (u4TcbIndex) & TCBF_RUPOK) ||
            (((INT4) u4Rup - GetTCBrupseq (u4TcbIndex)) > ZERO))
        {
            SetTCBrupseq (u4TcbIndex, (INT4) u4Rup);
            SetTCBflags (u4TcbIndex, TCBF_RUPOK);
        }
    }

    if (u1Code & TCPF_SYN)
    {
        INT4                temp;
        TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_CTRL_PLANE_TRC, "TCP",
                     "Got packet with SYN flag set. \nCONN ID - %d \n",
                     GET_TCB_INDEX (ptcb));

        TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_CTRL_PLANE_TRC, "TCP",
                     "SEQ - %d \nACK - %d \n",
                     i4SeqNo, OSIX_NTOHL (pTcpHdr->i4Ack));

        temp = i4SeqNo + TCP_ONE;
        SetTCBrnext (u4TcbIndex, GetTCBrnext (u4TcbIndex) + TCP_ONE);
        SetTCBflags (u4TcbIndex, TCBF_NEEDOUT);
        pTcpHdr->i4Seq = OSIX_HTONL (temp);
        /* Increment seq as SYN occupies one position in seq space */
    }

    u2TotLen = OSIX_NTOHS (pRcvSeg->ip->u2Totlen);
    u4Datalen = (UINT4) u2TotLen - (UINT4) u1TCPHdrlen;
    i4SeqNo = OSIX_NTOHL (pTcpHdr->i4Seq);
    i4First = i4SeqNo;

    if (u1Code & TCPF_FIN)
    {
        SetTCBfinseq (u4TcbIndex, i4SeqNo + u4Datalen);
    }

    if (SEQCMP (GetTCBrnext (u4TcbIndex), i4First) > ZERO)
    {
        /* Sequence number corresponds to data that has
           been already received. May be some data is already
           there and some data is new                      */

        u4Datalen -= (UINT4) GetTCBrnext (u4TcbIndex) - (UINT4) i4First;
        i4First = GetTCBrnext (u4TcbIndex);
    }

    if (SEQCMP ((INT4) (i4First + u4Datalen),
                (INT4) ((UINT4) GetTCBrnext (u4TcbIndex) +
                        GetTCBrbsize (u4TcbIndex) -
                        GetTCBrbcount (u4TcbIndex))) > ZERO)
    {

        u4Datalen -= (i4First + u4Datalen)
            - ((UINT4) GetTCBrnext (u4TcbIndex)
               + GetTCBrbsize (u4TcbIndex) - GetTCBrbcount (u4TcbIndex));

        /* Cutting off the data length that goes beyond the valid range */

    }

    /* Copy to user buffer */
    u4ReadOffset = (UINT4) i4First - (UINT4) i4SeqNo + (UINT4) u1TCPHdrlen
        + (UINT4) u1TCPHdrStart;
    u4WriteOffset = GetTCBrbstart (u4TcbIndex) + GetTCBrbcount (u4TcbIndex);
    u4WriteOffset += (UINT4) i4First - (UINT4) GetTCBrnext (u4TcbIndex);

    if (GetTCBrbsize (u4TcbIndex) != ZERO)
    {
        u4WriteOffset %= GetTCBrbsize (u4TcbIndex);
    }

    pBuf = GetTCBrcvbuf (u4TcbIndex) + u4WriteOffset;

    /* Number of bytes that can be copied without wrapping around */
    u4Buflen = GetTCBrbsize (u4TcbIndex) - u4WriteOffset;
    u4Buflen = (u4Datalen < u4Buflen) ? u4Datalen : u4Buflen;

    if ((CRU_BUF_Copy_FromBufChain (pSeg, pBuf, u4ReadOffset, u4Buflen)) !=
        (INT4) u4Buflen)
    {
        return TCP_NOT_OK;
    }
    u4ReadOffset += u4Buflen;

    if (u4Datalen > u4Buflen)
    {
        pBuf = GetTCBrcvbuf (u4TcbIndex);
        u4Buflen = u4Datalen - u4Buflen;
        if ((CRU_BUF_Copy_FromBufChain (pSeg, pBuf, u4ReadOffset, u4Buflen)) !=
            (INT4) u4Buflen)
        {
            return TCP_NOT_OK;
        }
        u4ReadOffset += u4Buflen;
    }
    /*To update rnext, do a lot of processing */
    LINFsmUpdateTcb (ptcb, pRcvSeg, i4First, u4Datalen);

#ifdef SLI_WANTED
    if (GetTCBrbcount (u4TcbIndex) > ZERO)
    {
        if (GetTCBflags (u4TcbIndex) & TCBF_RUPOK)
        {
            SliSelectScanList (GetTCBSockDesc (u4TcbIndex),
                               SELECT_READ | SELECT_EXCEPT);
        }
        else
        {
            SliSelectScanList (GetTCBSockDesc (u4TcbIndex), SELECT_READ);
        }
    }
#endif

    if ((GetTCBReadWaiting (u4TcbIndex)) && (GetTCBrbcount (u4TcbIndex) > ZERO))
    {
        SetTCBhlicmd (u4TcbIndex, ZERO);
        SetTCBReadWaiting (u4TcbIndex, FALSE);
        GiveTCBBlockSem (u4TcbIndex);
    }

    FsmProcessOption (ptcb, pRcvSeg);

    if (GetTCBflags (u4TcbIndex) & TCBF_NEEDOUT)
    {
        osm_switch[GetTCBostate (u4TcbIndex)] (u4TcbIndex, TCPE_SEND);    /*It includes ack */
        TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from LINFsmProcessData. Returned OK.\n");
        return TCP_OK;
    }
    else
    {
        TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from LINFsmProcessData. Returned NOT_OK.\n");
        return TCP_NOT_OK;
    }
}

/*************************************************************************/
/*  Function Name   : LINFsmUpdateTcb                                    */
/*  Input(s)        : ptcb - Pointer to the TCB.                         */
/*                    pSeg - Segment Processed                           */
/*                    i4First - first valid Offset in the segment        */
/*                    u4Datalen - No of bytes                            */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK                                             */
/*  Description     : TCP must accomodate out of order delivery. Because */
/*                    the window advertisement limits incoming data to   */
/*                    the buffer that has been allocated, TCP can always */
/*                    copy the arriving data directly into the buffer.   */
/*                    TCP must keep record of which octets from the      */
/*                    sequence have been received. Whenever data arrives */
/*                    out of order it calls FsmFrInsert to keep record   */
/*                    of sequence number.                                */
/* CALLING CONDITION: From LINFsmProcessData                             */
/* GLOBAL VARIABLES AFFECTED : TCB table.                                */
/*************************************************************************/
INT1
LINFsmUpdateTcb (tTcb * ptcb, tIpTcpHdr * pSeg, INT4 i4First, UINT4 u4Datalen)
{
    UINT4               u4TcbIndex;
    UINT4               u4Context;
    UINT1               u1Code;
    INT4                i4SeqNo;
    UINT1               u1NeedToCoalesce;

    u4TcbIndex = GET_TCB_INDEX (ptcb);

    TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering LINFsmUpdateTcb.\n");

    u1Code = pSeg->tcp->u1Code;
    i4SeqNo = OSIX_NTOHL (pSeg->tcp->i4Seq);
    u4Context = GetTCBContext (u4TcbIndex);

    if (SEQCMP (GetTCBrnext (u4TcbIndex), i4First) == ZERO)
    {
        /*Perfect match, you can update the rnext */
        if (u4Datalen > ZERO)
        {
            FsmFrCoalesce (ptcb, u4Datalen);
            SetTCBflags (u4TcbIndex, TCBF_NEEDOUT);    /* Send ACK */
            if ((GetTCBOptFlag (u4TcbIndex) & TCBOPF_SACK) &&
                (SackNodeCnt (u4Context, GetTCBSackHdr (u4TcbIndex)) != ZERO))
            {
                u1NeedToCoalesce = TCP_TRUE;
                if (FsmSackInsert (ptcb, i4First, u4Datalen, u1NeedToCoalesce)
                    == ERR)
                {
                    TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ALL_FAIL_TRC,
                                 "TCP", "SACK Insert Failure\n");
                }
            }
        }

        if ((u1Code & TCPF_FIN) &&
            (GetTCBfinseq (u4TcbIndex) == (INT4) (i4First + u4Datalen)))
        {
            TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_CTRL_PLANE_TRC, "TCP",
                         "Got packet with FIN flag set. \nCONN ID - %d\n",
                         GET_TCB_INDEX (ptcb));

            TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_CTRL_PLANE_TRC, "TCP",
                         "SEQ - %d \nACK - %d \n",
                         i4SeqNo, OSIX_NTOHL (pSeg->tcp->i4Ack));

            SetTCBflags (u4TcbIndex, (TCBF_RDONE | TCBF_NEEDOUT));
            if (GetTCBstate (u4TcbIndex) == TCPS_ESTABLISHED)
            {
                SetTCBstate (u4TcbIndex, TCPS_CLOSEWAIT);
                GiveTCBBlockSem (u4TcbIndex);
            }
            SetTCBrnext (u4TcbIndex, GetTCBrnext (u4TcbIndex) + TCP_ONE);
        }

        if (u1Code & (TCPF_PSH | TCPF_URG))
        {
            TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_CTRL_PLANE_TRC, "TCP",
                         "Got packet with PUSH flag set. \nCONN ID - %d\n",
                         GET_TCB_INDEX (ptcb));

            TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_CTRL_PLANE_TRC, "TCP",
                         "SEQ - %d \nACK - %d \n",
                         i4SeqNo, OSIX_NTOHL (pSeg->tcp->i4Ack));

            SetTCBflags (u4TcbIndex, TCBF_PUSH);
        }
        /* processing in-order segment. Update i4TcbOutOrderSeq, if necessary */
        if (gpTcpContext[u4Context]->TcpConfigParms.i4TcpAckOpt == TCP_NAKCONF)
        {
            if (SEQCMP ((i4First + (INT4) u4Datalen),
                        GetTCBOutOrderSeq (u4TcbIndex)) >= ZERO)
            {
                SetTCBOutOrderSeq (u4TcbIndex, ZERO);
            }
        }
    }
    else
    {
        if (u1Code & (TCPF_PSH | TCPF_URG))
        {
            SetTCBpushseq (u4TcbIndex, (INT4) ((UINT4) i4SeqNo + u4Datalen));
        }
        FsmFrInsert (ptcb, i4First, u4Datalen);
        if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_SACK)
        {
            u1NeedToCoalesce = FALSE;
            if (FsmSackInsert (ptcb, i4First, u4Datalen, u1NeedToCoalesce) ==
                ERR)
            {
                TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ALL_FAIL_TRC,
                             "TCP", "SACK Insert Failure \n");
            }
        }

        if (gpTcpContext[u4Context]->TcpConfigParms.i4TcpAckOpt == TCP_NAKCONF)
        {
            OsmGenerateNak (ptcb, i4First);
        }
    }
    TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from LINFsmUpdateTcb. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : FsmFrInsert                                        */
/*  Input(s)        : ptcb - Pointer to the TCB.                         */
/*                    i4Seq - Sequence Number                            */
/*                    u4Datalen - Length of data in the fragment         */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK                                             */
/*  Description     : It enqueues the fragment into a queue that is      */
/*                    always in ascending orderof sequences. The         */
/*                    fragment contains the the sequence number and the  */
/*                    length of incoming data pkt.                       */
/* CALLING CONDITION : It is called whenever data arrives out of order.  */
/*************************************************************************/
INT1
FsmFrInsert (tTcb * ptcb, INT4 i4Seq, UINT4 u4Datalen)
{
    tFrag              *pFrag;
    UINT4               u4TcbIndex;

    u4TcbIndex = GET_TCB_INDEX (ptcb);
    TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering FsmFrInsert.\n");

    if (u4Datalen <= ZERO)
    {
        TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmFrInsert b'coz data length <= ZERO. ");
        TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ENTRY_EXIT_TRC, "TCP",
                     "Returned OK.\n");
        return TCP_OK;
    }
    if ((pFrag = (tFrag *) MemAllocMemBlk (gTcpFragPoolId)) == NULL)
    {
        TCP_MOD_TRC (GetTCBContext (u4TcbIndex),
                     TCP_BUF_TRC | TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from FsmFrInsert b'coz mem alloc of pFrag failed. ");
        TCP_MOD_TRC (GetTCBContext (u4TcbIndex),
                     TCP_BUF_TRC | TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Returned OK.\n");
        return TCP_OK;
    }
    pFrag->i4Seq = i4Seq;
    pFrag->u2Len = (UINT2) u4Datalen;
    pFrag->pNext = NULL;

    /* Enqueue the fragment in ascending order of the sequence number */
    SetTCBfraghdr (u4TcbIndex,
                   FsmFrEnqueue (GetTCBContext (u4TcbIndex), pFrag,
                                 GetTCBfraghdr (u4TcbIndex)));
    SetTCBflags (u4TcbIndex, TCBF_NEEDOUT);
    TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FsmFrInsert. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : FsmFrEnqueue                                       */
/*  Input(s)        : u4Context - Context Id                             */
/*                    pFrag - Pointer to the fragment to enqueue         */
/*                    pHead - Head the Fragments list                    */
/*  Output(s)       : None.                                              */
/*  Returns         : Pointer to the head of the list                    */
/*  Description     : Put the incoming fragment into list pointed to by  */
/*                    the pHead. The position of the fragment depends    */
/*                    on its sequence number. The list in ascending      */
/*                    order of sequences.                                */
/* CALLING CONDITION:                                                    */
/* GLOBAL VARIABLES AFFECTED:                                            */
/*************************************************************************/
tFrag              *
FsmFrEnqueue (UINT4 u4Context, tFrag * pFrag, tFrag * pHead)
{
    tFrag              *ptr, *ptmp;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering FsmFrEnqueue.\n");
    ptmp = NULL;
    if (pHead == NULL)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmFrEnqueue. Returned %x.\n", pFrag);
        return pFrag;
    }
    for (ptr = pHead; ptr != NULL;)
    {
        if (pFrag->i4Seq > ptr->i4Seq)
        {
            ptmp = ptr;
            ptr = ptr->pNext;
        }
        else
        {
            if (ptr == pHead)
            {
                pFrag->pNext = pHead;
                TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                             "Exit from FsmFrEnqueue. Returned %x.\n", pFrag);
                return pFrag;
            }
            pFrag->pNext = ptr;
            ptmp->pNext = pFrag;
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from FsmFrEnqueue. Returned %x.\n", pHead);
            return pHead;
        }
    }
    if (ptr == NULL)
    {
        ptmp->pNext = pFrag;
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FsmFrEnqueue. Returned %x.\n", pHead);
    return pHead;
}

/*************************************************************************/
/*  Function Name   : FsmFrCoalesce                                      */
/*  Input(s)        : ptcb - Pointer to the TCB                          */
/*                    u4Datalen - Length of Data                         */
/*  Output(s)       : None                                               */
/*  Returns         : TCP_OK                                             */
/*  Description     : It advances the counter that tells how many        */
/*                    contiguous octets of sequence space have been      */
/*                    received successfully. In essence, it moves a      */
/*                    pointer along the sequence space until it finds    */
/*                    the next hole.                                     */
/* CALLING CONDITION : It is called whenever an fragment of expected     */
/*                     sequence number arrives.                          */
/* GLOBAL VARIABLES AFFECTED :                                           */
/*************************************************************************/
INT1
FsmFrCoalesce (tTcb * ptcb, UINT4 u4Datalen)
{
    UINT4               u4TcbIndex;
    UINT4               u4Context;
    UINT4               u4New;
    tFrag              *pf;

    u4TcbIndex = GET_TCB_INDEX (ptcb);
    u4Context = GetTCBContext (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering from FsmFrCoalesce.\n");
    SetTCBrnext (u4TcbIndex,
                 (INT4) ((UINT4) GetTCBrnext (u4TcbIndex) + u4Datalen));
    SetTCBrbcount (u4TcbIndex, GetTCBrbcount (u4TcbIndex) + u4Datalen);

    if (SEQCMP (GetTCBrnext (u4TcbIndex), GetTCBfinseq (u4TcbIndex)) == ZERO)
    {
        /*Release the fragments from the queue */
        while ((pf = FsmFrDequeue (u4Context, &(ptcb->pTcbFraghdr))))
        {
            MemReleaseMemBlock (gTcpFragPoolId, (UINT1 *) pf);
        }
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmFrCoalesce after releasing fragments. ");
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP", "Returned OK.\n");
        return TCP_OK;
    }
    if (SEQCMP (GetTCBrnext (u4TcbIndex), GetTCBpushseq (u4TcbIndex)) >= ZERO)
    {
        SetTCBpushseq (u4TcbIndex, ZERO);
    }

    pf = FsmFrDequeue (u4Context, &(ptcb->pTcbFraghdr));
    if (pf == NULL)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmFrCoalesce because dequeue returned NULL. \n");
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP", "Returned OK.\n");
        return TCP_OK;
    }

    while (SEQCMP ((INT4) pf->i4Seq, GetTCBrnext (u4TcbIndex)) <= ZERO)
    {
        u4New =
            (UINT4) pf->u2Len - ((UINT4) GetTCBrnext (u4TcbIndex) - pf->i4Seq);
        if (u4New > ZERO)
        {
            SetTCBrnext (u4TcbIndex,
                         (INT4) ((UINT4) GetTCBrnext (u4TcbIndex) + u4New));
            SetTCBrbcount (u4TcbIndex, GetTCBrbcount (u4TcbIndex) + u4New);
        }
        MemReleaseMemBlock (gTcpFragPoolId, (UINT1 *) pf);
        if (SEQCMP (GetTCBrnext (u4TcbIndex), GetTCBfinseq (u4TcbIndex)) ==
            ZERO)
        {
            /*Release the fragments from the queue */
            while ((pf = FsmFrDequeue (u4Context, &(ptcb->pTcbFraghdr))))
            {
                MemReleaseMemBlock (gTcpFragPoolId, (UINT1 *) pf);
            }
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from FsmFrCoalesce after releasing fragments.\n");
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Returned OK.\n");
            return TCP_OK;
        }

        if (SEQCMP (GetTCBrnext (u4TcbIndex), GetTCBpushseq (u4TcbIndex)) >=
            ZERO)
        {
            SetTCBpushseq (u4TcbIndex, ZERO);
        }

        pf = FsmFrDequeue (u4Context, &(ptcb->pTcbFraghdr));
        if (pf == NULL)
        {
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from FsmFrCoalesce because dequeue returned \n");
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "NULL. Returned OK.\n");
            return TCP_OK;
        }
    }
    SetTCBfraghdr (u4TcbIndex,
                   FsmFrEnqueue (u4Context, pf, GetTCBfraghdr (u4TcbIndex)));
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FsmFrCoalesce. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : FsmFrDequeue                                       */
/*  Input(s)        : uContext - Context Id                              */
/*                    pHdr - Pointer to the Fragment                     */
/*  Output(s)       : None.                                              */
/*  Returns         : Pointer to fragment                                */
/*  Description     : Take out the fragment from the queue head.         */
/*  CALLING CONDITION :                                                  */
/*  GLOBAL VARIABLES AFFECTED : None.                                    */
/*************************************************************************/
tFrag              *
FsmFrDequeue (UINT4 u4Context, tFrag ** pHdr)
{
    tFrag              *ptmp;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering from FsmFrDequeue.\n");
    if (*pHdr == NULL)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmFrDequeue. Returned NULL.");
        return NULL;
    }
    ptmp = *pHdr;
    (*pHdr) = (*pHdr)->pNext;
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FsmFrDequeue. Returned %x.", ptmp);
    return ptmp;
}
