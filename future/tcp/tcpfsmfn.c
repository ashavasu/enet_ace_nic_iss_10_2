/*##########################################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved                   #
# ----------------------------------------------------                   #
# $Id: tcpfsmfn.c,v 1.8 2016/07/05 08:22:23 siva Exp $                    #
##########################################################################
*/
/* SOURCE FILE HEADER
 *
 * ----------------------------------------------------------------------------
 *  FILE NAME             : tcpfsmfn.c
 * 
 *  PRINCIPAL AUTHOR      : Ramesh Kumar
 *
 *  SUBSYSTEM NAME        : TCP
 *
 *  MODULE NAME           : Finite State Machine
 *
 *  LANGUAGE              : C
 *
 *  TARGET ENVIRONMENT    : Any
 *
 *  DATE OF FIRST RELEASE :
 * 
 *  DESCRIPTION           : This file contains routines used in the fsm.
 *
 *
 * ---------------------------------------------------------------------------
 *
 *
 *  CHANGE RECORD :
 *
 * ---------------------------------------------------------------------------
 *   VERSION        AUTHOR/DATE           DESCRIPTION OF CHANGE
 * ---------------------------------------------------------------------------
 *
 *
 * ---------------------------------------------------------------------------
 */

#include "tcpinc.h"

/*************************************************************************/
/*  Function Name   : FsmStartWaitTmr                                    */
/*  Input(s)        : ptcb - Pointer to the TCB                          */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK                                             */
/*  Description     : To prevent duplicated segments from interfering    */
/*                    with later connections, TCP does not delete a TCB  */
/*                    immediately after a connection closes. Instead it  */
/*                    leaves TCB in place for short time. TCP should     */
/*                    wait for twice the maximum segment lifetime        */
/*                    before deleting the connection.                    */
/* CALLING CONDITION:Called after TIMEWAIT and LASTACK states in the FSM */
/* GLOBAL VARIABLES AFFECTED : None                                      */
/*************************************************************************/
INT1
FsmStartWaitTmr (tTcb * ptcb)
{
    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering FsmStartWaitTmr for CONN ID - %d\n",
                 GET_TCB_INDEX (ptcb));
    TmrKillTimers (ptcb);
    TCP_MOD_TRC (ptcb->u4Context, TCP_OS_RES_TRC, "TCP",
                 "All timers killed using TmrKillTimers.\n");
    TmrInitTimer (&(ptcb->Tcb2mslTmr), TCP_2MSL_TIMER, GET_TCB_INDEX (ptcb));
    TcpTmrSetTimer (&(ptcb->Tcb2mslTmr), TCP_TWOMSL);
    TCP_MOD_TRC (ptcb->u4Context, TCP_OS_RES_TRC, "TCP",
                 "The 2 MSL timer initiated and started.\n");
    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FsmStartWaitTmr. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : FsmAbortConn                                       */
/*  Input(s)        : u1Error - Type of ERROR                            */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS                                            */
/*  Description     : It immediately breaks the connection without       */
/*                    waiting for the buffer data to be delivered.       */
/*                    It places the connection in ABORT state and starts */
/*                    the user timeout. It also sends the user a message */
/*                    indicating that the conneciton has been aborted.   */
/*                    If the user is waiting for reply from some command */
/*                    he has issued, he is replied with appropriate      */
/*                    error message.                                     */
/* CALLING CONDITION :                                                   */
/* GLOBAL VARIABLES AFFECTED:                                            */
/*************************************************************************/
INT1
FsmAbortConn (tTcb * ptcb, UINT1 u1Error)
{
    UINT4               u4Index;
    UINT4               u4Temp;
    UINT4               u4Context;

    u4Index = GET_TCB_INDEX (ptcb);
    u4Context = GetTCBContext (u4Index);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering FsmAbortConn for CONN ID - %d\n",
                 GET_TCB_INDEX (ptcb));
    SetTCBflags (u4Index, TCBF_RDONE | TCBF_SDONE);
    SetTCBerror (u4Index, u1Error);
    if (GetTCBstate (u4Index) != TCPS_SYNRCVD)
    {
        SetTCBstate (u4Index, TCPS_ABORT);
        TCP_MOD_TRC (u4Context, TCP_FSM_TRC, "TCP",
                     "FSM state changed to ABORT for CONN ID - %d\n", u4Index);
    }
    u4Temp = GetTCBvalopts (u4Index);
    SetTCBvalopts (u4Index, TCPO_ASY_REPORT);    /* Force sending report */
    /* TODO : Since SOCK_DESC table is not semaphore proected we are
     *        going to comment this to avoid crash during connection close.
     *        This has to be revert back once semaphore protection is added
     *        SOCK_DESC table     
     * HliSendAsyMsg (u4Index, u1Error, (UINT1) NULL, (UINT1) NULL); */
    SetTCBvalopts (u4Index, u4Temp);

    /* Start Timer for aborting the connection if the user 
       doesn't respond with a explicit CLOSE operation within 
       the timeout.                                        */

    TmrStartUsertimeout (u4Index);

    if (GetTCBhlicmd (u4Index))
    {
        /* The user is waiting on some event to happen
           Hence return him error.                  */
        HliSendError (u4Index);
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FsmAbortConn. Returned SUCCESS.\n");
    return SUCCESS;
}

/*************************************************************************/
/*  Function Name   : LINFsmInitPassiveConn                              */
/*  Input(s)        : pParentTcb - Pointer to Parent TCB                 */
/*                    pNewtcb - Pointer to the TCB                       */
/*                    pTcpHdr - Pointer TCP header                       */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK                                             */
/*  Description     : It initialises window variables used to control    */
/*                    window and segment sizes.                          */
/* CALLING CONDITION :                                                   */
/* GLOBAL VARIABLES AFFECTED : Window variables of TCB                   */
/*************************************************************************/
INT1
LINFsmInitPassiveConn (tTcb * pParentTcb, tTcb * pNewtcb, tTcpHdr * pTcpHdr)
{
    UINT4               u4NewTcb, u4ParentTcb;
    UINT2               u2SWin;
    UINT4               i4SeqNo;

    TCP_MOD_TRC (pParentTcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering LINFsmInitPassiveConn for parent\n");
    TCP_MOD_TRC (pParentTcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "CONN ID - %d and child CONN ID - %d\n",
                 GET_TCB_INDEX (pParentTcb), GET_TCB_INDEX (pNewtcb));
    u4NewTcb = GET_TCB_INDEX (pNewtcb);
    u4ParentTcb = GET_TCB_INDEX (pParentTcb);

    u2SWin = OSIX_NTOHS (pTcpHdr->u2Window);
    i4SeqNo = OSIX_NTOHL (pTcpHdr->i4Seq);

    SetTCBswindow (u4NewTcb, (UINT4) u2SWin);
    if (GetTCBMaxswnd (u4NewTcb) < ((UINT4) u2SWin))
    {
        SetTCBMaxswnd (u4NewTcb, (UINT4) u2SWin);
    }
    SetTCBlwseq (u4NewTcb, i4SeqNo);

    SetTCBlwack (u4NewTcb, GetTCBiss (u4NewTcb));
    if (GetTCBsmss (u4ParentTcb))
    {
        SetTCBsmss (u4NewTcb,
                    MINIMUM (TCP_MAX_SEGMENT_SIZE, GetTCBsmss (u4ParentTcb)));
        SetTCBsmss (u4ParentTcb, ZERO);
    }
    else
    {
        SetTCBsmss (u4NewTcb, TCP_MAX_SEGMENT_SIZE);
    }

    if (GetTCBrmss (u4ParentTcb) > TCP_DEFAULT_RMSS)
    {
        SetTCBrmss (u4NewTcb, GetTCBrmss (u4ParentTcb));
        SetTCBrmss (u4ParentTcb, ZERO);
    }
    else
    {
        SetTCBrmss (u4NewTcb, TCP_DEFAULT_RMSS);
        SetTCBrmss (u4ParentTcb, ZERO);
    }

    SetTCBcwnd (u4NewTcb, GetTCBsmss (u4NewTcb));
    SetTCBssthresh (u4NewTcb, INIT_SSTHRESH);

    i4SeqNo = OSIX_NTOHL (pTcpHdr->i4Seq);
    SetTCBrnext (u4NewTcb, i4SeqNo);

    SetTCBcwin (u4NewTcb,
                (INT4) ((UINT4) GetTCBrnext (u4NewTcb) +
                        GetTCBrbsize (u4NewTcb)));
    TCP_MOD_TRC (pParentTcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from LINFsmInitPassiveConn. Returned OK.\n");
    return TCP_OK;
}
