/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstcpcli.c,v 1.2 2013/01/23 11:46:02 siva Exp $
*
* Description: This file contains the TCP CLI related routines.
*****************************************************************************/
#ifndef _FSTCPCLI_C_
#define _FSTCPCLI_C_

#include "tcpinc.h"
#include "tcputils.h"
#include "fstcpcli.h"

/***************************************************************************
 * FUNCTION NAME    : cli_process_fstcp_cmd 
 *
 * DESCRIPTION      : This function is exported to CLI module to handle the 
 *                    TCP cli commands to take the corresponding action. 
 *                    Only SNMP Low level routines are called from CLI.
 *
 * INPUT            : Variable arguments
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT1
cli_process_fstcp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[FS_TCP_MAX_ARGS];
    INT1                argno = 0;
    INT4                i4RetStatus = 0;
    UINT4               u4MaxRetries = 0;
    UINT4               u4ContextID = 0;
    UINT1              *pu1CxtName = NULL;

    va_start (ap, u4Command);

    /* Walk through the rest of the arguements and store in args array. 
     * Store 5 arguements at the max. This is because igmp commands do not
     * take more than 5 inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */

    MEMSET (args, 0, sizeof (args));

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == FS_TCP_MAX_ARGS)
            break;
    }

    va_end (ap);
    CLI_SET_ERR (0);

    CliRegisterLock (CliHandle, TcpLock, TcpUnLock);
    if (SNMP_FAILURE == TcpLock ())
    {
        return CLI_FAILURE;
    }

    switch (u4Command)
    {
        case TCP_CLI_MAX_RETRIES:
            /* args[0] - NULL
             * args[1] - Retries 
             * args[2] - VR name */
            pu1CxtName = (UINT1 *) args[2];
            if (NULL == pu1CxtName)
            {
                u4ContextID = VCM_INVALID_VC;
            }
            else
            {
                i4RetStatus = VcmIsVrfExist (pu1CxtName, &u4ContextID);
                if (VCM_FALSE == i4RetStatus)
                {
                    CliPrintf (CliHandle, "\r%% Invalid VRF name\r\n");
                    break;
                }

            }
            if (VCM_INVALID_VC == u4ContextID)    /* If no VR name input by user */
            {
                u4ContextID = VCM_DEFAULT_CONTEXT;
            }

            MEMCPY (&u4MaxRetries, args[1], sizeof (UINT4));
            i4RetStatus =
                TcpCliConfigureMaxRetries (CliHandle, u4MaxRetries,
                                           u4ContextID);
            break;

        default:
            CliPrintf (CliHandle, "%% Unknown command \r\n");
            return CLI_ERROR;
    }
    CLI_SET_CMD_STATUS (i4RetStatus);
    TcpUnLock ();
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : TcpCliConfigureMaxRetries 
 *
 * DESCRIPTION      : This function sets the maximum retries for TCP module.
 *
 * INPUT            : u4MaxRetries - Maximum number of retries 
 *               u4ContextID  - Context ID
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
TcpCliConfigureMaxRetries (tCliHandle CliHandle, UINT4 u4MaxRetries,
                           UINT4 u4ContextID)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    if (TcpSetContext (u4ContextID) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsTcpMaxReTries (&u4ErrorCode, u4MaxRetries) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsTcpMaxReTries (u4MaxRetries) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    TcpReleaseContext ();

    return CLI_SUCCESS;
}

#endif /* _FSTCPCLI_C_ */
