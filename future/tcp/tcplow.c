/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: tcplow.c,v 1.17 2013/06/07 13:32:13 siva Exp $
 * 
 * Description: Macro definitions for ANSI-C functions.
 * 
 **/

# include  "tcpinc.h"
#include "include.h"
extern INT1
    nmhValidateIndexInstanceTcpConnTable ARG_LIST ((UINT4, INT4, UINT4, INT4));

extern INT1
    nmhGetFirstIndexTcpConnTable ARG_LIST ((UINT4 *, INT4 *, UINT4 *, INT4 *));

extern INT1
    nmhGetNextIndexTcpConnTable ARG_LIST ((UINT4, UINT4 *,
                                           INT4, INT4 *, UINT4,
                                           UINT4 *, INT4, INT4 *));

extern INT1
    nmhValidateIndexInstanceTcpConnectionTable ARG_LIST ((INT4,
                                                          tSNMP_OCTET_STRING_TYPE
                                                          *, UINT4, INT4,
                                                          tSNMP_OCTET_STRING_TYPE
                                                          *, UINT4));

extern INT1
     
     
     
     
    nmhGetFirstIndexTcpConnectionTable
ARG_LIST ((INT4 *, tSNMP_OCTET_STRING_TYPE *, UINT4 *, INT4 *,
           tSNMP_OCTET_STRING_TYPE *, UINT4 *));

extern INT1
    nmhGetNextIndexTcpConnectionTable ARG_LIST ((INT4, INT4 *,
                                                 tSNMP_OCTET_STRING_TYPE *,
                                                 tSNMP_OCTET_STRING_TYPE *,
                                                 UINT4, UINT4 *, INT4, INT4 *,
                                                 tSNMP_OCTET_STRING_TYPE *,
                                                 tSNMP_OCTET_STRING_TYPE *,
                                                 UINT4, UINT4 *));

/* LOW LEVEL Routines for Table : FsTcpConnTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsTcpConnTable
 Input       :  The EIndices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsTcpConnTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceFsTcpConnTable (UINT4 u4FsTcpConnLocalAddress,
                                        INT4 i4FsTcpConnLocalPort,
                                        UINT4 u4FsTcpConnRemAddress,
                                        INT4 i4FsTcpConnRemPort)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/

    UINT4               u4TcbIndex;

    for (u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
    {
        if ((GetTCBstate (u4TcbIndex) == TCPS_FREE) ||
            (i4FsTcpConnLocalPort != GetTCBlport (u4TcbIndex)) ||
            (i4FsTcpConnRemPort != GetTCBrport (u4TcbIndex)) ||
            (u4FsTcpConnRemAddress != GetTCBv4rip (u4TcbIndex)) ||
            (u4FsTcpConnLocalAddress != GetTCBv4lip (u4TcbIndex)) ||
            (GetTCBstate (u4TcbIndex) == TCPS_ABORT))
        {
            continue;
        }
        TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                     "Validate index called with \n");

        TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                     "local port = %d \nremote port = %d \n",
                     i4FsTcpConnLocalPort, i4FsTcpConnRemPort);

        TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                     "remote ip addr = %x \nlocal ip addr = %x\n",
                     u4FsTcpConnLocalAddress, u4FsTcpConnRemAddress);

        TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                     "returned success. \n ");

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
        return SNMP_SUCCESS;
    }
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Validate index called with \n");
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "local port = %d \nremote port = %d \n",
                 i4FsTcpConnLocalPort, i4FsTcpConnRemPort);

    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "remote ip addr = %x \nlocal ip addr = %x\n",
                 u4FsTcpConnLocalAddress, u4FsTcpConnRemAddress);
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "returned failure. \n ");

   /*** $$TRACE_LOG (EXIT," SNMP Failure\n"); ***/
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsTcpConnTable
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstFsTcpConnTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexFsTcpConnTable (UINT4 *pu4FsTcpConnLocalAddress,
                                INT4 *pi4FsTcpConnLocalPort,
                                UINT4 *pu4FsTcpConnRemAddress,
                                INT4 *pi4FsTcpConnRemPort)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", *pu4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", *pi4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", *pu4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", *pi4FsTcpConnRemPort); ***/

    UINT4               u4TcbIndex;
    UINT4               u4FirstTcbIndex;
    UINT1               u1GotOne;

    u4FirstTcbIndex = 0;
    u1GotOne = FALSE;

    for (u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
    {
        if (GetTCBstate (u4TcbIndex) == TCPS_FREE ||
            GetTCBstate (u4TcbIndex) == TCPS_ABORT)
        {
            continue;
        }
        if (u1GotOne == FALSE)
        {
            u4FirstTcbIndex = u4TcbIndex;
            u1GotOne = TRUE;
        }
        if (u1GotOne == TRUE)
        {
            if (GetTCBv4lip (u4TcbIndex) < GetTCBv4lip (u4FirstTcbIndex))
            {
                u4FirstTcbIndex = u4TcbIndex;
                continue;
            }
            if (GetTCBv4lip (u4TcbIndex) > GetTCBv4lip (u4FirstTcbIndex))
            {
                continue;
            }
            if (GetTCBlport (u4TcbIndex) < GetTCBlport (u4FirstTcbIndex))
            {
                u4FirstTcbIndex = u4TcbIndex;
                continue;
            }
            if (GetTCBlport (u4TcbIndex) > GetTCBlport (u4FirstTcbIndex))
            {
                continue;
            }
            if (GetTCBv4rip (u4TcbIndex) < GetTCBv4rip (u4FirstTcbIndex))
            {
                u4FirstTcbIndex = u4TcbIndex;
                continue;
            }
            if (GetTCBv4rip (u4TcbIndex) > GetTCBv4rip (u4FirstTcbIndex))
            {
                continue;
            }
            if (GetTCBrport (u4TcbIndex) < GetTCBrport (u4FirstTcbIndex))
            {
                u4FirstTcbIndex = u4TcbIndex;
                continue;
            }
            if (GetTCBrport (u4TcbIndex) > GetTCBrport (u4FirstTcbIndex))
            {
                continue;
            }
        }
    }

    if (u1GotOne == FALSE)
    {
        TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                     "Get of first index in TCP ConnTable failed.\n");

   /*** $$TRACE_LOG (EXIT," SNMP Failure\n"); ***/
        return SNMP_FAILURE;
    }
    *pu4FsTcpConnLocalAddress = GetTCBv4lip (u4FirstTcbIndex);
    *pi4FsTcpConnLocalPort = GetTCBlport (u4FirstTcbIndex);
    *pu4FsTcpConnRemAddress = GetTCBv4rip (u4FirstTcbIndex);
    *pi4FsTcpConnRemPort = GetTCBrport (u4FirstTcbIndex);

    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of first index in TCP ConnTable returned \n");

    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Local Address = %d \nLocal Port = %d \n",
                 *pu4FsTcpConnLocalAddress, *pi4FsTcpConnLocalPort);

    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Remote Address = %d \nRemote Port = %d.\n",
                 *pu4FsTcpConnRemAddress, *pi4FsTcpConnRemPort);

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsTcpConnTable
 Input       :  The Indices
                FsTcpConnLocalAddress
                nextFsTcpConnLocalAddress
                FsTcpConnLocalPort
                nextFsTcpConnLocalPort
                FsTcpConnRemAddress
                nextFsTcpConnRemAddress
                FsTcpConnRemPort
                nextFsTcpConnRemPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextFsTcpConnTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexFsTcpConnTable (UINT4 u4FsTcpConnLocalAddress,
                               UINT4 *pu4NextFsTcpConnLocalAddress,
                               INT4 i4FsTcpConnLocalPort,
                               INT4 *pi4NextFsTcpConnLocalPort,
                               UINT4 u4FsTcpConnRemAddress,
                               UINT4 *pu4NextFsTcpConnRemAddress,
                               INT4 i4FsTcpConnRemPort,
                               INT4 *pi4NextFsTcpConnRemPort)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", *pu4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", *pi4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", *pu4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", *pi4FsTcpConnRemPort); ***/

    UINT4               u4TcbIndex;
    UINT4               u4NextTcpConnLocalAddress;    /*Current getnext LocalAddr */
    UINT4               u4NextTcpConnRemAddress;    /*Current getnext RemoteAddr */
    UINT4               u4TcpCurLocalAddr;    /*Local Addresses in TCB */
    UINT4               u4TcpCurRemoteAddr;    /*Remote Addresses in TCB */
    INT4                i4NextTcpConnLocalPort;    /*Current getnext Local port */
    INT4                i4NextTcpConnRemPort;    /*Current getnext Remoteport */
    INT4                i4TcpCurRemotePort;    /*Remote port in TCB */
    INT4                i4TcpCurLocalPort;    /*Local port in TCB */
    INT4                i4Found = 0;

    /*Initialise the get next values to 0. We will update this value once we
     *identify an index greater than the requested index*/
    u4NextTcpConnLocalAddress = 0;
    i4NextTcpConnLocalPort = 0;
    u4NextTcpConnRemAddress = 0;
    i4NextTcpConnRemPort = 0;

    for (u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
    {
        if (GetTCBstate (u4TcbIndex) == TCPS_FREE ||
            GetTCBstate (u4TcbIndex) == TCPS_ABORT)
        {
            continue;
        }

        u4TcpCurLocalAddr = GetTCBv4lip (u4TcbIndex);
        i4TcpCurLocalPort = GetTCBlport (u4TcbIndex);
        u4TcpCurRemoteAddr = GetTCBv4rip (u4TcbIndex);
        i4TcpCurRemotePort = GetTCBrport (u4TcbIndex);

        /*Check whether this entry's indexes are less than the requested
         * indexes. If so, skip this entry*/
        if (GetTCBContext (u4TcbIndex) == gpTcpCurrContext->u4ContextId)
        {
            if (u4TcpCurLocalAddr < u4FsTcpConnLocalAddress)
            {
                continue;
            }
            if ((u4TcpCurLocalAddr == u4FsTcpConnLocalAddress) &&
                (i4TcpCurLocalPort < i4FsTcpConnLocalPort))
            {
                continue;
            }
            if ((u4TcpCurLocalAddr == u4FsTcpConnLocalAddress) &&
                (i4TcpCurLocalPort == i4FsTcpConnLocalPort) &&
                (u4TcpCurRemoteAddr < u4FsTcpConnRemAddress))
            {
                continue;
            }
            if ((u4TcpCurLocalAddr == u4FsTcpConnLocalAddress) &&
                (i4TcpCurLocalPort == i4FsTcpConnLocalPort) &&
                (u4TcpCurRemoteAddr == u4FsTcpConnRemAddress) &&
                (i4TcpCurRemotePort <= i4FsTcpConnRemPort))
            {
                /*Last condition in if clause is <=, to avoid returning
                 * the incoming index!*/
                continue;
            }

            /*Now it is sure that current index is greater than the requested
             * Index. If we didn't identified a getnext so far, we can initialise
             * the getnext value with current index.*/

            if (i4Found == 0)
            {
                u4NextTcpConnLocalAddress = u4TcpCurLocalAddr;
                i4NextTcpConnLocalPort = i4TcpCurLocalPort;
                u4NextTcpConnRemAddress = u4TcpCurRemoteAddr;
                i4NextTcpConnRemPort = i4TcpCurRemotePort;
                i4Found = 1;
                continue;
            }

            /*Check whether this entry is greater than the currently
             * identified getnext index. If so, skip this entry*/

            if (u4TcpCurLocalAddr > u4NextTcpConnLocalAddress)
            {
                continue;
            }
            if ((u4TcpCurLocalAddr == u4NextTcpConnLocalAddress) &&
                (i4TcpCurLocalPort > i4NextTcpConnLocalPort))
            {
                continue;
            }
            if ((u4TcpCurLocalAddr == u4NextTcpConnLocalAddress) &&
                (i4TcpCurLocalPort == i4NextTcpConnLocalPort) &&
                (u4TcpCurRemoteAddr > u4NextTcpConnRemAddress))
            {
                continue;
            }
            if ((u4TcpCurLocalAddr == u4NextTcpConnLocalAddress) &&
                (i4TcpCurLocalPort == i4NextTcpConnLocalPort) &&
                (u4TcpCurRemoteAddr == u4NextTcpConnRemAddress) &&
                (i4TcpCurRemotePort > i4NextTcpConnRemPort))
            {
                continue;
            }

            /*Now it is sure that Current index is not greater than the
             * present get next value. So we need to take the new value
             * for get next :-)*/

            u4NextTcpConnLocalAddress = u4TcpCurLocalAddr;
            i4NextTcpConnLocalPort = i4TcpCurLocalPort;
            u4NextTcpConnRemAddress = u4TcpCurRemoteAddr;
            i4NextTcpConnRemPort = i4TcpCurRemotePort;
        }
    }
    if (i4Found == 0)
    {
        TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                     "Getnext of index returned failure.\n");
        return SNMP_FAILURE;
    }

    *pu4NextFsTcpConnLocalAddress = u4NextTcpConnLocalAddress;
    *pi4NextFsTcpConnLocalPort = i4NextTcpConnLocalPort;
    *pu4NextFsTcpConnRemAddress = u4NextTcpConnRemAddress;
    *pi4NextFsTcpConnRemPort = i4NextTcpConnRemPort;

    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "GetNext in TCP ConnTable returned\n");
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Local Address = %d \nLocal Port = %d\n ",
                 *pu4NextFsTcpConnLocalAddress, *pi4NextFsTcpConnLocalPort);

    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Remote Address = %d \nRemote Port = %d.\n",
                 *pu4NextFsTcpConnRemAddress, *pi4NextFsTcpConnRemPort);

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTcpConnOutState
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                retValFsTcpConnOutState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpConnOutState ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpConnOutState (UINT4 u4FsTcpConnLocalAddress,
                         INT4 i4FsTcpConnLocalPort, UINT4 u4FsTcpConnRemAddress,
                         INT4 i4FsTcpConnRemPort,
                         INT4 *pi4RetValFsTcpConnOutState)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;

    u4TcbIndex =
        tcpSnmpGetConnID (u4Context, u4FsTcpConnLocalAddress,
                          i4FsTcpConnLocalPort, u4FsTcpConnRemAddress,
                          i4FsTcpConnRemPort);
    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;

    *pi4RetValFsTcpConnOutState = GetTCBostate (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of TCP connection out state returned %d\n",
                 *pi4RetValFsTcpConnOutState);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsTcpConnSWindow
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                retValFsTcpConnSWindow
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpConnSWindow ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpConnSWindow (UINT4 u4FsTcpConnLocalAddress,
                        INT4 i4FsTcpConnLocalPort, UINT4 u4FsTcpConnRemAddress,
                        INT4 i4FsTcpConnRemPort,
                        INT4 *pi4RetValFsTcpConnSWindow)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;

    u4TcbIndex =
        tcpSnmpGetConnID (u4Context, u4FsTcpConnLocalAddress,
                          i4FsTcpConnLocalPort, u4FsTcpConnRemAddress,
                          i4FsTcpConnRemPort);
    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;

    *pi4RetValFsTcpConnSWindow = GetTCBswindow (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of TCP connection send window returned %d\n",
                 *pi4RetValFsTcpConnSWindow);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTcpConnRWindow
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                retValFsTcpConnRWindow
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpConnRWindow ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpConnRWindow (UINT4 u4FsTcpConnLocalAddress,
                        INT4 i4FsTcpConnLocalPort, UINT4 u4FsTcpConnRemAddress,
                        INT4 i4FsTcpConnRemPort,
                        INT4 *pi4RetValFsTcpConnRWindow)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;

    u4TcbIndex =
        tcpSnmpGetConnID (u4Context, u4FsTcpConnLocalAddress,
                          i4FsTcpConnLocalPort, u4FsTcpConnRemAddress,
                          i4FsTcpConnRemPort);
    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;

    *pi4RetValFsTcpConnRWindow = GetTCBrbsize (u4TcbIndex) -
        GetTCBrbcount (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of TCP connection receive window returned %d\n",
                 *pi4RetValFsTcpConnRWindow);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsTcpConnCWindow
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                retValFsTcpConnCWindow
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpConnCWindow ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpConnCWindow (UINT4 u4FsTcpConnLocalAddress,
                        INT4 i4FsTcpConnLocalPort, UINT4 u4FsTcpConnRemAddress,
                        INT4 i4FsTcpConnRemPort,
                        INT4 *pi4RetValFsTcpConnCWindow)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;

    u4TcbIndex =
        tcpSnmpGetConnID (u4Context, u4FsTcpConnLocalAddress,
                          i4FsTcpConnLocalPort, u4FsTcpConnRemAddress,
                          i4FsTcpConnRemPort);
    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;

    *pi4RetValFsTcpConnCWindow = GetTCBcwnd (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of TCP connection congestion window returned %d\n",
                 *pi4RetValFsTcpConnCWindow);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

  /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsTcpConnSSThresh
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                retValFsTcpConnSSThresh
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpConnSSThresh ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpConnSSThresh (UINT4 u4FsTcpConnLocalAddress,
                         INT4 i4FsTcpConnLocalPort, UINT4 u4FsTcpConnRemAddress,
                         INT4 i4FsTcpConnRemPort,
                         INT4 *pi4RetValFsTcpConnSSThresh)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;

    u4TcbIndex =
        tcpSnmpGetConnID (u4Context, u4FsTcpConnLocalAddress,
                          i4FsTcpConnLocalPort, u4FsTcpConnRemAddress,
                          i4FsTcpConnRemPort);
    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;

    *pi4RetValFsTcpConnSSThresh = GetTCBssthresh (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of TCP connection slow start threshold value returned %d\n",
                 *pi4RetValFsTcpConnSSThresh);
        /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTcpConnSMSS
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                retValFsTcpConnSMSS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpConnSMSS ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpConnSMSS (UINT4 u4FsTcpConnLocalAddress, INT4 i4FsTcpConnLocalPort,
                     UINT4 u4FsTcpConnRemAddress, INT4 i4FsTcpConnRemPort,
                     INT4 *pi4RetValFsTcpConnSMSS)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;
    u4TcbIndex =
        tcpSnmpGetConnID (u4Context, u4FsTcpConnLocalAddress,
                          i4FsTcpConnLocalPort, u4FsTcpConnRemAddress,
                          i4FsTcpConnRemPort);
    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;

    *pi4RetValFsTcpConnSMSS = GetTCBsmss (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of TCP connection sender MSS value returned %d\n",
                 *pi4RetValFsTcpConnSMSS);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsTcpConnRMSS
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                retValFsTcpConnRMSS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpConnRMSS ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpConnRMSS (UINT4 u4FsTcpConnLocalAddress, INT4 i4FsTcpConnLocalPort,
                     UINT4 u4FsTcpConnRemAddress, INT4 i4FsTcpConnRemPort,
                     INT4 *pi4RetValFsTcpConnRMSS)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;

    u4TcbIndex =
        tcpSnmpGetConnID (u4Context, u4FsTcpConnLocalAddress,
                          i4FsTcpConnLocalPort, u4FsTcpConnRemAddress,
                          i4FsTcpConnRemPort);
    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;

    *pi4RetValFsTcpConnRMSS = GetTCBrmss (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of TCP connection receiver MSS value returned %d\n",
                 *pi4RetValFsTcpConnRMSS);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsTcpConnSRT
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                retValFsTcpConnSRT
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpConnSRT ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpConnSRT (UINT4 u4FsTcpConnLocalAddress, INT4 i4FsTcpConnLocalPort,
                    UINT4 u4FsTcpConnRemAddress, INT4 i4FsTcpConnRemPort,
                    INT4 *pi4RetValFsTcpConnSRT)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;

    u4TcbIndex =
        tcpSnmpGetConnID (u4Context, u4FsTcpConnLocalAddress,
                          i4FsTcpConnLocalPort, u4FsTcpConnRemAddress,
                          i4FsTcpConnRemPort);
    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;

    *pi4RetValFsTcpConnSRT = GetTCBsrt (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of TCP connection smooth round trip time value returned %d\n",
                 *pi4RetValFsTcpConnSRT);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsTcpConnRTDE
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                retValFsTcpConnRTDE
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpConnRTDE ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpConnRTDE (UINT4 u4FsTcpConnLocalAddress, INT4 i4FsTcpConnLocalPort,
                     UINT4 u4FsTcpConnRemAddress, INT4 i4FsTcpConnRemPort,
                     INT4 *pi4RetValFsTcpConnRTDE)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;

    u4TcbIndex =
        tcpSnmpGetConnID (u4Context, u4FsTcpConnLocalAddress,
                          i4FsTcpConnLocalPort, u4FsTcpConnRemAddress,
                          i4FsTcpConnRemPort);
    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;

    *pi4RetValFsTcpConnRTDE = GetTCBrtde (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of TCP connection round trip deviation estimator value returned %d\n",
                 *pi4RetValFsTcpConnRTDE);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsTcpConnPersist
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                retValFsTcpConnPersist
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpConnPersist ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpConnPersist (UINT4 u4FsTcpConnLocalAddress,
                        INT4 i4FsTcpConnLocalPort, UINT4 u4FsTcpConnRemAddress,
                        INT4 i4FsTcpConnRemPort,
                        INT4 *pi4RetValFsTcpConnPersist)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;

    u4TcbIndex =
        tcpSnmpGetConnID (u4Context, u4FsTcpConnLocalAddress,
                          i4FsTcpConnLocalPort, u4FsTcpConnRemAddress,
                          i4FsTcpConnRemPort);
    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;

    *pi4RetValFsTcpConnPersist = GetTCBpersist (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of TCP connection persist timeout value returned %d\n",
                 *pi4RetValFsTcpConnPersist);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsTcpConnRexmt
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                retValFsTcpConnRexmt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpConnRexmt ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpConnRexmt (UINT4 u4FsTcpConnLocalAddress, INT4 i4FsTcpConnLocalPort,
                      UINT4 u4FsTcpConnRemAddress, INT4 i4FsTcpConnRemPort,
                      INT4 *pi4RetValFsTcpConnRexmt)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;

    u4TcbIndex =
        tcpSnmpGetConnID (u4Context, u4FsTcpConnLocalAddress,
                          i4FsTcpConnLocalPort, u4FsTcpConnRemAddress,
                          i4FsTcpConnRemPort);
    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;

    *pi4RetValFsTcpConnRexmt = GetTCBrexmt (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of TCP connection retransmit timeout value returned %d\n",
                 *pi4RetValFsTcpConnRexmt);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsTcpConnRexmtCnt
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                retValFsTcpConnRexmtCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpConnRexmtCnt ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpConnRexmtCnt (UINT4 u4FsTcpConnLocalAddress,
                         INT4 i4FsTcpConnLocalPort, UINT4 u4FsTcpConnRemAddress,
                         INT4 i4FsTcpConnRemPort,
                         INT4 *pi4RetValFsTcpConnRexmtCnt)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;

    u4TcbIndex =
        tcpSnmpGetConnID (u4Context, u4FsTcpConnLocalAddress,
                          i4FsTcpConnLocalPort, u4FsTcpConnRemAddress,
                          i4FsTcpConnRemPort);
    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;

    *pi4RetValFsTcpConnRexmtCnt = GetTCBrexmtcount (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of TCP connection retransmit count returned %d\n",
                 *pi4RetValFsTcpConnRexmtCnt);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsTcpConnSBCount
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                retValFsTcpConnSBCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpConnSBCount ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpConnSBCount (UINT4 u4FsTcpConnLocalAddress,
                        INT4 i4FsTcpConnLocalPort, UINT4 u4FsTcpConnRemAddress,
                        INT4 i4FsTcpConnRemPort,
                        INT4 *pi4RetValFsTcpConnSBCount)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;

    u4TcbIndex =
        tcpSnmpGetConnID (u4Context, u4FsTcpConnLocalAddress,
                          i4FsTcpConnLocalPort, u4FsTcpConnRemAddress,
                          i4FsTcpConnRemPort);
    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;

    *pi4RetValFsTcpConnSBCount = GetTCBsbcount (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of TCP connection send buffer count returned %d\n",
                 *pi4RetValFsTcpConnSBCount);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
}

/****************************************************************************
 Function    :  nmhGetFsTcpConnSBSize
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                retValFsTcpConnSBSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpConnSBSize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpConnSBSize (UINT4 u4FsTcpConnLocalAddress, INT4 i4FsTcpConnLocalPort,
                       UINT4 u4FsTcpConnRemAddress, INT4 i4FsTcpConnRemPort,
                       INT4 *pi4RetValFsTcpConnSBSize)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;

    u4TcbIndex =
        tcpSnmpGetConnID (u4Context, u4FsTcpConnLocalAddress,
                          i4FsTcpConnLocalPort, u4FsTcpConnRemAddress,
                          i4FsTcpConnRemPort);
    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;

    *pi4RetValFsTcpConnSBSize = GetTCBsbsize (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of TCP connection send buffer size returned %d\n",
                 *pi4RetValFsTcpConnSBSize);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsTcpConnRBCount
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                retValFsTcpConnRBCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpConnRBCount ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpConnRBCount (UINT4 u4FsTcpConnLocalAddress,
                        INT4 i4FsTcpConnLocalPort, UINT4 u4FsTcpConnRemAddress,
                        INT4 i4FsTcpConnRemPort,
                        INT4 *pi4RetValFsTcpConnRBCount)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;

    u4TcbIndex =
        tcpSnmpGetConnID (u4Context, u4FsTcpConnLocalAddress,
                          i4FsTcpConnLocalPort, u4FsTcpConnRemAddress,
                          i4FsTcpConnRemPort);
    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;

    *pi4RetValFsTcpConnRBCount = GetTCBrbcount (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of TCP connection receive buffer count returned %d\n",
                 *pi4RetValFsTcpConnRBCount);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsTcpConnRBSize
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                retValFsTcpConnRBSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpConnRBSize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpConnRBSize (UINT4 u4FsTcpConnLocalAddress, INT4 i4FsTcpConnLocalPort,
                       UINT4 u4FsTcpConnRemAddress, INT4 i4FsTcpConnRemPort,
                       INT4 *pi4RetValFsTcpConnRBSize)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;

    u4TcbIndex =
        tcpSnmpGetConnID (u4Context, u4FsTcpConnLocalAddress,
                          i4FsTcpConnLocalPort, u4FsTcpConnRemAddress,
                          i4FsTcpConnRemPort);
    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;

    *pi4RetValFsTcpConnRBSize = GetTCBrbsize (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of TCP connection receive buffer size returned %d\n",
                 *pi4RetValFsTcpConnRBSize);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsTcpKaMainTmr
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                retValFsTcpKaMainTmr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpKaMainTmr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpKaMainTmr (UINT4 u4FsTcpConnLocalAddress, INT4 i4FsTcpConnLocalPort,
                      UINT4 u4FsTcpConnRemAddress, INT4 i4FsTcpConnRemPort,
                      INT4 *pi4RetValFsTcpKaMainTmr)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;

    u4TcbIndex = tcpSnmpGetConnID (u4Context, u4FsTcpConnLocalAddress,
                                   i4FsTcpConnLocalPort,
                                   u4FsTcpConnRemAddress, i4FsTcpConnRemPort);
    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;

    *pi4RetValFsTcpKaMainTmr = (GetTCBKaMainTmOut (u4TcbIndex) / TCP_CLK_TCK);
    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of FsTcpKaMainTmr returned %d.\n",
                 *pi4RetValFsTcpKaMainTmr);
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsTcpKaRetransTmr
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                retValFsTcpKaRetransTmr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpKaRetransTmr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpKaRetransTmr (UINT4 u4FsTcpConnLocalAddress,
                         INT4 i4FsTcpConnLocalPort, UINT4 u4FsTcpConnRemAddress,
                         INT4 i4FsTcpConnRemPort,
                         INT4 *pi4RetValFsTcpKaRetransTmr)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;

    u4TcbIndex = tcpSnmpGetConnID (u4Context, u4FsTcpConnLocalAddress,
                                   i4FsTcpConnLocalPort,
                                   u4FsTcpConnRemAddress, i4FsTcpConnRemPort);
    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;

    *pi4RetValFsTcpKaRetransTmr =
        (UINT1) (GetTCBKaRetransTmOut (u4TcbIndex) / TCP_CLK_TCK);
    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of FsTcpKaRetransTmr returned %d.\n",
                 *pi4RetValFsTcpKaRetransTmr);
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsTcpKaRetransCnt
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                retValFsTcpKaRetransCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpKaRetransCnt ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpKaRetransCnt (UINT4 u4FsTcpConnLocalAddress,
                         INT4 i4FsTcpConnLocalPort, UINT4 u4FsTcpConnRemAddress,
                         INT4 i4FsTcpConnRemPort,
                         INT4 *pi4RetValFsTcpKaRetransCnt)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;

    u4TcbIndex = tcpSnmpGetConnID (u4Context, u4FsTcpConnLocalAddress,
                                   i4FsTcpConnLocalPort,
                                   u4FsTcpConnRemAddress, i4FsTcpConnRemPort);
    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;

    *pi4RetValFsTcpKaRetransCnt = GetTCBKaRetransCnt (u4TcbIndex);
    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of FsTcpKaRetransTmr returned %d.\n",
                 *pi4RetValFsTcpKaRetransCnt);
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsTcpKaMainTmr
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                setValFsTcpKaMainTmr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsTcpKaMainTmr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsTcpKaMainTmr (UINT4 u4FsTcpConnLocalAddress,
                      INT4 i4FsTcpConnLocalPort, UINT4 u4FsTcpConnRemAddress,
                      INT4 i4FsTcpConnRemPort, INT4 i4SetValFsTcpKaMainTmr)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpKaMainTmr = %d\n", i4SetValFsTcpKaMainTmr); ***/
    UINT4               u4TcbIndex;
    UINT4               u4TimeLeft;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;

    u4TcbIndex =
        tcpSnmpGetConnID (u4Context, u4FsTcpConnLocalAddress,
                          i4FsTcpConnLocalPort, u4FsTcpConnRemAddress,
                          i4FsTcpConnRemPort);
    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;

    /* If the value of KaRetransOverCnt is 0, then the main timer has to be
     * restarted with the new value and started again. If the expired time is
     * more than the new value, KA starts immediately */

    if ((GetTCBvalopts (u4TcbIndex) & TCPO_KEEPALIVE) &&
        (GetTCBKaRetransOverCnt (u4TcbIndex) == 0))
    {
        TmrGetRemainingTime ((tTimerListId) gTcpTimerListId,
                             &(TCBtable[u4TcbIndex].TcbKaTimer.node),
                             &u4TimeLeft);
        if (u4TimeLeft == TMR_FAILURE)
        {
            TCP_MOD_TRC (u4Context, TCP_MGMT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                         "In set KaMainTmr, timer failure\n");
            return SNMP_FAILURE;
        }
        if ((GetTCBKaMainTmOut (u4TcbIndex) - u4TimeLeft) >
            (i4SetValFsTcpKaMainTmr * TCP_CLK_TCK))
        {
            TmrStopTimer (gTcpTimerListId,
                          &(TCBtable[u4TcbIndex].TcbKaTimer.node));
            TmrKaTimerExpiryHandler (&(TCBtable[u4TcbIndex].TcbKaTimer));
        }
        else
        {
            TcpTmrRestartTimer (&(TCBtable[u4TcbIndex].TcbKaTimer),
                                (i4SetValFsTcpKaMainTmr * TCP_CLK_TCK) -
                                (GetTCBKaMainTmOut (u4TcbIndex) - u4TimeLeft));
        }
    }

    SetTCBKaMainTmOut (u4TcbIndex, i4SetValFsTcpKaMainTmr * TCP_CLK_TCK);
    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Keepalive timeout set to %d\n", i4SetValFsTcpKaMainTmr);
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

}

/****************************************************************************
 Function    :  nmhSetFsTcpKaRetransTmr
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                setValFsTcpKaRetransTmr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsTcpKaRetransTmr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsTcpKaRetransTmr (UINT4 u4FsTcpConnLocalAddress,
                         INT4 i4FsTcpConnLocalPort,
                         UINT4 u4FsTcpConnRemAddress, INT4 i4FsTcpConnRemPort,
                         INT4 i4SetValFsTcpKaRetransTmr)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpKaRetransTmr = %d\n", i4SetValFsTcpKaRetransTmr); ***/
    UINT4               u4TcbIndex;
    UINT4               u4TimeLeft;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;

    u4TcbIndex =
        tcpSnmpGetConnID (u4Context, u4FsTcpConnLocalAddress,
                          i4FsTcpConnLocalPort, u4FsTcpConnRemAddress,
                          i4FsTcpConnRemPort);

    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;

    if ((GetTCBvalopts (u4TcbIndex) & TCPO_KEEPALIVE) &&
        (GetTCBKaRetransOverCnt (u4TcbIndex) > 1))
    {
        TmrGetRemainingTime ((tTimerListId) gTcpTimerListId,
                             &(TCBtable[u4TcbIndex].
                               TcbKaTimer.node), &u4TimeLeft);
        if (u4TimeLeft == TMR_FAILURE)
        {
            TCP_MOD_TRC (u4Context, TCP_MGMT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                         "In set KaRetransTmr, timer failure\n");
            return SNMP_FAILURE;
        }

        if ((GetTCBKaRetransTmOut (u4TcbIndex) - u4TimeLeft) >
            (UINT2) (i4SetValFsTcpKaRetransTmr * TCP_CLK_TCK))
        {
            TmrStopTimer (gTcpTimerListId,
                          &(TCBtable[u4TcbIndex].TcbKaTimer.node));
            TmrKaTimerExpiryHandler (&(TCBtable[u4TcbIndex].TcbKaTimer));
        }
        else
        {
            TcpTmrRestartTimer (&(TCBtable[u4TcbIndex].TcbKaTimer),
                                (i4SetValFsTcpKaRetransTmr * TCP_CLK_TCK) -
                                (GetTCBKaRetransTmOut (u4TcbIndex) -
                                 u4TimeLeft));
        }
    }

    SetTCBKaRetransTmOut (u4TcbIndex,
                          (UINT2) (i4SetValFsTcpKaRetransTmr * TCP_CLK_TCK));
    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Keepalive retrans timeout set to %d\n",
                 i4SetValFsTcpKaRetransTmr);
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsTcpKaRetransCnt
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                setValFsTcpKaRetransCnt
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsTcpKaRetransCnt ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsTcpKaRetransCnt (UINT4 u4FsTcpConnLocalAddress,
                         INT4 i4FsTcpConnLocalPort,
                         UINT4 u4FsTcpConnRemAddress, INT4 i4FsTcpConnRemPort,
                         INT4 i4SetValFsTcpKaRetransCnt)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpKaRetransCnt = %d\n", i4SetValFsTcpKaRetransCnt); ***/
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;

    u4TcbIndex =
        tcpSnmpGetConnID (u4Context, u4FsTcpConnLocalAddress,
                          i4FsTcpConnLocalPort, u4FsTcpConnRemAddress,
                          i4FsTcpConnRemPort);

    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;

    if ((GetTCBvalopts (u4TcbIndex) & TCPO_KEEPALIVE) &&
        (GetTCBKaRetransOverCnt (u4TcbIndex) > 1))
    {

        if ((GetTCBKaRetransOverCnt (u4TcbIndex) != 0) &&
            (((GetTCBKaRetransOverCnt (u4TcbIndex)) - 1) >
             i4SetValFsTcpKaRetransCnt))
        {
            TmrStopTimer (gTcpTimerListId,
                          &(TCBtable[u4TcbIndex].TcbKaTimer.node));
            SetTCBKaRetransCnt (u4TcbIndex, (UINT1) i4SetValFsTcpKaRetransCnt);
            TmrKaTimerExpiryHandler (&TCBtable[u4TcbIndex].TcbKaTimer);
        }
    }

    SetTCBKaRetransCnt (u4TcbIndex, (UINT1) i4SetValFsTcpKaRetransCnt);
    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Keepalive retrans count set value to %d\n",
                 i4SetValFsTcpKaRetransCnt);
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsTcpKaMainTmr
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                testValFsTcpKaMainTmr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsTcpKaMainTmr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsTcpKaMainTmr (UINT4 *pu4ErrorCode, UINT4 u4FsTcpConnLocalAddress,
                         INT4 i4FsTcpConnLocalPort, UINT4 u4FsTcpConnRemAddress,
                         INT4 i4FsTcpConnRemPort, INT4 i4TestValFsTcpKaMainTmr)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpKaMainTmr = %d\n", i4TestValFsTcpKaMainTmr); ***/
    UNUSED_PARAM (u4FsTcpConnLocalAddress);
    UNUSED_PARAM (i4FsTcpConnLocalPort);
    UNUSED_PARAM (u4FsTcpConnRemAddress);
    UNUSED_PARAM (i4FsTcpConnRemPort);
    if ((i4TestValFsTcpKaMainTmr <= 0) || (i4TestValFsTcpKaMainTmr > 0xffff))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "KA main timer tested for value %d\n",
                 i4TestValFsTcpKaMainTmr);
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsTcpKaRetransTmr
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                testValFsTcpKaRetransTmr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsTcpKaRetransTmr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsTcpKaRetransTmr (UINT4 *pu4ErrorCode, UINT4 u4FsTcpConnLocalAddress,
                            INT4 i4FsTcpConnLocalPort,
                            UINT4 u4FsTcpConnRemAddress,
                            INT4 i4FsTcpConnRemPort,
                            INT4 i4TestValFsTcpKaRetransTmr)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpKaRetransTmr = %d\n", i4TestValFsTcpKaRetransTmr); ***/
    UNUSED_PARAM (u4FsTcpConnLocalAddress);
    UNUSED_PARAM (i4FsTcpConnLocalPort);
    UNUSED_PARAM (u4FsTcpConnRemAddress);
    UNUSED_PARAM (i4FsTcpConnRemPort);
    if ((i4TestValFsTcpKaRetransTmr < 0) || (i4TestValFsTcpKaRetransTmr > 0xff))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "KA retrans timer tested for value %d\n",
                 i4TestValFsTcpKaRetransTmr);
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsTcpKaRetransCnt
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort

                The Object 
                testValFsTcpKaRetransCnt
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsTcpKaRetransCnt ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsTcpKaRetransCnt (UINT4 *pu4ErrorCode, UINT4 u4FsTcpConnLocalAddress,
                            INT4 i4FsTcpConnLocalPort,
                            UINT4 u4FsTcpConnRemAddress,
                            INT4 i4FsTcpConnRemPort,
                            INT4 i4TestValFsTcpKaRetransCnt)
{
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnLocalAddress = %u\n", u4FsTcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnLocalPort = %d\n", i4FsTcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"FsTcpConnRemAddress = %u\n", u4FsTcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpConnRemPort = %d\n", i4FsTcpConnRemPort); ***/
   /*** $$TRACE_LOG (ENTRY, "FsTcpKaRetransCnt = %d\n", i4TestValFsTcpKaRetransCnt); ***/

    UNUSED_PARAM (u4FsTcpConnLocalAddress);
    UNUSED_PARAM (i4FsTcpConnLocalPort);
    UNUSED_PARAM (u4FsTcpConnRemAddress);
    UNUSED_PARAM (i4FsTcpConnRemPort);
    UNUSED_PARAM (pu4ErrorCode);
    if ((i4TestValFsTcpKaRetransCnt < 0) || (i4TestValFsTcpKaRetransCnt > 0xff))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "KA retrans count tested for value %d\n",
                 i4TestValFsTcpKaRetransCnt);
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsTcpConnTable
 Input       :  The Indices
                FsTcpConnLocalAddress
                FsTcpConnLocalPort
                FsTcpConnRemAddress
                FsTcpConnRemPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTcpConnTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTcpAckOption
 Input       :  The Indices

                The Object 
                retValFsTcpAckOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpAckOption ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpAckOption (INT4 *pi4RetValFsTcpAckOption)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    *pi4RetValFsTcpAckOption = gpTcpCurrContext->TcpConfigParms.i4TcpAckOpt;

    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of tcp ACK option returned %d\n",
                 *pi4RetValFsTcpAckOption);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsTcpTimeStampOption
 Input       :  The Indices

                The Object 
                retValFsTcpTimeStampOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpTimeStampOption ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpTimeStampOption (INT4 *pi4RetValFsTcpTimeStampOption)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    *pi4RetValFsTcpTimeStampOption =
        gpTcpCurrContext->TcpConfigParms.i4TcpTSOpt;

    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of tcp timestamp option returned %d\n",
                 *pi4RetValFsTcpTimeStampOption);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsTcpBigWndOption
 Input       :  The Indices

                The Object 
                retValFsTcpBigWndOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpBigWndOption ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpBigWndOption (INT4 *pi4RetValFsTcpBigWndOption)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    *pi4RetValFsTcpBigWndOption = gpTcpCurrContext->TcpConfigParms.i4TcpBWOpt;

    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of tcp bigwindow option returned %d\n",
                 *pi4RetValFsTcpBigWndOption);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsTcpIncrIniWnd
 Input       :  The Indices

                The Object 
                retValFsTcpIncrIniWnd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpIncrIniWnd ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpIncrIniWnd (INT4 *pi4RetValFsTcpIncrIniWnd)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    *pi4RetValFsTcpIncrIniWnd =
        gpTcpCurrContext->TcpConfigParms.i4TcpIncrIniWnd;

    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of tcp increment initial window returned %d\n",
                 *pi4RetValFsTcpIncrIniWnd);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsTcpMaxNumOfTCB
 Input       :  The Indices

                The Object 
                retValFsTcpMaxNumOfTCB
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpMaxNumOfTCB ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpMaxNumOfTCB (INT4 *pi4RetValFsTcpMaxNumOfTCB)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    *pi4RetValFsTcpMaxNumOfTCB = MAX_NUM_OF_TCB;

    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of tcp Maximum number of TCB returned %d\n",
                 *pi4RetValFsTcpMaxNumOfTCB);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

}

/***********************************************************************
 Function    :  nmhGetFsTcpTraceDebug
 Input       :  The Indices

                The Object
                retValFsTcpTraceDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsTcpTraceDebug ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsTcpTraceDebug (INT4 *pi4RetValFsTcpTraceDebug)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    *pi4RetValFsTcpTraceDebug = gu4TcpDbgMap;
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of Trace option returned %d\n",
                 *pi4RetValFsTcpTraceDebug);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsTcpAckOption
 Input       :  The Indices

                The Object 
                setValFsTcpAckOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsTcpAckOption ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsTcpAckOption (INT4 i4SetValFsTcpAckOption)
{
   /*** $$TRACE_LOG (ENTRY, "FsTcpAckOption = %d\n", i4SetValFsTcpAckOption); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    gpTcpCurrContext->TcpConfigParms.i4TcpAckOpt = i4SetValFsTcpAckOption;
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Set of tcp ACK option with %d returned success.\n",
                 i4SetValFsTcpAckOption);

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsTcpTimeStampOption
 Input       :  The Indices

                The Object 
                setValFsTcpTimeStampOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsTcpTimeStampOption ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsTcpTimeStampOption (INT4 i4SetValFsTcpTimeStampOption)
{
   /*** $$TRACE_LOG (ENTRY, "FsTcpTimeStampOption = %d\n", i4SetValFsTcpTimeStampOption); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    gpTcpCurrContext->TcpConfigParms.i4TcpTSOpt = i4SetValFsTcpTimeStampOption;
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Set of tcp timestamp option with %d returned success.\n",
                 i4SetValFsTcpTimeStampOption);

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsTcpBigWndOption
 Input       :  The Indices

                The Object 
                setValFsTcpBigWndOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsTcpBigWndOption ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsTcpBigWndOption (INT4 i4SetValFsTcpBigWndOption)
{
   /*** $$TRACE_LOG (ENTRY, "FsTcpBigWndOption = %d\n", i4SetValFsTcpBigWndOption); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    gpTcpCurrContext->TcpConfigParms.i4TcpBWOpt = i4SetValFsTcpBigWndOption;
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Set of tcp bigwindow option with %d returned success.\n",
                 i4SetValFsTcpBigWndOption);

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsTcpIncrIniWnd
 Input       :  The Indices

                The Object 
                setValFsTcpIncrIniWnd
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsTcpIncrIniWnd ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsTcpIncrIniWnd (INT4 i4SetValFsTcpIncrIniWnd)
{
   /*** $$TRACE_LOG (ENTRY, "FsTcpIncrIniWnd = %d\n", i4SetValFsTcpIncrIniWnd); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    gpTcpCurrContext->TcpConfigParms.i4TcpIncrIniWnd = i4SetValFsTcpIncrIniWnd;

    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Set of incr initial window with %d returned success.\n",
                 i4SetValFsTcpIncrIniWnd);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsTcpMaxNumOfTCB
 Input       :  The Indices

                The Object 
                setValFsTcpMaxNumOfTCB
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsTcpMaxNumOfTCB ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsTcpMaxNumOfTCB (INT4 i4SetValFsTcpMaxNumOfTCB)
{
   /*** $$TRACE_LOG (ENTRY, "FsTcpMaxNumOfTCB = %d\n", i4SetValFsTcpMaxNumOfTCB); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    tTcpSystemSize      TcpNewSizingParams;

    /* Set the Tcp System size params */
    GetTcpSizingParams (&TcpNewSizingParams);
    TcpNewSizingParams.u4TcpMaxNumOfTCB = i4SetValFsTcpMaxNumOfTCB;
    SetTcpSizingParams (&TcpNewSizingParams);

    /* Update the system size parameters */
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Set of TCP Maximum Number of TCB with %d returned success.\n",
                 i4SetValFsTcpMaxNumOfTCB);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

}

/*********************************************************************
 Function    :  nmhSetFsTcpTraceDebug
 Input       :  The Indices

                The Object
                setValFsTcpTraceDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsTcpTraceDebug ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsTcpTraceDebug (INT4 i4SetValFsTcpTraceDebug)
{
   /*** $$TRACE_LOG (ENTRY, "FsTcpTraceDebug = %d\n", i4SetValFsTcpTraceDebug);
***/
    gu4TcpDbgMap = i4SetValFsTcpTraceDebug;
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Set of TraceDebug with %d returned success.\n",
                 i4SetValFsTcpTraceDebug);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/*}***************************************************************************
 Function    :  nmhTestv2FsTcpAckOption
 Input       :  The Indices

                The Object 
                testValFsTcpAckOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsTcpAckOption ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsTcpAckOption (UINT4 *pu4ErrorCode, INT4 i4TestValFsTcpAckOption)
{
   /*** $$TRACE_LOG (ENTRY, "FsTcpAckOption = %d\n", i4TestValFsTcpAckOption); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    if ((i4TestValFsTcpAckOption >= TCP_DEFACK) &&
        (i4TestValFsTcpAckOption <= TCP_FST_RXMTCONF))
    {
        TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                     "Test of tcp ACK option for value %d returned success\n",
                     i4TestValFsTcpAckOption);
        return SNMP_SUCCESS;
    }
    else
    {
        TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                     "Test of tcp ACK option for value %d returned failure\n",
                     i4TestValFsTcpAckOption);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2FsTcpTimeStampOption
 Input       :  The Indices

                The Object 
                testValFsTcpTimeStampOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsTcpTimeStampOption ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsTcpTimeStampOption (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsTcpTimeStampOption)
{
   /*** $$TRACE_LOG (ENTRY, "FsTcpTimeStampOption = %d\n", i4TestValFsTcpTimeStampOption); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    if ((i4TestValFsTcpTimeStampOption == TCP_TSCONF) ||
        (i4TestValFsTcpTimeStampOption == TCP_NOCONF))
    {
        TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                     "Test of tcp ACK option for value %d returned success\n",
                     i4TestValFsTcpTimeStampOption);
        return SNMP_SUCCESS;
    }
    else
    {
        TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                     "Test of tcp ACK option for value %d returned failure\n",
                     i4TestValFsTcpTimeStampOption);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2FsTcpBigWndOption
 Input       :  The Indices

                The Object 
                testValFsTcpBigWndOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsTcpBigWndOption ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsTcpBigWndOption (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsTcpBigWndOption)
{
   /*** $$TRACE_LOG (ENTRY, "FsTcpBigWndOption = %d\n", i4TestValFsTcpBigWndOption); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    if ((i4TestValFsTcpBigWndOption == TCP_BWCONF) ||
        (i4TestValFsTcpBigWndOption == TCP_NOCONF))
    {
        TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                     "Test of tcp bigwindow option for value %d returned success.\n",
                     i4TestValFsTcpBigWndOption);
        return SNMP_SUCCESS;
    }
    else
    {
        TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                     "Test of tcp bigwindow option for value %d returned failure\n",
                     i4TestValFsTcpBigWndOption);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2FsTcpIncrIniWnd
 Input       :  The Indices

                The Object 
                testValFsTcpIncrIniWnd
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsTcpIncrIniWnd ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsTcpIncrIniWnd (UINT4 *pu4ErrorCode, INT4 i4TestValFsTcpIncrIniWnd)
{
   /*** $$TRACE_LOG (ENTRY, "FsTcpIncrIniWnd = %d\n", i4TestValFsTcpIncrIniWnd); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    if ((i4TestValFsTcpIncrIniWnd == TCP_INC_INI_WNDCONF) ||
        (i4TestValFsTcpIncrIniWnd == TCP_NOCONF))
    {
        TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                     "Test of tcp IncrIniWnd option for value %d returned success\n",
                     i4TestValFsTcpIncrIniWnd);
        return SNMP_SUCCESS;
    }
    else
    {
        TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                     "Test of tcp IncrIniWnd option for value %d returned failure.\n",
                     i4TestValFsTcpIncrIniWnd);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2FsTcpMaxNumOfTCB
 Input       :  The Indices

                The Object 
                testValFsTcpMaxNumOfTCB
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsTcpMaxNumOfTCB ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsTcpMaxNumOfTCB (UINT4 *pu4ErrorCode, INT4 i4TestValFsTcpMaxNumOfTCB)
{
   /*** $$TRACE_LOG (ENTRY, "FsTcpMaxNumOfTCB = %d\n", i4TestValFsTcpMaxNumOfTCB); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    if (i4TestValFsTcpMaxNumOfTCB > 0)
    {
        TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                     "Test of tcp Maximum Number of TCB %d returned success.\n",
                     i4TestValFsTcpMaxNumOfTCB);
        return SNMP_SUCCESS;
    }
    else
    {
        TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                     "Test of tcp Maximum Number of TCB %d returned failure.\n",
                     i4TestValFsTcpMaxNumOfTCB);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2FsTcpTraceDebug
 Input       :  The Indices

                The Object
                testValFsTcpTraceDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsTcpTraceDebug ***/
/**** PROCEDURE LEVEL -- LOW ****/
INT1
nmhTestv2FsTcpTraceDebug (UINT4 *pu4ErrorCode, INT4 i4TestValFsTcpTraceDebug)
{
   /*** $$TRACE_LOG (ENTRY, "FsTcpTraceDebug = %d\n", i4TestValFsTcpTraceDebug); ***/
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsTcpTraceDebug);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTcpMaxReTries
 Input       :  The Indices

                The Object 
                retValFsTcpMaxReTries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTcpMaxReTries (INT4 *pi4RetValFsTcpMaxReTries)
{
    *pi4RetValFsTcpMaxReTries =
        gpTcpCurrContext->TcpConfigParms.u2TcpMaxRetries;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsTcpMaxReTries
 Input       :  The Indices

                The Object 
                setValFsTcpMaxReTries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTcpMaxReTries (INT4 i4SetValFsTcpMaxReTries)
{
    gpTcpCurrContext->TcpConfigParms.u2TcpMaxRetries = i4SetValFsTcpMaxReTries;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsTcpMaxReTries
 Input       :  The Indices

                The Object 
                testValFsTcpMaxReTries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTcpMaxReTries (UINT4 *pu4ErrorCode, INT4 i4TestValFsTcpMaxReTries)
{
    /* Check the max and min values for MaxRetries */
    if ((i4TestValFsTcpMaxReTries < TCP_MINRETRIES)
        || (i4TestValFsTcpMaxReTries > TCP_MAXRETRIES))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTcpTrapAdminStatus
 Input       :  The Indices

                The Object 
                retValFsTcpTrapAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTcpTrapAdminStatus (INT4 *pi4RetValFsTcpTrapAdminStatus)
{
    *pi4RetValFsTcpTrapAdminStatus = gpTcpCurrContext->u1TcpTrapAdmnSts;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsTcpTrapAdminStatus
 Input       :  The Indices

                The Object 
                setValFsTcpTrapAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTcpTrapAdminStatus (INT4 i4SetValFsTcpTrapAdminStatus)
{
    gpTcpCurrContext->u1TcpTrapAdmnSts = (UINT1) i4SetValFsTcpTrapAdminStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsTcpTrapAdminStatus
 Input       :  The Indices

                The Object 
                testValFsTcpTrapAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTcpTrapAdminStatus (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsTcpTrapAdminStatus)
{
    if ((i4TestValFsTcpTrapAdminStatus != TCP_TRAP_ENABLE)
        && (i4TestValFsTcpTrapAdminStatus != TCP_TRAP_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsTcpAckOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTcpAckOption (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsTcpTimeStampOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTcpTimeStampOption (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsTcpBigWndOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTcpBigWndOption (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsTcpIncrIniWnd
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTcpIncrIniWnd (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsTcpMaxNumOfTCB
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTcpMaxNumOfTCB (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsTcpTraceDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTcpTraceDebug (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsTcpMaxReTries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTcpMaxReTries (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsTcpTrapAdminStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTcpTrapAdminStatus (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/******************************************************************************* ** RETURN  TCP_OK/ERR
 **
 ** FUNCTION
 ** Gets the connection index specified by the indices exist. It is done by a
 ** simple scan through the TCB table.
 **
 ** CALLING CONDITION
 **
 **
 ** GLOBAL VARIABLES AFFECTED none
******************************************************************************/
INT4
tcpSnmpGetConnID (u4Context, u4LocalAddress, u4LocalPort, u4RemoteAddress,
                  u4RemotePort)
     UINT4               u4Context;
     UINT4               u4LocalAddress;
     UINT4               u4LocalPort;
     UINT4               u4RemoteAddress;
     UINT4               u4RemotePort;
{
    UINT4               u4TcbIndex;

    for (u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
    {
        if ((u4Context != GetTCBContext (u4TcbIndex)) ||
            (GetTCBstate (u4TcbIndex) == TCPS_FREE) ||
            (u4LocalPort != GetTCBlport (u4TcbIndex)) ||
            (u4RemotePort != GetTCBrport (u4TcbIndex)) ||
            (u4RemoteAddress != GetTCBv4rip (u4TcbIndex)) ||
            (u4LocalAddress != GetTCBv4lip (u4TcbIndex)) ||
            (GetTCBstate (u4TcbIndex) == TCPS_ABORT))
        {
            continue;
        }
        return u4TcbIndex;
    }
    return ERR;
}

/* LOW LEVEL Routines for Table : FsTcpExtConnTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsTcpExtConnTable
 Input       :  The Indices
                TcpConnectionLocalAddressType
                TcpConnectionLocalAddress
                TcpConnectionLocalPort
                TcpConnectionRemAddressType
                TcpConnectionRemAddress
                TcpConnectionRemPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsTcpExtConnTable (INT4 i4TcpConnectionLocalAddressType,
                                           tSNMP_OCTET_STRING_TYPE
                                           * pTcpConnectionLocalAddress,
                                           UINT4 u4TcpConnectionLocalPort,
                                           INT4 i4TcpConnectionRemAddressType,
                                           tSNMP_OCTET_STRING_TYPE
                                           * pTcpConnectionRemAddress,
                                           UINT4 u4TcpConnectionRemPort)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        nmhValidateIndexInstanceTcpConnectionTable
        (i4TcpConnectionLocalAddressType,
         pTcpConnectionLocalAddress,
         u4TcpConnectionLocalPort,
         i4TcpConnectionRemAddressType,
         pTcpConnectionRemAddress, u4TcpConnectionRemPort);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsTcpExtConnTable
 Input       :  The Indices
                TcpConnectionLocalAddressType
                TcpConnectionLocalAddress
                TcpConnectionLocalPort
                TcpConnectionRemAddressType
                TcpConnectionRemAddress
                TcpConnectionRemPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsTcpExtConnTable (INT4 *pi4TcpConnectionLocalAddressType,
                                   tSNMP_OCTET_STRING_TYPE
                                   * pTcpConnectionLocalAddress,
                                   UINT4 *pu4TcpConnectionLocalPort,
                                   INT4 *pi4TcpConnectionRemAddressType,
                                   tSNMP_OCTET_STRING_TYPE
                                   * pTcpConnectionRemAddress,
                                   UINT4 *pu4TcpConnectionRemPort)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        nmhGetFirstIndexTcpConnectionTable
        (pi4TcpConnectionLocalAddressType,
         pTcpConnectionLocalAddress,
         pu4TcpConnectionLocalPort,
         pi4TcpConnectionRemAddressType,
         pTcpConnectionRemAddress, pu4TcpConnectionRemPort);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsTcpExtConnTable
 Input       :  The Indices
                TcpConnectionLocalAddressType
                nextTcpConnectionLocalAddressType
                TcpConnectionLocalAddress
                nextTcpConnectionLocalAddress
                TcpConnectionLocalPort
                nextTcpConnectionLocalPort
                TcpConnectionRemAddressType
                nextTcpConnectionRemAddressType
                TcpConnectionRemAddress
                nextTcpConnectionRemAddress
                TcpConnectionRemPort
                nextTcpConnectionRemPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsTcpExtConnTable (INT4 i4TcpConnectionLocalAddressType,
                                  INT4 *pi4NextTcpConnectionLocalAddressType,
                                  tSNMP_OCTET_STRING_TYPE
                                  * pTcpConnectionLocalAddress,
                                  tSNMP_OCTET_STRING_TYPE
                                  * pNextTcpConnectionLocalAddress,
                                  UINT4 u4TcpConnectionLocalPort,
                                  UINT4 *pu4NextTcpConnectionLocalPort,
                                  INT4 i4TcpConnectionRemAddressType,
                                  INT4 *pi4NextTcpConnectionRemAddressType,
                                  tSNMP_OCTET_STRING_TYPE
                                  * pTcpConnectionRemAddress,
                                  tSNMP_OCTET_STRING_TYPE
                                  * pNextTcpConnectionRemAddress,
                                  UINT4 u4TcpConnectionRemPort,
                                  UINT4 *pu4NextTcpConnectionRemPort)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        nmhGetNextIndexTcpConnectionTable
        (i4TcpConnectionLocalAddressType,
         pi4NextTcpConnectionLocalAddressType,
         pTcpConnectionLocalAddress,
         pNextTcpConnectionLocalAddress,
         u4TcpConnectionLocalPort,
         pu4NextTcpConnectionLocalPort,
         i4TcpConnectionRemAddressType,
         pi4NextTcpConnectionRemAddressType,
         pTcpConnectionRemAddress,
         pNextTcpConnectionRemAddress,
         u4TcpConnectionRemPort, pu4NextTcpConnectionRemPort);

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTcpConnMD5Option
 Input       :  The Indices
                TcpConnectionLocalAddressType
                TcpConnectionLocalAddress
                TcpConnectionLocalPort
                TcpConnectionRemAddressType
                TcpConnectionRemAddress
                TcpConnectionRemPort

                The Object
                retValFsTcpConnMD5Option
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTcpConnMD5Option (INT4 i4TcpConnectionLocalAddressType,
                          tSNMP_OCTET_STRING_TYPE * pTcpConnectionLocalAddress,
                          UINT4 u4TcpConnectionLocalPort,
                          INT4 i4TcpConnectionRemAddressType,
                          tSNMP_OCTET_STRING_TYPE * pTcpConnectionRemAddress,
                          UINT4 u4TcpConnectionRemPort,
                          INT4 *pi4RetValFsTcpConnMD5Option)
{
    INT4                i4TcbIndex = TCP_ZERO;
    INT4                i4Count = TCP_ZERO;
    UINT4               u4TempLocalTcpAddr = TCP_ZERO;
    UINT4               u4TempRemTcpAddr = TCP_ZERO;
    UINT4               u4Context;

    tIpAddr             TempLocalIP;
    tIpAddr             TempRemoteIP;

    MEMSET (&TempLocalIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TempRemoteIP, TCP_ZERO, sizeof (tIpAddr));

    UNUSED_PARAM (i4TcpConnectionRemAddressType);
    if (pTcpConnectionLocalAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempLocalIP,
                (tIpAddr *) (VOID *) pTcpConnectionLocalAddress->pu1_OctetList,
                pTcpConnectionLocalAddress->i4_Length);
    }
    if (pTcpConnectionRemAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempRemoteIP,
                (tIpAddr *) (VOID *) pTcpConnectionRemAddress->pu1_OctetList,
                pTcpConnectionRemAddress->i4_Length);
    }
    if (i4TcpConnectionLocalAddressType == TCP_ONE)
    {
        TempLocalIP.u4_addr[0] = OSIX_NTOHL (TempLocalIP.u4_addr[0]);
        TempRemoteIP.u4_addr[0] = OSIX_NTOHL (TempRemoteIP.u4_addr[0]);
    }
    else if (i4TcpConnectionLocalAddressType == TCP_TWO)
    {
        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            TempLocalIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempLocalIP.u4_addr[i4Count]);
            TempRemoteIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempRemoteIP.u4_addr[i4Count]);
        }
    }

    MEMCPY (pTcpConnectionRemAddress->pu1_OctetList, &TempRemoteIP,
            pTcpConnectionRemAddress->i4_Length);
    MEMCPY (pTcpConnectionLocalAddress->pu1_OctetList, &TempLocalIP,
            pTcpConnectionLocalAddress->i4_Length);

    u4Context = gpTcpCurrContext->u4ContextId;

    i4TcbIndex = tcpGetConnTableID (u4Context,
                                    i4TcpConnectionLocalAddressType,
                                    pTcpConnectionLocalAddress,
                                    u4TcpConnectionLocalPort,
                                    pTcpConnectionRemAddress,
                                    u4TcpConnectionRemPort);

    if (i4TcpConnectionLocalAddressType == TCP_ONE)
    {
        MEMCPY (&u4TempLocalTcpAddr, pTcpConnectionLocalAddress->pu1_OctetList,
                pTcpConnectionLocalAddress->i4_Length);
        u4TempLocalTcpAddr = OSIX_HTONL (u4TempLocalTcpAddr);
        MEMCPY (pTcpConnectionLocalAddress->pu1_OctetList, &u4TempLocalTcpAddr,
                pTcpConnectionLocalAddress->i4_Length);

        MEMCPY (&u4TempRemTcpAddr, pTcpConnectionRemAddress->pu1_OctetList,
                pTcpConnectionRemAddress->i4_Length);
        u4TempRemTcpAddr = OSIX_HTONL (u4TempRemTcpAddr);
        MEMCPY (pTcpConnectionRemAddress->pu1_OctetList, &u4TempRemTcpAddr,
                pTcpConnectionRemAddress->i4_Length);
    }
    else if (i4TcpConnectionLocalAddressType == TCP_TWO)
    {
        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            TempLocalIP.u4_addr[i4Count] =
                OSIX_HTONL (TempLocalIP.u4_addr[i4Count]);
            TempRemoteIP.u4_addr[i4Count] =
                OSIX_HTONL (TempRemoteIP.u4_addr[i4Count]);
        }

        MEMCPY (pTcpConnectionLocalAddress->pu1_OctetList, &TempLocalIP,
                pTcpConnectionLocalAddress->i4_Length);
        MEMCPY (pTcpConnectionRemAddress->pu1_OctetList, &TempRemoteIP,
                pTcpConnectionRemAddress->i4_Length);
    }

    if (i4TcbIndex == TCP_NOT_OK)
    {
        return SNMP_FAILURE;
    }

    if (GetTCBmd5keylen (i4TcbIndex) != TCP_ZERO)
    {
        *pi4RetValFsTcpConnMD5Option = TCP_MD5CONF;
    }
    else
    {
        *pi4RetValFsTcpConnMD5Option = TCP_NOCONF;
    }
    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of fsTcpConnMD5Option returned %d.\n",
                 *pi4RetValFsTcpConnMD5Option);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTcpConnMD5ErrCtr
 Input       :  The Indices
                TcpConnectionLocalAddressType
                TcpConnectionLocalAddress
                TcpConnectionLocalPort
                TcpConnectionRemAddressType
                TcpConnectionRemAddress
                TcpConnectionRemPort

                The Object
                retValFsTcpConnMD5ErrCtr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTcpConnMD5ErrCtr (INT4 i4TcpConnectionLocalAddressType,
                          tSNMP_OCTET_STRING_TYPE * pTcpConnectionLocalAddress,
                          UINT4 u4TcpConnectionLocalPort,
                          INT4 i4TcpConnectionRemAddressType,
                          tSNMP_OCTET_STRING_TYPE * pTcpConnectionRemAddress,
                          UINT4 u4TcpConnectionRemPort,
                          INT4 *pi4RetValFsTcpConnMD5ErrCtr)
{
    INT4                i4TcbIndex = TCP_ZERO;
    INT4                i4Count = TCP_ZERO;
    UINT4               u4TempLocalTcpAddr = TCP_ZERO;
    UINT4               u4TempRemTcpAddr = TCP_ZERO;
    UINT4               u4Context;

    tIpAddr             TempLocalIP;
    tIpAddr             TempRemoteIP;

    MEMSET (&TempLocalIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TempRemoteIP, TCP_ZERO, sizeof (tIpAddr));

    u4Context = gpTcpCurrContext->u4ContextId;

    UNUSED_PARAM (i4TcpConnectionRemAddressType);
    if (pTcpConnectionLocalAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempLocalIP,
                (tIpAddr *) (VOID *) pTcpConnectionLocalAddress->pu1_OctetList,
                pTcpConnectionLocalAddress->i4_Length);
    }
    if (pTcpConnectionRemAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempRemoteIP,
                (tIpAddr *) (VOID *) pTcpConnectionRemAddress->pu1_OctetList,
                pTcpConnectionRemAddress->i4_Length);
    }
    if (i4TcpConnectionLocalAddressType == TCP_ONE)
    {
        TempLocalIP.u4_addr[0] = OSIX_NTOHL (TempLocalIP.u4_addr[0]);
        TempRemoteIP.u4_addr[0] = OSIX_NTOHL (TempRemoteIP.u4_addr[0]);
    }
    else if (i4TcpConnectionLocalAddressType == TCP_TWO)
    {
        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            TempLocalIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempLocalIP.u4_addr[i4Count]);
            TempRemoteIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempRemoteIP.u4_addr[i4Count]);
        }
    }

    MEMCPY (pTcpConnectionRemAddress->pu1_OctetList, &TempRemoteIP,
            pTcpConnectionRemAddress->i4_Length);
    MEMCPY (pTcpConnectionLocalAddress->pu1_OctetList, &TempLocalIP,
            pTcpConnectionLocalAddress->i4_Length);

    i4TcbIndex = tcpGetConnTableID (u4Context,
                                    i4TcpConnectionLocalAddressType,
                                    pTcpConnectionLocalAddress,
                                    u4TcpConnectionLocalPort,
                                    pTcpConnectionRemAddress,
                                    u4TcpConnectionRemPort);

    if (i4TcpConnectionLocalAddressType == TCP_ONE)
    {
        MEMCPY (&u4TempLocalTcpAddr, pTcpConnectionLocalAddress->pu1_OctetList,
                pTcpConnectionLocalAddress->i4_Length);
        u4TempLocalTcpAddr = OSIX_HTONL (u4TempLocalTcpAddr);
        MEMCPY (pTcpConnectionLocalAddress->pu1_OctetList, &u4TempLocalTcpAddr,
                pTcpConnectionLocalAddress->i4_Length);

        MEMCPY (&u4TempRemTcpAddr, pTcpConnectionRemAddress->pu1_OctetList,
                pTcpConnectionRemAddress->i4_Length);
        u4TempRemTcpAddr = OSIX_HTONL (u4TempRemTcpAddr);
        MEMCPY (pTcpConnectionRemAddress->pu1_OctetList, &u4TempRemTcpAddr,
                pTcpConnectionRemAddress->i4_Length);
    }
    else if (i4TcpConnectionLocalAddressType == TCP_TWO)
    {
        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            TempLocalIP.u4_addr[i4Count] =
                OSIX_HTONL (TempLocalIP.u4_addr[i4Count]);
            TempRemoteIP.u4_addr[i4Count] =
                OSIX_HTONL (TempRemoteIP.u4_addr[i4Count]);
        }

        MEMCPY (pTcpConnectionLocalAddress->pu1_OctetList, &TempLocalIP,
                pTcpConnectionLocalAddress->i4_Length);
        MEMCPY (pTcpConnectionRemAddress->pu1_OctetList, &TempRemoteIP,
                pTcpConnectionRemAddress->i4_Length);
    }

    if (i4TcbIndex == TCP_NOT_OK)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsTcpConnMD5ErrCtr = (INT4) GetTCBmd5errs (i4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of fsTcpConnMD5ErrCtr returned %d.\n",
                 *pi4RetValFsTcpConnMD5ErrCtr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTcpConnTcpAOOption
 Input       :  The Indices
                TcpConnectionLocalAddressType
                TcpConnectionLocalAddress
                TcpConnectionLocalPort
                TcpConnectionRemAddressType
                TcpConnectionRemAddress
                TcpConnectionRemPort

                The Object 
                retValFsTcpConnTcpAOOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTcpConnTcpAOOption (INT4 i4TcpConnectionLocalAddressType,
                            tSNMP_OCTET_STRING_TYPE *
                            pTcpConnectionLocalAddress,
                            UINT4 u4TcpConnectionLocalPort,
                            INT4 i4TcpConnectionRemAddressType,
                            tSNMP_OCTET_STRING_TYPE * pTcpConnectionRemAddress,
                            UINT4 u4TcpConnectionRemPort,
                            INT4 *pi4RetValFsTcpConnTcpAOOption)
{
    INT4                i4TcbIndex = TCP_ZERO;
    INT4                i4Count = TCP_ZERO;
    UINT4               u4Context;

    tIpAddr             TempLocalIP;
    tIpAddr             TempRemoteIP;

    tSNMP_OCTET_STRING_TYPE TcpCurrConnLocalAddress;
    tSNMP_OCTET_STRING_TYPE TcpCurrConnRemAddress;
    UINT1               au1CurrLocalIp[IPVX_MAX_INET_ADR_LEN];
    UINT1               au1CurrRemoteIp[IPVX_MAX_INET_ADR_LEN];

    MEMSET (&TempLocalIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TempRemoteIP, TCP_ZERO, sizeof (tIpAddr));

    MEMSET (&TcpCurrConnLocalAddress, TCP_ZERO,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TcpCurrConnRemAddress, TCP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1CurrLocalIp, TCP_ZERO, IPVX_MAX_INET_ADR_LEN);
    MEMSET (au1CurrRemoteIp, TCP_ZERO, IPVX_MAX_INET_ADR_LEN);

    TcpCurrConnLocalAddress.pu1_OctetList = au1CurrLocalIp;
    TcpCurrConnRemAddress.pu1_OctetList = au1CurrRemoteIp;

    UNUSED_PARAM (i4TcpConnectionRemAddressType);
    if (pTcpConnectionLocalAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempLocalIP,
                (tIpAddr *) (VOID *) pTcpConnectionLocalAddress->pu1_OctetList,
                pTcpConnectionLocalAddress->i4_Length);
    }
    if (pTcpConnectionRemAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempRemoteIP,
                (tIpAddr *) (VOID *) pTcpConnectionRemAddress->pu1_OctetList,
                pTcpConnectionRemAddress->i4_Length);
    }
    if (i4TcpConnectionLocalAddressType == TCP_AF_IPV4_TYPE)
    {
        TempLocalIP.u4_addr[0] = OSIX_NTOHL (TempLocalIP.u4_addr[0]);
        TempRemoteIP.u4_addr[0] = OSIX_NTOHL (TempRemoteIP.u4_addr[0]);
    }
    else if (i4TcpConnectionLocalAddressType == TCP_AF_IPV6_TYPE)
    {
        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            TempLocalIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempLocalIP.u4_addr[i4Count]);
            TempRemoteIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempRemoteIP.u4_addr[i4Count]);
        }
    }

    MEMCPY (TcpCurrConnRemAddress.pu1_OctetList, &TempRemoteIP,
            pTcpConnectionRemAddress->i4_Length);
    MEMCPY (TcpCurrConnLocalAddress.pu1_OctetList, &TempLocalIP,
            pTcpConnectionLocalAddress->i4_Length);

    TcpCurrConnRemAddress.i4_Length = pTcpConnectionRemAddress->i4_Length;
    TcpCurrConnLocalAddress.i4_Length = pTcpConnectionLocalAddress->i4_Length;

    u4Context = gpTcpCurrContext->u4ContextId;

    i4TcbIndex = tcpGetConnTableID (u4Context,
                                    i4TcpConnectionLocalAddressType,
                                    &TcpCurrConnLocalAddress,
                                    u4TcpConnectionLocalPort,
                                    &TcpCurrConnRemAddress,
                                    u4TcpConnectionRemPort);

    if (i4TcbIndex == TCP_NOT_OK)
    {
        return SNMP_FAILURE;
    }

    if (GetTCBTcpAoEnabled (i4TcbIndex) != TCP_ZERO)
    {
        *pi4RetValFsTcpConnTcpAOOption = TRUE;
    }
    else
    {
        *pi4RetValFsTcpConnTcpAOOption = FALSE;
    }
    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of fsTcpConnTcpAOOption returned %d.\n",
                 *pi4RetValFsTcpConnTcpAOOption);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTcpConTcpAOCurKeyId
 Input       :  The Indices
                TcpConnectionLocalAddressType
                TcpConnectionLocalAddress
                TcpConnectionLocalPort
                TcpConnectionRemAddressType
                TcpConnectionRemAddress
                TcpConnectionRemPort

                The Object 
                retValFsTcpConTcpAOCurKeyId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTcpConTcpAOCurKeyId (INT4 i4TcpConnectionLocalAddressType,
                             tSNMP_OCTET_STRING_TYPE *
                             pTcpConnectionLocalAddress,
                             UINT4 u4TcpConnectionLocalPort,
                             INT4 i4TcpConnectionRemAddressType,
                             tSNMP_OCTET_STRING_TYPE * pTcpConnectionRemAddress,
                             UINT4 u4TcpConnectionRemPort,
                             INT4 *pi4RetValFsTcpConTcpAOCurKeyId)
{
    INT4                i4TcbIndex = TCP_ZERO;
    INT4                i4Count = TCP_ZERO;
    UINT4               u4Context;

    tIpAddr             TempLocalIP;
    tIpAddr             TempRemoteIP;

    tSNMP_OCTET_STRING_TYPE TcpCurrConnLocalAddress;
    tSNMP_OCTET_STRING_TYPE TcpCurrConnRemAddress;
    UINT1               au1CurrLocalIp[IPVX_MAX_INET_ADR_LEN];
    UINT1               au1CurrRemoteIp[IPVX_MAX_INET_ADR_LEN];

    MEMSET (&TempLocalIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TempRemoteIP, TCP_ZERO, sizeof (tIpAddr));

    MEMSET (&TcpCurrConnLocalAddress, TCP_ZERO,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TcpCurrConnRemAddress, TCP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1CurrLocalIp, TCP_ZERO, IPVX_MAX_INET_ADR_LEN);
    MEMSET (au1CurrRemoteIp, TCP_ZERO, IPVX_MAX_INET_ADR_LEN);

    TcpCurrConnLocalAddress.pu1_OctetList = au1CurrLocalIp;
    TcpCurrConnRemAddress.pu1_OctetList = au1CurrRemoteIp;

    UNUSED_PARAM (i4TcpConnectionRemAddressType);
    if (pTcpConnectionLocalAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempLocalIP,
                (tIpAddr *) (VOID *) pTcpConnectionLocalAddress->pu1_OctetList,
                pTcpConnectionLocalAddress->i4_Length);
    }
    if (pTcpConnectionRemAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempRemoteIP,
                (tIpAddr *) (VOID *) pTcpConnectionRemAddress->pu1_OctetList,
                pTcpConnectionRemAddress->i4_Length);
    }
    if (i4TcpConnectionLocalAddressType == TCP_AF_IPV4_TYPE)
    {
        TempLocalIP.u4_addr[0] = OSIX_NTOHL (TempLocalIP.u4_addr[0]);
        TempRemoteIP.u4_addr[0] = OSIX_NTOHL (TempRemoteIP.u4_addr[0]);
    }
    else if (i4TcpConnectionLocalAddressType == TCP_AF_IPV6_TYPE)
    {
        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            TempLocalIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempLocalIP.u4_addr[i4Count]);
            TempRemoteIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempRemoteIP.u4_addr[i4Count]);
        }
    }

    MEMCPY (TcpCurrConnRemAddress.pu1_OctetList, &TempRemoteIP,
            pTcpConnectionRemAddress->i4_Length);
    MEMCPY (TcpCurrConnLocalAddress.pu1_OctetList, &TempLocalIP,
            pTcpConnectionLocalAddress->i4_Length);

    TcpCurrConnRemAddress.i4_Length = pTcpConnectionRemAddress->i4_Length;
    TcpCurrConnLocalAddress.i4_Length = pTcpConnectionLocalAddress->i4_Length;

    u4Context = gpTcpCurrContext->u4ContextId;

    i4TcbIndex = tcpGetConnTableID (u4Context,
                                    i4TcpConnectionLocalAddressType,
                                    &TcpCurrConnLocalAddress,
                                    u4TcpConnectionLocalPort,
                                    &TcpCurrConnRemAddress,
                                    u4TcpConnectionRemPort);

    if (i4TcbIndex == TCP_NOT_OK)
    {
        return SNMP_FAILURE;
    }

    if (GetTCBTcpAoEnabled (i4TcbIndex) != TCP_ZERO)
    {
        *pi4RetValFsTcpConTcpAOCurKeyId = GetTCBTcpAoCurrKey (i4TcbIndex);
    }
    else
    {
        *pi4RetValFsTcpConTcpAOCurKeyId = TCP_ZERO;
    }
    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of fsTcpConTcpAOCurKeyId returned %d.\n",
                 *pi4RetValFsTcpConTcpAOCurKeyId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTcpConTcpAORnextKeyId
 Input       :  The Indices
                TcpConnectionLocalAddressType
                TcpConnectionLocalAddress
                TcpConnectionLocalPort
                TcpConnectionRemAddressType
                TcpConnectionRemAddress
                TcpConnectionRemPort

                The Object 
                retValFsTcpConTcpAORnextKeyId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTcpConTcpAORnextKeyId (INT4 i4TcpConnectionLocalAddressType,
                               tSNMP_OCTET_STRING_TYPE *
                               pTcpConnectionLocalAddress,
                               UINT4 u4TcpConnectionLocalPort,
                               INT4 i4TcpConnectionRemAddressType,
                               tSNMP_OCTET_STRING_TYPE *
                               pTcpConnectionRemAddress,
                               UINT4 u4TcpConnectionRemPort,
                               INT4 *pi4RetValFsTcpConTcpAORnextKeyId)
{
    INT4                i4TcbIndex = TCP_ZERO;
    INT4                i4Count = TCP_ZERO;
    UINT4               u4Context;

    tIpAddr             TempLocalIP;
    tIpAddr             TempRemoteIP;

    tSNMP_OCTET_STRING_TYPE TcpCurrConnLocalAddress;
    tSNMP_OCTET_STRING_TYPE TcpCurrConnRemAddress;
    UINT1               au1CurrLocalIp[IPVX_MAX_INET_ADR_LEN];
    UINT1               au1CurrRemoteIp[IPVX_MAX_INET_ADR_LEN];

    MEMSET (&TempLocalIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TempRemoteIP, TCP_ZERO, sizeof (tIpAddr));

    MEMSET (&TcpCurrConnLocalAddress, TCP_ZERO,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TcpCurrConnRemAddress, TCP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1CurrLocalIp, TCP_ZERO, IPVX_MAX_INET_ADR_LEN);
    MEMSET (au1CurrRemoteIp, TCP_ZERO, IPVX_MAX_INET_ADR_LEN);

    TcpCurrConnLocalAddress.pu1_OctetList = au1CurrLocalIp;
    TcpCurrConnRemAddress.pu1_OctetList = au1CurrRemoteIp;

    UNUSED_PARAM (i4TcpConnectionRemAddressType);
    if (pTcpConnectionLocalAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempLocalIP,
                (tIpAddr *) (VOID *) pTcpConnectionLocalAddress->pu1_OctetList,
                pTcpConnectionLocalAddress->i4_Length);
    }
    if (pTcpConnectionRemAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempRemoteIP,
                (tIpAddr *) (VOID *) pTcpConnectionRemAddress->pu1_OctetList,
                pTcpConnectionRemAddress->i4_Length);
    }
    if (i4TcpConnectionLocalAddressType == TCP_AF_IPV4_TYPE)
    {
        TempLocalIP.u4_addr[0] = OSIX_NTOHL (TempLocalIP.u4_addr[0]);
        TempRemoteIP.u4_addr[0] = OSIX_NTOHL (TempRemoteIP.u4_addr[0]);
    }
    else if (i4TcpConnectionLocalAddressType == TCP_AF_IPV6_TYPE)
    {
        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            TempLocalIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempLocalIP.u4_addr[i4Count]);
            TempRemoteIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempRemoteIP.u4_addr[i4Count]);
        }
    }

    MEMCPY (TcpCurrConnRemAddress.pu1_OctetList, &TempRemoteIP,
            pTcpConnectionRemAddress->i4_Length);
    MEMCPY (TcpCurrConnLocalAddress.pu1_OctetList, &TempLocalIP,
            pTcpConnectionLocalAddress->i4_Length);

    TcpCurrConnRemAddress.i4_Length = pTcpConnectionRemAddress->i4_Length;
    TcpCurrConnLocalAddress.i4_Length = pTcpConnectionLocalAddress->i4_Length;

    u4Context = gpTcpCurrContext->u4ContextId;

    i4TcbIndex = tcpGetConnTableID (u4Context,
                                    i4TcpConnectionLocalAddressType,
                                    &TcpCurrConnLocalAddress,
                                    u4TcpConnectionLocalPort,
                                    &TcpCurrConnRemAddress,
                                    u4TcpConnectionRemPort);

    if (i4TcbIndex == TCP_NOT_OK)
    {
        return SNMP_FAILURE;
    }

    if (GetTCBTcpAoEnabled (i4TcbIndex) != TCP_ZERO)
    {
        *pi4RetValFsTcpConTcpAORnextKeyId = GetTCBTcpAoRNextKey (i4TcbIndex);
    }
    else
    {
        *pi4RetValFsTcpConTcpAORnextKeyId = TCP_ZERO;
    }
    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of fsTcpConTcpAORnextKeyId returned %d.\n",
                 *pi4RetValFsTcpConTcpAORnextKeyId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTcpConTcpAORcvKeyId
 Input       :  The Indices
                TcpConnectionLocalAddressType
                TcpConnectionLocalAddress
                TcpConnectionLocalPort
                TcpConnectionRemAddressType
                TcpConnectionRemAddress
                TcpConnectionRemPort

                The Object 
                retValFsTcpConTcpAORcvKeyId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTcpConTcpAORcvKeyId (INT4 i4TcpConnectionLocalAddressType,
                             tSNMP_OCTET_STRING_TYPE *
                             pTcpConnectionLocalAddress,
                             UINT4 u4TcpConnectionLocalPort,
                             INT4 i4TcpConnectionRemAddressType,
                             tSNMP_OCTET_STRING_TYPE * pTcpConnectionRemAddress,
                             UINT4 u4TcpConnectionRemPort,
                             INT4 *pi4RetValFsTcpConTcpAORcvKeyId)
{
    INT4                i4TcbIndex = TCP_ZERO;
    INT4                i4Count = TCP_ZERO;
    UINT4               u4Context;

    tIpAddr             TempLocalIP;
    tIpAddr             TempRemoteIP;

    tSNMP_OCTET_STRING_TYPE TcpCurrConnLocalAddress;
    tSNMP_OCTET_STRING_TYPE TcpCurrConnRemAddress;
    UINT1               au1CurrLocalIp[IPVX_MAX_INET_ADR_LEN];
    UINT1               au1CurrRemoteIp[IPVX_MAX_INET_ADR_LEN];

    MEMSET (&TempLocalIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TempRemoteIP, TCP_ZERO, sizeof (tIpAddr));

    MEMSET (&TcpCurrConnLocalAddress, TCP_ZERO,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TcpCurrConnRemAddress, TCP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1CurrLocalIp, TCP_ZERO, IPVX_MAX_INET_ADR_LEN);
    MEMSET (au1CurrRemoteIp, TCP_ZERO, IPVX_MAX_INET_ADR_LEN);

    TcpCurrConnLocalAddress.pu1_OctetList = au1CurrLocalIp;
    TcpCurrConnRemAddress.pu1_OctetList = au1CurrRemoteIp;

    UNUSED_PARAM (i4TcpConnectionRemAddressType);
    if (pTcpConnectionLocalAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempLocalIP,
                (tIpAddr *) (VOID *) pTcpConnectionLocalAddress->pu1_OctetList,
                pTcpConnectionLocalAddress->i4_Length);
    }
    if (pTcpConnectionRemAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempRemoteIP,
                (tIpAddr *) (VOID *) pTcpConnectionRemAddress->pu1_OctetList,
                pTcpConnectionRemAddress->i4_Length);
    }
    if (i4TcpConnectionLocalAddressType == TCP_AF_IPV4_TYPE)
    {
        TempLocalIP.u4_addr[0] = OSIX_NTOHL (TempLocalIP.u4_addr[0]);
        TempRemoteIP.u4_addr[0] = OSIX_NTOHL (TempRemoteIP.u4_addr[0]);
    }
    else if (i4TcpConnectionLocalAddressType == TCP_AF_IPV6_TYPE)
    {
        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            TempLocalIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempLocalIP.u4_addr[i4Count]);
            TempRemoteIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempRemoteIP.u4_addr[i4Count]);
        }
    }

    MEMCPY (TcpCurrConnRemAddress.pu1_OctetList, &TempRemoteIP,
            pTcpConnectionRemAddress->i4_Length);
    MEMCPY (TcpCurrConnLocalAddress.pu1_OctetList, &TempLocalIP,
            pTcpConnectionLocalAddress->i4_Length);

    TcpCurrConnRemAddress.i4_Length = pTcpConnectionRemAddress->i4_Length;
    TcpCurrConnLocalAddress.i4_Length = pTcpConnectionLocalAddress->i4_Length;

    u4Context = gpTcpCurrContext->u4ContextId;

    i4TcbIndex = tcpGetConnTableID (u4Context,
                                    i4TcpConnectionLocalAddressType,
                                    &TcpCurrConnLocalAddress,
                                    u4TcpConnectionLocalPort,
                                    &TcpCurrConnRemAddress,
                                    u4TcpConnectionRemPort);

    if (i4TcbIndex == TCP_NOT_OK)
    {
        return SNMP_FAILURE;
    }

    if (GetTCBTcpAoEnabled (i4TcbIndex) != TCP_ZERO)
    {
        *pi4RetValFsTcpConTcpAORcvKeyId = GetTCBTcpAoRcvPckKeyId (i4TcbIndex);
    }
    else
    {
        *pi4RetValFsTcpConTcpAORcvKeyId = TCP_ZERO;
    }
    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of fsTcpConTcpAORcvKeyId returned %d.\n",
                 *pi4RetValFsTcpConTcpAORcvKeyId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTcpConTcpAORcvRnextKeyId
 Input       :  The Indices
                TcpConnectionLocalAddressType
                TcpConnectionLocalAddress
                TcpConnectionLocalPort
                TcpConnectionRemAddressType
                TcpConnectionRemAddress
                TcpConnectionRemPort

                The Object 
                retValFsTcpConTcpAORcvRnextKeyId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTcpConTcpAORcvRnextKeyId (INT4 i4TcpConnectionLocalAddressType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTcpConnectionLocalAddress,
                                  UINT4 u4TcpConnectionLocalPort,
                                  INT4 i4TcpConnectionRemAddressType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTcpConnectionRemAddress,
                                  UINT4 u4TcpConnectionRemPort,
                                  INT4 *pi4RetValFsTcpConTcpAORcvRnextKeyId)
{
    INT4                i4TcbIndex = TCP_ZERO;
    INT4                i4Count = TCP_ZERO;
    UINT4               u4Context;

    tIpAddr             TempLocalIP;
    tIpAddr             TempRemoteIP;

    tSNMP_OCTET_STRING_TYPE TcpCurrConnLocalAddress;
    tSNMP_OCTET_STRING_TYPE TcpCurrConnRemAddress;
    UINT1               au1CurrLocalIp[IPVX_MAX_INET_ADR_LEN];
    UINT1               au1CurrRemoteIp[IPVX_MAX_INET_ADR_LEN];

    MEMSET (&TempLocalIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TempRemoteIP, TCP_ZERO, sizeof (tIpAddr));

    MEMSET (&TcpCurrConnLocalAddress, TCP_ZERO,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TcpCurrConnRemAddress, TCP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1CurrLocalIp, TCP_ZERO, IPVX_MAX_INET_ADR_LEN);
    MEMSET (au1CurrRemoteIp, TCP_ZERO, IPVX_MAX_INET_ADR_LEN);

    TcpCurrConnLocalAddress.pu1_OctetList = au1CurrLocalIp;
    TcpCurrConnRemAddress.pu1_OctetList = au1CurrRemoteIp;

    UNUSED_PARAM (i4TcpConnectionRemAddressType);
    if (pTcpConnectionLocalAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempLocalIP,
                (tIpAddr *) (VOID *) pTcpConnectionLocalAddress->pu1_OctetList,
                pTcpConnectionLocalAddress->i4_Length);
    }
    if (pTcpConnectionRemAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempRemoteIP,
                (tIpAddr *) (VOID *) pTcpConnectionRemAddress->pu1_OctetList,
                pTcpConnectionRemAddress->i4_Length);
    }
    if (i4TcpConnectionLocalAddressType == TCP_AF_IPV4_TYPE)
    {
        TempLocalIP.u4_addr[0] = OSIX_NTOHL (TempLocalIP.u4_addr[0]);
        TempRemoteIP.u4_addr[0] = OSIX_NTOHL (TempRemoteIP.u4_addr[0]);
    }
    else if (i4TcpConnectionLocalAddressType == TCP_AF_IPV6_TYPE)
    {
        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            TempLocalIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempLocalIP.u4_addr[i4Count]);
            TempRemoteIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempRemoteIP.u4_addr[i4Count]);
        }
    }

    MEMCPY (TcpCurrConnRemAddress.pu1_OctetList, &TempRemoteIP,
            pTcpConnectionRemAddress->i4_Length);
    MEMCPY (TcpCurrConnLocalAddress.pu1_OctetList, &TempLocalIP,
            pTcpConnectionLocalAddress->i4_Length);

    TcpCurrConnRemAddress.i4_Length = pTcpConnectionRemAddress->i4_Length;
    TcpCurrConnLocalAddress.i4_Length = pTcpConnectionLocalAddress->i4_Length;

    u4Context = gpTcpCurrContext->u4ContextId;

    i4TcbIndex = tcpGetConnTableID (u4Context,
                                    i4TcpConnectionLocalAddressType,
                                    &TcpCurrConnLocalAddress,
                                    u4TcpConnectionLocalPort,
                                    &TcpCurrConnRemAddress,
                                    u4TcpConnectionRemPort);

    if (i4TcbIndex == TCP_NOT_OK)
    {
        return SNMP_FAILURE;
    }

    if (GetTCBTcpAoEnabled (i4TcbIndex) != TCP_ZERO)
    {
        *pi4RetValFsTcpConTcpAORcvRnextKeyId =
            GetTCBTcpAoRcvPckRNxtKeyId (i4TcbIndex);
    }
    else
    {
        *pi4RetValFsTcpConTcpAORcvRnextKeyId = TCP_ZERO;
    }
    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of fsTcpConTcpAORcvRnextKeyId returned %d.\n",
                 *pi4RetValFsTcpConTcpAORcvRnextKeyId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTcpConTcpAOConnErrCtr
 Input       :  The Indices
                TcpConnectionLocalAddressType
                TcpConnectionLocalAddress
                TcpConnectionLocalPort
                TcpConnectionRemAddressType
                TcpConnectionRemAddress
                TcpConnectionRemPort

                The Object 
                retValFsTcpConTcpAOConnErrCtr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTcpConTcpAOConnErrCtr (INT4 i4TcpConnectionLocalAddressType,
                               tSNMP_OCTET_STRING_TYPE *
                               pTcpConnectionLocalAddress,
                               UINT4 u4TcpConnectionLocalPort,
                               INT4 i4TcpConnectionRemAddressType,
                               tSNMP_OCTET_STRING_TYPE *
                               pTcpConnectionRemAddress,
                               UINT4 u4TcpConnectionRemPort,
                               UINT4 *pu4RetValFsTcpConTcpAOConnErrCtr)
{
    INT4                i4TcbIndex = TCP_ZERO;
    INT4                i4Count = TCP_ZERO;
    UINT4               u4Context;

    tIpAddr             TempLocalIP;
    tIpAddr             TempRemoteIP;

    tSNMP_OCTET_STRING_TYPE TcpCurrConnLocalAddress;
    tSNMP_OCTET_STRING_TYPE TcpCurrConnRemAddress;
    UINT1               au1CurrLocalIp[IPVX_MAX_INET_ADR_LEN];
    UINT1               au1CurrRemoteIp[IPVX_MAX_INET_ADR_LEN];

    MEMSET (&TempLocalIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TempRemoteIP, TCP_ZERO, sizeof (tIpAddr));

    MEMSET (&TcpCurrConnLocalAddress, TCP_ZERO,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TcpCurrConnRemAddress, TCP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1CurrLocalIp, TCP_ZERO, IPVX_MAX_INET_ADR_LEN);
    MEMSET (au1CurrRemoteIp, TCP_ZERO, IPVX_MAX_INET_ADR_LEN);

    TcpCurrConnLocalAddress.pu1_OctetList = au1CurrLocalIp;
    TcpCurrConnRemAddress.pu1_OctetList = au1CurrRemoteIp;

    UNUSED_PARAM (i4TcpConnectionRemAddressType);
    if (pTcpConnectionLocalAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempLocalIP,
                (tIpAddr *) (VOID *) pTcpConnectionLocalAddress->pu1_OctetList,
                pTcpConnectionLocalAddress->i4_Length);
    }
    if (pTcpConnectionRemAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempRemoteIP,
                (tIpAddr *) (VOID *) pTcpConnectionRemAddress->pu1_OctetList,
                pTcpConnectionRemAddress->i4_Length);
    }
    if (i4TcpConnectionLocalAddressType == TCP_AF_IPV4_TYPE)
    {
        TempLocalIP.u4_addr[0] = OSIX_NTOHL (TempLocalIP.u4_addr[0]);
        TempRemoteIP.u4_addr[0] = OSIX_NTOHL (TempRemoteIP.u4_addr[0]);
    }
    else if (i4TcpConnectionLocalAddressType == TCP_AF_IPV6_TYPE)
    {
        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            TempLocalIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempLocalIP.u4_addr[i4Count]);
            TempRemoteIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempRemoteIP.u4_addr[i4Count]);
        }
    }

    MEMCPY (TcpCurrConnRemAddress.pu1_OctetList, &TempRemoteIP,
            pTcpConnectionRemAddress->i4_Length);
    MEMCPY (TcpCurrConnLocalAddress.pu1_OctetList, &TempLocalIP,
            pTcpConnectionLocalAddress->i4_Length);

    TcpCurrConnRemAddress.i4_Length = pTcpConnectionRemAddress->i4_Length;
    TcpCurrConnLocalAddress.i4_Length = pTcpConnectionLocalAddress->i4_Length;

    u4Context = gpTcpCurrContext->u4ContextId;

    i4TcbIndex = tcpGetConnTableID (u4Context,
                                    i4TcpConnectionLocalAddressType,
                                    &TcpCurrConnLocalAddress,
                                    u4TcpConnectionLocalPort,
                                    &TcpCurrConnRemAddress,
                                    u4TcpConnectionRemPort);

    if (i4TcbIndex == TCP_NOT_OK)
    {
        return SNMP_FAILURE;
    }

    if (GetTCBTcpAoEnabled (i4TcbIndex) != TCP_ZERO)
    {
        *pu4RetValFsTcpConTcpAOConnErrCtr = (GetTcpAoMacVerErrCtr (i4TcbIndex));
    }
    else
    {
        *pu4RetValFsTcpConTcpAOConnErrCtr = TCP_ZERO;
    }
    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of fsTcpConTcpAOConnErrCtr returned %d.\n",
                 *pu4RetValFsTcpConTcpAOConnErrCtr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTcpConTcpAOSndSne
 Input       :  The Indices
                TcpConnectionLocalAddressType
                TcpConnectionLocalAddress
                TcpConnectionLocalPort
                TcpConnectionRemAddressType
                TcpConnectionRemAddress
                TcpConnectionRemPort

                The Object 
                retValFsTcpConTcpAOSndSne
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTcpConTcpAOSndSne (INT4 i4TcpConnectionLocalAddressType,
                           tSNMP_OCTET_STRING_TYPE * pTcpConnectionLocalAddress,
                           UINT4 u4TcpConnectionLocalPort,
                           INT4 i4TcpConnectionRemAddressType,
                           tSNMP_OCTET_STRING_TYPE * pTcpConnectionRemAddress,
                           UINT4 u4TcpConnectionRemPort,
                           INT4 *pi4RetValFsTcpConTcpAOSndSne)
{
    INT4                i4TcbIndex = TCP_ZERO;
    INT4                i4Count = TCP_ZERO;
    UINT4               u4Context;

    tIpAddr             TempLocalIP;
    tIpAddr             TempRemoteIP;

    tSNMP_OCTET_STRING_TYPE TcpCurrConnLocalAddress;
    tSNMP_OCTET_STRING_TYPE TcpCurrConnRemAddress;
    UINT1               au1CurrLocalIp[IPVX_MAX_INET_ADR_LEN];
    UINT1               au1CurrRemoteIp[IPVX_MAX_INET_ADR_LEN];

    MEMSET (&TempLocalIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TempRemoteIP, TCP_ZERO, sizeof (tIpAddr));

    MEMSET (&TcpCurrConnLocalAddress, TCP_ZERO,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TcpCurrConnRemAddress, TCP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1CurrLocalIp, TCP_ZERO, IPVX_MAX_INET_ADR_LEN);
    MEMSET (au1CurrRemoteIp, TCP_ZERO, IPVX_MAX_INET_ADR_LEN);

    TcpCurrConnLocalAddress.pu1_OctetList = au1CurrLocalIp;
    TcpCurrConnRemAddress.pu1_OctetList = au1CurrRemoteIp;

    UNUSED_PARAM (i4TcpConnectionRemAddressType);
    if (pTcpConnectionLocalAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempLocalIP,
                (tIpAddr *) (VOID *) pTcpConnectionLocalAddress->pu1_OctetList,
                pTcpConnectionLocalAddress->i4_Length);
    }
    if (pTcpConnectionRemAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempRemoteIP,
                (tIpAddr *) (VOID *) pTcpConnectionRemAddress->pu1_OctetList,
                pTcpConnectionRemAddress->i4_Length);
    }
    if (i4TcpConnectionLocalAddressType == TCP_AF_IPV4_TYPE)
    {
        TempLocalIP.u4_addr[0] = OSIX_NTOHL (TempLocalIP.u4_addr[0]);
        TempRemoteIP.u4_addr[0] = OSIX_NTOHL (TempRemoteIP.u4_addr[0]);
    }
    else if (i4TcpConnectionLocalAddressType == TCP_AF_IPV6_TYPE)
    {
        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            TempLocalIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempLocalIP.u4_addr[i4Count]);
            TempRemoteIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempRemoteIP.u4_addr[i4Count]);
        }
    }

    MEMCPY (TcpCurrConnRemAddress.pu1_OctetList, &TempRemoteIP,
            pTcpConnectionRemAddress->i4_Length);
    MEMCPY (TcpCurrConnLocalAddress.pu1_OctetList, &TempLocalIP,
            pTcpConnectionLocalAddress->i4_Length);

    TcpCurrConnRemAddress.i4_Length = pTcpConnectionRemAddress->i4_Length;
    TcpCurrConnLocalAddress.i4_Length = pTcpConnectionLocalAddress->i4_Length;

    u4Context = gpTcpCurrContext->u4ContextId;

    i4TcbIndex = tcpGetConnTableID (u4Context,
                                    i4TcpConnectionLocalAddressType,
                                    &TcpCurrConnLocalAddress,
                                    u4TcpConnectionLocalPort,
                                    &TcpCurrConnRemAddress,
                                    u4TcpConnectionRemPort);

    if (i4TcbIndex == TCP_NOT_OK)
    {
        return SNMP_FAILURE;
    }

    if (GetTCBTcpAoEnabled (i4TcbIndex) != TCP_ZERO)
    {
        *pi4RetValFsTcpConTcpAOSndSne =
            (INT4) (OSIX_HTONL (GetTcpAoSendSne (i4TcbIndex)));
    }
    else
    {
        *pi4RetValFsTcpConTcpAOSndSne = TCP_ZERO;
    }
    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of fsTcpConTcpAOSndSne returned %d.\n",
                 *pi4RetValFsTcpConTcpAOSndSne);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTcpConTcpAORcvSne
 Input       :  The Indices
                TcpConnectionLocalAddressType
                TcpConnectionLocalAddress
                TcpConnectionLocalPort
                TcpConnectionRemAddressType
                TcpConnectionRemAddress
                TcpConnectionRemPort

                The Object 
                retValFsTcpConTcpAORcvSne
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTcpConTcpAORcvSne (INT4 i4TcpConnectionLocalAddressType,
                           tSNMP_OCTET_STRING_TYPE * pTcpConnectionLocalAddress,
                           UINT4 u4TcpConnectionLocalPort,
                           INT4 i4TcpConnectionRemAddressType,
                           tSNMP_OCTET_STRING_TYPE * pTcpConnectionRemAddress,
                           UINT4 u4TcpConnectionRemPort,
                           INT4 *pi4RetValFsTcpConTcpAORcvSne)
{
    INT4                i4TcbIndex = TCP_ZERO;
    INT4                i4Count = TCP_ZERO;
    UINT4               u4Context;

    tIpAddr             TempLocalIP;
    tIpAddr             TempRemoteIP;

    tSNMP_OCTET_STRING_TYPE TcpCurrConnLocalAddress;
    tSNMP_OCTET_STRING_TYPE TcpCurrConnRemAddress;
    UINT1               au1CurrLocalIp[IPVX_MAX_INET_ADR_LEN];
    UINT1               au1CurrRemoteIp[IPVX_MAX_INET_ADR_LEN];

    MEMSET (&TempLocalIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TempRemoteIP, TCP_ZERO, sizeof (tIpAddr));

    MEMSET (&TcpCurrConnLocalAddress, TCP_ZERO,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TcpCurrConnRemAddress, TCP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1CurrLocalIp, TCP_ZERO, IPVX_MAX_INET_ADR_LEN);
    MEMSET (au1CurrRemoteIp, TCP_ZERO, IPVX_MAX_INET_ADR_LEN);

    TcpCurrConnLocalAddress.pu1_OctetList = au1CurrLocalIp;
    TcpCurrConnRemAddress.pu1_OctetList = au1CurrRemoteIp;

    UNUSED_PARAM (i4TcpConnectionRemAddressType);
    if (pTcpConnectionLocalAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempLocalIP,
                (tIpAddr *) (VOID *) pTcpConnectionLocalAddress->pu1_OctetList,
                pTcpConnectionLocalAddress->i4_Length);
    }
    if (pTcpConnectionRemAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempRemoteIP,
                (tIpAddr *) (VOID *) pTcpConnectionRemAddress->pu1_OctetList,
                pTcpConnectionRemAddress->i4_Length);
    }
    if (i4TcpConnectionLocalAddressType == TCP_AF_IPV4_TYPE)
    {
        TempLocalIP.u4_addr[0] = OSIX_NTOHL (TempLocalIP.u4_addr[0]);
        TempRemoteIP.u4_addr[0] = OSIX_NTOHL (TempRemoteIP.u4_addr[0]);
    }
    else if (i4TcpConnectionLocalAddressType == TCP_AF_IPV6_TYPE)
    {
        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            TempLocalIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempLocalIP.u4_addr[i4Count]);
            TempRemoteIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempRemoteIP.u4_addr[i4Count]);
        }
    }

    MEMCPY (TcpCurrConnRemAddress.pu1_OctetList, &TempRemoteIP,
            pTcpConnectionRemAddress->i4_Length);
    MEMCPY (TcpCurrConnLocalAddress.pu1_OctetList, &TempLocalIP,
            pTcpConnectionLocalAddress->i4_Length);

    TcpCurrConnRemAddress.i4_Length = pTcpConnectionRemAddress->i4_Length;
    TcpCurrConnLocalAddress.i4_Length = pTcpConnectionLocalAddress->i4_Length;

    u4Context = gpTcpCurrContext->u4ContextId;

    i4TcbIndex = tcpGetConnTableID (u4Context,
                                    i4TcpConnectionLocalAddressType,
                                    &TcpCurrConnLocalAddress,
                                    u4TcpConnectionLocalPort,
                                    &TcpCurrConnRemAddress,
                                    u4TcpConnectionRemPort);

    if (i4TcbIndex == TCP_NOT_OK)
    {
        return SNMP_FAILURE;
    }

    if (GetTCBTcpAoEnabled (i4TcbIndex) != TCP_ZERO)
    {
        *pi4RetValFsTcpConTcpAORcvSne =
            (INT4) (OSIX_HTONL (GetTcpAoRecvSne (i4TcbIndex)));
    }
    else
    {
        *pi4RetValFsTcpConTcpAORcvSne = TCP_ZERO;
    }
    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of fsTcpConTcpAORcvSne returned %d.\n",
                 *pi4RetValFsTcpConTcpAORcvSne);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsTcpAoConnTestTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsTcpAoConnTestTable
 Input       :  The Indices
                FsTcpAoConnTestLclAdrType
                FsTcpAoConnTestLclAdress
                FsTcpAoConnTestLclPort
                FsTcpAoConnTestRmtAdrType
                FsTcpAoConnTestRmtAdress
                FsTcpAoConnTestRmtPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsTcpAoConnTestTable (INT4 i4FsTcpAoConnTestLclAdrType,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsTcpAoConnTestLclAdress,
                                              UINT4 u4FsTcpAoConnTestLclPort,
                                              INT4 i4FsTcpAoConnTestRmtAdrType,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsTcpAoConnTestRmtAdress,
                                              UINT4 u4FsTcpAoConnTestRmtPort)
{
    UINT4               u4LocalAddress;
    UINT4               u4RemoteAddress;
    UINT4               u4TcbIndex = TCP_ZERO;
    UINT4               u4TcpCurLocalAddrType = TCP_ZERO;
    /*UINT4               u4TcpCurRemoteAddrType = TCP_ZERO; */
    UINT4               u4TempLocalTcpAddr = TCP_ZERO;
    UINT4               u4TempRemTcpAddr = TCP_ZERO;
    UINT4               u4TcbContext;

    INT4                i4Count = TCP_ZERO;

    tIpAddr             TempLocalIP;
    tIpAddr             TempRemoteIP;
    tIpAddr             TcpCurLocalAddr;
    tIpAddr             TcpCurRemoteAddr;

    u4LocalAddress = TCP_ZERO;
    u4RemoteAddress = TCP_ZERO;
    MEMSET (&TempLocalIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TempRemoteIP, TCP_ZERO, sizeof (tIpAddr));

    if (pFsTcpAoConnTestLclAdress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempLocalIP,
                (tIpAddr *) (VOID *) pFsTcpAoConnTestLclAdress->pu1_OctetList,
                pFsTcpAoConnTestLclAdress->i4_Length);
    }
    if (pFsTcpAoConnTestRmtAdress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempRemoteIP,
                (tIpAddr *) (VOID *) pFsTcpAoConnTestRmtAdress->pu1_OctetList,
                pFsTcpAoConnTestRmtAdress->i4_Length);
    }
    if (i4FsTcpAoConnTestLclAdrType == TCP_AF_IPV4_TYPE)
    {
        TempLocalIP.u4_addr[0] = OSIX_NTOHL (TempLocalIP.u4_addr[0]);
        TempRemoteIP.u4_addr[0] = OSIX_NTOHL (TempRemoteIP.u4_addr[0]);
    }
    else if (i4FsTcpAoConnTestLclAdrType == TCP_AF_IPV6_TYPE)
    {
        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            TempLocalIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempLocalIP.u4_addr[i4Count]);
            TempRemoteIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempRemoteIP.u4_addr[i4Count]);
        }
    }
    for (u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
    {

        u4TcpCurLocalAddrType = (UINT4) GetTCBLipAddType (u4TcbIndex);
        /*u4TcpCurRemoteAddrType = (UINT4)GetTCBRipAddType (u4TcbIndex); */
        u4TcbContext = GetTCBContext (u4TcbIndex);

        MEMSET (&TcpCurLocalAddr, TCP_ZERO, sizeof (tIpAddr));
        MEMSET (&TcpCurRemoteAddr, TCP_ZERO, sizeof (tIpAddr));

        if (u4TcbContext == gpTcpCurrContext->u4ContextId)
        {
            if (u4TcpCurLocalAddrType == TCP_AF_IPV4_TYPE)
            {
                u4TempLocalTcpAddr = GetTCBv4lip (u4TcbIndex);
                MEMCPY (&TcpCurLocalAddr, &u4TempLocalTcpAddr, sizeof (UINT4));
                u4TempRemTcpAddr = GetTCBv4rip (u4TcbIndex);
                MEMCPY (&TcpCurRemoteAddr, &u4TempRemTcpAddr, sizeof (UINT4));
            }
            else if (u4TcpCurLocalAddrType == TCP_AF_IPV6_TYPE)
            {
                MEMCPY (&TcpCurLocalAddr, &GetTCBlip (u4TcbIndex),
                        sizeof (tIpAddr));
                MEMCPY (&TcpCurRemoteAddr, &GetTCBrip (u4TcbIndex),
                        sizeof (tIpAddr));
            }
            if ((GetTCBstate (u4TcbIndex) == TCPS_FREE) ||
                (GetTCBstate (u4TcbIndex) == TCPS_ABORT) ||
                (u4FsTcpAoConnTestLclPort != GetTCBlport (u4TcbIndex)) ||
                (u4FsTcpAoConnTestRmtPort != GetTCBrport (u4TcbIndex)) ||
                (MEMCMP (&TcpCurLocalAddr, &TempLocalIP, sizeof (tIpAddr)) !=
                 TCP_ZERO)
                || (MEMCMP (&TcpCurRemoteAddr, &TempRemoteIP, sizeof (tIpAddr))
                    != TCP_ZERO))
            {
                continue;
            }
            TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                         "Validate index called with \n");

            TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                         "local address type = %d \n,"
                         "remote address type = %d \n",
                         i4FsTcpAoConnTestLclAdrType,
                         i4FsTcpAoConnTestRmtAdrType);

            TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                         "local port = %d \nremote port = %d \n",
                         u4FsTcpAoConnTestLclPort, u4FsTcpAoConnTestRmtPort);

            TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                         "remote ip addr = %x \nlocal ip addr = %x\n",
                         u4LocalAddress, u4RemoteAddress);

            TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                         "returned success. \n ");

        /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
            return SNMP_SUCCESS;
        }
    }
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Validate index called with \n");

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (i4FsTcpAoConnTestRmtAdrType);
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "local address type = %d \n,"
                 "remote address type = %d \n",
                 i4FsTcpAoConnTestLclAdrType, i4FsTcpAoConnTestRmtAdrType);

    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "local port = %d \nremote port = %d \n",
                 u4FsTcpAoConnTestLclPort, u4FsTcpAoConnTestRmtPort);
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "remote ip addr = %x \nlocal ip addr = %x\n",
                 u4LocalAddress, u4RemoteAddress);
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "returned failure. \n ");

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsTcpAoConnTestTable
 Input       :  The Indices
                FsTcpAoConnTestLclAdrType
                FsTcpAoConnTestLclAdress
                FsTcpAoConnTestLclPort
                FsTcpAoConnTestRmtAdrType
                FsTcpAoConnTestRmtAdress
                FsTcpAoConnTestRmtPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsTcpAoConnTestTable (INT4 *pi4FsTcpAoConnTestLclAdrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsTcpAoConnTestLclAdress,
                                      UINT4 *pu4FsTcpAoConnTestLclPort,
                                      INT4 *pi4FsTcpAoConnTestRmtAdrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsTcpAoConnTestRmtAdress,
                                      UINT4 *pu4FsTcpAoConnTestRmtPort)
{
    UINT4               u4TcbIndex = TCP_ZERO;
    UINT4               u4FirstTcbIndex = TCP_ZERO;
    UINT4               u4TempLocalTcpAddr = TCP_ZERO;
    UINT4               u4TempRemoteTcpAddr = TCP_ZERO;
    UINT1               u1GotOne = FALSE;
    UINT4               u4TcbContext;
    INT4                i4Count = 0;

    tIpAddr             TcpCurLocalAddr;
    tIpAddr             TcpCurRemoteAddr;

    MEMSET (&TcpCurLocalAddr, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TcpCurRemoteAddr, TCP_ZERO, sizeof (tIpAddr));

    for (u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
    {
        u4TcbContext = GetTCBContext (u4TcbIndex);
        if (u4TcbContext != gpTcpCurrContext->u4ContextId)
        {
            continue;
        }
        if (GetTCBstate (u4TcbIndex) == TCPS_FREE ||
            GetTCBstate (u4TcbIndex) == TCPS_ABORT)
        {
            continue;
        }
        if (u1GotOne == FALSE)
        {
            u4FirstTcbIndex = u4TcbIndex;
            u1GotOne = TRUE;
        }

        if (u1GotOne == TRUE)
        {
            if (GetTCBLipAddType (u4TcbIndex) <
                GetTCBLipAddType (u4FirstTcbIndex))
            {
                u4FirstTcbIndex = u4TcbIndex;
                continue;
            }
            if (GetTCBLipAddType (u4TcbIndex) >
                GetTCBLipAddType (u4FirstTcbIndex))
            {
                continue;
            }
            if (MEMCMP (&GetTCBlip (u4TcbIndex),
                        &GetTCBlip (u4FirstTcbIndex),
                        sizeof (tIpAddr)) < TCP_ZERO)
            {
                u4FirstTcbIndex = u4TcbIndex;
                continue;
            }
            if (MEMCMP (&GetTCBlip (u4TcbIndex),
                        &GetTCBlip (u4FirstTcbIndex),
                        sizeof (tIpAddr)) > TCP_ZERO)
            {
                continue;
            }
            if (GetTCBlport (u4TcbIndex) < GetTCBlport (u4FirstTcbIndex))
            {
                u4FirstTcbIndex = u4TcbIndex;
                continue;
            }
            if (GetTCBlport (u4TcbIndex) > GetTCBlport (u4FirstTcbIndex))
            {
                continue;
            }
            if (GetTCBRipAddType (u4TcbIndex) <
                GetTCBRipAddType (u4FirstTcbIndex))
            {
                u4FirstTcbIndex = u4TcbIndex;
                continue;
            }
            if (GetTCBRipAddType (u4TcbIndex) >
                GetTCBRipAddType (u4FirstTcbIndex))
            {
                continue;
            }
            if (MEMCMP (&GetTCBrip (u4TcbIndex),
                        &GetTCBrip (u4FirstTcbIndex),
                        sizeof (tIpAddr)) < TCP_ZERO)
            {
                u4FirstTcbIndex = u4TcbIndex;
                continue;
            }
            if (MEMCMP (&GetTCBrip (u4TcbIndex),
                        &GetTCBrip (u4FirstTcbIndex),
                        sizeof (tIpAddr)) > TCP_ZERO)
            {
                continue;
            }
            if (GetTCBrport (u4TcbIndex) < GetTCBrport (u4FirstTcbIndex))
            {
                u4FirstTcbIndex = u4TcbIndex;
                continue;
            }
            if (GetTCBrport (u4TcbIndex) > GetTCBrport (u4FirstTcbIndex))
            {
                continue;
            }
        }
    }

    if (u1GotOne == FALSE)
    {
        TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                     "Get of first index in FsTcpAoConnTestTable failed.\n");

        return SNMP_FAILURE;
    }

    *pi4FsTcpAoConnTestLclAdrType = GetTCBLipAddType (u4FirstTcbIndex);

    if (*pi4FsTcpAoConnTestLclAdrType == TCP_AF_IPV4_TYPE)
    {
        u4TempLocalTcpAddr = GetTCBv4lip (u4FirstTcbIndex);
        u4TempLocalTcpAddr = OSIX_HTONL (u4TempLocalTcpAddr);
        MEMCPY (pFsTcpAoConnTestLclAdress->pu1_OctetList,
                &u4TempLocalTcpAddr, sizeof (UINT4));
        u4TempRemoteTcpAddr = GetTCBv4rip (u4FirstTcbIndex);
        u4TempRemoteTcpAddr = OSIX_HTONL (u4TempRemoteTcpAddr);
        MEMCPY (pFsTcpAoConnTestRmtAdress->pu1_OctetList,
                &u4TempRemoteTcpAddr, sizeof (UINT4));
    }
    else if (*pi4FsTcpAoConnTestLclAdrType == TCP_AF_IPV6_TYPE)
    {

        MEMCPY (&TcpCurLocalAddr, &GetTCBlip (u4FirstTcbIndex),
                sizeof (tIpAddr));
        MEMCPY (&TcpCurRemoteAddr, &GetTCBrip (u4FirstTcbIndex),
                sizeof (tIpAddr));

        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            TcpCurLocalAddr.u4_addr[i4Count] =
                OSIX_HTONL (TcpCurLocalAddr.u4_addr[i4Count]);
            TcpCurRemoteAddr.u4_addr[i4Count] =
                OSIX_HTONL (TcpCurRemoteAddr.u4_addr[i4Count]);
        }

        MEMCPY ((tIpAddr *) (VOID *)
                pFsTcpAoConnTestLclAdress->pu1_OctetList,
                &TcpCurLocalAddr, sizeof (tIpAddr));
        MEMCPY ((tIpAddr *) (VOID *)
                pFsTcpAoConnTestRmtAdress->pu1_OctetList,
                &TcpCurRemoteAddr, sizeof (tIpAddr));
    }
    if (GetTCBLipAddType (u4FirstTcbIndex) == TCP_AF_IPV6_TYPE)
    {
        pFsTcpAoConnTestLclAdress->i4_Length = sizeof (tIpAddr);
    }
    else
    {
        pFsTcpAoConnTestLclAdress->i4_Length = sizeof (UINT4);
    }
    *pu4FsTcpAoConnTestLclPort = GetTCBlport (u4FirstTcbIndex);
    *pi4FsTcpAoConnTestRmtAdrType = GetTCBRipAddType (u4FirstTcbIndex);
    if (GetTCBRipAddType (u4FirstTcbIndex) == TCP_AF_IPV6_TYPE)
    {
        pFsTcpAoConnTestRmtAdress->i4_Length = sizeof (tIpAddr);
    }
    else
    {
        pFsTcpAoConnTestRmtAdress->i4_Length = sizeof (UINT4);
    }
    *pu4FsTcpAoConnTestRmtPort = GetTCBrport (u4FirstTcbIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsTcpAoConnTestTable
 Input       :  The Indices
                FsTcpAoConnTestLclAdrType
                nextFsTcpAoConnTestLclAdrType
                FsTcpAoConnTestLclAdress
                nextFsTcpAoConnTestLclAdress
                FsTcpAoConnTestLclPort
                nextFsTcpAoConnTestLclPort
                FsTcpAoConnTestRmtAdrType
                nextFsTcpAoConnTestRmtAdrType
                FsTcpAoConnTestRmtAdress
                nextFsTcpAoConnTestRmtAdress
                FsTcpAoConnTestRmtPort
                nextFsTcpAoConnTestRmtPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsTcpAoConnTestTable (INT4 i4FsTcpAoConnTestLclAdrType,
                                     INT4 *pi4NextFsTcpAoConnTestLclAdrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsTcpAoConnTestLclAdress,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextFsTcpAoConnTestLclAdress,
                                     UINT4 u4FsTcpAoConnTestLclPort,
                                     UINT4 *pu4NextFsTcpAoConnTestLclPort,
                                     INT4 i4FsTcpAoConnTestRmtAdrType,
                                     INT4 *pi4NextFsTcpAoConnTestRmtAdrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsTcpAoConnTestRmtAdress,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextFsTcpAoConnTestRmtAdress,
                                     UINT4 u4FsTcpAoConnTestRmtPort,
                                     UINT4 *pu4NextFsTcpAoConnTestRmtPort)
{
    INT4                i4NextLocalAddrType = TCP_ZERO;
    INT4                i4NextRemoteAddrType = TCP_ZERO;
    INT4                i4TcpCurLocalAddrType = TCP_ZERO;
    INT4                i4TcpCurRemoteAddrType = TCP_ZERO;
    INT4                i4Found = TCP_ZERO;
    INT4                i4Count = TCP_ZERO;
    UINT4               u4NextTcpConnLocalPort = TCP_ZERO;
    UINT4               u4NextTcpConnRemPort = TCP_ZERO;
    UINT4               u4TcpCurRemotePort = TCP_ZERO;
    UINT4               u4TcbIndex = TCP_ZERO;
    UINT4               u4TcpCurLocalPort = TCP_ZERO;
    tIpAddr             LocalIP;
    tIpAddr             RemoteIP;
    tIpAddr             TempLocalIP;
    tIpAddr             TempRemoteIP;
    tIpAddr             TcpCurLocalAddr;
    tIpAddr             TcpCurRemoteAddr;
    tIpAddr             NextTcpConnLocalAddress;
    tIpAddr             NextTcpConnRemAddress;

    UINT4               u4TempLocalTcpAddr;
    UINT4               u4TempRemTcpAddr;

    UINT4               u4TcbContext;

    MEMSET (&LocalIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&RemoteIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TempLocalIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TempRemoteIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TcpCurLocalAddr, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TcpCurRemoteAddr, TCP_ZERO, sizeof (tIpAddr));

    if ((pFsTcpAoConnTestLclAdress->i4_Length <= 0) ||
        (pFsTcpAoConnTestRmtAdress->i4_Length <= 0))
    {
        return SNMP_FAILURE;
    }

    if (i4FsTcpAoConnTestLclAdrType == TCP_AF_IPV4_TYPE)
    {
        MEMCPY (&TempLocalIP,
                (tIpAddr *) (VOID *) pFsTcpAoConnTestLclAdress->pu1_OctetList,
                pFsTcpAoConnTestLclAdress->i4_Length);
        TempLocalIP.u4_addr[0] = OSIX_NTOHL (TempLocalIP.u4_addr[0]);
        MEMCPY (&TempRemoteIP,
                (tIpAddr *) (VOID *) pFsTcpAoConnTestRmtAdress->pu1_OctetList,
                pFsTcpAoConnTestRmtAdress->i4_Length);
        TempRemoteIP.u4_addr[0] = OSIX_NTOHL (TempRemoteIP.u4_addr[0]);
    }
    else if (i4FsTcpAoConnTestLclAdrType == TCP_AF_IPV6_TYPE)
    {
        MEMCPY (&TempLocalIP,
                (tIpAddr *) (VOID *) pFsTcpAoConnTestLclAdress->pu1_OctetList,
                pFsTcpAoConnTestLclAdress->i4_Length);
        MEMCPY (&TempRemoteIP,
                (tIpAddr *) (VOID *) pFsTcpAoConnTestRmtAdress->pu1_OctetList,
                pFsTcpAoConnTestRmtAdress->i4_Length);
        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            TempLocalIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempLocalIP.u4_addr[i4Count]);
            TempRemoteIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempRemoteIP.u4_addr[i4Count]);
        }
    }

    for (u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
    {

        if (GetTCBstate (u4TcbIndex) == TCPS_FREE ||
            GetTCBstate (u4TcbIndex) == TCPS_ABORT)
        {
            continue;
        }
        i4TcpCurLocalAddrType = GetTCBLipAddType (u4TcbIndex);
        i4TcpCurRemoteAddrType = GetTCBRipAddType (u4TcbIndex);
        u4TcbContext = GetTCBContext (u4TcbIndex);

        if (u4TcbContext != gpTcpCurrContext->u4ContextId)
        {
            continue;
        }

        if (i4TcpCurLocalAddrType == TCP_AF_IPV4_TYPE)
        {
            u4TempLocalTcpAddr = GetTCBv4lip (u4TcbIndex);
            MEMSET (&TcpCurLocalAddr, TCP_ZERO, sizeof (tIpAddr));
            MEMCPY (&TcpCurLocalAddr, &u4TempLocalTcpAddr, sizeof (UINT4));
            u4TempRemTcpAddr = GetTCBv4rip (u4TcbIndex);
            MEMSET (&TcpCurRemoteAddr, TCP_ZERO, sizeof (tIpAddr));
            MEMCPY (&TcpCurRemoteAddr, &u4TempRemTcpAddr, sizeof (UINT4));
        }
        else if (i4TcpCurLocalAddrType == TCP_AF_IPV6_TYPE)
        {
            MEMSET (&TcpCurLocalAddr, TCP_ZERO, sizeof (tIpAddr));
            MEMCPY (&TcpCurLocalAddr, &GetTCBlip (u4TcbIndex),
                    sizeof (tIpAddr));
            MEMSET (&TcpCurRemoteAddr, TCP_ZERO, sizeof (tIpAddr));
            MEMCPY (&TcpCurRemoteAddr, &GetTCBrip (u4TcbIndex),
                    sizeof (tIpAddr));
        }
        u4TcpCurLocalPort = GetTCBlport (u4TcbIndex);
        u4TcpCurRemotePort = GetTCBrport (u4TcbIndex);

        /*As same application of same address type cannot 
         * use the same port,skip the entry when such entry is found*/
        /*As the Next port is always greater than the current one 
         *skip the entry when the address type of current entry
         *is less than are equal to next entry*/

        if ((u4TcpCurLocalPort <= u4FsTcpAoConnTestLclPort) &&
            (i4TcpCurLocalAddrType <= i4FsTcpAoConnTestLclAdrType) &&
            (MEMCMP (&TcpCurLocalAddr, &TempLocalIP, sizeof (tIpAddr)) <=
             TCP_ZERO) && (u4TcpCurRemotePort <= u4FsTcpAoConnTestRmtPort)
            && (MEMCMP (&TcpCurRemoteAddr, &TempRemoteIP, sizeof (tIpAddr)) <=
                TCP_ZERO))
        {
            continue;
        }
        /*Check whether this entry's indexes are less than the requested
         * indexes. If so, skip this entry*/
        if (i4TcpCurLocalAddrType < i4FsTcpAoConnTestLclAdrType)
        {
            continue;
        }
        if ((i4TcpCurLocalAddrType == i4FsTcpAoConnTestLclAdrType) &&
            (MEMCMP (&TcpCurLocalAddr, &TempLocalIP, sizeof (tIpAddr)) <
             TCP_ZERO))
        {
            continue;
        }
        if ((i4TcpCurLocalAddrType == i4FsTcpAoConnTestLclAdrType) &&
            (MEMCMP (&TcpCurLocalAddr, &TempLocalIP, sizeof (tIpAddr)) ==
             TCP_ZERO) && (u4TcpCurLocalPort < u4FsTcpAoConnTestLclPort))
        {
            continue;
        }
        if ((i4TcpCurLocalAddrType == i4FsTcpAoConnTestLclAdrType) &&
            (MEMCMP (&TcpCurLocalAddr, &TempLocalIP, sizeof (tIpAddr)) ==
             TCP_ZERO) && (u4TcpCurLocalPort == u4FsTcpAoConnTestLclPort)
            && (i4TcpCurRemoteAddrType < i4FsTcpAoConnTestRmtAdrType))
        {
            continue;
        }
        if ((i4TcpCurLocalAddrType == i4FsTcpAoConnTestLclAdrType) &&
            (MEMCMP (&TcpCurLocalAddr, &TempLocalIP, sizeof (tIpAddr)) ==
             TCP_ZERO) && (u4TcpCurLocalPort == u4FsTcpAoConnTestLclPort)
            && (i4TcpCurRemoteAddrType == i4FsTcpAoConnTestRmtAdrType)
            && (MEMCMP (&TcpCurRemoteAddr, &TempRemoteIP, sizeof (tIpAddr)) <
                TCP_ZERO))
        {
            continue;
        }
        if ((i4TcpCurLocalAddrType == i4FsTcpAoConnTestLclAdrType) &&
            (MEMCMP (&TcpCurLocalAddr, &TempLocalIP, sizeof (tIpAddr)) ==
             TCP_ZERO) && (u4TcpCurLocalPort == u4FsTcpAoConnTestLclPort)
            && (i4TcpCurRemoteAddrType == i4FsTcpAoConnTestRmtAdrType)
            && (MEMCMP (&TcpCurRemoteAddr, &TempRemoteIP, sizeof (tIpAddr)) ==
                TCP_ZERO) && (u4TcpCurRemotePort <= u4FsTcpAoConnTestRmtPort))
        {
            /*Last condition in if clause is <=, to avoid returning 
             * the incoming index!*/
            continue;
        }

        /* Now it is sure that current index is greater than the requested 
         * Index. If we didn't identify a getnext so far, we can initialise
         * the getnext value with current index.*/

        if (i4Found == 0)
        {
            i4NextLocalAddrType = i4TcpCurLocalAddrType;
            NextTcpConnLocalAddress = TcpCurLocalAddr;
            u4NextTcpConnLocalPort = u4TcpCurLocalPort;
            i4NextRemoteAddrType = i4TcpCurRemoteAddrType;
            NextTcpConnRemAddress = TcpCurRemoteAddr;
            u4NextTcpConnRemPort = u4TcpCurRemotePort;
            i4Found = 1;
            continue;
        }

        /*Check whether this entry is greater than the currently 
         * identified getnext index. If so, skip this entry*/
        if (i4TcpCurLocalAddrType > i4NextLocalAddrType)
        {
            continue;
        }
        if ((i4TcpCurLocalAddrType == i4NextLocalAddrType) &&
            (MEMCMP (&TcpCurLocalAddr,
                     &NextTcpConnLocalAddress, sizeof (tIpAddr)) > TCP_ZERO))
        {
            continue;
        }
        if ((i4TcpCurLocalAddrType == i4NextLocalAddrType) &&
            (MEMCMP (&TcpCurLocalAddr,
                     &NextTcpConnLocalAddress, sizeof (tIpAddr)) == TCP_ZERO) &&
            (u4TcpCurLocalPort > u4NextTcpConnLocalPort))
        {
            continue;
        }
        if ((i4TcpCurLocalAddrType == i4NextLocalAddrType) &&
            (MEMCMP (&TcpCurLocalAddr,
                     &NextTcpConnLocalAddress, sizeof (tIpAddr)) == TCP_ZERO) &&
            (u4TcpCurLocalPort == u4NextTcpConnLocalPort) &&
            (i4TcpCurRemoteAddrType > i4NextRemoteAddrType))
        {
            continue;
        }

        if ((i4TcpCurLocalAddrType == i4NextLocalAddrType) &&
            (MEMCMP (&TcpCurLocalAddr,
                     &NextTcpConnLocalAddress, sizeof (tIpAddr)) == TCP_ZERO) &&
            (u4TcpCurLocalPort == u4NextTcpConnLocalPort) &&
            (i4TcpCurRemoteAddrType == i4NextRemoteAddrType) &&
            (MEMCMP (&TcpCurRemoteAddr,
                     &NextTcpConnRemAddress, sizeof (tIpAddr)) > TCP_ZERO))
        {
            continue;
        }
        if ((i4TcpCurLocalAddrType == i4NextLocalAddrType) &&
            (MEMCMP (&TcpCurLocalAddr,
                     &NextTcpConnLocalAddress, sizeof (tIpAddr)) == TCP_ZERO) &&
            (u4TcpCurLocalPort == u4NextTcpConnLocalPort) &&
            (i4TcpCurRemoteAddrType == i4NextRemoteAddrType) &&
            (MEMCMP (&TcpCurRemoteAddr,
                     &NextTcpConnRemAddress, sizeof (tIpAddr)) == TCP_ZERO) &&
            (u4TcpCurRemotePort > u4NextTcpConnRemPort))
        {
            continue;
        }
        /*Now it is sure that Current index is not greater than the
         * present get next value. So we need to take the new value 
         * for get next :-)*/
        i4NextLocalAddrType = i4TcpCurLocalAddrType;
        NextTcpConnLocalAddress = TcpCurLocalAddr;
        u4NextTcpConnLocalPort = u4TcpCurLocalPort;
        i4NextRemoteAddrType = i4TcpCurRemoteAddrType;
        NextTcpConnRemAddress = TcpCurRemoteAddr;
        u4NextTcpConnRemPort = u4TcpCurRemotePort;
    }
    if (i4Found == 0)
    {
        TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                     "Getnext of index returned failure.\n");
        return SNMP_FAILURE;
    }

    *pi4NextFsTcpAoConnTestLclAdrType = i4NextLocalAddrType;
    if (i4NextLocalAddrType == TCP_AF_IPV4_TYPE)
    {
        MEMCPY (&u4TempLocalTcpAddr, &NextTcpConnLocalAddress, sizeof (UINT4));
        u4TempLocalTcpAddr = OSIX_HTONL (u4TempLocalTcpAddr);
        MEMCPY (&NextTcpConnLocalAddress, &u4TempLocalTcpAddr, sizeof (UINT4));
        MEMCPY (&u4TempRemTcpAddr, &NextTcpConnRemAddress, sizeof (UINT4));
        u4TempRemTcpAddr = OSIX_HTONL (u4TempRemTcpAddr);
        MEMCPY (&NextTcpConnRemAddress, &u4TempRemTcpAddr, sizeof (UINT4));

    }
    else if (i4NextLocalAddrType == TCP_AF_IPV6_TYPE)
    {
        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            NextTcpConnLocalAddress.u4_addr[i4Count] =
                OSIX_HTONL (NextTcpConnLocalAddress.u4_addr[i4Count]);
            NextTcpConnRemAddress.u4_addr[i4Count] =
                OSIX_HTONL (NextTcpConnRemAddress.u4_addr[i4Count]);
        }
    }
    MEMCPY ((tIpAddr *) (VOID *)
            pNextFsTcpAoConnTestLclAdress->pu1_OctetList,
            &NextTcpConnLocalAddress, sizeof (tIpAddr));
    if (i4NextLocalAddrType == TCP_AF_IPV6_TYPE)
    {
        pNextFsTcpAoConnTestLclAdress->i4_Length = sizeof (tIpAddr);
    }
    else
    {
        pNextFsTcpAoConnTestLclAdress->i4_Length = sizeof (UINT4);
    }
    *pu4NextFsTcpAoConnTestLclPort = u4NextTcpConnLocalPort;
    *pi4NextFsTcpAoConnTestRmtAdrType = i4NextRemoteAddrType;

    MEMCPY ((tIpAddr *) (VOID *)
            pNextFsTcpAoConnTestRmtAdress->pu1_OctetList,
            &NextTcpConnRemAddress, sizeof (tIpAddr));
    if (i4NextRemoteAddrType == TCP_AF_IPV6_TYPE)
    {
        pNextFsTcpAoConnTestRmtAdress->i4_Length = sizeof (tIpAddr);
    }
    else
    {
        pNextFsTcpAoConnTestRmtAdress->i4_Length = sizeof (UINT4);
    }
    *pu4NextFsTcpAoConnTestRmtPort = u4NextTcpConnRemPort;

    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "GetNext FsTcpAoConnTestTable returned\n");

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTcpConTcpAOIcmpIgnCtr
 Input       :  The Indices
                FsTcpAoConnTestLclAdrType
                FsTcpAoConnTestLclAdress
                FsTcpAoConnTestLclPort
                FsTcpAoConnTestRmtAdrType
                FsTcpAoConnTestRmtAdress
                FsTcpAoConnTestRmtPort

                The Object 
                retValFsTcpConTcpAOIcmpIgnCtr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTcpConTcpAOIcmpIgnCtr (INT4 i4FsTcpAoConnTestLclAdrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsTcpAoConnTestLclAdress,
                               UINT4 u4FsTcpAoConnTestLclPort,
                               INT4 i4FsTcpAoConnTestRmtAdrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsTcpAoConnTestRmtAdress,
                               UINT4 u4FsTcpAoConnTestRmtPort,
                               UINT4 *pu4RetValFsTcpConTcpAOIcmpIgnCtr)
{
    INT4                i4TcbIndex = TCP_ZERO;
    INT4                i4Count = TCP_ZERO;
    UINT4               u4Context;
    tIpAddr             TempLocalIP;
    tIpAddr             TempRemoteIP;

    tSNMP_OCTET_STRING_TYPE TcpCurrConnLocalAddress;
    tSNMP_OCTET_STRING_TYPE TcpCurrConnRemAddress;
    UINT1               au1CurrLocalIp[IPVX_MAX_INET_ADR_LEN];
    UINT1               au1CurrRemoteIp[IPVX_MAX_INET_ADR_LEN];

    MEMSET (&TempLocalIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TempRemoteIP, TCP_ZERO, sizeof (tIpAddr));

    MEMSET (&TcpCurrConnLocalAddress, TCP_ZERO,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TcpCurrConnRemAddress, TCP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1CurrLocalIp, TCP_ZERO, IPVX_MAX_INET_ADR_LEN);
    MEMSET (au1CurrRemoteIp, TCP_ZERO, IPVX_MAX_INET_ADR_LEN);

    TcpCurrConnLocalAddress.pu1_OctetList = au1CurrLocalIp;
    TcpCurrConnRemAddress.pu1_OctetList = au1CurrRemoteIp;

    UNUSED_PARAM (i4FsTcpAoConnTestRmtAdrType);
    if (pFsTcpAoConnTestLclAdress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempLocalIP,
                (tIpAddr *) (VOID *) pFsTcpAoConnTestLclAdress->pu1_OctetList,
                pFsTcpAoConnTestLclAdress->i4_Length);
    }
    if (pFsTcpAoConnTestRmtAdress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempRemoteIP,
                (tIpAddr *) (VOID *) pFsTcpAoConnTestRmtAdress->pu1_OctetList,
                pFsTcpAoConnTestRmtAdress->i4_Length);
    }
    if (i4FsTcpAoConnTestLclAdrType == TCP_ONE)
    {
        TempLocalIP.u4_addr[0] = OSIX_NTOHL (TempLocalIP.u4_addr[0]);
        TempRemoteIP.u4_addr[0] = OSIX_NTOHL (TempRemoteIP.u4_addr[0]);
    }
    else if (i4FsTcpAoConnTestLclAdrType == TCP_TWO)
    {
        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            TempLocalIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempLocalIP.u4_addr[i4Count]);
            TempRemoteIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempRemoteIP.u4_addr[i4Count]);
        }
    }

    MEMCPY (TcpCurrConnRemAddress.pu1_OctetList, &TempRemoteIP,
            pFsTcpAoConnTestRmtAdress->i4_Length);
    MEMCPY (TcpCurrConnLocalAddress.pu1_OctetList, &TempLocalIP,
            pFsTcpAoConnTestLclAdress->i4_Length);

    TcpCurrConnRemAddress.i4_Length = pFsTcpAoConnTestRmtAdress->i4_Length;
    TcpCurrConnLocalAddress.i4_Length = pFsTcpAoConnTestLclAdress->i4_Length;

    u4Context = gpTcpCurrContext->u4ContextId;

    i4TcbIndex = tcpGetConnTableID (u4Context,
                                    i4FsTcpAoConnTestLclAdrType,
                                    &TcpCurrConnLocalAddress,
                                    u4FsTcpAoConnTestLclPort,
                                    &TcpCurrConnRemAddress,
                                    u4FsTcpAoConnTestRmtPort);

    if (i4TcbIndex == TCP_NOT_OK)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsTcpConTcpAOIcmpIgnCtr = (GetIcmpIgnCtr (i4TcbIndex));

    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of fsTcpConTcpAOIcmpIgnCtr returned %d.\n",
                 *pu4RetValFsTcpConTcpAOIcmpIgnCtr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTcpConTcpAOSilentAccptCtr
 Input       :  The Indices
                FsTcpAoConnTestLclAdrType
                FsTcpAoConnTestLclAdress
                FsTcpAoConnTestLclPort
                FsTcpAoConnTestRmtAdrType
                FsTcpAoConnTestRmtAdress
                FsTcpAoConnTestRmtPort

                The Object 
                retValFsTcpConTcpAOSilentAccptCtr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTcpConTcpAOSilentAccptCtr (INT4 i4FsTcpAoConnTestLclAdrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsTcpAoConnTestLclAdress,
                                   UINT4 u4FsTcpAoConnTestLclPort,
                                   INT4 i4FsTcpAoConnTestRmtAdrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsTcpAoConnTestRmtAdress,
                                   UINT4 u4FsTcpAoConnTestRmtPort,
                                   UINT4 *pu4RetValFsTcpConTcpAOSilentAccptCtr)
{
    INT4                i4TcbIndex = TCP_ZERO;
    INT4                i4Count = TCP_ZERO;
    UINT4               u4Context;
    tIpAddr             TempLocalIP;
    tIpAddr             TempRemoteIP;

    tSNMP_OCTET_STRING_TYPE TcpCurrConnLocalAddress;
    tSNMP_OCTET_STRING_TYPE TcpCurrConnRemAddress;
    UINT1               au1CurrLocalIp[IPVX_MAX_INET_ADR_LEN];
    UINT1               au1CurrRemoteIp[IPVX_MAX_INET_ADR_LEN];

    MEMSET (&TempLocalIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TempRemoteIP, TCP_ZERO, sizeof (tIpAddr));

    MEMSET (&TcpCurrConnLocalAddress, TCP_ZERO,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TcpCurrConnRemAddress, TCP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1CurrLocalIp, TCP_ZERO, IPVX_MAX_INET_ADR_LEN);
    MEMSET (au1CurrRemoteIp, TCP_ZERO, IPVX_MAX_INET_ADR_LEN);

    TcpCurrConnLocalAddress.pu1_OctetList = au1CurrLocalIp;
    TcpCurrConnRemAddress.pu1_OctetList = au1CurrRemoteIp;

    UNUSED_PARAM (i4FsTcpAoConnTestRmtAdrType);
    if (pFsTcpAoConnTestLclAdress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempLocalIP,
                (tIpAddr *) (VOID *) pFsTcpAoConnTestLclAdress->pu1_OctetList,
                pFsTcpAoConnTestLclAdress->i4_Length);
    }
    if (pFsTcpAoConnTestRmtAdress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempRemoteIP,
                (tIpAddr *) (VOID *) pFsTcpAoConnTestRmtAdress->pu1_OctetList,
                pFsTcpAoConnTestRmtAdress->i4_Length);
    }
    if (i4FsTcpAoConnTestLclAdrType == TCP_ONE)
    {
        TempLocalIP.u4_addr[0] = OSIX_NTOHL (TempLocalIP.u4_addr[0]);
        TempRemoteIP.u4_addr[0] = OSIX_NTOHL (TempRemoteIP.u4_addr[0]);
    }
    else if (i4FsTcpAoConnTestLclAdrType == TCP_TWO)
    {
        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            TempLocalIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempLocalIP.u4_addr[i4Count]);
            TempRemoteIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempRemoteIP.u4_addr[i4Count]);
        }
    }

    MEMCPY (TcpCurrConnRemAddress.pu1_OctetList, &TempRemoteIP,
            pFsTcpAoConnTestRmtAdress->i4_Length);
    MEMCPY (TcpCurrConnLocalAddress.pu1_OctetList, &TempLocalIP,
            pFsTcpAoConnTestLclAdress->i4_Length);

    TcpCurrConnRemAddress.i4_Length = pFsTcpAoConnTestRmtAdress->i4_Length;
    TcpCurrConnLocalAddress.i4_Length = pFsTcpAoConnTestLclAdress->i4_Length;

    u4Context = gpTcpCurrContext->u4ContextId;

    i4TcbIndex = tcpGetConnTableID (u4Context,
                                    i4FsTcpAoConnTestLclAdrType,
                                    &TcpCurrConnLocalAddress,
                                    u4FsTcpAoConnTestLclPort,
                                    &TcpCurrConnRemAddress,
                                    u4FsTcpAoConnTestRmtPort);

    if (i4TcbIndex == TCP_NOT_OK)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsTcpConTcpAOSilentAccptCtr = (GetSilentAcceptCtr (i4TcbIndex));

    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "Get of fsTcpConTcpAOSilentAccptCtr returned %d.\n",
                 *pu4RetValFsTcpConTcpAOSilentAccptCtr);
    return SNMP_SUCCESS;
}
