/*##########################################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved                   #
# ----------------------------------------------------                   #
# $Id: tcptimer.c,v 1.14 2016/05/06 10:10:58 siva Exp $                    #
##########################################################################
*/
/* SOURCE FILE HEADER
 *
 * ----------------------------------------------------------------------------
 *  FILE NAME             : tcptimer.c
 * 
 *  PRINCIPAL AUTHOR      : Ramesh Kumar
 *
 *  SUBSYSTEM NAME        : TCP
 *
 *  MODULE NAME           : Timer Module
 *
 *  LANGUAGE              : C
 *
 *  TARGET ENVIRONMENT    : Any
 *
 *  DATE OF FIRST RELEASE :
 * 
 *  DESCRIPTION           : This file contains the function that are used
 *                          for timer handling.
 *
 * ---------------------------------------------------------------------------
 *
 *
 *  CHANGE RECORD :
 *
 * ---------------------------------------------------------------------------
 *   VERSION        AUTHOR/DATE           DESCRIPTION OF CHANGE
 * ---------------------------------------------------------------------------
 *     1.7          Rebecca Rufus         hliReplyForClose invoked at the
 *                  16-Sep-'98.           expiry of the 2MSL timer.Refer 
 *                                        Problem Id 7.                  
 *                                        TCB deallocated after the expiry of 
 *                                        the 2MSL timer.Refer Problem Id 12.
 *     1.8          Ramakrishnan          Added keepalive timer functionality.
 *                  05-Jun-'00.           Ported to FSAP2.               
 *     1.9          Ramakrishnan          The hliReplyForClose is compiled
 *                  20-Aug-'00.           out in the 2MSL timer expiry
 *                                        handler as reply is sent for close
 *                                        after the first FIN is sent.
 * ---------------------------------------------------------------------------
 */

#include "tcpinc.h"

/*************************************************************************/
/*  Function Name   : TcpTmrHandleExpiry                                 */
/*  Input(s)        : None.                                              */
/*  Output(s)       : None.                                              */
/*  Returns         : None                                               */
/*  Description     : Calls the routins for the timer expired            */
/*  CallingCondition: When a timer expiry event occurs in the main       */
/*                    loop. Called only from tcpMAINtask.                */
/* GLOBAL VARIABLES AFFECTED : timer_list, TCBtable                      */
/*************************************************************************/

void
TcpTmrHandleExpiry (void)
{
    tTmrAppTimer       *pExp;

    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TcpTmrHandleExpiry. \n");
    pExp = TmrGetNextExpiredTimer (gTcpTimerListId);

    while (pExp != ZERO)
    {
        switch (pExp->u4Data)
        {
            case TCP_SO_LINGER_TIMER:
            {
                TmrLingerTimerExpiryHandler ((tTcpTimer *) pExp);
                break;
            }
            case TCP_RETRANSMIT_TIMER:
            {
                TmrRexmtExpiryHandler ((tTcpTimer *) pExp);
                break;
            }
            case TCP_2MSL_TIMER:
            {
                Tmr2MSLExpiryHandler ((tTcpTimer *) pExp);
                break;
            }
            case TCP_PERSIST_TIMER:
            {
                TmrPersistExpiryHandler ((tTcpTimer *) pExp);
                break;
            }
            case TCP_USER_TIMEOUT_TIMER:
            {
                TmrUserTmOutExpiryHandler ((tTcpTimer *) pExp);
                break;
            }
            case TCP_KEEPALIVE_TIMER:
            {
                TmrKaTimerExpiryHandler ((tTcpTimer *) pExp);
                break;
            }
            case TCP_FINWAIT2_RECOVERY_TIMER:
            {
                TmrFin2RecTimerExpiryHandler ((tTcpTimer *) pExp);
                break;
            }
        }
        pExp = TmrGetNextExpiredTimer (gTcpTimerListId);
    }
    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from TcpTmrHandleExpiry.\n");
}

/*************************************************************************/
/*  Function Name   : TmrInitTimer                                       */
/*  Input(s)        : pTimer - Pointer to the timer to be initialised    */
/*                    u1TimerId - Timer Identifier                       */
/*                    u4TcbIndex - Index to the Tcb table                */
/*  Output(s)       : None.                                              */
/*  Returns         : None.                                              */
/*  Description     : Initialises the timer                              */
/*  CallingCondition:  Whenever a timer is to initilised                 */
/*  GLOBAL VARIABLES AFFECTED : timer_list, TCBtable                     */
/*************************************************************************/
void
TmrInitTimer (tTcpTimer * pTimer, UINT1 u1TimerId, UINT4 u4TcbIndex)
{
    UINT4               u4Context;

    u4Context = GetTCBContext (u4TcbIndex);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TmrInitTimer for CONN ID: %d.\n", u4TcbIndex);
    if (TmrIsActiveTimer (pTimer))
    {
        TmrStopTimer (gTcpTimerListId, &pTimer->node);
    }
    pTimer->node.u4Data = (UINT4) u1TimerId;
    pTimer->u1Active = INACTIVE;
    pTimer->u4TcbIndex = u4TcbIndex;
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from TmrInitTimer.\n");
}

/*************************************************************************/
/*  Function Name   : TcpTmrSetTimer                                     */
/*  Input(s)        : pTimer - Poniter to the timer structure            */
/*                    i4Ticks - No of time ticks to set the timer to     */
/*  Output(s)       : None.                                              */
/*  Returns         : None.                                              */
/*  Description     : Sets the No of time ticks                          */
/*  CallingCondition:                                                    */
/*  GLOBAL VARIABLES AFFECTED : timer_list, TCBtable                     */
/*************************************************************************/
void
TcpTmrSetTimer (tTcpTimer * pTimer, UINT4 u4Ticks)
{
    UINT4               u4Context;

    u4Context = GetTCBContext (pTimer->u4TcbIndex);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TcpTmrSetTimer for CONN ID : %d. \n",
                 pTimer->u4TcbIndex);
    if ((TmrStartTimer (gTcpTimerListId, &pTimer->node, u4Ticks)) ==
        TMR_SUCCESS)
    {
        pTimer->u1Active = ACTIVE;
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from TcpTmrSetTimer. \n");
}

/*************************************************************************/
/*  Function Name   : TcpTmrDeleteTimer                                  */
/*  Input(s)        : pTimer - Pointer to the timer structure            */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS                                            */
/*  Description     : Deletes a timer from the tcp timer list            */
/*  CallingCondition:                                                    */
/*  GLOBAL VARIABLES AFFECTED : timer_list, TCBtable                     */
/*************************************************************************/
INT4
TcpTmrDeleteTimer (tTcpTimer * pTimer)
{
    UINT4               u4Context;

    u4Context = GetTCBContext (pTimer->u4TcbIndex);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TcpTmrDeleteTimer for CONN ID: %d.\n",
                 pTimer->u4TcbIndex);

    TmrStopTimer (gTcpTimerListId, &pTimer->node);
    pTimer->u1Active = INACTIVE;
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from TcpTmrDeleteTimer. Returned.\n");
    return ZERO;
}

/*************************************************************************/
/*  Function Name   : TcpTmrRestartTimer                                 */
/*  Input(s)        : pTimer - Pointer to the timer structure            */
/*                    u4Ticks - No of time ticks                         */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK                                             */
/*  Description     : Restarts the the timer for given no of time ticks  */
/*  CallingCondition:                                                    */
/*  GLOBAL VARIABLES AFFECTED : timer_list, TCBtable                     */
/*************************************************************************/
INT4
TcpTmrRestartTimer (tTcpTimer * pTimer, UINT4 u4Ticks)
{
    UINT4               u4Context;

    u4Context = GetTCBContext (pTimer->u4TcbIndex);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TcpTmrRestartTimer for CONN ID: %d. \n",
                 pTimer->u4TcbIndex);
    if (TmrIsActiveTimer (pTimer) == TRUE)
        TmrStopTimer (gTcpTimerListId, &pTimer->node);
    if ((TmrStartTimer (gTcpTimerListId, &pTimer->node, (u4Ticks))) ==
        TMR_SUCCESS)
    {
        pTimer->u1Active = ACTIVE;
        return TCP_NOT_OK;
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from TcpTmrRestartTimer. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : Tmr2MSLExpiryHandler                               */
/*  Input(s)        : pExpdTimer - Pointer to the timer structure        */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK                                             */
/*  Description     : Deallocates the TCB when 2MSL timer has expired    */
/*  CallingCondition: When 2MSL timer has expired                        */
/*  GLOBAL VARIABLES AFFECTED : TCBtable                                 */
/*************************************************************************/
INT1
Tmr2MSLExpiryHandler (tTcpTimer * pExpdTimer)
{
    UINT4               u4Context;

    u4Context = GetTCBContext (pExpdTimer->u4TcbIndex);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_OS_RES_TRC, "TCP",
                 "Entering Tmr2MSLExpiryHandler for CONN ID: %d.\n",
                 pExpdTimer->u4TcbIndex);
    if ((TakeTCBmainsem (pExpdTimer->u4TcbIndex)) == OSIX_SUCCESS)
    {
        pExpdTimer->u1Active = INACTIVE;
        deallocate (&TCBtable[pExpdTimer->u4TcbIndex]);
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from Tmr2MSLExpiryHandler. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : TmrUserTmOutExpiryHandler                          */
/*  Input(s)        : pExpdTimer - Pointer to the timer structure        */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK                                             */
/*  Description     : Deallocates the TCB                                */
/*  CallingCondition: When User timeout timer expires                    */
/*  GLOBAL VARIABLES AFFECTED : TCBtable                                 */
/*************************************************************************/
INT1
TmrUserTmOutExpiryHandler (tTcpTimer * pExpdTimer)
{
    UINT4               u4Context;

    u4Context = GetTCBContext (pExpdTimer->u4TcbIndex);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_OS_RES_TRC, "TCP",
                 "Entering from TmrUserTmOutExpiryHandler for CONN ID: %d.\n",
                 pExpdTimer->u4TcbIndex);
    if ((TakeTCBmainsem (pExpdTimer->u4TcbIndex)) == OSIX_SUCCESS)
    {
        pExpdTimer->u1Active = INACTIVE;
        deallocate (&TCBtable[pExpdTimer->u4TcbIndex]);
    }

    /* We can safely leave this out as we shall assume that the
     * the application has done a close
     */
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from TmrUserTmOutExpiryHandler. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : TmrRexmtExpiryHandler                              */
/*  Input(s)        : pExpdTimer - Pointer to the timer structure        */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK                                             */
/*  Description     : Retransmitts a segment                             */
/*  CallingCondition: When Retrasmission timer expires.                  */
/*  GLOBAL VARIABLES AFFECTED : TCBtable                                 */
/*************************************************************************/
INT1
TmrRexmtExpiryHandler (tTcpTimer * pExpdTimer)
{
    UINT4               u4Context;

    u4Context = GetTCBContext (pExpdTimer->u4TcbIndex);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_OS_RES_TRC, "TCP",
                 "Entering from TmrRexmtExpiryHandler for CONN ID: %d.\n",
                 pExpdTimer->u4TcbIndex);
    if ((TakeTCBmainsem (pExpdTimer->u4TcbIndex)) == OSIX_SUCCESS)
    {
    pExpdTimer->u1Active = INACTIVE;
    if (GetTCBstate (pExpdTimer->u4TcbIndex) == TCPS_ABORT)
    {
        GiveTCBmainsem (pExpdTimer->u4TcbIndex);
        return TCP_OK;
    }
    osm_switch[GetTCBostate (pExpdTimer->u4TcbIndex)] (pExpdTimer->u4TcbIndex,
                                                       TCPE_RETRANSMIT);
    GiveTCBmainsem (pExpdTimer->u4TcbIndex);
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from TmrRexmtExpiryHandler. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : TmrPersistExpiryHandler                            */
/*  Input(s)        : pExpdTimer - Ponier to the timer structure         */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK                                             */
/*  Description     : calls the OSM,where the persist timeout is doubled */
/*  CallingCondition: When OSM persist timer expires                     */
/*  GLOBAL VARIABLES AFFECTED : TCBtable                                 */
/*************************************************************************/
INT1
TmrPersistExpiryHandler (tTcpTimer * pExpdTimer)
{
    UINT4               u4Context;

    u4Context = GetTCBContext (pExpdTimer->u4TcbIndex);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_OS_RES_TRC, "TCP",
                 "Entering TmrPersistExpiryHandler for CONN ID: %d.\n",
                 pExpdTimer->u4TcbIndex);
    pExpdTimer->u1Active = INACTIVE;
    if ((TakeTCBmainsem (pExpdTimer->u4TcbIndex)) == OSIX_SUCCESS)
    {
        osm_switch[GetTCBostate (pExpdTimer->u4TcbIndex)] (pExpdTimer->
                                                           u4TcbIndex,
                                                           TCPE_PERSIST);
        GiveTCBmainsem (pExpdTimer->u4TcbIndex);
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from TmrPersistExpiryHandler. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : TmrFin2RecTimerExpiryHandler                       */
/*  Input(s)        : pExpdTimer - Pointer to the timer structure        */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK                                             */
/*  Description     : Deallocates the TCB                                */
/*  CallingCondition: When FIN_WAIT2 recovery timeout timer expires      */
/*  GLOBAL VARIABLES AFFECTED : TCBtable                                 */
/*************************************************************************/
INT1
TmrFin2RecTimerExpiryHandler (tTcpTimer * pExpdTimer)
{
    UINT4               u4Context;

    u4Context = GetTCBContext (pExpdTimer->u4TcbIndex);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_OS_RES_TRC, "TCP",
                 "Entering from TmrFin2RecTimerExpiryHandler for CONN ID: %d.\n",
                 pExpdTimer->u4TcbIndex);
    if ((TakeTCBmainsem (pExpdTimer->u4TcbIndex)) == OSIX_SUCCESS)
    {
        pExpdTimer->u1Active = INACTIVE;
        deallocate (&TCBtable[pExpdTimer->u4TcbIndex]);
    }

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from TmrFin2RecTimerExpiryHandler. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : TmrKaTimerExpiryHandler                            */
/*  Input(s)        : pExpdTimer - Pointer to the timer structure        */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK/ERR                                         */
/*  Description     : Sends either the KA or RESET depeding on the       */
/*                    KA over count                                      */
/*  CallingCondition: When the KA main or retransmission timer expires   */
/*  LOBAL VARIABLES AFFECTED : TCBtable                                  */
/*************************************************************************/
INT1
TmrKaTimerExpiryHandler (tTcpTimer * pExpdTimer)
{
    UINT4               u4Context;
    UINT4               u4Temp;

    u4Context = GetTCBContext (pExpdTimer->u4TcbIndex);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TmrKaTimerExpiryHandler for CONN ID: %d. \n",
                 pExpdTimer->u4TcbIndex);
    pExpdTimer->u1Active = INACTIVE;
    if ((TakeTCBmainsem (pExpdTimer->u4TcbIndex)) != OSIX_SUCCESS)
    {
        return TCP_NOT_OK;
    }
    if ((GetTCBKaRetransCnt (pExpdTimer->u4TcbIndex) == ZERO) ||
        ((GetTCBKaRetransOverCnt (pExpdTimer->u4TcbIndex) - 1) <
         GetTCBKaRetransCnt (pExpdTimer->u4TcbIndex)))
    {
        OsmSendKaPkt (pExpdTimer->u4TcbIndex);
    }
    else
    {
        SetTCBvalopts (pExpdTimer->u4TcbIndex,
                       GetTCBvalopts (pExpdTimer->
                                      u4TcbIndex) & ~TCPO_KEEPALIVE);
        OsmResetConn (pExpdTimer->u4TcbIndex);
        SetTCBflags (pExpdTimer->u4TcbIndex, TCBF_RDONE | TCBF_SDONE);
        SetTCBerror (pExpdTimer->u4TcbIndex, TCPE_KA_TIMEDOUT);
        SetTCBstate (pExpdTimer->u4TcbIndex, TCPS_ABORT);
        TCP_MOD_TRC (u4Context, TCP_FSM_TRC, "TCP",
                     "FSM state set to ABORT for CONN ID - %d\n",
                     pExpdTimer->u4TcbIndex);

        u4Temp = GetTCBvalopts (pExpdTimer->u4TcbIndex);
        SetTCBvalopts (pExpdTimer->u4TcbIndex, TCPO_ASY_REPORT);
        /* Force sending report */
        HliSendAsyMsg (pExpdTimer->u4TcbIndex,
                       (UINT1) TCPE_KA_TIMEDOUT, (UINT1) ZERO, (UINT1) ZERO);
        SetTCBvalopts (pExpdTimer->u4TcbIndex, u4Temp);

        /* Start Timer for aborting the connection if the user
         * doesn't respond with a explicit CLOSE operation within
         * the timeout.                                        */
        TmrStartUsertimeout (pExpdTimer->u4TcbIndex);
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from TmrKaTimerExpiryHandler. Returned OK.\n");
    GiveTCBmainsem (pExpdTimer->u4TcbIndex);
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : TmrLingerTimerExpiryHandler                        */
/*  Input(s)        : pExpdTimer - Pointer to the timer structure        */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK/ERR                                         */
/*  Description     : Unblocks the Application in Hliclose               */
/*  CallingCondition: When the linger timer expires                      */
/*  GLOBAL VARIABLES AFFECTED : None                                     */
/*************************************************************************/
INT1
TmrLingerTimerExpiryHandler (tTcpTimer * pExpdTimer)
{
    UINT4               u4Context;

    u4Context = GetTCBContext (pExpdTimer->u4TcbIndex);
/* The Application would have been blocked in HliClose * Unblock it, and proceed with the closing of the socket */

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TmrLingerTimerExpiryHandler for CONN ID: %d.\n",
                 pExpdTimer->u4TcbIndex);
    pExpdTimer->u1Active = INACTIVE;
    GiveTCBBlockSem (pExpdTimer->u4TcbIndex);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from TmrLingerTimerExpiryHandler. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : TmrKillTimers                                      */
/*  Input(s)        : ptcb - Pointer to TCB                              */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK                                             */
/*  Description     : Deletes all the timers of the TCB                  */
/*  CallingCondition: Called from FsmStartWaitTmr() and deallocate()     */
/*  GLOBAL VARIABLES AFFECTED : timer_list, TCBtable                     */
/*************************************************************************/
INT1
TmrKillTimers (tTcb * ptcb)
{
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4TcbIndex = (UINT4) GET_TCB_INDEX (ptcb);
    u4Context = GetTCBContext (u4TcbIndex);
    /*Kill all timers */
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TmrKillTimers for CONN ID: %d.\n",
                 GET_TCB_INDEX (ptcb));
    TcpTmrDeleteTimer (&ptcb->TcbPersistTmr);
    TcpTmrDeleteTimer (&ptcb->TcbRexmtTmr);
    TcpTmrDeleteTimer (&ptcb->Tcb2mslTmr);
    TcpTmrDeleteTimer (&ptcb->TcbUserTimeout);
    TcpTmrDeleteTimer (&ptcb->TcbLingerTimer);
    TcpTmrDeleteTimer (&ptcb->TcbKaTimer);
    TcpTmrDeleteTimer (&ptcb->TcbFinWait2RecTimer);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from TmrKillTimers. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : TmrStartUsertimeout                                */
/*  Input(s)        : u4Index - Index to the TCB table                   */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK                                             */
/*  Description     : Starts the timeout after which the connection      */
/*                    will automatically be deallocated.  The user is    */
/*                    expected to do an explicit close on the connection.*/
/*                    However he can still learn about the connection    */
/*                    status before closing the connection.              */
/*  CallingCondition: Called FsmAbortConn().                             */
/*  GLOBAL VARIABLES AFFECTED : timer_list, TCBtable                     */
/*************************************************************************/
INT1
TmrStartUsertimeout (UINT4 u4Index)
{
    tTcb               *ptcb;
    UINT4               u4Context;

    u4Context = GetTCBContext (u4Index);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_OS_RES_TRC, "TCP",
                 "Entering TmrStartUsertimeout for CONN ID: %d.\n", u4Index);
    ptcb = &TCBtable[u4Index];
    TmrKillTimers (ptcb);
    TmrInitTimer (&ptcb->TcbUserTimeout, TCP_USER_TIMEOUT_TIMER, u4Index);
    TcpTmrSetTimer (&ptcb->TcbUserTimeout, TCP_USER_TIMEOUT_TICKS);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from TmrStartUsertimeout. Returned 1\n");
    return (TCP_ONE);
}

/*************************************************************************/
/*  Function Name   : TmrStartFinWait2Rectimeout                         */
/*  Input(s)        : u4Index - Index to the TCB table                   */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK                                             */
/*  Description     : Starts the timeout after which the connection      */
/*                    will automatically be deallocated.                 */
/*                    This timer is used to recover the TCB from         */
/*                    FIN_WAIT2 state if the FIN message is not received */
/*                    from the peer within FIN_WAIT2 recovery time.      */
/*  CallingCondition: Called FsmFin1().                                  */
/*  GLOBAL VARIABLES AFFECTED : timer_list, TCBtable                     */
/*************************************************************************/
INT1
TmrStartFinWait2Rectimeout (UINT4 u4Index)
{
    tTcb               *ptcb;
    UINT4               u4Context;

    u4Context = GetTCBContext (u4Index);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_OS_RES_TRC, "TCP",
                 "Entering TmrStartFinWait2Rectimeout for CONN ID: %d.\n",
                 u4Index);
    ptcb = &TCBtable[u4Index];
    TmrInitTimer (&ptcb->TcbFinWait2RecTimer, TCP_FINWAIT2_RECOVERY_TIMER,
                  u4Index);
    TcpTmrSetTimer (&ptcb->TcbFinWait2RecTimer, TCP_FINWAIT2_RECOVERY_TIMEOUT);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from TmrStartFinWait2Rectimeout. Returned 1\n");
    return (TCP_ONE);
}

/*************************************************************************/
/*  Function Name   : TmrIsActiveTimer                                   */
/*  Input(s)        : pTimer - Pointer to the timer structure            */
/*  Output(s)       : None                                               */
/*  Returns         : TRUE / FALSE                                       */
/*  Description     : Check if the given timer is in the timer list      */
/*  CallingCondition:  Before Initating & starting timer                 */
/*  GLOBAL VARIABLES AFFECTED : None                                     */
/*************************************************************************/
INT4
TmrIsActiveTimer (tTcpTimer * pTimer)
{
    UINT4               u4Context;

    u4Context = GetTCBContext (pTimer->u4TcbIndex);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_OS_RES_TRC, "TCP",
                 "Entering TmrIsActiveTimer for CONN ID : %d.\n",
                 pTimer->u4TcbIndex);
    if (pTimer->u1Active == ACTIVE)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "TmrIsActiveTimer called. Returned TRUE.\n");
        return (TRUE);
    }
    else
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "TmrIsActiveTimer called. Returned FALSE.\n");
        return (FALSE);
    }
}
