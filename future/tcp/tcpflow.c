/** $Id: tcpflow.c,v 1.22 2017/09/13 13:34:07 siva Exp $ */

/* SOURCE FILE HEADER
 *
 * ----------------------------------------------------------------------------
 *  FILE NAME             : tcpflow.c
 * 
 *  PRINCIPAL AUTHOR      : Ramesh Kumar
 *
 *  SUBSYSTEM NAME        : TCP
 *
 *  MODULE NAME           : Flow Control and Adaptive Retransmission Module
 *
 *  LANGUAGE              : C
 *
 *  TARGET ENVIRONMENT    : Any
 *
 *  DATE OF FIRST RELEASE :
 * 
 *  DESCRIPTION           : This file contains routines corresponding to 
 *                          Flow Control and Retransmission.
 * ---------------------------------------------------------------------------
 *
 *
 *  CHANGE RECORD :
 *
 * ---------------------------------------------------------------------------
 *   VERSION        AUTHOR/DATE           DESCRIPTION OF CHANGE
 * ---------------------------------------------------------------------------
 *                  Sri. Chellapa        Changed all sequence no. Comparisons
 *                  13/05/96             to use SEQCMP macro.   
 * ---------------------------------------------------------------------------
 *                  Vinay/29.11.99       +Modified the return value of 
 *                                        FarGetLwsize() from UINT2 to UINT4. 
 *                                       +Modified LINFarSetRwsize() to 
 *                                        extract the upper 15 bits of the 
 *                                        window advertised by the peer TCP. It 
 *                                        is placed under the compilation switch
 *                                        ST_ENH.
 *                                       +Modified LINFarHandleAck() by 
 *                                        calling farUpdateRxmtQ() to update the
 *                                        retransmission queue. It placed under
 *                                        the compilation switch ST_ENH.
 *                                       +Modified LINFarSendAck() to add the
 *                                        enabled options. The modifications is 
 *                                        done under the compilation switch   
 *                                        ST_ENH.
 * ---------------------------------------------------------------------------
 *               Ramakrishnan 1/3/2000   +Added code to support select call 
 *                                        under the compilation flag ST_ENHLISTINTF
 * ---------------------------------------------------------------------------
 *               Ramakrishnan 5/6/2000   +Changed ST_ENHLISTINTF to ST_ENH
 *                                       +Ported to FSAP2
 * ---------------------------------------------------------------------------
 */

#include "tcpinc.h"
/*************************************************************************/
/*  Function Name   : FarGetLwsize                                       */
/*  Input(s)        : ptcb - Pointer to the TCB                          */
/*  Output(s)       : None.                                              */
/*  Returns         : window size                                        */
/*  Description     : It implements receiver side silly window avoidance */
/*                    when it computes a window advertisement.           */
/* CALLING CONDITION : Whenever a segment is to be sent to the remote    */
/*                     side, this function is called to calculate        */
/*                     window advertisement.                             */
/* GLOBAL VARIABLES AFFECTED : None.                                     */
/*************************************************************************/
UINT4
FarGetLwsize (tTcb * ptcb)
{
    UINT4               u4WindowSize;
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4TcbIndex = GET_TCB_INDEX (ptcb);
    u4Context = GetTCBContext (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering FarGetLwsize for CONN ID - %d.\n", u4TcbIndex);
    u4WindowSize = GetTCBrbsize (u4TcbIndex) - GetTCBrbcount (u4TcbIndex);
    /*Window size is equal to the available buffer space */

    if (GetTCBstate (u4TcbIndex) < TCPS_ESTABLISHED)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FarGetLwsize. Returned %d.\n", u4WindowSize);
        return u4WindowSize;
    }

    /* Never shrink an already advertised window, but wait for
       at least 1/4 receiver buffer and 1 max sized segment before
       opening a zero window.                                   */

    if ((((UINT4) (u4WindowSize << TCP_BYTES_FOR_SHORT)) <
         GetTCBrbsize (u4TcbIndex)) &&
        /* Check if < 1/4 RCVBUF , or */
        (u4WindowSize < GetTCBrmss (u4TcbIndex)))
    {
        /* less than rcvr mss */
        u4WindowSize = ZERO;
    }

    /* avoid shrinking advertised window */
    u4WindowSize = MAXIMUM (u4WindowSize, (UINT4) (GetTCBcwin (u4TcbIndex)
                                                   - GetTCBrnext (u4TcbIndex)));
    SetTCBcwin (u4TcbIndex,
                (INT4) ((UINT4) GetTCBrnext (u4TcbIndex) + u4WindowSize));

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FarGetLwsize. Returned %d.\n", u4WindowSize);
    return u4WindowSize;
}

/*************************************************************************/
/*  Function Name   : LINFarSetRwsize                                    */
/*  Input(s)        : ptcb - Pointer to the TCB                          */
/*                    pTcpHdr - pointer to Tcp Header Structure          */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK                                             */
/*  Description     : Sets the Remote window size of the TCB.It handles  */
/*                    window advertisement in incoming segments, and     */
/*                    keeps track of the amount of data the peer TCP is  */
/*                    willing to accept.                                 */
/* CALLING CONDITION : Whenever a segment from the sender side comes,    */
/*                     it is used to calculate the sender window.        */
/*                     It is called from tcp_established, tcp_closewait  */
/*                     and tcp_fin1                                      */
/* GLOBAL VARIABLES AFFECTED : None                                      */
/* NOTE: After the ESTABLISHED state all segments transmitted should     */
/*       contain CURRENT ACKNOWLEDGMENT SEQ No.                          */
/*************************************************************************/
INT1
LINFarSetRwsize (tTcb * ptcb, tTcpHdr * pTcpHdr)
{
    UINT4               u4Owlast, u4Wlast;
    INT4                i4Ack;
    INT4                i4SeqNo;
    UINT4               u4Win, u4LowerWin;
    UINT4               u4TcbIndex;
    UINT4               u4UpperWin;
    UINT4               u4Context;

    u4TcbIndex = GET_TCB_INDEX (ptcb);
    u4Context = GetTCBContext (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering LINFarSetRwsize for CONN ID - %d.\n", u4TcbIndex);

    i4Ack = OSIX_NTOHL (pTcpHdr->i4Ack);
    i4SeqNo = OSIX_NTOHL (pTcpHdr->i4Seq);
    u4LowerWin = (UINT4) (OSIX_NTOHS (pTcpHdr->u2Window));
    u4Win = u4LowerWin;

    if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_BW)
    {                            /* BW is enabled */

        /* extract the upper 15 bits from the option & assign it to u4UpperWin */
        u4UpperWin = FsmBWExtractUpperBytes (u4Context, pTcpHdr);

        u4Win = u4LowerWin | (u4UpperWin << TCP_BIT_SHIFT_16);
    }
    else
    {
        /* Do Window scaling. Window scaling is not possible when 
         * Big Window is enabled.*/
        u4Win = u4LowerWin << GetTCBSendWindowScale (u4TcbIndex);
    }

    /* make sure that incoming segment was generated after the segment 
       that was last used to update send window                     */

    if (SEQCMP (i4SeqNo, GetTCBlwseq (u4TcbIndex)) < ZERO)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from LINFarSetRwsize. Returned ERR.\n");
        return ERR;
    }

    if ((SEQCMP (i4SeqNo, GetTCBlwseq (u4TcbIndex)) == ZERO) &&
        (SEQCMP (i4Ack, GetTCBlwack (u4TcbIndex)) < ZERO))
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from LINFarSetRwsize. Returned ERR.\n");
        return ERR;                /* may be a duplicate pkt return ok */
    }

    /* Previously advertised window size */
    u4Owlast = (UINT4) GetTCBlwack (u4TcbIndex) + GetTCBswindow (u4TcbIndex);

    /* Currently advertised window size */
    u4Wlast = (UINT4) i4Ack + u4Win;

    SetTCBswindow (u4TcbIndex, u4Win);
    if (GetTCBMaxswnd (u4TcbIndex) < u4Win)
    {
        SetTCBMaxswnd (u4TcbIndex, u4Win);
    }
    SetTCBlwseq (u4TcbIndex, i4SeqNo);
    SetTCBlwack (u4TcbIndex, i4Ack);

    if (SEQCMP ((INT4) u4Wlast, (INT4) u4Owlast) <= ZERO)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from LINFarSetRwsize. Returned ERR.\n");
        return ERR;
    }

    if (GetTCBostate (u4TcbIndex) == TCPO_PERSIST)
    {
        SetTCBostate (u4TcbIndex, TCPO_TRANSMIT);
        TcpTmrDeleteTimer (&TCBtable[u4TcbIndex].TcbPersistTmr);
        TCP_MOD_TRC (u4Context, TCP_OS_RES_TRC, "TCP",
                     "Persist timer Deleted.\n");
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from LINFarSetRwsize. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : LINFarHandleAck                                    */
/*  Input(s)        : ptcb - Pointer to the TCB                          */
/*                    pSeg - Segment received                            */
/*  Output(s)       : None.                                              */
/*  Returns         : Number of octets acknowledged                      */
/*  Description     : It handles acknowledgement in the incoming         */
/*                    segments. It computes the number of octets in the  */
/*                    sequence space beyond those octets acknowledged    */
/*                    by previous segments.                              */
/* CALLING CONDITION : For all the incoming segments, acknowledgement    */
/*                     is to be sent. Whenever a new segment comes,      */
/*                     this routine is called to handle the ack.         */
/* GLOBAL VARIABLES AFFECTED : None                                      */
/*************************************************************************/
INT4
LINFarHandleAck (tTcb * ptcb, tIpTcpHdr * pSeg)
{
    INT4                i4OctetsACKed, i4SynACKed;
    INT4                i4Ack;
    UINT4               u4TcbIndex;
    UINT1               u1Code;
    INT4                i4RttEst = SYSERR;
    UINT4               u4CurrSysTime = ZERO;
    UINT4               u4TSecr;
    UINT4               u4Context;

    u4TcbIndex = GET_TCB_INDEX (ptcb);
    u4Context = GetTCBContext (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering LINFarHandleAck for CONN ID - %d\n", u4TcbIndex);
    u1Code = pSeg->tcp->u1Code;
    i4Ack = OSIX_NTOHL (pSeg->tcp->i4Ack);

    if (!(u1Code & TCPF_ACK))
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from LINFarHandleAck. Returned SYSERR.\n");
        return SYSERR;
    }
    /* ANVL BUG FIX : ACK recvd for data out of sequence is outside 
     * the SND.UNA and SND.NXT. When such a case happens and state 
     * is SYNRECVD, then send a reset. */

    if (((i4Ack < GetTCBsuna (u4TcbIndex)) ||
         (i4Ack > GetTCBsnext (u4TcbIndex))) &&
        (GetTCBstate (u4TcbIndex) == TCPS_SYNRCVD))
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from LINFarHandleAck by call LINOsmSendReset.\n");
        return LINOsmSendReset (ptcb->u4Context, pSeg);
    }

    if (SEQCMP (i4Ack, GetTCBsnext (u4TcbIndex)) > ZERO)
    {
        /* Ack received for data out of seq. space */
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from LINFarHandleAck by calling LINFarSendAck.\n");
        return LINFarSendAck (ptcb, pSeg);
    }

    i4OctetsACKed = i4Ack - GetTCBsuna (u4TcbIndex);

    if (i4OctetsACKed <= ZERO)
    {
        if (LINFarSetRwsize (ptcb, pSeg->tcp) == TCP_OK)
            osm_switch[GetTCBostate (u4TcbIndex)] (u4TcbIndex, TCPE_SEND);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from LINFarHandleAck. Returned OK.\n");
        return TCP_OK;
    }

    FarUpdateRxmtQ (ptcb, i4Ack);

    u4TSecr = FsmTSExtractTSecr (u4Context, pSeg->tcp);

    if (u4TSecr)
    {
        u4TSecr = OSIX_NTOHL (u4TSecr);
        TCP_GET_SYS_TIME ((tOsixSysTime *) & u4CurrSysTime);
        i4RttEst = u4CurrSysTime - u4TSecr;
    }

/*
 * left edge of the window has advanced since i4OctetsACKed > ZERO.
 * perfect codition for recomputing RTT.
 */

    FarComputeRtt (ptcb, i4RttEst);    /* round trip estimate */

    SetTCBsuna (u4TcbIndex, i4Ack);

    /* By now all corrupted segments have been filtered */
    i4SynACKed = ZERO;

    if (GetTCBcode (u4TcbIndex) & TCPF_SYN)
    {
        i4OctetsACKed--;        /* SYN is not data */
        i4SynACKed++;
        SetTCBcode (u4TcbIndex,
                    (UINT1) ((GetTCBcode (u4TcbIndex) & ~TCPF_SYN)));
        SetTCBflags_AND (u4TcbIndex, ~TCBF_FIRSTSEND);
    }

    if ((GetTCBcode (u4TcbIndex) & TCPF_FIN) &&
        !(GetTCBflags (u4TcbIndex) & TCBF_FINNOTSENT) &&
        (i4Ack > GetTCBfinsnext (u4TcbIndex)))
    {
        i4OctetsACKed--;        /* FIN is not data */
        i4SynACKed++;
        SetTCBcode (u4TcbIndex,
                    (UINT1) ((GetTCBcode (u4TcbIndex) & ~TCPF_FIN)));
        SetTCBflags_AND (u4TcbIndex, ~TCBF_SNDFIN);
        SetTCBflags_AND (u4TcbIndex, ~TCBF_REXMTFIN);
    }

    SetTCBsbstart (u4TcbIndex,
                   ((GetTCBsbstart (u4TcbIndex) +
                     (UINT4) i4OctetsACKed) % GetTCBsbsize (u4TcbIndex)));
    SetTCBsbcount (u4TcbIndex,
                   MAXIMUM ((GetTCBsbcount (u4TcbIndex) -
                             (UINT4) i4OctetsACKed), ZERO));

#ifdef SLI_WANTED
    if (GetTCBsbsize (u4TcbIndex) > GetTCBsbcount (u4TcbIndex))
    {
        SliSelectScanList (GetTCBSockDesc (u4TcbIndex), SELECT_WRITE);
    }
#endif

    LINFarSetRwsize (ptcb, pSeg->tcp);

    /* change the output state of TCB and initiate osm */
    FarUpdateOstate (ptcb, i4OctetsACKed + i4SynACKed);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from LINFarHandleAck. Returned %d\n", i4OctetsACKed);
    return i4OctetsACKed;
}

/*************************************************************************/
/*  Function Name   : LINFarSendAck                                      */
/*  Input(s)        : ptcb - Pointer to the TCB                          */
/*                    pRcvSeg - Pointer IpTcp Header Structure           */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK / ERR                                       */
/*  Description     : TCP sends ACK whenever the remote side acknowledge */
/*                    data that lies beyond the current output window.   */
/*                    It builds the segment by allocating space and      */
/*                    filling up TCP header portion.                     */
/* CALLING CONDITION :  To send Acknowledgement                          */
/* GLOBAL VARIABLES AFFECTED : None                                      */
/*************************************************************************/
INT1
LINFarSendAck (tTcb * ptcb, tIpTcpHdr * pRcvSeg)
{
    tSegment           *pSegOut;
    tIpTcpHdr           SendSeg;
    tIpTcpHdr          *pSendSeg;
    tIpHdr             *pIpHdr;
    tTcpHdr            *pTcpHdr;
    INT4                i4SeqNo, i4Ack;
    UINT1               au1IpTcpHdr[sizeof (tIpHdr) + sizeof (tTcpHdr)];
    UINT2               u2SegLen, u1TCPHdrStart;
    UINT4               u4RcvWnd;
    UINT1               u1IpOptlen;
    UINT4               u4TcbIndex;
    INT1                i1TcpOptLen;
    UINT2               u2LowerWindow;
    tIpAddr             SrcAddr, DestAddr;
    UINT1               u1IpHdrSize;
    UINT1               u1IpToSend;
#ifdef IP6_WANTED
    tHlToIp6Params      Ip6SendParms;
#endif
    UINT1               au1Md5Digest[MD5_DIGEST_LEN];
    UINT1              *pTcpOpt = NULL;
    UINT4               u4TcpOptIndex = TCP_ZERO;
    UINT4               u4Context;
    UINT1               u1TCPHdrSizePrev = TCP_ZERO;

    u4TcbIndex = GET_TCB_INDEX (ptcb);
    u4Context = GetTCBContext (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering LINFarSendAck for CONN ID - %d\n", u4TcbIndex);

    i1TcpOptLen = OsmGetTCBOptLen (u4TcbIndex);
    if (i1TcpOptLen == ERR)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from LINFarSendAck. Error in optlen. Returned ERR.\n");
        return ERR;
    }

    MEMSET (au1IpTcpHdr, TCP_ZERO, (sizeof (tIpHdr) + sizeof (tTcpHdr)));

    SrcAddr = GetTCBlip (u4TcbIndex);
    DestAddr = GetTCBrip (u4TcbIndex);
    if ((IsIP6Addr (SrcAddr)) || (IsIP6Addr (DestAddr)))
    {
        u1IpToSend = IP6;
        u1IpOptlen = TCP_ZERO;
        u1IpHdrSize = TCP_ZERO;
        u2SegLen = (UINT2) (TCP_HEADER_LENGTH + i1TcpOptLen);
    }
    else
    {
        u1IpToSend = IP4;
        u1IpOptlen = OsmGetIpOptlen (u4TcbIndex);
        u1IpHdrSize = (UINT1) (IP_HEADER_LENGTH + u1IpOptlen);
        u2SegLen = (UINT2) (TCP_HEADER_LENGTH + IP_HEADER_LENGTH + i1TcpOptLen +
                            u1IpOptlen);
    }
    u1TCPHdrStart = (UINT1) (IP6_HEADER_LEN + u1IpOptlen);
    pSendSeg = &SendSeg;
    pIpHdr = (tIpHdr *) (VOID *) au1IpTcpHdr;
    if (u1TCPHdrStart >= (sizeof (tIpHdr) + sizeof (tTcpHdr)))
    {
        return ERR;
    }
    pTcpHdr = (tTcpHdr *) (VOID *) (au1IpTcpHdr + u1TCPHdrStart);
    pSendSeg->ip = pIpHdr;
    pSendSeg->tcp = pTcpHdr;

    if ((pSegOut = CRU_BUF_Allocate_MsgBufChain (u2SegLen, ZERO)) == NULL)
    {
        TCP_MOD_TRC (u4Context,
                     TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC | TCP_BUF_TRC, "TCP",
                     "Exit from LINFarSendAck. Allocate buf chain failed. Returned ERR.");
        return ERR;
    }
    MEMSET (pSegOut->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pSegOut, "TcpFarSndAck");

    TCP_MOD_TRC (u4Context, TCP_BUF_TRC, "TCP",
                 "Allocation of buffer chain successful.\n");
    i4SeqNo = GetTCBsnext (u4TcbIndex);
    i4Ack = GetTCBrnext (u4TcbIndex);
    u4RcvWnd = FarGetLwsize (ptcb);
    if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_BW)
    {
        u2LowerWindow = (UINT2) (u4RcvWnd & TCP_LOWER_WINDOW_MASK);
    }
    else
    {
        /* When window scaling is used, Lower window can assume values  
         * greater than 65535. We should right shift it with the 
         * Window scale value.
         * It is assumed that after right shifting the value is < 65535 */

        u2LowerWindow =
            (UINT2) (u4RcvWnd >> GetTCBRecvWindowScale (u4TcbIndex));
    }

    pTcpHdr->u2Sport = pRcvSeg->tcp->u2Dport;
    pTcpHdr->u2Dport = pRcvSeg->tcp->u2Sport;
    pTcpHdr->i4Seq = OSIX_HTONL (i4SeqNo);
    pTcpHdr->i4Ack = OSIX_HTONL (i4Ack);

    pTcpHdr->u1Hlen =
        (UINT1) ((((TCP_HEADER_LENGTH +
                    i1TcpOptLen) >> TCP_BYTES_FOR_SHORT) & TCP_IPHDRLEN_MASK) <<
                 TCP_VER_SHIFT);
    pTcpHdr->u1Code = TCPF_ACK;

    pTcpHdr->u2Window = OSIX_HTONS (u2LowerWindow);    /* clarify about typecasting */

    pTcpHdr->u2Cksum = ZERO;
    pTcpHdr->u2Urgptr = ZERO;

    pIpHdr->u1Proto = TCP_PROTOCOL;
    pIpHdr->DestAddr = pRcvSeg->ip->SrcAddr;
    pIpHdr->SrcAddr = pRcvSeg->ip->DestAddr;

    if (u1IpToSend == IP6)
    {
        pIpHdr->u2Totlen = OSIX_HTONS ((u2SegLen));
        pIpHdr->u1VHlen = TCP_ZERO;
    }
    else
    {
        LINOsmCopyIpOptions (pIpHdr->au1IpOpts, u4TcbIndex);
        pIpHdr->u2Totlen = OSIX_HTONS ((u2SegLen - IP_HEADER_LEN - u1IpOptlen));
    }
    pIpHdr->u2IpOptLen = u1IpOptlen;

    pTcpHdr->u2Cksum = ZERO;
    /* Last option to be added to the TCP header is TCP-AO */
    /* Both TCP-AO & MD5 will not be present on same TCP segment */
    if (GetTCBTcpAoEnabled (u4TcbIndex))
    {
        u1TCPHdrSizePrev = (UINT1)
            ((TCP_HEADER_LENGTH + i1TcpOptLen) - (TCPAO_OPT_LEN + TCP_TWO));

        /* MAC is calculated including the TCP-AO option also */
        /* MAC field in the TCP-AO option will be set to zero */
        pTcpOpt = pSendSeg->tcp->au1TcpOpts;
        u4TcpOptIndex = (UINT4) (u1TCPHdrSizePrev - TCP_HEADER_LENGTH);

        if (TcpOptFillTcpAoFields (u4TcbIndex, pSendSeg, pSegOut,
                                   &pTcpOpt[u4TcpOptIndex]) != TCP_OK)
        {
            TCP_MOD_TRC (u4Context,
                         TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC | TCP_BUF_TRC, "TCP", "Exit from LINFarSendAck. TCP-AO MAC generation failed for \
                Conn - %d.\n", u4TcbIndex);
            CRU_BUF_Release_MsgBufChain (pSegOut, FALSE);
            return ERR;
        }
    }
    /* If MD5 key is set in TCB, generate digest and add to header */
    else if (GetTCBmd5keylen (u4TcbIndex) != TCP_ZERO)
    {
        u1TCPHdrSizePrev = (UINT1)
            ((TCP_HEADER_LENGTH + i1TcpOptLen) - (MD5_OPT_LEN + TCP_TWO));

        MEMSET (au1Md5Digest, TCP_ZERO, sizeof (au1Md5Digest));
        if (TcpOptGenerateMD5Digest
            (u4Context, pSendSeg, pSegOut, au1Md5Digest,
             (UINT1 *) GetTCBpmd5key (u4TcbIndex),
             GetTCBmd5keylen (u4TcbIndex)) == TCP_OK)
        {
            pTcpOpt = pSendSeg->tcp->au1TcpOpts;
            u4TcpOptIndex = u1TCPHdrSizePrev - TCP_HEADER_LENGTH;
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;
            pTcpOpt[u4TcpOptIndex++] = TCP_MD5;
            pTcpOpt[u4TcpOptIndex++] = MD5_OPT_LEN;
            MEMCPY ((pTcpOpt + u4TcpOptIndex), au1Md5Digest, MD5_DIGEST_LEN);
        }
        else
        {
            TCP_MOD_TRC (u4Context,
                         TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC | TCP_BUF_TRC, "TCP", "Exit from LINFarSendAck.MD5 digest generation failed for \
                Conn - %d.\n", u4TcbIndex);
            CRU_BUF_Release_MsgBufChain (pSegOut, FALSE);
            return ERR;
        }
    }

    /* Copy TCP,IP Hdrs and IP Opts into CRU Buffer and send to IP */

    pTcpHdr->u2Cksum = TcpPktCksum (u4Context, pSegOut, pSendSeg);
    pTcpHdr->u2Cksum = OSIX_HTONS (pTcpHdr->u2Cksum);

    if ((CRU_BUF_Copy_OverBufChain
         (pSegOut, (au1IpTcpHdr + IP6_HEADER_LEN),
          u1IpHdrSize - u1IpOptlen, u2SegLen - u1IpHdrSize)) != CRU_SUCCESS)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from LINFarSendAck. Returned ERR.\n");
        CRU_BUF_Release_MsgBufChain (pSegOut, TRUE);
        return ERR;
    }

    /*SEND TO IP */
    if (u1IpToSend == IP6)
    {
#ifdef IP6_WANTED
        MEMSET (&Ip6SendParms, 0, sizeof (Ip6SendParms));
        LliGetIp6SendParms (pSegOut, u4TcbIndex, u2SegLen, &Ip6SendParms);
        if ((LliSendSegmentToIP6 (pSegOut, u2SegLen, &Ip6SendParms)) != TCP_OK)
        {
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from LINFarSendAck. Returned ERR.\n");
            return ERR;
        }
#endif
    }
#ifdef IP_WANTED
    else if ((LliSendSegmentToIP
              (pSegOut, u2SegLen,
               LliGetIpSendParms (pSegOut, u4TcbIndex,
                                  (UINT2) (u2SegLen - IP_HEADER_LENGTH -
                                           u1IpOptlen)))) != TCP_OK)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from LINFarSendAck. Returned ERR.\n");
        return ERR;
    }
#endif

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from LINFarSendAck. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : FarUpdateOstate                                    */
/*  Input(s)        : ptcb - Pointer to the TCB                          */
/*                    u4Acked - No of data sequence #s acked             */
/*  Output(s)       : None                                               */
/*  Returns         : TCP_OK                                             */
/*  Description     : After acknowledgement arrives, TCP must examine    */
/*                    the TCB to find if the output state should be      */
/*                    changed. If all outstanding data has been ACKed    */
/*                 the output reverts to idle state. If some outstanding */
/*                 data has been ACKed, the output state changes from    */
/*                 retransmit to transmit state.                         */
/* CALLING CONDITION : When the TCP sends ACK to the other end.          */
/* GLOBAL VARIABLES AFFECTED : Output state of the TCB.                  */
/*************************************************************************/
INT1
FarUpdateOstate (tTcb * ptcb, UINT4 u4Acked)
{
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4TcbIndex = GET_TCB_INDEX (ptcb);
    u4Context = GetTCBContext (u4TcbIndex);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering FarUpdateOstate for CONN ID - %d\n", u4TcbIndex);
    if (u4Acked <= ZERO)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FarUpdateOstate. Returned OK.\n");
        return TCP_OK;
    }
    if (GetTCBostate (u4TcbIndex) == TCPO_RETRANSMIT)
    {
        SetTCBostate (u4TcbIndex, TCPO_TRANSMIT);
        SetTCBrexmtcount (u4TcbIndex, ZERO);
    }
    if ((GetTCBsbcount (u4TcbIndex) == ZERO) &&
        (SEQCMP (GetTCBsnext (u4TcbIndex), GetTCBsuna (u4TcbIndex)) == ZERO))
    {
        /* all outstanding data has been ACKed */
        SetTCBostate (u4TcbIndex, TCPO_IDLE);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FarUpdateOstate. Returned OK.\n");
        return TCP_OK;
    }

    /* When the output state changes, generate a send event */
    osm_switch[GetTCBostate (u4TcbIndex)] (u4TcbIndex, TCPE_SEND);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FarUpdateOstate. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : FarComputeRtt                                      */
/*  Input(s)        : ptcb - pointer to the TCB                          */
/*                    u4RttEst - Round trip estimate                     */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK                                             */
/*  Description     : It is called whenever an acknowledgement arrives.  */
/*                    Thus a call to FarComputeRtt ends the retransmi-   */
/*                    ssion and the output state reverts to TRANSMIT.    */
/*                    First it deletes the associated timer, and         */
/*                    computes the time elapsed since the event was      */
/*                    scheduled.                                         */
/* CALLING CONDITION : It is called whenever an acknowledgement arrives. */
/* GLOBAL VARIABLE AFFECTED : None.                                      */
/*************************************************************************/
INT1
FarComputeRtt (tTcb * ptcb, INT4 i4RttEst)
{
    UINT4               u4TcbIndex;
    UINT4               u4RtoAlgorithm = 0;
    UINT4               u4Context = 0;
    INT4                i4Rrt, i4Delta, i4Temp;
    UINT4               u4Aw, u4OldCwnd = 0;
    FLT4                f4Bw;

    u4TcbIndex = GET_TCB_INDEX (ptcb);
    u4Context = GetTCBContext (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering FarComputeRtt for CONN ID - %d.\n", u4TcbIndex);
    /* It should return the remaining time for
       expiry or it should return error condition */

    i4Rrt = i4RttEst;
    u4Context = GetTCBContext (u4TcbIndex);
    u4RtoAlgorithm = gpTcpContext[u4Context]->TcpStatParms.u4RtoAlgorithm;

    if (i4Rrt != SYSERR && u4RtoAlgorithm == VAN_JACOBSON)
    {
        if (GetTCBsrt (u4TcbIndex) == ZERO)
        {
            SetTCBsrt (u4TcbIndex, (i4Rrt << TCP_BYTE_MULTIPLIER));
        }
        i4Delta = ((i4Rrt << TCP_BYTE_MULTIPLIER) - GetTCBsrt (u4TcbIndex));

        SetTCBsrt (u4TcbIndex, (GetTCBsrt (u4TcbIndex) +
                                (((INT4) i4Delta) >> TCP_BYTE_MULTIPLIER)));

        if (GetTCBsrt (u4TcbIndex) < 1)
        {
            SetTCBsrt (u4TcbIndex, 1);
        }
        i4Delta = i4Delta >> TCP_ONE;

        if (i4Delta < ZERO)
        {
            i4Delta = -i4Delta;
        }
        i4Temp = ((INT4) (i4Delta - GetTCBrtde (u4TcbIndex)) >>
                  TCP_BYTES_FOR_SHORT);

        SetTCBrtde (u4TcbIndex, (GetTCBrtde (u4TcbIndex) + i4Temp));
        if (GetTCBrtde (u4TcbIndex) < 1)
        {
            SetTCBrtde (u4TcbIndex, 1);
        }
        i4Temp = ((INT4) GetTCBsrt (u4TcbIndex) >> TCP_BYTES_FOR_SHORT) +
            GetTCBrtde (u4TcbIndex);

        SetTCBrexmt (u4TcbIndex, (i4Temp >> TCP_ONE));
        if (GetTCBrexmt (u4TcbIndex) < TCP_MINRETRANSMIT)
        {
            SetTCBrexmt (u4TcbIndex, TCP_MINRETRANSMIT);
        }
    }
    else if (i4Rrt != SYSERR && u4RtoAlgorithm == RFC2988)
    {
        /*If SRTT and RTTVAR is ZERO perform the operations as mentioned
         * in section 2.2 of RFC-2988*/
        if (GetTCBsrt (u4TcbIndex) == ZERO && GetTCBrtde (u4TcbIndex) == ZERO)
        {
            SetTCBsrt (u4TcbIndex, i4Rrt);
            SetTCBrtde (u4TcbIndex, i4Rrt >> TCP_ONE);
            SetTCBrexmt (u4TcbIndex, (GetTCBsrt (u4TcbIndex) +
                                      MAXIMUM (G_CLOCK,
                                               k * (GetTCBrtde (u4TcbIndex)))));
        }
        /*Else prefrom the operations as mentioned in section 2.3 of RFC-2988 */
        else
        {
            SetTCBrtde (u4TcbIndex,
                        (UINT4) ((((TCP_ONE -
                                    BETA) * GetTCBrtde (u4TcbIndex))) +
                                 (BETA *
                                  abs (GetTCBsrt (u4TcbIndex) - i4Rrt))));
            SetTCBsrt (u4TcbIndex,
                       (UINT4) ((((1 - ALPHA) * GetTCBsrt (u4TcbIndex)) +
                                 (ALPHA * i4Rrt))));
            SetTCBrexmt (u4TcbIndex,
                         (GetTCBsrt (u4TcbIndex) +
                          MAXIMUM (G_CLOCK, k * (GetTCBrtde (u4TcbIndex)))));
        }
        /*If RTO < 1sec,roundoff RTO to 1sec as mentioned in section 2.4 of
         * RFC-2988*/
        if (GetTCBrexmt (u4TcbIndex) < TCP_CLK_TCK)
        {
            SetTCBrexmt (u4TcbIndex, TCP_CLK_TCK);
        }
    }
    if (GetTCBcwnd (u4TcbIndex) < GetTCBssthresh (u4TcbIndex))
    {
        /* TCP Slow Start */
        SetTCBcwnd (u4TcbIndex, (GetTCBcwnd (u4TcbIndex)
                                 + GetTCBsmss (u4TcbIndex)));
    }
    else if (GetTCBcwnd (u4TcbIndex) <
             (gpTcpContext[u4Context]->u4TcpMinCwnd * GetTCBsmss (u4TcbIndex)))
    {
        /* Normal TCP congection avoidance */
        SetTCBcwnd (u4TcbIndex, (GetTCBcwnd (u4TcbIndex)
                                 + ((GetTCBsmss (u4TcbIndex)
                                     * GetTCBsmss (u4TcbIndex))
                                    / GetTCBcwnd (u4TcbIndex))));
    }
    else
    {
        /* Apply HSTCP formula for incrementing Congestion Window */
        u4OldCwnd = (GetTCBcwnd (u4TcbIndex) / GetTCBsmss (u4TcbIndex));
        f4Bw = ((TCP_DEF_BW_AT_MAX_CWND - TCP_DEF_BW_AT_MIN_CWND) *
                (log (u4OldCwnd) -
                 log (gpTcpContext[u4Context]->u4TcpMinCwnd)) /
                (log (gpTcpContext[u4Context]->u4TcpMaxCwnd) -
                 log (gpTcpContext[u4Context]->u4TcpMinCwnd))) +
            TCP_DEF_BW_AT_MIN_CWND;
        u4Aw =
            (UINT4) ((pow (u4OldCwnd, TCP_TWO) * TCP_TWO * f4Bw *
                      HSTCP_CONST_1) / (((TCP_TWO - f4Bw) *
                                         pow (u4OldCwnd, HSTCP_CONST_2))));
        if (u4Aw < TCP_ONE)
            u4Aw = TCP_ONE;
        SetTCBcwnd (u4TcbIndex, (GetTCBcwnd (u4TcbIndex)
                                 + (u4Aw * ((GetTCBsmss (u4TcbIndex)
                                             * GetTCBsmss (u4TcbIndex))
                                            / GetTCBcwnd (u4TcbIndex)))));

    }

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FarComputeRtt. Returned OK.\n");
    return TCP_OK;
}
