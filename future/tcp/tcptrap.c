/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: tcptrap.c,v 1.4 2015/02/17 12:40:46 siva Exp $
*
* Description: This file contains the TCP trap implementation.
*****************************************************************************/

#include "fssnmp.h"
#include "snmputil.h"
#include "tcpinc.h"
#include "fstcp.h"
#include "fsmptcp.h"

UINT1              *TcpParseSubIdNew (UINT1 *, UINT4 *);

tSNMP_OID_TYPE     *TcpMakeObjIdFromDotNew (INT1 *, INT1);

/******************************************************************************
* Function      : TcpMakeObjIdFromDotNew                                      
* Input         : pi1TextStr
*               : i1Flag
* Output        : NONE
* Description   : This function is used to get object id from given object
*                 and convert those object id to OID List.
* Returns       : pOidPtr or NULL
*******************************************************************************/

tSNMP_OID_TYPE     *
TcpMakeObjIdFromDotNew (INT1 *pi1TextStr, INT1 i1Flag)
{
    tSNMP_OID_TYPE     *pOidPtr;
    INT1               *pi1TempPtr, *pi1DotPtr;
    UINT1               u1Index;
    UINT1               u1DotCount;
    UINT4               u4BufferLen = 0;
    UINT4               u4TempBufferLen = 0;
    UINT4               u4Length = 0;
    UINT2               u2Len = 0;
    struct MIB_OID     *mib_oid_table = NULL;
    INT1                ai1TempBuffer[SNMP_MAX_OID_LENGTH];
    INT4                i4MibTblLen = 0;

    if (i1Flag == TCP_MI)
    {
        mib_oid_table = fsmptcp_mib_oid_table;
        i4MibTblLen = (sizeof (fsmptcp_mib_oid_table) /
                       sizeof (struct MIB_OID));
    }
    else
    {
        mib_oid_table = fstcp_mib_oid_table;
        i4MibTblLen = (sizeof (fsmptcp_mib_oid_table) /
                       sizeof (struct MIB_OID));
    }
    /* see if there is an alpha descriptor at begining */
    if (isalpha (*pi1TextStr))
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pi1TempPtr = pi1TextStr;

        for (u1Index = 0; ((pi1TempPtr < pi1DotPtr) && (u1Index < 255));
             u1Index++)
        {
            ai1TempBuffer[u1Index] = *pi1TempPtr++;
        }
        ai1TempBuffer[u1Index] = '\0';

        for (u1Index = 0; ((u1Index < i4MibTblLen) &&
                           (mib_oid_table[u1Index].pName != NULL)); u1Index++)
        {
            if ((STRCMP
                 (mib_oid_table[u1Index].pName,
                  (INT1 *) ai1TempBuffer) == 0)
                && (STRLEN ((INT1 *) ai1TempBuffer) ==
                    STRLEN (mib_oid_table[u1Index].pName)))
            {
                u2Len =
                    (UINT2) (STRLEN (mib_oid_table[u1Index].pNumber) <
                             SNMP_MAX_OID_LENGTH ?
                             STRLEN (mib_oid_table[u1Index].
                                     pNumber) : SNMP_MAX_OID_LENGTH - 1);
                STRNCPY ((INT1 *) ai1TempBuffer, mib_oid_table[u1Index].pNumber,
                         u2Len);
                ai1TempBuffer[u2Len] = '\0';
                break;
            }
        }

        if (u1Index < i4MibTblLen)
        {
            if (mib_oid_table[u1Index].pName == NULL)
            {
                return (NULL);
            }
        }
        /* now concatenate the non-alpha part to the begining */
        u4TempBufferLen = STRLEN (ai1TempBuffer);
        u4BufferLen =
            (UINT2) (sizeof (ai1TempBuffer) - STRLEN (ai1TempBuffer) - 1);
        u4Length =
            (u4BufferLen <
             STRLEN (pi1DotPtr) ? u4BufferLen : STRLEN (pi1DotPtr));
        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr, u4Length);
        ai1TempBuffer[u4Length + u4TempBufferLen] = '\0';
    }
    else
    {                            /* is not alpha, so just copy into ai1TempBuffer */
        STRNCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr, STRLEN((INT1 *) pi1TextStr));
	ai1TempBuffer[STRLEN((INT1 *) pi1TextStr)] = '\0';
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u1DotCount = 0;
    for (u1Index = 0; ai1TempBuffer[u1Index] != '\0'; u1Index++)
    {
        if (ai1TempBuffer[u1Index] == '.')
        {
            u1DotCount++;
        }
    }

    /* The object specified may be either Tabular or Scalar
     * Object. Tabular Objects needs index to be added at the
     * end. So, allocating for Maximum OID Length
     * */
    if ((pOidPtr = alloc_oid (SNMP_MAX_OID_LENGTH)) == NULL)
    {
        return (NULL);
    }
    pOidPtr->u4_Length = (UINT4) (u1DotCount + 1);

    /* now we convert number.number.... strings */
    pi1TempPtr = ai1TempBuffer;
    for (u1Index = 0; u1Index < u1DotCount + 1; u1Index++)
    {
        if ((pi1TempPtr = (INT1 *) TcpParseSubIdNew
             ((UINT1 *) pi1TempPtr, &(pOidPtr->pu4_OidList[u1Index]))) == NULL)
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if (*pi1TempPtr == '.')
        {
            pi1TempPtr++;        /* to skip over dot */
        }
        else if (*pi1TempPtr != '\0')
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
}

/******************************************************************************
* Function : TcpParseSubIdNew
* Input    : ppu1TempPtr
* Output   : value of ppu1TempPtr
* Returns  : OSIX_SUCCESS or OSIX_FAILURE
*******************************************************************************/

UINT1              *
TcpParseSubIdNew (UINT1 *pu1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    UINT1              *pu1Tmp = NULL;

    for (pu1Tmp = pu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                               ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                               ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pu1Tmp & 0xf);
    }

    if (pu1TempPtr == pu1Tmp)
    {
        pu1Tmp = NULL;
    }
    *pu4Value = u4Value;
    return pu1Tmp;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  TcpAoAuthErrorNotifyTrap 
 *                                                                          
 *    DESCRIPTION      :  This function is called when any threshold event or 
 *                        Non-threshold event occurs in EOAM. This function forms
 *                        the SNMP Trap VbLIst and Notify to the SNMP.
 *                                                                          
 *    INPUT            :  pTrapInfo - pointer to Trap information
 *                        u4IfIndex - Interface Index
 *                        u4TrapId  - Notification ID as specified in STD MIB.
 *    OUTPUT           :  None. 
 *                                                                          
 *    RETURNS          :  VOID                   
 *                                                                          
*******************************************************************************/
VOID
TcpAoAuthNotifyEventTrap (tTcpAoErrLogEntry * pTrapInfo, UINT4 u4TrapId)
{

#ifdef SNMP_3_WANTED

    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT4               au4snmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    UINT1               au1Buf[SNMP_MAX_OID_LENGTH];
    INT1                i1Flag = 0;

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

#ifdef VRF_WANTED
    i1Flag = TCP_MI;
#else
    i1Flag = TCP_SI;
#endif

    (i1Flag == TCP_MI) ? (SPRINTF ((char *) au1Buf, "fsMITcpTrap")) :
        (SPRINTF ((char *) au1Buf, "fstcpTrap"));

    pEnterpriseOid = TcpMakeObjIdFromDotNew ((INT1 *) au1Buf, i1Flag);
    if (pEnterpriseOid == NULL)
    {
        return;
    }
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u4TrapId;

    /* Allocate the Memory for SNMP Trap OID to form the VbList */
    pSnmpTrapOid = alloc_oid (sizeof (au4snmpTrapOid) / sizeof (UINT4));
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    MEMCPY (pSnmpTrapOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pSnmpTrapOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);
    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
        (pSnmpTrapOid, SNMP_DATA_TYPE_OBJECT_ID,
         0L, 0, NULL, pEnterpriseOid, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    pStartVb = pVbList;

    if (i1Flag == TCP_MI)
    {
        SPRINTF ((char *) au1Buf, "fsMITcpAoContextId");
        pOid = TcpMakeObjIdFromDotNew ((INT1 *) au1Buf, i1Flag);
        if (pOid == NULL)
        {
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            SNMP_AGT_FreeVarBindList (pStartVb);
            return;
        }
        pVbList->pNextVarBind = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
            (pOid, SNMP_DATA_TYPE_INTEGER32,
             0, pTrapInfo->i4Context, NULL, NULL, SnmpCnt64Type);

        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            SNMP_FreeOid (pOid);
            SNMP_AGT_FreeVarBindList (pStartVb);
            return;
        }
        pVbList = pVbList->pNextVarBind;
    }

    (i1Flag ==
     TCP_MI) ? (SPRINTF ((char *) au1Buf,
                         "fsMITcpAoLocalAddressType")) : (SPRINTF ((char *)
                                                                   au1Buf,
                                                                   "fstcpAoLocalAddressType"));

    pOid = TcpMakeObjIdFromDotNew ((INT1 *) au1Buf, i1Flag);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList->pNextVarBind = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
        (pOid, SNMP_DATA_TYPE_INTEGER32,
         0, pTrapInfo->i1AdType, NULL, NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }
    pVbList = pVbList->pNextVarBind;

    (i1Flag == TCP_MI) ? (SPRINTF ((char *) au1Buf, "fsMITcpAoLocalAddress")) :
        (SPRINTF ((char *) au1Buf, "fstcpAoLocalAddress"));

    pOid = TcpMakeObjIdFromDotNew ((INT1 *) au1Buf, i1Flag);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    if (pTrapInfo->i1AdType == TCP_AF_IPV6_TYPE)
    {
        pOstring =
            SNMP_AGT_FormOctetString (pTrapInfo->In6DAdr->u1addr,
                                      IPVX_MAX_INET_ADR_LEN);
    }
    else
    {
        pOstring =
            SNMP_AGT_FormOctetString ((UINT1 *) &(pTrapInfo->InDAdr->u4Addr),
                                      sizeof (UINT4));
    }
    if (pOstring == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
        (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0, pOstring, NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    (i1Flag == TCP_MI) ? (SPRINTF ((char *) au1Buf, "fsMITcpAoLocalPort")) :
        (SPRINTF ((char *) au1Buf, "fstcpAoLocalPort"));

    pOid = TcpMakeObjIdFromDotNew ((INT1 *) au1Buf, i1Flag);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList->pNextVarBind = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
        (pOid, SNMP_DATA_TYPE_INTEGER32,
         0, pTrapInfo->u2DPort, NULL, NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    (i1Flag ==
     TCP_MI) ? (SPRINTF ((char *) au1Buf,
                         "fsMITcpAoRemAddressType")) : (SPRINTF ((char *)
                                                                 au1Buf,
                                                                 "fstcpAoRemAddressType"));

    pOid = TcpMakeObjIdFromDotNew ((INT1 *) au1Buf, i1Flag);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList->pNextVarBind = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
        (pOid, SNMP_DATA_TYPE_INTEGER32,
         0, pTrapInfo->i1AdType, NULL, NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }
    pVbList = pVbList->pNextVarBind;

    (i1Flag == TCP_MI) ? (SPRINTF ((char *) au1Buf, "fsMITcpAoRemAddress")) :
        (SPRINTF ((char *) au1Buf, "fstcpAoRemAddress"));

    pOid = TcpMakeObjIdFromDotNew ((INT1 *) au1Buf, i1Flag);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    if (pTrapInfo->i1AdType == TCP_AF_IPV6_TYPE)
    {
        pOstring =
            SNMP_AGT_FormOctetString (pTrapInfo->In6SAdr->u1addr,
                                      IPVX_MAX_INET_ADR_LEN);
    }
    else
    {
        pOstring =
            SNMP_AGT_FormOctetString ((UINT1 *) &(pTrapInfo->InSAdr->u4Addr),
                                      sizeof (UINT4));
    }
    if (pOstring == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
        (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0, pOstring, NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    (i1Flag == TCP_MI) ? (SPRINTF ((char *) au1Buf, "fsMITcpAoRemPort")) :
        (SPRINTF ((char *) au1Buf, "fstcpAoRemPort"));

    pOid = TcpMakeObjIdFromDotNew ((INT1 *) au1Buf, i1Flag);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList->pNextVarBind = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
        (pOid, SNMP_DATA_TYPE_INTEGER32,
         0, pTrapInfo->u2SPort, NULL, NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    (i1Flag == TCP_MI) ? (SPRINTF ((char *) au1Buf,
                                   "fsMITcpConTcpAOConnErrCtr")) :
        (SPRINTF ((char *) au1Buf, "fsTcpConTcpAOConnErrCtr"));

    pOid = TcpMakeObjIdFromDotNew ((INT1 *) au1Buf, i1Flag);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList->pNextVarBind = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
        (pOid, SNMP_DATA_TYPE_COUNTER32,
         pTrapInfo->u4TcpAoErCtr, 0, NULL, NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);

#else
    UNUSED_PARAM (pTrapInfo);
    UNUSED_PARAM (u4TrapId);
#endif
}
