
# ifndef fstcpOCON_H
# define fstcpOCON_H
/*
 *  The Constant Declarations for
 *  fstcp
 */

# define FSTCPACKOPTION                                    (1)
# define FSTCPTIMESTAMPOPTION                              (2)
# define FSTCPBIGWNDOPTION                                 (3)
# define FSTCPINCRINIWND                                   (4)
# define FSTCPMAXNUMOFTCB                                  (5)
# define FSTCPTRACEDEBUG                                   (6)

/*
 *  The Constant Declarations for
 *  fsTcpConnTable
 */

# define FSTCPCONNLOCALADDRESS                             (1)
# define FSTCPCONNLOCALPORT                                (2)
# define FSTCPCONNREMADDRESS                               (3)
# define FSTCPCONNREMPORT                                  (4)
# define FSTCPCONNOUTSTATE                                 (5)
# define FSTCPCONNSWINDOW                                  (6)
# define FSTCPCONNRWINDOW                                  (7)
# define FSTCPCONNCWINDOW                                  (8)
# define FSTCPCONNSSTHRESH                                 (9)
# define FSTCPCONNSMSS                                     (10)
# define FSTCPCONNRMSS                                     (11)
# define FSTCPCONNSRT                                      (12)
# define FSTCPCONNRTDE                                     (13)
# define FSTCPCONNPERSIST                                  (14)
# define FSTCPCONNREXMT                                    (15)
# define FSTCPCONNREXMTCNT                                 (16)
# define FSTCPCONNSBCOUNT                                  (17)
# define FSTCPCONNSBSIZE                                   (18)
# define FSTCPCONNRBCOUNT                                  (19)
# define FSTCPCONNRBSIZE                                   (20)
# define FSTCPKAMAINTMR                                    (21)
# define FSTCPKARETRANSTMR                                 (22)
# define FSTCPKARETRANSCNT                                 (23)

#endif /*  fstcpOCON_H  */
