/* FILE HEADER
 *
 * ----------------------------------------------------------------------------
 *  FILE NAME             : tcptmr.h
 * 
 *  PRINCIPAL AUTHOR      : Ramesh Kumar
 *
 *  SUBSYSTEM NAME        : TCP
 *
 *  MODULE NAME           : High Level Interface Module
 *
 *  LANGUAGE              : C
 *
 *  TARGET ENVIRONMENT    : Any
 *
 *  DATE OF FIRST RELEASE :
 * 
 *  DESCRIPTION           : This file contains the timer data structure
 *                          and timer values.
 *
 * ---------------------------------------------------------------------------
 *
 *
 *  CHANGE RECORD :
 *
 * ---------------------------------------------------------------------------
 *   VERSION        AUTHOR/DATE           DESCRIPTION OF CHANGE
 * ---------------------------------------------------------------------------
 *  $Id: tcptmr.h,v 1.11 2014/11/29 10:28:53 siva Exp $
 *
 * ---------------------------------------------------------------------------
 */
#ifndef  __TCP_TIMER_H__
#define  __TCP_TIMER_H__



/* These are u4_DsName to identify a timer */
#define  TCP_CLK_TCK                   SYS_NUM_OF_TIME_UNITS_IN_A_SEC 
#define  TCP_RETRANSMIT_TIMER          0x01                           

#define  TCP_2MSL_TIMER                0x02                           

#define  TCP_PERSIST_TIMER             0x03                           

#define  TCP_USER_TIMEOUT_TIMER        0x04                           

#define  TCP_KEEPALIVE_TIMER           0x05                           

#define  TCP_SO_LINGER_TIMER           0x07                           

#define  TCP_FINWAIT2_RECOVERY_TIMER   0x08                           

#define  TCP_KA_DEF_RETRIES            10  /* default no. of retries 
                                               for keepalive */
                           
#define  TCP_MINRETRIES                1   /* minimum retries */
#define  TCP_MAXRETRIES                12  /* maximum retries before 
                                              giving up */
                           

/* Number of ticks for different timers */
#define  TCP_TWOMSL                    30 * TCP_CLK_TCK /* 30 seconds   */
#define  TCP_MAXRETRANSMIT             20  * TCP_CLK_TCK /* 20 seconds  */
#define  TCP_MINRETRANSMIT             (TCP_CLK_TCK/2)   /* 0.5 sec     */
#define  TCP_MAXPERSIST                60  * TCP_CLK_TCK /* 1 min       */
#define  TCP_USER_TIMEOUT_TICKS        60  * TCP_CLK_TCK /* 1 min       */
#define  TCP_FINWAIT2_RECOVERY_TIMEOUT 60  * TCP_CLK_TCK /* 1 min       */
#define  TCP_KA_DEF_MAIN_TMR_TICKS     7200 * TCP_CLK_TCK /* 2 hours */
#define  TCP_KA_DEF_RETRANS_TMR_TICKS  75   * TCP_CLK_TCK /* 75 seconds */
#define  INACTIVE                      0                              

#define TCP_RETRANSMIT_RFC2988      3 * TCP_CLK_TCK
#define G_CLOCK                     (TCP_CLK_TCK/10)
#endif   /*TCP_TIMER_H*/
