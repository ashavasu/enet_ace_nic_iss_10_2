/** $Id: tcpopt.c,v 1.20 2014/03/03 12:12:33 siva Exp $ */

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                       */
/*                                                                           */
/*    FILE NAME        : tcpopt.c                                   */
/*    PRINCIPAL AUTHOR    : Aricent Inc.                    */
/*      SUBSYSTEM NAME        :                                            */
/*      MODULE NAME        :                                            */
/*    LANGUAGE        : ANSI-C                                     */
/*    TARGET ENVIRONMENT    : Any                                        */
/*    DATE OF FIRST RELEASE    :                                            */
/*    DESCRIPTION        : This file contains routines related to     */
/*                                options                                    */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*    CHANGE RECORD:                                 */
/*    Version        Author        Description of change             */
/*****************************************************************************/
/*                     Vinay/15.12.99      Created                           */
/*****************************************************************************/

#include "tcpinc.h"
#include "cryartdf.h"
#include "arMD5_api.h"

/* Buffer used to calculate TCP-AO MAC */
UINT1               au1TxTcpAoBuffer[TCP_AO_MAC_BUF_LEN];
UINT1               au1RxTcpAoBuffer[TCP_AO_MAC_BUF_LEN];
/*****************************************************************************/
/* Function     : FsmProcessOption                                           */
/* Description  : This routine is called from LINFsmProcessData() to      */
/*                process the options in the incoming segment. If the        */
/*                FSM state is TCPS_SYNRCVD or TCPS_SYNSENT,                 */
/*                FsmEnableOption() is invoked to enable the preconfigured   */
/*          option if it is present in the incoming segment. Otherwise,*/
/*          the incoming options are processed if they are enabled ie. */
/*          they have been negotiated during connection establishment. */
/*          TS processing   : update u4TcbTSRecent.                    */
/*                SACK processing : invoke FsmUpdateSackBitInRxmtQ()         */
/*          BW processing   : it is already processed in               */
/*                                  LINFarSetRwsize()                     */
/*          NAK processing  : invoke OsmSendSegFromRxmtQ() if NAK is   */
/*                                  configured. There is no negotiation for  */
/*                                  NAK during connection establishment.     */
/*                                                                           */
/* Input        : pTcb       : ptr to the corresponding TCB                  */
/*                pRcvSeg    : ptr to the received segment                   */
/*                                                                           */
/* Output       : u2TcbOptionsFlag, after calling FsmEnableOption()          */
/*                u4TcbTSRecent, as a part of TS option processing           */
/*          i4TcbSackLargestSeq, after calling                         */
/*                               FsmUpdateSackBitInRxmtQ()             */
/*                                                                           */
/* Returns      : TCP_OK, if option processing is successful                     */
/*                ERR, otherwise                                             */
/*                                                                           */
/*****************************************************************************/

INT1
FsmProcessOption (tTcb * pTcb, tIpTcpHdr * pRcvSeg)
{
    tTcpHdr            *pTcpHdr;
    UINT4               u4OptOffset;
    UINT1               u1TcpHdrLen, u1OptLen, u1OptByteCnt;
    UINT1               u1Option;
    UINT4               u4TcbIndex;
    UINT4               u4Context = 0;

    u4Context = pTcb->u4Context;
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering FsmProcessOption. \n");
    pTcpHdr = pRcvSeg->tcp;

    u1TcpHdrLen = (UINT1) (((pTcpHdr->u1Hlen) >> TCP_VER_SHIFT) <<
                           TCP_BYTES_FOR_SHORT);
    if (u1TcpHdrLen == TCP_HEADER_LENGTH)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmProcessOption. Returned OK.\n");
        return (TCP_OK);
    }

    u4TcbIndex = GET_TCB_INDEX (pTcb);
    u1OptLen = (UINT1) (u1TcpHdrLen - TCP_HEADER_LENGTH);
    u4OptOffset = ZERO;

    if ((GetTCBstate (u4TcbIndex) == TCPS_SYNRCVD) ||
        (GetTCBstate (u4TcbIndex) == TCPS_SYNSENT))
    {
        /* note : state is changed to TCPS_SYNRCVD in FsmListen() */

        if (FsmEnableOption (pTcb, pTcpHdr) == ERR)
        {
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                         "TCP", "Exit from FsmProcessOption. Returned ERR.\n");
            return ERR;
        }
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from FsmProcessOption. Returned OK.\n");
        return TCP_OK;
    }

    for (u1OptByteCnt = INIT_COUNT; u1OptByteCnt < u1OptLen;)
    {
        u1Option = pTcpHdr->au1TcpOpts[u4OptOffset];
        u4OptOffset++;

        switch (u1Option)
        {
            case TCP_EOOL:
                u1OptByteCnt = u1OptLen;
                break;            /*no more option */

            case TCP_NOOP:
                u1OptByteCnt++;
                break;

            case TCP_MSS:
            case TCP_BWP:
            case TCP_SACKP:
                /* these options are processed during connection establishment */
                TCP_MOD_TRC (u4Context,
                             TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                             "Exit from FsmProcessOption. Returned ERR.\n");
                return ERR;

            case TCP_TS:
                u1Option = pTcpHdr->au1TcpOpts[u4OptOffset];    /* extract the 
                                                                   length */
                if (u1Option != TS_OPT_LEN)
                {
                    TCP_MOD_TRC (u4Context,
                                 TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                                 "Exit from FsmProcessOption. Returned ERR.\n");
                    return ERR;
                }
                if ((GetTCBstate (u4TcbIndex) != TCPS_SYNRCVD) &&
                    (GetTCBstate (u4TcbIndex) != TCPS_SYNSENT))
                {

                    UINT4               u4TSVal;
                    INT4                i4RcvSegSeq;

                    i4RcvSegSeq = OSIX_NTOHL (pRcvSeg->tcp->i4Seq);

                    MEMCPY (&u4TSVal, &pTcpHdr->au1TcpOpts[u4OptOffset +
                                                           TCP_ONE],
                            sizeof (UINT4));

                    u4TSVal = OSIX_NTOHL (u4TSVal);

                    if ((GetTCBOptFlag (u4TcbIndex) & TCBOPF_TS) &&
                        (u4TSVal >= GetTCBTSRecent (u4TcbIndex)) &&
                        ((SEQCMP (i4RcvSegSeq, GetTCBTSLastAckSent (u4TcbIndex))
                          <= ZERO)))
                    {
                        SetTCBTSRecent (u4TcbIndex, u4TSVal);
                    }
                }

                u1OptByteCnt += TS_OPT_LEN;
                u4OptOffset += (TS_OPT_LEN - TCP_ONE);
                break;

            case TCP_SACK:
                u1Option = pTcpHdr->au1TcpOpts[u4OptOffset];    /* extract the 
                                                                   length */

                if ((GetTCBstate (u4TcbIndex) == TCPS_ESTABLISHED) ||
                    (GetTCBstate (u4TcbIndex) == TCPS_FINWAIT1))
                {

                    /* process if SACK is enabled */
                    if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_SACK)
                    {
                        /* update SACKed bit in RxmtQ */
                        FsmUpdateSackBitInRxmtQ (pTcb, pTcpHdr, u4OptOffset);
                    }

                }

                u1OptByteCnt += u1Option;
                u4OptOffset += (u1Option - TCP_ONE);
                break;

            case TCP_BW:
                /* Incoming BW option is already processed in LINFarSetRwsize() */
                u1Option = pTcpHdr->au1TcpOpts[u4OptOffset];    /* extract the 
                                                                   length */
                if (u1Option != BW_OPT_LEN)
                {
                    TCP_MOD_TRC (u4Context,
                                 TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                                 "Exit from FsmProcessOption. Returned ERR.\n");
                    return ERR;
                }
                u1OptByteCnt += BW_OPT_LEN;
                u4OptOffset += (BW_OPT_LEN - TCP_ONE);
                break;

            case TCP_NAK:
                u1Option = pTcpHdr->au1TcpOpts[u4OptOffset];    /* extract the 
                                                                   length */
                if (u1Option != NAK_OPT_LEN)
                {
                    TCP_MOD_TRC (u4Context,
                                 TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                                 "Exit from FsmProcessOption. Returned ERR.\n");
                    return ERR;
                }

                if (gpTcpContext[u4Context]->TcpConfigParms.i4TcpAckOpt ==
                    TCP_NAKCONF)
                {

                    UINT1               u1NoOfSegNaked;
                    UINT1               u1NoOfRxmtNode;
                    UINT1               u1NoOfNodesTobTx;
                    UINT1               u1NodeCnt;
                    tRxmt              *pRxmtQHdr;

                    u1NoOfSegNaked = pTcpHdr->au1TcpOpts[u4OptOffset + 5];
                    pRxmtQHdr = GetTCBRxmtQHdr (u4TcbIndex);
                    u1NoOfRxmtNode = ZERO;

                    if (pRxmtQHdr != NULL)
                    {
                        u1NoOfRxmtNode = RxmtQNodeCnt (u4Context, pRxmtQHdr);
                    }

                    u1NoOfNodesTobTx = MINIMUM (u1NoOfSegNaked, u1NoOfRxmtNode);

                    for (u1NodeCnt = INIT_COUNT; u1NodeCnt < u1NoOfNodesTobTx;
                         u1NodeCnt++)
                    {
                        OsmSendSegFromRxmtQ (pTcb,
                                             (UINT1) (u1NodeCnt + TCP_ONE));
                    }
                }

                u1OptByteCnt += NAK_OPT_LEN;
                u4OptOffset += (NAK_OPT_LEN - TCP_ONE);
                break;

            default:
                u1Option = pTcpHdr->au1TcpOpts[u4OptOffset];
                if (u1Option < TCP_TWO)
                {
                    TCP_MOD_TRC (u4Context,
                                 TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                                 "Exit from FsmProcessOption. Returned ERR.\n");
                    return ERR;
                }
                u1OptByteCnt += u1Option;
                u4OptOffset += u1Option - TCP_ONE;
                break;            /* Ignore unknown option */
        }
    }
    TCP_MOD_TRC (u4Context,
                 TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FsmProcessOption. Returned OK.\n");
    return TCP_OK;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FsmEnableOption                                            */
/*                                                                           */
/* Description  : This routine is called from FsmProcessOption() to enable   */
/*                the preconfigured options if it is present in the incoming */
/*                segment.                                                   */
/*                                                                           */
/* Input        : pTcb    : ptr to the corresponding TCB                     */
/*                pTcpHdr : ptr to the TCP header of the incoming segment.   */
/*                                                                           */
/* Output       : u2TcbOptionsFlag                                           */
/*                u4TSVal                                                    */
/*                                                                           */
/* Returns      : TCP_OK, if the incoming options are successfully processed     */
/*                ERR, otherwise                                             */
/*                                                                           */
/*****************************************************************************/

INT1
FsmEnableOption (tTcb * pTcb, tTcpHdr * pTcpHdr)
{
    UINT4               u4OptOffset;
    UINT1               u1TcpHdrLen, u1OptLen, u1OptByteCnt;
    UINT1               u1Option;
    UINT4               u4TcbIndex;
    UINT4               u4Context = 0;
    UINT1              *pTcpOpt;
    UINT4               u4WsVal;
    UINT2               u2RcvWndScale = 0;
    UINT4               u4RcvWnd = 0;

    u4Context = pTcb->u4Context;
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering FsmEnableOption.\n");
    /*
     * Extract the options.Check if the corresponding options are configured.
     * If yes, update TCB by enabling that option.
     * In case of TS, update u4TcbTSRecent with the value in the incoming segment
     */

    u4TcbIndex = GET_TCB_INDEX (pTcb);

    pTcpOpt = pTcpHdr->au1TcpOpts;
    u1TcpHdrLen = (UINT1) (((pTcpHdr->u1Hlen) >> TCP_VER_SHIFT)
                           << TCP_BYTES_FOR_SHORT);

    u1OptLen = (UINT1) (u1TcpHdrLen - TCP_HEADER_LENGTH);
    u4OptOffset = ZERO;

    for (u1OptByteCnt = INIT_COUNT; u1OptByteCnt < u1OptLen;)
    {
        u1Option = pTcpOpt[u4OptOffset];
        u4OptOffset++;

        switch (u1Option)
        {
            case TCP_EOOL:
                u1OptByteCnt = u1OptLen;
                break;            /*no more option */

            case TCP_NOOP:
                u1OptByteCnt++;
                break;

            case TCP_WS:        /* Window scale Option */
                u1Option = pTcpOpt[u4OptOffset];
                if (u1Option != WS_OPT_LEN)
                {
                    TCP_MOD_TRC (u4Context,
                                 TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                                 "Exit from FsmEnableOption. Returned ERR.\n");
                    return ERR;
                }

                /* enable WS Option if configured */

                u4OptOffset++;

                u4WsVal = pTcpOpt[u4OptOffset];

                if (gpTcpContext[u4Context]->TcpConfigParms.i4TcpWSOpt ==
                    TCP_WSCONF)
                {
                    SetTCBOptFlag (u4TcbIndex, TCBOPF_WS);
                    if (u4WsVal <= TCP_MAX_WINDOW_SCALE)
                    {
                        SetTCBSendWindowScale (u4TcbIndex, (UINT2) u4WsVal);

                        /* Only if Window scale option is supported by the other end,
                         * set the recv Window scale to the value configured.
                         * Recv Window scale is set to 0 when a connection is
                         * initialized */

                        /* Calculate the Window scale that we need to advertise
                         * so that we can send our Window Size with least number of shifts
                         * which alows maximum accuracy of data */

                        u4RcvWnd = TCP_MAX_RECV_BUF_SIZE;

                        SetTCBRecvWindowScale (u4TcbIndex, u2RcvWndScale);
                    }
                }

                break;

            case TCP_MSS:
                /* MSS option has already been processed in LINInpCheckOptions() */
                u1Option = pTcpOpt[u4OptOffset];    /* extract the length */

                if (u1Option != MSS_OPT_LEN)
                {
                    TCP_MOD_TRC (u4Context,
                                 TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                                 "Exit from FsmEnableOption. Returned ERR.\n");
                    return ERR;
                }
                u4OptOffset += (MSS_OPT_LEN - TCP_ONE);
                u1OptByteCnt += MSS_OPT_LEN;
                break;

            case TCP_BWP:
                u1Option = pTcpOpt[u4OptOffset];    /* extract the length */

                if (u1Option != BW_PERM_OPT_LEN)
                {
                    TCP_MOD_TRC (u4Context,
                                 TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                                 "Exit from FsmEnableOption. Returned ERR.\n");
                    return ERR;
                }

                /* enable BW Option if configured */
                /* If incoming SYN has advertised Big Window 
                 * Permitted option, do not enable it on the new TCB
                 * if MD5 option is enabled */
                if ((gpTcpContext[u4Context]->TcpConfigParms.i4TcpBWOpt ==
                     TCP_BWCONF) && (GetTCBmd5keylen (u4TcbIndex) == TCP_ZERO))
                {
                    SetTCBOptFlag (u4TcbIndex, TCBOPF_BW);
                }
                u1OptByteCnt += BW_PERM_OPT_LEN;
                u4OptOffset += (BW_PERM_OPT_LEN - TCP_ONE);
                break;

            case TCP_SACKP:
                u1Option = pTcpOpt[u4OptOffset];    /* extract the length */

                if (u1Option != SACK_PERM_OPT_LEN)
                {
                    TCP_MOD_TRC (u4Context,
                                 TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                                 "Exit from FsmEnableOption. Returned ERR.\n");
                    return ERR;
                }

                /* enable SACK Option if configured */
                /* If incoming SYN has advertised SACK Permitted option, 
                 * do not enable it on the new TCB if MD5 option is enabled */
                if ((gpTcpContext[u4Context]->TcpConfigParms.i4TcpAckOpt ==
                     TCP_SACKCONF)
                    && (GetTCBmd5keylen (u4TcbIndex) == TCP_ZERO))
                {
                    SetTCBOptFlag (u4TcbIndex, TCBOPF_SACK);
                }
                u1OptByteCnt += SACK_PERM_OPT_LEN;
                u4OptOffset += (SACK_PERM_OPT_LEN - TCP_ONE);
                break;

            case TCP_TS:
                u1Option = pTcpOpt[u4OptOffset];    /* extract the length */

                if (u1Option != TS_OPT_LEN)
                {
                    TCP_MOD_TRC (u4Context,
                                 TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                                 "Exit from FsmEnableOption. Returned ERR.\n");
                    return ERR;
                }

                /* enable TS Option if configured */
                /* By default TS Option is configured */

                if (gpTcpContext[u4Context]->TcpConfigParms.i4TcpTSOpt ==
                    TCP_TSCONF)
                {
                    UINT4               u4TSVal;

                    SetTCBOptFlag (u4TcbIndex, TCBOPF_TS);

                    /* extract SEG.TSVal & update TCB */
                    u4TSVal = *((UINT4 *) (VOID *)
                                (&pTcpOpt[u4OptOffset + TCP_ONE]));
                    u4TSVal = OSIX_NTOHL (u4TSVal);
                    SetTCBTSRecent (u4TcbIndex, u4TSVal);
                }
                u1OptByteCnt += TS_OPT_LEN;
                u4OptOffset += (TS_OPT_LEN - TCP_ONE);

                break;

            default:
                u1Option = pTcpOpt[u4OptOffset];
                if (u1Option < OPT_SUB_LEN)
                {
                    TCP_MOD_TRC (u4Context,
                                 TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                                 "Exit from FsmEnableOption. Returned ERR.\n");
                    return ERR;
                }
                u1OptByteCnt += u1Option;
                u4OptOffset += u1Option - TCP_ONE;
                break;            /* Ignore unknown option */
        }
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC,
                 "TCP", "Exit from FsmEnableOption. Returned OK.\n");
    UNUSED_PARAM (u4RcvWnd);
    return TCP_OK;
}

/*****************************************************************************/
/* Function     : OsmAddOption                                               */
/* Description  : This routine adds the enabled options to the outgoing      */
/*                segments. It is called from LINFarSendAck(),               */
/*                OsmSendSegment() and OsmSendSegFromRxmtQ().                */
/* Input        : pTcb - ptr to the corresponding TCB                        */
/*                pSendSeg - ptr to the outgoing segment                     */
/*                u2UpperWindow - upper 15 bits of the big window            */
/* Output       : pSendSeg                                                   */
/*                i4TcbTSLastAckSent                                         */
/*                u2TcbFlags                                                 */
/* Returns      : TCP_OK                                                     */
/*****************************************************************************/
INT1
OsmAddOption (tTcb * pTcb, tIpTcpHdr * pSendSeg, UINT2 u2UpperWindow)
{
    UINT4               u4TcbIndex;
    UINT4               u4TcpOptIndex;
    UINT4               u4Context = 0;
    UINT1              *pTcpOpt;

    u4TcbIndex = GET_TCB_INDEX (pTcb);
    u4Context = GetTCBContext (u4TcbIndex);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering OsmAddOption. \n");
    u4TcpOptIndex = ZERO;

    pTcpOpt = pSendSeg->tcp->au1TcpOpts;

    if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_TS)
    {
        UINT4               u4TSVal;
        UINT4               u4TSecr;

        /*
         * add the following values to the TS field
         * u4TSVal is in seconds
         */
        TCP_GET_SYS_TIME ((tOsixSysTime *) & u4TSVal);
        u4TSecr = GetTCBTSRecent (u4TcbIndex);

        pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;
        pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;

        pTcpOpt[u4TcpOptIndex++] = TCP_TS;
        pTcpOpt[u4TcpOptIndex++] = TS_OPT_LEN;
        *((UINT4 *) (VOID *) (&pTcpOpt[u4TcpOptIndex])) = OSIX_HTONL (u4TSVal);
        u4TcpOptIndex += TCP_TSTAMP_FIELD_LEN;
        *((UINT4 *) (VOID *) (&pTcpOpt[u4TcpOptIndex])) = OSIX_HTONL (u4TSecr);
        u4TcpOptIndex += TCP_TSTAMP_FIELD_LEN;

        /* modify the TCP Header Length */
        pSendSeg->tcp->u1Hlen += TCP_HDR_LEN_WITH_TSTAMP;

        /* update TCB */
        SetTCBTSLastAckSent (u4TcbIndex, GetTCBrnext (u4TcbIndex));
    }

    if (gpTcpContext[u4Context]->TcpConfigParms.i4TcpAckOpt == TCP_NAKCONF)
    {
        if (GetTCBflags (u4TcbIndex) & TCBF_GENNAK)
        {

            /* 
             * include NAK option with the following values
             * [ RCV.NXT, greatest integer((i4TcbOutOrderSeq - RCV.NXT)/MSS) ] 
             */

            INT4                i4OutOrderSeq;
            INT4                i4RcvNext;
            UINT2               u2Mss;
            INT4                i4NakedSegCnt;

            i4NakedSegCnt = ZERO;
            i4OutOrderSeq = GetTCBOutOrderSeq (u4TcbIndex);
            i4RcvNext = GetTCBrnext (u4TcbIndex);
            u2Mss = (UINT2) (GetTCBsmss (u4TcbIndex));
            GREATEST_INTEGER (i4OutOrderSeq, i4RcvNext, u2Mss, i4NakedSegCnt);

            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;

            pTcpOpt[u4TcpOptIndex++] = TCP_NAK;
            pTcpOpt[u4TcpOptIndex++] = NAK_OPT_LEN;
            *((UINT4 *) (VOID *) (&pTcpOpt[u4TcpOptIndex])) =
                OSIX_HTONL (i4RcvNext);
            u4TcpOptIndex += TCP_TSTAMP_FIELD_LEN;
            pTcpOpt[u4TcpOptIndex++] = (UINT1) i4NakedSegCnt;

            /* modify the TCP Header Length */
            pSendSeg->tcp->u1Hlen += TCP_HDR_LEN_WITH_NACK;

            /* reset TCBF_GENNAK flag */
            SetTCBflags_AND (u4TcbIndex, ~TCBF_GENNAK);
        }
    }

    if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_BW)
    {

        /* include BW option with the value of u2UpperWindow */
        pTcpOpt[u4TcpOptIndex++] = TCP_BW;
        pTcpOpt[u4TcpOptIndex++] = BW_OPT_LEN;
        *((UINT2 *) (VOID *) (&pTcpOpt[u4TcpOptIndex])) =
            OSIX_HTONS (u2UpperWindow);
        u4TcpOptIndex += 2;

        /* modify the TCP Header Length */
        pSendSeg->tcp->u1Hlen += TCP_HDR_LEN_WITH_BIGWIN;
    }

    if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_SACK)
    {
        if (GetTCBSackHdr (u4TcbIndex) != NULL)
        {

            /*
             * extract the blocks from the SLL & add as many blocks as possible.
             * Note that it shudnt exceed 40 bytes(rfc 2018)
             */

            UINT4               u4SackLenIndex;
            UINT1               u1NoOfSackBlksAdded;
            tSack              *pNext;

            pNext = pTcb->pTcbSackHdr;
            u1NoOfSackBlksAdded = ZERO;

            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;

            pTcpOpt[u4TcpOptIndex++] = TCP_SACK;
            u4SackLenIndex = u4TcpOptIndex;
            u4TcpOptIndex++;

            /* modify the TCP Header Length */
            pSendSeg->tcp->u1Hlen += TCP_HDR_LEN_WITH_SACK;

            while ((u4TcpOptIndex < (MAX_OPT_LEN - TCP_SACK_FIELD_LEN))
                   && (pNext != NULL))
            {
                *((UINT4 *) (VOID *) (&pTcpOpt[u4TcpOptIndex])) =
                    OSIX_HTONL (pNext->i4Seq);
                u4TcpOptIndex += TCP_SACK_FIELD_LEN;
                *((UINT4 *) (VOID *) (&pTcpOpt[u4TcpOptIndex])) =
                    OSIX_HTONL ((pNext->i4Seq + pNext->u2Len));
                u4TcpOptIndex += TCP_SACK_FIELD_LEN;
                pNext = pNext->pNext;
                u1NoOfSackBlksAdded++;

                /* modify the TCP Header Length */
                pSendSeg->tcp->u1Hlen += TCP_HDR_LEN_WITH_SACKOPT;
            }

            /* update the SACK length field with 8n+2 */
            pTcpOpt[u4SackLenIndex] = (UINT1) ((SACK_BLOCK_LEN *
                                                u1NoOfSackBlksAdded) +
                                               OPT_SUB_LEN);
        }
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from OsmAddOption. Returned OK.\n");
    return TCP_OK;
}

/*****************************************************************************/
/* Function     : OsmGetTCBOptLen                                            */
/* Description  : This routine calculates the option length to allocate      */
/*                buffer of suitable size for the outgoing segment. It is    */
/*                called from LINFarSendAck(), OsmSendSegment() and          */
/*                OsmSendSegFromRxmtQ().                                     */
/* Input        : u4TcbIndex - index of the corresponding TCB                */
/* Output       : None                                                       */
/* Returns      : u1TcbOptLen, the option length                             */
/*****************************************************************************/

INT1
OsmGetTCBOptLen (UINT4 u4TcbIndex)
{
    UINT4               u4Context;
    UINT1               u1TcbOptLen;
    UINT1               u1NoOfSackNodes;

    u4Context = GetTCBContext (u4TcbIndex);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering OsmGetTCBOptLen. \n");
    u1TcbOptLen = ZERO;

    switch (GetTCBstate (u4TcbIndex))
    {
        case TCPS_CLOSED:
        case TCPS_LISTEN:
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                         "TCP", "Exit from OsmGetTCBOptLen. Returned ERR.\n");
            return ERR;

        case TCPS_SYNSENT:
            if (GetTCBcode (u4TcbIndex) & TCPF_SYN)
            {
                u1TcbOptLen += MSS_OPT_LEN;

                if (gpTcpContext[u4Context]->TcpConfigParms.i4TcpWSOpt ==
                    TCP_WSCONF)
                {
                    /* HSTCP Changes for Window scale option
                     * should be sent only in SYN segments*/
                    u1TcbOptLen += TCP_ONE * NOOP_LEN;
                    u1TcbOptLen += WS_OPT_LEN;
                }

            }

            if (gpTcpContext[u4Context]->TcpConfigParms.i4TcpTSOpt ==
                TCP_TSCONF)
            {
                if (GetTCBcode (u4TcbIndex) & TCPF_SYN)
                {
                    u1TcbOptLen += TCP_TWO * NOOP_LEN;
                    u1TcbOptLen += TS_OPT_LEN;
                }
                else if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_TS)
                {
                    u1TcbOptLen += TCP_TWO * NOOP_LEN;
                    u1TcbOptLen += TS_OPT_LEN;
                }
            }
            /* Add length of SACK Permitted Option only if MD5 option
             * is not enabled */
            if ((gpTcpContext[u4Context]->TcpConfigParms.i4TcpAckOpt ==
                 TCP_SACKCONF) && (GetTCBcode (u4TcbIndex) & TCPF_SYN)
                && (GetTCBmd5keylen (u4TcbIndex) == TCP_ZERO))
            {
                u1TcbOptLen += TCP_WORDS_FOR_SACK_BLOCK * NOOP_LEN;
                u1TcbOptLen += SACK_PERM_OPT_LEN;
            }

            /*
             * The client sends an ACK for SYN+ACK from the server. 
             * It will now include only the enabled option
             */
            /* Add length of Big Window permitted/Big Window option
             * only if MD5 option is not enabled */
            if ((gpTcpContext[u4Context]->TcpConfigParms.i4TcpBWOpt ==
                 TCP_BWCONF) && (GetTCBmd5keylen (u4TcbIndex) == TCP_ZERO))
            {
                if (GetTCBcode (u4TcbIndex) & TCPF_SYN)
                {
                    u1TcbOptLen += TCP_TWO * NOOP_LEN;
                    u1TcbOptLen += BW_PERM_OPT_LEN;
                }
                else
                {
                    if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_BW)
                    {
                        u1TcbOptLen += BW_OPT_LEN;
                    }
                }
            }

            /* Add length of MD5 option if enabled */
            if (GetTCBmd5keylen (u4TcbIndex) != TCP_ZERO)
            {
                u1TcbOptLen = u1TcbOptLen + ((UINT1) (TCP_TWO * NOOP_LEN));
                u1TcbOptLen += MD5_OPT_LEN;
            }
            if (GetTCBTcpAoEnabled (u4TcbIndex))
            {
                /*u1TcbOptLen += TCP_TWO * NOOP_LEN; */
                u1TcbOptLen = ((UINT1) (u1TcbOptLen + TCPAO_OPT_LEN));
            }
            break;

        case TCPS_SYNRCVD:
            u1TcbOptLen += MSS_OPT_LEN;

            if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_TS)
            {
                u1TcbOptLen += TCP_TWO * NOOP_LEN;
                u1TcbOptLen += TS_OPT_LEN;
            }

            if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_SACK)
            {
                u1TcbOptLen += TCP_TWO * NOOP_LEN;
                u1TcbOptLen += SACK_PERM_OPT_LEN;
            }

            if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_BW)
            {
                u1TcbOptLen += TCP_TWO * NOOP_LEN;
                u1TcbOptLen += BW_PERM_OPT_LEN;
            }
            if ((gpTcpContext[u4Context]->TcpConfigParms.i4TcpWSOpt ==
                 TCP_WSCONF) && (GetTCBOptFlag (u4TcbIndex) & TCBOPF_WS))
            {
                /* HSTCP Changes for Window scale option */
                u1TcbOptLen += TCP_ONE * NOOP_LEN;
                u1TcbOptLen += WS_OPT_LEN;
            }
            if (GetTCBmd5keylen (u4TcbIndex) != TCP_ZERO)
            {
                u1TcbOptLen += TCP_TWO * NOOP_LEN;
                u1TcbOptLen += MD5_OPT_LEN;
            }
            /* Add TCP-AO option if enabled */
            if (GetTCBTcpAoEnabled (u4TcbIndex))
            {
                /* u1TcbOptLen += TCP_TWO * NOOP_LEN; */
                u1TcbOptLen = ((UINT1) (u1TcbOptLen + TCPAO_OPT_LEN));
            }

            break;

        case TCPS_ESTABLISHED:
        case TCPS_FINWAIT1:
        case TCPS_FINWAIT2:
        case TCPS_CLOSEWAIT:
        case TCPS_LASTACK:
        case TCPS_CLOSING:
        case TCPS_TIMEWAIT:
            if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_TS)
            {
                u1TcbOptLen += TCP_TWO * NOOP_LEN;
                u1TcbOptLen += TS_OPT_LEN;
            }

            if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_SACK)
            {
                if (GetTCBSackHdr (u4TcbIndex) != NULL)
                {
                    u1TcbOptLen += TCP_TWO * NOOP_LEN;
                    u1NoOfSackNodes =
                        SackNodeCnt (u4Context, GetTCBSackHdr (u4TcbIndex));
                    u1TcbOptLen += (SACK_BLOCK_LEN * u1NoOfSackNodes);
                    u1TcbOptLen += OPT_SUB_LEN;
                }
            }

            if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_BW)
            {
                u1TcbOptLen += BW_OPT_LEN;
            }

            if (gpTcpContext[u4Context]->TcpConfigParms.i4TcpAckOpt ==
                TCP_NAKCONF)
            {
                if (GetTCBflags (u4TcbIndex) & TCBF_GENNAK)
                {
                    u1TcbOptLen += NOOP_LEN;
                    u1TcbOptLen += NAK_OPT_LEN;
                }
            }

            if (GetTCBmd5keylen (u4TcbIndex) != TCP_ZERO)
            {
                u1TcbOptLen += TCP_TWO * NOOP_LEN;
                u1TcbOptLen += MD5_OPT_LEN;
            }
            if (GetTCBTcpAoEnabled (u4TcbIndex))
            {
                /* u1TcbOptLen += TCP_TWO * NOOP_LEN; */
                u1TcbOptLen = ((UINT1) (u1TcbOptLen + TCPAO_OPT_LEN));
            }

            break;
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from OsmGetTCBOptLen. Returned %d\n", u1TcbOptLen);
    return u1TcbOptLen;
}

/*****************************************************************************/
/* Function     : RxmtQNodeCnt                                               */
/* Description  : It counts the number of retransmission nodes in the rxmtQ. */
/*                It is called from FsmProcessOption().                      */
/* Input        : u4Context - Context ID                                     */
/*                pRxmtQHdr - pointer to the rxmtQ header                    */
/* Output       : None                                                       */
/* Returns      : u1NoOfNodes, no. of nodes in the rxmtQ                     */
/*****************************************************************************/
UINT1
RxmtQNodeCnt (UINT4 u4Context, tRxmt * pRxmtQHdr)
{
    UINT1               u1NoOfNodes;
    tRxmt              *pNode;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering RxmtQNodeCnt. \n");
    u1NoOfNodes = ZERO;
    pNode = pRxmtQHdr;

    while (pNode != NULL)
    {
        u1NoOfNodes++;
        pNode = (tRxmt *) pNode->pNext;
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from RxmtQNodeCnt. Returned %d.\n", u1NoOfNodes);
    return u1NoOfNodes;
}

/*****************************************************************************/
/* Function     : FsmUpdateSackBitInRxmtQ                                    */
/* Description  : This routine updates the SACK bit in the rxmtQ, if the     */
/*                incoming segment contains SACK blocks. It is called from   */
/*                FsmProcessOption().                                        */
/* Input        : pTcb - ptr to the corresponding TCB                        */
/*                pTcpHdr - ptr to the TCP header                            */
/*                u4OptOffset - Options offset                               */
/* Output       : i4TcbSackLargestSeq                                        */
/* Returns      : ERR, if the SACK blocks are not proper                     */
/*                TCP_OK, otherwise                                          */
/*****************************************************************************/

INT1
FsmUpdateSackBitInRxmtQ (tTcb * pTcb, tTcpHdr * pTcpHdr, UINT4 u4OptOffset)
{
    UINT1               u1SackOptLen;
    UINT1               u1OptByteCnt;
    UINT4               u4TcbIndex;
    INT4                i4LeftEdge, i4RightEdge;
    INT4                i4StartSeq, i4EndSeq;
    UINT4               u4StartIndex, u4EndIndex;
    UINT4               u4Context;
    tRxmt              *pTemp;

    u4TcbIndex = GET_TCB_INDEX (pTcb);
    u4Context = GetTCBContext (u4TcbIndex);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering FsmUpdateSackBitInRxmtQ. \n");
    u1SackOptLen = pTcpHdr->au1TcpOpts[u4OptOffset++];

    for (u1OptByteCnt = INIT_COUNT;
         u1OptByteCnt < (u1SackOptLen - OPT_SUB_LEN);)
    {

        MEMCPY (&i4LeftEdge, &pTcpHdr->au1TcpOpts[u4OptOffset], sizeof (INT4));
        i4LeftEdge = OSIX_NTOHL (i4LeftEdge);
        u4OptOffset += TCP_SACK_FIELD_LEN;
        MEMCPY (&i4RightEdge, &pTcpHdr->au1TcpOpts[u4OptOffset], sizeof (INT4));
        i4RightEdge = OSIX_NTOHL (i4RightEdge);
        u4OptOffset += TCP_SACK_FIELD_LEN;
        u1OptByteCnt += SACK_BLOCK_LEN;

        if (SEQCMP (i4LeftEdge, i4RightEdge) > ZERO)
        {
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                         "TCP",
                         "Exit from FsmUpdateSackBitInRxmtQ. Returned ERR.\n");
            return ERR;
        }

        /* Update i4TcbSackLargestSeq */
        if (SEQCMP (i4RightEdge, GetTCBSackLargestSeq (u4TcbIndex)) > ZERO)
        {
            SetTCBSackLargestSeq (u4TcbIndex, i4RightEdge);
        }

        pTemp = pTcb->pTcbRxmtQHdr;

        while (pTemp != NULL)
        {
            u4StartIndex = pTemp->u4StartIndex;
            u4EndIndex = pTemp->u4EndIndex;
            i4StartSeq = pTemp->i4Seq;

            if (u4StartIndex <= u4EndIndex)
            {
                i4EndSeq = i4StartSeq + (u4EndIndex - u4StartIndex);
            }
            else
            {
                i4EndSeq = i4StartSeq +
                    (MAX_SEND_BUF_SIZE - u4StartIndex + u4EndIndex);
            }

            if (SEQCMP (i4LeftEdge, i4StartSeq) <= ZERO &&
                SEQCMP (i4RightEdge, i4EndSeq) >= ZERO)
            {
                /* turn ON the SACK flag */
                pTemp->u1SackFlag = TCP_TRUE;
            }
            pTemp = (tRxmt *) pTemp->pNext;
        }                        /* while */
    }                            /* for */
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FsmUpdateSackBitInRxmtQ. Returned OK.\n");
    return TCP_OK;
}

/*****************************************************************************/
/* Function     : FsmBWExtractUpperBytes                                     */
/* Description  : This routine extracts the upper 2 bytes from the incoming  */
/*                segment. It is used, along with the lower 2 bytes in the   */
/*                TCP header, to update the window size in the TCB. It is    */
/*                called from LINFarSetRwsize().                             */
/* Input        : u4Context - Context Id                                     */
/*                pTcpHdr - ptr to the TCP header                            */
/* Output       : None                                                       */
/* Returns      : the upper two bytes of the window                          */
/*                ERR, if BW option is not found                             */
/*****************************************************************************/
INT4
FsmBWExtractUpperBytes (UINT4 u4Context, tTcpHdr * pTcpHdr)
{
    UINT4               u4OptOffset;
    INT4                i4TcpOpts = 0;
    UINT1               u1TcpHdrLen, u1OptLen, u1OptByteCnt;
    UINT1               u1Option;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering FsmBWExtractUpperBytes. \n");
    u1TcpHdrLen = (UINT1) (((pTcpHdr->u1Hlen) >> TCP_VER_SHIFT)
                           << TCP_BYTES_FOR_SHORT);

    u1OptLen = (UINT1) (u1TcpHdrLen - TCP_HEADER_LENGTH);
    u4OptOffset = ZERO;

    for (u1OptByteCnt = INIT_COUNT; u1OptByteCnt < u1OptLen;)
    {
        u1Option = pTcpHdr->au1TcpOpts[u4OptOffset];
        u4OptOffset++;

        switch (u1Option)
        {
            case TCP_EOOL:
                u1OptByteCnt = u1OptLen;
                break;            /*no more option */

            case TCP_NOOP:
                u1OptByteCnt++;
                break;

            case TCP_BW:
                /* found BW option. Extract the upper 2 bytes and return */
                u1Option = pTcpHdr->au1TcpOpts[u4OptOffset];
                /* extract option length */
                if (u1Option != BW_OPT_LEN)
                {
                    TCP_MOD_TRC (u4Context,
                                 TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                                 "Exit from FsmBWExtractUpperBytes. Returned ERR.\n");
                    return ERR;
                }

                MEMCPY (&i4TcpOpts, &pTcpHdr->au1TcpOpts[u4OptOffset + 1],
                        sizeof (INT2));

                TCP_MOD_TRC (u4Context,
                             TCP_ENTRY_EXIT_TRC, "TCP",
                             "Exit from FsmBWExtractUpperBytes. Returned %d\n",
                             i4TcpOpts);
                return i4TcpOpts;

            default:
                u1Option = pTcpHdr->au1TcpOpts[u4OptOffset];
                /* extract option length */
                if (u1Option < OPT_SUB_LEN)
                {
                    TCP_MOD_TRC (u4Context,
                                 TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                                 "Exit from FsmBWExtractUpperBytes. Returned ERR.\n");
                    return ERR;
                }
                u1OptByteCnt += u1Option;
                u4OptOffset += u1Option - TCP_ONE;
                break;            /* Ignore unknown option */
        }
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                 "TCP", "Exit from FsmBWExtractUpperBytes. Returned ERR.\n");
    return ERR;
}

/*****************************************************************************/
/* Function     : OsmTurnOffSackBit                                          */
/* Description  : This routine is used to turn off the SACKed bit from all   */
/*                the nodes in the rxmtQ. It is invoked from                 */
/*                OsmRetransmit(). Refer rfc 2018.                           */
/* Input        : u4Context - Context ID                                     */
/*                pRxmtQHdr : ptr to the rxmtQ header                        */
/* Output       : None                                                       */
/* Returns      : TCP_OK                                                     */
/*****************************************************************************/

INT1
OsmTurnOffSackBit (UINT4 u4Context, tRxmt * pRxmtQHdr)
{
    tRxmt              *pTemp;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering OsmTurnOffSackBit. \n");
    pTemp = pRxmtQHdr;

    while (pTemp != NULL)
    {
        pTemp->u1SackFlag = TCP_FALSE;
        pTemp = (tRxmt *) pTemp->pNext;
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                 "TCP", "Exit from OsmTurnOffSackBit. Returned OK.\n");

    return TCP_OK;
}

/*****************************************************************************/
/* Function     : OsmGenerateNak                                             */
/* Description  : This routine is called from LINFsmUpdateTcb() when an      */
/*                out of order segment arrives. It sets the flag TCBF_NEEDOUT*/
/*                to generate the NAK option. It also sets the TCBF_GENNAK   */
/*                flag to indicate that NAK option is to be added to the     */
/*                outgoing segment.                                          */
/* Input        : pTcb - ptr to the corresponding TCB                        */
/*                i4First - value in the sequence no. field of the incoming  */
/*                    segment                                                */
/* Output       : sets u2TcbFlags to TCBF_NEEDOUT to generate the NAK        */
/*                option immediately.                                        */
/*                sets u2TcbFlags to TCBF_GENNAK to indicate that NAK        */
/*                option has to be added to the outgoing segment.            */
/*                i4TcbOutOrderSeq                                           */
/* Returns      : TCP_OK                                                     */
/*                ERR                                                        */
/*****************************************************************************/
INT1
OsmGenerateNak (tTcb * pTcb, INT4 i4First)
{
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4TcbIndex = GET_TCB_INDEX (pTcb);
    u4Context = GetTCBContext (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering OsmGenerateNak. \n");
    if (GetTCBOutOrderSeq (u4TcbIndex) == ZERO)
    {                            /*initialised when creating a TCB */

        /* first OOO segment arrival */

        SetTCBOutOrderSeq (u4TcbIndex, i4First);
        SetTCBflags (u4TcbIndex, TCBF_GENNAK);
        SetTCBflags (u4TcbIndex, TCBF_NEEDOUT);    /* Send NAK immediately */
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP", "Exit from OsmGenerateNak. Returned OK.\n");
        return TCP_OK;

    }

    if (SEQCMP (i4First, GetTCBOutOrderSeq (u4TcbIndex)) >= ZERO)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP", "Exit from OsmGenerateNak. Returned OK.\n");
        return TCP_OK;
    }
    else
    {
        SetTCBOutOrderSeq (u4TcbIndex, i4First);
        SetTCBflags (u4TcbIndex, TCBF_GENNAK);
        SetTCBflags (u4TcbIndex, TCBF_NEEDOUT);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP", "Exit from OsmGenerateNak. Returned OK.\n");
        return TCP_OK;
    }

}

/*****************************************************************************/
/* Function     : FsmSackInsert                                              */
/* Description  : This routine allocates memory for the new SACK block. It   */
/*                calls FsmSackUpdate() to coalesce the overlapping blocks   */
/*                and to maintain the SACK SLL based on the order of arrival.*/
/*                It is called from LINFsmUpdateTcb().                       */
/* Input        : pTcb             : ptr to the corresponding TCB            */
/*                i4First          : value in the sequence no field of the   */
/*                             incoming segment                              */
/*                u4Datalen        : length of data                          */
/*                u1NeedToCoalesce : flag to indicate if the incoming        */
/*                             segment is the expected one                   */
/* Output       : pTcbSackHdr                                                */
/* Returns      : TCP_OK, if the SACK updation is correct                    */
/*                ERR, otherwise                                             */
/*****************************************************************************/
INT1
FsmSackInsert (tTcb * pTcb, INT4 i4First, UINT4 u4Datalen,
               UINT1 u1NeedToCoalesce)
{
    tSack              *pSack, *pTemp;
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4TcbIndex = GET_TCB_INDEX (pTcb);
    u4Context = GetTCBContext (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering FsmSackInsert. \n");
    if ((pSack = (tSack *) MemAllocMemBlk (gTcpSackPoolId)) == NULL)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC |
                     TCP_BUF_TRC | TCP_ALL_FAIL_TRC,
                     "TCP",
                     "Exit from FsmSackInsert. Memory could not be allocated.Returned Error.\n");
        return ERR;
    }
    pSack->i4Seq = i4First;
    pSack->u2Len = (UINT2) u4Datalen;
    pSack->pNext = NULL;

    if (GetTCBSackHdr (u4TcbIndex) == NULL)
    {
        SetTCBSackHdr (u4TcbIndex, pSack);
    }
    else
    {
        pTemp = GetTCBSackHdr (u4TcbIndex);
        SetTCBSackHdr (u4TcbIndex, pSack);
        pSack->pNext = pTemp;
    }

    if (FsmSackUpdate (pTcb, u1NeedToCoalesce) != TCP_OK)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP", "Exit from FsmSackInsert. Returned Error.\n");
        return ERR;
    }

    SetTCBflags (u4TcbIndex, TCBF_NEEDOUT);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                 "TCP", "Exit from FsmSackInsert. Returned OK.\n");
    return TCP_OK;
}

/*****************************************************************************/
/* Function     : FsmSackUpdate                                              */
/* Description  : This routine checks whether the receied segments can fill  */
/*                up the holes within the SACKed blocks and then goes on to  */
/*                update it, freeing the blocks which are not required. It   */
/*                calls FsmSackUnlink() to remove the intermediate blocks    */
/*                and to free the memory. It is called from FsmSackInsert(). */
/* Input        : pTcb      : ptr to the corresponding TCB.                  */
/*                u1NeedToCoalesce : flag to indicate if the incoming segment*/
/*                                   is the expected one. The 1st block is   */
/*                                   removed if it is set.                   */
/* Output       : None                                                       */
/* Returns      : TCP_OK, if SACK SLL updation is successful                 */
/*                ERR, otherwise                                             */
/*****************************************************************************/
INT1
FsmSackUpdate (tTcb * pTcb, UINT1 u1NeedToCoalesce)
{
    tSack              *ptr;
    tSack              *pRightEdge = NULL, *pLeftEdge = NULL;
    tSack              *pSackHead;
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4TcbIndex = GET_TCB_INDEX (pTcb);
    u4Context = GetTCBContext (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering FsmSackUpdate. \n");

    pSackHead = GetTCBSackHdr (u4TcbIndex);
    ptr = pSackHead->pNext;
    pRightEdge = NULL;
    pLeftEdge = NULL;

    /* get pRightEdge & pLeftEdge */
    while (ptr != NULL)
    {
        if (SEQCMP (pSackHead->i4Seq, (ptr->i4Seq + (INT4) ptr->u2Len)) == ZERO)
        {
            pRightEdge = ptr;
        }
        if (SEQCMP ((pSackHead->i4Seq + (INT4) pSackHead->u2Len), ptr->i4Seq)
            == ZERO)
        {
            pLeftEdge = ptr;
        }
        ptr = ptr->pNext;
    }                            /* end while */

    /* no overlapping blocks */
    if (pLeftEdge == NULL && pRightEdge == NULL)
    {
        if (u1NeedToCoalesce)
        {
            SetTCBSackHdr (u4TcbIndex, pSackHead->pNext);
            MemReleaseMemBlock (gTcpSackPoolId, (UINT1 *) pSackHead);
        }
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP", "Exit from FsmSackUpdate. Returned OK.\n");
        return TCP_OK;
    }

    if (pRightEdge && pLeftEdge)
    {                            /* v need to coalesce 3 blocks */

        pSackHead->i4Seq = pRightEdge->i4Seq;
        pSackHead->u2Len += pRightEdge->u2Len + pLeftEdge->u2Len;

        /* We need to remove the links 
         * to the existing blocks pointed 
         * by pRightEdge & pLeftEdge     */

        FsmSackUnlink (u4Context, pSackHead, pRightEdge);
        if (pRightEdge != pLeftEdge)
        {
            FsmSackUnlink (u4Context, pSackHead, pLeftEdge);
        }
        if (u1NeedToCoalesce)
        {
            SetTCBSackHdr (u4TcbIndex, pSackHead->pNext);
            MemReleaseMemBlock (gTcpSackPoolId, (UINT1 *) pSackHead);
        }
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP", "Exit from FsmSackUpdate. Returned OK.\n");
        return TCP_OK;

    }
    else if (pLeftEdge)
    {
        pSackHead->u2Len += pLeftEdge->u2Len;
        FsmSackUnlink (u4Context, pSackHead, pLeftEdge);
        if (u1NeedToCoalesce)
        {
            SetTCBSackHdr (u4TcbIndex, pSackHead->pNext);
            MemReleaseMemBlock (gTcpSackPoolId, (UINT1 *) pSackHead);
        }
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP", "Exit from FsmSackUpdate. Returned OK.\n");
        return TCP_OK;
    }
    else if (pRightEdge)
    {
        pSackHead->i4Seq = pRightEdge->i4Seq;
        pSackHead->u2Len += pRightEdge->u2Len;
        FsmSackUnlink (u4Context, pSackHead, pRightEdge);
        if (u1NeedToCoalesce)
        {
            SetTCBSackHdr (u4TcbIndex, pSackHead->pNext);
            MemReleaseMemBlock (gTcpSackPoolId, (UINT1 *) pSackHead);
        }
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP", "Exit from FsmSackUpdate. Returned OK.\n");
        return TCP_OK;
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                 "TCP", "Exit from FsmSackUpdate. Returned Error.\n");
    return ERR;
}

/*****************************************************************************/
/* Function     : FsmSackUnlink                                              */
/* Description  : This routine unlinks and frees the node from the SACK SLL  */
/* Input        : u4Context - Context Id                                     */
/*                pSackHead     : ptr to the SACK SLL header                 */
/*                pNodeTobDeleted : ptr to the node to be unlinked and freed */
/* Output       : None                                                       */
/* Returns      : TCP_OK                                                     */
/*****************************************************************************/
INT1
FsmSackUnlink (UINT4 u4Context, tSack * pSackHead, tSack * pNodeTobDeleted)
{
    tSack              *ptr, *pPrev;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering FsmSackUnlink. \n");

    ptr = pSackHead;
    pPrev = ptr;
    while (ptr != NULL)
    {
        ptr = ptr->pNext;
        if (ptr == pNodeTobDeleted)
        {
            pPrev->pNext = ptr->pNext;
            break;
        }
        pPrev = ptr;
    }
    MemReleaseMemBlock (gTcpSackPoolId, (UINT1 *) pNodeTobDeleted);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                 "TCP", "Exit from FsmSackUnlink. Returned OK.\n");
    return TCP_OK;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : SackNodeCnt                                                */
/*                                                                           */
/* Description  : It counts the number of nodes in the SACK SLL. It is       */
/*                called from OsmGetTCBOptLen().                             */
/*                                                                           */
/* Input        : u4Context - Context ID                                     */
/*                pSackHdr : pointer to the SACK header                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : u1NoOfNodes, no. of nodes in the SACK SLL                  */
/*                                                                           */
/*****************************************************************************/

UINT1
SackNodeCnt (UINT4 u4Context, tSack * pSackHdr)
{
    UINT1               u1NoOfNodes;
    tSack              *pNode;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering SackNodeCnt. \n");

    u1NoOfNodes = ZERO;
    pNode = pSackHdr;

    while (pNode != NULL)
    {
        u1NoOfNodes++;
        pNode = (tSack *) pNode->pNext;
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from SackNodeCnt. Returned %d\n", u1NoOfNodes);
    return u1NoOfNodes;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FsmTSExtractTSecr                                          */
/*                                                                           */
/* Description  : This routine extracts the TSecr field of the TS option in  */
/*                the incoming segment                                       */
/*                                                                           */
/* Input        : u4Context - Context Id                                     */
/*                pTcpHdr  : ptr to the TCP header                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TSecr field of the TS option in the incoming segment       */
/*                ZERO, if TS option is not found                             */
/*                                                                           */
/*****************************************************************************/

UINT4
FsmTSExtractTSecr (UINT4 u4Context, tTcpHdr * pTcpHdr)
{
    UINT4               u4OptOffset;
    UINT4               u4TcpOpts = 0;
    UINT1               u1TcpHdrLen, u1OptLen, u1OptByteCnt;
    UINT1               u1Option;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering FsmTSExtractTSecr. \n");

    u1TcpHdrLen = (UINT1) (((pTcpHdr->u1Hlen) >> TCP_VER_SHIFT)
                           << TCP_BYTES_FOR_SHORT);

    u1OptLen = (UINT1) (u1TcpHdrLen - TCP_HEADER_LENGTH);
    u4OptOffset = ZERO;

    for (u1OptByteCnt = INIT_COUNT; u1OptByteCnt < u1OptLen;)
    {
        u1Option = pTcpHdr->au1TcpOpts[u4OptOffset];
        u4OptOffset++;

        switch (u1Option)
        {
            case TCP_EOOL:
                u1OptByteCnt = u1OptLen;
                break;            /*no more option */

            case TCP_NOOP:
                u1OptByteCnt++;
                break;

            case TCP_TS:
                /* found TS option. Extract TSecr field and return */
                u1Option = pTcpHdr->au1TcpOpts[u4OptOffset];
                /* extract option length */
                if (u1Option != TS_OPT_LEN)
                {
                    TCP_MOD_TRC (u4Context,
                                 TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                                 "Exit from FsmTSExtractTSecr. Returned Error.\n");
                    return ZERO;
                }

                MEMCPY (&u4TcpOpts, &pTcpHdr->au1TcpOpts[u4OptOffset + 5],
                        sizeof (UINT4));

                TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                             "Exit from FsmTSExtractTSecr. Returned %d\n",
                             u4TcpOpts);
                return u4TcpOpts;

            default:
                u1Option = pTcpHdr->au1TcpOpts[u4OptOffset];
                /* extract option length */
                if (u1Option < OPT_SUB_LEN)
                {
                    TCP_MOD_TRC (u4Context,
                                 TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                                 "Exit from FsmTSExtractTSecr. Returned Error.\n");
                    return ZERO;
                }
                u1OptByteCnt += u1Option;
                u4OptOffset += u1Option - TCP_ONE;
                break;            /* Ignore unknown option */
        }
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                 "TCP", "Exit from FsmTSExtractTSecr. Returned Error.\n");
    return ZERO;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : TcpOptGenerateMD5Digest                                    */
/* Description  : This routine generates MD5 digest of a segment. Called     */
/*                for all outgoing/incoming segments of an MD5 protected     */
/*                connection                                                 */
/* Inputs       : u4Context - Context Id                                     */
/*                pSegIpTcpHdr  : ptr to the IP+TCP header                   */
/*                pSeg          : ptr to the CRU buffer containing segment   */
/*                pInKey        : MD5 key for digest generation              */
/*                u1Keylen      : Length of MD5 key                          */
/* Output       : pOutDigest    : MD5 digest of segment                      */
/* Returns      : TCP_OK        : Digest is generated                        */
/*                TCP_NOT_OK       : Error during digest generation          */
/*****************************************************************************/
INT1
TcpOptGenerateMD5Digest (UINT4 u4Context, tIpTcpHdr * pSegIpTcpHdr,
                         tSegment * pSeg, UINT1 *pOutDigest, UINT1 *pInKey,
                         UINT1 u1Keylen)
{
    unArCryptoHash      md5;
    tTcp4PseudoHdr      V4PseudoHdr;
    tTcp6PseudoHdr      V6PseudoHdr;
    tTcpFixedHdr        TcpFixedHdr;
    tCRU_BUF_DATA_DESC *pDataDesc = NULL;
    UINT1              *pDataBuf = NULL;
    UINT4               u4DataLength = TCP_ZERO;
    UINT4               u4Len = TCP_ZERO;
    UINT4               u4ReadOffset = TCP_ZERO;
    UINT2               u2TCPHdrStart = TCP_ZERO;
    UINT2               u2TotLen = TCP_ZERO;    /* Total Segment Length i.e, 
                                                   TCP header + data */
    UINT1               u1TcpHlen = TCP_ZERO;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TcpOptGenerateMD5Digest. \n");

    MEMSET (&md5, TCP_ZERO, sizeof (unArCryptoHash));
    MEMSET (&V4PseudoHdr, TCP_ZERO, sizeof (tTcp4PseudoHdr));
    MEMSET (&V6PseudoHdr, TCP_ZERO, sizeof (tTcp6PseudoHdr));
    MEMSET (&TcpFixedHdr, TCP_ZERO, sizeof (tTcpFixedHdr));
    MEMSET (pOutDigest, TCP_ZERO, MD5_DIGEST_LEN);

    /* Start MD5 Context */
    arMD5_start (&md5);

    /* Forming TCP pseudo header and updating MD5 context */
    if ((IsIP6Addr (pSegIpTcpHdr->ip->SrcAddr)) ||
        (IsIP6Addr (pSegIpTcpHdr->ip->DestAddr)))
    {
        V6_ADDR_COPY (&(V6PseudoHdr.SrcAddr), &(pSegIpTcpHdr->ip->SrcAddr));
        V6_ADDR_COPY (&(V6PseudoHdr.DestAddr), &(pSegIpTcpHdr->ip->DestAddr));
        /*u2TotLen = OSIX_NTOHS (pSegIpTcpHdr->ip->u2Totlen);
           V6PseudoHdr.u4SegLen = OSIX_HTONL (u2TotLen);
         */
        V6PseudoHdr.u4SegLen = (UINT4) pSegIpTcpHdr->ip->u2Totlen;
        V6PseudoHdr.u1Padding[0] = TCP_ZERO;
        V6PseudoHdr.u1Padding[1] = TCP_ZERO;
        V6PseudoHdr.u1Padding[2] = TCP_ZERO;
        V6PseudoHdr.u1Proto = pSegIpTcpHdr->ip->u1Proto;
        u2TCPHdrStart = pSegIpTcpHdr->ip->u1VHlen;    /* No IP Header in the
                                                       OutPut Send Segment */
        arMD5_update (&md5, (UINT1 *) &V6PseudoHdr, sizeof (tTcp6PseudoHdr));
    }
    else
    {
        V4PseudoHdr.u4SrcAddr = V4_FROM_V6_ADDR (pSegIpTcpHdr->ip->SrcAddr);
        V4PseudoHdr.u4DestAddr = V4_FROM_V6_ADDR (pSegIpTcpHdr->ip->DestAddr);
        V4PseudoHdr.u1Padding = TCP_ZERO;
        V4PseudoHdr.u1Proto = pSegIpTcpHdr->ip->u1Proto;
        V4PseudoHdr.u2SegLen = pSegIpTcpHdr->ip->u2Totlen;
        u2TCPHdrStart = (UINT2) (IP_HEADER_LEN + pSegIpTcpHdr->ip->u2IpOptLen);
        arMD5_update (&md5, (UINT1 *) &V4PseudoHdr, sizeof (tTcp4PseudoHdr));
    }
    /* Forming TCP Header and updating MD5 context */
    TcpFixedHdr.u2Sport = pSegIpTcpHdr->tcp->u2Sport;
    TcpFixedHdr.u2Dport = pSegIpTcpHdr->tcp->u2Dport;
    TcpFixedHdr.i4Seq = pSegIpTcpHdr->tcp->i4Seq;
    TcpFixedHdr.i4Ack = pSegIpTcpHdr->tcp->i4Ack;
    TcpFixedHdr.u1Hlen = pSegIpTcpHdr->tcp->u1Hlen;
    TcpFixedHdr.u1Code = pSegIpTcpHdr->tcp->u1Code;
    TcpFixedHdr.u2Window = pSegIpTcpHdr->tcp->u2Window;
    TcpFixedHdr.u2Cksum = TCP_ZERO;
    TcpFixedHdr.u2Urgptr = pSegIpTcpHdr->tcp->u2Urgptr;

    arMD5_update (&md5, (UINT1 *) &TcpFixedHdr, sizeof (tTcpFixedHdr));

    /* Getting Segment Data if any and updating MD5 context */
    u2TotLen = OSIX_NTOHS (pSegIpTcpHdr->ip->u2Totlen);
    u1TcpHlen = (UINT1) (((pSegIpTcpHdr->tcp->u1Hlen) >> TCP_VER_SHIFT)
                         << TCP_BYTES_FOR_SHORT);
    u4DataLength = (UINT4) (u2TotLen - u1TcpHlen);
    u4ReadOffset = u2TCPHdrStart + u1TcpHlen;
    if (u4DataLength > TCP_ZERO)
    {
        pDataDesc = CRU_BUF_GetFirstDataDesc (pSeg);
        while (pDataDesc &&
               (u4ReadOffset > CRU_BUF_GetBlockValidByteCount (pDataDesc)))
        {
            u4ReadOffset -= CRU_BUF_GetBlockValidByteCount (pDataDesc);
            pDataDesc = CRU_BUF_GetNextDataDesc (pDataDesc);
        }
        if (pDataDesc == NULL)
        {
            TCP_MOD_TRC (u4Context,
                         TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                         "Exit from TcpOptGenerateMD5Digest. Returned Err.\n");
            return ERR;
        }

        pDataBuf = CRU_BUF_GetDataPtr (pDataDesc) + u4ReadOffset;

        while (u4DataLength > ZERO)
        {
            u4Len = (CRU_BUF_GetBlockValidByteCount (pDataDesc) - u4ReadOffset);
            u4ReadOffset = ZERO;

            u4Len = (u4Len < u4DataLength) ? u4Len : u4DataLength;
            u4DataLength -= u4Len;

            arMD5_update (&md5, (UINT1 *) pDataBuf, u4Len);

            pDataDesc = CRU_BUF_GetNextDataDesc (pDataDesc);
            if (!pDataDesc)
            {
                break;
            }
            pDataBuf = CRU_BUF_GetDataPtr (pDataDesc);
        }
    }                            /* End of - if (u4DataLength > ZERO) */

    /* Updating MD5 context with MD5 key */
    arMD5_update (&md5, pInKey, u1Keylen);
    arMD5_finish (&md5, pOutDigest);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                 "TCP", "Exit from TcpOptGenerateMD5Digest. Returned OK.\n");
    return TCP_OK;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : TcpOptGenerateTCPAOMac                                     */
/* Description  : This routine generates TCP-AO MAC of a segment. Called     */
/*                for all outgoing/incoming segments of an TCP-AO protected  */
/*                connection                                                 */
/* Inputs       : pSegIpTcpHdr  : ptr to the IP+TCP header                   */
/*                pSeg          : ptr to the CRU buffer containing segment   */
/*                pInKey        : TCP-AO key for MAC generation              */
/*                u1Keylen      : Length of TCP-AO key                       */
/*                u1TcpOptExc   : Exclude TCP options while calculating MAC  */
/*                u4Sne         : sequence number extension                  */
/* Output       : pOutMac       : TCP-AO MAC of segment                      */
/*                                                                           */
/* Returns      : TCP_OK        : MAC is generated                           */
/*                TCP_NOT_OK       : Error during MAC generation             */
/*****************************************************************************/
INT1 
     
     
     
     
     
     
     
    TcpOptGenerateTCPAOMac
    (UINT4 u4Context, tIpTcpHdr * pSegIpTcpHdr, tSegment * pSeg, UINT1 *pOutMac,
     UINT1 *pInKey, UINT1 u1Keylen, UINT1 u1TcpOptExc, UINT4 u4Sne,
     UINT1 *pu1TcpAoBuffer)
{

    tTcp4PseudoHdr      V4PseudoHdr;
    tTcp6PseudoHdr      V6PseudoHdr;
    tTcpFixedHdr        TcpFixedHdr;
    tCRU_BUF_DATA_DESC *pDataDesc = NULL;
    UINT1              *pDataBuf = NULL;
    UINT4               u4DataLength = TCP_ZERO;
    UINT4               u4Len = TCP_ZERO;
    UINT4               u4ReadOffset = TCP_ZERO;
    UINT2               u2TCPHdrStart = TCP_ZERO;
    UINT2               u2TotLen = TCP_ZERO;    /* Total Segment Length i.e, 
                                                   TCP header + data */
    UINT1               u1TcpHlen = TCP_ZERO;
    UINT4               u4TcpAoBufLen = TCP_ZERO;

    UINT1               au1TcpOptBuf[MAX_TCP_OPT_LEN];
    UINT4               u4TcpOptLen = TCP_ZERO;
    UINT4               u4OPtOff = TCP_ZERO;
    UINT1               u1Temp = TCP_ZERO;
    UINT1               u1Option = TCP_ZERO;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TcpOptGenerateTCPAOMac. \n");

    MEMSET (&V4PseudoHdr, TCP_ZERO, sizeof (tTcp4PseudoHdr));
    MEMSET (&V6PseudoHdr, TCP_ZERO, sizeof (tTcp6PseudoHdr));
    MEMSET (&TcpFixedHdr, TCP_ZERO, sizeof (tTcpFixedHdr));
    MEMSET (pOutMac, TCP_ZERO, TCPAO_MAC_LEN);

    /* copy sequence number extension */
    MEMCPY (&pu1TcpAoBuffer[u4TcpAoBufLen], (UINT1 *) &u4Sne, sizeof (UINT4));
    u4TcpAoBufLen += sizeof (UINT4);

    /* Forming TCP pseudo header and updating TCP-AO buffer */
    if (IsIP6Addr (pSegIpTcpHdr->ip->SrcAddr))
    {
        V6_ADDR_COPY (&(V6PseudoHdr.SrcAddr), &(pSegIpTcpHdr->ip->SrcAddr));
        V6_ADDR_COPY (&(V6PseudoHdr.DestAddr), &(pSegIpTcpHdr->ip->DestAddr));
/*      u2TotLen = OSIX_NTOHS (pSegIpTcpHdr->ip->u2Totlen);
        V6PseudoHdr.u4SegLen = OSIX_HTONL ((UINT4) u2TotLen);
*/
        V6PseudoHdr.u4SegLen = (UINT4) pSegIpTcpHdr->ip->u2Totlen;
        V6PseudoHdr.u1Padding[0] = TCP_ZERO;
        V6PseudoHdr.u1Padding[1] = TCP_ZERO;
        V6PseudoHdr.u1Padding[2] = TCP_ZERO;
        V6PseudoHdr.u1Proto = pSegIpTcpHdr->ip->u1Proto;
        MEMCPY (&pu1TcpAoBuffer[u4TcpAoBufLen], (UINT1 *) &V6PseudoHdr,
                sizeof (tTcp6PseudoHdr));
        u4TcpAoBufLen += sizeof (tTcp6PseudoHdr);
        u2TCPHdrStart = pSegIpTcpHdr->ip->u1VHlen;    /* No IP Header in the
                                                       OutPut Send Segment */

    }
    else
    {
        V4PseudoHdr.u4SrcAddr = V4_FROM_V6_ADDR (pSegIpTcpHdr->ip->SrcAddr);
        V4PseudoHdr.u4DestAddr = V4_FROM_V6_ADDR (pSegIpTcpHdr->ip->DestAddr);
        V4PseudoHdr.u1Padding = TCP_ZERO;
        V4PseudoHdr.u1Proto = pSegIpTcpHdr->ip->u1Proto;
        V4PseudoHdr.u2SegLen = pSegIpTcpHdr->ip->u2Totlen;
        MEMCPY (&pu1TcpAoBuffer[u4TcpAoBufLen], (UINT1 *) &V4PseudoHdr,
                sizeof (tTcp4PseudoHdr));
        u4TcpAoBufLen += sizeof (tTcp4PseudoHdr);
        u2TCPHdrStart = (UINT2) (IP_HEADER_LEN + pSegIpTcpHdr->ip->u2IpOptLen);

    }
    /* Forming TCP Header and updating MD5 context */
    TcpFixedHdr.u2Sport = pSegIpTcpHdr->tcp->u2Sport;
    TcpFixedHdr.u2Dport = pSegIpTcpHdr->tcp->u2Dport;
    TcpFixedHdr.i4Seq = pSegIpTcpHdr->tcp->i4Seq;
    TcpFixedHdr.i4Ack = pSegIpTcpHdr->tcp->i4Ack;
    TcpFixedHdr.u1Hlen = pSegIpTcpHdr->tcp->u1Hlen;
    TcpFixedHdr.u1Code = pSegIpTcpHdr->tcp->u1Code;
    TcpFixedHdr.u2Window = pSegIpTcpHdr->tcp->u2Window;
    TcpFixedHdr.u2Cksum = TCP_ZERO;
    TcpFixedHdr.u2Urgptr = pSegIpTcpHdr->tcp->u2Urgptr;

    MEMCPY (&pu1TcpAoBuffer[u4TcpAoBufLen], (UINT1 *) &TcpFixedHdr,
            sizeof (tTcpFixedHdr));
    u4TcpAoBufLen += sizeof (tTcp4PseudoHdr);

    /* copy tcp option */
    u4TcpOptLen = (UINT1) (((pSegIpTcpHdr->tcp->u1Hlen) >> TCP_VER_SHIFT)
                           << TCP_SHIFT_BYTE_TO_WORD);
    u4TcpOptLen -= TCP_HEADER_LENGTH;
    MEMCPY (au1TcpOptBuf, pSegIpTcpHdr->tcp->au1TcpOpts, u4TcpOptLen);

    /* go through the options and find the tcp-ao option */
    /* and set the MAC field to zero. */

    u1Temp = INIT_COUNT;
    while ((u1Temp < u4TcpOptLen) && (u4OPtOff == TCP_ZERO))
    {
        u1Option = au1TcpOptBuf[u1Temp];
        u1Temp++;

        switch (u1Option)
        {
            case TCP_EOOL:
                u1Temp = ((UINT1) u4TcpOptLen);
                break;
            case TCP_NOOP:
                break;
            case TCP_AO:
                /* Validate the Option Length field to be 16 */
                u1Option = au1TcpOptBuf[u1Temp];
                if (u1Option != TCPAO_OPT_LEN)
                {
                    return TCP_NOT_OK;
                }
                u4OPtOff = ((UINT4) (u1Temp - TCP_ONE));
                u1Temp = (UINT1) (u1Temp + (u1Option - TCP_ONE));

                /* Set TCP-AO MAC field to zero */
                MEMSET (&au1TcpOptBuf[u4OPtOff + TCPAO_OPT_HDR_LEN], 0,
                        TCPAO_MAC_LEN);

                break;
            default:
                u1Option = au1TcpOptBuf[u1Temp];
                if (u1Option < TCP_MIN_OPT_LENGTH)
                {
                    u1Option = TCP_MIN_OPT_LENGTH;
                }
                u1Temp = (UINT1) (u1Temp + (u1Option - TCP_ONE));
                break;
        }
    }
    if ((u4OPtOff == TCP_ZERO) && (u4TcpOptLen != TCPAO_OPT_LEN))
    {
        /* TCP-AO option not found */
        return TCP_NOT_OK;
    }
    if (u1TcpOptExc)
    {
        /* MAC need to be calculated on TCP-AO alone */
        MEMCPY (&pu1TcpAoBuffer[u4TcpAoBufLen], &au1TcpOptBuf[u4OPtOff],
                TCPAO_OPT_LEN);
        u4TcpAoBufLen += TCPAO_OPT_LEN;

    }
    else
    {

        /* Calculate MAC including all options */
        MEMCPY (&pu1TcpAoBuffer[u4TcpAoBufLen], &au1TcpOptBuf[TCP_ZERO],
                u4TcpOptLen);
        u4TcpAoBufLen += u4TcpOptLen;

    }

    /* Getting Segment Data if any and updating TCP-AO data buffer */
    u2TotLen = OSIX_NTOHS (pSegIpTcpHdr->ip->u2Totlen);
    u1TcpHlen = (UINT1) (((pSegIpTcpHdr->tcp->u1Hlen) >> TCP_VER_SHIFT)
                         << TCP_BYTES_FOR_SHORT);
    u4DataLength = (UINT4) (u2TotLen - u1TcpHlen);
    u4ReadOffset = ((UINT4) (u2TCPHdrStart + u1TcpHlen));
    if (u4DataLength > TCP_ZERO)
    {
        pDataDesc = CRU_BUF_GetFirstDataDesc (pSeg);
        while (pDataDesc &&
               (u4ReadOffset > CRU_BUF_GetBlockValidByteCount (pDataDesc)))
        {
            u4ReadOffset -= CRU_BUF_GetBlockValidByteCount (pDataDesc);
            pDataDesc = CRU_BUF_GetNextDataDesc (pDataDesc);
        }
        if (pDataDesc == NULL)
        {
            TCP_MOD_TRC (u4Context,
                         TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                         "Exit from TcpOptGenerateTCPAOMac. Returned Err.\n");
            return ERR;
        }

        pDataBuf = CRU_BUF_GetDataPtr (pDataDesc) + u4ReadOffset;

        while (u4DataLength > ZERO)
        {
            u4Len = (CRU_BUF_GetBlockValidByteCount (pDataDesc) - u4ReadOffset);
            u4ReadOffset = ZERO;

            u4Len = (u4Len < u4DataLength) ? u4Len : u4DataLength;
            u4DataLength -= u4Len;

            /* Copy data on the tcp segment to the buffer */
            MEMCPY (&pu1TcpAoBuffer[u4TcpAoBufLen], (UINT1 *) pDataBuf, u4Len);
            u4TcpAoBufLen += u4Len;

            pDataDesc = CRU_BUF_GetNextDataDesc (pDataDesc);
            if (!pDataDesc)
            {
                break;
            }
            pDataBuf = CRU_BUF_GetDataPtr (pDataDesc);
        }
    }                            /* End of - if (u4DataLength > ZERO) */

    /* unsigned int i;
       printf("\n-----In Key -------------------------------------key length %d\n", u1Keylen);
       for (i = 0; i < u1Keylen; i++)
       printf(" %x", pInKey[i]); */
    /*printf("\n-----Buffer  -----------------------------------Buffer length %d\n", u4TcpAoBufLen);
       for (i = 0; i < u4TcpAoBufLen; i++)
       printf(" %x", au1TcpAoBuffer[i]); */

    /* Calculate TCP-AO MAC */
    arHmac_Sha1 (pInKey, u1Keylen, pu1TcpAoBuffer, ((INT4) u4TcpAoBufLen),
                 pOutMac);

    /*printf("\n-----Out Mac  -----------------------------------\n");
       for (i = 0; i < 20; i++)
       printf(" %x", pOutMac[i]); */

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                 "TCP", "Exit from TcpOptGenerateTCPAOMac. Returned OK.\n");
    return TCP_OK;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : TcpGenerateTCPAOTrafficKey                                 */
/* Description  : This routine generates TCP-AO MAC of a segment.            */
/*                connection                                                 */
/* Inputs       : u4Index       : TCB Index                                  */
/*                u1keyId       : Send KeyId of the MKT                      */
/*                u1TrfKeyFlg   : Indicates which Traffic Key to be generated*/
/* Output       :               : Traffic Key will be generated and stored   */
/*                                  in the corresponding MKT in TCB          */
/*                                                                           */
/* Returns      : TCP_OK        : Traffic Key generated                      */
/*                TCP_NOT_OK    : Error during Traffic Key Generation        */
/*****************************************************************************/

INT1
TcpGenerateTCPAOTrafficKey (UINT4 u4Index, UINT1 u1keyId, UINT1 u1TrfKeyFlg)
{
    UINT1               u1MktInd = GetMktIndexFrmSendId (u4Index, u1keyId);
    UINT1               u1KdfBufLen = TCP_ZERO;
    UINT1               au1Key[BGP4_TCPAO_PWD_SIZE];
    UINT1               au1TrfcKey[TCPAO_TRAFFIC_KEY_MAXLEN];
    UINT1               u1KeyLen = TCP_ZERO;
    UINT2               u2Port = TCP_ZERO;
    UINT4               u4Seq = TCP_ZERO;
    UINT4               u4Context;
    tIpAddr             TmpIp;
    UINT1               au1TcpAoKdfBuf[TCP_AO_TRFC_BUF_LEN];

    UINT2               u2OutLen =
        ((TCPAO_TRAFFIC_KEY_MAXLEN) * sizeof (UINT1));
    u4Context = GetTCBContext (u4Index);

    if (TCPAO_MKT_INI_IND <= u1MktInd)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP",
                     "Exit from TcpGenerateTCPAOTrafficKey. Returned Not OK.\n");
        return TCP_NOT_OK;
    }

    /*
     * Input:     ( i || Label || Context || Output_Length)
     *     i = 1  Label - "TCP-AO", Output_Length - 160 bits
     *
     *  Context -
     *                                S-IP S-port S-ISN D-IP D-port D-ISN
     *      ----------------------------------------------------------------
     *     Send_SYN_traffic_key       l-IP l-port l-ISN r-IP r-port 0
     *     Receive_SYN_traffic_key    r-IP r-port r-ISN l-IP l-port 0
     *     Send_other_traffic_key     l-IP l-port l-ISN r-IP r-port r-ISN
     *     Receive_other_traffic_key  r-IP r-port r-ISN l-IP l-port l-ISN
     *    
     */

    au1TcpAoKdfBuf[u1KdfBufLen] = 1;
    u1KdfBufLen++;

    MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], "TCP-AO", STRLEN ("TCP-AO"));
    u1KdfBufLen = (UINT1) (u1KdfBufLen + STRLEN ("TCP-AO"));

    if (IsIP6Addr (GetTCBlip (u4Index)))
    {
        if ((u1TrfKeyFlg == TCPAO_SND_TRFKEY) ||
            (u1TrfKeyFlg == TCPAO_SND_SYN_TRFKEY))
        {
            V6_HTONL (&TmpIp, &(GetTCBlip (u4Index)));
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&TmpIp), 16);
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 16);
            u2Port = OSIX_HTONS (GetTCBlport (u4Index));
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u2Port), 2);
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 2);
            u4Seq = OSIX_HTONL ((UINT4) GetTCBiss (u4Index));
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u4Seq), 4);
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 4);

            V6_HTONL (&TmpIp, &(GetTCBrip (u4Index)));
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&TmpIp), 16);
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 16);
            u2Port = OSIX_HTONS (GetTCBrport (u4Index));
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u2Port), 2);
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 2);
            u4Seq = OSIX_HTONL (GetRcvIsn (u4Index));
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u4Seq), 4);
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 4);
        }
        else
        {
            V6_HTONL (&TmpIp, &(GetTCBrip (u4Index)));
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&TmpIp), 16);
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 16);
            u2Port = OSIX_HTONS (GetTCBrport (u4Index));
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u2Port), 2);
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 2);
            u4Seq = OSIX_HTONL (GetRcvIsn (u4Index));
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u4Seq), 4);
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 4);

            V6_HTONL (&TmpIp, &(GetTCBlip (u4Index)));
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&TmpIp), 16);
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 16);
            u2Port = OSIX_HTONS (GetTCBlport (u4Index));
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u2Port), 2);
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 2);
            u4Seq = OSIX_HTONL ((UINT4) GetTCBiss (u4Index));
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u4Seq), 4);
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 4);
        }
    }
    else
    {
        /*printf ("IsIP6Addr else flag %d\n", u1TrfKeyFlg); */
        if ((u1TrfKeyFlg == TCPAO_SND_TRFKEY) ||
            (u1TrfKeyFlg == TCPAO_SND_SYN_TRFKEY))
        {
            u4Seq = OSIX_HTONL (GetTCBv4lip (u4Index));
            /* printf("S IP %x  ",u4Seq ); */
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u4Seq), 4);
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 4);
            u2Port = OSIX_HTONS (GetTCBlport (u4Index));
            /* printf("S Port %x ", u2Port); */
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u2Port), 2);
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 2);
            u4Seq = OSIX_HTONL ((UINT4) GetTCBiss (u4Index));
            /*printf(" S seq %x ", u4Seq); */
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u4Seq), 4);
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 4);

            u4Seq = OSIX_HTONL (GetTCBv4rip (u4Index));
            /* printf("D IP %x  ",u4Seq ); */
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u4Seq), 4);
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 4);
            u2Port = OSIX_HTONS (GetTCBrport (u4Index));
            /*printf("D Port %x ", u2Port); */
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u2Port), 2);
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 2);
            u4Seq = OSIX_HTONL (GetRcvIsn (u4Index));
            /* printf("seq %x ", u4Seq); */
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u4Seq), 4);
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 4);
        }
        else
        {
            u4Seq = OSIX_HTONL (GetTCBv4rip (u4Index));
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u4Seq), 4);
            /* printf("S IP %x  ",u4Seq ); */
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 4);
            u2Port = OSIX_HTONS (GetTCBrport (u4Index));
            /* printf("S Port %x ", u2Port); */
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u2Port), 2);
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 2);
            u4Seq = OSIX_HTONL (GetRcvIsn (u4Index));
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u4Seq), 4);
            /*printf(" S seq %x ", u4Seq); */
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 4);

            u4Seq = OSIX_HTONL (GetTCBv4lip (u4Index));
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u4Seq), 4);
            /* printf("D IP %x  ",u4Seq ); */
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 4);
            u2Port = OSIX_HTONS (GetTCBlport (u4Index));
            /* printf("D Port %x ", u2Port); */
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u2Port), 2);
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 2);
            u4Seq = OSIX_HTONL ((UINT4) GetTCBiss (u4Index));
            /* printf(" D  seq %x ", u4Seq); */
            MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u4Seq), 4);
            u1KdfBufLen = (UINT1) (u1KdfBufLen + 4);
        }
    }

    u2Port = OSIX_HTONS (u2OutLen);

    MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) &u2Port, sizeof (UINT2));
    u1KdfBufLen = (UINT1) (u1KdfBufLen + sizeof (UINT2));

    GetMktMstKey (u4Index, u1MktInd, au1Key);
    u1KeyLen = GetMktPasswLen (u4Index, u1MktInd);

    /*int i; */

    /*printf ("\n----- key ------ key length %d\n", u1KeyLen);
       for (i = 0; i < u1KeyLen; i++)
       printf(" %x ", au1Key[i]);

       printf ("\n----- buf  ------ buf length %d\n", u1KdfBufLen);
       for (i = 0; i < u1KdfBufLen; i++)
       printf(" %x ", au1TcpAoKdfBuf[i]); */

    arHmac_Sha1 (au1Key, u1KeyLen, au1TcpAoKdfBuf, u1KdfBufLen, au1TrfcKey);

    /*printf ("\n----- trafic key  ------ buf \n");
       for (i = 0; i < 20; i++)
       printf(" %x ", au1TrfcKey[i]);

       printf("\n calc trf key u4Index %d mktindex %d ", u4Index, u1MktInd); */
    /* Update Traffic Key to the MKT */

    if (TCPAO_SND_SYN_TRFKEY == u1TrfKeyFlg)
    {
        SetMktSynKey (u4Index, u1MktInd, au1TrfcKey);
        /*MEMSET (au1TrfcKey, 0, 20);  to remove
           printf ("\n----- get trafic key  ------ buf \n");
           GetMktSynKey(u4Index, u1MktInd, au1TrfcKey);
           for (i = 0; i < 20; i++)
           printf(" %x ", au1TrfcKey[i]); */
    }
    else if (TCPAO_SND_TRFKEY == u1TrfKeyFlg)
    {
        SetMktSndTrfcKey (u4Index, u1MktInd, au1TrfcKey);
        /* MEMSET (au1TrfcKey, 0, 20);  to remove
           printf ("\n----- get trafic key  ------ buf \n");
           GetMktSndTrfcKey(u4Index, u1MktInd, au1TrfcKey);
           for (i = 0; i < 20; i++)
           printf(" %x ", au1TrfcKey[i]); */
    }
    else
    {
        SetMktRcvTrfcKey (u4Index, u1MktInd, au1TrfcKey);
        /* MEMSET (au1TrfcKey, 0, 20);  to remove
           printf ("\n----- get trafic key  ------ buf \n");
           GetMktRcvTrfcKey(u4Index, u1MktInd, au1TrfcKey);
           for (i = 0; i < 20; i++)
           printf(" %x ", au1TrfcKey[i]); */
    }
    /* printf ("\n #############################################\n"); */

    return TCP_OK;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : TcpGenerateTCPAOSynRcvTrafficKey                           */
/* Description  : This routine generates Receive  Syn Traffic Key            */
/*                of a incoming syn segment.                                 */
/*                                                                           */
/* Inputs       : SrcAddr       : Source Address of the packet               */
/*                DstAddr       : Destination Address of the packet          */
/*                SPort         : Source port                                */
/*                DPort         : Destination port                           */
/*                u4Seq         : Sequence number of the received packet     */
/*                pMKey         : Master Key                                 */
/*                u1KeyLen      : Length of Master Key                       */
/*                                                                           */
/* Output       :               : Traffic Key will be generated and stored   */
/*                                in pTkey                                   */
/*                                                                           */
/* Returns      : TCP_OK        : Traffic Key generated                      */
/*                TCP_NOT_OK    : Error during Traffic Key Generation        */
/*****************************************************************************/

/* TODO pass algo */
INT1
TcpGenerateTCPAOSynRcvTrafficKey (tIpAddr SrcAddr, tIpAddr DstAddr,
                                  UINT2 SPort, UINT2 DPort, UINT4 u4Seq,
                                  UINT1 *pMKey, UINT1 u1KeyLen, UINT1 *pTkey)
{
    UINT1               u1KdfBufLen = TCP_ZERO;
    UINT2               u2Temp = TCP_ZERO;

    UINT2               u2OutLen =
        ((TCPAO_TRAFFIC_KEY_MAXLEN) * sizeof (UINT1));
    UINT1               au1TcpAoKdfBuf[TCP_AO_TRFC_BUF_LEN];

    /*printf ("generate syn receive traffic key #############\n"); */
    /*printf("SIP %x %x DIP  %x %x sport %x dport %x seq %x   \n",
       SrcAddr.u4_addr[0],  SrcAddr.u4_addr[3], DstAddr.u4_addr[0],  DstAddr.u4_addr[3], SPort, DPort, u4Seq); */
    /*
     * Input:     ( i || Label || Context || Output_Length)
     *     i = 1  Label - "TCP-AO", Output_Lengt - 160 bits
     *
     *  Context -
     *                                S-IP S-port S-ISN D-IP D-port D-ISN
     *      ----------------------------------------------------------------
     *     Send_SYN_traffic_key       l-IP l-port l-ISN r-IP r-port 0
     *     Receive_SYN_traffic_key    r-IP r-port r-ISN l-IP l-port 0
     *     Send_other_traffic_key     l-IP l-port l-ISN r-IP r-port r-ISN
     *     Receive_other_traffic_key  r-IP r-port r-ISN l-IP l-port l-ISN
     *    
     */

    au1TcpAoKdfBuf[u1KdfBufLen] = 1;    /* i = 1 */
    u1KdfBufLen++;

    MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], "TCP-AO", STRLEN ("TCP-AO"));
    u1KdfBufLen = (UINT1) (u1KdfBufLen + STRLEN ("TCP-AO"));

    if (IsIP6Addr (SrcAddr))
    {

        MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&SrcAddr), 16);
        u1KdfBufLen = (UINT1) (u1KdfBufLen + 16);
        MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&SPort), 2);
        u1KdfBufLen = (UINT1) (u1KdfBufLen + 2);
        MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u4Seq), 4);
        u1KdfBufLen = (UINT1) (u1KdfBufLen + 4);

        MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&DstAddr), 16);
        u1KdfBufLen = (UINT1) (u1KdfBufLen + 16);
        MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&DPort), 2);
        u1KdfBufLen = (UINT1) (u1KdfBufLen + 2);
        u4Seq = 0;
        MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u4Seq), 4);
        u1KdfBufLen = (UINT1) (u1KdfBufLen + 4);

    }
    else
    {

        MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen],
                (UINT1 *) &(V4_FROM_V6_ADDR (SrcAddr)), 4);
        u1KdfBufLen = (UINT1) (u1KdfBufLen + 4);
        MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&SPort), 2);
        u1KdfBufLen = (UINT1) (u1KdfBufLen + 2);
        MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u4Seq), 4);
        u1KdfBufLen = (UINT1) (u1KdfBufLen + 4);

        MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen],
                (UINT1 *) &(V4_FROM_V6_ADDR (DstAddr)), 4);
        u1KdfBufLen = (UINT1) (u1KdfBufLen + 4);
        MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&DPort), 2);
        u1KdfBufLen = (UINT1) (u1KdfBufLen + 2);
        u4Seq = 0;
        MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) (&u4Seq), 4);
        u1KdfBufLen = (UINT1) (u1KdfBufLen + 4);

    }

    u2Temp = OSIX_HTONS (u2OutLen);

    /* Copy Length */
    MEMCPY (&au1TcpAoKdfBuf[u1KdfBufLen], (UINT1 *) &u2Temp, sizeof (UINT2));
    u1KdfBufLen = (UINT1) (u1KdfBufLen + sizeof (UINT2));

    arHmac_Sha1 (pMKey, u1KeyLen, au1TcpAoKdfBuf, u1KdfBufLen, pTkey);

    /*int i;
       printf ("\n-----  key  ------ keylen %d \n", u1KeyLen);
       for (i = 0; i < u1KeyLen; i++)
       printf(" %x ", pMKey[i]);
       printf ("\n-----  buf   ------ buflenlen %d \n", u1KdfBufLen);
       for (i = 0; i < u1KdfBufLen; i++)
       printf(" %x ", au1TcpAoKdfBuf[i]);
       printf ("\n-----  tr key   ------ \n");
       for (i = 0; i < 20; i++)
       printf(" %x ", pTkey[i]);
       printf ("\n#############################################\n"); */

    return TCP_OK;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : TcpAoGetMktIndexFrmSendId                                  */
/* Description  : This routine gets the Index of a MKT on providing          */
/*                SendKeyId                                                  */
/* Inputs       : u4Index       : TCB Index                                  */
/*                u1SndKeyId    : SendKeyId of a MKT                         */
/*                                                                           */
/* Output       :               : Finds MKT index corresponding to the       */
/*                                SendKeyId and returns the value            */
/*                                                                           */
/* Returns      : MKT Index         : Valid MKT INdex(0 to 2)                */
/*                TCPAO_MKT_INI_IND : MKT not found for the SendKeyId        */
/*****************************************************************************/

UINT1
TcpAoGetMktIndexFrmSendId (UINT4 u4Index, UINT1 u1SndKeyId)
{
    if (TCBtable[u4Index].u1ActiveInd < TCPAO_MKT_INI_IND &&
        (GetMktByIndx (u4Index, TCBtable[u4Index].u1ActiveInd).u1SendKeyId) ==
        u1SndKeyId)
    {
        return TCBtable[u4Index].u1ActiveInd;
    }
    if (TCBtable[u4Index].u1NewInd < TCPAO_MKT_INI_IND &&
        (GetMktByIndx (u4Index, TCBtable[u4Index].u1NewInd).u1SendKeyId) ==
        u1SndKeyId)
    {
        return TCBtable[u4Index].u1NewInd;
    }
    if (TCBtable[u4Index].u1BackUpInd < TCPAO_MKT_INI_IND &&
        (GetMktByIndx (u4Index, TCBtable[u4Index].u1BackUpInd).u1SendKeyId) ==
        u1SndKeyId)
    {
        return TCBtable[u4Index].u1BackUpInd;
    }
    return TCPAO_MKT_INI_IND;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : TcpAoGetMktIndexFromRcvId                                  */
/* Description  : This routine gets the Index of a MKT on providing          */
/*                ReceiveKeyId                                               */
/* Inputs       : u4Index       : TCB Index                                  */
/*                u1RcvKeyId    : ReceiveKeyId of a MKT                      */
/*                                                                           */
/* Output       :               : Finds MKT index corresponding to the       */
/*                                ReceiveKeyId and returns the value         */
/*                                                                           */
/* Returns      : MKT Index         : Valid MKT INdex(0 to 2)                */
/*                TCPAO_MKT_INI_IND : MKT not found for the ReceiveKeyId     */
/*****************************************************************************/

UINT1
TcpAoGetMktIndexFromRcvId (UINT4 u4Index, UINT1 u1RcvKeyId)
{
    if (TCBtable[u4Index].u1ActiveInd < TCPAO_MKT_INI_IND &&
        (GetMktByIndx (u4Index, TCBtable[u4Index].u1ActiveInd).
         u1ReceiveKeyId) == u1RcvKeyId)
    {
        return TCBtable[u4Index].u1ActiveInd;
    }
    if (TCBtable[u4Index].u1NewInd < TCPAO_MKT_INI_IND &&
        (GetMktByIndx (u4Index, TCBtable[u4Index].u1NewInd).u1ReceiveKeyId) ==
        u1RcvKeyId)
    {
        return TCBtable[u4Index].u1NewInd;
    }
    if (TCBtable[u4Index].u1BackUpInd < TCPAO_MKT_INI_IND &&
        (GetMktByIndx (u4Index, TCBtable[u4Index].u1BackUpInd).
         u1ReceiveKeyId) == u1RcvKeyId)
    {
        return TCBtable[u4Index].u1BackUpInd;
    }
    return TCPAO_MKT_INI_IND;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : TcpOptFillTcpAoFields                                     */
/* Description  : This routine generates TCP-AO MAC of a segment and     */
/*                fills TCP-AO MAC and other TCP-AO fields in the  */
/*                buffer                                                 */
/* Inputs       : pSegIpTcpHdr  : ptr to the IP+TCP header                   */
/*                pSeg          : ptr to the CRU buffer containing segment   */
/*                        :               */
/*                                                                           */
/* Output       :        :                                                   */
/*                                                                           */
/* Returns      : TCP_OK        : MAC is generated                           */
/*                TCP_NOT_OK       : Error during MAC generation             */
/*****************************************************************************/
INT1 
     
     
     
     
     
     
     
    TcpOptFillTcpAoFields
    (UINT4 u4TcbIndex, tIpTcpHdr * pSegIpTcpHdr, tSegment * pSeg,
     UINT1 *pTcpOpt)
{

    UINT1               au1TcpAoMac[TCPAO_MAC_MAX_OP_LEN];
    UINT1               au1TcpTrfcKey[TCPAO_TRAFFIC_KEY_MAXLEN];
    UINT4               u4TcpOptIndex = TCP_ZERO;
    UINT4               u4Context = GetTCBContext (u4TcbIndex);

    MEMSET (au1TcpAoMac, TCP_ZERO, TCPAO_MAC_MAX_OP_LEN);
    MEMSET (au1TcpTrfcKey, TCP_ZERO, TCPAO_MAC_MAX_OP_LEN);
    /* MAC is calculated including the TCP-AO option also */
    /* MAC field in the TCP-AO option will be set to zero */

    /*pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;
       pTcpOpt[u4TcpOptIndex++] = TCP_NOOP; */
    pTcpOpt[u4TcpOptIndex++] = TCP_AO;
    pTcpOpt[u4TcpOptIndex++] = TCPAO_OPT_LEN;
    pTcpOpt[u4TcpOptIndex++] = GetTCBTcpAoCurrKey (u4TcbIndex);
    pTcpOpt[u4TcpOptIndex++] = GetTCBTcpAoRNextKey (u4TcbIndex);
    if ((GetRcvIsn (u4TcbIndex)) != TCP_ZERO)
    {
        /*printf("receive isn != 0 getting Snd traffic key fill MAC\n");
           printf(" fill tcpao u4index %d mktindex %d ", u4TcbIndex, (GetTCBTcpAoActiveInd(u4TcbIndex))); */
        GetMktSndTrfcKey (u4TcbIndex, (GetTCBTcpAoActiveInd (u4TcbIndex)),
                          au1TcpTrfcKey);
    }
    else
    {
        /*printf("receive isn = 0 getting syn traffic key fill MAC\n"); */
        GetMktSynKey (u4TcbIndex, (GetTCBTcpAoActiveInd (u4TcbIndex)),
                      au1TcpTrfcKey);
    }
    /*if ((au1TcpTrfcKey[0] == 0) || (au1TcpTrfcKey[1] == 0))
       {
       printf ("\n fill tcp ao traffic key zero $$$ \n");
       printf ("send key  key %d receive  key %d ******* \n",
       TCBtable[u4TcbIndex].TcpAoAuthMktTCBList[0].au1TrafficKeySend[0],
       TCBtable[u4TcbIndex].TcpAoAuthMktTCBList[0].au1TrafficKeyReceive[0]);
       }
       else
       {
       printf ("\n fill tcp ao traffic key not zero ### \n");
       } */
    if (TcpOptGenerateTCPAOMac (u4Context, pSegIpTcpHdr, pSeg, au1TcpAoMac,
                                au1TcpTrfcKey, TCPAO_TRAFFIC_KEY_MAXLEN,
                                GetMktTcpOptIgn (u4TcbIndex,
                                                 (GetTCBTcpAoActiveInd
                                                  (u4TcbIndex))),
                                OSIX_HTONL (GetTcpAoSendSne (u4TcbIndex)),
                                au1TxTcpAoBuffer) == TCP_OK)
    {
        MEMCPY ((pTcpOpt + u4TcpOptIndex), au1TcpAoMac, TCPAO_MAC_LEN);
    }
    else
    {
        TCP_MOD_TRC (u4Context,
                     TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC | TCP_BUF_TRC, "TCP", "Exit from \
                TcpOptFillTcpAoFields. TCP-AO MAC generation failed for \
                Conn - %d.\n", u4TcbIndex);
        return TCP_NOT_OK;
    }
    return TCP_OK;
}
