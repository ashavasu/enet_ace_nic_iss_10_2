/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: tcposmfn.c,v 1.31 2017/09/13 13:34:08 siva Exp $
 *
 * Description: 
 *
 *******************************************************************/
/* SOURCE FILE HEADER
 *
 * ----------------------------------------------------------------------------
 *  FILE NAME             : tcposmfn.c
 * 
 *  PRINCIPAL AUTHOR      : Ramesh M.S
 *
 *  SUBSYSTEM NAME        : TCP
 *
 *  MODULE NAME           : Output State Machine
 *
 *  LANGUAGE              : C
 *
 *  TARGET ENVIRONMENT    : Any
 *
 *  DATE OF FIRST RELEASE :
 * 
 *  DESCRIPTION           : This file contains functions that are used by the
 *                          OSM module.
 *
 * ---------------------------------------------------------------------------
 *
 *
 *  CHANGE RECORD :
 *
 * ---------------------------------------------------------------------------
 *   VERSION        AUTHOR/DATE           DESCRIPTION OF CHANGE
 * ---------------------------------------------------------------------------
 *     1.7          Rebecca Rufus         Enabled the sending of PUSH Data.
 *     1.8          Rebecca Rufus         The source and destination IP 
 *                  12-Oct-'98.           addresses of RESET packets were 
 *                                        filled in incorrectly.This is   
 *                                        corrected Refer Problen Id 8.    
 *                                        Corrected the filling in of the 
 *                                        protocol Id in RESET packets.    
 *                                        Refer Problem Id 15.             
 * ---------------------------------------------------------------------------
 *                  Vinay/29.11.99       +Modified OsmSendSegment() to add the
 *                                        options during connection 
 *                                        establishment and data transfer. The 
 *                                        transmitted segments are added to the
 *                                        retransmission queue. The 
 *                                        modifications are placed under the 
 *                                        compilation switch ST_ENH. 
* ------------------------------------------------------------------------*/
/*     1.10     Saravanan.M    Implementation of                           */
/*              Purush            - Non Blocking Connect                   */
/*                                - Non Blocking Accept                    */
/*                                - IfMsg Interface between TCP and IP     */
/*                                - One Queue per Socket                   */
/* ------------------------------------------------------------------------*/

#include "tcpinc.h"

/*************************************************************************/
/*  Function Name   : OsmComputeSegmentLen                               */
/*  Input(s)        : ptcb - Pointer to the tcb                          */
/*                    u1Rexmt - flag to indiacte If it is a rxmtion      */
/*                    pOff - Offset                                      */
/*  Output(s)       : None                                               */
/*  Returns         : TCP_OK/ERR                                         */
/*  Description     : It is responsible for computing the length of      */
/*                    the segment.                                       */
/*  CALLING CONDITION : None                                             */
/*  GLOBAL VARIABLES AFFECTED                                            */
/*************************************************************************/
UINT4
OsmComputeSegmentLen (tTcb * ptcb, UINT1 u1Rexmt, UINT4 *pOff)
{
    UINT4               u4DataLength;
    UINT4               u4WinSize;
    UINT4               TcbIndex;
    UINT4               u4Context;

    TcbIndex = GET_TCB_INDEX (ptcb);
    u4Context = GetTCBContext (TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering OsmComputeSegmentLen. \n");

    if (u1Rexmt || (GetTCBcode (TcbIndex) & TCPF_SYN))
    {
        *pOff = ZERO;
    }
    else
    {
        *pOff = (UINT4) GetTCBsnext (TcbIndex) - (UINT4) GetTCBsuna (TcbIndex);
    }

    if (!u1Rexmt && GetTCBcode (TcbIndex) & TCPF_FIN)
    {
        (*pOff)--;
    }
    u4DataLength = GetTCBsbcount (TcbIndex) - *pOff;

    u4WinSize = GetTCBswindow (TcbIndex) + (UINT4) GetTCBlwack (TcbIndex);

    if (u1Rexmt)
    {
        u4WinSize -= (UINT4) GetTCBsuna (TcbIndex);
    }
    else
    {
        u4WinSize -= (UINT4) GetTCBsnext (TcbIndex);
    }

    u4DataLength = MINIMUM (u4DataLength, GetTCBswindow (TcbIndex));
    u4DataLength = MINIMUM (u4DataLength, u4WinSize);
    u4DataLength = MINIMUM (u4DataLength, GetTCBsmss (TcbIndex));
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from OsmComputeSegmentLen. Returned %d\n",
                 MINIMUM (u4DataLength, GetTCBrmss (TcbIndex)));

    return MINIMUM (u4DataLength, GetTCBrmss (TcbIndex));
}

/*************************************************************************/
/*  Function Name   : */
/*  Input(s)        : */
/*  Output(s)       : */
/*  Returns         : */
/*  Description     : */
/*  CallingCondition: */
/*************************************************************************/

/*------------------------------------------------------------------
RETURN TCP_OK/ERR
FUNCTION
It is responsible for  filling the IP options.
The allocation of cru_buf should take care of the options as well.
CALLING CONDITION
None
GLOBAL VARIABLES AFFECTED
TCBtable

--------------------------------------------------------------------*/
INT1
OsmSendSegment (UINT4 u4TcbIndex, UINT1 u1Rexmt)
{
    tTcb               *ptcb;
    tIpTcpHdr           SendSeg;
    tIpTcpHdr          *pSendSeg;
    tIpHdr             *pIpHdr;
    tTcpHdr            *pTcpHdr = NULL;
    tSegment           *pSegOut;
    UINT1              *pBuf;
    UINT1               au1IpTcpHdr[sizeof (tIpHdr) + sizeof (tTcpHdr)];
    UINT4               u4DataLength, u4CruBufSize, u4Off, u4NewData, u4Buflen;
    UINT4               u4RcvWnd;
    UINT4               u4Context = 0;
    INT4                i4SeqNum;
    UINT2               u2DataOffset, u2Up, u2LowerWindow;
    UINT4               u4Index;
    UINT1               u1CodeValue, u1IpOptLen;
    UINT1               u1IpHdrSize;
    INT1                i1TCPOptLen;
    UINT1               u1IPTCPHdrSize, u1TCPHdrSize;
    UINT2               u2UpperWindow;
    tIpAddr             SrcAddr, DestAddr;
    UINT1               u1Ip6HdrSize;
    UINT1               u1IpToSend;
#ifdef IP6_WANTED
    tHlToIp6Params      Ip6SendParms;
#endif
    UINT1               au1Md5Digest[MD5_DIGEST_LEN];
    UINT1              *pTcpOpt = NULL;
    UINT4               u4TcpOptIndex = TCP_ZERO;
    UINT1               u1TCPHdrSizePrev = TCP_ZERO;

    ptcb = &TCBtable[u4TcbIndex];
    u4Context = GetTCBContext (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering OsmSendSegment. \n");
    MEMSET (au1IpTcpHdr, TCP_ZERO, (sizeof (tIpHdr) + sizeof (tTcpHdr)));
    u4DataLength = OsmComputeSegmentLen (ptcb, u1Rexmt, &u4Off);
    /*Size of pure data, it does not include the TCP & IP headers */
    SrcAddr = GetTCBlip (u4TcbIndex);
    DestAddr = GetTCBrip (u4TcbIndex);
    if ((IsIP6Addr (SrcAddr)) || (IsIP6Addr (DestAddr)))
    {
        u1IpToSend = (UINT1) IP6;
        u1IpOptLen = TCP_ZERO;
        u1IpHdrSize = TCP_ZERO;
    }
    else
    {
        u1IpToSend = (UINT1) IP4;
        u1IpOptLen = OsmGetIpOptlen (u4TcbIndex);
        u1IpHdrSize = (UINT1) (IP_HEADER_LENGTH + u1IpOptLen);
    }
    /* u1IpHdrSize = IP Header length including Options in the Out Segment */

    i1TCPOptLen = OsmGetTCBOptLen (u4TcbIndex);
    if (i1TCPOptLen == ERR)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from OsmSendSegment. Returned ERR.\n");
        return ERR;
    }

    u1IPTCPHdrSize = (UINT1) (u1IpHdrSize + TCP_HEADER_LENGTH +
                              (UINT1) i1TCPOptLen);
    u4CruBufSize = (UINT4) u1IPTCPHdrSize + u4DataLength;

    if ((pSegOut = CRU_BUF_Allocate_MsgBufChain (u4CruBufSize, ZERO)) == NULL)
    {
        TCP_MOD_TRC (u4Context,
                     TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC | TCP_BUF_TRC, "TCP",
                     "Exit from OsmSendSegment. Allocation of buffer chain failed. Returned ERR.\n");
        return ERR;
    }
    MEMSET (pSegOut->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pSegOut, "TcpOsmSndSeg");

    TCP_MOD_TRC (u4Context, TCP_BUF_TRC, "TCP",
                 "Allocation of buffer chain successful.\n");

    pSendSeg = &SendSeg;
    pIpHdr = (tIpHdr *) (VOID *) au1IpTcpHdr;

    pIpHdr->u2IpOptLen = (UINT2) (u1IpOptLen);
    u1Ip6HdrSize = (UINT1) (IP6_HEADER_LEN + u1IpOptLen);

    if (u1Ip6HdrSize >= sizeof (au1IpTcpHdr))
    {
        TCP_MOD_TRC (u4Context,
                     TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from OsmSendSegment. Invalid IP6 Header size. Returned ERR.\n");
        CRU_BUF_Release_MsgBufChain (pSegOut, FALSE);
        return ERR;

    }

    pTcpHdr = (tTcpHdr *) (VOID *) (&au1IpTcpHdr[u1Ip6HdrSize]);

    pSendSeg->ip = pIpHdr;
    pSendSeg->tcp = pTcpHdr;
    /* Now pIpHdr and pTcpHdr are Pointers to the Respective 
       auxiliary IP and TCP Headers */

    u4RcvWnd = FarGetLwsize (ptcb);

    if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_BW)
    {
        u2LowerWindow = (UINT2) (u4RcvWnd & TCP_LOWER_WINDOW_MASK);
    }
    else
    {
        u2LowerWindow =
            (UINT2) (u4RcvWnd >> GetTCBRecvWindowScale (u4TcbIndex));
    }

    u2UpperWindow =
        (UINT2) ((u4RcvWnd & TCP_UPPER_WINDOW_MASK) >> TCP_BIT_SHIFT_16);

    /* The fields in the packet are being put in network byte order */

    V6_HTONL (&(pIpHdr->SrcAddr), &SrcAddr);
    V6_HTONL (&(pIpHdr->DestAddr), &DestAddr);
    pIpHdr->u1Proto = TCP_PROTOCOL;
    pTcpHdr->u2Sport = OSIX_HTONS (ptcb->u2TcbLocalPort);
    pTcpHdr->u2Dport = OSIX_HTONS (ptcb->u2TcbRemotePort);
    pTcpHdr->i4Ack = OSIX_HTONL (ptcb->i4TcbRnext);
    pTcpHdr->u2Window = OSIX_HTONS (u2LowerWindow);    /* clarify about typecasting */

    pTcpHdr->i4Seq = (INT4) (u1Rexmt) ? OSIX_HTONL (ptcb->i4TcbSuna) :
        OSIX_HTONL (ptcb->i4TcbSnext);
    pTcpHdr->u1Hlen =
        (TCP_HEADER_LENGTH >> TCP_BYTES_FOR_SHORT) << TCP_VER_SHIFT;

    u1CodeValue = GetTCBcode (u4TcbIndex);
    i4SeqNum = OSIX_NTOHL (pTcpHdr->i4Seq);

    if (GetTCBflags (u4TcbIndex) & TCBF_SNDFIN)
    {
        if (!u1Rexmt && (GetTCBflags (u4TcbIndex) & TCBF_REXMTFIN))
        {
            /* FIN bit should not be sent */
            u1CodeValue &= ~TCPF_FIN;
        }
        else if (((UINT4) i4SeqNum + u4DataLength -
                  (UINT4) GetTCBslast (u4TcbIndex)) == ZERO)
        {
            u1CodeValue |= TCPF_FIN;
            TCP_MOD_TRC (u4Context, TCP_CTRL_PLANE_TRC, "TCP",
                         " Sending a FIN segment.  \nCONN ID - %d ",
                         u4TcbIndex);
            TCP_MOD_TRC (u4Context, TCP_CTRL_PLANE_TRC,
                         "TCP",
                         "\nSEQ - %d \nACK - %d \n",
                         OSIX_NTOHL (pTcpHdr->i4Seq),
                         OSIX_NTOHL (pTcpHdr->i4Ack));
            SetTCBflags_AND (u4TcbIndex, ~TCBF_FINNOTSENT);
            SetTCBcode (u4TcbIndex, u1CodeValue);
        }
        else
        {
            /* FIN bit should not be sent */
            u1CodeValue &= ~TCPF_FIN;
        }
    }

    if ((GetTCBflags (u4TcbIndex) & TCBF_FIRSTSEND) == ZERO)
    {
        u1CodeValue |= TCPF_ACK;
    }

    if (u1CodeValue & TCPF_SYN)
    {
        TCP_MOD_TRC (u4Context, TCP_CTRL_PLANE_TRC, "TCP",
                     "Sending a SYN segment.\nCONN ID - %d ", u4TcbIndex);
        TCP_MOD_TRC (u4Context, TCP_CTRL_PLANE_TRC, "TCP",
                     "\nSEQ - %d, \nACK - %d \n",
                     OSIX_NTOHL (pTcpHdr->i4Seq), OSIX_NTOHL (pTcpHdr->i4Ack));
        OsmAddOptDuringEstb (ptcb, pSendSeg);
        /* TCP Hdr length is getting updated in above function */
    }
    else
    {
        /* Add the enabled options. Header length is modified */
        OsmAddOption (ptcb, pSendSeg, u2UpperWindow);
    }

    u2Up = ZERO;
    if (GetTCBflags (u4TcbIndex) & TCBF_SUPOK)
    {
        u2Up = (UINT2) ((UINT4) GetTCBsupseq (u4TcbIndex) - (UINT4) i4SeqNum);
        if ((INT2) u2Up >= ZERO)
        {
            TCP_MOD_TRC (u4Context, TCP_CTRL_PLANE_TRC,
                         "TCP", "Sending a segment with URG ptr set.");

            TCP_MOD_TRC (u4Context,
                         TCP_CTRL_PLANE_TRC,
                         "TCP",
                         "\nCONN ID - %d \nSEQ - %d ",
                         u4TcbIndex, OSIX_NTOHL (pTcpHdr->i4Seq));
            TCP_MOD_TRC (u4Context,
                         TCP_CTRL_PLANE_TRC,
                         "TCP",
                         "\nACK - %d \nURG PTR - %d \n ",
                         OSIX_NTOHL (pTcpHdr->i4Ack), u2Up);
            u1CodeValue |= TCPF_URG;
        }
        else
        {
            u2Up = ZERO;
        }
    }

    if (GetTCBflags (u4TcbIndex) & TCBF_SPUSH)
    {
        TCP_MOD_TRC (u4Context,
                     TCP_CTRL_PLANE_TRC,
                     "TCP", "Sending a segment with PSH flag set.");
        TCP_MOD_TRC (u4Context,
                     TCP_CTRL_PLANE_TRC, "TCP", "\nCONN ID - %d ", u4TcbIndex);

        TCP_MOD_TRC
            (u4Context, TCP_CTRL_PLANE_TRC,
             "TCP",
             "\nSEQ - %d \nACK - %d \n",
             OSIX_NTOHL (pTcpHdr->i4Seq), OSIX_NTOHL (pTcpHdr->i4Ack));
        u1CodeValue |= TCPF_PSH;
    }
    pTcpHdr->u2Urgptr = OSIX_HTONS (u2Up);
    pTcpHdr->u1Code = (UINT1) (u1CodeValue);
    u1TCPHdrSize =
        (UINT1) (((pTcpHdr->
                   u1Hlen & TCP_IPVER_MASK) >> TCP_VER_SHIFT) <<
                 TCP_BYTES_FOR_SHORT);

    /* If TCP-AO/MD5 option is enabled, final TCP header size updated 
     * with TCP-AO/MD5 option length. Required for correct offset in CRU buffer
     * before copy of segment payload. Current header length taken in temp 
     * variable for getting offset in TCP header where digest will be 
     * copied before sending segment to IP*/
    if (GetTCBTcpAoEnabled (u4TcbIndex))
    {
        u1TCPHdrSizePrev = u1TCPHdrSize;
        /*u1TCPHdrSize += TCPAO_OPT_LEN + TCP_TWO; */
        u1TCPHdrSize = (UINT1) (u1TCPHdrSize + TCPAO_OPT_LEN);
        pTcpHdr->u1Hlen = (UINT1)
            ((u1TCPHdrSize >> TCP_BYTES_FOR_SHORT) << TCP_VER_SHIFT);
    }
    else if (GetTCBmd5keylen (u4TcbIndex) != TCP_ZERO)
    {
        u1TCPHdrSizePrev = u1TCPHdrSize;
        u1TCPHdrSize += MD5_OPT_LEN + TCP_TWO;
        pTcpHdr->u1Hlen = (UINT1)
            ((u1TCPHdrSize >> TCP_BYTES_FOR_SHORT) << TCP_VER_SHIFT);
    }

    u4Index = (GetTCBsbstart (u4TcbIndex) + u4Off) % GetTCBsbsize (u4TcbIndex);
    u4Buflen = GetTCBsbsize (u4TcbIndex) - u4Index;
    u4Buflen = (u4DataLength < u4Buflen) ? u4DataLength : u4Buflen;
    pBuf = (UINT1 *) (&(ptcb->pTcbSndbuf[u4Index]));
    u2DataOffset = (UINT2) (u1IpHdrSize + u1TCPHdrSize);

    if ((CRU_BUF_Copy_OverBufChain (pSegOut, pBuf, u2DataOffset, u4Buflen))
        != OSIX_SUCCESS)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC,
                     "TCP", "Exit from OsmSendSegment. Returned ERR.\n");
        CRU_BUF_Release_MsgBufChain (pSegOut, FALSE);
        return ERR;
    }
    u2DataOffset += (UINT2) u4Buflen;
    /* if all data has not been copied , u4DataLength will be greater than
       u4Buflen. So wrap arround the send buffer and copy the remaining */
    if (u4DataLength > u4Buflen)
    {
        pBuf = (UINT1 *) &(ptcb->pTcbSndbuf[TCP_ZERO]);
        u4Buflen = u4DataLength - u4Buflen;
        if ((CRU_BUF_Copy_OverBufChain
             (pSegOut, pBuf, u2DataOffset, u4Buflen)) != OSIX_SUCCESS)
        {
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC,
                         "TCP", "Exit from OsmSendSegment. Returned ERR.\n");
            CRU_BUF_Release_MsgBufChain (pSegOut, FALSE);
            return ERR;
        }
    }

    SetTCBflags_AND (u4TcbIndex, ~TCBF_NEEDOUT);
    if (u1Rexmt)
    {
        u4NewData =
            (UINT4)
            GetTCBsuna
            (u4TcbIndex) + u4DataLength - (UINT4) GetTCBsnext (u4TcbIndex);
        if ((INT4) u4NewData < ZERO)
        {
            u4NewData = ZERO;
        }
        else if (u1CodeValue & TCPF_FIN)
        {
            u4NewData++;
        }
    }
    else
    {
        u4NewData = u4DataLength;
        if (u1CodeValue & TCPF_SYN)
        {
            u4NewData++;
        }
        if (u1CodeValue & TCPF_FIN)
        {
            u4NewData++;
        }
    }

    SetTCBsnext
        (u4TcbIndex, (INT4) ((UINT4) GetTCBsnext (u4TcbIndex) + u4NewData));
    if (GetTCBstate (u4TcbIndex) == TCPS_TIMEWAIT)
    {
        FsmStartWaitTmr (ptcb);
    }

    if (u1IpToSend == IP4)
    {
        LINOsmCopyIpOptions (pIpHdr->au1IpOpts, u4TcbIndex);
    }
    else
    {
        pIpHdr->u1VHlen = TCP_ZERO;
    }
    pIpHdr->u2Totlen = OSIX_HTONS (((UINT2) (u4CruBufSize - u1IpHdrSize)));
    /* Totlen is the only IP data Length 
       i.6 Tcp Header + Tcp Data  DataLen field Can be used 
       instead of Totlen for Dual Architecture */

    pTcpHdr->u2Cksum = ZERO;
    /* 
     * Last option to be added to the TCP header is TCP-AO
     * if tcp-AO is enabled add TCP-AO MAC , TCP-AO & MD5 will not be
     * allowed on same packet
     */
    if (GetTCBTcpAoEnabled (u4TcbIndex))
    {
        /*tcp_ao_en = 1;
           printf("\n tcp-ao ot len oof pack osm send %d ",i1TCPOptLen); */

        pTcpOpt = pSendSeg->tcp->au1TcpOpts;
        u4TcpOptIndex = (UINT4) (u1TCPHdrSizePrev - TCP_HEADER_LENGTH);

        if (TcpOptFillTcpAoFields (u4TcbIndex, pSendSeg, pSegOut,
                                   &pTcpOpt[u4TcpOptIndex]) != TCP_OK)
        {
            TCP_MOD_TRC (u4Context,
                         TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC | TCP_BUF_TRC, "TCP", "Exit from OsmSendSegment. TCP-AO MAC generation failed for \
                Conn - %d.\n", u4TcbIndex);
            CRU_BUF_Release_MsgBufChain (pSegOut, FALSE);
            return ERR;
        }
    }
    else if (GetTCBmd5keylen (u4TcbIndex) != TCP_ZERO)
    {
        MEMSET (au1Md5Digest, TCP_ZERO, sizeof (au1Md5Digest));
        if (TcpOptGenerateMD5Digest
            (u4Context, pSendSeg, pSegOut, au1Md5Digest,
             (UINT1 *) GetTCBpmd5key (u4TcbIndex),
             GetTCBmd5keylen (u4TcbIndex)) == TCP_OK)
        {
            pTcpOpt = pSendSeg->tcp->au1TcpOpts;
            u4TcpOptIndex = u1TCPHdrSizePrev - TCP_HEADER_LENGTH;
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;
            pTcpOpt[u4TcpOptIndex++] = TCP_MD5;
            pTcpOpt[u4TcpOptIndex++] = MD5_OPT_LEN;
            MEMCPY ((pTcpOpt + u4TcpOptIndex), au1Md5Digest, MD5_DIGEST_LEN);
        }
        else
        {
            TCP_MOD_TRC (u4Context,
                         TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC | TCP_BUF_TRC, "TCP", "Exit from OsmSendSegment.MD5 digest generation failed for\
                Conn - %d.\n", u4TcbIndex);
            CRU_BUF_Release_MsgBufChain (pSegOut, FALSE);
            return ERR;
        }
    }

    pTcpHdr->u2Cksum = TcpPktCksum (u4Context, pSegOut, pSendSeg);
    pTcpHdr->u2Cksum = OSIX_HTONS (pTcpHdr->u2Cksum);
    /* Copy IP and TCP headers to CRU Buffer and send to IP */

    if ((CRU_BUF_Copy_OverBufChain
         (pSegOut, (au1IpTcpHdr + IP6_HEADER_LEN),
          (u1IpHdrSize - u1IpOptLen),
          u1TCPHdrSize + u1IpOptLen)) != OSIX_SUCCESS)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC,
                     "TCP", "Exit from OsmSendSegment. Returned ERR.\n");
        CRU_BUF_Release_MsgBufChain (pSegOut, FALSE);
        return ERR;
    }
    if (u1IpToSend == IP6)
    {
#ifdef IP6_WANTED
        MEMSET (&Ip6SendParms, 0, sizeof (Ip6SendParms));

        LliGetIp6SendParms (pSegOut, u4TcbIndex, (UINT2) u4CruBufSize,
                            &Ip6SendParms);
        if (LliSendSegmentToIP6 (pSegOut, u4CruBufSize, &Ip6SendParms) ==
            TCP_OK)
        {
            if (u1Rexmt)
            {
                IncTCPstat (u4Context, RetransSegs);
            }
            else
            {
                IncTCPstat (u4Context, OutSegs);
                IncTCPHCstat (u4Context, u8HcOutSegs);
                if (!
                    ((u1CodeValue &
                      TCPF_SYN) || (u1CodeValue & TCPF_FIN))
                    && (u4DataLength != ZERO))
                {
                    FarAddToRxmtQ (ptcb,
                                   OSIX_NTOHL (pTcpHdr->i4Seq), u4Index,
                                   u4DataLength);
                }

            }
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC,
                         "TCP", "Exit from OsmSendSegment. Returned OK.\n");
            return TCP_OK;
        }
#endif
    }
#ifdef IP_WANTED
    else if (LliSendSegmentToIP
             (pSegOut,
              u4CruBufSize,
              LliGetIpSendParms
              (pSegOut, u4TcbIndex,
               (UINT2) (u4CruBufSize - u1IpHdrSize))) == TCP_OK)
    {
        if (u1Rexmt)
        {
            IncTCPstat (u4Context, RetransSegs);
        }
        else
        {
            IncTCPstat (u4Context, OutSegs);
            IncTCPHCstat (u4Context, u8HcOutSegs);
            if (!
                ((u1CodeValue &
                  TCPF_SYN) || (u1CodeValue & TCPF_FIN))
                && (u4DataLength != ZERO))
            {
                FarAddToRxmtQ (ptcb,
                               OSIX_NTOHL (pTcpHdr->i4Seq), u4Index,
                               u4DataLength);
            }

        }
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC,
                     "TCP", "Exit from OsmSendSegment. Returned OK.\n");
        return TCP_OK;
    }
#endif

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                 "TCP", "Exit from OsmSendSegment. Returned ERR.\n");
    return ERR;
}

/*************************************************************************/
/*  Function Name   : */
/*  Input(s)        : */
/*  Output(s)       : */
/*  Returns         : */
/*  Description     : */
/*  CallingCondition: */
/*************************************************************************/

/*------------------------------------------------------------------
RETURN TCP_OK/ERR

FUNCTION
Whenever segment arrives unexpectedly it generates and send a RESET 
segment. The argument points to the bad segment.

CALLING CONDITION
When a bad segment arrives.

GLOBAL VARIABLES AFFECTED
None

-------------------------------------------------------------------*/
INT1
LINOsmSendReset (UINT4 u4Context, tIpTcpHdr * pRcvSeg)
{
    tSegment           *pSegOut;
    t_IP_SEND_PARMS    *IpSendParms = NULL;
    tIpTcpHdr           SendSeg;
    tIpTcpHdr          *pSendSeg;
    tIpHdr             *pIpHdr, *pRcvIpHdr;
    tTcpHdr            *pTcpHdr, *pRcvTcpHdr;
    UINT1               au1IpTcpHdr[sizeof (tIpHdr) + sizeof (tTcpHdr)];
    UINT4               u4DataLen;
    INT4                i4AckSequence;
    UINT4               u4Seglen;
    INT4                i4SeqNo;
    UINT1               u1CodeValue;
    UINT1               u1TCPHdrStart, u1TCPCode, u1TCPHlen;
#ifdef IP6_WANTED
    tHlToIp6Params      Ip6SendParms;
#endif
    tIpAddr             SrcAddr, DestAddr;
    UINT1               u1IpHdrSize;
    UINT1               u1IpToSend = IP4;
    TCP_MOD_TRC (u4Context,
                 TCP_ENTRY_EXIT_TRC, "TCP", "Entering LINOsmSendReset. \n");
    MEMSET (au1IpTcpHdr, TCP_ZERO, (sizeof (tIpHdr) + sizeof (tTcpHdr)));
    pRcvIpHdr = pRcvSeg->ip;

    u1TCPHdrStart = (UINT1) (IP6_HEADER_LEN + (UINT1) (pRcvIpHdr->u2IpOptLen));

    pRcvTcpHdr = pRcvSeg->tcp;
    u1TCPCode = pRcvTcpHdr->u1Code;
    pSendSeg = &SendSeg;
    pIpHdr = (tIpHdr *) (VOID *) au1IpTcpHdr;
    if (u1TCPHdrStart >= sizeof (au1IpTcpHdr))
    {
        return ERR;
    }
    pTcpHdr = (tTcpHdr *) (VOID *) (au1IpTcpHdr + u1TCPHdrStart);
    pSendSeg->ip = pIpHdr;
    pSendSeg->tcp = pTcpHdr;
    if (u1TCPCode & TCPF_RST)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC,
                     "TCP", "Exit from LINOsmSendReset. Returned OK.\n");
        return TCP_OK;
    }

    SrcAddr = pRcvIpHdr->SrcAddr;
    DestAddr = pRcvIpHdr->DestAddr;
    if ((IsIP6Addr (SrcAddr)) || (IsIP6Addr (DestAddr)))
    {
        u1IpToSend = (UINT1) IP6;
        u1IpHdrSize = TCP_ZERO;
        pIpHdr->u1VHlen = TCP_ZERO;
    }
    else
    {
        u1IpToSend = (UINT1) IP4;
        u1IpHdrSize = IP_HEADER_LENGTH;
    }
    if ((pSegOut =
         CRU_BUF_Allocate_MsgBufChain
         (TCP_HEADER_LENGTH + u1IpHdrSize, ZERO)) == NULL)
    {
        TCP_MOD_TRC (u4Context,
                     TCP_ENTRY_EXIT_TRC | TCP_BUF_TRC | TCP_ALL_FAIL_TRC,
                     "TCP",
                     "Exit from LINOsmSendReset. Allocation of buffer chain failed. Returned ERR.\n");
        return ERR;
    }
    MEMSET (pSegOut->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pSegOut, "TcpOsmSndRst");

    TCP_MOD_TRC
        (u4Context,
         TCP_BUF_TRC, "TCP",
         "Memory allocation for buffer chain successful.\n");
    if (u1IpToSend == IP6)
    {
#ifdef IP6_WANTED
        MEMSET (&Ip6SendParms, 0, sizeof (Ip6SendParms));
#endif
    }
    else
    {
        IpSendParms = (t_IP_SEND_PARMS *) CRU_BUF_Get_ModuleData (pSegOut);
    }
    pIpHdr->u2IpOptLen = TCP_ZERO;
    /* divide by 4 to convert it into 32 bit multiple */
    pTcpHdr->u1Hlen = TCP_HEADER_LENGTH << 2;    /* Abhi 28-3-98 */
    /* both ip hdr and tcp hdr len are same */
    if (u1IpToSend == IP6)
    {
#ifdef IP6_WANTED
        GetIp6ParmsForReset (&Ip6SendParms, pIpHdr, pRcvIpHdr);
        Ip6SendParms.u4ContextId = u4Context;

#endif
    }
    else
    {
#ifdef IP_WANTED
        GetIpParmsForReset (IpSendParms, pIpHdr, pRcvIpHdr);
        IpSendParms->u4ContextId = u4Context;
#endif
    }
    /* ELLA_BUG_FIX: Added the below line.
     * RESET message was going out with a
     * Checksum error because this was not
     * properly set
     */
    pIpHdr->u1Proto = TCP_PROTOCOL;
    pTcpHdr->u2Dport = pRcvTcpHdr->u2Sport;
    pTcpHdr->u2Sport = pRcvTcpHdr->u2Dport;
    if (u1TCPCode & TCPF_ACK)
    {
        i4AckSequence = OSIX_NTOHL (pRcvTcpHdr->i4Ack);
        u1CodeValue = TCPF_RST;
    }
    else
    {
        i4AckSequence = ZERO;
        u1CodeValue = TCPF_RST | TCPF_ACK;
    }

    pTcpHdr->i4Seq = OSIX_HTONL (i4AckSequence);
    pTcpHdr->u1Code = u1CodeValue;
    u1TCPHlen = (UINT1) (((pRcvTcpHdr->u1Hlen) >> TCP_VER_SHIFT)
                         << TCP_BYTES_FOR_SHORT);
    u4Seglen = (UINT4) (OSIX_NTOHS (pRcvIpHdr->u2Totlen));
    u4DataLen = u4Seglen - u1TCPHlen;    /* u2Totlen carries only Tcp Datalen */
    if (u1TCPCode & TCPF_SYN)
    {
        u4DataLen++;
    }
    if (u1TCPCode & TCPF_FIN)
    {
        u4DataLen++;
    }
    i4SeqNo = (INT4) (OSIX_NTOHL (pRcvTcpHdr->i4Seq));
    /* ANVL BUG FIX : When the incoming packet has an ACK then the outgoing
     * reset packet will not have the ACK flag set in the TCP header. 
     * Therefore the ACK field should be zero in that case. */
    if (u1TCPCode & TCPF_ACK)
    {
        i4AckSequence = ZERO;
    }
    else
    {
        i4AckSequence = (INT4) u4DataLen + i4SeqNo;
    }

    pTcpHdr->i4Ack = OSIX_HTONL (i4AckSequence);
    pTcpHdr->u2Window = ZERO;
    pTcpHdr->u2Urgptr = ZERO;

    pIpHdr->u2Totlen = TCP_HEADER_LENGTH;    /* Only IP Data Len */
    pIpHdr->u2IpOptLen = ZERO;
    pIpHdr->u2Totlen = OSIX_HTONS (pIpHdr->u2Totlen);
    pTcpHdr->u2Cksum = ZERO;
    pTcpHdr->u2Cksum = TcpPktCksum (u4Context, pSegOut, pSendSeg);
    pTcpHdr->u2Cksum = OSIX_HTONS (pTcpHdr->u2Cksum);
    /* Copy IP and TCP headers to CRU Buffer and send to IP */
    TCP_MOD_TRC
        (u4Context,
         TCP_CTRL_PLANE_TRC,
         "TCP",
         "Sending a RST segment. \nSEQ - %d \nACK - %d \n ",
         OSIX_NTOHL (pTcpHdr->i4Seq), OSIX_NTOHL (pTcpHdr->i4Ack));
    /* Copy TCP Header */

    if ((CRU_BUF_Copy_OverBufChain
         (pSegOut, (au1IpTcpHdr + u1TCPHdrStart),
          u1IpHdrSize, TCP_HEADER_LENGTH)) != OSIX_SUCCESS)
    {
        TCP_MOD_TRC
            (u4Context,
             TCP_ENTRY_EXIT_TRC,
             "TCP", "Exit from LINOsmSendReset. Returned ERR.\n");
        CRU_BUF_Release_MsgBufChain (pSegOut, FALSE);
        return ERR;
    }

    if (u1IpToSend == IP6)
    {
#ifdef IP6_WANTED
        if (LliSendSegmentToIP6
            (pSegOut, TCP_HEADER_LENGTH, &Ip6SendParms) == TCP_OK)
        {
            IncTCPstat (u4Context, OutSegs);
            IncTCPHCstat (u4Context, u8HcOutSegs);
            IncTCPstat (u4Context, OutRsts);
            TCP_MOD_TRC (u4Context,
                         TCP_ENTRY_EXIT_TRC,
                         "TCP", "Exit from LINOsmSendReset. Returned OK.\n");
            return TCP_OK;
        }
#endif
    }
#ifdef IP_WANTED
    else if (LliSendSegmentToIP
             (pSegOut, TCP_HEADER_LENGTH + IP_HEADER_LENGTH,
              IpSendParms) == TCP_OK)
    {
        IncTCPstat (u4Context, OutSegs);
        IncTCPHCstat (u4Context, u8HcOutSegs);
        IncTCPstat (u4Context, OutRsts);
        TCP_MOD_TRC
            (u4Context,
             TCP_ENTRY_EXIT_TRC,
             "TCP", "Exit from LINOsmSendReset. Returned OK.\n");
        return TCP_OK;
    }
#endif
    TCP_MOD_TRC
        (u4Context,
         TCP_ENTRY_EXIT_TRC |
         TCP_ALL_FAIL_TRC, "TCP", "Exit from LINOsmSendReset. Returned ERR.\n");
    return ERR;
}

/*************************************************************************/
/*  Function Name   : */
/*  Input(s)        : */
/*  Output(s)       : */
/*  Returns         : */
/*  Description     : */
/*  CallingCondition: */
/*************************************************************************/

/*------------------------------------------------------------------
RETURN TCP_OK/ERR

FUNCTION
Sends a RST packet to the peer of the connection.

CALLING CONDITION

GLOBAL VARIABLES AFFECTED none

------------------------------------------------------------------*/
INT1
OsmResetConn (UINT4 u4TcbIndex)
{
    tSegment           *pSegOut;
    tTcb               *ptcb;
    tIpTcpHdr           SendSeg;
    tIpTcpHdr          *pSendSeg;
    tIpHdr             *pIpHdr;
    tTcpHdr            *pTcpHdr;
    UINT1               au1IpTcpHdr[sizeof (tIpHdr) + sizeof (tTcpHdr)];
    UINT4               u4Context = 0;
    INT4                i4Tmp;
    UINT2               u2SegLen;
    UINT2               u2Tmp;
    UINT1               u1TCPHdrStart;
    UINT1               u1IpOptLen;
    tIpAddr             SrcAddr, DestAddr;
    UINT2               u2IpOptDataLen;
    UINT1               u1IpToSend = IP4;
    UINT1               u1IpHdrSize = 0;
#ifdef IP6_WANTED
    tHlToIp6Params      Ip6SendParms;
#endif
    UINT1               au1Md5Digest[MD5_DIGEST_LEN];
    UINT1              *pTcpOpt = NULL;
    UINT4               u4TcpOptIndex = TCP_ZERO;
    UINT1               u1TCPHdrLen = TCP_ZERO;

    u4Context = GetTCBContext (u4TcbIndex);
    TCP_MOD_TRC
        (u4Context, TCP_ENTRY_EXIT_TRC, "TCP", "Entering OsmResetConn. \n");

    MEMSET (au1IpTcpHdr, TCP_ZERO, (sizeof (tIpHdr) + sizeof (tTcpHdr)));

    ptcb = &TCBtable[u4TcbIndex];
    SrcAddr = GetTCBlip (u4TcbIndex);
    DestAddr = GetTCBrip (u4TcbIndex);
    u1TCPHdrLen = TCP_HEADER_LENGTH;
    if (GetTCBTcpAoEnabled (u4TcbIndex))
    {
        /* u1TCPHdrLen += TCPAO_OPT_LEN + TCP_TWO; */
        u1TCPHdrLen = ((UINT1) (u1TCPHdrLen + TCPAO_OPT_LEN));
    }
    else if (GetTCBmd5keylen (u4TcbIndex) != TCP_ZERO)
    {
        u1TCPHdrLen += MD5_OPT_LEN + TCP_TWO;
    }

    if ((IsIP6Addr (SrcAddr)) || (IsIP6Addr (DestAddr)))
    {
        u1IpToSend = (UINT1) (IP6);
        u1IpOptLen = TCP_ZERO;
        u1IpHdrSize = 0;
        u2SegLen = (UINT2) (u1TCPHdrLen);
    }
    else
    {
        u1IpToSend = IP4;
        u1IpHdrSize = IP_HEADER_LENGTH;
        u1IpOptLen = OsmGetIpOptlen (u4TcbIndex);
        u2SegLen = (UINT2) (u1TCPHdrLen + IP_HEADER_LENGTH + u1IpOptLen);
    }
    u1TCPHdrStart = (UINT1) (IP6_HEADER_LEN + u1IpOptLen);

    pSendSeg = &SendSeg;
    pIpHdr = (tIpHdr *) (VOID *) au1IpTcpHdr;

    if (u1TCPHdrStart >= sizeof (au1IpTcpHdr))
    {
        return ERR;
    }
    pTcpHdr = (tTcpHdr *) (VOID *) (&au1IpTcpHdr[u1TCPHdrStart]);
    pSendSeg->ip = pIpHdr;
    pSendSeg->tcp = pTcpHdr;
    if ((pSegOut = CRU_BUF_Allocate_MsgBufChain (u2SegLen, ZERO)) == NULL)
    {
        TCP_MOD_TRC
            (u4Context,
             TCP_ENTRY_EXIT_TRC |
             TCP_BUF_TRC |
             TCP_ALL_FAIL_TRC,
             "TCP", "Exit from OsmResetConn. Returned ERR.\n");
        return ERR;
    }
    MEMSET (pSegOut->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pSegOut, "TcpOsmRstCon");

    TCP_MOD_TRC
        (u4Context,
         TCP_BUF_TRC, "TCP", "Allocation of buffer chain successful.\n");
    /* Store all in network byte order */
    V6_HTONL (&(pIpHdr->SrcAddr), &(SrcAddr));
    V6_HTONL (&(pIpHdr->DestAddr), &(DestAddr));
    u2Tmp = GetTCBlport (u4TcbIndex);
    pTcpHdr->u2Sport = OSIX_HTONS (u2Tmp);
    u2Tmp = GetTCBrport (u4TcbIndex);
    pTcpHdr->u2Dport = OSIX_HTONS (u2Tmp);
    i4Tmp = GetTCBsnext (u4TcbIndex);
    pTcpHdr->i4Seq = OSIX_HTONL (i4Tmp);
    i4Tmp = GetTCBrnext (u4TcbIndex);
    pTcpHdr->i4Ack = OSIX_HTONL (i4Tmp);
    pTcpHdr->u1Hlen = (UINT1)
        ((u1TCPHdrLen >> TCP_BYTES_FOR_SHORT) << TCP_VER_SHIFT);
    pTcpHdr->u1Code = TCPF_ACK | TCPF_RST;
    u2Tmp = (UINT2) FarGetLwsize (ptcb);
    pTcpHdr->u2Window = ZERO;
    pTcpHdr->u2Cksum = ZERO;
    pTcpHdr->u2Urgptr = ZERO;

    /*  u2Totlen IP Header DataLength i.e TCP Header + Data  */
    if (u1IpToSend == IP6)
    {
        pIpHdr->u1VHlen = ZERO;
        pIpHdr->u2Totlen = OSIX_HTONS ((u2SegLen));
    }
    else
    {
        LINOsmCopyIpOptions (pIpHdr->au1IpOpts, u4TcbIndex);
        pIpHdr->u2Totlen =
            OSIX_HTONS (((u2SegLen) - (IP_HEADER_LEN + u1IpOptLen)));
    }
    pIpHdr->u2IpOptLen = u1IpOptLen;
    pIpHdr->u1Proto = TCP_PROTOCOL;
    pTcpHdr->u2Cksum = ZERO;
    /* 
     * Last option to be added to the TCP header is TCP-AO
     * if tcp-AO is enabled add TCP-AO MAC , TCP-AO & MD5 will not be
     * allowed on same packet
     */
    if (GetTCBTcpAoEnabled (u4TcbIndex))
    {
        pTcpOpt = pSendSeg->tcp->au1TcpOpts;
        u4TcpOptIndex = TCP_ZERO;

        if (TcpOptFillTcpAoFields (u4TcbIndex, pSendSeg, pSegOut,
                                   &pTcpOpt[u4TcpOptIndex]) != TCP_OK)
        {
            TCP_MOD_TRC (u4Context,
                         TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC | TCP_BUF_TRC, "TCP", "Exit from OsmResetConn. TCP-AO MAC generation failed for \
                Conn - %d.\n", u4TcbIndex);
            CRU_BUF_Release_MsgBufChain (pSegOut, FALSE);
            return ERR;
        }
    }
    else if (GetTCBmd5keylen (u4TcbIndex) != TCP_ZERO)
    {
        MEMSET (au1Md5Digest, TCP_ZERO, sizeof (au1Md5Digest));
        if (TcpOptGenerateMD5Digest
            (u4Context, pSendSeg, pSegOut, au1Md5Digest,
             (UINT1 *) GetTCBpmd5key (u4TcbIndex),
             GetTCBmd5keylen (u4TcbIndex)) == TCP_OK)
        {
            pTcpOpt = pSendSeg->tcp->au1TcpOpts;
            /* No other TCP options are added in a RST segment
             * so MD5 opt offset in options field is 0 */
            u4TcpOptIndex = TCP_ZERO;
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;
            pTcpOpt[u4TcpOptIndex++] = TCP_MD5;
            pTcpOpt[u4TcpOptIndex++] = MD5_OPT_LEN;
            MEMCPY ((pTcpOpt + u4TcpOptIndex), au1Md5Digest, MD5_DIGEST_LEN);
        }
        else
        {
            TCP_MOD_TRC (u4Context,
                         TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC | TCP_BUF_TRC, "TCP", "Exit from OsmResetConn.MD5 digest generation failed for\
                Conn - %d\n", u4TcbIndex);
            CRU_BUF_Release_MsgBufChain (pSegOut, FALSE);
            return ERR;
        }
    }

    pTcpHdr->u2Cksum = TcpPktCksum (u4Context, pSegOut, pSendSeg);
    pTcpHdr->u2Cksum = OSIX_HTONS (pTcpHdr->u2Cksum);
    /* Copy IP, TCP headers and IP options to CRU Buffer and send to IP */
    /* Copy IP Options And TCP Header */
    u2IpOptDataLen = (UINT2) (u2SegLen + u1IpOptLen);

    if ((CRU_BUF_Copy_OverBufChain
         (pSegOut, au1IpTcpHdr + IP6_HEADER_LEN, u1IpHdrSize,
          u2IpOptDataLen)) != OSIX_SUCCESS)
    {
        TCP_MOD_TRC
            (u4Context,
             TCP_ENTRY_EXIT_TRC,
             "TCP", "Exit from OsmResetConn. Returned ERR.\n");
        CRU_BUF_Release_MsgBufChain (pSegOut, FALSE);
        return ERR;
    }
    if (u1IpToSend == IP6)
    {
#ifdef IP6_WANTED
        MEMSET (&Ip6SendParms, 0, sizeof (Ip6SendParms));
        LliGetIp6SendParms (pSegOut, u4TcbIndex, u2SegLen, &Ip6SendParms);

        if (LliSendSegmentToIP6 (pSegOut, u2SegLen, &Ip6SendParms) == TCP_OK)
        {
            IncTCPstat (u4Context, OutSegs);
            IncTCPstat (u4Context, OutRsts);
            IncTCPHCstat (u4Context, u8HcOutSegs);
            TCP_MOD_TRC
                (u4Context,
                 TCP_ENTRY_EXIT_TRC,
                 "TCP", "Exit from OsmResetConn. Returned OK.\n");
            return TCP_OK;
        }
#endif
    }
#ifdef IP_WANTED
    else if (LliSendSegmentToIP
             (pSegOut, u2SegLen,
              LliGetIpSendParms
              (pSegOut,
               u4TcbIndex, (UINT2) (u2SegLen - IP_HEADER_LENGTH - u1IpOptLen)))
             == TCP_OK)
    {
        IncTCPstat (u4Context, OutSegs);
        IncTCPHCstat (u4Context, u8HcOutSegs);
        IncTCPstat (u4Context, OutRsts);
        TCP_MOD_TRC
            (u4Context,
             TCP_ENTRY_EXIT_TRC,
             "TCP", "Exit from OsmResetConn. Returned OK.\n");
        return TCP_OK;
    }
#endif
    TCP_MOD_TRC
        (u4Context,
         TCP_ENTRY_EXIT_TRC |
         TCP_ALL_FAIL_TRC, "TCP", "Exit from OsmResetConn. Returned ERR.\n");
    return ERR;
}

/*************************************************************************/
/*  Function Name   : */
/*  Input(s)        : */
/*  Output(s)       : */
/*  Returns         : */
/*  Description     : */
/*  CallingCondition: */
/*************************************************************************/

/*------------------------------------------------------------------
RETURN TCP_OK/ERR

FUNCTION
Sends a KA packet to the peer of the connection.

CALLING CONDITION When the KA timer expires

GLOBAL VARIABLES AFFECTED none
------------------------------------------------------------------*/
INT1
OsmSendKaPkt (UINT4 u4TcbIndex)
{
    tTcb               *ptcb;
    tIpTcpHdr           SendSeg;
    tIpHdr             *pIpHdr;
    tTcpHdr            *pTcpHdr;
    tSegment           *pSegOut;
    UINT1               au1IpTcpHdr[sizeof (tIpHdr) + sizeof (tTcpHdr)];
    UINT4               u4DataLength, u4CruBufSize;
    UINT4               u4RcvWnd;
    UINT4               u4Context = 0;
    UINT2               u2DataOffset, u2LowerWindow;
    UINT1               u1IpHdrSize;
    UINT1               u1IPTCPHdrSize, u1TCPHdrSize;
    UINT1               u1IpOptLen = ZERO;
    UINT1               u1GarbageOctet = ZERO;
    tIpAddr             SrcAddr, DestAddr;
    UINT1               u1IpToSend = IP4;
    UINT1               u1TcpHdrStart;
#ifdef IP6_WANTED
    tHlToIp6Params      Ip6SendParms;
#endif
    UINT1               au1Md5Digest[MD5_DIGEST_LEN];
    UINT1              *pTcpOpt = NULL;
    UINT4               u4TcpOptIndex = TCP_ZERO;

    u4Context = GetTCBContext (u4TcbIndex);
    TCP_MOD_TRC
        (u4Context, TCP_ENTRY_EXIT_TRC, "TCP", "Entering OsmSendKaPkt.\n");
    ptcb = &TCBtable[u4TcbIndex];
    /* Update the retransmission count */
    /* When the first KA packet is sent the retransmission count is set to 1. 
     * So whenever the KA timer expires, the KaRetransOverCount - 1 is checked 
     * with the MaxKaRetransCount. This also helps resetting the timer values
     * when the values are set through the MIB */
    SetTCBKaRetransOverCnt (u4TcbIndex,
                            (UINT1) ((GetTCBKaRetransOverCnt (u4TcbIndex) +
                                      TCP_ONE)));
    SrcAddr = GetTCBlip (u4TcbIndex);
    DestAddr = GetTCBrip (u4TcbIndex);
    if ((IsIP6Addr (SrcAddr)) || (IsIP6Addr (DestAddr)))
    {
        u1IpToSend = (UINT1) (IP6);
        u1IpHdrSize = ZERO;
    }
    else
    {
        u1IpToSend = (UINT1) (IP4);
        u1IpHdrSize = IP_HEADER_LENGTH;
    }
    u4DataLength = TCP_ONE;        /* The dirty octet for incorrect TCP implmns */

    /* Update TCP header size with MD5 option length
     * if option is enabled in TCB, for CRU buffer allocation */
    u1TCPHdrSize = TCP_HEADER_LENGTH;
    if (GetTCBTcpAoEnabled (u4TcbIndex))
    {
        /* u1TCPHdrSize += TCPAO_OPT_LEN + TCP_TWO; */
        u1TCPHdrSize = ((UINT1) (u1TCPHdrSize + TCPAO_OPT_LEN));
    }
    else if (GetTCBmd5keylen (u4TcbIndex) != TCP_ZERO)
    {
        u1TCPHdrSize += MD5_OPT_LEN + TCP_TWO;
    }
    u1IPTCPHdrSize = (UINT1) (u1IpHdrSize + u1TCPHdrSize);

    u4CruBufSize = (UINT4) u1IPTCPHdrSize + u4DataLength;
    if ((pSegOut = CRU_BUF_Allocate_MsgBufChain (u4CruBufSize, ZERO)) == NULL)
    {
        TCP_MOD_TRC
            (u4Context,
             TCP_ENTRY_EXIT_TRC |
             TCP_BUF_TRC |
             TCP_ALL_FAIL_TRC,
             "TCP",
             "Exit from OsmSendKaPkt. Allocation of buffer chain failed. Returned ERR.\n");
        return ERR;
    }
    MEMSET (pSegOut->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pSegOut, "TcpOsmSndPkt");

    TCP_MOD_TRC
        (u4Context,
         TCP_BUF_TRC, "TCP", "Allocation of buffer chain successful.\n");
    pIpHdr = (tIpHdr *) (VOID *) au1IpTcpHdr;
    u1TcpHdrStart = IP6_HEADER_LEN;
    pTcpHdr = (tTcpHdr *) (VOID *) (&au1IpTcpHdr[u1TcpHdrStart]);
    SendSeg.ip = pIpHdr;
    SendSeg.tcp = pTcpHdr;
    u4RcvWnd = FarGetLwsize (ptcb);
    if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_BW)
    {
        u2LowerWindow = (UINT2) (u4RcvWnd & TCP_LOWER_WINDOW_MASK);
    }
    else
    {
        /* It is assumed that u2LowerWindow < 65535 after Window scale shift */
        u2LowerWindow =
            (UINT2) (u4RcvWnd >> GetTCBRecvWindowScale (u4TcbIndex));
    }

    /* The fields in the packet are being put in network byte order */
    V6_HTONL (&(pIpHdr->SrcAddr), &(SrcAddr));
    V6_HTONL (&(pIpHdr->DestAddr), &(DestAddr));
    pIpHdr->u1Proto = TCP_PROTOCOL;
    pTcpHdr->u2Sport = OSIX_HTONS (ptcb->u2TcbLocalPort);
    pTcpHdr->u2Dport = OSIX_HTONS (ptcb->u2TcbRemotePort);
    pTcpHdr->i4Ack = OSIX_HTONL (ptcb->i4TcbRnext);
    pTcpHdr->u2Window = OSIX_HTONS (u2LowerWindow);
    /* In an idle connection, SND.NXT=RCV.NXT */
    pTcpHdr->i4Seq = OSIX_HTONL ((ptcb->i4TcbSnext - 1));
    /* This will force an ACK from peer */
    pTcpHdr->u1Hlen = (UINT1)
        ((u1TCPHdrSize >> TCP_BYTES_FOR_SHORT) << TCP_VER_SHIFT);
    pTcpHdr->u2Urgptr = ZERO;
    pTcpHdr->u1Code = TCPF_ACK;
    u1TCPHdrSize =
        (UINT1) (((pTcpHdr->
                   u1Hlen & TCP_IPVER_MASK) >> TCP_VER_SHIFT) <<
                 TCP_BYTES_FOR_SHORT);
    u2DataOffset = (UINT2) (u1IpHdrSize + u1TCPHdrSize);
/* Copying the garbage octet to the buffer */
    if ((CRU_BUF_Copy_OverBufChain
         (pSegOut, &u1GarbageOctet, u2DataOffset, TCP_ONE)) != OSIX_SUCCESS)
    {
        TCP_MOD_TRC
            (u4Context,
             TCP_ENTRY_EXIT_TRC,
             "TCP", "Exit from OsmSendKaPkt. Returned ERR.\n");
        CRU_BUF_Release_MsgBufChain (pSegOut, FALSE);
        return ERR;
    }
    pIpHdr->u2Totlen = (UINT2) u4CruBufSize;
    if (u1IpToSend == IP6)
    {
        pIpHdr->u1VHlen = TCP_ZERO;
    }
    pIpHdr->u2IpOptLen = (UINT2) u1IpOptLen;
    pIpHdr->u2Totlen = OSIX_HTONS (((pIpHdr->u2Totlen) - u1IpHdrSize));
    pTcpHdr->u2Cksum = ZERO;

    /* 
     * Last option to be added to the TCP header is TCP-AO
     * if tcp-AO is enabled add TCP-AO MAC , TCP-AO & MD5 will not be
     * allowed on same packet
     */
    if (GetTCBTcpAoEnabled (u4TcbIndex))
    {

        /* MAC is calculated including the TCP-AO option also */
        /* MAC field in the TCP-AO option will be set to zero */
        pTcpOpt = pTcpHdr->au1TcpOpts;
        u4TcpOptIndex = ZERO;

        if (TcpOptFillTcpAoFields (u4TcbIndex, &SendSeg, pSegOut,
                                   &pTcpOpt[u4TcpOptIndex]) != TCP_OK)
        {
            TCP_MOD_TRC (u4Context,
                         TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC | TCP_BUF_TRC, "TCP", "Exit from OsmSendKaPkt. TCP-AO MAC generation failed for \
                Conn - %d.\n", u4TcbIndex);
            CRU_BUF_Release_MsgBufChain (pSegOut, FALSE);
            return ERR;
        }
    }
    else if (GetTCBmd5keylen (u4TcbIndex) != TCP_ZERO)
    {
        MEMSET (au1Md5Digest, TCP_ZERO, sizeof (au1Md5Digest));
        if (TcpOptGenerateMD5Digest
            (u4Context, &SendSeg, pSegOut, au1Md5Digest,
             (UINT1 *) GetTCBpmd5key (u4TcbIndex),
             GetTCBmd5keylen (u4TcbIndex)) == TCP_OK)
        {
            pTcpOpt = pTcpHdr->au1TcpOpts;
            /* No options are added in the KeepAlive segment
             * so MD5 opt offset in options field is 0 */
            u4TcpOptIndex = ZERO;
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;
            pTcpOpt[u4TcpOptIndex++] = TCP_MD5;
            pTcpOpt[u4TcpOptIndex++] = MD5_OPT_LEN;
            MEMCPY ((pTcpOpt + u4TcpOptIndex), au1Md5Digest, MD5_DIGEST_LEN);
        }
        else
        {
            TCP_MOD_TRC (u4Context,
                         TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC | TCP_BUF_TRC, "TCP", "Exit from OsmSendKaPkt.MD5 digest generation failed for \
                Conn - %d\n", u4TcbIndex);
            CRU_BUF_Release_MsgBufChain (pSegOut, FALSE);
            return ERR;
        }
    }

    pTcpHdr->u2Cksum = TcpPktCksum (u4Context, pSegOut, &SendSeg);
    pTcpHdr->u2Cksum = OSIX_HTONS (pTcpHdr->u2Cksum);
    /* Copy IP and TCP headers to CRU Buffer and send to IP */
    if ((CRU_BUF_Copy_OverBufChain
         (pSegOut, au1IpTcpHdr + u1TcpHdrStart, u1IpHdrSize,
          u1TCPHdrSize)) != OSIX_SUCCESS)
    {
        TCP_MOD_TRC
            (u4Context,
             TCP_ENTRY_EXIT_TRC,
             "TCP", "Exit from OsmSendKaPkt. Returned ERR.\n");
        CRU_BUF_Release_MsgBufChain (pSegOut, FALSE);
        return ERR;
    }
    /* Start the ka retrans timer */
    TmrInitTimer (&(ptcb->TcbKaTimer), TCP_KEEPALIVE_TIMER, u4TcbIndex);
    TcpTmrSetTimer (&(ptcb->TcbKaTimer), GetTCBKaRetransTmOut (u4TcbIndex));
    TCP_MOD_TRC
        (u4Context, TCP_OS_RES_TRC, "TCP", "Starting KA retrans timer.\n");
    if (u1IpToSend == IP6)
    {
#ifdef IP6_WANTED
        MEMSET (&Ip6SendParms, 0, sizeof (Ip6SendParms));
        LliGetIp6SendParms (pSegOut, u4TcbIndex, (UINT2) u4CruBufSize,
                            &Ip6SendParms);
        if (LliSendSegmentToIP6 (pSegOut, u4CruBufSize, &Ip6SendParms) ==
            TCP_OK)
        {
            IncTCPstat (u4Context, OutSegs);
            IncTCPHCstat (u4Context, u8HcOutSegs);
            TCP_MOD_TRC
                (u4Context,
                 TCP_ENTRY_EXIT_TRC,
                 "TCP", "Exit from OsmSendKaPkt. Returned OK.\n");
            return TCP_OK;
        }
#endif
    }
#ifdef IP_WANTED
    else if (LliSendSegmentToIP
             (pSegOut,
              u4CruBufSize,
              LliGetIpSendParms
              (pSegOut, u4TcbIndex, (UINT2) (u4CruBufSize - IP_HEADER_LENGTH)))
             == TCP_OK)
    {
        IncTCPstat (u4Context, OutSegs);
        IncTCPHCstat (u4Context, u8HcOutSegs);
        TCP_MOD_TRC
            (u4Context,
             TCP_ENTRY_EXIT_TRC,
             "TCP", "Exit from OsmSendKaPkt. Returned OK.\n");
        return TCP_OK;
    }
#endif
    TCP_MOD_TRC
        (u4Context,
         TCP_ENTRY_EXIT_TRC |
         TCP_ALL_FAIL_TRC, "TCP", "Exit from OsmSendKaPkt. Returned ERR.\n");
    return ERR;
}
