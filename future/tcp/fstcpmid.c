# include  "include.h"
# include  "fstcpmid.h"
# include  "fstcplow.h"
# include  "fstcpcon.h"
# include  "fstcpogi.h"
# include  "extern.h"
# include  "midconst.h"
# include  "tcphdr.h"
# include  "fstcpcli.h"

/****************************************************************************
 Function   : fstcpGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fstcpGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
          UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Scalar get routine. */

    UINT1               i1_ret_val = FALSE;
    INT4                LEN_fstcp_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  This Variable is declared for being used in the
     *  FOR Loop for extracting Indices from OID Given.
     */

/*** DECLARATION_END ***/

    LEN_fstcp_INDEX = p_in_db->u4_Length;

    /*  Incrementing the Length for the Extract of Scalar Tables. */
    LEN_fstcp_INDEX++;
    if (u1_search_type == EXACT)
    {
        if ((LEN_fstcp_INDEX != (INT4) p_incoming->u4_Length)
            || (p_incoming->pu4_OidList[p_incoming->u4_Length - 1] != 0))
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    else
    {
        /*  Get Next Operation on the Scalar Variable.  */
        if ((INT4) p_incoming->u4_Length >= LEN_fstcp_INDEX)
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    switch (u1_arg)
    {
        case FSTCPACKOPTION:
        {
            i1_ret_val = nmhGetFsTcpAckOption (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPTIMESTAMPOPTION:
        {
            i1_ret_val = nmhGetFsTcpTimeStampOption (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPBIGWNDOPTION:
        {
            i1_ret_val = nmhGetFsTcpBigWndOption (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPINCRINIWND:
        {
            i1_ret_val = nmhGetFsTcpIncrIniWnd (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPMAXNUMOFTCB:
        {
            i1_ret_val = nmhGetFsTcpMaxNumOfTCB (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPTRACEDEBUG:
        {
            i1_ret_val = nmhGetFsTcpTraceDebug (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    /* Incrementing the Length of the p_in_db. */
    p_in_db->u4_Length++;
    /* Adding the .0 to the p_in_db for scalar Objects. */
    p_in_db->pu4_OidList[p_in_db->u4_Length - 1] = ZERO;

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fstcpSet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fstcpSet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
          UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSTCPACKOPTION:
        {
            i1_ret_val = nmhSetFsTcpAckOption (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSTCPTIMESTAMPOPTION:
        {
            i1_ret_val = nmhSetFsTcpTimeStampOption (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSTCPBIGWNDOPTION:
        {
            i1_ret_val = nmhSetFsTcpBigWndOption (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSTCPINCRINIWND:
        {
            i1_ret_val = nmhSetFsTcpIncrIniWnd (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSTCPMAXNUMOFTCB:
        {
            i1_ret_val = nmhSetFsTcpMaxNumOfTCB (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSTCPTRACEDEBUG:
        {
            i1_ret_val = nmhSetFsTcpTraceDebug (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fstcpTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fstcpTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
           UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }
    switch (u1_arg)
    {

        case FSTCPACKOPTION:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsTcpAckOption (&u4ErrorCode, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSTCPTIMESTAMPOPTION:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsTcpTimeStampOption (&u4ErrorCode,
                                               p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSTCPBIGWNDOPTION:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsTcpBigWndOption (&u4ErrorCode,
                                            p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSTCPINCRINIWND:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsTcpIncrIniWnd (&u4ErrorCode, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSTCPMAXNUMOFTCB:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsTcpMaxNumOfTCB (&u4ErrorCode,
                                           p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSTCPTRACEDEBUG:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsTcpTraceDebug (&u4ErrorCode, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsTcpConnEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsTcpConnEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                   UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsTcpConnTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsTcpConnLocalAddress = FALSE;
    UINT4               u4_addr_next_fsTcpConnLocalAddress = FALSE;
    UINT1               u1_addr_fsTcpConnLocalAddress[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_fsTcpConnLocalAddress[ADDR_LEN] =
        NULL_STRING;

    INT4                i4_fsTcpConnLocalPort = FALSE;
    INT4                i4_next_fsTcpConnLocalPort = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsTcpConnRemAddress = FALSE;
    UINT4               u4_addr_next_fsTcpConnRemAddress = FALSE;
    UINT1               u1_addr_fsTcpConnRemAddress[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_fsTcpConnRemAddress[ADDR_LEN] =
        NULL_STRING;

    INT4                i4_fsTcpConnRemPort = FALSE;
    INT4                i4_next_fsTcpConnRemPort = FALSE;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += INTEGER_LEN;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsTcpConnTable_INDEX =
                p_in_db->u4_Length + ADDR_LEN + INTEGER_LEN + ADDR_LEN +
                INTEGER_LEN;

            if (LEN_fsTcpConnTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_fsTcpConnLocalAddress[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_fsTcpConnLocalAddress =
                    CRU_BMC_DWFROMPDU (u1_addr_fsTcpConnLocalAddress);

                /* Extracting The Integer Index. */
                i4_fsTcpConnLocalPort =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_fsTcpConnRemAddress[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_fsTcpConnRemAddress =
                    CRU_BMC_DWFROMPDU (u1_addr_fsTcpConnRemAddress);

                /* Extracting The Integer Index. */
                i4_fsTcpConnRemPort =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsTcpConnTable
                     (u4_addr_fsTcpConnLocalAddress, i4_fsTcpConnLocalPort,
                      u4_addr_fsTcpConnRemAddress,
                      i4_fsTcpConnRemPort)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_fsTcpConnLocalAddress[i4_count];

                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsTcpConnLocalPort;
                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_fsTcpConnRemAddress[i4_count];

                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsTcpConnRemPort;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsTcpConnTable
                     (&u4_addr_fsTcpConnLocalAddress, &i4_fsTcpConnLocalPort,
                      &u4_addr_fsTcpConnRemAddress,
                      &i4_fsTcpConnRemPort)) == SNMP_SUCCESS)
                {
                    TCP_DWTOPDU (u1_addr_fsTcpConnLocalAddress,
                                 u4_addr_fsTcpConnLocalAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_fsTcpConnLocalAddress[i4_count];

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsTcpConnLocalPort;
                    TCP_DWTOPDU (u1_addr_fsTcpConnRemAddress,
                                 u4_addr_fsTcpConnRemAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_fsTcpConnRemAddress[i4_count];

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsTcpConnRemPort;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_fsTcpConnLocalAddress[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_fsTcpConnLocalAddress =
                        CRU_BMC_DWFROMPDU (u1_addr_fsTcpConnLocalAddress);

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsTcpConnLocalPort =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_fsTcpConnRemAddress[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_fsTcpConnRemAddress =
                        CRU_BMC_DWFROMPDU (u1_addr_fsTcpConnRemAddress);

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsTcpConnRemPort =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsTcpConnTable
                     (u4_addr_fsTcpConnLocalAddress,
                      &u4_addr_next_fsTcpConnLocalAddress,
                      i4_fsTcpConnLocalPort, &i4_next_fsTcpConnLocalPort,
                      u4_addr_fsTcpConnRemAddress,
                      &u4_addr_next_fsTcpConnRemAddress, i4_fsTcpConnRemPort,
                      &i4_next_fsTcpConnRemPort)) == SNMP_SUCCESS)
                {
                    u4_addr_fsTcpConnLocalAddress =
                        u4_addr_next_fsTcpConnLocalAddress;
                    TCP_DWTOPDU (u1_addr_next_fsTcpConnLocalAddress,
                                 u4_addr_fsTcpConnLocalAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_fsTcpConnLocalAddress[i4_count];
                    i4_fsTcpConnLocalPort = i4_next_fsTcpConnLocalPort;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsTcpConnLocalPort;
                    u4_addr_fsTcpConnRemAddress =
                        u4_addr_next_fsTcpConnRemAddress;
                    TCP_DWTOPDU (u1_addr_next_fsTcpConnRemAddress,
                                 u4_addr_fsTcpConnRemAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_fsTcpConnRemAddress[i4_count];
                    i4_fsTcpConnRemPort = i4_next_fsTcpConnRemPort;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsTcpConnRemPort;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSTCPCONNLOCALADDRESS:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == EXACT) || (i4_partial_index_flag == FALSE))
            {
                TCP_DWTOPDU (u1_octet_string, u4_addr_fsTcpConnLocalAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                TCP_DWTOPDU (u1_octet_string,
                             u4_addr_next_fsTcpConnLocalAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case FSTCPCONNLOCALPORT:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == EXACT) || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsTcpConnLocalPort;
            }
            else
            {
                i4_return_val = i4_next_fsTcpConnLocalPort;
            }
            break;
        }
        case FSTCPCONNREMADDRESS:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == EXACT) || (i4_partial_index_flag == FALSE))
            {
                TCP_DWTOPDU (u1_octet_string, u4_addr_fsTcpConnRemAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                TCP_DWTOPDU (u1_octet_string, u4_addr_next_fsTcpConnRemAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case FSTCPCONNREMPORT:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == EXACT) || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsTcpConnRemPort;
            }
            else
            {
                i4_return_val = i4_next_fsTcpConnRemPort;
            }
            break;
        }
        case FSTCPCONNOUTSTATE:
        {
            i1_ret_val =
                nmhGetFsTcpConnOutState (u4_addr_fsTcpConnLocalAddress,
                                         i4_fsTcpConnLocalPort,
                                         u4_addr_fsTcpConnRemAddress,
                                         i4_fsTcpConnRemPort, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPCONNSWINDOW:
        {
            i1_ret_val =
                nmhGetFsTcpConnSWindow (u4_addr_fsTcpConnLocalAddress,
                                        i4_fsTcpConnLocalPort,
                                        u4_addr_fsTcpConnRemAddress,
                                        i4_fsTcpConnRemPort, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPCONNRWINDOW:
        {
            i1_ret_val =
                nmhGetFsTcpConnRWindow (u4_addr_fsTcpConnLocalAddress,
                                        i4_fsTcpConnLocalPort,
                                        u4_addr_fsTcpConnRemAddress,
                                        i4_fsTcpConnRemPort, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPCONNCWINDOW:
        {
            i1_ret_val =
                nmhGetFsTcpConnCWindow (u4_addr_fsTcpConnLocalAddress,
                                        i4_fsTcpConnLocalPort,
                                        u4_addr_fsTcpConnRemAddress,
                                        i4_fsTcpConnRemPort, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPCONNSSTHRESH:
        {
            i1_ret_val =
                nmhGetFsTcpConnSSThresh (u4_addr_fsTcpConnLocalAddress,
                                         i4_fsTcpConnLocalPort,
                                         u4_addr_fsTcpConnRemAddress,
                                         i4_fsTcpConnRemPort, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPCONNSMSS:
        {
            i1_ret_val =
                nmhGetFsTcpConnSMSS (u4_addr_fsTcpConnLocalAddress,
                                     i4_fsTcpConnLocalPort,
                                     u4_addr_fsTcpConnRemAddress,
                                     i4_fsTcpConnRemPort, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPCONNRMSS:
        {
            i1_ret_val =
                nmhGetFsTcpConnRMSS (u4_addr_fsTcpConnLocalAddress,
                                     i4_fsTcpConnLocalPort,
                                     u4_addr_fsTcpConnRemAddress,
                                     i4_fsTcpConnRemPort, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPCONNSRT:
        {
            i1_ret_val =
                nmhGetFsTcpConnSRT (u4_addr_fsTcpConnLocalAddress,
                                    i4_fsTcpConnLocalPort,
                                    u4_addr_fsTcpConnRemAddress,
                                    i4_fsTcpConnRemPort, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPCONNRTDE:
        {
            i1_ret_val =
                nmhGetFsTcpConnRTDE (u4_addr_fsTcpConnLocalAddress,
                                     i4_fsTcpConnLocalPort,
                                     u4_addr_fsTcpConnRemAddress,
                                     i4_fsTcpConnRemPort, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPCONNPERSIST:
        {
            i1_ret_val =
                nmhGetFsTcpConnPersist (u4_addr_fsTcpConnLocalAddress,
                                        i4_fsTcpConnLocalPort,
                                        u4_addr_fsTcpConnRemAddress,
                                        i4_fsTcpConnRemPort, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPCONNREXMT:
        {
            i1_ret_val =
                nmhGetFsTcpConnRexmt (u4_addr_fsTcpConnLocalAddress,
                                      i4_fsTcpConnLocalPort,
                                      u4_addr_fsTcpConnRemAddress,
                                      i4_fsTcpConnRemPort, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPCONNREXMTCNT:
        {
            i1_ret_val =
                nmhGetFsTcpConnRexmtCnt (u4_addr_fsTcpConnLocalAddress,
                                         i4_fsTcpConnLocalPort,
                                         u4_addr_fsTcpConnRemAddress,
                                         i4_fsTcpConnRemPort, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPCONNSBCOUNT:
        {
            i1_ret_val =
                nmhGetFsTcpConnSBCount (u4_addr_fsTcpConnLocalAddress,
                                        i4_fsTcpConnLocalPort,
                                        u4_addr_fsTcpConnRemAddress,
                                        i4_fsTcpConnRemPort, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPCONNSBSIZE:
        {
            i1_ret_val =
                nmhGetFsTcpConnSBSize (u4_addr_fsTcpConnLocalAddress,
                                       i4_fsTcpConnLocalPort,
                                       u4_addr_fsTcpConnRemAddress,
                                       i4_fsTcpConnRemPort, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPCONNRBCOUNT:
        {
            i1_ret_val =
                nmhGetFsTcpConnRBCount (u4_addr_fsTcpConnLocalAddress,
                                        i4_fsTcpConnLocalPort,
                                        u4_addr_fsTcpConnRemAddress,
                                        i4_fsTcpConnRemPort, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPCONNRBSIZE:
        {
            i1_ret_val =
                nmhGetFsTcpConnRBSize (u4_addr_fsTcpConnLocalAddress,
                                       i4_fsTcpConnLocalPort,
                                       u4_addr_fsTcpConnRemAddress,
                                       i4_fsTcpConnRemPort, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPKAMAINTMR:
        {
            i1_ret_val =
                nmhGetFsTcpKaMainTmr (u4_addr_fsTcpConnLocalAddress,
                                      i4_fsTcpConnLocalPort,
                                      u4_addr_fsTcpConnRemAddress,
                                      i4_fsTcpConnRemPort, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPKARETRANSTMR:
        {
            i1_ret_val =
                nmhGetFsTcpKaRetransTmr (u4_addr_fsTcpConnLocalAddress,
                                         i4_fsTcpConnLocalPort,
                                         u4_addr_fsTcpConnRemAddress,
                                         i4_fsTcpConnRemPort, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSTCPKARETRANSCNT:
        {
            i1_ret_val =
                nmhGetFsTcpKaRetransCnt (u4_addr_fsTcpConnLocalAddress,
                                         i4_fsTcpConnLocalPort,
                                         u4_addr_fsTcpConnRemAddress,
                                         i4_fsTcpConnRemPort, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsTcpConnEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsTcpConnEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                   UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsTcpConnLocalAddress = FALSE;
    UINT1               u1_addr_fsTcpConnLocalAddress[ADDR_LEN] = NULL_STRING;

    INT4                i4_fsTcpConnLocalPort = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsTcpConnRemAddress = FALSE;
    UINT1               u1_addr_fsTcpConnRemAddress[ADDR_LEN] = NULL_STRING;

    INT4                i4_fsTcpConnRemPort = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += ADDR_LEN;
        i4_size_offset += INTEGER_LEN;
        i4_size_offset += ADDR_LEN;
        i4_size_offset += INTEGER_LEN;
        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)
            u1_addr_fsTcpConnLocalAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_fsTcpConnLocalAddress =
            CRU_BMC_DWFROMPDU (u1_addr_fsTcpConnLocalAddress);

        /* Extracting The Integer Index. */
        i4_fsTcpConnLocalPort =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)
            u1_addr_fsTcpConnRemAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_fsTcpConnRemAddress =
            CRU_BMC_DWFROMPDU (u1_addr_fsTcpConnRemAddress);

        /* Extracting The Integer Index. */
        i4_fsTcpConnRemPort =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSTCPKAMAINTMR:
        {
            i1_ret_val =
                nmhSetFsTcpKaMainTmr (u4_addr_fsTcpConnLocalAddress,
                                      i4_fsTcpConnLocalPort,
                                      u4_addr_fsTcpConnRemAddress,
                                      i4_fsTcpConnRemPort,
                                      p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSTCPKARETRANSTMR:
        {
            i1_ret_val =
                nmhSetFsTcpKaRetransTmr (u4_addr_fsTcpConnLocalAddress,
                                         i4_fsTcpConnLocalPort,
                                         u4_addr_fsTcpConnRemAddress,
                                         i4_fsTcpConnRemPort,
                                         p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSTCPKARETRANSCNT:
        {
            i1_ret_val =
                nmhSetFsTcpKaRetransCnt (u4_addr_fsTcpConnLocalAddress,
                                         i4_fsTcpConnLocalPort,
                                         u4_addr_fsTcpConnRemAddress,
                                         i4_fsTcpConnRemPort,
                                         p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSTCPCONNLOCALADDRESS:
            /*  Read Only Variables. */
        case FSTCPCONNLOCALPORT:
            /*  Read Only Variables. */
        case FSTCPCONNREMADDRESS:
            /*  Read Only Variables. */
        case FSTCPCONNREMPORT:
            /*  Read Only Variables. */
        case FSTCPCONNOUTSTATE:
            /*  Read Only Variables. */
        case FSTCPCONNSWINDOW:
            /*  Read Only Variables. */
        case FSTCPCONNRWINDOW:
            /*  Read Only Variables. */
        case FSTCPCONNCWINDOW:
            /*  Read Only Variables. */
        case FSTCPCONNSSTHRESH:
            /*  Read Only Variables. */
        case FSTCPCONNSMSS:
            /*  Read Only Variables. */
        case FSTCPCONNRMSS:
            /*  Read Only Variables. */
        case FSTCPCONNSRT:
            /*  Read Only Variables. */
        case FSTCPCONNRTDE:
            /*  Read Only Variables. */
        case FSTCPCONNPERSIST:
            /*  Read Only Variables. */
        case FSTCPCONNREXMT:
            /*  Read Only Variables. */
        case FSTCPCONNREXMTCNT:
            /*  Read Only Variables. */
        case FSTCPCONNSBCOUNT:
            /*  Read Only Variables. */
        case FSTCPCONNSBSIZE:
            /*  Read Only Variables. */
        case FSTCPCONNRBCOUNT:
            /*  Read Only Variables. */
        case FSTCPCONNRBSIZE:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsTcpConnEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsTcpConnEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                    UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsTcpConnLocalAddress = FALSE;
    UINT1               u1_addr_fsTcpConnLocalAddress[ADDR_LEN] = NULL_STRING;

    INT4                i4_fsTcpConnLocalPort = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsTcpConnRemAddress = FALSE;
    UINT1               u1_addr_fsTcpConnRemAddress[ADDR_LEN] = NULL_STRING;

    INT4                i4_fsTcpConnRemPort = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += ADDR_LEN;
        i4_size_offset += INTEGER_LEN;
        i4_size_offset += ADDR_LEN;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)
            u1_addr_fsTcpConnLocalAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_fsTcpConnLocalAddress =
            CRU_BMC_DWFROMPDU (u1_addr_fsTcpConnLocalAddress);

        /* Extracting The Integer Index. */
        i4_fsTcpConnLocalPort =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)
            u1_addr_fsTcpConnRemAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_fsTcpConnRemAddress =
            CRU_BMC_DWFROMPDU (u1_addr_fsTcpConnRemAddress);

        /* Extracting The Integer Index. */
        i4_fsTcpConnRemPort =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsTcpConnTable(u4_addr_fsTcpConnLocalAddress , i4_fsTcpConnLocalPort , u4_addr_fsTcpConnRemAddress , i4_fsTcpConnRemPort)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case FSTCPKAMAINTMR:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsTcpKaMainTmr (&u4ErrorCode,
                                         u4_addr_fsTcpConnLocalAddress,
                                         i4_fsTcpConnLocalPort,
                                         u4_addr_fsTcpConnRemAddress,
                                         i4_fsTcpConnRemPort,
                                         p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSTCPKARETRANSTMR:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsTcpKaRetransTmr (&u4ErrorCode,
                                            u4_addr_fsTcpConnLocalAddress,
                                            i4_fsTcpConnLocalPort,
                                            u4_addr_fsTcpConnRemAddress,
                                            i4_fsTcpConnRemPort,
                                            p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSTCPKARETRANSCNT:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsTcpKaRetransCnt (&u4ErrorCode,
                                            u4_addr_fsTcpConnLocalAddress,
                                            i4_fsTcpConnLocalPort,
                                            u4_addr_fsTcpConnRemAddress,
                                            i4_fsTcpConnRemPort,
                                            p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSTCPCONNLOCALADDRESS:
        case FSTCPCONNLOCALPORT:
        case FSTCPCONNREMADDRESS:
        case FSTCPCONNREMPORT:
        case FSTCPCONNOUTSTATE:
        case FSTCPCONNSWINDOW:
        case FSTCPCONNRWINDOW:
        case FSTCPCONNCWINDOW:
        case FSTCPCONNSSTHRESH:
        case FSTCPCONNSMSS:
        case FSTCPCONNRMSS:
        case FSTCPCONNSRT:
        case FSTCPCONNRTDE:
        case FSTCPCONNPERSIST:
        case FSTCPCONNREXMT:
        case FSTCPCONNREXMTCNT:
        case FSTCPCONNSBCOUNT:
        case FSTCPCONNSBSIZE:
        case FSTCPCONNRBCOUNT:
        case FSTCPCONNRBSIZE:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */
