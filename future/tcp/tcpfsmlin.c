
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tcpfsmlin.c,v 1.30 2016/07/05 08:22:23 siva Exp $
 *
 * Description: This file contains the routines exported by CFA
 *              to the othe modules.
 *******************************************************************/
/* SOURCE FILE HEADER
  *
  * ----------------------------------------------------------------------------
  *  FILE NAME             : tcpfsm.c
  * 
  *  PRINCIPAL AUTHOR      : Ramesh Kumar
  *
  *  SUBSYSTEM NAME        : TCP
  *
  *  MODULE NAME           : Finite State Machine
  *
  *  LANGUAGE              : C
  *
  *  TARGET ENVIRONMENT    : Any
  *
  *  DATE OF FIRST RELEASE :
  * 
  *  DESCRIPTION           : This file contains routines corresponding to 
  *                          states of TCP finite state machine.
  *
  * ---------------------------------------------------------------------------
  *
  *
  *  CHANGE RECORD :
  *
  * ---------------------------------------------------------------------------
  *   VERSION        AUTHOR/DATE           DESCRIPTION OF CHANGE
  * ---------------------------------------------------------------------------
  *
  *                  Sri. Chellapa            Changed all    
  *                  13/05/96                 sequence no. Comparisons to use 
  *                                           SEQCMP macro.   
  *    1.5          Rebecca Rufus             Higher Layer Application is 
  *                                           intimated of connection     
  *                                           establishment only at the end of 
  *                                           the three way handshake.Refer       *                                           Problem Id 3.
  *    1.6          Rebecca Rufus             Generation of RESET on reception of
  *                  12-Oct-'98.              ACK in LISTEN state.Refer Problem  
  *                                           Id 13.                             
  *                                           Corrected the command responses to 
  *                                           SLI. Refer Problem Id 16.         
  *    -            Ramakrishnan              Added code to support select call
  *                  1 march 2000             under the ST_ENHLISTINTF switch
  *                 Ramakrishnan              Changed ST_ENHLISTINTF to ST_ENH
  *                 5th June 2000
  * ------------------------------------------------------------------------*
  *     1.8      Saravanan.M    Implementation of                           *
  *              Purush            - Non Blocking Connect                   * 
  *                                - Non Blocking Accept                    *
  *                                - IfMsg Interface between TCP and IP     *
  *                                - One Queue per Socket                   *
  * ------------------------------------------------------------------------ */

#include "tcpinc.h"

/*************************************************************************/
/*  Function Name   : FsmFree                                            */
/*  Input(s)        : ptcb - Pointer to the TCB                          */
/*                    pSeg - pointer to the Segment received             */
/*                    pSegIn - IpTcp Header Structure                    */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK                                             */
/*  Description     : Process segment in FREE state                      */
/*************************************************************************/
INT1
FsmFree (tTcb * ptcb, tSegment * pSeg, tIpTcpHdr * pSegIn)
{
    UNUSED_PARAM (pSeg);
    UNUSED_PARAM (pSegIn);
    UNUSED_PARAM (ptcb);

    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC | TCP_FSM_TRC, "TCP",
                 "Entering FsmFree for CONN ID - %d\n", GET_TCB_INDEX (ptcb));
    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FsmFree. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : FsmClosed                                          */
/*  Input(s)        : ptcb - Pointer to the TCB                          */
/*                    pSeg - pointer to the Segment received             */
/*                    pSegIn - IpTcp Header Structure                    */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK                                             */
/*  Description     : This routine handles both the application request  */
/*                    for close and any segment coming in at closedstate */
/* CALLING CONDITION : It may be called through user close command or    */
/*                     when the segment comes in the close state.        */
/* GLOBAL VARIABLES AFFECTED : None                                      */
/*************************************************************************/
INT1
FsmClosed (tTcb * ptcb, tSegment * pSeg, tIpTcpHdr * pSegIn)
{
    UNUSED_PARAM (pSeg);

    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC | TCP_FSM_TRC, "TCP",
                 "Entering FsmClosed for CONN ID - %d\n", GET_TCB_INDEX (ptcb));
    if (pSegIn->tcp->u1Code & TCPF_RST)
    {
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmClosed. Received RESET. Returned OK.\n");
        return TCP_OK;
    }
    LINOsmSendReset (ptcb->u4Context, pSegIn);
    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FsmClosed. Sent RST and returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : FsmListen                                          */
/*  Input(s)        : ptcb - Pointer to the TCB                          */
/*                    pSeg - pointer to the Segment received             */
/*                    pSegIn - IpTcp Header Structure                    */
/*  Output(s)       : None                                               */
/*  Returns         : SYSERR/SUCCESS                                     */
/*  Description     : This state is used by servers to await connections */
/*                    from clients.  It creates a new TCB in listen      */
/*                    state for each incoming connection, while updating */
/*                    the old TCB.                                       */
/* CALLING CONDITION :                                                   */
/* GLOBAL VARIABLES AFFECTED None                                        */
/*************************************************************************/
INT1
FsmListen (tTcb * ptcb, tSegment * pSeg, tIpTcpHdr * pSegIn)
{
    tTcb               *pNewTcb;
    tIpAddr             SrcAddr, DestAddr;
    UINT2               u2Sport, u2Dport;
    UINT1               u1Code;
    UINT4               u4NewIndex, u4OldIndex;
    INT1                i1RetVal;
    UINT1               au1BlockSemName[4];    /* Read block sema4 name */
    UINT1               au1MainSemName[4];    /* connection MUX sema4 name */
    UINT4               u4RcvBufLen = 0;
#ifdef SLI_WANTED
    UINT4               u4PendingMsgs;
#endif
    UINT4               u4Context;
    INT4                i4SockDesc = TCP_ZERO;
    UINT1               au1Md5Key[TCP_MD5SIG_MAXKEYLEN];
    UINT1               u1Md5Keylen = TCP_ZERO;
    tsliTcpAoMktListNode *ptcpAoMktNd = NULL;

    u4OldIndex = GET_TCB_INDEX (ptcb);
    u4Context = GetTCBContext (u4OldIndex);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_FSM_TRC, "TCP",
                 "Entering FsmListen for CONN ID - %d\n", GET_TCB_INDEX (ptcb));

    MEMSET (au1Md5Key, TCP_ZERO, sizeof (au1Md5Key));

    u1Code = pSegIn->tcp->u1Code;

    if (u1Code & TCPF_RST)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmListen. Received RESET.Returned SUCCESS.\n");
        return SUCCESS;            /*Because there is no connection */
    }

    /* In Listen, only SYN is acceptable (no other flags should be set) */
    if (u1Code != TCPF_SYN)
    {
        i1RetVal = LINOsmSendReset (u4Context, pSegIn);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmListen.SYN not Received.or "
                     " received with Other Flags set. Returned %d.\n",
                     i1RetVal);
        return i1RetVal;
    }

    /* See if this request can be accomodated. If the application's
       queue is already full then send a reset. */

#ifdef SLI_WANTED
    AcceptMesgCount (u4OldIndex, &u4PendingMsgs);
    if (u4PendingMsgs >= GetTCBmsgcnt (u4OldIndex))
    {
        i1RetVal = LINOsmSendReset (u4Context, pSegIn);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmListen. Application queue full.Returned %d.\n",
                     i1RetVal);
        return i1RetVal;
    }
#endif
    /*When a SYN comes change TCB state to syn_rcvd and
       allocate a new TCB in listen state */

    pNewTcb = tcballoc (u4Context);

    if (pNewTcb == NULL)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from FsmListen. Returned SYSERR.\n");
        return SYSERR;
    }

    u4NewIndex = GET_TCB_INDEX (pNewTcb);

    IncTCPstat (u4Context, PassiveOpens);
    GetTCBSockDesc (u4NewIndex) = -1;
    SetTCBvalopts (u4NewIndex, GetTCBvalopts (u4OldIndex));
    SetTCBtype (u4NewIndex, TCP_CONNECTION);
    SetTCBstate (u4NewIndex, TCPS_SYNRCVD);
    TCP_MOD_TRC (u4Context, TCP_FSM_TRC, "TCP",
                 "FSM change of state to SYNRCVD for CONN ID - %d\n",
                 u4NewIndex);
    SetTCBostate (u4NewIndex, TCPO_IDLE);
    SetTCBpptcb (u4NewIndex, ptcb);

    u4RcvBufLen = (GetTCBrbsize (u4OldIndex) > TCP_MIN_RECV_BUF_SIZE) ?
        GetTCBrbsize (u4OldIndex) : TCP_MIN_RECV_BUF_SIZE;

    if ((TcpSync (pNewTcb, u4RcvBufLen)) != TCP_OK)
    {
        deallocate (pNewTcb);
        return ERR;
    }

    /*Copy local & remote socket address from the TCP seg */
    u2Dport = OSIX_NTOHS (pSegIn->tcp->u2Dport);
    u2Sport = OSIX_NTOHS (pSegIn->tcp->u2Sport);

    SetTCBlport (u4NewIndex, u2Dport);
    SetTCBrport (u4NewIndex, u2Sport);

    /* Note that the packet contains all fields in network byte order */
    V6_NTOHL (&SrcAddr, &((pSegIn->ip)->DestAddr));
    V6_NTOHL (&DestAddr, &((pSegIn->ip)->SrcAddr));
    SetTCBlip (u4NewIndex, SrcAddr);
    SetTCBrip (u4NewIndex, DestAddr);
    if (IsIP6Addr (SrcAddr))
    {
        SetTCBLipAddType (u4NewIndex, IPV6_ADD_TYPE);
    }
    else
    {
        SetTCBLipAddType (u4NewIndex, IPV4_ADD_TYPE);
    }
    if (IsIP6Addr (DestAddr))
    {
        SetTCBRipAddType (u4NewIndex, IPV6_ADD_TYPE);
    }
    else
    {
        SetTCBRipAddType (u4NewIndex, IPV4_ADD_TYPE);
    }

    i4SockDesc = SliTcpFindSockDesc (u4OldIndex);
    i1RetVal = SliTcpFindMd5Key (i4SockDesc, (pSegIn->ip)->SrcAddr,
                                 au1Md5Key, &u1Md5Keylen);
    if ((i1RetVal == TRUE) && (u1Md5Keylen <= TCP_MD5SIG_MAXKEYLEN))
    {
        MEMSET (GetTCBpmd5key (u4NewIndex), TCP_ZERO, TCP_MD5SIG_MAXKEYLEN);
        MEMCPY (GetTCBpmd5key (u4NewIndex), au1Md5Key, u1Md5Keylen);
        SetTCBmd5keylen (u4NewIndex, u1Md5Keylen);
    }
    else
    {
        /* Set Md5 key length to ZERO for default case */
        SetTCBmd5keylen (u4NewIndex, TCP_ZERO);
    }
    /*if (NULL != (ptcpAoMktNd = SliGetTcpAo(i4SockDesc, SrcAddr))) */
    if (NULL != (ptcpAoMktNd = SliGetTcpAo (i4SockDesc, pSegIn->ip->SrcAddr)))
    {
        SetRcvIsn (u4NewIndex, ((OSIX_NTOHL ((UINT4) pSegIn->tcp->i4Seq))));
        SetTcpAoMacVerErrCtr (u4NewIndex, TCP_ZERO);
        SetTcpAoSendSne (u4NewIndex, TCP_ZERO);
        SetTcpAoRecvSne (u4NewIndex, TCP_ZERO);
        SetIcmpIgnCtr (u4NewIndex, TCP_ZERO);
        SetSilentAcceptCtr (u4NewIndex, TCP_ZERO);
        SetTCBTcpAoEnabled (u4NewIndex, TRUE);
        SetTCBTcpAoNoMktDisc (u4NewIndex, TCP_ZERO);
        SetTCBTcpAoIcmpAcc (u4NewIndex, TCP_ZERO);
        SetTCBTcpAoRcvPckKeyId (u4NewIndex, TCP_ZERO);
        SetTCBTcpAoRcvPckRNxtKeyId (u4NewIndex, TCP_ZERO);
        SetTCBTcpAoActiveInd (u4NewIndex, TCPAO_MKT_INI_IND);
        SetTCBTcpAoNewInd (u4NewIndex, TCPAO_MKT_INI_IND);
        SetTCBTcpAoBackupInd (u4NewIndex, TCPAO_MKT_INI_IND);
        SetTcbTcpAoMkt (u4NewIndex, ptcpAoMktNd->TcpAoKey.u1SendKeyId,
                        ptcpAoMktNd->TcpAoKey.u1RcvKeyId,
                        ptcpAoMktNd->TcpAoKey.u1ShaAlgo,
                        ptcpAoMktNd->TcpAoKey.u1ShaAlgo,
                        ptcpAoMktNd->TcpAoKey.au1Key,
                        ((UINT1) STRLEN (ptcpAoMktNd->TcpAoKey.au1Key)),
                        ptcpAoMktNd->TcpAoKey.u1TcpOptIgnore);
        SetTCBTcpAoNoMktDisc (u4NewIndex, (ptcpAoMktNd->u1NoMktMchPckDsc));
        SetTCBTcpAoIcmpAcc (u4NewIndex, (ptcpAoMktNd->u1IcmpAccpt));
        /* Calculate traffic keys */
        TcpGenerateTCPAOTrafficKey (u4NewIndex,
                                    ptcpAoMktNd->TcpAoKey.u1SendKeyId,
                                    TCPAO_SND_TRFKEY);
        TcpGenerateTCPAOTrafficKey (u4NewIndex,
                                    ptcpAoMktNd->TcpAoKey.u1SendKeyId,
                                    TCPAO_RCV_TRFKEY);
    }
    else
    {
        UINT1              *pu1TcpAoCfg;
        if ((pu1TcpAoCfg = SliGetTcpAoNghMktCfg (i4SockDesc, SrcAddr)) != NULL)
        {
            SetTCBTcpAoNoMktDisc (u4NewIndex, (*pu1TcpAoCfg));
            pu1TcpAoCfg = SliGetTcpAoNghIcmpCfg (i4SockDesc, SrcAddr);
            if (pu1TcpAoCfg != NULL)
                SetTCBTcpAoIcmpAcc (u4NewIndex, (*pu1TcpAoCfg));
        }
        else
        {
            SetTCBTcpAoNoMktDisc (u4NewIndex, TCP_ONE);
            SetTCBTcpAoIcmpAcc (u4NewIndex, TCP_TWO);
        }
        SetTCBTcpAoEnabled (u4NewIndex, TCP_ZERO);
        SetIcmpIgnCtr (u4NewIndex, TCP_ZERO);
        SetSilentAcceptCtr (u4NewIndex, TCP_ZERO);
    }

    SetTCBContext (u4NewIndex, u4Context);
    SetTCBIfIndex (u4NewIndex, IPIF_INVALID_INDEX);

    SetTCBmsgcnt (u4NewIndex, GetTCBmsgcnt (u4OldIndex));

    LINFsmInitPassiveConn (ptcb, pNewTcb, pSegIn->tcp);

    SetTCBfinseq (u4NewIndex, ZERO);
    SetTCBpushseq (u4NewIndex, ZERO);

    SetTCBKaMainTmOut (u4NewIndex, TCP_KA_DEF_MAIN_TMR_TICKS);
    SetTCBKaRetransTmOut (u4NewIndex, TCP_KA_DEF_RETRANS_TMR_TICKS);
    SetTCBKaRetransCnt (u4NewIndex, TCP_KA_DEF_RETRIES);
    SetTCBKaRetransOverCnt (u4NewIndex, ZERO);
    GenerateSemName ('B', (char *) au1BlockSemName);
    if (OsixCreateSem (au1BlockSemName,
                       TCP_ONE,
                       OSIX_WAIT,
                       &(TCBtable[u4NewIndex].BlockSemId)) != OSIX_SUCCESS)
    {
        deallocate (pNewTcb);
        return ERR;
    }
    GenerateSemName ('M', (char *) au1MainSemName);
    if (OsixCreateSem (au1MainSemName,
                       TCP_ONE,
                       OSIX_WAIT,
                       &(TCBtable[u4NewIndex].MainSemId)) != OSIX_SUCCESS)
    {
        deallocate (pNewTcb);
        return ERR;
    }
    pNewTcb->u2TcbFlags = TCBF_NEEDOUT;
    LINFsmProcessData (pNewTcb, pSeg, pSegIn);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FsmListen. Returned OK.\n");

    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : FsmSynsent                                         */
/*  Input(s)        : ptcb - Pointer to the TCB                          */
/*                    pSeg - pointer to the Segment received             */
/*                    pSegIn - IpTcp Header Structure                    */
/*  Output(s)       : None.                                              */
/*  Returns         : SYSERR/SUCCESS                                     */
/*  Description     : Once TCP sends a SYN request, it moves to the      */
/*                    syn_sent state.                                    */
/* CALLING CONDITION :                                                   */
/* GLOBAL VARIABLES AFFECTED None                                        */
/*************************************************************************/
INT1
FsmSynsent (tTcb * ptcb, tSegment * pSeg, tIpTcpHdr * pSegIn)
{
    INT4                i4Ack;
    INT4                i4SeqNo;
    UINT2               u2Win;
    UINT4               u4Index;
    UINT1               u1Code;
    INT4                i4RetVal = 0;

    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC | TCP_FSM_TRC, "TCP",
                 "Entering FsmSynsent for CONN ID - %d\n",
                 GET_TCB_INDEX (ptcb));
    u4Index = GET_TCB_INDEX (ptcb);
    u1Code = pSegIn->tcp->u1Code;
    i4Ack = OSIX_NTOHL (pSegIn->tcp->i4Ack);

    if (u1Code & TCPF_ACK)
    {
        if ((SEQCMP (i4Ack, GetTCBiss (u4Index)) <= ZERO) ||
            (SEQCMP (i4Ack, GetTCBsnext (u4Index)) > ZERO))
        {
            if (u1Code & TCPF_RST)
            {
                TCP_MOD_TRC (ptcb->u4Context,
                             TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                             "Exit from FsmSynsent. Received RESET.Returned ERR.\n");
                return ERR;
            }
            LINOsmSendReset (GetTCBContext (u4Index), pSegIn);
            TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                         "TCP", "Exit from FsmSynsent. Returned ERR.\n");
            return ERR;
        }
    }

    if (u1Code & TCPF_RST)
    {
        SetTCBstate (u4Index, TCPS_CLOSED);
        TCP_MOD_TRC (ptcb->u4Context, TCP_FSM_TRC, "TCP",
                     "FSM state changed to CLOSED for CONN ID - %d\n", u4Index);
        IncTCPstat (GetTCBContext (u4Index), AttemptFails);
        FsmAbortConn (ptcb, TCPE_CONN_REFUSED);
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmSynsent. Received RESET.Returned SUCCESS.\n");
        return SUCCESS;
    }

    if ((u1Code & TCPF_SYN) == ZERO)
    {
        /*No SYN has come */
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmSynsent. Received SYN. Returned SUCCESS.\n");
        return SUCCESS;
    }

    /*Incoming segment has SYN */

    u2Win = OSIX_NTOHS (pSegIn->tcp->u2Window);
    i4SeqNo = OSIX_NTOHL (pSegIn->tcp->i4Seq);

    SetTCBswindow (u4Index, (UINT4) u2Win);
    if (GetTCBMaxswnd (u4Index) < ((UINT4) u2Win))
    {
        SetTCBMaxswnd (u4Index, (UINT4) u2Win);
    }
    SetTCBrnext (u4Index, i4SeqNo);
    SetTCBlwseq (u4Index, i4SeqNo);

    if (u1Code & TCPF_ACK)
    {
        /* FsmSynsent */
        i4Ack = OSIX_NTOHL (pSegIn->tcp->i4Ack);
        SetTCBlwack (u4Index, i4Ack);
    }
    else
    {
        /* Send SYN with previous Sequence */
        SetTCBsnext (u4Index, GetTCBiss (u4Index));
    }

    SetTCBflags_AND (u4Index, ~TCBF_FIRSTSEND);    /* Enable ACKs */
    SetTCBcwin (u4Index, (INT4) ((UINT4) GetTCBrnext (u4Index)
                                 + GetTCBrbsize (u4Index)));

    i4RetVal = LINFarHandleAck (ptcb, pSegIn);
    LINFsmProcessData (ptcb, pSeg, pSegIn);    /* increments rnext by one */
    SetTCBcwin (u4Index, (INT4) ((UINT4) GetTCBrnext (u4Index)
                                 + GetTCBrbsize (u4Index)));

    u1Code = pSegIn->tcp->u1Code;
    if (u1Code & TCPF_SYN)
    {
        SetTCBstate (u4Index, TCPS_SYNRCVD);
        TCP_MOD_TRC (ptcb->u4Context, TCP_FSM_TRC, "TCP",
                     "FSM state changed to SYNRCVD for CONN ID - %d\n",
                     u4Index);
    }

    if (u1Code & TCPF_ACK)
    {
        SetTCBstate (u4Index, TCPS_ESTABLISHED);
        TCP_MOD_TRC (ptcb->u4Context, TCP_FSM_TRC, "TCP",
                     "FSM state changed to ESTABLISHED for CONN ID - %d\n",
                     u4Index);
        HliReplyForOpen (u4Index);
        SetTCBhlicmd (u4Index, ZERO);
        /* TCP connection is ready for write when connection moves to
         * established state, useful for non-blocking connect */
#ifdef SLI_WANTED
        SliSelectScanList (GetTCBSockDesc (u4Index), SELECT_WRITE);
#endif
    }
    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FsmSynsent. Returned SUCCESS.\n");
    UNUSED_PARAM (i4RetVal);
    return SUCCESS;
}

/*************************************************************************/
/*  Function Name   : FsmSynrcvd                                         */
/*  Input(s)        : ptcb - Pointer to the TCB                          */
/*                    pSeg - pointer to the Segment received             */
/*                    pSegIn - IpTcp Header Structure                    */
/*  Output(s)       : None.                                              */
/*  Returns         : SYSERR/SUCCESS                                     */
/*  Description     : TCP places a connection in this state either when  */
/*                    a SYN arrives from the other end to inititate a    */
/*                    3 way handshake, or when a SYN arrives without an  */
/*                    ACK and the connection is in the syn_sent state.   */
/* CALLING CONDITION :                                                   */
/* GLOBAL VARIABLES AFFECTED None.                                       */
/*************************************************************************/
INT1
FsmSynrcvd (tTcb * ptcb, tSegment * pSeg, tIpTcpHdr * pSegIn)
{
    UINT4               u4Index;
    UINT1               u1Code;
    UINT2               u2IpHdrLen;
    UINT1               u1TCPHlen;
    UINT4               u4TcpDataLen;
    INT1                i1RetVal;

    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC | TCP_FSM_TRC, "TCP",
                 "Entering FsmSynrcvd for CONN ID - %d\n",
                 GET_TCB_INDEX (ptcb));
    u4Index = GET_TCB_INDEX (ptcb);
    u1Code = pSegIn->tcp->u1Code;

    if (u1Code & TCPF_RST)
    {
        /* Direct transition from SYN-RCVD state to CLOSED or LISTEN state
           Hence update the MIB variable.                               */

        IncTCPstat (GetTCBContext (u4Index), AttemptFails);
        if (GetTCBpptcb (u4Index) != ZERO)
        {
            i1RetVal = deallocate (ptcb);
            TCP_MOD_TRC (ptcb->u4Context,
                         TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                         "Exit from FsmSynrcvd. Received RESET. Returned %d.\n",
                         i1RetVal);
            return i1RetVal;
        }
        i1RetVal = FsmAbortConn (ptcb, TCPE_RESET);
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP",
                     "Exit from FsmSynrcvd. Received RESET.Returned %d.\n",
                     i1RetVal);
        return i1RetVal;
    }

    if (u1Code & TCPF_SYN)
    {
        LINOsmSendReset (GetTCBContext (u4Index), pSegIn);
        i1RetVal = FsmAbortConn (ptcb, TCPE_RESET);
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmSynrcvd. Received SYN. Returned %d.\n",
                     i1RetVal);
        return i1RetVal;
    }

    if (LINFarHandleAck (ptcb, pSegIn) < ZERO)
    {
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP", "Exit from FsmSynrcvd. Returned ERR.\n");
        return ERR;
    }

    if (GetTCBpptcb (u4Index) != ZERO)
    {
        if (HliReplyForNewConn (GET_TCB_INDEX (GetTCBpptcb (u4Index)), u4Index)
            < ZERO)
        {
            /* Unable to reply for new connection. Kill it */
            i1RetVal = deallocate (ptcb);
            return i1RetVal;
        }
    }

    SetTCBstate (u4Index, TCPS_ESTABLISHED);
    TCP_MOD_TRC (ptcb->u4Context, TCP_FSM_TRC, "TCP",
                 "FSM state changed to ESTABLISHED for CONN ID - %d\n",
                 GET_TCB_INDEX (ptcb));
    u2IpHdrLen = TCP_ZERO;
    u1TCPHlen =
        (UINT1) (((((tTcpHdr *) pSegIn->tcp)->
                   u1Hlen) >> TCP_VER_SHIFT) << TCP_SHIFT_BYTE_TO_WORD);
    u4TcpDataLen =
        (UINT4) ((tIpHdr *) pSegIn->ip)->u2Totlen - (u2IpHdrLen + u1TCPHlen);
    /* Call LINFsmProcessData only if there is any data */
    if (u4TcpDataLen != ZERO)
        LINFsmProcessData (ptcb, pSeg, pSegIn);

    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FsmSynrcvd. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : FsmEstablished                                     */
/*  Input(s)        : ptcb - Pointer to the TCB                          */
/*                    pSeg - pointer to the Segment received             */
/*                    pSegIn - IpTcp Header Structure                    */
/*  Output(s)       : None.                                              */
/*  Returns         : SYSERR/SUCCESS                                     */
/*  Description     : Once the connection is established, both side      */
/*             remains in this state while they exchange data and ACKs.  */
/* CALLING CONDITION :                                                   */
/* GLOBAL VARIABLES AFFECTED None.                                       */
/*************************************************************************/
INT1
FsmEstablished (tTcb * ptcb, tSegment * pSeg, tIpTcpHdr * pSegIn)
{
    UINT4               u4Index;
    UINT1               u1Code;
    INT1                i1RetVal;

    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC | TCP_FSM_TRC, "TCP",
                 "Entering FsmEstablished for CONN ID - %d\n",
                 GET_TCB_INDEX (ptcb));
    u4Index = GET_TCB_INDEX (ptcb);
    u1Code = pSegIn->tcp->u1Code;

    if (u1Code & TCPF_RST)
    {
        IncTCPstat (GetTCBContext (u4Index), EstabResets);
        i1RetVal = FsmAbortConn (ptcb, TCPE_RESET);
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmEstablished. Received RESET. Returned %d\n",
                     i1RetVal);
        return i1RetVal;
    }

    if (u1Code & TCPF_SYN)
    {
        LINOsmSendReset (GetTCBContext (u4Index), pSegIn);
        IncTCPstat (GetTCBContext (u4Index), EstabResets);
        i1RetVal = FsmAbortConn (ptcb, TCPE_RESET);
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmEstablished. Received SYN.Returned %d\n",
                     i1RetVal);
        return i1RetVal;
    }

    if (LINFarHandleAck (ptcb, pSegIn) == SYSERR)
    {
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmEstablished. Returned OK.\n");
        return TCP_OK;
    }

    LINFsmProcessData (ptcb, pSeg, pSegIn);

    u1Code = pSegIn->tcp->u1Code;

    if (!(u1Code & TCPF_FIN))
    {
        if ((!(GetTCBflags (u4Index) & TCBF_RUPOK))
            && (GetTCBrbcount (u4Index)))
        {
            HliSendAsyMsg (u4Index, TCP_NORMAL_DATA_ARRIVED, ZERO, ZERO);
        }
    }

    /* LINFsmProcessData takes the pain of setting RDONE */
    if (GetTCBflags (u4Index) & TCBF_RDONE)
    {
        /*Got a FIN from other side */
        SetTCBstate (u4Index, TCPS_CLOSEWAIT);
        TCP_MOD_TRC (ptcb->u4Context, TCP_FSM_TRC, "TCP",
                     "FSM state changed to CLOSEWAIT for CONN ID - %d\n",
                     u4Index);
        HliSendAsyMsg (u4Index, TCP_CLOSE_ARRIVED, ZERO, ZERO);
#ifdef SLI_WANTED
           SliSelectScanList (GetTCBSockDesc (u4Index), SELECT_READ);
#endif
    }

    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FsmEstablished. Returned SUCCESS.\n");
    return SUCCESS;
}

/*************************************************************************/
/*  Function Name   : FsmFin1                                            */
/*  Input(s)        : ptcb - Pointer to the TCB                          */
/*                    pSeg - pointer to the Segment received             */
/*                    pSegIn - IpTcp Header Structure                    */
/*  Output(s)       : None.                                              */
/*  Returns         : SYSERR/SUCCESS                                     */
/*  Description     : TCP enters fin_wait_1 when the user issues a close */
/*                    operation, causing TCP to send a FIN. The other    */
/*                    can respond with an ACK of the FIN or with FIN of  */
/*                    its own or both. If a FIN arrives alone, the other */
/*                    side must have started to close, so TCP must move  */
/*                    to closing state. If an ACK arrives alone, TCP     */
/*                    moves to fin_wait_2 state. Finally if both arrive, */
/*                    TCP moves to time_wait state.                      */
/* CALLING CONDITION :                                                   */
/* GLOBAL VARIABLES AFFECTED None                                        */
/*************************************************************************/
INT1
FsmFin1 (tTcb * ptcb, tSegment * pSeg, tIpTcpHdr * pSegIn)
{
    UINT4               u4Index;
    INT4                i4AckRetVal;
    UINT1               u1Code;
    INT1                i1RetVal;

    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC | TCP_FSM_TRC, "TCP",
                 "Entering FsmFin1 for CONN ID - %d\n", GET_TCB_INDEX (ptcb));
    u4Index = GET_TCB_INDEX (ptcb);
    u1Code = pSegIn->tcp->u1Code;

    SetTCBcode (u4Index, (GetTCBcode (u4Index) | u1Code));

    if (u1Code & TCPF_RST)
    {
        i1RetVal = FsmAbortConn (ptcb, TCPE_RESET);
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmFin1. Received RESET.Returned %d\n",
                     i1RetVal);
        return i1RetVal;
    }

    if (u1Code & TCPF_SYN)
    {
        LINOsmSendReset (GetTCBContext (u4Index), pSegIn);
        i1RetVal = FsmAbortConn (ptcb, TCPE_RESET);
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmFin1. Received SYN.Returned %d\n", i1RetVal);
        return i1RetVal;
    }

    if (GetTCBflags (u4Index) & TCBF_FINNOTSENT)
    {
        /* FIN is not sent to the peer in transmission/re-transmission, 
         * below peer ack handle may trigger the data send with 
         * FIN flag set incase the peer window is available,
         * so disable retransmit fin flag */
        SetTCBflags_AND (u4Index, ~TCBF_REXMTFIN);
    }
    i4AckRetVal = LINFarHandleAck (ptcb, pSegIn);
    if (GetTCBflags (u4Index) & TCBF_SNDFIN)
    {
        /* Enable the retransmit fin flag, so that FIN will be 
         * sent in the re-transmission */
        SetTCBflags (u4Index, TCBF_REXMTFIN);
    }
    if (i4AckRetVal == SYSERR)
    {
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmFin1. Returned OK.\n");
        return TCP_OK;
    }

    LINFsmProcessData (ptcb, pSeg, pSegIn);

    if (u1Code & TCPF_FIN)
    {
        SetTCBstate (u4Index, TCPS_CLOSING);
        TCP_MOD_TRC (ptcb->u4Context, TCP_FSM_TRC, "TCP",
                     "FSM state changed to CLOSING for CONN ID - %d.\n",
                     u4Index);
        if (((GetTCBcode (u4Index) & TCPF_FIN) == ZERO) &&
            !(GetTCBflags (u4Index) & TCBF_FINNOTSENT))
        {
            SetTCBstate (u4Index, TCPS_TIMEWAIT);
            TCP_MOD_TRC (ptcb->u4Context, TCP_FSM_TRC, "TCP",
                         "FSM state changed to TIMEWAIT for CONN ID - %d.\n",
                         u4Index);
            FsmStartWaitTmr (ptcb);
        }
    }
    else if (((GetTCBcode (u4Index) & TCPF_FIN) == ZERO) &&
             !(GetTCBflags (u4Index) & TCBF_FINNOTSENT))
    {
        /*We got ACK for the FIN, we have sent */
        SetTCBstate (u4Index, TCPS_FINWAIT2);
        /* Start the FIN_WAIT2 recovery timer */
        TmrStartFinWait2Rectimeout (u4Index);
        TCP_MOD_TRC (ptcb->u4Context, TCP_FSM_TRC, "TCP",
                     "FSM state changed to FINWAIT2 for CONN ID - %d.\n",
                     u4Index);
    }
    /* The FIN we sent, has not been acked */
    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FsmFin1. Returned SUCCESS.\n");
    return (SUCCESS);
}

/*************************************************************************/
/*  Function Name   : FsmFin2                                            */
/*  Input(s)        : ptcb - Pointer to the TCB                          */
/*                    pSeg - pointer to the Segment received             */
/*                    pSegIn - IpTcp Header Structure                    */
/*  Output(s)       : None.                                              */
/*  Returns         : SYSERR/SUCCESS                                     */
/*  Description     : If one side sends a FIN, the other side ACKs it    */
/*                    immediately and delays before sending the second   */
/*                    FIN. The state machine handles the delay with      */
/*                    state fin_wait2.                                   */
/* CALLING CONDITION :                                                   */
/* GLOBAL VARIABLES AFFECTED None                                        */
/*************************************************************************/
INT1
FsmFin2 (tTcb * ptcb, tSegment * pSeg, tIpTcpHdr * pSegIn)
{
    UINT4               u4Index;
    UINT1               u1Code;
    INT1                i1RetVal;

    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC | TCP_FSM_TRC, "TCP",
                 "Entering FsmFin2 for CONN ID - %d\n", GET_TCB_INDEX (ptcb));

    u4Index = GET_TCB_INDEX (ptcb);
    u1Code = pSegIn->tcp->u1Code;

    if (u1Code & TCPF_RST)
    {
        i1RetVal = FsmAbortConn (ptcb, TCPE_RESET);
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmFin2. Received RESET.Returned %d\n",
                     u4Index);
        return i1RetVal;
    }

    if (u1Code & TCPF_SYN)
    {
        LINOsmSendReset (GetTCBContext (u4Index), pSegIn);
        i1RetVal = FsmAbortConn (ptcb, TCPE_RESET);
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmFin2. Received SYN.Returned %d\n", u4Index);
        return i1RetVal;
    }

    if (LINFarHandleAck (ptcb, pSegIn) == SYSERR)
    {
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmFin2. Returned OK.\n");
        return TCP_OK;
    }

    LINFsmProcessData (ptcb, pSeg, pSegIn);

    /*If all the data and FIN arrives then TCBF_RDONE is set */

    u1Code = pSegIn->tcp->u1Code;
    if (u1Code & TCPF_FIN)
    {
        SetTCBstate (u4Index, TCPS_TIMEWAIT);
        TCP_MOD_TRC (ptcb->u4Context, TCP_FSM_TRC, "TCP",
                     "FSM state changed to TIMEWAIT for CONN ID - %d\n",
                     u4Index);
        FsmStartWaitTmr (ptcb);
    }
    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FsmFin2. Returned SUCCESS.\n");
    return (SUCCESS);
}

/*************************************************************************/
/*  Function Name   : FsmTimewait                                        */
/*  Input(s)        : ptcb - Pointer to the TCB                          */
/*                    pSeg - pointer to the Segment received             */
/*                    pSegIn - IpTcp Header Structure                    */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK                                             */
/*  Description     : TCP leaves a connection in the time_wait state     */
/*                    after succesful completion of graceful shutdown.   */
/*  CALLING CONDITION :                                                  */
/*  GLOBAL VARIABLES AFFECTED None                                       */
/*************************************************************************/
INT1
FsmTimewait (tTcb * ptcb, tSegment * pSeg, tIpTcpHdr * pSegIn)
{
    UINT1               u1Code;
    INT1                i1RetVal;

    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC | TCP_FSM_TRC, "TCP",
                 "Entering FsmTimewait for CONN ID - %d\n",
                 GET_TCB_INDEX (ptcb));
    u1Code = pSegIn->tcp->u1Code;

    if (u1Code & TCPF_RST)
    {
        i1RetVal = deallocate (ptcb);
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmTimewait. Received RESET.Returned %d.\n",
                     i1RetVal);
        return i1RetVal;
    }

    if (u1Code & TCPF_SYN)
    {
        LINOsmSendReset (ptcb->u4Context, pSegIn);
        i1RetVal = deallocate (ptcb);
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmTimewait. Received SYN.Returned %d.\n",
                     i1RetVal);
        return i1RetVal;
    }

    if (LINFarHandleAck (ptcb, pSegIn) == SYSERR)
    {
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmTimewait. Returned OK.\n");
        return TCP_OK;
    }

    LINFsmProcessData (ptcb, pSeg, pSegIn);
    FsmStartWaitTmr (ptcb);        /* Wait for 2 MSL */
    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FsmTimewait. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : FsmClosing                                         */
/*  Input(s)        : ptcb - Pointer to the TCB                          */
/*                    pSeg - pointer to the Segment received             */
/*                    pSegIn - IpTcp Header Structure                    */
/*  Output(s)       : None.                                              */
/*  Returns         : SYSERR/SUCCESS                                     */
/*  Description     : TCP reaches closing state after receiving a FIN    */
/*                    in response to FIN.  Thus both sides have agreed   */
/*                    to shutdown, and TCP entered closing state to      */
/*                    await an ACK of its FIN.                           */
/* CALLING CONDITION :                                                   */
/* GLOBAL VARIABLES AFFECTED None                                        */
/*************************************************************************/
INT1
FsmClosing (tTcb * ptcb, tSegment * pSeg, tIpTcpHdr * pSegIn)
{
    UINT4               u4Index;
    UINT1               u1Code;
    INT1                i1RetVal;

    UNUSED_PARAM (pSeg);
    u4Index = GET_TCB_INDEX (ptcb);
    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC | TCP_FSM_TRC, "TCP",
                 "Entering FsmClosing for CONN ID - %d\n", u4Index);

    u1Code = pSegIn->tcp->u1Code;

    if (u1Code & TCPF_RST)
    {
        i1RetVal = deallocate (ptcb);
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmClosing. Received RESET.Returned %d.\n",
                     i1RetVal);
        return i1RetVal;
    }

    if (u1Code & TCPF_SYN)
    {
        LINOsmSendReset (GetTCBContext (u4Index), pSegIn);
        i1RetVal = deallocate (ptcb);
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmClosing. Received SYN.Returned %d.\n",
                     i1RetVal);
        return i1RetVal;
    }

    if (LINFarHandleAck (ptcb, pSegIn) == SYSERR)
    {
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmClosing. Returned OK.\n");
        return TCP_OK;
    }
    if (SEQCMP (GetTCBsnext (u4Index), GetTCBsuna (u4Index)) != ZERO)
    {
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmClosing. Returned OK.\n");
        return TCP_OK;
    }
    if ((GetTCBcode (u4Index) & TCPF_FIN) == ZERO)
    {
        SetTCBstate (u4Index, TCPS_TIMEWAIT);
        TCP_MOD_TRC (ptcb->u4Context, TCP_FSM_TRC, "TCP",
                     "FSM state changed to TIMEWAIT for CONN ID - %d\n",
                     u4Index);
        FsmStartWaitTmr (ptcb);
    }
    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FsmClosing. Returned SUCCESS.\n");
    return (SUCCESS);
}

/*************************************************************************/
/*  Function Name   : FsmClosewait                                       */
/*  Input(s)        : ptcb - Pointer to the TCB                          */
/*                    pSeg - pointer to the Segment received             */
/*                    pSegIn - IpTcp Header Structure                    */
/*  Output(s)       : None.                                              */
/*  Returns         : SYSERR/SUCCESS                                     */
/*  Description     : When a FIN arrives before the application issues   */
/*                    a close, TCP enters the close_wait state. It waits */
/*                    for the application issue a close operation before */
/*                    moving to last_ack state.                          */
/* CALLING CONDITION                                                     */
/* GLOBAL VARIABLES AFFECTED none                                        */
/*************************************************************************/
INT1
FsmClosewait (tTcb * ptcb, tSegment * pSeg, tIpTcpHdr * pSegIn)
{
    UINT4               u4Context;
    UINT1               u1Code;
    INT1                i1RetVal;

    UNUSED_PARAM (pSeg);

    u4Context = ptcb->u4Context;
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_FSM_TRC, "TCP",
                 "Entering FsmClosewait for CONN ID - %d\n",
                 GET_TCB_INDEX (ptcb));

    u1Code = pSegIn->tcp->u1Code;

    if (u1Code & TCPF_RST)
    {
        IncTCPstat (u4Context, EstabResets);
        i1RetVal = FsmAbortConn (ptcb, TCPE_RESET);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmClosewait. Received RESET.Returned %d\n",
                     i1RetVal);
        return i1RetVal;
    }

    if (u1Code & TCPF_SYN)
    {
        IncTCPstat (u4Context, EstabResets);
        LINOsmSendReset (u4Context, pSegIn);
        i1RetVal = FsmAbortConn (ptcb, TCPE_RESET);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmClosewait. Received SYN.Returned %d\n",
                     i1RetVal);
        return i1RetVal;
    }

    if (LINFarHandleAck (ptcb, pSegIn) == SYSERR)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmClosewait. Returned OK.\n");
        return TCP_OK;
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FsmClosewait. Returned SUCCESS.\n");
    return SUCCESS;
}

/*************************************************************************/
/*  Function Name   : FsmLastack                                         */
/*  Input(s)        : ptcb - Pointer to the TCB                          */
/*                    pSeg - pointer to the Segment received             */
/*                    pSegIn - IpTcp Header Structure                    */
/*  Output(s)       : None.                                              */
/*  Returns         : SYSERR/SUCCESS                                     */
/*  Description     : The transition from close_wait to last_ack state   */
/*                    occurs when an application issues a close operation*/
/*                    During the transition, TCP schedules a FIN to be   */
/*                    sent and last_ack state to await ack.              */
/* CALLING CONDITION                                                     */
/* GLOBAL VARIABLES AFFECTED None                                        */
/*************************************************************************/
INT1
FsmLastack (tTcb * ptcb, tSegment * pSeg, tIpTcpHdr * pSegIn)
{
    UINT4               TcbIndex;
    UINT1               u1Code;
    INT1                i1RetVal;

    UNUSED_PARAM (pSeg);

    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC | TCP_FSM_TRC, "TCP",
                 "Entering FsmLastack for CONN ID - %d\n",
                 GET_TCB_INDEX (ptcb));
    TcbIndex = GET_TCB_INDEX (ptcb);
    u1Code = pSegIn->tcp->u1Code;

    if (u1Code & TCPF_RST)
    {
        i1RetVal = FsmAbortConn (ptcb, TCPE_RESET);
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmLastack. Received RESET.Returned %d\n",
                     i1RetVal);
        return i1RetVal;
    }

    if (u1Code & TCPF_SYN)
    {
        LINOsmSendReset (GetTCBContext (TcbIndex), pSegIn);
        i1RetVal = FsmAbortConn (ptcb, TCPE_RESET);
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmLastack.Received SYN.Returned %d\n",
                     i1RetVal);
        return i1RetVal;
    }

    if (LINFarHandleAck (ptcb, pSegIn) == SYSERR)
    {
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmLastack. Returned OK.\n");
        return TCP_OK;
    }
    if (SEQCMP (GetTCBsnext (TcbIndex), GetTCBsuna (TcbIndex)) != ZERO)
    {
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmLastack. Returned OK.\n");
        return TCP_OK;
    }

    SetTCBstate (TcbIndex, TCPS_CLOSED);
    TCP_MOD_TRC (ptcb->u4Context, TCP_FSM_TRC, "TCP",
                 "FSM state changed to CLOSED for CONN ID - %d\n", TcbIndex);

    deallocate (ptcb);

    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FsmLastack. Returned SUCCESS.\n");
    return SUCCESS;
}

/*************************************************************************/
/*  Function Name   : FsmAbort                                           */
/*  Input(s)        : ptcb - Pointer to the TCB                          */
/*                    pSeg - pointer to the Segment received             */
/*                    pSegIn - IpTcp Header Structure                    */
/*  Output(s)       : None                                               */
/*  Returns         : SYSERR/SUCCESS                                     */
/*  Description     : Since the connection is placed in this state after */
/*                    it has been aborted due to some reason, any        */
/*                    packets received to this indicates that the peer   */
/*                    is still alive. Hence a RST is sent to the peer.   */
/* CALLING CONDITION :                                                   */
/* GLOBAL VARIABLES AFFECTED None                                        */
/*************************************************************************/
INT1
FsmAbort (tTcb * ptcb, tSegment * pSeg, tIpTcpHdr * pSegIn)
{
    UNUSED_PARAM (pSeg);
    UNUSED_PARAM (ptcb);

    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC | TCP_FSM_TRC, "TCP",
                 "Entering FsmAbort for CONN ID - %d\n", GET_TCB_INDEX (ptcb));

    if ((pSegIn->tcp->u1Code & TCPF_RST) == ZERO)
    {
        LINOsmSendReset (ptcb->u4Context, pSegIn);
    }
    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FsmAbort. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : FsmClosed1                                         */
/*  Input(s)        : u4Context - Context in which close should be       */
/*                                 handled                               */
/*                    pSegIn - IpTcp Header Structure                    */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK                                             */
/*  Description     : This routine sends tcp reset when an appropriate   */
/*                    connection handler is not found for the given ip   */
/*                    and port.                                          */
/*************************************************************************/
INT1
FsmClosed1 (UINT4 u4Context, tIpTcpHdr * pSegIn)
{
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_FSM_TRC, "TCP",
                 "Entering FsmClosed1\n");
    if (pSegIn->tcp->u1Code & TCPF_RST)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from FsmClosed1. Received RESET. Returned OK.\n");
        return TCP_OK;
    }
    LINOsmSendReset (u4Context, pSegIn);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from FsmClosed1. Sent RST and returned OK.\n");
    return TCP_OK;
}
