/** $Id: fstcpwr.h,v 1.6 2013/06/07 13:32:13 siva Exp $ */

#ifndef _FSTCPWRAP_H 
#define _FSTCPWRAP_H 
VOID RegisterFSTCP(VOID);
INT4 FsTcpAckOptionTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTcpAckOptionSet (tSnmpIndex *, tRetVal *);
INT4 FsTcpAckOptionGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpTimeStampOptionTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTcpTimeStampOptionSet (tSnmpIndex *, tRetVal *);
INT4 FsTcpTimeStampOptionGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpBigWndOptionTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTcpBigWndOptionSet (tSnmpIndex *, tRetVal *);
INT4 FsTcpBigWndOptionGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpIncrIniWndTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTcpIncrIniWndSet (tSnmpIndex *, tRetVal *);
INT4 FsTcpIncrIniWndGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpMaxNumOfTCBTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTcpMaxNumOfTCBSet (tSnmpIndex *, tRetVal *);
INT4 FsTcpMaxNumOfTCBGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpTraceDebugTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTcpTraceDebugSet (tSnmpIndex *, tRetVal *);
INT4 FsTcpTraceDebugGet (tSnmpIndex *, tRetVal *);

INT4 FsTcpAckOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsTcpTimeStampOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsTcpBigWndOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsTcpIncrIniWndDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsTcpMaxNumOfTCBDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsTcpTraceDebugDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);


INT4 FsTcpConnLocalAddressGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpConnLocalPortGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpConnRemAddressGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpConnRemPortGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpConnOutStateGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpConnSWindowGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpConnRWindowGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpConnCWindowGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpConnSSThreshGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpConnSMSSGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpConnRMSSGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpConnSRTGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpConnRTDEGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpConnPersistGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpConnRexmtGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpConnRexmtCntGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpConnSBCountGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpConnSBSizeGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpConnRBCountGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpConnRBSizeGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpKaMainTmrTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTcpKaMainTmrSet (tSnmpIndex *, tRetVal *);
INT4 FsTcpKaMainTmrGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpKaRetransTmrTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTcpKaRetransTmrSet (tSnmpIndex *, tRetVal *);
INT4 FsTcpKaRetransTmrGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpKaRetransCntTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTcpKaRetransCntSet (tSnmpIndex *, tRetVal *);
INT4 FsTcpKaRetransCntGet (tSnmpIndex *, tRetVal *);
INT4 FsTcpMaxReTriesGet(tSnmpIndex *, tRetVal *);
INT4 FsTcpMaxReTriesSet(tSnmpIndex *, tRetVal *);
INT4 FsTcpMaxReTriesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTcpMaxReTriesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 FsTcpTrapAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsTcpTrapAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsTcpTrapAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTcpTrapAdminStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
 /*  GetNext Function Prototypes */

INT4 GetNextIndexFsTcpConnTable( tSnmpIndex *, tSnmpIndex *);

INT4 FsTcpConnTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

 /* FsTcpExtConnTable Prototypes */
INT4 GetNextIndexFsTcpExtConnTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsTcpConnMD5OptionGet(tSnmpIndex *, tRetVal *);
INT4 FsTcpConnMD5ErrCtrGet(tSnmpIndex *, tRetVal *);
INT4 FsTcpConnTcpAOOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsTcpConTcpAOCurKeyIdGet(tSnmpIndex *, tRetVal *);
INT4 FsTcpConTcpAORnextKeyIdGet(tSnmpIndex *, tRetVal *);
INT4 FsTcpConTcpAORcvKeyIdGet(tSnmpIndex *, tRetVal *);
INT4 FsTcpConTcpAORcvRnextKeyIdGet(tSnmpIndex *, tRetVal *);
INT4 FsTcpConTcpAOConnErrCtrGet(tSnmpIndex *, tRetVal *);
INT4 FsTcpConTcpAOSndSneGet(tSnmpIndex *, tRetVal *);
INT4 FsTcpConTcpAORcvSneGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsTcpAoConnTestTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsTcpConTcpAOIcmpIgnCtrGet(tSnmpIndex *, tRetVal *);
INT4 FsTcpConTcpAOSilentAccptCtrGet(tSnmpIndex *, tRetVal *);
#endif
