/** $Id: tcptcb.h,v 1.19 2016/05/06 10:10:58 siva Exp $ */

/* FILE HEADER
 *
 * ----------------------------------------------------------------------------
 *  FILE NAME             : tcptcb.h
 * 
 *  PRINCIPAL AUTHOR      : Ramesh Kumar
 *
 *  SUBSYSTEM NAME        : TCP
 *
 *  MODULE NAME           : High Level Interface Module
 *
 *  LANGUAGE              : C
 *
 *  TARGET ENVIRONMENT    : Any
 *
 *  DATE OF FIRST RELEASE :
 * 
 *  DESCRIPTION           : This file contains TCB (Transmission Control Block)
 *                          structure and macros to access them.
 *
 * ---------------------------------------------------------------------------
 *
 *
 *  CHANGE RECORD :
 *
 * ---------------------------------------------------------------------------
 *   VERSION        AUTHOR/DATE           DESCRIPTION OF CHANGE
 * ---------------------------------------------------------------------------
 *                  Vinay/28.11.99       +Added flags to indicate if the options
 *                                        are enabled under the compilation 
 *                                        switch ST_ENH. Also added 
 *                                        TCBF_GENNAK to indicate if NAK has to
 *                                        be generated for a particular 
 *                                        connection.
 *                                       +Modified the following in tTcb 
 *                                        structure:
 *                                        * Modified u2_tcb_swindow to 
 *                                          u4TcbSwindow.
 *                                        * Added option related parameters 
 *                                          under the compilation switch 
 *                                          ST_ENH.
 *                                       +Added Get/Set macros for additional 
 *                                        parameters in the tTcb structure
 *                                       +Added global declaration for 
 *                                        tTcpConfigParms under the compilation 
 *                                        switch ST_ENH
 *                                       +Removed macro GetTCBOptLen. It has now *                                        been replaced by a routine
 *                                        osmGetTCBOptLen().
 * ---------------------------------------------------------------------------
 */
#ifndef  __TCP_TCB_H__
#define  __TCP_TCB_H__

/* IPv6 Related macros */
#define  IP6_HEADER_LEN       44                         
#define  IPv6_HEADER_LEN      40                         
#define  IP6                  0x06                       
#define  IP4                  0x04                       
#define  IPv6_VERSION_OFFSET  0                          
#define  IsIP6Addr(x)         !(IN6_IS_ADDR_V4MAPPED(x)) 

/* tcb_flags */
#define  TCBF_NEEDOUT     0x01       /* need output                      */
#define  TCBF_FIRSTSEND   0x02       /* no data to ACK                   */
#define  TCBF_GOTFIN      0x04       /* no more to receive               */
#define  TCBF_RDONE       0x08       /* no more receive data to process  */
#define  TCBF_SDONE       0x10       /* no more send data allowed        */
#define  TCB_DELACK       0x20       /* Delayed Ack                      */
#define  TCBF_BUFFER      0x40       /* do TCP buffering                 */
#define  TCBF_PUSH        0x80       /* got a push                       */
#define  TCBF_SNDFIN      0x100      /* user process closed, send finish */
#define  TCBF_RUPOK       0x200      /* receive urgent pointer valid     */
#define  TCBF_SUPOK       0x400      /* send urgent pointer valid        */
#define  TCBF_NORMALDATA  0x800      /* It is normal case                */
#define  TCBF_SPUSH       0x1000     /* Push Data is to be transmitted   */

#define  TCBF_GENNAK      0x2000     /* NAK has to be generated */
#define  TCBF_REXMTFIN    0x4000     /* It is useful in re-transmitting 
                                        the message with FIN flag */
#define  TCBF_FINNOTSENT  0x8000     /* It is useful in sending 
                                        the message with FIN flag */

/* option flags */
#define  TCBOPF_TS    0x01     /* flag for indicating if TS is enabled   */
#define  TCBOPF_BW    0x02     /* flag for indicating if BW is enabled   */
#define  TCBOPF_SACK  0x04     /* flag for indicating if SACK is enabled */
#define  TCBOPF_WS    0x08     /* flag for indicating if Window Scaling
                                * is enabled */

#define  MAX_OPT_LEN  40       /* Maximum Option Length */
 
#define  TCP_KA_OFF   0        /* Keepalive is not being done now */
#define  TCP_KA_ON    1        /* Keepalive is on. TCP sends KA packet */

#define  MAX_SEND_BUF_SIZE     TCP_MAX_RECV_BUF_SIZE
#define  TCP_MAX_SEGMENT_SIZE  1400
#define  TCP_DEFAULT_RMSS      536
#define  MAX_NUM_MESSAGES      65535    /* Max number of outstanding messages */

#define  GET_TCB_INDEX(ptcb)  ( ptcb - TCBtable)            
#define  GetTCBmsgcnt(x)      ( TCBtable[x].u2MaxMsgCnt)    

#define  GetTCBstate(x)       ( TCBtable[x].u1TcbState )    
#define  GetTCBostate(x)      ( TCBtable[x].u1TcbOutState ) 
#define  GetTCBtype(x)        ( TCBtable[x].u1TcbType )     
#define  GetTCBcode(x)        ( TCBtable[x].u1TcbCode )     
#define  GetTCBflags(x)       ( TCBtable[x].u2TcbFlags )    

#define  GetTCBOptFlag(x)           ( TCBtable[x].u2TcbOptionsFlag )    
#define  GetTCBTSLastAckSent(x)     ( TCBtable[x].i4TcbTSLastAckSent )  
#define  GetTCBTSRecent(x)          ( TCBtable[x].u4TcbTSRecent )       
#define  GetTCBOutOrderSeq(x)       ( TCBtable[x].i4TcbOutOrderSeq )    
#define  GetTCBSackLargestSeq(x)    ( TCBtable[x].i4TcbSackLargestSeq ) 
#define  GetTCBRxmtQHdr(x)          ( TCBtable[x].pTcbRxmtQHdr )        
#define  GetTCBSackHdr(x)           ( TCBtable[x].pTcbSackHdr )         
#define  GetTCBKaMainTmOut(x)       ( TCBtable[x].u4KaMainTmOut )       
#define  GetTCBKaRetransTmOut(x)    ( TCBtable[x].u2KaRetransTmOut )    
#define  GetTCBSendWindowScale(x)   ( TCBtable[x].u2SendWindowScale )    
#define  GetTCBRecvWindowScale(x)   ( TCBtable[x].u2RecvWindowScale )    
#define  GetTCBKaRetransCnt(x)      ( TCBtable[x].u1KaRetransCnt )      
#define  GetTCBKaRetransOverCnt(x)  ( TCBtable[x].u1KaRetransOverCnt )  
#define  GetTCBIfIndex(x)           ( TCBtable[x].u4IfIndex )           
#define  GetTCBLingerState(x)  ((TCBtable[x].TcbLingerParms).u2LingerOnOff) 
#define  GetTCBLingerTmOut(x)  ((TCBtable[x].TcbLingerParms).u2LingerTimer) 

#define  GetTCBlip(x)  ( TCBtable[x].TcbLocalIp )  
#define  GetTCBrip(x)  ( TCBtable[x].TcbRemoteIp ) 
#define  GetTCBTtl(x)  ( TCBtable[x].u1Ttl)        

#define  GetTCBv4lip(x)  ( V4_FROM_V6_ADDR(TCBtable[x].TcbLocalIp) )  
#define  GetTCBv4rip(x)  ( V4_FROM_V6_ADDR(TCBtable[x].TcbRemoteIp) ) 

#define  GetTCBLipAddType(x) ( TCBtable[x].i4LocalAddrType)
#define  GetTCBRipAddType(x) ( TCBtable[x].i4RemoteAddrType)
#define  GetTCBPID(x)        ( TCBtable[x].u4ProcessID )

#define  GetTCBlport(x)          ( TCBtable[x].u2TcbLocalPort )    
#define  GetTCBrport(x)          ( TCBtable[x].u2TcbRemotePort )   
#define  GetTCBerror(x)          ( TCBtable[x].i1TcbError )        
#define  GetTCBsuna(x)           ( TCBtable[x].i4TcbSuna )         
#define  GetTCBsnext(x)          ( TCBtable[x].i4TcbSnext )        
#define  GetTCBfinsnext(x)       ( TCBtable[x].i4TcbFinSnext )
#define  GetTCBslast(x)          ( TCBtable[x].i4TcbSlast )        
#define  GetTCBswindow(x)        ( TCBtable[x].u4TcbSwindow )      

#define  GetTCBMaxswnd(x)        ( TCBtable[x].u4MaxSwnd )      

#define  GetTCBlwseq(x)          ( TCBtable[x].i4TcbLwseq )        
#define  GetTCBlwack(x)          ( TCBtable[x].i4TcbLwack )        
#define  GetTCBcwnd(x)           ( TCBtable[x].u4TcbCwnd )         
#define  GetTCBssthresh(x)       ( TCBtable[x].u4TcbSsthresh )     
#define  GetTCBsmss(x)           ( TCBtable[x].u2TcbSmss )         
#define  GetTCBiss(x)            ( TCBtable[x].i4TcbIss )          
#define  GetTCBsrt(x)            ( TCBtable[x].u4TcbSrt )          
#define  GetTCBrtde(x)           ( TCBtable[x].u4TcbRtde )         
#define  GetTCBpersist(x)        ( TCBtable[x].u4TcbPersist )      
#define  GetTCBrexmt(x)          ( TCBtable[x].u4TcbRexmt )        
#define  GetTCBrexmtcount(x)     ( TCBtable[x].u1TcbRexmtcount )   
#define  GetTCBrnext(x)          ( TCBtable[x].i4TcbRnext )        

#define  GetTCBrupseq(x)         ( TCBtable[x].i4TcbRupseq )       
#define  GetTCBpptcb(x)          ( TCBtable[x].pTcbPptcb )       
#define  GetTCBsupseq(x)         ( TCBtable[x].i4TcbSupseq )       
#define  GetTCBsndbuf(x)         ( TCBtable[x].pTcbSndbuf )        
#define  GetTCBsbstart(x)        ( TCBtable[x].u4TcbSbstart )      
#define  GetTCBsbcount(x)        ( TCBtable[x].u4TcbSbcount )      
#define  GetTCBsbsize(x)         ( TCBtable[x].u4TcbSbsize )       
#define  GetTCBrcvbuf(x)         ( TCBtable[x].pTcbRcvbuf )        
#define  GetTCBrbstart(x)        ( TCBtable[x].u4TcbRbstart )      
#define  GetTCBrbcount(x)        ( TCBtable[x].u4TcbRbcount )      
#define  GetTCBrbsize(x)         ( TCBtable[x].u4TcbRbsize )       
#define  GetTCBrmss(x)           ( TCBtable[x].u2TcbRmss )         
#define  GetTCBcwin(x)           ( TCBtable[x].i4TcbCwin )         
#define  GetTCBfraghdr(x)        ( TCBtable[x].pTcbFraghdr )       
#define  GetTCBfinseq(x)         ( TCBtable[x].i4TcbFinseq )       
#define  GetTCBpushseq(x)        ( TCBtable[x].i4TcbPushseq )      

#define  GetTCBvalopts(x)        ( TCBtable[x].u4TcbValidOptions ) 
#define  GetTCBtos(x)            ( TCBtable[x].u1TcbTos )          
#define  GetTCBsecurity(x)       ( TCBtable[x].u2TcbSecurity )     
#define  GetTCBtimeout(x)        ( TCBtable[x].u4TcbTimeout )      
#define  GetTCBpipopt(x)         ( TCBtable[x].pTcbIpOptions )     
#define  GetTCBipoptlen(x)       ( TCBtable[x].u1TcbIpOptlen )     
#define  GetTCBpmd5key(x)        ( TCBtable[x].au1TcbMd5Key )
#define  GetTCBmd5keylen(x)      ( TCBtable[x].u1TcbMd5Keylen )
#define  GetTCBmd5errs(x)        ( TCBtable[x].u4InMd5Errs )
#define  GetTCBhlicmd(x)         ( TCBtable[x].u1TcbHlicmd )       

#define  GetTCBTcpToAppParms(x)  ( TCBtable[x].pParms )            
#define  GetTCBContext(x)        (TCBtable[x].u4Context)

/* TCP-AO Macros for Get */
#define GetRcvIsn(x)                  ( TCBtable[x].u4RcvIsn )
#define GetTcpAoMacVerErrCtr(x)       ( TCBtable[x].u4TcpAoMacVerErrs )
#define GetTcpAoSendSne(x)            ( TCBtable[x].u4TcpAoSneSnd )
#define GetTcpAoRecvSne(x)            ( TCBtable[x].u4TcpAoSneRcv )
#define GetIcmpIgnCtr(x)              ( TCBtable[x].u4IcmpIgnoreCtr )
#define GetSilentAcceptCtr(x)         ( TCBtable[x].u4SilentAcceptCtr )
#define GetTCBTcpAoEnabled(x)         ( TCBtable[x].u1TcpAoEnabled )
#define GetTCBTcpAoCurrKey(x)         ( TCBtable[x].u1TcpAoCurrentKey )
#define GetTCBTcpAoRNextKey(x)        ( TCBtable[x].u1TcpAoRNextKey )
#define GetTCBTcpAoNoMktDisc(x)       ( TCBtable[x].u1TcpAoNoMktDisc )
#define GetTCBTcpAoIcmpAcc(x)         ( TCBtable[x].u1TcpAoIcmpAcc )
#define GetTCBTcpAoRcvPckKeyId(x)     ( TCBtable[x].u1RcvPackKeyId )
#define GetTCBTcpAoRcvPckRNxtKeyId(x) ( TCBtable[x].u1RcvPackRNextKeyId )
#define GetTCBTcpAoActiveInd(x)       ( TCBtable[x].u1ActiveInd )
#define GetTCBTcpAoNewInd(x)          ( TCBtable[x].u1NewInd )
#define GetTCBTcpAoBackupInd(x)       ( TCBtable[x].u1BackUpInd )
#define GetMktByIndx(x, y)            ( TCBtable[x].TcpAoAuthMktTCBList[y])

/*
 * x - Tcb Index , y - send key id
 */
#define GetMktIndexFrmSendId(x, y) (TcpAoGetMktIndexFrmSendId(x, y))
/*
 * x - Tcb Index , y - receive key id
 */
#define GetMktIndexFromRcvId(x, y) (TcpAoGetMktIndexFromRcvId(x, y))

/* Get for MKT */
/* x - Tcb Index y - Mkt Index  */
#define GetMktSendId(x, y) (GetMktByIndx(x, y).u1SendKeyId)
#define GetMktRcvId(x, y)  (GetMktByIndx(x, y).u1ReceiveKeyId)
#define GetMktKdfAlgo(x, y) (GetMktByIndx(x, y).u1KDFAlgo)
#define GetMktMacAlgo(x, y) (GetMktByIndx(x, y).u1MACAlgo)
#define GetMktPasswLen(x, y) (GetMktByIndx(x, y).u1TcpAOPasswdLength)
#define GetMktTcpOptIgn(x, y) (GetMktByIndx(x, y).u1TcpOptionIgnore)

/* x - Tcb Index y - Mkt Index  z - pointer to store key */
#define GetMktMstKey(x, y, z)                                                  \
MEMCPY (z, (GetMktByIndx(x, y).au1TcpAOMasterKey), (GetMktByIndx(x, y).u1TcpAOPasswdLength));
#define GetMktSynKey(x, y, z)                                                  \
MEMCPY (z, (GetMktByIndx(x, y).au1TcpTrafficKeySyn), TCPAO_TRAFFIC_KEY_MAXLEN);
#define GetMktSndTrfcKey(x, y, z)                                              \
MEMCPY (z, (GetMktByIndx(x, y).au1TrafficKeySend), TCPAO_TRAFFIC_KEY_MAXLEN);
#define GetMktRcvTrfcKey(x, y, z)                                              \
MEMCPY (z, (GetMktByIndx(x, y).au1TrafficKeyReceive), TCPAO_TRAFFIC_KEY_MAXLEN);
#define  SetTCBasynchndlr(x,y)   ( TCBtable[x].fpTcpHandleAsyncMesg = (y))  
#define  SetTCBmsgcnt(x,y)       ( TCBtable[x].u2MaxMsgCnt = (y))            
#define  SetTCBAppTaskName(x,y)  ( STRNCPY(TCBtable[x].au1AppTaskName, y,4)) 
#define  SetTCBAppQName(x,y)     ( STRNCPY(TCBtable[x].au1AppQName, y,4))    
#define  SetTCBstate(x,y)        ( TCBtable[x].u1TcbState = (y) )            
#define  SetTCBostate(x,y)       ( TCBtable[x].u1TcbOutState = (y) )         
#define  SetTCBtype(x,y)         ( TCBtable[x].u1TcbType = (y) )             
#define  SetTCBcode(x,y)         ( TCBtable[x].u1TcbCode = (y) )             
#define  SetTCBflags(x,y)        ( TCBtable[x].u2TcbFlags |= (y) )           
#define  SetTCBflags_AND(x,y)    ( TCBtable[x].u2TcbFlags &= (y) )           

#define  SetTCBOptFlag(x,y)           ( TCBtable[x].u2TcbOptionsFlag |= (y) )   
#define  SetTCBTSLastAckSent(x,y)     ( TCBtable[x].i4TcbTSLastAckSent = (y) )  
#define  SetTCBTSRecent(x,y)          ( TCBtable[x].u4TcbTSRecent = (y) )       
#define  SetTCBOutOrderSeq(x,y)       ( TCBtable[x].i4TcbOutOrderSeq = (y) )    
#define  SetTCBSackLargestSeq(x,y)    ( TCBtable[x].i4TcbSackLargestSeq = (y) ) 
#define  SetTCBRxmtQHdr(x,y)          ( TCBtable[x].pTcbRxmtQHdr = (y) )        
#define  SetTCBSackHdr(x,y)           ( TCBtable[x].pTcbSackHdr = (y) )         
#define  SetTCBKaMainTmOut(x,y)       ( TCBtable[x].u4KaMainTmOut = (y) )       
#define  SetTCBKaRetransTmOut(x,y)    ( TCBtable[x].u2KaRetransTmOut=(UINT2)(y))    
#define  SetTCBSendWindowScale(x,y)   ( TCBtable[x].u2SendWindowScale = (y) )    
#define  SetTCBRecvWindowScale(x,y)   ( TCBtable[x].u2RecvWindowScale = (y) )    
#define  SetTCBKaRetransCnt(x,y)      ( TCBtable[x].u1KaRetransCnt = (y) )      
#define  SetTCBKaRetransOverCnt(x,y)  ( TCBtable[x].u1KaRetransOverCnt = (y) )  
#define  SetTCBIfIndex(x,y)           ( TCBtable[x].u4IfIndex = (y) )           
#define  SetTCBLingerState(x,y) ((TCBtable[x].TcbLingerParms).u2LingerOnOff=(y))
#define  SetTCBLingerTmOut(x,y) ((TCBtable[x].TcbLingerParms).u2LingerTimer=(y))

#define  SetTCBlip(x,y)            ( TCBtable[x].TcbLocalIp = (y) )        
#define  SetTCBrip(x,y)            ( TCBtable[x].TcbRemoteIp = (y) )       
#define  SetTCBTtl(x,y)            ( TCBtable[x].u1Ttl = (y) )             

#define  SetTCBlport(x,y)          ( TCBtable[x].u2TcbLocalPort = (y) )    
#define  SetTCBrport(x,y)          ( TCBtable[x].u2TcbRemotePort = (y) )   
#define  SetTCBerror(x,y)          ( TCBtable[x].i1TcbError = (y) )        
#define  SetTCBpni(x,y)            ( TCBtable[x].i1TcbError = (y) )        
#define  SetTCBsuna(x,y)           ( TCBtable[x].i4TcbSuna = (y) )         
#define  SetTCBsnext(x,y)          ( TCBtable[x].i4TcbSnext = (y) )        
#define  SetTCBfinsnext(x,y)       ( TCBtable[x].i4TcbFinSnext = (y) )
#define  SetTCBslast(x,y)          ( TCBtable[x].i4TcbSlast = (y) )        
#define  SetTCBswindow(x,y)        ( TCBtable[x].u4TcbSwindow = (y) )      

#define  SetTCBMaxswnd(x,y)        ( TCBtable[x].u4MaxSwnd = (y) )      

#define  SetTCBlwseq(x,y)          ( TCBtable[x].i4TcbLwseq = (y) )        
#define  SetTCBlwack(x,y)          ( TCBtable[x].i4TcbLwack = (y) )        
#define  SetTCBcwnd(x,y)           ( TCBtable[x].u4TcbCwnd = (y) )         
#define  SetTCBssthresh(x,y)       ( TCBtable[x].u4TcbSsthresh = (y) )     
#define  SetTCBsmss(x,y)           ( TCBtable[x].u2TcbSmss = (y) )         
#define  SetTCBiss(x,y)            ( TCBtable[x].i4TcbIss = (y) )          
#define  SetTCBsrt(x,y)            ( TCBtable[x].u4TcbSrt = (y) )          
#define  SetTCBrtde(x,y)           ( TCBtable[x].u4TcbRtde = (y) )         
#define  SetTCBpersist(x,y)        ( TCBtable[x].u4TcbPersist = (y) )      
#define  SetTCBrexmt(x,y)          ( TCBtable[x].u4TcbRexmt = (y) )        
#define  SetTCBrexmtcount(x,y)     ( TCBtable[x].u1TcbRexmtcount = (y) )   
#define  SetTCBrnext(x,y)          ( TCBtable[x].i4TcbRnext = (y) )        

#define  SetTCBrupseq(x,y)         ( TCBtable[x].i4TcbRupseq = (y) )       
#define  SetTCBpptcb(x,y)          ( TCBtable[x].pTcbPptcb = (y) )       
#define  SetTCBsndbuf(x,y)         ( TCBtable[x].pTcbSndbuf = (y) )        
#define  SetTCBsupseq(x,y)         ( TCBtable[x].i4TcbSupseq = (y) )       
#define  SetTCBsbstart(x,y)        ( TCBtable[x].u4TcbSbstart = (y) )      
#define  SetTCBsbcount(x,y)        ( TCBtable[x].u4TcbSbcount = (y) )      
#define  SetTCBsbsize(x,y)         ( TCBtable[x].u4TcbSbsize = (y) )       
#define  SetTCBrcvbuf(x,y)         ( TCBtable[x].pTcbRcvbuf = (y) )        
#define  SetTCBrbstart(x,y)        ( TCBtable[x].u4TcbRbstart = (y) )      
#define  SetTCBrbcount(x,y)        ( TCBtable[x].u4TcbRbcount = (y) )      
#define  SetTCBrbsize(x,y)         ( TCBtable[x].u4TcbRbsize = (y) )       
#define  SetTCBrmss(x,y)           ( TCBtable[x].u2TcbRmss = (y) )         
#define  SetTCBcwin(x,y)           ( TCBtable[x].i4TcbCwin = (y) )         
#define  SetTCBfraghdr(x,y)        ( TCBtable[x].pTcbFraghdr = (y) )       
#define  SetTCBfinseq(x,y)         ( TCBtable[x].i4TcbFinseq = (y) )       
#define  SetTCBpushseq(x,y)        ( TCBtable[x].i4TcbPushseq = (y) )      

#define  SetTCBvalopts(x,y)        ( TCBtable[x].u4TcbValidOptions = (y) ) 
#define  SetTCBsecurity(x,y)       ( TCBtable[x].u2TcbSecurity = (y) )     
#define  SetTCBtos(x,y)            ( TCBtable[x].u1TcbTos = (y) )          
#define  SetTCBtimeout(x,y)        ( TCBtable[x].u4TcbTimeout = (y) )      
#define  SetTCBipoptlen(x,y)       ( TCBtable[x].u1TcbIpOptlen = (y) )     
#define  SetTCBmd5keylen(x,y)      ( TCBtable[x].u1TcbMd5Keylen = (y) )
#define  IncrTCBmd5errs(x)         ( TCBtable[x].u4InMd5Errs++ )
#define  SetTCBhlicmd(x,y)         ( TCBtable[x].u1TcbHlicmd = (y) )       
#define  SetTCBTcpToAppParms(x,y)  ( TCBtable[x].pParms = y )              
#define  SetTCBContext(x,y)        (TCBtable[x].u4Context = y)

#define  TakeTCBmainsem(x) \
    (TCBtable[x].MainSemId == ZERO) \
    ? OSIX_FAILURE : ( (OsixSemTake (TCBtable[x].MainSemId) == OSIX_SUCCESS) ? \
    (( TCBtable[x].MainSemId == ZERO )? OSIX_FAILURE : OSIX_SUCCESS) : OSIX_FAILURE) 

#define  GiveTCBmainsem(x) \
    (TCBtable[x].MainSemId == ZERO) \
    ? OSIX_FAILURE : OsixSemGive (TCBtable[x].MainSemId)
#define  TakeTCBBlockSem(x) \
    (TCBtable[x].BlockSemId == ZERO) \
    ? OSIX_FAILURE : OsixSemTake (TCBtable[x].BlockSemId)
#define  GiveTCBBlockSem(x) \
    (TCBtable[x].BlockSemId == ZERO) \
    ? OSIX_FAILURE : OsixSemGive (TCBtable[x].BlockSemId)
#define  GetTCBReadWaiting(x)    (TCBtable[x].u1ReadWaiting)
#define  SetTCBReadWaiting(x,y)  (TCBtable[x].u1ReadWaiting = y )
#define  GetTCBSockDesc(x)       ( TCBtable[x].i4SockDesc )         
#define  SEQCMP(a, b)            ((a) - (b))                        

#define  SetTCBLipAddType(x,y) ( TCBtable[x].i4LocalAddrType= (y) )
#define  SetTCBRipAddType(x,y) ( TCBtable[x].i4RemoteAddrType= (y) )
#define  SetTCBPID(x,y)        ( TCBtable[x].u4ProcessID = (y) )

/* TCP-AO Macros for Set & Increment */

#define SetRcvIsn(x, y)                  ( TCBtable[x].u4RcvIsn = (y) )
#define SetTcpAoMacVerErrCtr(x, y)       ( TCBtable[x].u4TcpAoMacVerErrs = (y) )
#define SetTcpAoSendSne(x, y)            ( TCBtable[x].u4TcpAoSneSnd = (y) )
#define SetTcpAoRecvSne(x, y)            ( TCBtable[x].u4TcpAoSneRcv = (y) )
#define SetIcmpIgnCtr(x, y)              ( TCBtable[x].u4IcmpIgnoreCtr = (y))
#define SetSilentAcceptCtr(x, y)         ( TCBtable[x].u4SilentAcceptCtr = (y) )
#define SetTCBTcpAoEnabled(x, y)         ( TCBtable[x].u1TcpAoEnabled = (y) )
#define SetTCBTcpAoCurrKey(x, y)         ( TCBtable[x].u1TcpAoCurrentKey = (y) )
#define SetTCBTcpAoRNextKey(x, y)        ( TCBtable[x].u1TcpAoRNextKey = (y) )
#define SetTCBTcpAoNoMktDisc(x, y)       ( TCBtable[x].u1TcpAoNoMktDisc = (y) )
#define SetTCBTcpAoIcmpAcc(x, y)         ( TCBtable[x].u1TcpAoIcmpAcc = (y) )
#define SetTCBTcpAoRcvPckKeyId(x, y)     ( TCBtable[x].u1RcvPackKeyId = (y) )
#define SetTCBTcpAoRcvPckRNxtKeyId(x, y) ( TCBtable[x].u1RcvPackRNextKeyId = (y))
#define SetTCBTcpAoActiveInd(x, y)       ( TCBtable[x].u1ActiveInd = (y) )
#define SetTCBTcpAoNewInd(x, y)          ( TCBtable[x].u1NewInd = (y) )
#define SetTCBTcpAoBackupInd(x, y)       ( TCBtable[x].u1BackUpInd = (y) )

#define IncrTcpAoMacErrCtr(x)         ( TCBtable[x].u4TcpAoMacVerErrs++ )
#define IncrTcpAoSilentAccCtr(x)      ( TCBtable[x].u4SilentAcceptCtr++ )
#define IncrTcpAoIcmpIgnCtr(x)        ( TCBtable[x].u4IcmpIgnoreCtr++ )

/* 
 *  Called when a new MKT is configured
 *  a - tcb index, b - key id c - receive key id d- kdf algorithm 
 *  e - mac algorithm f - pointer to master key 
 *  g - password length h -tcp option ignore
 */

#define SetTcbTcpAoMkt(a, b, c, d, e, f, g, h)                \
{                                                                             \
    UINT1 Ind = TCPAO_MKT_INI_IND;                                            \
                                                                              \
    /*                                                                        \
     * Active Index is not used, assign index 0 and save the MKT to active    \
     * (fist config). On configuring Mkt for the second time New index will   \
     * not be used, initialise it to 1 and use index. All other times use     \
     * the new index field.                                                   \
     */                                                                       \
    (TCBtable[a].u1ActiveInd == TCPAO_MKT_INI_IND) ?                          \
     (TCBtable[a].u1ActiveInd = Ind = TCP_ZERO) :                             \
     ((TCBtable[a].u1NewInd == TCPAO_MKT_INI_IND) ?                           \
       (TCBtable[a].u1NewInd = Ind = TCP_ONE) :                              \
       (Ind = TCBtable[a].u1NewInd));                                         \
                                                                              \
                                                                              \
    GetMktByIndx(a, Ind).u1SendKeyId = b;                                     \
    GetMktByIndx(a, Ind).u1ReceiveKeyId = c;                                  \
    GetMktByIndx(a, Ind).u1KDFAlgo = d;                                       \
    GetMktByIndx(a, Ind).u1MACAlgo = e;                                       \
    GetMktByIndx(a, Ind).u1TcpAOPasswdLength = g;                             \
    GetMktByIndx(a, Ind).u1TcpOptionIgnore = h;                               \
                                                                              \
    MEMCPY (GetMktByIndx(a, Ind).au1TcpAOMasterKey, f, g);                    \
                                                                              \
    SetTCBTcpAoRNextKey(a, c);                                                \
    if ( TCBtable[a].u1ActiveInd == Ind )                                     \
    {                                                                         \
        SetTCBTcpAoCurrKey(a, b);                                             \
    }                                                                         \
}

/*
 * Called to change NewInd  to Active if required on reception
 * of a TCP segment with TCP-AO
 * This will change the current key field also.
 * x - Tcb Index, y - RNextKeyId field of the received packet
 */
#define SetMktNewIndToActiveIfReq(x, y)\
{ \
    UINT1 u1Temp; \
    UINT1 u1RNext = GetTCBTcpAoRNextKey(x); \
    if ((y !=  GetMktByIndx(x, TCBtable[x].u1ActiveInd).u1SendKeyId) && \
        (TCPAO_MKT_INI_IND  > (u1Temp = GetMktIndexFromRcvId(x, u1RNext))) && \
        (y == GetMktSendId(x, u1Temp))) \
    { \
        (TCBtable[x].u1BackUpInd >= TCPAO_MKT_INI_IND) ? \
        ( u1Temp = TCP_TWO) : ( u1Temp = TCBtable[x].u1BackUpInd) ; \
     \
        TCBtable[x].u1BackUpInd = TCBtable[x].u1ActiveInd; \
        TCBtable[x].u1ActiveInd = TCBtable[x].u1NewInd; \
        TCBtable[x].u1NewInd = u1Temp; \
     \
        if (TCBtable[x].u1ActiveInd < TCPAO_MKT_INI_IND) \
        SetTCBTcpAoCurrKey(x, GetMktByIndx(x, TCBtable[x].u1ActiveInd) \
                                                          .u1SendKeyId); \
    }    \
} 

/* Set for MKT */
/* x - Tcb Index y - Mkt Index  z - pointer to key */
#define SetMktSynKey(x, y, z)                                                  \
MEMCPY ((GetMktByIndx(x, y).au1TcpTrafficKeySyn), z, TCPAO_TRAFFIC_KEY_MAXLEN);
#define SetMktSndTrfcKey(x, y, z)                                              \
MEMCPY ((GetMktByIndx(x, y).au1TrafficKeySend), z, TCPAO_TRAFFIC_KEY_MAXLEN);
#define SetMktRcvTrfcKey(x, y, z)                                              \
MEMCPY ((GetMktByIndx(x, y).au1TrafficKeyReceive), z, TCPAO_TRAFFIC_KEY_MAXLEN);

/* End of Set & Increment for TCP-AO */

/* Function Prototype Declarations for TCP-AO  */
UINT1
TcpAoGetMktIndexFromRcvId(UINT4 u4Index, UINT1 u1RcvKeyId);
UINT1
TcpAoGetMktIndexFrmSendId (UINT4 u4Index, UINT1 u1SndKeyId);
#endif   /*TCP_TCB_H*/

