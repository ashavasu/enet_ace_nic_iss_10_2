/** $Id: tcpinpfn.c,v 1.17 2014/03/07 12:08:06 siva Exp $ */

/* SOURCE FILE HEADER
 *
 * ----------------------------------------------------------------------------
 *  FILE NAME             : tcpinpfn.c
 * 
 *  PRINCIPAL AUTHOR      : Ramesh Kumar
 *
 *  SUBSYSTEM NAME        : TCP
 *
 *  MODULE NAME           : Input Module
 *
 *  LANGUAGE              : C
 *
 *  TARGET ENVIRONMENT    : Any
 *
 *  DATE OF FIRST RELEASE :
 * 
 *  DESCRIPTION           : This file contains function used when a segment 
 *                          arrives.
 *
 * ---------------------------------------------------------------------------
 *
 *
 *  CHANGE RECORD :
 *
 * ---------------------------------------------------------------------------
 *   VERSION        AUTHOR/DATE           DESCRIPTION OF CHANGE
 * ---------------------------------------------------------------------------
 *
 *             Sri. Chellapa     Changed all
 *             13/05/96          sequence no. Comparisons to use SEQCMP
 *                               macro.   
 *   1.7       Rebecca Rufus     Check if the destination IP address of every   
 *             28-Sep-'98        every incoming packet is equal to Local IP
 *                               address bound.Refer Problen Id. 6.        
 *                               Calculation of offset corrected when processing
 *                               options.Refer Problem Id 11.                   
 * ------------------------------------------------------------------------*
 *     1.8      Saravanan.M    Implementation of                           *
 *              Purush            - Non Blocking Connect                   * 
 *                                - Non Blocking Accept                    *
 *                                 - IfMsg Interface between TCP and IP    *
 *                                - One Queue per Socket                   *
 * ------------------------------------------------------------------------*/

#include "tcpinc.h"
#include "fsmptclw.h"
#include "ip.h"
#include "fssyslog.h"

extern INT4         gi4TcpSysLogId;
extern UINT1        au1RxTcpAoBuffer[TCP_AO_MAC_BUF_LEN];
/*************************************************************************/
/*  Function Name   :  LINInpFindConnection                              */
/*  Input(s)        : pSegment - Pointer to the segment                  */
/*                    pIpHdr - pointer to Ip Header                      */
/*                    pTcpHdr - pointer to Tcp Header                    */
/*  Output(s)       : None.                                              */
/*  Returns         : TCB pointer on success                             */
/*                    ERR on failure                                     */
/*  Description     : Select the TCB corresponding to the segment        */
/* CALLING CONDITION : Whenever a segment comes from IP, it is called    */
/*                     to find the corresponding TCB.                    */
/* GLOBAL VARIABLES AFFECTED : None.                                     */
/*************************************************************************/
tTcb               *
LINInpFindConnection (UINT1 *pSegment, tIpHdr * pIpHdr, tTcpHdr * pTcpHdr)
{
    tIpAddr             IpSrc;
    tIpAddr             IpDest;
    tIpAddr             IpAddr;
    UINT1               u1TcpCode;    /* TCP Codes SYN,FIN etc.. */
    UINT2               u2TcpSrcPort;
    UINT2               u2TcpDstPort;
    UINT4               u4TcbIndex;
    UINT2               u2TempForSwapping;
    INT4                i4LastTcbIndex;
    INT4                i4TcbIndexFlag;
    t_IP_TO_HLMS_PARMS *pIp2TcpParms = NULL;
    UINT4               u4Context = 0;

    pIp2TcpParms = (t_IP_TO_HLMS_PARMS *) (VOID *) pSegment;
    u4Context = pIp2TcpParms->u4ContextId;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering LINInpFindConnection. \n");
    V6_NTOHL (&IpSrc, &(pIpHdr->SrcAddr));
    V6_NTOHL (&IpDest, &(pIpHdr->DestAddr));

    u1TcpCode = ((pTcpHdr->u1Code) & TCP_CODE_MASK);    /* only Flags */
    u2TcpSrcPort = OSIX_NTOHS (pTcpHdr->u2Sport);
    u2TcpDstPort = OSIX_NTOHS (pTcpHdr->u2Dport);

    if (pIp2TcpParms->u1Cmd == IP_ICMP_DATA)
    {
        /* Swap source and destination (IP and TCP) */
        IpAddr = IpSrc;
        IpSrc = IpDest;
        IpDest = IpAddr;
        u2TempForSwapping = u2TcpSrcPort;
        u2TcpSrcPort = u2TcpDstPort;
        u2TcpDstPort = u2TempForSwapping;
    }

    /* if local socket and remote socket matches with that of 
       segment then TCB is found                           */

    for (u4TcbIndex = INIT_COUNT, i4LastTcbIndex =
         TCP_MINUS_ONE, i4TcbIndexFlag = TCP_MINUS_ONE;
         u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
    {

        if (GetTCBstate (u4TcbIndex) == TCPS_FREE)
        {
            continue;
        }

        if ((GetTCBlport (u4TcbIndex) == u2TcpDstPort) &&
            /* Check Local Port */
            (GetTCBrport (u4TcbIndex) == u2TcpSrcPort) &&
            /* Check Dest. Port */
            /* Since V6_ADDR_CMP retunrs 0 for matching addresses */
            ((V6_ADDR_CMP (&(GetTCBrip (u4TcbIndex)), &IpSrc)) == TCP_ZERO) &&
            /* Check Dest. IP address */
            ((V6_ADDR_CMP (&(GetTCBlip (u4TcbIndex)), &IpDest)) == TCP_ZERO) &&
            /* Check Local IP address */
            (GetTCBContext (u4TcbIndex) == u4Context) &&
            /* Check TCB context */
            (GetTCBtype (u4TcbIndex) != TCP_SERVER))
        {
            /* since even servers can have same values in them */
            if (GetTCBstate (u4TcbIndex) != TCPS_TIMEWAIT)
                return (&TCBtable[u4TcbIndex]);    /* Got the right TCB */
        }

        /* Checking for a Server TCB */
        if ((GetTCBtype (u4TcbIndex) == TCP_SERVER) &&
            /* Check if it is a SERVER, and */
            (GetTCBlport (u4TcbIndex) == u2TcpDstPort) &&
            /* Check if port is ok, and */
            (GetTCBContext (u4TcbIndex) == u4Context) &&
            /* Check TCB context */
            (GetTCBstate (u4TcbIndex) == TCPS_LISTEN))
        {
            if (IN6_IS_ADDR_V4MAPPED (GetTCBlip (u4TcbIndex)))
            {
                if ((IN6_IS_ADDR_V4MAPPED (IpDest)) &&
                    (IN6_IS_ADDR_V4MAPPED (IpSrc)))
                {
                    if ((V4_FROM_V6_ADDR (GetTCBlip (u4TcbIndex))) !=
                        INADDR_ANY)
                    {
                        if (((V6_ADDR_CMP (&(GetTCBlip (u4TcbIndex)), &IpDest))
                             == TCP_ZERO) && (i4TcbIndexFlag != ZERO))
                        {
                            i4LastTcbIndex = u4TcbIndex;
                            i4TcbIndexFlag = TCP_ZERO;
                            /* If Only Syn bit is set i.e SERVER */
                            if ((u1TcpCode | TCPF_SYN) == TCPF_SYN)
                                break;
                        }
                    }
                    else
                    {
                        i4LastTcbIndex = u4TcbIndex;
                        i4TcbIndexFlag = TCP_ONE;
                    }
                }
            }
            else
            {
                if (!(IN6_IS_ADDR_V4MAPPED (IpDest)))
                {
                    if (!(IS_ADDR_IN6ADDRANYINIT ((GetTCBlip (u4TcbIndex)))))
                    {
                        if (((V6_ADDR_CMP (&(GetTCBlip (u4TcbIndex)), &IpDest))
                             == TCP_ZERO) && (i4TcbIndexFlag != ZERO))
                        {
                            i4LastTcbIndex = u4TcbIndex;
                            i4TcbIndexFlag = TCP_ZERO;
                            if ((u1TcpCode | TCPF_SYN) == TCPF_SYN)
                                break;
                        }
                    }
                    else
                    {
                        i4LastTcbIndex = u4TcbIndex;
                        i4TcbIndexFlag = TCP_ONE;
                    }
                }
            }
        }
    }

    /* Only SYN can arrive for server */
    if ((u1TcpCode & TCPF_SYN) && (i4TcbIndexFlag != TCP_NOT_OK))
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from LINInpFindConnection. Returned %x\n",
                     &TCBtable[i4LastTcbIndex]);
        return &TCBtable[i4LastTcbIndex];
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                 "Exit from LINInpFindConnection. Returned ERR.\n");
    return (tTcb *) ERR;
}

/*************************************************************************/
/*  Function Name   : LINInpIsSegInRcvWin                                */
/*  Input(s)        : pSegment - Pointer to the segment                  */
/*                    pIpHdr - pointer to Ip Header                      */
/*                    pTcpHdr - pointer to Tcp Header                    */
/*                  : i1Beyond - flag to indicate if segment is ok       */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK on success                                  */
/*                    ERR on failure                                     */
/*  Description     : Compare the incoming segment to information in     */
/*                    the TCB to see if the segment lies in the          */
/*                    receive window.                                    */
/* CALLING CONDITION : It is called for every incoming segment.          */
/* GLOBAL VARIABLES AFFECTED None                                        */
/*************************************************************************/
INT1
LINInpIsSegInRcvWin (tTcb * ptcb, tIpHdr * pIpHdr, tTcpHdr * pTcpHdr,
                     INT1 *i1Beyond)
{
    UINT4               u4Rwin, u4Wlast, u4Slast;
    INT4                i4TCPSeq, i4Rnext;
    UINT2               u2SegLen, u2SegDataLen;
    INT1                i1RetVal;
    UINT4               u4Tcb;
    UINT1               u1TCPCode;
    UINT1               u1TCPHlen;

    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering LINInpIsSegInRcvWin. \n");
    u4Tcb = GET_TCB_INDEX (ptcb);
    /*allow all segments in unsynchronised states */
    if (GetTCBstate (u4Tcb) < TCPS_SYNRCVD)
    {
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from LINInpIsSegInRcvWin. Returned TRUE\n");
        return (TRUE);
    }

    /*for others calculate segment length */
    u1TCPHlen = (UINT1) (((pTcpHdr->u1Hlen) >> TCP_VER_SHIFT)
                         << TCP_SHIFT_BYTE_TO_WORD);
    u2SegLen = (UINT2) (OSIX_NTOHS (pIpHdr->u2Totlen));
    u2SegDataLen = (UINT2) (u2SegLen - u1TCPHlen);
    /*Conceptually SYN & FIN occupy one position in the sequence space */

    u1TCPCode = pTcpHdr->u1Code;

    if (u1TCPCode & TCPF_SYN)
    {
        u2SegDataLen++;
    }
    if (u1TCPCode & TCPF_FIN)
    {
        u2SegDataLen++;
    }

    /*Compute receiver window size */
    u4Rwin = GetTCBrbsize (u4Tcb) - GetTCBrbcount (u4Tcb);
    i4Rnext = GetTCBrnext (u4Tcb);

    /*highest possible sequence number that lies in window */
    u4Wlast = (UINT4) i4Rnext + u4Rwin - TCP_ONE;
    i4TCPSeq = OSIX_NTOHL (pTcpHdr->i4Seq);

/******************************************************************
    Segment Receive  Test
    Length  Window
    ------- -------  -------------------------------------------
[1]    0       0     SEG.SEQ = RCV.NXT
[2]    0      >0     RCV.NXT =< SEG.SEQ < RCV.NXT+RCV.WND
[3]   >0       0     not acceptable
[4]   >0      >0     RCV.NXT =< SEG.SEQ < RCV.NXT+RCV.WND
                  or RCV.NXT =< SEG.SEQ+SEG.LEN-1 < RCV.NXT+RCV.WND
******************************************************************/

    *i1Beyond = FALSE;

    if (u4Rwin == ZERO && u2SegDataLen == ZERO)
    {                            /* [1] */
        i1RetVal = (INT1) ((SEQCMP (i4TCPSeq, GetTCBrnext (u4Tcb)) == ZERO));
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from LINInpIsSegInRcvWin. Returned %d\n", i1RetVal);
        return i1RetVal;
    }

    /*If data lies in the acceptable range, then it is fine */

    i1RetVal = (INT1) (((SEQCMP (i4TCPSeq, GetTCBrnext (u4Tcb)) >= ZERO) &&
                        (SEQCMP (i4TCPSeq, (INT4) u4Wlast) <= ZERO)));

    if (u2SegDataLen == ZERO)
    {                            /* [2] */
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from LINInpIsSegInRcvWin. Returned %d\n", i1RetVal);
        return i1RetVal;
    }

    u4Slast = (UINT4) i4TCPSeq + u2SegDataLen - TCP_ONE;
    i1RetVal |= (SEQCMP ((INT4) u4Slast, GetTCBrnext (u4Tcb)) >= ZERO) &&    /* [4] */
        (SEQCMP ((INT4) u4Slast, (INT4) u4Wlast) <= ZERO);

    /*Even if window size is zero allow some seg */
    if (u4Rwin == ZERO)
    {
        UINT2               newLength = u1TCPHlen;
        pIpHdr->u2Totlen = OSIX_HTONS (newLength);

        *i1Beyond = FALSE;
        if (u1TCPCode & TCPF_RST)
        {
            *i1Beyond = TRUE;
            TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                         "TCP",
                         "Exit from LINInpIsSegInRcvWin. Returned FALSE.\n");
            return FALSE;
        }
        return FALSE;
    }
    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from LINInpIsSegInRcvWin. Returned %d\n", i1RetVal);
    return i1RetVal;
}

/*************************************************************************/
/*  Function Name   : getinfo4                                           */
/*  Input(s)        : pSeg - Pointer to segment                          */
/*                    off - offset                                       */
/*  Output(s)       : None.                                              */
/*  Returns         : 4 byte number                                      */
/*  Description     : It copies 4 bytes from the segment starting        */
/*                    from the given offset.                             */
/*************************************************************************/
UINT4
getinfo4 (tSegment * pSeg, UINT4 off)
{
    UINT4               u4Val;

    CRU_BUF_Copy_FromBufChain (pSeg, (UINT1 *) &u4Val, off, 4);
    return (u4Val);
}

/*************************************************************************/
/*  Function Name   : getinfo2                                           */
/*  Input(s)        : pSeg - Pointer to segment                          */
/*                    off - offset                                       */
/*  Output(s)       : None.                                              */
/*  Returns         : 2 byte number                                      */
/*  Description     : It copies 2 bytes from the segment starting        */
/*                    from the given offset.                             */
/*************************************************************************/
UINT2
getinfo2 (tSegment * pSeg, UINT4 off)
{
    UINT2               u2Val;

    CRU_BUF_Copy_FromBufChain (pSeg, (UINT1 *) &u2Val, off, 2);
    return (u2Val);
}

/*************************************************************************/
/*  Function Name   : getinfo1                                           */
/*  Input(s)        : pSeg - Pointer to segment                          */
/*                    off - offset                                       */
/*  Output(s)       : None.                                              */
/*  Returns         : 1 byte number                                      */
/*  Description     : It copies 1 bytes from the segment starting        */
/*                    from the given offset.                             */
/*************************************************************************/
UINT1
getinfo1 (tSegment * pSeg, UINT4 off)
{
    UINT1               u1Val;

    CRU_BUF_Copy_FromBufChain (pSeg, (UINT1 *) &u1Val, off, 1);
    return (u1Val);
}

/*************************************************************************/
/*  Function Name   : LINInpCheckOptions                                 */
/*  Input(s)        : ptcb - pointer to Tcb                              */
/*                    pTcpHdr - pointer to Tcp header                    */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK   on success                                */
/*                    ERR   on failure                                   */
/*  Description     : It checks all the TCP options specified by appl'n  */
/*                    Right now only the MSS option is defined.          */
/* CALLING CONDITION  : It is called during connection setup to see      */
/*                      the user specified MSS.                          */
/* GLOBAL VARIABLES AFFECTED : None                                      */
/*************************************************************************/
INT1
LINInpCheckOptions (tTcb * ptcb, tTcpHdr * pTcpHdr)
{
    UINT4               u4OptOffset;
    UINT1               u1Len, u1Optlen, u1Temp;
    UINT1               u1Option;

    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering LINInpCheckOptions.\n");
    u1Len = (UINT1) (((pTcpHdr->u1Hlen) >> TCP_VER_SHIFT)
                     << TCP_SHIFT_BYTE_TO_WORD);
    if (u1Len == TCP_HEADER_LENGTH)
    {
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from LINInpCheckOptions. Returned OK.\n");
        return (TCP_OK);
    }

    u1Optlen = (UINT1) (u1Len - TCP_HEADER_LENGTH);
    u4OptOffset = ZERO;

    for (u1Temp = INIT_COUNT; u1Temp < u1Optlen; u1Temp++)
    {
        u1Option = pTcpHdr->au1TcpOpts[u4OptOffset];
        u4OptOffset++;

        switch (u1Option)
        {
            case TCP_EOOL:
                u1Temp = u1Optlen;
                break;            /*no more option */
            case TCP_NOOP:
                u1Temp++;
                break;
            case TCP_MSS:
                u1Temp += LINInpUpdateIncomingMss (ptcb, pTcpHdr, u4OptOffset);
                u4OptOffset += (u1Temp - TCP_ONE);
                break;
            default:
                u1Option = pTcpHdr->au1TcpOpts[u4OptOffset];
                if (u1Option < TCP_MIN_OPT_LENGTH)
                {
                    /* ANVL BUG FIX - RFC 1122 says that every option even if it 
                     * illegal, has length. If the length is zero, it does not 
                     * say what is to be done. It suggests resetting the connection. 
                     * We will ignore the option taking the length to be 2, the option
                     * code and the illegal length. */
                    u1Option = TCP_MIN_OPT_LENGTH;
                }
                u1Temp += u1Option;
                u4OptOffset += u1Option - TCP_ONE;
                break;            /* Ignore unknown option */
        }
    }
    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from LINInpCheckOptions. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : LINInpUpdateIncomingMss                            */
/*  Input(s)        : ptcb - pointer to Tcb                              */
/*                    pTcpHdr - pointer to Tcp header                    */
/*                    u4Offset - Data offset                             */
/*  Output(s)       : None                                               */
/*  Returns         : Length of the mss option.                          */
/*  Description     : Updates the mss option from the incoming SYN.      */
/*  CALLING CONDITION : None.                                            */
/*  GLOBAL VARIABLES AFFECTED : TCBtable.                                */
/*************************************************************************/
UINT1
LINInpUpdateIncomingMss (tTcb * ptcb, tTcpHdr * pTcpHdr, UINT4 u4Offset)
{
    UINT4               u4Mss = 0;
    UINT2               u2Mss = 0;
    UINT4               u4BufOffset;
    UINT1               u1Len;
    UINT4               u4TcbIndex;

    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering LINInpUpdateIncomingMss.\n");
    u4TcbIndex = GET_TCB_INDEX (ptcb);
    u1Len = pTcpHdr->au1TcpOpts[u4Offset];    /* Extract Option Length */

    /*check for a SYN */
    if (!(pTcpHdr->u1Code & TCPF_SYN))
    {
        TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from LINInpUpdateIncomingMss. Returned %d\n", u1Len);
        return u1Len;
    }

    u4BufOffset = u4Offset + TCP_ONE;

    switch (u1Len - OPT_SUB_LEN)
    {

        case TCP_MSS_ONE_BYTE_OPT:
            u4Mss = pTcpHdr->au1TcpOpts[u4BufOffset];
            break;

        case TCP_MSS_TWO_BYTE_OPT:
            MEMCPY (&u2Mss, &pTcpHdr->au1TcpOpts[u4BufOffset], sizeof (UINT2));
            u4Mss = (UINT4) u2Mss;
            u4Mss = OSIX_NTOHS (u4Mss);
            break;

        case TCP_MSS_FOUR_BYTE_OPT:
            MEMCPY (&u4Mss, &pTcpHdr->au1TcpOpts[u4BufOffset], sizeof (UINT4));
            u4Mss = OSIX_NTOHL (u4Mss);
            break;

        default:
            u4Mss = TCP_DEFAULT_RMSS;
            break;
    }

    if (GetTCBrmss (u4TcbIndex) < u4Mss)
    {
        SetTCBrmss (u4TcbIndex, (UINT2) u4Mss);
    }

    if (GetTCBsmss (u4TcbIndex))
    {
        SetTCBsmss (u4TcbIndex,
                    (UINT2) MINIMUM (u4Mss, GetTCBsmss (u4TcbIndex)));
    }
    else
    {
        SetTCBsmss (u4TcbIndex, (UINT2) u4Mss);
    }
    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from LINInpUpdateIncomingMss. Returned %d\n", u1Len);
    return u1Len;
}

/*************************************************************************/
/*  Function Name   : LINInpMD5Locate                                    */
/*  Input(s)        : u4Context - Context Id                             */
/*                    pTcpHdr - pointer to Tcp header                    */
/*  Output(s)       : None                                               */
/*  Returns         : Position of TCP MD5 option in TCP header           */
/*  Description     : Locates TCP MD5 option in incoming segment's TCP   */
/*                    header                                             */
/*  CALLING CONDITION : It is called for every incoming segment          */
/*  GLOBAL VARIABLES AFFECTED : None                                     */
/*************************************************************************/
UINT1              *
LINInpMD5Locate (UINT4 u4Context, tTcpHdr * pTcpHdr)
{
    UINT4               u4OptOffset = TCP_ZERO;
    UINT1              *pMD5OptPos = NULL;
    UINT1               u1Len = TCP_ZERO;
    UINT1               u1Optlen = TCP_ZERO;
    UINT1               u1Temp = TCP_ZERO;
    UINT1               u1Option = TCP_ZERO;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering LINInpMD5Locate.\n");
    u1Len = (UINT1) (((pTcpHdr->u1Hlen) >> TCP_VER_SHIFT)
                     << TCP_SHIFT_BYTE_TO_WORD);

    if (u1Len == TCP_HEADER_LENGTH)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from LINInpMD5Locate. No MD5 option in \
                 incoming segment. Returned NULL.\n");
        return NULL;
    }

    u1Optlen = (UINT1) (u1Len - TCP_HEADER_LENGTH);
    if (u1Optlen < MD5_OPT_LEN)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from LINInpMD5Locate. No MD5 option in \
                 incoming segment. Returned NULL.\n");
        return NULL;
    }
    u4OptOffset = ZERO;

    for (u1Temp = INIT_COUNT; u1Temp < u1Optlen; u1Temp++)
    {
        u1Option = pTcpHdr->au1TcpOpts[u4OptOffset];
        u4OptOffset++;

        switch (u1Option)
        {
            case TCP_EOOL:
                u1Temp = u1Optlen;
                break;
            case TCP_NOOP:
                u1Temp++;
                break;
            case TCP_MD5:
                /* Validate the Option Length field to be 18 */
                u1Option = pTcpHdr->au1TcpOpts[u4OptOffset];
                if (u1Option != MD5_OPT_LEN)
                {
                    return NULL;
                }
                /* Increment to next byte for start of digest */
                u4OptOffset++;
                pMD5OptPos = (UINT1 *) &(pTcpHdr->au1TcpOpts[u4OptOffset]);
                TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                             "Exit from LINInpMD5Locate. MD5 option found.\n");
                return (pMD5OptPos);
            default:
                u1Option = pTcpHdr->au1TcpOpts[u4OptOffset];
                if (u1Option < TCP_MIN_OPT_LENGTH)
                {
                    u1Option = TCP_MIN_OPT_LENGTH;
                }
                u1Temp += u1Option;
                u4OptOffset += u1Option - TCP_ONE;
                break;            /* Ignore unknown option */
        }
    }

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from LINInpCheckOptions. No MD5 option in \
                 incoming segment. Returned NULL.\n");
    return (NULL);
}

/*************************************************************************/
/*  Function Name   : LINInpCheckMd5Option                               */
/*  Input(s)        : pRcvSeg - pointer to Ip+Tcp header of incoming     */
/*                              segment                                  */
/*                    pSeg    - ptr to CRU buffer containing segment     */
/*                    u4TcbIndex - Connection ID                         */
/*  Output(s)       : None                                               */
/*  Returns         : TCP_OK     - If it is an MD5 protected connection  */
/*                                 and incoming digest is correct OR if  */
/*                                 connection is not MD5 protected and   */
/*                                 incoming segment also does not contain*/
/*                                 MD5 digest                            */
/*                    TCP_NOT_OK - If it is an MD5 protected connection  */
/*                                 and incoming segment does not contain */
/*                                 or has an incorrect digest OR if      */
/*                                 connection not MD5 protected but      */
/*                                 segment contains the option           */
/*  Description     : The routine verifies incoming segment for MD5      */
/*                    option                                             */
/*                    header                                             */
/*  CALLING CONDITION : It is called for every incoming segment          */
/*  GLOBAL VARIABLES AFFECTED : None                                     */
/*************************************************************************/
INT1
LINInpCheckMd5Option (tIpTcpHdr * pRcvSeg, tSegment * pSeg, UINT4 u4TcbIndex)
{
    UINT1              *pDigestPos = NULL;
    INT4                i4SockDesc = TCP_ZERO;
    UINT1               au1Key[TCP_MD5SIG_MAXKEYLEN];
    UINT1               au1CalcDigest[MD5_DIGEST_LEN];
    UINT1               u1Keylen = TCP_ZERO;
    UINT1               u1DigestExpected = FALSE;
    UINT4               u4Context;
#ifdef SYSLOG_WANTED
    tUtlIn6Addr         In6Addr;
    tUtlInAddr          InAddr;
    CHR1                au1SrcAddr[50];
    CHR1                au1DstAddr[50];
    UINT2               u2Len = 0;

    MEMSET (au1SrcAddr, TCP_ZERO, sizeof (au1SrcAddr));
    MEMSET (au1DstAddr, TCP_ZERO, sizeof (au1DstAddr));
#endif

    u4Context = GetTCBContext (u4TcbIndex);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering LINInpCheckMd5Option.\n");

    MEMSET (au1Key, TCP_ZERO, TCP_MD5SIG_MAXKEYLEN);
    MEMSET (au1CalcDigest, TCP_ZERO, MD5_DIGEST_LEN);

    /* Check incoming segment has digest */
    pDigestPos = LINInpMD5Locate (u4Context, pRcvSeg->tcp);

    /* Check if MD5 digest is expected */

    /* If segment is for a passively opened connection,
     * interface with SLI to check if MD5 key is set for peer 
     * on corresponding listen socket */
    if (GetTCBtype (u4TcbIndex) == TCP_SERVER)
    {
        i4SockDesc = SliTcpFindSockDesc (u4TcbIndex);
        if (SliTcpFindMd5Key
            (i4SockDesc, pRcvSeg->ip->SrcAddr, au1Key, &u1Keylen) == TRUE)
        {
            u1DigestExpected = TRUE;
        }
    }
    else
    {
        /* Segment is part of a connected TCB. Check TCB if MD5 key is set */
        if (GetTCBmd5keylen (u4TcbIndex) != TCP_ZERO)
        {
            u1DigestExpected = TRUE;
            MEMCPY (au1Key, (UINT1 *) GetTCBpmd5key (u4TcbIndex),
                    GetTCBmd5keylen (u4TcbIndex));
            u1Keylen = GetTCBmd5keylen (u4TcbIndex);
        }
    }

    /* If digest is not expected return OK if incoming segment also 
     * does not contain it */
    if ((u1DigestExpected == FALSE) && (pDigestPos == NULL))
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from LINInpCheckMd5Option. Returned OK.\n");
        return TCP_OK;
    }

    /* If digest is not expected but segment contains it return NOT OK */
    if ((u1DigestExpected == FALSE) && (pDigestPos != NULL))
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP",
                     "Exit from LINInpCheckMd5Option. MD5 option not expected \
            but segment has it. Returned NOT OK.\n");
        return TCP_NOT_OK;
    }

    /* If digest is expected but segment does not contain it return NOT OK */
    if ((u1DigestExpected == TRUE) && (pDigestPos == NULL))
    {
        IncrTCBmd5errs (u4TcbIndex);
        IncTCPstat (GetTCBContext (u4TcbIndex), InErrs);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP",
                     "Exit from LINInpCheckMd5Option. MD5 option expected but \
            segment does not have it. Returned NOT OK.\n");
#ifdef SYSLOG_WANTED
        if ((IsIP6Addr (pRcvSeg->ip->SrcAddr)) ||
            (IsIP6Addr (pRcvSeg->ip->DestAddr)))
        {
            MEMCPY (&In6Addr.u1addr, &((pRcvSeg->ip->SrcAddr).u1_addr),
                    IPVX_IPV6_ADDR_LEN);
            u2Len =
                (UINT2) (STRLEN (INET_NTOA6 (In6Addr)) <
                         sizeof (au1SrcAddr) ? STRLEN (INET_NTOA6 (In6Addr)) :
                         sizeof (au1SrcAddr) - 1);
            STRNCPY (au1SrcAddr, INET_NTOA6 (In6Addr), u2Len);
            au1SrcAddr[u2Len] = '\0';
            MEMCPY (&In6Addr.u1addr, &((pRcvSeg->ip->DestAddr).u1_addr),
                    IPVX_IPV6_ADDR_LEN);
            STRNCPY (au1DstAddr, INET_NTOA6 (In6Addr), u2Len);
            au1DstAddr[u2Len] = '\0';
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4TcpSysLogId,
                          "No MD5 digest from %s(%d) to %s(%d)",
                          au1SrcAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Sport),
                          au1DstAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Dport)));
        }
        else
        {
            InAddr.u4Addr = V4_FROM_V6_ADDR (pRcvSeg->ip->SrcAddr);
            u2Len = (UINT2) (STRLEN (INET_NTOA (InAddr)) < sizeof (au1SrcAddr) ?
                             STRLEN (INET_NTOA (InAddr)) : sizeof (au1SrcAddr) -
                             1);
            STRNCPY (au1SrcAddr, INET_NTOA (InAddr), u2Len);
            au1SrcAddr[u2Len] = '\0';
            InAddr.u4Addr = V4_FROM_V6_ADDR (pRcvSeg->ip->DestAddr);
            STRNCPY (au1DstAddr, INET_NTOA (InAddr), u2Len);
            au1DstAddr[u2Len] = '\0';
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4TcpSysLogId,
                          "No MD5 digest from %s(%d) to %s(%d)",
                          au1SrcAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Sport),
                          au1DstAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Dport)));
        }
#endif
        return TCP_NOT_OK;
    }

    /* Digest is expected and segment contains it. Verify it */
    if (TcpOptGenerateMD5Digest
        (u4Context, pRcvSeg, pSeg, au1CalcDigest, au1Key, u1Keylen) == TCP_OK)
    {
        if (MEMCMP (au1CalcDigest, pDigestPos, MD5_DIGEST_LEN) != TCP_ZERO)
        {
            IncrTCBmd5errs (u4TcbIndex);
            IncTCPstat (GetTCBContext (u4TcbIndex), InErrs);
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                         "TCP", "Exit from LINInpCheckMd5Option.\
                         MD5 digest is incorrect. Returned NOT OK.\n");
#ifdef SYSLOG_WANTED
            if ((IsIP6Addr (pRcvSeg->ip->SrcAddr)) ||
                (IsIP6Addr (pRcvSeg->ip->DestAddr)))
            {
                MEMCPY (&In6Addr.u1addr, &((pRcvSeg->ip->SrcAddr).u1_addr),
                        IPVX_IPV6_ADDR_LEN);
                u2Len =
                    (UINT2) (STRLEN (INET_NTOA6 (In6Addr)) <
                             sizeof (au1SrcAddr) ? STRLEN (INET_NTOA6 (In6Addr))
                             : sizeof (au1SrcAddr) - 1);
                STRNCPY (au1SrcAddr, INET_NTOA6 (In6Addr), u2Len);
                au1SrcAddr[u2Len] = '\0';
                MEMCPY (&In6Addr.u1addr, &((pRcvSeg->ip->DestAddr).u1_addr),
                        IPVX_IPV6_ADDR_LEN);
                STRNCPY (au1DstAddr, INET_NTOA6 (In6Addr), u2Len);
                au1DstAddr[u2Len] = '\0';
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4TcpSysLogId,
                              "Bad MD5 digest from %s(%d) to %s(%d)",
                              au1SrcAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Sport),
                              au1DstAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Dport)));
            }
            else
            {
                InAddr.u4Addr = V4_FROM_V6_ADDR (pRcvSeg->ip->SrcAddr);
                u2Len =
                    (UINT2) (STRLEN (INET_NTOA (InAddr)) <
                             sizeof (au1SrcAddr) ? STRLEN (INET_NTOA (InAddr)) :
                             sizeof (au1SrcAddr) - 1);
                STRNCPY (au1SrcAddr, INET_NTOA (InAddr), u2Len);
                au1SrcAddr[u2Len] = '\0';
                InAddr.u4Addr = V4_FROM_V6_ADDR (pRcvSeg->ip->DestAddr);
                STRNCPY (au1DstAddr, INET_NTOA (InAddr), u2Len);
                au1DstAddr[u2Len] = '\0';
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4TcpSysLogId,
                              "Bad MD5 digest from %s(%d) to %s(%d)",
                              au1SrcAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Sport),
                              au1DstAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Dport)));
            }
#endif
            return TCP_NOT_OK;
        }
    }
    else
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP",
                     "Exit from LINInpCheckMd5Option. MD5 digest generation error.\
            Returned NOT OK.\n");
        return TCP_NOT_OK;
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from LINInpCheckMd5Option. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : LINInpTCPAOLocate                                  */
/*  Input(s)        : pTcpHdr - pointer to Tcp header                    */
/*  Output(s)       : None                                               */
/*  Returns         : Position of TCP-AO option in TCP header            */
/*  Description     : Locates TCP-AO option in incoming segment's TCP    */
/*                    header                                             */
/*  CALLING CONDITION : It is called for every incoming segment          */
/*  GLOBAL VARIABLES AFFECTED : None                                     */
/*************************************************************************/
UINT1              *
LINInpTCPAOLocate (UINT4 u4Context, tTcpHdr * pTcpHdr, UINT1 *pMultTcpAoOpt)
{
    UINT4               u4OptOffset = TCP_ZERO;
    UINT1              *pTcpAoOptPos = NULL;
    UINT1               u1Len = TCP_ZERO;
    UINT1               u1Optlen = TCP_ZERO;
    UINT1               u1Temp = TCP_ZERO;
    UINT1               u1Option = TCP_ZERO;
    UINT1               u1NumTcpAo = TCP_ZERO;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering LINInpTCPAOLocate .\n");
    u1Len = (UINT1) (((pTcpHdr->u1Hlen) >> TCP_VER_SHIFT)
                     << TCP_SHIFT_BYTE_TO_WORD);

    if (u1Len == TCP_HEADER_LENGTH)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from LINInpTCPAOLocate. No TCP-AO option in \
                 incoming segment. Returned NULL.\n");
        return NULL;
    }

    u1Optlen = (UINT1) (u1Len - TCP_HEADER_LENGTH);
    if (u1Optlen < TCPAO_OPT_LEN)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from LINInpTCPAOLocate No TCP-AO option in \
                 incoming segment. Returned NULL.\n");
        return NULL;
    }
    u4OptOffset = ZERO;

    for (u1Temp = INIT_COUNT; u1Temp < u1Optlen; u1Temp++)
    {
        u1Option = pTcpHdr->au1TcpOpts[u4OptOffset];
        u4OptOffset++;

        switch (u1Option)
        {
            case TCP_EOOL:
                u1Temp = u1Optlen;
                break;
            case TCP_NOOP:
                u1Temp++;
                break;
            case TCP_AO:
                /* Validate the Option Length field to be 16 */
                u1Option = pTcpHdr->au1TcpOpts[u4OptOffset];
                if (u1Option != TCPAO_OPT_LEN)
                {
                    return NULL;
                }
                if (!u1NumTcpAo)
                {
                    /* Increment to next byte points to keyId */
                    u4OptOffset++;
                    pTcpAoOptPos =
                        (UINT1 *) &(pTcpHdr->au1TcpOpts[u4OptOffset]);
                    u1NumTcpAo++;
                    /* move the pointer to check more than one TCP-AO option is present */
                    u1Temp = (UINT1) (u1Temp + u1Option);
                    u4OptOffset = u4OptOffset + ((UINT4) (u1Option - TCP_TWO));

                }
                else
                {
                    *pMultTcpAoOpt = u1NumTcpAo;
                    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                                 "Exit from LINInpTCPAOLocate. Multiple TCP-AO option found.\n");
                    return (pTcpAoOptPos);

                }
                break;
            default:
                u1Option = pTcpHdr->au1TcpOpts[u4OptOffset];
                if (u1Option < TCP_MIN_OPT_LENGTH)
                {
                    u1Option = TCP_MIN_OPT_LENGTH;
                }
                u1Temp = (UINT1) (u1Temp + u1Option);
                u4OptOffset = u4OptOffset + ((UINT4) (u1Option - TCP_ONE));
                break;            /* Ignore unknown option */
        }
    }
    if (u1NumTcpAo)
    {
        *pMultTcpAoOpt = TCP_ZERO;
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from LINInpTCPAOLocate. TCP-AO option found.\n");
        return (pTcpAoOptPos);

    }

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from LINInpCheckOptions. No TCP-AO option in \
                 incoming segment. Returned NULL.\n");
    return (NULL);
}

/*************************************************************************/
/*  Function Name   : LINInpCheckTCPAOOption                               */
/*  Input(s)        : pRcvSeg - pointer to Ip+Tcp header of incoming     */
/*                              segment                                  */
/*                    pSeg    - ptr to CRU buffer containing segment     */
/*                    u4TcbIndex - Connection ID                         */
/*  Output(s)       : None                                               */
/*  Returns         : TCP_OK     - If it is an TCP-AO protected          */
/*                                   connection and incoming MAC           */
/*                                 digest is correct OR if connection    */
/*                                 is not TCP-AO protected and           */
/*                                 incoming segment also does not contain*/
/*                                 TCP-AO MAC                            */
/*                    TCP_NOT_OK - If it is an TCP-AO protected connection */
/*                                 and incoming segment does not contain */
/*                                 or has an incorrect MAC OR if      */
/*                                 connection not TCP-AO protected but      */
/*                                 segment contains the option           */
/*  Description     : The routine verifies incoming segment for TCP-AO      */
/*                    option                                             */
/*                    header                                             */
/*  CALLING CONDITION : It is called for every incoming segment          */
/*  GLOBAL VARIABLES AFFECTED : None                                     */
/*************************************************************************/

INT1
LINInpCheckTCPAOOption (tIpTcpHdr * pRcvSeg, tSegment * pSeg, UINT4 u4TcbIndex)
{
    UINT1              *pDigestPos = NULL;
    UINT1              *pMacPos = NULL;
    INT4                i4SockDesc = TCP_ZERO;
    UINT1               au1Key[TCPAO_TRAFFIC_KEY_MAXLEN];
    /*
     * Generated MAC is TCPAO_TRAFFIC_KEY_MAXLEN(20), 
     * but need to use only TCPAO_MAC_LEN(12) in TCP-AO authentication
     */
    UINT1               au1TcpAoMac[TCPAO_TRAFFIC_KEY_MAXLEN];
    UINT1               u1KeyLen = TCP_ZERO;
    UINT1               u1MacExpected = FALSE;
    UINT1               u1TcpAoMult = TCP_ZERO;
    UINT4               u4Context;
    tUtlIn6Addr         In6Addr;
    tUtlIn6Addr         In6SAddr;
    tUtlInAddr          InAddr;
    tUtlInAddr          InSAddr;
    UINT1               u1RcvKeyId;
    UINT1               u1SndKeyId;
    UINT1               u1Indx;
    CHR1                au1SrcAddr[50];
    CHR1                au1DstAddr[50];
    tsliTcpAoMktListNode *ptcpAoMktNd = NULL;
    UINT1               u1TcpOptIgn = TCP_ZERO;
    tTcpAoErrLogEntry   TcpAoErrLog;
    INT4                i4Trap;
    UINT2               u2Len = 0;

    MEMSET (au1SrcAddr, TCP_ZERO, sizeof (au1SrcAddr));
    MEMSET (au1DstAddr, TCP_ZERO, sizeof (au1DstAddr));

    MEMSET (au1Key, TCP_ZERO, TCPAO_TRAFFIC_KEY_MAXLEN);
    MEMSET (au1TcpAoMac, TCP_ZERO, TCPAO_MAC_LEN);

    u4Context = GetTCBContext (u4TcbIndex);
    TcpAoErrLog.i4Context = (INT4) u4Context;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering LINInpCheckTCPAOOption.\n");
    /* Check incoming packet has TCP-AO Mac */
    pMacPos = LINInpTCPAOLocate (u4Context, pRcvSeg->tcp, &u1TcpAoMult);

    if (u1TcpAoMult)
    {
        /* multiple TCP-AO option found */
        IncrTcpAoMacErrCtr (u4TcbIndex);
        IncTCPstat (GetTCBContext (u4TcbIndex), InErrs);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP",
                     "Exit from LINInpCheckTCPAOOption . multiple TCP-AO option  \
              found in the segment. Returned NOT OK.\n");
        if (IsIP6Addr (pRcvSeg->ip->SrcAddr))
        {
            MEMCPY (&In6Addr.u1addr, &((pRcvSeg->ip->SrcAddr).u1_addr),
                    IPVX_IPV6_ADDR_LEN);
            In6SAddr = In6Addr;
            TcpAoErrLog.In6SAdr = &In6SAddr;
            u2Len =
                (UINT2) (STRLEN (INET_NTOA6 (In6Addr)) <
                         sizeof (au1SrcAddr) ? STRLEN (INET_NTOA6 (In6Addr)) :
                         sizeof (au1SrcAddr) - 1);
            STRNCPY (au1SrcAddr, INET_NTOA6 (In6Addr), u2Len);
            au1SrcAddr[u2Len] = '\0';
            MEMCPY (&In6Addr.u1addr, &((pRcvSeg->ip->DestAddr).u1_addr),
                    IPVX_IPV6_ADDR_LEN);
            TcpAoErrLog.In6DAdr = &In6Addr;
            STRNCPY (au1DstAddr, INET_NTOA6 (In6Addr), u2Len);
            au1DstAddr[u2Len] = '\0';
            TcpAoErrLog.i1AdType = TCP_AF_IPV6_TYPE;
#ifdef SYSLOG_WANTED
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4TcpSysLogId,
                          "Multiple TCP-AO option in one segment from %s(%d) to %s(%d)",
                          au1SrcAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Sport),
                          au1DstAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Dport)));
#endif
        }
        else
        {
            InAddr.u4Addr = V4_FROM_V6_ADDR (pRcvSeg->ip->SrcAddr);
            u2Len = (UINT2) (STRLEN (INET_NTOA (InAddr)) < sizeof (au1SrcAddr) ?
                             STRLEN (INET_NTOA (InAddr)) : sizeof (au1SrcAddr) -
                             1);
            STRNCPY (au1SrcAddr, INET_NTOA (InAddr), u2Len);
            au1SrcAddr[u2Len] = '\0';
            InSAddr = InAddr;
            TcpAoErrLog.InSAdr = &InSAddr;
            InAddr.u4Addr = V4_FROM_V6_ADDR (pRcvSeg->ip->DestAddr);
            STRNCPY (au1DstAddr, INET_NTOA (InAddr), u2Len);
            au1DstAddr[u2Len] = '\0';
            TcpAoErrLog.InDAdr = &InAddr;
            TcpAoErrLog.i1AdType = TCP_AF_IPV4_TYPE;
#ifdef SYSLOG_WANTED
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4TcpSysLogId,
                          "Multiple TCP-AO option in one segment from %s(%d) to %s(%d)",
                          au1SrcAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Sport),
                          au1DstAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Dport)));
#endif
        }
        TcpAoErrLog.u2SPort = OSIX_NTOHS (pRcvSeg->tcp->u2Sport);
        TcpAoErrLog.u2DPort = OSIX_NTOHS (pRcvSeg->tcp->u2Dport);
        TcpAoErrLog.u4TcpAoErCtr = GetTcpAoMacVerErrCtr (u4TcbIndex);

        nmhGetFsMITcpTrapAdminStatus ((INT4) u4Context, &i4Trap);
        if (i4Trap == TCP_TRAP_ENABLE)
        {
            TcpAoAuthNotifyEventTrap (&TcpAoErrLog, TCP_ONE);
        }

        return TCP_NOT_OK;
    }

    /* Check incoming segment has MD5 digest */
    pDigestPos = LINInpMD5Locate (u4Context, pRcvSeg->tcp);

    if ((pDigestPos != NULL) && (pMacPos != NULL))
    {
        /* TCP-AO & TCP MD5 option found */
        IncrTcpAoMacErrCtr (u4TcbIndex);
        IncTCPstat (GetTCBContext (u4TcbIndex), InErrs);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP",
                     "Exit from LINInpCheckTCPAOOption . TCP-AO & TCP-MD5 option  \
              found in the segment. Returned NOT OK.\n");

        if (IsIP6Addr (pRcvSeg->ip->SrcAddr))
        {
            MEMCPY (&In6Addr.u1addr, &((pRcvSeg->ip->SrcAddr).u1_addr),
                    IPVX_IPV6_ADDR_LEN);
            In6SAddr = In6Addr;
            TcpAoErrLog.In6SAdr = &In6SAddr;
            u2Len =
                (UINT2) (STRLEN (INET_NTOA6 (In6Addr)) <
                         sizeof (au1SrcAddr) ? STRLEN (INET_NTOA6 (In6Addr)) :
                         sizeof (au1SrcAddr) - 1);
            STRNCPY (au1SrcAddr, INET_NTOA6 (In6Addr), u2Len);
            au1SrcAddr[u2Len] = '\0';
            MEMCPY (&In6Addr.u1addr, &((pRcvSeg->ip->DestAddr).u1_addr),
                    IPVX_IPV6_ADDR_LEN);
            TcpAoErrLog.In6DAdr = &In6Addr;
            TcpAoErrLog.i1AdType = TCP_AF_IPV6_TYPE;
            STRNCPY (au1DstAddr, INET_NTOA6 (In6Addr), u2Len);
            au1DstAddr[u2Len] = '\0';
#ifdef SYSLOG_WANTED
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4TcpSysLogId,
                          "TCP-AO & TCP MD5 option in one segment from %s(%d) to %s(%d)",
                          au1SrcAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Sport),
                          au1DstAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Dport)));
#endif
        }
        else
        {
            InAddr.u4Addr = V4_FROM_V6_ADDR (pRcvSeg->ip->SrcAddr);
            InSAddr = InAddr;
            TcpAoErrLog.InSAdr = &InSAddr;
            u2Len = (UINT2) (STRLEN (INET_NTOA (InAddr)) < sizeof (au1SrcAddr) ?
                             STRLEN (INET_NTOA (InAddr)) : sizeof (au1SrcAddr) -
                             1);
            STRNCPY (au1SrcAddr, INET_NTOA (InAddr), u2Len);
            au1SrcAddr[u2Len] = '\0';
            InAddr.u4Addr = V4_FROM_V6_ADDR (pRcvSeg->ip->DestAddr);
            TcpAoErrLog.InDAdr = &InAddr;
            TcpAoErrLog.i1AdType = TCP_AF_IPV4_TYPE;
            STRNCPY (au1DstAddr, INET_NTOA (InAddr), u2Len);
            au1DstAddr[u2Len] = '\0';
#ifdef SYSLOG_WANTED
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4TcpSysLogId,
                          "TCP-AO & TCP MD5 option in one segment from %s(%d) to %s(%d)",
                          au1SrcAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Sport),
                          au1DstAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Dport)));
#endif
        }
        TcpAoErrLog.u2SPort = OSIX_NTOHS (pRcvSeg->tcp->u2Sport);
        TcpAoErrLog.u2DPort = OSIX_NTOHS (pRcvSeg->tcp->u2Dport);
        TcpAoErrLog.u4TcpAoErCtr = GetTcpAoMacVerErrCtr (u4TcbIndex);

        nmhGetFsMITcpTrapAdminStatus ((INT4) u4Context, &i4Trap);
        if (i4Trap == TCP_TRAP_ENABLE)
        {
            TcpAoAuthNotifyEventTrap (&TcpAoErrLog, TCP_ONE);
        }

        return TCP_NOT_OK;
    }

    /* Check if TCP-AO option is expected */

    /*
     * If segment is for a passively opened connection,
     * interface with SLI to check if TCP-AO MKT is configured for peer 
     * on corresponding listen socket 
     */
    if (GetTCBtype (u4TcbIndex) == TCP_SERVER)
    {
        i4SockDesc = SliTcpFindSockDesc (u4TcbIndex);
        /*V6_NTOHL(&TempAddr,&(pRcvSeg->ip->SrcAddr)); */
        if (NULL !=
            (ptcpAoMktNd = SliGetTcpAo (i4SockDesc, pRcvSeg->ip->SrcAddr)))
        {
            u1KeyLen = (UINT1) (STRLEN ((char *) ptcpAoMktNd->TcpAoKey.au1Key));

            TcpGenerateTCPAOSynRcvTrafficKey ((pRcvSeg->ip->SrcAddr),
                                              (pRcvSeg->ip->DestAddr),
                                              (pRcvSeg->tcp->u2Sport),
                                              (pRcvSeg->tcp->u2Dport),
                                              ((UINT4) pRcvSeg->tcp->i4Seq),
                                              (ptcpAoMktNd->TcpAoKey.au1Key),
                                              u1KeyLen, au1Key);
            u1TcpOptIgn = ptcpAoMktNd->TcpAoKey.u1TcpOptIgnore;

            u1MacExpected = TRUE;

        }
    }
    else
    {
        /* Segment is part of a connected TCB. Check TCP-AO is enabled */
        if (GetTCBTcpAoEnabled (u4TcbIndex) != FALSE)
        {
            u1MacExpected = TRUE;
        }
    }

    /* If TCP-AO MAC is not expected return OK if incoming segment also 
     * does not contain it */
    if ((u1MacExpected == FALSE) && (pMacPos == NULL))
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from LINInpCheckTCPAOOption. Returned OK.\n");
        return TCP_OK;
    }

    if ((u1MacExpected == FALSE) && (pMacPos != NULL))
    {
        /* 
         * check configuration is done for discarding packets received with
         * TCP_AO option which doesn't have a matching MKT configuration
         */
        UINT1              *pu1Mktcfg = NULL;
        (GetTCBtype (u4TcbIndex) == TCP_SERVER) ?
            (pu1Mktcfg =
             SliGetTcpAoNghMktCfg (i4SockDesc,
                                   pRcvSeg->ip->SrcAddr)) : (pu1Mktcfg =
                                                             &
                                                             (GetTCBTcpAoNoMktDisc
                                                              (u4TcbIndex)));

        if ((pu1Mktcfg != NULL) && (*pu1Mktcfg) == TCP_TWO)
        {
            /* Silent Discard configured if no matching MKT found */
            IncrTcpAoMacErrCtr (u4TcbIndex);
            IncTCPstat (GetTCBContext (u4TcbIndex), InErrs);
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                         "TCP",
                         "Exit from LINInpCheckTCPAOOption. TCP-AO option expected but \
            segment does not have it. Returned NOT OK.\n");

            if (IsIP6Addr (pRcvSeg->ip->SrcAddr))
            {
                MEMCPY (&In6Addr.u1addr, &((pRcvSeg->ip->SrcAddr).u1_addr),
                        IPVX_IPV6_ADDR_LEN);
                In6SAddr = In6Addr;
                TcpAoErrLog.In6SAdr = &In6SAddr;
                u2Len =
                    (UINT2) (STRLEN (INET_NTOA6 (In6Addr)) <
                             sizeof (au1SrcAddr) ? STRLEN (INET_NTOA6 (In6Addr))
                             : sizeof (au1SrcAddr) - 1);
                STRNCPY (au1SrcAddr, INET_NTOA6 (In6Addr), u2Len);
                au1SrcAddr[u2Len] = '\0';
                MEMCPY (&In6Addr.u1addr, &((pRcvSeg->ip->DestAddr).u1_addr),
                        IPVX_IPV6_ADDR_LEN);
                TcpAoErrLog.In6DAdr = &In6Addr;
                TcpAoErrLog.i1AdType = TCP_AF_IPV6_TYPE;
                STRNCPY (au1DstAddr, INET_NTOA6 (In6Addr), u2Len);
                au1DstAddr[u2Len] = '\0';
#ifdef SYSLOG_WANTED
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4TcpSysLogId,
                              "TCP-AO MAC from %s(%d) to %s(%d) \
                               No MKT Match Discard Packet", au1SrcAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Sport), au1DstAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Dport)));
#endif
            }
            else
            {
                InAddr.u4Addr = V4_FROM_V6_ADDR (pRcvSeg->ip->SrcAddr);
                InSAddr = InAddr;
                TcpAoErrLog.InSAdr = &InSAddr;
                u2Len =
                    (UINT2) (STRLEN (INET_NTOA (InAddr)) <
                             sizeof (au1SrcAddr) ? STRLEN (INET_NTOA (InAddr)) :
                             sizeof (au1SrcAddr) - 1);
                STRNCPY (au1SrcAddr, INET_NTOA (InAddr), u2Len);
                au1SrcAddr[u2Len] = '\0';
                InAddr.u4Addr = V4_FROM_V6_ADDR (pRcvSeg->ip->DestAddr);
                TcpAoErrLog.InDAdr = &InAddr;
                TcpAoErrLog.i1AdType = TCP_AF_IPV4_TYPE;
                STRNCPY (au1DstAddr, INET_NTOA (InAddr), u2Len);
                au1DstAddr[u2Len] = '\0';
#ifdef SYSLOG_WANTED
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4TcpSysLogId,
                              "TCP-AO MAC from %s(%d) to %s(%d) \
                               No MKT Match Discard Packet", au1SrcAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Sport), au1DstAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Dport)));
#endif
            }

            TcpAoErrLog.u2SPort = OSIX_NTOHS (pRcvSeg->tcp->u2Sport);
            TcpAoErrLog.u2DPort = OSIX_NTOHS (pRcvSeg->tcp->u2Dport);

            nmhGetFsMITcpTrapAdminStatus ((INT4) u4Context, &i4Trap);
            TcpAoErrLog.u4TcpAoErCtr = GetTcpAoMacVerErrCtr (u4TcbIndex);

            if (i4Trap == TCP_TRAP_ENABLE)
            {
                TcpAoAuthNotifyEventTrap (&TcpAoErrLog, TCP_ONE);
            }
            return TCP_NOT_OK;

        }                        /* end of  if ((*pu1Mktcfg) == TCP_TWO) */

        /* If TCP-AO MAC is not expected but segment contains it return OK(silent accept) */
        IncrTcpAoSilentAccCtr (u4TcbIndex);
        IncTCPstat (GetTCBContext (u4TcbIndex), InErrs);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP",
                     "Exit from LINInpCheckTCPAOOption. TCP-AO option not expected \
            but segment has it. Returned OK.\n");
#ifdef SYSLOG_WANTED
        if (IsIP6Addr (pRcvSeg->ip->SrcAddr))
        {
            MEMCPY (&In6Addr.u1addr, &((pRcvSeg->ip->SrcAddr).u1_addr),
                    IPVX_IPV6_ADDR_LEN);
            u2Len =
                (UINT2) (STRLEN (INET_NTOA6 (In6Addr)) <
                         sizeof (au1SrcAddr) ? STRLEN (INET_NTOA6 (In6Addr)) :
                         sizeof (au1SrcAddr) - 1);
            STRNCPY (au1SrcAddr, INET_NTOA6 (In6Addr), u2Len);
            au1SrcAddr[u2Len] = '\0';
            MEMCPY (&In6Addr.u1addr, &((pRcvSeg->ip->DestAddr).u1_addr),
                    IPVX_IPV6_ADDR_LEN);
            STRNCPY (au1DstAddr, INET_NTOA6 (In6Addr), u2Len);
            au1DstAddr[u2Len] = '\0';
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4TcpSysLogId,
                          "TCP-AO not expected but found, silent accept from %s(%d) to %s(%d)",
                          au1SrcAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Sport),
                          au1DstAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Dport)));
        }
        else
        {
            InAddr.u4Addr = V4_FROM_V6_ADDR (pRcvSeg->ip->SrcAddr);
            u2Len = (UINT2) (STRLEN (INET_NTOA (InAddr)) < sizeof (au1SrcAddr) ?
                             STRLEN (INET_NTOA (InAddr)) : sizeof (au1SrcAddr) -
                             1);
            STRNCPY (au1SrcAddr, INET_NTOA (InAddr), u2Len);
            au1SrcAddr[u2Len] = '\0';
            InAddr.u4Addr = V4_FROM_V6_ADDR (pRcvSeg->ip->DestAddr);
            STRNCPY (au1DstAddr, INET_NTOA (InAddr), u2Len);
            au1DstAddr[u2Len] = '\0';
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4TcpSysLogId,
                          "TCP-AO not expected but found, silent accept from %s(%d) to %s(%d)",
                          au1SrcAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Sport),
                          au1DstAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Dport)));
        }
#endif

        return TCP_OK;

    }

    /* If TCP-AO MAC is expected but segment does not contain it return NOT OK */
    if ((u1MacExpected == TRUE) && (pMacPos == NULL))
    {
        IncrTcpAoMacErrCtr (u4TcbIndex);
        IncTCPstat (GetTCBContext (u4TcbIndex), InErrs);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP",
                     "Exit from LINInpCheckTCPAOOption. TCP-AO option expected but \
            segment does not have it. Returned NOT OK.\n");

        if (IsIP6Addr (pRcvSeg->ip->SrcAddr))
        {
            MEMCPY (&In6Addr.u1addr, &((pRcvSeg->ip->SrcAddr).u1_addr),
                    IPVX_IPV6_ADDR_LEN);
            In6SAddr = In6Addr;
            TcpAoErrLog.In6SAdr = &In6SAddr;
            u2Len =
                (UINT2) (STRLEN (INET_NTOA6 (In6Addr)) <
                         sizeof (au1SrcAddr) ? STRLEN (INET_NTOA6 (In6Addr)) :
                         sizeof (au1SrcAddr) - 1);
            STRNCPY (au1SrcAddr, INET_NTOA6 (In6Addr), u2Len);
            au1SrcAddr[u2Len] = '\0';
            MEMCPY (&In6Addr.u1addr, &((pRcvSeg->ip->DestAddr).u1_addr),
                    IPVX_IPV6_ADDR_LEN);
            TcpAoErrLog.In6DAdr = &In6Addr;
            TcpAoErrLog.i1AdType = TCP_AF_IPV6_TYPE;
            STRNCPY (au1DstAddr, INET_NTOA6 (In6Addr), u2Len);
            au1DstAddr[u2Len] = '\0';
#ifdef SYSLOG_WANTED
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4TcpSysLogId,
                          "No TCP-AO MAC from %s(%d) to %s(%d)",
                          au1SrcAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Sport),
                          au1DstAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Dport)));
#endif
        }
        else
        {
            InAddr.u4Addr = V4_FROM_V6_ADDR (pRcvSeg->ip->SrcAddr);
            InSAddr = InAddr;
            TcpAoErrLog.InSAdr = &InSAddr;
            u2Len = (UINT2) (STRLEN (INET_NTOA (InAddr)) < sizeof (au1SrcAddr) ?
                             STRLEN (INET_NTOA (InAddr)) : sizeof (au1SrcAddr) -
                             1);
            STRNCPY (au1SrcAddr, INET_NTOA (InAddr), u2Len);
            au1SrcAddr[u2Len] = '\0';
            InAddr.u4Addr = V4_FROM_V6_ADDR (pRcvSeg->ip->DestAddr);
            TcpAoErrLog.InDAdr = &InAddr;
            TcpAoErrLog.i1AdType = TCP_AF_IPV4_TYPE;
            STRNCPY (au1DstAddr, INET_NTOA (InAddr), u2Len);
            au1DstAddr[u2Len] = '\0';
#ifdef SYSLOG_WANTED
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4TcpSysLogId,
                          "No TCP-AO MAC from %s(%d) to %s(%d)",
                          au1SrcAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Sport),
                          au1DstAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Dport)));
#endif
        }

        TcpAoErrLog.u2SPort = OSIX_NTOHS (pRcvSeg->tcp->u2Sport);
        TcpAoErrLog.u2DPort = OSIX_NTOHS (pRcvSeg->tcp->u2Dport);

        nmhGetFsMITcpTrapAdminStatus ((INT4) u4Context, &i4Trap);
        TcpAoErrLog.u4TcpAoErCtr = GetTcpAoMacVerErrCtr (u4TcbIndex);

        if (i4Trap == TCP_TRAP_ENABLE)
        {
            TcpAoAuthNotifyEventTrap (&TcpAoErrLog, TCP_ONE);
        }
        return TCP_NOT_OK;
    }

    /* 
     * TCP-AO MAC is expected and segment contains it. Verify it
     * Get Rcv Key id from the segment and check a matching MKT
     * is there, if so get the traffic key and calculate the 
     * MAC. Comapre the calculated MAC with the MAC on the packet.
     */

    u1RcvKeyId = *pMacPos;
    u1SndKeyId = *(pMacPos + 1);
    pMacPos += TCP_TWO;            /* point to the TCP-AO MAC */

    if (GetTCBtype (u4TcbIndex) != TCP_SERVER)
    {
        u1Indx = GetMktIndexFromRcvId (u4TcbIndex, u1RcvKeyId);
        /* TCP-AO expected but MKT is not matching with received packet */
        if (u1Indx >= TCPAO_MKT_INI_IND)
        {
            IncrTcpAoMacErrCtr (u4TcbIndex);
            IncTCPstat (GetTCBContext (u4TcbIndex), InErrs);
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                         "TCP",
                         "Exit from LINInpCheckTCPAOOption. TCP-AO option expected but \
            MKT mismatch with received packet. Returned NOT OK.\n");

            if (IsIP6Addr (pRcvSeg->ip->SrcAddr))
            {
                MEMCPY (&In6Addr.u1addr, &((pRcvSeg->ip->SrcAddr).u1_addr),
                        IPVX_IPV6_ADDR_LEN);
                In6SAddr = In6Addr;
                TcpAoErrLog.In6SAdr = &In6SAddr;
                u2Len =
                    (UINT2) (STRLEN (INET_NTOA6 (In6Addr)) <
                             sizeof (au1SrcAddr) ? STRLEN (INET_NTOA6 (In6Addr))
                             : sizeof (au1SrcAddr) - 1);
                STRNCPY (au1SrcAddr, INET_NTOA6 (In6Addr), u2Len);
                au1SrcAddr[u2Len] = '\0';
                MEMCPY (&In6Addr.u1addr, &((pRcvSeg->ip->DestAddr).u1_addr),
                        IPVX_IPV6_ADDR_LEN);
                TcpAoErrLog.In6DAdr = &In6Addr;
                TcpAoErrLog.i1AdType = TCP_AF_IPV6_TYPE;
                STRNCPY (au1DstAddr, INET_NTOA6 (In6Addr), u2Len);
                au1DstAddr[u2Len] = '\0';
#ifdef SYSLOG_WANTED
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4TcpSysLogId,
                              "TCP-AO MAC MKT mismatch from %s(%d) to %s(%d)",
                              au1SrcAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Sport),
                              au1DstAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Dport)));
#endif
            }
            else
            {
                InAddr.u4Addr = V4_FROM_V6_ADDR (pRcvSeg->ip->SrcAddr);
                InSAddr = InAddr;
                TcpAoErrLog.InSAdr = &InSAddr;
                u2Len =
                    (UINT2) (STRLEN (INET_NTOA (InAddr)) <
                             sizeof (au1SrcAddr) ? STRLEN (INET_NTOA (InAddr)) :
                             sizeof (au1SrcAddr) - 1);
                STRNCPY (au1SrcAddr, INET_NTOA (InAddr), u2Len);
                au1SrcAddr[u2Len] = '\0';
                InAddr.u4Addr = V4_FROM_V6_ADDR (pRcvSeg->ip->DestAddr);
                TcpAoErrLog.InDAdr = &InAddr;
                TcpAoErrLog.i1AdType = TCP_AF_IPV4_TYPE;
                STRNCPY (au1DstAddr, INET_NTOA (InAddr), u2Len);
                au1DstAddr[u2Len] = '\0';
#ifdef SYSLOG_WANTED
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4TcpSysLogId,
                              "TCP-AO MAC MKT mismatch from %s(%d) to %s(%d)",
                              au1SrcAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Sport),
                              au1DstAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Dport)));
#endif
            }

            TcpAoErrLog.u2SPort = OSIX_NTOHS (pRcvSeg->tcp->u2Sport);
            TcpAoErrLog.u2DPort = OSIX_NTOHS (pRcvSeg->tcp->u2Dport);

            nmhGetFsMITcpTrapAdminStatus ((INT4) u4Context, &i4Trap);
            TcpAoErrLog.u4TcpAoErCtr = GetTcpAoMacVerErrCtr (u4TcbIndex);

            if (i4Trap == TCP_TRAP_ENABLE)
            {
                TcpAoAuthNotifyEventTrap (&TcpAoErrLog, TCP_ONE);
            }
            return TCP_NOT_OK;
        }
        u1TcpOptIgn = GetMktTcpOptIgn (u4TcbIndex, u1Indx);
        /* Received a packet on syn sent update Recv ISN & traffic keys */
        if (GetTCBstate (u4TcbIndex) == TCPS_SYNSENT)
        {
            SetRcvIsn (u4TcbIndex,
                       ((OSIX_NTOHL ((UINT4) pRcvSeg->tcp->i4Seq))));
            TcpGenerateTCPAOTrafficKey (u4TcbIndex,
                                        GetTCBTcpAoCurrKey (u4TcbIndex),
                                        TCPAO_SND_TRFKEY);
            TcpGenerateTCPAOTrafficKey (u4TcbIndex,
                                        GetTCBTcpAoCurrKey (u4TcbIndex),
                                        TCPAO_RCV_TRFKEY);
        }
        GetMktRcvTrfcKey (u4TcbIndex, u1Indx, au1Key);

    }                            /* end of if (GetTCBtype (u4TcbIndex) != TCP_SERVER) */

    if (TcpOptGenerateTCPAOMac (u4Context, pRcvSeg, pSeg, au1TcpAoMac,
                                au1Key, TCPAO_TRAFFIC_KEY_MAXLEN, u1TcpOptIgn,
                                GetTcpAoRecvSne (u4TcbIndex),
                                au1RxTcpAoBuffer) == TCP_OK)
    {

        if (MEMCMP (au1TcpAoMac, pMacPos, TCPAO_MAC_LEN) != TCP_ZERO)
        {
            IncrTcpAoMacErrCtr (u4TcbIndex);
            IncTCPstat (GetTCBContext (u4TcbIndex), InErrs);
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                         "TCP", "Exit from LINInpCheckTCPAOOption.\
                          TCP-AO MAC is incorrect. Returned NOT OK.\n");

            if (IsIP6Addr (pRcvSeg->ip->SrcAddr))
            {
                MEMCPY (&In6Addr.u1addr, &((pRcvSeg->ip->SrcAddr).u1_addr),
                        IPVX_IPV6_ADDR_LEN);
                In6SAddr = In6Addr;
                TcpAoErrLog.In6SAdr = &In6SAddr;
                u2Len =
                    (UINT2) (STRLEN (INET_NTOA6 (In6Addr)) <
                             sizeof (au1SrcAddr) ? STRLEN (INET_NTOA6 (In6Addr))
                             : sizeof (au1SrcAddr) - 1);
                STRNCPY (au1SrcAddr, INET_NTOA6 (In6Addr), u2Len);
                au1SrcAddr[u2Len] = '\0';
                MEMCPY (&In6Addr.u1addr, &((pRcvSeg->ip->DestAddr).u1_addr),
                        IPVX_IPV6_ADDR_LEN);
                TcpAoErrLog.In6DAdr = &In6Addr;
                TcpAoErrLog.i1AdType = TCP_AF_IPV6_TYPE;
                STRNCPY (au1DstAddr, INET_NTOA6 (In6Addr), u2Len);
                au1DstAddr[u2Len] = '\0';
#ifdef SYSLOG_WANTED
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4TcpSysLogId,
                              "Bad TCP-AO MAC from %s(%d) to %s(%d)",
                              au1SrcAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Sport),
                              au1DstAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Dport)));
#endif
            }
            else
            {
                InAddr.u4Addr = V4_FROM_V6_ADDR (pRcvSeg->ip->SrcAddr);
                InSAddr = InAddr;
                TcpAoErrLog.InSAdr = &InSAddr;
                u2Len =
                    (UINT2) (STRLEN (INET_NTOA (InAddr)) <
                             sizeof (au1SrcAddr) ? STRLEN (INET_NTOA (InAddr)) :
                             sizeof (au1SrcAddr) - 1);
                STRNCPY (au1SrcAddr, INET_NTOA (InAddr), u2Len);
                au1SrcAddr[u2Len] = '\0';
                InAddr.u4Addr = V4_FROM_V6_ADDR (pRcvSeg->ip->DestAddr);
                TcpAoErrLog.InDAdr = &InAddr;
                TcpAoErrLog.i1AdType = TCP_AF_IPV4_TYPE;
                STRNCPY (au1DstAddr, INET_NTOA (InAddr), u2Len);
                au1DstAddr[u2Len] = '\0';
#ifdef SYSLOG_WANTED
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4TcpSysLogId,
                              "Bad TCP-AO MAC from %s(%d) to %s(%d)",
                              au1SrcAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Sport),
                              au1DstAddr, OSIX_NTOHS (pRcvSeg->tcp->u2Dport)));
#endif
            }

            TcpAoErrLog.u2SPort = OSIX_NTOHS (pRcvSeg->tcp->u2Sport);
            TcpAoErrLog.u2DPort = OSIX_NTOHS (pRcvSeg->tcp->u2Dport);

            nmhGetFsMITcpTrapAdminStatus ((INT4) u4Context, &i4Trap);
            TcpAoErrLog.u4TcpAoErCtr = GetTcpAoMacVerErrCtr (u4TcbIndex);

            if (i4Trap == TCP_TRAP_ENABLE)
            {
                TcpAoAuthNotifyEventTrap (&TcpAoErrLog, TCP_ONE);
            }
            return TCP_NOT_OK;
        }

    }
    else
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP",
                     "Exit from LINInpCheckTCPAOOption. TCP-AO MAC generation error.\
            Returned NOT OK.\n");
        return TCP_NOT_OK;
    }

    if (GetTCBtype (u4TcbIndex) != TCP_SERVER)
    {
        /* 
         * If MKT corresponding to  RNextKeyId is present 
         * make it as the active MKT.
         */
        SetMktNewIndToActiveIfReq (u4TcbIndex, u1SndKeyId);
        /* Update Latest Received packet RNextKeyId & KeyId */
        SetTCBTcpAoRcvPckKeyId (u4TcbIndex, u1RcvKeyId);
        SetTCBTcpAoRcvPckRNxtKeyId (u4TcbIndex, u1SndKeyId);
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from LINInpCheckTCPAOOption. Returned OK.\n");
    return TCP_OK;
}
