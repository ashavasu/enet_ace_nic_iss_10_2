/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: tcptsttrace.h,v 1.1 2013/01/23 12:00:17 siva Exp $
*
* Description: Trace macros for TCP TEST Module
*********************************************************************/
#ifndef __TCPTSTTRACE_H__
#define __TCPTSTTRACE_H__


 /* Common bitmasks for various events. */
#define  TCPTST_INIT_SHUT_TRC   INIT_SHUT_TRC     
#define  TCPTST_MGMT_TRC        MGMT_TRC          
#define  TCPTST_DATA_PATH_TRC   DATA_PATH_TRC     
#define  TCPTST_CTRL_PLANE_TRC  CONTROL_PLANE_TRC 
#define  TCPTST_DUMP_TRC        DUMP_TRC          
#define  TCPTST_OS_RES_TRC      OS_RESOURCE_TRC   
#define  TCPTST_ALL_FAIL_TRC    ALL_FAILURE_TRC   
#define  TCPTST_BUF_TRC         BUFFER_TRC        
#define  TCPTST_ENTRY_EXIT_TRC  0x00000100        
#define  TCPTST_FSM_TRC         0x00000200        
#define  TCPTST_OSM_TRC         0x00000400        
#define  TCPTST_DBG_MAP		gu4TcpTstDbgMap
#define  TCPTSTMOD		"TCPTST"

 /* General Macros */
#define  TCPTST_MOD_FN_ENTRY  	MOD_FN_ENTRY
#define  TCPTST_MOD_FN_EXIT 	MOD_FN_EXIT
#define  TCPTST_PKT_DUMP      	MOD_PKT_DUMP 
#define  TCPTST_MOD_TRC      	MOD_TRC      
#define  TCPTST_MOD_TRC_ARG1  	MOD_TRC_ARG1 
#define  TCPTST_MOD_TRC_ARG2  	MOD_TRC_ARG2 
#define  TCPTST_MOD_TRC_ARG3  	MOD_TRC_ARG3 
#define  TCPTST_MOD_TRC_ARG4  	MOD_TRC_ARG4 
#define  TCPTST_MOD_TRC_ARG5  	MOD_TRC_ARG5 
#define  TCPTST_MOD_TRC_ARG6  	MOD_TRC_ARG6 

#define TCPTST_FN_ENTRY() \
        if (TCPTST_DBG_MAP & TCPTST_ENTRY_EXIT_TRC) \
{\
        TCPTST_MOD_FN_ENTRY (TCPTST_DBG_MAP, TCPTST_ENTRY_EXIT_TRC, \
		                  (const CHR1 *) TCPTSTMOD)\
}

#define TCPTST_FN_EXIT() \
        if (TCPTST_DBG_MAP & TCPTST_ENTRY_EXIT_TRC) \
{\
        TCPTST_MOD_FN_EXIT (TCPTST_DBG_MAP, TCPTST_ENTRY_EXIT_TRC, \
		                  (const CHR1 *) TCPTSTMOD)\
}

#endif /* _TCPTSTTRACE_H_ */

