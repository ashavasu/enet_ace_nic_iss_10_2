/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: tcpsnmp.h,v 1.5 2013/01/23 11:16:17 siva Exp $
 *
 **********************************************************************/
/* FILE HEADER
 *
 * ----------------------------------------------------------------------------
 *  FILE NAME             : tcpsnmp.h
 * 
 *  PRINCIPAL AUTHOR      : Ramesh Kumar
 *
 *  SUBSYSTEM NAME        : TCP
 *
 *  MODULE NAME           : High Level Interface Module
 *
 *  LANGUAGE              : C
 *
 *  TARGET ENVIRONMENT    : Any
 *
 *  DATE OF FIRST RELEASE :
 * 
 *  DESCRIPTION           : This file contains TCP Stats data structure ,
 *                          values for SNMP mib variables and macros to
 *                          access them.
 * ---------------------------------------------------------------------------
 *
 *
 *  CHANGE RECORD :
 *
 * ---------------------------------------------------------------------------
 *   VERSION        AUTHOR/DATE           DESCRIPTION OF CHANGE
 * ---------------------------------------------------------------------------
 *     1.1          RSR                   Included snmcdefn.h
 *
 * ------------------------------------------------------------------------*/
/*     1.2      Saravanan.M    Implementation of                           */
/*              Purush            - Non Blocking Connect                   */ 
/*                                - Non Blocking Accept                    */
/*                                - IfMsg Interface between TCP and IP     */
/*                                - One Queue per Socket                   */
/* ------------------------------------------------------------------------*/
#ifndef  __TCP_SNMP_H__
#define  __TCP_SNMP_H__

#include   "snmcdefn.h"


#define  MAX_UINT4_VAL       0xFFFFFFFFu                            
#define  VAN_JACOBSON        4                                      
#define  RFC2988             5
#define  GetTCPstat(u4Context,Scalar)  gpTcpContext[u4Context]->TcpStatParms.u4 ## Scalar         
#define  IncTCPstat(u4Context,Scalar)  { gpTcpContext[u4Context]->TcpStatParms.u4 ## Scalar ++; } 

#define  GetTCPHCstat(u4Context,Scalar)  gpTcpContext[u4Context]->TcpStatParms.Scalar
#define  IncTCPHCstat(u4Context,Scalar)  FSAP_U8_INC (&gpTcpContext[u4Context]->TcpStatParms.Scalar)


#define  GetConnState (u4LocalAddress, u4LocalPort, u4RemoteAddress, \
                       u4RemotePort) \
         GetTCBstate  (tcpSnmpGetConnID (u4LocalAddress, u4LocalPort, \
                                           u4RemoteAddress, u4RemotePort))

#define  GetConnOutState (u4LocalAddress, u4LocalPort, u4RemoteAddress, \
                          u4RemotePort) \
         GetTCBostate    (tcpSnmpGetConnID (u4LocalAddress, u4LocalPort, \
                                              u4RemoteAddress, u4RemotePort))

#define  GetConnSwindow (u4LocalAddress, u4LocalPort, u4RemoteAddress, \
                         u4RemotePort) \
         GetTCBswindow  (tcpSnmpGetConnID (u4LocalAddress, u4LocalPort, \
                                            u4RemoteAddress, u4RemotePort))

#define  GetConnCwindow (u4LocalAddress, u4LocalPort, u4RemoteAddress, \
                         u4RemotePort) \
         GetTCBcwnd     (tcpSnmpGetConnID (u4LocalAddress, u4LocalPort, \
                                             u4RemoteAddress, u4RemotePort))

#define  GetConnSSthresh (u4LocalAddress, u4LocalPort, u4RemoteAddress, \
                          u4RemotePort) \
         GetTCBssthresh  (tcpSnmpGetConnID (u4LocalAddress, u4LocalPort, \
                                              u4RemoteAddress, u4RemotePort))

#define  GetConnSMSS (u4LocalAddress, u4LocalPort, u4RemoteAddress, \
                      u4RemotePort) \
         GetTCBsmss  (tcpSnmpGetConnID (u4LocalAddress, u4LocalPort, \
                                          u4RemoteAddress, u4RemotePort))

#define  GetConnRMSS (u4LocalAddress, u4LocalPort, u4RemoteAddress, \
                      u4RemotePort) \
         GetTCBrmss  (tcpSnmpGetConnID (u4LocalAddress, u4LocalPort, \
                                          u4RemoteAddress, u4RemotePort))

#define  GetConnSRT (u4LocalAddress, u4LocalPort, u4RemoteAddress, \
                     u4RemotePort) \
         GetTCBsrt  (tcpSnmpGetConnID (u4LocalAddress, u4LocalPort, \
                                         u4RemoteAddress, u4RemotePort))

#define  GetConnRTDE (u4LocalAddress, u4LocalPort, u4RemoteAddress, \
                      u4RemotePort) \
         GetTCBrtde  (tcpSnmpGetConnID (u4LocalAddress, u4LocalPort, \
                                          u4RemoteAddress, u4RemotePort))

#define  GetConnPersist (u4LocalAddress, u4LocalPort, u4RemoteAddress, \
                         u4RemotePort) \
         GetTCBpersist  (tcpSnmpGetConnID (u4LocalAddress, u4LocalPort, \
                                             u4RemoteAddress, u4RemotePort))

#define  GetConnRexmt (u4LocalAddress, u4LocalPort, u4RemoteAddress, \
                       u4RemotePort) \
         GetTCBrexmt  (tcpSnmpGetConnID (u4LocalAddress, u4LocalPort, \
                                           u4RemoteAddress, u4RemotePort))

#define  GetConnRexmtCnt  (u4LocalAddress, u4LocalPort, u4RemoteAddress, \
                           u4RemotePort) \
         GetTCBrexmtcount (tcpSnmpGetConnID (u4LocalAddress, u4LocalPort, \
                                               u4RemoteAddress, u4RemotePort))

#define  GetConnSBCount (u4LocalAddress, u4LocalPort, u4RemoteAddress, \
                         u4RemotePort) \
         GetTCBsbcount  (tcpSnmpGetConnID (u4LocalAddress, u4LocalPort, \
                                             u4RemoteAddress, u4RemotePort))

#define  GetConnSBSize (u4LocalAddress, u4LocalPort, u4RemoteAddress, \
                        u4RemotePort) \
         GetTCBsbsize  (tcpSnmpGetConnID (u4LocalAddress, u4LocalPort, \
                                            u4RemoteAddress, u4RemotePort))

#define  GetConnRBCount (u4LocalAddress, u4LocalPort, u4RemoteAddress, \
                         u4RemotePort) \
         GetTCBrbcount  (tcpSnmpGetConnID (u4LocalAddress, u4LocalPort, \
                                             u4RemoteAddress, u4RemotePort))

#define  GetConnRBSize (u4LocalAddress, u4LocalPort, u4RemoteAddress, \
                        u4RemotePort) \
         GetTCBrbsize  (tcpSnmpGetConnID (u4LocalAddress, u4LocalPort, \
                                            u4RemoteAddress, u4RemotePort))


/* constants used by tcplow.c */
#define  TCP_EQUAL        (0) 
#define  TCP_LESS         (1) 
#define  TCP_GREATER      (2) 

#endif   /*TCP_SNMP_H*/

