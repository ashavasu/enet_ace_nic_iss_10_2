/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tcpsz.c,v 1.4 2013/11/29 11:04:15 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _TCPSZ_C
#include "tcpinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
TcpSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < TCP_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = MemCreateMemPool (FsTCPSizingParams[i4SizingId].u4StructSize,
                                     FsTCPSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(TCPMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            TcpSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
TcpSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsTCPSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, TCPMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
TcpSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < TCP_MAX_SIZING_ID; i4SizingId++)
    {
        if (TCPMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (TCPMemPoolIds[i4SizingId]);
            TCPMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
