#!/bin/csh
##########################################################
#  Copyright (C) 2012 Aricent Inc . All Rights Reserved
#
#  $Id: make.h,v 1.6 2013/01/23 11:46:02 siva Exp $
#
##########################################################
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : MAKE.H                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                     |
# |                                                                          |
# |   MAKE TOOL(S) USED      : None                                          |
# |                                                                          |
# |   TARGET ENVIRONMENT     : Any                                           | 
# |                                                                          |
# |   DATE                   : 25th August 2000                              |
# |                                                                          |
# |   DESCRIPTION            : It specifies the environvent variables for    |
# |                            compiling FutureTCP
# +--------------------------------------------------------------------------+
#
#     CHANGE RECORD :
# +--------------------------------------------------------------------------+
# | VERSION | AUTHOR/    | DESCRIPTION OF CHANGE                             |
# |         | DATE       |                                                   |
# +---------|------------|---------------------------------------------------+
# |   1     | RSR        | Creation of file                                  |
# |         | 25/08/2000 |                                                   |
# +--------------------------------------------------------------------------+

# Top level make include files
# ----------------------------

include ../LR/make.h
include ../LR/make.rule

# Compilation switches
# --------------------
TCP_FINAL_COMPILATION_SWITCHES =${GENERAL_COMPILATION_SWITCHES} \
    ${SYSTEM_COMPILATION_SWITCHES} 
# Directories
# -----------
TCP_BASE_DIR     = ${BASE_DIR}/tcp
SLI_BASE_DIR     = ${BASE_DIR}/sli
TCP_OBJ_DIR      = ${TCP_BASE_DIR}
TCP_SRC_DIR      = ${TCP_BASE_DIR}
TCP_INC_DIR      = ${TCP_BASE_DIR}
SLI_INC_DIR      = ${SLI_BASE_DIR}
TCPIPVX_BASE_DIR = ${BASE_DIR}/netipvx/tcp
TCPIPVX_INC_DIR  = ${TCPIPVX_BASE_DIR}/inc

# Include files
# -------------

TCP_INCLUDE_FILES = ${TCP_INC_DIR}/tcpdefn.h \
  ${TCP_INC_DIR}/tcptcb.h \
  ${TCP_INC_DIR}/tcptdfs.h \
  ${TCP_INC_DIR}/tcphdr.h \
  ${TCP_INC_DIR}/tcpfsm.h \
  ${TCP_INC_DIR}/tcposm.h \
         ${TCP_INC_DIR}/tcptmr.h \
  ${TCP_INC_DIR}/tcpsnmp.h

ifeq (${SNMP}, YES)
TCP_INCLUDE_FILES += ${SLI_INC_DIR}/slitdfs.h
endif

TCP_FINAL_INCLUDE_FILES = ${TCP_INCLUDE_FILES}

# Include directories
# -------------------

TCP_INCLUDE_DIRS = -I${TCP_INC_DIR} \
                   -I${SLI_INC_DIR} \
     -I${TCPIPVX_INC_DIR}

TCP_FINAL_INCLUDE_DIRS = ${TCP_INCLUDE_DIRS} \
                         ${COMMON_INCLUDE_DIRS}


# Project dependencies
# --------------------

TCP_DEPENDENCIES = $(COMMON_DEPENDENCIES) \
                   $(TCP_FINAL_INCLUDE_FILES) \
                   $(TCP_BASE_DIR)/Makefile \
                   $(TCP_BASE_DIR)/make.h

# -----------------------------  END OF FILE  -------------------------------
