/*##########################################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved                   #
# ----------------------------------------------------                   #
# $Id: tcphli.c,v 1.6 2013/03/08 13:30:11 siva Exp $                    #
##########################################################################
*/
/* SOURCE FILE HEADER
 *
 * ----------------------------------------------------------------------------
 *  FILE NAME             : tcphli.c
 * 
 *  PRINCIPAL AUTHOR      : Ramesh Kumar
 *
 *  SUBSYSTEM NAME        : TCP
 *
 *  MODULE NAME           : High Level Interface Module
 *
 *  LANGUAGE              : C
 *
 *  TARGET ENVIRONMENT    : Any
 *
 *  DATE OF FIRST RELEASE :
 * 
 *  DESCRIPTION           : This file contains routines that interprets requests
 *                          from the application.
 *
 * ---------------------------------------------------------------------------
 *
 *
 *  CHANGE RECORD :
 *
 * ---------------------------------------------------------------------------
 *   VERSION        AUTHOR/DATE           DESCRIPTION OF CHANGE
 * ---------------------------------------------------------------------------
 *    1.6           Rebecca Rufus         Removed the SLI_TCP_PROC_IFACE_WANTED
 *                                        switch from HliHandleCmd,and the
 *                                        prototype of this function was 
 *                                        made common for both the Blocking
 *                                        and Non-Blocking Mode.
 *     -            Ramakrishnan 
 *                  1 march 2000          Added support for select call under
 *                                        the ST_ENHLISTINTF switch.
* ------------------------------------------------------------------------*
*     1.8      Saravanan.M    Implementation of                           *
*              Purush            - Non Blocking Connect                   * 
*                                - Non Blocking Accept                    *
*                                - IfMsg Interface between TCP and IP     *
*                                - One Queue per Socket                   *
* ------------------------------------------------------------------------*/

#include "tcpinc.h"

/*************************************************************************/
/*  Function Name   : pHliError                                          */
/*  Input(s)        : pParms - pointer to App to Tcp params              */
/*                    i1ErrCode - Error Code                             */
/*                    u4TcbIndex - Index to TCB table                    */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK/SYSERR                                      */
/*  Description     : Return error message to the application.           */
/* CALLING CONDITION : When a error occurs.                              */
/* GLOBAL VARIABLES AFFECTED : None.                                     */
/*************************************************************************/
tTcpToApplicationParms *
pHliError (tApplicationToTcpParms * pParms, INT1 i1ErrCode, UINT4 u4TcbIndex)
{
    tTcpToApplicationParms *pRetParms = NULL;
    UINT4               u4Context;

    u4Context = GetTCBContext (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP", "Entering pHliError.\n");
    if ((pRetParms = ALLOCATE_TCP_TO_APPLICATION_PARMS ()) == NULL)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC |
                     TCP_ALL_FAIL_TRC | TCP_BUF_TRC,
                     "TCP",
                     "Exit from pHliError. Allocation of memory for tcp to app parms failed. Returned ERR.\n");

        return (tTcpToApplicationParms *) NULL;
    }

    pRetParms->u1Cmd = ERROR_MSG;
    pRetParms->u4ConnId = u4TcbIndex;
    pRetParms->cmdtype.error.u1ErrorCmd = pParms->u1Cmd;
    pRetParms->cmdtype.error.i1ErrorCode = i1ErrCode;
    SetTCBerror (u4TcbIndex, i1ErrCode);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from pHliError. Returned OK\n");
    return (pRetParms);
}

/*************************************************************************/
/*  Function Name   : HliError                                           */
/*  Input(s)        : pParms - App to Tcp params                         */
/*                    i1ErrCode - Error Code                             */
/*                    u4TcbIndex - Index to TCB table                    */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK/SYSERR                                      */
/*  Description     : Return error message to the application.           */
/* CALLING CONDITION : When a error occurs.                              */
/* GLOBAL VARIABLES AFFECTED : None                                      */
/*************************************************************************/
INT1
HliError (tApplicationToTcpParms * pParms, INT1 i1ErrCode, UINT4 u4TcbIndex)
{
    tTcpToApplicationParms *pRetParms = NULL;
    UINT4               u4Context;
    INT1                i1RetVal = TCP_ZERO;

    u4Context = GetTCBContext (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP", "Entering HliError.\n");

    if ((pRetParms = ALLOCATE_TCP_TO_APPLICATION_PARMS ()) == NULL)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC |
                     TCP_ALL_FAIL_TRC | TCP_BUF_TRC,
                     "TCP",
                     "Exit from HliError. Allocation of memory for tcp to app parms failed. Returned ERR.\n");
        return ERR;
    }

    pRetParms->u1Cmd = ERROR_MSG;
    pRetParms->u4ConnId = u4TcbIndex;
    pRetParms->cmdtype.error.u1ErrorCmd = pParms->u1Cmd;
    pRetParms->cmdtype.error.i1ErrorCode = i1ErrCode;
    SetTCBerror (u4TcbIndex, i1ErrCode);

#ifdef SLI_WANTED
    if (pParms->u1Cmd == OPEN_CMD)
    {
        i1RetVal = SliReply (pRetParms);
        if ((i1RetVal == FAILURE) && (pRetParms != NULL))
        {
            FREE_TCP_TO_APPLICATION_PARMS (pRetParms);
        }

    }
    else
#endif
    {
        FREE_TCP_TO_APPLICATION_PARMS (pRetParms);
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from HliError. Returned %d\n", i1RetVal);
    return i1RetVal;
}

/*************************************************************************/
/*  Function Name   : HliSendError                                       */
/*  Input(s)        : u4TcbIndex - Index to TCB table                    */
/*  Output(s)       : None                                               */
/*  Returns         : TCP_OK/SYSERR                                      */
/*  Description     : Return error message to the application. The       */
/*                    error code is obtained from the TCB structure.     */
/* CALLING CONDITION : When a error occurs.                              */
/* GLOBAL VARIABLES AFFECTED None.                                       */
/*************************************************************************/
INT1
HliSendError (UINT4 u4TcbIndex)
{
    tTcpToApplicationParms *pRetParms = NULL;
    UINT4               u4Context;
    INT1                i1RetVal = TCP_ZERO;

    u4Context = GetTCBContext (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering HliSendError.\n");
    if ((pRetParms = ALLOCATE_TCP_TO_APPLICATION_PARMS ()) == NULL)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC |
                     TCP_ALL_FAIL_TRC | TCP_BUF_TRC, "TCP",
                     "Exit from HliSendError. Allocation of memory for tcp to app parms failed. Returned ERR.\n");
        return ERR;
    }
    pRetParms->u1Cmd = ERROR_MSG;
    pRetParms->u4ConnId = u4TcbIndex;
    pRetParms->cmdtype.error.u1ErrorCmd = GetTCBhlicmd (u4TcbIndex);
    pRetParms->cmdtype.error.i1ErrorCode = GetTCBerror (u4TcbIndex);

#ifdef SLI_WANTED
    if (pRetParms->cmdtype.error.u1ErrorCmd == OPEN_CMD)
    {
        i1RetVal = SliReply (pRetParms);
        if ((i1RetVal == FAILURE) && (pRetParms != NULL))
        {
            FREE_TCP_TO_APPLICATION_PARMS (pRetParms);
        }
    }
    else
#endif
        FREE_TCP_TO_APPLICATION_PARMS (pRetParms);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from HliSendError. Returned %d\n", i1RetVal);
    return i1RetVal;
}
