/*******************************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved                       *
 *                                                                             *
 *  $Id: tcphlifn.c,v 1.26 2017/06/13 13:17:17 siva Exp $                      *
 *                                                                             *
 *   Description: This file contains procedures related to the HLI.            *
 *                                                                             *
 * *****************************************************************************/

/* SOURCE FILE HEADER
 *
 * ----------------------------------------------------------------------------
 *  FILE NAME             : tcphlifn.c
 * 
 *  PRINCIPAL AUTHOR      : Ramesh Kumar
 *
 *  SUBSYSTEM NAME        : TCP
 *
 *  MODULE NAME           : High Level Interface Module
 *
 *  LANGUAGE              : C
 *
 *  TARGET ENVIRONMENT    : Any
 *
 *  DATE OF FIRST RELEASE :
 * 
 *  DESCRIPTION           : This file contains procedures related to the 
 *                          HLI.
 *
 * ---------------------------------------------------------------------------
 *
 *
 *  CHANGE RECORD :
 *
 * ---------------------------------------------------------------------------
 *   VERSION        AUTHOR/DATE           DESCRIPTION OF CHANGE
 * ---------------------------------------------------------------------------
 *
 *
 * ---------------------------------------------------------------------------
 */

#include <stdlib.h>
#include "tcpinc.h"
extern INT4         SliCheckSocketConnection (UINT4 u4ConnId);
#ifndef __UTILRAND__
#define __UTILRAND__
extern INT4         UtilRand (VOID);
#endif
/*************************************************************************/
/*  Function Name   : tcballoc                                           */
/*  Input(s)        : u4Context - Context Id                             */
/*  Output(s)       : None.                                              */
/*  Returns         : pointer to the newly allocated TCB                 */
/*  Description     : Allocate TCB for a new connection                  */
/* CALLING CONDITION : Whenever a new connection request comes up.       */
/* GLOBAL VARIABLES AFFECTED TCBtable.                                   */
/*************************************************************************/
tTcb               *
tcballoc (UINT4 u4Context)
{
    UINT4               u4Index = TCP_ZERO;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP", "Entering tcballoc.\n");

    OsixSemTake (gTcbAllocSemId);
    for (u4Index = INIT_COUNT; u4Index < MAX_NUM_OF_TCB; u4Index++)
    {
        if ((GetTCBstate (u4Index) == TCPS_FREE)
            && (FALSE == SliCheckSocketConnection (u4Index)))
        {
            break;
        }
    }
    if (u4Index < MAX_NUM_OF_TCB)
    {
        SetTCBstate (u4Index, TCPS_CLOSED);
        TCP_MOD_TRC (u4Context, TCP_FSM_TRC, "TCP",
                     "FSM state set to CLOSED for CONN ID - %d\n", u4Index);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from tcballoc. Returned %x\n", &TCBtable[u4Index]);
        OsixSemGive (gTcbAllocSemId);
        return &TCBtable[u4Index];
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                 "Exit from tcballoc. Returned NULL\n");
    OsixSemGive (gTcbAllocSemId);
    return (tTcb *) NULL;
}

/*************************************************************************/
/*  Function Name   : deallocate                                         */
/*  Input(s)        : ptcb - Pointer to Tcb                              */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS                                            */
/*  Description     : Remove the TCB for the connection                  */
/* CALLING CONDITION : When a user requests for a close operation        */
/* GLOBAL VARIABLES AFFECTED TCBtable.                                   */
/*************************************************************************/
INT1
deallocate (tTcb * ptcb)
{
    UINT4               u4TcbIndex;
    tRxmt              *pTemp = NULL, *pPrev = NULL;
    tFrag              *pTempFrag = NULL, *pPrevFrag = NULL;
    tSack              *pTempSack = NULL, *pPrevSack = NULL;
    tOsixSemId          MainSemId = NULL;

    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering deallocate.\n");

    OsixSemTake (gTcbAllocSemId);

    u4TcbIndex = (UINT4) GET_TCB_INDEX (ptcb);
    TmrKillTimers (ptcb);        /*Kill all timers */
    TCP_MOD_TRC (ptcb->u4Context, TCP_OS_RES_TRC, "TCP",
                 "Stopped all the timers.\n");
    SetTCBstate (u4TcbIndex, TCPS_FREE);
    TCP_MOD_TRC (ptcb->u4Context, TCP_FSM_TRC, "TCP",
                 "FSM state changed to FREE for CONN ID - %d\n", u4TcbIndex);

    if (ptcb->pTcbRcvbuf)
        MemReleaseMemBlock (gTcbRcvBufPoolId, (UINT1 *) (ptcb->pTcbRcvbuf));
    ptcb->pTcbRcvbuf = NULL;

    if (ptcb->pTcbSndbuf)
        MemReleaseMemBlock (gTcbSendBufPoolId, (UINT1 *) (ptcb->pTcbSndbuf));
    ptcb->pTcbSndbuf = NULL;

    /* Drain out RXMT queue */
    pTemp = ptcb->pTcbRxmtQHdr;
    while (pTemp != NULL)
    {
        pPrev = pTemp;
        pTemp = (tRxmt *) pTemp->pNext;
        MemReleaseMemBlock (gTcpRxmtPoolId, (UINT1 *) pPrev);
    }

    /* Drain out SACK hdr */
    pTempFrag = ptcb->pTcbFraghdr;
    while (pTempFrag != NULL)
    {
        pPrevFrag = pTempFrag;
        pTempFrag = (tFrag *) pTempFrag->pNext;
        MemReleaseMemBlock (gTcpFragPoolId, (UINT1 *) pPrevFrag);
    }

    /* drain out Frag header queue */
    pTempSack = ptcb->pTcbSackHdr;
    while (pTempSack != NULL)
    {
        pPrevSack = pTempSack;
        pTempSack = (tSack *) pTempSack->pNext;
        MemReleaseMemBlock (gTcpSackPoolId, (UINT1 *) pPrevSack);
    }

    if (TCBtable[u4TcbIndex].BlockSemId != ZERO)
    {
        GiveTCBBlockSem (u4TcbIndex);
        OsixSemDel (TCBtable[u4TcbIndex].BlockSemId);
        TCBtable[u4TcbIndex].BlockSemId = ZERO;
    }
    if (TCBtable[u4TcbIndex].MainSemId != ZERO)
    {
        MainSemId = TCBtable[u4TcbIndex].MainSemId;
        TCBtable[u4TcbIndex].MainSemId = ZERO;
        OsixSemGive (MainSemId);
        OsixSemDel (MainSemId);
    }
    MEMSET (ptcb, ZERO, sizeof (tTcb));
    ptcb->i4SockDesc = -1;

    OsixSemGive (gTcbAllocSemId);
    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from deallocate. Returned SUCCESS.\n");
    return SUCCESS;
}

/*************************************************************************/
/*  Function Name   : TcpSync                                            */
/*  Input(s)        : ptcb - Pointer to Tcb                              */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK                                             */
/*  Description     : This function initialises fields in TCB structure  */
/*                    for connection.  It calls tcbiss() to get new      */
/*                  initial seq. no.It allocates send and receive buffer */
/* CALLING CONDITION : This function is called when a connection is      */
/*                    established and made active.                       */
/* GLOBAL VARIABLES AFFECTED TCBtable.                                   */
/*************************************************************************/
INT1
TcpSync (tTcb * ptcb, UINT4 u4RcvBufLen)
{
    UINT4               u4TcbIndex;
    UINT4               u4Context;

    u4TcbIndex = (UINT4) GET_TCB_INDEX (ptcb);
    u4Context = GetTCBContext (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP", "Entering TcpSync. \n");
    /*initialise TCB for new connection request */

    SetTCBiss (u4TcbIndex, (INT4) tcpiss ());
    SetTCBsuna (u4TcbIndex, GetTCBiss (u4TcbIndex));
    SetTCBsnext (u4TcbIndex, GetTCBiss (u4TcbIndex));
    SetTCBlwack (u4TcbIndex, GetTCBiss (u4TcbIndex));

    SetTCBsbsize (u4TcbIndex, u4RcvBufLen);    /* Send Buffer */
    if ((ptcb->pTcbSndbuf = MemAllocMemBlk (gTcbSendBufPoolId)) == NULL)
        return TCP_NOT_OK;
    SetTCBsbstart (u4TcbIndex, ZERO);
    SetTCBsbcount (u4TcbIndex, ZERO);

    SetTCBrbsize (u4TcbIndex, u4RcvBufLen);    /* Receive Buffer */
    if ((ptcb->pTcbRcvbuf = MemAllocMemBlk (gTcbRcvBufPoolId)) == NULL)
        return TCP_NOT_OK;
    SetTCBrbstart (u4TcbIndex, ZERO);
    SetTCBrbcount (u4TcbIndex, ZERO);
    /* Recv Window scale is initalized to 0 (No Window scaling)
     * Only when we receive a SYN from the other end with
     * window scale option set, we set the Window scale to the
     * approptiate value */
    SetTCBRecvWindowScale (u4TcbIndex, ZERO);

    /*timer stuff */
    SetTCBsrt (u4TcbIndex, ZERO);
    SetTCBrtde (u4TcbIndex, ZERO);
    if (gpTcpContext[u4Context]->TcpStatParms.u4RtoAlgorithm == RFC2988)
    {
        /*Supposed to be 3 sec's as per RFC-2988 */
        SetTCBrexmt (u4TcbIndex, TCP_RETRANSMIT_RFC2988);
    }
    else
    {
        SetTCBrexmt (u4TcbIndex, TCP_MINRETRANSMIT);    /* supposed to be 0.5 sec */
    }
    SetTCBrexmtcount (u4TcbIndex, ZERO);

    SetTCBcode (u4TcbIndex, TCPF_SYN);
    ptcb->u2TcbFlags = ZERO;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from TcpSync. Returned OK.\n");
    return (TCP_OK);
}

/*************************************************************************/
/*  Function Name   : TcpInitiateConnection                              */
/*  Input(s)        : ptcb - Pointer to Tcb                              */
/*                    pOpen - Pointer to tOpen params                    */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK                                             */
/*  Description     : Sets up flags and other fields in TCB.  It also    */
/*                 invokes the OSM for sending the first segment (SYN).  */
/* CALLING CONDITION : Called only when Active open is done.             */
/* GLOBAL VARIABLES AFFECTED  TCBtable                                   */
/*************************************************************************/
INT1
TcpInitiateConnection (tTcb * ptcb, tOpen * pOpen)
{
    UINT2               u2Mss;
    UINT4               u4TcbIndex;

    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TcpInitiateConnection. \n");
    UNUSED_PARAM (pOpen);

    u4TcbIndex = (UINT4) GET_TCB_INDEX (ptcb);

    u2Mss = TCP_MAX_SEGMENT_SIZE;

    SetTCBsmss (u4TcbIndex, u2Mss);
    SetTCBrmss (u4TcbIndex, TCP_DEFAULT_RMSS);
    SetTCBswindow (u4TcbIndex, GetTCBsmss (u4TcbIndex));
    SetTCBMaxswnd (u4TcbIndex, (GetTCBsmss (u4TcbIndex)));
    SetTCBcwnd (u4TcbIndex, GetTCBsmss (u4TcbIndex));
    SetTCBssthresh (u4TcbIndex, INIT_SSTHRESH);
    SetTCBrnext (u4TcbIndex, ZERO);
    SetTCBfinseq (u4TcbIndex, ZERO);
    SetTCBpushseq (u4TcbIndex, ZERO);
    SetTCBflags (u4TcbIndex, TCBF_NEEDOUT | TCBF_FIRSTSEND);
    SetTCBostate (u4TcbIndex, TCPO_IDLE);
    SetTCBstate (u4TcbIndex, TCPS_SYNSENT);
    /* Recv Window scale is initalized to 0 (No Window scaling)
     * Only when we receive a SYN from the other end with
     * window scale option set, we set the Window scale to the
     * approptiate value */
    SetTCBRecvWindowScale (u4TcbIndex, ZERO);
    TCP_MOD_TRC (ptcb->u4Context, TCP_FSM_TRC, "TCP",
                 "FSM state changed to SYNSENT for CONN ID - %d\n", u4TcbIndex);

    /* We should invoke OSM, to send the first seg */
    osm_switch[GetTCBostate (u4TcbIndex)] (u4TcbIndex, TCPE_SEND);

    /*This will invoke tcp_Idle, which in turn invokes tcp_Transmit */
    TCP_MOD_TRC (ptcb->u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from TcpInitiateConnection. Returned OK.\n");
    return (TCP_OK);
}

/*************************************************************************/
/*  Function Name  : tcpiss                                              */
/*  Description    : Get the initial sequence number for new connection  */
/*  Input(s)       : None.                                               */
/*  Output(s)      : None.                                               */
/*  Returns        : Sequence number                                     */
/*************************************************************************/

UINT4
tcpiss ()
{
    static UINT4        u4Seq = ZERO;
    UINT4               u4Time = ZERO;

    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering tcpiss. \n");
    if (u4Seq == ZERO)
    {
        u4Seq = (UINT4) UtilRand ();
    }
    OsixGetSysTime ((tOsixSysTime *) & u4Time);
    u4Seq += u4Time;
    u4Seq += TCP_RAND_ISS_GEN;
    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from tcpiss. Returned %d.\n", u4Seq);
    return u4Seq;
}

/****************************************************************/
/*  Function Name  : PortNotPresent                             */
/*  Description    : This function searches,in the TCBtable,    */
/*                   for the portnumber passed                  */
/*  Input(s)       : u4Context - Context Id                     */
/*                   u2Port - The port number to search for     */
/*                   LocalAddr - Ip address                     */
/*  Output(s)      : None.                                      */
/*  Returns        : TRUE/FALSE.                                */
/****************************************************************/
INT4
PortNotPresent (UINT4 u4Context, UINT2 u2Port, tIpAddr LocalAddr)
{
    tTcb               *ptcb = NULL;
    UINT4               u4Index;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering PortNotPresent.\n");
    ptcb = &TCBtable[ZERO];
    for (u4Index = INIT_COUNT; u4Index < MAX_NUM_OF_TCB; u4Index++, ptcb++)
    {
        if (ptcb->u1TcbState < TCPS_SYNSENT)
        {
            continue;
        }
        if (ptcb->u2TcbLocalPort == u2Port)
        {
            if (V6_ADDR_CMP (&(ptcb->TcbLocalIp), &LocalAddr) != TCP_ZERO)
            {
                if (((IN6_IS_ADDR_V4MAPPED (LocalAddr)) &&
                     (IN6_IS_ADDR_V4MAPPED (ptcb->TcbLocalIp))) ||
                    ((IsIP6Addr (LocalAddr)) && (IsIP6Addr (ptcb->TcbLocalIp))))
                    return FALSE;
                else
                    return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                 "Exit from PortNotPresent. Returned TRUE.\n");
    return TRUE;
}

/**********************************************************************/
/*  Function Name  : HliAssignPort                                    */
/*  Description    : This function allots a local port for the caller */
/*  Input(s)       : u4Context - Context Id                           */
/*                   LocalAddr - Ip address                           */
/*  Output(s)      : None.                                            */
/*  Returns        : The allotted Port number.                        */
/**********************************************************************/
UINT2
HliAssignPort (UINT4 u4Context, tIpAddr LocalAddr)
{
    UINT2               u2Port;
    INT4                i4PortAssigned;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering HliAssignPort.\n");
    /* The range 1 - 1023 is reserved for the well Known ports
       The range 1024 to 5000 is the one where TCP arbitarily
       assigns the port numbers. */

    u2Port = TCP_MAX_RESERVED_PORT;
    i4PortAssigned = FALSE;

    while (i4PortAssigned == FALSE)
    {
        if (PortNotPresent (u4Context, ++u2Port, LocalAddr) == TRUE)
        {
            i4PortAssigned = TRUE;
        }
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from HliAssignPort. Returned %d\n", u2Port);
    return u2Port;

}
