/** $Id: fstcplow.h,v 1.10 2013/06/07 13:32:13 siva Exp $ */

#ifndef __FSTCPLOW_H__
#define __FSTCPLOW_H__
/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsTcpAckOption ARG_LIST((INT4 *));

INT1
nmhGetFsTcpTimeStampOption ARG_LIST((INT4 *));

INT1
nmhGetFsTcpBigWndOption ARG_LIST((INT4 *));

INT1
nmhGetFsTcpIncrIniWnd ARG_LIST((INT4 *));

INT1
nmhGetFsTcpMaxNumOfTCB ARG_LIST((INT4 *));

INT1
nmhGetFsTcpTraceDebug ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */
INT1
nmhGetFsTcpTrapAdminStatus ARG_LIST((INT4 *));
INT1
nmhSetFsTcpAckOption ARG_LIST((INT4 ));

INT1
nmhSetFsTcpTimeStampOption ARG_LIST((INT4 ));

INT1
nmhSetFsTcpBigWndOption ARG_LIST((INT4 ));

INT1
nmhSetFsTcpIncrIniWnd ARG_LIST((INT4 ));

INT1
nmhSetFsTcpMaxNumOfTCB ARG_LIST((INT4 ));

INT1
nmhSetFsTcpTraceDebug ARG_LIST((INT4 ));
INT1
nmhSetFsTcpTrapAdminStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsTcpAckOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsTcpTimeStampOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsTcpBigWndOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsTcpIncrIniWnd ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsTcpMaxNumOfTCB ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsTcpTraceDebug ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsTcpTrapAdminStatus ARG_LIST((UINT4 *  ,INT4 ));
/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsTcpAckOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsTcpTimeStampOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsTcpBigWndOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsTcpIncrIniWnd ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsTcpMaxNumOfTCB ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsTcpTraceDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
INT1
nmhDepv2FsTcpTrapAdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsTcpConnTable. */
INT1
nmhValidateIndexInstanceFsTcpConnTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsTcpConnTable  */

INT1
nmhGetFirstIndexFsTcpConnTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsTcpConnTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsTcpConnOutState ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsTcpConnSWindow ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsTcpConnRWindow ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsTcpConnCWindow ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsTcpConnSSThresh ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsTcpConnSMSS ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsTcpConnRMSS ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsTcpConnSRT ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsTcpConnRTDE ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsTcpConnPersist ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsTcpConnRexmt ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsTcpConnRexmtCnt ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsTcpConnSBCount ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsTcpConnSBSize ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsTcpConnRBCount ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsTcpConnRBSize ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsTcpKaMainTmr ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsTcpKaRetransTmr ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsTcpKaRetransCnt ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsTcpKaMainTmr ARG_LIST((UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsTcpKaRetransTmr ARG_LIST((UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsTcpKaRetransCnt ARG_LIST((UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsTcpKaMainTmr ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTcpKaRetransTmr ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTcpKaRetransCnt ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsTcpConnTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsTcpExtConnTable. */
INT1
nmhValidateIndexInstanceFsTcpExtConnTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsTcpExtConnTable  */
INT1
nmhGetFirstIndexFsTcpExtConnTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsTcpExtConnTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */
INT1
nmhGetFsTcpConnMD5Option ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsTcpConnMD5ErrCtr ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsTcpConnTcpAOOption ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));
INT1
nmhGetFsTcpConTcpAOCurKeyId ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));
INT1
nmhGetFsTcpConTcpAORnextKeyId ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));
INT1
nmhGetFsTcpConTcpAORcvKeyId ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));
INT1
nmhGetFsTcpConTcpAORcvRnextKeyId ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));
INT1
nmhGetFsTcpConTcpAOConnErrCtr ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));
INT1
nmhGetFsTcpConTcpAOSndSne ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));
INT1
nmhGetFsTcpConTcpAORcvSne ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));
INT1
nmhValidateIndexInstanceFsTcpAoConnTestTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));
INT1
nmhGetFirstIndexFsTcpAoConnTestTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));
INT1
nmhGetNextIndexFsTcpAoConnTestTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));
INT1
nmhGetFsTcpConTcpAOIcmpIgnCtr ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));
INT1
nmhGetFsTcpConTcpAOSilentAccptCtr ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));
INT1
nmhGetFsTcpMaxReTries ARG_LIST((INT4 *));
INT1
nmhSetFsTcpMaxReTries ARG_LIST((INT4 ));
INT1
nmhTestv2FsTcpMaxReTries ARG_LIST((UINT4 *  ,INT4 ));
INT1
nmhDepv2FsTcpMaxReTries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Extern declarations of STDTCP mib table defined in netipvx directory */
extern INT4
tcpGetConnTableID (UINT4, INT4, tSNMP_OCTET_STRING_TYPE *, UINT4, tSNMP_OCTET_STRING_TYPE *, UINT4);
#endif
