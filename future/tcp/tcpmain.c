/*##########################################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved                   #
# ----------------------------------------------------                   #
# $Id: tcpmain.c,v 1.37 2016/05/06 10:10:58 siva Exp $                    #
                  ##########################################################################
*//* SOURCE FILE HEADER
   *
   * ----------------------------------------------------------------------------
   *  FILE NAME             : tcpmain.c
   * 
   *  PRINCIPAL AUTHOR      : Ramesh Kumar
   *
   *  SUBSYSTEM NAME        : TCP
   *
   *  MODULE NAME           : TCP Main File
   *
   *  LANGUAGE              : C
   *
   *  TARGET ENVIRONMENT    : Any
   *
   *  DATE OF FIRST RELEASE :
   *
   *  $Id: tcpmain.c,v 1.37 2016/05/06 10:10:58 siva Exp $  
   *
   *  DESCRIPTION           : This file contains function that waits for events  
   *                          in different queues.
   *
   * ---------------------------------------------------------------------------
   *
   *
   *  CHANGE RECORD :
   *
   * ---------------------------------------------------------------------------
   *   VERSION        AUTHOR/DATE           DESCRIPTION OF CHANGE
   * ---------------------------------------------------------------------------
   *     1.6          Rebecca Rufus         Removed the SLI_TCP_PROC_IFACE_WANTED
   *                                        switch.Dequeing and getting the 
   *                                        Application to TCP pParms was done 
   *                                        here ,Call to HliHandleCmd was 
   *                                        done by passing this parameter pParms.
   *     1.7          Rebecca Rufus         Chain Buffer allocated while issuing 
   *                                        commands is released before invoking
   *                                        hli_handle_command() function.Refer   
   *                                        Problem Id 4.                     
   *      -           Ramakrishnan          Added a semaphore init function for 
   *                                        the global select table under the 
   *                                        ST_ENHLISTINTF compilation switch.
   *                                        Changed the way tcp handles the lli
   *                                        packet by dequeing and call
   *                                        LliHandlePacket for every packet
   *                                        in the queue.
   * ------------------------------------------------------------------------*
   *     1.9      Saravanan.M    Implementation of                           *
   *              Purush            - Non Blocking Connect                   * 
   *                                - Non Blocking Accept                    *
   *                                - IfMsg Interface between TCP and IP     *
   *                                - One Queue per Socket                   *
   * ------------------------------------------------------------------------*/

#ifndef _TCPMAIN_C
#define _TCPMAIN_C
#include "tcpinc.h"

#ifdef SNMP_2_WANTED
#include "snmctdfs.h"
#include "fstcpwr.h"
#include "fsmptcwr.h"
#include "fsmstcwr.h"
#include "stdtcpwr.h"
#endif
#include "fssyslog.h"

tTcb               *TCBtable;    /* TCP control block table */
INT4                gu4TcpDbgMap;
INT4                gi4TcpGlobalTraceDebug;
tTimerListId        gTcpTimerListId;    /* TCP timers list identifier */
tTcpContext       **gpTcpContext;
tTcpContext        *gpTcpCurrContext = NULL;

/* global variables */
tOsixSemId          gTcbAllocSemId;    /* global TCB allocation sema4 */
tOsixSemId          gTcpProtoSemId;    /* global TCP Protocol lock semaphore */
tOsixQId            gTcpQId;    /* TCP queue identifier */
tOsixQId            gTcpCtrlQId;    /* TCP queue for control information */
tOsixTaskId         gTcpTaskId;    /* TCP task identifier  */

tMemPoolId          gTcbTblPoolId;
tMemPoolId          gTcbSendBufPoolId;
tMemPoolId          gTcbRcvBufPoolId;
tMemPoolId          gTcpFragPoolId;
tMemPoolId          gTcpSackPoolId;
tMemPoolId          gTcpRxmtPoolId;
tMemPoolId          gTcpToAppPoolId;
tMemPoolId          gTcpCxtPoolId;
tMemPoolId          gTcpParamsPoolId;
tMemPoolId          gTcpHlParamsPoolId;
tMemPoolId          gTcpIcmpParamsPoolId;

tTcpSystemSize      gTcpSysSizingParams;    /* Tcp system sizing parameter */

#ifdef SYSLOG_WANTED
INT4                gi4TcpSysLogId;
#endif

#ifdef IP6_WANTED
VOID                TcpRcvRegFn (tNetIpv6HliParams * pNetIpv6HlParams);
VOID                TcpIcmp6RegFn (tNetIpv6HliParams * pNetIpv6HlParams);
#endif

/*************************************************************************/
/*  Function Name   : TcpInit                                            */
/*  Input(s)        : None.                                              */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS/Error Code.                                */
/*  Description     : Initialises Tcp and spawn tcp task                 */
/*************************************************************************/
INT4
TcpInit ()
{
    INT4                i4RetVal;

    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TcpInit.\n");

    /* TCP Sizing  parameters i.e Max no. of TCP connections */
    GetTcpSizingParams (&gTcpSysSizingParams);

    if ((i4RetVal =
         (OsixTskCrt
          (TCP_TASK_NAME, TCP_TASK_PRIORITY, TCP_TASK_STACK_SIZE,
           (OsixTskEntry) TcpTaskMain, 0, &gTcpTaskId))) != OSIX_SUCCESS)
    {
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID,
                     TCP_INIT_SHUT_TRC | TCP_ALL_FAIL_TRC |
                     TCP_ENTRY_EXIT_TRC, "TCP",
                     "TCP task spawning returned %d. TcpInit failed.\n",
                     i4RetVal);
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from TcpInit. Returned Failure.\n");
        return (i4RetVal);
    }
    else
    {
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID,
                     TCP_INIT_SHUT_TRC, "TCP", "TCP task spawned.\n");
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID,
                     TCP_ENTRY_EXIT_TRC,
                     "TCP", "Exit from TcpInit. Returned OK.\n");
        return SUCCESS;
    }
}

/*************************************************************************/
/*  Function Name   : TcpTaskMain                                        */
/*  Input(s)        : None.                                              */
/*  Output(s)       : None.                                              */
/*  Returns         : None.                                              */
/*  Description     : Tcp task handles events                            */
/*************************************************************************/
void
TcpTaskMain (INT1 *pDummy)
{
    UINT4               u4Event;
    INT4                i4RetVal;

    /* Dummy pointers for system sizing during run time */
    tTcpTblBlock       *pTcpTblBlock = NULL;
    tTcpSendBuffBlock  *pTcpSendBuffBlock = NULL;
    tTcpRcvBuffBlock   *pTcpRcvBuffBlock = NULL;

    gu4MaxTcbLimit =
        MEM_MAX_BYTES (FsTCPSizingParams[MAX_TCP_TCB_TBL_BLOCKS_SIZING_ID].
                       u4PreAllocatedUnits, MAX_NUM_OF_TCB_LIMIT);

    UNUSED_PARAM (pTcpTblBlock);
    UNUSED_PARAM (pTcpSendBuffBlock);
    UNUSED_PARAM (pTcpRcvBuffBlock);

    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TCPTaskMain.\n");
    UNUSED_PARAM (pDummy);
    /* TCP Sizing  parameters i.e Max no. of TCP connections */
    GetTcpSizingParams (&gTcpSysSizingParams);

    if (TcpSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID,
                     TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from TCPTaskMain. MemPool Creation failed.\n");
        /* Indicate the status of initialization to the main routine */
        TCP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    gTcbTblPoolId = TCPMemPoolIds[MAX_TCP_TCB_TBL_BLOCKS_SIZING_ID];
    gTcbSendBufPoolId = TCPMemPoolIds[MAX_TCP_SND_BUFF_BLOCKS_SIZING_ID];
    gTcbRcvBufPoolId = TCPMemPoolIds[MAX_TCP_RCV_BUFF_BLOCKS_SIZING_ID];
    gTcpFragPoolId = TCPMemPoolIds[MAX_TCP_NUM_OF_FRAG_SIZING_ID];
    gTcpSackPoolId = TCPMemPoolIds[MAX_TCP_NUM_OF_SACK_SIZING_ID];
    gTcpRxmtPoolId = TCPMemPoolIds[MAX_TCP_NUM_OF_RXMT_SIZING_ID];
    gTcpToAppPoolId = TCPMemPoolIds[MAX_TCP_TO_APP_BLOCKS_SIZING_ID];
    gTcpCxtPoolId = TCPMemPoolIds[MAX_TCP_NUM_CONTEXT_SIZING_ID];
    gTcpParamsPoolId = TCPMemPoolIds[MAX_TCP_NUM_PARAMS_SIZING_ID];
    gTcpHlParamsPoolId = TCPMemPoolIds[MAX_TCP_IP_HL_PARMS_SIZING_ID];
    gTcpIcmpParamsPoolId = TCPMemPoolIds[MAX_TCP_ICMP_PARAMS_SIZING_ID];

    i4RetVal = TcpTaskInit ();

    if (i4RetVal != SUCCESS)
    {
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID,
                     TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from TCPTaskMain. Returned ERR.\n");
        /* Indicate the status of initialization to the main routine */
        TCP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    if (OsixTskIdSelf (&gTcpTaskId) == OSIX_FAILURE)
    {
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID,
                     TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from TCPTaskMain. Returned ERR.\n");
        /* Indicate the status of initialization to the main routine */
        TCP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    /* Indicate the status of initialization to the main routine */
    TCP_INIT_COMPLETE (OSIX_SUCCESS);
#ifdef SNMP_2_WANTED
    /* Register the protocol MIBs with SNMP */
    RegisterSTDTCP ();
    RegisterFSTCP ();
    RegisterFSMSTC ();
    RegisterFSMPTC ();
#endif

    while (1)
    {
        if (OsixEvtRecv (gTcpTaskId, TCP_IP_IF_EVENT |
                         TCP_TIMER_EVENT | TCP_VCM_EVENT,
                         OSIX_WAIT, &u4Event) == OSIX_SUCCESS)
        {
            if (u4Event & TCP_IP_IF_EVENT)
            {
                TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_OS_RES_TRC, "TCP",
                             "Received packet from IP. \n");
                TcpProcessQMsg ();
            }

            if (u4Event & TCP_TIMER_EVENT)
            {
                TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_OS_RES_TRC, "TCP",
                             "Received Timer Event.\n");
                TcpTmrHandleExpiry ();
            }
            if (u4Event & TCP_VCM_EVENT)
            {
                TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_OS_RES_TRC, "TCP",
                             "Received VCM Event.\n");
                if (SNMP_FAILURE == TcpLock ())
                {
                    continue;
                }
                TcpProcessVcmMsg ();
                TcpUnLock ();
            }
        }
    }
}

/*************************************************************************/
/*  Function Name   : TcpTaskInit                                        */
/*  Input(s)        : None.                                              */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS/FAILURE.                                   */
/*  Description     : allocates the resource required for TCP task       */
/*************************************************************************/
INT4
TcpTaskInit ()
{
    tTcpContext        *pTcpContext = NULL;
    INT4                i4RetVal = FAILURE;
#ifdef SYSLOG_WANTED
    INT4                i4SysLogId = 0;
#endif

    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC,
                 "TCP", "Entering TcpTaskInit.\n");

    gpTcpContext =
        MemAllocMemBlk (TCPMemPoolIds[MAX_TCP_CXT_STRUCTS_SIZING_ID]);
    if (NULL == gpTcpContext)
    {
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from TcpTaskInit. Returned FAILURE.\n");
        return FAILURE;
    }
    MEMSET (gpTcpContext, 0,
            (FsTCPSizingParams[MAX_TCP_CXT_STRUCTS_SIZING_ID].
             u4PreAllocatedUnits * sizeof (UINT4 *)));
    if ((TcpMemInit ()) == FAILURE)
    {
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from TcpTaskInit. Returned FAILURE.\n");
        return FAILURE;
    }

    if ((i4RetVal = TmrCreateTimerList (TCP_TASK_NAME, TCP_TIMER_EVENT,
                                        NULL,
                                        &gTcpTimerListId)) != OSIX_SUCCESS)
    {
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID,
                     TCP_INIT_SHUT_TRC | TCP_ENTRY_EXIT_TRC |
                     TCP_ALL_FAIL_TRC | TCP_OS_RES_TRC, "TCP",
                     "Timer list creation failed with error %d.\n", i4RetVal);
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from TcpTaskInit. Returned FAILURE.\n");
        TcpMemFree ();
        return FAILURE;
    }

    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID,
                 TCP_INIT_SHUT_TRC | TCP_OS_RES_TRC,
                 "TCP", "Timer list creation successful.");

    /* The LL to TCP Q creation */
    if ((i4RetVal = OsixQueCrt (TCP_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                                TCP_Q_DEPTH, &gTcpQId)) != OSIX_SUCCESS)
    {
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID,
                     TCP_INIT_SHUT_TRC | TCP_ENTRY_EXIT_TRC |
                     TCP_ALL_FAIL_TRC | TCP_OS_RES_TRC, "TCP",
                     "IP to TCP Q creation failed with error %d.", i4RetVal);
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from TcpTaskInit. Returned FAILURE.\n");
        /* delete timer list */
        TmrDeleteTimerList (gTcpTimerListId);
        TcpMemFree ();
        return FAILURE;
    }

    /* The Vcm to TCP Q creation */
    if ((i4RetVal = OsixQueCrt (TCP_CTRL_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                                TCP_Q_DEPTH, &gTcpCtrlQId)) != OSIX_SUCCESS)
    {
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID,
                     TCP_INIT_SHUT_TRC | TCP_ENTRY_EXIT_TRC |
                     TCP_ALL_FAIL_TRC | TCP_OS_RES_TRC, "TCP",
                     "IP to TCP Q creation failed with error %d.", i4RetVal);
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from TcpTaskInit. Returned FAILURE.\n");
        /* delete timer list */
        TmrDeleteTimerList (gTcpTimerListId);
        TcpMemFree ();
        return FAILURE;
    }

    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID,
                 TCP_INIT_SHUT_TRC | TCP_OS_RES_TRC,
                 "TCP", "TCP main Q creation successful.");

    /* Semaphore required for mutexing when (de)allocating TCB */

    if ((i4RetVal = OsixCreateSem (TCP_TCB_MUX_SEM,
                                   TCP_ONE, OSIX_WAIT,
                                   &gTcbAllocSemId)) != OSIX_SUCCESS)
    {
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_INIT_SHUT_TRC | TCP_OS_RES_TRC,
                     "TCP", "TCB mutex semaphore allocation failed\n");
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from TcpTaskInit. Returned FAILURE.\n");
        TmrDeleteTimerList (gTcpTimerListId);
        OsixQueDel (gTcpQId);
        TcpMemFree ();
        return FAILURE;
    }

    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_INIT_SHUT_TRC | TCP_OS_RES_TRC,
                 "TCP", "TCB mutex semaphore allocated successfully\n");

    /* Semaphore required for mutexing when accessing Current Context */

    if ((i4RetVal = OsixCreateSem ((UINT1 *) "TCPSEM",
                                   TCP_ONE, OSIX_WAIT,
                                   &gTcpProtoSemId)) != OSIX_SUCCESS)
    {
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_INIT_SHUT_TRC | TCP_OS_RES_TRC,
                     "TCP", "TCP mutex semaphore allocation failed\n");
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from TcpTaskInit. Returned FAILURE.\n");
        TmrDeleteTimerList (gTcpTimerListId);
        OsixQueDel (gTcpQId);
        TcpMemFree ();
        return FAILURE;
    }

    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_INIT_SHUT_TRC | TCP_OS_RES_TRC,
                 "TCP", "TCP semaphore allocated successfully\n");
    i4RetVal = TcpCreateContext (VCM_DEFAULT_CONTEXT);
    if (i4RetVal != SUCCESS)
    {
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from TcpTaskInit. Context creation FAILURE.\n");
        OsixQueDel (gTcpQId);
        OsixSemDel (gTcbAllocSemId);
        TcpMemFree ();
        return FAILURE;
    }
    pTcpContext = gpTcpContext[VCM_DEFAULT_CONTEXT];
    gpTcpCurrContext = gpTcpContext[VCM_DEFAULT_CONTEXT];

    if ((i4RetVal = TcpRegisterWithVcm ()) != SUCCESS)
    {
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from TcpTaskInit. Register with VCM failed.\n");
        TmrDeleteTimerList (gTcpTimerListId);
        OsixQueDel (gTcpQId);
        OsixSemDel (gTcbAllocSemId);
        TcpMemFree ();
        MemReleaseMemBlock (gTcpCxtPoolId, (UINT1 *) pTcpContext);
        return FAILURE;
    }

#ifdef SYSLOG_WANTED
    /* Register with SysLog Server to Log Error Messages. */
    i4SysLogId =
        SYS_LOG_REGISTER ((CONST UINT1 *) "TCP", SYSLOG_CRITICAL_LEVEL);

    if (i4SysLogId <= ZERO)
    {
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_INIT_SHUT_TRC | TCP_OS_RES_TRC,
                     "TCP", "Registration with sylog moddule failed\n");
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from TcpTaskInit. Returned FAILURE.\n");
        TmrDeleteTimerList (gTcpTimerListId);
        OsixQueDel (gTcpQId);
        OsixSemDel (gTcbAllocSemId);
        TcpMemFree ();
        MemReleaseMemBlock (gTcpCxtPoolId, (UINT1 *) pTcpContext);
        return FAILURE;
    }
    gi4TcpSysLogId = i4SysLogId;
#endif
    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from TcpTaskInit. Returned SUCCESS.\n");

    return SUCCESS;
}

/*************************************************************************/
/*  Function Name   : TcpTaskShutDown                                    */
/*  Input(s)        : None.                                              */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS/FAILURE                                    */
/*  Description     : deallocates all the resources allocated to TCP     */
/*************************************************************************/
VOID
TcpTaskShutDown ()
{
    UINT4               u4Context = 0;

    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TcpTaskShutDown.\n");
    TcpProcessQMsg ();

    OsixQueDel (gTcpQId);
    TmrDeleteTimerList (gTcpTimerListId);
    OsixSemDel (gTcbAllocSemId);
    for (u4Context = 0;
         u4Context < FsTCPSizingParams[MAX_TCP_NUM_CONTEXT_SIZING_ID].
         u4PreAllocatedUnits; u4Context++)
    {
        TcpDeleteContext (u4Context);
    }
    VcmDeRegisterHLProtocol (IP_TCP_PROTOCOL_ID);
    TcpMemFree ();
    OsixTskDel (gTcpTaskId);

    return;
}

/*************************************************************************/
/*  Function Name   : TcpMemInit                                         */
/*  Input(s)        : None.                                              */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS/FAILURE                                    */
/*  Description     : allocates the meory resources required for TCP     */
/*************************************************************************/
INT4
TcpMemInit ()
{
    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TcpMemeInit.\n");

    if ((TCBtable = MemAllocMemBlk (gTcbTblPoolId)) == NULL)
    {
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID,
                     TCP_INIT_SHUT_TRC | TCP_ENTRY_EXIT_TRC |
                     TCP_ALL_FAIL_TRC | TCP_OS_RES_TRC,
                     "TCP", "TCB Memory allocation failed.");
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from TcpMemInit.\n");
        return FAILURE;
    }
    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID,
                 TCP_INIT_SHUT_TRC | TCP_OS_RES_TRC,
                 "TCP", "TCB memory allocation successful.");

    MEMSET (TCBtable, ZERO, sizeof (tTcb) * MAX_NUM_OF_TCB);

    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from TcpMemInit.\n");
    return SUCCESS;
}

/*************************************************************************/
/*  Function Name   : TcpMemInit                                         */
/*  Input(s)        : None.                                              */
/*  Output(s)       : None.                                              */
/*  Returns         : None.                                              */
/*  Description     : deallocates the meory resources of TCP             */
/*************************************************************************/

VOID
TcpMemFree ()
{
    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TcpMemFree.\n");

    MemReleaseMemBlock (gTcbTblPoolId, (UINT1 *) TCBtable);

    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_OS_RES_TRC, "TCP",
                 "Deallocated TCB memory\n");
    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from TcpMemFree.\n");
    return;
}

#ifdef IP6_WANTED
/* function to receive packet from IP6 to IP */
/**************************************************************************** 
    Function    :  TcpRcvRegFn 
    Input       :  pRcvInfo 
    Output      :  None. 
    Returns     :  None. 
    Description :  Receives the IPV6 Packet 
****************************************************************************/
VOID
TcpRcvRegFn (tNetIpv6HliParams * pRcvInfo)
{
    tCRU_INTERFACE      tCruIf;
    MEMSET (&tCruIf, ZERO, sizeof (tCruIf));
    Ip6taskTcpInput (pRcvInfo->unIpv6HlCmdType.AppRcv.pBuf,
                     (UINT2) pRcvInfo->unIpv6HlCmdType.AppRcv.u4PktLength,
                     (UINT2) pRcvInfo->unIpv6HlCmdType.AppRcv.u4Index, tCruIf,
                     0);
    return;
}

/**************************************************************************** 
    Function    :  TcpIcmp6RegFn 
    Input       :  pIfInfo 
    Output      :  None. 
    Returns     :  None. 
    Description :  Receives the ICMP6 Notification message. 
****************************************************************************/
VOID
TcpIcmp6RegFn (tNetIpv6HliParams * pIcmp6Info)
{
    UINT4               u4Context = 0;

    if (VcmGetContextIdFromCfaIfIndex
        (pIcmp6Info->unIpv6HlCmdType.Icmpv6ErrorSend.u4Index, &u4Context)
        == VCM_FAILURE)
    {
        return;
    }
    Tcp6RcvIcmp6Pkt (u4Context,
                     pIcmp6Info->unIpv6HlCmdType.Icmpv6ErrorSend.pBuf,
                     pIcmp6Info->unIpv6HlCmdType.Icmpv6ErrorSend.Ip6Addr,
                     (UINT1) pIcmp6Info->unIpv6HlCmdType.Icmpv6ErrorSend.u4Type,
                     (UINT1) pIcmp6Info->unIpv6HlCmdType.Icmpv6ErrorSend.u4Code,
                     (UINT2) pIcmp6Info->unIpv6HlCmdType.Icmpv6ErrorSend.
                     u4PktLength);
    return;
}
#endif

/*************************************************************************/
/*  Function Name   : TcpRegisterWithLL                                  */
/*  Input(s)        : u4ContextId                                        */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS/FAILURE                                    */
/*  Description     : registration of TCP with IP/IPv6 is done           */
/*************************************************************************/
INT4
TcpRegisterWithLL (UINT4 u4ContextId)
{
    INT4                i4RetVal = FAILURE;
#ifdef IP_WANTED
    tNetIpRegInfo       IpRegInfo;

    MEMSET (&IpRegInfo, 0, sizeof (tNetIpRegInfo));

    IpRegInfo.u1ProtoId = TCP_PROTOCOL;
    IpRegInfo.u2InfoMask |= NETIPV4_PROTO_PKT_REQ;
    IpRegInfo.pProtoPktRecv = IptaskTcpInput;
    IpRegInfo.pIfStChng = NULL;
    IpRegInfo.pRtChng = NULL;
    IpRegInfo.u4ContextId = u4ContextId;

    i4RetVal = NetIpv4RegisterHigherLayerProtocol (&IpRegInfo);
    if (i4RetVal != SUCCESS)
    {
        TCP_MOD_TRC (u4ContextId,
                     TCP_INIT_SHUT_TRC |
                     TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from TcpRegisterWithLL. Registration with IP returned error %d.\
                           TcpRegisterWithLL failed. Returned ERR.\n", i4RetVal);
        return FAILURE;
    }
    TCP_MOD_TRC (u4ContextId, TCP_INIT_SHUT_TRC, "TCP",
                 "Registered successfully with IP. TcpRegisterWithLL successful.\n");
#endif
#ifdef IP6_WANTED
/* TCP Registration to receive Packet and ICMP6 Notification.. */
    NetIpv6RegisterHigherLayerProtocolInCxt (u4ContextId, TCP_PROTOCOL,
                                             NETIPV6_APPLICATION_RECEIVE,
                                             (VOID *) TcpRcvRegFn);

    NetIpv6RegisterHigherLayerProtocolInCxt (u4ContextId, TCP_PROTOCOL,
                                             NETIPV6_ICMPV6_ERROR_SEND,
                                             (VOID *) TcpIcmp6RegFn);

    TCP_MOD_TRC (u4ContextId, TCP_INIT_SHUT_TRC, "TCP",
                 "Registered successfully with IP6 & ICMP6. TCP init successful.\n");

#endif
    return i4RetVal;
}

/*************************************************************************/
/*  Function Name   : TcpProcessQMsg                                     */
/*  Input(s)        : None.                                              */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS/FAILURE                                    */
/*  Description     : registration of TCP with IP/IPv6 is done           */
/*************************************************************************/
VOID
TcpProcessQMsg ()
{
    UINT1              *pSeg;
    UINT4 u4Count = 0;

    while ((OsixQueRecv (gTcpQId, (UINT1 *) &pSeg, OSIX_DEF_MSG_LEN,
                         OSIX_NO_WAIT)) == OSIX_SUCCESS)
    {
        LliHandlePacket (pSeg);
        u4Count++;
        if(u4Count % TCP_PROCESS_Q_MSG_CURR_DEPTH)
        {
           OsixEvtSend (gTcpTaskId, TCP_IP_IF_EVENT);
           break;
        }
    }
}

/*************************************************************************/
/*  Function Name   : TcpCreateContext                                   */
/*  Input(s)        : u4ContextId - Context Id                           */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS/FAILURE                                    */
/*  Description     : Creates the tcp information for the context and    */
/*                    and registers with lower layers for that context   */
/*************************************************************************/
INT4
TcpCreateContext (UINT4 u4ContextId)
{
    tTcpContext        *pTcpContext = NULL;

    TCP_MOD_TRC (u4ContextId, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TcpCreateContext. \n");

    if (u4ContextId >=
        FsTCPSizingParams[MAX_TCP_NUM_CONTEXT_SIZING_ID].u4PreAllocatedUnits)
    {
        TCP_MOD_TRC (u4ContextId, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP",
                     "Exit from TcpCreateContext. Invalid context id %d obtained.\n",
                     u4ContextId);

        return FAILURE;
    }
    if (gpTcpContext[u4ContextId] != NULL)
    {
        TCP_MOD_TRC (u4ContextId, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP",
                     "Exit from TcpCreateContext. Context %d already exists.\n",
                     u4ContextId);

        return FAILURE;
    }
    gpTcpContext[u4ContextId] = MemAllocMemBlk (gTcpCxtPoolId);
    if (gpTcpContext[u4ContextId] == NULL)
    {
        TCP_MOD_TRC (u4ContextId, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP",
                     "Exit from TcpCreateContext. Unable to allocate memory for context %d.\n",
                     u4ContextId);
        return FAILURE;
    }

    pTcpContext = gpTcpContext[u4ContextId];
    MEMSET (pTcpContext, 0, sizeof (tTcpContext));

    /* Assign the default values */
    pTcpContext->i2IcmpMessagesEnabled = FALSE;
    pTcpContext->u4TcpMinCwnd = TCP_DEF_MIN_CWND;
    pTcpContext->u4TcpMaxCwnd = TCP_DEF_MAX_CWND;
    pTcpContext->TcpConfigParms.i4TcpAckOpt = TCP_DEFACK;
    pTcpContext->TcpConfigParms.i4TcpTSOpt = TCP_TSCONF;
    pTcpContext->TcpConfigParms.i4TcpBWOpt = TCP_NOCONF;
    pTcpContext->TcpConfigParms.i4TcpIncrIniWnd = TCP_NOCONF;
    pTcpContext->TcpConfigParms.i4TcpWSOpt = TCP_WSCONF;
    pTcpContext->TcpConfigParms.u2TcpMaxRetries = TCP_MAXRETRIES;
    pTcpContext->TcpStatParms.u4RtoAlgorithm = VAN_JACOBSON;
    pTcpContext->TcpStatParms.u4RtoMin = TCP_MINRETRANSMIT;
    pTcpContext->TcpStatParms.u4RtoMax = TCP_MAXRETRANSMIT;
    pTcpContext->TcpStatParms.u4MaxConn = MAX_NUM_OF_TCB;
    pTcpContext->u4ContextId = u4ContextId;

    /* Register with Lowerlayers */
    if (TcpRegisterWithLL (u4ContextId) != SUCCESS)
    {
        TCP_MOD_TRC (u4ContextId, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP",
                     "Exit from TcpCreateContext. Unable to register with LL in context %d.\n",
                     u4ContextId);
        MEMSET (gpTcpContext[u4ContextId], 0, sizeof (tTcpContext));
        MemReleaseMemBlock (gTcpCxtPoolId, (UINT1 *) gpTcpContext[u4ContextId]);
        gpTcpContext[u4ContextId] = NULL;
        return FAILURE;
    }
    return SUCCESS;
}

/*************************************************************************/
/*  Function Name   : TcpRegisterWithVcm                                 */
/*  Input(s)        : None.                                              */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS/FAILURE                                    */
/*  Description     : Registers with VCM for handling context creation   */
/*                    and deletion                                       */
/*************************************************************************/
INT4
TcpRegisterWithVcm (VOID)
{
    tVcmRegInfo         VcmRegInfo;

    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TcpRegisterWithVcm. \n");

    MEMSET (&VcmRegInfo, 0, sizeof (tVcmRegInfo));
    VcmRegInfo.u1ProtoId = IP_TCP_PROTOCOL_ID;
    VcmRegInfo.pIfMapChngAndCxtChng = TcpNotifyContextChange;
    VcmRegInfo.u1InfoMask = VCM_CONTEXT_CREATE | VCM_CONTEXT_DELETE;
    if (VcmRegisterHLProtocol (&VcmRegInfo) == VCM_FAILURE)
    {
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exiting TcpRegisterWithVcm. Registration with VCM failed \n");
        return FAILURE;
    }
    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exiting TcpRegisterWithVcm. \n");
    return SUCCESS;

}

/*************************************************************************/
/*  Function Name   : TcpNotifyContextChange                             */
/*  Input(s)        : u4IfIndex - interface index                        */
/*                    u4ContextId - context id                           */
/*                    u1Event - Context creation/deletion event          */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS/FAILURE                                    */
/*  Description     : Sends context creation/ deletion messages to the   */
/*                    tcp task;                                          */
/*************************************************************************/
VOID
TcpNotifyContextChange (UINT4 u4IfIndex, UINT4 u4ContextId, UINT1 u1Event)
{
    tTcpVcmParams      *pTcpVcmParams = NULL;

    UNUSED_PARAM (u4IfIndex);
    TcpLock ();
    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TcpNotifyContextChange \n");
    pTcpVcmParams = MemAllocMemBlk (gTcpParamsPoolId);
    if (pTcpVcmParams == NULL)
    {
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exiting TcpNotifyContextChange.Unable to allocate TcpVcmParams \n");
        return;
    }
    pTcpVcmParams->u4ContextId = u4ContextId;
    pTcpVcmParams->u1Event = u1Event;

    if (OSIX_FAILURE ==
        OsixQueSend (gTcpCtrlQId, (UINT1 *) &pTcpVcmParams, OSIX_DEF_MSG_LEN))
    {
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exiting TcpNotifyContextChange.Unable to send message \n");
        MemReleaseMemBlock (gTcpParamsPoolId, (UINT1 *) pTcpVcmParams);
        return;
    }

    OsixEvtSend (gTcpTaskId, TCP_VCM_EVENT);
    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exiting TcpNotifyContextChange.\n");
    TcpUnLock ();
    return;
}

/*************************************************************************/
/*  Function Name   : TcpProcessVcmMsg                                   */
/*  Input(s)        : None                                               */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS/FAILURE                                    */
/*  Description     : Processes the context creation/ deletion messages  */
/*                    received from VCM and creates/deletes the          */
/*                    corresponding tcp context.                         */
/*************************************************************************/
VOID
TcpProcessVcmMsg (VOID)
{
    tTcpVcmParams      *pTcpVcmParams = NULL;

    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TcpProcessVcmMsg\n");

    while ((OsixQueRecv (gTcpCtrlQId, (UINT1 *) &pTcpVcmParams,
                         OSIX_DEF_MSG_LEN, OSIX_NO_WAIT)) == OSIX_SUCCESS)
    {
        if (pTcpVcmParams->u1Event == VCM_CONTEXT_CREATE)
        {
            TcpCreateContext (pTcpVcmParams->u4ContextId);
        }
        else if (pTcpVcmParams->u1Event == VCM_CONTEXT_DELETE)
        {
            TcpDeleteContext (pTcpVcmParams->u4ContextId);
        }
        MemReleaseMemBlock (gTcpParamsPoolId, (UINT1 *) pTcpVcmParams);
        pTcpVcmParams = NULL;
    }
    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exiting TcpProcessVcmMsg\n");
    return;
}

/*************************************************************************/
/*  Function Name   : TcpDeleteContext                                   */
/*  Input(s)        : u4ContextId                                        */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS/FAILURE                                    */
/*  Description     : De-registers tcp with IP and Ipv6 and release the  */
/*                    tcp context                                        */
/*************************************************************************/
VOID
TcpDeleteContext (UINT4 u4ContextId)
{
    UINT4               u4TcbIndex = 0;
    TCP_MOD_TRC (u4ContextId, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TcpDeleteContext\n");
    if (gpTcpContext[u4ContextId] == NULL)
    {
        TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Context is already deleted\n");
        return;
    }
#ifdef IP6_WANTED
    NetIpv6DeRegisterHigherLayerProtocolInCxt (u4ContextId, TCP_PROTOCOL);
#endif
#ifdef IP_WANTED
    NetIpv4DeRegisterHigherLayerProtocolInCxt (u4ContextId, TCP_PROTOCOL);
#endif
    /* release the TCB blocks used for this context */
    for (u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
    {
        if (GetTCBContext (u4TcbIndex) == u4ContextId)
        {
            if ((TakeTCBmainsem (u4TcbIndex)) == OSIX_SUCCESS)
	    {
            deallocate (&TCBtable[u4TcbIndex]);
        }
    }
    }

    MEMSET (gpTcpContext[u4ContextId], 0, sizeof (tTcpContext));
    MemReleaseMemBlock (gTcpCxtPoolId, (UINT1 *) gpTcpContext[u4ContextId]);
    gpTcpContext[u4ContextId] = NULL;

    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exiting TcpDeleteContext\n");
    return;
}
#endif

/********************************************************************/
/* Function Name   : TcpLock                                        */
/* Description     : Take the TCP protocol semaphore                */
/* Input(s)        : None                                           */
/* Output(s)       : None                                           */
/* Returns         : SNMP_SUCCESS or SNMP_FAILURE                   */
/********************************************************************/
INT4
TcpLock ()
{
    if (OsixSemTake (gTcpProtoSemId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/********************************************************************/
/* Function Name   : TcpUnLock                                      */
/* Description     : Releases the TCP protocol semaphore            */
/* Input(s)        : None                                           */
/* Output(s)       : None                                           */
/* Returns         : OSIX_SUCCESS                                   */
/********************************************************************/
INT4
TcpUnLock ()
{
    OsixSemGive (gTcpProtoSemId);
    return SNMP_SUCCESS;
}

/*************************************************************************/
/*  Function Name   : TcpStopAllTimers                                   */
/*  Input(s)        : None.                                              */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS/FAILURE                                    */
/*  Description     : Stops all the timers running in TCP                */
/*************************************************************************/

VOID TcpStopAllTimers()
{

    UINT4               u4Context = 0;
    UINT4               u4TcbIndex = 0;
    INT4        i4TcbFtSrvSockFd = SliGetFtSrvSockFdTCBTableIndex();
    INT4                i4TcbTcpSrvSockFd = SliGetTcpSrvSockFdTCBTableIndex();

    for (u4Context = 0;
         u4Context < FsTCPSizingParams[MAX_TCP_NUM_CONTEXT_SIZING_ID].
         u4PreAllocatedUnits; u4Context++)
    {
        if (gpTcpContext[u4Context] == NULL)
        {
            TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Context is already deleted\n");
            return;
        }
        for (u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
        {
            if (GetTCBContext (u4TcbIndex) == u4Context)
            {
                u4TcbIndex = (UINT4) GET_TCB_INDEX (&TCBtable[u4TcbIndex]);

                /* Proceed with normal flow for Listening servers and
                   RM TCP connections */
                if ((GetTCBstate(u4TcbIndex) != TCPS_LISTEN) &&
                    (((INT4)u4TcbIndex) != i4TcbFtSrvSockFd) &&
                    (((INT4)u4TcbIndex) != i4TcbTcpSrvSockFd))
                {
                    if ((TakeTCBmainsem (u4TcbIndex)) == OSIX_SUCCESS)
                    {
                        deallocate (&TCBtable[u4TcbIndex]);
                        /* No need to unlock as deallocate() will delete the semaphore also */
                    }
                }
                else
                {
                    TmrKillTimers (&TCBtable[u4TcbIndex]);        /*Kill all timers */
                }
            }
        }
    }

}

