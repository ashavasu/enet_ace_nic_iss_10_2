/** $Id: tcpprot.h,v 1.15 2013/09/13 14:27:00 siva Exp $ */

#ifndef __TCPPROT_H__
#define __TCPPROT_H__
/* Prototypes of all the functions */

/* tcpflow.c */
UINT4 FarGetLwsize           PROTO ((tTcb *));
INT1  LINFarSetRwsize        PROTO ((tTcb *, tTcpHdr *));
INT4  LINFarHandleAck        PROTO ((tTcb *, tIpTcpHdr *));
INT1  LINFarSendAck          PROTO ((tTcb *, tIpTcpHdr *));
INT1  FarUpdateOstate        PROTO ((tTcb *, UINT4));
INT1  FarComputeRtt          PROTO ((tTcb *, INT4));

/* tcphli.c */
INT1  HliHandleCmd      PROTO((tApplicationToTcpParms *));
INT1  HliError          PROTO((tApplicationToTcpParms *, INT1, UINT4));
INT1  HliSendError      PROTO((UINT4));
tTcpToApplicationParms *pHliError PROTO((tApplicationToTcpParms *, 
                                       INT1, UINT4));


/* tcphlcmd.c */
INT1  HliOpen    PROTO ((tOpen *, tApplicationToTcpParms *));
tTcpToApplicationParms *HliClose   PROTO ((tClose   *, 
                                       tApplicationToTcpParms *));
tTcpToApplicationParms *HliWrite   PROTO ((tSend *, 
                                       tApplicationToTcpParms *));
tTcpToApplicationParms *HliStatus  PROTO ((tStatus  *, 
                                     tApplicationToTcpParms *));
tTcpToApplicationParms *HliAbort   PROTO ((tAbort   *, 
                                       tApplicationToTcpParms *));
tTcpToApplicationParms *HliOptions PROTO ((tOption  *, 
                                       tApplicationToTcpParms *));

INT1  TcpGetSelectStatus PROTO(( UINT4*, UINT4*, UINT4*, UINT4*, UINT4*, UINT4*));
INT4  TcpSliReceiveData \
                  PROTO ((tApplicationToTcpParms*, \
                          tTcpToApplicationParms*));
INT4  tcpgetdata  PROTO ((tTcb  *, UINT1 *, UINT2));

INT1  HliRegAsyncHandler   PROTO ((tRegAsyncHandler *, 
                                      tApplicationToTcpParms *));

/* tcpresp.c */
tTcpToApplicationParms *pHliReplyForClose  PROTO ((UINT4));
tTcpToApplicationParms *HliReplyForSend       PROTO ((UINT4, UINT1 *, UINT4));
tTcpToApplicationParms *HliReplyForStatus     PROTO ((UINT4));
tTcpToApplicationParms *HliReplyForAbort      PROTO ((UINT4));
tTcpToApplicationParms *HliReplyForOptions    PROTO ((UINT4, 
                                                    UINT4, UINT4, UINT1));
INT1  HliReplyForOpen      PROTO ((UINT4));
INT1  hliReplyForClose     PROTO ((UINT1));
INT1  HliReplyForReceive   PROTO ((UINT4, UINT1 *, UINT2, UINT1));
INT1  hliReplyForAsyMsg    PROTO ((UINT1, UINT1));
INT1  HliReplyForNewConn   PROTO ((UINT4, UINT4));
INT4 TcpCheckLocalAddrPort PROTO ((tIpAddr, UINT2, UINT1 *));
INT1  hliAppendToCommQueue PROTO((tTcpToApplicationParms *));
INT4 TcpChangeRecvBufSize PROTO ((UINT4, UINT4));
INT4 TcpChangeSendBufSize PROTO ((UINT4, UINT4));
INT1 TcpCheckListenConn PROTO ((UINT4));
INT4 TcpCheckLocalAddrPortInCxt PROTO ((UINT4, tIpAddr, UINT2, UINT1 *));

/* tcphlifn.c */
tTcb *tcballoc                 PROTO ((UINT4));
INT1  deallocate               PROTO ((tTcb *));
INT1  TcpSync                  PROTO ((tTcb *, UINT4));
INT1  TcpInitiateConnection    PROTO ((tTcb *, tOpen *));

UINT4 tcpiss                    PROTO ((void));
INT4  PortNotPresent          PROTO ((UINT4, UINT2 , tIpAddr));
UINT2 HliAssignPort           PROTO ((UINT4, tIpAddr));

/* tcpicmp.c */
INT4  IcmpHandleMessage       PROTO ((UINT1 *, UINT4));        
INT4 Tcp6RcvIcmp6Pkt          PROTO  ((UINT4, tCRU_BUF_CHAIN_HEADER *, tIp6Addr,
                                      UINT1, UINT1, UINT2));


/* tcpinpfn.c */
tTcb *LINInpFindConnection       PROTO ((UINT1 *, tIpHdr *, tTcpHdr *));
INT1  LINInpIsSegInRcvWin        PROTO ((tTcb *, tIpHdr *,
                                            tTcpHdr *, INT1 *));
INT1  LINInpCheckOptions         PROTO ((tTcb *, tTcpHdr *));
UINT1 LINInpUpdateIncomingMss    PROTO ((tTcb *, tTcpHdr *, UINT4));
UINT2 InpGetTcplen               PROTO ((tSegment *));
UINT4 getinfo4                   PROTO ((tSegment *, UINT4));
UINT2 getinfo2                   PROTO ((tSegment *, UINT4));
UINT1 getinfo1                   PROTO ((tSegment *, UINT4));
VOID  IptaskTcpInput             PROTO ((tCRU_BUF_CHAIN_HEADER *, UINT2,
                                            UINT4, tCRU_INTERFACE, UINT1));
UINT1 *LINInpMD5Locate           PROTO ((UINT4, tTcpHdr *));
INT1  LINInpCheckMd5Option       PROTO ((tIpTcpHdr *, tSegment *, UINT4));
UINT1 *LINInpTCPAOLocate         PROTO ((UINT4, tTcpHdr *, UINT1 *));
INT1  LINInpCheckTCPAOOption     PROTO ((tIpTcpHdr *, tSegment *, UINT4));

/* tcpinp.c */
INT1  LliHandlePacket            PROTO ((UINT1 *));
VOID  Ip6taskTcpInput            PROTO ((tCRU_BUF_CHAIN_HEADER *, UINT2,
                                         UINT4, tCRU_INTERFACE, UINT1));
VOID  TcpReleaseParams           PROTO ((UINT4, UINT1 *, UINT1)); 
/* tcpipif.c */
INT1  LliSendSegmentToIP6        PROTO ((tSegment *, UINT4, tHlToIp6Params *));
VOID GetIp6ParmsForReset         PROTO ((tHlToIp6Params *,tIpHdr *, tIpHdr *));
VOID GetIpParmsForReset          PROTO ((t_IP_SEND_PARMS *, tIpHdr * , tIpHdr *));
INT1 LliUpdateIp6TcpHdr          PROTO ((UINT4, UINT1 *, tIpTcpHdr*));
INT1 LliUpdateIp4TcpHdr          PROTO ((UINT4, UINT1 *, tIpTcpHdr*));
INT1  LliSendSegmentToIP         PROTO ((tSegment *, UINT4, 
                                             t_IP_SEND_PARMS *));
INT1 LliGetIp6SendParms   PROTO ((tSegment *, UINT4, UINT2,tHlToIp6Params *));
t_IP_SEND_PARMS *LliGetIpSendParms   PROTO ((tSegment *, UINT4, UINT2));

/* tcpmain.c */
INT4  TcpTaskInit             PROTO (( VOID ));
VOID  TcpTaskShutDown         PROTO (( VOID ));
INT4  TcpMemInit              PROTO (( VOID ));
VOID  TcpMemFree              PROTO (( VOID ));
INT4  TcpRegisterWithLL       PROTO (( UINT4 ));
VOID  TcpProcessQMsg          PROTO (( VOID ));
VOID  TcpDeleteContext        PROTO ((UINT4));
VOID  TcpProcessVcmMsg        PROTO ((VOID));
VOID  TcpNotifyContextChange  PROTO ((UINT4, UINT4, UINT1));
INT4  TcpRegisterWithVcm      PROTO ((VOID));
INT4  TcpCreateContext        PROTO ((UINT4));
INT4  TcpLock               PROTO (( VOID ));
INT4  TcpUnLock               PROTO (( VOID ));

/* tcposm.c */
INT1  OsmIdle                 PROTO ((UINT4, UINT1));
INT1  OsmPersist              PROTO ((UINT4, UINT1));
INT1  OsmTransmit             PROTO ((UINT4, UINT1));
INT1  OsmRetransmit           PROTO ((UINT4, UINT1));
INT4  OsmHowMuchDataIsLeft    PROTO ((UINT4));
INT1 LINOsmCopyIpOptions      PROTO ((UINT1 *, UINT4));
UINT1 OsmGetIpOptlen          PROTO ((UINT4));
INT1  OsmAddOptDuringEstb     PROTO ((tTcb *, tIpTcpHdr *));
INT1  OsmSendKaPkt            PROTO ((UINT4));

/* tcposmfn.c */
UINT4 OsmComputeSegmentLen    PROTO ((tTcb *, UINT1, UINT4 *));
INT1  OsmSendSegment          PROTO ((UINT4, UINT1));
INT1  LINOsmSendReset         PROTO ((UINT4, tIpTcpHdr *));
INT1  OsmResetConn            PROTO ((UINT4));

/* tcpfsm.c */
INT1  FsmFree               PROTO ((tTcb *, tSegment *, tIpTcpHdr *));
INT1  FsmClosed             PROTO ((tTcb *, tSegment *, tIpTcpHdr *));
INT1  FsmListen             PROTO ((tTcb *, tSegment *, tIpTcpHdr *));
INT1  FsmSynsent            PROTO ((tTcb *, tSegment *, tIpTcpHdr *));
INT1  FsmSynrcvd            PROTO ((tTcb *, tSegment *, tIpTcpHdr *));
INT1  FsmEstablished        PROTO ((tTcb *, tSegment *, tIpTcpHdr *));
INT1  FsmFin1               PROTO ((tTcb *, tSegment *, tIpTcpHdr *));
INT1  FsmFin2               PROTO ((tTcb *, tSegment *, tIpTcpHdr *));
INT1  FsmClosewait          PROTO ((tTcb *, tSegment *, tIpTcpHdr *));
INT1  FsmClosing            PROTO ((tTcb *, tSegment *, tIpTcpHdr *));
INT1  FsmLastack            PROTO ((tTcb *, tSegment *, tIpTcpHdr *));
INT1  FsmTimewait           PROTO ((tTcb *, tSegment *, tIpTcpHdr *));
INT1  FsmAbort              PROTO ((tTcb *, tSegment *, tIpTcpHdr *));
INT1  FsmClosed1            PROTO ((UINT4, tIpTcpHdr *));

/* tcpdata.c */
INT1  LINFsmProcessData   PROTO ((tTcb *, tSegment *, tIpTcpHdr *));
INT1  LINFsmUpdateTcb     PROTO ((tTcb *, tIpTcpHdr *, INT4, UINT4));
INT1  FsmFrInsert         PROTO ((tTcb *, INT4, UINT4));
tFrag *FsmFrEnqueue       PROTO ((UINT4, tFrag *, tFrag *));
INT1 FsmFrCoalesce        PROTO ((tTcb *, UINT4));
tFrag *FsmFrDequeue       PROTO ((UINT4, tFrag **));

/* tcpfsmfn.c */
INT1  FsmStartWaitTmr          PROTO ((tTcb *));
INT1  FsmAbortConn             PROTO ((tTcb *, UINT1));
INT1  LINFsmInitPassiveConn    PROTO ((tTcb *, tTcb *, tTcpHdr *));

/* tcpcksum.c */
UINT2 CalcDataCksum PROTO ((UINT4, tSegment *pBuf, UINT4 u4Size, UINT4 u4ReadOffset));
UINT2 TcpPktCksum   PROTO ((UINT4, tSegment *, tIpTcpHdr *));

/* tcptimer.c */
void  TcpTmrHandleExpiry        PROTO ((void));
void  TmrInitTimer              PROTO ((tTcpTimer *, UINT1, UINT4));
void  TcpTmrSetTimer            PROTO ((tTcpTimer *, UINT4));
INT4  TcpTmrDeleteTimer         PROTO ((tTcpTimer *));
INT4 TcpTmrRestartTimer         PROTO ((tTcpTimer *, UINT4));
INT1 TmrKaTimerExpiryHandler    PROTO ((tTcpTimer *));
INT1 TmrFin2RecTimerExpiryHandler PROTO ((tTcpTimer *));
INT1  Tmr2MSLExpiryHandler        PROTO ((tTcpTimer *));
INT1  TmrRexmtExpiryHandler       PROTO ((tTcpTimer *));
INT1  TmrPersistExpiryHandler     PROTO ((tTcpTimer *));
INT1  TmrUserTmOutExpiryHandler   PROTO ((tTcpTimer *));
INT1 TmrKillTimers                PROTO ((tTcb *));
INT1 TmrStartUsertimeout          PROTO ((UINT4));
INT1 TmrStartFinWait2Rectimeout   PROTO ((UINT4));
INT4  TmrIsActiveTimer            PROTO ((tTcpTimer *));
INT1 TmrLingerTimerExpiryHandler  PROTO ((tTcpTimer *));

/* tcplow.c */
INT1 tcpSnmpConnTableEntryExists   PROTO ((UINT4 u4LocalAddress, 
                                               UINT4 u4LocalPort, 
                                               UINT4 u4RemoteAddress, 
                                               UINT4 u4RemotePort));

INT4 tcpSnmpGetConnID              PROTO ((UINT4 u4Context, 
                UINT4 u4LocalAddress, 
                                               UINT4 u4LocalPort, 
                                               UINT4 u4RemoteAddress, 
                                               UINT4 u4RemotePort));
UINT1 TcpCompareIpAddress          PROTO ((UINT1 *, UINT1 *));

/* tcpopt.c */
INT1 FsmProcessOption          PROTO((tTcb *, tIpTcpHdr *));
INT1 FsmEnableOption           PROTO((tTcb *, tTcpHdr *));
INT1 OsmAddOption              PROTO((tTcb *, tIpTcpHdr *, UINT2));
INT1 OsmGetTCBOptLen           PROTO((UINT4));
UINT1 RxmtQNodeCnt             PROTO((UINT4, tRxmt *));
UINT1 SackNodeCnt              PROTO((UINT4, tSack *));
INT1 FsmUpdateSackBitInRxmtQ   PROTO((tTcb *, tTcpHdr *, UINT4));
INT4 FsmBWExtractUpperBytes    PROTO((UINT4, tTcpHdr *));
UINT4 FsmTSExtractTSecr        PROTO((UINT4, tTcpHdr *));
INT1 OsmTurnOffSackBit         PROTO((UINT4, tRxmt *));
INT1 OsmGenerateNak            PROTO((tTcb *, INT4));
INT1 FsmSackInsert             PROTO((tTcb *, INT4, UINT4, UINT1));
INT1 FsmSackUpdate             PROTO((tTcb *, UINT1));
INT1 FsmSackUnlink             PROTO((UINT4, tSack *, tSack *));
INT1 TcpOptGenerateMD5Digest   PROTO((UINT4, tIpTcpHdr *, tSegment *, UINT1 *, 
                                      UINT1 *, UINT1));
INT1 TcpOptGenerateTCPAOMac    PROTO((UINT4, tIpTcpHdr *, tSegment *, UINT1 *,
                                      UINT1 *, UINT1 , UINT1 , UINT4, UINT1 *));
INT1 TcpGenerateTCPAOTrafficKey PROTO((UINT4 , UINT1 , UINT1 ));
INT1 TcpGenerateTCPAOSynRcvTrafficKey PROTO((tIpAddr , tIpAddr , 
                   UINT2 , UINT2 , UINT4 , UINT1* , UINT1 , UINT1*));
INT1 TcpOptFillTcpAoFields PROTO((UINT4 ,  tIpTcpHdr *, 
                                  tSegment *, UINT1 *));
/* tcprxmt.c */
INT1 FarAddToRxmtQ             PROTO((tTcb *, INT4, UINT4, UINT4));
INT1 FarUpdateRxmtQ            PROTO((tTcb *, INT4));
INT1 OsmSendSegFromRxmtQ       PROTO((tTcb *, UINT1));
tRxmt *FarTraverseRxmtQ        PROTO((tTcb *, INT4, UINT1 *));

/* IPV6 Registration and ICMP6 call back functions */
INT4 iptask_icmp_tcp           PROTO((tCRU_BUF_CHAIN_HEADER *, UINT4, 
                                            INT1, INT1));
INT4 iptask_icmp_tcp_in_cxt    PROTO ((UINT4, tCRU_BUF_CHAIN_HEADER *, 
                                       UINT4, INT1, INT1));
VOID TcpLogMsg PROTO ((UINT4 u4ContextId, UINT4 u4TrcFlag, const char *pu1ModName, const char *pu1Fmt, ... ));

/* tcptrap.c */
VOID TcpAoAuthNotifyEventTrap PROTO((tTcpAoErrLogEntry *, UINT4));


#endif
