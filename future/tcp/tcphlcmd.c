/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tcphlcmd.c,v 1.49 2017/09/13 13:34:07 siva Exp $
 *
 * Description: This file has the handle routines for cli SET/GET
 *              destined for OSPF  Module
 *
 *
 ********************************************************************/
#include "tcpinc.h"
#include "fssnmp.h"

extern INT4 SliCheckSocketConnection PROTO ((UINT4 u4ConnId));

/************************************************************************/
/*  Function Name   : HliOpen                                           */
/*  Input(s)        : pOpen - pointer to tOpen params                   */
/*                    pParms - App tp Tcp params                        */
/*  Output(s)       : None.                                             */
/*  Returns         : TCP_OK/ERR                                        */
/*
FUNCTION  
It opens a TCP connection. On succesful opening of a connection
it returns the connection Id to the application, which uses this Id
for subsequent transaction. The return value is passed through the TMO
queues to the application task. The connection Id is unique, no two 
applications can have the same Id.

CALLING CONDITION 
Whenever an application has to start the transaction,
it must form a message with open command, and give to TCP. The data_rcv()
function decodes the command and calls the corresponding function module
to handle it. This function is for command open.

GLOBAL VARIABLES AFFECTED TCBtable                                      */
/************************************************************************/
INT1
HliOpen (tOpen * pOpen, tApplicationToTcpParms * pParms)
{
    tTcb               *ptcb = NULL;
    UINT4               u4Index;
    UINT4               u4FreeIdx;
    UINT4               u4Context = 0;
#ifdef IP_WANTED
    UINT4               u4Bcastaddr;
    UINT4               u4LocalIp;
#endif
    UINT1               au1BlockSemName[4];    /* Read block sema4 name */
    UINT1               au1MainSemName[4];    /* connection MUX sema4 name */

#ifdef IP6_WANTED
    tIpAddr             IpTempAddr, *IpTempLocAddr = NULL, LocalAddr;
#endif

    u4Context = pParms->u4ContextId;
    if (gpTcpContext[u4Context] == NULL)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP", "Exit from HliOpen. Invalid context Id.\n");
        return TCPE_CANNOT_REACH;
    }

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP", "Entering HliOpen.\n");
    if (IsIP6Addr (pOpen->RemoteIp))
    {
#ifdef IP6_WANTED
        if ((IS_ADDR_MULTI (pOpen->RemoteIp)) == TRUE)
        {
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                         "TCP", "Exit from HliOpen. Returned ERR.\n");
            return TCPE_ILL_DST_ADDRESS;
        }
        if (IS_ADDR_IN6ADDRANYINIT ((pOpen->LocalIp)))
        {
            if (pOpen->u1Flag == ACTIVE_OPEN)
            {
                /* Remote address in Netorder */
                V6_HTONL (&IpTempAddr, &pOpen->RemoteIp);

                if (TCPIN6_IS_ADDR_LLOCAL (IpTempAddr))
                {
                    if ((IpTempLocAddr = ((tIpAddr *)
                                          (Ip6GetLlocalAddr
                                           (pOpen->u4IfIndex)))) == NULL)
                    {
                        TCP_MOD_TRC (u4Context,
                                     TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                                     "TCP",
                                     "Exit from HliOpen. Returned ERR.\n");
                        return TCPE_CANNOT_REACH;
                    }
                    LocalAddr = *IpTempLocAddr;
                    /*MEM_FREE (IpTempLocAddr); */
                }
                else if (Ip6SrcAddrForDestAddrInCxt (pParms->u4ContextId,
                                                     (tIp6Addr *) & IpTempAddr,
                                                     (tIp6Addr *) & LocalAddr)
                         == FAILURE)
                {
                    TCP_MOD_TRC (u4Context,
                                 TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                                 "Exit from HliOpen. Returned ERR.\n");
                    return TCPE_CANNOT_REACH;
                }
                V6_NTOHL (&pOpen->LocalIp, &LocalAddr);
                if (pOpen->u2LocalPort == UNALLOTTED_PORT)
                {
                    pOpen->u2LocalPort =
                        HliAssignPort (u4Context, pOpen->LocalIp);
                }
            }
        }
        else
        {
            if (Ip6IsOurAddrInCxt
                (pParms->u4ContextId,
                 (tIp6Addr *) (V6_HTONL (&IpTempAddr, &pOpen->LocalIp)),
                 NULL) == FAILURE)
            {
                TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                             "TCP", "Exit from HliOpen. Returned ERR.\n");
                return TCPE_ILL_SRC_ADDRESS;
            }
        }
#endif
    }
    else
    {
#ifdef IP_WANTED
        u4Bcastaddr =
            IpGetDefaultBroadcastAddr (V4_FROM_V6_ADDR (pOpen->RemoteIp));
        if (u4Bcastaddr == V4_FROM_V6_ADDR (pOpen->RemoteIp))
        {
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                         "TCP", "Exit from HliOpen. Returned ERR.\n");
            return TCPE_ILL_DST_ADDRESS;
        }
        if (V4_FROM_V6_ADDR (pOpen->LocalIp) == INADDR_ANY)
        {
            if (pOpen->u1Flag == ACTIVE_OPEN)
            {
                if (ip_src_addr_to_use_for_dest_InCxt
                    (pParms->u4ContextId,
                     V4_FROM_V6_ADDR (pOpen->RemoteIp), &u4LocalIp) == FAILURE)
                {
                    TCP_MOD_TRC (u4Context,
                                 TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                                 "Exit from HliOpen. Returned ERR.\n");
                    return TCPE_CANNOT_REACH;
                }
                V4_MAPPED_ADDR_COPY (&pOpen->LocalIp, u4LocalIp);
                if (pOpen->u2LocalPort == UNALLOTTED_PORT)
                {
                    pOpen->u2LocalPort =
                        HliAssignPort (u4Context, pOpen->LocalIp);
                }
            }
        }
        else
        {
            if (IpifIsOurAddressInCxt (pParms->u4ContextId,
                                       V4_FROM_V6_ADDR (pOpen->LocalIp))
                == FALSE)
            {
                TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                             "TCP", "Exit from HliOpen. Returned ERR.\n");
                return TCPE_ILL_SRC_ADDRESS;
            }
        }
#endif
    }
    /* Find a free TCB */
    u4FreeIdx = MAX_NUM_OF_TCB;

    if ((OsixSemTake (gTcbAllocSemId)) != OSIX_SUCCESS)
    {
        return ERR;
    }
    for (u4Index = INIT_COUNT; u4Index < MAX_NUM_OF_TCB; u4Index++)
    {
        if ((GetTCBstate (u4Index) == TCPS_FREE)
            && (FALSE == SliCheckSocketConnection (u4Index)))
        {
            u4FreeIdx = u4Index;
            break;
        }
        if (((pOpen->u2LocalPort != GetTCBlport (u4Index)) ||
             (V6_ADDR_CMP (&(pOpen->LocalIp), &(GetTCBlip (u4Index)))))
            ||
            ((pOpen->u2RemotePort != GetTCBrport (u4Index))
             && (V6_ADDR_CMP (&(pOpen->RemoteIp), &(GetTCBrip (u4Index)))))
            || (pParms->u4ContextId != GetTCBContext (u4Index)))
        {
            continue;
        }
        else
        {
            if ((GetTCBvalopts (u4Index) & TCPO_SO_REUSEADDR) != ZERO)
            {
                if (GetTCBstate (u4Index) != TCPS_TIMEWAIT)
                {
                    SetTCBerror (u4Index, TCPE_CONN_EXISTS);
                    TCP_MOD_TRC (u4Context,
                                 TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                                 "Exit from HliOpen. Returned ERR.\n");

                    OsixSemGive (gTcbAllocSemId);
                    return ERR;
                }
            }
            else
            {
                SetTCBerror (u4Index, TCPE_CONN_EXISTS);
                TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                             "TCP", "Exit from HliOpen. Returned ERR.\n");
                OsixSemGive (gTcbAllocSemId);
                return ERR;
            }
        }
    }

    if (u4FreeIdx == MAX_NUM_OF_TCB)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP", "Exit from HliOpen. Returned ERR.\n");
        OsixSemGive (gTcbAllocSemId);
        return TCPE_TOO_MANY_CONN;
    }

    GetTCBSockDesc (u4Index) = pParms->i4SockId;
    SetTCBContext (u4Index, pParms->u4ContextId);
    /* u1_free_idx is the new free TCB */
    u4Index = u4FreeIdx;
    ptcb = &TCBtable[u4Index];
#ifdef SLI_WANTED
    if (SliTcpRegisterConnid (pParms->i4SockId, u4Index) != TCP_OK)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP", "Exit from HliOpen. Returned ERR.\n");
        OsixSemGive (gTcbAllocSemId);
        return ERR;
    }
#endif
    SetTCBstate (u4Index, TCPS_CLOSED);
    OsixSemGive (gTcbAllocSemId);

    TCP_MOD_TRC (u4Context, TCP_FSM_TRC, "TCP",
                 "FSM state initialized to CLOSED for CONN ID - %d\n", u4Index);
    SetTCBvalopts (u4Index, pOpen->u4ValidOptions);

    if (GetTCBvalopts (u4Index) & TCPO_TOS)
    {
        SetTCBtos (u4Index, pOpen->u1Tos);
    }
    else
    {
        SetTCBtos (u4Index, ZERO);
    }

    if (GetTCBvalopts (u4Index) & TCPO_SECURITY)
    {
        SetTCBsecurity (u4Index, pOpen->u2Security);
    }

    /* Intialize the option pointer and the option length fields */
    SetTCBipoptlen (u4Index, ZERO);

    if (GetTCBvalopts (u4Index) & TCPO_IPOPTION)
    {
        MEMCPY ((UINT1 *) (GetTCBpipopt (u4Index) + GetTCBipoptlen (u4Index)),
                (UINT1 *) pOpen->pOptions, pOpen->u1Optlen);
        SetTCBipoptlen (u4Index,
                        (UINT1) (GetTCBipoptlen (u4Index) + pOpen->u1Optlen));
    }

    if (GetTCBvalopts (u4Index) & TCPO_TIMEOUT)
    {
        SetTCBtimeout (u4Index, pOpen->u4Timeout);
    }

    if (GetTCBvalopts (u4Index) & TCPO_ENBL_ICMP_MSGS)
    {
        gpTcpContext[u4Context]->i2IcmpMessagesEnabled = TRUE;
    }

    if (pOpen->u1Flag == PASSIVE_OPEN)
    {
        /*do passive open */
        SetTCBtype (u4Index, TCP_SERVER);
        SetTCBstate (u4Index, TCPS_LISTEN);
        TCP_MOD_TRC (u4Context, TCP_FSM_TRC, "TCP",
                     "FSM state initialized to LISTEN for CONN ID - %d\n",
                     u4Index);
        SetTCBlport (u4Index, pOpen->u2LocalPort);
        SetTCBrport (u4Index, pOpen->u2RemotePort);
        SetTCBlip (u4Index, pOpen->LocalIp);
        SetTCBrip (u4Index, pOpen->RemoteIp);
        if (IsIP6Addr (pOpen->LocalIp))
        {
            SetTCBLipAddType (u4Index, IPV6_ADD_TYPE);
        }
        else
        {
            SetTCBLipAddType (u4Index, IPV4_ADD_TYPE);
        }
        if (IsIP6Addr (pOpen->RemoteIp))
        {
            SetTCBRipAddType (u4Index, IPV6_ADD_TYPE);
        }
        else
        {
            SetTCBRipAddType (u4Index, IPV4_ADD_TYPE);
        }
        SetTCBPID (u4Index, TCP_ZERO);
        SetTCBsmss (u4Index, TCP_MAX_SEGMENT_SIZE);
        SetTCBmsgcnt (u4Index, (UINT2) pOpen->i4MaxPendingMsgs);
        SetTCBasynchndlr (u4Index, NULL);
        SetTCBpptcb (u4Index, NULL);

        SetTCBTSLastAckSent (u4Index, ZERO);
        SetTCBTSRecent (u4Index, ZERO);
        SetTCBOutOrderSeq (u4Index, ZERO);
        SetTCBSackLargestSeq (u4Index, ZERO);
        SetTCBRxmtQHdr (u4Index, ZERO);
        SetTCBSackHdr (u4Index, ZERO);
        SetTCBIfIndex (u4Index, pOpen->u4IfIndex);
        SetTCBKaMainTmOut (u4Index, TCP_KA_DEF_MAIN_TMR_TICKS);
        SetTCBKaRetransTmOut (u4Index, TCP_KA_DEF_RETRANS_TMR_TICKS);
        SetTCBKaRetransCnt (u4Index, TCP_KA_DEF_RETRIES);
        SetTCBKaRetransOverCnt (u4Index, ZERO);
        GenerateSemName ('B', (char *) au1BlockSemName);
        SetTCBTcpAoEnabled (u4Index, TCP_ZERO);
        SetIcmpIgnCtr (u4Index, TCP_ZERO);
        SetSilentAcceptCtr (u4Index, TCP_ZERO);
        if (OsixCreateSem ((const UINT1 *) au1BlockSemName,
                           TCP_ONE,
                           OSIX_WAIT,
                           &(TCBtable[u4Index].BlockSemId)) != OSIX_SUCCESS)
        {
            deallocate (ptcb);
            return ERR;
        }

        GenerateSemName ('M', (char *) au1MainSemName);
        if (OsixCreateSem ((const UINT1 *) au1MainSemName,
                           TCP_ONE,
                           OSIX_WAIT,
                           &(TCBtable[u4Index].MainSemId)) != OSIX_SUCCESS)
        {
            deallocate (ptcb);
            return ERR;
        }

        HliReplyForOpen (u4Index);

        /* The server is assumed to do a listen call for waiting for request */
    }

    else if (pOpen->u1Flag == ACTIVE_OPEN)
    {
        /* CLIENT */
        if ((TcpSync (ptcb, TCP_MIN_RECV_BUF_SIZE)) != TCP_OK)
        {
            deallocate (ptcb);
            return ERR;
        }

        if ((pOpen->u1Md5Keylen != TCP_ZERO) &&
            (pOpen->u1Md5Keylen <= TCP_MD5SIG_MAXKEYLEN) &&
            (pOpen->pMd5Key != NULL))
        {
            MEMSET (GetTCBpmd5key (u4Index), TCP_ZERO, TCP_MD5SIG_MAXKEYLEN);
            MEMCPY (GetTCBpmd5key (u4Index), pOpen->pMd5Key,
                    pOpen->u1Md5Keylen);
            SetTCBmd5keylen (u4Index, pOpen->u1Md5Keylen);
        }
        else
        {
            /* Set Md5 key length to ZERO for default case */
            SetTCBmd5keylen (u4Index, TCP_ZERO);
        }

        if (pOpen->pTcpAoMkt != NULL)
        {
            SetRcvIsn (u4Index, TCP_ZERO);
            SetTcpAoMacVerErrCtr (u4Index, TCP_ZERO);
            SetTcpAoSendSne (u4Index, TCP_ZERO);
            SetTcpAoRecvSne (u4Index, TCP_ZERO);
            SetIcmpIgnCtr (u4Index, TCP_ZERO);
            SetSilentAcceptCtr (u4Index, TCP_ZERO);

            SetTCBTcpAoEnabled (u4Index, TRUE);

            SetTCBTcpAoNoMktDisc (u4Index, TCP_ZERO);
            SetTCBTcpAoIcmpAcc (u4Index, TCP_ZERO);
            SetTCBTcpAoRcvPckKeyId (u4Index, TCP_ZERO);
            SetTCBTcpAoRcvPckRNxtKeyId (u4Index, TCP_ZERO);

            SetTCBTcpAoActiveInd (u4Index, TCPAO_MKT_INI_IND);
            SetTCBTcpAoNewInd (u4Index, TCPAO_MKT_INI_IND);
            SetTCBTcpAoBackupInd (u4Index, TCPAO_MKT_INI_IND);

            SetTcbTcpAoMkt (u4Index, pOpen->pTcpAoMkt->u1SendKeyId,
                            pOpen->pTcpAoMkt->u1RcvKeyId,
                            pOpen->pTcpAoMkt->u1ShaAlgo,
                            pOpen->pTcpAoMkt->u1ShaAlgo,
                            pOpen->pTcpAoMkt->au1Key,
                            ((UINT1) STRLEN (pOpen->pTcpAoMkt->au1Key)),
                            pOpen->pTcpAoMkt->u1TcpOptIgnore);

        }
        else
        {
            SetTCBTcpAoEnabled (u4Index, TCP_ZERO);
            SetIcmpIgnCtr (u4Index, TCP_ZERO);
            SetSilentAcceptCtr (u4Index, TCP_ZERO);
        }
        SetTCBTcpAoNoMktDisc (u4Index, (pOpen->u1TcpAoNoMktDisc));
        SetTCBTcpAoIcmpAcc (u4Index, (pOpen->u1TcpAoIcmpAcc));
        SetTCBhlicmd (u4Index, OPEN_CMD);
        SetTCBtype (u4Index, TCP_CLIENT);
        SetTCBstate (u4Index, TCPS_SYNSENT);
        TCP_MOD_TRC (u4Context, TCP_FSM_TRC, "TCP",
                     "FSM state initialized to SYNSENT for CONN ID - %d\n",
                     u4Index);
        SetTCBlport (u4Index, pOpen->u2LocalPort);
        SetTCBrport (u4Index, pOpen->u2RemotePort);
        SetTCBlip (u4Index, pOpen->LocalIp);
        SetTCBrip (u4Index, pOpen->RemoteIp);
        if (IsIP6Addr (pOpen->LocalIp))
        {
            SetTCBLipAddType (u4Index, IPV6_ADD_TYPE);
        }
        else
        {
            SetTCBLipAddType (u4Index, IPV4_ADD_TYPE);
        }
        if (IsIP6Addr (pOpen->RemoteIp))
        {
            SetTCBRipAddType (u4Index, IPV6_ADD_TYPE);
        }
        else
        {
            SetTCBRipAddType (u4Index, IPV4_ADD_TYPE);
        }
        SetTCBPID (u4Index, TCP_ZERO);
        SetTCBmsgcnt (u4Index, MAX_NUM_MESSAGES);
        SetTCBasynchndlr (u4Index, NULL);
        SetTCBpptcb (u4Index, NULL);

        SetTCBTSLastAckSent (u4Index, ZERO);
        SetTCBTSRecent (u4Index, ZERO);
        SetTCBOutOrderSeq (u4Index, ZERO);
        SetTCBSackLargestSeq (u4Index, ZERO);
        SetTCBRxmtQHdr (u4Index, ZERO);
        SetTCBSackHdr (u4Index, ZERO);
        SetTCBKaMainTmOut (u4Index, TCP_KA_DEF_MAIN_TMR_TICKS);
        SetTCBKaRetransTmOut (u4Index, TCP_KA_DEF_RETRANS_TMR_TICKS);
        SetTCBKaRetransCnt (u4Index, TCP_KA_DEF_RETRIES);
        SetTCBKaRetransOverCnt (u4Index, ZERO);
        SetTCBIfIndex (u4Index, pOpen->u4IfIndex);
        GenerateSemName ('B', (char *) au1BlockSemName);
        if (pOpen->pTcpAoMkt != NULL)
        {
            /* TCP-AO Calculate syn sent traffic key */
            TcpGenerateTCPAOTrafficKey (u4Index, pOpen->pTcpAoMkt->u1SendKeyId,
                                        TCPAO_SND_SYN_TRFKEY);
        }
        if (OsixCreateSem (au1BlockSemName,
                           TCP_ONE,
                           OSIX_WAIT,
                           &(TCBtable[u4Index].BlockSemId)) != OSIX_SUCCESS)
        {
            deallocate (ptcb);
            return ERR;
        }

        GenerateSemName ('M', (char *) au1MainSemName);
        if (OsixCreateSem (au1MainSemName,
                           TCP_ONE,
                           OSIX_WAIT,
                           &(TCBtable[u4Index].MainSemId)) != OSIX_SUCCESS)
        {
            deallocate (ptcb);
            return ERR;
        }

        TcpInitiateConnection (ptcb, pOpen);
        IncTCPstat (pParms->u4ContextId, ActiveOpens);

    }

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from HliOpen. Returned OK.\n");
    return TCP_OK;
}

/************************************************************************/
/*  Function Name   : HliRegAsyncHandler                                */
/*  Input(s)        : pAsync - Poniter Async params                     */
/*  Output(s)       : None.                                             */
/*  Returns         : TCP_OK                                            */
/*  Description     : Registers the asynchrounour handler.              */
/*  CALLING CONDITION : When the application wants to register a        */
/*                      asynchrous handler.                             */
/*  GLOBAL VARIABLES AFFECTED associated element of TCB.                */
/************************************************************************/
INT1
HliRegAsyncHandler (tRegAsyncHandler * pAsync, tApplicationToTcpParms * pParms)
{
    UINT4               u4Context;

    UNUSED_PARAM (pParms);
    u4Context = GetTCBContext (pAsync->u4ConnId);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering HliRegAsyncHandler.\n");
    SetTCBasynchndlr (pAsync->u4ConnId, pAsync->fpTcpHandleAsyncMesg);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from HliRegAsyncHandler. Returned OK.\n");
    return TCP_OK;
}

/************************************************************************/
/*  Function Name   : HliClose                                          */
/*  Input(s)        : pClose - Pointer to Close Params                  */
/*                    pParms - App tp Tcp params                        */
/*  Output(s)       : None.                                             */
/*  Returns         : TCP_OK                                            */
/*  Description     : In order to close an existing connection, the     */
/*                    user has to give an explicit close command.       */
/*                    This function implements close command.           */
/* CALLING CONDITION : It is called from data_rcv(), whenever a         */
/*                     close command is found.                          */
/* GLOBAL VARIABLES AFFECTED : Associated element of TCB.               */
/************************************************************************/
tTcpToApplicationParms *
HliClose (tClose * pClose, tApplicationToTcpParms * pParms)
{
    UINT4               u4Index;
    UINT4               u4Context;
    tTcb               *ptcb;
    tTcpToApplicationParms *pRetParms;

    u4Index = pClose->u4ConnId;
    ptcb = &TCBtable[u4Index];
    u4Context = GetTCBContext (u4Index);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP", "Entering HliClose.\n");
    if ((TakeTCBmainsem (u4Index)) != OSIX_SUCCESS)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from HliClose.  Take Main sem failed Returned NULL %x.\n");
        return (tTcpToApplicationParms *) NULL;
    }
    if (GetTCBstate (u4Index) <= TCPS_CLOSED)
    {
        GiveTCBmainsem (u4Index);
        pRetParms = pHliError (pParms, TCPE_CONN_NOT_EXIST, u4Index);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP", "Exit from HliClose. Returned %x.\n", pRetParms);
        return pRetParms;
    }

    if ((GetTCBstate (u4Index) == TCPS_LISTEN) ||
        (GetTCBstate (u4Index) == TCPS_ABORT))
    {
        GiveTCBmainsem (u4Index);
        pRetParms = pHliReplyForClose (u4Index);
        deallocate (&TCBtable[u4Index]);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from HliClose. Returned %x.\n", pRetParms);
        return pRetParms;
    }

    if ((GetTCBstate (u4Index) == TCPS_FINWAIT1) ||
        (GetTCBstate (u4Index) == TCPS_FINWAIT2))
    {
        GiveTCBmainsem (u4Index);
        pRetParms = pHliReplyForClose (u4Index);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from HliClose. Returned %x.\n", pRetParms);
        return pRetParms;

    }

    if ((GetTCBstate (u4Index) == TCPS_CLOSING) ||
        (GetTCBstate (u4Index) == TCPS_LASTACK) ||
        (GetTCBstate (u4Index) == TCPS_TIMEWAIT))
    {
        GiveTCBmainsem (u4Index);
        pRetParms = pHliError (pParms, TCPE_CONN_CLOSING, u4Index);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP", "Exit from HliClose. Returned %x.\n", pRetParms);
        return pRetParms;
    }

    if (GetTCBstate (u4Index) == TCPS_SYNSENT)
    {
        SetTCBstate (u4Index, TCPS_CLOSED);
        GiveTCBmainsem (u4Index);
        deallocate (&TCBtable[u4Index]);
        pRetParms = pHliReplyForClose (u4Index);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from HliClose. Returned %x.\n", pRetParms);
        return pRetParms;
    }

    if ((GetTCBvalopts (u4Index) & TCPO_SO_LINGER) &&
        (GetTCBLingerTmOut (u4Index) != ZERO) &&
        (OsmHowMuchDataIsLeft (u4Index) > ZERO))
    {
        osm_switch[GetTCBostate (u4Index)] (u4Index, TCPE_SEND);
        TmrInitTimer (&(ptcb->TcbLingerTimer), TCP_SO_LINGER_TIMER, u4Index);
        TcpTmrSetTimer (&(ptcb->TcbLingerTimer), GetTCBLingerTmOut (u4Index));
        TCP_MOD_TRC (u4Context, TCP_OS_RES_TRC, "TCP",
                     "Linger timer initiated and started.\n");
    }

    SetTCBflags (u4Index, TCBF_SNDFIN);    /* Send a FIN segment */
    /*Update the window variables */
    SetTCBslast (u4Index, (INT4) ((UINT4) GetTCBsuna (u4Index) +
                                  GetTCBsbcount (u4Index)));

    if (GetTCBstate (u4Index) == TCPS_ESTABLISHED)
    {
        SetTCBstate (u4Index, TCPS_FINWAIT1);
    }

    if (GetTCBstate (u4Index) == TCPS_SYNRCVD)
    {
        SetTCBstate (u4Index, TCPS_FINWAIT1);
    }

    if (GetTCBstate (u4Index) == TCPS_CLOSEWAIT)
    {
        SetTCBstate (u4Index, TCPS_LASTACK);
    }

    SetTCBflags (u4Index, TCBF_NEEDOUT);    /* Send immediately */
    SetTCBrbstart (u4Index, ZERO);
    SetTCBrbcount (u4Index, ZERO);

    osm_switch[GetTCBostate (u4Index)] (u4Index, TCPE_SEND);

    /* The normal behaviour of sockets is to return as soon as a FIN is 
     * sent to the peer. After that, TCP takes care of closing the conn
     * using the 3-way handshake. In case the user tries to bind to the
     * same port before TCP closes the connection, bind call will fail.
     * The user has to wait for a reasonable amount of time before 
     * binding to the same port. */
    SetTCBhlicmd (u4Index, ZERO);
    pRetParms = pHliReplyForClose (u4Index);

    GiveTCBBlockSem (u4Index);
    GiveTCBmainsem (u4Index);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from HliClose. Returned %x.\n", pRetParms);
    return pRetParms;
}

/************************************************************************/
/*  Function Name   : tcpgetdata                                        */
/*  Input(s)        : ptcb - Pointer to Tcb                             */
/*                    pBuf - pointer to the buffer to read into         */
/*                    u2Len - No of bytes.                              */
/*  Output(s)       : None.                                             */
/*  Returns         : data bytes                                        */
/*  Description     :                                                   */
/*  CALLING CONDITION                                                   */
/*  GLOBAL VARIABLES AFFECTED : associated element of TCB.              */
/************************************************************************/
INT4
tcpgetdata (tTcb * ptcb, UINT1 *pi1Buf, UINT2 u2Len)
{
    UINT2               u2ByteCount, u2Normal, u2Urgent;
    UINT2               u2Rbleft;
    UINT2               u2Cplen;
    UINT4               u4TcbIndex;
    UINT4               u4Context;
    UINT4               u4TempWindow = 0;

    u4TcbIndex = GET_TCB_INDEX (ptcb);
    u4Context = GetTCBContext (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering tcpgetdata.\n");

    if (GetTCBflags (u4TcbIndex) & TCBF_RUPOK)
    {
        /* If Urgent Data */
        u2Normal = (UINT2) ((UINT4) GetTCBrnext (u4TcbIndex)
                            - (UINT4) GetTCBrupseq (u4TcbIndex) - 1);
        if ((INT2) u2Normal >= ZERO)
        {
            /* urgent boundary in buffer */
            u2Urgent = (UINT2) (GetTCBrbcount (u4TcbIndex) - u2Normal);
            if (u2Len >= u2Urgent)
            {
                /* Given buffer is >= urgent bytes to be delivered */
                if (!(GetTCBvalopts (u4TcbIndex) & TCPO_OOBINLINE))
                {
                    u2Len = u2Urgent;
                }
                SetTCBflags_AND (u4TcbIndex, ~TCBF_RUPOK);
            }
        }
    }

    if (GetTCBrbcount (u4TcbIndex) == ZERO)
    {
        SetTCBReadWaiting (u4TcbIndex, TRUE);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from tcpgetdata. Returned ZERO.\n");
        return ZERO;
    }

    if (GetTCBrbstart (u4TcbIndex) >= GetTCBrbsize (u4TcbIndex))
    {
        SetTCBrbstart (u4TcbIndex, ZERO);    /* Wrap around the buffer */
    }

    u2Rbleft = (UINT2) (GetTCBrbsize (u4TcbIndex) - GetTCBrbstart (u4TcbIndex));
    u2ByteCount = (UINT2) MINIMUM (GetTCBrbcount (u4TcbIndex), u2Len);
    u2Cplen = MINIMUM (u2ByteCount, u2Rbleft);

    MEMCPY (pi1Buf, ptcb->pTcbRcvbuf + GetTCBrbstart (u4TcbIndex), u2Cplen);
    TCP_MOD_TRC (u4Context, TCP_BUF_TRC, "TCP",
                 "Copy over Buf Chain successful.\n");

    SetTCBrbcount (u4TcbIndex, (GetTCBrbcount (u4TcbIndex) - u2Cplen));
    SetTCBrbstart (u4TcbIndex, (GetTCBrbstart (u4TcbIndex) + u2Cplen));

    if (u2ByteCount > u2Cplen)
    {
        UINT1              *pi1TempBuf = pi1Buf + u2Rbleft;

        u2Cplen = (UINT2) (u2ByteCount - u2Cplen);
        if (GetTCBrbstart (u4TcbIndex) >= GetTCBrbsize (u4TcbIndex))
        {
            SetTCBrbstart (u4TcbIndex, ZERO);    /* Wrap around the buffer */
        }
        MEMCPY (pi1TempBuf, (ptcb->pTcbRcvbuf + GetTCBrbstart (u4TcbIndex)),
                u2Cplen);
        TCP_MOD_TRC (u4Context, TCP_BUF_TRC, "TCP",
                     "Buffer copy successful.\n");
        SetTCBrbcount (u4TcbIndex, (GetTCBrbcount (u4TcbIndex) - u2Cplen));
        SetTCBrbstart (u4TcbIndex, (GetTCBrbstart (u4TcbIndex) + u2Cplen));
    }

    if (GetTCBrbstart (u4TcbIndex) >= GetTCBrbsize (u4TcbIndex))
    {
        SetTCBrbstart (u4TcbIndex, ZERO);    /* Wrap around the buffer */
    }

    if (GetTCBrbcount (u4TcbIndex) == ZERO)
    {
        /* reset the PUSH flag if no more data */
        SetTCBflags_AND (u4TcbIndex, ~TCBF_PUSH);
    }

    /*Now open up the window */
    u4TempWindow = SEQCMP (GetTCBcwin (u4TcbIndex), GetTCBrnext (u4TcbIndex));
    if ((abs (u4TempWindow) < GetTCBrmss (u4TcbIndex)) &&
        /* if ZERO window, and */
        (FarGetLwsize (ptcb)))
    {
        /* and now window is nonzero */

        SetTCBflags (u4TcbIndex, TCBF_NEEDOUT);    /* then send ACK */
        osm_switch[GetTCBostate (u4TcbIndex)] (u4TcbIndex, TCPE_SEND);
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from tcpgetdata. Returned %d\n", u2ByteCount);
    return (INT4) u2ByteCount;
}

/************************************************************************/
/*  Function Name   : HliWrite                                          */
/*  Input(s)        : pWrite - pointer to Send params                   */
/*                    pParms - App to Tcp Params                        */
/*  Output(s)       : None.                                             */
/*  Returns         : TCP_OK/ERR                                        */
/*  Description     : It is called only when a user gives a send        */
/*                    command. This is strictly synchronised with the   */
/*                    user send command. It sends the data to the       */
/*                    destination.                                      */
/* CALLING CONDITION It is invoked through the user send command.       */
/* GLOBAL VARIABLES AFFECTED None                                       */
/************************************************************************/

tTcpToApplicationParms *
HliWrite (tSend * pWrite, tApplicationToTcpParms * pParms)
{
    UINT1              *pi1DataBuf = NULL;
    UINT1              *pi1TempBuf = NULL;
    tTcb               *ptcb;
    UINT4               u4SndOff;
    UINT4               u4ByteCount, u4Cplen;
    UINT4               u4Index;
    UINT4               u4Context;
    UINT4               u4RdOffset = ZERO;
    UINT4               u4WriteBufCnt = ZERO;
    tTcpToApplicationParms *pRetParms;

    u4Index = pWrite->u4ConnId;
    ptcb = &TCBtable[u4Index];
    u4Context = GetTCBContext (u4Index);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP", "Entering HliWrite.\n");

    switch (GetTCBstate (u4Index))
    {
        case TCPS_FREE:
        case TCPS_CLOSED:
        case TCPS_ABORT:
        {
            pRetParms = pHliError (pParms, TCPE_CONN_NOT_EXIST, u4Index);
            TCP_MOD_TRC (u4Context,
                         TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                         "Exit from HliWrite. Connection in State %d. Returned ERR.\n",
                         GetTCBstate (u4Index));
            return pRetParms;
        }

        case TCPS_LISTEN:
        case TCPS_SYNSENT:
        case TCPS_SYNRCVD:
        {
            pRetParms = pHliError (pParms, TCPE_INSUF_RESOURCES, u4Index);
            TCP_MOD_TRC (u4Context,
                         TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                         "Exit from HliWrite. Connection in State %d. Returned ERR.\n",
                         GetTCBstate (u4Index));
            return pRetParms;
        }

        case TCPS_FINWAIT1:
        case TCPS_FINWAIT2:
        case TCPS_CLOSING:
        case TCPS_LASTACK:
        case TCPS_TIMEWAIT:
        {
            pRetParms = pHliError (pParms, TCPE_CONN_CLOSING, u4Index);
            TCP_MOD_TRC (u4Context,
                         TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                         "Exit from HliWrite. Connection in State %d. Returned ERR.\n",
                         GetTCBstate (u4Index));
            return pRetParms;
        }
    }

    /* Current state is TCPS_ESTABLISHED or TCPS_CLOSEWAIT */

    if (pWrite->u2BufLen > GetTCBsbsize (u4Index))
    {
        pRetParms = pHliError (pParms, TCPE_BUF_OVERFLOW, u4Index);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP",
                     "Exit from HliWrite. BUFFER OVERFLOW. Returned ERR.\n");
        return pRetParms;
    }

    u4WriteBufCnt = ((pWrite->u2BufLen >
                      (GetTCBsbsize (u4Index) -
                       GetTCBsbcount (u4Index)))
                     ? ((GetTCBsbsize (u4Index) -
                         GetTCBsbcount (u4Index))) : (pWrite->u2BufLen));

    if (u4WriteBufCnt == 0)
    {
        pRetParms = pHliError (pParms, TCPE_AGAIN, u4Index);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                     "Exit from HliWrite. Required buffer not free . Returned ERR.\n");
        return pRetParms;
    }

    SetTCBIfIndex (u4Index, pWrite->u4IfIndex);

    u4SndOff = (GetTCBsbstart (u4Index) + GetTCBsbcount (u4Index)) %
        GetTCBsbsize (u4Index);

    if (pWrite->u1Flag & URGENT_DATA)
    {
        /* The urgent pointer points to the sequence number of the LAST 
           octet (not LAST+1) in a sequence of urgent data           */
        SetTCBsupseq (u4Index, (INT4) ((UINT4) GetTCBsnext (u4Index) +
                                       (u4WriteBufCnt) - TCP_ONE));
        SetTCBflags (u4Index, TCBF_SUPOK);
    }

    if (pWrite->u1Flag & PUSH_DATA)
    {
        SetTCBflags (u4Index, TCBF_SPUSH);
    }

    /* TCP never gives its buffer to Application */

    u4ByteCount = u4WriteBufCnt;
    pi1DataBuf = pWrite->pBuf;
    pi1TempBuf = pi1DataBuf;

    if (u4SndOff >= GetTCBsbsize (u4Index))
    {
        u4SndOff = ZERO;        /* Wrap around the buffer */
    }

    u4Cplen = (GetTCBsbsize (u4Index) - u4SndOff);
    u4Cplen = MINIMUM (u4Cplen, u4ByteCount);

    while (u4ByteCount > ZERO)
    {
        MEMCPY (&ptcb->pTcbSndbuf[u4SndOff], pi1TempBuf + u4RdOffset, u4Cplen);
        u4RdOffset += u4Cplen;
        u4SndOff += u4Cplen;
        SetTCBsbcount (u4Index, (GetTCBsbcount (u4Index) + u4Cplen));
        u4ByteCount -= u4Cplen;
        u4Cplen = (GetTCBsbsize (u4Index) - u4SndOff);
        u4Cplen = MINIMUM (u4Cplen, u4ByteCount);
        if (u4SndOff >= GetTCBsbsize (u4Index))
        {
            u4SndOff = ZERO;    /* Wrap around the buffer */
        }
    }

    pWrite->pBuf = NULL;
    SetTCBflags (u4Index, TCBF_NEEDOUT);

#ifndef  NO_BUFFER_SEND            /* Earlier it was DONT_BUFFER_SEND */
/*    if (pWrite->u1Flag | PUSH_DATA || pWrite->u1Flag | URGENT_DATA ||
       / * send if PUSH or URGENT or */
/*        (GetTCBsbcount (u4Index) >= GetTCBsbsize (u4Index)) ||
        (SEQCMP (GetTCBsuna (u4Index), GetTCBsnext (u4Index)) == ZERO))
    {
*/
    /* if conn is idling */
    osm_switch[GetTCBostate (u4Index)] (u4Index, TCPE_SEND);
/*    } 
 */
#else
    osm_switch[GetTCBostate (u4Index)] (u4Index, TCPE_SEND);
#endif /*NO_BUFFER_SEND */

    pRetParms = HliReplyForSend (u4Index, pWrite->pBuf, u4WriteBufCnt);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from HliWrite. Returned %x.\n", pRetParms);
    return pRetParms;
}

/************************************************************************/
/*  Function Name   : HliStatus                                         */
/*  Input(s)        : pStatus - Pointer to Status Params                */
/*                    pParms - App to Tcp params                        */
/*  Output(s)       : None.                                             */
/*  Returns         : TCP_OK                                            */
/*  Description     : It copies all the parameters of the connection,   */
/*                    taking information from TCB. It sends back the    */
/*                    all these parameters back to the application.     */
/* CALLING CONDITION : It is called every the application sends a       */
/*                     request for status.                              */
/* GLOBAL VARIABLES AFFECTED : None                                     */
/************************************************************************/

tTcpToApplicationParms *
HliStatus (tStatus * pStatus, tApplicationToTcpParms * pParms)
{
    UINT4               u4Index;
    UINT4               u4Context;
    tTcpToApplicationParms *pRetParms;

    u4Index = pStatus->u4ConnId;
    u4Context = GetTCBContext (u4Index);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP", "Entering HliStatus.\n");

    if (GetTCBstate (u4Index) <= TCPS_CLOSED)
    {
        pRetParms = pHliError (pParms, TCPE_CONN_NOT_EXIST, u4Index);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP", "Exit from HliStatus. Returned %x\n", pRetParms);
        return pRetParms;
    }

    pRetParms = HliReplyForStatus (u4Index);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from HliStatus. Returned %x\n", pRetParms);
    return pRetParms;
}

/************************************************************************/
/*  Function Name   : HliAbort                                          */
/*  Input(s)        : pAbort - pointer to Abort Params                  */
/*  Output(s)       : None                                              */
/*  Returns         : TCP_OK                                            */
/*  Description     : This command causes all pending SENDs and RECVS   */
/*                    to be aborted, the TCB to be removed, and a       */
/*                    special RESET message to be sent to the TCP on    */
/*                    the other side of the connection.                 */
/* CALLING CONDITION : It is called when the application gives an       */
/*                     abort command.                                   */
/*  GLOBAL VARIABLES AFFECTED  associated element of TCBtable.          */
/************************************************************************/

tTcpToApplicationParms *
HliAbort (tAbort * pAbort, tApplicationToTcpParms * pParms)
{
    tTcb               *ptcb;
    UINT4               u4Index;
    UINT4               u4Context;
    tTcpToApplicationParms *pRetParms;

    u4Index = pAbort->u4ConnId;
    ptcb = &TCBtable[u4Index];
    u4Context = GetTCBContext (u4Index);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP", "Entering HliAbort.\n");

    if (GetTCBstate (u4Index) == TCPS_FREE)
    {
        pRetParms = pHliError (pParms, TCPE_CONN_NOT_EXIST, u4Index);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP", "Exit from HliAbort. Returned %x.\n", pRetParms);
        return pRetParms;
    }

    if (GetTCBstate (u4Index) != TCPS_ABORT)
    {
        OsmResetConn (u4Index);    /* Send RST to other side */
    }
    pRetParms = HliReplyForAbort (u4Index);
    deallocate (ptcb);            /* delete TCB */
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from HliAbort. Returned %x.\n", pRetParms);
    return pRetParms;
}

/************************************************************************/
/*  Function Name   : HliOptions                                        */
/*  Input(s)        : pOptions - Pointer to Option Params               */
/*  Output(s)       : None.                                             */
/*  Returns         : TCP_OK/ERR                                        */
/*  Description     :                                                   */
/* CALLING CONDITION:It is called when the application issues some      */
/*                   options.                                           */
/* GLOBAL VARIABLES AFFECTED Associated element of TCBtable.            */
/************************************************************************/
tTcpToApplicationParms *
HliOptions (tOption * pOptions, tApplicationToTcpParms * pParms)
{
    UINT4               u4Index;
    UINT4               u4Context = 0;
    tTcpLingerParms     LingerParms;
    tTcpLingerParms    *pLingerParms = NULL;
    tTcb               *ptcb;
    tTcpToApplicationParms *pRetParms;
    tSliTcpAoMkt       *pTcpAoMkt = NULL;
    tSliTcpAoMkt       *pBaseTcpAoMkt = NULL;
    UINT4               u4Val = TCP_ZERO;
    UINT1               u1Len = TCP_ZERO;

    ptcb = &(TCBtable[pOptions->u4ConnId]);
    u4Index = pOptions->u4ConnId;
    u4Context = GetTCBContext (u4Index);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering HliOptions. \n");
    if (GetTCBstate (u4Index) == TCPS_FREE)
    {
        pRetParms = pHliError (pParms, TCPE_CONN_NOT_EXIST, u4Index);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP", "Exit from HliOptions. Returned %x.\n", pRetParms);
        return pRetParms;
    }

    /* We should be able to change the TCP receive buffer size even when
     * the socket is not in established state (ie Listening state etc).
     * So we are by-passing the connected check for TCP receive buff option */

    if ((pOptions->u4Option != TCPO_RCVBUF) &&
        (GetTCBstate (u4Index) != TCPS_ESTABLISHED))
    {
        pRetParms = pHliError (pParms, TCPE_INVALID_OPERATION, u4Index);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                     "TCP", "Exit from HliOptions. Returned %x.\n", pRetParms);
        return pRetParms;
    }

    switch (pOptions->u4Option)
    {
        case TCPO_KEEPALIVE:
            if (pOptions->u1Flag == SET)
            {
                if (pOptions->u8Value)
                {
                    if (!(GetTCBvalopts (u4Index) & TCPO_KEEPALIVE))
                    {
                        SetTCBvalopts (u4Index,
                                       GetTCBvalopts (u4Index) |
                                       TCPO_KEEPALIVE);
                        ptcb = &TCBtable[u4Index];
                        TmrInitTimer (&(ptcb->TcbKaTimer),
                                      TCP_KEEPALIVE_TIMER, u4Index);
                        TcpTmrSetTimer (&(ptcb->TcbKaTimer),
                                        GetTCBKaMainTmOut (u4Index));
                        TCP_MOD_TRC (u4Context, TCP_OS_RES_TRC, "TCP",
                                     "KeepAlive timer initiated and started.\n");
                    }
                }
                else
                {
                    TcpTmrDeleteTimer (&(ptcb->TcbKaTimer));
                    TCP_MOD_TRC (u4Context, TCP_OS_RES_TRC, "TCP",
                                 "KeepAlive timer stopped.\n");
                    SetTCBvalopts (pOptions->u4ConnId,
                                   GetTCBvalopts (pOptions->
                                                  u4ConnId) & ~TCPO_KEEPALIVE);
                }
            }
            pRetParms = HliReplyForOptions (pOptions->u4ConnId,
                                            pOptions->u4Option,
                                            (GetTCBvalopts
                                             (pOptions->
                                              u4ConnId) & TCPO_KEEPALIVE) ?
                                            TRUE : FALSE, sizeof (UINT4));
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from HliOptions. Returned %x.\n", pRetParms);
            return pRetParms;
        case TCPO_SO_LINGER:
            MEMSET (&LingerParms, 0, sizeof (tTcpLingerParms));
            if (pOptions->u1Flag == SET)
            {
                MEMCPY (&pLingerParms, &(pOptions->u8Value),
                        sizeof (tTcpLingerParms *));
                if (pLingerParms->u2LingerOnOff != ZERO)
                {
                    SetTCBvalopts (u4Index,
                                   GetTCBvalopts (u4Index) | TCPO_SO_LINGER);
                }
                else
                {
                    SetTCBvalopts (pOptions->u4ConnId,
                                   GetTCBvalopts (pOptions->
                                                  u4ConnId) & ~TCPO_SO_LINGER);
                }
                SetTCBLingerState (u4Index, pLingerParms->u2LingerOnOff);

                SetTCBLingerTmOut (u4Index, pLingerParms->u2LingerTimer);
            }
            else
            {
                LingerParms.u2LingerOnOff = GetTCBLingerState (u4Index);
                LingerParms.u2LingerTimer = GetTCBLingerTmOut (u4Index);
            }
            pRetParms = HliReplyForOptions (pOptions->u4ConnId,
                                            pOptions->u4Option,
                                            (UINT4) LingerParms.u2LingerOnOff,
                                            sizeof (UINT4));
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from HliOptions. Returned %x.\n", pRetParms);
            return pRetParms;

        case TCPO_SO_REUSEADDR:
            if (pOptions->u1Flag == SET)
            {
                if (pOptions->u8Value)
                {
                    SetTCBvalopts (u4Index,
                                   (GetTCBvalopts (u4Index) |
                                    TCPO_SO_REUSEADDR));
                }
                else
                {
                    SetTCBvalopts (pOptions->u4ConnId,
                                   GetTCBvalopts (pOptions->
                                                  u4ConnId) &
                                   ~TCPO_SO_REUSEADDR);
                }
            }
            pRetParms = HliReplyForOptions (pOptions->u4ConnId,
                                            pOptions->u4Option,
                                            (GetTCBvalopts
                                             (pOptions->
                                              u4ConnId) &
                                             TCPO_SO_REUSEADDR) ? TRUE :
                                            FALSE, sizeof (UINT4));
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from HliOptions. Returned %x.\n", pRetParms);
            return pRetParms;

        case TCPO_TOS:
        {
            if (pOptions->u1Flag == SET)
            {
                SetTCBtos (pOptions->u4ConnId, (UINT1) pOptions->u8Value);
            }
            pRetParms = HliReplyForOptions (pOptions->u4ConnId,
                                            pOptions->u4Option,
                                            GetTCBtos (pOptions->u4ConnId),
                                            sizeof (UINT4));
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from HliOptions. Returned %x.\n", pRetParms);
            return pRetParms;
        }

        case TCPO_SECURITY:
        {
            if (pOptions->u1Flag == SET)
            {
                SetTCBsecurity (pOptions->u4ConnId, (UINT2) pOptions->u8Value);
                SetTCBvalopts (pOptions->u4ConnId,
                               TCPO_SECURITY | GetTCBvalopts (pOptions->
                                                              u4ConnId));
                SetTCBipoptlen (pOptions->u4ConnId,
                                (UINT1) ((GetTCBipoptlen (pOptions->u4ConnId) +
                                          pOptions->u1Optlen)));
            }
            pRetParms = HliReplyForOptions (pOptions->u4ConnId,
                                            pOptions->u4Option,
                                            GetTCBsecurity (pOptions->
                                                            u4ConnId),
                                            sizeof (UINT4));
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from HliOptions. Returned %x.\n", pRetParms);
            return pRetParms;
        }

        case TCPO_UNICAST_HOPS:
        {
            if (pOptions->u1Flag == SET)
            {
                SetTCBTtl (pOptions->u4ConnId, pOptions->u1Ttl);
            }
            pRetParms = HliReplyForOptions (pOptions->u4ConnId,
                                            pOptions->u4Option,
                                            GetTCBTtl (pOptions->u4ConnId),
                                            sizeof (UINT4));
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from HliOptions. Returned %x.\n", pRetParms);
            return pRetParms;
        }
        case TCPO_IPOPTION:
        {
            if (pOptions->u1Flag == SET)
            {

                MEMCPY (&pLingerParms, &(pOptions->u8Value), sizeof (UINT1 *));
                if ((pLingerParms == NULL) || (pOptions->u1Optlen == ZERO))
                {
                    SetTCBipoptlen (pOptions->u4ConnId, ZERO);
                    SetTCBvalopts (pOptions->u4ConnId,
                                   ~TCPO_IPOPTION &
                                   GetTCBvalopts (pOptions->u4ConnId));
                }
                else
                {
                    MEMCPY (&pLingerParms, &(pOptions->u8Value),
                            sizeof (UINT1 *));
                    MEMCPY ((GetTCBpipopt (pOptions->u4ConnId) +
                             GetTCBipoptlen (pOptions->u4ConnId)), pLingerParms,
                            pOptions->u1Optlen);
                    SetTCBipoptlen (pOptions->u4ConnId,
                                    (UINT1) ((GetTCBipoptlen
                                              (pOptions->u4ConnId) +
                                              pOptions->u1Optlen)));
                    SetTCBvalopts (pOptions->u4ConnId,
                                   TCPO_IPOPTION | GetTCBvalopts (pOptions->
                                                                  u4ConnId));
                }
            }
            pRetParms = HliReplyForOptions (pOptions->u4ConnId,
                                            pOptions->u4Option,
                                            PTR_TO_U4 (GetTCBpipopt (pOptions->
                                                                     u4ConnId)),
                                            GetTCBipoptlen (pOptions->
                                                            u4ConnId));
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from HliOptions. Returned %x.\n", pRetParms);
            return pRetParms;
        }

        case TCPO_TIMEOUT:
        {
            pRetParms = HliReplyForOptions (pOptions->u4ConnId,
                                            pOptions->u4Option,
                                            GetTCBrexmt (pOptions->
                                                         u4ConnId),
                                            sizeof (UINT4));
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from HliOptions. Returned %x.\n", pRetParms);
            return pRetParms;
        }

        case TCPO_NODELAY:
        {
            if (pOptions->u1Flag == SET)
            {
                if (pOptions->u8Value)
                {
                    SetTCBvalopts (pOptions->u4ConnId,
                                   TCPO_NODELAY |
                                   GetTCBvalopts (pOptions->u4ConnId));
                }
                else
                {
                    SetTCBvalopts (pOptions->u4ConnId,
                                   ~TCPO_NODELAY &
                                   GetTCBvalopts (pOptions->u4ConnId));
                }
            }
            pRetParms = HliReplyForOptions (pOptions->u4ConnId,
                                            pOptions->u4Option,
                                            (GetTCBvalopts
                                             (pOptions->
                                              u4ConnId) & TCPO_NODELAY) ?
                                            TRUE : FALSE, sizeof (UINT4));
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from HliOptions. Returned %x.\n", pRetParms);
            return pRetParms;
        }

        case TCPO_ASY_REPORT:
        {
            if (pOptions->u1Flag == SET)
            {
                if (pOptions->u8Value)
                {
                    SetTCBvalopts (pOptions->u4ConnId,
                                   TCPO_ASY_REPORT |
                                   GetTCBvalopts (pOptions->u4ConnId));
                }
                else
                {
                    SetTCBvalopts (pOptions->u4ConnId,
                                   ~TCPO_ASY_REPORT &
                                   GetTCBvalopts (pOptions->u4ConnId));
                }
            }
            pRetParms = HliReplyForOptions (pOptions->u4ConnId,
                                            pOptions->u4Option,
                                            (GetTCBvalopts
                                             (pOptions->
                                              u4ConnId) & TCPO_ASY_REPORT)
                                            ? TRUE : FALSE, sizeof (UINT4));
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from HliOptions. Returned %x.\n", pRetParms);
            return pRetParms;
        }

        case TCPO_ENBL_ICMP_MSGS:
        {
            if (pOptions->u1Flag == SET)
            {
                if (pOptions->u8Value)
                {
                    SetTCBvalopts (pOptions->u4ConnId,
                                   TCPO_ENBL_ICMP_MSGS |
                                   GetTCBvalopts (pOptions->u4ConnId));
                    gpTcpContext[u4Context]->i2IcmpMessagesEnabled = TRUE;
                }
                else
                {
                    SetTCBvalopts (pOptions->u4ConnId,
                                   ~TCPO_ENBL_ICMP_MSGS &
                                   GetTCBvalopts (pOptions->u4ConnId));
                    gpTcpContext[u4Context]->i2IcmpMessagesEnabled = FALSE;
                }
            }
            pRetParms = HliReplyForOptions (pOptions->u4ConnId,
                                            pOptions->u4Option,
                                            (GetTCBvalopts
                                             (pOptions->
                                              u4ConnId) &
                                             TCPO_ENBL_ICMP_MSGS) ? TRUE :
                                            FALSE, sizeof (UINT4));
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from HliOptions. Returned %x.\n", pRetParms);
            return pRetParms;
        }

        case TCPO_MSS:
        {
            if (pOptions->u1Flag == GET)
            {
                pRetParms = HliReplyForOptions (pOptions->u4ConnId,
                                                pOptions->u4Option,
                                                (GetTCBsmss
                                                 (pOptions->u4ConnId)),
                                                sizeof (UINT4));
                TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                             "Exit from HliOptions. Returned %x.\n", pRetParms);
                return pRetParms;
            }
            break;
        }

        case TCPO_SENDBUF:
        {

            if (pOptions->u1Flag == SET)
            {
                if (TcpChangeSendBufSize (pOptions->u4ConnId,
                                          (UINT4) pOptions->u8Value) != TCP_OK)
                {
                    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                                 "Cannot change Sender buffer size\n");
                    return (NULL);
                }
            }

            pRetParms = HliReplyForOptions (pOptions->u4ConnId,
                                            pOptions->u4Option,
                                            (GetTCBsbsize
                                             (pOptions->u4ConnId)),
                                            sizeof (UINT4));
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from HliOptions. Returned %x.\n", pRetParms);
            return pRetParms;
        }

        case TCPO_RCVBUF:
        {
            if (pOptions->u1Flag == SET)
            {
                if (TcpChangeRecvBufSize (pOptions->u4ConnId,
                                          (UINT4) pOptions->u8Value) != TCP_OK)
                {
                    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                                 "Cannot change receive buffer size\n");
                    return (NULL);
                }
            }

            pRetParms = HliReplyForOptions (pOptions->u4ConnId,
                                            pOptions->u4Option,
                                            (GetTCBrbsize
                                             (pOptions->u4ConnId)),
                                            sizeof (UINT4));
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from HliOptions. Returned %x.\n", pRetParms);
            return pRetParms;
        }

        case TCPO_OOBINLINE:
        {
            if (pOptions->u1Flag == SET)
            {
                if (pOptions->u8Value)
                {
                    SetTCBvalopts (pOptions->u4ConnId,
                                   TCPO_OOBINLINE |
                                   GetTCBvalopts (pOptions->u4ConnId));
                }
                else
                {
                    SetTCBvalopts (pOptions->u4ConnId,
                                   ~TCPO_OOBINLINE &
                                   GetTCBvalopts (pOptions->u4ConnId));
                }
            }
            pRetParms = HliReplyForOptions (pOptions->u4ConnId,
                                            pOptions->u4Option,
                                            (GetTCBvalopts
                                             (pOptions->
                                              u4ConnId) & TCPO_OOBINLINE) ?
                                            TRUE : FALSE, sizeof (UINT4));
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from HliOptions. Returned %x.\n", pRetParms);
            return pRetParms;
        }

        case TCPO_MD5SIG:
        {
            if (pOptions->u1Flag == SET)
            {
                if (GetTCBmd5keylen (pOptions->u4ConnId) == TCP_ZERO)
                {
                    /* Md5 option was not enabled during conn open, 
                     * hence no change in md5 settings allowed */
                    TCP_MOD_TRC
                        (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                         "TCP", "Exit from HliOptions. Returned ERR.\
                         MD5 option not set for Conn - %d\n", pOptions->u4ConnId);
                    return (NULL);
                }

                if ((pOptions->u1Optlen != TCP_ZERO) &&
                    (pOptions->u1Optlen <= TCP_MD5SIG_MAXKEYLEN))
                {
                    MEMSET (GetTCBpmd5key (pOptions->u4ConnId), TCP_ZERO,
                            TCP_MD5SIG_MAXKEYLEN);
                    MEMCPY (&pLingerParms, &(pOptions->u8Value),
                            sizeof (UINT1 *));
                    MEMCPY (GetTCBpmd5key (pOptions->u4ConnId), pLingerParms,
                            pOptions->u1Optlen);
                    SetTCBmd5keylen (pOptions->u4ConnId, pOptions->u1Optlen);
                }
                else
                {
                    SetTCBmd5keylen (pOptions->u4ConnId, TCP_ZERO);
                }
            }
            pRetParms = HliReplyForOptions
                (pOptions->u4ConnId, pOptions->u4Option,
                 PTR_TO_U4 (GetTCBpmd5key (pOptions->u4ConnId)),
                 GetTCBmd5keylen (pOptions->u4ConnId));

            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from HliOptions. Returned %x.\n", pRetParms);
            return pRetParms;
        }
        case TCPO_AO:
        {
            if (pOptions->u1Flag == SET)
            {
                /*Todo check for AO option already being set in TCB */
                if (GetTCBTcpAoEnabled (pOptions->u4ConnId) == TCP_ZERO)
                {
                    /* AO option was not enabled during conn open, 
                     * hence no change in AO settings allowed */
                    TCP_MOD_TRC
                        (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                         "TCP", "Exit from HliOptions. Returned ERR.\
                             AO option not set for Conn - %d\n", pOptions->u4ConnId);

                    return (NULL);
                }

                if ((pOptions->u1Optlen != TCP_ZERO) &&
                    (pOptions->u1Optlen <= sizeof (tSliTcpAoMkt)))
                {
                    MEMCPY (&pBaseTcpAoMkt, &(pOptions->u8Value),
                            sizeof (tSliTcpAoMkt *));
                    pTcpAoMkt = (tSliTcpAoMkt *) (pBaseTcpAoMkt);
                    SetTcbTcpAoMkt (pOptions->u4ConnId, pTcpAoMkt->u1SendKeyId,
                                    pTcpAoMkt->u1RcvKeyId, pTcpAoMkt->u1ShaAlgo,
                                    pTcpAoMkt->u1ShaAlgo, pTcpAoMkt->au1Key,
                                    ((UINT1) STRLEN (pTcpAoMkt->au1Key)),
                                    pTcpAoMkt->u1TcpOptIgnore);

                    /* Calculate syn sent traffic key */
                    TcpGenerateTCPAOTrafficKey (pOptions->u4ConnId,
                                                pTcpAoMkt->u1SendKeyId,
                                                TCPAO_SND_SYN_TRFKEY);
                    /* if recv initial sequence number is available calculate send & rev traffic key */
                    if (GetRcvIsn (pOptions->u4ConnId))
                    {
                        TcpGenerateTCPAOTrafficKey (u4Index,
                                                    pTcpAoMkt->u1SendKeyId,
                                                    TCPAO_SND_TRFKEY);
                        TcpGenerateTCPAOTrafficKey (u4Index,
                                                    pTcpAoMkt->u1SendKeyId,
                                                    TCPAO_RCV_TRFKEY);
                    }

                }
                else
                {
                    /* if TCP-AO is already configured will not allow unconfiguring it */
                    TCP_MOD_TRC
                        (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                         "TCP", "Exit from HliOptions. Returned ERR.\
                                 AO option unconfiguring not allowed for existing Conn - %d\n", pOptions->u4ConnId);

                    return (NULL);
                }
            }
            if (GetTCBTcpAoEnabled (pOptions->u4ConnId))
            {
                u4Val = PTR_TO_U4 (&(GetMktByIndx (pOptions->u4ConnId,
                                                   (GetTCBTcpAoActiveInd
                                                    (pOptions->u4ConnId)))));
                u1Len = sizeof (UINT4);
            }

            pRetParms = HliReplyForOptions
                (pOptions->u4ConnId, pOptions->u4Option, u4Val, u1Len);

            TCP_MOD_TRC
                (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from HliOptions. Returned %x.\n", pRetParms);
            return pRetParms;
        }

        case TCPO_AO_MKTMCH:
        {
            if (pOptions->u1Flag == SET)
            {

                if ((pOptions->u1Optlen != TCP_ZERO) &&
                    (pOptions->u1Optlen <= sizeof (UINT4)))
                {
                    SetTCBTcpAoNoMktDisc (u4Index, ((UINT1) pOptions->u8Value));
                }
                else
                {
                    /* Invalid Length for no mkt match packet discard config */
                    TCP_MOD_TRC
                        (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                         "TCP", "Exit from HliOptions. Returned ERR.\
                                 AO Invalid Length field for no mkt-match packet discard  - %d\n", pOptions->u4ConnId);

                    return (NULL);
                }
            }
            pRetParms = HliReplyForOptions
                (pOptions->u4ConnId, pOptions->u4Option,
                 ((UINT4) GetTCBTcpAoNoMktDisc (u4Index)), sizeof (UINT4));

            TCP_MOD_TRC
                (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from HliOptions. Returned %x.\n", pRetParms);
            return pRetParms;
        }
        case TCPO_AO_ICMP:
        {
            if (pOptions->u1Flag == SET)
            {

                if ((pOptions->u1Optlen != TCP_ZERO) &&
                    (pOptions->u1Optlen <= sizeof (UINT4)))
                {
                    SetTCBTcpAoIcmpAcc (u4Index, ((UINT1) pOptions->u8Value));
                }
                else
                {
                    /* Invalid Length for no mkt match packet discard config */
                    TCP_MOD_TRC
                        (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                         "TCP", "Exit from HliOptions. Returned ERR.\
                                 AO Invalid Length field for tcp-ao icmp-accept config  - %d\n", pOptions->u4ConnId);

                    return (NULL);
                }
            }
            pRetParms = HliReplyForOptions
                (pOptions->u4ConnId, pOptions->u4Option,
                 ((UINT4) GetTCBTcpAoIcmpAcc (u4Index)), sizeof (UINT4));

            TCP_MOD_TRC
                (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from HliOptions. Returned %x.\n", pRetParms);
            return pRetParms;
        }
    }                            /* switch option */
    pRetParms = pHliError (pParms, SLI_ENOPROTOOPT, u4Index);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC, "TCP",
                 "Exit from HliOptions. Returned NOT_OK.\n");
    return (tTcpToApplicationParms *) pRetParms;
}

/************************************************************************/
/*  Function Name   : TcpSliReceiveData                                 */
/*  Input(s)        : pParms - App to Tcp parameters                    */
/*                    pRetParms - Tcp to App Parameters                 */
/*  Output(s)       : None                                              */
/*  Returns         : TCP_OK/Error code                                 */
/*  Description     : Reads the data for the connection                 */
/*  CALLING CONDITION : Called by SLI for non-blocking recv             */
/* GLOBAL VARIABLES AFFECTED :                                          */
/************************************************************************/
INT4
TcpSliReceiveData (tApplicationToTcpParms * pParms,
                   tTcpToApplicationParms * pRetParms)
{
    tReceive            receive;
    INT4                i4Rd;
    UINT4               u4Index;
    UINT4               u4Context;
    UINT1               u1Flag;
    UINT4               u4RetVal = OSIX_FAILURE;

#ifdef SLI_WANTED
    /* Variables required for Non-blocking checking */
    INT4                i4SocketDesc;
    tSdtEntry          *pSockEntry;
#endif
    BOOL1               bBlockLockStatus = OSIX_FALSE;

    i4Rd = ZERO;
    receive = pParms->cmdtype.receive;
    u4Index = receive.u4ConnId;
    u4Context = GetTCBContext (u4Index);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TcpSliReceiveData.\n");
    switch (GetTCBstate (u4Index))
    {

        case TCPS_FREE:
        case TCPS_CLOSED:
        {
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from TcpSliReceiveData. Returned TCPE_CONN_NOT_EXIST\n");
            return TCPE_CONN_NOT_EXIST;
        }

        case TCPS_ABORT:
        {
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from TcpSliReceiveData. Returned %d\n",
                         GetTCBerror (u4Index));
            return GetTCBerror (u4Index);
        }

        case TCPS_FINWAIT1:
        case TCPS_FINWAIT2:
        case TCPS_CLOSING:
        case TCPS_LASTACK:
        case TCPS_TIMEWAIT:
        {
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from TcpSliReceiveData. Returned TCPE_CONN_CLOSING\n");
            return TCPE_CONN_CLOSING;
        }

        case TCPS_LISTEN:
        case TCPS_SYNSENT:
        case TCPS_SYNRCVD:
        {
            TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from TcpSliReceiveData. Returned TCPE_INSUF_RESOURCES\n");
            return TCPE_INSUF_RESOURCES;
        }
    }

    do
    {
        /* Current state is TCPS_ESTABLISHED or TCPS_CLOSEWAIT */
        if (GetTCBflags (u4Index) & TCBF_RUPOK)
        {
            /*Put the return flag as URG and copy to user buffer */
            i4Rd = tcpgetdata (&TCBtable[u4Index],
                               receive.pBuf, receive.u2BufLen);
            u1Flag = URGENT_DATA;
        }
        else
        {
            i4Rd = tcpgetdata (&TCBtable[u4Index],
                               receive.pBuf, receive.u2BufLen);
            u1Flag = ZERO;
        }

        if (i4Rd == ZERO)
        {                        /* Check if Non-blocking option is set */
#ifdef SLI_WANTED
            i4SocketDesc = GetTCBSockDesc (u4Index);
            pSockEntry = (tSdtEntry *) SliGetSockEntry (i4SocketDesc);
            if (pSockEntry == NULL)
            {
                TCP_MOD_TRC (u4Context,
                             TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                             "TCP",
                             "Exit from TcpSliReceiveData. Socket does not exist\n");
                if (bBlockLockStatus == OSIX_TRUE)
                {
                    GiveTCBBlockSem (u4Index);
                }
                return TCP_NOT_OK;
            }
            if (pSockEntry->u1FcntlOpns == FCNTL_NOBLK)
            {
                TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                             "Exit from TcpSliReceiveData. Returned EWOULDBLOCK\n");
                switch (GetTCBstate (u4Index))
                {
                    case TCPS_CLOSEWAIT:
                        pRetParms->u1Cmd = RECEIVE_CMD;
                        pRetParms->u4ConnId = u4Index;
                        pRetParms->cmdtype.receive.u1Flag = 0;
                        pRetParms->cmdtype.receive.u2BufLen = 0;
                        pRetParms->cmdtype.receive.pBuf = receive.pBuf;
                        if (bBlockLockStatus == OSIX_TRUE)
                        {
                            GiveTCBBlockSem (u4Index);
                        }

                        return TCP_OK;

                    case TCPS_FINWAIT1:
                    case TCPS_FINWAIT2:
                    case TCPS_CLOSING:
                    case TCPS_LASTACK:
                    case TCPS_TIMEWAIT:
                    {
                        if (bBlockLockStatus == OSIX_TRUE)
                        {
                            GiveTCBBlockSem (u4Index);
                        }

                        return TCPE_CONN_CLOSING;
                    }
                    default:
                    {
                        if (bBlockLockStatus == OSIX_TRUE)
                        {
                            GiveTCBBlockSem (u4Index);
                        }
                        return TCPE_WOULDBLOCK;
                    }
                }
            }
#endif
            /* Checking State */
            switch (GetTCBstate (u4Index))
            {
                case TCPS_FINWAIT1:
                case TCPS_FINWAIT2:
                case TCPS_CLOSING:
                case TCPS_LASTACK:
                case TCPS_TIMEWAIT:
                case TCPS_ABORT:
                {
                    SetTCBReadWaiting (u4Index, FALSE);
                    if (bBlockLockStatus == OSIX_TRUE)
                    {
                        GiveTCBBlockSem (u4Index);
                    }

                    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                                 "Exit from TcpSliReceiveData. Returned TCPE_CONN_CLOSING\n");
                    return TCPE_CONN_CLOSING;
                }
                case TCPS_CLOSEWAIT:
                {
                    SetTCBReadWaiting (u4Index, FALSE);
                    if (bBlockLockStatus == OSIX_TRUE)
                    {
                        GiveTCBBlockSem (u4Index);
                        bBlockLockStatus = OSIX_FALSE;
                    }

                    break;
                }
            }

            /* State OK, have to block on data */
            SetTCBhlicmd (u4Index, RECEIVE_CMD);
            SetTCBTcpToAppParms (u4Index, (tTcpToApplicationParms *) pParms);

            GiveTCBmainsem (u4Index);
            SetTCBReadWaiting (u4Index, TRUE);
            if (bBlockLockStatus == OSIX_FALSE)
            {
                u4RetVal = TakeTCBBlockSem (u4Index);
                bBlockLockStatus = OSIX_TRUE;
                if (u4RetVal != OSIX_SUCCESS)
                {
                    SetTCBReadWaiting (u4Index, FALSE);
                    if ((TakeTCBmainsem (u4Index)) != OSIX_SUCCESS)
                    {
                        return TCP_NOT_OK;
                    }
                    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                                 "Exit from TcpSliReceiveData. Returned TCPE_CONN_NOT_EXIST\n");
                    return TCPE_CONN_NOT_EXIST;
                }
            }
            /* Check if the state has changed */
            switch (GetTCBstate (u4Index))
            {
                case TCPS_FREE:
                case TCPS_CLOSED:
                {
                    SetTCBReadWaiting (u4Index, FALSE);
                    GiveTCBBlockSem (u4Index);
                    bBlockLockStatus = OSIX_FALSE;

                    if ((TakeTCBmainsem (u4Index)) != OSIX_SUCCESS)
                    {
                        return TCP_NOT_OK;
                    }
                    TCP_MOD_TRC (u4Context,
                                 TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                                 "TCP",
                                 "Exit from TcpSliReceiveData. Returned TCPE_CONN_NOT_EXIST\n");
                    return TCPE_CONN_NOT_EXIST;
                }

                case TCPS_ABORT:
                {
                    SetTCBReadWaiting (u4Index, FALSE);
                    GiveTCBBlockSem (u4Index);
                    bBlockLockStatus = OSIX_FALSE;
                    if ((TakeTCBmainsem (u4Index)) != OSIX_SUCCESS)
                    {
                        return TCP_NOT_OK;
                    }
                    TCP_MOD_TRC (u4Context,
                                 TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                                 "TCP",
                                 "Exit from TcpSliReceiveData. Returned %d\n",
                                 GetTCBerror (u4Index));
                    return GetTCBerror (u4Index);
                }

                case TCPS_FINWAIT1:
                case TCPS_FINWAIT2:
                case TCPS_CLOSING:
                case TCPS_LASTACK:
                case TCPS_TIMEWAIT:
                {
                    SetTCBReadWaiting (u4Index, FALSE);
                    GiveTCBBlockSem (u4Index);
                    bBlockLockStatus = OSIX_FALSE;
                    if ((TakeTCBmainsem (u4Index)) != OSIX_SUCCESS)
                    {
                        return TCP_NOT_OK;
                    }
                    TCP_MOD_TRC (u4Context,
                                 TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                                 "TCP",
                                 "Exit from TcpSliReceiveData. Returned TCPE_CONN_CLOSING\n");
                    return TCPE_CONN_CLOSING;
                }

                case TCPS_LISTEN:
                case TCPS_SYNSENT:
                case TCPS_SYNRCVD:
                {
                    SetTCBReadWaiting (u4Index, FALSE);
                    GiveTCBBlockSem (u4Index);
                    bBlockLockStatus = OSIX_FALSE;
                    if ((TakeTCBmainsem (u4Index)) != OSIX_SUCCESS)
                    {
                        return TCP_NOT_OK;
                    }
                    TCP_MOD_TRC (u4Context,
                                 TCP_ENTRY_EXIT_TRC | TCP_ALL_FAIL_TRC,
                                 "TCP",
                                 "Exit from TcpSliReceiveData. Returned TCPE_INSUF_RESOURCES\n");
                    return TCPE_INSUF_RESOURCES;
                }
            }
            /* State is still Established or CloseWait */
            if ((TakeTCBmainsem (u4Index)) != OSIX_SUCCESS)
            {
                SetTCBReadWaiting (u4Index, FALSE);
                GiveTCBBlockSem (u4Index);
                bBlockLockStatus = OSIX_FALSE;
                return TCP_NOT_OK;
            }
        }
    }
    while ((i4Rd == ZERO)
           && ((GetTCBrbcount (u4Index) != ZERO)
               || (GetTCBstate (u4Index) != TCPS_CLOSEWAIT)));

    pRetParms->u1Cmd = RECEIVE_CMD;
    pRetParms->u4ConnId = u4Index;
    pRetParms->cmdtype.receive.u1Flag = u1Flag;
    pRetParms->cmdtype.receive.u2BufLen = (UINT2) i4Rd;
    pRetParms->cmdtype.receive.pBuf = receive.pBuf;
    SetTCBReadWaiting (u4Index, FALSE);
    GiveTCBBlockSem (u4Index);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from TcpSliReceiveData. Returned OK.\n");
    return TCP_OK;

}

/*************************************************************************/
/*  Function Name   : TcpGetSelectStatus                                 */
/*  Input(s)        : rdconids, wrconids, exconids                       */
/*                    u4Rdsize, u4Wrsize, u4Exsize                       */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK                                             */
/*  CallingCondition: Called by SLI when select is called with zero      */
/*                    timeout.                                           */
/* GLOBAL VARIABLES AFFECTED : None.                                     */
/*************************************************************************/
INT1
TcpGetSelectStatus (UINT4 *rdconids, UINT4 *wrconids, UINT4 *exconids,
                    UINT4 *u4Rdsize, UINT4 *u4Wrsize, UINT4 *u4Exsize)
{

    UINT1               u1State;
    UINT4               u4Index;
    UINT4               u4Raindex;
    UINT4               u4Waindex;
    UINT4               u4Eaindex;
#ifdef SLI_WANTED
    UINT4               u4PendingMsgs;
#endif
    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering TcpGetSelectStatus. \n");

    /* for each element in rdconids upto read size begin if TCB
       corresponding to connid is in listen state check whether any
       new connections are available or if it is established or close
       wait state and it has data in its queue add it to read  arr end */

    u4Raindex = ZERO;

    for (u4Index = INIT_COUNT; u4Index < *u4Rdsize; u4Index++)
    {
        u1State = GetTCBstate (rdconids[u4Index]);
#ifdef SLI_WANTED
        if (u1State == TCPS_LISTEN)
        {
            AcceptMesgCount (rdconids[u4Index], &u4PendingMsgs);
            if (u4PendingMsgs > ZERO)
            {
                rdconids[u4Raindex] = rdconids[u4Index];
                u4Raindex++;
            }
        }
#endif
        if (((u1State == TCPS_ESTABLISHED)
             && (GetTCBrbcount (rdconids[u4Index]) > ZERO))
            || (u1State == TCPS_CLOSEWAIT))
        {
            rdconids[u4Raindex] = rdconids[u4Index];
            u4Raindex++;
        }
    }

    /*for each element in write conids upto write size begin if TCB 
       corresponding to connid is in established or close wait state 
       and if its write buffer is not full add it to write  arr end */

    u4Waindex = ZERO;
    for (u4Index = INIT_COUNT; u4Index < *u4Wrsize; u4Index++)
    {
        u1State = GetTCBstate (wrconids[u4Index]);
        if (((u1State == TCPS_ESTABLISHED) ||
             (u1State == TCPS_CLOSEWAIT)) &&
            ((GetTCBsbcount (wrconids[u4Index]) <
              GetTCBsbsize (wrconids[u4Index]))))
        {
            wrconids[u4Waindex] = wrconids[u4Index];
            u4Waindex++;
        }
    }

    /*for each element in except conids upto except size begin 
       if TCB has the rupok flag set add it to except  arr end */

    u4Eaindex = ZERO;
    for (u4Index = INIT_COUNT; u4Index < *u4Exsize; u4Index++)
    {
        u1State = GetTCBstate (exconids[u4Index]);
        if ((u1State == TCPS_ESTABLISHED) || (u1State == TCPS_CLOSEWAIT))
        {
            exconids[u4Eaindex] = exconids[u4Index];
            u4Eaindex++;
        }
    }
    *u4Rdsize = u4Raindex;
    *u4Wrsize = u4Waindex;
    *u4Exsize = u4Eaindex;
    TCP_MOD_TRC (TCP_INVALID_CONTEXT_ID, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from TcpGetSelectStatus. Returned OK.\n");
    return (TCP_OK);
}
