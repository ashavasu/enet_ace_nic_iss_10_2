/** $Id: tcposm.c,v 1.22 2016/07/05 08:22:23 siva Exp $ */

/* SOURCE FILE HEADER
 *
 * ----------------------------------------------------------------------------
 *  FILE NAME             : tcposm.c
 * 
 *  PRINCIPAL AUTHOR      : Ramesh Kumar
 *
 *  SUBSYSTEM NAME        : TCP
 *
 *  MODULE NAME           : Output State Machine
 *
 *  LANGUAGE              : C
 *
 *  TARGET ENVIRONMENT    : Any
 *
 *  DATE OF FIRST RELEASE :
 * 
 *  DESCRIPTION           : This file contains functions corresponding to each
 *                          Output State.
 *
 * ---------------------------------------------------------------------------
 *
 *
 *  CHANGE RECORD :
 *
 * ---------------------------------------------------------------------------
 *   VERSION        AUTHOR/DATE           DESCRIPTION OF CHANGE
 * ---------------------------------------------------------------------------
 *
 *                  Sri. Chellapa         Changed all sequence no. Comparisons
 *                  13/05/96              to use SEQCMP macro.   
 * ---------------------------------------------------------------------------
 *                  Vinay/29.11.99       +Modified OsmRetransmit() to transmit
 *                                        the non-SACKed segments from the 
 *                                        retransmission queue. It is placed 
 *                                        under the compilation switch 
 *                                        ST_ENH.
 *                                       +Renamed LIN_osm_put_rcvr_mss() to 
 *                                        OsmAddOptDuringEstb(). Modified it to
 *                                        add options during connection 
 *                                        establishment. It is placed under the 
 *                                        compilation switch ST_ENH.
 * ---------------------------------------------------------------------------
 */

#include "tcpinc.h"

/*
OSM is message driven.
A message specifies an action that is required, while a state 
specifies the current status of the connection.
*/
/*************************************************************************/
/*  Function Name   : OsmIdle                                            */
/*  Input(s)        : u4TcbIndex - Index to Tcb table                    */
/*                    u1Event - Event to OSM                             */
/*  Output(s)       : None                                               */
/*  Returns         : TCP_OK                                             */
/*  Description     :                                                    */
/*  CallingCondition:                                                    */
/*  GLOBAL VARIABLES AFFECTED None                                       */
/*************************************************************************/
INT1
OsmIdle (UINT4 u4TcbIndex, UINT1 u1Event)
{
    UINT4               u4Context;

    u4Context = GetTCBContext (u4TcbIndex);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_OSM_TRC, "TCP",
                 "Entering OsmIdle for CONN ID - %d\n", u4TcbIndex);
    /*check if you have something to send */
    if (u1Event == TCPE_SEND)
    {
        OsmTransmit (u4TcbIndex, TCPE_SEND);
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit form OsmIdle. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : OsmPersist                                         */
/*  Input(s)        : u4TcbIndex - Index to Tcb table                    */
/*                    u1Event - Event to OSM                             */
/*  Output(s)       : None.                                              */
/*  Returns         : TCP_OK/ERR                                         */
/*  Description     : 
It handles events when the remote receiver advertises a zero window.
The sender probes the receiver periodically by sending a segment. The
receiver will return its latest window size in the ACK.
   The function invokes the SendSegment function with RETRANSMIT event.
The SendSegment takes care of sending ZERO sized segment. If the segment is txed
successfully then the function proceeds by doubling the persist time. Finally
it sets the PERSIST timer and returns the status of the transmitted segment.

CALLING CONDITION
When the send window becomes zero.

GLOBAL VARIABLES AFFECTED
**************************************************************************/
INT1
OsmPersist (UINT4 u4TcbIndex, UINT1 u1Event)
{
    tTcb               *ptcb;
    INT1                i1RetValue;

    TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ENTRY_EXIT_TRC | TCP_OSM_TRC,
                 "TCP", "Entering OsmPersist for CONN ID - %d\n", u4TcbIndex);
    ptcb = &TCBtable[u4TcbIndex];
    if (u1Event != TCPE_PERSIST)
    {
        TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from OsmPersist. Returned OK\n");
        return TCP_OK;
    }

    if ((i1RetValue = OsmSendSegment (u4TcbIndex, TSF_REXMT)) == TCP_OK)
    {
        /* increase the persist timeout value */
        SetTCBpersist (u4TcbIndex,
                       MINIMUM ((GetTCBpersist (u4TcbIndex) << TCP_ONE),
                                TCP_MAXPERSIST));
    }
    if (SEQCMP (GetTCBsnext (u4TcbIndex), GetTCBsuna (u4TcbIndex)) != ZERO)
    {
        SetTCBostate (u4TcbIndex, TCPO_TRANSMIT);
        TmrInitTimer (&(ptcb->TcbRexmtTmr), TCP_RETRANSMIT_TIMER, u4TcbIndex);
        TcpTmrSetTimer (&(ptcb->TcbRexmtTmr),
                        MINIMUM (GetTCBrexmt (u4TcbIndex), TCP_MAXRETRANSMIT));
    }
    else
    {
        TmrInitTimer (&(ptcb->TcbPersistTmr), TCP_PERSIST_TIMER, u4TcbIndex);
        TcpTmrSetTimer (&(ptcb->TcbPersistTmr), GetTCBpersist (u4TcbIndex));
    }
    TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_OS_RES_TRC, "TCP",
                 "Initialized and started persist timer.\n");
    TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from OsmPersist. Returned %d\n", i1RetValue);
    /* reschedule an other persist event */
    return i1RetValue;
}

/*************************************************************************/
/*  Function Name   : OsmHowMuchDataIsLeft                               */
/*  Input(s)        : u4TcbIndex - Index to Tcb table                    */
/*  Output(s)       : None                                               */
/*  Returns         : No data bytes to send                              */
/*  Description     : Computes the number of data left in the send       */
/*                    buffer which have to transmitted.                  */
/* CALLING CONDITION : None                                              */
/* GLOBAL VARIABLES AFFECTED : None                                      */
/*************************************************************************/
INT4
OsmHowMuchDataIsLeft (UINT4 u4TcbIndex)
{
    INT4                i4DataToSend;

    TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering OsmHowMuchDataIsLeft for CONN ID - %d\n",
                 u4TcbIndex);
    i4DataToSend =
        GetTCBsuna (u4TcbIndex) + GetTCBsbcount (u4TcbIndex) -
        GetTCBsnext (u4TcbIndex);

    if (GetTCBcode (u4TcbIndex) & TCPF_SYN)
    {
        i4DataToSend++;
    }
    if ((GetTCBflags (u4TcbIndex) & TCBF_SNDFIN) &&
        !(GetTCBflags (u4TcbIndex) & TCBF_REXMTFIN))
    {
        i4DataToSend++;
    }
    if (i4DataToSend < ZERO)
    {
        i4DataToSend = ZERO;
    }
    TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from OsmHowMuchDataIsLeft. Returned %d\n", i4DataToSend);
    return i4DataToSend;
}

/*************************************************************************/
/*  Function Name   : OsmTransmit                                        */
/*  Input(s)        : u4TcbIndex - Index to Tcb table                    */
/*                    u1Event - Event to OSM                             */
/*  Output(s)       : None                                               */
/*  Returns         : TCP_OK  -- If all was ok.                          */
/*                    ERR -- If atleast one of the buffers to be         */
/*                    transmitted was lost due to some reason.           */
/*  Description     : 
   This function is responsible for transmitting segments.
   It first checks the event that caused it to be invoked. If it was some
event other than SEND, such as RETRANSMIT, it switches the output state
to the respective state.
   Otherwise, it proceeds by calculating number of bytes to be txed by calling
osm_how_much_data_left() function. If there is no data bytes to be sent (which
means that it might be just an ACK segment), it calls OsmSendSegment to
send it.
   If there is some data to send and the peer's window size is ZERO, it
sets the OUTPUT STATE to PERSIST. It also sets the PERSIST TIMER and calls
OsmSendSegment to txmt a segment.
   Finally, if the remote window size is non-zero, then it computes the no. of
bytes to be sent which is the minimum of Congestion window size and Remote
window size. Then it transmits until all the bytes have been transmitted or
until the window size has become zero.

CALLING CONDITION
   When there is data to be transmitted.

GLOBAL VARIABLES AFFECTED None.
**************************************************************************/
INT1
OsmTransmit (UINT4 u4TcbIndex, UINT1 u1Event)
{
    tTcb               *ptcb;
    INT4                i4DataToSend;
    UINT4               u4BytesToSend, u4Window;
    UINT4               u4BytesSent;
    INT1                i1RetValue;
    tOsixSysTime        CurrTime = 0;

    TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ENTRY_EXIT_TRC | TCP_OSM_TRC,
                 "TCP", "Entering OsmTransmit for CONN ID - %d \n", u4TcbIndex);
    i1RetValue = TCP_OK;
    ptcb = &TCBtable[u4TcbIndex];

    if (u1Event == TCPE_RETRANSMIT)
    {
        OsixGetSysTime (&CurrTime);
        if ((CurrTime - (ptcb->u4LastDataSentTime)) < TCP_MINRETRANSMIT)
        {
            TmrInitTimer (&(ptcb->TcbRexmtTmr), TCP_RETRANSMIT_TIMER,
                          u4TcbIndex);
            TcpTmrSetTimer (&(ptcb->TcbRexmtTmr),
                            MINIMUM (GetTCBrexmt (u4TcbIndex),
                                     TCP_MAXRETRANSMIT));
            TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from OsmTransmit after restarting ReTx Timer\n");
            return TCP_OK;
        }
        i1RetValue = OsmRetransmit (u4TcbIndex, TCPE_RETRANSMIT);
        SetTCBostate (u4TcbIndex, TCPO_RETRANSMIT);
        TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from OsmTransmit after invoking OsmRetransmit(). Returned %d\n",
                     i1RetValue);
        return i1RetValue;
    }

    i4DataToSend = OsmHowMuchDataIsLeft (u4TcbIndex);

    if (i4DataToSend == ZERO)
    {
        /* No data to send */
        if (GetTCBflags (u4TcbIndex) & TCBF_NEEDOUT)
        {
            i1RetValue = OsmSendSegment (u4TcbIndex, TSF_NEWDATA);
        }
        if (SEQCMP (GetTCBsnext (u4TcbIndex), GetTCBsuna (u4TcbIndex)) == ZERO)
        {
            /* All data sent have been acknowledged */
            TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from OsmTransmit. Returned %d\n", i1RetValue);
            return i1RetValue;
        }

        /* If Timer is not already active, then set it. */
        if (TmrIsActiveTimer (&(ptcb->TcbRexmtTmr)) == FALSE)
        {
            TmrInitTimer (&(ptcb->TcbRexmtTmr),
                          TCP_RETRANSMIT_TIMER, u4TcbIndex);
            TcpTmrSetTimer (&(ptcb->TcbRexmtTmr),
                            MINIMUM (GetTCBrexmt (u4TcbIndex),
                                     TCP_MAXRETRANSMIT));
            TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_OS_RES_TRC, "TCP",
                         "Initialized and started retransmit timer.\n");
        }
        TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from OsmTransmit. Returned %d\n", i1RetValue);
        return i1RetValue;
    }

    else
    {
        /*u4Window of the remote side is zero */
        if (GetTCBswindow (u4TcbIndex) == ZERO)
        {
            SetTCBostate (u4TcbIndex, TCPO_PERSIST);
            SetTCBpersist (u4TcbIndex, GetTCBrexmt (u4TcbIndex));

            i1RetValue = OsmSendSegment (u4TcbIndex, TSF_NEWDATA);

            TmrInitTimer (&(ptcb->TcbPersistTmr), TCP_PERSIST_TIMER,
                          u4TcbIndex);
            TcpTmrSetTimer ((tTcpTimer *) &
                            ((&(TCBtable[u4TcbIndex]))->TcbPersistTmr),
                            GetTCBpersist (u4TcbIndex));
            TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_OS_RES_TRC, "TCP",
                         "Persist timer started.\n");
            TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ENTRY_EXIT_TRC, "TCP",
                         "Exit from OsmTransmit. Returned %d\n", i1RetValue);
            return i1RetValue;
        }
    }

    /* Data has to be send and the receiver's window can accept it */
    SetTCBostate (u4TcbIndex, TCPO_TRANSMIT);
    u4Window = MINIMUM (GetTCBswindow (u4TcbIndex), GetTCBcwnd (u4TcbIndex));
    u4BytesToSend =
        (UINT4) GetTCBsnext (u4TcbIndex) - (UINT4) GetTCBsuna (u4TcbIndex);
    u4BytesSent = u4BytesToSend;
    while ((OsmHowMuchDataIsLeft (u4TcbIndex) > ZERO)
           && (u4BytesSent < u4Window))
    {
        /* OsmSendSegment might fails when memory resources drainout
         * break in this case */
        if ((i1RetValue = OsmSendSegment (u4TcbIndex, TSF_NEWDATA)) != TCP_OK)
            break;
        u4BytesSent =
            (UINT4) GetTCBsnext (u4TcbIndex) - (UINT4) GetTCBsuna (u4TcbIndex);
        /* for the syn packet (sent) if the ack packet came quickly then
         * GetTCBsuna could have again updated to GetTCBsnextGetTCBsnext value
         * So take care of this situation to avoid duplicate send in this loop
         */ 
        if (u4BytesSent == 0)
        {
            break;
        }
    }
    if (GetTCBstate (u4TcbIndex) != TCPS_FREE)
    {
        if ((GetTCBflags (u4TcbIndex) & TCBF_SNDFIN) &&
            !(GetTCBflags (u4TcbIndex) & TCBF_REXMTFIN) &&
            (u4BytesToSend == u4BytesSent))
        {
            SetTCBflags (u4TcbIndex, TCBF_FINNOTSENT);
        }
        OsixGetSysTime (&CurrTime);
        ptcb->u4LastDataSentTime = CurrTime;
        /* If Timer is not already active, then set it */
        if (TmrIsActiveTimer (&(ptcb->TcbRexmtTmr)) == FALSE)
        {
            TmrInitTimer (&(ptcb->TcbRexmtTmr), TCP_RETRANSMIT_TIMER,
                          u4TcbIndex);
            TcpTmrSetTimer (&(ptcb->TcbRexmtTmr),
                            MINIMUM (GetTCBrexmt (u4TcbIndex),
                                     TCP_MAXRETRANSMIT));
            TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_OS_RES_TRC, "TCP",
                         "Retransmit timer started.\n");
        }
    }
    TCP_MOD_TRC (GetTCBContext (u4TcbIndex), TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from OsmTransmit. Returned %d\n", i1RetValue);
    return i1RetValue;
}

/*************************************************************************/
/*  Function Name   : OsmRetransmit                                      */
/*  Input(s)        : u4TcbIndex - Index to Tcb table                    */
/*                    u1Event - Event to OSM                             */
/*  Output(s)       : None                                               */
/*  Returns         : TCP_OK/ERR                                         */
/*  Description     : 
When congestion occurs, delay increase, causing TCP to retransmit 
segments. In the worst case, retransmissions increase congestion and
produce congestion collapse. To avoid adding to the congestion TCP
uses multiplicative decrease strategy.
While no congestion or loss occurs, set the congestion window size to
the receivers advertised window size. That is use the recivers advertised 
window to determine how much data to send. Reduce the congestion window 
by half each time retransmission occurs, but never reduce it to less 
than the size required for one segment.

CALLING CONDITION

GLOBAL VARIABLES AFFECTED
None
**************************************************************************/
INT1
OsmRetransmit (UINT4 u4TcbIndex, UINT1 u1Event)
{
    tTcb               *pTcb;
    UINT4               u4NewCongestionValue;
    UINT4               u4Context = 0;
    UINT1               u1RetransmissionCnt;
    INT1                i1RetValue;
    UINT4               u4OldCwnd = 0;
    FLT4                f4Bw;

    pTcb = &TCBtable[u4TcbIndex];
    i1RetValue = TCP_OK;
    u4Context = GetTCBContext (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC | TCP_OS_RES_TRC,
                 "TCP",
                 "Entering OsmRetransmit for CONN ID - %d\n", u4TcbIndex);
    if (u1Event != TCPE_RETRANSMIT)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from OsmRetransmit as event is not RETRANMSIT. Returned OK.\n");
        return TCP_OK;            /* Ignore all other events like SEND, PERSIST */
    }

    u1RetransmissionCnt = (UINT1) (GetTCBrexmtcount (u4TcbIndex) + TCP_ONE);
    SetTCBrexmtcount (u4TcbIndex, u1RetransmissionCnt);

    if (u1RetransmissionCnt ==
        MAXIMUM (gpTcpContext[u4Context]->TcpConfigParms.
                 u2TcpMaxRetries >> TCP_ONE, TCP_RXMT_TIMING_OUT_COUNT))
    {
        HliSendAsyMsg (u4TcbIndex, TCPE_TIMINGOUT, (UINT1) ZERO, (UINT1) ZERO);
    }

    if (u1RetransmissionCnt >
        gpTcpContext[u4Context]->TcpConfigParms.u2TcpMaxRetries)
    {
        FsmAbortConn (pTcb, TCPE_TIMEDOUT);
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from OsmRetransmit. Returned OK.\n");
        return TCP_OK;
    }

    if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_SACK)
    {                            /* SACK is enabled */
        /* retransmit these SACK blocks */

        UINT1               u1NodeCounter, u1NthNode;
        tRxmt              *pNode;

        u1NodeCounter = ZERO;
        pNode = pTcb->pTcbRxmtQHdr;
        while (pNode != NULL)
        {
            u1NodeCounter++;
            if (!(pNode->u1SackFlag) &&
                (SEQCMP (pNode->i4Seq, GetTCBSackLargestSeq (u4TcbIndex)) <
                 ZERO))
            {
                u1NthNode = u1NodeCounter;
                OsmSendSegFromRxmtQ (pTcb, u1NthNode);
            }
            if (GetTCBSackLargestSeq (u4TcbIndex) == ZERO)
            {
                OsmSendSegFromRxmtQ (pTcb, u1NodeCounter);
                break;            /* transmit only 1st segment from the rxmtQ */
            }
            pNode = (tRxmt *) pNode->pNext;
        }

        /* turn off SACKed bits from all the nodes */
        OsmTurnOffSackBit (u4Context, pTcb->pTcbRxmtQHdr);

    }
    else
    {
        i1RetValue = OsmSendSegment (u4TcbIndex, TSF_REXMT);
    }

    /*Doubling the delay for each retransmission */
    TmrInitTimer (&(pTcb->TcbRexmtTmr), TCP_RETRANSMIT_TIMER, u4TcbIndex);
    TcpTmrSetTimer (&(pTcb->TcbRexmtTmr),
                    MINIMUM ((GetTCBrexmt (u4TcbIndex) <<
                              u1RetransmissionCnt), TCP_MAXRETRANSMIT));

    TCP_MOD_TRC (u4Context, TCP_OS_RES_TRC, "TCP",
                 "Retransmission timer value doubled and timer started.\n");
    if (gpTcpContext[u4Context]->TcpStatParms.u4RtoAlgorithm == RFC2988)
    {
        /*Reset to the initial value
         * Inoder to avoid bogus SRTT and RTTVAR as per section 5 of RFC-2988*/
        SetTCBsrt (u4TcbIndex, ZERO);
        SetTCBrtde (u4TcbIndex, ZERO);
    }
    if (GetTCBostate (u4TcbIndex) != TCPO_RETRANSMIT)
    {
        /* When this fn is executed by OsmTransmit procedure on a 
           RETRANSMIT event and not on expiry of Retransmission timer. */
        SetTCBssthresh (u4TcbIndex, GetTCBcwnd (u4TcbIndex));
    }

    /* This was changed from GetTCBssthresh to GetTCBcwnd since proper implementation
     * of Flight Size is min(congestion wnd, receiver wnd) */
    u4NewCongestionValue = MINIMUM (GetTCBswindow (u4TcbIndex),
                                    GetTCBcwnd (u4TcbIndex));

    if (GetTCBcwnd (u4TcbIndex) <=
        (gpTcpContext[u4Context]->u4TcpMinCwnd * GetTCBsmss (u4TcbIndex)))
    {
        /* Reduce it by half */
        SetTCBssthresh (u4TcbIndex, (u4NewCongestionValue >> TCP_ONE));
    }
    else
    {
        /* Apply HSTCP formula for decrementing ssthresh and Congestion Window */
        u4OldCwnd = (GetTCBcwnd (u4TcbIndex) / GetTCBsmss (u4TcbIndex));
        f4Bw = ((TCP_DEF_BW_AT_MAX_CWND - TCP_DEF_BW_AT_MIN_CWND) *
                (log (u4OldCwnd) -
                 log (gpTcpContext[u4Context]->u4TcpMinCwnd)) /
                (log (gpTcpContext[u4Context]->u4TcpMaxCwnd) -
                 log (gpTcpContext[u4Context]->u4TcpMinCwnd))) +
            TCP_DEF_BW_AT_MIN_CWND;
        SetTCBssthresh (u4TcbIndex,
                        (UINT4) ((1 - f4Bw) * u4NewCongestionValue));
    }

    if (GetTCBssthresh (u4TcbIndex) <
        (UINT4) (TCP_TWO * GetTCBsmss (u4TcbIndex)))
    {
        SetTCBssthresh (u4TcbIndex,
                        (UINT4) (TCP_TWO * GetTCBsmss (u4TcbIndex)));
    }

    SetTCBcwnd (u4TcbIndex,
                (GetTCBssthresh (u4TcbIndex) + (3 * GetTCBsmss (u4TcbIndex))));

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from OsmRetransmit. Returned %d\n", i1RetValue);
    return i1RetValue;
}

/*************************************************************************/
/*  Function Name   : LINOsmCopyIpOptions                                */
/*  Input(s)        : pIpOpts - pointer Ipoptions                        */
/*                    u4Index - Index to Tcb table                       */
/*  Output(s)       : None                                               */
/*  Returns         : TCP_OK                                             */
/*  Description     : Copy IP options from the TCB into the outgoing     */
/*                    segment                                            */
/*  CALLING CONDITION : When need to send a segment.                     */
/* GLOBAL VARIABLES AFFECTED : None                                      */
/*************************************************************************/
INT1
LINOsmCopyIpOptions (UINT1 *pIpOpts, UINT4 u4Index)
{
    UINT1               u1OptLen;
    UINT1               u1Count;
    UINT2               u2Temp;

    TCP_MOD_TRC (GetTCBContext (u4Index), TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering LINOsmCopyIpOptions for CONN ID - %d\n", u4Index);
    u1Count = ZERO;
    if (GetTCBvalopts (u4Index) & TCPO_SECURITY)
    {
        pIpOpts[u1Count++] = TCP_SEC_OPT_TYPE;    /* Write Option Type */
        pIpOpts[u1Count++] = TCP_SEC_OPT_LENGTH;    /* Write Option Length */

        u2Temp = OSIX_HTONS (GetTCBsecurity (u4Index));
        *((UINT2 *) (VOID *) (&pIpOpts[u1Count])) = u2Temp;
        u1Count = TCP_SEC_OPT_LENGTH;
    }

    if ((GetTCBvalopts (u4Index) & TCPO_IPOPTION) && (u1Count < MAX_IP_OPT_LEN))
    {
        MEMCPY ((UINT1 *) (&pIpOpts[u1Count]), (UINT1 *) GetTCBpipopt (u4Index),
                (UINT4) GetTCBipoptlen (u4Index));
        u1Count += GetTCBipoptlen (u4Index);
    }

    u1OptLen = OsmGetIpOptlen (u4Index);
    if (u1OptLen - u1Count)
    {
        pIpOpts[u1Count++] = ZERO;    /* Terminate Options with End Of Options */
    }
    TCP_MOD_TRC (GetTCBContext (u4Index), TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from LINOsmCopyIpOptions. Returned OK.\n");
    return TCP_OK;
}

/*************************************************************************/
/*  Function Name   : OsmGetIpOptlen                                     */
/*  Input(s)        : u4Index - Index to Tcb table                       */
/*  Output(s)       : None.                                              */
/*  Returns         : Length of IP options                               */
/*  Description     : Computes the length of the IP options to be sent.  */
/*  CALLING CONDITION : When a segment is to be sent.                    */
/*  GLOBAL VARIABLES AFFECTED None                                       */
/*************************************************************************/
UINT1
OsmGetIpOptlen (UINT4 u4Index)
{
    UINT1               u1OptLen;

    TCP_MOD_TRC (GetTCBContext (u4Index), TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering OsmGetIpOptlen for CONN ID - %d\n", u4Index);
    u1OptLen = ZERO;
    if (GetTCBvalopts (u4Index) & TCPO_IPOPTION)
    {
        u1OptLen += GetTCBipoptlen (u4Index);
    }

    if (GetTCBvalopts (u4Index) & TCPO_SECURITY)
    {
        u1OptLen += TCP_SEC_OPT_LENGTH;
    }

    if (u1OptLen % TCP_BYTES_FOR_WORD)
    {
        u1OptLen += TCP_BYTES_FOR_WORD - (u1OptLen % TCP_BYTES_FOR_WORD);    /* Convert to 4 byte boundary */
    }
    TCP_MOD_TRC (GetTCBContext (u4Index), TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from OsmGetIpOptlen. Returned %d\n", u1OptLen);
    return u1OptLen;
}

/*************************************************************************/
/*  Function Name   : OsmAddOptDuringEstb                                */
/*  Input(s)        : pTcb - Pointer to the Tcb                          */
/*                    pSegOut - pointer to the segment to send out       */
/*  Output(s)       : None                                               */
/*  Returns         : TCP_OK                                             */
/*  Description     : It fills the mss option in the TCP header and      */
/*                    updates the TCP header length also.                */
/* CALLING CONDITION : It is called when a segment is to be sent during  */
/*                     3 way handshake, i.e. when the SYN bit is set     */
/*                     in the outgoing segment.                          */
/*  GLOBAL VARIABLES AFFECTED none                                       */
/*************************************************************************/

INT1
OsmAddOptDuringEstb (tTcb * pTcb, tIpTcpHdr * pSegOut)
{
    UINT1              *pTcpOpt;
    UINT2               u2Mss;
    UINT4               u4TcbIndex, u4TcpOptIndex;
    UINT4               u4RcvWnd = 0;
    UINT4               u4Context = 0;
    UINT2               u2RcvWndScale = 0;

    u4TcbIndex = GET_TCB_INDEX (pTcb);
    u4TcpOptIndex = ZERO;
    u4Context = GetTCBContext (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering OsmAddOptDuringEstb for CONN ID - %d\n",
                 GET_TCB_INDEX (pTcb));

    u2Mss = GetTCBsmss (u4TcbIndex);
    pTcpOpt = pSegOut->tcp->au1TcpOpts;
    pTcpOpt[TCP_ZERO] = TCP_MSS;    /* Option type */
    pTcpOpt[TCP_ONE] = TCP_MSS_FOUR_BYTE_OPT;    /* Option length */
    *((UINT2 *) (VOID *) (&pTcpOpt[TCP_TWO])) = OSIX_HTONS (u2Mss);
    u4TcpOptIndex += TCP_MSS_FOUR_BYTE_OPT;

    /* Increment the header length to indicate 4 bytes added  */
    /* If  earlier header length = H, new header length is :  */
    /* ((((H >> 4) * 4) + 4) / 4) << 4                        */
    pSegOut->tcp->u1Hlen += TCP_MSS_HEADER_LEN;

    /* Simultaneous OPEN not taken care of */

    if (GetTCBstate (u4TcbIndex) == TCPS_SYNSENT)
    {

        /*
         * if option is configured, add the corresponding option
         * in the TCP header & modify the header length
         */

        if (gpTcpContext[u4Context]->TcpConfigParms.i4TcpTSOpt == TCP_TSCONF)
        {
            UINT4               u4TSVal;
            UINT4               u4TSecr;

            /* add the following values to the TS field */
            /* u4TSVal is in seconds */
            TCP_GET_SYS_TIME ((tOsixSysTime *) & u4TSVal);
            u4TSecr = ZERO;

            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;

            pTcpOpt[u4TcpOptIndex++] = TCP_TS;
            pTcpOpt[u4TcpOptIndex++] = TS_OPT_LEN;
            *((UINT4 *) (VOID *) (&pTcpOpt[u4TcpOptIndex])) =
                OSIX_HTONL (u4TSVal);
            u4TcpOptIndex += TCP_BYTES_FOR_WORD;
            *((UINT4 *) (VOID *) (&pTcpOpt[u4TcpOptIndex])) =
                OSIX_HTONL (u4TSecr);
            u4TcpOptIndex += TCP_BYTES_FOR_WORD;

            /* modify the TCP Header Length */
            pSegOut->tcp->u1Hlen += TCP_HDR_LEN_WITH_TSTAMP;
        }

        /* Add SACK permitted option only if MD5 option is
         * not enabled on the TCB */
        if ((gpTcpContext[u4Context]->TcpConfigParms.i4TcpAckOpt ==
             TCP_SACKCONF) && (GetTCBmd5keylen (u4TcbIndex) == TCP_ZERO))
        {
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;

            /* add SACK-permitted option */
            pTcpOpt[u4TcpOptIndex++] = TCP_SACKP;
            pTcpOpt[u4TcpOptIndex++] = SACK_PERM_OPT_LEN;

            /* modify the TCP Header Length */
            pSegOut->tcp->u1Hlen += TCP_HDR_LEN_WITH_SACK;
        }

        /* Add BW permitted option only if MD5 option is
         * not enabled on the TCB */
        if ((gpTcpContext[u4Context]->TcpConfigParms.i4TcpBWOpt == TCP_BWCONF)
            && (GetTCBmd5keylen (u4TcbIndex) == TCP_ZERO))
        {
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;

            /* add BW-permitted option. use small wnd during negotiation */
            pTcpOpt[u4TcpOptIndex++] = TCP_BWP;
            pTcpOpt[u4TcpOptIndex++] = BW_PERM_OPT_LEN;
            *((UINT4 *) (VOID *) (&pTcpOpt[u4TcpOptIndex])) = ZERO;
            u4TcpOptIndex += TCP_BYTES_FOR_WORD;

            /* modify the TCP Header Length */
            pSegOut->tcp->u1Hlen += TCP_HDR_WITH_BWPERM_OPT;
        }

        if (gpTcpContext[u4Context]->TcpConfigParms.i4TcpWSOpt == TCP_WSCONF)
        {
            /* Add the Window scale option */
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;

            pTcpOpt[u4TcpOptIndex++] = TCP_WS;
            pTcpOpt[u4TcpOptIndex++] = WS_OPT_LEN;

            u4RcvWnd = TCP_MAX_RECV_BUF_SIZE;

            /* Get the value foe Window scale parameter
             * from the maximum Window size.
             * Do not set it in the SetTCBRecvWindowScale 
             * as this is initialized to 0 and is set only once
             * Window scale is confirmed by the other side */

            pTcpOpt[u4TcpOptIndex++] = (UINT1) u2RcvWndScale;
            /* modify the TCP Header Length */
            pSegOut->tcp->u1Hlen += TCP_WS_HEADER_LEN;
        }

        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from OsmAddOptDuringEstb. Returned OK.\n");
        return TCP_OK;
    }

    if (GetTCBstate (u4TcbIndex) == TCPS_SYNRCVD)
    {
        /*
         * If option is enabled, add the corresponding option
         * in the TCP header & modify the header length.
         * Option flag is enabled when the peer TCP is
         * trying to negotiate it.
         * In case of TS, update TCB.LastAckSnt = RCV.NXT
         */

        if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_TS)
        {
            UINT4               u4TSVal = ZERO;    /* to remove warning */
            UINT4               u4TSecr;

            /* add the following values to the TS field */
            /* u4TSVal is in seconds */
            TCP_GET_SYS_TIME ((tOsixSysTime *) & u4TSVal);
            u4TSecr = GetTCBTSRecent (u4TcbIndex);

            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;

            pTcpOpt[u4TcpOptIndex++] = TCP_TS;
            pTcpOpt[u4TcpOptIndex++] = TS_OPT_LEN;
            *((UINT4 *) (VOID *) (&pTcpOpt[u4TcpOptIndex])) =
                OSIX_HTONL (u4TSVal);
            u4TcpOptIndex += TCP_TSTAMP_FIELD_LEN;
            *((UINT4 *) (VOID *) (&pTcpOpt[u4TcpOptIndex])) =
                OSIX_HTONL (u4TSecr);
            u4TcpOptIndex += TCP_TSTAMP_FIELD_LEN;

            /* modify the TCP Header Length */
            pSegOut->tcp->u1Hlen += TCP_HDR_LEN_WITH_TSTAMP;

            /* Update TCBTSLastAckSent */
            SetTCBTSLastAckSent (u4TcbIndex, GetTCBrnext (u4TcbIndex));
        }

        if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_SACK)
        {
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;

            /* add SACK-permitted option */
            pTcpOpt[u4TcpOptIndex++] = TCP_SACKP;
            pTcpOpt[u4TcpOptIndex++] = SACK_PERM_OPT_LEN;

            /* modify the TCP Header Length */
            pSegOut->tcp->u1Hlen += TCP_HDR_LEN_WITH_SACK;
        }

        if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_BW)
        {
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;

            /* add BW-permitted option. use small wnd during negotiation */
            pTcpOpt[u4TcpOptIndex++] = TCP_BWP;
            pTcpOpt[u4TcpOptIndex++] = BW_PERM_OPT_LEN;
            *((UINT4 *) (VOID *) (&pTcpOpt[u4TcpOptIndex])) = ZERO;
            u4TcpOptIndex += TCP_BYTES_FOR_WORD;

            /* modify the TCP Header Length */
            pSegOut->tcp->u1Hlen += TCP_HDR_WITH_BWPERM_OPT;
        }

        if (GetTCBOptFlag (u4TcbIndex) & TCBOPF_WS)
        {
            /* Add the Window scale option */
            pTcpOpt[u4TcpOptIndex++] = TCP_NOOP;

            pTcpOpt[u4TcpOptIndex++] = TCP_WS;
            pTcpOpt[u4TcpOptIndex++] = WS_OPT_LEN;

            u4RcvWnd = TCP_MAX_RECV_BUF_SIZE;
            /* Get the value foe Window scale parameter
             * from the maximum window size.
             * Do not set it in the SetTCBRecvWindowScale 
             * as this is initialized to 0 and is set only once
             * Window scale is confirmed by the other side */

            pTcpOpt[u4TcpOptIndex++] = (UINT1) u2RcvWndScale;
            /* modify the TCP Header Length */
            pSegOut->tcp->u1Hlen += TCP_WS_HEADER_LEN;
        }

        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from OsmAddOptDuringEstb. Returned OK.\n");
        return TCP_OK;
    }
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from OsmAddOptDuringEstb. Returned Error.\n");
    UNUSED_PARAM (u4RcvWnd);
    return ERR;
}
