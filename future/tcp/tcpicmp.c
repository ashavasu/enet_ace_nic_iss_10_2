/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: tcpicmp.c,v 1.8 2013/03/08 13:30:11 siva Exp $
 *
 **********************************************************************/
/* SOURCE FILE HEADER
 *
 * ----------------------------------------------------------------------------
 *  FILE NAME             : tcpicmp.c
 * 
 *  PRINCIPAL AUTHOR      : Sri. Chellappa.
 *
 *  SUBSYSTEM NAME        : TCP
 *
 *  MODULE NAME           : ICMP Message Handler
 *
 *  LANGUAGE              : C
 *
 *  TARGET ENVIRONMENT    : Any
 *
 *  DATE OF FIRST RELEASE :
 * 
 *  DESCRIPTION           : This file contains routines to handle ICMP   
 *                          messages.
 *
 * ---------------------------------------------------------------------------
 *
 *
 *  CHANGE RECORD :
 *
 * ---------------------------------------------------------------------------
 *   VERSION        AUTHOR/DATE           DESCRIPTION OF CHANGE
 * ---------------------------------------------------------------------------
 *
 *             Sri. Chellapa        Created.
 *             13/05/96       
* ------------------------------------------------------------------------*
*     1.1      Saravanan.M    Implementation of                           *
*              Purush            - Non Blocking Connect                   * 
*                                - Non Blocking Accept                    *
*                                - IfMsg Interface between TCP and IP     *
*                                - One Queue per Socket                   *
* ------------------------------------------------------------------------*/

#include "tcpinc.h"
#include "ip.h"                    /* FutureIP header file */

/*********************************************************************/
/*  Function Name   : IcmpHandleMessage                              */
/*  Input(s)        : None.                                          */
/*  Output(s)       : None.                                          */
/*  Returns         : SUCCESS or FAILURE                             */
/*  Action          :                                                */
/*    Called when TCP wants to handle the ICMP error message         */
/*    that arrived.                                                  */
/*********************************************************************/
INT4
IcmpHandleMessage (UINT1 *pSeg, UINT4 u4TcbIndex)
{
    tTcpIcmpParams     *pParms = NULL;
    UINT4               u4Context;
    UINT1               u1IcmpType;
    UINT1               u1IcmpCode;

    u4Context = GetTCBContext (u4TcbIndex);

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Extering IcmpHandleMessage. \n");
    pParms = (tTcpIcmpParams *) (VOID *) pSeg;
    u1IcmpType = pParms->u1Type;
    u1IcmpCode = pParms->u1Code;

    HliSendAsyMsg (u4TcbIndex, TCPE_ICMP_MSG_ARRIVED, u1IcmpType, u1IcmpCode);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from IcmpHandleMessage. Returned SUCCESS.\n");
    return TCP_OK;
}

/********************************************************************/
/* Function Name  : iptask_icmp_tcp                                 */
/* Description    : Call back function For ICMP, receive the mesgs  */
/*                  from icmp and sends to the TCP queue            */
/* Input(s)       : pBuf, u4Src, i1Type, i1Code                     */
/* Output(s)      : None                                            */
/* Returns        : SUCCESS, FAILURE                                */
/* Calling Cond'n : Called from ICMP if there is a message to send  */
/*                  to Applications                                 */
/********************************************************************/
#ifdef IP_WANTED
INT4
iptask_icmp_tcp (tCRU_BUF_CHAIN_HEADER * pBuf,
                 UINT4 u4Src, INT1 i1Type, INT1 i1Code)
{
    return iptask_icmp_tcp_in_cxt (VCM_DEFAULT_CONTEXT, pBuf, u4Src,
                                   i1Type, i1Code);
}

/********************************************************************/
/* Function Name  : IptaskIcmpTcp                                   */
/* Description    : Call back function For ICMP, receive the mesgs  */
/*                  from icmp and sends to the TCP queue            */
/* Input(s)       : u4Context, pBuf, u4Src, i1Type, i1Code          */
/* Output(s)      : None                                            */
/* Returns        : SUCCESS, FAILURE                                */
/* Calling Cond'n : Called from ICMP if there is a message to send  */
/*                  to Applications                                 */
/********************************************************************/
INT4
iptask_icmp_tcp_in_cxt (UINT4 u4Context, tCRU_BUF_CHAIN_HEADER * pBuf,
                        UINT4 u4Src, INT1 i1Type, INT1 i1Code)
{
    tTcpIcmpParams     *pParms;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering IptaskIcmpTcp.\n");
    pParms = (tTcpIcmpParams *) MemAllocMemBlk (gTcpIcmpParamsPoolId);
    if (pParms == NULL)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from iptask_icmp_tcp_in_cxt. Unable to allocate icmp parms\n");
        return FAILURE;
    }

    pParms->pBuf = pBuf;
    pParms->u1Cmd = IP_ICMP_DATA;
    pParms->IpSrc.u4_addr[3] = u4Src;
    pParms->u1Type = (UINT1) i1Type;
    pParms->u1Code = (UINT1) i1Code;
    pParms->u4ContextId = u4Context;

    if (OsixQueSend (gTcpQId, (UINT1 *) &pParms, OSIX_DEF_MSG_LEN))
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MemReleaseMemBlock (gTcpIcmpParamsPoolId, (UINT1 *) pParms);
        TCP_MOD_TRC (u4Context,
                     TCP_ENTRY_EXIT_TRC | TCP_OS_RES_TRC | TCP_ALL_FAIL_TRC |
                     TCP_BUF_TRC, "TCP",
                     "Exit from IptaskIcmpTcp. Send to queue failed. Buffer released. Returned FAILURE.\n");
        return FAILURE;
    }
    TCP_MOD_TRC (u4Context, TCP_OS_RES_TRC, "TCP",
                 "Send to queue succeeded. \n");
    OsixEvtSend (gTcpTaskId, TCP_IP_IF_EVENT);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from IptaskIcmpTcp. Returned SUCCESS.\n");
    return SUCCESS;
}
#endif

/********************************************************************/
/* Function Name  : Tcp6RcvIcmp6Pkt                                 */
/* Description    : Call back function For ICMP, receive the mesgs  */
/*                  from icmp and sends to the TCP queue            */
/* Input(s)       : pBuf, SrcAddr, i1Type, i1Code                   */
/* Output(s)      : None                                            */
/* Returns        : SUCCESS, FAILURE                                */
/* Calling Cond'n : Called from ICMP if there is a message to send  */
/*                  to Applications                                 */
/********************************************************************/
#ifdef IP6_WANTED
INT4
Tcp6RcvIcmp6Pkt (UINT4 u4Context, tCRU_BUF_CHAIN_HEADER * pBuf,
                 tIp6Addr SrcAddr, UINT1 u1Type, UINT1 u1Code, UINT2 u2Len)
{
    tTcpIcmpParams     *pParms = NULL;

    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Entering Tcp6RcvIcmp6Pkt.\n");

    pParms = (tTcpIcmpParams *) MemAllocMemBlk (gTcpIcmpParamsPoolId);
    if (pParms == NULL)
    {
        TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                     "Exit from Tcp6RcvIcmp6Pkt. Unable to allocate icmp parms\n");
        return FAILURE;
    }

    pParms->pBuf = pBuf;
    pParms->u1Cmd = IP_ICMP_DATA;
    pParms->u1Type = u1Type;
    pParms->u1Code = u1Code;
    UNUSED_PARAM (u2Len);
    MEMCPY (&(pParms->IpSrc), &SrcAddr, sizeof (tIp6Addr));
    pParms->u4ContextId = u4Context;

    if (OsixQueSend (gTcpQId, (UINT1 *) &pParms, OSIX_DEF_MSG_LEN))
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MemReleaseMemBlock (gTcpIcmpParamsPoolId, (UINT1 *) pParms);
        TCP_MOD_TRC (u4Context,
                     TCP_ENTRY_EXIT_TRC | TCP_OS_RES_TRC | TCP_ALL_FAIL_TRC |
                     TCP_BUF_TRC, "TCP",
                     "Exit from Tcp6RcvIcmp6Pkt. Send to queue failed. Buffer released. Returned FAILURE.\n");
        return FAILURE;
    }

    TCP_MOD_TRC (u4Context, TCP_OS_RES_TRC, "TCP",
                 "Send to queue succeeded. \n");
    OsixEvtSend (gTcpTaskId, TCP_IP_IF_EVENT);
    TCP_MOD_TRC (u4Context, TCP_ENTRY_EXIT_TRC, "TCP",
                 "Exit from Tcp6RcvIcmp6Pkt. Returned SUCCESS.\n");
    return SUCCESS;
}
#endif
