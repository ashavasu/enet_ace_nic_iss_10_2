#####################################################################
# Copyright (C) 2010 Aricent Inc . All Rights Reserved                   #
#
# $Id: make.h,v 1.9 2014/09/08 11:18:13 siva Exp $
#
# Description: header file for IKE make file. 
#
#####################################################################


###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

GLOBAL_OPNS = ${SYSTEM_COMPILATION_SWITCHES} 


############################################################################
#                         Directories                                      #
############################################################################

IKE_DIR = ${BASE_DIR}/ike
IKE_INCLUDE_DIR = ${BASE_DIR}/ike/inc
IKE1_INCLUDE_DIR = ${BASE_DIR}/ike/ikev1/inc
IKE2_INCLUDE_DIR = ${BASE_DIR}/ike/ikev2/inc
ifeq (${PACKAGE}, METRO_E)
ifneq (,$(filter YES ,${WLC} ${WTP}))
ifeq (${OPENSOURCEV101C}, YES)
CRYPTO_BASE_DIR = ${BASE_DIR}/../opensource/v1_0_1c/ssl/crypto
else
CRYPTO_BASE_DIR = ${BASE_DIR}/../opensource/ssl/crypto
endif
else
CRYPTO_BASE_DIR = ${BASE_DIR}/../opensource/ssl/crypto
endif
else
CRYPTO_BASE_DIR = ${BASE_DIR}/../opensource/ssl/crypto
endif
IKE_SRC_DIR = ${BASE_DIR}/ike/src
IKE1_SRC_DIR = ${BASE_DIR}/ike/ikev1/src
IKE2_SRC_DIR = ${BASE_DIR}/ike/ikev2/src

IKE_OBJ_DIR = ${BASE_DIR}/ike/obj
IKE1_OBJ_DIR = ${BASE_DIR}/ike/ikev1/obj
IKE2_OBJ_DIR = ${BASE_DIR}/ike/ikev2/obj

IKE_DPNDS   =     ${IKE1_INCLUDE_DIR}/ikeprocess.h\
                  ${IKE1_INCLUDE_DIR}/ikeconstruct.h
                  
IKE_DPNDS  +=     ${IKE_INCLUDE_DIR}/ikegen.h\
                  ${IKE_INCLUDE_DIR}/ikenm.h\
                  ${IKE_INCLUDE_DIR}/fsikelow.h\
                  ${IKE_INCLUDE_DIR}/iketmr.h\
                  ${IKE_INCLUDE_DIR}/iketdfs.h\
                  ${IKE_INCLUDE_DIR}/ikestat.h\
                  ${IKE_INCLUDE_DIR}/iketrace.h\
                  ${IKE_INCLUDE_DIR}/ikedhgroup.h\
                  ${IKE_INCLUDE_DIR}/ikecertsave.h\
                  ${IKE_INCLUDE_DIR}/ikersa.h\
                  ${IKE_INCLUDE_DIR}/ikedsa.h\
                  ${IKE_INCLUDE_DIR}/ikex509.h\
                  ${IKE_INCLUDE_DIR}/fsopenssl.h\
                  ${IKE_INCLUDE_DIR}/ikecrypto.h\
                  ${IKE_INCLUDE_DIR}/auth_db.h\
                  ${IKE_INCLUDE_DIR}/authcli.h\
                  ${IKE_INCLUDE_DIR}/ikecert.h\
                  ${IKE_INCLUDE_DIR}/fsikewr.h\
                  ${IKE_INCLUDE_DIR}/ikeextern.h\
                  ${IKE_INCLUDE_DIR}/fsikedb.h\
                  ${IKE_INCLUDE_DIR}/ikememmac.h\
                  ${IKE_DIR}/make.h\
                  ${IKE_DIR}/Makefile
 
#############################################################################

